/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package me.tangke.slidemenu;

public final class R {
    public static final class attr {
        public static final int edgeSlide = 0x7f01015a;
        public static final int edgeSlideWidth = 0x7f01015b;
        public static final int interpolator = 0x7f01015d;
        public static final int layout_role = 0x7f01015e;
        public static final int primaryShadowDrawable = 0x7f010158;
        public static final int primaryShadowWidth = 0x7f010156;
        public static final int secondaryShadowDrawable = 0x7f010159;
        public static final int secondaryShadowWidth = 0x7f010157;
        public static final int slideDirection = 0x7f01015c;
        public static final int slideMenuStyle = 0x7f010005;
    }
    public static final class id {
        public static final int content = 0x7f0e0077;
        public static final int left = 0x7f0e003d;
        public static final int primaryMenu = 0x7f0e0078;
        public static final int right = 0x7f0e003e;
        public static final int secondaryMenu = 0x7f0e0079;
    }
    public static final class styleable {
        public static final int[] SlideMenu = { 0x7f010156, 0x7f010157, 0x7f010158, 0x7f010159, 0x7f01015a, 0x7f01015b, 0x7f01015c, 0x7f01015d };
        public static final int[] SlideMenu_Layout = { 0x7f01015e };
        public static final int SlideMenu_Layout_layout_role = 0;
        public static final int SlideMenu_edgeSlide = 4;
        public static final int SlideMenu_edgeSlideWidth = 5;
        public static final int SlideMenu_interpolator = 7;
        public static final int SlideMenu_primaryShadowDrawable = 2;
        public static final int SlideMenu_primaryShadowWidth = 0;
        public static final int SlideMenu_secondaryShadowDrawable = 3;
        public static final int SlideMenu_secondaryShadowWidth = 1;
        public static final int SlideMenu_slideDirection = 6;
    }
}
