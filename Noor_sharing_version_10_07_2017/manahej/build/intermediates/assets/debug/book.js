//re written by raviteja
function getSelectedNode() {
    if (document.selection)
        return document.selection.createRange().parentElement();
    else {
        var selection = window.getSelection();
        if (selection.rangeCount > 0)
            return selection.getRangeAt(0).startContainer.parentNode;
    }
}

function getOffset(el) {
    var rect = el.getBoundingClientRect();
    return rect;

}

function enterKeyPressHandler(evt) {
    var sel, range, br, addedBr = false;
    var charCode;


    if (evt) {
        evt = evt;
    } else {
        evt = window.event;
    }

    if (evt.which) {
        charCode = evt.which
    } else {
        charCode = evt.keyCode;
    }


    if (charCode == 13) {
        if (typeof window.getSelection != "undefined") {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();
                br = document.createElement("br");
                range.insertNode(br);
                range.setEndAfter(br);
                range.setStartAfter(br);
                sel.removeAllRanges();
                sel.addRange(range);
                addedBr = true;
                var el;


                // if (getSelectedNode().parentElement.getAttribute("style") == null) {
                //     el = //document.querySelector('div[style="' + getSelectedNode().getAttribute("style") + '"]').nextElementSibling;

                // } else {
                //     el = document.querySelector('div[style="' + getSelectedNode().parentElement.getAttribute("style") + '"]').nextElementSibling;
                // }

                var selection = window.getSelection();
                if (selection && selection.anchorNode && selection.anchorNode.parentElement)
                    el = selection.anchorNode.parentElement;

                var next = el.nextElementSibling;
                i = 1;

                while (el) {
                    if (el.nodeName == "DIV") {
                        var pos = getOffset(el);
                      //  var px1 = parseInt(pos.top) + pos.height + "px";
                      var px1 = parseInt(pos.top)+ "px";
                        console.log(px1);
                        el.style.top = px1;
                        el.style.bottom = '';

                    };
                    el = el.nextSibling;
                    i++;
                }


                //$(getSelectedNode()).nextAll().each(function () {
                //    var px1 = parseInt($(this).css("top")) + $(this).outerHeight() + "px";
                //$(this).css("top", px1);
                //});



            }
        } else if (typeof document.selection != "undefined") {
            sel = document.selection;
            if (sel.createRange) {
                range = sel.createRange();
                range.pasteHTML("<br>");
                range.select();
                addedBr = true;

            }
        }

        // If successful, prevent the browser's default handling of the keypress
        if (addedBr) {
            if (typeof evt.preventDefault != "undefined") {
                evt.preventDefault();
            } else {
                evt.returnValue = false;
            }
        }
    }
}


var el = document.getElementById("content");
if (el != null) {
    if (typeof el.addEventListener != "undefined") {
        el.addEventListener("keypress", enterKeyPressHandler, false);
    } else if (typeof el.attachEvent != "undefined") {
        el.attachEvent("onkeypress", enterKeyPressHandler);
    }
}
