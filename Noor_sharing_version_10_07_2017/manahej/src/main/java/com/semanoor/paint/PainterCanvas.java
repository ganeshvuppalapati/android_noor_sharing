package com.semanoor.paint;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.Elesson;


import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.mqtt.MQTTSubscriptionService;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.paint.PainterThread.State;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UndoManager;
import com.semanoor.source_sboookauthor.UndoRedoObjectAction;
import com.semanoor.source_sboookauthor.UndoRedoObjectData;


import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.WindowManager;

import org.json.JSONObject;

public class PainterCanvas extends SurfaceView implements Callback {

	public PainterThread mThread;
	private Bitmap mBitmap;

	public BrushPreset getmPreset() {
		return mPreset;
	}

	public void setmPreset(BrushPreset mPreset) {
		this.mPreset = mPreset;
	}

	private BrushPreset mPreset;
	private State mThreadState;

	private boolean mIsSetup;
	private boolean mIsChanged;
	private boolean mUndo;


	public static final int BLUR_TYPE_NONE = 0;
	public static final int BLUR_TYPE_NORMAL = 1;
	public static final int BLUR_TYPE_INNER = 2;
	public static final int BLUR_TYPE_OUTER = 3;
	public static final int BLUR_TYPE_SOLID = 4;

	public String objContentPath;
	private Context context;
	int deviceWidth;
	int deviceHeight;
	private UndoManager undoManager;
	String topicName;

	public PainterCanvas(Context _context, String objContent, AttributeSet attrs) {
		super(_context, attrs);

		this.context = _context;
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		this.setBackgroundColor(Color.TRANSPARENT);
		this.setZOrderOnTop(true);
		holder.setFormat(PixelFormat.RGBA_8888);
		mPreset = new BrushPreset(BrushPreset.PEN, Color.BLACK);
		mThreadState = new State();
		this.objContentPath = objContent;
		undoManager = new UndoManager(null, this);
		getScreenResolution(context);
		topicName = ManahijApp.getInstance().getPrefManager().getMainChannel();
		//this.setFocusable(true);

	}

	/*public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
			getThread().freeze();
		} else {
			if (!isSetup()) {
				getThread().activate();
			} else {
				getThread().setup();
			}
		}
	}*/

	private Bitmap getSavedBitmapFromPath(String frompath) {
		Bitmap savedBitmap = null;
		if (new File(frompath).exists()) {
			savedBitmap = BitmapFactory.decodeFile(frompath);
		}
		return savedBitmap;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		if (mBitmap == null) {

			mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

			getThread().setBitmap(mBitmap, true);
			Bitmap bitmap = getSavedBitmapFromPath(objContentPath);

			if (bitmap != null) {
				float bitmapWidth = bitmap.getWidth();
				float bitmapHeight = bitmap.getHeight();
				float scale = 1.0f;

				Matrix matrix = new Matrix();
				if (width != bitmapWidth || height != bitmapHeight) {
					if (width == bitmapHeight || height == bitmapWidth) {
						if (width > height) {
							matrix.postRotate(-90, width / 2, height / 2);
						} else if (bitmapWidth != bitmapHeight) {
							matrix.postRotate(90, width / 2, height / 2);
						} else {
							if (((Activity) context).getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
								matrix.postRotate(-90, width / 2, height / 2);
							}
						}
					} else {
						if (((Activity) context).getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
							if (bitmapWidth > bitmapHeight
									&& bitmapWidth > width) {
								scale = (float) width / bitmapWidth;
							} else if (bitmapHeight > bitmapWidth
									&& bitmapHeight > height) {
								scale = (float) height / bitmapHeight;
							}
						} else {
							if (bitmapHeight > bitmapWidth
									&& bitmapHeight > height) {
								scale = (float) height / bitmapHeight;
							} else if (bitmapWidth > bitmapHeight
									&& bitmapWidth > width) {
								scale = (float) width / bitmapWidth;
							}
						}
					}

					if (scale == 1.0f) {
						matrix.preTranslate((width - bitmapWidth) / 2,
								(height - bitmapHeight) / 2);
					} else {
						matrix.postScale(scale, scale, bitmapWidth / 2,
								bitmapHeight / 2);
						matrix.postTranslate((width - bitmapWidth) / 2,
								(height - bitmapHeight) / 2);
					}
				}
				getThread().restoreBitmap(bitmap, matrix);
			}
		} else {
			getThread().setBitmap(mBitmap, false);
		}

		getThread().setPreset(mPreset);
		if (!isSetup()) {
			getThread().activate();
		} else {
			getThread().setup();
		}
	}

	public void surfaceCreated(SurfaceHolder holder) {
		getThread().on();
		getThread().start();
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		getThread().off();
		while (retry) {
			try {
				getThread().join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}

		mThread = null;
	}

	public void clearView() {
		UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
		uRPrevObjData.setPaintBuffer(getThread().mState.redoBuffer);
		createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_PAINT, Globals.OBJECT_PREVIOUS_STATE);

		getThread().clearBitmap();

		UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
		uRCurrObjData.setPaintBuffer(getThread().mState.undoBuffer);
		createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_PAINT, Globals.OBJECT_PRESENT_STATE);
	}

	ArrayList<Path> drawPath = new ArrayList<>();
	Path path;
	MQTTSubscriptionService service = new MQTTSubscriptionService();
	JSONObject obj = new JSONObject();

	public boolean onTouchEvent(MotionEvent event) {
		if (!getThread().isReady()) {
			return false;
		}
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:


				try {

					obj.put("type", Constants.DRAWING);
					obj.put("action", Constants.ACTION_DOWN);
					obj.put("device_height", deviceHeight);
					obj.put("device_width", deviceWidth);
					obj.put("x", event.getX());
					obj.put("y", event.getY());
					obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
					service.publishMsg(topicName, obj.toString(), context);
				} catch (Exception e) {
					Log.e("Exception", e.toString());
				}
				path = new Path();
				path.moveTo((int) event.getX(), (int) event.getY());
				drawPath.add(path);

				/************************/


				mIsChanged = true;
				getThread().drawBegin();
				UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
				uRPrevObjData.setPaintBuffer(getThread().mState.undoBuffer);
				createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_PAINT, Globals.OBJECT_PREVIOUS_STATE);
				mUndo = false;
				//Here passing action down event to receiver......


				break;
			case MotionEvent.ACTION_MOVE:
				path.lineTo((int) event.getX(), (int) event.getY());
				getThread().draw1(path);
				//  getThread().draw((int) event.getX(), (int) event.getY());
				try {
					obj.put("type", Constants.DRAWING);
					obj.put("action", Constants.ACTION_MOVE);
					obj.put("x", event.getX());
					obj.put("y", event.getY());
					obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
					service.publishMsg(topicName, obj.toString(), context);
				} catch (Exception e) {
					Log.e("Exception", e.toString());
				}
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
				getThread().drawEnd();
				UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
				uRCurrObjData.setPaintBuffer(getThread().mState.redoBuffer);
				createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_PAINT, Globals.OBJECT_PRESENT_STATE);

				break;
		}
		return true;
	}


	public void createAndRegisterUndoRedoObjectData(UndoRedoObjectData uRObjData, String actionName, String objState) {
		UndoRedoObjectAction urObJAction = new UndoRedoObjectAction();
		urObJAction.setRuObjActionName(actionName);
		urObJAction.setRuObject(uRObjData);
		urObJAction.setRuObjState(objState);
		undoManager.setUndoRedoAction(urObJAction);
	}

	/**
	 * this sets the undo and redo button to enable or disable
	 *
	 * @param eDbtnUndo
	 * @param eDbtnRedo
	 */
	public void setUndoRedoBtnEnableOrDisable(final boolean eDbtnUndo, final boolean eDbtnRedo) {
		if (context instanceof BookViewActivity) {
			((BookViewActivity) context).btnPaintUndo.setEnabled(eDbtnUndo);
			((BookViewActivity) context).btnPaintRedo.setEnabled(eDbtnRedo);
		} else if (context instanceof BookViewReadActivity) {
			((BookViewReadActivity) context).btnPaintUndo.setEnabled(eDbtnUndo);
			((BookViewReadActivity) context).btnPaintRedo.setEnabled(eDbtnRedo);
		} else if (context instanceof Elesson) {
			((Elesson) context).btn_paint_undo.setEnabled(eDbtnUndo);
			((Elesson) context).btn_paint_redo.setEnabled(eDbtnRedo);
		}
	}

	/**
	 * this performs the undo and redo action for the object
	 *
	 * @param undoRedoObject
	 */
	public void performUndoRedoActioForObjects(UndoRedoObjectAction undoRedoObject) {
		if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_PAINT) {
			UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
			byte[] paintBuffer = uRObjectData.getPaintBuffer();
			getThread().undoRedoPaintBuffer(paintBuffer);
		}
	}

	public void paintUndo() {
		undoManager.shiftPointerOnUndo();
	}

	public void paintRedo() {
		undoManager.shiftPointerOnRedo();
	}

	public PainterThread getThread() {
		if (mThread == null) {
			mThread = new PainterThread(getHolder());
			mThread.setState(mThreadState);
		}
		return mThread;
	}

	/*
     * TODO: Make save quality changeable (possibly with a slider on a save
     * dialogue?)
     */
	public void saveBitmap(String pictureName) throws FileNotFoundException {
		synchronized (getHolder()) {
			if (getThread().getBitmap() != null) {
				FileOutputStream fos = new FileOutputStream(pictureName);
				getThread().getBitmap().compress(CompressFormat.PNG, 100, fos);
			}
		}
	}

	/*
     * NOTE: This is commented simply because it is unused as of now. If anyone
	 * implements JPEG save support, just uncomment this.
	 */

	/*
     * TODO: Make save quality changeable (possibly with a slider on a save
	 * dialogue?)
	 */
    /*
     * public void saveBitmapAsJPEG(String pictureName) throws
	 * FileNotFoundException { synchronized (getHolder()) { FileOutputStream fos
	 * = new FileOutputStream(pictureName);
	 * getThread().getBitmap().compress(CompressFormat.JPEG, 100, fos); } }
	 */

	public BrushPreset getCurrentPreset() {
		return mPreset;
	}

	public void setPresetColor(int color) {
		mPreset.setColor(color);
		getThread().setPreset(mPreset);
	}

	public void setPresetSize(float size) {
		mPreset.setSize(size);
		getThread().setPreset(mPreset);
	}

	public void setOpacity(int opacity) {
		mPreset.setOpacity(opacity);
		getThread().setPreset(mPreset);
	}

	public void setPresetBlur(Blur blurStyle, int blurRadius) {
		mPreset.setBlur(blurStyle, blurRadius);
		getThread().setPreset(mPreset);
	}

	public void setPresetBlur(int blurStyle, int blurRadius) {
		mPreset.setBlur(blurStyle, blurRadius);
		getThread().setPreset(mPreset);
	}

	public void setPresetPorterMode(PorterDuffXfermode porterDuffMode) {
		mPreset.setPorterDuffMode(porterDuffMode);
		getThread().setPreset(mPreset);
	}

	public void setPreset(BrushPreset preset) {
		mPreset = preset;
		getThread().setPreset(mPreset);
	}

	public boolean isSetup() {
		return mIsSetup;
	}

	public void setup(boolean setup) {
		mIsSetup = setup;

		if (mIsSetup) {
			getThread().setup();
		} else {
			getThread().activate();
		}
	}

	public boolean isChanged() {
		return mIsChanged;
	}

	public void changed(boolean changed) {
		mIsChanged = changed;
	}

	public void undo() {
		if (!mUndo) {
			mUndo = true;
			getThread().undo();
		} else {
			mUndo = false;
			getThread().redo();
		}
	}

	public boolean canUndo() {
		return isChanged() && !mUndo;
	}

	public boolean canRedo() {
		return isChanged() && mUndo;
	}

	public void setLineType(String lineType) {

		mPreset.setLineType(lineType);
		getThread().setPreset(mPreset);
	}

	/*********************************************************************/


	private void getScreenResolution(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		deviceWidth = metrics.widthPixels;
		deviceHeight = metrics.heightPixels;


	}


}
