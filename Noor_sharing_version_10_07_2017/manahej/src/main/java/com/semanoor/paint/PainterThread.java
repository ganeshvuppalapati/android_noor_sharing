package com.semanoor.paint;

import java.io.ByteArrayOutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import com.semanoor.manahij.BookViewReadActivity;


import com.semanoor.manahij.ManahijApp;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UndoRedoObjectAction;
import com.semanoor.source_sboookauthor.UndoRedoObjectData;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposePathEffect;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.VectorDrawable;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.Toast;

import org.json.JSONObject;

public class PainterThread extends Thread {


	/**
	 * Freeze when freeze() called
	 */
	public static final int SLEEP = 0;

	/**
	 * Application ready when activate() called
	 */
	public static final int READY = 1;

	public static final int SETUP = 2;

	/**
	 * Holder
	 */
	private SurfaceHolder mHolder;

	/**
	 * Brush object instance of Paint Class
	 */
	private Paint mBrush;

	/**
	 * Brush size in pixels
	 */
	private float mBrushSize;

	/**
	 * Last brush point detect for anti-alias
	 */
	private int mLastBrushPointX;

	/**
	 * Last brush point detect for anti-alias
	 */
	private int mLastBrushPointY;

	/**
	 * Canvas clear color
	 */
	private int mCanvasBgColor;

	/**
	 * Canvas object for drawing bitmap
	 */
	private Canvas mCanvas;

	/**
	 * Bitmap for drawing
	 */
	private Bitmap mBitmap;

	/**
	 * True if application is running
	 */
	private boolean mIsActive;

	/**
	 * Status of the running application
	 */
	private int mStatus;

	public State mState;


	public PainterThread(SurfaceHolder surfaceHolder) {
		// base data
		mHolder = surfaceHolder;
		// defaults brush settings
		mBrushSize = 20;
		mBrush = new Paint();
		mBrush.setAntiAlias(true);
		mBrush.setColor(Color.rgb(0, 0, 0));
		mBrush.setStrokeWidth(mBrushSize);
		mBrush.setStrokeCap(Cap.ROUND);

		// default canvas settings
		mCanvasBgColor = Color.TRANSPARENT;
		// set negative coordinates for reset last point
		mLastBrushPointX = -1;
		mLastBrushPointY = -1;


	}

	@Override
	public void run() {
		waitForBitmap();
		while (isRun()) {
			Canvas canvas = null;
			//System.out.println("Thread running");
			try {
				canvas = mHolder.lockCanvas();
				synchronized (mHolder) {
					switch (mStatus) {
						case PainterThread.READY: {
							if (canvas != null) {
								canvas.drawColor(0xFF, Mode.CLEAR);
								canvas.drawBitmap(mBitmap, 0, 0, null);

								//System.out.println("drawBitmap");
							}
							break;
						}
						case SETUP: {
							if (canvas != null) {
								canvas.drawColor(mCanvasBgColor);
								canvas.drawLine(50, (mBitmap.getHeight() / 100) * 35, mBitmap.getWidth() - 50, (mBitmap.getHeight() / 100) * 35, mBrush);
							}
							break;
						}
					}
				}
			} finally {
				if (canvas != null) {
					mHolder.unlockCanvasAndPost(canvas);
				}
				if (isFreeze()) {
					try {
						Thread.sleep(100);
						//System.out.println("sleep");
					} catch (InterruptedException e) {
					}
				}
			}
		}
	}

	public void setPreset(BrushPreset preset) {
		//commented bcz not drawing after comming in eraser mode.....
		//if (preset.porterMode != null)
		mBrush.setXfermode(preset.porterMode);
		mBrush.setColor(preset.color);
		mBrushSize = preset.size;
		mBrush.setStyle(Paint.Style.STROKE);
		mBrush.setStrokeWidth(preset.size);
		if (!preset.lineType.equalsIgnoreCase(BrushPreset.BARCODE_LINE)) {
			if (preset.blurRadius == 0)
				preset.blurRadius = 1;
			if (preset.blurStyle != null) {
				mBrush.setMaskFilter(new BlurMaskFilter(preset.blurRadius, preset.blurStyle));
			} else {
				mBrush.setMaskFilter(null);

			}
		} else {
			preset.blurRadius = 1;


		}

		switch (preset.lineType) {
			case BrushPreset.DOTTED_LINE:
				mBrush.setStrokeCap(Cap.ROUND);
				mBrush.setStyle(Paint.Style.STROKE);
				if (preset.size > 40 && preset.size < 60) {
					mBrush.setPathEffect(new DashPathEffect(new float[]{1, 10 * (preset.size / 8)}, 10));
				} else if (preset.size > 60) {
					mBrush.setPathEffect(new DashPathEffect(new float[]{1, 10 * (preset.size / 16)}, 5));
				} else {
					mBrush.setPathEffect(new DashPathEffect(new float[]{1, 10 * (preset.size / 4)}, 40));
				}
				break;
			case BrushPreset.DASHED_LINE:
				mBrush.setStrokeCap(Cap.ROUND);
				mBrush.setStyle(Paint.Style.STROKE);
				if (preset.size > 40 && preset.size < 60) {
					mBrush.setPathEffect(new DashPathEffect(new float[]{50, 10 * (preset.size / 5)}, 5));
				} else if (preset.size > 60) {
					mBrush.setPathEffect(new DashPathEffect(new float[]{100, 20 * (preset.size / 10)}, 1));
				} else {
					mBrush.setPathEffect(new DashPathEffect(new float[]{10, 2 * (preset.size)}, 40));
				}
				break;
			case BrushPreset.BARCODE_LINE:
				mBrush.setStyle(Paint.Style.STROKE);
				mBrush.setStrokeWidth(preset.size);
				mBrush.setStrokeJoin(Paint.Join.ROUND);
				mBrush.setStrokeCap(Cap.BUTT);
				float[] intervals;
				float[] compositeIntervals;



				if (preset.size > 40 && preset.size < 60) {
					intervals = new float[]{2.0f * (preset.size / 5), 2.0f * (preset.size / 5)};
					compositeIntervals = new float[]{2.0f * (preset.size / 5), 2.0f * (preset.size / 5)};
				} else if (preset.size > 60) {
					intervals = new float[]{2.0f * (preset.size / 10), 2.0f * (preset.size / 10)};
					compositeIntervals = new float[]{2.0f * (preset.size / 10), 2.0f * (preset.size / 10)};
				} else {
					intervals = new float[]{2.0f, 2.0f};
					compositeIntervals = new float[]{2.0f, 2.0f};
				}

				float phase = 0;
				DashPathEffect dashPathEffect = new DashPathEffect(intervals, phase);
				DashPathEffect dashPathEffectBar = new DashPathEffect(compositeIntervals, phase);
				ComposePathEffect composePathEffect6 = new ComposePathEffect(dashPathEffectBar, dashPathEffect);
				mBrush.setPathEffect(composePathEffect6);
				break;
			case BrushPreset.NORMAL_LINE:
              /*  if (preset.blurStyle != null) {
                    mBrush.setMaskFilter(new BlurMaskFilter(preset.blurRadius, preset.blurStyle));
                } else {
                    mBrush.setMaskFilter(null);
                }*/
				mBrush.setPathEffect(new PathEffect());
				break;
		}


	}

	public void drawBegin() {
		mLastBrushPointX = -1;
		mLastBrushPointY = -1;

		if (mState.redoBuffer != null && !mState.isUndo) {
			mState.undoBuffer = mState.redoBuffer;
		}
		mState.isUndo = false;

	}

	public void drawEnd() {

		mState.redoBuffer = saveBuffer();
		mLastBrushPointX = -1;
		mLastBrushPointY = -1;
	}

	public void draw(int x, int y) {
		if (mLastBrushPointX > 0) {
			if (mLastBrushPointX - x == 0 && mLastBrushPointY - y == 0) {
				return;
			}
			if (x != 0 && y != 0)
				mCanvas.drawLine(x, y, mLastBrushPointX, mLastBrushPointY, mBrush);

		} else {
			if (x != 0 && y != 0)
				mCanvas.drawCircle(x, y, mBrushSize * .5f, mBrush);
		}

		mLastBrushPointX = x;
		mLastBrushPointY = y;
	}

	public void setBitmap(Bitmap bitmap, boolean clear) {
		mBitmap = bitmap;
		if (clear) {
			mBitmap.eraseColor(mCanvasBgColor);
		}

		mCanvas = new Canvas(mBitmap);
	}

	public void restoreBitmap(Bitmap bitmap, Matrix matrix) {
		mCanvas.drawBitmap(bitmap, matrix, new Paint(Paint.FILTER_BITMAP_FLAG));
		mState.undoBuffer = saveBuffer();
		mState.redoBuffer = mState.undoBuffer;
	}

	public void clearBitmap() {

		mBitmap.eraseColor(mCanvasBgColor);
		mState.undoBuffer = null;
		mState.redoBuffer = null;


	}

	public Bitmap getBitmap() {
		return mBitmap;
	}

	public void on() {
		mIsActive = true;
	}

	public void off() {
		mIsActive = false;
	}

	public void freeze() {
		mStatus = SLEEP;
	}

	public void activate() {
		mStatus = READY;
	}

	public void setup() {
		mStatus = SETUP;
	}

	public boolean isFreeze() {
		return (mStatus == SLEEP);
	}

	public boolean isSetup() {
		return (mStatus == SETUP);
	}

	public boolean isReady() {
		return (mStatus == READY);
	}

	public boolean isRun() {
		return mIsActive;
	}

	public void undoRedoPaintBuffer(byte[] buffer) {
		if (buffer == null) {
			mBitmap.eraseColor(mCanvasBgColor);
		} else {
			restoreBuffer(buffer);
		}
	}

	public void undo() {
		if (mState.undoBuffer == null) {
			mBitmap.eraseColor(mCanvasBgColor);
		} else {
			restoreBuffer(mState.undoBuffer);
		}

		mState.isUndo = true;
	}

	public void redo() {
		if (mState.redoBuffer != null) {
			restoreBuffer(mState.redoBuffer);
		}

		mState.isUndo = false;
	}

	public int getBackgroundColor() {
		return mCanvasBgColor;
	}

	public void setState(State state) {
		this.mState = state;
	}

	private void waitForBitmap() {
		while (mBitmap == null) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private byte[] saveBuffer() {
		byte[] buffer = new byte[mBitmap.getRowBytes() * mBitmap.getHeight()];
		//ByteArrayOutputStream bao = new ByteArrayOutputStream();
		//mBitmap.compress(Bitmap.CompressFormat.PNG, 100, bao);
		//byte[] buffer = bao.toByteArray();
		Buffer byteBuffer = ByteBuffer.wrap(buffer);
		mBitmap.copyPixelsToBuffer(byteBuffer);
		return buffer;
	}

	private void restoreBuffer(byte[] buffer) {
		Buffer byteBuffer = ByteBuffer.wrap(buffer);
		mBitmap.copyPixelsFromBuffer(byteBuffer);
		mState.redoBuffer = buffer;
	}

	public static class State {
		public byte[] undoBuffer = null;
		public byte[] redoBuffer = null;
		public boolean isUndo = false;
	}

	/*******************************************New methods are added for noor drawing***************************************/
	public void draw1(Path drawPath) {
		initializePaint();
		if (drawPath == null) {
			return;
		} else {
			synchronized (drawPath) {

				mCanvas.drawPath(drawPath, mBrush);


			}
		}

	}

	Paint mPaint;

	public void initializePaint() {
		mPaint = new Paint();
		// mPaint.setDither(true);
		mPaint.setColor(Color.RED);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Cap.BUTT);
		mPaint.setStrokeWidth(20);
		float[] intervals = new float[]{2.0f, 4.0f};
		float[] compositeIntervals = new float[]{2.0f, 2.0f};
		float phase = 0;

		DashPathEffect dashPathEffect = new DashPathEffect(intervals, phase);
		DashPathEffect dashPathEffectBar = new DashPathEffect(compositeIntervals, phase);
		ComposePathEffect composePathEffect6 = new ComposePathEffect(dashPathEffectBar, dashPathEffect);
		mPaint.setPathEffect(composePathEffect6);
	}

}
