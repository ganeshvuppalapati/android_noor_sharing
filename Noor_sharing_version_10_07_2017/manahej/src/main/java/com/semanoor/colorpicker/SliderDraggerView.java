package com.semanoor.colorpicker;





import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;


public class SliderDraggerView extends View{
	 Paint pp ;
	 float[] locs=null;
	 int clors[] =null;
	 LinearGradient lg=null;
	 Rect r=null;
	 Context mContext=null;
	ColorsRead colosRead;
	public SliderDraggerView(Context context,ColorsRead _colosRead) {
		super(context);
		pp= new Paint();
		colosRead=_colosRead;
		float step=0.166666666666667f;
		mContext =context;
	    locs=new float[]{ 0.00f,step, step*2, step*3,step*4,step*5,1.0f};
	    clors =new int[]{ Color.parseColor("#828f99"),
	    		Color.parseColor("#755443"),
	    		Color.parseColor("#3fd1cd"),
	    		Color.parseColor("#3aac4f"),
	    		Color.parseColor("#ffcc28"),
	    		Color.parseColor("#ed556e"),
	    		Color.parseColor("#b27aaa")};
	    lg = new LinearGradient( toPx(180), toPx(20),0,0, clors, null, 
	    		android.graphics.Shader.TileMode.REPEAT);
	    r = new Rect(0, 0, (int)toPx(180), (int)toPx(20));
	}
	public float toPx(float value,Context mContext)
	 {
		 float px =0;
		
		    px = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, mContext.getResources().getDisplayMetrics());
		 return px;
	 }
	/* (non-Javadoc)
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		
		
		 pp= new Paint();
		 pp.setShader(lg);
		 canvas.drawRect(r, pp);
		 
	}
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			/*CalloutActivity main =(CalloutActivity)mContext;
			RelativeLayout.LayoutParams  relParams =(RelativeLayout.LayoutParams)main.calloutView.colosRead .ss.rl.getLayoutParams();
			int margin =(int)event.getX();
			relParams.leftMargin =margin ;
			main.calloutView.colosRead .ss.rl.setLayoutParams(relParams);
			main.calloutView.colosRead.cscrollView.setScrollX((int)Math.ceil(margin*main.calloutView.colosRead.mFactor));
			*/break;

		default:
			break;
		}

		return true;
	}
	public  float toPx(float value)
	 {
		 return toPx(value,mContext);
	 }
}
