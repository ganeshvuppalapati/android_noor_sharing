package com.semanoor.colorpicker;

public interface VScrollViewListener {

    void onVScrollChanged(ObservableVScrollView scrollView, int x, int y, int oldx, int oldy);
   // void onScrollChanged(ObservableScrollView1 scrollView, int x, int y, int oldx, int oldy);
}
