package com.semanoor.colorpicker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;

import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.UserFunctions;

public class ColorBox extends FrameLayout {

	public String getBgColor() {
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	String bgColor;
	Paint fillPaint;
	DrawView dv;
	boolean isetBorder = false;
	Paint borderPaint = null;
	private Context mContext;
	public CheckBox checkBox;
	String cColor="";

	public ColorBox(Context context, String color) {
		super(context);
		checkBox = new CheckBox(context);
		checkBox.setButtonDrawable(R.drawable.checkmark);
		checkBox.setVisibility(GONE);
		this.bgColor = color;
		mContext = context;
		fillPaint = new Paint();
		borderPaint = new Paint();
		dv = new DrawView(context);

		this.addView(dv);
		this.addView(checkBox);

	}

	public void draw(String color, boolean isetBorder) {
		this.bgColor = color;
		this.isetBorder = isetBorder;
		dv.invalidate();
	}

	public void invalidateColor(){
		dv.invalidate();
	}

	class DrawView extends View {

		public DrawView(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}


		@Override
		protected void onDraw(Canvas canvas) {
			// TODO Auto-generated method stub
			super.onDraw(canvas);

			canvas.drawARGB(255, 255, 255, 255);

			if (isetBorder) {
				fillPaint.setColor(Color.parseColor(bgColor));
				fillPaint.setStyle(Style.FILL);

				canvas.drawRect(UserFunctions.toPx(2), UserFunctions.toPx(2), UserFunctions.toPx(28), UserFunctions.toPx(28), fillPaint);

				borderPaint.setColor(Color.WHITE);
				borderPaint.setStyle(Style.STROKE);
				borderPaint.setStrokeWidth(3f);
				borderPaint.setAntiAlias(true);

				canvas.drawRect(UserFunctions.toPx(0), UserFunctions.toPx(0), UserFunctions.toPx(26), UserFunctions.toPx(29), borderPaint);
			} else {
				fillPaint.setColor(Color.parseColor(bgColor));
				fillPaint.setStyle(Style.FILL);

				canvas.drawRect(UserFunctions.toPx(0), UserFunctions.toPx(0), UserFunctions.toPx(30), UserFunctions.toPx(30), fillPaint);
			}
		}

	}

}
