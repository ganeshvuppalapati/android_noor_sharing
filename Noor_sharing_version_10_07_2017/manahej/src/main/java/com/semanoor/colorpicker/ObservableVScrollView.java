package com.semanoor.colorpicker;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;


public class ObservableVScrollView extends ScrollView {

    private VScrollViewListener scrollViewListener = null;

    public ObservableVScrollView(Context context) {
        super(context);
        this.setSmoothScrollingEnabled(true);
    }

    public ObservableVScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ObservableVScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setScrollViewListener(VScrollViewListener scrollViewListener) {
        this.scrollViewListener = scrollViewListener;
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);
        if(scrollViewListener != null) {
            scrollViewListener.onVScrollChanged(this, x, y, oldx, oldy);
        }
    }

	/* (non-Javadoc)
	 * @see android.widget.HorizontalScrollView#onOverScrolled(int, int, boolean, boolean)
	 */
	@Override
	public void onOverScrolled(int scrollX, int scrollY, boolean clampedX,
			boolean clampedY) {
		// TODO Auto-generated method stub
		super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
	}
    
}
