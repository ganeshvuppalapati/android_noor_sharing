package com.semanoor.colorpicker;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.artifex.mupdfdemo.MuPDFView;
import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.PdfFragment;
import com.semanoor.manahij.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;


public class ColorsRead extends LinearLayout implements ScrollViewListener {

    public SliderStripView ss = null;
    public int pageCount = 0;
    public int colCount = 0;
    public Activity activity;
    private boolean bookviewReadActivity = false;
    LinearLayout column = null;
    ColorBox colorBox = null;

    boolean colorValueFromEditText = false;
    selectedColor selectedColor;

    public float toPx(float value, Context mContext) {
        float px = 0;
        px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, mContext.getResources().getDisplayMetrics());
        return px;
    }

    public int toPx(float value) {
        return (int) toPx(value, mContext);
    }

    public ColorBox prevSeleced = null;
    public ColorBox prevBgSeleced = null;
    public LinearLayout parentLayout;
    RelativeLayout silderLay;
    public ObservableScrollView cscrollView;
    public Context mContext;
    public float mFactor;
    PdfFragment fragment;
    LinearLayout llLay;
    LayoutInflater inflate;
    RadioButton checkBox;
    LayoutParams pageParams;
    int lastchild;
    int numberOfColorCellsCount;
    String editTextColorCode;
    public String finalHex;
    String lastIndexColor;

    public ColorsRead(Context context, PdfFragment fragment) {
        super(context);
        mContext = context;
        activity = (Activity) context;

        this.fragment = fragment;
        try {
            inflate = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            llLay = (LinearLayout) inflate.inflate(R.layout.customcolorpicker, null);
            parentLayout = (LinearLayout) llLay.findViewById(R.id.horLay);
            silderLay = (RelativeLayout) llLay.findViewById(R.id.slider);
            cscrollView = (ObservableScrollView) llLay.findViewById(R.id.cscrollView);
            ss = new SliderStripView(context, cscrollView, this);
            cscrollView.setHorizontalScrollBarEnabled(false);
            silderLay.addView(ss);
            lastchild = readXml(context.getAssets().open("ColorCodes.xml"));
            this.addView(llLay);
            //addscrollViewListener();
            cscrollView.setScrollViewListener(this);
            mFactor = (colCount) / 6;
            //mFactor = mFactor+1;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //This is for normal books......
    public ColorsRead(Context context, String colorCode) {
        super(context);
        mContext = context;
        activity = (Activity) context;
        selectedColor = (selectedColor) context;
        bookviewReadActivity = true;
        try {
            inflate = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            llLay = (LinearLayout) inflate.inflate(R.layout.customcolorpicker, null);
            parentLayout = (LinearLayout) llLay.findViewById(R.id.horLay);
            silderLay = (RelativeLayout) llLay.findViewById(R.id.slider);
            cscrollView = (ObservableScrollView) llLay.findViewById(R.id.cscrollView);
            ss = new SliderStripView(context, cscrollView, this);
            cscrollView.setHorizontalScrollBarEnabled(false);
            silderLay.addView(ss);
            colorValueFromEditText = true;
            editTextColorCode = colorCode;
            numberOfColorCellsCount = readXml(context.getAssets().open("ColorCodes.xml"));
            currView = (ColorBox) parentLayout.findViewById(numberOfColorCellsCount - 1);
            lastIndexColor = currView.getBgColor();

            colorBox.invalidate();
            this.addView(llLay);
            //addscrollViewListener();
            cscrollView.setScrollViewListener(this);
            mFactor = (colCount) / 6;
            //mFactor = mFactor+1;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //
//	private void addscrollViewListener()
//	{
//		cscrollView.setOnTouchListener(new View.OnTouchListener() {
//
//		    @Override
//			public boolean onTouch(View v, MotionEvent event) {
//				// TODO Auto-generated method stub
//
//		    	RelativeLayout.LayoutParams  relParams =(RelativeLayout.LayoutParams)ss.rl.getLayoutParams();
//		    	int leftM =(int)Math.ceil(cscrollView.getScrollX()/mFactor);
//		    	relParams.leftMargin = leftM;
//				    	ss.rl.setLayoutParams(relParams);
//		        return false;
//			}
//		});
//	}


    public ColorBox updatelastColorBox(String color, boolean custom) {
        ColorBox colorBox = null;
        {
            colorBox = (ColorBox) parentLayout.findViewById(numberOfColorCellsCount - 1);
            if (custom) {
                finalHex = color;
                colorBox.cColor = colorBox.bgColor;
                if (color != null && !color.equals(""))
                    colorBox.setBgColor(color);

                colorBox.invalidateColor();
                colorBox.checkBox.setVisibility(VISIBLE);
            } else {
               /* finalHex = null;
                colorBox.setBgColor(lastIndexColor);
                colorBox.invalidateColor();
                colorBox.checkBox.setVisibility(GONE);*/
            }
        }
        return colorBox;
    }

    public int readXml(InputStream is) {
        int i = 0;
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();
            parser.setInput(is, null);
            //LinearLayout page = null;

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = parser.getName();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("Table")) {
//                		page = new LinearLayout(mContext);
//                		LinearLayout.LayoutParams pageParams = new LinearLayout.LayoutParams(toPx(150),toPx(200));
//                		page.setOrientation(LinearLayout.HORIZONTAL);
//                		page.setLayoutParams(pageParams);
                            pageCount = pageCount + 1;
                        } else if (tagname.equalsIgnoreCase("column")) {
                            column = new LinearLayout(mContext);
                            pageParams = new LayoutParams(toPx(30), toPx(195));
                            column.setOrientation(LinearLayout.VERTICAL);
                            column.setPadding(toPx(1), toPx(1), toPx(1), toPx(1));
                            column.setLayoutParams(pageParams);
                            colCount = colCount + 1;

                        } else if (tagname.equalsIgnoreCase("color")) {
                            String value = parser.getAttributeValue(null, "value");
                            colorBox = new ColorBox(mContext, value);
                            colorBox.setId(i++);
                            pageParams = new LayoutParams(toPx(30), toPx(30));
                            colorBox.setLayoutParams(pageParams);
                            pageParams.setMargins(toPx(1), toPx(1), toPx(1), toPx(1));
                            colorBox.setTag(value);


                            //colorBox.setBackgroundColor(Color.parseColor(value));
                            onClorBoxClickListener(colorBox);
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("Table")) {
                        } else if (tagname.equalsIgnoreCase("column")) {
                            parentLayout.addView(column);
                        } else if (tagname.equalsIgnoreCase("color")) {
                            if (prevSeleced != null) {
                                String preTag = (String) prevSeleced.getTag();
                                String coTag = (String) colorBox.getTag();
                                if (preTag.equalsIgnoreCase(coTag))
                                    currView.checkBox.setVisibility(VISIBLE);
                            }


                            column.addView(colorBox);

                        }

                        break;
                    default:
                        break;
                }

                eventType = parser.next();
            }
        } catch (Exception e)

        {
            e.printStackTrace();
        }
        return i;
    }

    public ColorBox currView;
    String currentColor;

    private void onClorBoxClickListener(final FrameLayout colorBox) {
        colorBox.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finalHex = null;
                if (colorValueFromEditText == true) {
                    currentColor = editTextColorCode;
                } else {
                    currentColor = String.valueOf(colorBox.getTag());
                }
                if (bookviewReadActivity == false) {
                    MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                    if (pageView != null)
                        pageView.saveDraw();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (currView == null) {
                    currView = (ColorBox) v;
                    //if user clicked from pdf then allowing to set color ot core object.....
                    if (bookviewReadActivity == false) {
                        fragment.core.setINK_COLOR(currView.getTag().toString());
                    } else {

                        selectedColor.selectedColor(currView.getTag().toString());

                    }
                    prevSeleced = currView;
                    currView.checkBox.setVisibility(View.VISIBLE);
                } else {
                    if (currView.cColor != null && currView.cColor.trim().length() > 0) {
                        currView.bgColor = currView.cColor;
                        currView.cColor = "";
                        currView.invalidateColor();
                    }
                    prevSeleced = currView;

                    prevSeleced.checkBox.setVisibility(View.GONE);
                    currView = (ColorBox) v;
                    if (bookviewReadActivity == false) {
                        fragment.core.setINK_COLOR(currView.getTag().toString());
                    } else {
                        selectedColor.selectedColor(currView.getTag().toString());
                    }
                    currView.checkBox.setVisibility(View.VISIBLE);
                }
                setVisibility(GONE);
            }
        });
    }

    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        //cscrollView.scrollTo(x, y);
        //cscrollView.onOverScrolled(x,y,true,true);
        RelativeLayout.LayoutParams relParams = (RelativeLayout.LayoutParams) ss.rl.getLayoutParams();
        int leftM = (int) Math.ceil(cscrollView.getScrollX() / mFactor);
        relParams.leftMargin = leftM;
        ss.rl.setLayoutParams(relParams);

    }

    @Override
    public void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
    }


    public  void hideColorPicker() {

        setVisibility(GONE);

    }

    public void showColorPicker() {
        setVisibility(VISIBLE);

    }
    public LinearLayout showColorPicker1() {
        llLay.setVisibility(VISIBLE);
        return  llLay;

    }


    public interface selectedColor {
        public void selectedColor(String selcetedColor);
    }


}