package com.semanoor.colorpicker;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;



public class SliderStripView extends RelativeLayout {
	 Paint p ;
	 Paint p1 ;
	 public float intitX=0,finalX=0;
	
	 public int initMargin=0;
	public InnerView innerView;
	public SliderDraggerView sv;
	public RelativeLayout rl;
	Context mcontext;
	Activity activity;
	ObservableScrollView cscrollView;
	ColorsRead colosRead;
	public SliderStripView(Context context,ObservableScrollView scrollView,ColorsRead _colosRead) {
		super(context);
		cscrollView=scrollView;
		colosRead=_colosRead;
		mcontext  = context;
		activity =(Activity)context;
		sv = new SliderDraggerView(context,colosRead);
		this.addView(sv);
		rl = new RelativeLayout(context);
		LayoutParams rlParams = new LayoutParams((int)toPx(30),(int)toPx(30));
		rl.setLayoutParams(rlParams);
		innerView = new InnerView(context);
		rl.addView(innerView);
		this.addView(rl);
		 p= new Paint();
		 p1 =  new Paint();
		 rl.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				LayoutParams params = (LayoutParams) v.getLayoutParams();
	             switch(event.getAction())
			     {
			     	case MotionEvent.ACTION_DOWN :
			        {
			        	intitX = event.getRawX();
			        	initMargin =params.leftMargin;
			         }
			         break;
			         case MotionEvent.ACTION_MOVE :
			         {
			        	 
			            	 int actualMargin = setLefMargin(event.getRawX(),intitX);
			            	 
				    		 params.leftMargin = actualMargin ;
				             v.setLayoutParams(params);
				             
				             //activity.calloutView.colosRead.
						  cscrollView.setScrollX((int)(Math.ceil((actualMargin*/*activity.calloutView.*/colosRead.mFactor))));
			            
			         }
			         break;
			         case MotionEvent.ACTION_UP :
			         {
			        	 
			        	 
				            int  actualMargin = setLefMargin(event.getRawX(), intitX);
				            
				            params.leftMargin = actualMargin ;
				            v.setLayoutParams(params);
				            
				           // activity.calloutView.colosRead.
						 cscrollView.setScrollX((int)(Math.ceil((actualMargin*/*activity.calloutView.*/colosRead.mFactor))));
			        	 
			        	 
			         }
			         break;
			         
			     }
				 v.getParent().requestDisallowInterceptTouchEvent(true);
	            return true;			}
		});
	}
	 private int setLefMargin(float finalX,float intitX)
	 {
		 int leftMargin=0;
		 
         leftMargin =(int) (finalX-intitX);
        
         float temp = initMargin;
         if(leftMargin <0)
           temp = initMargin +leftMargin;
       
        int actualMargin =(int)(leftMargin+temp);
        if(actualMargin<0) actualMargin =0;
        int value =(int)toPx(180-15f) ;
        if(actualMargin > value)
       	 actualMargin =value;
        return actualMargin;
	 }
	class InnerView extends View{

		public InnerView(Context context) {
			super(context);
			
		}

		@Override
		public void draw(Canvas canvas) {
			// TODO Auto-generated method stub
			super.draw(canvas);
			
			 p.setStyle(Paint.Style.FILL);
			 p.setColor(Color.TRANSPARENT);
			 
			 p1.setStyle(Paint.Style.STROKE);
			 p1.setColor(Color.WHITE);
			 p1.setStrokeWidth(toPx(2.5f));
			 p1.setAntiAlias(true);
			 
			 canvas.drawRect(toPx(1),toPx(1),toPx(15),toPx(15),p);
			 canvas.drawRect(toPx(0),toPx(0),toPx(13),toPx(14),p1);
//			 canvas.drawCircle(toPx(7), toPx(7), toPx(7), p);
//			 canvas.drawCircle(toPx(7), toPx(7), toPx(7), p1);
	}
	}
	public float toPx(float value,Context mContext)
	 {
		 float px =0;
		
		    px = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, mContext.getResources().getDisplayMetrics());
		 return px;
	 }
	public  float toPx(float value)
	 {
		 return toPx(value,mcontext);
	 }
}
