/*
 * Copyright (C) 2012 - 2014 Brandon Tate, bossturbo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.semanoor.androidwebviewselection;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.widget.ImageView;

import com.semanoor.drag.DragController;
import com.semanoor.drag.DragController.DragBehavior;
import com.semanoor.drag.DragLayer;
import com.semanoor.drag.DragListener;
import com.semanoor.drag.DragSource;
import com.semanoor.drag.MyAbsoluteLayout;
import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.R;
import com.semanoor.source_manahij.SemaWebView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

@SuppressLint("DefaultLocale")
public class TextSelectionSupport implements TextSelectionControlListener, OnTouchListener, OnLongClickListener, DragListener, OnDoubleTapListener{
    public interface SelectionListener {
        void startSelection();
        void selectionChanged(String text, Rect mSelectionBounds, Rect noDensityRect);
        void endSelection();
        void getSplit(String selText);
    }

    private enum HandleType {
        START,
        END,
        UNKNOWN
    }
    private static final String TAG = "SelectionSupport";
    private static final float CENTERING_SHORTER_MARGIN_RATIO = 12.0f / 48.0f;
    public static final int JACK_UP_PADDING = 2;
    private static final int SCROLLING_THRESHOLD = 10;
    
    private Activity mActivity;
    private SemaWebView mWebView;
    private SelectionListener mSelectionListener;
    private DragLayer mSelectionDragLayer;
    private DragController mDragController;
    public ImageView mStartSelectionHandle;
    private ImageView mEndSelectionHandle;
    private Rect mSelectionBounds = null;
    private final Rect mSelectionBoundsTemp = new Rect();
    private TextSelectionController mSelectionController = null;
    private int mContentWidth = 0;
    private HandleType mLastTouchedSelectionHandle = HandleType.UNKNOWN;
    private boolean mScrolling = false;
    private float mScrollDiffY = 0;
    private float mLastTouchY = 0;
    private float mScrollDiffX = 0;
    private float mLastTouchX = 0;
    public static float mScale = 1.0f;
    public static boolean ScaleChanged=false;
    private boolean isDoubleTapped = false;
 
    private Runnable mStartSelectionModeHandler = new Runnable() {
        public void run() {
            if (mSelectionBounds != null) {
                mWebView.addView(mSelectionDragLayer);
                drawSelectionHandles();
                final int contentHeight = (int)Math.ceil(getDensityDependentValue(mWebView.getContentHeight(), mActivity));
                final int contentWidth = mWebView.getWidth();
                ViewGroup.LayoutParams layerParams = mSelectionDragLayer.getLayoutParams();
                layerParams.height = contentHeight;
                layerParams.width = Math.max(contentWidth, mContentWidth);
                mSelectionDragLayer.setLayoutParams(layerParams);
                if (mSelectionListener != null) {
                    mSelectionListener.startSelection();
                }
            }
        }
    };
    private Runnable endSelectionModeHandler = new Runnable(){
        public void run() {
            mWebView.removeView(mSelectionDragLayer);
            mSelectionBounds = null;
            mLastTouchedSelectionHandle = HandleType.UNKNOWN;
            //mWebView.loadUrl("javascript: android.selection.clearSelection();");
            mWebView.loadJSScript("javascript: android.selection.clearSelection();", mWebView);
            if (mSelectionListener != null) {
                mSelectionListener.endSelection();
            }
        }
    };

    private TextSelectionSupport(Activity activity, SemaWebView webview) {
        mActivity = activity;
        mWebView = webview;
    }
    public static TextSelectionSupport support(Activity activity, SemaWebView webview) {
        final TextSelectionSupport selectionSupport = new TextSelectionSupport(activity, webview);
        selectionSupport.setup();
        return selectionSupport;
    }

    public void onScaleChanged(float oldScale, float newScale) {
    	//double newoldScale = oldScale*1.5;
    	if(newScale > 2.5){ //It's like pinch zoom :
 	    	ScaleChanged=true;
	    }else{
	    	ScaleChanged=false;
	    }
        mScale = newScale;
    }
    public void setSelectionListener(SelectionListener listener) {
        mSelectionListener = listener;
    }

    //
    // Interfaces of TextSelectionControlListener
    //
    @Override
    public void jsError(String error) {
        Log.e(TAG, "JSError: " + error);
    }
    @Override
    public void jsLog(String message) {
        Log.d(TAG, "JSLog: " + message);
    }
    @Override
    public void startSelectionMode() {
        mActivity.runOnUiThread(mStartSelectionModeHandler);
    }
    @Override
    public void endSelectionMode() {
        mActivity.runOnUiThread(endSelectionModeHandler);
    }
    @Override
    public void setContentWidth(float contentWidth){
        mContentWidth = (int)getDensityDependentValue(contentWidth, mActivity);
    }
    @Override
    public void selectionChanged(String range, String text, String handleBounds, boolean isReallyChanged, String handleBoundsNoLHeight){
        final Context ctx = mActivity;
        try {
        	final JSONObject selectionBoundsNoDensity = new JSONObject(handleBoundsNoLHeight);
        	Rect noDensityRect = new Rect();
        	noDensityRect.left = selectionBoundsNoDensity.getInt("left");
        	noDensityRect.top = selectionBoundsNoDensity.getInt("top");
        	noDensityRect.right = selectionBoundsNoDensity.getInt("right");
        	noDensityRect.bottom = selectionBoundsNoDensity.getInt("bottom");
        	
            final JSONObject selectionBoundsObject = new JSONObject(handleBounds);
            final float scale = getDensityIndependentValue(mScale, ctx);
            Rect rect = mSelectionBoundsTemp;
            rect.left = (int)(getDensityDependentValue(selectionBoundsObject.getInt("left"), ctx) * scale);
            rect.top = (int)(getDensityDependentValue(selectionBoundsObject.getInt("top"), ctx) * scale);
            rect.right = (int)(getDensityDependentValue(selectionBoundsObject.getInt("right"), ctx) * scale);
            rect.bottom = (int)(getDensityDependentValue(selectionBoundsObject.getInt("bottom"), ctx) * scale);
            mSelectionBounds = rect;
            if (!isInSelectionMode()){
                startSelectionMode();
            }
            drawSelectionHandles();
            if (mSelectionListener != null && isReallyChanged) {
                mSelectionListener.selectionChanged(text, mSelectionBounds, noDensityRect);
                //mWebView.loadUrl("javascript:fn()");
                String version = "ICS";
                mWebView.loadJSScript("javascript:fn('" + version + "')", mWebView);
            }
        } 
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
	public void tsjigetSplit(String selText) {
    	if (mSelectionListener != null) {
			mSelectionListener.getSplit(selText);
		}
	}

    //
    // Interface of OnTouchListener
    //
    @Override
    public boolean onTouch(View v, MotionEvent event) { 
        final Context ctx = mActivity;
        float xPoint = getDensityIndependentValue(event.getX(), ctx) / getDensityIndependentValue(mScale, ctx);
        float yPoint = getDensityIndependentValue(event.getY(), ctx) / getDensityIndependentValue(mScale, ctx);
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
        	//if(!BookView.type.equals("Search")){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return false;
            }
        	   final String startTouchUrl = String.format(Locale.US, "javascript:android.selection.startTouch(%f, %f);", xPoint, yPoint);
               mLastTouchX = xPoint;
               mLastTouchY = yPoint;
               //mWebView.loadUrl(startTouchUrl);
               mWebView.loadJSScript(startTouchUrl, mWebView);
        	
            break;
        case MotionEvent.ACTION_UP:

            if(((BookViewReadActivity)mActivity).seekBar.isShown()){
                if(((BookViewReadActivity)mActivity).CurrentRecyclerView.getAdapter().getItemCount()>1) {
                    ((BookViewReadActivity) mActivity).currentParentEnrRLayout.setVisibility(View.INVISIBLE);
                }
                if(((BookViewReadActivity)mActivity).currentBkmarkLayout.getChildCount()>0){
                    ((BookViewReadActivity) mActivity).currentParentBklayout.setVisibility(View.INVISIBLE);
                }
                ((BookViewReadActivity)mActivity).searchLayout.setVisibility(View.INVISIBLE);
                ((BookViewReadActivity)mActivity).seekBar.setVisibility(View.INVISIBLE);
                ((BookViewReadActivity)mActivity).tvSeekBar.setVisibility(View.INVISIBLE);
            } else{
                if(((BookViewReadActivity)mActivity).CurrentRecyclerView.getAdapter().getItemCount()>1) {
                    ((BookViewReadActivity) mActivity).currentParentEnrRLayout.setVisibility(View.VISIBLE);
                }
                if(((BookViewReadActivity)mActivity).currentBkmarkLayout.getChildCount()>0){
                    ((BookViewReadActivity) mActivity).currentParentBklayout.setVisibility(View.VISIBLE);
                }
                ((BookViewReadActivity)mActivity).searchLayout.setVisibility(View.VISIBLE);
                ((BookViewReadActivity)mActivity).seekBar.setVisibility(View.VISIBLE);
                ((BookViewReadActivity)mActivity).tvSeekBar.setVisibility(View.VISIBLE);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return false;
            }
            if (!mScrolling) {
                endSelectionMode();
                ((BookViewReadActivity)mActivity).hideEnrichmentAndBookmarkTabs();
                //hideSeekbarAndEnrichmentTabs();
                //
                // Fixes 4.4 double selection
                // See: http://stackoverflow.com/questions/20391783/how-to-avoid-default-selection-on-long-press-in-android-kitkat-4-4
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    return false;
                }
            }

            mScrollDiffX = 0;
            mScrollDiffY = 0;
            mScrolling = false;
            //
            // Fixes 4.4 double selection
            // See: http://stackoverflow.com/questions/20391783/how-to-avoid-default-selection-on-long-press-in-android-kitkat-4-4
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            	return false;
            }*/
            
            

            break;
        case MotionEvent.ACTION_MOVE:
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return false;
            }
            mScrollDiffX += (xPoint - mLastTouchX);
            mScrollDiffY += (yPoint - mLastTouchY);
            mLastTouchX = xPoint;
            mLastTouchY = yPoint;
            if (Math.abs(mScrollDiffX) > SCROLLING_THRESHOLD || Math.abs(mScrollDiffY) > SCROLLING_THRESHOLD) {
                mScrolling = true;
                //isDoubleTapped = false;
            }
            break;
        }
        return false;
    }
    
    /*public void hideSeekbarAndEnrichmentTabs(){
        if(BookView.sb.isShown()){

        	BookView.sb.setVisibility(View.GONE);
        	BookView.tv_pageNumber.setVisibility(View.GONE);
        	BookView.parentEnrRLayout.setVisibility(View.GONE);
        	BookView.currentparentbklayout.setVisibility(View.GONE);
        }else{
        	BookView.sb.setVisibility(View.VISIBLE);
        	BookView.tv_pageNumber.setVisibility(View.VISIBLE);
        	//System.out.println("EnrLayout child count: "+BookView.currentEnrTabsLayout.getChildCount());
        	if(BookView.currentEnrTabsLayout.getChildCount()>1){
        		BookView.parentEnrRLayout.setVisibility(View.VISIBLE);
        	}else{
        		BookView.parentEnrRLayout.setVisibility(View.GONE);
        	}
        	if(BookView.currentbkmarkLayout.getChildCount()>0){
        		BookView.currentparentbklayout.setVisibility(View.VISIBLE);
        	}else{
        		BookView.currentparentbklayout.setVisibility(View.GONE);
        	}
        }
    }*/

    //
    // Interface of OnLongClickListener
    //
    @Override 
    public boolean onLongClick(View v){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return false;
        }
        if (!isInSelectionMode()) {
            //mWebView.loadUrl("javascript:android.selection.longTouch();");
            //mWebView.loadJSScript("javascript:android.selection.longTouch();", mWebView);
            String version = "ICS";
            mScrolling = true;
            mWebView.loadJSScript("javascript:fn('"+version+"')", mWebView);
            mWebView.loadJSScript("javascript:android.selection.longTouch();", mWebView);
        }
        return true;
    }
    

    //
    // Interface of DragListener
    //
    @Override
    public void onDragStart(DragSource source, Object info, DragBehavior dragBehavior) {
    }
    @Override
    public void onDragEnd() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MyAbsoluteLayout.LayoutParams startHandleParams = (MyAbsoluteLayout.LayoutParams)mStartSelectionHandle.getLayoutParams();
                MyAbsoluteLayout.LayoutParams endHandleParams = (MyAbsoluteLayout.LayoutParams)mEndSelectionHandle.getLayoutParams();
                final Context ctx = mActivity;
                final float scale = getDensityIndependentValue(mScale, ctx);
                float startX = startHandleParams.x - mWebView.getScrollX() + mStartSelectionHandle.getWidth() * (1 - CENTERING_SHORTER_MARGIN_RATIO);
                float startY = startHandleParams.y - mWebView.getScrollY() - JACK_UP_PADDING;
                float endX = endHandleParams.x - mWebView.getScrollX() + mEndSelectionHandle.getWidth() * CENTERING_SHORTER_MARGIN_RATIO;
                float endY = endHandleParams.y - mWebView.getScrollY() - JACK_UP_PADDING;
                startX = getDensityIndependentValue(startX, ctx) / scale;
                startY = getDensityIndependentValue(startY, ctx) / scale;
                endX = getDensityIndependentValue(endX, ctx) / scale;
                endY = getDensityIndependentValue(endY, ctx) / scale;
                if (mLastTouchedSelectionHandle == HandleType.START && startX > 0 && startY > 0){
                    String saveStartString = String.format(Locale.US, "javascript: android.selection.setStartPos(%f, %f);", startX, startY);
                    //mWebView.loadUrl(saveStartString);
                    mWebView.loadJSScript(saveStartString, mWebView);
                }
                else if (mLastTouchedSelectionHandle == HandleType.END && endX > 0 && endY > 0){
                    String saveEndString = String.format(Locale.US, "javascript: android.selection.setEndPos(%f, %f);", endX, endY);
                    //mWebView.loadUrl(saveEndString);
                    mWebView.loadJSScript(saveEndString, mWebView);
                }
                else {
                    //mWebView.loadUrl("javascript: android.selection.restoreStartEndPos();");
                    mWebView.loadJSScript("javascript: android.selection.restoreStartEndPos();", mWebView);
                }
            }
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setup(){
        mScale = mActivity.getResources().getDisplayMetrics().density;
        
        mWebView.setOnLongClickListener(this);
        mWebView.setOnTouchListener(this);
        mWebView.clearCache(true);
        mWebView.setFocusable(true);
        
        final WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setDomStorageEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        //settings.setSupportZoom(true);
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
    	   settings.setBuiltInZoomControls(true);
    	   settings.setDisplayZoomControls(false);
       }
       settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		
        mSelectionController = new TextSelectionController(this);
        mWebView.addJavascriptInterface(mSelectionController, TextSelectionController.INTERFACE_NAME);
        createSelectionLayer(mActivity);
    }
    private void createSelectionLayer(Context context){
        final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSelectionDragLayer = (DragLayer)inflater.inflate(R.layout.selection_drag_layer, null);
        mDragController = new DragController(context);
        mDragController.setDragListener(this);
        mDragController.addDropTarget(mSelectionDragLayer);
        mSelectionDragLayer.setDragController(mDragController);
        mStartSelectionHandle = (ImageView)mSelectionDragLayer.findViewById(R.id.startHandle);
        mStartSelectionHandle.setTag(HandleType.START);
        mEndSelectionHandle = (ImageView)mSelectionDragLayer.findViewById(R.id.endHandle);
        mEndSelectionHandle.setTag(HandleType.END);
        final OnTouchListener handleTouchListener = new OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean handledHere = false;
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    handledHere = startDrag(v);
                    mLastTouchedSelectionHandle = (HandleType)v.getTag();
                }
                return handledHere;
            }
        };
        mStartSelectionHandle.setOnTouchListener(handleTouchListener);
        mEndSelectionHandle.setOnTouchListener(handleTouchListener);
    }
    private void drawSelectionHandles(){
        mActivity.runOnUiThread(drawSelectionHandlesHandler);
    }
    private Runnable drawSelectionHandlesHandler = new Runnable(){
        public void run() {
            MyAbsoluteLayout.LayoutParams startParams = (com.semanoor.drag.MyAbsoluteLayout.LayoutParams)mStartSelectionHandle.getLayoutParams();
            final int startWidth = mStartSelectionHandle.getDrawable().getIntrinsicWidth();
            startParams.x = (int)(mSelectionBounds.left - startWidth * (1.0f - CENTERING_SHORTER_MARGIN_RATIO));
            startParams.y = (int)(mSelectionBounds.top);
            final int startMinLeft = -(int)(startWidth * (1 - CENTERING_SHORTER_MARGIN_RATIO));
            startParams.x = (startParams.x < startMinLeft) ? startMinLeft : startParams.x;
            startParams.y = (startParams.y < 0) ? 0 : startParams.y;
            mStartSelectionHandle.setLayoutParams(startParams);

            MyAbsoluteLayout.LayoutParams endParams = (com.semanoor.drag.MyAbsoluteLayout.LayoutParams)mEndSelectionHandle.getLayoutParams();
            final int endWidth = mEndSelectionHandle.getDrawable().getIntrinsicWidth();
            endParams.x = (int) (mSelectionBounds.right - endWidth * CENTERING_SHORTER_MARGIN_RATIO);
            endParams.y = (int) (mSelectionBounds.bottom);
            final int endMinLeft = -(int)(endWidth * (1- CENTERING_SHORTER_MARGIN_RATIO));
            endParams.x = (endParams.x < endMinLeft) ? endMinLeft : endParams.x;
            endParams.y = (endParams.y < 0) ? 0 : endParams.y;
            mEndSelectionHandle.setLayoutParams(endParams);
        }
    };

    private boolean isInSelectionMode(){
        return this.mSelectionDragLayer.getParent() != null;
    }
    private boolean startDrag(View v) {
        Object dragInfo = v;
        mDragController.startDrag(v, mSelectionDragLayer, dragInfo, DragBehavior.MOVE);
        return true;
    }

    public float getDensityDependentValue(float val, Context ctx){
        Display display = ((WindowManager)ctx.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        return val * (metrics.densityDpi / 160f);
    }
    public float getDensityIndependentValue(float val, Context ctx){
        Display display = ((WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        return val / (metrics.densityDpi / 160f);
    }
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		return false;
	}
	@Override
	public boolean onDoubleTap(MotionEvent e) {
		isDoubleTapped = true;
		return false;
	}
	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		return false;
	}
}