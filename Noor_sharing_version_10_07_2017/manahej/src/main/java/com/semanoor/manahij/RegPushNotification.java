package com.semanoor.manahij;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.semanoor.source_sboookauthor.Globals;

import java.io.IOException;

/**
 * Created by Suriya on 18/06/15.
 */
public class RegPushNotification extends AsyncTask<Void, Void, String> {

    GoogleCloudMessaging gcm;
    SlideMenuWithActivityGroup context;
    String regId;
    String PROJECT_NUMBER = "";
    String scopeId;
    String userName;
    int scopeType;
    boolean isLogin;
    ProgressDialog progressDialog;
    String emailId;

    public RegPushNotification(SlideMenuWithActivityGroup context, String scopeId, String userName, int scopeType, boolean isLogin, ProgressDialog progressDialog, String email){
        this.context = context;
        this.scopeId = scopeId;
        this.userName = userName;
        this.scopeType = scopeType;
        this.isLogin = isLogin;
        this.progressDialog = progressDialog;
        this.emailId=email;
    }

    @Override
    protected String doInBackground(Void... params) {
        String msg = "";
        if (isLogin){
//            Clients clients = Clients.getInstance();
//            clients.getClientsList(Globals.redeemURL+scopeId,scopeId);
        }
        /*try {
            if (gcm == null){
                gcm = GoogleCloudMessaging.getInstance(context.getApplicationContext());
            }
            if (isLogin) {
//                regId = gcm.register(PROJECT_NUMBER);
//                context.webService.updateDeviceTokenWithUserID(scopeId, regId, "Android");
            } else {
                gcm.unregister();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        return msg;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (isLogin) {
            context.updateCredentialsForLoginView(scopeId, userName, scopeType, progressDialog,emailId);
        } else {
            context.updateCredentialsForLogoutView(scopeId, userName, scopeType, progressDialog,emailId);
        }
    }

}
