package com.semanoor.manahij;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.SegmentedRadioGroup;
import com.semanoor.source_sboookauthor.TextCircularProgressBar;
import com.semanoor.source_sboookauthor.UserFunctions;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by karthik on 06-01-2017.
 */

public class OnlineTemplatesDialog {
    private Context context;
    public GridView templateGrid;
    private ProgressBar progressBar;
    private Dialog addNewBookDialog;
    public ArrayList<OnlineTemplates> templatesList = new ArrayList<>();
    private SharedPreferences sharedPreference;
    private TextCircularProgressBar circularProgressBar;
    private boolean downloadTaskRunning;
    private GridShelf grdShelf;

    public OnlineTemplatesDialog(Context ctx, GridShelf gridShelf){
        this.context = ctx;
        this.grdShelf = gridShelf;
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
        showTemplatesDialog();
        new callWebserviceForTemplateList(Globals.templatesUrl).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    private void showTemplatesDialog(){
        addNewBookDialog = new Dialog(context);
        //addNewBookDialog.setTitle(R.string.create_new_book);
        addNewBookDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addNewBookDialog.setContentView(((Activity)context).getLayoutInflater().inflate(R.layout.add_new_book_dialog, null));
        addNewBookDialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.3), (int)(Globals.getDeviceHeight()/1.5));

        final SegmentedRadioGroup btnSegmentGroup = (SegmentedRadioGroup) addNewBookDialog.findViewById(R.id.btn_segment_group);

        templateGrid = (GridView) addNewBookDialog.findViewById(R.id.gridView1);
        templateGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position!=0 && !isDownloading(position+1) && !isDownloaded(position+1)){
                    addTemplateIdToSharedPreferences(Globals.DownloadingTemplates,position+1);
                    updateTemplateList(position);
                    templateGrid.invalidateViews();
                }else if (isDownloaded(position+1)){
                    if (context instanceof MainActivity){
                        grdShelf.addNewBook(0, position+1);
                        ((MainActivity)context).gridView.invalidateViews();
                        addNewBookDialog.dismiss();
                    }
                }
            }
        });
        Button btn = (Button) addNewBookDialog.findViewById(R.id.book_btn);
        btn.setVisibility(View.GONE);
        progressBar = (ProgressBar) addNewBookDialog.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        CircleButton back_btn = (CircleButton) addNewBookDialog.findViewById(R.id.btnback);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewBookDialog.dismiss();
            }
        });
        addNewBookDialog.show();
    }
    public class callWebserviceForTemplateList extends AsyncTask<Void, Void, String> {
        String webServiceUrl;
        JSONObject json;
        String jsonData;

        public callWebserviceForTemplateList(String url) {
            this.webServiceUrl = url;
        }

        @Override
        protected String doInBackground(Void... params) {
            jsonData = newWebService(webServiceUrl);
            jsonData = jsonData.replaceAll("\\\\r\\\\n", "");
            jsonData = jsonData.replace("\"{", "{");
            jsonData = jsonData.replace("}\",", "},");
            jsonData = jsonData.replace("}\"", "}");
            return jsonData;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            if (jsonData == null || jsonData.equals("")) {
                String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
                File file = new File(templatePath);
                File[] path = file.listFiles();
                for(File tempFile : path){
                    OnlineTemplates templates1 = new OnlineTemplates();
                    templates1.setTemplateName("Template "+tempFile.getName());
                    templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
                    templates1.setThumbUrl("");
                    templates1.setContentUrl("");
                    templates1.setDownloadStatus("Downloaded");
                    templatesList.add(templates1);
                    templateGrid.setAdapter(new OnlineTemplatesGridAdapter(context,templatesList));
                }
            }else {
                JSONObject json = null;
                try {
                    json = new JSONObject(jsonData);
                    Map<String, Object> jsonMap = toMap(json);
                    ArrayList<Object> mapObject = (ArrayList<Object>) jsonMap.get("Templates");
                    parseAndSaveTemplatesList(mapObject);
                    templateGrid.setAdapter(new OnlineTemplatesGridAdapter(context,templatesList));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private String newWebService(String url) {
        HttpURLConnection urlConnection = null;
        StringBuilder sb = new StringBuilder();
        String returnResponse = "";
        try {
            URL urlToRequest = new URL(url);

            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(25000);
            urlConnection.setRequestProperty("accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
                System.out.println("" + sb.toString());
            } else {
                System.out.println(urlConnection.getResponseMessage());
            }

        } catch (MalformedURLException e) {

        } catch (SocketTimeoutException e) {

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return sb.toString();
    }
    private Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();
        Iterator<String> keysIter = object.keys();
        while (keysIter.hasNext()) {
            String key = keysIter.next();
            Object value = object.get(key);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    private List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
    private void parseAndSaveTemplatesList(ArrayList<Object> templates){
        for (int i=0;i<templates.size();i++){
            HashMap<String,Object> hashMap = (HashMap<String, Object>) templates.get(i);
            OnlineTemplates templates1 = new OnlineTemplates();
            templates1.setTemplateName((String) hashMap.get("name"));
            templates1.setTemplateId((Integer) hashMap.get("ID"));
            templates1.setThumbUrl((String) hashMap.get("ThumbURL"));
            templates1.setContentUrl((String) hashMap.get("contentURL"));
            if (isDownloading(templates1.getTemplateId())){
                templates1.setDownloadStatus("Downloading");
            }else {
                if (new File(Globals.TARGET_BASE_TEMPATES_PATH + templates1.getTemplateId()).exists()) {
                    templates1.setDownloadStatus("Downloaded");
                } else {
                    templates1.setDownloadStatus("Not Downloaded");
                }
            }
            templatesList.add(templates1);
        }
    }
    private void updateTemplateList(int position){
        for (int i=0;i<templatesList.size();i++){
            OnlineTemplates templates = templatesList.get(i);
            if (position == i){
                templates.setDownloadStatus("Downloading");
                break;
            }
        }
    }

    private void addTemplateIdToSharedPreferences(String key,int ID){
        SharedPreferences.Editor editor = sharedPreference.edit();
        ArrayList<Integer> existList = null;
        try {
            existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<Integer>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        existList.add(ID);
        try {
            editor.putString(key, ObjectSerializer.serialize(existList));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        editor.commit();
    }
    private boolean isDownloading(int Id){
        ArrayList<Integer> existList = null;
        try {
            existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.DownloadingTemplates, ObjectSerializer.serialize(new ArrayList<Integer>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i=0;i<existList.size();i++){
            int existId = existList.get(i);
            if (existId == Id){
                return true;
            }
        }
        return false;
    }
    private void removeFromSharedPreference(int Id){
        SharedPreferences.Editor editor = sharedPreference.edit();
        ArrayList<Integer> existList = null;
        try {
            existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.DownloadingTemplates, ObjectSerializer.serialize(new ArrayList<Integer>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i=0;i<existList.size();i++){
            int existId = existList.get(i);
            if (existId == Id){
                existList.remove(i);
                break;
            }
        }
        try {
            editor.putString(Globals.DownloadingTemplates, ObjectSerializer.serialize(existList));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        editor.commit();
    }

    private int getNextDownloadTemplateId(){
        ArrayList<Integer> existList = null;
        try {
            existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.DownloadingTemplates, ObjectSerializer.serialize(new ArrayList<Integer>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (existList.size()>0){
            return existList.get(0);
        }
        return -1;
    }

    private void updateArrayList(int Id){
        for (int i=0;i<templatesList.size();i++){
            OnlineTemplates templates = templatesList.get(i);
            if (Id == templates.getTemplateId()&& templates.getDownloadStatus().equals("Downloading")){
                templates.setDownloadStatus("Downloaded");
                break;
            }
        }
    }
    private boolean isDownloaded(int Id){
        for (int i=0;i<templatesList.size();i++){
            OnlineTemplates templates = templatesList.get(i);
            if (Id == templates.getTemplateId()&& templates.getDownloadStatus().equals("Downloaded")){
                return true;
            }
        }
        return false;
    }

     class downloadTemplate extends AsyncTask<String,Integer,String>{
        private int ID;
        private boolean downloadSuccess;

        public downloadTemplate(int templateId){
            this.ID = templateId;
        }

        @Override
        protected String doInBackground(String... downloadurl) {
            final int TIMEOUT_CONNECTION = 5000;//5sec
            final int TIMEOUT_SOCKET = 30000;//30sec
            long total = 0;
            int fileLength;
            try {
                if (UserFunctions.isInternetExist(context)) {
                    URL url = new URL(downloadurl[0]);
                    URLConnection ucon = url.openConnection();
                    File newFile = new File((Globals.TARGET_BASE_FILE_PATH) + ID);

                    if (newFile.exists()) {
                        total = newFile.length();
                        ucon.setRequestProperty("Range", "bytes=" + total + "-");
                        ucon.connect();
                    } else {
                        ucon.setRequestProperty("Range", "bytes=" + total + "-");
                        ucon.connect();
                    }
                    fileLength = ucon.getContentLength();
                    long startTime = System.currentTimeMillis();
                    ucon.setReadTimeout(TIMEOUT_CONNECTION);
                    ucon.setConnectTimeout(TIMEOUT_SOCKET);
                    InputStream is = ucon.getInputStream();
                    BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
                    String templateId= String.valueOf(ID);
                    FileOutputStream outStream = (total == 0) ? context.openFileOutput(templateId, Context.MODE_PRIVATE) : context.openFileOutput(templateId, Context.MODE_APPEND);
                    byte[] buff = new byte[5 * 1024];
                    int len;
                    while (!isCancelled() && (len = inStream.read(buff)) != -1) {
                        total += len;
                        publishProgress((int) ((total * 100) / fileLength));
                        outStream.write(buff, 0, len);
                    }
                    outStream.flush();
                    outStream.close();
                    inStream.close();
                    downloadSuccess = true;
                }
            }catch (IOException e) {
                downloadSuccess = false;
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            if (circularProgressBar!=null) {
                circularProgressBar.setProgress(progress[0]);
                circularProgressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (downloadSuccess) {
               DatabaseHandler db = DatabaseHandler.getInstance(context);
                String tempPath, zipfile, unzippathWithSlash;
                tempPath = String.valueOf(ID);   //Changes after merge
                zipfile = Globals.TARGET_BASE_FILE_PATH.concat(tempPath);
                unzippathWithSlash = tempPath.concat("/");
                String unziplocation = Globals.TARGET_BASE_TEMPATES_PATH; //"/" is a must here..
                boolean unzipSuccess = unzipDownloadfileNew(zipfile, unziplocation);
                if (unzipSuccess){
                    String name = "Template "+ID;
                    db.executeQuery("insert into templates(templateID,name,type,titleFont,titleSize,titleColor,authorFont,authorSize,authorColor,paraFont,paraColor,paraSize) values('"+ID+"','"+name+"','portrait','Verdana','54','232-219-184','Arial','25','232-219-184','Arial','56-56-48','25')");
                    removeFromSharedPreference(ID);
                    updateArrayList(ID);
                    templateGrid.invalidateViews();
                    final int nextId = getNextDownloadTemplateId();
                    if (nextId!= -1){
                        final String url = getUrl(nextId);
                        new android.os.Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                new downloadTemplate(nextId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,url);
                            }
                        }, 1000);
                    }else{
                        downloadTaskRunning = false;
                    }
                }
            }
        }
    }

    private String getUrl(int ID){
        for (int i=0;i<templatesList.size();i++){
            OnlineTemplates templates = templatesList.get(i);
                if (ID == templates.getTemplateId()){
                    return templates.getContentUrl();
                }
            }
        return "";
    }

    private boolean unzipDownloadfileNew(String zipfilepath, String unziplocation) {
        ZipFile zipFile = null;
        List<FileHeader> headers = null;
        try {
            zipFile = new ZipFile(new File(zipfilepath));
            headers = zipFile.getFileHeaders();
        } catch (ZipException e) {
            e.printStackTrace();
        }

        if (headers != null) {
            for (int i = 0; i < headers.size(); i++) {
                FileHeader header = headers.get(i);
                header.setFileName(header.getFileName().replace("\\", "//"));
                //File  file = new File(eleessonPath+header.getFileName());
                try {
                    zipFile.extractFile(header, unziplocation);
                } catch (ZipException e) {
                    e.printStackTrace();
                }

            }
        }
        return true;
    }

    public class OnlineTemplatesGridAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<OnlineTemplates> templatesListArray;

        public OnlineTemplatesGridAdapter(Context ctx, ArrayList<OnlineTemplates> _templatesListArray) {
            this.context = ctx;
            this.templatesListArray = _templatesListArray;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return templatesListArray.size();

        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return templatesListArray.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View view = convertView;
            if (view == null) {
                view = ((Activity)context).getLayoutInflater().inflate(R.layout.inflate_book_template, null);
            }
            OnlineTemplates templates = templatesListArray.get(position);
            view.setId(templates.getTemplateId());
            ImageView bookImg = (ImageView) view.findViewById(R.id.imageView2);
            TextCircularProgressBar textCircularProgressBar = (TextCircularProgressBar) view.findViewById(R.id.circular_progress);
            if (templates.getDownloadStatus().equals("Not Downloaded")){
                bookImg.setAlpha(0.65f);
                textCircularProgressBar.setVisibility(View.GONE);
            }else if (templates.getDownloadStatus().equals("Downloading")){
                if (getNextDownloadTemplateId() != -1){
                    if (!downloadTaskRunning){
                        new downloadTemplate(getNextDownloadTemplateId()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,templates.getContentUrl());
                        downloadTaskRunning = true;
                    }
                    if (templates.getTemplateId() == getNextDownloadTemplateId()){
                        circularProgressBar = textCircularProgressBar;
                    }
                }
                bookImg.setAlpha(0.65f);
                textCircularProgressBar.setMax(100);
                textCircularProgressBar.getCircularProgressBar().setCircleWidth(14);
                textCircularProgressBar.setVisibility(View.VISIBLE);
            }else{
                bookImg.setAlpha(1.0f);
                textCircularProgressBar.setVisibility(View.GONE);
            }
            if (templates.getDownloadStatus().equals("Downloaded")){
                String freeFilesPath = Globals.TARGET_BASE_TEMPATES_PATH;
                Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templates.getTemplateId()+"/card.png");
                bookImg.setImageBitmap(bitmap);
            }else {
                Glide.with(context).load(templates.getThumbUrl())
                        .thumbnail(0.5f)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .override(200, 200)
                        .into(bookImg);
            }
            return view;
        }
    }
}
