package com.semanoor.manahij;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.inappbilling.util.IabResult;
import com.semanoor.inappbilling.util.Inventory;
import com.semanoor.inappbilling.util.Purchase;
import com.semanoor.manahij.ManahijApp.TrackerName;
import com.semanoor.paint.BrushPreset;
import com.semanoor.paint.PainterCanvas;
import com.semanoor.sboookauthor_store.BookMarkEnrichments;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.CustomViewPager;
import com.semanoor.source_manahij.EnrichPages;
import com.semanoor.source_manahij.ListContentAdapter;
import com.semanoor.source_manahij.NotesEditText;
import com.semanoor.source_manahij.Resources;
import com.semanoor.source_manahij.SemaWebView;
import com.semanoor.source_manahij.checklistAdapter;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.PopoverView.PopoverViewDelegate;
import com.semanoor.source_sboookauthor.PopupColorPicker;
import com.semanoor.source_sboookauthor.PopupColorPicker.ColorPickerListener;
import com.semanoor.source_sboookauthor.SegmentedRadioGroup;
import com.semanoor.source_sboookauthor.UserCredentials;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;


public class BookView extends Activity implements OnSeekBarChangeListener,OnClickListener, TextWatcher, PopoverViewDelegate,android.content.DialogInterface.OnDismissListener{

	//Download enrichments variables :
	private ViewPager enrichViewpager;
	public ListView enrichPageListView, selectEnrichListView;
	public ProgressBar enrichPage1bar, enrichprogressbar;
	public String EnrSelectAll = "select";
	private RelativeLayout noenrichPage1_layout, noenrich_layout;
	public RelativeLayout rootviewRL;
	public ArrayList<Boolean> itemChecked = new ArrayList<Boolean>();
	private PopoverView popoverView,advbkmarkpopup;
    
	public CardContainer mCardContainer;
	public Dialog showStudyCardsDialog ;
	public static Object[] noteStudycards;
	
	public static DatabaseHandler db;
	Point p;
	public static int pageNumber=1, lastbuttonclicked;
	public static boolean enriched=false;
	
	public static String epath;
	public static ImageView iv_bookmark;
	static String filePath = new String();
	public static String bookName; 
	public static SemaWebView webView;
	public static SemaWebView viewPagerWebView;
	private static CustomViewPager viewPager;

	public static int pageCount=0; 
	private static int viewPagerPgNo = 0;
	private static ArrayList<String> imagePath = new ArrayList<String>();

	int getLastViewedPage=0;
	int MY_REQUEST_ID;
	int LSearchFlag;

	String searchFilePath = new String();
	public ArrayList<String> searchIndexArray = new ArrayList<String>();
	public ArrayList<String> resultFound = new ArrayList<String>();

	EditText searchEditText;
	ImageView clearSearchBox;

	static Dialog searchPopup;
	RelativeLayout searchLinLayout;
	ListView searchListView;
	static EditText etSearch;
	static ImageButton imgBtnBack;

	PopupWindow bookMarkPopup;
	LinearLayout bookMarklinLayout;
	static Button btnBookMark;

	static View view_notepop;
	View notePopupView;
	static PopupWindow notepop;
	static NotesEditText et, etNote;
	static FrameLayout poparrow_bottomcontainer,poparrow_topcontainer;
	static ImageView iv_toparrow_left,iv_toparrow_right,iv_toparrow_center,iv_bottomarrow_left,iv_bottomarrow_right,iv_bottomarrow_center;
	static boolean isTablet=false;

	static PopupWindow notePopUp;
	static RelativeLayout noteRelativeLayout;
	static ListView noteList;
	static ImageView imgNote;
	static TextView txtNote;
	static Button btnDone, btnCancel;
	static ImageView imgBtnC1, imgBtnC2, imgBtnC3, imgBtnC4, imgBtnC5, imgBtnC6;
	static int CC;
	static android.content.res.Resources res;
	static String SN = "";
	static String sourceString = "";
	static ArrayList<String> pages = new ArrayList<String>();
	static ArrayList<String> pagesValue = new ArrayList<String>();
	
	static Integer[] stickImgId = {R.drawable.stick1, R.drawable.stick2, R.drawable.stick3, R.drawable.stick4, R.drawable.stick5,
		R.drawable.stick6};
	static Integer[] cardImgId = {R.color.yellow,R.color.green,R.color.skyblue,R.color.pink,R.color.lavender,R.color.white};

	String javascriptSrc = "";
	private Handler mHandler = new Handler();

	//public static int lngCategoryID;

	ClipboardManager mClipboard;

	static String selectedText;

	static Dialog advSearchPopUp;
	static LinearLayout advSearchLinLayout;
	ListView advListView;
	String[] advSearch = {"Book","MOE Curriculum","NOOOR","Google","Wikipedia","Youtube","Yahoo","Bing","Ask"};
	static EditText etAdv;
	int advBookFlag;

	static Dialog sharePopUp;
	LinearLayout shareLinLayout;
	String[] share = {"Facebook","Twitter","Email"};
	//Twitter
	private Twitter mTwitter;
	private RequestToken mRequestToken;
	private SharedPreferences pref;

	//Facebook
	private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            //onSessionStateChange(session, state, exception);
        }
    };
    private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	
	/*private static String APP_ID = "215281548499646";
	private Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;*/
	
	String FILENAME = "AndroidSSO_data";
	private SharedPreferences mPrefs;
	private String name;

	int mSelectX = 0;
	int mSelectY = 0;
	int occurence = 0;
	int x = 0;
	int y = 0;
	int ex = 0;
	int ey = 0;
	int width = 0;
	int height = 0;
	int touch = 1;

	Button btnLibrary;
	Button btnIndex;

	Point bookMPoint,btnenrichPoint;
	Point searchPoint;
	public Globals g;
	static int deviceWidth;
	static int deviceHeight;

	public static String result = "";
	public static String selText = "";

	public static String LSearch = "", LBookmark = "", LCopy = "", LFlip = "", LNavigation = "", LHighlight = "", LNote = "", LGotoPage = "", LindexPage = "", LZoom = "", LWebSearch = "", LTranslationSearch = "", LBooklanguage = "";

	static PopupWindow purchasePopup;
	static RelativeLayout purchaseRelLayout;
	static WebView purchaseAgreementWebView;
	static ProgressBar purchaseProgressBar;
	static Button btnAgree;
	static Button btnDonAgree;
	static TextView purchaseTextView;
	static TextView tv_noresult;

	/* Page Slider */
	public static SeekBar sb;
	public static TextView tv_pageNumber;
	public static int sb_pNo;

	private static File dir; 
	public static ArrayList<String> webdataArray = new ArrayList<String>(); 

	ProgressDialog mDialog;
	public EnrichPages enrichpages;
	LinearLayout popLayout;
	public static LinearLayout currentEnrTabsLayout;
	public static RelativeLayout parentEnrRLayout,parentbkmarkLayout,currentparentbklayout;
	//--Google in-app --//
	IabHelper mHelper;
	static final String SKU_bookFeature = "bookfeature";
	//static final String SKU_bookFeature = "android.test.cancelled";
	//static final String SKU_bookFeature = "android.test.refunded";
	//static final String SKU_bookFeature = "android.test.item_unavailable";
	//static final String SKU_bookFeature = "android.test.purchased";
	static final int RC_REQUEST = 10001;
	private String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmrdMwiC+F6jqAh74G7NUC6/S3MTiHcWtMQxeIbpo5FzTbMj9DGSlw3vvKGP4CWQCHi1tITTXLqpVRqAJAw0G2DPm3j8ksYI4kwG1ZjWM0M2L3nhtnqKQjYeaZn9kOtAn9/Mz4g+YmHCcB+4rmddf37v6h18YXEP7gD7gMR3QKmsShYQ6bhs+8KPioOhm3FkfvRabLH3CS9O5kDFU3dTZqsB+Uin1AN3UrYzoCEGFHWFAKa8yyF3vS5LNFhERqF1SF+5rncQ8Sj+CG/6nsBpaoPntOynW+EuyEx+0p9Ib8qrMk5Z+uSrD6wmOPljkDqKSdM8v04seZlvnAgJRZZr4lwIDAQAB";

	/* Enriched */
	//public static ArrayList<String> enrichPath = new ArrayList<String>();
	//public static ArrayList<String> enrichTitle = new ArrayList<String>();
	public static ArrayList<String> enrichPageArray = new ArrayList<String>();
	public String xmlValues = "";
	private Button btnEnrich,btnEnrichPage, btnEnrichEdit, btnEnrResExport, btnResSync;
	//private static Button[] btn;
	private static int enrich_id=0;
	
	/* current page layouts */
	private static HorizontalScrollView btnScrollView,sv,bookmrk_sv;
	private static HorizontalScrollView currentbkmarkscroll;
	private static LinearLayout ll_container;
	public static LinearLayout bookmark_container;
	public static LinearLayout currentbkmarkLayout;
	protected static Context ctx;

	private boolean loadingfromIndexpage;
	public static int tabNo=0;
	public static String type="Home";

	public Animation animEnrichSlideIn,animEnrichSlideOut;
	/* admob */
	//static AdView adView;
	static InterstitialAd interstitial;
	boolean adsPurchased;

	public static Rect mNoteBounds = null;
	public static float mScale;
	private static RelativeLayout rl_notepop;
	private static RelativeLayout noteColorSelectionContainer;

	//private static String paidOrUnPaid = "";
	static String style = "";
	static String jsFilePath = "";
	static String jsAndroidSelection = "";
	static String jsjquery = "";
	static String jsRange = "";
	static String jsRangeSerializer = "";
	static String jsJpnText = "";

	//public static ArrayList<View> bookViewArray = new ArrayList<View>();
	public static Book currentBook;
	public static String currentBookPath;
	private int bookViewActivityrequestCode = 1001;
	
	private AdView adView;
	
	//variables for paint
    Button btn_done,btnPen, btnBrush, btnEarser, btnClearDrawing,btnpaintindex,btnHideAd;
	
	    public Button btnUndo, btnRedo, btnPaintUndo, btnPaintRedo,btnMarker,btnStudyCards;
		public PainterCanvas canvasView;
		private SeekBar mBrushSize, mBrushBlurRadius;
		private Spinner mBrushBlurStyle;
		
		public static String objContent;
		//public UndoManager undoManager;
		public static ImageView canvasImgView,viewPagerCanvasImgView;
		
		private String drawnFilesDir = "";
		private RelativeLayout objectsToolbar,drawingToolbar,designPageLayout;
		
		public  ArrayList<Enrichments> currentEnrichmentList = new ArrayList<Enrichments>();
		public  Enrichments currentEnrichment;
		
		//arrayList advSearchBookMark
		public  BookMarkEnrichments currentbookmarkEnrichment;
		public  ArrayList<BookMarkEnrichments> Bookmark = new ArrayList<BookMarkEnrichments>();

		private ArrayList<ArrayList<BookMarkEnrichments>> bookMarkedAdvTabList = new ArrayList<ArrayList<BookMarkEnrichments>>();
	    
		//dismissing dialog
		public boolean advsearchbkmark;
		public static boolean ineditmode;
		public static boolean deletemode;
		public static boolean bkmarkselected;
		public static WebView adv_search;
		public SimpleCardStackAdapter cardsAdapter;
        public static ProgressBar progressbar;
        protected ProgressDialog downloadProgDialog;
        
        public WebService webService;
        public UserCredentials userCredentials;
        public ViewPager exportEnrViewPager;
        public boolean clearMode=false;
        
        public String downloadType;
        TextView tv_StudyCardsAlert;
    		
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		// No title Bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		loadLocale();
		
		setContentView(R.layout.bookview);

		g = Globals.getInstance();
		deviceWidth  = g.getDeviceWidth();
		deviceHeight = g.getDeviceHeight();
          
		
		db = new DatabaseHandler(this);
		webService = new WebService(BookView.this, Globals.getNewCurriculumWebServiceURL());
		new onCreateBackgroundTask().execute();

		enrichpages = new EnrichPages();
		rootviewRL = (RelativeLayout) findViewById(R.id.rootView);
        copyNoteImage();
       
        //cardsAdapter = new SimpleCardStackAdapter(getApplicationContext(), this, noteStudycards);
    	
        style = GetStyle();
		jsFilePath = "file:///android_asset/getSelection.js";
		jsAndroidSelection = "file:///android_asset/android.selection.js";
		jsjquery = "file:///android_asset/jquery-1.8.3.js";
		jsRange = "file:///android_asset/rangy-core.js";
		jsRangeSerializer = "file:///android_asset/rangy-serializer.js";
		jsJpnText = "file:///android_asset/jpntext.js";

		SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
		adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);

		if (!adsPurchased) {
			interstitial = new InterstitialAd(BookView.this);
			interstitial.setAdUnitId(Globals.INTERSTITIAL_AD_UNIT_ID);
			btnHideAd = (Button) findViewById(R.id.btnHideAd);
			btnHideAd.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					AlertDialog.Builder builder = new AlertDialog.Builder(BookView.this);
					builder.setTitle(R.string.ad_remover_title);
					builder.setMessage(R.string.ad_remover_message);
					builder.setCancelable(false);
					builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
							dialog.cancel();
							btnHideAd.setVisibility(View.GONE);
							adView.setVisibility(View.GONE);
						}
					});
					builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
							startAdsPurchase();
						}
					});
					AlertDialog alert = builder.create();
					alert.show();
				}
			});
			adView = (AdView) findViewById(R.id.adView);
			AdRequest adRequest = new AdRequest.Builder()
			//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
			//.addTestDevice("505F22CBB28695386696655EED14B98B") 
			.build();
			adView.loadAd(adRequest);
			adView.setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					super.onAdLoaded();
					adView.setVisibility(View.VISIBLE);
					btnHideAd.setVisibility(View.VISIBLE);
				}
				
			});
			interstitial.loadAd(adRequest);
		}

		pref = getSharedPreferences(Globals.PREF_NAME, MODE_PRIVATE);

		//mAsyncRunner = new AsyncFacebookRunner(facebook);

		dir = getFilesDir();
		res = getResources();

		Intent i = getIntent();
		//bookName = i.getExtras().getString("string");
		//lngCategoryID = i.getExtras().getInt("lngID");
		
		currentBook = (Book) getIntent().getSerializableExtra("Book");
		
		userCredentials = (UserCredentials) getIntent().getSerializableExtra("UserCredentials");
		bookName = currentBook.get_bStoreID();
		pageCount = currentBook.getTotalPages();
		
		currentBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookName+"Book/";

		imagePath.clear();
		//Assigning bookviewarray :
		for (int j = 0; j <= pageCount; j++) {
			//bookViewArray.add(j, null);
			bookMarkedAdvTabList.add(j, null);
		}
		setPageFilter();
		
		getLastViewedPage = currentBook.get_lastViewedPage();
		if (LBooklanguage.equalsIgnoreCase("English")) {
			pageNumber = getLastViewedPage;
		}else{
			pageNumber = getLastViewedPage+1;
		}

		selectedBook();
		//loadingfromcurlpage = true;

		//changes-anim
		animEnrichSlideIn = AnimationUtils.loadAnimation(this, R.anim.slidein);
		animEnrichSlideOut = AnimationUtils.loadAnimation(this, R.anim.slideout);        

		intializeCustomViewPager();

		isTablet = getResources().getBoolean(R.bool.isTablet);

		//revert_book_hide :
		filePath = "file:///"+currentBookPath+pageNumber+".htm";
		//filePath ="file:///data/data/com.semanoor.manahij/files/."+bookName+"Book/"+pageNumber+".htm"; //Store Modifications
		//filePath ="file:///data/data/com.semanoor.manahij/files/"+bookName+"Book/"+pageNumber+".htm"; //Store Modifications
		drawnFilesDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookName+"Book/"+"FreeFiles/Drawfiles/";
		UserFunctions.createNewDirectory(drawnFilesDir);
		
        designPageLayout = (RelativeLayout) findViewById(R.id.DesignPageLayout);
		
        drawingToolbar = (RelativeLayout) findViewById(R.id.draw_view);
        
        objectsToolbar = (RelativeLayout) findViewById(R.id.relativeLayout1);
		
		btnLibrary = (Button) findViewById(R.id.btnLibrary);
		btnLibrary.setOnClickListener(this);

		btnBookMark = (Button) findViewById(R.id.btnBookMark);
		btnBookMark.setOnClickListener(this);

		btnIndex = (Button) findViewById(R.id.btnIndex);
		btnIndex.setOnClickListener(this);
		
		btnMarker = (Button) findViewById(R.id.btnMarker);
		btnMarker.setOnClickListener(this);
		
		btn_done = (Button) findViewById(R.id.btn_done);
		btn_done.setOnClickListener(this);
		
		btnPen = (Button) findViewById(R.id.btn_pen);
		btnPen.setOnClickListener(this);
		
		btnBrush = (Button) findViewById(R.id.btn_brush);
		btnBrush.setOnClickListener(this);
		
		btnEarser = (Button) findViewById(R.id.btn_eraser);
		btnEarser.setOnClickListener(this);
		
		btnClearDrawing = (Button) findViewById(R.id.btn_clear);
		btnClearDrawing.setOnClickListener(this);
		
		btnPaintUndo = (Button) findViewById(R.id.btn_paint_undo);
		btnPaintUndo.setOnClickListener(this);
		
		btnPaintRedo = (Button) findViewById(R.id.btn_paint_redo);
		btnPaintRedo.setOnClickListener(this);
		
		btnStudyCards= (Button) findViewById(R.id.btnStudyCards);
		btnStudyCards.setOnClickListener(this);
		/*Button btnFirst = (Button) findViewById(R.id.btnFirst);
		btnFirst.setOnClickListener(this);

		Button btnPrevious = (Button) findViewById(R.id.btnPrevious);
		btnPrevious.setOnClickListener(this);

		Button btnNext = (Button) findViewById(R.id.btnNext);
		btnNext.setOnClickListener(this);

		Button btnLast = (Button) findViewById(R.id.btnLast);
		btnLast.setOnClickListener(this);*/

		btnEnrichPage = (Button)findViewById(R.id.btnEnrichment);
		btnEnrichPage.setOnClickListener(this);
		
		btnEnrichEdit = (Button) findViewById(R.id.btnEnrichEdit);
		btnEnrichEdit.setOnClickListener(this);
		
		btnEnrResExport = (Button) findViewById(R.id.btnEnrResExport);
		btnEnrResExport.setOnClickListener(this);
		
		btnResSync = (Button) findViewById(R.id.btnResSync);
		btnResSync.setOnClickListener(this);

		/* Page Slider-Begin */
		tv_pageNumber=(TextView) findViewById(R.id.tv_pagenumber);

		if(pageNumber==1)
		{
			tv_pageNumber.setText("Cover Page");
		}
		else if(pageNumber==pageCount){
			tv_pageNumber.setText("Last Page");
		}
		else{
			tv_pageNumber.setText(""+(pageNumber-1)+" of "+(pageCount-2)+"");
		}

		sb = (SeekBar)findViewById(R.id.sb_pageslider);
		sb.setOnSeekBarChangeListener(this);
		sb.setMax(pageCount-1);
		if (LBooklanguage.equalsIgnoreCase("English")) {
			sb.setProgress(pageNumber);
		}else{
			sb.setProgress(pageCount-pageNumber);
		}

		/*bookMarkPopup = new PopupWindow(this);
		bookMarklinLayout=new LinearLayout(this);
		View bookMarkView = getLayoutInflater().inflate(R.layout.bookmark, null, false);
		bookMarklinLayout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		bookMarklinLayout.setBackgroundColor(Color.rgb(235,235,235));
		bookMarklinLayout.addView(bookMarkView);
		bookMarkPopup.setContentView(bookMarklinLayout);
		bookMarkPopup.setFocusable(true);
		bookMarkPopup.setTouchable(true); 
		//Bookmark button images :
		ImageButton btnBM1 = (ImageButton)bookMarkView.findViewById(R.id.bookMarkImgButton1);
		btnBM1.setOnClickListener(new OnClickListener() {

			//@Override
			public void onClick(View v) {
				addBookMark(1);
				displayBMark(pageNumber);
				displayBookMark(pageNumber);
				bookMarkPopup.dismiss();
			}
		});

		ImageButton btnBM2 = (ImageButton)bookMarkView.findViewById(R.id.bookMarkImgButton2);
		btnBM2.setOnClickListener(new OnClickListener() {

			//@Override
			public void onClick(View v) {
				addBookMark(2);
				displayBMark(pageNumber);
				displayBookMark(pageNumber);
				bookMarkPopup.dismiss();
			}
		});

		ImageButton btnBM3 = (ImageButton)bookMarkView.findViewById(R.id.bookMarkImgButton3);
		btnBM3.setOnClickListener(new OnClickListener() {

			//@Override
			public void onClick(View v) {
				addBookMark(3);
				displayBMark(pageNumber);
				displayBookMark(pageNumber);
				bookMarkPopup.dismiss();
			}
		});

		ImageButton btnBM4 = (ImageButton)bookMarkView.findViewById(R.id.bookMarkImgButton4);
		btnBM4.setOnClickListener(new OnClickListener() {

			//@Override
			public void onClick(View v) {
				addBookMark(4);
				displayBMark(pageNumber);
				displayBookMark(pageNumber);
				bookMarkPopup.dismiss();
			}
		});*/
		//changes-dialog
		advSearchPopUp = new Dialog(BookView.this);
		advSearchPopUp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
		advSearchPopUp.setTitle("Search via");
		advSearchPopUp.setContentView(getLayoutInflater().inflate(R.layout.advsearch, null));
		advListView = (ListView) advSearchPopUp.findViewById(R.id.listViewAdv);
		etAdv = (EditText) advSearchPopUp.findViewById(R.id.editTextAdv);
		advSearchPopUp.getWindow().setLayout((int) ((deviceWidth/4)*3.1), (int) ((deviceHeight/4)*2.1));//resize popup
		advListView.setDividerHeight(2);
		advListView.setAdapter(new advAdapter(this));
		advListView.setOnItemClickListener(new OnItemClickListener() 
		{
			public void onItemClick(AdapterView<?> parent, View v, int position,long id) 
			{
				String url = null;
				String advSearchtext;
				switch (position) 
				{
				case 0:
					advBookFlag = 1;
					advSearchPopUp.dismiss();
					resultFound.removeAll(resultFound);

					etSearch.setText(etAdv.getText().toString());
					etSearch.setVisibility(View.GONE);

					if(searchIndexArray.isEmpty())
					{
						processXML();
					}
					serachTableReload(selectedText);
					searchPopup.show();
					break;

				case 1:
					url = "http://projects.nooor.com/SemaBooksSearch/BkSearch.aspx?Srch="+etAdv.getText().toString()+"";
					advSearchtext=etAdv.getText().toString();
					//clickedAdvancedSearch(advSearch[position], url);
					new clickedAdvancedSearchTabs(advSearch[position], url).execute();
					advSearchPopUp.setOnDismissListener(BookView.this);
					advSearchPopUp.dismiss();
					break;

				case 2:
					url = "http://www.nooor.com/search/"+etAdv.getText().toString()+"/";
					advSearchtext=etAdv.getText().toString();
					//clickedAdvancedSearch(advSearch[position], url);
					new clickedAdvancedSearchTabs(advSearch[position], url).execute();
					advSearchPopUp.setOnDismissListener(BookView.this);
					advSearchPopUp.dismiss();
					break;

				case 3:
					url = "http://www.google.com/search?q="+etAdv.getText().toString()+"";
					advSearchtext=etAdv.getText().toString();
					//clickedAdvancedSearch(advSearch[position], url);
					new clickedAdvancedSearchTabs(advSearch[position], url).execute();
					advSearchPopUp.setOnDismissListener(BookView.this);
					advSearchPopUp.dismiss();
					break;

				case 4:
					String text = etAdv.getText().toString();
					String language = detectLanguage(text);

					String percentEscpe = Uri.encode(text);
					if(language.equals("ar"))
					{
						url = "http://ar.wikipedia.org/wiki/"+percentEscpe+"";
					}
					else
					{
						url = "http://en.wikipedia.org/wiki/"+percentEscpe+"";
					}
					advSearchtext=etAdv.getText().toString();
					//clickedAdvancedSearch( advSearch[position], url);
					new clickedAdvancedSearchTabs(advSearch[position], url).execute();
					advSearchPopUp.setOnDismissListener(BookView.this);
					advSearchPopUp.dismiss();
					break;

				case 5:
					url = "http://m.youtube.com/#/results?q="+etAdv.getText().toString()+"";
					advSearchtext=etAdv.getText().toString();
					//clickedAdvancedSearch(advSearch[position], url);
					new clickedAdvancedSearchTabs(advSearch[position], url).execute();
					advSearchPopUp.setOnDismissListener(BookView.this);
					advSearchPopUp.dismiss();
					break;

				case 6:
					url = "http://search.yahoo.com/search?p="+etAdv.getText().toString()+"";
					advSearchtext=etAdv.getText().toString();
					//clickedAdvancedSearch(advSearch[position], url);
					new clickedAdvancedSearchTabs(advSearch[position], url).execute();
					advSearchPopUp.setOnDismissListener(BookView.this);
					advSearchPopUp.dismiss();
					break;

				case 7:
					url = "http://www.bing.com/search?q="+etAdv.getText().toString()+"";
					advSearchtext=etAdv.getText().toString();
					//clickedAdvancedSearch( advSearch[position], url);
					//new clickedAdvancedSearchTabs(advSearch[position], url).execute();
					new clickedAdvancedSearchTabs(advSearch[position], url).execute();
					advSearchPopUp.setOnDismissListener(BookView.this);
					advSearchPopUp.dismiss();
					break;

				case 8:
					url = "http://www.ask.com/web?q="+etAdv.getText().toString()+"";
					advSearchtext=etAdv.getText().toString();
					//clickedAdvancedSearch(advSearch[position], url);
					new clickedAdvancedSearchTabs(advSearch[position], url).execute();
					advSearchPopUp.setOnDismissListener(BookView.this);
					advSearchPopUp.dismiss();
					break;

				default:
					break;
				}
			}
		});      
        

		searchEditText = (EditText) findViewById(R.id.search_box);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		searchEditText.setHint(getResources().getString(R.string.search));
			
		searchEditText.addTextChangedListener(this);
		searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			//@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

				if(actionId == EditorInfo.IME_ACTION_SEARCH){
					resultFound.removeAll(resultFound);

					if(searchIndexArray.isEmpty()){
						processXML();
					}
					serachTableReload(searchEditText.getText().toString());

					searchPopup.show();

					InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
					inputmanager.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);

					if(resultFound.isEmpty()==true)
					{
						tv_noresult.setVisibility(View.VISIBLE);
					}

					etSearch.setVisibility(View.GONE);
					imgBtnBack.setVisibility(View.GONE);
					searchPopup.findViewById(R.id.footer).setVisibility(View.GONE);
					return true;
				}
				return false;
			}
		});

		//small note popup
		notepop = new PopupWindow(this);
		view_notepop = this.getLayoutInflater().inflate(R.layout.note_popup_small, null, false);

		notepop.setContentView(view_notepop);
		notepop.setBackgroundDrawable(new BitmapDrawable());
		notepop.setOutsideTouchable(true);
		notepop.setFocusable(true);
		notepop.setTouchable(true);

		rl_notepop = (RelativeLayout) view_notepop.findViewById(R.id.rl_note_popup_small_container);
		rl_notepop.setFocusable(true);
		poparrow_topcontainer = (FrameLayout) view_notepop.findViewById(R.id.poparrow_topcontainer);
		poparrow_bottomcontainer = (FrameLayout) view_notepop.findViewById(R.id.poparrow_bottomcontainer);
		iv_toparrow_left = (ImageView) view_notepop.findViewById(R.id.iv_toparrow_left);
		iv_toparrow_right = (ImageView) view_notepop.findViewById(R.id.iv_toparrow_right);
		iv_toparrow_center = (ImageView) view_notepop.findViewById(R.id.iv_toparrow_center);
		iv_bottomarrow_left = (ImageView) view_notepop.findViewById(R.id.iv_bottomarrow_left);
		iv_bottomarrow_right = (ImageView) view_notepop.findViewById(R.id.iv_bottomarrow_right);
		iv_bottomarrow_center = (ImageView) view_notepop.findViewById(R.id.iv_bottomarrow_center);

		final LinearLayout ll_notes_et_container = (LinearLayout) view_notepop.findViewById(R.id.ll_notes_et_container);
		ImageView iv_nColor1 = (ImageView) view_notepop.findViewById(R.id.iv_nColorSelect1);
		ImageView iv_nColor2 = (ImageView) view_notepop.findViewById(R.id.iv_nColorSelect2);
		ImageView iv_nColor3 = (ImageView) view_notepop.findViewById(R.id.iv_nColorSelect3);
		ImageView iv_nColor4 = (ImageView) view_notepop.findViewById(R.id.iv_nColorSelect4);
		ImageView iv_nColor5 = (ImageView) view_notepop.findViewById(R.id.iv_nColorSelect5);
		ImageView iv_nColor6 = (ImageView) view_notepop.findViewById(R.id.iv_nColorSelect6);

		LayoutParams textViewLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		et = new NotesEditText(this, null);
		et.setText("");
		et.setTextColor(Color.rgb(0,0,0));
		et.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.store_header_text_size));
		et.setLineSpacing(8, 1);
		et.setBackgroundResource(R.drawable.transparent);
		et.setLayoutParams(textViewLayoutParams);
		et.setGravity(Gravity.TOP);
		ll_notes_et_container.addView(et);

		notepop.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {

				InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
				inputmanager.hideSoftInputFromWindow(et.getWindowToken(), 0);
				mWebviewVisibility.post(mDoneNote);
			}
		});

		//custom_et.setKeyListener(null); //used for making et to readonly
		iv_nColor1.setOnClickListener(new OnClickListener() 
		{	
			@Override
			public void onClick(View v) 
			{
				CC = 0;
				rl_notepop.setBackgroundResource(R.drawable.notepop_bg1);
				iv_toparrow_left.setBackgroundResource(R.drawable.arrow_1);
				iv_toparrow_center.setBackgroundResource(R.drawable.arrow_1);
				iv_toparrow_right.setBackgroundResource(R.drawable.arrow_1);
				iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_1);
				iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_1);
				iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_1);

			}
		});
		iv_nColor2.setOnClickListener(new OnClickListener() 
		{	
			@Override
			public void onClick(View v) 
			{
				CC = 1;
				rl_notepop.setBackgroundResource(R.drawable.notepop_bg2);
				iv_toparrow_left.setBackgroundResource(R.drawable.arrow_2);
				iv_toparrow_center.setBackgroundResource(R.drawable.arrow_2);
				iv_toparrow_right.setBackgroundResource(R.drawable.arrow_2);
				iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_2);
				iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_2);
				iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_2);
			}
		});
		iv_nColor3.setOnClickListener(new OnClickListener() 
		{	
			@Override
			public void onClick(View v) 
			{
				CC = 2;
				rl_notepop.setBackgroundResource(R.drawable.notepop_bg3);
				iv_toparrow_left.setBackgroundResource(R.drawable.arrow_3);
				iv_toparrow_center.setBackgroundResource(R.drawable.arrow_3);
				iv_toparrow_right.setBackgroundResource(R.drawable.arrow_3);
				iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_3);
				iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_3);
				iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_3);
			}
		});
		iv_nColor4.setOnClickListener(new OnClickListener() 
		{	
			@Override
			public void onClick(View v) 
			{
				CC = 3;
				rl_notepop.setBackgroundResource(R.drawable.notepop_bg4);
				iv_toparrow_left.setBackgroundResource(R.drawable.arrow_4);
				iv_toparrow_center.setBackgroundResource(R.drawable.arrow_4);
				iv_toparrow_right.setBackgroundResource(R.drawable.arrow_4);
				iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_4);
				iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_4);
				iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_4);
			}
		});
		iv_nColor5.setOnClickListener(new OnClickListener() 
		{	
			@Override
			public void onClick(View v) 
			{
				CC = 4;
				rl_notepop.setBackgroundResource(R.drawable.notepop_bg5);
				iv_toparrow_left.setBackgroundResource(R.drawable.arrow_5);
				iv_toparrow_center.setBackgroundResource(R.drawable.arrow_5);
				iv_toparrow_right.setBackgroundResource(R.drawable.arrow_5);
				iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_5);
				iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_5);
				iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_5);
			}
		});
		iv_nColor6.setOnClickListener(new OnClickListener() 
		{	
			@Override
			public void onClick(View v) 
			{
				CC = 5;
				rl_notepop.setBackgroundResource(R.drawable.notepop_bg6);
				iv_toparrow_left.setBackgroundResource(R.drawable.arrow_6);
				iv_toparrow_center.setBackgroundResource(R.drawable.arrow_6);
				iv_toparrow_right.setBackgroundResource(R.drawable.arrow_6);
				iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_6);
				iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_6);
				iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_6);
			}
		});

		//small note popup end

		

		//changes-dialog
		searchPopup = new Dialog(BookView.this);
		searchPopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
		searchPopup.setTitle("Search Results");

		searchPopup.setContentView(getLayoutInflater().inflate(R.layout.search_list_popup, null));
		searchPopup.getWindow().setLayout((int) ((deviceWidth/4)*3.1), (int) ((deviceHeight/4)*2.1));//resize popup
		tv_noresult = (TextView) searchPopup.findViewById(R.id.tv_noresult);
		final ListView lv = (ListView)  searchPopup.findViewById(R.id.listViewSearch);

		etSearch = (EditText)  searchPopup.findViewById(R.id.editTextSearch);
		imgBtnBack = (ImageButton)  searchPopup.findViewById(R.id.imageButtonBack);
		imgBtnBack.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				searchPopup.dismiss();
				advSearchPopUp.show();
				etSearch.setVisibility(View.VISIBLE);
				imgBtnBack.setVisibility(View.VISIBLE);
				searchPopup.findViewById(R.id.footer).setVisibility(View.VISIBLE);
			}
		});
		lv.setDividerHeight(2);
		lv.setAdapter(new searchListAdapter(this));
		lv.setOnItemClickListener(new OnItemClickListener() 
		{
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3) 
			{
				String str1 = resultFound.get(arg2);
				String[] str = str1.split("page ");
				//pageNumber = Integer.parseInt(str[1]);
			 	final int page = Integer.parseInt(str[1]);
				searchPopup.dismiss();
				LSearchFlag=1;
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
                        if (LBooklanguage.equalsIgnoreCase("English")) {
                        	if(pageNumber!=page+1){
                        		viewPager.setCurrentItem(page);
                        		pageNumber=page+1;
                        	}else{
                        	    viewPager.setAdapter(new ViewAdapter());
                        	    viewPager.setCurrentItem(page);
                        	    pageNumber=page+1;
                        	}
						}else{
							if(pageNumber!=page+1){
								 viewPager.setCurrentItem(pageCount-(page+1));
								 pageNumber=page+1;
							}else{
							   viewPager.setAdapter(new ViewAdapter());
							   viewPager.setCurrentItem(pageCount-(page+1));
							   pageNumber=page+1;
							}
						}
					}
				});
			}
		});

		notePopUp = new PopupWindow(this);
	    notePopupView = getLayoutInflater().inflate(R.layout.note_popup, null, false);
		noteRelativeLayout = (RelativeLayout) notePopupView.findViewById(R.id.noteRelativeLayout);
		noteColorSelectionContainer = (RelativeLayout) notePopupView.findViewById(R.id.ll_color_selection_container);
		notePopUp.setContentView(notePopupView);
		notePopUp.setBackgroundDrawable(null);
		notePopUp.setOutsideTouchable(false);
		notePopUp.setFocusable(true);
		notePopUp.setTouchable(true);
		noteList = (ListView) notePopupView.findViewById(R.id.notelistView);

		noteList.setDividerHeight(2);

		noteList.setAdapter(new noteListAdapter(this));
		noteList.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {

				String TESN = pagesValue.get(position);
				SN = TESN;
				String[] searchResults = SN.split("\\|");
				if(searchResults[0].contentEquals("E")){

					noteColorSelectionContainer.setVisibility(View.VISIBLE);
					txtNote.setVisibility(View.GONE);
					btnDone.setVisibility(View.VISIBLE);
					btnCancel.setVisibility(View.GONE);
					etNote.setVisibility(View.VISIBLE);
					noteList.setVisibility(View.GONE);
					String[] searchResultsText = db.getRecord(searchResults[1]).split("\\|");
					etNote.setText(searchResultsText[0]);
					CC = Integer.parseInt(searchResultsText[1])-1;
				    etNote.requestFocus();
	                Editable text=etNote.getText();
	                etNote.setSelection(etNote.getText().length());
	                InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
	    		    inputmanager.showSoftInput(etNote, inputmanager.SHOW_IMPLICIT);
                   // noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
                    noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
				}
			}

		});

		txtNote = (TextView) notePopupView.findViewById(R.id.txtNote);
		btnDone = (Button) notePopupView.findViewById(R.id.btnDone);
		btnDone.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
                 //InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
				//inputmanager.hideSoftInputFromWindow(etNote.getWindowToken(), 0);
				mWebviewVisibility.post(mDoneNote);
				notePopUp.dismiss();
			}
		});
		btnCancel = (Button) notePopupView.findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				notePopUp.dismiss();
			}
		});
		imgBtnC1 = (ImageView) notePopupView.findViewById(R.id.imgBtnC1);
		imgBtnC1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				CC = 0;

				//noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
				noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
			}
		});
		imgBtnC2 = (ImageView) notePopupView.findViewById(R.id.imgBtnC2);
		imgBtnC2.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				CC = 1;

				//noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
				noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
			}
		});
		imgBtnC3 = (ImageView) notePopupView.findViewById(R.id.imgBtnC3);
		imgBtnC3.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				CC = 2;

				//noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
				noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
			}
		});
		imgBtnC4 = (ImageView) notePopupView.findViewById(R.id.imgBtnC4);
		imgBtnC4.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				CC = 3;

				//noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
				noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
			}
		});
		imgBtnC5 = (ImageView) notePopupView.findViewById(R.id.imgBtnC5);
		imgBtnC5.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				CC = 4;

				//noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
				noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
			}
		});
		imgBtnC6 = (ImageView) notePopupView.findViewById(R.id.imgBtnC6);
		imgBtnC6.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				CC = 5;

				//noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
				noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
			}
		});


		purchasePopup = new PopupWindow(this);
		purchaseRelLayout=new RelativeLayout(this);
		View purchaseView = getLayoutInflater().inflate(R.layout.purchase_agreement, null, false);

		purchaseRelLayout.addView(purchaseView);
		purchasePopup.setContentView(purchaseRelLayout);
		purchasePopup.setFocusable(true);
		purchasePopup.setTouchable(true);
		mDialog = new ProgressDialog(this);

		purchaseAgreementWebView = (WebView) purchaseView.findViewById(R.id.webViewPurchase);
		purchaseTextView = (TextView) purchaseView.findViewById(R.id.textViewPurchaseAgrreement);
		purchaseProgressBar = (ProgressBar) purchaseView.findViewById(R.id.progressBar1);
		btnAgree = (Button) purchaseView.findViewById(R.id.btnAgree);
		btnDonAgree = (Button) purchaseView.findViewById(R.id.btnDonAgree);
		btnAgree.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				//Set up in-app :
				setup_In_App();
				//googleAnalyticsEcommerceInApp();
				purchasePopup.dismiss();
			}
		});

		btnDonAgree.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				//google Analytics for Events :
				googleAnalyticsEventDontAgreeInApp();
				purchasePopup.dismiss();
			}
		});

		purchaseAgreementWebView.setWebViewClient(new WebViewClient(){

			@Override
			public void onPageFinished(WebView view, String url){
				purchaseProgressBar.setVisibility(View.GONE);
			}
		});
		LinearLayout customEditTxtContainer = (LinearLayout) notePopupView.findViewById(R.id.customEditTxtContainer);
		LayoutParams textViewLayoutParams2 = new LayoutParams(LayoutParams.MATCH_PARENT, deviceHeight/4);
		etNote = new NotesEditText(this, null);
		etNote.setText("");
		etNote.setTextColor(Color.rgb(0,0,0));
		etNote.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.store_header_text_size));
		etNote.setLineSpacing(8, 1);
		etNote.setBackgroundResource(R.drawable.transparent);
		etNote.setLayoutParams(textViewLayoutParams2);
		etNote.setGravity(Gravity.TOP);
		etNote.setCursorVisible(true);
		etNote.requestFocus();
		customEditTxtContainer.addView(etNote);

		clearSearchBox = (ImageView) findViewById(R.id.clear_search_box);
		clearSearchBox.setOnClickListener(this);

		GoogleAnalyticsBookScreenView();
		filterViewsBasedOnLimitedVersion();
		
		uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
	}
	
	private void filterViewsBasedOnLimitedVersion(){
		if (!Globals.isTablet()) {
			btnEnrichPage.setVisibility(View.GONE);
			btnEnrichEdit.setVisibility(View.GONE);
			btnEnrResExport.setVisibility(View.GONE);
			btnResSync.setVisibility(View.GONE);
		}
	}
	
	/**
	 * Load Locale language from shared preference and change language in the application
	 */
	private void loadLocale() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String language = prefs.getString(Globals.languagePrefsKey, "en");
		changeLang(language);
	}
	
	/**
	 * change language in the application
	 * @param language
	 */
	private void changeLang(String language) {
		Locale myLocale = new Locale(language);
		Locale.setDefault(myLocale);
		android.content.res.Configuration config = new android.content.res.Configuration();
		config.locale = myLocale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
	}
	
	/**
	 * Initiate the purchase
	 * @param inAppProductId 
	 */
	private void startAdsPurchase(){
		//ITEM_SKU = "bookfeature";
		//ITEM_SKU = inAppProductId;
		mHelper = new IabHelper(BookView.this, Globals.manahijBase64EncodedPublicKey);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			
			@Override
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					////System.out.println("In-app Billing setup failed: " + result);
					String str = getResources().getString(R.string.in_app_setup_failed);
					UserFunctions.DisplayAlertDialog(BookView.this, str + result, R.string.billing_setup_failed);
				} else {
					////System.out.println("In-app Billing setup isOK");
					mHelper.launchPurchaseFlow(BookView.this, Globals.ADS_INAPP_PURCHASE_PRODUCTID, 10001, mPurchaseFinishedListener, "purchasetoken");
				}
			}
		});
	}
	
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		
		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			if (result.isFailure()) {
				String message = result.getMessage();
				int response = result.getResponse();
				//System.out.println(result+":"+response);
				if (response == 7) {
					UserFunctions.DisplayAlertDialog(BookView.this, R.string.item_already_owned, R.string.restoring_purchase);
					disableAds();
				} else {
					String str = getResources().getString(R.string.in_app_purchase_failed_);
					UserFunctions.DisplayAlertDialog(BookView.this, str +result, R.string.purchase_failed);
				}
				return;
			} else if (purchase.getSku().equals(Globals.ADS_INAPP_PURCHASE_PRODUCTID)) {
				consumeItem();
			}
		}
	};
	
	public void consumeItem() {
		mHelper.queryInventoryAsync(mReceivedInventoryListener);
	}
	
	IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {
			if (result.isFailure()) {
				////System.out.println("Failed");
				UserFunctions.DisplayAlertDialogNotFromStringsXML(BookView.this, result.getMessage(), "");
			} else {
				mHelper.consumeAsync(inv.getPurchase(Globals.ADS_INAPP_PURCHASE_PRODUCTID), mConsumeFinishedListener);
			}
		}
	};
	
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		
		@Override
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (result.isSuccess()) {
				////System.out.println("Success");
				//UserFunctions.DisplayAlertDialog(fa_enrich, R.string.purchase_completed_successfully, R.string.purchased);
				disableAds();
			} else {
				////System.out.println("Failed");
				UserFunctions.DisplayAlertDialog(BookView.this, result.getMessage(), R.string.purchase_failed);
			}
		}
	};
	
	private void disableAds(){
		SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
		Editor editor = preference.edit();
		editor.putBoolean(Globals.ADS_DISPLAY_KEY, true);
		editor.commit();
		adView.setVisibility(View.GONE);
		btnHideAd.setVisibility(View.GONE);
		adsPurchased = true;
	}

	/**
	 * Adding  advsearchEnrichment entry in book.xml,database
	 * @param epath 
	 */
	  
	/*public void clickedAdvancedSearch(String title, String url) {
		title = title.replace("'","''");
		String bookXmlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/"+"Book.xml";
        db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path)values('"+currentBook.getBookID()+"','"+pageNumber+"','"+title+"','Search', '0','"+url+"')");
	    int objUniqueId = db.getMaxUniqueRowID("enrichments");
	    title = title.replace("''","'");
	    Enrichments enrichments = new Enrichments(BookView.this);
		enrichments.setEnrichmentId(objUniqueId);
		enrichments.setEnrichmentPageNo(pageNumber);
		enrichments.setEnrichmentTitle(title);
		enrichments.setEnrichmentType("Search");
		enrichments.setEnrichmentPath(url);
		enrichments.setEnrichmentBid(currentBook.getBookID());
		enrichments.setEnrichmentSequenceId(currentEnrTabsLayout.getChildCount()+1);
		currentEnrichmentList.add(enrichments);
		enrichments.UpdateAdvSearchintoBookXml(bookXmlPath);
		
		Button enrichBtnCreated = enrichments.createSearchEnrichmentTabs();
		makeEnrichedBtnSlected(enrichBtnCreated);
		tabNo=objUniqueId;
		progressbar.setVisibility(View.VISIBLE);
		displayBookMark(pageNumber,tabNo);
		webView.setVisibility(View.INVISIBLE);
		adv_search.setVisibility(View.VISIBLE);
		selectedBook();
		loadAdvancedSearchWebView(url,"Search");
		//enrichments.new downloadingimage("http://www.google.com/s2/favicons?domain="+url,enrichments).execute();
		
		
	}*/
	public class clickedAdvancedSearchTabs extends AsyncTask<Void, Void, Void>{
		String title;
	    String url;
	    int objUniqueId;
		
		public clickedAdvancedSearchTabs(String Title, String Url) {
			title=Title;
			url=Url;
		}

		
        @Override
		protected void onPreExecute() {
			super.onPreExecute();
			title = title.replace("'","''");
			String bookXmlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/"+"Book.xml";
	        db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,categoryID)values('"+currentBook.getBookID()+"','"+pageNumber+"','"+title+"','Search', '0','"+url+"','0')");
		    objUniqueId = db.getMaxUniqueRowID("enrichments");
		    title = title.replace("''","'");
		    Enrichments enrichments = new Enrichments(BookView.this);
			enrichments.setEnrichmentId(objUniqueId);
			enrichments.setEnrichmentPageNo(pageNumber);
			enrichments.setEnrichmentTitle(title);
			enrichments.setEnrichmentType("Search");
			enrichments.setEnrichmentPath(url);
			enrichments.setEnrichmentBid(currentBook.getBookID());
			enrichments.setEnrichmentSequenceId(currentEnrTabsLayout.getChildCount()+1);
			currentEnrichmentList.add(enrichments);
			enrichments.UpdateAdvSearchintoBookXml(bookXmlPath);
			
			Button enrichBtnCreated = enrichments.createSearchEnrichmentTabs();
			makeEnrichedBtnSlected(enrichBtnCreated);
			tabNo=objUniqueId;
			//BookView.progressbar.setVisibility(View.VISIBLE);
			BookView.displayBookMark(pageNumber, tabNo);
			webView.setVisibility(View.INVISIBLE);
			adv_search.setVisibility(View.VISIBLE);
			selectedBook();
			loadAdvancedSearchWebView(url,"Search");
         }
		
		@Override
		protected Void doInBackground(Void... params) {
			try{
			
			}
			catch(Exception e){
				e.printStackTrace();
			}
            return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (currentEnrTabsLayout.getWidth()>parentEnrRLayout.getWidth()) {
				 int  enrTabsWidth1 = currentEnrTabsLayout.getWidth();
				 sv.smoothScrollTo(enrTabsWidth1, 0);
			}
		}
		
	}
	/*public void applyTheme(int selectedTheme) {

		switch (selectedTheme) {
		case 1:
			rootviewRL.setBackgroundResource(R.drawable.shelvesbg_1);
			break;
		case 2:
			rootviewRL.setBackgroundResource(R.drawable.shelvesbg_2);
			break;
		case 3:
			rootviewRL.setBackgroundResource(R.drawable.shelvesbg_3);
			break;
			/*case 4:
			rootviewRL.setBackgroundResource(R.drawable.shelvesbg_4);
			break;
		case 5:
			rootviewRL.setBackgroundResource(R.drawable.shelvesbg_5);
			break;
		case 6:zz
			rootviewRL.setBackgroundResource(R.drawable.shelvesbg_6);
			break;

		default:
			rootviewRL.setBackgroundResource(R.drawable.shelvesbg_2);
			break;
		}
	}*/
	//Google in-app :
	public void setup_In_App(){

		mHelper = new IabHelper(this, base64EncodedPublicKey);
		//mHelper.enableDebugLogging(true);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

			@Override
			public void onIabSetupFinished(IabResult result) {

				if(!result.isSuccess()){
					////System.out.println("Problem setting up in-app Billing:" +result);
				}else{
					////System.out.println("Set up successful");
					String payload = "";
					mHelper.launchPurchaseFlow(BookView.this, SKU_bookFeature, RC_REQUEST, mPurchaseFinishedListener, payload);
				}
			}
		});
	}

	// Callback for when a purchase is finished
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener1 = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			////System.out.println("Purchase finished: " + result + ", purchase: " + purchase);

			if (mHelper == null) return;

			if (result.isFailure()) {
				String message = result.getMessage();
				int response = result.getResponse();
				////System.out.println(result+":"+response);
				if (response == 7) {
					////System.out.println("Restore In-App");
					Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
					toast.show();
					//Restore In-App :
					//update DB file :
					dbCheckandUpdate();
					//paidOrUnPaid = db.checkPaidVersion();
				}else{
					////System.out.println("In-App purchase failed");
					Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
					toast.show();
				}
				return;
			}
			/*if (!verifyDeveloperPayload(purchase)) {
	                //complain("Error purchasing. Authenticity verification failed.");
	                return;
	            }*/
			////System.out.println("Purchase successful.");

			if (purchase.getSku().equals(SKU_bookFeature)) {

				//mHelper.consumeAsync(purchase, mConsumeFinishedListener);
				mHelper.queryInventoryAsync(mGotInventoryListener);
			}
		}
	};


	// Listener that's called when we finish querying the items and subscriptions we own
	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
			////System.out.println("Query inventory finished.");

			if (mHelper == null) return;

			if (result.isFailure()) {
				////System.out.println("Failed in queryinventory:"+result.getMessage());
				Toast toast = Toast.makeText(getApplicationContext(), result.getMessage(), Toast.LENGTH_SHORT);
				toast.show();
				return;
			}

			////System.out.println("Query inventory was successful.");

			/*
			 * Check for items we own. Notice that for each purchase, we check
			 * the developer payload to see if it's correct! See
			 * verifyDeveloperPayload().
			 */

			Purchase bookFeature = inventory.getPurchase(SKU_bookFeature);
			if (bookFeature != null) {
				mHelper.consumeAsync(inventory.getPurchase(SKU_bookFeature), mConsumeFinishedListener);
				return;
			}

			////System.out.println("Initial inventory query finished; enabling main UI.");
		}
	};

	// Called when consumption is complete
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener1 = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			////System.out.println("Consumption finished. Purchase: " + purchase + ", result: " + result);

			if (mHelper == null) return;
			if (result.isSuccess()) {
				////System.out.println("Consumption successful. Provisioning.");
				//update DB file :
				dbCheckandUpdate();
				//paidOrUnPaid = db.checkPaidVersion();
				googleAnalyticsEcommerceInApp();
			}
			else {
				////System.out.println("Failed in consumeFinishedListener:"+result.getMessage());
				Toast toast = Toast.makeText(getApplicationContext(), result.getMessage(), Toast.LENGTH_SHORT);
				toast.show();
			}
		}
	};


	/** Verifies the developer payload of a purchase. */
	boolean verifyDeveloperPayload(Purchase p) {
		String payload = p.getDeveloperPayload();

		return true;
	}

	private void googleAnalyticsEventDontAgreeInApp() {

		Tracker t = ((ManahijApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
		//Build and send an Event :
		t.send(new HitBuilders.EventBuilder()
		.setCategory("Book view")
		.setAction("In App Don't Agree")
		.setLabel("Clicked Don't Agree for In App feature")
		.build());
	}

	private void googleAnalyticsEcommerceInApp(){

		String Inapp_transID = "In_App-"+getCurrentDateTimeInString();
		double Inapp_price = 4.00;
		double Inapp_tax = 0.0;
		double Inapp_shipping = 0.0;
		double Inapp_revenue = Inapp_price + Inapp_tax + Inapp_shipping;
		long Inapp_quantity = 1;
		try{
			//Build the transaction :
			sendDataToTwoTrackers( new HitBuilders.TransactionBuilder()
			.setTransactionId(Inapp_transID)
			.setAffiliation("Manahij Android features")
			.setRevenue(Inapp_revenue)
			.setTax(Inapp_tax)
			.setShipping(Inapp_shipping)
			.setCurrencyCode("SAR")
			.build());

			//Build an item :
			sendDataToTwoTrackers( new HitBuilders.ItemBuilder()
			.setTransactionId("")
			.setName("Manahij In App")
			.setSku("bookfeature")
			//.setCategory("Features")
			.setPrice(Inapp_price)
			.setQuantity(Inapp_quantity)
			.setCurrencyCode("SAR")
			.build());

		}catch (Exception e) {
			////System.out.println("Google Analytics Ecommerce In-App:"+e.getMessage());
		}
	}

	private String getCurrentDateTimeInString(){
		String rtnValue = "";
		Calendar Cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		rtnValue = df.format(Cal.getTime());
		Toast.makeText(this, rtnValue, Toast.LENGTH_SHORT).show();
		return rtnValue;
	}

	private void sendDataToTwoTrackers(Map<String, String> params) {

		ManahijApp app = ((ManahijApp) getApplication());
		Tracker appTracker = app.getTracker(TrackerName.APP_TRACKER);
		Tracker ecommerceTracker = app.getTracker(TrackerName.ECOMMERCE_TRACKER);
		appTracker.send(params);
		ecommerceTracker.send(params);
	}

	////////////////////////////////////////////////////////////////////////////////////////////

	private class onCreateBackgroundTask extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {

			tt2();
			//loadHighlightAndNote(dir);
			return null;
		}
	}

	private void intializeCustomViewPager(){
		/* Enriched */
		viewPager = (CustomViewPager) findViewById(R.id.viewPager);
		viewPager.setAdapter(new ViewAdapter());
		//viewPager.setOffscreenPageLimit(2); //2
		viewPager.setClipChildren(false);
		if (LBooklanguage.equalsIgnoreCase("English")) {
			viewPager.setCurrentItem(pageNumber, false);
		}else{
			viewPager.setCurrentItem(pageCount-pageNumber, false);
		}
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {

				//////System.out.println("onPageSelected");
				if (LBooklanguage.equalsIgnoreCase("English")) {
					pageNumber = viewPager.getCurrentItem() + 1;
					sb.setProgress(pageNumber-1);
				}else{
					pageNumber = pageCount - viewPager.getCurrentItem();
					sb.setProgress(pageCount-pageNumber);
				}

				if(arg0 == 0 || arg0 == pageCount-1){
					displayBookMark(pageNumber,tabNo);
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				  ////System.out.println("onPageScrolled");
				  loadEnrichmentObjectList(pageNumber);
				  displayBookMark(pageNumber,tabNo);
				  displayBMark(pageNumber);
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				//////System.out.println("onPageScrollStateChanged");
			}
		});
	}

	private class ViewAdapter extends PagerAdapter{

		private Button btnEnrHome;

		@Override
		public int getCount() {

			return pageCount;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {

			return (view == object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position){
			////System.out.println(position);
			if (LBooklanguage.equalsIgnoreCase("English")) {
				pageNumber = viewPager.getCurrentItem() + 1;
				viewPagerPgNo = position + 1;
			}else{
				pageNumber = pageCount - viewPager.getCurrentItem();
				viewPagerPgNo = pageCount-position;
			}
			//tabNo = 0;
			//if (bookViewArray.get(viewPagerPgNo) == null) {
				View view = getLayoutInflater().inflate(R.layout.bookview_inflate, null);
				viewPagerWebView = (SemaWebView) view.findViewById(R.id.bTWebView1);
				viewPagerWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
				viewPagerWebView.getSettings().setJavaScriptEnabled(true);
				viewPagerWebView.setWebViewClient(new WebViewClient());
				viewPagerWebView.getSettings().setAllowContentAccess(true);
				viewPagerWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
				viewPagerWebView.getSettings().setAllowFileAccess(true);
				viewPagerWebView.getSettings().setPluginState(PluginState.ON);
			    adv_search= (WebView) view.findViewById(R.id.adv_search);
			    adv_search.setVisibility(View.INVISIBLE);
				viewPagerCanvasImgView = (ImageView)view.findViewById(R.id.bgImgView);
               
				iv_bookmark = (ImageView) view.findViewById(R.id.iv_bookmark); //changes-b
				iv_bookmark.setOnClickListener(new OnClickListener() {


					@Override
					public void onClick(View v) {
						removeBookMark();
						displayBMark(pageNumber);
						btnBookMark.setEnabled(true);
					}
				});

				parentEnrRLayout = (RelativeLayout) view.findViewById(R.id.rl_tabs_layout);
				btnScrollView = (HorizontalScrollView) view.findViewById(R.id.btnScrollView);
				ll_container = (LinearLayout) view.findViewById(R.id.ll_container);
				btnScrollView.setHorizontalFadingEdgeEnabled(false);
				btnEnrHome = (Button) view.findViewById(R.id.btnEnrHome);
				btnEnrHome.setSelected(true);
				
				parentbkmarkLayout = (RelativeLayout)view.findViewById(R.id.rl_bkmark_layout);
				bookmrk_sv = (HorizontalScrollView) view.findViewById(R.id.btn_bkmark_Sv);
				bookmark_container = (LinearLayout) view.findViewById(R.id.ll_bookmark_container);
				bookmrk_sv.setHorizontalFadingEdgeEnabled(false);
				Button currentbkedit=(Button)view.findViewById(R.id.btn_popup);
				currentbkedit.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						displaycurrentBookMarkTitle(v);
					}
				});
				progressbar=(ProgressBar)view.findViewById(R.id.progressBar1);
				btnEnrHome.setTag(0);
				//tabNo=0;
				btnEnrHome.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						enriched=false;
						//loadingfromcurlpage=false;
						//tabNo=0;
						tabNo=(Integer)v.getTag();
						type="Home";
						webView.setVisibility(View.VISIBLE);
				    	adv_search.setVisibility(View.INVISIBLE);
						loadHighlightAndNote(dir);
						makeEnrichedBtnSlected(v);
						displayBookMark(pageNumber,tabNo);
						checkForPaintAndApply(canvasImgView,pageNumber);
					}
				});
				progressbar.setVisibility(View.VISIBLE);
				loadHighlightAndNoteForViewPager(dir);
				checkForPaintAndApply(viewPagerCanvasImgView,viewPagerPgNo);
				//Enrich changes:
				selectedBook();
				reloadll_container();
				loadTabs(viewPagerPgNo);
				
				viewPagerWebView.setWebViewClient(new WebViewClient(){
					@Override
					public void onScaleChanged(WebView view, float oldScale,
							float newScale) {
						((SemaWebView)view).mTextSelectionSupport.onScaleChanged(oldScale, newScale);
					}
					@Override
					public void onPageFinished(WebView view, String url){
						String searcText;
						////System.out.println("On page finished URL: "+url+" and TabNo:"+tabNo+" and pageno:"+pageNumber+" and viewPager:"+viewPagerPgNo);
						int pNo= viewPagerPgNo - 1;
						String[] str = url.split("/");  //URL: file:///data/data/com.semanoor.manahij/files/.M33Book/Enrichments/Page5/Enriched299/index.htm
						String[] str1 = str[str.length-1].split("\\.");
						if(str1[0].equals("index")){
							pNo = Integer.parseInt(str[9].replace("Page", ""));
						}else{
							//System.out.println(Integer.parseInt(str1[0]));
							pNo = Integer.parseInt(str1[0]);
						}
						loadHighlightForViewPager(view,pNo);
						loadNoteForViewPager(view,pNo);
						if(advBookFlag == 1){
							searcText = etAdv.getText().toString();
						}
						else{
							searcText = searchEditText.getText().toString();
						}
						searcText = searcText.replaceAll("\'\'", "\'").replaceAll("\"", "dqzd");
						int i =0;
						//webView.loadUrl("javascript:" + javascriptSrc + "Highlight('"+ searcText +"','"+ i +"', \"searchhighlight\");");
						((SemaWebView)webView).loadJSScript("javascript:" + javascriptSrc + "Highlight('"+ searcText +"','"+ i +"', \"searchhighlight\");", webView);
						LSearchFlag = 0;
						advBookFlag = 0;
						//Method to make tab selected based on tabNo:
						if(loadingfromIndexpage==true){
							webView.setVisibility(View.VISIBLE);
					    	adv_search.setVisibility(View.INVISIBLE);
							setEnrichTabSelected();
							if(type.equals("Search")){
								adv_search.setVisibility(View.VISIBLE);
						    	webView.setVisibility(View.INVISIBLE);
								loadAdvancedSearchWebView(epath,type);
							}
							loadingfromIndexpage=false;

						}else{
							 setTabValue();
							 displayBookMark(pageNumber,tabNo);
							 displayBMark(pageNumber);
						}
						//progressbar.setVisibility(View.INVISIBLE);
						adView.setVisibility(View.VISIBLE);
					}

					private void loadHighlightForViewPager(final WebView view, int pNumber) {
                            
						final String[] sTextArray = db.getStext(bookName, pNumber, tabNo);
						final int[] occcurenceArray = db.getOccurence(bookName, pNumber, tabNo);
						for(int i = 0; i<occcurenceArray.length; i++){
							//webView.loadUrl("javascript:Highlight('"+sTextArray[i].replaceAll("\'\'", "\'").replaceAll("\"", "dqzd")+"', '"+occcurenceArray[i]+"', \"highlight\")");
					
							((SemaWebView)view).loadJSScript("javascript:Highlight('"+sTextArray[i].replaceAll("\'\'", "\'").replaceAll("\"", "dqzd")+"', '"+occcurenceArray[i]+"', \"highlight\")", view);
						}
					}

					@Override
					public boolean shouldOverrideUrlLoading(WebView view, String url){
						//////System.out.println("url"+url);
						String[] searchResults = url.split("/");
						if (searchResults.length > 3) {
							if(searchResults[3].equalsIgnoreCase("E")){
								webView.addNoteFromPopMenu = false;
								String vc = "E|"+searchResults[4];
								addNote("", vc);
							}
						}
						return true;
					}
				});
				//bookViewArray.remove(viewPagerPgNo);
				//bookViewArray.add(viewPagerPgNo, view);
			 //} 
			  if(bookMarkedAdvTabList.get(viewPagerPgNo)==null){
				   loadbookmarkTabs(viewPagerPgNo);
			  }
			  //loadbookmarkTabs(viewPagerPgNo);
			((ViewPager)container).addView(view, 0);
			//return bookViewArray.get(viewPagerPgNo);
			return view;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object){
			((ViewPager)container).removeView((View) object);
			int destroyPos;
			if (LBooklanguage.equalsIgnoreCase("English")) {
				destroyPos = position + 1;
			}else{
				destroyPos = pageCount-position;
			}
			 bookMarkedAdvTabList.remove(destroyPos);
			 bookMarkedAdvTabList.add(destroyPos, null);
		}

		@Override
		public void setPrimaryItem(ViewGroup container, int position, Object object){

			////System.out.println("Primary item: "+position);
			RelativeLayout rl = (RelativeLayout)object;
			webView = (SemaWebView) rl.getChildAt(0);
			adv_search=(WebView)rl.getChildAt(1);
			
			iv_bookmark = (ImageView) rl.getChildAt(3);
			canvasImgView = (ImageView) rl.getChildAt(2);

			parentEnrRLayout = (RelativeLayout)rl.getChildAt(4);
		    sv = (HorizontalScrollView) parentEnrRLayout.getChildAt(0);
			currentEnrTabsLayout = (LinearLayout) sv.getChildAt(0);
			
			
			currentparentbklayout = (RelativeLayout)rl.getChildAt(5);
		    currentbkmarkscroll = (HorizontalScrollView) currentparentbklayout.getChildAt(0);
		    currentbkmarkLayout = (LinearLayout) currentbkmarkscroll.getChildAt(0);
		    //Button currentbkedit=(Button)currentparentbklayout.getChildAt(1);
		    progressbar=(ProgressBar)rl.getChildAt(6);
		    viewPager.webView = webView;
			webView.setWebViewClient(new WebViewClient(){
				@Override
				public void onScaleChanged(WebView view, float oldScale,
						float newScale) {
					((SemaWebView)view).mTextSelectionSupport.onScaleChanged(oldScale, newScale);
				}
				@Override
				public void onPageFinished(WebView view, String url){

					displayBMark(pageNumber);
					String searcText;
					loadHighlight();
					loadNote();
					if(advBookFlag == 1){
						searcText = etAdv.getText().toString();
					}
					else{
						searcText = searchEditText.getText().toString();
					}
					searcText = searcText.replaceAll("\'\'", "\'").replaceAll("\"", "dqzd");
					int i =0;
					//webView.loadUrl("javascript:" + javascriptSrc + "Highlight('"+ searcText +"','"+ i +"', \"searchhighlight\");");
					((SemaWebView)webView).loadJSScript("javascript:" + javascriptSrc + "Highlight('"+ searcText +"','"+ i +"', \"searchhighlight\");", webView);
					LSearchFlag = 0;
					advBookFlag = 0;
					adView.setVisibility(View.VISIBLE);
				}

				private void loadHighlight() {
					String[] sTextArray = db.getStext(bookName, pageNumber, tabNo);
					int[] occcurenceArray = db.getOccurence(bookName, pageNumber, tabNo);
					for(int i = 0; i<occcurenceArray.length; i++){
						//webView.loadUrl("javascript:Highlight('"+sTextArray[i].replaceAll("\'\'", "\'").replaceAll("\"", "dqzd")+"', '"+occcurenceArray[i]+"', \"highlight\")");
						((SemaWebView)webView).loadJSScript("javascript:Highlight('"+sTextArray[i].replaceAll("\'\'", "\'").replaceAll("\"", "dqzd")+"', '"+occcurenceArray[i]+"', \"highlight\")", webView);
					}
				}
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url){
					//////System.out.println("url"+url);
					String[] searchResults = url.split("/");
					if (searchResults.length > 3) {
						if(searchResults[3].equalsIgnoreCase("E")){
							webView.addNoteFromPopMenu = false;
							String vc = "E|"+searchResults[4];
							addNote("", vc);
						}
					}
					return true;
				}
			});
		}
	}
	
	public void checkForPaintAndApply(ImageView cvsImageView, int pageNo){
		
		if(tabNo>=1){
			objContent = drawnFilesDir+"Page"+" "+pageNo+"-"+tabNo+".png";
			}
			else{
				objContent = drawnFilesDir+"Page"+" "+pageNo+".png";
			}
		setBackgroundImageForDrawing(cvsImageView);
	}

	public void copyNoteImage(){
		Bitmap noteIcon = BitmapFactory.decodeResource(getResources(), R.drawable.stick_samll);
			BitmapFactory.Options bmOptions;
			bmOptions = new BitmapFactory.Options();
			bmOptions.inSampleSize = 1;

			String bookMarkPath =Environment.getDataDirectory().getPath().concat("/data/"+Globals.getCurrentProjPackageName()+"/files/").concat("BookMarkImage/");
			File wallpaperDirectory = new File(bookMarkPath);
			wallpaperDirectory.mkdirs();
			OutputStream outStream = null;

			File file = new File(wallpaperDirectory,"stick_samll.png");

			try {
				outStream = new FileOutputStream(file);
				noteIcon.compress(Bitmap.CompressFormat.PNG, 100, outStream);
				outStream.flush();
				outStream.close();


			} catch (FileNotFoundException e) {
				e.printStackTrace();


			} catch (IOException e) {
				e.printStackTrace();

			}

	}
	/* Enriched */

	public void selectedBook() {

		if(bookName.contains("M")){
			enrichPageArray.clear();
			try {

				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();
				DefaultHandler handler = new DefaultHandler(){
					String currentNode = "";
					public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException 
					{
						if(qName.contentEquals("Cover1"))
						{
							String pageTitle = attributes.getValue("ATitle");
							String path = attributes.getValue("Path");
							String pathAndTitle = pageTitle+"U+202B"+path+"|";
							//////System.out.println("pathTitle:"+pathAndTitle);
							xmlValues = pathAndTitle;
							currentNode = qName;
							//////System.out.println("xmlValues:"+xmlValues);
						}
						else if(qName.contentEquals("Cover2"))
						{
							String pageTitle = attributes.getValue("ATitle");
							String path = attributes.getValue("Path");
							String pathAndTitle = pageTitle+"U+202B"+path+"|";
							//////System.out.println("pathTitle:"+pathAndTitle);
							xmlValues = pathAndTitle;
							currentNode = qName;
							//////System.out.println("xmlValues:"+xmlValues);
						}
						else if(qName.contains("P")){
							String pageTitle = attributes.getValue("ATitle");
							String path = attributes.getValue("Path");
							String pathAndTitle = pageTitle+"U+202B"+path+"|";
							//////System.out.println("pathTitle:"+pathAndTitle);
							xmlValues = pathAndTitle;
							currentNode = qName;
							//////System.out.println("xmlValues:"+xmlValues);
						}
						else if(qName.contains("Enr")){
							xmlValues = xmlValues+"##"+attributes.getValue("Path");
							xmlValues = xmlValues+"$$"+attributes.getValue("Title");
							xmlValues = xmlValues+"$$"+attributes.getValue("ID");
							xmlValues = xmlValues+"$$"+attributes.getValue("Type");
						}
					}

					@Override
					public void endElement(String uri, String localName, String qName) throws SAXException {

						if(currentNode.equals(qName)){
							//////System.out.println("xmlValues:"+xmlValues);
							if(!xmlValues.isEmpty()){
								enrichPageArray.add(xmlValues);
								xmlValues = "";
							}
						}
						//super.endElement(uri, localName, qName);
					}
				};

				File sdPath = getFilesDir();
				//revert_book_hide :
				filePath = currentBookPath+"Book.xml";
				//filePath = sdPath+"/."+bookName+"Book/Book.xml";
				//filePath = sdPath+"/"+bookName+"Book/Book.xml";
				InputStream inputStream = new FileInputStream(filePath);
				Reader reader = new InputStreamReader(inputStream, "UTF-8");

				InputSource is = new InputSource(reader);
				is.setEncoding("UTF-8");

				saxParser.parse(is, handler);
				//changes for empty mainpage in book
				if(!xmlValues.isEmpty())
				{
					int lastElement=enrichPageArray.size()-1;
					enrichPageArray.set(lastElement, xmlValues);
				}

				////System.out.println("enrichPge:"+enrichPageArray);
				////System.out.println("XmlValues: "+xmlValues);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Adding current page enrichments path,title,type,pageNo into Enrichments array
	 * @param epath 
	 */
	public void loadEnrichmentObjectList(int position) {
		String[] currentPageContents;
		ArrayList<String> enrich = new ArrayList<String>();
		boolean updated = false;
		if(bookName.contains("M") && !enrichPageArray.isEmpty()){
			currentPageContents = enrichPageArray.get(position-1).split("\\|");
			currentEnrichmentList.clear();
			if(currentPageContents.length > 1){
				String[] enrichments = currentPageContents[1].split("##");
				 for(int i=0;i<enrichments.length;i++){
				    
					 enrich.add(enrichments[i]);
				 }
				for(int i=0; i<enrich.size(); i++){
					String str = enrich.get(i);
					if(!str.isEmpty()){
						String[] str1 = str.split("\\$\\$");
						Enrichments enrichment = new Enrichments(BookView.this);
						enrichment.setEnrichmentId(Integer.parseInt(str1[2]));
						enrichment.setEnrichmentPath(str1[0]);
						enrichment.setEnrichmentTitle(str1[1]);
					    enrichment.setEnrichmentType(str1[3]);
					    enrichment.setEnrichmentPageNo(pageNumber);
					    enrichment.setEnrichmentSequenceId(i);
						currentEnrichmentList.add(enrichment);
					}
				}
			}else{
				tabNo=0;
			 }
		}
 	}
	
	/**
	 * Creating Advanced search bookmarkbutton for current page
	 * @param epath 
	 */
	
	public void addingBookMarkEnrichmnets(String enrTitle) {
		ArrayList<BookMarkEnrichments> currentbmrkEnrichmentList = bookMarkedAdvTabList.get(pageNumber);
		enrTitle = enrTitle.replace("''","'");
 		int objUniqueId = db.getMaxUniqueRowID("BookmarkedSearchTabs");
		
 		BookMarkEnrichments bookmarkenrichment = new BookMarkEnrichments(BookView.this);
		bookmarkenrichment.setEnrichmentId(objUniqueId);
		bookmarkenrichment.setEnrichmentPath(currentEnrichment.getEnrichmentPath());
		bookmarkenrichment.setEnrichmentTitle(enrTitle);
		bookmarkenrichment.setEnrichmentPageNo(pageNumber);
		bookmarkenrichment.setEnrichmentBid(currentBook.getBookID());
		bookmarkenrichment.setSearchTabId(currentEnrichment.getEnrichmentId());
		currentbmrkEnrichmentList.add(bookmarkenrichment);
		
	    Button enrichBtnCreated = bookmarkenrichment.createEnrichmentTabs(currentbkmarkLayout);
	    //makeBookmarkEnrichedBtnSlected(enrichBtnCreated);
	}
	
	/**
	 *Displaying Advsearchbookmark Tabs in Popoverview
	 * @param epath 
	 */
	private void displaycurrentBookMarkTitle(View v) {
		// TODO Auto-generated method stub
		ineditmode=false;
		deletemode=false;
		final ArrayList<BookMarkEnrichments> currentbmrkList = bookMarkedAdvTabList.get(pageNumber);
		
	    advbkmarkpopup = new PopoverView(BookView.this, R.layout.advsearchtitles);
	    advbkmarkpopup.setContentSizeForViewInPopover(new Point((int) getResources().getDimension(R.dimen.info_popover_width), (int) getResources().getDimension(R.dimen.info_popover_height)));
	    advbkmarkpopup.setDelegate(BookView.this);
	    advbkmarkpopup.showPopoverFromRectInViewGroup(BookView.this.designPageLayout, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionUp, true);
		final ListView adv_bkmrktitle = (ListView) advbkmarkpopup.findViewById(R.id.lv_bkmark);
		//adv_bkmrktitle.setAdapter(new adv_bkmrkadapter(BookView.this, currentbmrkList));
		final Button btn_edit=(Button)advbkmarkpopup.findViewById(R.id.btn_edit);
		final Button bt_del=(Button)advbkmarkpopup.findViewById(R.id.btn_del);
		
		btn_edit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!ineditmode){
					ineditmode=true;
					adv_bkmrktitle.invalidateViews();
					btn_edit.setText(getResources().getString(R.string.done));
				}else{
					ineditmode=false;
					adv_bkmrktitle.invalidateViews();
					btn_edit.setText(getResources().getString(R.string.edit));
					
				}
			}
		 });
		
		bt_del.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!deletemode){
					deletemode=true;
					adv_bkmrktitle.invalidateViews();
					bt_del.setText(getResources().getString(R.string.done));
			  }else{
					deletemode=false;
					adv_bkmrktitle.invalidateViews();
					bt_del.setText(getResources().getString(R.string.delete));
             
				}
			}
		});
		adv_bkmrktitle.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View v, int position,
					long arg3) {
				// TODO Auto-generated method stub
				  new clickedAdvancedSearchTabs(currentbmrkList.get(position).getEnrichmentTitle(),currentbmrkList.get(position).getEnrichmentPath()).execute();
				 // clickedAdvancedSearch(currentbmrkList.get(position).getEnrichmentTitle(),currentbmrkList.get(position).getEnrichmentPath());
			      advbkmarkpopup.dissmissPopover(true);
			  }
		  });
     }
	
	/**
	 * Removing Advsearchbookmarkbutton from layout
	 * @param epath 
	 */
	public void removingBkMrkButton(int position, int pageno, int id) {
		// TODO Auto-generated method stub
		 currentbkmarkLayout.removeViewAt(position);
		 db.executeQuery("delete from BookmarkedSearchTabs where Id='"+id+"' and PageNo='"+pageno+"' and Bid='"+currentBook.getBookID()+"'");
	     if(currentbkmarkLayout.getChildCount()<1){
	    	 currentparentbklayout.setVisibility(View.GONE);
	    	 advbkmarkpopup.dissmissPopover(true);
	    }
	}
	
	
	/**
	 * Changing the name to bookmarkbutton
	 * @param epath 
	 */
	public void updateNameToButton(int position, String newtext) {
		 newtext = newtext.replace("''","'");
		 //RelativeLayout rlview = (RelativeLayout) currentbkmarkLayout.getChildAt(position);
		 Button nextEnrichSelectView = (Button)currentbkmarkLayout.getChildAt(position);
		 nextEnrichSelectView.setText(newtext);
	}
	/**
	 * Enrichment Button clicked event
	 * @param epath 
	 */
	public void btnSearchEnrichmentClicked(View view){
		  makeEnrichedBtnSlected(view);
	}
	
	/**
	 * Loading url for advsearchEnrichment tabs
	 * @param epath 
	 */
	public void loadAdvancedSearchWebView(final String url,String type) {
	//parentEnrRLayout.setVisibility(View.VISIBLE);
		
			adv_search.setVisibility(View.GONE);
			if(BookView.currentEnrTabsLayout.getChildCount()>1){
				parentEnrRLayout.setVisibility(View.VISIBLE);
			}else{
				parentEnrRLayout.setVisibility(View.GONE);
			}
			adv_search.clearHistory();
			adv_search.clearFormData();
			adv_search.getSettings().setJavaScriptEnabled(true);
			adv_search.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			adv_search.getSettings().setJavaScriptEnabled(true);
			adv_search.setWebViewClient(new WebViewClient());
			adv_search.getSettings().setAllowContentAccess(true);
			adv_search.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		    adv_search.getSettings().setAllowFileAccess(true);
			adv_search.getSettings().setPluginState(PluginState.ON);
			adv_search.clearCache(true);
			adv_search.setVisibility(View.VISIBLE);
			adv_search.getSettings().setDomStorageEnabled(true);
			adv_search.setWebViewClient(new WebViewClient());
			adv_search.loadUrl(url);
			progressbar.setVisibility(View.VISIBLE);
		
	  adv_search.setWebViewClient(new WebViewClient(){
		@Override
		public void onPageFinished(WebView view, String url){
		     progressbar.setVisibility(View.INVISIBLE);	
		}
	});
	
	adv_search.setOnTouchListener(new OnTouchListener() {
       
		  @Override
		  public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			  adv_search.setWebViewClient(new CustomWebViewClient());
			
			return false;
		}
	  });
   }
/**
 * Your Webview Client.
 * @author ru
 *
 */
 public class CustomWebViewClient extends WebViewClient {
    @Override
    public void onReceivedError(WebView view, int errorCode,
            String description, String failingUrl) {
        Log.d("WEB_ERROR", "error code:" + errorCode + ":" + description);
     String title;
     title=currentEnrichment.getEnrichmentTitle();
     String url="";
     //   String url="http://www.google.com";
 	  new clickedAdvancedSearchTabs(title, url).execute(); 
       // clickedAdvancedSearch(title, url);
    }

    @Override
    // Method where you(get) load the 'URL' which you have clicked
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
    	
        try {
        	String title;
  	        title=currentEnrichment.getEnrichmentTitle();
            if (url != null ) {
            	  String path=url;
          		  String[] adv_bkmrktype=path.split("/");
          		  String url1=  adv_bkmrktype[2].replace(".", "/");
          		  String[] type=url1.split("/");
          		  for(int i=0;i<type.length;i++){
          			  if(i==type.length-2){
          				 // String semasearch=type[0];
          				 new clickedAdvancedSearchTabs(type[i], url).execute();
                          //clickedAdvancedSearch(type[i], url);
                      }
          		  }
          		     return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        // mWebView.setVisibility(View.VISIBLE);
     }

	}
	
	/**
	 * Deleting  advsearchEnrichment tabs in current page
	 * @param epath 
	 */
	public void deletingAdvsearchEnrichments(View v, int id) {
		// TODO Auto-generated method stub
		
		for(int i=0;i<currentEnrichmentList.size();i++){
			
			currentEnrichment = currentEnrichmentList.get(i);
		    
			if(id==currentEnrichment.getEnrichmentId() && currentEnrichment.getEnrichmentType().equals("Search")){
		    	int pageno=currentEnrichment.getEnrichmentPageNo();
		    	String path=currentEnrichment.getEnrichmentPath();
		 	    String enrtype=currentEnrichment.getEnrichmentType();
		 		String Id=String.valueOf(currentEnrichment.getEnrichmentId());
		 		//System.out.println(currentEnrichmentList.get(i)+"current");
		 		//System.out.println(currentEnrichmentList+"current");
		 		
		 		String bookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/"+"Book.xml";
		 		currentEnrichment.deleteAdvSearchEnrichmentInBookXml(pageno,Id,enrtype,bookPath);
		 		currentEnrichmentList.remove(i);
		 		//deletingadvsearch(i);
		 		selectedBook();
		 		//System.out.println(currentEnrichmentList.size());
		 		currentEnrTabsLayout.removeViewAt(i+1);
				db.executeQuery("delete from enrichments where enrichmentID='"+currentEnrichment.getEnrichmentId()+"' and pageNO='"+pageNumber+"' and Type='"+enrtype+"'");
				int tabcount =currentEnrTabsLayout.getChildCount()-1;
				
				if(tabcount==0){
				   View nextEnrichSelectView = currentEnrTabsLayout.getChildAt(currentEnrTabsLayout.getChildCount()-1);
				   makeEnrichedBtnSlected(nextEnrichSelectView);
				   tabNo=0;
				   webView.setVisibility(View.VISIBLE);
	 			   adv_search.setVisibility(View.INVISIBLE);
				   displayBookMark(pageNumber,tabNo);
				   enriched=false;
				   loadHighlightAndNote(dir);
				}else{
					Enrichments nextEnr = null;
					 if(currentEnrichmentList.size()==tabcount)
						  nextEnr = currentEnrichmentList.get(tabcount-1);
					 for (int j = 0; j < currentEnrTabsLayout.getChildCount(); j++) {
							if(j==0){
							    Button btnEnr = (Button) currentEnrTabsLayout.getChildAt(j);
							}else{
							    
								RelativeLayout rlview = (RelativeLayout) currentEnrTabsLayout.getChildAt(j);
								Button btnEnrTab = (Button) rlview.getChildAt(0);
								
								if (btnEnrTab.getTag().equals(nextEnr.getEnrichmentId())) {
									if (nextEnr.getEnrichmentType().equals("Search")) {
										makeEnrichedBtnSlected(btnEnrTab);
										type=nextEnr.getEnrichmentType();
										tabNo=nextEnr.getEnrichmentId();
										webView.setVisibility(View.INVISIBLE);
										adv_search.setVisibility(View.VISIBLE);
										loadAdvancedSearchWebView(nextEnr.getEnrichmentPath(),nextEnr.getEnrichmentType());
										displayBookMark(pageNumber,tabNo);
									} else {
										webView.setVisibility(View.VISIBLE);
						 				adv_search.setVisibility(View.INVISIBLE);
										makeEnrichedBtnSlected(btnEnrTab);
										enriched=false;
										tabNo=nextEnr.getEnrichmentId();
									    type=nextEnr.getEnrichmentType();
										displayBookMark(pageNumber,tabNo);
										loadHighlightAndNote(dir);
									}
								 }
							   }
							}
					     
			  	       }
				if (currentEnrTabsLayout.getWidth()>parentEnrRLayout.getWidth()) {
					int  enrTabsWidth1 = currentEnrTabsLayout.getWidth();
					sv.smoothScrollTo(enrTabsWidth1, 0);
				}
				checkForPaintAndApply(canvasImgView,pageNumber);
				  }
			  }
		  }


	
   /*Enriched*/
	public void loadTabs(int position){
		String[] currentPageContents;
		if(bookName.contains("M") && !enrichPageArray.isEmpty()){
			////System.out.println("Current page number:"+position);
			currentPageContents = enrichPageArray.get(position-1).split("\\|");
			//displayBookMark(position-1,tabNo);
			ArrayList<String> enrich = new ArrayList<String>();
			boolean updated = false;

			//[√É∆í√Ç¬ø√É‚Äö√Ç¬µ√É‚Ä¶√Ç¬∏√É∆í√¢‚Ç¨¬¶√É∆í√Ç¬ø√É¬¢√¢‚Ç¨¬∞√Ç¬†√É∆í√Ç¬ø√É‚Äö√Ç¬© 2U+202B3.htm, -Enrichments/Page2/Enriched343/index.htm+Enrichment1-Enrichments/Page2/Enriched313/index.htm+Enrichment1]
			//header = (ImageView) findViewById(R.id.imageView2);//changes-e

			if(currentPageContents.length > 1){
				////System.out.println("CurrentPageContents:"+currentPageContents[1]);
				String[] enrichments = currentPageContents[1].split("##");
			    for(int i=0;i<enrichments.length;i++){
					 enrich.add(enrichments[i]);
						
			       }
                parentEnrRLayout.setVisibility(View.VISIBLE);
				btnScrollView.setVisibility(View.VISIBLE);
				createTabs(enrich);
				btnScrollView.setAnimation(animEnrichSlideIn);
				animEnrichSlideIn.start();
			}else{
				parentEnrRLayout.setVisibility(View.GONE);
			}
		}
	}
	/*Enriched*/

	private void reloadll_container(){
		for (int i = 0; i < ll_container.getChildCount(); i++) {
			View view = ll_container.getChildAt(i);
			if (view != ll_container.getChildAt(0)) {
				//Don't remove view:
				ll_container.removeView(view);
			}
		}
	}

	public Button createTabs(ArrayList<String> enrich){
       
		final ArrayList<String> enrichPath = new ArrayList<String>();
		ArrayList<String> enrichTitle = new ArrayList<String>();
		final ArrayList<String> enrichId = new ArrayList<String>();
		final ArrayList<String> enrichtype = new ArrayList<String>();
		for(int i=0; i<enrich.size(); i++){
			String str = enrich.get(i);
			if(!str.isEmpty()){
				String[] str1 = str.split("\\$\\$");
				enrichPath.add(str1[0]);
				enrichTitle.add(str1[1]);
				enrichtype.add(str1[3]);
				enrichId.add(str1[2]);
			}
		}
		//Enrich button properties :
		for(enrich_id=1;enrich_id<=enrichTitle.size();enrich_id++){
			RelativeLayout layout=new RelativeLayout(this);
			layout.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));

			final String enrtype=enrichtype.get(enrich_id-1);
			final Button btnEnrichmentTab = new Button(this);
			btnEnrichmentTab.setId(enrich_id);
			btnEnrichmentTab.setTag(Integer.parseInt(enrichId.get(enrich_id-1)));
			btnEnrichmentTab.setSingleLine(true);
			btnEnrichmentTab.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.enrichments_tex_size));
			btnEnrichmentTab.setTextColor(Color.parseColor("#000000"));
			btnEnrichmentTab.setBackgroundResource(R.drawable.btn_enrich_tab);
			if(enrtype.equals("Search")){
				String text=enrichTitle.get(enrich_id-1);
				if(text.length()>9){
					String subString=text.substring(0,8);
					String t=subString+"....";
					//System.out.println(t+"text");
					btnEnrichmentTab.setText(t);
				}else{
					btnEnrichmentTab.setText(enrichTitle.get(enrich_id-1));
				}
				String url=enrichPath.get(enrich_id-1);
				btnEnrichmentTab.setGravity(Gravity.CENTER); 
				int id=Integer.parseInt(enrichId.get(enrich_id-1));
				new lvimage(btnEnrichmentTab,0).execute("http://www.google.com/s2/favicons?domain="+url);
			}else{
				btnEnrichmentTab.setText(enrichTitle.get(enrich_id-1));
				btnEnrichmentTab.setGravity(Gravity.CENTER); 
			}

			layout.addView(btnEnrichmentTab);	 
			btnEnrichmentTab.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) { 
					adv_search.setVisibility(View.GONE);
					makeEnrichedBtnSlected(btnEnrichmentTab);
					epath=currentEnrichment.getEnrichmentPath();
					tabNo=(Integer)v.getTag();
					type=currentEnrichment.getEnrichmentType();

					if(type.equals("Search")){
						adv_search.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
						adv_search.clearCache(true);
						adv_search.clearHistory();
						progressbar.setVisibility(View.VISIBLE);
						webView.setVisibility(View.INVISIBLE);
						adv_search.setVisibility(View.VISIBLE);
						loadAdvancedSearchWebView(epath,type);
						//displayBookMark(pageNumber,tabNo);
					}else{
						progressbar.setVisibility(View.VISIBLE);
						webView.setVisibility(View.VISIBLE);
						adv_search.setVisibility(View.INVISIBLE);
						enriched=true;
						loadHighlightAndNote(dir);
						progressbar.setVisibility(View.INVISIBLE);
					}

					checkForPaintAndApply(canvasImgView,pageNumber);
					displayBookMark(pageNumber,tabNo);
				}
			});
			btnEnrichmentTab.setOnLongClickListener(new OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {

					//tabNo=v.getId();
					tabNo=(Integer)v.getTag();
					//epath=currentEnrichment.getEnrichmentPath();
					if(tabNo!=0){
						currentEnrichment = currentEnrichmentList.get(v.getId()-1);
					}
					type=currentEnrichment.getEnrichmentType();
					//enrichPath.get(index);
					if(type.equals("Search")){
						showAdvsearchEnrichDialog((Button) v);
					}
					return false;
				}
			});
			if(enrtype.equals("Search")){

				RelativeLayout.LayoutParams   lp = new RelativeLayout.LayoutParams((int)getResources().getDimension(R.dimen.bookview_searchTab_remove),(int)getResources().getDimension(R.dimen.bookview_searchTab_height));

				Button btnEnrichDelete = new Button(this);
				btnEnrichDelete.setId(enrich_id);
				btnEnrichDelete.setTag(Integer.parseInt(enrichId.get(enrich_id-1)));
				btnEnrichDelete.setBackgroundResource(R.drawable.btn_enr_del);
				btnEnrichDelete.bringToFront();
				lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				lp.addRule(RelativeLayout.ALIGN_BASELINE,btnEnrichmentTab.getId());
				lp.addRule(RelativeLayout.CENTER_VERTICAL,btnEnrichmentTab.getId());
				lp.setMargins(0, 0,(int)getResources().getDimension(R.dimen.btn_remove),0); //30
				btnEnrichDelete.setLayoutParams(lp);
				layout.addView(btnEnrichDelete);

				btnEnrichDelete.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						tabNo=(Integer)v.getTag();
						String id= enrichId.get(v.getId()-1);
						BookView.removeBookMark();
						deletingAdvsearchEnrichments(v, Integer.parseInt(id));
				 	}
			   });
            }
            //ll_container.addView(layout, new LayoutParams((int) getResources().getDimension(R.dimen.btn_enrichment_width), LayoutParams.MATCH_PARENT));
           // //System.out.println(Globals.getDeviceIndependentPixels(150, BookView.this));
			ll_container.addView(layout, new LayoutParams(Globals.getDeviceIndependentPixels(150, BookView.this), LayoutParams.WRAP_CONTENT));
           // ll_container.addView(layout, new LayoutParams((int)getResources().getDimension(R.dimen.enrichment_tab_width), LayoutParams.MATCH_PARENT));
		}
		//setEnrichTabSelected();
		//changes for empty mainpage in book
		if(sourceString.contains("inullpageexists") && loadingfromIndexpage==false)
		{
			if(pageNumber==1)
			{
				tv_pageNumber.setText("Enriched Page");
			}
		}

		if(loadingfromIndexpage==true)
		{
			if(tabNo == 0)
			{
				//enriched=false;
				//loadingfromcurlpage=false;
			}
			else
			{	
				//btn[0].setBackgroundResource(R.drawable.tabhome);
				//btn[tabNo].setBackgroundResource(R.drawable.tab_off);
				//enriched=true;
				//epath=enrichPath.get(tabNo-1);
				//lastbuttonclicked=tabNo;
			}

			//loadingfromIndexpage=false;
			loadHighlightAndNote(dir);
		}
		else
		{
			tabNo=0;
		}

		return btnEnrich;
	}

	
	public class lvimage extends AsyncTask<String, Void, Bitmap>{
		ImageView imagepath;
		ImageView img;
		Button btn;
		int id;
		public lvimage(Button btnEnrichmentTab, int i) {
			// TODO Auto-generated constructor stub
			this.btn=btnEnrichmentTab;
			id=i;
		}
		protected Bitmap doInBackground(String... urls) {
			String URLpath=urls[0];
			Bitmap bmp = null;
			try {
				URL url=new URL(URLpath);
				URLConnection ucon = url.openConnection();
				ucon.connect();
				bmp=BitmapFactory.decodeStream(url.openConnection().getInputStream());
				//System.out.println(bmp.getWidth());;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return bmp;

	    }
       protected void onPostExecute(Bitmap result){
    	   if(result==null){

    	   }else{
    		   int width=(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, result.getWidth(), getResources().getDisplayMetrics());
    		   int height=(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, result.getHeight(), getResources().getDisplayMetrics());

    		   Bitmap b=Bitmap.createScaledBitmap(result, width,height, false);
    		   Drawable f=new BitmapDrawable(getResources(), b);
    		   String Path = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+"bkmarkimages/";
    		   //String imgpath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BookView.bookName+"/";
    		   UserFunctions.createNewDirectory(Path);
    		   String correct= Path+id+".png";
    		   btn.setCompoundDrawablesWithIntrinsicBounds(f, null, null, null);

    		   if(id!=0){
    			   UserFunctions.saveBitmapImage(b, correct);
    		   }
    	   }
       }
	}
	/**
	 * Creating the Advanced Search bookmark button in the viewpager
	 * @param epath 
	 */
	private void loadbookmarkTabs(int viewPagerPgNo) {
		// TODO Auto-generated method stub
		BookMarkEnrichments currentPagebookmark;
		if(bookName.contains("M")){
             ArrayList<BookMarkEnrichments> bkMarkList= db.getAllbookMarkTabListForTheBook(viewPagerPgNo,currentBook.getBookID(),BookView.this);

			bookMarkedAdvTabList.remove(viewPagerPgNo);
			bookMarkedAdvTabList.add(viewPagerPgNo, bkMarkList);
			if(bookMarkedAdvTabList.get(viewPagerPgNo).size()>0){
			
			 for(int i=0;i< bookMarkedAdvTabList.get(viewPagerPgNo).size();i++) {

				currentPagebookmark = bookMarkedAdvTabList.get(viewPagerPgNo).get(i); 
				parentbkmarkLayout.setVisibility(View.VISIBLE);
				bookmrk_sv.setVisibility(View.VISIBLE);

				BookMarkEnrichments bookmarkenrichment = new BookMarkEnrichments(BookView.this);
				bookmarkenrichment.setEnrichmentBid(currentPagebookmark.getEnrichmentBid());
				bookmarkenrichment.setEnrichmentId(currentPagebookmark.getEnrichmentId());
				bookmarkenrichment.setEnrichmentPath(currentPagebookmark.getEnrichmentPath());
				bookmarkenrichment.setEnrichmentPageNo(currentPagebookmark.getEnrichmentPageNo());
				bookmarkenrichment.setEnrichmentTitle(currentPagebookmark.getEnrichmentTitle());
				bookmarkenrichment.setIsForAllPages(currentPagebookmark.isIsForAllPages());
				bookmarkenrichment.setSearchTabId(currentPagebookmark.getSearchTabId());
                 
				
				Button enrichBtnCreated = bookmarkenrichment.BkMarkTabsForAllPagesinReadMode();
				bookmrk_sv.setAnimation(animEnrichSlideIn);
				animEnrichSlideIn.start();
			  } 
			}else{
				parentbkmarkLayout.setVisibility(View.GONE);
			}
		}else{
			parentbkmarkLayout.setVisibility(View.GONE);
		}
	}

	/**
	 * Make Enrich Button to be selected
	 * @param v
	 */
	public void makeEnrichedBtnSlected(View v){
		for (int i = 0; i < currentEnrTabsLayout.getChildCount(); i++) {
			Button btnEnrTab;
			if (i == 0) {
				btnEnrTab = (Button) currentEnrTabsLayout.getChildAt(i);
			} else {
			RelativeLayout rlview = (RelativeLayout) currentEnrTabsLayout.getChildAt(i);
			btnEnrTab = (Button) rlview.getChildAt(0);
			}
			if (btnEnrTab == v) {
				v.setSelected(true);
				 if(i!=0){
					currentEnrichment = currentEnrichmentList.get(i-1);
				    }
			} else {
				btnEnrTab.setSelected(false);
			}
		}
	}

	public void makeBookmarkEnrichedBtnSlected(View v){
		for (int i = 0; i < currentbkmarkLayout.getChildCount(); i++) {
			View view = currentbkmarkLayout.getChildAt(i);
			if (view == v) {
				v.setSelected(true);
			} else {
				view.setSelected(false);
			}
		}
	}
	
	/** 
	 *  displaying dialog to add bookmark for advsearch buttons 
	 *  @param epath 
	 */
	public void showAdvsearchEnrichDialog(final Button btnEnr){
		final Dialog enrichDialog = new Dialog(this);
		enrichDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
		enrichDialog.setTitle(R.string.advsearch_bookmark);
	    enrichDialog.setContentView(this.getLayoutInflater().inflate(R.layout.advsearch_bkmark_dialog, null));
		enrichDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, this), LayoutParams.WRAP_CONTENT);
		final EditText editText = (EditText) enrichDialog.findViewById(R.id.enr_editText);
		editText.setText(currentEnrichment.getEnrichmentTitle());
        Button btnSave = (Button) enrichDialog.findViewById(R.id.enr_save);
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String enrTitle = editText.getText().toString();
				enrTitle = enrTitle.replace("'", "''");
				currentparentbklayout.setVisibility(View.VISIBLE);
				db.executeQuery("insert into BookmarkedSearchTabs (Title, SearchTabId, PageNo,IsForAllPages,Path,Bid) values('"+enrTitle+"', '"+currentEnrichment.getEnrichmentId()+"', '"+currentEnrichment.getEnrichmentPageNo()+"','False','"+currentEnrichment.getEnrichmentPath()+"','"+currentBook.getBookID()+"')");
				addingBookMarkEnrichmnets(enrTitle);
				advsearchbkmark=true;
				enrichDialog.setOnDismissListener(BookView.this);
				enrichDialog.dismiss();
			}
		});
		
		enrichDialog.show();
	}
	
	
	private static void setPageFilter(){
		String pageFilter = db.getPageFilter(bookName);
		String[] pageFilterArray = pageFilter.split("\\|");
		LSearch = pageFilterArray[0];
		LBookmark = pageFilterArray[1];
		LCopy = pageFilterArray[2];
		LFlip = pageFilterArray[3];
		LNavigation = pageFilterArray[4];
		LHighlight = pageFilterArray[5];
		LNote = pageFilterArray[6];
		LGotoPage = pageFilterArray[7];
		LindexPage = pageFilterArray[8];
		LZoom = pageFilterArray[9];
		LWebSearch = pageFilterArray[10];
		LTranslationSearch = pageFilterArray[11];
		LBooklanguage = pageFilterArray[12];
	}

	//Purchase Agreement
	public void purchaseGeneral()
	{
		int popWidth = 0;
		int popHeight = 0;
		purchasePopup.showAtLocation(purchaseRelLayout, Gravity.CENTER, 0, 0);
		popWidth = 70*deviceWidth/100;
		popHeight = 56*deviceHeight/100;
		purchasePopup.update(popWidth, popHeight);
		purchaseProgressBar.setVisibility(View.VISIBLE);
		//if(lngCategoryID == 1)
		//{
			purchaseAgreementWebView.loadUrl("http://school.nooor.com/sboook/purchaseEN.html");
			purchaseTextView.setText(getResources().getString(R.string.purchase_agreement));
			btnAgree.setText(getResources().getString(R.string.agree));
			btnDonAgree.setText(getResources().getString(R.string.do_not_agree));
		/*}
		else
		{
			purchaseAgreementWebView.loadUrl("http://school.nooor.com/sboook/purchaseAR.html");
			purchaseTextView.setText("√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√Ü‚Äô√É‚Ä¶√Ç¬∏√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√Ç¬¢√É¬¢√¢‚Äö¬¨√Ö¬æ√É‚Äö√Ç¬¢√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É¬¢√¢‚Äö¬¨√Ç¬¶√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√Ü‚Äô√É‚Ä¶√Ç¬∏√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É¬¢√¢‚Äö¬¨√Ç¬°√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É‚Äö√Ç¬§√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√¢‚Ç¨≈°√É‚Äö√Ç¬© √É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√Ü‚Äô√É‚Ä¶√Ç¬∏√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É¬¢√¢‚Äö¬¨√ã≈ì√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√¢‚Ç¨≈°√É‚Äö√Ç¬•√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√¢‚Ç¨≈°√É‚Äö√Ç¬±√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√Ü‚Äô√É‚Ä¶√Ç¬∏√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√¢‚Ç¨≈°√É‚Äö√Ç¬∞");
			btnAgree.setText("√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√¢‚Ç¨≈°√É‚Äö√Ç¬£√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É‚Äö√Ç¬†√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√Ü‚Äô√É‚Ä¶√Ç¬∏√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É¬¢√¢‚Äö¬¨√Ç¬¶√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É¬¢√¢‚Äö¬¨√Ç¬°");
			btnDonAgree.setText("√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É¬¢√¢‚Äö¬¨√ã≈ì√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√Ü‚Äô√É‚Ä¶√Ç¬∏ √É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√¢‚Ç¨≈°√É‚Äö√Ç¬£√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É‚Äö√Ç¬†√É∆í√Ü‚Äô√É‚Äö√Ç¬ø√É∆í√Ü‚Äô√É‚Ä¶√Ç¬∏√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É¬¢√¢‚Äö¬¨√Ç¬¶√É∆í√¢‚Ç¨¬¶√É‚Äö√Ç¬∏√É∆í√Ü‚Äô√É¬¢√¢‚Äö¬¨√Ç¬°");
		}*/
	}

	//Purchase Agreement

	private void dbCheckandUpdate(){
		String query = "update tblBook set Search='"+"yes"+"',Bookmark='"+"yes"+"',Copy='"+"yes"+"',Flip='"+"yes"+"',Navigation='"+"yes"+"',Highlight='"+"yes"+"',Note='"+"yes"+"',GotoPage='"+"yes"+"',IndexPage='"+"yes"+"',Zoom='"+"yes"+"',WebSearch='"+"yes"+"',WikiSearch='"+"yes"+"',google='"+"yes"+"',youtube='"+"yes"+"',semaBook='"+"yes"+"',semaContent='"+"yes"+"',yahoo='"+"yes"+"',bing='"+"yes"+"',ask='"+"yes"+"',jazeera='"+"yes"+"',TranslationSearch='"+"yes"+"',dictionarySearch='"+"yes"+"',booksearch='"+"yes"+"'";
		/*SQLiteDatabase sql = db.openDataBase();
		sql.execSQL("update tblBook set Search='"+"yes"+"',Bookmark='"+"yes"+"',Copy='"+"yes"+"',Flip='"+"yes"+"',Navigation='"+"yes"+"',Highlight='"+"yes"+"',Note='"+"yes"+"',GotoPage='"+"yes"+"',IndexPage='"+"yes"+"',Zoom='"+"yes"+"',WebSearch='"+"yes"+"',WikiSearch='"+"yes"+"',google='"+"yes"+"',youtube='"+"yes"+"',semaBook='"+"yes"+"',semaContent='"+"yes"+"',yahoo='"+"yes"+"',bing='"+"yes"+"',ask='"+"yes"+"',jazeera='"+"yes"+"',TranslationSearch='"+"yes"+"',dictionarySearch='"+"yes"+"',booksearch='"+"yes"+"'");
		sql.execSQL("Update tblPurchase set purchase = '"+"paid"+"'");
		
		sql.close();
		db.close();*/
		db.executeQuery(query);
		setPageFilter();
	}

	private class searchListAdapter extends BaseAdapter{

		public searchListAdapter(BookView bookView) {

		}
		//@Override
		public int getCount() {

			return resultFound.size();
		}
		///@Override
		public Object getItem(int position) {

			return null;
		}

		//@Override
		public long getItemId(int position) {

			return 0;
		}

		//@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View vi = convertView;
			if(convertView == null)
			vi = getLayoutInflater().inflate(R.layout.lng_txtfield, null);
			TextView tv = (TextView) vi.findViewById(R.id.textView1);
			tv.setText(resultFound.get(position));
			tv.setTextColor(Color.rgb(0, 0, 0));
			if(resultFound.isEmpty()==true)//changes-s
			{
				tv_noresult.setVisibility(View.VISIBLE);
			}
			else
			{
				tv_noresult.setVisibility(View.GONE);
			}
			return vi;
		}
	}

	public void GoogleAnalyticsBookScreenView(){

		////System.out.println("Google Analytics Store Screen View");
		//Get tracker:
		Tracker t = ((ManahijApp) getApplication()).getTracker(TrackerName.APP_TRACKER);
		//Set screen name :
		String path = getString(R.string.google_analytics_bookview_name);
		t.setScreenName(path);
		//Send a screen view :
		t.send(new HitBuilders.AppViewBuilder().build());
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus){

		int[] location = new int[2];
		btnBookMark.getLocationOnScreen(location);
		bookMPoint = new Point();
		bookMPoint.x = location[0];
		bookMPoint.y = location[1];

		btnEnrichPage.getLocationOnScreen(location);
		p = new Point();
		p.x = location[0];
		p.y = location[1];
		int y=  (int)btnEnrichPage.getHeight();
		height = y;

		btnenrichPoint= new Point();
		btnenrichPoint.x = location[0];
		btnenrichPoint.y = location[1];

		searchPoint = new Point();
		searchPoint.x = location[0];
		searchPoint.y = location[1];
	}

	public static void removeBookMark() {
		String query = "delete from tblBookMark where BName='"+bookName+"' and PageNo='"+pageNumber+"' and TabNo='"+tabNo+"'";
		db.executeQuery(query);
	}

	private void serachTableReload(String searchText) {
		//////System.out.println("searchText:"+searchText);
		String withoutVowels = escapeHarakat(searchText);
		resultFound.clear();//changes-s
		for(int i=0;i<searchIndexArray.size();i++){
			String fileContent = searchIndexArray.get(i);
			if(fileContent.contains(withoutVowels))
			{
				if(bookName.contains("M")){
					resultFound.add("page " +(i+1));
				}else{
					resultFound.add("page " +i);
				}
			}
			//////System.out.println("fileCont:"+fileContent);
		}
	}

	private String escapeHarakat(String searchText) {

		if(searchText.contains("َ")){
			searchText = searchText.replaceAll("َ", "");
		}
		if(searchText.contains("ً"))
		{
			searchText = searchText.replaceAll("ً", "");
		}
		if(searchText.contains("ُ")){
			searchText = searchText.replaceAll("ُ", "");
		}
		if(searchText.contains("ٌ")){
			searchText = searchText.replaceAll("ٌ", "");
		}
		if(searchText.contains("ِ")){
			searchText = searchText.replaceAll("ِ" , "");
		}
		if(searchText.contains("ٍ")){
			searchText = searchText.replaceAll("ٍ", "");
		}
		if(searchText.contains("ْ")){
			searchText = searchText.replaceAll("ْ", "");
		}
		if(searchText.contains("ّ")){
			searchText = searchText.replaceAll("ّ", "");
		}
		return searchText;
	}

	////facebook
	
	/*private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened()) {
	    	//Log.i(TAG, "Logged in...");
	        //publishButton.setVisibility(View.VISIBLE);
	    } else if (state.isClosed()) {
	    	//Log.i(TAG, "Logged out...");
	        //publishButton.setVisibility(View.INVISIBLE);
	    }
	}*/
	
	private void publishFeedDialog(String userName) {
		Bundle parameters = new Bundle();
		//parameters.putString("link", "http://itunes.apple.com/us/app/manahij/id477095484?ls=1&mt=8");
		parameters.putString("link", "");
		parameters.putString("picture", "http://dev.sboook.com/manahijLogo.png");
		parameters.putString("name", userName+" has Shared");
		parameters.putString("caption", getResources().getString(R.string.app_name));
		parameters.putString("description", selectedText);
		parameters.putString("message", "Share your message!");
        
        // Invoke the dialog
    	WebDialog feedDialog = (
    			new WebDialog.FeedDialogBuilder(this,
    					Session.getActiveSession(),
    					parameters))
    					.setOnCompleteListener(new OnCompleteListener() {

    						@Override
    						public void onComplete(Bundle values,
    								FacebookException error) {
    							if (error == null) {
    								// When the story is posted, echo the success
    				                // and the post Id.
    								final String postId = values.getString("post_id");
        							if (postId != null) {
        								Toast.makeText(BookView.this,
        										"Posted successfully",
        										Toast.LENGTH_SHORT).show();
        							} else {
        								// User clicked the Cancel button
        								Toast.makeText(BookView.this.getApplicationContext(), 
        		                                "Publish cancelled", 
        		                                Toast.LENGTH_SHORT).show();
        							}
    							} else if (error instanceof FacebookOperationCanceledException) {
    								// User clicked the "x" button
    								Toast.makeText(BookView.this.getApplicationContext(), 
    		                                "Publish cancelled", 
    		                                Toast.LENGTH_SHORT).show();
    							} else {
    								// Generic, ex: network error
    								Toast.makeText(BookView.this.getApplicationContext(), 
    		                                "Error Publish text", 
    		                                Toast.LENGTH_SHORT).show();
    							}
    						}

    						
    						})
    					.build();
    	feedDialog.show();
    }

	/*public void postToWall(){

		//getProfileInformation();
		Session session = Session.getActiveSession();
		if (session != null) {
		Bundle parameters = new Bundle();
		//parameters.putString("link", "http://itunes.apple.com/us/app/manahij/id477095484?ls=1&mt=8");
		parameters.putString("link", "");
		parameters.putString("picture", "http://dev.sboook.com/manahijLogo.png");
		parameters.putString("name", name+" has Shared");
		parameters.putString("caption", getResources().getString(R.string.app_name));
		parameters.putString("description", selectedText);
		parameters.putString("message", "Share your message!");

		Request.Callback callback= new Request.Callback() {
	        public void onCompleted(Response response) {
	            JSONObject graphResponse = response
	                                       .getGraphObject()
	                                       .getInnerJSONObject();
	            String postId = null;
	            try {
	                postId = graphResponse.getString("id");
	            } catch (JSONException e) {
	                //Log.i(TAG,"JSON error "+ e.getMessage());
	            }
	            FacebookRequestError error = response.getError();
	            if (error != null) {
	                Toast.makeText(BookView.this
	                     .getApplicationContext(),
	                     error.getErrorMessage(),
	                     Toast.LENGTH_SHORT).show();
	                } else {
	                    Toast.makeText(BookView.this
	                         .getApplicationContext(), 
	                         postId,
	                         Toast.LENGTH_LONG).show();
	            }
	        }
	    };

	    Request request = new Request(session, "me/feed", parameters, 
	                          HttpMethod.POST, callback);

	    RequestAsyncTask task = new RequestAsyncTask(request);
	    task.execute();
	    
		}
		
		/*facebook.dialog(this, "feed", parameters, new DialogListener() {

			//@Override
			public void onFacebookError(FacebookError e) {


			}
			//@Override

			//@Override
			public void onComplete(Bundle values) {


			}

			//@Override
			public void onCancel() {


			}

			public void onError(DialogError e) {


			}
		});
	}*/

	/*public void getProfileInformation(){

		mAsyncRunner.request("me", new RequestListener() {

			//@Override
			public void onComplete(String response, Object state) {
				Log.d("Profile", response);
				String json = response;

				try {
					// Facebook Profile JSON data
					JSONObject profile = new JSONObject(json);

					// getting name of the user
					name = profile.getString("name");

					// getting email of the user
					//final String email = profile.getString("email");

					runOnUiThread(new Runnable() {

						//@Override
						public void run() {
							//Toast.makeText(getApplicationContext(), "Name: " + name + "\nEmail: " + email, Toast.LENGTH_LONG).show();
							postToWall();
						}

					});


				} catch (JSONException e) {
					e.printStackTrace();
				}


			}

			//@Override
			public void onIOException(IOException e, Object state) {
			}

			//@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
			}

			//@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
			}

			//@Override
			public void onFacebookError(FacebookError e, Object state) {
			}
		});

	}*/

	/*public void sessionValidate(){
		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) {
			facebook.setAccessToken(access_token);

			Log.d("FB Sessions", "" + facebook.isSessionValid());
		}
		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}
	}

	public void loginToFacebook() {

		sessionValidate();

		if (!facebook.isSessionValid()) {
			facebook.authorize(this,
					new String[] { "email", "publish_stream" }, Facebook.FORCE_DIALOG_AUTH,
					new DialogListener() {

				//@Override
				public void onCancel() {
					// Function to handle cancel event
				}

				//@Override
				public void onComplete(Bundle values) {
					// Function to handle complete event
					// Edit Preferences and update facebook acess_token
					SharedPreferences.Editor editor = mPrefs.edit();
					editor.putString("access_token",
							facebook.getAccessToken());
					editor.putLong("access_expires",
							facebook.getAccessExpires());
					editor.commit();

					getProfileInformation();
					// Making Login button invisible
					//postToWall();
				}

				//@Override
				public void onError(DialogError error) {
					// Function to handle error
					//////System.out.println("Error:"+error);
				}

				//@Override
				public void onFacebookError(FacebookError fberror) {
					// Function to handle Facebook errors
					//////System.out.println("fbError:"+fberror);
				}

			});
		}
	}*/

	////End of facebook

	///getSelectedText
	/*public void copySelection(){
		 mClipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
		 mClipboard.setText(null);

		Class<? extends WebView> webViewClass = webView.getClass();
		java.lang.Class<WebView>[] parameterType = null;
		java.lang.reflect.Method method = null;
		try {
			method = webViewClass.getDeclaredMethod("copySelection", null);
		} catch (NoSuchMethodException e) {

			e.printStackTrace();
		}
		java.lang.Object[] argument = null;
		try {
			method.invoke(webView, argument);
		} catch (IllegalArgumentException e) {

			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {

			e.printStackTrace();
		}
		////System.out.println("CopiedSelection");
		if(mClipboard!=null){
			selectedText = mClipboard.getText().toString();
		}
		////System.out.println("clipboardText:"+selectedText);
	}
	 public void copySelection(){
	 try{
			for(Method m : WebView.class.getDeclaredMethods()){
				//////System.out.println("Method:"+m);
				if(m.getName().equals("getSelection")){
					m.setAccessible(true);
					selectedText = (String) m.invoke(webView, null);
					//////System.out.println("selectedText:"+selectedText);
					break;
				}
			}
		}
		catch (Exception e) {

		}
	 }*/

	public void share(String webSelectedText){
		selectedText = webSelectedText;
		sharePopUp = new Dialog(BookView.this);
		sharePopUp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
		sharePopUp.setTitle("Share via");

		sharePopUp.setContentView(getLayoutInflater().inflate(R.layout.share, null));
		advListView = (ListView) sharePopUp.findViewById(R.id.listViewShare);
		advListView.setDividerHeight(2);
		advListView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
		advListView.setAdapter(new shareAdapter(this));
		advListView.setOnItemClickListener(new OnItemClickListener() 
		{
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
			{
				switch (position) 
				{
				case 0:
					if(webView.allowPublish(200))
					{
						//mHandler.post(mUpdateResults);
					}
					break;

				case 1:
					if(webView.allowPublish(117))
					{
						new twitterLogin().execute();
					}

					break;

				case 2:
					if(webView.allowPublish(200))
					{
						Intent i = new Intent(android.content.Intent.ACTION_SEND);
						i.setType("text/plain");
						i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"recipient@example.com"});
						i.putExtra(android.content.Intent.EXTRA_CC, new String[]{""});
						i.putExtra(android.content.Intent.EXTRA_BCC, new String[]{""});
						i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Note From Manahij");
						i.putExtra(android.content.Intent.EXTRA_TEXT, selectedText);

						try
						{
							startActivity(Intent.createChooser(i, "Send mail...."));
						} 
						catch(android.content.ActivityNotFoundException ex)
						{
							Toast.makeText(BookView.this, "There are no email clients installed", Toast.LENGTH_SHORT).show();
						}
					}
					break;

				default:
					break;
				}
			}


			class twitterLogin extends AsyncTask<Void, Void, Void>{

				@Override
				protected Void doInBackground(Void... params) {


					ConfigurationBuilder confbuilder = new ConfigurationBuilder();
					Configuration conf = confbuilder
							.setOAuthConsumerKey(Globals.CONSUMER_KEY)
							.setOAuthConsumerSecret(Globals.CONSUMER_SECRET)
							.build();
					mTwitter = new TwitterFactory(conf).getInstance();
					mTwitter.setOAuthAccessToken(null);
					try {
						mRequestToken = mTwitter.getOAuthRequestToken(Globals.CALLBACK_URL);
					} catch (TwitterException e) {
						e.printStackTrace();
					}

					return null;
				}
				
				@Override
				protected void onPostExecute(Void result) {
					super.onPostExecute(result);
					if (mRequestToken != null) {
						Intent intent = new Intent(BookView.this, TwitterLogin.class);
						intent.putExtra(Globals.IEXTRA_AUTH_URL, mRequestToken.getAuthorizationURL());
						startActivityForResult(intent, 1);
					}
					sharePopUp.dismiss();
				}

			}
		});
		sharePopUp.show();
	}
	
	/*final Runnable mUpdateResults = new Runnable(){

		public void run() {

			sessionValidate();
			if(facebook.isSessionValid()){

				getProfileInformation();
			}
			else{
				loginToFacebook();
			}
		}
	};*/

	public static void textToSpeech(String webSelectedText) {

		selectedText = webSelectedText;
		new speakToME().execute();
	}

	public static class speakToME extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			String language = detectLanguage(selectedText);
			//////System.out.println("language:"+language);

			String percentEscpe = Uri.encode(selectedText);
			try {
				if(language.equals("ar")){

					ByteArrayOutputStream bais = readBytes(new URL("http://translate.google.com/translate_tts?tl=ar&ie=utf-8&q="+percentEscpe+""));
					webView.playMp3(bais.toByteArray());
				}
				else{
					ByteArrayOutputStream bais = readBytes(new URL("http://translate.google.com/translate_tts?tl=en&q="+percentEscpe+""));
					webView.playMp3(bais.toByteArray());
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}

			return null;
		}

	}

	public static ByteArrayOutputStream readBytes(URL url) throws IOException{
		ByteArrayOutputStream bais = new ByteArrayOutputStream();
		InputStream is = null;
		try {
			is = url.openStream ();
			byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
			int n;
			while ( (n = is.read(byteChunk)) > 0 ) {
				bais.write(byteChunk, 0, n);
			}
		}
		catch (IOException e) {
			System.err.printf ("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
			e.printStackTrace ();
			// Perform any other exception handling that's appropriate.
		}
		finally {
			if (is != null) { is.close(); }
		}
		return bais;
	}

	public void advancedSearch(String text) {
		
		selectedText = text;
		
		if (Globals.isLimitedVersion()) {
			searchEditText.setText(selectedText);
			//etAdv.setText(selectedText);
			//advSearchPopUp.show();

			//etSearch.setVisibility(View.VISIBLE);
			//imgBtnBack.setVisibility(View.VISIBLE);
			//searchPopup.findViewById(R.id.footer).setVisibility(View.VISIBLE);
			
			resultFound.removeAll(resultFound);

			if(searchIndexArray.isEmpty()){
				processXML();
			}
			serachTableReload(searchEditText.getText().toString());

			searchPopup.show();

			if(resultFound.isEmpty()==true)
			{
				tv_noresult.setVisibility(View.VISIBLE);
			}

			etSearch.setVisibility(View.GONE);
			imgBtnBack.setVisibility(View.GONE);
			searchPopup.findViewById(R.id.footer).setVisibility(View.GONE);
		} else {
			etAdv.setText(selectedText);
			advSearchPopUp.show();

			etSearch.setVisibility(View.VISIBLE);
			imgBtnBack.setVisibility(View.VISIBLE);
			searchPopup.findViewById(R.id.footer).setVisibility(View.VISIBLE);
		}
		
	}


	private class advAdapter extends BaseAdapter{
		//private Context mContext;

		public Integer[] mThumbIds = {
				R.drawable.book,
				R.drawable.more_curriculum,
				R.drawable.nooor, R.drawable.google1, R.drawable.wikipedia1, R.drawable.youtube1, R.drawable.yahoo_icon, 
				R.drawable.bing_icon, R.drawable.ask
		};

		public advAdapter(BookView bookView) {

		}

		//@Override
		public int getCount() {

			return advSearch.length;
		}

		//@Override
		public Object getItem(int position) {

			return null;
		}

		//@Override
		public long getItemId(int position) {

			return 0;
		}

		//@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View vi = convertView;
			if(convertView==null){
				vi = getLayoutInflater().inflate(R.layout.advpopgrid, null);
			}
			TextView txtSearchItem = (TextView) vi.findViewById(R.id.textViewAdv);
			ImageView imgSearchImg = (ImageView) vi.findViewById(R.id.imageViewAdv);
			txtSearchItem.setText(advSearch[position]);
			imgSearchImg.setImageResource(mThumbIds[position]);
			return vi;
		}

	}

	private class shareAdapter extends BaseAdapter{
		
		private boolean isFbSessionActive;
		private GraphUser user;
		Session session;
		

		public Integer[] mThumbIds = {
				R.drawable.facebook,R.drawable.twitter,R.drawable.emailiphone
		};

		public shareAdapter(BookView bookView) {
			session = Session.getActiveSession();
			if (session != null && session.isOpened()) {
				isFbSessionActive = true;
				Request.newMeRequest(session, new Request.GraphUserCallback() {
					
					@Override
					public void onCompleted(GraphUser _user, Response response) {
						user = _user;
					}
				}).executeAsync();
			} else {
				isFbSessionActive = false;
			}
		}

		@Override
		public int getCount() {
			return share.length;
		}

		@Override
		public Object getItem(int position) {

			return null;
		}

		@Override
		public long getItemId(int position) {

			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			View vi = convertView;
			if(convertView==null){
				vi = getLayoutInflater().inflate(R.layout.advpopgrid, null);
			}
			TextView txtSearchItem = (TextView) vi.findViewById(R.id.textViewAdv);
			ImageView imgSearchImg = (ImageView) vi.findViewById(R.id.imageViewAdv);
			LinearLayout fbLayout = (LinearLayout) vi.findViewById(R.id.fb_layout);
			LoginButton authButton = (LoginButton) vi.findViewById(R.id.authButton);
			authButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					sharePopUp.dismiss();
				}
			});
			Button btnShare = (Button) vi.findViewById(R.id.button1);
			btnShare.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (user != null) {
						publishFeedDialog(user.getName());
					}
				}
			});
			
			if (position == 0) {
				if (isFbSessionActive) {
					btnShare.setVisibility(View.VISIBLE);
				} else {
					btnShare.setVisibility(View.GONE);
				}
				fbLayout.setVisibility(View.VISIBLE);
				txtSearchItem.setVisibility(View.GONE);
				imgSearchImg.setVisibility(View.GONE);
			} else {
				fbLayout.setVisibility(View.GONE);
				txtSearchItem.setVisibility(View.VISIBLE);
				imgSearchImg.setVisibility(View.VISIBLE);
			}
			
			txtSearchItem.setText(share[position]);
			imgSearchImg.setImageResource(mThumbIds[position]);
			return vi;
		}

	}

	public static String detectLanguage(String selectedText) {

		String alpha = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
		String[] alphaArray = alpha.split(",");
		for (int i = 0; i < alphaArray.length; i++) {
			String current = alphaArray[i];
			if(selectedText.contains(current)){
				return "en";
			}
		}
		return "ar";
	}

	//Highlight

	public static void addHighlights(String iVal) {

		String[] searchResults = iVal.split("\\|");
		String sTextTemp = searchResults[3].trim();
		String query = "insert into tblHighlight(BName,PageNo,SText,Occurence,Color,TabNo) values('"+bookName+"', '"+pageNumber+"', '"+sTextTemp+"', '"+searchResults[1]+"', '', '"+tabNo+"')";
		db.executeQuery(query);
		if(tabNo>0){
			enriched = true;
		}
	}

	public static void removeHighlights(String iVal){
		String[] searchResults = iVal.split("\\|");
		String sTextTemp = searchResults[3].trim();
		String query = "delete from tblHighlight where BName='"+bookName+"' and PageNo='"+pageNumber+"' and TabNo='"+tabNo+"' and SText='"+sTextTemp+"' and Occurence='"+searchResults[1]+"'";
		db.executeQuery(query);
		if(tabNo>0){
			enriched = true;
		}
	}

	public static int getHiighCount(String IVal){
		 /*if(type.equals("duplicate")||type.equals("blank")){
	        	type="created";
	     }*/
		int highlightCount = db.getHighlightCount(bookName, pageNumber, IVal);
		return highlightCount;
	}

	public static String getSelectedPosition(String selText2){

		result = selText2;
		result = result.replaceAll("</span>", "");
		result = result.replaceAll("<span style=\"background-Color:#FFCCCC\">", "");
		result = result.replaceAll("<span style=\"background-Color:#F6E993\">", "");
		result = result.replaceAll("<span style=\"background-Color:#C1E391\">", "");
		result = result.replaceAll("<span style=\"background-Color:#B1D8FF\">", "");
		result = result.replaceAll("<span style=\"background-Color:#FCACDA\">", "");
		result = result.replaceAll("<span style=\"background-Color:#E0B4F0\">", "");
		result = result.replaceAll("<span style=\"background-Color:#F2F2F2\">", "");

		String[] splres = result.split("\\|");

		String result1 = splres[0];

		String encodedString = result.replaceAll("\"", "\"\"");
		encodedString = encodedString.replaceAll("\'", "\'\'");
		encodedString = encodedString.replaceAll("\r", " ");
		encodedString = encodedString.replaceAll("\n", " ");
		encodedString = encodedString.replaceAll("\t", " ");

		String retVal = encodedString;

		String str = retVal+"|"+result1;
		return str;
	}
	//Highlight

	//Note

	public void addNote(String iVal, String vc){
		int popWidth = 0;
		int popHeight = 0;
		//int coordsY = Math.round(mNoteBounds.bottom/mScale);
		//int coordsX = Math.round(mNoteBounds.left/mScale);
		//int x =  coordsX-((deviceHeight/4)*2);
		//int y =  coordsY-((deviceHeight/4)*2);
		//Rect handleRect = new Rect();
		//handleRect = mNoteBounds;
		selText = iVal;
		/*if(webView.addNoteFromPopMenu){
			if((deviceHeight-(deviceHeight/3))<coordsY){
				notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, coordsX, coordsY-((deviceHeight/4)*2));
			}
			else{
				notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, coordsX, coordsY);
			}
		notepop.update(deviceWidth/2, deviceHeight/4);
		}
		else{
			popWidth = 65*deviceWidth/100;
			popHeight = 50*deviceHeight/100;
			notePopUp.showAtLocation(noteRelativeLayout, Gravity.CENTER, 0, 0);
			notePopUp.update(popWidth, popHeight);*/
		//}

		if(isTablet)
		{
			if(webView.addNoteFromPopMenu)
			{
				int coordsX = Math.round(mNoteBounds.left);//mScale);
				int coordsY = Math.round(mNoteBounds.bottom);//mScale);

				displaySmallNotesPopup(coordsX-30,coordsY+50);
			}
			else
			{
				popWidth = 65*deviceWidth/100;
				//popHeight = 50*deviceHeight/100;
				popHeight = deviceHeight/3;
				notePopUp.showAtLocation(noteRelativeLayout, Gravity.CENTER, 0, 0);
				notePopUp.update(popWidth, popHeight);
			}
		}
		else
		{
			/*popWidth = 65*deviceWidth/100;
			popHeight = 50*deviceHeight/100;
			notePopUp.showAtLocation(noteRelativeLayout, Gravity.CENTER, 0, 0);
			notePopUp.update(popWidth, popHeight);*/
			if(webView.addNoteFromPopMenu)
			{
				int coordsX = Math.round(mNoteBounds.left);//mScale);
				int coordsY = Math.round(mNoteBounds.bottom);//mScale);

				displaySmallNotesPopup(coordsX-30,coordsY+50);
			}
			else
			{
				popWidth = 65*deviceWidth/100;
				//popHeight = 50*deviceHeight/100;
				popHeight = deviceHeight/3;
				notePopUp.showAtLocation(noteRelativeLayout, Gravity.CENTER, 0, 0);
				notePopUp.update(popWidth, popHeight);
			}

		}



		//String[] ts = iVal.split("\\|");
		//String vc = "A|"+ts[0]+"|"+ts[1]+"|"+ts[2]+"|"+ts[4]+"";
		loadNote(vc);
		if(tabNo>0){
			enriched=true;
		}
	}

	/*private static void displaySmallNotesPopup(int sX, int sY)
	{
		//s-start e-end m-middle
		int notepopW,notepopH,mX,mY,paddingW,paddingH,paddingH2,keyboardH;
		//calculate popup Height and Width based on device rsln
		notepopW =(int) (deviceWidth/2.5);
		notepopH =(int) (deviceHeight/4.5);
		keyboardH = (int) (deviceHeight/1.8);//space left after shwing keyboard
		paddingW=20;
		paddingH=2;
		paddingH2=5;
		mX=sX;
		//mX=(int) ((sX+eX)/2);//calculate middle point of selected txt
		mY=sY+paddingH;//used as padding to prevent highlight overlay(bottom)

			if(mY+notepopH<=deviceHeight)//displaying popup below selected txt
			{
				if(mX+notepopW<=deviceWidth)
				{
					if(mX>=(deviceWidth/2)-75 && mX<=(deviceWidth/2)+75)//btm center
					{
						displayArrow(2);
						notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-(notepopW/2), mY);
					}
					else//btm left
					{
						displayArrow(1);
						notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-paddingW, mY);
					}
				}
				else if(mX+notepopW>=deviceWidth)
				{
					if(mX>=(deviceWidth/2)-75 && mX<=(deviceWidth/2)+75)//btm center
					{
						displayArrow(2);
						notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-(notepopW/2), mY);
					}
					else//btm right
					{
						displayArrow(3);
						notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, (mX-notepopW+paddingW), mY);
					}
				}
			}
			else if(mY+notepopH>deviceHeight)//displaying popup above selected txt
			{
				mY=sY-paddingH2;//used as padding to prevent highlight overlay(top)
				if(mX+notepopW<=deviceWidth)
				{
					if(mX>=(deviceWidth/2)-75 && mX<=(deviceWidth/2)+75)//top center
					{
						displayArrow(4);
						notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-(notepopW/2), mY-notepopH);
					}
					else//top left
					{
						displayArrow(5);
						notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-paddingW, mY-notepopH);
					}
				}
				else if(mX+notepopW>=deviceWidth)
				{
					if(mX>=(deviceWidth/2)-75 && mX<=(deviceWidth/2)+75)//top center
					{
						displayArrow(4);
						notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-(notepopW/2), mY-notepopH);
					}
					else//top right
					{
						displayArrow(6);
						notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, (mX-notepopW+paddingW), mY-notepopH);
					}
				}
			}
		notepop.update(notepopW,notepopH);
	}*/

	public void loadNote(String vc){
		SN = vc;

		//if(!(lngCategoryID == 1)){
			btnDone.setText(getResources().getString(R.string.done));
			btnCancel.setText(getResources().getString(R.string.cancel));
			txtNote.setText(getResources().getString(R.string.select_item_view_note));

		//}
		txtNote.setVisibility(View.GONE);
		btnDone.setVisibility(View.VISIBLE);
		btnCancel.setVisibility(View.GONE);
		etNote.setVisibility(View.VISIBLE);
		noteList.setVisibility(View.GONE);
		String[] searchResults = vc.split("\\|");
		if(searchResults[0].equalsIgnoreCase("E")){
			String MultRecs = db.getMultipleRecord(searchResults[1]);
			pages = db.selectPageRecords(MultRecs);

			pagesValue = db.selectPageValuesRecord(MultRecs);

			if(pages.size() == 1){
				noteColorSelectionContainer.setVisibility(View.VISIBLE);
				txtNote.setVisibility(View.GONE);
				btnDone.setVisibility(View.VISIBLE);
				btnCancel.setVisibility(View.GONE);
				etNote.setVisibility(View.VISIBLE);
				noteList.setVisibility(View.GONE);
				String[] searchResultsText = db.getRecord(searchResults[1]).split("\\|");
				etNote.setText(searchResultsText[0]);
				CC = Integer.parseInt(searchResultsText[1])-1;
				etNote.setSelection(etNote.getText().length());
                InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
 				inputmanager.showSoftInput(etNote, inputmanager.SHOW_IMPLICIT);
	
				noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
			}
			else{
				noteColorSelectionContainer.setVisibility(View.GONE);
				txtNote.setVisibility(View.VISIBLE);
				btnDone.setVisibility(View.GONE);
				btnCancel.setVisibility(View.VISIBLE);
				etNote.setVisibility(View.GONE);
				noteList.setVisibility(View.VISIBLE);
				noteRelativeLayout.setBackgroundResource(cardImgId[0]);
			}
		}
		else{
			noteColorSelectionContainer.setVisibility(View.VISIBLE);
			CC = 0;
			etNote.setText("");
			et.setText("");
			noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
			rl_notepop.setBackgroundResource(R.drawable.notepop_bg1);
			iv_toparrow_left.setBackgroundResource(R.drawable.arrow_1);
			iv_toparrow_center.setBackgroundResource(R.drawable.arrow_1);
			iv_toparrow_right.setBackgroundResource(R.drawable.arrow_1);
			iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_1);
			iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_1);
			iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_1);
		}
		InputMethodManager imm1=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm1.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	public class noteListAdapter extends BaseAdapter{

		public noteListAdapter(BookView bookView) {

		}

		public int getCount() {

			return pages.size();
		}

		public Object getItem(int position) {

			return null;
		}

		public long getItemId(int position) {

			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {

			View vi = convertView;
			if(convertView == null)
				vi = getLayoutInflater().inflate(R.layout.notelist_txtfield, null);
			TextView tv = (TextView) vi.findViewById(R.id.textView1);
			tv.setText((CharSequence) pages.get(position));
			return vi;
		}

	}

	public void addNotetoDatabase(String[] searchresults){
		String noteText = "";
		if(isTablet){
			noteText = et.getText().toString();
		}else{
			noteText = et.getText().toString();
		}
		/* if(type.equals("duplicate")||type.equals("blank")){
	        	type="created";
	     }*/
		//////System.out.println("note text: "+et.getText());
		//////System.out.println("Etnote text:"+etNote.getText());
		String StextTemp = searchresults[1].trim();
		int colorVal = CC+1;
		if(searchresults[0].contentEquals("A")){
			String query = "insert into tblNote(BName,PageNo,SText,Occurence,SDesc,NPos,Color,ProcessSText,TabNo,Exported) values" +
					"('"+bookName+"','"+pageNumber+"','"+StextTemp+"','"+searchresults[2]+"','"+noteText+"','"+searchresults[3]+"','"+colorVal+"','"+searchresults[4]+"','"+tabNo+"','"+"0"+"')";
			db.executeQuery(query);
		}
		else{
			String query = "update tblNote set SDesc='"+etNote.getText()+"',Color='"+colorVal+"' where TID='"+searchresults[1]+"'";
			db.executeQuery(query);
		}
	}

	public void SReloadOne(String BID, int PN, int RS){
		if(RS == 0){
			String[] searchresults = selText.split("\\|");
			int sameLinePosition = db.getSameLinePosition(bookName, pageNumber,Integer.parseInt(searchresults[2]),tabNo);
			if(sameLinePosition>0){
				updateNote(searchresults[0]+"|"+searchresults[1]+"|"+sameLinePosition);
			}
			else{
				updateNote(searchresults[0]+"|"+searchresults[1]+"|"+sameLinePosition);
			}
		}
		loadHighlightAndNote(dir);
	}



	private void updateNote(String iVal) {
		String[] searchresults = iVal.split("\\|");
		String StextTemp = searchresults[0].trim();
		String query = "update tblNote set NPos='"+searchresults[2]+"' where BName='"+bookName+"' and PageNo='"+pageNumber+"' and TabNo='"+tabNo+"' and SText='"+StextTemp+"' and Occurence='"+searchresults[1]+"'";
		db.executeQuery(query);
	}

	public static void btnCShow(){
		imgBtnC1.setVisibility(View.VISIBLE);
		imgBtnC2.setVisibility(View.VISIBLE);
		imgBtnC3.setVisibility(View.VISIBLE);
		imgBtnC4.setVisibility(View.VISIBLE);
		imgBtnC5.setVisibility(View.VISIBLE);
		imgBtnC6.setVisibility(View.VISIBLE);
	}

	public static void btnCHidden(){
		imgBtnC1.setVisibility(View.GONE);
		imgBtnC2.setVisibility(View.GONE);
		imgBtnC3.setVisibility(View.GONE);
		imgBtnC4.setVisibility(View.GONE);
		imgBtnC5.setVisibility(View.GONE);
		imgBtnC6.setVisibility(View.GONE);
	}

	public static void loadNoteDiv(){
		/* if(type.equals("duplicate")||type.equals("blank")){
	        	type="created";
	     }*/
		Object[] str = db.loadNoteDivToDatabase(bookName, pageNumber, tabNo);
		String[] TID = (String[]) str[0];
		String[] SText = (String[]) str[1];
		String[] Ocuurences = (String[]) str[2];
		String[] NPos = (String[]) str[3];
		String[] CID = (String[]) str[4];
		for(int i =0; i<SText.length; i++){
			String noteKeyword = NoteKeywordLoadDiv(SText[i], Ocuurences[i], NPos[i], sourceString, TID[i], CID[i]);
			sourceString = sourceString.replace("</body>", ""+noteKeyword+"</body>");
		}
	}

	public void loadNote(){
		Object[] str = db.loadNoteDivToDatabase(bookName, pageNumber, tabNo);
		String[] TID = (String[]) str[0];
		String[] SText = (String[]) str[1];
		String[] Ocuurences = (String[]) str[2];
		String[] NPos = (String[]) str[3];
		String[] CID = (String[]) str[4];
		for(int i =0; i<SText.length; i++){
			NoteKeywordLoad(SText[i].replaceAll("\'\'", "\'").replaceAll("\"", "dqzd"), Ocuurences[i], NPos[i], sourceString, TID[i], CID[i]);
		}
	}

	private void NoteKeywordLoad(String stext, String Occurences,
			String YPos, String sourceString2, String TID, String CID) {

		String ColorValue = "";
		if(CID.contentEquals("1")){
			ColorValue = "color1";
		}
		else if(CID.contentEquals("2")){
			ColorValue = "color2";
		}
		else if(CID.contentEquals("3")){
			ColorValue = "color3";
		}
		else if(CID.contentEquals("4")){
			ColorValue = "color4";
		}
		else if(CID.contentEquals("5")){
			ColorValue = "color5";
		}
		else if(CID.contentEquals("6")){
			ColorValue = "color6";
		}
		//webView.loadUrl("javascript:Highlight('"+stext+"', '"+Occurences+"', '"+ColorValue+"')");
		((SemaWebView)webView).loadJSScript("javascript:Highlight('"+stext+"', '"+Occurences+"', '"+ColorValue+"')", webView);
	}

	public void loadNoteForViewPager(final WebView view, int pNumber){
		Object[] str = db.loadNoteDivToDatabase(bookName, pNumber, tabNo);
		final String[] TID = (String[]) str[0];
		final String[] SText = (String[]) str[1];
		final String[] Ocuurences = (String[]) str[2];
		final String[] NPos = (String[]) str[3];
		final String[] CID = (String[]) str[4];
		for(int i =0; i<SText.length; i++){
			NoteKeywordLoadForViewPager(SText[i].replaceAll("\'\'", "\'").replaceAll("\"", "dqzd"), Ocuurences[i], NPos[i], sourceString, TID[i], CID[i], view);
		}
	}

	private void NoteKeywordLoadForViewPager(String stext, String Occurences,
			String YPos, String sourceString2, String TID, String CID, WebView view) {

		String ColorValue = "";
		if(CID.contentEquals("1")){
			ColorValue = "color1";
		}
		else if(CID.contentEquals("2")){
			ColorValue = "color2";
		}
		else if(CID.contentEquals("3")){
			ColorValue = "color3";
		}
		else if(CID.contentEquals("4")){
			ColorValue = "color4";
		}
		else if(CID.contentEquals("5")){
			ColorValue = "color5";
		}
		else if(CID.contentEquals("6")){
			ColorValue = "color6";
		}
		//view.loadUrl("javascript:Highlight('"+stext+"', '"+Occurences+"', '"+ColorValue+"')");
		((SemaWebView)view).loadJSScript("javascript:Highlight('"+stext+"', '"+Occurences+"', '"+ColorValue+"')", view);
	}

	private static String NoteKeywordLoadDiv(String stext, String Occurences,
			String YPos, String sourceString2, String TID, String CID) {

		String noteImagePath =Environment.getDataDirectory().getPath().concat("/data/"+Globals.getCurrentProjPackageName()+"/files/").concat("BookMarkImage/stick_samll.png");
		//System.out.println(noteImagePath+"path");
		String lNoteDiv = "<div style=z-index:101;position:absolute;width:46;height:46;right:5;top:"+YPos+";background-image:url("+noteImagePath+");background-repeat:no-repeat; onclick=window.location=\"/E/"+TID+"\";>&nbsp;</div>";
		return lNoteDiv;
	}

	public static int getNoteCount(String IVal){
		int noteCount = db.getNoteCountFromDB(bookName, pageNumber, IVal);
		return noteCount;
	}

	public static void removeNote(String IVal){
		String[] searchResults = IVal.split("\\|");
		String sTextTemp = searchResults[3].trim();

		String query = "delete from tblNote where BName='"+bookName+"' and PageNo='"+pageNumber+"' and TabNo='"+tabNo+"' and SText='"+sTextTemp+"' and Occurence='"+searchResults[1]+"'";
		db.executeQuery(query);
		if(tabNo>0){
			enriched=true;
		}

	}

	public static void loadHighlightAndNote(File directory) {

		/* Enriched */

		if(enriched==true)//changes-e
		{
			//revert_book_hide :
			filePath ="file:///"+currentBookPath+epath; //Store Modifications
			File file = new File(currentBookPath+epath);
			//filePath ="file:///data/data/com.semanoor.manahij/files/"+bookName+"Book/"+epath; //Store Modifications
			//File file = new File(directory+"/"+bookName+"Book/"+epath);
			sourceString = readFileFromSDCard(file);
			enriched=false;
		}

		/* Enriched */
		else if(enriched==false)
		{
			//revert_book_hide :
			filePath ="file:///"+currentBookPath+pageNumber+".htm"; //Store Modifications
			File file = new File(currentBookPath+pageNumber+".htm");
			//filePath ="file:///data/data/com.semanoor.manahij/files/"+bookName+"Book/"+pageNumber+".htm"; //Store Modifications
			//File file = new File(directory+"/"+bookName+"Book/"+pageNumber+".htm");
			sourceString = readFileFromSDCard(file);
		}

		/* Enriched */
		//////System.out.println(viewPager.getCurrentItem());
		if (LBooklanguage.equalsIgnoreCase("English")) {
			sb.setProgress(pageNumber-1);
		}else{
			sb.setProgress(pageCount-pageNumber);
		}
		//displayBookMark(pageNumber,tabNo);
		loadNoteDiv();
		sourceString = sourceString.replaceAll("(?:</title>)", "</title>"+style);
		String jsFilePath = "file:///android_asset/getSelection.js";
		String jsAndroidSelection = "file:///android_asset/android.selection.js";
		String jsjquery = "file:///android_asset/jquery.js";
		String jsRange = "file:///android_asset/rangy-core.js";
		String jsRangeSerializer = "file:///android_asset/rangy-serializer.js";
		String jsJpnText = "file:///android_asset/jpntext.js";

		sourceString = sourceString.replaceFirst("<head>", "<head><script src="+jsFilePath+"></script><script src="+jsAndroidSelection+"></script><script src="+jsjquery+"></script><script src="+jsRange+"></script><script src="+jsRangeSerializer+"></script><script src="+jsJpnText+"></script>");
		sourceString = sourceString.replaceAll("file:///data/data/"+Globals.getCurrentProjPackageName()+"/files/books/"+currentBook.getBookID()+"/", "");
		sourceString = sourceString.replaceAll("../FreeFiles", "FreeFiles");
		webView.loadDataWithBaseURL(filePath, sourceString, null, "utf-8", null);
		
	}

	public static void loadHighlightAndNoteForViewPager(File directory) {


		/* Enriched */

		if(enriched==true)//changes-e
		{
			//revert_book_hide :
			filePath ="file:///"+currentBookPath+epath; //Store Modifications
			File file = new File(currentBookPath+epath);
			//filePath ="file:///data/data/com.semanoor.manahij/files/"+bookName+"Book/"+epath; //Store Modifications
			//File file = new File(directory+"/"+bookName+"Book/"+epath);
			sourceString = readFileFromSDCard(file);
			enriched=false;
		}

		/* Enriched */
		else if(enriched==false)
		{
			//revert_book_hide :
			filePath ="file:///"+currentBookPath+viewPagerPgNo+".htm"; //Store Modifications
			File file = new File(currentBookPath+viewPagerPgNo+".htm");
			//filePath ="file:///data/data/com.semanoor.manahij/files/"+bookName+"Book/"+viewPagerPgNo+".htm"; //Store Modifications
			//File file = new File(directory+"/"+bookName+"Book/"+viewPagerPgNo+".htm");
			sourceString = readFileFromSDCard(file);
		}

		/* Enriched */

	    //changes-b
		loadNoteDivForViewPager();

		sourceString = sourceString.replaceAll("(?:</title>)", "</title>"+style);
		
		sourceString = sourceString.replaceAll("file:///data/data/"+Globals.getCurrentProjPackageName()+"/files/books/"+currentBook.getBookID()+"/", "");

		sourceString = sourceString.replaceAll("../FreeFiles", "FreeFiles");
		
		sourceString = sourceString.replaceFirst("<head>", "<head><script src="+jsFilePath+"></script><script src="+jsAndroidSelection+"></script><script src="+jsjquery+"></script><script src="+jsRange+"></script><script src="+jsRangeSerializer+"></script>");

		viewPagerWebView.loadDataWithBaseURL(filePath, sourceString, null, "utf-8", null);
		
	}

	public static void loadNoteDivForViewPager(){
		progressbar.setVisibility(View.INVISIBLE);
		Object[] str = db.loadNoteDivToDatabase(bookName, viewPagerPgNo, tabNo);
		String[] TID = (String[]) str[0];
		String[] SText = (String[]) str[1];
		String[] Ocuurences = (String[]) str[2];
		String[] NPos = (String[]) str[3];
		String[] CID = (String[]) str[4];
		for(int i =0; i<SText.length; i++){
			String noteKeyword = NoteKeywordLoadDiv(SText[i], Ocuurences[i], NPos[i], sourceString, TID[i], CID[i]);
			sourceString = sourceString.replace("</body>", ""+noteKeyword+"</body>");
		}
	}

	//changes-b
	private static void displayBMark(int pNo)
	{
		int count = db.getBookMarkCount(bookName, pNo, tabNo);
		int TagID = db.getBookMarkTagID(bookName, pNo, tabNo);
		/*if(count>0)
		{
			if(TagID==1)
			{
				iv_bookmark.setBackgroundResource(R.drawable.bm1);
			}
			else if(TagID==2)
			{
				iv_bookmark.setBackgroundResource(R.drawable.bm2);
			}
			else if(TagID==3)
			{
				iv_bookmark.setBackgroundResource(R.drawable.bm3);
			}
			else if(TagID==4)
			{
				iv_bookmark.setBackgroundResource(R.drawable.bm4);
			}
			iv_bookmark.setVisibility(View.VISIBLE);
		}
		else
		{
			iv_bookmark.setVisibility(View.GONE);
		}*/
	}

	public static void displayBookMark(int pNo,int TabNo) {

		int count = db.getBookMarkCount(bookName, pNo, TabNo);
		if(count>0){
			btnBookMark.setSelected(true);
			//btnBookMark.setEnabled(false);
		 }
		else{
			btnBookMark.setSelected(false);
			//btnBookMark.setEnabled(true);
		}
	}

	private void addBookMark(int page, int TabNo) {
		String query = "insert into tblBookMark(BName,PageNo,TagID,TabNo) values('"+bookName+"','"+pageNumber+"','0','"+TabNo+"')";
		db.executeQuery(query);
	}

	private void tt2() {

		InputStream is;

		try {
			is = getAssets().open("getSelection.js");
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();

			javascriptSrc = new String(buffer);
		} catch (Exception e) {

			e.printStackTrace();
		}
		//////System.out.println("javascriptSrc:"+javascriptSrc);
		//
	}

	private static String GetStyle(){
		String colorValue = "<style type=\"text/css\">.highlight {background-color:#FFCCCC;color:inherit;text-decoration:inherit}";
		colorValue = colorValue+".Color1 {background-color:#F1E9B8;color:inherit;text-decoration:inherit}";
		colorValue = colorValue+".Color2 {background-color:#D6F2AF;color:inherit;text-decoration:inherit}";
		colorValue = colorValue+".Color3 {background-color:#B1D4F5;color:inherit;text-decoration:inherit}"; //#66CCFF
		colorValue = colorValue+".Color4 {background-color:#FEC2E5;color:inherit;text-decoration:inherit}";
		colorValue = colorValue+".Color5 {background-color:#E2C4EE;color:inherit;text-decoration:inherit}";
		colorValue = colorValue+".Color6 {background-color:#F2F2F2;color:inherit;text-decoration:inherit}";
		colorValue = colorValue+".searchhighlight {background-Color:#CC9966;color:inherit;text-decoration:inherit}";
		colorValue = colorValue+"</style>";
		return colorValue;

	}

	private static String readFileFromSDCard(File file)
	{	
		if(!file.exists())
		{
			//changes for empty mainpage in book
			filePath = "file:///android_asset/nomainpage.html";
			sourceString = "<html> <head> <title>Main Page Not Found</title> </head> <body><!-- inullpageexists --> </body> </html>";
			return sourceString;
			//throw new RuntimeException("File not found");
		}

		BufferedReader reader = null;
		StringBuilder builder = null;
		try 
		{
			reader = new BufferedReader(new FileReader(file));
			builder = new StringBuilder();
			String line;
			while((line = reader.readLine())!=null)
			{
				builder.append(line+"\n");//bug fixed
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(reader != null)
			{
				try 
				{
					reader.close();
				} 
				catch (Exception e2) 
				{

					e2.printStackTrace();
				}
			}
		}
		return builder.toString();
	}

	public String getHtml(String url) throws ClientProtocolException, IOException{
		HttpClient vClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpGet vGet = new HttpGet(url);

		HttpResponse response = vClient.execute(vGet, localContext);

		String result = "";

		BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		//BufferedReader reader = new BufferedReader(new InputStreamReader();
		String line  = null;
		while((line = reader.readLine())!=null)
		{
			result += line + "\n";
		}
		return result;
	}

	public void processXML(){

		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler(){
				public void startElement(String uri, String localName,String qName, 
						Attributes attributes) throws SAXException {
					if(qName != "Search")
					{
						String attrValue = attributes.getValue("Data");
						searchIndexArray.add(attrValue);
					}
				}

			};

			File sdPath = getFilesDir();
			//revert_book_hide :
			filePath = currentBookPath+"Search.xml";
			//filePath = sdPath+"/"+bookName+"Book/Search.xml";
			InputStream inputStream = new FileInputStream(filePath);
			Reader reader = new InputStreamReader(inputStream, "UTF-8");

			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");

			saxParser.parse(is, handler);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == MY_REQUEST_ID)
		{
			if(resultCode == RESULT_OK){

				String[] pgval = data.getExtras().getStringArray("id");
				if(pgval == null){
					int myValue = data.getExtras().getInt("id");     //cover=1
					tabNo=0;
					//passingPageNumberValues(myValue);
					passingPageNumberValues(myValue);
					//setPageNumberValues(myValue,tabNo,null,null);
				}else{
					int value= data.getExtras().getInt("id");
					int myValue = Integer.parseInt(pgval[0])+1;  //8  3-5
					//passingPageNumberValues(myValue);
					//pageNumber = Integer.parseInt(pgval[0])+1;
					loadingfromIndexpage = true;
					//loadingfromcurlpage = true;
					String[] currentPageContents;
					String no = null;
					String path = null;
					String enrtype=null;
					
					int v = Integer.parseInt(pgval[1]);
				 if(bkmarkselected){
					enriched = true;
					currentPageContents = enrichPageArray.get(myValue-1).split("\\|"); 
					 if(currentPageContents.length > 1){
						////System.out.println("CurrentPageContents:"+currentPageContents[1]);
						String[] enrichments = currentPageContents[1].split("##");
					       for(int i=0;i<enrichments.length;i++){
					    	   String str = enrichments[i];
								if(!str.isEmpty()){
						            String[] str1 = str.split("\\$\\$");
									if(str1[2].equals(""+v)) {
										path=str1[0];
										no=str1[2];
										enrtype=str1[3];
										BookView.bkmarkselected=false;
										setPageNumberValues(myValue,Integer.parseInt(no),path,enrtype);
									 }
								  }
					           }
					       }
				  }else{
					  if(v>0){
						  enriched = true;
						  //Get the epath :
						  ////System.out.println("Current page number:"+myValue);
						  currentPageContents = enrichPageArray.get(myValue-1).split("\\|"); 
						if(currentPageContents.length > 1){
							////System.out.println("CurrentPageContents:"+currentPageContents[1]);
							String[] enrichments = currentPageContents[1].split("##");
							path = enrichments[v].split("\\$\\$")[0];   //tabNo=enrichments[tabNo].split("\\+")[2];
						    no=enrichments[v].split("\\$\\$")[2];
							enrtype=enrichments[v].split("\\$\\$")[3];
						 }
						  //if(viewPager.getCurrentItem()==)
						  setPageNumberValues(myValue,Integer.parseInt(no),path,enrtype);
					
					}else{
						tabNo=v;
						setPageNumberValues(myValue,v,null,null);
					    //passingPageNumberValues(myValue);
						
					 }
					}
				}
			}
		} else if (requestCode == 1) {
			if (resultCode == RESULT_OK) {

				new getoauthVerifierToken(data).execute();

				//Toast.makeText(this, "authorized", Toast.LENGTH_SHORT).show();

			} else if (resultCode == RESULT_CANCELED) {
				//Log.w(TAG, "Twitter auth canceled.");
				//////System.out.println("Twitter auth canceled.");
			}
		}
		else if (requestCode == 2){
			if (resultCode == Activity.RESULT_OK){
				dbCheckandUpdate();
				//paidOrUnPaid = db.checkPaidVersion();
				AlertDialog.Builder builder = new AlertDialog.Builder(BookView.this);
				builder.setMessage("You have successfully completed your transaction.");
				builder.setCancelable(false);
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					//@Override
					public void onClick(DialogInterface dialog, int which) {

						dialog.cancel();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			}else if(resultCode == Activity.RESULT_CANCELED){
				AlertDialog.Builder builder = new AlertDialog.Builder(BookView.this);
				builder.setMessage("The transaction has been cancelled.");
				builder.setCancelable(false);
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					//@Override
					public void onClick(DialogInterface dialog, int which) {

						dialog.cancel();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			}
		}else if (requestCode == bookViewActivityrequestCode) {
			reloadEnrichments(pageNumber - 1);
		}else if(requestCode == 64206){
			Session session = Session.getActiveSession();
			session.onActivityResult(this, requestCode, resultCode, data);
			if (session != null && session.isOpened()) {
				Request.newMeRequest(session, new Request.GraphUserCallback() {
					
					@Override
					public void onCompleted(GraphUser _user, Response response) {
						publishFeedDialog(_user.getName());
					}
				}).executeAsync();
			}
			/*if (session != null &&
					(session.isOpened() || session.isClosed()) ) {
				//onSessionStateChange(session, session.getState(), null);
			}*/
		}
	}

	public void passingPageNumberValues(int myValue){
		int pageno = myValue-1;
		//System.out.println( viewPager.getCurrentItem());
		 /* if(viewPager.getCurrentItem()==pageNumber){
			  setEnrichTabSelected();
			  loadHighlightAndNote(dir);
		  }else{*/
		   
		  if (LBooklanguage.equalsIgnoreCase("English")) {
		      viewPager.setCurrentItem(pageno);
		  }else{
			  viewPager.setCurrentItem(pageCount-(pageno+1));
		  }
	}
	
	public void setPageNumberValues(int myValue, int tabno, String path, String enrtype){
		  viewPager.setAdapter(new ViewAdapter());
	      //System.out.println( viewPager.getCurrentItem());
	      int pageno;
		 if (LBooklanguage.equalsIgnoreCase("English")) {
			  pageno = myValue - 1;
			  tabNo=tabno;
			  if(path!=null&& enrtype!=null){
			     epath=path;
			     type=enrtype;
			     enriched=true;
			  }
			  viewPager.setCurrentItem(pageno);
		 }else{
			  pageno = myValue - 1;
			  tabNo=tabno;
			  if(path!=null&& enrtype!=null){
			     epath=path;
			     type=enrtype;
			     enriched=true;
			  }
			  viewPager.setCurrentItem(pageCount-(pageno+1));
		 }
	}
	
	
	public void setTabValue(){

		if(currentEnrTabsLayout.getChildCount()>1){
			for (int i = 0; i < currentEnrTabsLayout.getChildCount(); i++) {
				Button btnEnrTab;
				if (i == 0) {
					btnEnrTab = (Button) currentEnrTabsLayout.getChildAt(i);
				} else {
				RelativeLayout rlview = (RelativeLayout) currentEnrTabsLayout.getChildAt(i);
				btnEnrTab = (Button) rlview.getChildAt(0);
				}
				//int btn_tag=(Integer) btnEnrTab.getTag();
				if (btnEnrTab.isSelected()) {
					tabNo = (Integer) btnEnrTab.getTag();
				} 
		   }
		}else{
			 tabNo=0;
		}
	}

	private void setEnrichTabSelected(){
		int  enrTabsWidth1 = 0;
		RelativeLayout rlview = null;
		//get current page tab no : Its reassigns at viewpager instantiate
		for (int i = 0; i < currentEnrTabsLayout.getChildCount(); i++) {
			Button btnEnrTab;
			if (i == 0) {
				btnEnrTab = (Button) currentEnrTabsLayout.getChildAt(i);
			} else {
			    rlview = (RelativeLayout) currentEnrTabsLayout.getChildAt(i);
			    enrTabsWidth1+=rlview.getWidth();
				btnEnrTab = (Button) rlview.getChildAt(0);
				}
				int btn_tag=(Integer) btnEnrTab.getTag();
				if (btn_tag == tabNo) {
					btnEnrTab.setSelected(true);
					sv.smoothScrollTo(enrTabsWidth1, 0); 
				} else {
					btnEnrTab.setSelected(false);
			 }
		}
	}

	public class getoauthVerifierToken extends AsyncTask<Void, Void, Void>{

		Intent i;
		AccessToken accessToken = null;
		public getoauthVerifierToken(Intent data) {
			i = data;
		}

		@Override
		protected void onPreExecute(){
			//////System.out.println("preExecute");
		}

		@Override
		protected Void doInBackground(Void... params) {


			try {
				String oauthVerifier = i.getExtras().getString(Globals.IEXTRA_OAUTH_VERIFIER);
				accessToken = mTwitter.getOAuthAccessToken(mRequestToken, oauthVerifier);
				pref = getSharedPreferences(Globals.PREF_NAME, MODE_PRIVATE);
				SharedPreferences.Editor editor = pref.edit();
				editor.putString(Globals.PREF_KEY_ACCESS_TOKEN, accessToken.getToken());
				editor.putString(Globals.PREF_KEY_ACCESS_TOKEN_SECRET, accessToken.getTokenSecret());
				editor.commit();
				//////System.out.println("authorised");

			} catch(TwitterException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void unused){
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(BookView.this);
			String uName = accessToken.getScreenName().toString();
			alertDialog.setTitle(uName);
			alertDialog.setMessage(selectedText);
			alertDialog.setPositiveButton("Tweet", new DialogInterface.OnClickListener() {

				//@Override
				public void onClick(DialogInterface dialog, int which) {

					new tweet().execute();
				}
			});

			alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				//@Override
				public void onClick(DialogInterface dialog, int which) {

					dialog.cancel();
				}
			});

			alertDialog.show();
		}
	}

	public class tweet extends AsyncTask<Void, Void, Void>{
		String accessToken;
		String accessTokenSecret;
		@Override
		protected Void doInBackground(Void... params) {

			try {
				if (mTwitter == null) {
					ConfigurationBuilder confbuilder = new ConfigurationBuilder();
					Configuration conf = confbuilder
							.setOAuthConsumerKey(Globals.CONSUMER_KEY)
							.setOAuthConsumerSecret(Globals.CONSUMER_SECRET)
							.build();
					mTwitter = new TwitterFactory(conf).getInstance();
				}

				pref = getSharedPreferences(Globals.PREF_NAME, MODE_PRIVATE);
				accessToken = pref.getString(Globals.PREF_KEY_ACCESS_TOKEN, null);
				accessTokenSecret = pref.getString(Globals.PREF_KEY_ACCESS_TOKEN_SECRET, null);
				if (accessToken == null || accessTokenSecret == null) {
					//Toast.makeText(BookView.this, "not authorize yet", Toast.LENGTH_SHORT).show();
					return null;
				}
				mTwitter.setOAuthAccessToken(new AccessToken(accessToken, accessTokenSecret));

				//EditText statusText = (EditText)findViewById(R.id.status);
				//String status = statusText.getText().toString();
				mTwitter.updateStatus(selectedText +" by Manahij goo.gl/nT2yG");

				//statusText.setText(null);
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void unused){
			if (accessToken == null || accessTokenSecret == null) {
				Toast.makeText(BookView.this, "not authorize yet", Toast.LENGTH_SHORT).show();
			}
			else{
				Toast.makeText(BookView.this, "tweeted", Toast.LENGTH_SHORT).show();
			}
		}
	}

	public void goToPageNo(){
		//filePath = "file:///mnt/sdcard/Manahij/"+bookName+"/"+pageNumber+".htm";
		//filePath = "file:///mnt/sdcard/Manahij/Book/"+bookName+"/"+pageNumber+".htm";
		//revert_book_hide :
		filePath ="file:///"+currentBookPath+pageNumber+".htm"; //Store Modifications
		//filePath ="file:///data/data/com.semanoor.manahij/files/"+bookName+"Book/"+pageNumber+".htm"; //Store Modifications
		webView.clearCache(true);
		webView.loadUrl(filePath);
	}

	//@Override
	public void onClick(View v) {
		webView.endSelectionMode();
		if (v.getId() == R.id.clear_search_box) {
			clearSearchBox.setVisibility(View.GONE);
			searchEditText.setText("");
		}
		else if (v.getId() == R.id.btnLibrary) {
			onBackPressed();
		}
		 else if (v.getId() == R.id.btnBookMark) {
			//int h = btnBookMark.getHeight();
			//bookMarkPopup.showAtLocation(bookMarklinLayout, Gravity.NO_GRAVITY, bookMPoint.x, bookMPoint.y+h);
			//bookMarkPopup.update(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			 if(pageNumber==1){
				   btnBookMark.setSelected(false);
			 }else{
			  if(!btnBookMark.isSelected()){
				  btnBookMark.setSelected(true);
				  addBookMark(pageNumber,tabNo);
			 }else{
				  btnBookMark.setSelected(false);
				  removeBookMark();	
			  }
			 }
			
		}
		else if (v.getId() == R.id.btnIndex) {
			Intent i = new Intent(getApplicationContext(), Index.class);
			i.putExtra("Book", currentBook);
			//i.putExtra("string", bookName);                                                                                                                              
			//i.putExtra("lngID", lngCategoryID);
			if(bookName.contains("M")){
				i.putStringArrayListExtra("Enriched", enrichPageArray);
			}
			startActivityForResult(i, MY_REQUEST_ID);
		}
		
		else if (v.getId() == R.id.btnStudyCards) {
			showStudyCardsDialogg();
		}
		else if (v.getId() == R.id.btnMarker) {
			clearMode = false;
			objectsToolbar.setVisibility(View.INVISIBLE);
			drawingToolbar.setVisibility(View.VISIBLE);
			sb.setVisibility(View.GONE);
        	tv_pageNumber.setVisibility(View.GONE);
			btnPen.setSelected(true);
			
			if(tabNo>=1){
				objContent = drawnFilesDir+"Page"+" "+pageNumber+"-"+tabNo+".png";
			}
			else{
				objContent = drawnFilesDir+"Page"+" "+pageNumber+".png";
			}
			canvasImgView.setVisibility(View.GONE);
			canvasView = new PainterCanvas(BookView.this, objContent, null);
			designPageLayout.addView(canvasView);
			canvasView.setPresetPorterMode(null);
		}
		
		else if (v.getId() == R.id.btn_done) {
			//clearMode = false;
			if(clearMode==true){
				designPageLayout.removeView(canvasView);
				clearMode=false;
				doneFromDrawingMode();
			}else{
				new saveDrawnTask().execute();
			}
		}
		
		else if (v.getId() == R.id.btn_pen) {
			if (!btnPen.isSelected()) {
				btnPen.setSelected(true);
			}
			btnEarser.setSelected(false);
			canvasView.getThread().activate();
			canvasView.setPresetPorterMode(null);
		}
		
		else if (v.getId() == R.id.btn_brush) {
			showBrushSettingsDialog();
		}
		
		else if (v.getId() == R.id.btn_eraser) {
			if (!btnEarser.isSelected()) {
				btnEarser.setSelected(true);
			}
			btnPen.setSelected(false);
			canvasView.setPresetPorterMode(new PorterDuffXfermode(Mode.CLEAR));
		}
		
		else if (v.getId() == R.id.btn_paint_undo) {
			    canvasView.paintUndo();
		}
		
		else if (v.getId() == R.id.btn_paint_redo) {
			    canvasView.paintRedo();
		}
		
		else if (v.getId() == R.id.btn_clear) {
            clearMode = true;
			
		    canvasView.clearView();
		    
		    File enrichFile = new File(objContent);
		    
			if(enrichFile.exists()){
				enrichFile.delete();
			}
	}
		
		else if (v.getId() == R.id.btnEnrichment) {

			//Check for internet :
			if(UserFunctions.isInternetExist(getApplicationContext())){
				downloadType = "private";
				enrichPopup();
				enrichpages.LoadEnrichmentFirst(BookView.this, bookName, 0); //serviceType = 0 for GetAllEnrPages
			}else{
				//No internet connection, display toast :
				Toast toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_check_your_internet_connection), Toast.LENGTH_SHORT);
				toast.show();
			}  
		}
		else if (v.getId() == R.id.btnEnrichEdit) {
			Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
			bookViewIntent.putExtra("Book", currentBook);
			bookViewIntent.putExtra("currentPageNo", pageNumber);
			startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
		}
		else if (v.getId() == R.id.btnEnrResExport) {
			exportpopup();
		}
		else if (v.getId() == R.id.btnResSync) {
			new ResourceSync().execute();
		}
	}
	
	/**
	 * Export Enrichment/resource PopUp
	 */
    public void exportpopup(){
		PopoverView exportEnrPopover = new PopoverView(BookView.this, R.layout.export_viewpager);
		int width =  (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_width);
		int height =  (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_height);
		exportEnrPopover.setContentSizeForViewInPopover(new Point(width, height));
		exportEnrPopover.setDelegate(this);
		exportEnrPopover.showPopoverFromRectInViewGroup(this.rootviewRL, PopoverView.getFrameForView(btnEnrResExport), PopoverView.PopoverArrowDirectionUp, true);

		exportEnrViewPager = (ViewPager)exportEnrPopover.findViewById(R.id.viewPager);
		//exportEnrViewPager.setAdapter(new ExportEnrResViewAdapter(BookView.this));
		exportEnrViewPager.setOffscreenPageLimit(2);
		exportEnrViewPager.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});
	}

	/**
	 * Syncs all the resource in the book
	 * @author Suriya
	 *
	 */
	private class ResourceSync extends AsyncTask<Void, Void, Void>{
		
		String bookDir;
		ProgressDialog resourceProgressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			resourceProgressDialog = ProgressDialog.show(BookView.this, "", getResources().getString(R.string.syncing_resource), true);

			bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/";
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			int BookId = Integer.parseInt(currentBook.get_bStoreID().replace("M", ""));
			String xmlUpdateRecordsPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/xmlForUpdateRecords.xml";
			if (new File(xmlUpdateRecordsPath).exists()) {
				webService.getDownloadedEnrichmentsAndResourcesFromXmlPath(xmlUpdateRecordsPath);
			}
			String xmlUpdate = getXmlUpdate();
			
			webService.getResourcePagesBasedOnBookId(xmlUpdate, BookId, 0, userCredentials.getUserScopeId());
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			if (webService.resourceList.size() > 0) {
				for (int i = 0; i < webService.resourceList.size(); i++) {
					Resources resource = webService.resourceList.get(i);
					//resource.new getResourceXmlFromUrl(db, webService, null,resourceProgressDialog).execute();
				}
			} else {
				if(resourceProgressDialog!=null){
					resourceProgressDialog.dismiss();
				}
			}
			super.onPostExecute(result);
		}
		
	    /**
	     * Method to get the XmlUpdate content :
	     * @return 
	     */
	    private String getXmlUpdate(){
	    	
	    	String XMLfilePath = bookDir+"xmlForUpdateRecords.xml";
	    	File xmlFile = new File(XMLfilePath);
	    	String xmlUpdate;
			if(xmlFile.exists()){
	    		String xmlContent = UserFunctions.readFileFromPath(new File(XMLfilePath));
	    		String startNode = "<Resources>";
	    		String endNode = "</Resources>";
	    		String xmlUpdateContent = getEnrichmentsFromXML(xmlContent, startNode, endNode);
	   
	    		if(xmlUpdateContent !=""){
	    			xmlUpdate ="<Resource>"+xmlUpdateContent+"</Resource>";
	    			//xmlUpdateContent = URLEncoder.encode(xmlUpdateContent, "UTF-8");
	    			xmlUpdate = 	xmlUpdate.replace("<", "&lt;"); 
	    			xmlUpdate = xmlUpdate.replace(">", "&gt;");
	    		}else{
	    			xmlUpdate = "";
	    		}
	    	}else{
	    		xmlUpdate = "";
	    	}
	    	return xmlUpdate;
	    }
	    
	    /**
	     * Method to split the updateXml to get the current page content :
	     * @param xmlContent
	     * @param startNode
	     * @param endNode
	     * @return
	     */
	    private String getEnrichmentsFromXML(String xmlContent, String startNode, String endNode){
	    	
	    	String result = "";
	    	if(xmlContent.contains(startNode)){
	    		int tillStartElement = xmlContent.indexOf(startNode, 0) + startNode.length();
	    		int tillEndElement = xmlContent.indexOf(endNode, 0);
	    		result = (String) xmlContent.subSequence(tillStartElement, tillEndElement);
	    		////System.out.println("Result :"+result);
	    	}
	    	return result;
	    }
	}
	
	public void StudyCardsAdapter(){
	//	noteStudycards = db.loadNoteDivToArray(bookName);
		String[] PageNo = (String[]) noteStudycards[0];
		String[] SText = (String[]) noteStudycards[1];
		String[] TabNo = (String[]) noteStudycards[4];
		String[] Color = (String[]) noteStudycards[3];
		//Calling the Simple Card class :
		if(SText.length==0){
			tv_StudyCardsAlert.setVisibility(View.VISIBLE);
			tv_StudyCardsAlert.setText(getResources().getString(R.string.study_cards_alert));	
		}
		//SimpleCardStackAdapter	adapter = new SimpleCardStackAdapter(getApplicationContext(), this,noteStudycards);
		
		android.content.res.Resources r = this.getResources();
		for(int tag =0; tag<SText.length; tag++){
			int pageNo = Integer.parseInt(PageNo[tag]) - 1;
			int tabNo = Integer.parseInt(TabNo[tag]); 
			int color = Integer.parseInt(Color[tag]); 
			String finalPageNo;
			if(tabNo > 0){
				
				finalPageNo = ""+pageNo + "-" +tabNo;
			}
			else{
				finalPageNo = ""+pageNo;
			}
			if(pageNo <= 0){
				//adapter.add(new CardModel("Cover page", SText[tag], r.getDrawable(R.drawable.turn_bg)));
				
			}else{
				//adapter.add(new CardModel("Page "+finalPageNo, SText[tag], r.getDrawable(R.drawable.turn_bg)));
			}
			
		}

//		if(adapter.getCount()==0){
//			adapter.notifyDataSetInvalidated();
//		}
		//mCardContainer.setAdapter(adapter);
	}
	
	/***
	 * Method to display study cards :
	 */
	public void showStudyCardsDialogg(){
		
		//Declaring and defining the Dialog
				showStudyCardsDialog = new Dialog(this);
				showStudyCardsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
				showStudyCardsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				showStudyCardsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.card_layout, null));
				showStudyCardsDialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.2), (int)(Globals.getDeviceHeight()/2.0));
				mCardContainer = (CardContainer)showStudyCardsDialog.findViewById(R.id.layoutview);
				tv_StudyCardsAlert=(TextView)showStudyCardsDialog. findViewById(R.id.txt);
				

	
				StudyCardsAdapter();
		
				showStudyCardsDialog.show();
			}
	
	/***
	 * Method to dismiss study cards and Navigate to selected page :
	 */
	public void studyCardNavigatePage(int navigatePage,int navigateTab){
		//dimiss study cards
		// load Index code :
		showStudyCardsDialog .dismiss();
		
			loadingfromIndexpage = true;
			//loadingfromcurlpage = true;
			tabNo =navigateTab;
			if(tabNo>0){
				//enriched = true;
				//Get the epath :
				String[] currentPageContents;
				////System.out.println("Current page number:"+myValue);
				currentPageContents = enrichPageArray.get(navigatePage).split("\\|"); 
				if(currentPageContents.length > 1){
					////System.out.println("CurrentPageContents:"+currentPageContents[1]);
					String[] enrichments = currentPageContents[1].split("##");
					epath = enrichments[tabNo].split("\\$\\$")[0];
				}
				passingPageNumberValues(navigatePage);

			}else{
				passingPageNumberValues(navigatePage);
			}
			
	}
	/**
	 * Method to call the Enrichment pages of the current book.
	 * @param view 
	 */
	public void enrichPopup(){

		popoverView = new PopoverView(BookView.this, R.layout.enrich_viewpager);
		int width =  (int) getResources().getDimension(R.dimen.enrichpopup_width);
		int height =  (int) getResources().getDimension(R.dimen.enrichpopup_height);
		popoverView.setContentSizeForViewInPopover(new Point(width, height));
		popoverView.setDelegate(this);
		popoverView.showPopoverFromRectInViewGroup(this.rootviewRL, PopoverView.getFrameForView(btnEnrichPage), PopoverView.PopoverArrowDirectionUp, true);

		enrichViewpager = (ViewPager)popoverView.findViewById(R.id.viewPager);
		enrichViewpager.setAdapter(new EnrichViewAdapter());    
		enrichViewpager.setOffscreenPageLimit(0);
		enrichViewpager.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});	
	}

	/**
	 * Method called after finish downloading the enrichments :
	 */
	public void reloadEnrichments(int enrichPageNo){
		//Dismiss view :
		if (popoverView != null) {
			popoverView.dissmissPopover(true);
		}
		//Call to navigate to the enrichement pages :
		//loadingfromcurlpage=false;
		//bookViewArray.clear();
		/*for (int j = 0; j <= pageCount; j++) {
			bookViewArray.add(j, null);
		}*/

		if (LBooklanguage.equalsIgnoreCase("English")) {
			//viewPager.setCurrentItem(enrichPageNo);
			pageNumber = enrichPageNo;
		}else{
			//viewPager.setCurrentItem(pageCount-(pageNumber+1));
			pageNumber = pageCount-(enrichPageNo+1);
		}
		//intializeCustomViewPager();
		viewPager.setAdapter(new ViewAdapter());
		if (LBooklanguage.equalsIgnoreCase("English")) {
			viewPager.setCurrentItem(enrichPageNo); 
		}else{
			viewPager.setCurrentItem(pageCount-(enrichPageNo+1));   //16-11
		}
	}

	// Handler for Hiding / Showing webview
	final static Handler mWebviewVisibility = new Handler();
	final Runnable mHideWebview = new Runnable()
	{
		public void run() 
		{
			//BookView.webView.setVisibility(View.GONE);
		}
	};
	final Runnable mShowWebview = new Runnable()
	{
		public void run() 
		{
			//BookView.webView.setVisibility(View.VISIBLE);
		}
	};

	final Runnable mDoneNote = new Runnable()
	{
		public void run() 
		{
			String[] searchresults = SN.split("\\|");
			addNotetoDatabase(searchresults);
			if(searchresults[0].contentEquals("A")){
				SReloadOne(bookName, pageNumber, 0);
			}
			else{
				SReloadOne("B", 0, 1);
			}
		}
	};


	@Override
	public void onPause(){
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		Session session = Session.getActiveSession();
		if (session != null && session.isOpened()) {
			
		} else {
			
		}
		uiHelper.onResume();
	}

	public void setViewPagerCurrentItem(){

		enrichViewpager.setCurrentItem(1, true);

	}
	
	public void setPreset(View v) {
		switch (v.getId()) {
		case R.id.preset_pencil:
			canvasView.setPreset(new BrushPreset(BrushPreset.PENCIL,canvasView
					.getCurrentPreset().color));
			if (btnEarser.isSelected()) {
				canvasView.setPresetPorterMode(new PorterDuffXfermode(Mode.CLEAR));
			}
			break;
		case R.id.preset_brush:
			canvasView.setPreset(new BrushPreset(BrushPreset.BRUSH, canvasView
					.getCurrentPreset().color));
			if (btnEarser.isSelected()) {
				canvasView.setPresetPorterMode(new PorterDuffXfermode(Mode.CLEAR));
			}
			break;
		case R.id.preset_marker:
			canvasView.setPreset(new BrushPreset(BrushPreset.MARKER, canvasView
					.getCurrentPreset().color));
			if (btnEarser.isSelected()) {
				canvasView.setPresetPorterMode(new PorterDuffXfermode(Mode.CLEAR));
			}
			break;
		case R.id.preset_pen:
			canvasView.setPreset(new BrushPreset(BrushPreset.PEN, canvasView
					.getCurrentPreset().color));
			if (btnEarser.isSelected()) {
				canvasView.setPresetPorterMode(new PorterDuffXfermode(Mode.CLEAR));
			}
			break;
		}

		updateControls();
	}
	
	private void updateControls() {
		mBrushSize.setProgress((int) canvasView.getCurrentPreset().size);
		if (canvasView.getCurrentPreset().blurStyle != null) {
			mBrushBlurStyle.setSelection(canvasView.getCurrentPreset().blurStyle
					.ordinal() + 1);
			mBrushBlurRadius.setProgress(canvasView.getCurrentPreset().blurRadius);
		} else {
			mBrushBlurStyle.setSelection(0);
			mBrushBlurRadius.setProgress(0);
		}
	}
	/**
	 * @author ShowBrushSettingsDialog
	 */
	private void showBrushSettingsDialog(){
		Dialog brushSettingsDialog = new Dialog(this);
		brushSettingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
		brushSettingsDialog.setTitle(R.string.brush_settings);
		brushSettingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.brush_settings_dialog, null));
		//brushSettingsDialog.getWindow().setLayout((int)(g.getDeviceWidth()/1.5), (int)(g.getDeviceHeight()/1.5));
		brushSettingsDialog.getWindow().setLayout((int) getResources().getDimension(R.dimen.drawing_brush_dialog), (int) getResources().getDimension(R.dimen.drawing_brush_dialog_height));
		mBrushSize = (SeekBar) brushSettingsDialog.findViewById(R.id.brush_size);
		mBrushSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				if (seekBar.getProgress() > 0) {
					canvasView.setPresetSize(seekBar.getProgress());
				}
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (progress > 0) {
					if (fromUser) {
						canvasView.setPresetSize(seekBar.getProgress());
					}
				} else {
					mBrushSize.setProgress(1);
				}
			}
		});
		mBrushBlurRadius = (SeekBar) brushSettingsDialog.findViewById(R.id.brush_blur_radius);
		mBrushBlurRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				updateBlurSeek(seekBar.getProgress());

				if (seekBar.getProgress() > 0) {
					setBlur();
				} else {
					canvasView.setPresetBlur(null, 0);
				}
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (fromUser) {
					updateBlurSeek(progress);
					if (progress > 0) {
						setBlur();
					} else {
						canvasView.setPresetBlur(null, 0);
					}
				}
			}
		});
		mBrushBlurStyle = (Spinner) brushSettingsDialog.findViewById(R.id.brush_blur_style);
		mBrushBlurStyle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View v,
					int position, long id) {
				if (id > 0) {
					updateBlurSpinner(id);
					setBlur();
				} else {
					mBrushBlurRadius.setProgress(0);
					canvasView.setPresetBlur(null, 0);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		updateControls();
		brushSettingsDialog.show();
	}
	
	private void updateBlurSeek(int progress) {
		if (progress > 0) {
			if (mBrushBlurStyle.getSelectedItemId() < 1) {
				mBrushBlurStyle.setSelection(1);
			}
		} else {
			mBrushBlurStyle.setSelection(0);
		}
	}
	
	private void setBlur() {
		canvasView.setPresetBlur((int) mBrushBlurStyle.getSelectedItemId(),
				mBrushBlurRadius.getProgress());
	}
	
	private void updateBlurSpinner(long blur_style) {
		if (blur_style > 0 && mBrushBlurRadius.getProgress() < 1) {
			mBrushBlurRadius.setProgress(1);
		}
	}
	
	/**
	 * @author showColorPickerDialogtochangeDrawingcolor
	 */
	public void changeBrushColor(View v) {
		final ArrayList<String> recentPaintColorList = this.getRecentColorsFromSharedPreference(Globals.paintColorKey);
		new PopupColorPicker(this, canvasView.getCurrentPreset().color, v, recentPaintColorList, false, new ColorPickerListener() {
			int pickedColor = 0;
			String hexColor = null;
			@Override
			public void pickedColor(int color) {
				hexColor = String.format("#%06X", (0xFFFFFF & color));
				pickedColor = color;
			}
			
			@Override
			public void dismissPopWindow() {
				canvasView.setPresetColor(pickedColor);
				if (hexColor != null) {
					setandUpdateRecentColorsToSharedPreferences(Globals.paintColorKey, recentPaintColorList, hexColor);
				}
			}

			@Override
			public void pickedTransparentColor(String transparentColor) {
				
			}

			@Override
			public void pickColorFromBg() {
				
			}
		});
	}
	
	
	/**
	 * getRecentColorsFromSharedPreferences
	 * @param key
	 * @return 
	 * 
	 */
	public ArrayList<String> getRecentColorsFromSharedPreference(String key){
		SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
		ArrayList<String> recentColorList = null;
		try {
			recentColorList = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<String>())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (recentColorList.size() == 0) {
			recentColorList.add("#B0171F");
			recentColorList.add("#FF82AB");
			recentColorList.add("#0000FF");
			recentColorList.add("#000000");
			recentColorList.add("#006400");
		}
		return recentColorList;
	}
	
	/**
	 * setandUpdateRecentColorsToSharedPreferences
	 * @param key
	 * @param recentColorList 
	 * @param recentColor
	 */
	public void setandUpdateRecentColorsToSharedPreferences(String key, ArrayList<String> recentColorList, String recentColor){
		ArrayList<String> tempArrayList = new ArrayList<String>();
		for (int i = 0; i < recentColorList.size(); i++) {
			if (i == 0) {
				tempArrayList.add(recentColor);
			} else {
				tempArrayList.add(recentColorList.get(i-1));
			}
		}
		recentColorList.clear();
		recentColorList = tempArrayList;
		
		SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor = sharedPreference.edit();
		try {
			editor.putString(key, ObjectSerializer.serialize(recentColorList));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		editor.commit();
	}
	/**
	 * @author saveDrawnImagetoFilesDir
	 */
	private class saveDrawnTask extends AsyncTask<Void, Void, String>{
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {
			saveBitmap(canvasView.objContentPath);
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			setBackgroundImageForDrawing(canvasImgView);
			designPageLayout.removeView(canvasView);
			doneFromDrawingMode();
		}
	}
	
	private void doneFromDrawingMode(){
		drawingToolbar.setVisibility(View.GONE);
		objectsToolbar.setVisibility(View.VISIBLE);
		sb.setVisibility(View.VISIBLE);
    	tv_pageNumber.setVisibility(View.VISIBLE);
		
		if (btnPen.isSelected() || btnEarser.isSelected()) {
			btnPen.setSelected(false);
			btnEarser.setSelected(false);
		}
	}
	
	
	/**
	 * @author setBackgroundImageForDrawing
	 */
	public void setBackgroundImageForDrawing(ImageView cvsImageView){
		if (new File(objContent).exists()) {
			Bitmap bitmap = BitmapFactory.decodeFile(objContent);
			cvsImageView.setVisibility(View.VISIBLE);
			cvsImageView.setImageBitmap(bitmap);
			
		} else {
			cvsImageView.setImageBitmap(null);
			cvsImageView.setVisibility(View.GONE);
			
		}

		}
	
	/**
	 * @author saveBitmapImage
	 * @param path
	 */
	private void saveBitmap(String pictureName) {
		try {
			canvasView.saveBitmap(pictureName);
			canvasView.changed(false);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Enrich viewAdapter to load viewPager for Enrich onclick.
	 * @author vamsi
	 *
	 */
	private class EnrichViewAdapter extends PagerAdapter{

		@Override
		public int getCount() {

			return 2;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {

			return (view == object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {

			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			int resId = 0;
			View view = null;
			switch (position) {
			case 0:
				resId = R.layout.enrich_page_list;
				view = inflater.inflate(resId, null);
				enrichPageListView = (ListView)view.findViewById(R.id.listView1);
				enrichPage1bar =(ProgressBar)view.findViewById(R.id.enrich_progressbar);
				noenrichPage1_layout = (RelativeLayout)view.findViewById(R.id.noenrich_layout);
				
				downloadType = "private";
				final SegmentedRadioGroup btnSegmentGroup = (SegmentedRadioGroup) view.findViewById(R.id.btn_segment_group);
				
				TextView titleTextView = (TextView) view.findViewById(R.id.textView1);
				final TextView noEnrichtext = (TextView) view.findViewById(R.id.noenrich);
				enrichPageListView.setVisibility(View.INVISIBLE);
				enrichPage1bar.setVisibility(View.VISIBLE);
				
				
                btnSegmentGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						if (checkedId == R.id.btn_segment_private) {
							downloadType = "private";
							noenrichPage1_layout.setVisibility(View.GONE);
							enrichPage1bar.setVisibility(View.VISIBLE);
						} else if (checkedId == R.id.btn_segment_public) {
							downloadType = "public";
							noenrichPage1_layout.setVisibility(View.GONE);
							enrichPage1bar.setVisibility(View.VISIBLE);
						}
						enrichpages.LoadEnrichmentFirst(BookView.this, bookName, 0);
					}
					
				});

				enrichPageListView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {

						//set EnrSelectAll to select:
						EnrSelectAll = "select";
						itemChecked.clear();
						enrichpages.enrichDownloadArray.clear();
						selectEnrichListView.setVisibility(View.INVISIBLE);
						if(enrichprogressbar != null){
							enrichprogressbar.setVisibility(View.VISIBLE);
						}
						enrichViewpager.setCurrentItem(1, true);
						try {
							enrichpages.currentPageNumber = Integer.parseInt(enrichpages.enrichPagesArray.get(arg2));
						} catch (Exception e) {
							enrichpages.currentPageNumber = 1;
						}
						//Call to EnrichPages class to method LoadEnrichmentFirst with serviceType = 1
						enrichpages.LoadEnrichmentFirst(BookView.this,bookName, 1);
					}
				});

				//Check language :
				titleTextView.setText(getResources().getString(R.string.downloadEnrich));
				noEnrichtext.setText(getResources().getString(R.string.noenrichments));
				break;

			case 1:
				resId = R.layout.enrichment_list;
				view = inflater.inflate(resId, null);
				selectEnrichListView = (ListView)view.findViewById(R.id.listView2);
				enrichprogressbar =(ProgressBar)view.findViewById(R.id.enrich_progressbar);
				noenrich_layout = (RelativeLayout)view.findViewById(R.id.noenrich_layout);
				enrichprogressbar.setVisibility(View.VISIBLE);
				TextView noEnrichText = (TextView) view.findViewById(R.id.noenrich);
				selectEnrichListView.setAdapter(new checklistAdapter(BookView.this, enrichpages.enrichDownloadArray));
				selectEnrichListView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						Boolean listSelected = itemChecked.get(arg2);

						if(listSelected == false){
							itemChecked.set(arg2,true);
						}else{
							itemChecked.set(arg2,false);
						}
						selectEnrichListView.invalidateViews();
					}
				});

				enrichpages.enrichDownloadArray.clear();
				final	Button select  = (Button)view.findViewById(R.id.btnselectall);
				select.setTag(1);
				select.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						final int count = (Integer)v.getTag();
						if(count==1){
							EnrSelectAll = "selectAll";
							select.setText(getResources().getString(R.string.select_none));
							v.setTag(0);
						}else{
							EnrSelectAll = "selectNone";
							select.setText(getResources().getString(R.string.selectall));
							v.setTag(1);
						}

						itemChecked.clear();
						for(int i=0;i<enrichpages.enrichDownloadArray.size();i++){
							if(EnrSelectAll.equals("selectAll") || EnrSelectAll.equals("√É∆í√Ç¬ø√É¬¢√¢‚Ç¨≈æ√Ç¬¢√É∆í√Ç¬ø√É¬¢√¢‚Ç¨¬∞√Ç¬†√É∆í√Ç¬ø√É∆í√ã≈ì√É‚Ä¶√Ç¬∏√É∆í√Ç¬§√É∆í√Ç¬ø√É∆í√ã≈ì √É∆í√Ç¬ø√É∆í√Ö¬∏√É‚Ä¶√Ç¬∏√É∆í√¢‚Ç¨Àú√É‚Ä¶√Ç¬∏√É∆í√¢‚Ç¨¬∞√É‚Ä¶√Ç¬∏√É∆í√¢‚Ç¨Àú")){
								itemChecked.add(i,true);
							}else if(EnrSelectAll.equals("selectNone") || EnrSelectAll.equals("√É∆í√Ç¬ø√É¬¢√¢‚Äö¬¨√Ç¬¢√É‚Ä¶√Ç¬∏√É∆í√¢‚Ç¨Àú√É∆í√Ç¬ø√É¬¢√ã‚Ä†√Ç¬´√É∆í√Ç¬ø√É∆í√Ö¬∏√É∆í√Ç¬ø√É‚Äö√Ç¬∞ √É∆í√Ç¬ø√É∆í√Ö¬∏√É‚Ä¶√Ç¬∏√É∆í√¢‚Ç¨Àú√É∆í√Ç¬ø√É¬¢√¢‚Ç¨≈æ√Ç¬¢√É∆í√Ç¬ø√É¬¢√¢‚Ç¨¬∞√Ç¬†√É∆í√Ç¬ø√É∆í√ã≈ì√É‚Ä¶√Ç¬∏√É∆í√Ç¬§√É∆í√Ç¬ø√É∆í√ã≈ì")){
								itemChecked.add(i,false);
							}
						}

						selectEnrichListView.invalidateViews();
					}
				});

				Button btnback = (Button)view.findViewById(R.id.btnback);
				btnback.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						enrichViewpager.setCurrentItem(0,true);
					}
				});
				Button btnDownload = (Button)view.findViewById(R.id.btndownload);
				btnDownload.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						//Download enrichments :
						callToDownloadEnrichments();
					}
				});

				//Check language :
					btnback.setText(getResources().getString(R.string.back));
					select.setText(getResources().getString(R.string.selectall));
					btnDownload.setText(getResources().getString(R.string.download));
					noEnrichText.setText(getResources().getString(R.string.noenrichments));
				break;

			default:
				break;
			}
			((ViewPager)container).addView(view, 0);
			return view;
		}
	}

	public void callToDownloadEnrichments(){
		if(enrichpages.enrichDownloadArray.size() == 0){
			String text =""+ R.string.select_enrichments;
			Toast toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
			toast.show();
		}else{

			for(int i=0;i<itemChecked.size();i++){
				if(itemChecked.get(i)){

					enrichpages.enrichSelectArray.add(enrichpages.enrichDownloadArray.get(i));
					////System.out.println("The download files: "+enrichpages.enrichDownloadArray.get(i));
					enrichpages.downloadEnrichFiles(enrichpages.enrichDownloadArray.get(i));
				}
			}
		}
	}

	public void listviewPage1Invalidate(){

		if(enrichpages.enrichPagesArray.size()>0){
			noenrichPage1_layout.setVisibility(View.GONE);
			enrichPageListView.setVisibility(View.VISIBLE);
			enrichPageListView.setAdapter(new ListContentAdapter(BookView.this, enrichpages.enrichPagesArray));
		}else{
			noenrichPage1_layout.setVisibility(View.VISIBLE);
		}
	}

	public void listviewInvalidate(){
		//Check for enrichdownloadarray count: if zero display textview with "no new enrichments"

		for(int i=0;i<enrichpages.enrichDownloadArray.size();i++){
			itemChecked.add(i,false);
		}		
		if(enrichpages.enrichDownloadArray.size()>0){
			noenrich_layout.setVisibility(View.GONE);
			selectEnrichListView.setVisibility(View.VISIBLE);
			selectEnrichListView.invalidateViews();
		}else{

			noenrich_layout.setVisibility(View.VISIBLE);
		}
	}

	public void enrichPage1Progressbar(){

		enrichPage1bar.setVisibility(View.GONE);
	}

	public void enrichprogressbar(){

		enrichprogressbar.setVisibility(View.GONE);
	}

	//@Override
	public void afterTextChanged(Editable s) {

		if (searchEditText.getText().toString().equals("")) {
			clearSearchBox.setVisibility(View.GONE);
		} else {
			clearSearchBox.setVisibility(View.VISIBLE);
		}
	}

	//@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		//webView.endSelectionMode();

		//////System.out.println("before text Changed");
	}

	//@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

		//////System.out.println("On text Changed");
	}
	
	public void onBackPressed() 
	{
		int pNo = pageNumber-1;
		String query = "update books set LastViewedPage='"+pNo+"' where storeID='"+bookName+"'";
		db.executeQuery(query);
		currentBook.set_lastViewedPage(pNo);
		setResult(RESULT_OK, getIntent().putExtra("Book", currentBook));
		if (!adsPurchased) {
			if (interstitial.isLoaded()) {
				interstitial.show();
			}
		}
		finish();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		db.unbindDrawables(findViewById(R.id.rootView));
		System.gc();
		uiHelper.onDestroy();
	}

	/* Page Slider - Begin */
	@Override
	public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) 
	{

		if (LBooklanguage.equalsIgnoreCase("English")) {
			sb_pNo = arg1;
			if(arg1==0)
			{
				tv_pageNumber.setText("Cover Page");
			}
			else if(arg1==pageCount-1)
			{
				tv_pageNumber.setText("Last Page");
			}
			else
			{
				//tv_pageNumber.setText("Page ("+(arg1)+"/"+(pageCount-2)+")");
				tv_pageNumber.setText(""+(arg1)+" of "+(pageCount-2)+"");
			}
		}else{
			sb_pNo = pageCount-(arg1+1);
			if(arg1==0)
			{
				tv_pageNumber.setText("Last Page");
			}
			else if(arg1==pageCount-1)
			{
				tv_pageNumber.setText("Cover Page");
			}
			else
			{
				//tv_pageNumber.setText("Page ("+(arg1)+"/"+(pageCount-2)+")");
				tv_pageNumber.setText(""+(pageCount-2)+" of "+(pageCount-(arg1+1))+"");
			}
		}

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) 
	{

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) 
	{	
		passingPageNumberValues(sb_pNo);
	}
	/* Page Slider - End */


	//chnages-notes
	public static void displayNotesPopup()
	{
		//notepop.update(tipsWidth,tipsHeight);
		notepop.showAtLocation(view_notepop, Gravity.CENTER, 10, 10);
		notepop.update(deviceWidth/2, deviceHeight/4);
	}

	private static void displaySmallNotesPopup(int sX, int sY)
	{
		//s-start e-end m-middle
		int notepopW,notepopH,mX,mY,paddingW,paddingH,keyboardH;
		//calculate popup Height and Width based on device rsln
		notepopW =(int) (deviceWidth/2.0); //2.5
		notepopH =(int) (deviceHeight/3.5); //4.5
		keyboardH = (int) ((deviceHeight/4)*3);//space left after shwing keyboard
		paddingW=20;
		paddingH=2;
		mX=sX;//(int) ((sX+eX)/2);//calculate middle point of selected txt
		mY=sY+paddingH;//used as padding to prevent highlight overlay(bottom)


		if(mY<=(int)(keyboardH-notepopH))//displaying popup below selected txt
		{
			if(mX+notepopW<=deviceWidth)
			{
				if(mX>=(deviceWidth/2)-75 && mX<=(deviceWidth/2)+75)//btm center
				{
					displayArrow(2);
					notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-(notepopW/2), mY);
				}
				else//btm left
				{
					displayArrow(1);
					notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-paddingW, mY);
				}
			}
			else if(mX+notepopW>=deviceWidth)
			{
				if(mX>=(deviceWidth/2)-75 && mX<=(deviceWidth/2)+75)//btm center
				{
					displayArrow(2);
					notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-(notepopW/2), mY);
				}
				else//btm right      5.4  Y=233 X=178
				{
					displayArrow(3); 
					notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, (mX-notepopW+paddingW), mY);
				}
			}
		}

		else//displaying popup above selected txt
		{
			//mY=sY-paddingH2;//used as padding to prevent highlight overlay(top)
			mY=(int) (deviceHeight/2);
			if(mX+notepopW<=deviceWidth)
			{
				if(mX>=(deviceWidth/2)-75 && mX<=(deviceWidth/2)+75)//top center
				{
					displayArrow(4);
					notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-(notepopW/2), mY-notepopH);
				}
				else//top left
				{
					displayArrow(5);
					notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-paddingW, mY-notepopH);
				}
			}
			else if(mX+notepopW>=deviceWidth)
			{
				if(mX>=(deviceWidth/2)-75 && mX<=(deviceWidth/2)+75)//top center
				{
					displayArrow(4);
					notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, mX-(notepopW/2), mY-notepopH);
				}
				else//top right
				{
					displayArrow(6);
					notepop.showAtLocation(view_notepop, Gravity.NO_GRAVITY, (mX-notepopW+paddingW), mY-notepopH);
				}
			}
		}

		notepop.update(notepopW,notepopH);
	}

	//used for displaying the small popup arrow
	private static void displayArrow(int tmp)
	{	
		if(tmp==1)
		{
			poparrow_topcontainer.setVisibility(View.VISIBLE);
			poparrow_bottomcontainer.setVisibility(View.GONE);
			iv_toparrow_left.setVisibility(View.VISIBLE);
			iv_toparrow_right.setVisibility(View.GONE);
			iv_toparrow_center.setVisibility(View.GONE);
		}
		else if(tmp==2)
		{
			poparrow_topcontainer.setVisibility(View.VISIBLE);
			poparrow_bottomcontainer.setVisibility(View.GONE);
			iv_toparrow_left.setVisibility(View.GONE);
			iv_toparrow_right.setVisibility(View.GONE);
			iv_toparrow_center.setVisibility(View.VISIBLE);
		}
		else if(tmp==3)
		{
			poparrow_topcontainer.setVisibility(View.VISIBLE);
			poparrow_bottomcontainer.setVisibility(View.GONE);
			iv_toparrow_left.setVisibility(View.GONE);
			iv_toparrow_right.setVisibility(View.VISIBLE);
			iv_toparrow_center.setVisibility(View.GONE);
		}
		else if(tmp==4)
		{
			poparrow_topcontainer.setVisibility(View.GONE);
			poparrow_bottomcontainer.setVisibility(View.VISIBLE);
			iv_bottomarrow_left.setVisibility(View.VISIBLE);
			iv_bottomarrow_right.setVisibility(View.GONE);
			iv_bottomarrow_center.setVisibility(View.GONE);
		}
		else if(tmp==5)
		{
			poparrow_topcontainer.setVisibility(View.GONE);
			poparrow_bottomcontainer.setVisibility(View.VISIBLE);
			iv_bottomarrow_left.setVisibility(View.GONE);
			iv_bottomarrow_right.setVisibility(View.GONE);
			iv_bottomarrow_center.setVisibility(View.VISIBLE);
		}
		else if(tmp==6)
		{
			poparrow_topcontainer.setVisibility(View.GONE);
			poparrow_bottomcontainer.setVisibility(View.VISIBLE);
			iv_bottomarrow_left.setVisibility(View.GONE);
			iv_bottomarrow_right.setVisibility(View.VISIBLE);
			iv_bottomarrow_center.setVisibility(View.GONE);
		}
	}

	//Enrich popup dismiss :
	@Override
	public void popoverViewWillShow(PopoverView view) {

	}

	@Override
	public void popoverViewDidShow(PopoverView view) {

	}

	@Override
	public void popoverViewWillDismiss(PopoverView view) {
		EnrSelectAll = "select";
		enrichpages.enrichDownloadArray.clear();
		
	}

	@Override
	public void popoverViewDidDismiss(PopoverView view) {
		
	}


	@Override
	public void onDismiss(DialogInterface d) {
		// TODO Auto-generated method stub
	   if(advsearchbkmark==true){
			 if (currentbkmarkLayout.getWidth()>currentparentbklayout.getWidth()) {
				   int  enrTabsWidth1 = currentbkmarkLayout.getWidth();
				   currentbkmarkscroll.smoothScrollTo(enrTabsWidth1, 0);
			   }
			 advsearchbkmark=false;
		}else{
			 if (currentEnrTabsLayout.getWidth()>parentEnrRLayout.getWidth()) {
				   int  enrTabsWidth1 = currentEnrTabsLayout.getWidth();
				   sv.smoothScrollTo(enrTabsWidth1, 0);
			 }
		}
	
		
	}

	
	
}
