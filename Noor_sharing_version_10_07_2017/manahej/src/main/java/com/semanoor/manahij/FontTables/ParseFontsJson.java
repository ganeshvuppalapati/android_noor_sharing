package com.semanoor.manahij.FontTables;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.semanoor.manahij.ManahijApp;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by semanoor on 4/17/2017.
 */

public class ParseFontsJson {
    private final String URL_STRING_REQ = "https://www.nooor.com/fonttables/fonttables.json";
    FontTablesDB db = null;
    Context context;
    IMyService service;
    private static final String TAG = "Parsing";
    private Request.Priority priority = Request.Priority.HIGH;
    fonttablescallback fonttablescallback;

    public ParseFontsJson(Context context, fonttablescallback _fonttablescallback, FontTablesDB _db) {
        this.context = context;
        fonttablescallback = _fonttablescallback;
        this.db = _db;
    }

    public void parseJson() {


        StringRequest strReq = new StringRequest(Request.Method.GET, URL_STRING_REQ, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    int i = 0;
                    if (fonttablescallback != null)
                        fonttablescallback.response(response);


                } catch (Exception e) {
                    Log.e("exception", e.toString());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Priority getPriority() {
                return priority;
            }

        };

        ManahijApp.getInstance().addToRequestQueue(strReq);

    }



    public interface IMyService {
        @GET
        @Streaming
        Call<ResponseBody> getFile(@Url String url);
    }

    public void getJsonDataFronServer(final String fileName, String url) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url + "/")
                .build();
        service = retrofit.create(IMyService.class);
        service.getFile(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(context, "FAILED", Toast.LENGTH_LONG).show();
                    return;
                }

                DownloadFontTablesAsyncTask downloadFileAsyncTask = new DownloadFontTablesAsyncTask(fileName);
                downloadFileAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, response.body().byteStream());

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }
}
