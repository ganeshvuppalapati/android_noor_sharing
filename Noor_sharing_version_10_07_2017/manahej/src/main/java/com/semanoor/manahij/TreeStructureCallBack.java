package com.semanoor.manahij;

import com.semanoor.source_sboookauthor.PopoverView;
import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by karthik on 10-05-2017.
 */

public interface TreeStructureCallBack {
    public void onClick(TreeNode node, Object object, PopoverView popoverView);
}
