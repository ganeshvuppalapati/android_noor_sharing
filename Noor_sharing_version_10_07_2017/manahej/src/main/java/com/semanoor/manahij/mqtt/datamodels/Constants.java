package com.semanoor.manahij.mqtt.datamodels;

/**
 * Created by semanoor on 18/04/17.
 */

public class Constants {
    //reciwver
    public static final String BROADCAST_ACTION_MQTT_STATUS = "org.semanoor.noor.MQTT_STATUS";
    public static final String BROADCAST_ACTION_SUBSCRIBE_TO_SINGLE_TOPIC = "org.semanoor.noor.TOPIC_SUBSCRIPTION";
    public static final String BROADCAST_ACTION_SUBSCRIBE_LIST_OF_TOPICS = "org.semanoor.noor.LIST_TOPIC_SUBSCRIPTION";
    public static final String BROADCAST_ACTION_PUBLISH = "org.semanoor.noor.PUBLISH";
    public static final String BROADCAST_ACTION_UNSUBSCRIBE = "org.semanoor.noor.UNSUBSCRIBE";

    public static final String TOPIC_NAME = "topic_name";
    public static final String PUBLISH_MSG = "publishmsg";
    public static final String PUBLISH_TOPIC = "publishtopic";
    public static final String MQTT_STATUS = "mqttstatus";
    //preferences
    public static final String CLIENT_IDENTITY = "clientidentity";
    public static final String TOPIC_SUBSCRIPTION_STATUS = "topic_state";
    public static  final String NOORUSERSTATUS ="nooruserstatus";
    public static final String OTHERTOPICNAME = "NOOR_OTHER_Topic";
    public static final String MAINCHANNEL = "NoorMainChannel";
    public static final String ISMAINCHANNEL = "isMainChannel";
    public static final String ISBACKGROUND = "isBackground";
    public static final String SUBSCRIBETOPICNAME = "subscribe_Topic_Name";
    public static final String lastviewdpage = "lastviewedpage";
    public static final String KEYMESSAGE = "noormessage";
    public static final String ANDROID_ID = "android_id";


    public static final String KEYTYPE = "keytype";
    // connection  KEYS
    public static final String KEYCALLCONNECTION = "call";
    public static final String KEYCALLREQUEST = "call_request";
    public static final String KEYCALLREQUESTR = "call_cancel";
    public static final String KEYCALLDISCONNECTIONREQUEST = "call_disconnect";
   //book
    public static final String KEYOPENBOOK = "open_book";//main
    public static final String KEYBOOK = "book";
    public static final String KEYPAGECHANGE = "page_change";
    public static final String NOTE = "note";
    public static final String HIGHLIGHT = "highlight";
    public static final String KEYBOOKCLOSE = "book_close";
    public static final String  KEYOPENFLASHCARD = "open";
    //DRAWING
    public static final String DRAWING = "drawing";
    public static final String ACTION_DOWN = "action_down";
    public static final String ACTION_MOVE = "action_move";
    public static final String x = "x";
    public static final String y = "y";
    public static final String DEVICE_HEIGHT = "device_height";
    public static final String DEVICE_WIDTH = "device_width";
    public static final String CLEAR_DRAW = "clear_draw";
    public static final String OPACITY = "opacity";
    public static final String THICKNESS = "thickness";
    public static final String DRAWING_LINE_TYPE = "drawing_line_type";
    public static final String DRAWING_BTN_DONE = "drawing_btnDone";
    public static final String BTN_ERASER = "eraser";
    public static final String COLOR_CODE = "color_code";
    public static final String SET_SELECTED_COLOR_TO_DRAW = "set_selected_color_to_draw";
    public static final String DRAW_PEN = "draw_pen";
    public static final String ENABLE_DRAWING = "enable_drawing";


    //flashcard
    public static final String KEYFLASHSELECTION = "flashcard_selection";
    public static final String KEYADDFLASHCARD = "add_flashcard_layout";
    public static final String KEYCLOSEFLASHCARD = "close";
    public static final String KEYWELLREVIEW = "review";
    public static final String FLASHCARD = "flashcard";
    public static final String KEYNOTESBASEDONTYPE=  "load_notes_based_on_review_type";
    public static final String FLASHCOLORSELECION = "color_selection";
    public static final String FLASHANIMATION = "animation";
    public static final String KEYFLASHCOLORSELECION = "notecolorselection";
    public static final String KEYFLASHADDNOTE = "add_flashcard";
    public static final String KEYFLASHNOTETURN = "flash_note_turn";
    public static final String KEYFLASHTURN = "flip_flash";
    //vedio
    public static final String KEYVEDIOCALL = "vedio_call";





    //urls
    public static final String NOOR_USER_STATUS = "http://174.129.30.141:8085/NoorCommunication/webapi/noorusersonlinestatus/getonlineuserstatus";
    public static final String TWILIO_ACCESS_TOKEN_URL="http://174.129.30.141:8085/TokenGenerator/gentoken?";



}
