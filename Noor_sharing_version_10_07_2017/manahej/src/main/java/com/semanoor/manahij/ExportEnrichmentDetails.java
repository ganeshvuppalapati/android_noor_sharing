package com.semanoor.manahij;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.Note;

import java.util.ArrayList;

/**
 * Created by Krishna on 03-10-2016.
 */
public class ExportEnrichmentDetails {

    private Activity context;
    private AccountManager accountManager;
    private Account mAccount;
    public Drive drive;
    private String fromEmailId;
    private static ExportEnrichmentDetails instance;
    private GoogleApiClient googleApiClient;
    public ArrayList<File> categoryList;
    public ArrayList<Enrichments> mindmapSelectedList;
    public ArrayList<Enrichments> enrichmentSelectedLinkList;
    public ArrayList<Enrichments> enrichmentSelectedList;
    public ArrayList<Note> noteSelectedList;


    public static synchronized ExportEnrichmentDetails getInstance() {
        if (instance == null) {
            instance = new ExportEnrichmentDetails();
        }
        return instance;
    }

    public ArrayList<Enrichments> getMindmapSelectedList() {
        return mindmapSelectedList;
    }

    public void setMindmapSelectedList(ArrayList<Enrichments> mindmapSelectedList) {
        this.mindmapSelectedList = mindmapSelectedList;
    }

    public ArrayList<Enrichments> getEnrichmentSelectedLinkList() {
        return enrichmentSelectedLinkList;
    }

    public void setEnrichmentSelectedLinkList(ArrayList<Enrichments> enrichmentSelectedLinkList) {
        this.enrichmentSelectedLinkList = enrichmentSelectedLinkList;
    }

    public ArrayList<Enrichments> getEnrichmentSelectedList() {
        return enrichmentSelectedList;
    }

    public void setEnrichmentSelectedList(ArrayList<Enrichments> enrichmentSelectedList) {
        this.enrichmentSelectedList = enrichmentSelectedList;
    }

    public ArrayList<Note> getNoteSelectedList() {
        return noteSelectedList;
    }

    public void setNoteSelectedList(ArrayList<Note> noteSelectedList) {
        this.noteSelectedList = noteSelectedList;
    }
}
