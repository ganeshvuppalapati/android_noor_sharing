package com.semanoor.manahij;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.semanoor.sboookauthor_store.Enrichments;

import java.util.ArrayList;

/**
 * Created by karthik on 10-04-2017.
 */

public class CallBackObjects {

  public int adapterPosition;
  public View view;
  public boolean loadEnrPoview;
  public boolean doubleTap;
  public RecyclerView recyclerView;
  public ArrayList<Enrichments> horizontalList;

    public CallBackObjects(int pos,View v,boolean loadPopview,boolean doubleClick,RecyclerView rView,ArrayList<Enrichments> enrList){
        this.adapterPosition = pos;
        this.view = v;
        this.loadEnrPoview = loadPopview;
        this.doubleTap = doubleClick;
        this.horizontalList = enrList;
        this.recyclerView = rView;
    }

}
