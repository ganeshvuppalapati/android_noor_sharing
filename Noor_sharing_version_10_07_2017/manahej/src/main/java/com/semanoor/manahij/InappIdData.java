package com.semanoor.manahij;

/**
 * Created by karthik on 12-01-2017.
 */

public class InappIdData {
    private String clientIds;
    private String endDate;
    private String inAppIds;

    public String getClientIds() {
        return clientIds;
    }

    public void setClientIds(String clientIds) {
        this.clientIds = clientIds;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getInAppIds() {
        return inAppIds;
    }

    public void setInAppIds(String inAppIds) {
        this.inAppIds = inAppIds;
    }
}
