package com.semanoor.manahij;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.artifex.mupdfdemo.AsyncTask;
import com.artifex.mupdfdemo.MuPDFCore;
import com.artifex.mupdfdemo.TextChar;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.TextCircularProgressBar;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by semanoor on 3/21/2017.
 */

public class PdfParseSearchXml {
    MuPDFCore mCore;
    Context mContext;
    String fontTable = "";
    JSONObject mainJSONObj = new JSONObject();
    String bookPath;
    DatabaseHandler db;

    public PdfParseSearchXml(Context context, String path, TextCircularProgressBar progressBar, DatabaseHandler dataBase) {
        bookPath = path.substring(0, path.lastIndexOf("/") + 1);
        mContext = context;
        db = dataBase;
        try {
            mCore = new MuPDFCore(context, path, null);
            fontTable = loadJSONFromAsset();
            mainJSONObj = new JSONObject(fontTable);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!new File(bookPath + "/Search.xml").exists())
            new getPagesContent(mCore, progressBar).execute();
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open("font_table.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private class getPagesContent extends AsyncTask<Integer, Void, String> {
        private TextCircularProgressBar progress;
        MuPDFCore muPDFCore;

        public getPagesContent(MuPDFCore core, TextCircularProgressBar progressBar) {
            muPDFCore = core;
            progress = progressBar;
        }

        @Override
        protected void onPreExecute() {

            if (progress != null) {
                progress.setVisibility(View.VISIBLE);
            }
        }

        int pageCount = 0;
        public boolean isEnglish = false;
        int i = 0;

        @Override
        protected String doInBackground(Integer... params) {
            pageCount = muPDFCore.countPages();
            progress.setMax(pageCount);
            searchMapping.clear();
            search.clear();
            searchBridge.clear();
            normal.clear();
            i = 0;
            for (i = 0; i < pageCount; i++) {
                searchMapping.add(i, "");
                search.add(i, "");
                searchBridge.add(i, "");
                normal.add(i, "");
            }
            for (i = 0; i < pageCount; i++) {
               /* if (i != 0)*/
                {
                    try {
                        Thread.sleep(500);
                        publishProgress();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    TextChar[][][][] pageData = muPDFCore.textChars(i);
                    if (pageData != null && pageData.length > 0)
                        getPageFormat(pageData, i);

                /*search.add(getTextofPage(i));
                searchMapping.add(getTextofPage(i));
				searchBridge.add(getTextofPage(i));*/
                }

            }
            return null;
        }


        public void getPageFormat(TextChar[][][][] pageData, int index) {
            String searchText = "";
            String searchMappingText = "";
            String searchBridgeText = "";
            String normalText = "";
            int letterCount = 1;
            for (TextChar[][][] bl : pageData) {
                if (bl == null)
                    continue;
                for (TextChar[][] ln : bl) {
                    for (TextChar[] sp : ln) {
                        for (TextChar tc : sp) {
                            {
                                String mappingCode = "";

                                String fntName = tc.font;
                                String[] fntArray = fntName.split("\\+");
                                if (fntArray.length == 2) {
                                    String fntCode = fntArray[1];
                                    String charCode = "";
                                    if (tc.mGlyph > 0) {
                                        int eseries = 57344 + tc.mGlyph;
                                        charCode = dec2Hex(eseries);
                                    } else {
                                        charCode = toUnicode(tc.c);
                                    }

                                    String uniCode = null;
                                    try {
                                        switch (fntCode) {
                                            case "AXtManalBold":
                                            case "AXtAhlam":
                                            case "AXtTRaditionalBold":
                                            case "AXtManal":
                                            case "AXtManalBLack":
                                            case "AXtYousra":
                                            case "YakoutLinotypeLight-Bold":
                                            case "AXtRana":
                                                uniCode = mainJSONObj.getJSONObject(fntCode).get(charCode).toString();
                                                break;
                                            case "GEEast-ExtraBold":
                                                uniCode = charCode;
                                                break;
                                        }

                                    } catch (JSONException e) {

                                        uniCode = null;
                                    }
                                    if (uniCode == null)
                                        mappingCode = charCode;
                                    else
                                        mappingCode = uniCode;

                                }
                                if (!mappingCode.equals("0x0000") && !mappingCode.equals("0xfffd")) {
                                    searchText = searchText + mappingCode;

                                    searchBridgeText = searchBridgeText + letterCount + ",";

                                }
                                //int eseries=57344+tc.mGlyph;
                                //Log.e("mapping",tc.c+" glyph "+tc.mGlyph+" eseries "+dec2Hex(eseries)/*Integer.valueOf(String.valueOf(eseries), 16)*/);

                                searchMappingText = searchMappingText + toUnicode(tc.c);
                                normalText = normalText + tc.c;
                                letterCount = letterCount + 1;
                            }
                        }
                    }

                }
            }

            search.set(index, searchText);
            searchBridge.set(index, searchBridgeText);
            //  searchMappingText=   searchMappingText.replaceAll("0x00200x0020","0x0020");
            searchMapping.set(index, searchMappingText);
            normal.set(index, normalText);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            progress.setProgress(i);
            Log.d("", "sdfsdfsdfsdf   " + i);
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            /*for (int i = 0; i <pageCount-1; i++) {
                if(i!=0) {
					gotoPage(i);
					TextChar[][][][] pageData = text();
					if (pageData != null && pageData.length > 0)
						getPageFormat(pageData);
				*//*search.add(getTextofPage(i));
                searchMapping.add(getTextofPage(i));
				searchBridge.add(getTextofPage(i));*//*
                }
			}*/


            ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
            Runnable worker = new MyRunnable("Search.xml", search);
            executor.execute(worker);
            Runnable worker1 = new MyRunnable("searchMapping.xml", searchMapping);
            executor.execute(worker1);
            Runnable worker2 = new MyRunnable("searchBridging.xml", searchBridge);
            executor.execute(worker2);
            if (progress != null)
                progress.setVisibility(View.GONE);

            int lastBook = db.getMaxUniqueRowID("books");
            String updateBookStatusQuery = "UPDATE books SET isDownloadedCompleted = 'downloaded' WHERE BID='" + lastBook + "'";
            db.executeQuery(updateBookStatusQuery);
            /*generateXml("Search.xml",search);
            generateXml("searchMapping.xml",searchMapping);
			generateXml("searchBridging.xml",searchBridge);*/

        }
    }

    private static int MYTHREADS = 3;
    ArrayList<String> searchMapping = new ArrayList<>();
    ArrayList<String> search = new ArrayList<>();
    ArrayList<String> normal = new ArrayList<>();
    ArrayList<String> searchBridge = new ArrayList<>();

    public class MyRunnable implements Runnable {

        String name;
        ArrayList<String> data;

        MyRunnable(String name, ArrayList<String> data) {
            this.name = name;
            this.data = data;
        }

        @Override
        public void run() {

            String result = "";

            try {
                generateXml(name, data);
                result = "success";
            } catch (Exception e) {
                result = "Fail";
            }
            //System.out.println(src + "\t\tStatus:" + result);
        }
    }

    public static String dec2Hex(int num) {
        String hex = "";

        while (num != 0) {
            if (num % 16 < 10)
                hex = Integer.toString(num % 16) + hex;
            else
                hex = (char) ((num % 16) + 55) + hex;
            num = num / 16;
        }

        return "0x" + hex;
    }

    public void generateXml(String fileName, ArrayList<String> searchMapping) {
        String pageText = "'";
        String words;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            Element rootElement = doc.createElement("Search");
            doc.appendChild(rootElement);
                /*Element supercar = doc.createElement("supercars");
                rootElement.appendChild(supercar);*/
            Attr attr = doc.createAttribute("Name");
            attr.setValue("Search");
            rootElement.setAttributeNode(attr);

            for (int i = 0; i < searchMapping.size(); i++) {
                pageText = searchMapping.get(i);
                String nodeName = "P" + (i + 1);
                Element carname1 = doc.createElement(nodeName);
                Attr attrName = doc.createAttribute("Name");
                attrName.setValue("" + (i + 1));
                carname1.setAttributeNode(attrName);


                Attr attrData = doc.createAttribute("Data");
                attrData.setValue(pageText);
                carname1.setAttributeNode(attrData);
                //carname1.appendChild(doc.createTextNode("endTag"));
                rootElement.appendChild(carname1);
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result1 = new StreamResult(new File(bookPath + "/" + fileName));
            transformer.transform(source, result1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String toUnicode(char ch) {

        String uniqueCode = String.format("%04x", (int) ch).toUpperCase();
        return "0x" + uniqueCode;

    }
}
