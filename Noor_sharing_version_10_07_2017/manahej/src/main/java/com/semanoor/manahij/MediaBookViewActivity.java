package com.semanoor.manahij;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

/**
 * Created by Krishna on 07-01-2016.
 */
public class MediaBookViewActivity  extends Activity implements View.OnClickListener{

    LinearLayout layout;
    RelativeLayout layout1;
    String currentbookpath;
    private GridShelf gridShelf;
    private Book currentBook;
    DatabaseHandler db;
    ArrayList<String> filesList=new ArrayList<String>();
    ViewPager viewPager;
    String path;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title Bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_elessonfullscreen);

        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
        ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

        UserFunctions.changeFont(rootView,font);
        currentBook = (Book) getIntent().getSerializableExtra("Book");
        gridShelf = (GridShelf) getIntent().getSerializableExtra("Shelf");
        int pageNumber = getIntent().getIntExtra("currentPageNo", 1);
        db = DatabaseHandler.getInstance(this);
        layout = (LinearLayout) findViewById(R.id.closeLayout);
        layout1 = (RelativeLayout) findViewById(R.id.btnlayout);
        CircleButton btnClose = (CircleButton) findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
        Button btnPrev = (Button) findViewById(R.id.btn_Prev);
        btnPrev.setOnClickListener(this);
        Button btnNext = (Button) findViewById(R.id.btn_Next);
        btnNext.setOnClickListener(this);
        Button btnStart = (Button) findViewById(R.id.btn_Start);
        btnStart.setOnClickListener(this);
        Button btnLast = (Button) findViewById(R.id.btn_Last);
        btnLast.setOnClickListener(this);
        path= Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/";


        File f = new File(path);
       // File[] currentFiles=f.listFiles();
        File[] currentFiles=f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return !file.isHidden();
            }
        });
        for(int i=0;i<currentFiles.length;i++){
            if(currentFiles[i].isDirectory()){

            }else if(currentFiles[i].isFile()){
                filesList.add(currentFiles[i].getPath());
            }
        }
        viewpagerAdapter pageradapter = new viewpagerAdapter(MediaBookViewActivity.this);
        viewPager = (ViewPager) findViewById(R.id.fullscreenview);
        viewPager.setAdapter(pageradapter);
        viewPager.setCurrentItem(pageNumber);

    }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnClose:
                    onBackPressed();
                    break;
                case R.id.btn_Prev:
                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                    break;
                case R.id.btn_Next:
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    break;
                case R.id.btn_Start:
                    viewPager.setCurrentItem(filesList.size() - 1);
                    break;
                case R.id.btn_Last:
                    viewPager.setCurrentItem(0);
                    break;
            }
   }



    public void onBackPressed() {
        db.executeQuery("update books set LastViewedPage='" + viewPager.getCurrentItem() + "' where BID='" + currentBook.getBookID() + "'");
        currentBook.set_lastViewedPage(viewPager.getCurrentItem());
        setResult(RESULT_OK, getIntent().putExtra("Book", currentBook));
        setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
        finish();
    }
class viewpagerAdapter extends PagerAdapter {
    Context context;

    public viewpagerAdapter(Context c) {
        context = c;
    }

    public Object instantiateItem(View collection, final int position) {
        String urlpath;
        int pageNumber=position+1;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.media_image, null);
        final TextView txtview = (TextView) view.findViewById(R.id.pagno_Txt);
        txtview.setText(pageNumber + "/" + filesList.size());
        if (layout1.isShown()) {
            txtview.setVisibility(View.VISIBLE);
        } else {
            txtview.setVisibility(View.GONE);
        }

        ImageView imageView= (ImageView) view.findViewById(R.id.iv_pages);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        final Bitmap bitmap = BitmapFactory.decodeFile(filesList.get(position),options);
        imageView.setImageBitmap(bitmap);

        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override                                              //onTouch for visible and invisible buttons
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        if (layout.isShown() && layout1.isShown()) {
                            layout.setVisibility(View.GONE);
                            layout1.setVisibility(View.GONE);
                            txtview.setVisibility(View.GONE);
                        } else {
                            layout.setVisibility(View.VISIBLE);
                            layout1.setVisibility(View.VISIBLE);
                            txtview.setVisibility(View.VISIBLE);
                        }
                        break;
                    }
                    default:
                        break;
                }
                return true;
            }
        });
        ((ViewPager) collection).addView(view, 0);
        return view;
    }

    @Override
    public int getCount() {
        return filesList.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        RelativeLayout view = (RelativeLayout) object;
        TextView txtview = (TextView) view.getChildAt(1);
        if (layout1.isShown()) {
            txtview.setVisibility(View.VISIBLE);
        } else {
            txtview.setVisibility(View.GONE);
        }
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {

        return arg0 == ((View) arg1);
    }

    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
}