package com.semanoor.manahij;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.os.AsyncTask;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveRequest;
import com.google.api.services.drive.DriveRequestInitializer;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by karthik on 04-08-2016.
 */
public class NooorCloud {
    private Activity context;
    private AccountManager accountManager;
    private Account mAccount;
    public Drive drive;
    private String fromEmailId;
    private static NooorCloud instance;
    private ArrayList<com.google.api.services.drive.model.File> categoryList = new ArrayList<>();
    private ArrayList<HashMap<Integer, Object>> categoryFilesMap = new ArrayList<>();
    private ArrayList<HashMap<Integer,Object>> prevDriveContent = new ArrayList<>();


    public static synchronized NooorCloud getInstance() {
        if (instance == null) {
            instance = new NooorCloud();
        }
        return instance;
    }

    public void Authorize() {
        accountManager = AccountManager.get(context);
        Account[] account = accountManager.getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        if (account != null && account.length == 0) {
            UserFunctions.alert("Authorization Failed", context);

            return;
        }
        for (int i = 0; i < account.length; i++) {
            if (account[i].name.equals(fromEmailId)) {
                mAccount = account[i];
                String name = mAccount.name;
                //accountManager.getAuthToken(mAccount, "oauth2:https://www.googleapis.com/auth/drive", new Bundle(), true, new OnTokenAcquired(), null);
                HttpTransport httpTransport = new NetHttpTransport();
                JacksonFactory jsonFactory = new JacksonFactory();
                GoogleAccountCredential credential =
                        GoogleAccountCredential.usingOAuth2(context, DriveScopes.DRIVE);
                credential.setSelectedAccountName(mAccount.name);
                // Trying to get a token right away to see if we are authorized
                //credential.getToken();
                Drive.Builder b = new Drive.Builder(httpTransport, jsonFactory, credential);
                b.setDriveRequestInitializer(new DriveRequestInitializer() {
                    @Override
                    protected void initializeDriveRequest(DriveRequest<?> driveRequest) throws IOException {

                    }
                });
                drive = b.build();
                break;
            }
        }
    }




//    public void getFilesFromGdrive() {
//        if (getGoogleClient().isConnected()) {
//            final SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
//            String id = pref.getString(Globals.Exist_Folder_Id,"");
//            DriveId folderID = DriveId.decodeFromString(id);
//            DriveFolder driveFolder = com.google.android.gms.drive.Drive.DriveApi.getFolder(googleApiClient, folderID);
//            driveFolder.listChildren(googleApiClient).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
//                @Override
//                public void onResult(@NonNull DriveApi.MetadataBufferResult result) {
//                    if (!result.getStatus().isSuccess()) {
//                        return;
//                    }
//                    int count = result.getMetadataBuffer().getCount();
//                    ArrayList<Metadata> list = new ArrayList<Metadata>();
//                    for (Metadata data:result.getMetadataBuffer()){
//                        if (data.isFolder()) {
//                            list.add(data);
//                        }
//                    }
//                    setCategoryList(list);
//                    ((CloudActivity)context).gridView.setAdapter(((CloudActivity)context).adapter);
//                }
//            });
//        }
//    }



    public Activity getContext() {
        return context;
    }

    public void setContext(Activity context) {
        this.context = context;
    }

    public String getFromEmailId() {
        return fromEmailId;
    }

    public void setFromEmailId(String fromEmailId) {
        this.fromEmailId = fromEmailId;
    }

//    public void saveFileToDrive(final File file,final String mimeType,final DriveId driveId){
//        if (googleApiClient!=null) {
//            com.google.android.gms.drive.Drive.DriveApi.newDriveContents(googleApiClient)
//                    .setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
//                        @Override
//                        public void onResult(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
//                            if (!driveContentsResult.getStatus().isSuccess()) {
//                                return;
//                            }
//                            OutputStream outputStream = driveContentsResult.getDriveContents()
//                                    .getOutputStream();
//                            FileInputStream fis;
//                            try {
//                                fis = new FileInputStream(file.getPath());
//                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                                byte[] buf = new byte[1024];
//                                int n;
//                                while (-1 != (n = fis.read(buf))) baos.write(buf, 0, n);
//                                byte[] photoBytes = baos.toByteArray();
//                                outputStream.write(photoBytes);
//                                outputStream.close();
//                                outputStream = null;
//                                fis.close();
//                                fis = null;
//                            } catch (FileNotFoundException e) {
//                                e.printStackTrace();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                            String title = file.getName();
//                            MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
//                                    .setTitle(title)
//                                    .setMimeType(mimeType)
//                                    .build();
//                            com.google.android.gms.drive.Drive.DriveApi.getFolder(googleApiClient, driveId)
//                                    .createFile(googleApiClient, metadataChangeSet, driveContentsResult.getDriveContents())
//                                    .setResultCallback(new ResultCallback<DriveFolder.DriveFileResult>() {
//                                        @Override
//                                        public void onResult(@NonNull DriveFolder.DriveFileResult driveFileResult) {
//                                            if (!driveFileResult.getStatus().isSuccess()){
//                                                return;
//                                            }
//                                            if (mimeType.contains("image")){
//                                                DriveFile driveFile = driveFileResult.getDriveFile();
//                                                if (context instanceof CloudActivity){
//                                                    ((CloudActivity)context).saveImage(driveFile,null);
//                                                    ((CloudActivity)context).horizontalAdapter.notifyDataSetChanged();
//                                                }
//                                            }
//                                            Toast.makeText(context,"FileUploaded",Toast.LENGTH_SHORT).show();
//                                            ((CloudActivity)context).horizontalAdapter.notifyDataSetChanged();
//                                            ((CloudActivity)context).gridView.invalidateViews();
//                                        }
//                                    });
//                        }
//                    });
//        }
//    }

    public ArrayList<com.google.api.services.drive.model.File> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<com.google.api.services.drive.model.File> categoryList) {
        this.categoryList = categoryList;
    }

    public ArrayList<HashMap<Integer,Object>> getCategoryFilesMap() {
        return categoryFilesMap;
    }

    public void setCategoryFilesMap(ArrayList<HashMap<Integer, Object>> categoryFilesMap) {
        this.categoryFilesMap = categoryFilesMap;
    }

    public ArrayList<HashMap<Integer, Object>> getPrevDriveContent() {
        return prevDriveContent;
    }

    public void setPrevDriveContent(ArrayList<HashMap<Integer, Object>> prevDriveContent) {
        this.prevDriveContent = prevDriveContent;
    }
    public void createAppFolder1(String name){
        com.google.api.services.drive.model.File fileMetaData = new com.google.api.services.drive.model.File();
        fileMetaData.setTitle(name);
        fileMetaData.setMimeType("application/vnd.google-apps.folder");
        try {
            com.google.api.services.drive.model.File file = drive.files().insert(fileMetaData)
                    .setFields("id")
                    .execute();
            String[] str = {"Cloud_files","Enrichments_resources","Shared_books"};
            for (int i=0;i<str.length;i++){
                createSubFolder1(file.getId(),str[i]);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void createSubFolder1(String id,String name){
        com.google.api.services.drive.model.File file = new com.google.api.services.drive.model.File();
        file.setTitle(name);
        if (id!=null && id.length()>0){
            file.setParents(Arrays.asList(new ParentReference().setId(id)));
        }
        try {
            com.google.api.services.drive.model.File driveFile = drive.files().insert(file)
                    .setFields("id, parents")
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public class createFolderTask extends AsyncTask<Void,Void,String>{
        String title;
        String ID;

        public createFolderTask(String name, String id){
            this.title = name;
            this.ID = id;
        }

        @Override
        protected String doInBackground(Void... params) {
          //  createFolder(title,ID);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            ((CloudActivity)context).gridView.invalidateViews();

        }
    }
    public ArrayList<com.google.api.services.drive.model.File> listChildren(Drive drive,String folderID) throws IOException {
       ArrayList<com.google.api.services.drive.model.File> filesList = new ArrayList<>();
        com.google.api.services.drive.Drive.Files f1 = drive.files();
        com.google.api.services.drive.Drive.Files.List request = null;
        do {
            try {
                request = f1.list();
                request.setQ("trashed=false and '"+folderID+"' in parents");
                FileList fileList = request.execute();
                filesList.addAll(fileList.getItems());

            } catch (IOException e) {
                e.printStackTrace();
                if (request != null) {
                    request.setPageToken(null);
                }
            }
        } while (request.getPageToken() != null && request.getPageToken().length() > 0);
        return filesList;

    }
}
