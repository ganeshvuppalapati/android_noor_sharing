package com.semanoor.manahij;

import android.content.Context;

import com.semanoor.source_sboookauthor.Book;

/**
 * Created by Krishna on 18-12-2015.
 */
public class PageNumbers {
        private int pageNumber;
        private boolean isChecked;
        private int bookId;
        private String storeId;
        private Context context;
        private Book book;
        private String htmlPath;

    public PageNumbers(Context _context) {
        this.context = _context;
    }
        /**
         * @return the searchValues
         */
        public int getpageNumber() {
            return pageNumber;
        }

        /**
         * @param pagenumber the searchValues to set
         */
        public void setpageNumber(int pagenumber) {
            this.pageNumber = pagenumber;
        }

        /**
         * @return the isChecked
         */
        public boolean isChecked() {
            return isChecked;
        }

        /**
         * @param isChecked the isChecked to set
         */
        public void setChecked(boolean isChecked) {
            this.isChecked = isChecked;
        }

    public int getBookID() {
        return bookId;
    }

    public void setBookID(int bookId) {
        this.bookId = bookId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String getHtmlPath() {
        return htmlPath;
    }

    public void setHtmlPath(String htmlPath) {
        this.htmlPath = htmlPath;
    }
}
