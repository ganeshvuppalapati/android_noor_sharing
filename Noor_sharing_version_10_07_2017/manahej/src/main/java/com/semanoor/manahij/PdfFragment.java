package com.semanoor.manahij;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.artifex.mupdfdemo.FilePicker;
import com.artifex.mupdfdemo.MuPDFCore;
import com.artifex.mupdfdemo.MuPDFPageAdapter;
import com.artifex.mupdfdemo.MuPDFReaderView;
import com.artifex.mupdfdemo.SearchCallBack;
import com.artifex.mupdfdemo.SearchTask;
import com.artifex.mupdfdemo.SearchTaskResult;
import com.artifex.mupdfdemo.Selectionrectangle;

public class PdfFragment extends Fragment implements FilePicker.FilePickerSupport {
    private RelativeLayout mainLayout;
    public MuPDFCore core;
    public MuPDFReaderView mDocView;
    private Context mContext;
    private String mFilePath, mFileLoaction;
    Bundle args = new Bundle();
    private static final String TAG = "PdfFragment";
    public SearchTask mSearchTask;
    int atFirstLoad = 0;

    public PdfFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getActivity();
        args = this.getArguments();
        mFilePath = args.getString("filename");
        mFileLoaction = args.getString("filepath");
        View rootView = inflater.inflate(R.layout.pdf, container, false);
        mainLayout = (RelativeLayout) rootView.findViewById(R.id.pdflayout);

        core = openFile(Uri.decode(mFilePath));


        if (core != null && core.countPages() == 0) {
            core = null;
        }
        if (core == null || core.countPages() == 0 || core.countPages() == -1) {
            Log.e(TAG, "Document Not Opening");
        }
        if (core != null) {
            if (mFilePath != null) {
                core.setBookName(mFilePath.substring(mFilePath.lastIndexOf("/") + 1, mFilePath.length()));
                core.setBookPath(mFileLoaction);
            }
            mDocView = new MuPDFReaderView(mContext, (Selectionrectangle) mContext, core) {
                @Override
                protected void onMoveToChild(int i) {
                    if (core == null)
                        return;
                    super.onMoveToChild(i);
                    mDocView.showSelectionHandles(false);
                    ((pdfActivity) mContext).onPageChanged(i + 1, core.countPages());
                }


            };
            //mDocView.setMode(MuPDFReaderView.Mode.Selecting);
            ((pdfActivity) mContext).onPageChanged(1, core.countPages());


            mDocView.setAdapter(new MuPDFPageAdapter(mContext,(Selectionrectangle)mContext, this,core));
            mainLayout.addView(mDocView);
            mDocView.setupHandles(mainLayout);
            mDocView.showSelectionHandles(false);
            ((pdfActivity) mContext).setSeekbar(core.countPages(),core.mDirection);
            if(atFirstLoad==0){

                atFirstLoad=1;
                try {


                    mDocView.moveHandlesToCornersatFirst();
                    mDocView. showSelectionHandles(true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mDocView. showSelectionHandles(false);
                            // mDocView.deselectText();
                        }
                    }, 1);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }

                mDocView.moveHandlesToCornersatFirst();
            }
        }


        mSearchTask = new SearchTask(mContext, core) {

            @Override
            public void onTextFound(SearchTaskResult result) {

                SearchTaskResult.set(result);
                mDocView.setDisplayedViewIndex(result.pageNumber);
                mDocView.resetupChildren();
            }
        };

        return rootView;
    }

    public void search(int direction, String text) {
        int displayPage = mDocView.getDisplayedViewIndex();
        SearchTaskResult r = SearchTaskResult.get();
        int searchPage = r != null ? r.pageNumber : -1;
        mSearchTask.go(text, direction, displayPage, searchPage);
    }


//    private MuPDFCore openBuffer(byte[] buffer) {
//        System.out.println("Trying to open byte buffer");
//        try {
//            core = new MuPDFCore(mContext, buffer);
//
//        } catch (Exception e) {
//            Log.e(TAG, e.getMessage());
//            return null;
//        }
//        return core;
//    }

    private MuPDFCore openFile(String path) {
       /* int lastSlashPos = path.lastIndexOf('/');
        mFilePath = new String(lastSlashPos == -1
                ? path
                : path.substring(lastSlashPos + 1));*/
        try {
            if (path != null && path.length() > 0)
                core = new MuPDFCore(mContext, path, (SearchCallBack) mContext);
            // New file: drop the old outline data
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
        return core;
    }

    public void onDestroy() {
        if (core != null)
            core.onDestroy();
        core = null;
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mSearchTask != null)
            mSearchTask.stop();
    }

    @Override
    public void performPickFor(FilePicker picker) {
       /* mFilePicker = picker;
        Intent intent = new Intent(this, ChoosePDFActivity.class);
        intent.setAction(ChoosePDFActivity.PICK_KEY_FILE);
        startActivityForResult(intent, FILEPICK_REQUEST);*/
    }

    @Override
    public void setSelectedMode(MotionEvent event) {

    }

    @Override
    public void clearSelection() {

    }


}


