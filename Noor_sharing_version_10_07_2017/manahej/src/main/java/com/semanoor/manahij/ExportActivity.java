package com.semanoor.manahij;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.inappbilling.util.IabResult;
import com.semanoor.inappbilling.util.Inventory;
import com.semanoor.inappbilling.util.Purchase;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.BookStatusListAdapter;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.ExportEpubPdfListAdapter;
import com.semanoor.source_sboookauthor.ExportToEpub;
import com.semanoor.source_sboookauthor.GenerateHTML;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.SegmentedRadioGroup;
import com.semanoor.source_sboookauthor.UploadSearchListadapter;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;
import com.semanoor.source_sboookauthor.Zip;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExportActivity extends Activity implements OnClickListener {
	private Book currentBook;
	public WebService webService;
	private static final int DATE_PICKER_ID = 1111;
	private int year, month, day;
	Spinner spinnerSecurityQy,spinnerGroups,spinnerCountryName, spinnerUniversityName, spinnerCategoryName, spinnerPriceList;
	int gender_id = 1, directionType_id = 1;
	private EditText et_login_username,et_login_password;
	private EditText et_dob,et_username,et_password,et_country,et_state,et_city,et_address,et_emailid,et_full_name,et_academylevel,et_pincode,et_phone,et_answer,et_verifyPassword,et_startPage,et_endingPage;
	private Button btnLogin,btnSignUp,btnUploadBook,btnBookStatus, btnPdf, btnEpub, btnBack;
	private ProgressBar progress_signIn;
	private PopoverView loginPopoverView;
	private TextView tv_searchFeatures, tv_activeDays, bookname, book_author_name;
	private EditText et_description, et_keyword;
	String username, password;
	StringBuilder searchFormat;
	ArrayList<String> universityBasedOnCntryIdList;
	ArrayList<String> countryIdList;
	ArrayList<String> priceList;
	String bookCardImagePath;
	private Dialog uploadBookPopwindow, signUpPopwindow;
	
	private DatabaseHandler db;
	
	private InputStream chunkInputStream;
	private ByteArrayOutputStream chunkBaos;
	private byte[] chunkBytes;
	private int chunkBytesRead;
	private String chunkBookFlag;	
	private int chunkNoOfExecution;
	
	private String bookZipFilePath, bookSourceFilePath;
	private ProgressDialog uploadBookProgressDialog;
	private int categoryId;
	private String countryUniversityId;
	private SharedPreferences sharedPreference;
	public RelativeLayout rootView;
	
	private IabHelper mHelper;
	private static final String ITEM_SKU = "sa001";
	//static final String ITEM_SKU = "android.test.purchased";
	//static final String ITEM_SKU = "android.test.cancelled";
	//static final String ITEM_SKU = "android.test.refunded";
	//static final String ITEM_SKU = "android.test.item_unavailable";
    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfAWdB6FXFoyVXufHBg+JfScBwQg33M3E87MgGSTCCM4uUXN8JX/dxEZ2d5EtcOfNSIcH2t9xXAvBRM8MfxwWaRdnPAgPdUhixDOeSVfeKpE14L9kSwNV3bfg7FB7c/29L54rNRrhgf3ddqr0s+BlX7rOp0KIf1n/KtCo58przpA0elgJ5bBtXYcxYlbbK+2p0Y4+f73Bln1FIOEahFQXXUFVXMN0fDmdiD/9t1ksgOsdJWiM/0Yb8CKDhZEUQAyfKLj0gpYLFyd37fndObyyx4qdDXEP4SDIjCkEggrrSHLXVePZlb8nSpn8UDFx/MIYNUZTSgAeBIpWtBxLdNLOQIDAQAB";

    private String ePubPath, pdfPath;
  //  private UserCredentials userCredentials;
	public String userName;
	public String scopeId;
	public int scopeType;
	public String email;

    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title Bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_export);
		Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
		ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

		UserFunctions.changeFont(rootView,font);
		db = DatabaseHandler.getInstance(this);
		
		webService = new WebService(ExportActivity.this, Globals.getNewCurriculumWebServiceURL());
		
		currentBook = (Book) getIntent().getSerializableExtra("Book");
		//userCredentials = (UserCredentials) getIntent().getSerializableExtra("UserCredentials");
		
		//Get current date by calender

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		btnLogin = (Button) findViewById(R.id.btn_login);
		btnLogin.setOnClickListener(this);
		btnSignUp = (Button) findViewById(R.id.btn_signup);
		btnSignUp.setOnClickListener(this);
		btnUploadBook = (Button) findViewById(R.id.btn_uploadbook);
		btnUploadBook.setOnClickListener(this);
		//btnUploadBook.setEnabled(false);
		btnBookStatus = (Button) findViewById(R.id.btn_bookstatus);
		btnBookStatus.setOnClickListener(this);
		//btnBookStatus.setEnabled(false);
		btnPdf = (Button) findViewById(R.id.btn_pdf);
		btnPdf.setOnClickListener(this);
		btnEpub = (Button) findViewById(R.id.btn_epub);
		btnEpub.setOnClickListener(this);
		btnBack = (Button) findViewById(R.id.btn_back);
		btnBack.setOnClickListener(this);
		
		rootView = (RelativeLayout)findViewById(R.id.export_rootview);
		
		sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
		
		File fileImgDir = new File(Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR);
		if (!fileImgDir.exists()) {
			fileImgDir.mkdir();
		}
		
		File epubPathDir = new File(Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR+"ePub");
		File pdfPathDir = new File(Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR+"Pdf");
		if (!epubPathDir.exists() || !pdfPathDir.exists()) {
			epubPathDir.mkdirs();
			pdfPathDir.mkdirs();
		}
		
		//ePubPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/SBA_"+currentBook.getBookID()+".epub";
		//pdfPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/SBA_"+currentBook.getBookID()+".pdf";
		ePubPath = epubPathDir.toString()+"/SBA_"+currentBook.getBookID()+".epub";
		pdfPath = pdfPathDir.toString()+"/SBA_"+currentBook.getBookID()+".pdf";


		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		scopeId = prefs.getString(Globals.sUserIdKey, "");
		userName = prefs.getString(Globals.sUserNameKey, "");
		scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
		email = prefs.getString(Globals.sUserEmailIdKey, "");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_login:
			if (!UserFunctions.isInternetExist(this)) {
				UserFunctions.DisplayAlertDialogNotFromStringsXML(this, this.getResources().getString(R.string.check_internet_connectivity), this.getResources().getString(R.string.connection_error));
				return;
			}
			if (!webService.LogInResponse) {
				loginPopoverView = new PopoverView(this, R.layout.login_popup);
				loginPopoverView.setContentSizeForViewInPopover(new Point(Globals.getDeviceIndependentPixels(370, this), Globals.getDeviceIndependentPixels(310, this)));
				loginPopoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionAny, true);

				et_login_username = (EditText)loginPopoverView.findViewById(R.id.et_username);
				et_login_password= (EditText)loginPopoverView.findViewById(R.id.et_password);
				progress_signIn = (ProgressBar)loginPopoverView.findViewById(R.id.progressBar_signIn);
				Button btnSignIn = (Button)loginPopoverView.findViewById(R.id.btn_signin);
				btnSignIn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (et_login_username.getText().toString().isEmpty() || et_login_password.getText().toString().isEmpty()) {
							UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.forget_username_password, R.string.login_details);
						} else {
							if (UserFunctions.isInternetExist(ExportActivity.this)) {
								new LogIn().execute();
							} else {
								UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportActivity.this, ExportActivity.this.getResources().getString(R.string.check_internet_connectivity), ExportActivity.this.getResources().getString(R.string.connection_error));
							}
						}
					}
				});
			} else {
				setUpLogoutProperties();
			}
			break;

		case R.id.btn_signup:
			if (!UserFunctions.isInternetExist(this)) {
				UserFunctions.DisplayAlertDialogNotFromStringsXML(this, this.getResources().getString(R.string.check_internet_connectivity), this.getResources().getString(R.string.connection_error));
				return;
			}
			new signUpSoapCall().execute();
			break;

		case R.id.btn_uploadbook:
			if (scopeType != 0) {
				UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.please_login_download_publish, R.string.please_login);
			} else {
				if (!UserFunctions.isInternetExist(this)) {
					UserFunctions.DisplayAlertDialogNotFromStringsXML(this, this.getResources().getString(R.string.check_internet_connectivity), this.getResources().getString(R.string.connection_error));
					return;
				}
				new uploadBookSoapCall().execute();
			}
			break;

		case R.id.btn_bookstatus:
			if (scopeType != 0) {
				UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.please_login_download_publish, R.string.please_login);
			} else {
				if (!UserFunctions.isInternetExist(this)) {
					UserFunctions.DisplayAlertDialogNotFromStringsXML(this, this.getResources().getString(R.string.check_internet_connectivity), this.getResources().getString(R.string.connection_error));
					return;
				}
				new bookStatusSoapCall().execute();
			}
			break;
			
		case R.id.btn_pdf:
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
				boolean isPdfPurchased = sharedPreference.getBoolean(Globals.inAppKey, false);
				//isPdfPurchased = true;
				if (isPdfPurchased) {
					showPopOverToSaveOrOpenFile(v);
				} else {
					startPurchase();
				}
            } else {
            	UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.supports_above_kitkat_version, R.string.not_supported);
            }
			break;
			
		case R.id.btn_epub:
			boolean isEpubPurchased = sharedPreference.getBoolean(Globals.inAppKey, false);
			//isEpubPurchased = true;
			if (isEpubPurchased) {
				showPopOverToSaveOrOpenFile(v);
			} else {
				startPurchase();
			}
			break;
			
		case R.id.btn_back:
			finish();
			break;
			
		default:
			break;
		}
	}
	
	/**
	 * Create ePub File
	 * @author Suriya
	 *
	 */
	private class createEpubFile extends AsyncTask<Void, Void, Void>{
		
		ProgressDialog progressDialog;
		String ePubPath;
		boolean isOpenFile;
		
		public createEpubFile(String _ePubPath, boolean isOpenFile) {
			this.ePubPath = _ePubPath;
			this.isOpenFile = isOpenFile;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (!new File(ePubPath).exists()) {
				String str = getResources().getString(R.string.create_epub);
				progressDialog = ProgressDialog.show(ExportActivity.this, "", str, true);
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			if (!new File(ePubPath).exists()) {
				ExportToEpub exportEpub = new ExportToEpub(ExportActivity.this, currentBook, db, ePubPath, rootView);
				exportEpub.copyDefaultEpubTemplatetoFilesDir();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (progressDialog != null) {
				String msg = "File saved successfully to "+ePubPath;
		    	Toast.makeText(ExportActivity.this, msg, Toast.LENGTH_LONG).show();
				progressDialog.dismiss();
			}
			if (isOpenFile) {
				openEpubFile(ePubPath);
			} else {
				saveFile(ePubPath);
			}
		}
		
	}
	
	/*
	 * this lets to create pdf file
	 */
	public class createPdfFile extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog progressDialog;
		String pdfPath;
		boolean isOpenFile;
		
		public createPdfFile(String _pdfPath, boolean isOpenFile) {
			this.pdfPath = _pdfPath;
			this.isOpenFile = isOpenFile;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (!new File(pdfPath).exists()) {
				String str = getResources().getString(R.string.create_pdf);
				progressDialog = ProgressDialog.show(ExportActivity.this, "", str, true);
			}
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			if (!new File(pdfPath).exists()) {
				//ExportToPdf exportPdf = new ExportToPdf(ExportActivity.this, currentBook, db, pdfPath, this);
				//exportPdf.callToGeneratePDF();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (new File(pdfPath).exists()) {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				if (isOpenFile) {
					openPdfFile(pdfPath);
				} else {
					saveFile(pdfPath);
				}
			}
		}
		
		public void onFinishCreatePdf() {
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
			if (isOpenFile) {
				openPdfFile(pdfPath);
			} else {
				saveFile(pdfPath);
			}
		}
		
	}
	
	public void showPopOverToSaveOrOpenFile(final View v) {
		final PopoverView popoverView = new PopoverView(ExportActivity.this, R.layout.list_view);
		popoverView.setContentSizeForViewInPopover(new Point((int)getResources().getDimension(R.dimen.export_epub_pdf_popover_width), (int)getResources().getDimension(R.dimen.export_epub_pdf_popover_height)));
		popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionUp, true);
		
		ListView listView = (ListView) popoverView.findViewById(R.id.listView1);
		listView.setAdapter(new ExportEpubPdfListAdapter(ExportActivity.this, Globals.listArray));
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int position, long arg3) {
				boolean isOpenFile = false;
				if (position == 0) { //Open File
					isOpenFile = true;
				} else if (position == 1) { //SaveFile
					isOpenFile = false;
				}
				if (v.getId() == R.id.btn_pdf) {
					new createPdfFile(pdfPath, isOpenFile).execute();
				} else {
					new createEpubFile(ePubPath, isOpenFile).execute();
				}
				popoverView.dissmissPopover(true);
			}
		});
	}
	
	public void saveFile(String ePubPath) {
		Intent intent = new Intent(ExportActivity.this, DirectoryPickerActivity.class);
		intent.putExtra(DirectoryPickerActivity.SOURCE_PATH, ePubPath);
		startActivityForResult(intent, DirectoryPickerActivity.PICK_DIRECTORY);
	}
	
	public void openEpubFile(String ePubPath){
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(ePubPath)), "application/epub");
			startActivity(intent);
		} catch (Exception e) {
			e.getMessage();
			UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.no_application_found_to_open_file, R.string.no_application_found);
		}
	}
	
	public void openPdfFile(String pdfPath){
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(pdfPath)), "application/pdf");
			startActivity(intent);
		} catch (Exception e) {
			e.getMessage();
			UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.no_application_found_to_open_file, R.string.no_application_found);
		}
	}
	
	/**
	 * Initiate the purchase
	 */
	private void startPurchase(){
		mHelper = new IabHelper(this, base64EncodedPublicKey);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			
			@Override
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					////System.out.println("In-app Billing setup failed: " + result);
					String str = getResources().getString(R.string.in_app_setup_failed);
					UserFunctions.DisplayAlertDialog(ExportActivity.this, str + result, R.string.billing_setup_failed);
				} else {
					////System.out.println("In-app Billing setup isOK");
					mHelper.launchPurchaseFlow(ExportActivity.this, ITEM_SKU, 10001, mPurchaseFinishedListener, "purchasetoken");
				}
			}
		});
	}
	
	/**
	 * Update Inapp Purchase in the shared preference
	 */
	private void updateInAppPurchaseState(){
		Editor editor = sharedPreference.edit();
		editor.putBoolean(Globals.inAppKey, true);
		editor.commit();
	}
	
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		
		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			if (result.isFailure()) {
				String message = result.getMessage();
				int response = result.getResponse();
				//System.out.println(result+":"+response);
				if (response == 7) {
					updateInAppPurchaseState();
					UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.item_already_owned, R.string.restoring_purchase);
				} else {
					String str = getResources().getString(R.string.in_app_purchase_failed_);
					UserFunctions.DisplayAlertDialog(ExportActivity.this, str +result, R.string.purchase_failed);
				}
				return;
			} else if (purchase.getSku().equals(ITEM_SKU)) {
				consumeItem();
			}
		}
	};
	
	public void consumeItem() {
		mHelper.queryInventoryAsync(mReceivedInventoryListener);
	}
	
	IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {
			if (result.isFailure()) {
				//System.out.println("Failed");
				UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportActivity.this, result.getMessage(), "");
			} else {
				mHelper.consumeAsync(inv.getPurchase(ITEM_SKU), mConsumeFinishedListener);
			}
		}
	};
	
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		
		@Override
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (result.isSuccess()) {
				//System.out.println("Success");
				updateInAppPurchaseState();
				UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.purchase_completed_successfully, R.string.purchased);
			} else {
				//System.out.println("Failed");
				UserFunctions.DisplayAlertDialog(ExportActivity.this, result.getMessage(), R.string.purchase_failed);
			}
		}
	};
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == DirectoryPickerActivity.PICK_DIRECTORY && resultCode == RESULT_OK) {
			Bundle extras = data.getExtras();
			String fileSourcePath = extras.getString(DirectoryPickerActivity.SOURCE_PATH);
			String fileDestpath = extras.getString(DirectoryPickerActivity.CHOSEN_DIRECTORY)+"/"+new File(fileSourcePath).getName();
			
			new saveFileToUserDefinedPath(fileSourcePath, fileDestpath).execute();
		} else {
			if (mHelper != null) {
				if (!mHelper.handleActivityResult(requestCode, 
						resultCode, data)) {
					//System.out.println("OnActivityResult");
					super.onActivityResult(requestCode, resultCode, data);
				}
			}
		}
	}
	
	/**
	 * this lets to save files to user defined path
	 * @author Suriya
	 *
	 */
	private class saveFileToUserDefinedPath extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog progressDialog;
		String fileSourcePath, fileDestpath;
		boolean savedSuccessfully;

		public saveFileToUserDefinedPath(String fileSourcePath, String fileDestpath) {
			this.fileSourcePath = fileSourcePath;
			this.fileDestpath = fileDestpath;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String str = getResources().getString(R.string.saving_file_please_wait);
			progressDialog = ProgressDialog.show(ExportActivity.this, "", str, true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			savedSuccessfully = UserFunctions.copyFiles(fileSourcePath, fileDestpath);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if (savedSuccessfully) {
				String msg = "File saved successfully to "+fileDestpath;
	        	Toast.makeText(ExportActivity.this, msg, Toast.LENGTH_LONG).show();
			} else {
				String msg = "Failed to save file to "+fileDestpath;
	        	Toast.makeText(ExportActivity.this, msg, Toast.LENGTH_LONG).show();
			}
		}
		
	}
	
	/**
	 * Soap Call for book status
	 * @author Suriya
	 *
	 */
	private class bookStatusSoapCall extends AsyncTask<Void, Void, Void>{

		Dialog bookStatusDialogView;
		ProgressBar progressBar;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			bookStatusDialogView = new Dialog(ExportActivity.this);
			bookStatusDialogView.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
			bookStatusDialogView.setTitle(R.string.book_status);
			bookStatusDialogView.setContentView(ExportActivity.this.getLayoutInflater().inflate(R.layout.font_list_view, null));
			bookStatusDialogView.getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			progressBar = (ProgressBar) bookStatusDialogView.findViewById(R.id.progressBar1);
			progressBar.setVisibility(View.VISIBLE);
			bookStatusDialogView.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			webService.loadBookStatus(username, password);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			ListView listView = (ListView) bookStatusDialogView.findViewById(R.id.listView1);
			listView.setAdapter(new BookStatusListAdapter(ExportActivity.this, webService.bookStatusList));
			progressBar.setVisibility(View.GONE);
		}
	}
	
	/**
	 * Soap call for Uploading the book
	 * @author Suriya
	 *
	 */
	private class uploadBookSoapCall extends AsyncTask<Void, Void, Void>{

		ArrayList<String> countryList = new ArrayList<String>();
		ArrayList<String> universityList;
		String[] searchFeatures = {"SelectAll", "Search", "BookMark", "Copy", "Flip", "Next Previous", "Highlight", "Note", "GoTo Page", "Index", "Zoom", "Web search", "Wiki search",
				"Book search", "Nooor search", "Noor Book search", "Youtube search", "Google search", "Multimedia search", "Plain text search", "Dictionary search", "Transition search", 
				"Jazira", "Bing", "Ask", "Yahoo"};
		ArrayList<Object> searchFeaturesObjectList = new ArrayList<Object>();
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			countryIdList = new ArrayList<String>();
			uploadBookPopwindow = new Dialog(ExportActivity.this);
			uploadBookPopwindow.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
			uploadBookPopwindow.setTitle(R.string.upload_book);
			uploadBookPopwindow.setContentView(ExportActivity.this.getLayoutInflater().inflate(R.layout.upload_book, null));
			uploadBookPopwindow.getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			ImageView image_book = (ImageView)uploadBookPopwindow.findViewById(R.id.img_book);
			image_book.setScaleType(ScaleType.FIT_XY);
			bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/card.png";
			Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
			image_book.setImageBitmap(bitmap);
			bookname=  (TextView)uploadBookPopwindow.findViewById(R.id.txt_book_name);
			bookname.setText(currentBook.getBookTitle());
			book_author_name=  (TextView)uploadBookPopwindow.findViewById(R.id.txt_book_author_name);
			book_author_name.setText(currentBook.getBookAuthorName());
			tv_activeDays = (TextView) uploadBookPopwindow.findViewById(R.id.txt_activedays_365);
			et_description = (EditText) uploadBookPopwindow.findViewById(R.id.et_description);
			et_keyword = (EditText) uploadBookPopwindow.findViewById(R.id.et_keyword);
			et_startPage = (EditText) uploadBookPopwindow.findViewById(R.id.et_startingPage);
			et_endingPage = (EditText) uploadBookPopwindow.findViewById(R.id.et_endingPage);
			et_endingPage.setText(String.valueOf(currentBook.getTotalPages()));
			SegmentedRadioGroup directionType = (SegmentedRadioGroup)uploadBookPopwindow.findViewById(R.id.btn_segment_group);
			directionType.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub
					if(checkedId==R.id.btn_segment_ltr){
						directionType_id=1;
					}else if(checkedId==R.id.btn_segment_rtl){
						directionType_id=2;
					}
				}
			});
			spinnerCountryName = (Spinner) uploadBookPopwindow.findViewById(R.id.spinner_country);
			spinnerUniversityName = (Spinner) uploadBookPopwindow.findViewById(R.id.spinner_university);
			spinnerCategoryName = (Spinner) uploadBookPopwindow.findViewById(R.id.spinner_category);
			spinnerPriceList = (Spinner) uploadBookPopwindow.findViewById(R.id.spinner_price);
			spinnerCountryName.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View view,
						int position, long arg3) {
					universityList = getUniversityListBasedOnCountrySelected(webService.universityHashMapList, webService.KEY_UNIVERSITY_EN_NAME, webService.KEY_UNIVERSITY_COUNTRY_ID, countryIdList.get(position));
					ArrayAdapter<String> spinnerUniversityAdapter = new ArrayAdapter<String>(ExportActivity.this, android.R.layout.simple_spinner_dropdown_item, universityList);
					spinnerUniversityName.setAdapter(spinnerUniversityAdapter);
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					
				}
			});
			tv_searchFeatures = (TextView) uploadBookPopwindow.findViewById(R.id.tv_searchfeatures);
			tv_searchFeatures.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Dialog searchFeaturePopwindow = new Dialog(ExportActivity.this);
					searchFeaturePopwindow.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
					searchFeaturePopwindow.setTitle("Select Search");
					searchFeaturePopwindow.setContentView(ExportActivity.this.getLayoutInflater().inflate(R.layout.font_list_view, null));
					searchFeaturePopwindow.getWindow().setLayout(LayoutParams.WRAP_CONTENT, (int)(Globals.getDeviceHeight()/2.0));
					if (searchFeaturesObjectList.isEmpty()) {
						for (int i = 0; i < searchFeatures.length; i++) {
							SearchFeatues search = new SearchFeatues();
							search.setSearchValues(searchFeatures[i]);
							search.setChecked(false);
							searchFeaturesObjectList.add(search);
						}
					}
					final ListView listView = (ListView) searchFeaturePopwindow.findViewById(R.id.listView1);
					final UploadSearchListadapter searchArrayAdapter = new UploadSearchListadapter(ExportActivity.this, searchFeaturesObjectList);
					listView.setAdapter(searchArrayAdapter);
					listView.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> adapterView, View view,
								int position, long arg3) {
							SearchFeatues searchSelectAll = (SearchFeatues) searchFeaturesObjectList.get(0);
							if (position == 0) {
								for (int i = 1; i < searchFeaturesObjectList.size(); i++) {
									SearchFeatues search = (SearchFeatues) searchFeaturesObjectList.get(i);
									if (searchSelectAll.isChecked) {
										search.setChecked(false);
									} else {
										search.setChecked(true);
									}
								}
								if (searchSelectAll.isChecked) {
									searchSelectAll.setChecked(false);
								} else {
									searchSelectAll.setChecked(true);
								}
								listView.invalidateViews();
							} else {
								SearchFeatues searchOtherItems = (SearchFeatues) searchFeaturesObjectList.get(position);
								boolean isAllCheckBoxSelected = false;
								if (searchOtherItems.isChecked) {
									searchOtherItems.setChecked(false);
									if (searchSelectAll.isChecked) {
										searchSelectAll.setChecked(false);
									}
								} else {
									searchOtherItems.setChecked(true);
									for (int i = 1; i < searchFeaturesObjectList.size(); i++) {
										SearchFeatues search = (SearchFeatues) searchFeaturesObjectList.get(i);
										if (search.isChecked) {
											isAllCheckBoxSelected = true;
										} else {
											isAllCheckBoxSelected = false;
											break;
										}
									}
									if (isAllCheckBoxSelected) {
										searchSelectAll.setChecked(true);
									}
								}
								listView.invalidateViews();
							}
						}
					});
					searchFeaturePopwindow.show();
					searchFeaturePopwindow.setOnDismissListener(new OnDismissListener() {

						@Override
						public void onDismiss(DialogInterface dialog) {
							StringBuilder strBuilder = new StringBuilder();
							searchFormat = new StringBuilder();
							for (int i = 1; i < searchFeaturesObjectList.size(); i++) {
								SearchFeatues search = (SearchFeatues) searchFeaturesObjectList.get(i);
								if (search.isChecked) {
									if (strBuilder.toString() == "") {
										strBuilder.append(search.getSearchValues());
									} else {
										strBuilder.append(", "+search.getSearchValues());
									}
									if (i==1) {
										searchFormat.append("1");
									} else {
										searchFormat.append(",1");
									}
								} else {
									if (i==1) {
										searchFormat.append("0");
									} else {
										searchFormat.append(",0");
									}
								}
							}
							if (strBuilder.toString() == "") {
								tv_searchFeatures.setText("Choose search features");
							} else {
								tv_searchFeatures.setText(strBuilder.toString());
							}
						}
					});
				}
			});
			Button btnPublish = (Button) uploadBookPopwindow.findViewById(R.id.btn_publish);
			btnPublish.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					validateAndUploadBook();
				}
			});
			uploadBookPopwindow.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			if (webService.countryHashMapList == null || webService.universityHashMapList == null || webService.categoryHashMapList == null || webService.priceList == null) {
				webService.loadCountryList();
				webService.loadUniversityList();
				webService.loadCategoryList();
				//webService.loadPriceList();
				priceList = loadPriceList();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			loadCountryNameAndIdList(webService.countryHashMapList, webService.KEY_COUNTRY_EN_NAME, webService.KEY_COUNTRY_ID);
			ArrayAdapter<String> spinnerCountryAdapter = new ArrayAdapter<String>(ExportActivity.this, android.R.layout.simple_spinner_dropdown_item, countryList);
			spinnerCountryName.setAdapter(spinnerCountryAdapter);
			
			ArrayList<String> categoryList = getValuesFromHashMapArrayList(webService.categoryHashMapList, webService.KEY_CATEGORY_EN_NAME);
			ArrayAdapter<String> spinnerCategoryAdapter = new ArrayAdapter<String>(ExportActivity.this, android.R.layout.simple_spinner_dropdown_item, categoryList);
			spinnerCategoryName.setAdapter(spinnerCategoryAdapter);
			ArrayAdapter<String> spinnerPriceAdapter = new ArrayAdapter<String>(ExportActivity.this, android.R.layout.simple_spinner_dropdown_item, priceList);
			spinnerPriceList.setAdapter(spinnerPriceAdapter);
		}
		
		/**
		 * Load Price List
		 * @return
		 */
		private ArrayList<String> loadPriceList(){
			ArrayList<String> priceListArray = new ArrayList<String>();
			String str = "0.00";
			priceListArray.add(str);
			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(2);

			for (int i=0; i<50; i++) {
				float price = (float) (i + 0.99);
				String priceStr = df.format(price);
				priceListArray.add(priceStr);
			}
			for (int i= 49; i<95; i=i+5) {
				float price =  (float)(i+ 5.99);
				String priceStr = df.format(price);
				priceListArray.add(priceStr);
			}
			for (int i=99; i<240; i=i+10) {
				float price = (float)(i+ 10.99);
				String priceStr = df.format(price);
				priceListArray.add(priceStr);
			}
			for (int i=249; i<450; i=i+50) {
				float price = (float)(i+ 50.99);
				String priceStr = df.format(price);
				priceListArray.add(priceStr);
			}
			for (int i=499; i<900; i=i+100) {
				float price =  (float)(i+ 100.99);
				String priceStr = df.format(price);
				priceListArray.add(priceStr);
			}
			return priceListArray;
		}
		
		/**
		 * Load Country name and Id to arraylist from hash map
		 * @param hashMapList
		 * @param keyCountryName
		 * @param keyCountryId
		 */
		private void loadCountryNameAndIdList(ArrayList<HashMap<String, String>> hashMapList, String keyCountryName, String keyCountryId){
			for (HashMap<String, String> map : hashMapList) {
				String countryName = map.get(keyCountryName);
				String countryId = map.get(keyCountryId);
				countryList.add(countryName);
				countryIdList.add(countryId);
			}
		}
		
		/**
		 * Get University list based on the country selected id
		 * @param hashMapList
		 * @param key
		 * @param keyCountryId
		 * @param countryId
		 * @return
		 */
		private ArrayList<String> getUniversityListBasedOnCountrySelected(ArrayList<HashMap<String, String>> hashMapList, String key, String keyCountryId, String countryId){
			ArrayList<String> arrayList = new ArrayList<String>();
			universityBasedOnCntryIdList = new ArrayList<String>();
			for (HashMap<String, String> map : hashMapList) {
				String idValue = map.get(keyCountryId);
				if (idValue.equals(countryId)) {
					String value = map.get(key);
					arrayList.add(value);
					String unividValue = map.get(webService.KEY_UNIVERSITY_ID);
					universityBasedOnCntryIdList.add(unividValue);
				}
			}
			return arrayList;
		}
	}
	
	/**
	 * Search list class for the upload book
	 * @author Suriya
	 *
	 */
	public class SearchFeatues {
		private String searchValues;
		private boolean isChecked;
		/**
		 * @return the searchValues
		 */
		public String getSearchValues() {
			return searchValues;
		}
		/**
		 * @param searchValues the searchValues to set
		 */
		public void setSearchValues(String searchValues) {
			this.searchValues = searchValues;
		}
		/**
		 * @return the isChecked
		 */
		public boolean isChecked() {
			return isChecked;
		}
		/**
		 * @param isChecked the isChecked to set
		 */
		public void setChecked(boolean isChecked) {
			this.isChecked = isChecked;
		}
	};
	
	/**
	 * getValues from array list hash map
	 * @param hashMapList
	 * @param key
	 * @return
	 */
	private ArrayList<String> getValuesFromHashMapArrayList(ArrayList<HashMap<String, String>> hashMapList, String key){
		ArrayList<String> arrayList = new ArrayList<String>();
		for (HashMap<String, String> map : hashMapList) {
			String value = map.get(key);
			arrayList.add(value);
		}
		return arrayList;
	}
	
	/**
	 * Validate All the fields in upload Book and publish it
	 */
	private void validateAndUploadBook(){
		if(et_description.getText().toString().length()<2 || et_description.getText().toString()==null) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_description, R.string.invalid_description);
		}else if (et_keyword.getText().toString().length()<2 || et_keyword.getText().toString()==null) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_keyword, R.string.invalid_keyword);
		}else if (tv_searchFeatures.getText().toString().equals("Choose search features")) {
			UserFunctions.DisplayAlertDialog(this, R.string.select_valid_search_option, R.string.invalid_search_option);
		}else if (countryIdList.isEmpty()) {
			UserFunctions.DisplayAlertDialog(this, R.string.select_valid_country_name, R.string.invalid_country_name);
		}else if (universityBasedOnCntryIdList.isEmpty()) {
			UserFunctions.DisplayAlertDialog(this, R.string.select_valid_university_name, R.string.invalid_university_name);
		}else if (webService.categoryHashMapList.isEmpty()) {
			UserFunctions.DisplayAlertDialog(this, R.string.select_valid_category_name, R.string.invalid_categort_name);
		}else if (priceList.isEmpty()) {
			UserFunctions.DisplayAlertDialog(this, R.string.select_valid_price, R.string.invalid_price);
		}else if (!UserFunctions.isInternetExist(this)) {
			UserFunctions.DisplayAlertDialogNotFromStringsXML(this, this.getResources().getString(R.string.check_internet_connectivity), this.getResources().getString(R.string.connection_error));
		}else {
			new initPublishBook().execute();
		}
	}
	
	/**
	 * initialise all the values to publish book
	 *
	 */
	private class initPublishBook extends AsyncTask<Void, Void, Void>{

		protected void onPreExecute() {
			uploadBookProgressDialog = ProgressDialog.show(ExportActivity.this, "", "Uploading book. Please wait...", true);
		};


		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			int countryId, universityId;
			int sp_selectedCategoryPos = spinnerCategoryName.getSelectedItemPosition();
			ArrayList<String> categoryIdList = getValuesFromHashMapArrayList(webService.categoryHashMapList, webService.KEY_CATEGORY_ID);
			categoryId =Integer.parseInt(categoryIdList.get(sp_selectedCategoryPos));

			int sp_selectedCountryPos = spinnerCountryName.getSelectedItemPosition();
			//ArrayList<String> countryIdList = getValuesFromHashMapArrayList(webService.countryHashMapList, webService.KEY_COUNTRY_ID);
			countryId = Integer.parseInt(countryIdList.get(sp_selectedCountryPos));

			int sp_selectedUniversityPos = spinnerUniversityName.getSelectedItemPosition();
			universityId = Integer.parseInt(universityBasedOnCntryIdList.get(sp_selectedUniversityPos));

			countryUniversityId = "#"+countryId+"|"+universityId+"#";

			bookSourceFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID();
			bookZipFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+".zip";

			db = DatabaseHandler.getInstance(ExportActivity.this);
			generateHtmlAndCreateBookToZipFile();
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			initValuesToPublishBookAsChunks();
		}
		
		/**
		 * Generate HTML files and create book zip file
		 */
		private void generateHtmlAndCreateBookToZipFile(){
			String destCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/Card.gif";
			UserFunctions.copyFiles(bookCardImagePath, destCardImagePath);
			
			String bookXml = "";
			String fIndexXml = "";
			String searchXml = "";
			String htmlFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/";
			for (int i = 0; i < currentBook.getTotalPages(); i++) {
				new GenerateHTML(ExportActivity.this, db, currentBook, String.valueOf(i+1), null,false).generatePage(htmlFilePath, null);
				bookXml = generateBookXML(i+1, bookXml);
				fIndexXml = generateFIndexXml(i, fIndexXml);
				searchXml = generateSearchXml(i+1, searchXml);
			}
			
			File bookXmlFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/Book.xml") ;
			try {
				FileWriter fileWriter = new FileWriter(bookXmlFilePath);
				fileWriter.write(bookXml);
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			File fIndexXmlFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FIndex.xml");
			try {
				FileWriter fileWriter = new FileWriter(fIndexXmlFilePath);
				fileWriter.write(fIndexXml);
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			File searchXmlFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/Search.xml");
			try {
				FileWriter fileWriter = new FileWriter(searchXmlFilePath);
				fileWriter.write(searchXml);
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			// Copy book files to temporary directory and zip it
			String tempBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+"tempBookFiles/"+currentBook.getBookID()+"/"+currentBook.getBookID();
			String sourcePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID();
			try {
				UserFunctions.copyDirectory(new File(sourcePath), new File(tempBookPath));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String tempBookSourceFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+"tempBookFiles/"+currentBook.getBookID();
			Zip zip = new Zip();
			zip.zipFileAtPath(tempBookSourceFilePath, bookZipFilePath);
			
			//Delete the Temporary Directory
			UserFunctions.DeleteDirectory(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+"tempBookFiles"));
			
			/*String externalpath = Environment.getExternalStorageDirectory().toString();
			String externalZipPath = externalpath+"/SboookAuthor/2.zip";
			UserFunctions.copyFiles(bookZipFilePath, externalZipPath);*/
		}
		
		/**
		 * generateXML for book
		 * @param pageNo
		 * @param bookXml
		 * @return
		 */
		private String generateBookXML(int pageNo, String bookXml){
			String xml;
			if (pageNo == 1) {
				xml = bookXml.concat("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Book><Cover1 Path=\"1.htm\" ETitle=\"Cover 1\" ATitle=\"1 الغلاف\"/>");
			} else if (pageNo == currentBook.getTotalPages()) {
				xml = bookXml.concat("<Cover2 Path=\""+pageNo+".htm\" ETitle=\"Cover 2\" ATitle=\"2 الغلاف\"/></Book>");
			} else {
				int tempPageNo = pageNo - 1;
				xml = bookXml.concat("<P1 Path=\""+pageNo+".htm\" ETitle=\"Page "+tempPageNo+"\" ATitle=\""+tempPageNo+" صفحة\"/>");
			}
			return xml;
		}
		
		/**
		 * generate FIndexXML for book
		 * @param pageNo
		 * @param fIndexXml
		 * @return
		 */
		private String generateFIndexXml(int pageNo, String fIndexXml){
			String xml;
			if (pageNo == 0) {
				xml = fIndexXml.concat("<Index Name=\""+currentBook.getBookTitle()+"\" IndexPg=\"0\" Lang=\"0\">\n<A1 Name=\"Cover Page\" Pg=\"0\" />\n");
			} else if (pageNo == currentBook.getTotalPages()-1) {
				xml = fIndexXml.concat("<A1 Name=\"End Page\" Pg=\""+pageNo+"\" />\n</Index>");
			} else {
				xml = fIndexXml.concat("<A1 Name=\"Page "+pageNo+"\" Pg=\""+pageNo+"\" />\n");
			}
			return xml;
		}
		
		/**
		 * generate Search Xml for book
		 * @param pageNo
		 * @param searchXml
		 * @return
		 */
		private String generateSearchXml(int pageNo, String searchXml){
			String xml;
			File htmlFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+pageNo+".htm");
			String htmlString = UserFunctions.readFileFromPath(htmlFilePath);
			String[] split = htmlString.split("</head>");
			String htmlTextWithoutStyles = "<html>"+split[1];
			String plainText = Html.fromHtml(htmlTextWithoutStyles).toString();
			if (pageNo == 1) {
				xml = searchXml.concat("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<Search Name=\"\"><P1 Data=\""+plainText+"\" Name=\"1\"/>");
			} else if (pageNo == currentBook.getTotalPages()) {
				xml = searchXml.concat("<P"+pageNo+" Data=\""+plainText+"\" Name=\""+pageNo+"\"/>\n</Search>");
			} else {
				xml = searchXml.concat("<P"+pageNo+" Data=\""+plainText+"\" Name=\""+pageNo+"\"/>\n");
			}
			return xml;
		}
		
		/**
		 * Initialize all values to publish book as chunks
		 */
		private void initValuesToPublishBookAsChunks(){
			try {
				chunkInputStream = new FileInputStream(bookZipFilePath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			chunkBaos = new ByteArrayOutputStream();
			chunkBytes = new byte[1024*1024];
			chunkBytesRead = 0;
			chunkNoOfExecution = 1;
			new publishBookAsChunks().execute();
		}
	}
	
	/**
	 * convert the image to base64 binary
	 * @param imgPath
	 * @return
	 */
	private String convertImageToBase64Binary(String imgPath){
		String base64Binary;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Bitmap imgBitmap = BitmapFactory.decodeFile(imgPath);
			imgBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte[] byte_arr = baos.toByteArray();
			base64Binary = Base64.encodeToString(byte_arr, Base64.DEFAULT).trim();
		} 
		catch (OutOfMemoryError e){base64Binary="Out Of Memory !"; }
		catch (IndexOutOfBoundsException e){base64Binary="Index Out Of Bound !"; }
		catch (NullPointerException e){base64Binary="Null Pointer Exception !";}
		catch (RuntimeException e){base64Binary="Runtime Exception !";}
		catch (Exception e){base64Binary="Some Error Occured !";}
		
		return base64Binary;
	}
	
	
	/**
	 * Async task class to publish book as chunks
	 * @author Suriya
	 *
	 */
	private class publishBookAsChunks extends AsyncTask<Void, Void, Void>{

		String bookZipBase64Chunks = null;
		byte[] byteArray = null;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			convertFileToBase64Binary(new File(bookZipFilePath));
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			if (chunkBytesRead > 0) {
				//webService.uploadBookAsChunks(username, bookname.getText().toString(), currentBook.getBookAuthorName(), bookZipBase64Chunks, chunkBookFlag);
				webService.newUploadBookAsChunks(scopeId, currentBook.getBookID()+".zip", bookZipBase64Chunks, chunkBookFlag);
			} else {
				int language = 1; //English = 1, Arabic = 2 //Need to check
				String bookcardImageName = "Card.gif";
				String bookCardImageBase64 = convertImageToBase64Binary(bookCardImagePath);
				String bookZipFileName = currentBook.getBookID()+".zip";
				String bookZipFileBase64 = "";
				String priceValue = spinnerPriceList.getSelectedItem().toString();
				int isBookFree = 1;
				if (Float.parseFloat(priceValue) > 0.00) {
					isBookFree = 0;
				}
				int bookOrientation = currentBook.getBookOrientation();
				String activeDays = tv_activeDays.getText().toString();
				String sAccountCat = "1";
				String currentYear = "1";
				int indexPage = 0;
				int startingPage = Integer.parseInt(et_startPage.getText().toString());
				int endingPage = Integer.parseInt(et_endingPage.getText().toString());
				//webService.uploadBook(username, password, language, "", activeDays, bookname.getText().toString(), book_author_name.getText().toString(), et_description.getText().toString(), directionType_id, bookCardImageBase64, bookcardImageName, bookZipFileBase64, bookZipFileName, priceValue, categoryId, et_keyword.getText().toString(), searchFormat.toString(), countryUniversityId, bookOrientation);
				webService.newUploadBook(scopeId, currentBook.getBookID()+".zip", sAccountCat, activeDays, bookname.getText().toString(), et_description.getText().toString(), isBookFree, currentYear, startingPage, endingPage, countryUniversityId, String.valueOf(categoryId), et_keyword.getText().toString(), indexPage, currentBook.getTotalPages(), language, searchFormat.toString());
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			/*if (chunkBytesRead > 0) {
				if (webService.insertAddNewEbookIpadJunkResult.equals("true")) {
					chunkNoOfExecution++;
					new publishBookAsChunks().execute();
				} else {
					uploadBookProgressDialog.dismiss();
					new File(bookZipFilePath).delete();
					UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.went_wrong, R.string.oops);
				}
			} else {
				uploadBookProgressDialog.dismiss();
				new File(bookZipFilePath).delete();
				if (webService.uploadBookResponse.equals("Book Created Successfully.") || webService.uploadBookResponse.equals("ÿ™ŸÖ ÿ•ŸÜÿ¥ÿßÿ° ÿßŸÑŸÉÿ™ÿßÿ® ÿ®ŸÜÿ¨ÿßÿ≠ ")) {
					UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportActivity.this, webService.uploadBookResponse, "");
					uploadBookPopwindow.dismiss();
				} else {
					UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportActivity.this, webService.uploadBookResponse, "");
				}
			}*/
			
			if (chunkBytesRead > 0) {
				if (webService.uploadJunkFileIpadResult != "") {
					if (webService.uploadJunkFileIpadResult.equals("true")) {
						chunkNoOfExecution++;
						new publishBookAsChunks().execute();
					} else {
						uploadBookProgressDialog.dismiss();
						new File(bookZipFilePath).delete();
						UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.went_wrong, R.string.oops);
					}
				}
			} else {
				uploadBookProgressDialog.dismiss();
				new File(bookZipFilePath).delete();
				/*if (webService.uploadBookResponse.equals("Book Created Successfully.") || webService.uploadBookResponse.equals("ÿ™ŸÖ ÿ•ŸÜÿ¥ÿßÿ° ÿßŸÑŸÉÿ™ÿßÿ® ÿ®ŸÜÿ¨ÿßÿ≠ ")) {
					UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportActivity.this, webService.uploadBookResponse, "");
					uploadBookPopwindow.dismiss();
				} else {
					UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportActivity.this, webService.uploadBookResponse, "");
				}*/
				if (webService.uploadBookResponse != "") {
					int returnValue = Integer.parseInt(webService.uploadBookResponse);
					int value;
					switch (returnValue) {
					case 0:
						value = R.string.book_error_upload;
						break;
					case 1:
						value = R.string.book_add_successful;
						break;
					case 2:
						value = R.string.book_exists;
						break;
					case 3:
						value = R.string.book_not_select_enrichments;
						break;
					case 4:
						value = R.string.book_publish_success;
						break;
					case 5:
						value = R.string.book_problem_publish;
						break;
					case 6:
						value = R.string.book_not_authorize;
						break;
					default:
						value = R.string.book_error_upload;
						break;
					}
					UserFunctions.DisplayAlertDialog(ExportActivity.this, value, R.string.book_publish);
				}
			}
		}
		
		/**
		 * convert file to base64 binary
		 * @param file
		 * @return
		 */
		private void convertFileToBase64Binary(File file) {
			try {
				if ((chunkBytesRead = chunkInputStream.read(chunkBytes)) != -1) {
					////System.out.println("bytesRead: "+chunkBytesRead+"i: "+i);
					chunkBaos.write(chunkBytes, 0, chunkBytesRead);
					byteArray = chunkBaos.toByteArray();
					bookZipBase64Chunks = Base64.encodeToString(byteArray, Base64.DEFAULT).trim();
					if (chunkNoOfExecution == 1) {
						chunkBookFlag = "Start";
					} else if (chunkBytesRead < 1024*1024) {
						chunkBookFlag = "End";
					} else {
						chunkBookFlag = "Middle";
					}
					chunkBaos.reset();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/*private String convertFileToBase64Binary(File file) {
			String base64Binary = null;
			byte[] byteArray = null;
			try {
				InputStream inputStream = new FileInputStream(file);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] b = new byte[1024*1024];
				int bytesRead = 0;
				//int i = 0;
				while ((bytesRead = inputStream.read(b)) != -1) {
					////System.out.println("bytesRead: "+bytesRead+"i: "+i);
					baos.write(b, 0, bytesRead);
					//i++;
				}
				byteArray = baos.toByteArray();
				base64Binary = Base64.encodeToString(byteArray, Base64.DEFAULT).trim();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return base64Binary;
		}*/
		
	}
	
	/**
	 * sign up soap call to show the popup window and load the password question and groups in background
	 * @author Suriya
	 *
	 */
	private class signUpSoapCall extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			signUpPopwindow = new Dialog(ExportActivity.this);
			signUpPopwindow.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
			signUpPopwindow.setTitle(R.string.sign_up);
			signUpPopwindow.setContentView(ExportActivity.this.getLayoutInflater().inflate(R.layout.signup, null));
			//bookModalPopwindow.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.5), (int)(Globals.getDeviceHeight()/1.5));
			signUpPopwindow.getWindow().setLayout(LayoutParams.WRAP_CONTENT, (int)(Globals.getDeviceHeight()/1.5));
			ImageButton imgBtn_dob = (ImageButton) signUpPopwindow.findViewById(R.id.imgBtn_dob);
			imgBtn_dob.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					showDialog(DATE_PICKER_ID);
				}
			});
			spinnerSecurityQy = (Spinner) signUpPopwindow.findViewById(R.id.spinner_securityquestion);
			spinnerGroups = (Spinner) signUpPopwindow.findViewById(R.id.spinner_group);
			et_dob = (EditText) signUpPopwindow.findViewById(R.id.et_dob);
			et_dob.setText(new StringBuilder().append(month + 1).append("/").append(day).append("/").append(year));
			et_username = (EditText) signUpPopwindow.findViewById(R.id.et_username);
			et_password = (EditText) signUpPopwindow.findViewById(R.id.et_password);
			et_verifyPassword = (EditText) signUpPopwindow.findViewById(R.id.et_verify_password);
			et_country = (EditText) signUpPopwindow.findViewById(R.id.et_country);
			et_state = (EditText) signUpPopwindow.findViewById(R.id.et_state);
			et_city = (EditText) signUpPopwindow.findViewById(R.id.et_city);
			et_address = (EditText) signUpPopwindow.findViewById(R.id.et_address);
			et_emailid = (EditText) signUpPopwindow.findViewById(R.id.et_email);
			et_full_name = (EditText) signUpPopwindow.findViewById(R.id.et_fullname);
			et_academylevel = (EditText) signUpPopwindow.findViewById(R.id.et_academic);
			et_pincode = (EditText) signUpPopwindow.findViewById(R.id.et_pincode);
			et_phone = (EditText) signUpPopwindow.findViewById(R.id.et_phone);
			et_answer = (EditText) signUpPopwindow.findViewById(R.id.et_answer);
			SegmentedRadioGroup gender = (SegmentedRadioGroup)signUpPopwindow.findViewById(R.id.btn_segment_group);
			gender.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub
					if(checkedId==R.id.btn_segment_male){
						gender_id=1;
					}else if(checkedId==R.id.btn_segment_female){
						gender_id=2;
					}
				}
			});

			Button signup = (Button) signUpPopwindow.findViewById(R.id.btn_server_signup);
			signup.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					validateSignUpAndRegister();
				}
			});

			signUpPopwindow.show();
		}

		@Override 
		protected Void doInBackground(Void... params) {
			if (webService.questionHashMapList == null || webService.groupHashMapList == null) {
				webService.loadPwdQuestion();
				webService.loadGroups();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			ArrayList<String> questionList = getValuesFromHashMapArrayList(webService.questionHashMapList, webService.KEY_QUESTION_EN_NAME);
			ArrayAdapter<String> spinnerSecurityQyAdapter = new ArrayAdapter<String>(ExportActivity.this, android.R.layout.simple_spinner_dropdown_item, questionList);
			spinnerSecurityQy.setAdapter(spinnerSecurityQyAdapter);
			ArrayList<String> groupList = getValuesFromHashMapArrayList(webService.groupHashMapList, webService.KEY_GROUP_EN_NAME);
			ArrayAdapter<String> spinnerGroupAdapter = new ArrayAdapter<String>(ExportActivity.this, android.R.layout.simple_spinner_dropdown_item, groupList);
			spinnerGroups.setAdapter(spinnerGroupAdapter);
		}
	}
	
	/**
	 * Validate all the fields in sign up and register
	 */
	private void validateSignUpAndRegister(){
		String passwor = et_password.getText().toString();
		String verifyPwd = et_verifyPassword.getText().toString();
		//System.out.println("pwd:"+passwor+"verify_pwd:"+verifyPwd);
		if(et_username.getText().toString().length()<2 || et_username.getText().toString()==null || et_username.getText().toString().length()>60) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_user_name, R.string.invalid_userName);
		} else if (et_full_name.getText().toString().length()<2 || et_full_name.getText().toString()==null || et_full_name.getText().toString().length()>60) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_fullname, R.string.invalid_fullname);
		} else if (et_password.getText().toString().length()<2 || et_password.getText().toString()==null || et_password.getText().toString().length()>40) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_password, R.string.invalid_password);
		} else if (et_answer.getText().toString().length()<1 || et_answer.getText().toString() == null) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_answer, R.string.invalid_answer);
		} else if(et_dob.getText().toString()==null || et_dob.getText().toString().length()<5) {
			UserFunctions.DisplayAlertDialog(this, R.string.select_valid_date_of_birth, R.string.invalid_date_of_birth);
		} else if (et_academylevel.getText().toString() == null) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_academic_level, R.string.invalid_academic_level);
		} else if (et_address.getText().toString() == null || et_address.getText().toString().length()<2) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_address, R.string.invalid_address);
		} else if (et_city.getText().toString() == null || et_city.getText().toString().length()<2) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_city, R.string.invalid_city);
		} else if (et_state.getText().toString() == null || et_state.getText().toString().length()<2) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_state, R.string.invalid_state);
		} else if (et_country.getText().toString() == null || et_country.getText().toString().length()<2) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_country, R.string.invalid_country);
		} else if (et_phone.getText().toString() == null || et_phone.getText().toString().length()<2) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_phone_no, R.string.invalid_phone_no);
		} else if (!CheckValidEmail(et_emailid.getText().toString())) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_mail_id, R.string.invalid_email_id);
		} else if (et_pincode.getText().toString() == null || et_pincode.getText().toString().length()<2) {
			UserFunctions.DisplayAlertDialog(this, R.string.input_valid_pincode, R.string.invalid_pincode);
		} else if (!et_password.getText().toString().equals(et_verifyPassword.getText().toString())) {
			UserFunctions.DisplayAlertDialog(this, R.string.password_doesnt_match, R.string.check_password);
		} else if (!UserFunctions.isInternetExist(ExportActivity.this)) {
			UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportActivity.this, ExportActivity.this.getResources().getString(R.string.check_internet_connectivity), ExportActivity.this.getResources().getString(R.string.connection_error));
		} else {
			new RegisterNewUser().execute();
		}
	}
	
	/**
	 * Validate the Email ID
	 * @param str
	 * @return
	 */
	private boolean CheckValidEmail(String str)
	{
		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		Pattern pattern = Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(str);
		if(matcher.matches())
		{
			return true;
		}
		else
			return false;
	}

	/**
	 * Register new user async task which makes new user to register in the background
	 * @author Suriya
	 *
	 */
	private class RegisterNewUser extends AsyncTask<Void, Void, Void>{
		int forgotquestionId;
		int groupId;
		String currentculture;
		ProgressDialog progressDialog;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			int sp_selectedpos = spinnerSecurityQy.getSelectedItemPosition();
			ArrayList<String> questionIdList = getValuesFromHashMapArrayList(webService.questionHashMapList, webService.KEY_QUESTION_ID);
			forgotquestionId =Integer.parseInt(questionIdList.get(sp_selectedpos));
			int sp_selectedgroups = spinnerGroups.getSelectedItemPosition();
			ArrayList<String> groupIdList = getValuesFromHashMapArrayList(webService.groupHashMapList, webService.KEY_GROUP_ID);
			groupId = Integer.parseInt(groupIdList.get(sp_selectedgroups));
			currentculture="en_US";
			progressDialog = ProgressDialog.show(ExportActivity.this, "", "Signing Up. Please wait...", true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			webService.RegistrationUser(et_username.getText().toString(),forgotquestionId,et_password.getText().toString(),et_answer.getText().toString(),et_full_name.getText().toString(),et_emailid.getText().toString(),et_dob.getText().toString(),gender_id,et_academylevel.getText().toString(),groupId,et_address.getText().toString(),et_city.getText().toString(),et_country.getText().toString(),et_state.getText().toString(),et_pincode.getText().toString(),et_phone.getText().toString(),currentculture);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if(webService.SignUpResponse.equals("Registered Successfully Login Information Sent to Your Email")){
				UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportActivity.this, webService.SignUpResponse, "");
				webService.LogInResponse = true;
				if (webService.LogInResponse) {
					setUpLoginProperties(et_username.getText().toString(), et_password.getText().toString());
				}
				signUpPopwindow.dismiss();
			}else{
				UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportActivity.this, webService.SignUpResponse, "");
			}
		}
	}
	
	/**
	 * Set up the properties once the user is logged in
	 * @param _password 
	 * @param _userName 
	 */
	private void setUpLoginProperties(String _username, String _password){
		username = _username;
		password = _password;
		btnLogin.setText(R.string.logout);
		btnSignUp.setEnabled(false);
		btnBookStatus.setEnabled(true);
		btnUploadBook.setEnabled(true);
	}

	/**
	 * set up the properties once the user is logged out
	 */
	private void setUpLogoutProperties(){
		username = "";
		password = "";
		btnLogin.setText(R.string.login);
		btnSignUp.setEnabled(true);
		btnBookStatus.setEnabled(false);
		btnUploadBook.setEnabled(false);
		webService.LogInResponse = false;
	}
	/**
	 * login class async task which make the user to login to the domain
	 * @author Suriya
	 *
	 */
	private class LogIn extends AsyncTask<Void, Void, Void>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress_signIn.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			webService.CheckUserNamePassword(et_login_username.getText().toString(), et_login_password.getText().toString());
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (webService.LogInResponse) {
				setUpLoginProperties(et_login_username.getText().toString(), et_login_password.getText().toString());
			} else {
				UserFunctions.DisplayAlertDialog(ExportActivity.this, R.string.check_username_password, R.string.login_details);
			}
			progress_signIn.setVisibility(View.INVISIBLE);
			loginPopoverView.dissmissPopover(true);
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_ID:
			return new DatePickerDialog(this, pickerListener, year, month, day);
		}
		return null;
	}
	
	@Override
	public void finish() {
		super.finish();
		String htmlFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID();
		UserFunctions.deleteGeneratedHtmlPage(htmlFilePath);
		
		File epubFilePath = new File(ePubPath);
		File pdfFilePath = new File(pdfPath);
		if (epubFilePath.exists() || pdfFilePath.exists()) {
			epubFilePath.delete();
			pdfFilePath.delete();
		}
		File epubDirPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/ePub");
		UserFunctions.DeleteDirectory(epubDirPath);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (mHelper != null) {
			mHelper.dispose();
			mHelper = null;
		}
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int selectedYear, int selectedMonth,
				int selectedDay) {
			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			et_dob.setText(new StringBuilder().append(month + 1).append("/").append(day).append("/").append(year));
		}
	};
}
