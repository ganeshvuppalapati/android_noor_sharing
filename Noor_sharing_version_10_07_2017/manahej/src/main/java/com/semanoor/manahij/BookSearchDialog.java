package com.semanoor.manahij;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Krishna on 14-12-2015.
 */
public class BookSearchDialog {
    MainActivity activity_main;
    public ArrayList<String> searchIndexArray = new ArrayList<String>();
    ArrayList<BookDetails> BookTitle;
    ArrayList<BookDetails> SearchDetails;
    ArrayList<BookDetails> SelectedBookPages;
    Book currentBook;
    ViewPager viewPager;
    public bookSearchAdapter searchAdapter;

    int wantedPosition;
    boolean clicked;

    ArrayList<String> malzamahBookList;
    ArrayList<PageNumbers> selectedPageList = new ArrayList<PageNumbers>();
    String selectedBook;
    Malzamah malzamah;
    public ArrayList<HashMap<String,String>> existList;
    public BookSearchDialog(MainActivity mainActivity) {
        activity_main = mainActivity;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity_main);
        try {
            existList = (ArrayList<HashMap<String,String>>) ObjectSerializer.deserialize(prefs.getString(Globals.SUBSLIST, ObjectSerializer.serialize(new ArrayList<HashMap<String,String>>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void displayPopoverView() {
        activity_main.PopoverView = new PopoverView(activity_main, R.layout.enrich_viewpager);
        int width =  (int) activity_main.getResources().getDimension(R.dimen.malzamahpopup_width);
        int height =  (int) activity_main.getResources().getDimension(R.dimen.malzamahpopup_height);
        activity_main.PopoverView.setContentSizeForViewInPopover(new Point(width, height));
        activity_main.PopoverView.setDelegate(activity_main);
        activity_main.PopoverView.showPopoverFromRectInViewGroup(activity_main.rl, activity_main.PopoverView.getFrameForView(activity_main.et_layout), activity_main.PopoverView.PopoverArrowDirectionUp, true);
        viewPager = (ViewPager)activity_main.PopoverView.findViewById(R.id.viewPager);
        searchAdapter = new bookSearchAdapter();
        viewPager.setAdapter(searchAdapter);
        viewPager.setOffscreenPageLimit(2);
        //viewPager.setCurrentItem(Integer.parseInt(currentPageNumber) - 1);
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    public void getAllBookContent(String text) {
        if (activity_main.PopoverView == null) {
            displayPopoverView();
        }

        if (SearchDetails != null && SearchDetails.size() > 0) {
            SearchDetails.clear();
        }
        SearchDetails = new ArrayList<BookDetails>();
        for (int i = 0; i < activity_main.gridShelf.bookListArray.size(); i++) {
            currentBook = (Book) activity_main.gridShelf.bookListArray.get(i);
            if (searchIndexArray.size() > 0) {
                searchIndexArray.clear();
            }

            processXML();
            serachTableReload(text);
        }
       BookTitle = getTotalNumberOfBookList();
        if (searchAdapter.lv != null) {
           searchAdapter.lv.setAdapter(new listAdapter(activity_main, BookTitle));
        }
    }

    public class checkingForCoverPage extends AsyncTask<Void, Void, Void> {

        String text;

        public checkingForCoverPage(String s) {
        this.text=s;
        }

        @Override
        protected void onPreExecute() {
            if (activity_main.PopoverView == null) {
                displayPopoverView();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (SearchDetails != null && SearchDetails.size() > 0) {
                SearchDetails.clear();
            }
            SearchDetails = new ArrayList<BookDetails>();
            for (int i = 0; i < activity_main.gridShelf.bookListArray.size(); i++) {
                currentBook = (Book) activity_main.gridShelf.bookListArray.get(i);
                if (searchIndexArray.size() > 0) {
                    searchIndexArray.clear();
                }

                processXML();
                serachTableReload(text);
            }
            BookTitle = getTotalNumberOfBookList();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (searchAdapter.lv != null) {
                searchAdapter.lv.setAdapter(new listAdapter(activity_main, BookTitle));
            }
            if (BookTitle.size()==0){
                searchAdapter.progresBar.setVisibility(View.INVISIBLE);
            }
        }


    }

    private ArrayList<BookDetails> getTotalNumberOfBookList() {
        ArrayList<BookDetails> enrichPageNoList = new ArrayList<BookDetails>();
       // ArrayList<BookDetails> Sample = new ArrayList<BookDetails>();
        //int p = 0;
        for (int i = 0; i < SearchDetails.size(); i++){
            //for (BookDetails enrichment : SearchDetails) {
             BookDetails enrichment=SearchDetails.get(i);
                boolean isEnrichPageNoExist = false;
                for (BookDetails filteredEnrichments : enrichPageNoList) {
                    if (filteredEnrichments.getBookName() == enrichment.getBookName()) {
                        isEnrichPageNoExist = true;
                        break;
                    } else {
                       isEnrichPageNoExist = false;
                    }
                }
                if (!isEnrichPageNoExist) {
                   enrichPageNoList.add(enrichment);
                }
            }

        return enrichPageNoList;
    }

    public void processXML() {
        // searchIndexArray.clear();
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {
                public void startElement(String uri, String localName, String qName,
                                         Attributes attributes) throws SAXException {
                    if (qName != "Search") {
                        String attrValue = attributes.getValue("Data");
                        searchIndexArray.add(attrValue);
                    }
                }

            };

            //File sdPath = getFilesDir();
            //revert_book_hide :
            String filePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/" + "Search.xml";
            //filePath = sdPath+"/"+bookName+"Book/Search.xml";
            InputStream inputStream = new FileInputStream(filePath);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");

            saxParser.parse(is, handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void serachTableReload(String searchText) {
        String withoutVowels = escapeHarakat(searchText);
        //resultFound.clear();//changes-s

        if (!searchText.equals("")) {
            for (int i = 0; i < searchIndexArray.size(); i++) {
                String fileContent = searchIndexArray.get(i);
                if (fileContent.contains(withoutVowels)) {
                    if (currentBook.get_bStoreID().contains("M")) {
                        //resultFound.add("page " +(i+1));
                        BookDetails book = new BookDetails();
                        book.setBookName(currentBook.getBookTitle());
                        book.setPageNum(i + 2);
                        book.setTypedText(searchText);
                        book.setBookId(currentBook.getBookID());
                        book.setBookStoreId(currentBook.get_bStoreID());
                        book.setChecked(false);
                        book.setBook(currentBook);
                        SearchDetails.add(book);

                    } /*else {
                        BookDetails book = new BookDetails();
                        book.setBookName(currentBook.getBookTitle());
                        book.setPageNum(i+1);
                        book.setTypedText(searchText);
                        book.setBookId(currentBook.getBookID());
                        book.setBookStoreId(currentBook.get_bStoreID());
                        book.setChecked(false);
                        SearchDetails.add(book);
                        //resultFound.add("page " +i);
                    }*/
                }
            }
        }
    }

    private String escapeHarakat(String searchText) {

        if (searchText.contains("َ")) {
            searchText = searchText.replaceAll("َ", "");
        }
        if (searchText.contains("ً")) {
            searchText = searchText.replaceAll("ً", "");
        }
        if (searchText.contains("ُ")) {
            searchText = searchText.replaceAll("ُ", "");
        }
        if (searchText.contains("ٌ")) {
            searchText = searchText.replaceAll("ٌ", "");
        }
        if (searchText.contains("ِ")) {
            searchText = searchText.replaceAll("ِ", "");
        }
        if (searchText.contains("ٍ")) {
            searchText = searchText.replaceAll("ٍ", "");
        }
        if (searchText.contains("ْ")) {
            searchText = searchText.replaceAll("ْ", "");
        }
        if (searchText.contains("ّ")) {
            searchText = searchText.replaceAll("ّ", "");
        }
        return searchText;
    }

    public class BookDetails {
        private String bookName;
        private int pageNum;
        private String typedText;
        private int bookId;
        private String bookStoreId;
        private boolean checked;
        private Book book;


        public String getBookName() {
            return bookName;
        }

        public void setBookName(String bookName) {
            this.bookName = bookName;
        }

        public String getTypedText() {
            return typedText;
        }

        public void setTypedText(String typedText) {
            this.typedText = typedText;
        }

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getBookId() {
            return bookId;
        }

        public void setBookId(int bookId) {
            this.bookId = bookId;
        }

        public String getBookStoreId() {
            return bookStoreId;
        }

        public void setBookStoreId(String bookStoreId) {
            this.bookStoreId = bookStoreId;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public Book getBook() {
            return book;
        }

        public void setBook(Book book) {
            this.book = book;
        }
    }


    public class bookSearchAdapter extends PagerAdapter {

        public ListView lv;
        public ListView listView;
        public WebView webView;
        public ProgressBar progressBar,progresBar;
        boolean resourceSelected,isDeleteBookDir;

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            LayoutInflater inflater = (LayoutInflater) activity_main.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = null;
            switch (position) {
                case 0:
                    // view = inflater.inflate(R.layout.export_enr, null);
                    view = inflater.inflate(R.layout.listview_pages, null);
                    RelativeLayout rl_toolBar = (RelativeLayout) view.findViewById(R.id.rl_toolBar);
                    rl_toolBar.setVisibility(View.GONE);
                    progresBar=(ProgressBar)view.findViewById(R.id.progressBar1);
                    progresBar.setVisibility(View.VISIBLE);
                    lv = (ListView) view.findViewById(R.id.listView1);
                    //lv=(ListView)view.findViewById(R.id.listViewAdv);
                   // searchAdapter.lv.setAdapter(new listAdapter(activity_main, BookTitle));
                    if(BookTitle!=null) {
                        lv.setAdapter(new listAdapter(activity_main, BookTitle));
                    }
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View v, int position, long l) {
                            BookDetails book = BookTitle.get(position);
                            getAllPagesFromSelectBook(book);
                            listView.setAdapter(new pageListAdapter(activity_main, SelectedBookPages));
                            viewPager.setCurrentItem(1);
                            //listView.set
                        }
                    });

                    break;

                case 1:
                    view = inflater.inflate(R.layout.listview_pages, null);
                    RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.layout);
                    //RelativeLayout rl_toolBar = (RelativeLayout) view.findViewById(R.id.rl_toolBar);
                    listView = (ListView) view.findViewById(R.id.listView1);
                    Button btn_Malzamah = (Button) view.findViewById(R.id.btn_malzamah);
                    //btn_Export.setVisibility(View.GONE);
                    Button btn_back = (Button) view.findViewById(R.id.btnBack);
                    btn_back.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //new checkingForCoverPage().execute();
                            viewPager.setCurrentItem(0);
                        }
                    });
                    btn_Malzamah.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (selectedPageList.size() > 0) {
                                PageNumbers selectedPage = selectedPageList.get(0);
                                if (UserFunctions.checkLoginAndSubscription(activity_main,false,existList,selectedPage.getBook().getClientID(),activity_main.group.getShelfCreateMalzamahEnable(),selectedPage.getBook().getPurchaseType())) {
                                    String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedPage.getBookID() + "/FreeFiles/pageBG_1.png";
                                    if (!new File(bookCardImagePath).exists()) {
                                        bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedPage.getBookID() + "/FreeFiles/card.png";
                                    }
                                    Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
                                    activity_main.imgViewCover.setImageBitmap(bitmap);
                                    viewPager.setCurrentItem(3);
                                }
                            } else {

                                UserFunctions.DisplayAlertDialog(activity_main, R.string.page_alert, R.string.warning);
                            }
                        }
                    });
                    break;


                case 2:
                    // view = inflater.inflate(R.layout.book_preview_image, null);
                    view = inflater.inflate(R.layout.book_preview_image, null);
                    webView = (WebView) view.findViewById(R.id.bTWebView1);
                    progressBar=(ProgressBar)view.findViewById(R.id.progressBar);
                    webView.getSettings().setLoadWithOverviewMode(true);
                    webView.getSettings().setUseWideViewPort(true);
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                    webView.clearCache(true);
                    webView.setWebViewClient(new WebViewClient());
                    webView.getSettings().setAllowContentAccess(true);
                    webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                    webView.getSettings().setAllowFileAccess(true);
                    webView.getSettings().setPluginState(WebSettings.PluginState.ON);
                    webView.getSettings().setDomStorageEnabled(true);

                    Button btn_back1 = (Button) view.findViewById(R.id.btnBack);
                    btn_back1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            viewPager.setCurrentItem(1);
                        }
                    });
                    webView.setWebViewClient(new WebViewClient() {

                        @Override
                        public void onPageFinished(WebView view, String url) {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
                break;

                case 3:
                    view=inflater.inflate(R.layout.malzamah_creation,null);
                    CircleButton btn_previous = (CircleButton) view.findViewById(R.id.btn_previous);
                    Button btn_Create = (Button) view.findViewById(R.id.btn_createbook);
                    final EditText et_bookName = (EditText) view.findViewById(R.id.et_book_name);
                    final EditText et_Description = (EditText) view.findViewById(R.id.et_description);
                    activity_main.imgViewCover = (ImageView) view.findViewById(R.id.imgView_cover);
                    malzamahBookList = activity_main.db.getMalzamahBooksList();
                    btn_previous.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            viewPager.setCurrentItem(1);
                        }
                    });
                  /*  btnswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                resourceSelected = true;
                            } else {
                                resourceSelected = false;
                            }
                        }
                    });*/
                    btn_Create.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String title = et_bookName.getText().toString();
                            String description = et_Description.getText().toString();
                            if (title.isEmpty()) {
                                et_bookName.requestFocus();
                                et_bookName.getBackground().setColorFilter(activity_main.getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
                            } else {
                                if(malzamah==null) {
                                    malzamah = new Malzamah(activity_main);
                                }
                                //malzamah.bookId=bookId;
                                malzamah.isDeleteBookDir = false;
                                activity_main.isDeleteBookDir=false;
                                for (int i = 0; i < activity_main.gridShelf.bookListArray.size(); i++) {
                                    currentBook = (Book) activity_main.gridShelf.bookListArray.get(i);
                                    PageNumbers selectedPage = selectedPageList.get(0);
                                    if(currentBook.is_bStoreBook()) {
                                        if (currentBook.getBookID()==selectedPage.getBookID() &&currentBook.get_bStoreID().equals(selectedPage.getStoreId())) {
                                            malzamah.resourceSelected = resourceSelected;
                                            if(malzamah.coverPageSelected) {
                                                malzamah.new CreateMalzamahBook(title, description, selectedPageList, currentBook).execute();
                                            }else {
                                                malzamah.showCoverImagesForBook(true,title, description, selectedPageList, currentBook);
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    });
                    if(selectedPageList.size()>0) {
                        PageNumbers selectedPage = selectedPageList.get(0);
                        String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedPage.getBookID() + "/FreeFiles/pageBG_1.png";
                        if (!new File(bookCardImagePath).exists()) {
                            bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedPage.getBookID() + "/FreeFiles/card.png";
                        }
                        Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
                        activity_main.imgViewCover.setImageBitmap(bitmap);
                    }
                    activity_main.imgViewCover.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            malzamah = new Malzamah(activity_main);
                            malzamah.showCoverImagesForBook(false,null, null, null, null);
                           //Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                           //galleryIntent.setType("image/*");
                          // activity_main.startActivityForResult(galleryIntent, activity_main.CAPTURE_GALLERY);
                        }
                    });
                    Button btn_add = (Button) view.findViewById(R.id.add_btn_malzamah);
                    final ListView lv_bookImages= (ListView) view.findViewById(R.id.lv_books);
                    lv_bookImages.setAdapter(new MalzBookListAdapter());
                    lv_bookImages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            clicked = true;
                            wantedPosition = position;
                            int firstPosition = lv_bookImages.getFirstVisiblePosition() - lv_bookImages.getHeaderViewsCount();
                            int calculatedPosition = wantedPosition - firstPosition;
                            View currentSelectedListView = parent.getChildAt(calculatedPosition);

                            if (currentSelectedListView != null && currentSelectedListView != view) {
                                view.setBackgroundColor(Color.TRANSPARENT);
                            }
                            currentSelectedListView = view;
                            view.setBackgroundColor(Color.parseColor("#604e3634"));
                            selectedBook = malzamahBookList.get(wantedPosition);
                            lv_bookImages.invalidateViews();
                        }
                    });
                    btn_add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (selectedBook != null) {
                               Malzamah malzamah = new Malzamah(activity_main);
                               for (int i = 0; i < activity_main.gridShelf.bookListArray.size(); i++) {
                                    currentBook = (Book) activity_main.gridShelf.bookListArray.get(i);
                                    PageNumbers selectedPage = selectedPageList.get(0);
                                    if(currentBook.is_bStoreBook()) {
                                        if (currentBook.getBookID()==selectedPage.getBookID() &&currentBook.get_bStoreID().equals(selectedPage.getStoreId())) {
                                            PageExportDialog page_Export = new PageExportDialog(activity_main, activity_main.db, activity_main.gridShelf);
                                            // page_Export.showBookDialog();

                                            String[] str=selectedBook.split("##");
                                            int bookId= Integer.parseInt(str[0]);
                                            for(int j=0;j<activity_main.gridShelf.bookListArray.size();j++){
                                                Book book =(Book)activity_main.gridShelf.bookListArray.get(j);
                                                if(bookId==book.getBookID()){
                                                    page_Export.new AddingPagestoExistingNormalBook(selectedPageList,book).execute();
                                                    break;
                                                }
                                            }
                                          // malzamah.new AddingPagestoExistingMalzamahBook(selectedBook, currentBook, selectedPageList).execute();  // selectedBook==Malzamah booksList , currentBook =pagesSelectedfrombook
                                            break;
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(activity_main, R.string.select_malzamah_books_message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    break;
                default:
                    break;
            }
            ((ViewPager) container).addView(view, 0);
            return view;

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position,
                                   Object object) {

        }
    }

    public class listAdapter extends BaseAdapter {
        MainActivity activity;
        ArrayList<BookDetails> books;

        public listAdapter(MainActivity activity_main, ArrayList<BookDetails> searchDetails) {
            activity = activity_main;
            books = searchDetails;
        }

        @Override
        public int getCount() {
            return books.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (convertView == null) {
                v = activity.getLayoutInflater().inflate(R.layout.inflate_booksearch, null);
            }
            ImageView imageView = (ImageView) v.findViewById(R.id.enr_img);
            TextView tv = (TextView) v.findViewById(R.id.textView1);
            BookDetails book = books.get(position);
            String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookId() + "/FreeFiles/pageBG_1.png";
            if (!new File(bookCardImagePath).exists()) {
                bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookId() + "/FreeFiles/card.png";
            }
            if(position==books.size()-1){
                searchAdapter.progresBar.setVisibility(View.INVISIBLE);
            }
            Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
            imageView.setImageBitmap(bitmap);
            tv.setText(book.getBookName());

            return v;
        }
    }


    public class pageListAdapter extends BaseAdapter {
        MainActivity activity;
        ArrayList<BookDetails> books;

        public pageListAdapter(MainActivity activity_main, ArrayList<BookDetails> SelectedBookPages) {
            activity = activity_main;
            books = SelectedBookPages;
        }

        @Override
        public int getCount() {
            return SelectedBookPages.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (convertView == null) {
                v = activity.getLayoutInflater().inflate(R.layout.booksearchpages, null);
            }
            ImageView imageView = (ImageView) v.findViewById(R.id.enr_img);
            TextView tv = (TextView) v.findViewById(R.id.textView1);
            final CheckBox checkbox=(CheckBox) v.findViewById(R.id.checkBox);
            Button btn_page = (Button) v.findViewById(R.id.btn_page);
            Button btn_preview = (Button) v.findViewById(R.id.btn_preview);
            Button btn_malzamah = (Button) v.findViewById(R.id.btn_malzamah);
            final BookDetails book = books.get(position);
            String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookId() + "/thumbnail_" + book.getPageNum() + ".png";
            Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
            imageView.setImageBitmap(bitmap);
            int pageNo = book.getPageNum() - 1;
            tv.setText(activity_main.getResources().getString(R.string.page) + pageNo);
            btn_page.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pageNo = book.getPageNum();
                    activity_main.selectedPageInBook(book.getPageNum(),book.getBookId(),book.getTypedText(),book.getBookStoreId());

                }
            });
            btn_preview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pageNo = book.getPageNum();
                    //String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookId() + "/thumbnail_" + book.getPageNum() + ".png";
                    //Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
                    int width= (int) activity_main.getResources().getDimension(R.dimen.info_popover_booksearchwidth);
                    int height=(int) activity_main.getResources().getDimension(R.dimen.info_popover_booksearchheight);
                    //Bitmap b=Bitmap.createScaledBitmap(bitmap,width,height,false);
                    File htmlFile = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+book.getBookStoreId()+"Book/"+book.getPageNum()+".htm");
                    String srcString = UserFunctions.decryptFile(htmlFile);
                    String filePath ="file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+book.getBookStoreId()+"Book/";
                    searchAdapter.webView.loadDataWithBaseURL(filePath, srcString, "text/html", "utf-8", null);
                    searchAdapter.progressBar.setVisibility(View.VISIBLE);
                    viewPager.setCurrentItem(2);
                }
            });
            btn_malzamah.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (book.isChecked()) {
                        book.setChecked(false);
                        //SelectedBookPages.add(book);
                        selectingpages(book);
                        checkbox.setChecked(false);
                    } else {
                        book.setChecked(true);
                        //  SelectedBookPages.add(book);
                        selectingpages(book);
                        checkbox.setChecked(true);
                    }

                }
            });
            if(book.isChecked()){
                checkbox.setChecked(true);
            }else{
                checkbox.setChecked(false);
            }
            checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(book.isChecked()){
                        book.setChecked(false);
                        //SelectedBookPages.add(book);
                        selectingpages(book);

                        checkbox.setChecked(false);
                    }else {
                        book.setChecked(true);
                        //SelectedBookPages.add(book);
                        selectingpages(book);
                        checkbox.setChecked(true);
                    }
                }
            });
            return v;
        }
    }

    private void getAllPagesFromSelectBook(BookDetails selectedBook) {
        if (SelectedBookPages != null && SelectedBookPages.size() > 0) {
            SelectedBookPages.clear();
        }
        SelectedBookPages = new ArrayList<BookDetails>();
        for (BookDetails book : SearchDetails) {
            if (book.getBookName().equals(selectedBook.getBookName()) && book.getBookId() == selectedBook.getBookId()) {
                SelectedBookPages.add(book);
            }
        }

    }



   private class MalzBookListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return malzamahBookList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            View view = convertView;
            if (view == null) {
                view = activity_main.getLayoutInflater().inflate(R.layout.malzamah_inflate_book_image, null);
            }
            ImageView image = (ImageView) view.findViewById(R.id.iv_bookimage);
            image.setVisibility(View.GONE);
            TextView tv_bookName=(TextView)view.findViewById(R.id.tv_pageNo);
            String List = malzamahBookList.get(position);
            String[] bookList=List.split("##");

         /*   String  BID = bookList[4];
            String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/Book/fpimages/1.jpg";
            if(!new File(bookCardImagePath).exists()){
                int id=Integer.parseInt(bookList[0]);
                bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+id+"/FreeFiles/card.png";
            }
            Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
            image.setImageBitmap(bitmap);*/
            if(wantedPosition==position){
                if(clicked) {
                    view.setBackgroundColor(Color.parseColor("#604e3634"));
                }
            }else{
                view.setBackgroundColor(Color.TRANSPARENT);
            }
            tv_bookName.setText(bookList[1]);

            return view;
        }
    }

    private void selectingpages(BookDetails selectedBook) {
        addingSelectedPages(selectedBook);
        for (BookDetails book : SearchDetails) {
             if (book.getBookName().equals(selectedBook.getBookName()) && book.getBookId() == selectedBook.getBookId() &&book.getPageNum()==selectedBook.getPageNum()) {
                book.setChecked(selectedBook.isChecked());
                break;
            }
        }
    }

    private void addingSelectedPages(BookDetails book) {
        PageNumbers page = new PageNumbers(activity_main);
        page.setChecked(book.isChecked());
        page.setpageNumber(book.getPageNum());
        page.setStoreId(book.getBookStoreId());
        page.setBookID(book.getBookId());
        page.setBook(book.getBook());
        if (book.isChecked()) {
            selectedPageList.add(page);
        } else {
            for(int i=0;i<selectedPageList.size();i++){
                PageNumbers p=selectedPageList.get(i);
                if(p.getBookID()==page.getBookID()&&page.getpageNumber()==p.getpageNumber()){
                    selectedPageList.remove(i);
                    break;
                }
            }
        }
    }

    public void imagePicked(Bitmap bitmap) {
        activity_main.imgViewCover.setImageBitmap(bitmap);
        activity_main.malzamah = new Malzamah(activity_main);
        activity_main.isDeleteBookDir = true;
        activity_main.malzamah.isDeleteBookDir=true;
        activity_main.malzamah.creatingCoverImage(bitmap);
        ///activity_main.malzamah.imagePicked(bitmap);
   }

   /* private class checkingForCoverPage extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            boolean notexist = false;
            int Id = 0;
            if(bookId!=0) {
                for (BookDetails book : SearchDetails) {
                    if (book.getBookId() == bookId && !book.isChecked()) {
                        notexist=true;
                        Id=book.getBookId();
                    }else if(book.getBookId() == bookId && book.isChecked()){
                        notexist=false;
                    }
               }
                if(notexist&& bookId==Id){
                    bookId=0;
                }
            }
        }
        @Override
        protected Void doInBackground(Void... voids) {
            if(bookId==0) {
                for (BookDetails book : SearchDetails) {
                    if (book.isChecked()) {
                        bookId = book.getBookId();
                        break;

                    }
                }
            }
            return null;
        }
    }*/

}

