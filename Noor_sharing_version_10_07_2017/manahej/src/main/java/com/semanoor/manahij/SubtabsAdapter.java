package com.semanoor.manahij;

import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.dragsortadapter.DragSortAdapter;
import com.dragsortadapter.NoForegroundShadowBuilder;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by karthik on 10-04-2017.
 */

public class SubtabsAdapter extends DragSortAdapter<SubtabsAdapter.MyViewHolder> {

    private ArrayList<Enrichments> horizontalList;
    RecyclerView recyclerView;
    int pageNo;
    Context context;
    GestureDetector gestureDetector;
    OnclickCallBackInterface<Object> onclickCallBackInterface;
    public static int level1 = 0;
    public static int level2 = 0;
    public static int level3 = 0;
    View gestureView;
    private DatabaseHandler db;
    int fromPos;
    RecyclerView recyclerView2,recyclerView3;
    RelativeLayout layout2,layout3;

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getPositionForId(long id) {
        return horizontalList.indexOf((int) id);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public boolean move(int fromPosition, int toPosition) {
        if (fromPos != toPosition && toPosition != -1) {
            if (toPosition==0?(recyclerView.getId()==R.id.gridView2 || recyclerView.getId()==R.id.gridView3):true) {
                Enrichments previousPosition = horizontalList.get(fromPos);
                Enrichments CurrentPosition = horizontalList.get(toPosition);
                db.executeQuery("update enrichments set categoryID = '" + CurrentPosition.getEnrichmentId() + "'where enrichmentID = '" + previousPosition.getEnrichmentId() + "'and pageNO = '" + previousPosition.getEnrichmentPageNo() + "'and BID = '" + previousPosition.getEnrichmentBid() + "'");
                RecyclerView curRecyclerView;
                if (recyclerView.getId() == R.id.gridView2) {
                    curRecyclerView = recyclerView3;
                } else if (recyclerView.getId() == R.id.gridView3) {
                    curRecyclerView = recyclerView3;
                } else {
                    curRecyclerView = recyclerView2;
                }
                int catId;
                if (recyclerView.getId() == R.id.gridView3) {
                    catId = CurrentPosition.getCategoryID();
                } else {
                    catId = CurrentPosition.getEnrichmentId();
                }
                for (int i = 0; i < horizontalList.size(); i++) {
                    Enrichments enrichments = horizontalList.get(i);
                    if (enrichments.getEnrichmentId() == previousPosition.getEnrichmentId()) {
                        horizontalList.remove(i);
                    }
                }
                if (context instanceof BookViewActivity) {
                    ((BookViewActivity) context).loadView(horizontalList, recyclerView);
                } else if (context instanceof pdfActivity) {
                    ((pdfActivity) context).loadView(previousPosition.getEnrichmentPageNo(), horizontalList, recyclerView);
                }else if (context instanceof BookViewReadActivity) {
                    ((BookViewReadActivity) context).loadView( horizontalList, recyclerView);
                }else if (context instanceof PreviewActivity){
                    ((PreviewActivity) context).loadView(previousPosition.getEnrichmentPageNo(), horizontalList, recyclerView);
                }
                new loadSubEnrichmentTabs(previousPosition.getEnrichmentBid(), previousPosition.getEnrichmentPageNo(), curRecyclerView, 0, catId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }else{
                Enrichments previousPosition = horizontalList.get(fromPos);
                if (context instanceof BookViewActivity) {
                    ((BookViewActivity)context).loadView(horizontalList, recyclerView);
                }else if (context instanceof pdfActivity){
                    ((pdfActivity)context).loadView(previousPosition.getEnrichmentPageNo(),horizontalList, recyclerView);
                }else if (context instanceof BookViewReadActivity) {
                    ((BookViewReadActivity) context).loadView( horizontalList, recyclerView);
                }else if (context instanceof PreviewActivity){
                    ((PreviewActivity) context).loadView(previousPosition.getEnrichmentPageNo(), horizontalList, recyclerView);
                }
            }
        }else{
            Enrichments previousPosition = horizontalList.get(fromPos);
            if (context instanceof BookViewActivity) {
                ((BookViewActivity)context).loadView(horizontalList, recyclerView);
            }else if (context instanceof pdfActivity){
                ((pdfActivity)context).loadView(previousPosition.getEnrichmentPageNo(),horizontalList, recyclerView);
            }else if (context instanceof BookViewReadActivity) {
                ((BookViewReadActivity) context).loadView( horizontalList, recyclerView);
            }else if (context instanceof PreviewActivity){
                ((PreviewActivity) context).loadView(previousPosition.getEnrichmentPageNo(), horizontalList, recyclerView);
            }
        }
        return false;
    }

    public class MyViewHolder extends DragSortAdapter.ViewHolder {
        private Button btn_home;
        private Button del_btn;
        private Button add_btn;
        private RelativeLayout addbtn_layout;
        private Button add_btn_tab;


        public MyViewHolder( DragSortAdapter adapter,View view) {
            super(adapter,view);
            btn_home = (Button) view.findViewById(R.id.btnEnrHome);
            del_btn = (Button) view.findViewById(R.id.btn_enr_del);
            add_btn = (Button) view.findViewById(R.id.add_btn);
            addbtn_layout = (RelativeLayout) view.findViewById(R.id.add_layout);
            add_btn_tab = (Button) view.findViewById(R.id.add_btn_tab);
            //enrichmentTabList = db.getEnrichmentTabListForBookReadAct1(currentBook.getBookID(), pageNo, BookViewReadActivity.this, enrichmentLayout);
        }

        @Override
        public View.DragShadowBuilder getShadowBuilder(View itemView, Point touchPoint) {
            return new NoForegroundShadowBuilder(itemView, touchPoint);
        }
    }


    public SubtabsAdapter(RelativeLayout rLayout2,RelativeLayout rLayout3,RecyclerView subRecyclerView2,RecyclerView subRecyclerView3,DatabaseHandler dBase,Context ctx,int pageNumber, ArrayList<Enrichments> horizontalList, RecyclerView recycler,OnclickCallBackInterface<Object> onClickCallBack) {
        super(recycler);
        this.horizontalList = horizontalList;
        this.pageNo=pageNumber;
        this.recyclerView=recycler;
        if (recyclerView.getTag()==null){
            recyclerView.setTag(recyclerView.getId());
        }
        this.onclickCallBackInterface = onClickCallBack;
        this.db = dBase;
        this.layout2 = rLayout2;
        this.layout3 = rLayout3;
        this.recyclerView2 = subRecyclerView2;
        this.recyclerView3 = subRecyclerView3;
        this.context = ctx;
        gestureDetector = new GestureDetector(context,new GestureListener());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pdf_enrichment_btn, parent, false);

        MyViewHolder holder = new MyViewHolder(this,itemView);
        return holder;
    }


    @Override
    public void onBindViewHolder(final SubtabsAdapter.MyViewHolder holder, final int position) {
        if (recyclerView.getId() == R.id.gridView2){
            holder.btn_home.setBackgroundResource(R.drawable.sub_tab_btn);
        }else if (recyclerView.getId() == R.id.gridView3){
            holder.btn_home.setBackgroundResource(R.drawable.sub_tab_btn_1);
        }
        if (recyclerView.getId() != R.id.gridView2 && recyclerView.getId() != R.id.gridView3 && horizontalList.size()<4 && holder.getAdapterPosition() == horizontalList.size()-1){
            holder.addbtn_layout.setVisibility(View.VISIBLE);
            hideAddLayout(true);
        }else{
            holder.addbtn_layout.setVisibility(View.GONE);
            if (recyclerView.getId() != R.id.gridView2 && recyclerView.getId() != R.id.gridView3) {
                hideAddLayout(false);
            }
        }
        final Enrichments enrich= horizontalList.get(position);
        holder.btn_home.setText(enrich.getEnrichmentTitle());
        holder.add_btn.setVisibility(View.GONE);
        if (recyclerView.getId() == R.id.gridView2) {
            if (enrich.getEnrichmentId() == level2) {
                holder.btn_home.setSelected(true);
                LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
                llm.scrollToPositionWithOffset(position, horizontalList.size());
                llm.scrollToPosition(position);
            } else {
                holder.btn_home.setSelected(false);
            }
        }else if (recyclerView.getId() == R.id.gridView3) {
            if (enrich.getEnrichmentId() == level3) {
                holder.btn_home.setSelected(true);
                LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
                llm.scrollToPositionWithOffset(position, horizontalList.size());
                llm.scrollToPosition(position);
            } else {
                holder.btn_home.setSelected(false);
            }
        }else {
            if (enrich.getEnrichmentId() == level1) {
                holder.btn_home.setSelected(true);
                LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
                llm.scrollToPositionWithOffset(position, horizontalList.size());
                llm.scrollToPosition(position);
            } else {
                holder.btn_home.setSelected(false);
            }
        }

        if(enrich.getEnrichmentType().equals(Globals.advancedSearchType)){
            holder.del_btn.setVisibility(View.VISIBLE);
        }else{
            holder.del_btn.setVisibility(View.GONE);
        }

        holder.add_btn_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof BookViewActivity) {
                    ((BookViewActivity)context).viewTreeList(v,true,0);
                }else if (context instanceof pdfActivity){
                    ((pdfActivity)context).viewTreeList(v,true,0);
                }else if (context instanceof BookViewReadActivity) {
                    ((BookViewReadActivity) context).viewTreeList(v,true,0);
                }else if (context instanceof PreviewActivity){
                    ((PreviewActivity) context).viewTreeList(v,true,0);
                }
            }
        });

        holder.btn_home.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gestureView = v;
                return gestureDetector.onTouchEvent(event);
            }
        });

        holder.btn_home.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (position==0?(recyclerView.getId()==R.id.gridView2 || recyclerView.getId()==R.id.gridView3):true) {
                    Globals.shelfPosition = recyclerView.getId();
                    fromPos = holder.getAdapterPosition();
                    RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(horizontalList.size()-1);
                    if (viewHolder!=null){

                    }
                    holder.startDrag();
                }
                return true;
            }
        });

        holder.btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.btn_home.setSelected(true);
                CallBackObjects objects = new CallBackObjects(position,v,false,false,recyclerView,horizontalList);
                onclickCallBackInterface.onClick(objects);
                notifyDataSetChanged();
            }
        });
        holder.del_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof BookViewActivity) {
                    ((BookViewActivity)context).currentBtnEnrichment = horizontalList.get(holder.getAdapterPosition());
                    ((BookViewActivity)context).deletePopup();
                }else if (context instanceof pdfActivity){
                    ((pdfActivity)context).currentBtnEnrichment = horizontalList.get(holder.getAdapterPosition());
                    ((pdfActivity)context).deletePopup();
                }else if (context instanceof BookViewReadActivity) {
                    ((BookViewReadActivity)context).currentBtnEnrichment = horizontalList.get(holder.getAdapterPosition());
                    ((BookViewReadActivity) context).deletePopup();
                }else if (context instanceof PreviewActivity){
                    ((PreviewActivity)context).currentBtnEnrichment = horizontalList.get(holder.getAdapterPosition());
                    ((PreviewActivity) context).deletePopup();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

    public class GestureListener extends GestureDetector.SimpleOnGestureListener{

        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            boolean isLoadEnrInpopview;
            if (recyclerView.getId()== R.id.gridView3) {
                isLoadEnrInpopview = false;
            }else{
                isLoadEnrInpopview = false;
            }
            CallBackObjects objects = new CallBackObjects(0,gestureView,isLoadEnrInpopview,true,null,null);
            onclickCallBackInterface.onClick(objects);
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
        }
    }

    public class loadSubEnrichmentTabs extends AsyncTask<Void, Void, Void> {

        ArrayList<Enrichments> enrichmentTabList;
        int pageNumber;
        RecyclerView recyclerview;
        int Id;
        int categoryId;
        private int BID;

        public loadSubEnrichmentTabs(int bookID,int pageNumber, RecyclerView recyclerView, int SelectecEnrId,int catId){
            this.pageNumber = pageNumber;
            this.recyclerview=recyclerView;
            this.Id=SelectecEnrId;
            this.categoryId = catId;
            this.BID = bookID;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            enrichmentTabList = db.getEnrichmentTabListForCatId(BID, pageNumber,context,categoryId);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (enrichmentTabList.size()>0) {
                layout2.setVisibility(View.VISIBLE);
                if (recyclerview.getId() == R.id.gridView3) {
                    layout3.setVisibility(View.VISIBLE);
                }else{
                    layout3.setVisibility(View.GONE);
                }
                if (context instanceof BookViewActivity) {
                    ((BookViewActivity)context).loadView(enrichmentTabList, recyclerview);
                }else if (context instanceof pdfActivity){
                    ((pdfActivity)context).loadView(pageNumber,enrichmentTabList, recyclerview);
                }else if (context instanceof BookViewReadActivity) {
                    ((BookViewReadActivity) context).loadView( enrichmentTabList, recyclerview);
                }else if (context instanceof PreviewActivity){
                    ((PreviewActivity) context).loadView(pageNumber, enrichmentTabList, recyclerview);
                }
               /* recyclerView.getAdapter().notifyDataSetChanged();
                if (recyclerView2.getAdapter()!=null) {
                    recyclerView2.getAdapter().notifyDataSetChanged();
                }
                if (recyclerView3.getAdapter()!=null) {
                    recyclerView3.getAdapter().notifyDataSetChanged();
                }*/
            }
        }
    }

    private void hideAddLayout(boolean hide){
        if (hide) {
            if (context instanceof BookViewActivity) {
                ((BookViewActivity) context).add_btn_layout.setVisibility(View.GONE);
            } else if (context instanceof pdfActivity) {
                ((pdfActivity) context).add_btn_layout.setVisibility(View.GONE);
            } else if (context instanceof BookViewReadActivity) {
                ((BookViewReadActivity) context).add_btn_layout.setVisibility(View.GONE);
            } else if (context instanceof PreviewActivity) {
                ((PreviewActivity) context).add_btn_layout.setVisibility(View.GONE);
            }
        }else{
            if (context instanceof BookViewActivity) {
                ((BookViewActivity) context).add_btn_layout.setVisibility(View.VISIBLE);
            } else if (context instanceof pdfActivity) {
                ((pdfActivity) context).add_btn_layout.setVisibility(View.VISIBLE);
            } else if (context instanceof BookViewReadActivity) {
                ((BookViewReadActivity) context).add_btn_layout.setVisibility(View.VISIBLE);
            } else if (context instanceof PreviewActivity) {
                ((PreviewActivity) context).add_btn_layout.setVisibility(View.VISIBLE);
            }
        }
    }
}