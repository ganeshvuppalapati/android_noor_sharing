/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.semanoor.manahij;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.ptg.mindmap.enums.MapState;
import com.ptg.mindmap.layouts.MPEbag;
import com.ptg.mindmap.xml.LruBitmapCache;
import com.semanoor.manahij.mqtt.MQTTSubscriptionService;
import com.semanoor.manahij.mqtt.NetWorkBroadcastReceiver;
import com.semanoor.manahij.mqtt.datamodels.MyPreferenceManager;

//import org.acra.ACRA;
//import org.acra.ReportField;
//import org.acra.ReportingInteractionMode;
//import org.acra.annotation.ReportsCrashes;

import org.webrtc.NetworkMonitorAutoDetect;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.HashMap;

//@ReportsCrashes(mailTo = "mahroof@semanoor.com.sa",
//        customReportContent = { ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT },
//        mode = ReportingInteractionMode.TOAST,
//        resToastText = R.string.yes)

public class ManahijApp extends Application {

    // The following line should be changed to include the correct property id.
    private static final String PROPERTY_ID = "UA-54308578-3"; //UA-54764784-1  //UA-54308578-3

    public static int GENERAL_TRACKER = 0;

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
    public static final String TAG = ManahijApp.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private com.android.volley.toolbox.ImageLoader mImageLoader;

    private static ManahijApp mInstance;

    ManahijApp manahijApplication;

    public static MPEbag m_curEbag;
    public static Context mAppContext;
    MyPreferenceManager pref;

    public void startSubscriptionService() {
        if(isInternetAvailable()) {
            stopSubscriptionService();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Intent mIntent = new Intent(getApplicationContext(), MQTTSubscriptionService.class);
            startService(mIntent);
        }
    }

    public void stopSubscriptionService() {
        if(isInternetAvailable()){
        Intent mIntent = new Intent(getApplicationContext(), MQTTSubscriptionService.class);
        stopService(mIntent);}
    }

    public Activity getVideoActivityContext() {
        return videoActivityContext;
    }

    public void setVideoActivityContext(Activity videoActivityContext) {
        this.videoActivityContext = videoActivityContext;
    }

    Activity videoActivityContext;

    public MyPreferenceManager getPrefManager() {
        if (pref == null) {
            pref = new MyPreferenceManager(this);
        }

        return pref;
    }

        @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(ManahijApp.this);
    }
    private boolean myState,mainActivitysatate,callState,networkState;


    public boolean getState()
    {
        return myState;
    }//End method

    public void setState(boolean s)
    {
        myState = s;
    }
    public boolean getMainState()
    {
        return mainActivitysatate;
    }//End method

    public void setMainState(boolean s)
    {
        mainActivitysatate = s;
    }
    public boolean getCallState()
    {
        return callState;
    }//End method

    public void setCallState(boolean s)
    {
        callState = s;
    }
    public boolean getNetworkState()
    {
        return networkState;
    }//End method

    public void setNetworkState(boolean s)
    {
        networkState = s;
    }
    public void setConnectivityListener(NetWorkBroadcastReceiver.ConnectivityReceiverListener listener) {
        NetWorkBroadcastReceiver.connectivityReceiverListener = listener;
    }

    public boolean isNetworkStatus(){
        try {
            InetAddress ipAddr = InetAddress.getByName("www.google.co.in"); //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }

    }
    public boolean isInternetAvailable() {





        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
       /* if(mWifi.isConnected()){
            return true;
        }else {
           return false;
        }*/


        //  ConnectivityManager connectivity = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connManager != null)
        {
            NetworkInfo[] info = connManager.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
      /*  if(mWifi.isAvailable()){
            return mWifi.isAvailable();
        }else {
            return false;
        }*/
    }
    @Override
    public void onCreate() {
        super.onCreate();
//        ACRA.init(this);
        this.m_curEbag = new MPEbag();
        this.m_curEbag.m_impstate = MapState.MPStateNormal;
        mInstance = this;
        mAppContext=this;
        initImageLoader(getApplicationContext());
    }



    public ManahijApp() {
        super();
    }

     synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(
                            R.xml.global_tracker)
                            : analytics.newTracker(R.xml.ecommerce_tracker);
            t.enableAdvertisingIdCollection(true);
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
       // ImageLoader image=ImageLoader.getInstance();
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }
    public static synchronized ManahijApp getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public com.android.volley.toolbox.ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new com.android.volley.toolbox.ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
