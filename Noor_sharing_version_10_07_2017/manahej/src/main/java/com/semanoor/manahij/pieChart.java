package com.semanoor.manahij;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;


/**
 * Created by karthik on 10-10-2015.
 */
public class pieChart extends View {
    private RectF rect = new RectF();
    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    int percentages;
    String[] barChart_colors;
    float sweepAngle;

    public pieChart(Context context, String[] colors, int percentage) {
        super(context);
        percentages = percentage;
        barChart_colors = colors;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x = getWidth();
        int y = getHeight();
        rect.set(x / 2 - y / 2, 0, x / 2 + y / 2, y);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        float initialAngle = 270;
        sweepAngle = (percentages * (360f / 100));
        paint.setColor(Color.parseColor(barChart_colors[1]));
        setCanvas(canvas, rect, sweepAngle, 360f, paint);
        paint.setColor(Color.parseColor(barChart_colors[0]));
        setCanvas(canvas, rect, initialAngle, sweepAngle, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(1.5f);
        if (sweepAngle == 360) {
            sweepAngle = (float) 359.8;
        }
        canvas.drawArc(rect, initialAngle, sweepAngle, true, paint);
    }

    public void setCanvas(Canvas canvas, RectF rect, Float start, Float end, Paint paint1) {
        canvas.drawArc(rect, start, end, true, paint1);
    }

}
