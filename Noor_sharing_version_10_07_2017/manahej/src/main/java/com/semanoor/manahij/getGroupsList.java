package com.semanoor.manahij;

import android.content.Context;

import com.semanoor.source_sboookauthor.Globals;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by karthik on 08-07-2016.
 */
public class getGroupsList {
    private ArrayList<Object> jsonMap = new ArrayList<Object>();
    private Context context;

    public getGroupsList(Context c){
        this.context = c;
    }

    public ArrayList<Object> getJsonMap() {
        return jsonMap;
    }

    public void setJsonMap(ArrayList<Object> jsonMap) {
        this.jsonMap = jsonMap;
    }

    public void getGroupsList(String downloadurl,String scopeID){
        JSONObject json;
        String jsonData;
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        try {
            URL urlToRequest = new URL(downloadurl);
            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(25000);
            urlConnection.setRequestProperty("accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
            } else {

            }
            String returnResponse = sb.toString();
            jsonData = returnResponse;
            jsonData = jsonData.replaceAll("\\\\r\\\\n", "");
            jsonData = jsonData.replace("\"{", "{");
            jsonData = jsonData.replace("}\",", "},");
            jsonData = jsonData.replace("}\"", "}");
            createAndSaveFile(scopeID+"_groups",jsonData);
            loadGroupData(scopeID+"_groups");

        } catch (MalformedURLException e) {

        } catch (SocketTimeoutException e) {

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }
    private Map<String,Object> toMap(JSONObject object) throws JSONException {
        Map<String,Object> map = new HashMap<String,Object>();
        Iterator<String> keysIter = object.keys();
        while (keysIter.hasNext()){
            String key = keysIter.next();
            Object value = object.get(key);
            if (value instanceof JSONArray){
                value = toList((JSONArray) value);
            }else if (value instanceof JSONObject){
                value = toMap((JSONObject)value);
            }
            map.put(key,value);
        }
        return map;
    }
    private List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0;i<array.length();i++){
            Object value = array.get(i);
            if (value instanceof JSONArray){
                value = toList((JSONArray) value);
            }else if (value instanceof JSONObject){
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
    public void createAndSaveFile(String fileName,String jsonResponse){
        File newFile = new File(Globals.TARGET_BASE_FILE_PATH+"groups");
        if (newFile.exists()){
            File existFile = new File(Globals.TARGET_BASE_FILE_PATH+"groups/"+fileName+".json");
            if (existFile.exists()){
                existFile.delete();
            }
        }else{
            newFile.mkdir();
        }
        try{
            FileWriter file = new FileWriter(Globals.TARGET_BASE_FILE_PATH+"groups/"+fileName+".json");
            file.write(jsonResponse);
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String loadGroupData(String fileName){
        JSONArray jsonArray;
        String str = "";
        try {
            File file = new File(Globals.TARGET_BASE_FILE_PATH+"groups/"+ fileName + ".json");
            FileInputStream fis = new FileInputStream(file);
            int size = fis.available();
            byte[] buffer = new byte[size];
            fis.read(buffer);
            fis.close();
            String response = new String(buffer);
            str = response;
            jsonArray = new JSONArray(response);
            setJsonMap((ArrayList<Object>) toList(jsonArray));
            loadData(getJsonMap());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return str;
    }
   public void loadData(ArrayList<Object> groupList){
       if (groupList.size() > 0 ) {
           for (int i = 0;i<groupList.size();i++) {
               HashMap<String, Object> groupDict = (HashMap<String, Object>) groupList.get(i);
               ArrayList<HashMap<String, Object>> adsTypeList = (ArrayList<HashMap<String, Object>>) groupDict.get("AdsTypeList");
               Groups groups = Groups.getInstance();
               for (HashMap<String, Object> adsTypeDict : adsTypeList) {
                   String adsTypeName = (String) adsTypeDict.get("EngName");
                   groups.setAdsTypeList(adsTypeName);
               }
               ArrayList<HashMap<String, Object>> shelveFeatureList = (ArrayList<HashMap<String, Object>>) groupDict.get("ShelveFeature");
               for (HashMap<String, Object> shelveFeatureDict : shelveFeatureList) {
                   String shelveFeatureName = (String) shelveFeatureDict.get("EngName");
                   groups.setShelveFeatureList(shelveFeatureName);
               }
               ArrayList<HashMap<String, Object>> bookReaderFeatureList = (ArrayList<HashMap<String, Object>>) groupDict.get("BookReaderFeature");
               for (HashMap<String, Object> bookReaderFeatureDict : bookReaderFeatureList) {
                   String bookReaderFeatureName = (String) bookReaderFeatureDict.get("EngName");
                   groups.setBookReaderFeatureList(bookReaderFeatureName);
               }
               ArrayList<HashMap<String, Object>> elessonfeatureList = (ArrayList<HashMap<String, Object>>) groupDict.get("ElessonFeature");
               for (HashMap<String, Object> elessonFeatureDict : elessonfeatureList) {
                   String elessonFeatureName = (String) elessonFeatureDict.get("EngName");
                   groups.setElessonFeatureList(elessonFeatureName);
               }
               ArrayList<HashMap<String, Object>> editorFeatureList = (ArrayList<HashMap<String, Object>>) groupDict.get("BookEditorFeatures");
               for (HashMap<String, Object> editorFeatureDict : editorFeatureList) {
                   String editorFeatureName = (String) editorFeatureDict.get("EngName");
                   groups.setBookEditorFeature(editorFeatureName);
               }
           }
       }
   }
    public void getInAppIdList(String downloadurl,String scopeID){
        JSONObject json;
        String jsonData;
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        try {
            URL urlToRequest = new URL(downloadurl);
            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(25000);
            urlConnection.setRequestProperty("accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
            } else {

            }
            String returnResponse = sb.toString();
            jsonData = returnResponse;
            jsonData = jsonData.replaceAll("\\\\r\\\\n", "");
            jsonData = jsonData.replace("\"{", "{");
            jsonData = jsonData.replace("}\",", "},");
            jsonData = jsonData.replace("}\"", "}");
            createAndSaveFile(scopeID+"_inAppIds",jsonData);
            saveInAppIds(scopeID+"_inAppIds");
        } catch (MalformedURLException e) {

        } catch (SocketTimeoutException e) {

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }
    public void saveInAppIds(String fileName){
        JSONArray jsonArray;
        String str = "";
        try {
            File file = new File(Globals.TARGET_BASE_FILE_PATH+"groups/"+ fileName + ".json");
            FileInputStream fis = new FileInputStream(file);
            int size = fis.available();
            byte[] buffer = new byte[size];
            fis.read(buffer);
            fis.close();
            String response = new String(buffer);
            jsonArray = new JSONArray(response);
            setJsonMap((ArrayList<Object>) toList(jsonArray));
            saveInAppIdData(getJsonMap());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void saveInAppIdData(ArrayList<Object> groupList){
        if (groupList.size() > 0 ) {
            InAppIds inAppIds = InAppIds.getInstance();
            ArrayList<InappIdData> datas = new ArrayList<>();
            for (int i = 0;i<groupList.size();i++) {
                HashMap<String, String> groupDict = (HashMap<String, String>) groupList.get(i);
                InappIdData data = new InappIdData();
                data.setClientIds(groupDict.get("clientIDs"));
                data.setEndDate(groupDict.get("endDate"));
                data.setInAppIds(groupDict.get("storeIDs"));
                datas.add(data);
            }
            inAppIds.setInAppIdsArrayList(datas);
        }
    }
}
