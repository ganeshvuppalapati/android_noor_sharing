package com.semanoor.manahij;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.semanoor.source_sboookauthor.DirectoryTextViewListAdapter;
import com.semanoor.source_sboookauthor.Globals;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class DirectoryPickerActivity extends Activity {
	
	public static final int PICK_DIRECTORY = 435;
	private File dir;
	private boolean showHidden = false;
	private boolean onlyDirs = true ;
	private ArrayList<File> files;
	private String[] names;
	private ListView listView;
	public static final String CHOSEN_DIRECTORY = "chosenDir";
	public static final String SOURCE_PATH = "sourcePath";
	private String fileSourcePath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_directory_picker);
		
		dir = Environment.getExternalStorageDirectory();
		WindowManager.LayoutParams params = getWindow().getAttributes();
 		params.width = Globals.getDeviceIndependentPixels(450, this);
		params.height = Globals.getDeviceIndependentPixels(400, this);
		this.getWindow().setAttributes(params);
		
		fileSourcePath = getIntent().getExtras().getString(SOURCE_PATH);
		
		Button btnBack = (Button) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String childPath = dir.getParentFile().getAbsolutePath();
				if (dir.equals(Environment.getExternalStorageDirectory())) {
					return;
				}
				String preferredStartDir = childPath;
            	if(preferredStartDir != null) {
                	File startDir = new File(preferredStartDir);
                	if(startDir.isDirectory()) {
                		dir = startDir;
                		reloadFiles();
                	}
                }
			}
		});
		
		Button btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				returnDir(dir.getAbsolutePath());
			}
		});
		
		listView = (ListView) findViewById(R.id.listView1);
		reloadFiles();
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				if(!files.get(position).isDirectory())
        			return;
        		String path = files.get(position).getAbsolutePath();
        		String preferredStartDir = path;
            	if(preferredStartDir != null) {
                	File startDir = new File(preferredStartDir);
                	if(startDir.isDirectory()) {
                		dir = startDir;
                		reloadFiles();
                	}
                }
			}
		});
	}
	
	private void reloadFiles(){
		if(!dir.canRead()) {
        	Context context = getApplicationContext();
        	String msg = "Could not read folder contents.";
        	Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        	toast.show();
        	return;
        }
		files = filter(dir.listFiles(), onlyDirs, showHidden);
		names = names(files);
		listView.setAdapter(new DirectoryTextViewListAdapter(this, names));
	}
	
	private void returnDir(String path) {
    	Intent result = new Intent();
    	result.putExtra(CHOSEN_DIRECTORY, path);
    	result.putExtra(SOURCE_PATH, fileSourcePath);
        setResult(RESULT_OK, result);
    	finish();    	
    }
	
	public ArrayList<File> filter(File[] file_list, boolean onlyDirs, boolean showHidden) {
		ArrayList<File> files = new ArrayList<File>();
		for(File file: file_list) {
			if(onlyDirs && !file.isDirectory())
				continue;
			if(!showHidden && file.isHidden())
				continue;
			files.add(file);
		}
		Collections.sort(files);
		return files;
	}
	
	public String[] names(ArrayList<File> files) {
		String[] names = new String[files.size()];
		int i = 0;
		for(File file: files) {
			names[i] = file.getName();
			i++;
		}
		return names;
	}

}
