package com.semanoor.manahij;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GoogleImageSearchAdapter;
import com.semanoor.source_sboookauthor.GoogleImageSearchDialogWindow;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.ImageLoader;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Krishna on 14-10-2016.
 */
public class ThemeActivity  extends Activity implements View.OnClickListener{

    DatabaseHandler db;
    Globals global;
    Book currentBook;
    GridShelf gridShelf;

    public String userName;
    public String scopeId;
    public int scopeType;
    public String email;
    //String[] categoryList ={"My THEMES","Edit Image"};
    ArrayList<com.semanoor.source_sboookauthor.Category> Category;
    ArrayList<Theme> themesList ;
    LinearLayout li_edit_image;
    int themeId;
    File[] adapterFiles;
    ImageView iv_selectedImage;
    Button btn_add,btn_gallery,btn_camera,btn_search,btn_apply;
    CircleButton btn_back;
    RecyclerView recyclerView;
    public File selectedFile;
    int objUniqueId=2;
    public  int MEDIA_TYPE_IMAGE = 1;
    public  int MEDIA_TYPE_VIDEO = 2;
    public  int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    Uri imageFileUri;
    public String filePath;
    public  int CAPTURE_GALLERY = 1;
    Dialog googleImageSearchDialog;

    private EditText txtSearchText;
    private ListView googleImageListView;
    public TextView totalResultsText;
    private Button prevBtn;
    private Button nextBtn;
    //Old Api
    //private int MAXIMUM_NO_OF_IMAGES = 8;
    //New Api
    private int MAXIMUM_NO_OF_IMAGES = 10;
    private long totalItems;
    public long currentJSONData;
    private int CURRENT_IMAGE_RESULTS = 0;
    String strSearch = null;
    private ArrayList<Object> listImages;
    private ProgressBar imageProgress;


    //New Api
    private String TITLE = "title";
    private String THUMB_URL = "thumbnailLink";
    private String URL = "link";

    private GoogleImageSearchAdapter adapter;
    private ImageLoader imageLoader;
    String themesPath =  Globals.TARGET_BASE_FILE_PATH+"Themes/current/";
    RelativeLayout shelfRootViewLayout,rl_camera,rl_galary,rl_search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadLocale();


        //Remove title Bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        db = DatabaseHandler.getInstance(this);

        global = Globals.getInstance();
        setContentView(R.layout.theme_activity);

        btn_add= (Button) findViewById(R.id.btn_add);
        shelfRootViewLayout = (RelativeLayout) findViewById(R.id.shelfRootViewLayout);
        recyclerView = (RecyclerView) findViewById(R.id.gridView1);
        li_edit_image=(LinearLayout) findViewById(R.id.li_edit_image);
        rl_camera = (RelativeLayout) findViewById(R.id.rl1);
        rl_galary= (RelativeLayout) findViewById(R.id.rl_galary);
        rl_search= (RelativeLayout) findViewById(R.id.rl_search);
        iv_selectedImage= (ImageView) findViewById(R.id.img_view1);
        btn_back= (CircleButton)findViewById(R.id.btn_back);
        btn_camera=(Button)findViewById(R.id.btn_takephoto);
        btn_gallery=(Button)findViewById(R.id.btn_galary);
        btn_search=(Button)findViewById(R.id.btn_search);
        btn_apply=(Button)findViewById(R.id.btn_apply);
        btn_camera.setOnClickListener(this);
        btn_gallery.setOnClickListener(this);
        btn_search.setOnClickListener(this);

        btn_apply.setOnClickListener(this);
        btn_add.setOnClickListener(this);
        btn_back.setOnClickListener(this);

        String query="SELECT * FROM Themes";
        UserFunctions.applyingBackgroundImage(themesPath+"themes_bg.png",shelfRootViewLayout, ThemeActivity.this);
        themesList= db.getThemeDetails(query,ThemeActivity.this);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        //recyclerView.setAdapter(mAdapter);
        ThemeAdapter enr=new ThemeAdapter(ThemeActivity.this);
        filePath= Globals.TARGET_BASE_FILE_PATH+"Themes/";
        recyclerView.setAdapter(enr.new ThemesItemAdapter(recyclerView, ThemeActivity.this, themesList));
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ThemeActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
       // recyclerView.setAdapter(enr.new ThemeShelfAdapter(ThemeActivity.this,Category));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
               /* if(li_edit_image.getVisibility()==View.INVISIBLE) {
                    Theme themes = themesList.get(position);
                    ThemeAdapter enr = new ThemeAdapter(ThemeActivity.this);
                    //themeId= themes.getThemeID();
                    filePath= Globals.TARGET_BASE_FILE_PATH+"Themes/"+themes.getThemeID();
                    //if(themes.getThemeID()!=0) {
                        li_edit_image.setVisibility(View.VISIBLE);
                        btn_add.setVisibility(View.INVISIBLE);
                   // }
                    adapterFiles =getFilesList(filePath);

                    recyclerView.setAdapter(enr.new ThemesListAdapter(recyclerView, ThemeActivity.this, adapterFiles));
                }else{
                    selectedFile=adapterFiles[position];
                    String imageUrl= filePath+"/"+selectedFile.getName();
                    displayingImages(imageUrl);
                } */
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    @Override
    public void onClick(View v) {
        if ( v.getId() == R.id.btn_add) {
            alertDialog();


        }else if(v.getId()==R.id.btn_back){
            onBackPressed();

        }else if(v.getId()==R.id.btn_takephoto) {
            callCameraToTakePhoto();

        } else if(v.getId()==R.id.btn_galary) {
            takePhotoFromGallery();

        }else if(v.getId()==R.id.btn_search) {
            GoogleImageSearchDialogWindow googleImgSearchView = new GoogleImageSearchDialogWindow(ThemeActivity.this,null);
            googleImgSearchView.callDialogWindow();

        }else if(v.getId()==R.id.btn_apply) {
            //UserFunctions.copyDirectory();
            String srcStoreBookFreefilesDir = filePath;
            String destCreatedBookFreefilesDir =  Globals.TARGET_BASE_FILE_PATH+"Themes/current/";
            try {
                UserFunctions.copyDirectory(new File(srcStoreBookFreefilesDir), new File(destCreatedBookFreefilesDir));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(ThemeActivity.this,getResources().getString(R.string.applied),Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onBackPressed() {
        if(li_edit_image.getVisibility()==View.INVISIBLE) {
            //String[] path= filePath.split(Globals.TARGET_BASE_FILE_PATH+"Themes/");
            if(filePath.equals(Globals.TARGET_BASE_FILE_PATH+"Themes/")){
                finish();
            }else{
                filePath=Globals.TARGET_BASE_FILE_PATH+"Themes/";
                li_edit_image.setVisibility(View.INVISIBLE);
                btn_add.setVisibility(View.VISIBLE);
                ThemeAdapter enr=new ThemeAdapter(ThemeActivity.this);
                recyclerView.setAdapter(enr.new ThemesItemAdapter(recyclerView, ThemeActivity.this, themesList));
            }
          //  UserFunctions.applyingBackgroundImage(themesPath+"themes_bg.png",shelfRootViewLayout);
        }else{
            filePath=Globals.TARGET_BASE_FILE_PATH+"Themes/";
            li_edit_image.setVisibility(View.INVISIBLE);
            btn_add.setVisibility(View.VISIBLE);
            ThemeAdapter enr=new ThemeAdapter(ThemeActivity.this);
            recyclerView.setAdapter(enr.new ThemesItemAdapter(recyclerView, ThemeActivity.this, themesList));

        }
        UserFunctions.applyingBackgroundImage(themesPath+"themes_bg.png",shelfRootViewLayout, ThemeActivity.this);
    }

    public void callCameraToTakePhoto(){

        //Checking camera availability
        if(!isDeviceSupportCamera()){
            Toast.makeText(ThemeActivity.this, "Sorry! Your device doesn't support camera", Toast.LENGTH_LONG).show();
        }else{

            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            //System.out.println("The image path: "+this.objPathContent);


            imageFileUri = Uri.fromFile(getOutputMediaFile(MEDIA_TYPE_IMAGE));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }
    private boolean isDeviceSupportCamera(){
        if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return true;
        }else{
            return false;
        }
    }
    /**
     *  Creates path to save camera image
     */
    private File getOutputMediaFile(int type){

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "SboookAuthor");
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("CamCamera", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String ImageName =""+objUniqueId;
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +"image_"+ ImageName + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +"VID_"+ ImageName + ".mp4");
        }
        else {
            return null;
        }
        return mediaFile;
    }


    private void loadLocale() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String language = prefs.getString(Globals.languagePrefsKey, "en");
        scopeId = prefs.getString(Globals.sUserIdKey, "");
        userName = prefs.getString(Globals.sUserNameKey, "");
        scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
        email = prefs.getString(Globals.sUserEmailIdKey, "");
        // email="krishna@semasoft.co.in";
        //changeLang(language);
    }
    public File[] getFilesList(String filePath) {
        File f = new File(filePath);
        return f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return !file.isHidden();
            }
        });
    }

    public void displayingImages(String imageUrl){
        Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);
        iv_selectedImage.setImageBitmap(bitmap);
        /*Glide.with(ThemeActivity.this).load(imageUrl)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(200,200)
                .into(iv_selectedImage);*/
    }

    public void alertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ThemeActivity.this);
        builder.setTitle(getResources().getString(R.string.theme));
        builder.setMessage(getResources().getString(R.string.enter_themeName));
        builder.setCancelable(false);
        LayoutInflater inflator =(ThemeActivity.this).getLayoutInflater();
        View dialogView=inflator.inflate(R.layout.grpdialogwindow,null);
        builder.setView(dialogView);
        final EditText et_name = (EditText)dialogView.findViewById(R.id.et_grpname);
        builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String grpname = et_name.getText().toString();
                boolean exist = false;
                if (!grpname.equals("") && grpname.length() > 0) {
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    String formattedDate = df.format(c.getTime());
                    String foregroundColor="1-1-1";
                    String query = "insert into Themes(themeName,createDate,foregroundColor,category) values('"+grpname+"','"+formattedDate+"','"+foregroundColor+"','"+1+"')";
                    db.executeQuery(query);
                    int Id = db.getMaxUniqueRowID("Themes");
                    int folderId=Id;
                    String newFolder= Globals.TARGET_BASE_FILE_PATH+"Themes/"+Id;
                    UserFunctions.createNewDirectory(newFolder);
                    String srcStoreBookFreefilesDir =  Globals.TARGET_BASE_FILE_PATH+"Themes/0";
                    String destCreatedBookFreefilesDir =  Globals.TARGET_BASE_FILE_PATH+"Themes/"+folderId+"/";
                    try {
                        UserFunctions.copyDirectory(new File(srcStoreBookFreefilesDir), new File(destCreatedBookFreefilesDir));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //UserFunctions.copyTemplatesDirFromAssetToFilesDir(ThemeActivity.this, Globals.TARGET_BASE_FILE_PATH+"Themes/"+folderId+"/", "themes/0");
                    Theme themeData=new Theme(ThemeActivity.this);
                    themeData.setThemeID(Id);
                    themeData.setThemeName(grpname);
                    themeData.setCreateDate(formattedDate);
                    //themeData.setCategory(category);
                    themeData.setForeGroundColor(foregroundColor);
                    themesList.add(themeData);
                    ThemeAdapter enr=new ThemeAdapter(ThemeActivity.this);
                    recyclerView.setAdapter(enr.new ThemesItemAdapter(recyclerView, ThemeActivity.this, themesList));

                    dialog.cancel();
                }
            }
        });
        builder.setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();

        // Dialog dialog=builder.create();
        //dialog.getWindow().setBackgroundDrawable(R.color.bookview_bluecolor);
        alert.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                SaveUriFile(imageFileUri.getPath());

            } else if (resultCode == RESULT_CANCELED) {
                //System.out.println("User cancelled the image capture");
            } else {
                //System.out.println("Image capture failed");
            }
        }else if (requestCode == CAPTURE_GALLERY) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
				 String picturePath = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
                if (picturePath != null) {
                    SaveUriFile(picturePath);
                }
            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }
       /**
     * Replaces the image in the files folder.
     */
    public void SaveUriFile(String sourceUri) {
        Bitmap b = BitmapFactory.decodeFile(sourceUri);
        //String fileName=selectedFile.getName().replace("png","jpg");
        if(b!=null) {
           // if (selectedFile.getName().contains("Shelf_bg.jpg")) {
                String cardImage = filePath + "/."+selectedFile.getName();
                Bitmap cardBitmap = Bitmap.createScaledBitmap(b, Globals.getDeviceIndependentPixels(200, ThemeActivity.this), Globals.getDeviceIndependentPixels(300, ThemeActivity.this), true);
                UserFunctions.saveBitmapImage(cardBitmap, cardImage);

           // }
            String imageUrl = filePath + "/" + selectedFile.getName();
            // String absPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
            //String path= absPath+"/Download/"+selectedFile.getName();
            Bitmap frontThumbBitmap = Bitmap.createScaledBitmap(b, Globals.getDeviceWidth(), Globals.getDeviceHeight(), true);
            UserFunctions.saveBitmapImage(frontThumbBitmap, imageUrl);
            ThemeAdapter enr = new ThemeAdapter(ThemeActivity.this);
            recyclerView.setAdapter(enr.new ThemesListAdapter(recyclerView, ThemeActivity.this, adapterFiles));
            displayingImages(imageUrl);
        }

    }

    /**
     * @author Take photo from gallery
     *
     */
    public void takePhotoFromGallery(){
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, CAPTURE_GALLERY);
    }
}
