package com.semanoor.manahij;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


public class FileManagerActivity extends Activity implements View.OnClickListener {

    public static String filesDirPath =Globals.TARGET_BASE_FILE_PATH+"UserLibrary";
    GridView gridView;
    public RelativeLayout rl_tab,rl_btn_layout;
    public LinearLayout ll_container;
    String currentFilePath;
    int i = 1;
    String SelectedFile;
    Book currentBook;
    Button btn_home, addFolder, addImage, btn_Copy_File, btn_paste_File, btn_Delete_File, btn_Cut_File,btn_done,btn_refresh,btn_cleartext,btn_Search,btn_media,btn_media_import,btn_media_next,btn_media_prev,btn_back;
    int pick_up_image = 1;
    int VIDEO_GALLERY=2;
    int pick_Audio=3;
    String copiedPath;
    String fileName;
    String FilePath;
    boolean cutOperation;
    UserFunctions userFunctions;
    String copiedFileName;
    boolean multipleSelection;
    File[] adapterFiles;
    ArrayList<ClickedFilePath> folders = new ArrayList<ClickedFilePath>();
    ArrayList<ClickedFilePath> selectedFolders = new ArrayList<ClickedFilePath>();
    public BookViewActivity context;
    String currentPageNumber;
    int enrichId;
    DatabaseHandler db;
    public BookViewActivity bookViewActivity;
    private boolean SearchMode;
    EditText et_search;
    ArrayList<String> searchArray = new ArrayList<String>();
    HorizontalScrollView btnScrollView;
    RelativeLayout search_layout;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 101;
    RelativeLayout rl_mymedia;
    private WebService webService;
    private ArrayList<HashMap<String, String>> catHashMapList;
    boolean importMedia;
    TextView tv_pageNo;
    int pageNo=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.filesmanager);

        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
        ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

        UserFunctions.changeFont(rootView,font);
        currentBook = (Book) getIntent().getSerializableExtra("Book");
       // currentPageNumber=getIntent().getStringExtra("pageNo");
        //enrichId=getIntent().getIntExtra("enrichId", 1);
       // bookViewActivity=(BookViewActivity) getIntent().getParcelableExtra("BookViewActivity");

        db = DatabaseHandler.getInstance(this);
        userFunctions = UserFunctions.getInstance();
        //bookViewActivity=new BookViewActivity();
        gridView = (GridView) findViewById(R.id.gridView);
        rl_tab = (RelativeLayout) findViewById(R.id.rl_tabs_layout);
        ll_container = (LinearLayout) findViewById(R.id.ll_container);
        btn_home = (Button) findViewById(R.id.btnEnrHome);
        search_layout = (RelativeLayout)findViewById(R.id.search_layout);
        btnScrollView=(HorizontalScrollView)findViewById(R.id.btnScrollView);
        rl_mymedia= (RelativeLayout) findViewById(R.id.rl_mymedia);
        rl_btn_layout= (RelativeLayout) findViewById(R.id.toolBar_view);


        addFolder = (Button) findViewById(R.id.btn_add_folder);
        addImage = (Button) findViewById(R.id.btn_add_image);
        btn_Copy_File = (Button) findViewById(R.id.btn_copy_file);
        btn_Cut_File = (Button) findViewById(R.id.btn_cut_file);
        btn_paste_File = (Button) findViewById(R.id.btn_paste);
        btn_Delete_File = (Button) findViewById(R.id.btn_delete);
        btn_done = (Button) findViewById(R.id.btn_done);
        btn_back=(Button)findViewById(R.id.btn_back);
        btn_refresh=(Button)findViewById(R.id.btn_refresh);
        et_search = (EditText) findViewById(R.id.search_box);
        btn_cleartext=(Button) findViewById(R.id.btn_cleartext);
        btn_Search=(Button) findViewById(R.id.btn_search);
        btn_media=(Button) findViewById(R.id.btn_media);
        btn_media_import=(Button) findViewById(R.id.btn_media_import);
        btn_media_next=(Button) findViewById(R.id.btn_next);
        btn_media_prev=(Button) findViewById(R.id.btn_prev);
        tv_pageNo=(TextView) findViewById(R.id.tv_pageNo);
        //btn_cleartext.setVisibility(View.INVISIBLE);

        btn_home.setOnClickListener(this);
        addFolder.setOnClickListener(this);
        addImage.setOnClickListener(this);
        btn_Copy_File.setOnClickListener(this);
        btn_Cut_File.setOnClickListener(this);
        btn_paste_File.setOnClickListener(this);
        btn_Delete_File.setOnClickListener(this);
        btn_paste_File.setAlpha(0.5f);
        btn_done.setOnClickListener(this);
        btn_refresh.setOnClickListener(this);
        btn_Search.setOnClickListener(this);
        btn_cleartext.setOnClickListener(this);
        btn_media.setOnClickListener(this);
        btn_media_import.setOnClickListener(this);
        btn_media_next.setOnClickListener(this);
        btn_media_prev.setOnClickListener(this);
        btn_back.setOnClickListener(this);

        String[] fullname = filesDirPath.split("/");
       // btn_home.setText(fullname[fullname.length - 1]);
        btn_home.setText("My Library");
        currentFilePath = filesDirPath;

        //enrichments.setEnrichmentExportValue(0);
       // makeEnrichedBtnSlected(btn_home);
       if(!importMedia) {
           gridView.setAdapter(new FileManagerGridAdapter(filesDirPath));
       }
       // searchEditText.addTextChangedListener(this);

        et_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_search.getText().toString().equals("")) {
                    SearchMode = false;
                    gridView.setAdapter(new FileManagerGridAdapter(currentFilePath));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (et_search.getText().toString().equals("")) {
                    btn_cleartext.setVisibility(View.GONE);
                } else {
                    btn_cleartext.setVisibility(View.VISIBLE);
                }

            }
        });


        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {


                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            //clearSearchBox.setVisibility(View.VISIBLE);
                            //inSearchMode = true;
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                if (!et_search.getText().toString().equals("")) {
                                    if(searchArray.size()>0){
                                        searchArray.clear();
                                    }
                                    for (int i = 0; i < adapterFiles.length; i++) {
                                        String fileContent = adapterFiles[i].getName();
                                        if (fileContent.toLowerCase().contains(et_search.getText().toString().toLowerCase())) {
                                            searchArray.add(adapterFiles[i].getName());
                                        }
                                    }
                                    SearchMode = true;
                                    gridView.setAdapter(new FileManagerGridAdapter(filesDirPath));

                                }
                            }
                            return false;
                        }
                    });


        /*
         *Multiple selection on file manager
         */
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (!importMedia) {
                    if (multipleSelection) {
                        multipleSelection = false;
                        btn_done.setVisibility(View.GONE);
                        btn_back.setVisibility(View.VISIBLE);
                    } else {
                        multipleSelection = true;
                        btn_done.setVisibility(View.VISIBLE);
                        btn_back.setVisibility(View.GONE);
                    }
                    //btn_done.setVisibility(View.INVISIBLE);
                    SelectedFile = i + "";
                    gridView.invalidateViews();
                }else{

                }
                return false;
             }
        });

                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                            //File mainFile = new File(currentFilePath);
                            //  File[] files = mainFile.listFiles();
                            if (!importMedia) {
                                RelativeLayout rl_imageLayout = (RelativeLayout) view;

                                if (multipleSelection) {
                                    ClickedFilePath folder = folders.get(position);
                                    if (folder.isFolderSelected()) {
                                        folder.setFolderSelected(false);
                                        selectedFolders.remove(folder);
                                        ((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
                                        //enrichmentSelectedList.remove(enrichment);
                                    } else {
                                        folder.setFolderSelected(true);
                                        selectedFolders.add(folder);
                                        ((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
                                        // enrichmentSelectedList.add(enrichment);
                                    }

                                } else {
                                    if (adapterFiles[position].isDirectory()) {
                                        String path = adapterFiles[position].getPath();
                                        String[] fullname = path.split("/");
                                        ClickedFilePath folder = new ClickedFilePath(FileManagerActivity.this);
                                        i = i + 1;


                                        folder.setFolderId(i);
                                        folder.setFolderTitle(fullname[fullname.length - 1]);
                                        folder.setFolderPath(path);
                                        folder.setFolderSelected(true);
                                        //Button enrichBtnCreated = folder.createFolderTabs();
                                       // makeEnrichedBtnSlected(enrichBtnCreated);
                                        //makeEnrichedBtnSlected(btnEnr);
                                        SelectedFile = String.valueOf(position);
                                        SearchMode = false;
                                        btn_home.setText("> "+fullname[fullname.length - 1]);
                                        gridView.setAdapter(new FileManagerGridAdapter(path));
                                        currentFilePath = path;
                                        // SelectedFile = position;
                                        gridView.invalidate();
                                        if (search_layout.isShown()) {
                                            btnScrollView.setVisibility(View.VISIBLE);
                                            search_layout.setVisibility(View.GONE);
                                        }
                                    } else {
                                        // if(files[position].toString().contains(".png")||files[position].toString().contains("jpg")||files[position].toString().contains(".mp4")) {
                                        if (adapterFiles[position].isFile()) {
                                            SelectedFile = String.valueOf(position);
                                            ((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
                                            // }
                                        }
                                    }
                                }
                            } else {
                                HashMap<String, String> map = catHashMapList.get(position);
                                map.get("Media");
                                MyFileManager mFileManager = new MyFileManager(filesDirPath, FileManagerActivity.this);
                                mFileManager.new progressDownload().execute(map.get("Media"));
                            }
                        }


        });

    }


    /*
     *Displaying files for selected button
     */
    public void DisplayingFiles(Button v) {
        makeEnrichedBtnSlected(v);
        ClickedFilePath enrich = (ClickedFilePath) v.getTag();
        gridView.setAdapter(new FileManagerGridAdapter(enrich.getFolderPath()));
        currentFilePath = enrich.getFolderPath();
        gridView.invalidate();
//        ll_container.removeViews(1,ll_container.getChildCount()-1);
//        i=ll_container.getChildCount();
        for (int j = 0; j < ll_container.getChildCount(); j++) {
            if (j == 0) {
                Button btnEnr = (Button) ll_container.getChildAt(j);
            } else {
                RelativeLayout rlview = (RelativeLayout) ll_container.getChildAt(j);
                Button btnEnrTab = (Button) rlview.getChildAt(0);
                final ClickedFilePath enriche = (ClickedFilePath) btnEnrTab.getTag();
                if (enriche.getFolderId() == enrich.getFolderId()) {
                    ll_container.removeViews(enriche.getFolderId(), ll_container.getChildCount() - enriche.getFolderId());
                    i = ll_container.getChildCount();
                    break;
                }
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:{
                onBackPressed();
                break;
            }
            case R.id.btnEnrHome: {
               // makeEnrichedBtnSlected(v);
                String path = currentFilePath;
                String[] fullname = path.split("/");
                if(currentFilePath.equals(filesDirPath)) {
                    gridView.setAdapter(new FileManagerGridAdapter(filesDirPath));
                    currentFilePath = filesDirPath;
                    btn_home.setText("My Library");
                    gridView.invalidate();
                }else {
                    String uniquePath = null;
                   // String path = currentFilePath;
                  //  String[] fullname = path.split("/");
                   String folderPath[]=currentFilePath.split("/");
                    for(int i=0;i<folderPath.length;i++) {
                        if(i==0) {
                            uniquePath = folderPath[0];
                        }else if(i!=folderPath.length-1){
                            uniquePath=uniquePath+"/"+folderPath[i];
                        }
                    }
                    gridView.setAdapter(new FileManagerGridAdapter(uniquePath));
                    currentFilePath = uniquePath;
                    String title[]=uniquePath.split("/");
                    if(currentFilePath.equals(filesDirPath)) {
                        btn_home.setText("My Library");
                    }else {
                        btn_home.setText("> "+title[title.length - 1]);
                    }
                    gridView.invalidate();
                }
                //ll_container.removeViews(1, ll_container.getChildCount() - 1);
               // i = ll_container.getChildCount();

                break;
            }
            case R.id.btn_add_folder: {
                MyFileManager mFileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
                mFileManager.showFolderDialog();

                break;
            }
            case R.id.btn_add_image: {
                MyFileManager mFileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
                mFileManager.showGalleryDialog();
                break;
            }
            case R.id.btn_copy_file: {
                if (multipleSelection) {
                    if (selectedFolders.size() > 0) {
                        MyFileManager mFileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
                        mFileManager.multipleFileOperation(true, false, false, false);
                        btn_paste_File.setAlpha(1.0f);
//                        btn_done.setVisibility(View.GONE);
//                        btn_back.setVisibility(View.VISIBLE);
                    }
                } else {
                    if(copiedPath!=null&&SelectedFile!=null) {
                        MyFileManager mFileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
                        mFileManager.FileOperation(true, false, false, false);
                        btn_paste_File.setAlpha(1.0f);
//                        btn_done.setVisibility(View.GONE);
                    }

                }
                btn_done.setVisibility(View.GONE);
                btn_back.setVisibility(View.VISIBLE);
                //btn_paste_File.setEnabled(true);
                break;
            }
            case R.id.btn_cut_file: {
                if (multipleSelection) {
                    if (selectedFolders.size() > 0) {
                        MyFileManager mFileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
                        mFileManager.multipleFileOperation(false, true, false, false);
                        btn_paste_File.setAlpha(1.0f);
                      //  btn_done.setVisibility(View.GONE);
                    }
                } else {
                    if(copiedPath!=null&&SelectedFile!=null) {
                        MyFileManager mFileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
                        mFileManager.FileOperation(false, true, false, false);
                        btn_paste_File.setAlpha(1.0f);
                       // btn_done.setVisibility(View.GONE);
                    }
                }
                btn_done.setVisibility(View.GONE);
                btn_back.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.btn_paste: {
                if (selectedFolders.size() > 0) {
                    if (selectedFolders.size() > 0) {
                        MyFileManager mFileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
                        mFileManager.multipleFileOperation(false, false, true, false);
                        btn_paste_File.setAlpha(0.5f);
                    }
                } else {
                    if(copiedPath!=null) {
                        MyFileManager mFileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
                        mFileManager.FileOperation(false, false, true, false);
                        btn_paste_File.setAlpha(0.5f);;
                    }
                }

                break;
            }
            case R.id.btn_delete: {
                if (selectedFolders.size() > 0 ||copiedPath != null && SelectedFile != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (multipleSelection) {
                                MyFileManager mFileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
                                mFileManager.multipleFileOperation(false, false, false, true);
                               // btn_done.setVisibility(View.GONE);

                            } else {
                                MyFileManager mFileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
                                mFileManager.FileOperation(false, false, false, true);
                               // btn_done.setVisibility(View.GONE);

                            }
                            btn_done.setVisibility(View.GONE);
                            btn_back.setVisibility(View.VISIBLE);
                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.setTitle(R.string.delete);
                            builder.setMessage(R.string.suredelete);
                            builder.show();
                        }
                        break;
                    }
                    case R.id.btn_done: {
                        if (multipleSelection) {
                            multipleSelection = false;
                            btn_done.setVisibility(View.GONE);
                            btn_back.setVisibility(View.VISIBLE);
                        }
                        if (selectedFolders.size() > 0) {
                            selectedFolders.clear();
                        }
                        for (ClickedFilePath files : folders) {
                            if (files.isFolderSelected()) {
                                files.setFolderSelected(false);
                            }
                        }
                        //gridView.setAdapter(new FileManagerGridAdapter(currentFilePath));
                        gridView.invalidateViews();
                        break;
                    }
                    case R.id.btn_refresh: {
                        SearchMode = false;
                        gridView.setAdapter(new FileManagerGridAdapter(currentFilePath));
                        break;
                    }
                    case R.id.btn_search: {
                        if (btnScrollView.isShown()) {
                            btnScrollView.setVisibility(View.GONE);
                            search_layout.setVisibility(View.VISIBLE);
                        } else {
                            btnScrollView.setVisibility(View.VISIBLE);
                            search_layout.setVisibility(View.GONE);
                        }
                        break;
                    }
                    case R.id.btn_cleartext:{
                        SearchMode = false;
                        et_search.getText().clear();
                        btn_cleartext.setVisibility(View.GONE);
                        gridView.setAdapter(new FileManagerGridAdapter(currentFilePath));
                        break;
                    }
                    case R.id.btn_media:{
                        if(rl_mymedia.isShown()){
                            if(importMedia) {
                                importMedia = false;
                            }
                            if(!importMedia) {
                                gridView.setAdapter(new FileManagerGridAdapter(filesDirPath));
                            }
                            rl_mymedia.setVisibility(View.GONE);
                            rl_btn_layout.setVisibility(View.VISIBLE);
                        }else{
                            /*if(!importMedia) {
                                importMedia = true;
                            }*/
                          //  gridView.setAdapter(new FileManagerGridAdapter(""));
                            rl_mymedia.setVisibility(View.VISIBLE);
                            rl_btn_layout.setVisibility(View.GONE);
                        }
                        break;
                    }
                  case R.id.btn_media_import:{
                      pageNo=1;
                      tv_pageNo.setText(pageNo+"");
                      new importMediaLibrary(pageNo).execute();

                      break;
                   }
                 case R.id.btn_next:{
                     pageNo=pageNo+1;
                     tv_pageNo.setText(pageNo+"");
                   new importMediaLibrary(pageNo).execute();
                     break;
                   }
                 case R.id.btn_prev:{
                    if(pageNo==1) {
                       // pageNo = 1;
                        tv_pageNo.setText(pageNo+"");
                    }else{
                        pageNo = pageNo - 1;
                        tv_pageNo.setText(pageNo+"");
                        new importMediaLibrary(pageNo).execute();
                    }
                     break;
                   }
                    default:
                        break;
                }
            }

    private class importMediaLibrary extends AsyncTask<Void, Void, Void> {
         int pageNumber;

        public importMediaLibrary(int pageNo) {
            pageNumber=pageNo;
        }

        @Override
        protected Void doInBackground(Void... params) {
            webService = new WebService(FileManagerActivity.this, Globals.mediaLibraryWebserviceURL);
            // webService.GetMediaLibraryMediaByUserID();
            webService.GetMediaLibraryMediaByUserID("3", pageNumber, 10, "MImage");
            // webService. GetMediaLibraryMediaByUserID1("3",1,10 ,"MImage");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            String response = webService.importMediaResponseResult;
           /* if(response==null){
                response="{\"totalcount\":\"56\",\"DoneDateTime\":\"2016-02-03T00:00:00\",\"Data\":\"[{\"Price\":null,\"ID\":3,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/3/Thumb/6b2db656-178f-4742-a36b-0fa56a59fe8c.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/3_0a06d031-b753-4a37-81c9-a750d7ac64c6.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":106,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0,\"MediaUnLike\":0,\"UserName\":\"emad\",\"TotalCount\":56}]\",\"ErrorState\":\"0\"}";
            }*/
            if (response != null) {
                catHashMapList = new ArrayList<HashMap<String, String>>();
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String totalCount = jsonObj.getString("totalcount");
                    String doneDateTime = jsonObj.getString("DoneDateTime");
                    String data = jsonObj.getString("Data");
                    //data = data.replace("\"", "");
                    String errorState = jsonObj.getString("ErrorState");
                    //String[] splitData = data.split("[");
                    data=data.replace("}","}#");
                    data=data.replace("[","");
                    data=data.replace("]","");
                    String[] jsonData=data.split("#");
                   // String splitData = data.replace("[","");
                   for(int i=0;i<jsonData.length;i++) {
                        if(!jsonData[i].equals("") ||!jsonData[i].equals("]")) {
                            String xml = jsonData[i].replace(",{","{");
                            //char d=xml.charAt(918);
                           // System.out.println(d);
                            JSONObject jsonObj2 = new JSONObject(xml);
                            //  String[] dataContent = splitData[i].split(",");
                            // for(int j=0;j<dataContent.length;j++) {
                            //  String d=splitData[i];
                            //   JSONObject jsonObj1 = new JSONObject(splitData[i]);
                            String titleAr1 = jsonObj2.getString("TitleAr");
                            String titleEn1 = jsonObj2.getString("TitleEn");
                            String ThumbImage1 = jsonObj2.getString("ThumbName");
                            String downloadFile1 = jsonObj2.getString("Media");
                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("TitleAr", titleAr1);
                            map.put("TitleEn", titleEn1);
                            map.put("ThumbName", ThumbImage1);
                            map.put("Media", downloadFile1);
                            catHashMapList.add(map);
                        }
                    }
                      } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else{
                jsonObject();
            }
            if(catHashMapList.size()>0) {
                //MyFileManager mFileManager = new MyFileManager("", FileManagerActivity.this);
                importMedia = true;
                gridView.setAdapter(new importMedia(catHashMapList));
                btn_media_next.setVisibility(View.VISIBLE);
                btn_media_prev.setVisibility(View.VISIBLE);
                tv_pageNo.setVisibility(View.VISIBLE);
            }
           // super.onPostExecute(aVoid);


        }
    }
    public void jsonObject() {
        catHashMapList = new ArrayList<HashMap<String, String>>();
        String data = "{\"Price\":null,\"ID\":3,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/3/Thumb/6b2db656-178f-4742-a36b-0fa56a59fe8c.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/3_0a06d031-b753-4a37-81c9-a750d7ac64c6.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":116,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0,\"MediaUnLike\":0,\"UserName\":\"emad\",\"TotalCount\":56}{\"Price\":null,\"ID\":4,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/4/Thumb/0a457c83-cb3b-48ca-a580-116a7994fd04.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/4_15ed93e3-d7bf-4633-a789-7265c4aab09e.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":92,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0,\"MediaUnLike\":0,\"UserName\":\"emad\",\"TotalCount\":56},{\"Price\":null,\"ID\":5,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/5/Thumb/a5afaf37-b871-45be-ac82-ecef313c5508.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/5_3037901f-097d-4fea-a726-6fa924095603.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":93,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0,\"MediaUnLike\":0,\"UserName\":\"emad\",\"TotalCount\":56},{\"Price\":null,\"ID\":6,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/6/Thumb/ef0b6d57-4cc6-45bd-bd30-c83e63148c79.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/6_11c1c965-30f1-4ede-832d-888b6faa70d1.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":92,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0,\"MediaUnLike\":0,\"UserName\":\"emad\",\"TotalCount\":56},{\"Price\":null,\"ID\":7,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/7/Thumb/683592bb-7b6c-4dfe-8a86-3042272097b5.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/7_d734d295-198a-46ac-b210-f46aecabefbc.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":65,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0,\"MediaUnLike\":0,\"UserName\":\"emad\",\"TotalCount\":56},{\"Price\":null,\"ID\":8,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/8/Thumb/de9063d4-e629-4e8e-990d-9dc98c6b0665.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/8_96da69db-299e-4924-bdd4-7428acce9f72.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":65,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0,\"MediaUnLike\":0,\"UserName\":\"emad\",\"TotalCount\":56},{\"Price\":null,\"ID\":9,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/9/Thumb/8c3d11cd-e632-4d22-9ff5-8242f9d0bb22.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/9_2caf5927-af32-4b6a-8113-e506432e392e.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":64,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0,\"MediaUnLike\":0,\"UserName\":\"emad\",\"TotalCount\":56},{\"Price\":null,\"ID\":10,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/10/Thumb/c9eab05d-30da-497a-9582-147909ed441c.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/10_622feb08-e66c-4f9d-a61f-2f84476bdf03.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":64,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0,\"MediaUnLike\":0,\"UserName\":\"emad\",\"TotalCount\":56},{\"Price\":null,\"ID\":11,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/11/Thumb/b3b76db7-9903-4715-8631-5a79efc31732.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/11_331dc06a-99a3-4dcf-86dc-205415cb91a5.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":62,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0,\"MediaUnLike\":0,\"UserName\":\"emad\",\"TotalCount\":56},{\"Price\":null,\"ID\":12,\"TitleAr\":\"تحميل مجموعة وسائط \",\"TitleEn\":\"تحميل مجموعة وسائط \",\"ThumbName\":\"http://dev1.semanoor.com/M.Sbook/Librarystorage/UserMedia/3/6/12/Thumb/bda5a4a6-4715-4bc6-a177-389ab0c23439.png\",\"StatePublish\":true,\"StateDelete\":false,\"StateRating\":true,\"StateEmbedding\":true,\"StateComments\":true,\"StateMature\":true,\"StateFeature\":true,\"StateFeatureAdmin\":false,\"UserID\":3,\"Description\":\"تحميل مجموعة وسائط \",\"Media\":\"http://dev1.semanoor.com/M.Sbook/M.SbookApi/DownloadTemp/12_745a34dc-0942-4e50-a55e-149c0d99c650.zip\",\"Tags\":\"تحميل مجموعة وسائط \",\"Date\":\"2015-12-22T02:06:25\",\"ViewCount\":62,\"SizeOrCount\":0.0,\"IsPackage\":false,\"CatID\":6,\"CatTitleAr\":\"صور\",\"CatTitleEn\":\"Images\",\"FullName\":\"Emad\",\"UserType\":\"mumbers\",\"PermissionView\":true,\"PermissionUpload\":true,\"RateResult\":0,\"MediaType\":\"Image\",\"MediaTypeAr\":\"صورة\",\"MediaTypeID\":12,\"MediaLike\":0," +
                "\"MediaUnLike\":0,\"UserName\":\"emad\"," +
                "\"TotalCount\":56}";
        try {
            data = data.replace("}", "}#");
            data = data.replace("[", "");
            data = data.replace("]", "");
            String[] jsonData = data.split("#");
            //String[] jsonData=data.split("#");
            for (int i = 0; i < jsonData.length; i++) {
                if (!jsonData[i].equals("") || !jsonData[i].equals("]")) {
                    String xml = jsonData[i].replace(",{", "{");
                    //char d=xml.charAt(918);
                    // System.out.println(d);
                    JSONObject jsonObj2 = new JSONObject(xml);
                    // JSONObject jsonObj2 = new JSONObject(r);
                    //  String[] dataContent = splitData[i].split(",");
                    // for(int j=0;j<dataContent.length;j++) {
                    //  String d=splitData[i];
                    //   JSONObject jsonObj1 = new JSONObject(splitData[i]);
                    String titleAr1 = jsonObj2.getString("TitleAr");
                    String titleEn1 = jsonObj2.getString("TitleEn");
                    String ThumbImage1 = jsonObj2.getString("ThumbName");
                    String downloadFile1 = jsonObj2.getString("Media");
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("TitleAr", titleAr1);
                    map.put("TitleEn", titleEn1);
                    map.put("ThumbName", ThumbImage1);
                    map.put("Media", downloadFile1);
                    catHashMapList.add(map);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public class FileManagerGridAdapter extends BaseAdapter {

        String[] filesList;

        String filePath;

                public FileManagerGridAdapter(String Filepath) {
                    filePath = Filepath;
                  // if(!importMedia) {
                        MyFileManager mFileManager = new MyFileManager(filePath, FileManagerActivity.this);
                        if (SearchMode) {
                            adapterFiles = mFileManager.getSearchFilesList(searchArray);
                        } else {
                            adapterFiles = mFileManager.getFilesList();
                        }
                //  }else{

                // }

            /*File[] unHiddenFiles=f.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return !file.isHidden();
                }
            });*/
        }

        @Override
        public int getCount() {
           // if(!importMedia) {
                return adapterFiles.length;
//            }else {
//                return catHashMapList.size();
//            }
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.filesandfolders, null);
            }
            RelativeLayout rl_imageLayout = (RelativeLayout) view.findViewById(R.id.rl_imageLayout);
            final ImageView imgView = (ImageView) view.findViewById(R.id.imageView1);
            TextView textView = (TextView) view.findViewById(R.id.text);
            Button btn_preview = (Button) view.findViewById(R.id.btn_info);
            Button btn_add = (Button) view.findViewById(R.id.btn_add_image);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            btn_preview.setVisibility(View.INVISIBLE);
            btn_add.setVisibility(View.INVISIBLE);
            imgView.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);
            imgView.setBackgroundResource(0);
          //if(!importMedia) {
                btn_preview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // if (!multipleSelection) {
                        if (adapterFiles[position].toString().contains(".png") || adapterFiles[position].toString().contains("jpg") || adapterFiles[position].toString().contains(".gif")) {
                            // Image
                            MyFileManager mFileManager = new MyFileManager(filePath, FileManagerActivity.this);
                            mFileManager.ImagePreview(adapterFiles[position]);

                        } else if (adapterFiles[position].toString().contains(".mp4") || adapterFiles[position].toString().contains(".mkv") || adapterFiles[position].toString().contains(".3gp") || fileName.contains(".avi") || fileName.contains(".flv")) {
                            // VideoView
                            Intent ExportIntent = new Intent(FileManagerActivity.this, Preview_video.class);
                            ExportIntent.putExtra("Path", adapterFiles[position].getAbsolutePath());
                            startActivity(ExportIntent);

                        } else if (adapterFiles[position].toString().contains(".mp3") || adapterFiles[position].toString().contains(".m4a")) {
                            // Audio
                            MyFileManager mFileManager = new MyFileManager(filePath, FileManagerActivity.this);
                            mFileManager.audioPreview(adapterFiles[position]);
                        }
                    }
                    // }
                });

                btn_add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MyFileManager mFileManager = new MyFileManager(filePath, FileManagerActivity.this);
                        mFileManager.addingImagesInCurrentPage(adapterFiles[position]);
                        //   db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+objType+"', '"+objBID+"', '"+objPageNo+"', '"+location+"', '"+size+"', '"+objContent.replace("'", "''")+"', '"+objSequentialId+"', '"+objSclaPageToFit+"', '"+objLocked+"', '"+page.enrichedPageId+"')");

                    }
                });
             /*
              * For Single Selection of files & Folders
              */
                if (SelectedFile != null && position == Integer.parseInt(SelectedFile) && !multipleSelection) {
                    rl_imageLayout.setBackgroundColor(Color.parseColor("#604e3634"));

                    if (adapterFiles[position].toString().contains(".png") || adapterFiles[position].toString().contains(".jpg") || adapterFiles[position].toString().contains(".gif") || adapterFiles[position].toString().contains(".mp4") || adapterFiles[position].toString().contains(".3gp") || adapterFiles[position].toString().contains(".mkv") || adapterFiles[position].toString().contains(".avi") || adapterFiles[position].toString().contains(".flv") || adapterFiles[position].toString().contains(".mp3") || adapterFiles[position].toString().contains(".m4a")) {
                        //info buttons for image video and audio
                        String filename = adapterFiles[position].getName();
                        textView.setVisibility(View.VISIBLE);
                        textView.setText(filename);

                        btn_preview.setVisibility(View.VISIBLE);
                        btn_add.setVisibility(View.VISIBLE);
                        copiedPath = adapterFiles[position].getAbsolutePath();
                        fileName = filename;

                    }
                } else {
                    rl_imageLayout.setBackgroundColor(Color.TRANSPARENT);
                    btn_preview.setVisibility(View.INVISIBLE);
                    btn_add.setVisibility(View.INVISIBLE);
                    if (multipleSelection) {
                        checkBox.setVisibility(View.VISIBLE);
                    } else {
                        checkBox.setVisibility(View.GONE);
                    }
                }
           /*
            * For Multiple Selection of file & folders
            */
                if (adapterFiles[position].isDirectory()) {
                    // rl_imageLayout.setBackgroundColor(Color.parseColor("#604e3634"));
                    String fileName = adapterFiles[position].getName();
                    imgView.setVisibility(View.VISIBLE);
                    imgView.setBackgroundResource(0);
                    imgView.setBackgroundResource(R.drawable.collectioncell_normal);
                    textView.setVisibility(View.VISIBLE);
                    textView.setText(fileName);
                    if (multipleSelection) {
                        ClickedFilePath currentFolder = folders.get(position);
                        // checkBox.setChecked(currentFolder.isFolderSelected());
                        if (currentFolder.isFolderSelected()) {
                            checkBox.setChecked(true);
                        } else {
                            checkBox.setChecked(false);
                        }
                    } else {
                        checkBox.setVisibility(View.GONE);
                    }

                } else {
                    if (!adapterFiles[position].isHidden()) {
                        String fileName = adapterFiles[position].getName();
                        textView.setText(fileName);
                        textView.setVisibility(View.VISIBLE);
                        imgView.setBackgroundResource(0);
                        imgView.setVisibility(View.VISIBLE);
                        if (fileName.contains(".mp4") || fileName.contains(".3gp") || fileName.contains(".mkv") || fileName.contains(".avi") || fileName.contains(".flv")) {
                            //thumbnail image for video
                            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(adapterFiles[position].getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
                            if (thumb != null) {
                                imgView.setImageBitmap(thumb);
                            } else {
                                String split[] = adapterFiles[position].getAbsolutePath().split("/");
                                String FileName = adapterFiles[position].getAbsolutePath().replace(split[split.length - 1], "." + split[split.length - 1]);
                                Bitmap bitmap = BitmapFactory.decodeFile(FileName);
                                imgView.setImageBitmap(bitmap);
                            }
                        } else if (adapterFiles[position].toString().contains(".mp3") || fileName.contains(".m4a")) {
                            //Bitmap bitmap = BitmapFactory.decodeFile(FileManagerActivity.getResources().getDrawable(R.drawable.default_audio_stop));
                            imgView.setBackgroundResource(R.drawable.default_audio_stop);
                        } else {
                            String split[] = adapterFiles[position].getAbsolutePath().split("/");
                            String FileName = adapterFiles[position].getAbsolutePath().replace(split[split.length - 1], "." + split[split.length - 1]);
                            Bitmap bitmap = BitmapFactory.decodeFile(FileName);
                            if (bitmap != null) {
                                Bitmap b = bitmap.createScaledBitmap(bitmap, (int) FileManagerActivity.this.getResources().getDimension(R.dimen.filemanager_folder_size), (int) FileManagerActivity.this.getResources().getDimension(R.dimen.filemanager_folder_size), false);
                                imgView.setImageBitmap(b);
                            } else {
                                Bitmap b = BitmapFactory.decodeFile(adapterFiles[position].getAbsolutePath());
                                Bitmap bit = bitmap.createScaledBitmap(bitmap, (int) FileManagerActivity.this.getResources().getDimension(R.dimen.filemanager_folder_size), (int) FileManagerActivity.this.getResources().getDimension(R.dimen.filemanager_folder_size), false);
                                imgView.setImageBitmap(bit);
                            }
                        }
                        if (multipleSelection) {
                            ClickedFilePath currentFolder = folders.get(position);
                            if (currentFolder.isFolderSelected()) {
                                checkBox.setChecked(true);
                            } else {
                                checkBox.setChecked(false);
                            }
                        } else {
                            checkBox.setVisibility(View.GONE);
                        }

                    }
                /*else{
                    textView.setVisibility(View.GONE);
                    imgView.setVisibility(View.GONE);
                }*/
                }
//            }else{
//                rl_imageLayout.setBackgroundColor(Color.parseColor("#604e3634"));
//                HashMap<String, String> map =catHashMapList.get(position);
//                textView.setText(map.get("TitleEn"));
//                MyFileManager fileManager=new MyFileManager(currentFilePath, FileManagerActivity.this);
//                fileManager.new lvimage(imgView).execute(map.get("ThumbName"));
//
//            }
            return view;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed(){
        setResult(RESULT_OK, getIntent().putExtra("Book", currentBook));
        setResult(RESULT_OK, getIntent().putExtra("imageType", "Exit"));
        setResult(RESULT_OK, getIntent().putExtra("path", "null"));
        finish();
    }

    /*
     *  Picking video,audio and image
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == pick_up_image && null != data) {
            if (resultCode == RESULT_OK) {

                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.JELLY_BEAN){
                    if(data.getClipData()!=null) {
                        ClipData mclipData = data.getClipData();
                        for (int i = 0; i < mclipData.getItemCount(); i++) {
                            ClipData.Item item = mclipData.getItemAt(i);
                            Uri uri = item.getUri();
                            //selectedGalleryImageUri = uri;
                            String pickedSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, uri);
                            if (Build.VERSION.SDK_INT >= 23) {
                                requestReadExternalStoragePermission(uri,"Image");
                            } else {
                                pickingFilesfromGallery(uri, "Image");
                            }

                        }
                    }else if(data.getData()!=null){
                        Uri selectedImage = data.getData();
                        pickingFilesfromGallery(selectedImage, "Image");
                    }
                }else {
                    Uri selectedImage = data.getData();
                    // userFunctions.createNewDirectory(filesDirPath+"/Images");
                    //userFunctions.createNewDirectory(filesDirPath+"/Images/.Images");
                    pickingFilesfromGallery(selectedImage, "Image");
                }


            } else if (resultCode == RESULT_CANCELED) {
                //System.out.println("Cancelled operation to pic image from gallery");
            }
        } else if (requestCode == VIDEO_GALLERY) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                //userFunctions.createNewDirectory(filesDirPath+"/Videos");
                //userFunctions.createNewDirectory(filesDirPath+"/Videos/.Images");
                pickingFilesfromGallery(selectedImage,"Video");

			}else if (resultCode == RESULT_CANCELED) {
                //System.out.println("Cancelled operation to pic image from gallery");
            }
        } else if (requestCode ==pick_Audio ) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
               // userFunctions.createNewDirectory(filesDirPath+"/Audio");
                //userFunctions.createNewDirectory(filesDirPath+"/Audio/.Images");
                pickingFilesfromGallery(selectedImage, "Audio");
            }
        }

        gridView.setAdapter(new FileManagerGridAdapter(currentFilePath));

    }

    public void requestReadExternalStoragePermission(Uri uri, String fileType) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            /*if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

			} else {*/
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            //}
        } else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            pickingFilesfromGallery(uri, fileType);
        }
    }

    private void pickingFilesfromGallery(Uri selectedFile, String fileType){
        String Path = UserFunctions.getPathFromGalleryForPickedItem(this, selectedFile);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss.SSS");
        String formattedDate = df.format(c.getTime());
        // Bitmap thumb = ThumbnailUtils.createVideoThumbnail(adapterFiles[position].getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
        File file = new File(Path);
        String audio_path = currentFilePath + "/" +formattedDate+".mp3";
        String img_path = currentFilePath + "/" +formattedDate+".png";
        String video_path = currentFilePath + "/" +formattedDate+".mp4";
        String[]split=currentFilePath.split("/");


        if(fileType.equals("Image")){
               Bitmap b=BitmapFactory.decodeFile(Path);
               Bitmap backThumbBitmap = Bitmap.createScaledBitmap(b, Globals.getDeviceIndependentPixels(130, FileManagerActivity.this), Globals.getDeviceIndependentPixels(170, FileManagerActivity.this), true);
               //  if(split[split.length-2].equals("UserLibrary")){
               if(!new File( currentFilePath + "/." + formattedDate+".png").exists()) {
                   UserFunctions.saveBitmapImage(backThumbBitmap, currentFilePath + "/." + formattedDate+".png");
               }

        }else if(fileType.equals("Video")){
               Bitmap thumb = ThumbnailUtils.createVideoThumbnail(Path, MediaStore.Video.Thumbnails.MINI_KIND);
               Bitmap backThumbBitmap = Bitmap.createScaledBitmap(thumb, Globals.getDeviceIndependentPixels(130, FileManagerActivity.this), Globals.getDeviceIndependentPixels(170, FileManagerActivity.this), true);
               // UserFunctions.saveBitmapImage(backThumbBitmap, currentFilePath + "/." + file.getName());
               if (!new File(currentFilePath + "/." + formattedDate+".mp4").exists()) {
                   UserFunctions.saveBitmapImage(backThumbBitmap, currentFilePath + "/." + formattedDate+".mp4");
               }
        }
        if(!new File(img_path).exists() && fileType.equals("Image")) {
            userFunctions.copyFiles(Path, img_path);
        }else  if(!new File(video_path).exists() && fileType.equals("Audio")) {
            userFunctions.copyFiles(Path, audio_path);
        } else if(!new File(video_path).exists() && fileType.equals("Video")) {
            userFunctions.copyFiles(Path, video_path);
        }
        /*else{
            Toast.makeText(FileManagerActivity.this, R.string.file_exists, Toast.LENGTH_SHORT).show();
        }*/

    }

    public void makeEnrichedBtnSlected(View v){
        for (int i = 0; i < ll_container.getChildCount(); i++) {
            Button btnEnrTab;
            if (i == 0) {
                btnEnrTab = (Button) ll_container.getChildAt(i);
            } else {
                RelativeLayout rlview = (RelativeLayout) ll_container.getChildAt(i);
                btnEnrTab = (Button) rlview.getChildAt(0);
            }
            if (btnEnrTab == v) {
                v.setSelected(true);
            } else {
                btnEnrTab.setSelected(false);
            }
        }
    }

    public class importMedia extends BaseAdapter {

        ArrayList<HashMap<String, String>> FilesList;

        String filePath;

        public importMedia(ArrayList<HashMap<String, String>> Filepath) {
            FilesList = Filepath;
         }

        @Override
        public int getCount() {
            // if(!importMedia) {
            return FilesList.size();
//            }else {
//                return catHashMapList.size();
//            }
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.filesandfolders, null);
            }
            RelativeLayout rl_imageLayout = (RelativeLayout) view.findViewById(R.id.rl_imageLayout);
            final ImageView imgView = (ImageView) view.findViewById(R.id.imageView1);
            TextView textView = (TextView) view.findViewById(R.id.text);
            Button btn_preview = (Button) view.findViewById(R.id.btn_info);
            Button btn_add = (Button) view.findViewById(R.id.btn_add_image);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            btn_preview.setVisibility(View.INVISIBLE);
            btn_add.setVisibility(View.INVISIBLE);
            imgView.setVisibility(View.GONE);
            textView.setVisibility(View.GONE);
            imgView.setBackgroundResource(0);
          //  rl_imageLayout.setBackgroundColor(Color.parseColor("#604e3634"));

            HashMap<String, String> map = FilesList.get(position);
            textView.setText(map.get("TitleEn"));
            textView.setVisibility(View.VISIBLE);
            MyFileManager fileManager = new MyFileManager(currentFilePath, FileManagerActivity.this);
            fileManager.new lvimage(imgView).execute(map.get("ThumbName"));

            return view;
        }



    }
}

