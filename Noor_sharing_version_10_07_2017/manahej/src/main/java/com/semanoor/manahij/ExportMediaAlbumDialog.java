package com.semanoor.manahij;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;
import com.semanoor.source_sboookauthor.Zip;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Suriya on 06/01/16.
 */
public class ExportMediaAlbumDialog {

    private Context context;
    private WebService webService;
    private ArrayList<HashMap<String, String>> catHashMapList;
    private ListView listView;
    private ProgressBar progressBar;
    private TextView catTxtView;
    private Book currentBook;
    private HashMap<String, String> selectedCategoryMap;
    private EditText etTitle, etKeyword;
    private Switch privateSwitch;
    private ProgressDialog progressDialog;

    public ExportMediaAlbumDialog(Context context, Book currentBook){
        this.context = context;
        this.currentBook = currentBook;
        webService = new WebService(context, Globals.mediaLibraryWebserviceURL);
    }

    public void showExportMediaAlbumDialog(){
        Dialog dialog = new Dialog(context);
        View view = LayoutInflater.from(context).inflate(R.layout.export_media_album, null);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
        dialog.setContentView(view);
        int width = (int)(Globals.getDeviceWidth()/1.8);
        int height = (int)(Globals.getDeviceHeight()/1.8);
        dialog.getWindow().setLayout(width, height);

        etTitle = (EditText) view.findViewById(R.id.editText6);
        etTitle.setText(currentBook.getBookTitle());
        etKeyword = (EditText) view.findViewById(R.id.editText7);
        privateSwitch = (Switch) view.findViewById(R.id.switch3);
        privateSwitch.setChecked(true);
        Button btnPublish = (Button) view.findViewById(R.id.button9);
        btnPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateForm()) {
                    String str = context.getResources().getString(R.string.please_wait);
                    progressDialog = ProgressDialog.show(context, "", str, true);
                    new mediaAlbumPublish().execute();
                }
            }
        });
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.GONE);
        catTxtView = (TextView) view.findViewById(R.id.textView16);
        catTxtView.setVisibility(View.GONE);
        listView = (ListView) view.findViewById(R.id.listView5);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> map = catHashMapList.get(position);
                String isSlected = map.get("Selected");
                if (isSlected.equals("true")){
                    selectedCategoryMap = map;
                    map.put("Selected", "true");
                }
                for (int i = 0; i<catHashMapList.size(); i++) {
                    HashMap<String, String> hmap = catHashMapList.get(i);
                    if (i != position) {
                        hmap.put("Selected", "false");
                    } else {
                        selectedCategoryMap = map;
                        hmap.put("Selected", "true");
                    }
                }
                ((BaseAdapter) parent.getAdapter()).notifyDataSetChanged();
            }
        });

        new GetCategoryList().execute();

        dialog.show();
    }

    private boolean validateForm(){
        if (etTitle.getText().toString().trim().length() > 0 && etKeyword.getText().toString().trim().length() > 0 && selectedCategoryMap != null) {
            return true;
        }
        return false;
    }

    private class mediaAlbumPublish extends AsyncTask<Void, Void, Void> {

        String base64Binary;
        boolean isPrivateChecked;
        String title, description;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            generateZipFile();
            if (privateSwitch.isChecked()){
                isPrivateChecked = true;
            } else {
                isPrivateChecked = false;
            }
            title = etTitle.getText().toString();
            description = etKeyword.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... params) {
            progressDialog.setMessage(context.getResources().getString(R.string.upload_please_wait));
            String mediaType = "Image";
            String bookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID();
            String[] bookFileLength = new File(bookPath).list();
            if (bookFileLength.length > 4) {
                mediaType = "Album2";
            }
            String published = "0";
            if (isPrivateChecked) {
                published = "1";
            }
            String catId = selectedCategoryMap.get("id");
            webService.mediaLibraryUpload("temp.zip", base64Binary, catId, "3", title, description, mediaType, published);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
        }

        private void generateZipFile(){
            Zip zip = new Zip();
            String srcPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/";
            String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp.zip";
            if (new File(destPath).exists()) {
                new File(destPath).delete();
            }
            zip.zipFilesForMediaAlbum(new File(srcPath), destPath);
            byte[] fileBytes = UserFunctions.convertFileToBytearray(new File(destPath));
            base64Binary= Base64.encodeToString(fileBytes, Base64.DEFAULT);
        }
    }

    private class GetCategoryList extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            webService.getMediaCategoryList();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String response = webService.mediaCategoryResponseResult;
            if (response != null) {
                catHashMapList = new ArrayList<HashMap<String, String>>();
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    String totalCount = jsonObj.getString("totalcount");
                    String doneDateTime = jsonObj.getString("DoneDateTime");
                    String data = jsonObj.getString("Data");
                    data = data.replace("\"", "");
                    String errorState = jsonObj.getString("ErrorState");

                    String[] splitData = data.split("#");
                    for (int i = 0; i < splitData.length; i++) {
                        String[] dataContent = splitData[i].split("\\$");
                        String id = dataContent[0];
                        String catArab = dataContent[1];
                        String catEng = dataContent[2];
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("id", id);
                        map.put("CategoryEng", catEng);
                        map.put("CategoryArab", catArab);
                        map.put("Selected", "false");
                        catHashMapList.add(map);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                listView.setAdapter(new CataegoryListAdapter());
            } else {
                catTxtView.setVisibility(View.VISIBLE);
            }
            progressBar.setVisibility(View.GONE);
            super.onPostExecute(aVoid);
        }
    }

    private class CataegoryListAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return catHashMapList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                view = LayoutInflater.from(context).inflate(R.layout.inflate_media_checklist, null);
            }

            HashMap<String, String> map = catHashMapList.get(position);
            TextView textView = (TextView) view.findViewById(R.id.textView1);
            textView.setText(map.get("CategoryEng"));
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            String isSlected = map.get("Selected");
            if (isSlected.equals("true")){
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            return view;
        }
    }


}
