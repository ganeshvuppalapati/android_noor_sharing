package com.semanoor.manahij;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PointF;

import com.artifex.mupdfdemo.MuPDFCore;
import com.artifex.mupdfdemo.SearchCallBack;

import org.apache.http.cookie.Cookie;


/**
 * Created by karthik on 20-07-2016.
 */
public class GenerateThumbnailsPdf extends MuPDFCore {

    public GenerateThumbnailsPdf(Context context, String filename, SearchCallBack callBack) throws Exception {
        super(context, filename, callBack);
    }

    public Bitmap createThumbnail(int w, int h,int page){
        PointF pageSize = getPageSize(0);
        float mSourceScale = Math.max(w/pageSize.x, h/pageSize.y);

        Point size = new Point((int)(pageSize.x*mSourceScale), (int)(pageSize.y*mSourceScale));
        final Bitmap bp = Bitmap.createBitmap(size.x,size.y, Bitmap.Config.ARGB_8888);

        drawPage(bp,page,size.x, size.y, 0, 0, size.x, size.y,new Cookie());
        return bp;
    }
}
