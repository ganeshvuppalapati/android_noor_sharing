package com.semanoor.manahij;

import java.util.ArrayList;

/**
 * Created by karthik on 12-01-2017.
 */

public class InAppIds {

    private static InAppIds instance;
    private ArrayList<InappIdData> inAppIdsArrayList = new ArrayList<>();

    public static synchronized InAppIds getInstance(){
        if(instance == null){
            instance = new InAppIds();
        }
        return instance;
    }

    public ArrayList<InappIdData> getInAppIdsArrayList() {
        return inAppIdsArrayList;
    }

    public void setInAppIdsArrayList(ArrayList<InappIdData> inAppIdsArrayList) {
        this.inAppIdsArrayList = inAppIdsArrayList;
    }
}
