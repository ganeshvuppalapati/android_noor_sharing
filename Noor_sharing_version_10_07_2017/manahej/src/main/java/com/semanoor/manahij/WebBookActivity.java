package com.semanoor.manahij;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.SegmentedRadioGroup;
import com.semanoor.source_sboookauthor.Templates;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebBookActivity extends Activity implements View.OnClickListener {

    private String selectedPosition;
    private HorizontalAdapter adapter;
    private EditText urlText;
    private ArrayList<String> urlList = new ArrayList<>();
    private ArrayList<String> titleList = new ArrayList<>();
    private String url;
    private RecyclerView recyclerView;
    private DatabaseHandler databaseHandler;
    private WebView webView;
    private ProgressBar progressBar;
    private Button createWebBookBtn;
    private GridShelf gridShelf;
    private Button addtabBtn;
    private boolean isTemplateSelected;
    private boolean onlineTemplates = true;
    private String isFrom;
    private Book createBook;
    private ArrayList<Book> bookArrayList = new ArrayList<>();
    public ArrayList<OnlineTemplates> templatesList = new ArrayList<>();
    private SharedPreferences sharedPreference;
    private Button new_Webbook,add_btn_tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_web_book);
        databaseHandler = DatabaseHandler.getInstance(WebBookActivity.this);
        gridShelf= (GridShelf) getIntent().getSerializableExtra("Shelf");
        createBook = (Book) getIntent().getSerializableExtra("book");
        isFrom = getIntent().getStringExtra("From");
        CreatedBookList();
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
        add_btn_tab = (Button) findViewById(R.id.add_btn_tab);
        add_btn_tab.setOnClickListener(this);
        new_Webbook = (Button) findViewById(R.id.new_webbook);
        new_Webbook.setOnClickListener(this);
        webView = (WebView) findViewById(R.id.wv_advsearch);
        urlText = (EditText) findViewById(R.id.editTextAdv);
        progressBar = (ProgressBar) findViewById(R.id.webBookProgressBar);
        createWebBookBtn = (Button) findViewById(R.id.add_web_tab);
        CircleButton backBtn = (CircleButton) findViewById(R.id.btn_import);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        createWebBookBtn.setOnClickListener(this);
//        addtabBtn = (Button) findViewById(R.id.web_book_btn);
//        addtabBtn.setOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.gridView1);
        adapter = new HorizontalAdapter(urlList);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(WebBookActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setAdapter(adapter);
        ItemTouchHelper.Callback callback = new MovieTouchHelper(adapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(recyclerView);
        ArrayList<String> webBookList = getSavedList();
        if (webBookList!=null && webBookList.size()>0){
            for (String str : webBookList){
                urlList.add(str);
            }
            urlText.setText(urlList.get(0));
            for (String str : urlList){
                String title = getHost(str);
                titleList.add(title);
            }
            selectedPosition = String.valueOf(0);
            if (webView.getVisibility()==View.INVISIBLE){
                webView.setVisibility(View.VISIBLE);
            }
            loadWebView();
            adapter.notifyDataSetChanged();
        }else {
            url = "http://www.google.co.in/search?q=";
            urlText.setText(url);
            urlList.add(url);
            String title = getHost(url);
            titleList.add(title);
            selectedPosition = String.valueOf(urlList.size() - 1);
            if (webView.getVisibility() == View.INVISIBLE) {
                webView.setVisibility(View.VISIBLE);
            }
            loadWebView();
            adapter.notifyDataSetChanged();
        }
        if (urlList.size()>3){
            add_btn_tab.setVisibility(View.VISIBLE);
        }

        urlText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    url = urlText.getText().toString().trim();
                    if (url.contains("http://www.google.co.in/search?q=")){
                        urlList.add(url);
                    }else{
                        String str = urlText.getText().toString().trim();
                        if (str.contains("www.")) {
                            url = str;
                        }else{
                            url = "www." + str;
                        }
                        url = "http://"+url+"/";
                        urlList.add(url);
                    }
                    String title = getHost(url);
                    titleList.add(title);
                    adapter.notifyDataSetChanged();
                    selectedPosition = String.valueOf(urlList.size()-1);
                    if (webView.getVisibility()==View.INVISIBLE){
                        webView.setVisibility(View.VISIBLE);
                    }
                    loadWebView();
                    addToSharedPreferences();
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_web_tab:
                if (urlList.size()>0) {
                    if (isFrom.equals("FromShelf")) {
                        showAddNewBookDialog();
                    }else if (isFrom.equals("FromBook")){
                        addInsideNormalBook();
                        onBackPressed();
                    }
                }else{
                    UserFunctions.DisplayAlertDialog(WebBookActivity.this,R.string.add_web_pages,R.string.add_page);
                }
                break;
            case R.id.new_webbook:
                urlList.clear();
                titleList.clear();
                url = "http://www.google.co.in/search?q=";
                urlText.setText(url);
                urlList.add(url);
                String title = getHost(url);
                titleList.add(title);
                adapter.notifyDataSetChanged();
                selectedPosition = String.valueOf(urlList.size()-1);
                if (webView.getVisibility()==View.INVISIBLE){
                    webView.setVisibility(View.VISIBLE);
                }
                loadWebView();
                addToSharedPreferences();
                break;
            case R.id.add_btn_tab:
                addTab();
                break;
        }

    }

    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

        private List<String> horizontalList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public Button txtView;
            public Button del_btn;
            public Button add_btn;

            public MyViewHolder(View view) {
                super(view);
                txtView = (Button) view.findViewById(R.id.btnEnrHome);
                del_btn = (Button) view.findViewById(R.id.btn_enr_del);
                add_btn = (Button) view.findViewById(R.id.add_btn);
            }
        }


        public HorizontalAdapter(List<String> horizontalList) {
            this.horizontalList = horizontalList;
        }

        public void swap(int firstPosition, int secondPosition){
            Collections.swap(horizontalList, firstPosition, secondPosition);
            notifyItemMoved(firstPosition, secondPosition);
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.webbook_rcycler_item, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            if (position == urlList.size()-1 && urlList.size() < 4){
                holder.add_btn.setVisibility(View.VISIBLE);
                add_btn_tab.setVisibility(View.GONE);
            }else{
                add_btn_tab.setVisibility(View.VISIBLE);
                holder.add_btn.setVisibility(View.GONE);
            }
            if (selectedPosition!=null){
                if (position==Integer.parseInt(selectedPosition)){
                    holder.txtView.setSelected(true);
                }else{
                    holder.txtView.setSelected(false);
                }
            }
            holder.txtView.setText(titleList.get(position));
            holder.add_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addTab();
                }
            });
            holder.txtView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(titleList.size()-1);
                    RelativeLayout layout = (RelativeLayout) viewHolder.itemView;
                    if (layout.getChildCount()>1){
                        Button addBtn = (Button) layout.getChildAt(1);
                        addBtn.setVisibility(View.GONE);
                    }
                    return false;
                }
            });
            holder.txtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = String.valueOf(position);
                    loadWebView();
                    adapter.notifyDataSetChanged();
                }
            });
            holder.del_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (urlList.size()>1) {
                        urlList.remove(position);
                        titleList.remove(position);
                        if (position == urlList.size()) {
                            if (urlList.size() > 0 && Integer.parseInt(selectedPosition) > urlList.size() - 1) {
                                selectedPosition = String.valueOf(urlList.size() - 1);
                            }
                        }else{
                            if (position < Integer.parseInt(selectedPosition)){
                                if (urlList.size() > 0 ) {
                                    selectedPosition = String.valueOf(Integer.parseInt(selectedPosition) - 1);
                                }
                            }
                        }
                        if (urlList.size() > 0) {
                            loadWebView();
                        } else {
                            webView.setVisibility(View.INVISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                        adapter.notifyDataSetChanged();
                        addToSharedPreferences();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return horizontalList.size();
        }

    }

    private class MovieTouchHelper extends ItemTouchHelper.Callback {
        private HorizontalAdapter mMovieAdapter;

        public MovieTouchHelper(HorizontalAdapter movieAdapter){
            this.mMovieAdapter = movieAdapter;
        }

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            final int dragFlags = ItemTouchHelper.START | ItemTouchHelper.END;
            final int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
            return makeMovementFlags(dragFlags, swipeFlags);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            mMovieAdapter.swap(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            Collections.swap(titleList,viewHolder.getAdapterPosition(), target.getAdapterPosition());
            selectedPosition = String.valueOf(viewHolder.getAdapterPosition());
            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

        }

        @Override
        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            postAndNotifyAdapter(recyclerView,mMovieAdapter);
            addToSharedPreferences();
            loadWebView();
        }

        @Override
        public boolean isLongPressDragEnabled() {
            return true;
        }
    }

    private void postAndNotifyAdapter(final RecyclerView recyclerView, final RecyclerView.Adapter adapter) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (!recyclerView.isComputingLayout()) {
                    adapter.notifyDataSetChanged();
                } else {
                    postAndNotifyAdapter(recyclerView, adapter);
                }
            }
        });
    }

    private void loadWebView(){
        progressBar.setVisibility(View.VISIBLE);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setDomStorageEnabled(true);
      //  webView.setWebChromeClient(new customWebchromeClient());
        if (urlList.size()>0) {
            webView.loadUrl(urlList.get(Integer.parseInt(selectedPosition)));
        }
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                webView.setWebViewClient(new CustomWebViewClient());
                return false;
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
               /* if (url!=null && !url.equals("")) {
                    RecyclerView.ViewHolder holder = recyclerView.findViewHolderForAdapterPosition(Integer.parseInt(selectedPosition));
                    if (holder != null && holder.itemView != null) {
                        if (holder.itemView instanceof RelativeLayout) {
                            RelativeLayout relativeLayout = (RelativeLayout) holder.itemView;
                            RelativeLayout linearLayout = (RelativeLayout) relativeLayout.getChildAt(0);
                            Button button = (Button) linearLayout.getChildAt(0);
                            if (!view.getTitle().equals("")) {
                                button.setText(view.getTitle());
                                titleList.remove(Integer.parseInt(selectedPosition));
                                titleList.add(Integer.parseInt(selectedPosition), view.getTitle());
                            }
                        }
                    }
                }*/
            }
        });
    }

    private String getHost(String url){
        if(url == null || url.length() == 0)
            return "";

        int doubleslash = url.indexOf("//");
        if(doubleslash == -1)
            doubleslash = 0;
        else
            doubleslash += 2;

        int end = url.indexOf('/', doubleslash);
        end = end >= 0 ? end : url.length();

        int port = url.indexOf(':', doubleslash);
        end = (port > 0 && port < end) ? port : end;

        String title = url.substring(doubleslash, end);
        if (title.contains("www.")){
            title = title.replace("www.","");
        }
        return title;
    }

    public class CustomWebViewClient extends WebViewClient {
        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            Log.d("WEB_ERROR", "error code:" + errorCode + ":" + description);
            String url = "";
        }

        @Override
        // Method where you(get) load the 'URL' which you have clicked
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            try {
                if (url != null) {
                    urlList.add(url);
                    String title = getHost(url);
                    titleList.add(title);
                    adapter.notifyDataSetChanged();
                    selectedPosition = String.valueOf(urlList.size()-1);
                    loadWebView();
                    recyclerView.smoothScrollToPosition(Integer.parseInt(selectedPosition));
                    addToSharedPreferences();
                    return true;
                } else {
                    return false;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
          /*  if (url!=null && !url.equals("")) {
                RecyclerView.ViewHolder holder = recyclerView.findViewHolderForAdapterPosition(Integer.parseInt(selectedPosition));
                if (holder != null && holder.itemView != null) {
                    if (holder.itemView instanceof RelativeLayout) {
                        RelativeLayout relativeLayout = (RelativeLayout) holder.itemView;
                        RelativeLayout linearLayout = (RelativeLayout) relativeLayout.getChildAt(0);
                        Button button = (Button) linearLayout.getChildAt(0);
                        button.setText(view.getTitle());
                        titleList.remove(Integer.parseInt(selectedPosition));
                        titleList.add(Integer.parseInt(selectedPosition), view.getTitle());
                    }
                }
            }*/
        }
    }

    public void showAddNewBookDialog(){
        final Dialog addNewBookDialog = new Dialog(this);
        //addNewBookDialog.setTitle(R.string.create_new_book);
        addNewBookDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addNewBookDialog.setContentView(this.getLayoutInflater().inflate(R.layout.add_new_book_dialog, null));
        addNewBookDialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.2), (int)(Globals.getDeviceHeight()/1.5));
        final SegmentedRadioGroup btnSegmentGroup = (SegmentedRadioGroup) addNewBookDialog.findViewById(R.id.btn_segment_group);
        isTemplateSelected = true;
        final ProgressBar progressBar = (ProgressBar) addNewBookDialog.findViewById(R.id.progressBar);
        final OnlineTemplatesGridAdapter adapter = new OnlineTemplatesGridAdapter(WebBookActivity.this,templatesList);
        final RecyclerView templateGrid = (RecyclerView) addNewBookDialog.findViewById(R.id.gridView1);
        final RadioButton online_temp = (RadioButton)addNewBookDialog.findViewById(R.id.online);
        Button create_btn = (Button) addNewBookDialog.findViewById(R.id.btnNext);
        create_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!onlineTemplates) {
                    online_temp.setChecked(true);
                    onlineTemplates = true;
                    progressBar.setVisibility(View.VISIBLE);
                    templateGrid.setVisibility(View.INVISIBLE);
                    apiCallBackTask(templateGrid,progressBar, adapter);
                }
                Intent bookViewIntent = new Intent(getApplicationContext(), UserTemplates.class);
                startActivity(bookViewIntent);
            }
        });
        SegmentedRadioButton segmentedRadioGroup = (SegmentedRadioButton)addNewBookDialog.findViewById(R.id.segment_text);
        segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.online) {
                    onlineTemplates = true;
                    progressBar.setVisibility(View.VISIBLE);
                    templateGrid.setVisibility(View.INVISIBLE);
                    apiCallBackTask(templateGrid,progressBar,adapter);
                } else if (checkedId == R.id.create) {
                    onlineTemplates = false;
                    templateGrid.setVisibility(View.VISIBLE);
                    templatesList.clear();
                    progressBar.setVisibility(View.GONE);
                    String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
                    java.io.File file = new java.io.File(templatePath);
                    java.io.File[] path = file.listFiles();
                    for(java.io.File tempFile : path){
                        OnlineTemplates templates1 = new OnlineTemplates();
                        templates1.setTemplateName("Template "+tempFile.getName());
                        templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
                        templates1.setThumbUrl("");
                        templates1.setContentUrl("");
                        if (Integer.parseInt(tempFile.getName())<1000){
                            templates1.setDownloadStatus("Downloaded");
                        }else{
                            templates1.setDownloadStatus("Created");
                            templatesList.add(templates1);
                        }
                    }
                    final OnlineTemplatesGridAdapter tempAdapter = new OnlineTemplatesGridAdapter(WebBookActivity.this,templatesList);
                    templateGrid.setAdapter(tempAdapter);
                }
            }
        });
        GridLayoutManager gridLayout = new GridLayoutManager(WebBookActivity.this, getResources().getInteger(R.integer.grid_column), GridLayoutManager.VERTICAL, false);
        templateGrid.setLayoutManager(gridLayout);
        templateGrid.setItemAnimator(new DefaultItemAnimator());
        final Button btn = (Button) addNewBookDialog.findViewById(R.id.book_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTemplateSelected){
                    progressBar.setVisibility(View.GONE);
                    templateGrid.setVisibility(View.VISIBLE);
                    isTemplateSelected = false;
                    btn.setText(R.string.templates);
                    TemplateListAdapter templateGridAdapter = new TemplateListAdapter(bookArrayList, gridShelf.templatesPortraitListArray,isTemplateSelected);
                    templateGrid.setAdapter(templateGridAdapter);
                }else{
                    isTemplateSelected = true;
                    btn.setText(R.string.books);
                    progressBar.setVisibility(View.VISIBLE);
                    templateGrid.setVisibility(View.INVISIBLE);
                    apiCallBackTask(templateGrid,progressBar,adapter);
                }
            }
        });
        CircleButton back_btn = (CircleButton) addNewBookDialog.findViewById(R.id.btnback);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewBookDialog.dismiss();
            }
        });
        apiCallBackTask(templateGrid,progressBar,adapter);
        templateGrid.addOnItemTouchListener(new RecyclerTouchListener(WebBookActivity.this, templateGrid, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (isTemplateSelected) {
                    OnlineTemplates templates = templatesList.get(position);
                    if (!isDownloading(templates.getTemplateId()) && !isDownloaded(templates.getTemplateId())){
                        addTemplateIdToSharedPreferences(Globals.DownloadingTemplates,templates.getTemplateId());
                        updateTemplateList(position);
                        adapter.notifyDataSetChanged();
                    }else if (isDownloaded(templates.getTemplateId())){
                        int templateId = view.getId();
                        int orientationIndex = btnSegmentGroup.indexOfChild(addNewBookDialog.findViewById(btnSegmentGroup.getCheckedRadioButtonId()));
                        gridShelf.createWebBook(orientationIndex, templateId, urlList.size() + 2, urlList, databaseHandler);
                        addNewBookDialog.dismiss();
                        setResult(RESULT_OK,getIntent().putExtra("Shelf", gridShelf));
                        setResult(RESULT_OK,getIntent().putExtra("From",isFrom));
                        setResult(RESULT_OK, getIntent().putExtra("book", createBook));
                        setResult(RESULT_OK, getIntent().putExtra("IsWebBookCreated", true));
                        finish();
                    }
                }else{
                    for (int i=0;i<urlList.size();i++) {
                        Book book = (Book) bookArrayList.get(position);
                        int pageCount = book.getTotalPages() + 1;
                        book.setTotalPages(pageCount);
                        databaseHandler.executeQuery("update books set totalPages='" + pageCount + "' where BID='" + book.getBookID() + "'");
                        String imageType = "IFrame";
                        String objSclaPageToFit = "no";
                        int width = (int) (Globals.getDeviceWidth() - 60);
                        int height = (int) (Globals.getDeviceHeight() - 120);
                        String size = width + "|" + height;
                        int imageXpos = Globals.getDeviceWidth() / 2 - width / 2;
                        int imageYpos = Globals.getDeviceHeight() / 2 - height / 2;
                        String location = imageXpos + "|" + imageYpos;
                        boolean objLocked = false;
                        String objContent = urlList.get(i);
                        int pageNo = pageCount-1;
                        databaseHandler.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + imageType + "', '" + book.getBookID() + "', '" + pageNo + "', '" + location + "', '" + size + "', '" + objContent.replace("'", "''") + "', '" + 1 + "', '" + objSclaPageToFit + "', '" + objLocked + "', '" + 0 + "')");
                    }
                    addNewBookDialog.dismiss();
                    setResult(RESULT_OK,getIntent().putExtra("Shelf", gridShelf));
                    setResult(RESULT_OK,getIntent().putExtra("From",isFrom));
                    setResult(RESULT_OK, getIntent().putExtra("book", createBook));
                    setResult(RESULT_OK, getIntent().putExtra("IsWebBookCreated", true));
                    finish();
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        addNewBookDialog.show();
    }

    public class TemplateListAdapter extends RecyclerView.Adapter<TemplateListAdapter.MyViewHolder> {

        private ArrayList<Templates> templatesListArray;
        private ArrayList<Book> bookListArray;
        private boolean isTemplate;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView bookImg;

            public MyViewHolder(View view) {
                super(view);
                bookImg = (ImageView) view.findViewById(R.id.imageView2);
            }
        }

        public TemplateListAdapter(ArrayList<Book> bookList,ArrayList<Templates> _templatesListArray, boolean isTemplateList) {
            this.templatesListArray = _templatesListArray;
            this.bookListArray = bookList;
            this.isTemplate = isTemplateList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.inflate_book_template, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public int getItemCount() {
            if (isTemplate){
                return templatesListArray.size();
            }else {
                return bookListArray.size();
            }
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            String freeFilesPath = Globals.TARGET_BASE_TEMPATES_PATH;
            if (isTemplate) {
                Templates template = templatesListArray.get(position);
                holder.itemView.setId(template.getTemplateId());
                Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath + template.getTemplateId() + "/card.png");
                holder.bookImg.setImageBitmap(bitmap);
            }else{
                Book book = (Book) bookListArray.get(position);
                int BID = book.getBookID();
                String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/card.png";
                Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
                holder.bookImg.setImageBitmap(bitmap);
            }
        }

    }

    private void addInsideNormalBook(){
        for (int i=0;i<urlList.size();i++) {
            int pageCount = createBook.getTotalPages() + 1;
            createBook.setTotalPages(pageCount);
            databaseHandler.executeQuery("update books set totalPages='" + pageCount + "' where BID='" + createBook.getBookID() + "'");
            String imageType = "IFrame";
            String objSclaPageToFit = "no";
            int width = (int) (Globals.getDeviceWidth() - 60);
            int height = (int) (Globals.getDeviceHeight() - 120);
            String size = width + "|" + height;
            int imageXpos = Globals.getDeviceWidth() / 2 - width / 2;
            int imageYpos = Globals.getDeviceHeight() / 2 - height / 2;
            String location = imageXpos + "|" + imageYpos;
            boolean objLocked = false;
            String objContent = urlList.get(i);
            int pageNo = pageCount - 1;
            databaseHandler.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + imageType + "', '" + createBook.getBookID() + "', '" + pageNo + "', '" + location + "', '" + size + "', '" + objContent.replace("'", "''") + "', '" + 1 + "', '" + objSclaPageToFit + "', '" + objLocked + "', '" + 0 + "')");
        }
    }
    private void CreatedBookList(){
        for (int i=0;i<gridShelf.bookListArray.size();i++){
            Book book = (Book) gridShelf.bookListArray.get(i);
            if (!book.is_bStoreBook()&& book.get_bStoreID()==null){
                bookArrayList.add(book);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isFrom.equals("FromBook")) {
            setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
            setResult(RESULT_OK, getIntent().putExtra("From", isFrom));
            setResult(RESULT_OK, getIntent().putExtra("book", createBook));
            setResult(RESULT_OK, getIntent().putExtra("IsWebBookCreated", false));
        }
        finish();
    }
    private void addTemplateIdToSharedPreferences(String key,int ID){
        SharedPreferences.Editor editor = sharedPreference.edit();
        ArrayList<Integer> existList = null;
        try {
            existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<Integer>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        existList.add(ID);
        try {
            editor.putString(key, ObjectSerializer.serialize(existList));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        editor.commit();
    }
    private boolean isDownloading(int Id){
        ArrayList<Integer> existList = null;
        try {
            existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.DownloadingTemplates, ObjectSerializer.serialize(new ArrayList<Integer>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i=0;i<existList.size();i++){
            int existId = existList.get(i);
            if (existId == Id){
                return true;
            }
        }
        return false;
    }
    private void updateTemplateList(int position){
        for (int i=0;i<templatesList.size();i++){
            OnlineTemplates templates = templatesList.get(i);
            if (position == i){
                templates.setDownloadStatus("Downloading");
                break;
            }
        }
    }
    private boolean isDownloaded(int Id){
        for (int i=0;i<templatesList.size();i++){
            OnlineTemplates templates = templatesList.get(i);
            if (Id == templates.getTemplateId()&& (templates.getDownloadStatus().equals("Downloaded") || templates.getDownloadStatus().equals("Created"))){
                return true;
            }
        }
        return false;
    }
    private void parseAndSaveTemplatesList(ArrayList<Object> templates){
        templatesList.clear();
        for (int i=0;i<templates.size();i++){
            HashMap<String,Object> hashMap = (HashMap<String, Object>) templates.get(i);
            OnlineTemplates templates1 = new OnlineTemplates();
            templates1.setTemplateName((String) hashMap.get("name"));
            templates1.setTemplateId((Integer) hashMap.get("ID"));
            templates1.setThumbUrl((String) hashMap.get("ThumbURL"));
            templates1.setContentUrl((String) hashMap.get("contentURL"));
            if (isDownloading(templates1.getTemplateId())){
                templates1.setDownloadStatus("Downloading");
            }else {
                if (new java.io.File(Globals.TARGET_BASE_TEMPATES_PATH + templates1.getTemplateId()).exists()) {
                    templates1.setDownloadStatus("Downloaded");
                } else {
                    templates1.setDownloadStatus("Not Downloaded");
                }
            }
            templatesList.add(templates1);
        }
    }
    private void apiCallBackTask(final RecyclerView templateGrid, final ProgressBar progress, final OnlineTemplatesGridAdapter adapter){
        progress.setVisibility(View.VISIBLE);
        ApiCallBackTask apiCallBackTask = new ApiCallBackTask(WebBookActivity.this,Globals.templatesUrl,null, new ApiCallBackListener<Object>() {

            @Override
            public void onSuccess(Object object) {
                progress.setVisibility(View.GONE);
                templateGrid.setVisibility(View.VISIBLE);
                if (onlineTemplates) {
                    templatesList.clear();
                    if (object != null) {
                        Map<String, Object> jsonMap = (Map<String, Object>) object;
                        ArrayList<Object> mapObject = (ArrayList<Object>) jsonMap.get("Templates");
                        parseAndSaveTemplatesList(mapObject);
                    } else {
                        String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
                        java.io.File file = new java.io.File(templatePath);
                        java.io.File[] path = file.listFiles();
                        for (java.io.File tempFile : path) {
                            OnlineTemplates templates1 = new OnlineTemplates();
                            templates1.setTemplateName("Template " + tempFile.getName());
                            templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
                            templates1.setThumbUrl("");
                            templates1.setContentUrl("");
                            if (Integer.parseInt(tempFile.getName()) < 1000) {
                                templates1.setDownloadStatus("Downloaded");
                                templatesList.add(templates1);
                            } else {
                                templates1.setDownloadStatus("Created");
                            }
                        }
                    }
                    templateGrid.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Exception e) {
                templatesList.clear();
                templateGrid.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
                java.io.File file = new java.io.File(templatePath);
                java.io.File[] path = file.listFiles();
                for(java.io.File tempFile : path){
                    OnlineTemplates templates1 = new OnlineTemplates();
                    templates1.setTemplateName("Template "+tempFile.getName());
                    templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
                    templates1.setThumbUrl("");
                    templates1.setContentUrl("");
                    if (Integer.parseInt(tempFile.getName())<1000){
                        templates1.setDownloadStatus("Downloaded");
                        templatesList.add(templates1);
                    }else{
                        templates1.setDownloadStatus("Created");
                    }
                    templateGrid.setAdapter(adapter);
                }
            }
        });
        apiCallBackTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    private void addTab(){
        url = "http://www.google.co.in/search?q=";
        urlText.setText(url);
        urlList.add(url);
        String title = getHost(url);
        titleList.add(title);
        adapter.notifyDataSetChanged();
        selectedPosition = String.valueOf(urlList.size()-1);
        if (webView.getVisibility()==View.INVISIBLE){
            webView.setVisibility(View.VISIBLE);
        }
        recyclerView.smoothScrollToPosition(Integer.parseInt(selectedPosition));
        loadWebView();
        addToSharedPreferences();
    }
    private void addToSharedPreferences(){
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(WebBookActivity.this);
        SharedPreferences.Editor editor = sharedPreference.edit();
        try {
            editor.putString(Globals.WEBLIST, ObjectSerializer.serialize(urlList));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }
    private ArrayList<String> getSavedList(){
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(WebBookActivity.this);
        ArrayList<String> existList = null;
        try {
            existList = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.WEBLIST, ObjectSerializer.serialize(new ArrayList<String>())));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return existList;
    }
}
