package com.semanoor.manahij;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Random;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class ElessonReportActivity extends Activity {
    String path;
    private String currentBookPath;
    private ArrayList<HashMap<String, String>> statisticsArray;
    private ArrayList<HashMap<String, String>> sortedArrayList;
    private ArrayList<HashMap<String, String>> sortedSequenceArray;
    private Button barBtn1, barBtn2, barBtn3, barBtn4, barBtn5;
    private TextView txtView1, txtView2, txtView3, txtView4, txtView5, pieCharttxt1, pieCharttxt2, draw_count;
    private ImageView imgView1, imgView2, imgView3, imgView4, imgView5, pieChartimg1, pieChartimg2;
    public String[] barChart_colors;
    float readPercent;
    pieChart pieChart;
    String[] colors;
    private Book currentBook;

    public DatabaseHandler db;
    public RelativeLayout rootviewRL;
    RAdataobject radataobj;
    ArrayList<HashMap<String, String>> favobjlist;
    ArrayList<HashMap<String, String>> noteobjlist;
    public ArrayList<RAdataobject> mainobjList;
    public ArrayList<RAdataobject> drawingArrayList;

    public ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_elesson_report);
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
        ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

        UserFunctions.changeFont(rootView,font);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        db = new DatabaseHandler(this);
        rootviewRL = (RelativeLayout) findViewById(R.id.rootview);
        radataobj = (RAdataobject) getIntent().getSerializableExtra("currentpath");

        String elessonBookTitle = getIntent().getStringExtra("BookTitle");
        currentBook = (Book) getIntent().getSerializableExtra("Book");
        favobjlist = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("favList");
        noteobjlist = db.getAllElessonNote(currentBook.get_bStoreID());
        mainobjList = (ArrayList<RAdataobject>) getIntent().getSerializableExtra("mainlist");
        currentBookPath = getIntent().getStringExtra("currentBookPath");
        draw_count = (TextView) findViewById(R.id.draw_count);

        TextView fav_count = (TextView) findViewById(R.id.fav_count);
        fav_count.setText(String.valueOf(favobjlist.size()));

        TextView note_count = (TextView) findViewById(R.id.note_count);
        note_count.setText(String.valueOf(noteobjlist.size()));

        final LinearLayout linearDrawing = (LinearLayout) findViewById(R.id.linear_drawing);
        linearDrawing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawingArrayList.size() == 0){
                    return;
                }
                PopoverView favPopover = new PopoverView(ElessonReportActivity.this, R.layout.fav_list);
                int width = (int) getResources().getDimension(R.dimen.e_report_popover_width);
                int height = (int) getResources().getDimension(R.dimen.e_report_popover_height);
                favPopover.setContentSizeForViewInPopover(new Point(width, height));
                favPopover.showPopoverFromRectInViewGroup(ElessonReportActivity.this.rootviewRL, PopoverView.getFrameForView(linearDrawing), PopoverView.PopoverArrowDirectionUp, true);
                lv = (ListView) favPopover.findViewById(R.id.fav_list);
                ListAdapter favListAdapter = new favListAdapater(ElessonReportActivity.this, true);
                lv.setAdapter(favListAdapter);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        RAdataobject currentObject = drawingArrayList.get(position);
                        Intent resuIntent = new Intent();
                        resuIntent.putExtra("pageID", currentObject.getPageId());
                        setResult(Activity.RESULT_OK, resuIntent);
                        finish();
                    }
                });
            }
        });

        final LinearLayout linearFavourites = (LinearLayout) findViewById(R.id.linear_favourites);
        linearFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (favobjlist.size() == 0) {
                    return;
                }
                PopoverView favPopover = new PopoverView(ElessonReportActivity.this, R.layout.fav_list);
                int width = (int) getResources().getDimension(R.dimen.e_report_popover_width);
                int height = (int) getResources().getDimension(R.dimen.e_report_popover_height);
                favPopover.setContentSizeForViewInPopover(new Point(width, height));
                favPopover.showPopoverFromRectInViewGroup(ElessonReportActivity.this.rootviewRL, PopoverView.getFrameForView(linearFavourites), PopoverView.PopoverArrowDirectionUp, true);
                lv = (ListView) favPopover.findViewById(R.id.fav_list);
                ListAdapter favListAdapter = new favListAdapater(ElessonReportActivity.this, false);
                lv.setAdapter(favListAdapter);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        HashMap<String, String> map = favobjlist.get(position);
                        Intent resuIntent = new Intent();
                        resuIntent.putExtra("pageID", map.get("pageID"));
                        setResult(Activity.RESULT_OK, resuIntent);
                        finish();
                    }
                });
            }
        });

        Button btnBack = (Button) findViewById(R.id.btnback);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView txtTitle = (TextView) findViewById(R.id.textView);
        txtTitle.setText(elessonBookTitle);

        barChart_colors = new String[]{"#E1944A", "#DAC94D", "#695757", "#73A2B1", "#9679B5", "#3F6985", "#40ACDC",
                "#7B8E8A", "#D6655A", "#FFC84E", "#7CB756"};
        loadFav_page();
        calculateViewHeight();
        calcReadAndUnreadPagesAndUpdatePiechart();
        FrameLayout pieChart_View = (FrameLayout) findViewById(R.id.pie_chartview);
        Collections.shuffle(Arrays.asList(barChart_colors));
        colors = new String[]{barChart_colors[2], barChart_colors[5]};
        pieChart = new pieChart(this, colors, (int) readPercent);
//        loadAnimation();
        pieChart_View.addView(pieChart);
        loadPieChartPercent();
        getDrawingsForElesson();
    }

    private class favListAdapater extends BaseAdapter {

        private Context mContext;
        private boolean isForDrawing;

        public favListAdapater(Context context, boolean isForDrawing) {
            mContext = context;
            this.isForDrawing = isForDrawing;
        }

        @Override
        public int getCount() {

            if (isForDrawing) {

                return drawingArrayList.size();

            } else {

                return favobjlist.size();

            }

        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = LayoutInflater.from(ElessonReportActivity.this).inflate(R.layout.fav_text, null);

            final TextView fav_txt = (TextView) vi.findViewById(R.id.fav_Text);

            if (isForDrawing) {
                RAdataobject currentObject = drawingArrayList.get(position);
                fav_txt.setText(currentObject.getName());
            } else {
                HashMap<String, String> map = favobjlist.get(position);
                fav_txt.setText(map.get("PageTitle"));
            }
            return vi;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_elesson_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * parsing statistics.xml
     */
    public void parsestatisticsXml() {
        statisticsArray = new ArrayList<HashMap<String, String>>();
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            DefaultHandler handler = new DefaultHandler() {
                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                    if (qName.equals("Page")) {
                        String pageViewed = attributes.getValue("IsPageViewed");
                        String pageTitle = attributes.getValue("Title");
                        String timeSpent = attributes.getValue("TimeSpent");
                        String PageID = attributes.getValue("PageID");
                        String sequenceId = attributes.getValue("SequenceId");
                        String isFavourite=attributes.getValue("isFavourite");
                        String isDrawn=attributes.getValue("isDrawn");
                        String notes=attributes.getValue("Notes");
                        HashMap<String, String> map = new HashMap<>();
                        map.put("IsPageViewed", pageViewed);
                        map.put("Title", pageTitle);
                        map.put("TimeSpent", timeSpent);
                        map.put("PageID", PageID);
                        map.put("SequenceId", sequenceId);
                        map.put("isFavourite",isFavourite);
                        map.put("isDrawn",isDrawn);
                        map.put("Notes",notes);
                        statisticsArray.add(map);
                    }
                }

                @Override
                public void endElement(String uri, String localName, String qName) throws SAXException {

                }
            };

            String filePath = currentBookPath + "Data/statistics.xml";
            InputStream inputStream = new FileInputStream(filePath);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");
            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");
            saxParser.parse(is, handler);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDrawingsForElesson() {
        drawingArrayList = new ArrayList<RAdataobject>();
        for (int i = 0; i < mainobjList.size(); i++) {

            RAdataobject currentObject = mainobjList.get(i);
            String bgPath = currentBookPath + currentObject.getPageId() + ".png";
            System.out.println(bgPath);
            if (new File(bgPath).exists()) {
                drawingArrayList.add(currentObject);
            }
        }
        draw_count.setText(String.valueOf(drawingArrayList.size()));
    }

    /**
     * sorting top 5 pages viewed of statistics list
     */
    public void sortedList() {
        Collections.sort(statisticsArray, new Comparator<HashMap<String, String>>() {
            @Override
            public int compare(HashMap<String, String> lhs, HashMap<String, String> rhs) {
                String val1 = lhs.get("TimeSpent");
                String val2 = rhs.get("TimeSpent");
                return Integer.valueOf(val2).compareTo(Integer.valueOf(val1));
            }
        });
        sortedArrayList = new ArrayList<HashMap<String, String>>();
        sortedSequenceArray = new ArrayList<HashMap<String, String>>();
        if (statisticsArray.size()<5){
            for (int i = 0; i < statisticsArray.size(); i++) {
                sortedArrayList.add(statisticsArray.get(i));
                sortedSequenceArray.add(statisticsArray.get(i));
            }
        }else {
            for (int i = 0; i < 5; i++) {
                sortedArrayList.add(statisticsArray.get(i));
                sortedSequenceArray.add(statisticsArray.get(i));
            }
        }
    }

    /**
     * calculate and update view height and text values for bar chart
     */
    public void calculateViewHeight() {
        LinearLayout chartLayout = (LinearLayout) findViewById(R.id.barChartLayout);
        HashMap<String, String> map = sortedArrayList.get(0);
        float maxTimeSpent = Integer.parseInt(map.get("TimeSpent"));
        float layoutHeight = chartLayout.getLayoutParams().height - 15;
        float viewHeight = layoutHeight / maxTimeSpent;
        Collections.sort(sortedSequenceArray, new Comparator<HashMap<String, String>>() {
            @Override
            public int compare(HashMap<String, String> lhs, HashMap<String, String> rhs) {
                String val1 = lhs.get("SequenceId");
                String val2 = rhs.get("SequenceId");
                return Integer.valueOf(val1).compareTo(Integer.valueOf(val2));
            }
        });
        initializeViews();
        Button[] btnArray = {barBtn1, barBtn2, barBtn3, barBtn4, barBtn5};
        TextView[] txtArray = {txtView1, txtView2, txtView3, txtView4, txtView5};
        ImageView[] imgArray = {imgView1, imgView2, imgView3, imgView4, imgView5};
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.barchart_anim);
        Collections.shuffle(Arrays.asList(barChart_colors), new Random());
        for (int i = 0; i < sortedSequenceArray.size(); i++) {
            HashMap<String, String> seqMap = sortedSequenceArray.get(i);
            int timeSpent = Integer.parseInt(seqMap.get("TimeSpent"));
            int scaleFactor = (int) (viewHeight * timeSpent);
            Button barchart_btn = btnArray[i];
            int width = barchart_btn.getLayoutParams().width;
            barchart_btn.setLayoutParams(new LinearLayout.LayoutParams(width, scaleFactor));
            barchart_btn.setAnimation(anim);
            barchart_btn.setBackgroundColor(Color.parseColor(barChart_colors[i]));
            if (i != 0) {
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) barchart_btn.getLayoutParams();
                params.setMargins(10, 0, 0, 0);
            }
            barchart_btn.setVisibility(View.VISIBLE);
            TextView textView = txtArray[i];
            ImageView imageView = imgArray[i];
            if (scaleFactor == 0) {
                textView.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
            } else {
                textView.setText(seqMap.get("Title"));
                imageView.setBackgroundColor(Color.parseColor(barChart_colors[i]));
                textView.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.VISIBLE);
            }
        }
    }

    private void initializeViews() {
        barBtn1 = (Button) findViewById(R.id.bar_btn1);
        barBtn2 = (Button) findViewById(R.id.bar_btn2);
        barBtn3 = (Button) findViewById(R.id.bar_btn3);
        barBtn4 = (Button) findViewById(R.id.bar_btn4);
        barBtn5 = (Button) findViewById(R.id.bar_btn5);

        txtView1 = (TextView) findViewById(R.id.txt_view1);
        txtView2 = (TextView) findViewById(R.id.txt_view2);
        txtView3 = (TextView) findViewById(R.id.txt_view3);
        txtView4 = (TextView) findViewById(R.id.txt_view4);
        txtView5 = (TextView) findViewById(R.id.txt_view5);

        imgView1 = (ImageView) findViewById(R.id.img_view1);
        imgView3 = (ImageView) findViewById(R.id.img_view3);
        imgView2 = (ImageView) findViewById(R.id.img_view2);
        imgView4 = (ImageView) findViewById(R.id.img_view4);
        imgView5 = (ImageView) findViewById(R.id.img_view5);

    }

    private void loadFav_page() {
        parsestatisticsXml();
        sortedList();
        final String pageId;
        ArrayList<HashMap<String, String>> favArray = new ArrayList<HashMap<String, String>>();
        ImageButton img_Btn = (ImageButton) findViewById(R.id.book_image);
        TextView fav_title = (TextView) findViewById(R.id.fav_Title);
        for (int i = 0; i < statisticsArray.size(); i++) {
            HashMap<String, String> favCount = statisticsArray.get(i);
            if (favCount.get("isFavourite").equals("yes")) {
                favArray.add(favCount);
            }
        }
        if (favArray.size() > 0) {
            Collections.sort(favArray, new Comparator<HashMap<String, String>>() {
                @Override
                public int compare(HashMap<String, String> lhs, HashMap<String, String> rhs) {
                    String val1 = lhs.get("TimeSpent");
                    String val2 = rhs.get("TimeSpent");
                    return Integer.valueOf(val2).compareTo(Integer.valueOf(val1));
                }
            });
            if (favArray.size()==1){
                pageId=favArray.get(0).get("PageID");
                path = currentBookPath + pageId + "_thumb.png";
                loadBitmap(img_Btn, path);
                fav_title.setText(favArray.get(0).get("Title"));
            }else {
                if (favArray.get(0).get("TimeSpent").equals(favArray.get(1).get("TimeSpent"))) {
                    if (favArray.get(0).get("Notes").equals(favArray.get(1).get("Notes"))) {
                        if (favArray.get(0).get("isDrawn").equals(favArray.get(1).get("isDrawn"))) {
                            pageId = favArray.get(0).get("PageID");
                            path = currentBookPath + pageId + "_thumb.png";
                            loadBitmap(img_Btn, path);
                            fav_title.setText(favArray.get(0).get("Title"));
                        } else {
                            if (favArray.get(0).get("isDrawn").equals("yes")) {
                                pageId = favArray.get(0).get("PageID");
                                path = currentBookPath + pageId + "_thumb.png";
                                loadBitmap(img_Btn, path);
                                fav_title.setText(favArray.get(0).get("Title"));
                            } else {
                                pageId = favArray.get(1).get("PageID");
                                path = currentBookPath + pageId + "_thumb.png";
                                loadBitmap(img_Btn, path);
                                fav_title.setText(favArray.get(1).get("Title"));
                            }
                        }
                    } else {
                        int noteCount = Integer.parseInt(favArray.get(0).get("Notes"));
                        int noteCount1 = Integer.parseInt(favArray.get(1).get("Notes"));
                        if (noteCount > noteCount1) {
                            pageId = favArray.get(0).get("PageID");
                            path = currentBookPath + pageId + "_thumb.png";
                            loadBitmap(img_Btn, path);
                            fav_title.setText(favArray.get(0).get("Title"));
                        } else {
                            pageId = favArray.get(1).get("PageID");
                            path = currentBookPath + pageId + "_thumb.png";
                            loadBitmap(img_Btn, path);
                            fav_title.setText(favArray.get(1).get("Title"));
                        }
                    }
                } else {
                    pageId = favArray.get(0).get("PageID");
                    path = currentBookPath + pageId + "_thumb.png";
                    loadBitmap(img_Btn, path);
                    fav_title.setText(favArray.get(0).get("Title"));
                }
            }
        } else {
            pageId= sortedArrayList.get(0).get("PageID");
            path = currentBookPath + pageId + "_thumb.png";
            loadBitmap(img_Btn, path);
            fav_title.setText(sortedArrayList.get(0).get("Title"));
        }
        img_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.putExtra("pageID",pageId);
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });
    }

    private void loadBitmap(ImageView img, String path) {
        img.setImageBitmap(decodeFile(new File(path)));
    }

    private void calcReadAndUnreadPagesAndUpdatePiechart() {
        ArrayList<Integer> viewedCount = new ArrayList<Integer>();
        for (int i = 0; i < statisticsArray.size(); i++) {
            HashMap<String, String> pageViewedMap = statisticsArray.get(i);
            if (pageViewedMap.get("IsPageViewed").equalsIgnoreCase("yes")) {
                viewedCount.add(i);
            }
        }
        float calc = (float) statisticsArray.size() / viewedCount.size();
        readPercent = (float) 100 / calc;
    }

    private void loadPieChartPercent() {
        pieChartimg1 = (ImageView) findViewById(R.id.pie_chart_img1);
        pieChartimg2 = (ImageView) findViewById(R.id.pie_chart_img2);
        pieCharttxt1 = (TextView) findViewById(R.id.pie_chart_text1);
        pieCharttxt2 = (TextView) findViewById(R.id.pie_chart_text2);

        pieChartimg1.setBackgroundColor(Color.parseColor(colors[0]));
        pieChartimg2.setBackgroundColor(Color.parseColor(colors[1]));
        pieCharttxt1.setText("Read  " + (int) readPercent + "%");
        int text = (100 - (int) readPercent);
        pieCharttxt2.setText("Unread  " + text + "%");
    }

   public void loadAnimation(){
       int x=(pieChart.getLeft()+pieChart.getRight())/2;
       int y =(pieChart.getTop()+pieChart.getBottom())/2;
       int finalRadius=Math.max(pieChart.getWidth(),pieChart.getHeight());
       Animator anim = ViewAnimationUtils.createCircularReveal(pieChart, x,y,0,finalRadius);
       anim.setDuration(5000);
       pieChart.setVisibility(View.VISIBLE);
       anim.start();
   }

    @Override
    public void finish() {
        super.finish();
    }
    private Bitmap decodeFile(File f){
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);
            o.outWidth=1000;
            final int REQUIRED_SIZE=40;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale++;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }
}
