package com.semanoor.manahij;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ptg.views.CircleButton;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.PopoverView;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;

/**
 * Created by karthik on 10-05-2017.
 */

public class TreeStructureView {
    private PopoverView popoverView;
    private Context context;
    private RelativeLayout rootView;
    private TreeStructureCallBack callBack;
    private int currentPageNumber;
    private DatabaseHandler db;
    private Book currentBook;
    private boolean isLoadEnrInpopview;
    private Enrichments currentBtnEnrichment;

    public TreeStructureView(Book book,DatabaseHandler databaseHandler,int pageNumber, PopoverView popView, Context ctx, RelativeLayout root, TreeStructureCallBack treeStructureCallBack){
        this.popoverView = popView;
        this.context = ctx;
        this.rootView = root;
        this.callBack = treeStructureCallBack;
        this.currentPageNumber = pageNumber;
        this.db = databaseHandler;
        this.currentBook = book;
    }

    public void treeViewStructureList(boolean isLoadEnrInpopup,Enrichments currentEnr,View view, boolean isAddEnrich, int catId){
        isLoadEnrInpopview = isLoadEnrInpopup;
        currentBtnEnrichment = currentEnr;
        popoverView = new PopoverView(context, R.layout.tree_view_list);
        popoverView.setContentSizeForViewInPopover(new Point(500,1000));
        popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(view), PopoverView.PopoverArrowDirectionUp, true);
        LinearLayout layout = (LinearLayout) popoverView.findViewById(R.id.tree_view_root);
        TreeNode root = TreeNode.root();
        if (isAddEnrich){
            treeNodeSelectionList(layout,root,catId);
        }else{
            enrichmentTreeList(layout,root,catId);
        }
    }
    private void inflateChild(Integer[] parentArray,TreeNode parent){
        for (int i=0;i<parentArray.length;i++){
            String value = context.getResources().getString(parentArray[i]);
            Enrichments enrichments = new Enrichments(context);
            enrichments.setEnrichmentTitle(value);
            TreeNode child = new TreeNode(new MyTreeHolder.IconTreeItem(enrichments)).setViewHolder(new MyTreeHolder(context));
            parent.addChildren(child);
        }
    }
    private void treeNodeSelectionList(LinearLayout layout, TreeNode root, final int catId){
        Enrichments enrichments = new Enrichments(context);
        enrichments.setEnrichmentTitle("Pages");
        TreeNode parent = new TreeNode(new MyTreeHolder.IconTreeItem(enrichments)).setViewHolder(new MyTreeHolder(context));
        Enrichments enrichments1 = new Enrichments(context);
        enrichments1.setEnrichmentTitle("Content");
        TreeNode parent1 = new TreeNode(new MyTreeHolder.IconTreeItem(enrichments1)).setViewHolder(new MyTreeHolder(context));
        Enrichments enrichments2 = new Enrichments(context);
        enrichments2.setEnrichmentTitle("Services");
        TreeNode parent2 = new TreeNode(new MyTreeHolder.IconTreeItem(enrichments2)).setViewHolder(new MyTreeHolder(context));
        Integer[] parentArray = {R.string.blank_page,R.string.duplicate_page,R.string.internet_search,R.string.tab_from_book};
        Integer[] parentArray1 = {R.string.mindmap,R.string.flash_card};
        Integer[] parentArray2 = {R.string.cloud,R.string.you_tube};
        inflateChild(parentArray,parent);
        inflateChild(parentArray1,parent1);
        inflateChild(parentArray2,parent2);
        root.addChildren(parent,parent1,parent2);
        AndroidTreeView tView = new AndroidTreeView(context, root);
        tView.setDefaultNodeClickListener(new TreeNode.TreeNodeClickListener() {
            @Override
            public void onClick(TreeNode node, Object value) {
                callBack.onClick(node,value,popoverView);
            }
        });
        tView.setDefaultAnimation(true);
        layout.addView(tView.getView());
    }
    private void enrichmentTreeList(LinearLayout layout, TreeNode root, final int catId){
        new SubEnrichmentTabs(currentPageNumber, catId, root,layout).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public class SubEnrichmentTabs extends AsyncTask<Void, Void, Void>{

        ArrayList<Enrichments> enrichmentTabList;
        int pageNumber;
        int categoryId;
        TreeNode root;
        LinearLayout layout;

        public SubEnrichmentTabs(int pageNumber,int catId,TreeNode node,LinearLayout linearLayout){
            this.pageNumber = pageNumber;
            this.categoryId = catId;
            this.root = node;
            this.layout = linearLayout;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (isLoadEnrInpopview) {
                enrichmentTabList = db.getEnrichmentTabListForCatId(currentBook.getBookID(), pageNumber, context, categoryId);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ArrayList<TreeNode> nodeList = new ArrayList<>();
            final Enrichments enrichments = new Enrichments(context);
            enrichments.setCategoryID(currentBtnEnrichment.getEnrichmentId());
            enrichments.setEnrichmentTitle("");
            TreeNode parent = new TreeNode(new MyTreeHolder.IconTreeItem(enrichments)).setViewHolder(new MyTreeHolder(context));
            nodeList.add(parent);
            if (enrichmentTabList != null) {
                for (Enrichments enrichmentsList : enrichmentTabList) {
                    TreeNode enrParent = new TreeNode(new MyTreeHolder.IconTreeItem(enrichmentsList)).setViewHolder(new MyTreeHolder(context));
                    Enrichments enrichment = new Enrichments(context);
                    enrichment.setCategoryID(enrichmentsList.getEnrichmentId());
                    enrichment.setEnrichmentTitle("");
                    TreeNode parent1 = new TreeNode(new MyTreeHolder.IconTreeItem(enrichment)).setViewHolder(new MyTreeHolder(context));
                    enrParent.addChildren(parent1);
                    nodeList.add(enrParent);
                    ArrayList<Enrichments> newList = db.getEnrichmentTabListForCatId(currentBook.getBookID(), pageNumber, context, enrichmentsList.getEnrichmentId());
                    loadSubChild(newList, enrParent,enrichmentsList);
                }
            }
            Enrichments enrichments1 = new Enrichments(context);
            enrichments1.setEnrichmentTitle("Tools");
            TreeNode parent1 = new TreeNode(new MyTreeHolder.IconTreeItem(enrichments1)).setViewHolder(new MyTreeHolder(context));
            Integer[] parentArray = {R.string.rename, R.string.delete, R.string.edit};
            inflateChild(parentArray, parent1);
            nodeList.add(parent1);
            root.addChildren(nodeList);
            final AndroidTreeView tView = new AndroidTreeView(context, root);
            tView.setDefaultNodeClickListener(new TreeNode.TreeNodeClickListener() {
                @Override
                public void onClick(TreeNode node, Object value) {
                   callBack.onClick(node, value,popoverView);
                }
            });
            tView.setDefaultAnimation(true);
            layout.addView(tView.getView());
        }
    }
    private void loadSubChild(ArrayList<Enrichments> tabArray,TreeNode root,Enrichments rootEnr){
        for (Enrichments enrichments : tabArray){
            if (checkSubChildExist(tabArray,rootEnr)) {
                TreeNode parent = new TreeNode(new MyTreeHolder.IconTreeItem(enrichments)).setViewHolder(new MyTreeHolder(context));
                Enrichments enrichment = new Enrichments(context);
                enrichment.setCategoryID(enrichments.getEnrichmentId());
                enrichment.setEnrichmentTitle("");
                TreeNode parent1 = new TreeNode(new MyTreeHolder.IconTreeItem(enrichment)).setViewHolder(new MyTreeHolder(context));
                parent.addChildren(parent1);
                root.addChildren(parent);
                ArrayList<Enrichments> newList = db.getEnrichmentTabListForCatId(currentBook.getBookID(), currentPageNumber, context, enrichments.getEnrichmentId());
                if (checkSubChildExist(newList, enrichments)) {
                    loadSubChild(newList, parent, enrichments);
                }
            }
        }
    }
    private boolean checkSubChildExist(ArrayList<Enrichments> tabArray,Enrichments enrichment){
        for (Enrichments enrichments : tabArray){
            if (enrichments.getCategoryID() == enrichment.getEnrichmentId()){
                return true;
            }
        }
        return false;
    }

    public void treeViewStructureListNew(View view,Enrichments currentEnr){
        currentBtnEnrichment = currentEnr;
        final PopoverView popView = new PopoverView(context, R.layout.tools_layout);
        popView.setContentSizeForViewInPopover(new Point(400,700));
        popView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(view), PopoverView.PopoverArrowDirectionUp, true);
        CircleButton add_btn = (CircleButton) popView.findViewById(R.id.btn_import);
        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enrichments enrichments = new Enrichments(context);
                enrichments.setCategoryID(currentBtnEnrichment.getEnrichmentId());
                enrichments.setEnrichmentTitle("");
                callBack.onClick(null,enrichments,popoverView);
                popView.dissmissPopover(true);
            }
        });
        Button rename_btn = (Button) popView.findViewById(R.id.rename_btn);
        Button delete_btn = (Button) popView.findViewById(R.id.delete_btn);
        Button edit_btn = (Button) popView.findViewById(R.id.edit_btn);
        rename_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enrichments enrichments = new Enrichments(context);
                enrichments.setEnrichmentTitle(context.getResources().getString(R.string.rename));
                callBack.onClick(null,enrichments,popoverView);
                popView.dissmissPopover(true);
            }
        });
        delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enrichments enrichments = new Enrichments(context);
                enrichments.setEnrichmentTitle(context.getResources().getString(R.string.delete));
                callBack.onClick(null,enrichments,popoverView);
                popView.dissmissPopover(true);
            }
        });
        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enrichments enrichments = new Enrichments(context);
                enrichments.setEnrichmentTitle(context.getResources().getString(R.string.edit));
                callBack.onClick(null,enrichments,popoverView);
                popView.dissmissPopover(true);
            }
        });
    }
}
