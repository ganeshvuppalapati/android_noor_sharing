package com.semanoor.manahij;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Point;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobeta.android.dslv.DragSortListView;
import com.mobeta.android.dslv.SimpleDragSortCursorAdapter;
import com.semanoor.source_sboookauthor.Category;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.PopoverView;

import java.util.ArrayList;



/**
 * Created by Suriya on 21/12/15.
 */
public class RearrangeShelf implements DragSortListView.DropListener {

    private MainActivity context;
    private PopoverView popoverView;
    private SimpleDragSortCursorAdapter adapter;
    private ArrayList<Category> categoryListArray;

    public RearrangeShelf(MainActivity context, ArrayList<Category> categoryListArray){
        this.context = context;
        this.categoryListArray = categoryListArray;
    }

    public void displayRearrangeShelfPopUp(){
        popoverView = new PopoverView(context, R.layout.shelf_rearrange_list);
        int width =  (int) context.getResources().getDimension(R.dimen.rearrange_shelf_popup_width);
        int height =  (int) context.getResources().getDimension(R.dimen.rearrange_shelf_popup_height);
        popoverView.setContentSizeForViewInPopover(new Point(width, height));
        popoverView.showPopoverFromRectInViewGroup(context.rl, PopoverView.getFrameForView(context.btnArrange), PopoverView.PopoverArrowDirectionUp, true);

        String[] cols = {"name"};
        int[] ids = {R.id.text};
        adapter = new MAdapter(context,
                R.layout.list_item_click_rearrange, null, cols, ids, 0);

        DragSortListView dslv = (DragSortListView) popoverView.findViewById(android.R.id.list);
        dslv.setAdapter(adapter);
        dslv.setDropListener(this);

        // build a cursor from the String array
        MatrixCursor cursor = new MatrixCursor(new String[] {"_id", "name"});
        //String[] artistNames = getResources().getStringArray(R.array.jazz_artist_names);
        for (int i = 0; i < categoryListArray.size() - 1; i++) {
            Category category = categoryListArray.get(i);
            cursor.newRow()
                    .add(i)
                    .add(category.getCategoryName());
        }
        adapter.changeCursor(cursor);
    }

    @Override
    public void drop(int from, int to) {
        System.out.println(from);
        int frmCID = from + 1;
        int toCID = to + 1;
        Category category = categoryListArray.get(from);
        //categoryListArray.add(to, category);
        if (from > to) {
            context.db.executeQuery("update tblCategory set newCID=newCID+1 where newCID>='"+toCID+"' and newCID<'"+frmCID+"'");
        } else {
            context.db.executeQuery("update tblCategory set newCID=newCID-1 where newCID<='"+toCID+"' and newCID>'"+frmCID+"'");
        }
        context.db.executeQuery("update tblCategory set newCID='"+toCID+"' where CID='"+category.getCategoryId()+"'");
        categoryListArray = context.gridShelf.reloadCategoryListArray();
        adapter.notifyDataSetChanged();
        context.gridView.invalidateViews();
    }

    private class MAdapter extends SimpleDragSortCursorAdapter {
        private Context mContext;

        public MAdapter(Context ctxt, int rmid, Cursor c, String[] cols, int[] ids, int something) {
            super(ctxt, rmid, c, cols, ids, something);
            mContext = ctxt;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = super.getView(position, convertView, parent);
            TextView tv = (TextView) v.findViewById(R.id.text);
            setTexttoCategoryName(categoryListArray.get(position),tv);
           // tv.setText(categoryListArray.get(position).getCategoryName());
            return v;
        }
    }

    public void setTexttoCategoryName(Category category, TextView txtView){
        if (category.getCategoryId()==1){
            txtView.setText(R.string.books);

        }else if (category.getCategoryId()==2){
            txtView.setText(R.string.mybooks);

        }else if (category.getCategoryId()==3){
            txtView.setText(R.string.Quizzes);

        }else if (category.getCategoryId()==4){
            txtView.setText(R.string.elesson);

        }else if (category.getCategoryId()==5){
            txtView.setText(R.string.rzooom);

        }else if (category.getCategoryId()== Globals.mindMapCatId){
            txtView.setText(R.string.shelf_Mindmap);
        }else if(category.getCategoryId()==Globals.myMediaCatId){
            txtView.setText(R.string.my_media);
        }
        else if (category.getCategoryName().equals("New Category")){
            txtView.setText(R.string.new_category);
        }else if (category.getCategoryName().equals("Add category")){
            txtView.setText(R.string.add_category);
        }else {
            txtView.setText(category.getCategoryName());
        }
    }
}
