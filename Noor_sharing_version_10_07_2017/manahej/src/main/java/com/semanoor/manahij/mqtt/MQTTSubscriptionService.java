package com.semanoor.manahij.mqtt;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.FlashCardActivity;
import com.semanoor.manahij.SimpleCardStackAdapter;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.manahij.mqtt.datamodels.Message;
import com.semanoor.manahij.MainActivity;
import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.SlideMenuWithActivityGroup;
import com.semanoor.source_manahij.SemaWebView;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by semanoor on 18/04/17.
 */

public class MQTTSubscriptionService extends Service implements MqttCallback {

    MqttAndroidClient mqttAndroidClient;
    ArrayList<String> allTopicesSubscribed = new ArrayList<>();
    ManahijApp mNoorApplication;
    String TopicName;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mNoorApplication = (ManahijApp) getApplication();
        subScribeToMqtt();
        subscribeToInAppCommunication();

    }

    @Override
    public void onDestroy() {
        sendUsersList( ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(),String.valueOf(0),getApplicationContext());
        try {
            String[] topicNames = new String[allTopicesSubscribed.size()];
            allTopicesSubscribed.toArray(topicNames);
            mqttAndroidClient.unsubscribe(topicNames);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
           // mqttAndroidClient.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        this.unregisterReceiver(mInAppCommunicator);
        mqttAndroidClient.unregisterResources();
//        mqttAndroidClient.close();
        mqttAndroidClient = null;
        super.onDestroy();
    }

    private void subscribeToInAppCommunication() {
        IntentFilter mIntentFilter = new IntentFilter(Constants.BROADCAST_ACTION_SUBSCRIBE_LIST_OF_TOPICS);
        mIntentFilter.addAction(Constants.BROADCAST_ACTION_SUBSCRIBE_TO_SINGLE_TOPIC);
        mIntentFilter.addAction(Constants.BROADCAST_ACTION_PUBLISH);
        mIntentFilter.addAction(Constants.BROADCAST_ACTION_UNSUBSCRIBE);
        this.registerReceiver(mInAppCommunicator, mIntentFilter);


    }

    private void subScribeToMqtt() {
        String   SERVER_IP = "174.129.30.141";
        String  SERVER_PORT = "1883";
        String  CLIENT_IDENTITY = ManahijApp.getInstance().getPrefManager().getClientName();

        String serverURL = "tcp://" + SERVER_IP + ":" + SERVER_PORT;
        String clientIdentity =CLIENT_IDENTITY + System.currentTimeMillis();

        try {
            mqttAndroidClient = new MqttAndroidClient(MQTTSubscriptionService.this, serverURL, clientIdentity);
            mqttAndroidClient.setCallback(this);
            MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
            mqttConnectOptions.setKeepAliveInterval(Integer.MAX_VALUE);
            mqttConnectOptions.setCleanSession(false);
            mqttAndroidClient.connect(mqttConnectOptions, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.e("MQTT Client", "Success and connected to MQTT Client.");
                    sendConnectionChangeBroadCastToAll(1);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.e("MQTT Client", "Failed and connected to MQTT Client.");
                    Toast.makeText(getApplicationContext(), "Failed to connect to MQTT service.", Toast.LENGTH_LONG).show();
                    sendConnectionChangeBroadCastToAll(0);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("MQTT Client", "Failed  MQTT Client.");

            // Toast.makeText(getApplicationContext(), "Failed to connect to MQTT service.", Toast.LENGTH_LONG).show();
        }
    }

    public void unSubScribe(String topic){
        try {
            mqttAndroidClient.unsubscribe(topic);

        } catch (MqttException e) {
            e.printStackTrace();
        }

    }
    private void sendConnectionChangeBroadCastToAll(int status) {
        try {
            LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                    new Intent(NoorActivity.FILTER).putExtra(Constants.KEYTYPE, "status").putExtra(Constants.MQTT_STATUS, status));

            JSONObject obj = new JSONObject();
            obj.put("action", "status");
            obj.put(Constants.MQTT_STATUS, status);
            LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                    new Intent(CallConnectionStatusService.FILTER).putExtra(Constants.KEYMESSAGE,obj.toString()));

            Intent mIntent = new Intent(Constants.BROADCAST_ACTION_MQTT_STATUS);
            mIntent.putExtra(Constants.MQTT_STATUS, status);
            sendBroadcast(mIntent);
        }catch (Exception e){
            Log.e("ss",e.toString());
        }

    }


    private void subScribeToTopic(String topic) {
        try {
            mqttAndroidClient.subscribe(topic, 0);
        } catch (MqttException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Failed to subscribe to MQTT service.", Toast.LENGTH_LONG).show();
        }
    }



    public void publishMsg(String topic, String msgs, Context context) {
        if(ManahijApp.getInstance().isInternetAvailable()) {

                try {
                //
                    //
                    //  Log.e("mspb", topic);
                    if (!msgs.isEmpty()) {
                        Intent mIntent = new Intent();
                        mIntent.setAction(Constants.BROADCAST_ACTION_PUBLISH);
                        mIntent.putExtra(Constants.PUBLISH_TOPIC, topic);
                       // String msg = msgs;
                      //  msg = msg + "---" + System.currentTimeMillis();
                        mIntent.putExtra(Constants.PUBLISH_MSG, msgs);
                        context.sendBroadcast(mIntent);
                    }


                } catch (Exception ex) {
                    Log.e("ex", ex.toString());
                }

        }


    }

    public void sendUsersList(String TopicName,String status,Context context) {

        if (!status.isEmpty()) {
            Intent mIntent = new Intent();
            mIntent.setAction(Constants.BROADCAST_ACTION_PUBLISH);
            mIntent.putExtra(Constants.PUBLISH_TOPIC, Constants.NOORUSERSTATUS);
            String msg = TopicName + ":" + status;


            byte[] data;
            String base64msg = "";
            try {
                data = msg.getBytes();
                base64msg = Base64.encodeToString(data, Base64.DEFAULT);
                base64msg = base64msg.replace("\n", "");
               // Log.e("Base 64 ", base64msg);

            } catch (Exception e) {

                e.printStackTrace();

            }


            mIntent.putExtra(Constants.PUBLISH_MSG, base64msg);
            context.sendBroadcast(mIntent);


        }
    }
    public void publishMsg(String topic, String msgs) {
        try {
            mqttAndroidClient.publish(topic, msgs.getBytes(), 0, false);


        } catch (Exception ex) {
            Log.e("ex", ex.toString());
        }


    }


    @Override
    public void connectionLost(Throwable cause) {
        Toast.makeText(getApplicationContext(), " MQTT connectionLost.", Toast.LENGTH_LONG).show();
        sendConnectionChangeBroadCastToAll(2);
        Log.e("MQTT ", "MQTT connectionLost.");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        Log.e("noormsg",topic+""+message);
        try {


            String msg = message.toString();
           // String[] msgDat = msg.split("---");
           // String fist = msgDat[0];
            JSONObject    object=new JSONObject(msg);
            if(!isTrue(object.getString("device_id"))) {
                String type=  object.getString("type");
                switch (type) {
                    case Constants.KEYCALLCONNECTION:

                       /* LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                new Intent(NoorActivity.FILTER).putExtra(Constants.KEYTYPE, object.getString("action")).putExtra(Constants.KEYMESSAGE,object.toString()));
                        break;*/
                        switch (object.getString("action")) {

                            case Constants.KEYCALLREQUEST:
                                ManahijApp app = (ManahijApp)getApplicationContext();
                                if(!app.getCallState()){
                                    LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                            new Intent(NoorActivity.FILTER).putExtra(Constants.KEYTYPE, object.getString("action")).putExtra(Constants.KEYMESSAGE,object.toString()));
                                }else {
                                    JSONObject obj = new JSONObject();
                                    obj.put("type", Constants.KEYCALLCONNECTION);
                                    obj.put("action","user_is_in_another_call");
                                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                                    publishMsg(object.getString("topic_name"),obj.toString(),this);
                                    LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                            new Intent(NoorActivity.FILTER).putExtra(Constants.KEYTYPE, "call_waiting").putExtra(Constants.KEYMESSAGE,object.toString()));
                                }
                                break;
                            case Constants.KEYCALLREQUESTR:
                            case Constants.KEYCALLDISCONNECTIONREQUEST:
                                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                        new Intent(NoorActivity.FILTER).putExtra(Constants.KEYTYPE, object.getString("action")).putExtra(Constants.KEYMESSAGE,object.toString()));
                                break;
                            case "user_is_in_another_call":
                            case "call_accept":
                            case "call_reject":
                                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                        new Intent(SlideMenuWithActivityGroup.FILTER).putExtra(Constants.KEYMESSAGE, object.toString()));
                                //sendBroadCastCALL(SlideMenuWithActivityGroup.FILTER,object.toString());

                                break;
                        /*    LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                    new Intent(NoorActivity.FILTER).putExtra(Constants.KEYTYPE, object.getString("action")).putExtra(Constants.KEYMESSAGE,object.toString()));
                            break;*/


                        }


                        break;

                 /*   case Constants.KEYOPENBOOK:

                            LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                    new Intent(MainActivity.FILTER).putExtra(Constants.KEYMESSAGE, object.toString()));

                        break;*/

                    case Constants.KEYBOOK:
                        if(object.getString("action").equals(Constants.KEYPAGECHANGE)) {
                            if(ManahijApp.getInstance().getPrefManager().isBookOpend()) {
                                //  sendBroadCastCALL(BookViewReadActivity.FILTER,object.toString());

                                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                        new Intent(BookViewReadActivity.FILTER).putExtra(Constants.KEYMESSAGE, object.toString()));
                            }else {
                                //sendBroadCastCALL(MainActivity.FILTER,object.toString());
                                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                        new Intent(MainActivity.FILTER).putExtra(Constants.KEYMESSAGE, object.toString()));
                            }
                        }else if(object.getString("action").equals(Constants.KEYOPENBOOK)){
                            // sendBroadCastCALL(MainActivity.FILTER,object.toString());
                            LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                    new Intent(MainActivity.FILTER).putExtra(Constants.KEYMESSAGE, object.toString()));
                        }
                        else {
                            //sendBroadCastCALL(BookViewReadActivity.FILTER,object.toString());

                            LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                    new Intent(BookViewReadActivity.FILTER).putExtra(Constants.KEYMESSAGE, object.toString()));
                        }

                        break;

                    case Constants.DRAWING:
                        //sendBroadCastCALL(BookViewReadActivity.FILTER,object.toString());
                        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                new Intent(BookViewReadActivity.FILTER).putExtra(Constants.KEYMESSAGE, object.toString()));
                        break;

                    case Constants.KEYVEDIOCALL:
                        //   sendBroadCastCALL(CallConnectionStatusService.FILTER,object.toString());
                        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                new Intent(CallConnectionStatusService.FILTER).putExtra(Constants.KEYMESSAGE,object.toString()));

                        break;


                    case Constants.FLASHCARD:
                        // sendBroadCastCALL(FlashCardActivity.this,object.toString());
                        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                                new Intent(FlashCardActivity.FILTER).putExtra(Constants.KEYMESSAGE,object.toString()));

                        break;



                }
            }
            Toast.makeText(getApplicationContext(),message.toString(),Toast.LENGTH_LONG).show();

        }catch (Exception e){
            Log.e("ex",e.toString());
        }

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }
    private boolean isTrue(String deviceId){
        boolean ist= false;
        if(deviceId.equals(ManahijApp.getInstance().getPrefManager().getDeviceID())){
            ist=true;
        }
        return ist;
    }

    public void subscribeToNewTopic(String name,Context context) {

        Intent mIntent = new Intent(Constants.BROADCAST_ACTION_SUBSCRIBE_TO_SINGLE_TOPIC);
        mIntent.putExtra(Constants.TOPIC_NAME, name);
        mIntent.putExtra(Constants.TOPIC_SUBSCRIPTION_STATUS, 1);
        context. sendBroadcast(mIntent);

    }
    public void unSubScribeTopic(String name,Context context) {
        try {

            if (name.equals(ManahijApp.getInstance().getPrefManager().getMainChannel())) {
                JSONObject object = new JSONObject();
                object.put("action", "close_inCall");
                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(

                        new Intent(FlashCardActivity.FILTER).putExtra(Constants.KEYMESSAGE, object.toString()));

            }
            Intent mIntent = new Intent();
            mIntent.setAction(Constants.BROADCAST_ACTION_UNSUBSCRIBE);
            mIntent.putExtra(Constants.PUBLISH_TOPIC, name);
            context.sendBroadcast(mIntent);

        }catch (Exception e){
            Log.e("unsubsrcibe","unSubScribeT");
        }

    }


    BroadcastReceiver mInAppCommunicator = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Constants.BROADCAST_ACTION_SUBSCRIBE_LIST_OF_TOPICS)) {
                ArrayList<String> topics = intent.getExtras().getStringArrayList(Constants.TOPIC_NAME);
                for (String topic : topics)
                    subScribeToTopic(topic);
                allTopicesSubscribed.addAll(topics);
            } else if (intent.getAction().equals(Constants.BROADCAST_ACTION_SUBSCRIBE_TO_SINGLE_TOPIC)) {
                String topic = intent.getExtras().getString(Constants.TOPIC_NAME);
                    subScribeToTopic(topic);
                allTopicesSubscribed.add(topic);
            } else if (intent.getAction().equals(Constants.BROADCAST_ACTION_PUBLISH)) {

                String topic = intent.getExtras().getString(Constants.PUBLISH_TOPIC);
                String msg = intent.getExtras().getString(Constants.PUBLISH_MSG);
                    publishMsg(topic, msg);

            }
            else if (intent.getAction().equals(Constants.BROADCAST_ACTION_UNSUBSCRIBE)) {
                String topic = intent.getExtras().getString(Constants.PUBLISH_TOPIC);
                //  String msg = intent.getExtras().getString(Constants.PUBLISH_MSG);
                    unSubScribe(topic);


            }
        }

    };

    public String getTime() {
        Calendar mCalendar = Calendar.getInstance();
        return mCalendar.get(Calendar.DAY_OF_MONTH) + "-" + (mCalendar.get(Calendar.MONTH) + 1) + "-" + mCalendar.get(Calendar.YEAR) + " " + mCalendar.get(Calendar.HOUR_OF_DAY) + ":" + mCalendar.get(Calendar.MINUTE) + ":" + mCalendar.get(Calendar.SECOND);
    }





    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }
    public void disConnectCall(){

        try {
            JSONObject object = new JSONObject();
            object.put("type", Constants.KEYCALLCONNECTION);
            object.put("action",Constants.KEYCALLDISCONNECTIONREQUEST);
            object.put("device_id",ManahijApp.getInstance().getPrefManager().getDeviceID());

            publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(), object.toString(), getApplicationContext());
            sendUsersList(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(), String.valueOf(0), getApplicationContext());
            unSubScribeTopic(ManahijApp.getInstance().getPrefManager().getMainChannel(), getApplicationContext());
            subscribeToNewTopic(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(), getApplicationContext());
            ManahijApp.getInstance().getPrefManager().addIsMainChannel(false);
            stopService(new Intent(getApplicationContext(), CallConnectionStatusService.class));
            ManahijApp app = (ManahijApp)getApplicationContext();
            app.setCallState(false);
        }catch (Exception e){
            Log.e("MainDEstroy",e.toString());
        }
    }
}
/*
try {
        JSONObject object = new JSONObject();
        object.put("type", Constants.KEYCALLDISCONNECTION);
        object.put("callRequest","disconnect");
        object.put("deviceId",ManahijApp.getInstance().getPrefManager().getDeviceID());

        service.publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(),object.toString(),getApplicationContext() );
        service.sendUsersList(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(),String.valueOf(1),getApplicationContext());
        service.unSubScribeTopic(ManahijApp.getInstance().getPrefManager().getMainChannel(), getApplicationContext());
        service.subscribeToNewTopic(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(), getApplicationContext());
        ManahijApp.getInstance().getPrefManager().addIsMainChannel(false);
        }catch (Exception e){
        Log.e("call service",e.toString());
        }*/
