package com.semanoor.manahij;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.pdf.PdfRenderer;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ptg.views.CircleButton;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.PageDetails;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IllegalFormatCodePointException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import static android.graphics.Bitmap.createBitmap;
import static android.graphics.Bitmap.createScaledBitmap;

public class Index_new extends ListActivity implements OnClickListener, RadioGroup.OnCheckedChangeListener{
	
	private DatabaseHandler db;
	String filePath = new String();
	public String bookName;
	//public ArrayList<String> indexArray = new ArrayList<String>();
	public ArrayList<String> bookMarkPno = new ArrayList<String>(); 
	//public ArrayList<Integer> bookMarkTno = new ArrayList<Integer>();
	//public ArrayList<Integer> indexPgNoArray = new ArrayList<Integer>();
	/* Enriched */
	public ArrayList<PageDetails> enrichPageArray = new ArrayList<PageDetails>();
	//public ArrayList<String> finalArray = new ArrayList<String>();
	public ArrayList<String> finalListEnrichedArray = new ArrayList<String>();
	//private drawView drawLayer;
	/* Enriched */
	SegmentedRadioButton segmentText;
	Toast mToast;
	int selectedSegment = 1;
	ListView lv;
	int pageCount;
	//int lngCategoryID;
	public int indexlistsize;
	public Boolean enriched=false;
	private RelativeLayout rootviewLL;
	public Book currentBook;
	private RecyclerView gridview;
	ArrayList<String> drawPno = new ArrayList<String>();
	ArrayList<String> drawPnoArray=new ArrayList<String>();
	public String xmlValues = "";
	PageDetails page;
	ArrayList<Enrichments> enrichmentTabList;
	String pdfName;
	public ArrayList<HashMap<String, String>> PageEnrichTabCount;
	private ProgressBar progressBar;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		// No title Bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.index);
		
		db = new DatabaseHandler(this);
		segmentText = (SegmentedRadioButton) findViewById(R.id.segment_text);
		RadioButton btn_drawing= (RadioButton)segmentText. findViewById(R.id.segDrawing);
		segmentText.setOnCheckedChangeListener(this);

		Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
		ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

		UserFunctions.changeFont(rootView,font);
		
		//mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
		
		Intent i = getIntent();

		currentBook = (Book) getIntent().getSerializableExtra("Book");
		
		
		bookName = currentBook.get_bStoreID();

		File srcDir = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book");
		String[] path = srcDir.list();
		for(String file : path) {
			String fromPath = srcDir + "/" + file;
			String[] str = file.split("\\.");
			if(file.contains(".pdf")){
				pdfName=fromPath;
				break;
			}
		}

		/* Enriched */
		if(bookName.contains("M") || bookName.contains("C")) {
			btn_drawing.setVisibility(View.VISIBLE);
			drawPno = getDrawingFiles();
			selectedBook();
			enrichmentTabList = db.getAllEnrichmentTabListForTheBook(currentBook.getBookID(), this);
			pagesWithEnrichments();
			getEnrichmentsizeforpage();
			//enrichPageArray = i.getStringArrayListExtra("Enriched");
		/* Enriched */
		}else if(bookName.contains("P")){
                 for(int pNo=0;pNo<currentBook.getTotalPages();pNo++){
					 if(pNo==0){
						 page=new PageDetails();
						 //page.setTitle("Page 1");
						 page.setTitle(getResources().getString(R.string.cover_page));
						 page.setPath("1.Page");
                         enrichPageArray.add(page);
					 }else if(pNo==currentBook.getTotalPages()-1){
						 int p=pNo+1;
						 page=new PageDetails();
						 //page.setTitle("Page 2");
						 page.setTitle(getResources().getString(R.string.end_page));
						 page.setPath(p+".Page");
						 enrichPageArray.add(page);
					 }else{
						 int p=pNo+1;
						 page=new PageDetails();
						// page.setTitle("Page "+pNo+"");
						 page.setTitle(+pNo+" "+getResources().getString(R.string.page));
						 page.setPath(p + ".Page");
						 enrichPageArray.add(page);
					 }
			}
			enrichmentTabList = db.getAllEnrichmentTabListForTheBook(currentBook.getBookID(), this);
			pagesWithEnrichments();
            getEnrichmentsizeforpage();
		}

		gridview = (RecyclerView) findViewById(R.id.gridview);
		gridview.addItemDecoration(new ItemDecoration(Index_new.this,R.dimen.recycler_padding));
		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		pageCount = currentBook.getTotalPages();
		////System.out.println("pageC:"+pageCount);

		 selectBookMark();

		/* changes for enrich */
		lv = getListView();
		if(selectedSegment == 3){
		lv.setAdapter(new listAdapter(this));
		}else if(selectedSegment == 1){
			lv.setVisibility(View.GONE);
			gridview.setVisibility(View.VISIBLE);
			GridLayoutManager gridLayout=new GridLayoutManager(Index_new.this,getResources().getInteger(R.integer.grid_column),GridLayoutManager.VERTICAL,false);
			gridview.setHasFixedSize(true);
			gridview.setNestedScrollingEnabled(false);
			gridview.setLayoutManager(gridLayout);
			gridview.setItemAnimator(new DefaultItemAnimator());
			gridview.setAdapter(new RecyclerAdapter());
			gridview.addOnItemTouchListener(new RecyclerTouchListener(Index_new.this, gridview, new RecyclerTouchListener.ClickListener() {
				@Override
				public void onClick(View view, int position) {
					int pageno = position;
					//String[] pgValue = mahuMsg.split("\\|");
					Intent resuIntent = new Intent();
					resuIntent.putExtra("id", pageno+1);
					setResult(Activity.RESULT_OK, resuIntent);
					finish();
				}

				@Override
				public void onLongClick(View view, int position) {

				}
			}));

		}
		else if(selectedSegment == 2){
			    lv = getListView();
				lv.setAdapter(new indexlistadapter(this));
				enriched=true;

		}
		else if(selectedSegment == 4){
			//lv.setAdapter(new drawListAdapter(this));
		}


		lv.setOnItemClickListener(new OnItemClickListener() {

			
			//@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				if(selectedSegment == 2){
					if(finalListEnrichedArray.size()>0){
						//String mahuMsg = enrichPageArray.get(position);

						boolean textAdded=false;
						String currentpagepath=finalListEnrichedArray.get(position);
						String[] path=currentpagepath.split("\\|");
						for(Enrichments enrich:enrichmentTabList){
							if(enrich.getEnrichmentTitle().equals(path[1])&& path[2].equals(String.valueOf(enrich.getEnrichmentPageNo()))){
								textAdded=true;
							}
						}
						if(textAdded) {
							String p[]=path[0].split("##");
							//String[] pageno=p[2].split("-");
							Intent resuIntent = new Intent();
							resuIntent.putExtra("id", p);
							setResult(Activity.RESULT_OK, resuIntent);

						}else{
							String root[]=path[0].split("\\.");
							int pNo=Integer.parseInt(root[0]);
							Intent resuIntent = new Intent();
							resuIntent.putExtra("id", pNo);
							setResult(Activity.RESULT_OK, resuIntent);
						}

					}

				}
				else if(selectedSegment == 3){
					 Intent resuIntent = new Intent();
					 String str1 = bookMarkPno.get(position);
					 String[] pgValue = str1.split("\\-");
					 if(!(pgValue.length>1)){
	                 String[] str = str1.split("Page ");
	                 int pNO = Integer.parseInt(str[1]) + 1;
	                 resuIntent.putExtra("id", pNO);
					 setResult(Activity.RESULT_OK, resuIntent);
					 }
					 else{
						 pgValue[0] = pgValue[0].replace("Page ", "");
						 BookView.bkmarkselected=true;
						 resuIntent.putExtra("id", pgValue);
						 setResult(Activity.RESULT_OK, resuIntent);
					 }
				}
				else if(selectedSegment == 4){

					Intent resuIntent = new Intent();
					String str1 = drawPno.get(position);
					str1 = str1.substring(0,str1.lastIndexOf(".png")); //page8-1
					String[] pgValue = str1.split("\\-");//page8,1
					if(!(pgValue.length>1)){
						String[] str = str1.split("Page ");
						int pNO = Integer.parseInt(str[1])+1;
						resuIntent.putExtra("id",pNO-1 );
						setResult(Activity.RESULT_OK, resuIntent);
					}
					else{
						String pageVal=pgValue[0].replace("Page ", "");

						int pNO=Integer.parseInt(pageVal)-1;
						pgValue[0] =""+pNO;
						resuIntent.putExtra("id", pgValue);
						setResult(Activity.RESULT_OK, resuIntent);
					}


				}
				finish(); 
			}
		});

		CircleButton btnBack = (CircleButton) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(this);
		
		RadioButton rdTblContent = (RadioButton) findViewById(R.id.segTblCon);
		RadioButton rdBookMark = (RadioButton) findViewById(R.id.segBookMark);
		rdTblContent.setText(getResources().getString(R.string.table_of_contents));
		rdBookMark.setText(getResources().getString(R.string.bookmarks_caps));
		
		rootviewLL = (RelativeLayout) findViewById(R.id.indexView);
		//applyTheme(g.getSelectedTheme());
	}
      private ArrayList<String> getDrawingFiles(){
		
		String path = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookName+"Book/"+"FreeFiles/Drawfiles/";
		
	    	File file = new File(path);
	    	if(file.exists()) {
				File[] allfiles = file.listFiles();
				if (allfiles.length == 0) {
					return null;
				} else {
					for (int i = 0; i < allfiles.length; i++) {
						drawPnoArray.add(allfiles[i].getName());
					}
				}
			}
	     	return drawPnoArray;
	}

    /* changes for enrich */

	private void selectBookMark() {
		SQLiteDatabase sql = db.getReadableDatabase();
		String query = "select PageNo,TagID,TabNo from tblBookMark where BName='"+bookName+"' order by PageNo and TabNo";
		Cursor c = sql.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			if(c.getInt(2)==0){
				bookMarkPno.add("Page "+(c.getInt(0)-1));
			}else{
				bookMarkPno.add("Page "+(c.getInt(0)-1)+"-"+c.getInt(2));
			}
           //bookMarkTno.add(c.getInt(1));

			c.moveToNext();
		}
		c.close();
		sql.close();
	}

	//list adapter for custom index page
    public class indexlistadapter extends BaseAdapter
    {

		public indexlistadapter(Index_new index)
		{
			// TODO Auto-generated constructor stub
		}

		public int getCount() 
		{
			// TODO Auto-generated method stub

			return finalListEnrichedArray.size();
		}

		public Object getItem(int position) 
		{
			// TODO Auto-generated method stub
			return null;
		}

		public long getItemId(int position) 
		{
			// TODO Auto-generated method stub  android:paddingRight="10dp"
			return 0;
		}

		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			// TODO Auto-generated method stub
			View indexlistview = convertView;
			Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
			UserFunctions.changeFont(parent,font);
			if(convertView == null)
			indexlistview = getLayoutInflater().inflate(R.layout.indexlistview, null);
			TextView tv_indexlistview = (TextView) indexlistview.findViewById(R.id.tv_indexlistview);
			TextView tv_enrich = (TextView) indexlistview.findViewById(R.id.tv_enrich);

			       //String[] str = enrichPageArray.get(position).split("U\\+202B");
			       //mahumsg = str[0];
			        boolean textAdded=false;
			        String currentpagepath=finalListEnrichedArray.get(position);
                    String[] path=currentpagepath.split("\\|");
			        for(int i=0;i<enrichmentTabList.size();i++){
						Enrichments enrich=enrichmentTabList.get(i);
						if(enrich.getEnrichmentTitle().equals(path[1])&& path[2].equals(String.valueOf(enrich.getEnrichmentPageNo()))){
							textAdded=true;
						}
					}
			/*String[] page = new String[2];
			if (position == 0 || position == finalListEnrichedArray.size() - 1){
				if(path[1].contains(getResources().getString(R.string.pagee))) {
					page = path[1].split(getResources().getString(R.string.pagee));
				}else if(path[1].contains("Page")){
					page = path[1].split("Page");
				}
			}else {
				if(path[1].contains(getResources().getString(R.string.page_arabic))){
					page = path[1].split(getResources().getString(R.string.page_arabic));
				}else if(path[1].contains("Page")){
					page = path[1].split("Page");
				}
			}  */
			         if(textAdded){
						 tv_indexlistview.setVisibility(View.INVISIBLE);
						 tv_enrich.setVisibility(View.VISIBLE);
						 tv_enrich.setText(path[1]);
						// tv_enrich.setText(""+page[1]+" "+getResources().getString(R.string.page));

					 }else{
						 tv_enrich.setVisibility(View.INVISIBLE);
						 tv_indexlistview.setVisibility(View.VISIBLE);
						 tv_indexlistview.setText(path[1]);
						// tv_indexlistview.setText(""+page[1]+" "+getResources().getString(R.string.page));

					 }


			return indexlistview;
		}
		
    }

    public class indexthumbnailAdapter extends BaseAdapter
    {

		public indexthumbnailAdapter(Index_new index)
		{
		}

		public int getCount() 
		{
			return pageCount;
		}

		public Object getItem(int position) 
		{
			return null;
		}

		public long getItemId(int position) 
		{
			return 0;
		}

		public View getView(final int position, View convertView, ViewGroup parent)
		{
			// TODO Auto-generated method stub

			//View indexlistview = convertView;

			final ViewHolder holder;
			View view = convertView;
			if (convertView == null){
				Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
				UserFunctions.changeFont(parent,font);
				view = getLayoutInflater().inflate(R.layout.thumbnailindex, null);
				holder = new ViewHolder();
				assert view != null;
				holder.img_indexlistview = (ImageView) view.findViewById(R.id.imageView2);
				holder.tv_pageno= (TextView) view.findViewById(R.id.tv_pageno);
				holder.tv_enrichCount= (TextView) view.findViewById(R.id.tv_enrichCount);
				holder.tv_enrichCount.setVisibility(View.GONE);
				view.setTag(holder);
		    }else {
				holder = (ViewHolder) view.getTag();
			}

			int pageNumber=position+1;
            if(view!=null) {
				 if(currentBook.get_bStoreID().contains("P")){
					 holder.img_indexlistview.setBackgroundColor(Color.WHITE);
				 }
				if (position == 0) {
					holder.tv_pageno.setText(R.string.cover_page);
					String thumbnailImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "FreeFiles/frontThumb.png";
					//}
					if (new File(thumbnailImagePath).exists()) {
						Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
						holder.img_indexlistview.setImageBitmap(bitmap);
					}
					/*else{
						if (!new File(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + position + ".png").exists()){
							if (currentBook.get_bStoreID().contains("P")&&holder.img_indexlistview!=null){
								new ImageLoader(holder.img_indexlistview,position).execute();
							}
						}else{
							holder.img_indexlistview.setImageBitmap(null);
						}
					}*/

				} else if (position == pageCount-1) {
					String page = getResources().getString(R.string.end_page);
					holder.tv_pageno.setText(page);
					String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_end.png";
					if (!new File(thumbnailImagePath).exists()) {
						thumbnailImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "FreeFiles/backThumb.png";
					}
					if (new File(thumbnailImagePath).exists()) {
						Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
						holder.img_indexlistview.setImageBitmap(bitmap);
					}
					/*else{
						if (!new File(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + position + ".png").exists()){
							if (currentBook.get_bStoreID().contains("P")&&holder.img_indexlistview!=null){
								new ImageLoader(holder.img_indexlistview,position).execute();
							}
						}else{
							holder.img_indexlistview.setImageBitmap(null);
						}
					}*/
				} else {
					//int pno = position;
					int pno = pageNumber - 1;
					//String page = getResources().getString(R.string.page);
					holder.tv_pageno.setText(pno + " ");
					HashMap<String, String> map =PageEnrichTabCount.get(pno);
					//String enrichCount=map.get(pageNumber);
					int enrichCount = Integer.parseInt(map.get(pageNumber+""));
					if(enrichCount!=0){
						holder.tv_enrichCount.setVisibility(View.VISIBLE);
						holder.tv_enrichCount.setText(enrichCount+"");
					}else{
						holder.tv_enrichCount.setVisibility(View.GONE);
					}
							//System.out.println(position+"position");
					String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + pageNumber + ".png";

					if (new File(thumbnailImagePath).exists()) {
						Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
						holder.img_indexlistview.setImageBitmap(bitmap);
					}
					/*else{
						if (!new File(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + position + ".png").exists()){
							if (currentBook.get_bStoreID().contains("P")&&holder.img_indexlistview!=null){
								new ImageLoader(holder.img_indexlistview,position).execute();
							}
						}else{
							holder.img_indexlistview.setImageBitmap(null);
						}
					}*/
				}
			}
			return view;
		}
		 class ViewHolder {
			 ImageView img_indexlistview;
			 TextView tv_pageno,tv_enrichCount;
		}

	 }
	private class listAdapter extends BaseAdapter{

		private Context mContext;
		
		 public Integer[] mThumbIds = {
	               R.drawable.bm1,
	               R.drawable.bm2,
	               R.drawable.bm3,
	               R.drawable.bm4
	        };
		 
		public listAdapter(Context context){
			mContext = context;
		}
		//@Override
		public int getCount() {
			// TODO Auto-generated method stub
			////System.out.println("idexSize:"+bookMarkPno.size());
			return bookMarkPno.size();
		}

		//@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		//@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		//@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View vi = convertView;
			Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
			UserFunctions.changeFont(parent,font);
			if(convertView == null)
				vi = getLayoutInflater().inflate(R.layout.indexbookmark, null);
				TextView tv = (TextView)vi.findViewById(R.id.index_tv);
				String str = bookMarkPno.get(position);
				if(str.equals("Page 0"))
				{	
					tv.setText(getResources().getString(R.string.cover_page));
				}
				else if(str.equals("Page "+(pageCount-1))){
					tv.setText(getResources().getString(R.string.last_cover_page));
				}
				else{
					//tv.setText(bookMarkPno.get(position));
					String[] page=bookMarkPno.get(position).split("Page");
					tv.setText(getResources().getString(R.string.page)+""+page[1]+"");

				}

				return vi;
		}
		
	}
private class drawListAdapter extends BaseAdapter{
		
		private Context mContext;
		
		
		public drawListAdapter(Context context){
			mContext = context;
		}
		//@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return drawPnoArray.size();
		}

		//@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		//@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		//@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View vi = convertView;
			if (convertView == null)
				vi = getLayoutInflater().inflate(R.layout.indexpaint, null);
			Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
			UserFunctions.changeFont(parent,font);
			TextView tv = (TextView) vi.findViewById(R.id.index_paint);

			//String str1 = drawPno.get(position);
			//str1 = str1.substring(0,str1.lastIndexOf(".png"));
			String str1 = drawPno.get(position);                     //page4-1.png
			str1 = str1.substring(0, str1.lastIndexOf(".png"));       //page4-1
			String[] pgValue = str1.split("\\-");                       //page4,1
			if ((pgValue.length > 1)) {                                 //true

				String[] str = pgValue[0].split("Page ");
				int pNO = Integer.parseInt(str[1]) - 1;
				// tv.setText("Page "+pNO);
				// tv.setText("Page "+pNO+"-"+pgValue[1]);
				// tv.setText(pNO+"-"+pgValue[1]+" "+getResources().getString(R.string.page)+"");
				tv.setText(getResources().getString(R.string.page) + " " + pNO + "-" + pgValue[1] + "");
			} else {
				String[] str = str1.split("Page ");
				int pNO = Integer.parseInt(str[1]) - 1;
				//tv.setText("Page "+pNO);
				// tv.setText(""+pNO+" "+getResources().getString(R.string.page));
				tv.setText(getResources().getString(R.string.page) + " " + pNO + "");
			}

			return vi;
		}
		
	}
	//@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.btnBack){
			finish();
		}
	}

	//@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		if(group == segmentText){
			if(checkedId == R.id.segTblCon){
				selectedSegment = 2;
				lv.setVisibility(View.VISIBLE);
				gridview.setVisibility(View.GONE);
		       	lv.setAdapter(new indexlistadapter(this));
				enriched=true;

			}
			else if(checkedId == R.id.segBookMark){
				selectedSegment = 3;
				lv.setVisibility(View.VISIBLE);
				gridview.setVisibility(View.GONE);
		
				lv.setAdapter(new listAdapter(this));
				//mToast.setText("BookMark");
				//mToast.show();
			}
			else if(checkedId == R.id.segIndex){
				selectedSegment = 1;
				lv.setVisibility(View.GONE);
				gridview.setVisibility(View.VISIBLE);
				GridLayoutManager gridLayout=new GridLayoutManager(Index_new.this,getResources().getInteger(R.integer.grid_column),GridLayoutManager.VERTICAL,false);
				gridview.setHasFixedSize(true);
				gridview.setNestedScrollingEnabled(false);
				gridview.setLayoutManager(gridLayout);
				gridview.setItemAnimator(new DefaultItemAnimator());
				gridview.setAdapter(new RecyclerAdapter());
				gridview.addOnItemTouchListener(new RecyclerTouchListener(Index_new.this, gridview, new RecyclerTouchListener.ClickListener() {
					@Override
					public void onClick(View view, int position) {
						int pageno = position;
						//String[] pgValue = mahuMsg.split("\\|");
						Intent resuIntent = new Intent();
						resuIntent.putExtra("id", pageno+1);
						setResult(Activity.RESULT_OK, resuIntent);
						finish();
					}

					@Override
					public void onLongClick(View view, int position) {

					}
				}));
			}
			else if(checkedId == R.id.segDrawing){
				selectedSegment = 4;
				lv.setVisibility(View.VISIBLE);
				gridview.setVisibility(View.GONE);
		
				lv.setAdapter(new drawListAdapter(this));
				
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		db.unbindDrawables(findViewById(R.id.indexView));
		System.gc();
	}

	public void selectedBook() {

		if(bookName.contains("M") || bookName.contains("C")){
			enrichPageArray.clear();
			try {

				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();
				DefaultHandler handler = new DefaultHandler(){
					String currentNode = "";
					public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException
					{
						if(qName.contentEquals("Cover1"))
						{
							page=new PageDetails();
							String pageTitle = attributes.getValue("ATitle");
							String path = attributes.getValue("Path");
							page.setTitle(pageTitle);
							page.setPath(path);

							enrichPageArray.add(page);

						}
						else if(qName.contentEquals("Cover2"))
						{
							page=new PageDetails();
							String pageTitle = attributes.getValue("ATitle");
							String path = attributes.getValue("Path");
							page.setTitle(pageTitle);
							page.setPath(path);

							enrichPageArray.add(page);

						}
						else if(qName.contains("P")){
							page=new PageDetails();
							String pageTitle = attributes.getValue("ATitle");
							String path = attributes.getValue("Path");
							page.setTitle(pageTitle);
							page.setPath(path);

							enrichPageArray.add(page);
						}
						else if(qName.contains("Enr")){

						}
					}

					@Override
					public void endElement(String uri, String localName, String qName) throws SAXException {

										}
				};

				filePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookName+"Book/"+"Book.xml";
				System.out.println(filePath);
				InputStream inputStream = new FileInputStream(filePath);
				Reader reader = new InputStreamReader(inputStream, "UTF-8");

				InputSource is = new InputSource(reader);
				is.setEncoding("UTF-8");

				saxParser.parse(is, handler);

            } catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

      public void pagesWithEnrichments(){
		  int k=0;
		  boolean add=false;
              for(int j=0;j<enrichPageArray.size();j++) {
				  PageDetails currentPage = enrichPageArray.get(j);
				   String[] str = currentPage.getPath().split("\\.");
				   add=false;
				  for(int i=0;i<enrichmentTabList.size();i++) {
					  Enrichments currentEnrichment = enrichmentTabList.get(i);
					  if (str[0].equals(String.valueOf(currentEnrichment.getEnrichmentPageNo()))) {
						  add = true;
						  if(k==0){
							  finalListEnrichedArray.add(currentPage.getPath() + "|" + currentPage.getTitle()+ "|" +str[0]);
						  }
						  k++;
						  finalListEnrichedArray.add(j+"##"+currentEnrichment.getEnrichmentId()+ "|" + currentEnrichment.getEnrichmentTitle()+ "|" +str[0]);
                       }

				  }

				  if(!add){
				     finalListEnrichedArray.add(currentPage.getPath() + "|" + currentPage.getTitle()+ "|" +str[0]);
				  }
				    k=0;
			  }

		  }

		 // enrichmentTabList
		 // enrichPageArray
	private void getEnrichmentsizeforpage(){
		PageEnrichTabCount= new ArrayList<HashMap<String, String>>();
		    for(int i=0;i<currentBook.getTotalPages();i++){
				int enrichTabCount = db.getCountForEnrichmentTabList(currentBook.getBookID(), i + 1);
				//if(enrichTabCount>0){
					String pageNo=String.valueOf(i+1);
					String enrichTabs=String.valueOf(enrichTabCount);
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(pageNo,enrichTabs);
					PageEnrichTabCount.add(map);
				//}
			}
	}

	public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {


		public class MyViewHolder extends RecyclerView.ViewHolder {
			ImageView img_indexlistview;
			TextView tv_pageno,tv_enrichCount;

			public MyViewHolder(View view) {
				super(view);
				img_indexlistview = (ImageView) view.findViewById(R.id.imageView2);
				tv_pageno= (TextView) view.findViewById(R.id.tv_pageno);
				tv_enrichCount= (TextView) view.findViewById(R.id.tv_enrichCount);
			}
		}

		public RecyclerAdapter() {

		}

		@Override
		public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.thumbnailindex, parent, false);
			return new MyViewHolder(itemView);
		}

		@Override
		public void onBindViewHolder(final MyViewHolder holder, int position) {
			int pageNumber = position + 1;
			holder.tv_enrichCount.setVisibility(View.GONE);
			if (currentBook.get_bStoreID().contains("P")) {
				holder.img_indexlistview.setBackgroundColor(Color.WHITE);
			}
			if (position == 0) {
				holder.tv_pageno.setText(R.string.cover_page);
				String thumbnailImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "FreeFiles/frontThumb.png";
				//}
				if (new File(thumbnailImagePath).exists()) {
					Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
					holder.img_indexlistview.setImageBitmap(bitmap);
				}
			} else if (position == pageCount - 1) {
				String page = getResources().getString(R.string.end_page);
				holder.tv_pageno.setText(page);
				String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_end.png";
				if (!new File(thumbnailImagePath).exists()) {
					thumbnailImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "FreeFiles/backThumb.png";
				}
				if (new File(thumbnailImagePath).exists()) {
					Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
					holder.img_indexlistview.setImageBitmap(bitmap);
				}
			} else {
				//int pno = position;
				int pno = pageNumber - 1;
				//String page = getResources().getString(R.string.page);
				holder.tv_pageno.setText(pno + " ");
				HashMap<String, String> map = PageEnrichTabCount.get(pno);
				//String enrichCount=map.get(pageNumber);
				int enrichCount = Integer.parseInt(map.get(pageNumber + ""));
				if (enrichCount != 0) {
					holder.tv_enrichCount.setVisibility(View.VISIBLE);
					holder.tv_enrichCount.setText(enrichCount + "");
				} else {
					holder.tv_enrichCount.setVisibility(View.GONE);
				}
				//System.out.println(position+"position");
				String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + pageNumber + ".png";

				if (new File(thumbnailImagePath).exists()) {
					Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
					holder.img_indexlistview.setImageBitmap(bitmap);
				}
			}

		}

		@Override
		public int getItemCount() {
			return pageCount;
		}
	}

}


