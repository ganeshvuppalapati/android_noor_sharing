package com.semanoor.manahij;

import android.content.Context;

public class SharedUsers {
	
	public int sharedusersId;
	public String sharedUsersId;
	public String sharedUsersName;
	public String sharedUsersEmailId;
	public String sharedUsersBlacklist;
	public String sharedUsersType;
	private boolean sharedUsersSelected;
	public Context context;
	
	public SharedUsers(Context _context) {
		this.context = _context;
	}
	/**
	 * @return the enrichmentId
	 */
	public int getUsersId() {
		return sharedusersId;
	}
	/**
	 * @param enrichmentId the enrichmentId to set
	 */
	public void setUsersId(int sharedusersId) {
		this.sharedusersId = sharedusersId;
	}
	/**
	 * @return the enrichmentId
	 */
	public String getSharedUsersId() {
		return sharedUsersId;
	}
	/**
	 * @param enrichmentId the enrichmentId to set
	 */
	public void setSharedUsersId(String sharedUsersId) {
		this.sharedUsersId = sharedUsersId;
	}
	/**
	 * @return the enrichmentTitle
	 */
	public String getSharedUsersName() {
		return sharedUsersName;
	}
	/**
	 * @param enrichmentTitle the enrichmentTitle to set
	 */
	public void setSharedUsersName(String sharedUsersName) {
		this.sharedUsersName = sharedUsersName;
	}
	
	/**
	 * @return the enrichmentType
	 */
	public String getSharedUsersEmailId() {
		return sharedUsersEmailId;
	}
	/**
	 * @param enrichmentType the enrichmentType to set
	 */
	public void setSharedUsersEmailId(String sharedUsersEmailId) {
		this.sharedUsersEmailId = sharedUsersEmailId;
	}
	/**
	 * @return the enrichmentType
	 */
	public String getSharedUsersBlacklist() {
		return sharedUsersBlacklist;
	}
	/**
	 * @param enrichmentType the enrichmentType to set
	 */
	public void setSharedUsersBlacklist(String sharedUsersBlacklist) {
		this.sharedUsersBlacklist = sharedUsersBlacklist;
	}
	/**
	 * @return the enrichmentType
	 */
	public String getSharedUsersType() {
		return sharedUsersType;
	}
	/**
	 * @param enrichmentType the enrichmentType to set
	 */
	public void setSharedUsersType(String sharedUsersType) {
		this.sharedUsersType = sharedUsersType;
	}
	/**
	 * @return the enrichmentSelected
	 */
	public boolean isSharedUsersSelected() {
		return sharedUsersSelected;
	}
	/**
	 * @param enrichmentSelected the enrichmentSelected to set
	 */
	public void setSharedUsersSelected(boolean sharedUsersSelected) {
		this.sharedUsersSelected = sharedUsersSelected;
	}
}
