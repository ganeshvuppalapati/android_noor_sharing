package com.semanoor.manahij;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Bhavya on 10-10-2015.
 */
public class ElessonCardStackAdapter extends CardStackAdapter {
    public static boolean animation=false;
    Button btn_turn;
    static int studycard_y;
    public RelativeLayout layout;
    public BookViewReadActivity bookview;
    private Object[] noteAdapterObject;
    ArrayList<String> studyCardsPgNo = new ArrayList<String>();
    public static boolean changeanimation=false;
    public Elesson eLesson;

    public ElessonCardStackAdapter(Context mContext,Elesson elesson, Object[] noteAdapter) {
        super(mContext);

            eLesson=elesson;

        noteAdapterObject = noteAdapter;
    }

    @Override
    public View getCardView(final int position, CardModel model, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.std_card_inner, parent, false);
            assert convertView != null;
        }

        layout=(RelativeLayout)convertView.findViewById(R.id.global_container);

        String[] pageText = (String[]) eLesson.elessonNotecards[0];

        final TextView tv_selected = (TextView)convertView.findViewById(R.id.image);
        if(pageText.length>0){
            int currentPos = (pageText.length-1)-position;
            tv_selected .setText(pageText[currentPos]);
        }

        btn_turn=(Button)convertView.findViewById(R.id.btn_turn);

         btn_turn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] SText1 = (String[]) eLesson.elessonNotecards[0];
                if(SText1.length>0){
                    int currentPos = (SText1.length-1)-position;
                    tv_selected .setText(SText1[currentPos]);
                }
                if(animation==false){
                    //System.out.println(CardContainer.mTopCard.getLeft()+"left");
                    //System.out.println(CardContainer.mTopCard.getY()+"Y");
                    if(CardContainer.mTopCard!=null){
                        studycard_y= (int) CardContainer.mTopCard.getY();
                        Flip3dAnimation rotation=new Flip3dAnimation(180,90,CardContainer.mTopCard.getWidth()/2.0f,CardContainer.mTopCard.getHeight()/2.0f);
                        rotation.setDuration(150);
                        rotation.setFillAfter(true);
                        rotation.setInterpolator(new AccelerateInterpolator());
                        rotation.setAnimationListener(new DisplayNextView());
                        CardContainer.mTopCard.startAnimation(rotation);
                        //System.out.println(layout.getX());
                        //System.out.println(layout.getY());
                        animation=true;
                    }
                }else{

                    String[] SText = (String[]) eLesson.elessonNotecards[0];
                    if(SText.length>0){
                        int currentPos = (SText.length-1)-position;
                        tv_selected .setText(SText[currentPos]);
                    }

                    //System.out.println("entered");
                    if(CardContainer.mTopCard!=null){
                        Flip3dAnimation rotation=new Flip3dAnimation(180,90,CardContainer.mTopCard.getWidth()/2.0f,CardContainer.mTopCard.getHeight()/2.0f);
                        rotation.setDuration(150);
                        rotation.setFillAfter(true);
                        rotation.setInterpolator(new AccelerateInterpolator());
                        rotation.setAnimationListener(new DisplayNextView());
                        CardContainer.mTopCard.startAnimation(rotation);
                        //System.out.println(layout.getX());
                        //System.out.println(layout.getY());
                        //CardContainer.mTopCard.setY(CardContainer.mTopCard.getHeight()/2.0f+10);
                        //System.out.println(layout.getY()+"layout y");
                        animation=false;
                    }
                }
            }
        });

        return convertView;
    }

    private int Color(Integer integer) {
        // TODO Auto-generated method stub
        return 0;
    }

    public static  void changeanimation() {
        ////System.out.println("haiiii");
        //CardContainer.mTopCard.setY(CardContainer.mTopCard.getHeight()/2.0f+10);
        Flip3dAnimation rotation=new Flip3dAnimation(90,0,CardContainer.mTopCard.getWidth()/2.0f,CardContainer.mTopCard.getHeight()/2.0f);
        rotation.setDuration(500);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new DecelerateInterpolator());
        CardContainer.mTopCard.startAnimation(rotation);

    }
}