package com.semanoor.manahij;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Krishna on 08-02-2016.
 */
public class StoreList implements Serializable {
    private String nameEn;
    private String nameAr;
    private int id;
    private String description;
    private String ArDescription;
    private String thumbUrl;
    private String country;
    private ArrayList<SubStoreList> childobjlist=new ArrayList<SubStoreList>();
    private ArrayList<StoreList> subClientsList = new ArrayList<StoreList>();
    private boolean isPublic;
    private boolean isSubcription;
    private boolean isEdit;
    private boolean isShare;
    private int clientID;
    private String price;
    private int period;
    private int clientType;
    private String inAppId;


    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public ArrayList<SubStoreList> getChildobjlist() {
        return childobjlist;
    }

    public void setChildobjlist(ArrayList<SubStoreList> childobjlist) {
        this.childobjlist = childobjlist;
    }

    public ArrayList<StoreList> getSubClientsList() {
        return subClientsList;
    }

    public void setSubClientsList(ArrayList<StoreList> subClientsList) {
        this.subClientsList = subClientsList;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isSubcription() {
        return isSubcription;
    }

    public void setSubcription(boolean subcription) {
        isSubcription = subcription;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }

    public boolean isShare() {
        return isShare;
    }

    public void setShare(boolean share) {
        isShare = share;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getClientType() {
        return clientType;
    }

    public void setClientType(int clientType) {
        this.clientType = clientType;
    }

    public String getArDescription() {
        return ArDescription;
    }

    public void setArDescription(String arDescription) {
        ArDescription = arDescription;
    }

    public String getInAppId() {
        return inAppId;
    }

    public void setInAppId(String inAppId) {
        this.inAppId = inAppId;
    }
}
