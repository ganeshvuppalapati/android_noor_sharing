package com.semanoor.manahij;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.ptg.views.CircleButton;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.Note;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.Category;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.GenerateHTML;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;
import com.semanoor.source_sboookauthor.Zip;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Krishna on 28-09-2016.
 */
public class ExportEnrActivity  extends Activity implements View.OnClickListener{
    DatabaseHandler db;
    Globals global;
    private int bookViewActivityrequestCode = 1;

    public ArrayList<Enrichments> enrichmentList,  enrichmentTotalPageNoList, enrichmentSelectedList;
    public ArrayList<Enrichments> enrichmentLinksList, enrichmentLinkTotalPageNoList, enrichmentSelectedLinkList;
    public ArrayList<Note> noteList, noteTotalPageNoList, noteSelectedList;
    public ArrayList<Enrichments> mindmapList, mindmapTotalPageNoList, mindmapSelectedList;

    Book currentBook;
    GridShelf gridShelf;
    String[] categoryList ={"Enrichments","Notes","Links","Mindmap"};
    ArrayList<com.semanoor.source_sboookauthor.Category> Category;
    ExportEnrichmentDetails enrichmentDetails;
    UserContactDetails userContacts;
    public String userName;
    public String scopeId;
    public int scopeType;
    public String email;
    AdView adView;
    Groups groups;
    private WebService webService;
    ProgressDialog progresdialog;
    private InputStream chunkInputStream;
    private ByteArrayOutputStream chunkBaos;
    private byte[] chunkBytes;
    private int chunkBytesRead;
    String filepath;
    File enrichZipFilePath;
    String enrichXmlString;
    private String chunkBookFlag;
    private int chunkNoOfExecution;
    String desc;
    EditText et_resourcetitle;
    boolean isprivate;
    String mailid,enrichTitle;
    private int enrichPageNo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadLocale();


        //Remove title Bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        db = DatabaseHandler.getInstance(this);
        global = Globals.getInstance();
        groups = Groups.getInstance();

        enrichmentDetails=ExportEnrichmentDetails.getInstance();
        userContacts=UserContactDetails.getInstance(ExportEnrActivity.this,email);
        webService = new WebService(ExportEnrActivity.this, Globals.getNewCurriculumWebServiceURL());

        //webService = new WebService(MainActivity.this, Globals.getNewCurriculumWebServiceURL());

        setContentView(R.layout.export_enr_list);
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
        ViewGroup rootView_font = (ViewGroup) findViewById(android.R.id.content).getRootView();

        UserFunctions.changeFont(rootView_font,font);
        currentBook = (Book) getIntent().getSerializableExtra("Book");
        gridShelf = (GridShelf) getIntent().getSerializableExtra("Shelf");

        File file=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp");
        if (file.exists()){
            //file.delete();
            UserFunctions.deleteAllFilesFromDirectory(file);
        }
        enrichmentList = db.getAllEnrichmentTabListForExport(currentBook.getBookID(), ExportEnrActivity.this);
        enrichmentLinksList = db.getAllEnrichmentLinksForExport(currentBook.getBookID(), ExportEnrActivity.this);
        noteList = db.getAllNotesForExport(currentBook.get_bStoreID(), ExportEnrActivity.this);
        mindmapList =db.getAllEnrichmentMindmapForExport(currentBook.getBookID(), ExportEnrActivity.this);

        enrichmentDetails.setEnrichmentSelectedList(new ArrayList<Enrichments>());
        enrichmentDetails.setEnrichmentSelectedLinkList(new ArrayList<Enrichments>());
        enrichmentDetails.setNoteSelectedList(new ArrayList<Note>());
        enrichmentDetails.setMindmapSelectedList(new ArrayList<Enrichments>());

        Category=new ArrayList<com.semanoor.source_sboookauthor.Category>();
        for(int i=0;i<categoryList.length;i++) {
            Category category = new Category();
            category.setCategoryId(i);
            category.setCategoryName(categoryList[i]);
            if(i==0){
                category.setCategoryName(ExportEnrActivity.this.getResources().getString(R.string.enrichments));
            }else if(i==1){
                category.setCategoryName(ExportEnrActivity.this.getResources().getString(R.string.notes));
            }else if(i==2){
                category.setCategoryName(ExportEnrActivity.this.getResources().getString(R.string.links));
            }else if(i==3){
                category.setCategoryName(ExportEnrActivity.this.getResources().getString(R.string.mindmap));
            }
            category.setNewCatID(i);
            category.setHidden(false);
            Category.add(category);
        }

        enrichmentTotalPageNoList = getTotalNumberOfEnrichedPageList();
        enrichmentLinkTotalPageNoList = getTotalNumberOfEnrichedLinkPageList();
        noteTotalPageNoList = getTotalNumberOfNotePageList();
        mindmapTotalPageNoList = getTotalNumberOfMindmapPageList();
       // mindmapSelectedList = new ArrayList<Enrichments>();
        RelativeLayout rl= (RelativeLayout) findViewById(R.id.shelfRootViewLayout);
        //rl.setBackgroundColor(Color.parseColor("#DEE1E3"));
        rl.setBackgroundResource(R.drawable.bg_store);
        if (Globals.isLimitedVersion()) {
            initializeAds();
        }
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        Button btn_next=(Button)findViewById(R.id.btn_next);
        Button btn_share=(Button)findViewById(R.id.btn_share);
        btn_share.setVisibility(View.GONE); //visible
        btn_share.setText(R.string.online);
        btn_share.setOnClickListener(ExportEnrActivity.this);
        CircleButton btn_back=(CircleButton)findViewById(R.id.btn_back);

        btn_back.setOnClickListener(ExportEnrActivity.this);
        btn_next.setOnClickListener(ExportEnrActivity.this);
        EnrAdapterClass enr=new EnrAdapterClass(ExportEnrActivity.this);

        // mAdapter = new ExportEnrlistAdapter(ExportEnrActivity.this,Category);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
       // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.setAdapter(mAdapter);
        recyclerView.setAdapter(enr.new ExportEnrAdapter(ExportEnrActivity.this,Category));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                //Movie movie = movieList.get(position);
                //Toast.makeText(getApplicationContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next: {
                boolean Exist=false;
                if(enrichmentDetails.getEnrichmentSelectedList().size()>0){
                    Exist=true;
                }else  if (enrichmentDetails.getMindmapSelectedList().size() > 0) {
                    Exist=true;
                }else if(enrichmentDetails.getNoteSelectedList().size()>0){
                    Exist=true;
                }else if(enrichmentDetails.getEnrichmentSelectedLinkList().size()>0){
                    Exist=true;
                }else if(enrichmentDetails.getNoteSelectedList().size()>0){
                    Exist=true;
                }
                if(Exist) {
                    Intent intent = new Intent(ExportEnrActivity.this, ExportEnrGroupActivity.class);
                    intent.putExtra("Book", currentBook);
                    intent.putExtra("Shelf", gridShelf);
                    startActivity(intent);
                }else {
                    UserFunctions.DisplayAlertDialog(ExportEnrActivity.this, R.string.select_enrichment_to_export, R.string.share);
                    //  UserFunctions.alert(ExportEnrActivity.this.getResources().getString(R.string.select_enrichment_to_export), ExportEnrActivity.this);
                }

                break;

            }

            case R.id.btn_back: {
                onBackPressed();
                break;
            }
            case R.id.btn_share: {
                if(true){
                    for (Enrichments enrichments : enrichmentDetails.getEnrichmentSelectedList()){
                      //  if (enrichments.getEnrichmentType().equals(Globals.onlineEnrichmentsType)){
                            enrichPageNo = enrichments.getEnrichmentPageNo();
                            enrichTitle = enrichments.getEnrichmentTitle();
                            showEmailDialog();
                        break;
                    //    }
                    }
                }else{
                    UserFunctions.DisplayAlertDialog(ExportEnrActivity.this, R.string.select_online_enr, R.string.share);
                }
                break;
            }
            default:
                break;
        }
    }

    private boolean isOnlineEnrSelected(){
        if(enrichmentDetails.getEnrichmentSelectedList().size()>0){
            for (Enrichments enrichments : enrichmentDetails.getEnrichmentSelectedList()){
                if (enrichments.getEnrichmentType().equals(Globals.onlineEnrichmentsType)){
                    return true;
                }else{
                    return false;
                }
            }
        }
        return false;
    }

    private void loadLocale() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String language = prefs.getString(Globals.languagePrefsKey, "en");
        scopeId = prefs.getString(Globals.sUserIdKey, "");
        userName = prefs.getString(Globals.sUserNameKey, "");
        scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
        email = prefs.getString(Globals.sUserEmailIdKey, "");
       // email="krishna@semasoft.co.in";
        //changeLang(language);
    }
    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data){
//        if (mHelper != null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
//            super.onActivityResult(requestCode, resultCode, data);
//        }

        }

    private void initializeAds(){
        SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
        boolean adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);
        final InterstitialAd interstitial;
        if (!adsPurchased) {
            //interstitial = new InterstitialAd(ExportEnrActivity.this);
           // interstitial.setAdUnitId(Globals.INTERSTITIAL_AD_UNIT_ID);
            adView = (AdView) findViewById(R.id.adView);
            adView.bringToFront();
            AdRequest adRequest = new AdRequest.Builder()
                    //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    //.addTestDevice("505F22CBB28695386696655EED14B98B")
                    .build();
            adView.loadAd(adRequest);
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (!groups.getBannerAdsVisible()) {
                        adView.setVisibility(View.VISIBLE);
                    }
                   // interstitial.show();
                }
            });
//            if (interstitial.isLoaded()) {
//                interstitial.show();
//            }
//            interstitial.loadAd(adRequest);
        }
    }
    /**
     * Get All the total number of Enriched page list
     * @return
     */
    private ArrayList<Enrichments> getTotalNumberOfEnrichedPageList(){
        ArrayList<Enrichments> enrichPageNoList = new ArrayList<Enrichments>();
        for (Enrichments enrichment : enrichmentList) {
            boolean isEnrichPageNoExist = false;
            for (Enrichments filteredEnrichments : enrichPageNoList) {
                if (filteredEnrichments.getEnrichmentPageNo() == enrichment.getEnrichmentPageNo()) {
                    isEnrichPageNoExist = true;
                    break;
                } else {
                    isEnrichPageNoExist = false;
                }
            }
            if (!isEnrichPageNoExist && enrichment.getEnrichmentExportValue()==0) {
                enrichPageNoList.add(enrichment);
            }
        }
        return enrichPageNoList;
    }
    /**
     * Get All the total number of Mindmap page list
     * @return
     */
    private ArrayList<Enrichments> getTotalNumberOfMindmapPageList(){
        ArrayList<Enrichments> mindmapPageNoList = new ArrayList<Enrichments>();
        for (Enrichments enrichment : mindmapList) {
            boolean isEnrichPageNoExist = false;
            for (Enrichments filteredEnrichments : mindmapPageNoList) {
                if (filteredEnrichments.getEnrichmentPageNo() == enrichment.getEnrichmentPageNo()) {
                    isEnrichPageNoExist = true;
                    break;
                } else {
                    isEnrichPageNoExist = false;
                }
            }
            if (!isEnrichPageNoExist && enrichment.getEnrichmentExportValue()==0) {
                mindmapPageNoList.add(enrichment);
            }
        }
        return mindmapPageNoList;
    }

    /**
     * Get All the total number of Enriched Link page list
     * @return
     */
    private ArrayList<Enrichments> getTotalNumberOfEnrichedLinkPageList(){
        ArrayList<Enrichments> enrichPageNoList = new ArrayList<Enrichments>();
        for (Enrichments enrichment : enrichmentLinksList) {
            boolean isEnrichPageNoExist = false;
            for (Enrichments filteredEnrichments : enrichPageNoList) {
                if (filteredEnrichments.getEnrichmentPageNo() == enrichment.getEnrichmentPageNo()) {
                    isEnrichPageNoExist = true;
                    break;
                } else {
                    isEnrichPageNoExist = false;
                }
            }
            if (!isEnrichPageNoExist && enrichment.getEnrichmentExportValue()==0) {
                enrichPageNoList.add(enrichment);
            }
        }
        return enrichPageNoList;
    }

    /**
     * Get All the total number of Note page list
     * @return
     */
    private ArrayList<Note> getTotalNumberOfNotePageList(){
        ArrayList<Note> notePageNoList = new ArrayList<Note>();
        for (Note note : noteList) {
            boolean isNotePageNoExist = false;
            for (Note filteredNote : notePageNoList) {
                if (filteredNote.getPageNo() == note.getPageNo()) {
                    isNotePageNoExist = true;
                    break;
                } else {
                    isNotePageNoExist = false;
                }
            }
            if (!isNotePageNoExist && note.getExportedId()==0) {
                notePageNoList.add(note);
            }
        }
        return notePageNoList;
    }





    /**
     * Get the total number of enriched page no by passing the page number
     *
     * @param pageNo
     * @return
     */
    public ArrayList<Enrichments> getTotalNoOfEnrichedPages(int pageNo) {
        ArrayList<Enrichments> enrichedPageList = new ArrayList<Enrichments>();
        for (Enrichments enrichments : enrichmentList) {
            if (pageNo == enrichments.getEnrichmentPageNo()) {
                enrichedPageList.add(enrichments);
            }
        }
        return enrichedPageList;
    }

    /**
     * Get the total number of mindmap page no by passing the page number
     *
     * @param pageNo
     * @return
     */
    public ArrayList<Enrichments> getTotalNoOfMindmapPages(int pageNo) {
        ArrayList<Enrichments> mindmapPageList = new ArrayList<Enrichments>();
        for (Enrichments enrichments : mindmapList) {
            if (pageNo == enrichments.getEnrichmentPageNo()) {
                mindmapPageList.add(enrichments);
            }
        }
        return mindmapPageList;
    }

    /**
     * Get the total number of  link page no by passing the page number
     *
     * @param pageNo
     * @return
     */
    public ArrayList<Enrichments> getTotalNoOfEnrichedLinkPages(int pageNo) {
        ArrayList<Enrichments> enrichedLinkPageList = new ArrayList<Enrichments>();
        for (Enrichments enrichments :enrichmentLinksList) {
            if (pageNo == enrichments.getEnrichmentPageNo()) {
                enrichedLinkPageList.add(enrichments);
            }
        }
        return enrichedLinkPageList;
    }

    /**
     * Get the total number of  note page no by passing the page number
     *
     * @param pageNo
     * @return
     */
    public ArrayList<Note> getTotalNoOfNotePages(int pageNo) {
        ArrayList<Note> notePageList = new ArrayList<Note>();
        for (Note note :noteList) {
            if (pageNo == note.getPageNo()) {
                notePageList.add(note);
            }
        }
        return notePageList;
    }

    public class publishEnrichmentsAsChunks extends AsyncTask<Void, Void, Void> {

        String bookZipBase64Chunks = null;
        byte[] byteArray = null;
        String base64Binary;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            convertFileToBase64BinaryChunks();
        }


        @Override
        protected Void doInBackground(Void... params) {
            if (chunkBytesRead > 0) {
                webService.newUploadBookAsChunks(scopeId, enrichZipFilePath.getName(), bookZipBase64Chunks, chunkBookFlag);
            } else {
                int BookId = Integer.parseInt(currentBook.get_bStoreID().replace("M", ""));
                webService.ExportOnlineEnrichment(scopeId, enrichZipFilePath.getName(), enrichXmlString, false, BookId,enrichPageNo,enrichTitle);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (chunkBytesRead > 0) {
                if (webService.uploadJunkFileIpadResult != "") {
                    if (webService.uploadJunkFileIpadResult.equals("true")) {
                        chunkNoOfExecution++;
                        if (enrichZipFilePath.exists()) {
                            new publishEnrichmentsAsChunks().execute();
                        }
                    } else {
                        progresdialog.dismiss();
                        String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp";
                        File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                        UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                        UserFunctions.DisplayAlertDialog(ExportEnrActivity.this, R.string.went_wrong, R.string.oops);
                    }
                }else{
                    progresdialog.dismiss();
                    String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp";
                    File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                    UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                    UserFunctions.DisplayAlertDialog(ExportEnrActivity.this, R.string.went_wrong, R.string.oops);
                }
            } else {
                if (webService.enrichedPageResponseServerIdList.size() > 0) {
                    for (int i = 0; i < webService.enrichedPageResponseServerIdList.size(); i++) {
                        Enrichments enrichments = enrichmentDetails.enrichmentSelectedList.get(i);
                        int enrichPageServerId = webService.enrichedPageResponseServerIdList.get(i);
                        enrichments.setEnrichmentExportValue(enrichPageServerId);
                        enrichments.setEnrichmentSelected(false);
                        db.executeQuery("update enrichments set Exported='" + enrichPageServerId + "' where enrichmentID='" + enrichments.getEnrichmentId() + "'");
                    }
                    //enrichmentSelectedList.clear();
                    progresdialog.dismiss();
                    File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                    UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                    UserFunctions.DisplayAlertDialog(ExportEnrActivity.this, R.string.publish_completed_successfully, R.string.published);

                } else {
                    progresdialog.dismiss();
                    File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                    UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                    UserFunctions.DisplayAlertDialog(ExportEnrActivity.this, R.string.went_wrong, R.string.oops);

                }
            }
        }
        private void convertFileToBase64BinaryChunks() {
            try {
                if ((chunkBytesRead = chunkInputStream.read(chunkBytes)) != -1) {
                    ////System.out.println("bytesRead: "+chunkBytesRead+"i: "+i);
                    chunkBaos.write(chunkBytes, 0, chunkBytesRead);
                    byteArray = chunkBaos.toByteArray();
                    bookZipBase64Chunks = Base64.encodeToString(byteArray, Base64.DEFAULT).trim();
                    if (chunkNoOfExecution == 1) {
                        chunkBookFlag = "Start";
                    } else if (chunkBytesRead < 1024 * 1024) {
                        chunkBookFlag = "End";
                    } else {
                        chunkBookFlag = "Middle";
                    }
                    chunkBaos.reset();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public class validateLoginAndPublishEnrichments extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            progresdialog.setMessage(ExportEnrActivity.this.getResources().getString(R.string.upload_please_wait));
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
            String formattedDate = df.format(c.getTime());
            filepath = Globals.TARGET_BASE_BOOKS_DIR_PATH +currentBook.getBookID() + "/temp/" + formattedDate + ".zip";
            initUpload(filepath);
            enrichZipFilePath = new File(filepath);
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (enrichZipFilePath.exists()) {
                PublishEnrichmentsAsChunks();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

        /**
         * Generate all the selected enrichment html pages and and make it as zip and create an xml to prepare for publish or update.
         */
        private void initUpload(String destPath) {

            //String storBookId = bookView.currentBook.get_bStoreID().replace("M", "");
            int BookId = Integer.parseInt(currentBook.get_bStoreID().replace("M", ""));
            enrichXmlString = "<Enrichments>";
            int i = 0;
            for (Enrichments enrichments : enrichmentDetails.enrichmentSelectedList) {
                if (enrichments.isEnrichmentSelected()) {
                    new GenerateHTML(ExportEnrActivity.this, db, currentBook, String.valueOf(enrichments.getEnrichmentPageNo()), null, false).generateEnrichedPageForExportAndZipIt(enrichments);
                    String fileName = currentBook.getBookID() + "-" + enrichments.getEnrichmentPageNo() + "-" + enrichments.getEnrichmentId() + ".zip";
                    //String fileName = enrichments.getEnrichmentPageNo()+"-"+enrichments.getEnrichmentId()+".zip";

                    int idPage = enrichments.getEnrichmentPageNo();
                    String bookname = currentBook.get_bStoreID().replace("M", "");
                    //bookname=bookname.replace("M", "");

                    enrichXmlString = enrichXmlString.concat("<Enriched IDUser='"+scopeId+"' IDBook='"+BookId+"' IDPage='"+idPage+"' Title='"+enrichments.getEnrichmentTitle()+"' Password='' isPublished='3' PublishToEnrichementsSite='true' IsEditable='0' file='"+fileName+"' />");
                    i++;
                }
            }
            enrichXmlString = enrichXmlString.concat("</Enrichments>");

            Zip zip = new Zip();
            String sourcePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp/";
            //String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/zipFile222.zip";
            zip.zipFilesForEnrichments(new File(sourcePath), destPath);

        }

        private void PublishEnrichmentsAsChunks() {
            try {
                chunkInputStream = new FileInputStream(filepath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            chunkBaos = new ByteArrayOutputStream();
            chunkBytes = new byte[1024 * 1024];
            chunkBytesRead = 0;
            chunkNoOfExecution = 1;
            new publishEnrichmentsAsChunks().execute();
        }
    }
    private void showEmailDialog(){
        Dialog exportEnrPopover = new Dialog(ExportEnrActivity.this);
        exportEnrPopover.getWindow().setBackgroundDrawable(new ColorDrawable(ExportEnrActivity.this.getResources().getColor(R.color.semi_transparent)));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        exportEnrPopover.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exportEnrPopover.setContentView(ExportEnrActivity.this.getLayoutInflater().inflate(R.layout.enr_export_dialog, null));
        exportEnrPopover.getWindow().setLayout((int) getResources().getDimension(R.dimen.export_newbook_width), (int) getResources().getDimension(R.dimen.export_newbook_height));
        Button btn_export = (Button) exportEnrPopover.findViewById(R.id.btn_export);
        final EditText et_title = (EditText) exportEnrPopover.findViewById(R.id.et_name);
        final EditText et_email = (EditText) exportEnrPopover.findViewById(R.id.et_description);

        btn_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toEmailId = et_email.getText().toString();
                String title = et_title.getText().toString();
                if (!toEmailId.equals("") && !title.equals("")) {
                    String str = ExportEnrActivity.this.getResources().getString(R.string.please_wait);
                    progresdialog = ProgressDialog.show(ExportEnrActivity.this, "", str, true);
                    new validateLoginAndPublishEnrichments().execute();
                }

            }
        });
        exportEnrPopover.show();
    }
}
