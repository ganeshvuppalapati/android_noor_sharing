package com.semanoor.manahij;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class Flip3dAnimation extends Animation {
	private final float mFromDegrees;
    private final float mToDegrees;	
    private float centerx;
    private float centery;
    private Camera mcamera;
	
	public Flip3dAnimation(float fromDegrees, float toDegrees, float centerX, float centerY) {
		// TODO Auto-generated constructor stub
		mFromDegrees= fromDegrees;
		mToDegrees=toDegrees;
		centerx=centerX;
		centery=centerY;
		
	}
	public void initialize(int width,int height,int parentWidth,int parentHeight){
	super.initialize(width,height,parentWidth,parentHeight);
	mcamera=new Camera();
	}
	protected void applyTransformation(float interpolatedTime,Transformation t){
		final float fromDegrees=mFromDegrees;
		float degrees=fromDegrees+((mToDegrees-fromDegrees)*interpolatedTime);
		
		final float centerX=centerx;
		final float centerY=centery;
		final Camera camera=mcamera;
		
		final Matrix matrix=t.getMatrix();
		camera.save();
		camera.rotateY(degrees);
		camera.getMatrix(matrix);
		camera.restore();
		matrix.preTranslate(-centerX, -centerY);
		matrix.postTranslate(centerX, centerY);
	    //System.out.println(centerY+"centerY");
	/*if(SimpleCardStackAdapter.changeanimation==true){
		SimpleCardStackAdapter.changeanimation=false;
		SimpleCardStackAdapter.changeanimation();
		}*/
	}
	

}
