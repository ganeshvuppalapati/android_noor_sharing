package com.semanoor.manahij;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.semanoor.source_sboookauthor.Globals;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by Krishna on 12-10-2015.
 */
public class TwitterDialog {
   Context bookViewActivity;


    /* Shared preference keys */
    private static final String PREF_NAME = "sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String PREF_USER_NAME = "twitter_user_name";

    /* Any number for uniquely distinguish your request */
    public static final int WEBVIEW_REQUEST_CODE = 100;

    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;

    String selectedText;
    EditText mShareEditText;

    public TwitterDialog(Context context) {
         bookViewActivity=context;

    }

    public void loginToTwitter(String text) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        selectedText=text;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(bookViewActivity);
        boolean isLoggedIn = prefs.getBoolean(PREF_KEY_TWITTER_LOGIN, false);

        if (!isLoggedIn) {
            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(Globals.CONSUMER_KEY);
            builder.setOAuthConsumerSecret(Globals.CONSUMER_SECRET);

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            if(bookViewActivity instanceof BookViewReadActivity) {
                ((BookViewReadActivity) bookViewActivity).mTwitter = factory.getInstance();
            }else{
                ((Elesson) bookViewActivity).mTwitter = factory.getInstance();
            }

            try {
                if(bookViewActivity instanceof BookViewReadActivity) {
                    ((BookViewReadActivity) bookViewActivity).mRequestToken = ((BookViewReadActivity) bookViewActivity).mTwitter.getOAuthRequestToken(Globals.CALLBACK_URL);
                }else{
                    ((Elesson) bookViewActivity).mRequestToken = ((Elesson) bookViewActivity).mTwitter.getOAuthRequestToken(Globals.CALLBACK_URL);

                }
                /**
                 *  Loading twitter login page on webview for authorization
                 *  Once authorized, results are received at onActivityResult
                 *  */

                if(bookViewActivity instanceof BookViewReadActivity) {
                    final Intent intent = new Intent(bookViewActivity, TwitterLogin.class);
                    intent.putExtra("extra_url", ((BookViewReadActivity)bookViewActivity).mRequestToken.getAuthenticationURL());
                    intent.putExtra("text",selectedText);
                    ((BookViewReadActivity) bookViewActivity).startActivityForResult(intent, WEBVIEW_REQUEST_CODE);
                }else{
                    final Intent intent = new Intent(bookViewActivity, TwitterLogin.class);
                    intent.putExtra("extra_url", ((Elesson)bookViewActivity).mRequestToken.getAuthenticationURL());
                    intent.putExtra("text",selectedText);
                    ((Elesson) bookViewActivity).startActivityForResult(intent, WEBVIEW_REQUEST_CODE);

                }
            } catch (TwitterException e) {
                e.printStackTrace();
            }
        } else {
            if(bookViewActivity instanceof BookViewReadActivity) {
                  twitterReadActivityAlertDialog(selectedText);
               // ((BookViewReadActivity) bookViewActivity).sharingText(selectedText);
            }else {
                 twitterShareAlertDialog(selectedText);
            }

        }
    }

    public void twitterReadActivityAlertDialog(final String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder((BookViewReadActivity)bookViewActivity);
        builder.setTitle("Twitter");
        builder.setMessage(text);
        builder.setCancelable(false);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(R.string.tweet, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                // TwitterDialog twitterDialog = new TwitterDialog((BookViewReadActivity)bookViewActivity);
                //twitterDialog.new updateTwitterStatus().execute(selectedtext);
                new updateTwitterStatus().execute(text);
                //android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public class updateTwitterStatus extends AsyncTask<String, String, Void> {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(bookViewActivity instanceof  Elesson) {
                pDialog = new ProgressDialog((Elesson)bookViewActivity);
            }else{
                pDialog = new ProgressDialog(((BookViewReadActivity)bookViewActivity));
            }
            pDialog.setMessage("Posting to twitter...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected Void doInBackground(String... args) {

            String status = args[0];
            try {

                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(Globals.CONSUMER_KEY);
                builder.setOAuthConsumerSecret(Globals.CONSUMER_SECRET);
                SharedPreferences prefs;
                if(bookViewActivity instanceof  Elesson) {
                   prefs = PreferenceManager.getDefaultSharedPreferences((Elesson)bookViewActivity);
                }else{
                    prefs = PreferenceManager.getDefaultSharedPreferences(((BookViewReadActivity)bookViewActivity));
                }
                // Access Token
                String access_token = prefs.getString("oauth_token", "");
                // Access Token Secret
                String access_token_secret = prefs.getString("oauth_token_secret", "");

                AccessToken accessToken = new AccessToken(access_token, access_token_secret);
                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

                // Update status
                StatusUpdate statusUpdate = new StatusUpdate(status);
                //InputStream is = getResources().openRawResource(R.drawable.lakeside_view);
                // statusUpdate.setMedia("test.jpg", is);

                twitter4j.Status response = twitter.updateStatus(statusUpdate);

                Log.d("Status", response.getText());

            } catch (TwitterException e) {
                Log.d("Failed to post!", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

			/* Dismiss the progress dialog after sharing */
            pDialog.dismiss();
            if(bookViewActivity instanceof  Elesson) {
                Toast.makeText((Elesson) bookViewActivity, "Posted to Twitter!", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText((BookViewReadActivity) bookViewActivity, "Posted to Twitter!", Toast.LENGTH_SHORT).show();
            }

            // Clearing EditText field
           // mShareEditText.setText("");

        }
    }

    /**
     * Saving user information, after user is authenticated for the first time.
     * You don't need to show user to login, until user has a valid access toen
     */
    public void saveTwitterInfo(AccessToken accessToken) {

        long userID = accessToken.getUserId();

        User user;
        try {
            if(bookViewActivity instanceof BookViewReadActivity) {
                user = ((BookViewReadActivity) bookViewActivity).mTwitter.showUser(userID);
            }else{
                user = ((Elesson) bookViewActivity).mTwitter.showUser(userID);
            }

            String username = user.getName();

			/* Storing oAuth tokens to shared preferences */
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(bookViewActivity);
            SharedPreferences.Editor e = prefs.edit();
            e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
            e.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
            e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
            e.putString(PREF_USER_NAME, username);
            e.commit();


        } catch (TwitterException e1) {
            e1.printStackTrace();
        }
    }


  /*  public void sharingText(String text) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(bookViewActivity);
        boolean isLoggedIn = prefs.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
        final Intent intent = new Intent(bookViewActivity, TwitterLogin.class);
        intent.putExtra("text", text);
        if(bookViewActivity instanceof BookViewReadActivity) {
             intent.putExtra("extra_url",((BookViewReadActivity)bookViewActivity). mRequestToken.getAuthenticationURL());
            ((BookViewReadActivity) bookViewActivity).startActivity(intent);
        }else{
            intent.putExtra("extra_url",((Elesson)bookViewActivity). mRequestToken.getAuthenticationURL());
            ((Elesson) bookViewActivity).startActivity(intent);
        }
    }*/

    public void twitterShareAlertDialog(String text){
        AlertDialog.Builder builder = new AlertDialog.Builder((Elesson)bookViewActivity);
        builder.setTitle("Twitter");
        builder.setMessage(text);
        builder.setCancelable(false);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        builder.setPositiveButton(R.string.tweet, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
               new updateTwitterStatus().execute(selectedText);

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }



    }
