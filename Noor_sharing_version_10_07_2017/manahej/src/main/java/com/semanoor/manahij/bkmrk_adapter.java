package com.semanoor.manahij;

import java.io.File;
import java.util.ArrayList;

import com.semanoor.sboookauthor_store.BookMarkEnrichments;
import com.semanoor.source_sboookauthor.Globals;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class bkmrk_adapter extends BaseAdapter {
	
	private BookViewActivity context;
	ArrayList<BookMarkEnrichments> bookMarkTitles;
	private boolean edit_done;
	public bkmrk_adapter(BookViewActivity context,ArrayList<BookMarkEnrichments> enrichmentbkmrkList) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.bookMarkTitles=enrichmentbkmrkList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return bookMarkTitles.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	public static class ViewHolder{
		public ImageView imgView;
		public EditText etTxtView;
		public TextView textview;
		public Button  btn_del;
	}


	@Override
	public View getView(final int position, View convertView, ViewGroup parent){
		View view = convertView;
		final ViewHolder holder;
		if (convertView == null) {
			view = context.getLayoutInflater().inflate(R.layout.advbkmarktitles, null);
			holder=new ViewHolder();
			holder.imgView=(ImageView)view.findViewById(R.id.iv_advsearch);
			holder.etTxtView=(EditText)view.findViewById(R.id.et_bkmarktitle);
			holder.btn_del=(Button)view.findViewById(R.id.btn_del);
			holder.textview=(TextView)view.findViewById(R.id.tv_title);
			view.setTag(holder);
		} else {
			holder=(ViewHolder)view.getTag();
		}   
		 
		  
		  if(BookViewActivity.deletemode){
		    	 holder.btn_del.setVisibility(View.VISIBLE);
		   }else{
		         holder.btn_del.setVisibility(View.INVISIBLE);
		   }
		  holder.btn_del.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//bookMarkTitles.get(position).getEnrichmentTitle()
				int id=bookMarkTitles.get(position).getEnrichmentId();
				int pageno=bookMarkTitles.get(position).getEnrichmentPageNo();
				      
				String Path = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/"+"bkmarkimages/";
				String correct= Path+(id)+".png";
				File file=new File(correct);
				if (file.exists()) {
					file.delete();
					
				}
				bookMarkTitles.remove(position);
			    context.removingBkMrkButton(position,pageno,id);
			}
		});
		 
		 		
		  //String type=adv_bkmrktype.split(".");
		if(BookViewActivity.ineditmode){
			holder.textview.setVisibility(View.INVISIBLE);
			holder.etTxtView.setVisibility(View.VISIBLE);
			holder.etTxtView.setText(bookMarkTitles.get(position).getEnrichmentTitle());
			holder.etTxtView.setBackgroundResource(R.drawable.border);
		    holder.etTxtView.setFocusableInTouchMode(true);
		    //edit_done=true;
			
		}else{
			holder.etTxtView.setBackgroundResource(R.drawable.edit_text_without_border);
		        //System.out.println(holder.etTxtView.getText().toString()+"text");
				holder.etTxtView.setFocusableInTouchMode(false);
			    holder.textview.setVisibility(View.VISIBLE);
				holder.etTxtView.setVisibility(View.INVISIBLE);
                holder.textview.setText(bookMarkTitles.get(position).getEnrichmentTitle());
				holder.etTxtView.setText(bookMarkTitles.get(position).getEnrichmentTitle());
				holder.imgView.setVisibility(View.INVISIBLE);
						
				holder.imgView.setVisibility(View.INVISIBLE);
				String enrichPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/"+"bkmarkimages/";
				String objContent= enrichPath+(bookMarkTitles.get(position).getEnrichmentId())+".png";
				
				File file1=new File(objContent);
				if (file1.exists()) {
					Bitmap bitmap = BitmapFactory.decodeFile(objContent);
					holder.imgView.setImageBitmap(bitmap);
					holder.imgView.setVisibility(View.VISIBLE);
					
				}
			//}
               
		}  
		 holder.etTxtView.setOnFocusChangeListener(new OnFocusChangeListener() {
				
				@Override
				public void onFocusChange(View arg0, boolean arg1) {
					// TODO Auto-generated method stub
					//System.out.println(position+"position");
					//System.out.println((holder.etTxtView).getText().toString()+"text");
					String newtext=holder.etTxtView.getText().toString();
				    bookMarkTitles.get(position).setEnrichmentTitle(newtext);
				    String title = bookMarkTitles.get(position).getEnrichmentTitle();
				    title=title.replace("'", "''");
				    context.db.executeQuery("update  BookmarkedSearchTabs set Title='"+title+"'where Id='"+bookMarkTitles.get(position).getEnrichmentId()+"'and PageNo= '"+bookMarkTitles.get(position).getEnrichmentPageNo()+"' ");
				    context.updateNameToButton(position,newtext);

							
				}
			});
		return view;
	}
	
	
}
