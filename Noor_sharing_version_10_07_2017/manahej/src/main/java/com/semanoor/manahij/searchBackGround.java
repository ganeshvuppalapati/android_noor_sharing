package com.semanoor.manahij;

import com.artifex.mupdfdemo.SearchResult;

import java.util.ArrayList;

/**
 * Created by semanoor on 4/3/2017.
 */

public interface searchBackGround {
    void onSearchCompleted(ArrayList<SearchResult> searchResult);
}
