package com.semanoor.manahij;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by karthik on 11-01-2017.
 */

public class ApiCallBackTask extends AsyncTask<Void,Void,Object> {
    private Context context;
    private ApiCallBackListener<Object> callBackListener;
    public Exception mException;
    private String url;
    private String urlParameter;

    public ApiCallBackTask(Context ctx,String URL,String URLParameter,ApiCallBackListener listener){
        callBackListener = listener;
        context = ctx;
        url = URL;
        urlParameter = URLParameter;
    }

    @Override
    protected String doInBackground(Void... params) {
        String returnResponse = newWebService(url);
        returnResponse = returnResponse.replaceAll("\\\\r\\\\n", "");
        returnResponse = returnResponse.replace("\"{", "{");
        returnResponse = returnResponse.replace("}\",", "},");
        returnResponse = returnResponse.replace("}\"", "}");
        return returnResponse;
    }

    @Override
    protected void onPostExecute(Object obj) {
        HashMap<String,Object> jsonList = null;
        try {
            JSONObject array = new JSONObject((String)obj);
            jsonList = (HashMap<String,Object>) toMap(array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (callBackListener != null) {
            if (mException == null) {
                callBackListener.onSuccess(jsonList);
            } else {
                callBackListener.onFailure(mException);
            }
        }
    }

    public String makePostRequest(String methodURL, String urlParameters) {
        try {

            URL url = new URL(methodURL);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
            connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
            dStream.writeBytes(urlParameters);
            dStream.flush();
            dStream.close();
            int responseCode = connection.getResponseCode();

            final StringBuilder output = new StringBuilder("Request URL " + url);
            output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters);
            output.append(System.getProperty("line.separator")  + "Response Code " + responseCode);
            output.append(System.getProperty("line.separator")  + "Type " + "POST");
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = "";
            StringBuilder responseOutput = new StringBuilder();
            System.out.println("output===============" + br);
            while((line = br.readLine()) != null ) {
                responseOutput.append(line);
            }
            br.close();

            output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());

            return responseOutput.toString();

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            mException = e;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            mException = e;
        }
        return "";
    }


    private String newWebService(String url) {
        HttpURLConnection urlConnection = null;
        StringBuilder sb = new StringBuilder();
        String returnResponse = "";
        try {
            URL urlToRequest = new URL(url);

            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(25000);
            urlConnection.setRequestProperty("accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();
                System.out.println("" + sb.toString());
            } else {
                System.out.println(urlConnection.getResponseMessage());
            }

        } catch (MalformedURLException e) {

        } catch (SocketTimeoutException e) {

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return sb.toString();
    }
    private Map<String,Object> toMap(JSONObject object) throws JSONException {
        Map<String,Object> map = new HashMap<String,Object>();
        Iterator<String> keysIter = object.keys();
        while (keysIter.hasNext()){
            String key = keysIter.next();
            Object value = object.get(key);
            if (value instanceof JSONArray){
                value = toList((JSONArray) value);
            }else if (value instanceof JSONObject){
                value = toMap((JSONObject)value);
            }
            map.put(key,value);
        }
        return map;
    }
    private List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0;i<array.length();i++){
            Object value = array.get(i);
            if (value instanceof JSONArray){
                value = toList((JSONArray) value);
            }else if (value instanceof JSONObject){
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
}