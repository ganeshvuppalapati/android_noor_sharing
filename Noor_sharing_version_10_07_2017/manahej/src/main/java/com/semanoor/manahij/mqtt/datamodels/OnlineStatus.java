package com.semanoor.manahij.mqtt.datamodels;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.internal.ParcelableSparseArray;

/**
 * Created by Pushpa on 24/05/17.
 */

public class OnlineStatus  implements Parcelable {
    String email;

    public static final Creator<OnlineStatus> CREATOR = new Creator<OnlineStatus>() {
        @Override
        public OnlineStatus createFromParcel(Parcel in) {
            return new OnlineStatus(in);
        }

        @Override
        public OnlineStatus[] newArray(int size) {
            return new OnlineStatus[size];
        }
    };

    public String getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(String onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String onlineStatus;


    public OnlineStatus(String email, String onlineStatus) {
        this.email = email;
        this.onlineStatus = onlineStatus;
    }

    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(email);
        dest.writeString(onlineStatus);
    }


    public OnlineStatus(Parcel in){
        this.email = in.readString();
        this.onlineStatus = in.readString();

    }


}
