package com.semanoor.manahij;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.View;

/**
 * Created by Krishna on 05-01-2017.
 */

public class MyDragShadowBuilder extends View.DragShadowBuilder {

    // The drag shadow image, defined as a drawable thing
    private Drawable shadow;
    BookViewReadActivity context;
    // Defines the constructor for myDragShadowBuilder
    public MyDragShadowBuilder(View v, BookViewReadActivity cxt) {


        // Stores the View parameter passed to myDragShadowBuilder.

        super(v);
        context=cxt;
        // Creates a draggable image that will fill the Canvas provided by the system.
        // shadow = new ColorDrawable(Color.LTGRAY);
        shadow=(Drawable)context.getResources().getDrawable(R.drawable.tab_enr_dark);
//            shadow = new Drawable(Sample.this.getResources().getDrawable(R.drawable.tab_enr_dark));
    }

//        @Override
//        public void onDrawShadow(Canvas canvas) {
//            canvas.scale(1.5F, 1.5F);
//            super.onDrawShadow(canvas);
//        }

    // Defines a callback that sends the drag shadow dimensions and touch point back to the
    // system.
    @Override
    public void onProvideShadowMetrics (Point size, Point touch) {
        // Defines local variables
        int width, height;

        // Sets the width of the shadow to half the width of the original View
        width = getView().getWidth();

        // Sets the height of the shadow to half the height of the original View
        height = getView().getHeight();

        // The drag shadow is a ColorDrawable. This sets its dimensions to be the same as the
        // Canvas that the system will provide. As a result, the drag shadow will fill the
        // Canvas.
        shadow.setBounds(0, 0, width, height);

        // Sets the size parameter's width and height values. These get back to the system
        // through the size parameter.
        size.set(width, height);

//            shadowSize.set((int) (view.getWidth() * 1.5F),
//                        (int) (view.getHeight() * 1.5F));
//               shadowTouchPoint.set(shadowSize.x / 2, shadowSize.y / 2);
        // Sets the touch point's position to be in the middle of the drag shadow

        touch.set(width / 2, height / 2);
    }

    // Defines a callback that draws the drag shadow in a Canvas that the system constructs
    // from the dimensions passed in onProvideShadowMetrics().
    @Override
    public void onDrawShadow(Canvas canvas) {
        //  canvas.scale(1.5F, 1.5F);
        // Draws the ColorDrawable in the Canvas passed in from the system.
        shadow.draw(canvas);
    }
}