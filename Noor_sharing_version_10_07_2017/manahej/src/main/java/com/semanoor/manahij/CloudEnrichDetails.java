package com.semanoor.manahij;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptg.views.CircleButton;
import com.semanoor.sboookauthor_store.RenderHTMLObjects;
import com.semanoor.source_manahij.Resources;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Krishna on 22-08-2016.
 */
public class CloudEnrichDetails extends Activity implements View.OnClickListener {

    public CloudData book;
    public  ArrayList<CloudData> enrich_Resource;
    public  ArrayList<CloudData> archieve_enrich;
    ArrayList<CloudData> selectedenr_resour=new ArrayList<CloudData>();
    String KEY_CENRICHMENTS = "CENRICHMENT", KEY_CEMESSAGE = "EMESSAGE", KEY_CECONTENT = "ECONTENT", KEY_CLOUDENRSELECTED = "ESELECTED";
    public GridShelf gridShelf;
    WebService webService;
    DatabaseHandler db;
    ArrayList<HashMap<String, String>> selectedCloudEnrList;
    public RenderHTMLObjects webView;
    public ListView lv;
    String archiveData;
    Button btn_archive,btn_archivedata;
    TextView txtNooorTitle;
    boolean archieve;
    String ScopeId;
    CheckBox cb_selectall;
    String emaild;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title Bar
        archiveData= loadLocale();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.enrichmentdetails);

        webService = new WebService(CloudEnrichDetails.this, Globals.getNewCurriculumWebServiceURL());
        db = DatabaseHandler.getInstance(this);
        lv=(ListView)findViewById(R.id.lv_enrich_resource);
        webView = (RenderHTMLObjects)findViewById(R.id.webView1);
        txtNooorTitle=(TextView)findViewById(R.id.txtNooorTitle);
        CircleButton btn_back= (CircleButton) findViewById(R.id.btn_back);
        cb_selectall=(CheckBox)findViewById(R.id.cb_selectall);
        btn_archive=(Button)findViewById(R.id.btn_archive);
        btn_archivedata=(Button)findViewById(R.id.btn_archivedata);
        Button btn_del=(Button)findViewById(R.id.btn_del);
        Button btn_download_all=(Button)findViewById(R.id.btn_download_all);
        btn_del.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        btn_archive.setOnClickListener(this);
        btn_archivedata.setOnClickListener(this);
        btn_download_all.setOnClickListener(this);

        book=(CloudData) getIntent().getSerializableExtra("selectedBook");
        gridShelf= (GridShelf) getIntent().getSerializableExtra("Shelf");
        enrich_Resource=getResouceAndEnrichments();
        if(selectedenr_resour.size()>0){
            selectedenr_resour.clear();
        }
        invalidatingViews(enrich_Resource,archieve);
        cb_selectall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cb_selectall.isChecked()){
                    if(selectedCloudEnrList.size()>0) {
                        selectedCloudEnrList.clear();
                    }
                }
               if(archieve){
                      selectingEnrichmentandResources(archieve_enrich,cb_selectall);

                   }else if(!archieve){
                       selectingEnrichmentandResources(enrich_Resource,cb_selectall);
                   }


            }
        });


    }
    private String loadLocale() {
        String data;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        ScopeId=prefs.getString(Globals.sUserIdKey, "");
        data = prefs.getString(ScopeId, "");
        emaild = prefs.getString(Globals.sUserEmailIdKey, "");
        return data;
    }
    private void saveLocale(String language) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ScopeId, language);
        editor.commit();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back: {

               onBackPressed();
                break;
            }
            case R.id.btn_archive:{
                if(selectedCloudEnrList.size()>0 &&!archieve) {
                   // btn_archive.setBackgroundResource(R.drawable.shared_archive);
                    boolean exist = false;
                    int selected = 0;
                    String enrichmentId = null;
                    //enrichmentId=getSelectedData(e,true);
                    for (int i = 0; i < selectedCloudEnrList.size(); i++) {
                        HashMap<String, String> hashmap = selectedCloudEnrList.get(i);
                        String cloudEnrselected = hashmap.get(KEY_CLOUDENRSELECTED);
                        String enrichmentNo = hashmap.get(KEY_CENRICHMENTS);
                        String jsonContent = hashmap.get(KEY_CECONTENT);
                        if(enrichmentId==null){
                            enrichmentId=enrichmentNo;
                        }else {
                            enrichmentId = enrichmentId+"#"+ enrichmentNo;
                        }
                     }
                    archiveData=loadLocale();
                    if(archiveData!=null && !archiveData.equals("")){
                        archiveData=archiveData+"#"+enrichmentId;
                    }else{
                        archiveData =enrichmentId;
                    }
                    saveLocale(archiveData);
                    selectedCloudEnrList.clear();
                    enrich_Resource=getResouceAndEnrichments();
                    archieve = false;
                    invalidatingViews(enrich_Resource,archieve);
                    //lv.invalidateViews();
                }else if(selectedCloudEnrList.size()>0 &&archieve) {
                   // btn_archive.setBackgroundResource(R.drawable.shared_unarchive);
                    int selected = 0;
                    String enrichmentId = null;
                    archiveData=loadLocale();
                    String[] data=archiveData.split("#");
                    //enrichmentId=getSelectedData(archieve_enrich,false);
                    for (int i = 0; i < archieve_enrich.size(); i++) {
                        boolean exist = false;
                        CloudData cloud = archieve_enrich.get(i);
                        HashMap<String, String> hashmap = cloud.getMap();
                        String cloudEnrselected = hashmap.get(KEY_CLOUDENRSELECTED);
                        String enrichmentNo = hashmap.get(KEY_CENRICHMENTS);
                        String jsonContent = hashmap.get(KEY_CECONTENT);
                        if(cloudEnrselected.equals("true")){
                           for(int j=0;j<data.length;j++) {
                               String id=data[j];
                               if(id.equals(enrichmentNo)){
                                   exist=true;
                                   break;
                               }
                           }
                        }
                       if(!exist){
                            if(enrichmentId ==null){
                                enrichmentId=enrichmentNo;
                            }else {
                                enrichmentId = enrichmentId+"#"+ enrichmentNo;
                            }
                        }
                    }
                    archiveData=enrichmentId;
                    saveLocale(archiveData);
                    selectedCloudEnrList.clear();
                    enrich_Resource=getResouceAndEnrichments();
                    //archieve = true;
                    invalidatingViews(archieve_enrich,archieve);
                }
                break;
            }
            case R.id.btn_archivedata:{
               // saveLocale(archiveData);
                if(archieve){
                    //btn_archive.setBackgroundResource(R.drawable.shared_archive);
                    archieve = false;
                    if (selectedCloudEnrList != null && selectedCloudEnrList.size() > 0) {
                        selectedCloudEnrList.clear();
                    }
                    // enrich_Resource=getResouceAndEnrichments();
                    invalidatingViews(enrich_Resource, archieve);
                }else {
                    archieve = true;
                    //btn_archive.setBackgroundResource(R.drawable.shared_unarchive);
                    if (selectedCloudEnrList != null && selectedCloudEnrList.size() > 0) {
                        selectedCloudEnrList.clear();
                    }
                    // enrich_Resource=getResouceAndEnrichments();
                    invalidatingViews(archieve_enrich, archieve);
                }
                break;
            }
            case R.id.btn_del:{
                if(archieve){
                   new  deletingEnrichandResources(archieve_enrich).execute();
                }else{
                   new  deletingEnrichandResources(enrich_Resource).execute();
                }
                break;
            }
            case R.id.btn_download_all:{
                if(archieve) {
                    downloadallSelectedEnrichandResources(archieve_enrich);
                }else{
                    downloadallSelectedEnrichandResources(enrich_Resource);
                }
                break;
            }
            default:
                break;
        }

    }



    private class enrichAdapter extends BaseAdapter {
        ArrayList<CloudData> enrich_resource_data;

        public enrichAdapter(ArrayList<CloudData> enrich_resource) {
            enrich_resource_data=enrich_resource;
        }

        @Override
        public int getCount() {
           return enrich_resource_data.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }



        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if(convertView == null){
                LayoutInflater layoutinflater = (LayoutInflater) CloudEnrichDetails.this.getSystemService( CloudEnrichDetails.this.LAYOUT_INFLATER_SERVICE);
                view = layoutinflater.inflate(R.layout.enr_res_details, null);
                Typeface font = Typeface.createFromAsset( CloudEnrichDetails.this.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
            }
            TextView tv_Shared = (TextView) view.findViewById(R.id.tv_shared);
            Button btn_download= (Button)view.findViewById(R.id.btn_download);
            TextView tv_Desc = (TextView) view.findViewById(R.id.tv_desc);
            TextView tv_Type = (TextView) view.findViewById(R.id.tv_type);
            TextView tv_Date = (TextView) view.findViewById(R.id.tv_date);
            final CheckBox cb= (CheckBox)view. findViewById(R.id.checkBox);
            cb.setChecked(false);
            CloudData content = null;
            try {
                content= enrich_resource_data.get(position);
                HashMap<String, String> map = content.getMap();
                String cloudEnrselected = map.get(KEY_CLOUDENRSELECTED);
                String enrichmentNo=map.get(KEY_CENRICHMENTS);
                String jsonContent = map.get(KEY_CECONTENT);
                final String message = map.get(KEY_CEMESSAGE);
                String selection= map.get(webService.KEY_CLOUDENRSELECTED);

                JSONObject json=new JSONObject(jsonContent);
                String senderId=json.getString("sharedBy");
                String type=json.getString("type");
                String date_Time=json.getString("dateTime");
                tv_Shared.setText(senderId);
                tv_Type.setText(type);
                tv_Date.setText(date_Time);
                tv_Desc.setText(message);
                if(selection!=null){
                    if(selection.equals("true")){
                        cb.setChecked(true);
                    }else if(selection.equals("false")){
                        cb.setChecked(false);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            final CloudData finalContent = content;
            btn_download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  // if(selectedCloudEnrList.size()>0) {
                       boolean exist=false;
                       int selected = 0;
                      // CloudData content = enrich_resource_data.get(position);
                       HashMap<String, String> map = finalContent.getMap();
                       String Enrselected = map.get(KEY_CLOUDENRSELECTED);
                       String enrichment=map.get(KEY_CENRICHMENTS);
                       String Content = map.get(KEY_CECONTENT);
                       final String msg = map.get(KEY_CEMESSAGE);
                       String bookTitle = "";
                       String bookDetails = null;
                       String storeBookId = null;
                       String categoryId= null;
//                       for(int i=0;i<selectedCloudEnrList.size();i++){
//                           HashMap<String, String> hashmap =selectedCloudEnrList.get(i);
//                           String cloudEnrselected = hashmap.get(KEY_CLOUDENRSELECTED);
//                           String enrichmentNo=hashmap.get(KEY_CENRICHMENTS);
//                           String jsonContent = hashmap.get(KEY_CECONTENT);
//                           if(enrichmentNo.equals(enrichment)&&Enrselected.equals(cloudEnrselected)&&Content.equals(jsonContent)){
//                               exist=true;
//                               selected=i;
//                           }
//                       }
//                       if(exist){
                           try {
                               final JSONObject jsonObj = new JSONObject(Content);
                               String type = jsonObj.getString("type");
                               try {

                                  // JSONObject jsonObject = new JSONObject(jsonContent);
                                   storeBookId = jsonObj.getString("IDBook");
                                   //type = jsonObject.getString("type");
                                   if (type.equals("Enrichment")) {
                                       String idBook = jsonObj.getString("BOOK");
                                       JSONObject jsonObject1 = new JSONObject(idBook);
                                       String bookUrl = jsonObject1.getString("downloadURL");
                                       String storeBook = jsonObject1.getString("storeBook");
                                       bookTitle = jsonObject1.getString("title");
                                       String language = jsonObject1.getString("language");
                                       String imageUrl = jsonObject1.getString("imageURL");
                                       String template = jsonObject1.getString("template");
                                       if (template.equals("0")) template = "1";
                                       if(jsonObject1.getString("storeID").contains("M")){
                                           categoryId="1";
                                       }else if(jsonObject1.getString("storeID").contains("P")){
                                           categoryId="6";
                                       }
                                       //String categoryId = jsonObject1.getString("CID");
                                       if (categoryId.equals("0")) categoryId = "1";
                                       String descritpion = jsonObject1.getString("description");
                                       String totalPages = jsonObject1.getString("totalPages");
                                       //if(jsonObject1.getString("domainURL"))
                                       String domainURL = null;
                                       if(jsonObject1.has("domainURL")) {
                                           domainURL = jsonObject1.getString("domainURL");
                                       }

                                       bookDetails = enrichment + "#" + storeBookId + "#" + bookUrl + "#" + bookTitle + "#" + descritpion + "#" + template + "#" + totalPages + "#" + jsonObject1.getString("storeID") + "#" + categoryId + "#" + language + "#" + jsonObject1.getString("downloadCompleted") + "#" + bookUrl + "#" + jsonObject1.getString("bookEditable") + "#" + jsonObject1.getString("price") + "#" + imageUrl + "#" + "free"+ "#" +domainURL;
                                   }
                                   if (!checkStorBookExist(storeBookId)) {
                                      // cb.setChecked(false);
                                       if (type.equals("Enrichment")) {

                                           AlertDialog.Builder builder = new AlertDialog.Builder(CloudEnrichDetails.this);
                                           String alertMsg = getResources().getString(R.string.the_book) + " " + bookTitle + " " + getResources().getString(R.string.book_not_exist);
                                           builder.setMessage(alertMsg);
                                           builder.setCancelable(false);


                                           final String BookDataDetails = bookDetails;
                                           builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                               @Override
                                               public void onClick(DialogInterface dialog, int which) {
                                                 downloadingBooks(BookDataDetails);


                                               }
                                           });
                                           builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                               @Override
                                               public void onClick(DialogInterface dialog, int which) {
                                                   dialog.cancel();
                                               }
                                           });
                                           AlertDialog alert = builder.create();
                                           alert.show();
                                           return;
                                       } else {
                                           //cb.setChecked(false);
                                           UserFunctions.DisplayAlertDialog(CloudEnrichDetails.this, R.string.please_download_book_store, R.string.download_book);
                                           return;
                                       }
                                   }
                               } catch (JSONException e) {
                                   e.printStackTrace();
                               }
                               if (!checkStorBookstatus(storeBookId)) {
                                   UserFunctions.DisplayAlertDialog(CloudEnrichDetails.this, R.string.alt_downloading, R.string.download_book);
                                  // cb.setChecked(false);
                                   return;
                               }else {
                                  // GDriveExport gdrive=new GDriveExport(CloudEnrichDetails.this,msg);
                                   if (type.equals("Enrichment")) {
                                       map.put(KEY_CLOUDENRSELECTED, "true");
                                       ProgressDialog syncProgressDialog = ProgressDialog.show(CloudEnrichDetails.this, "", getResources().getString(R.string.syncing_please_wait), true);
                                       ArrayList<Object> bookList = gridShelf.bookListArray;
                                       ProgressDialog downloadProgDialog = new ProgressDialog(CloudEnrichDetails.this);
                                       new GDriveExport.EnrichmentDownloadFromGDrive(CloudEnrichDetails.this, downloadProgDialog, msg, jsonObj, bookList, selectedCloudEnrList, selected, webService.cloudEnrList, null, syncProgressDialog).execute();

                                   } else if (jsonObj.has("downloadPath") && type.equals("Resource")) {
                                       String downloadFromPath = jsonObj.getString("downloadPath");
                                       map.put(KEY_CLOUDENRSELECTED, "true");
                                       ProgressDialog syncProgressDialog = ProgressDialog.show(CloudEnrichDetails.this, "", getResources().getString(R.string.syncing_please_wait), true);
                                       ArrayList<Object> bookList = gridShelf.bookListArray;
                                       ProgressDialog downloadProgDialog = new ProgressDialog(CloudEnrichDetails.this);
                                       new GDriveExport.EnrichmentDownloadFromGDrive(CloudEnrichDetails.this, downloadProgDialog, msg, jsonObj, bookList, selectedCloudEnrList, selected, webService.cloudEnrList, null, syncProgressDialog).execute();

                                   } else {
                                       ProgressDialog syncProgressDialog = ProgressDialog.show(CloudEnrichDetails.this, "", getResources().getString(R.string.syncing_please_wait), true);
                                       ArrayList<Object> bookList = gridShelf.bookListArray;
                                       storeBookId = jsonObj.getString("IDBook");
                                       String contentXML = jsonObj.getString("ContentXMl");
                                       Resources resource = new Resources(CloudEnrichDetails.this);
                                       resource.setResourceId(10);
                                       resource.new getResourceXmlFromUrl(db, webService, bookList, contentXML, storeBookId, selectedCloudEnrList, selected, webService.cloudEnrList, null, syncProgressDialog).execute();
                                   }
                               }
                           }catch (JSONException e) {
                               e.printStackTrace();
                           }
//                       }else{
//                           UserFunctions.DisplayAlertDialog(CloudEnrichDetails.this, R.string.select_enrichment, R.string.warning);
//                       }

//                   }else{
//                       UserFunctions.DisplayAlertDialog(CloudEnrichDetails.this, R.string.select_enrichment, R.string.warning);
//                   }
                }
            });


           // final CloudData finalContent1 = content;
            cb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                       // CloudData content = enrich_resource_data.get(position);
                        HashMap<String, String> map = finalContent.getMap();
                        String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
                        String enrichmentNo = map.get(webService.KEY_CENRICHMENTS);
                        String jsonContent = map.get(webService.KEY_CECONTENT);
                        String bookTitle = "";
                        String bookDetails = null;
                        String storeBookId = null;
                      //else {
                            if (cloudEnrselected.equals("false")) {
                                map.put(webService.KEY_CLOUDENRSELECTED, "true");
                                selectedCloudEnrList.add(map);
                                cb.setChecked(true);
                            } else {
                                map.put(webService.KEY_CLOUDENRSELECTED, "false");
                                selectedCloudEnrList.remove(map);
                                cb.setChecked(false);
                            }
//                            boolean selected = true;
//
//                        }

                    invalidatingViews(enrich_resource_data,archieve);

                }
            });


            boolean selected=true;
            for(int i=0;i<enrich_resource_data.size();i++) {
                selected=false;
                CloudData enrich = enrich_resource_data.get(i);
                HashMap<String, String> enrData = enrich.getMap();
                String Enrselection = enrData.get(webService.KEY_CLOUDENRSELECTED);
                if(Enrselection.equals("false")){
                    selected=true;
                    break;
                }
            }
            if(selected){
                cb_selectall.setChecked(false);
            }else{
                cb_selectall.setChecked(true);
            }
            return view;
        }
    }
    private void downloadingBooks(final String bookDataDetails){
        final Dialog bookModalPopwindow = new Dialog(CloudEnrichDetails.this);
        bookModalPopwindow.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        bookModalPopwindow.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        bookModalPopwindow.setContentView(CloudEnrichDetails.this.getLayoutInflater().inflate(R.layout.bookmodal_neww, null));
        bookModalPopwindow.getWindow().setLayout(Globals.getDeviceWidth(),  Globals.getDeviceHeight());
        bookModalPopwindow.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ViewGroup viewGroup = (RelativeLayout) bookModalPopwindow.findViewById(R.id.bookmodal_layout);
        Typeface font = Typeface.createFromAsset(CloudEnrichDetails.this.getAssets(), "fonts/FiraSans-Regular.otf");
        UserFunctions.changeFont(viewGroup, font);
        viewGroup.setBackgroundResource(R.color.white);
        TextView titleLabel = (TextView) bookModalPopwindow.findViewById(R.id.book_Label);
        TextView languageLabel = (TextView) bookModalPopwindow.findViewById(R.id.langLabel);
        TextView descriptionLabel = (TextView) bookModalPopwindow.findViewById(R.id.desLabel);
        TextView authorLabel = (TextView) bookModalPopwindow.findViewById(R.id.authorLabel);
        TextView totalPagesLabel = (TextView) bookModalPopwindow.findViewById(R.id.totalPagesLabel);
        TextView txtSize = (TextView) bookModalPopwindow.findViewById(R.id.txtSize);
        TextView txtCategory = (TextView) bookModalPopwindow.findViewById(R.id.txtCategory);
        //TextView priceLabel =  (TextView) bookModalPopwindow.findViewById(R.id.priceLabel);
        ImageView bookImage = (ImageView) bookModalPopwindow.findViewById(R.id.imageView1);
        CircleButton back_btn = (CircleButton) bookModalPopwindow.findViewById(R.id.btn_back_bookmodel);
        final String[] Bookdetails = bookDataDetails.split("#");
        String priceContent = Bookdetails[1];
        String bookTitleContent = Bookdetails[3];
        String descriptionContent = Bookdetails[4];
        String authorContent = Bookdetails[5];
        if (authorContent.equals("null")) {
            authorContent = "";
        }
        String totalPagesContent = Bookdetails[6];
        if (totalPagesContent.equals("null")) {
            totalPagesContent = "-";
        }
        String langContent = Bookdetails[9];
        if (langContent.equals("null")) {
            langContent = "-";
        }
        String imageURL = Bookdetails[14];
        if (imageURL != null) {
            new saveImage(bookImage).execute(imageURL);
        }

        String size = "-";

        titleLabel.setText(bookTitleContent);
        languageLabel.setText(langContent);
        descriptionLabel.setText(descriptionContent);
        authorLabel.setText("Semanoor");
        totalPagesLabel.setText(totalPagesContent);
        txtSize.setText(size);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookModalPopwindow.dismiss();
            }
        });

        final Button download = (Button) bookModalPopwindow.findViewById(R.id.downloadBtn);
        boolean bookExist=false;
        for(int i=0;i<gridShelf.bookListArray.size();i++){
            bookExist=false;
            Book book= (Book) gridShelf.bookListArray.get(i);
            if(book.is_bStoreBook()&&book.get_bStoreID().equals(Bookdetails[1])){
                download.setText(book.getIsDownloadCompleted());
                download.setBackgroundColor(getResources().getColor(R.color.hovercolor));
                bookExist=true;
                break;
            }
        }
        if(!bookExist) {
            download.setText(getResources().getString(R.string.download));
            download.setBackgroundColor(getResources().getColor(R.color.hovercolor));
        }
        bookModalPopwindow.show();
        download.setBackgroundColor(CloudEnrichDetails.this.getResources().getColor(R.color.hovercolor));
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!download.getText().toString().equals(CloudEnrichDetails.this.getResources().getString(R.string.downloaded)) && !download.getText().toString().equals(CloudEnrichDetails.this.getResources().getString(R.string.downloading))) {
                    //downloadClicked();
                    //}else if(download.getText().toString().equals(CloudEnrichDetails.this.getResources().getString(R.string.download))){
                    download.setText(CloudEnrichDetails.this.getResources().getString(R.string.downloading));
                    GDriveExport gdrive = new GDriveExport(CloudEnrichDetails.this, bookDataDetails);
                    gdrive.downloadBook();
                    // download.setText(CloudEnrichDetails.this.getResources().getString(R.string.downloading));

                }
                v.invalidate();

            }
        });


    }
    public class saveImage extends AsyncTask<String, Void, Bitmap> {
        ImageView img;

        public saveImage(ImageView bookImg) {
            img=bookImg;
        }

        protected Bitmap doInBackground(String... urls) {
            String URLpath=urls[0];
            Bitmap bmp = null;
            try {
                URL url=new URL(URLpath);
                URLConnection ucon = url.openConnection();
                ucon.connect();
                bmp= BitmapFactory.decodeStream(url.openConnection().getInputStream());
                //System.out.println(bmp.getWidth());;
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return bmp;
        }
        protected void onPostExecute(Bitmap result){
            if(result!=null){
                img.setImageBitmap(result);
            }
        }
    }
    public void invalidatingViews(ArrayList<CloudData> enrich_Resource,boolean statement){
        lv.setAdapter(new enrichAdapter(enrich_Resource));
        lv.invalidateViews();
        if(statement){
            btn_archive.setBackgroundResource(R.drawable.shared_unarchive);
            txtNooorTitle.setText(CloudEnrichDetails.this.getResources().getString(R.string.Archive));
            btn_archivedata.setVisibility(View.GONE);
        }else{
            btn_archive.setBackgroundResource(R.drawable.shared_archive);
            txtNooorTitle.setText(this.getResources().getString(R.string.cloud_Inbox));
            if(archieve_enrich!=null&&archieve_enrich.size()>0) {
                btn_archivedata.setVisibility(View.VISIBLE);
            }else {
                btn_archivedata.setVisibility(View.GONE);
            }
         }
    }
    public boolean checkStorBookExist(String storeBookID){
        ArrayList<Object> bookList = gridShelf.bookListArray;
        for (int i = 0; i<bookList.size(); i++) {
            Book book = (Book) bookList.get(i);
            if (book.is_bStoreBook()) {
                if (book.get_bStoreID().equals(storeBookID)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkStorBookstatus(String storeBookID){
        ArrayList<Object> bookList = gridShelf.bookListArray;
        for (int i = 0; i<bookList.size(); i++) {
            Book book = (Book) bookList.get(i);
            if (book.is_bStoreBook()) {
                if (book.get_bStoreID().equals(storeBookID)&&!book.getIsDownloadCompleted().equals("downloading")) {
                    return true;
                }
            }
        }
        return false;
    }
    private ArrayList<CloudData> getResouceAndEnrichments() {
        selectedCloudEnrList = new ArrayList<HashMap<String, String>>();
        archieve_enrich= new ArrayList<CloudData>();
        ArrayList<CloudData> miniBook = new ArrayList<CloudData>();
        String[] enrlist=null;
         if(archiveData!=null) {
             enrlist=archiveData.split("#");
         }


        for (int i = 0; i < book.getEnrichments().size(); i++) {
            boolean exist = false;
            CloudData data = book.getEnrichments().get(i);
            HashMap<String, String> map = data.getMap();
            String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
            String enrichmentNo = map.get(webService.KEY_CENRICHMENTS);
            if(archiveData!=null) {
                for (int j = 0; j < enrlist.length; j++) {
                    String enrNo = enrlist[j];
                    if (enrNo.equals(enrichmentNo)) {
                        exist = true;
                        map.put(webService.KEY_CLOUDENRSELECTED, "false");
                        archieve_enrich.add(data);
                    }
                }
            }
            if(!exist){
                map.put(webService.KEY_CLOUDENRSELECTED, "false");
                miniBook.add(data);
            }
         }

        for (int i = 0; i < book.getResources().size(); i++) {
            boolean exist = false;
            CloudData data = book.getResources().get(i);
            HashMap<String, String> map = data.getMap();
            String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
            String enrichmentNo = map.get(webService.KEY_CENRICHMENTS);
            if(archiveData!=null) {
                for (int j = 0; j < enrlist.length; j++) {
                    String enrNo = enrlist[j];
                    if (enrNo.equals(enrichmentNo)) {
                        exist = true;
                        map.put(webService.KEY_CLOUDENRSELECTED, "false");
                        archieve_enrich.add(data);
                    }
                }
            }
            if(!exist){
                map.put(webService.KEY_CLOUDENRSELECTED, "false");
                miniBook.add(data);
            }
        }
        if(archieve_enrich!=null&&archieve_enrich.size()>0){
            btn_archivedata.setVisibility(View.VISIBLE);
            int size=archieve_enrich.size();
            btn_archivedata.setText(CloudEnrichDetails.this.getResources().getString(R.string.Archive)+"-"+size);
        }
        return miniBook;
    }

    @Override
    public void onBackPressed() {
        if(archieve){
            archieve = false;
            if (selectedCloudEnrList != null && selectedCloudEnrList.size() > 0) {
                selectedCloudEnrList.clear();
            }
            enrich_Resource=getResouceAndEnrichments();
            invalidatingViews(enrich_Resource, archieve);
        }else {
            setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
            finish();
        }
    }

   private String getSelectedData(ArrayList<CloudData> archieve_enrich, boolean locale){
       String enrichmentId = null;
       String[] data=archiveData.split("#");
        for (int i = 0; i < archieve_enrich.size(); i++) {
            boolean exist = false;
            CloudData cloud = archieve_enrich.get(i);
            HashMap<String, String> hashmap = cloud.getMap();
            String cloudEnrselected = hashmap.get(KEY_CLOUDENRSELECTED);
            String enrichmentNo = hashmap.get(KEY_CENRICHMENTS);
            String jsonContent = hashmap.get(KEY_CECONTENT);
            if(cloudEnrselected.equals("true")){
                for(int j=0;j<data.length;j++) {
                    String id=data[j];
                    if(id.equals(enrichmentNo)){
                        exist=true;
                        break;
                    }
                }
            }
            if(locale &&exist){
                if(enrichmentId==null){
                    enrichmentId=enrichmentNo;
                }else {
                    enrichmentId = enrichmentId+"#"+ enrichmentNo;
                }
            }else if(!locale &&!exist) {
                if (!exist) {
                    if (enrichmentId == null) {
                        enrichmentId = enrichmentNo;
                    } else {
                        enrichmentId = enrichmentId + "#" + enrichmentNo;
                    }
                }
            }
        }
       return enrichmentId;
   }




    private void selectingEnrichmentandResources(ArrayList<CloudData> archieve_enrich, CheckBox cb){
        for(int i = 0; i< archieve_enrich.size(); i++) {
            CloudData content =archieve_enrich.get(i);
            HashMap<String, String> map = content.getMap();
            String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
            String enrichmentNo = map.get(webService.KEY_CENRICHMENTS);
            String jsonContent = map.get(webService.KEY_CECONTENT);
            String bookTitle = "";
            String bookDetails = null;
            String storeBookId = null;
           /* try {
                //JSONObject jsonObject = new JSONObject(jsonContent);
                //String storeBookId = jsonObject.getString("IDBook");
                JSONObject jsonObject = new JSONObject(jsonContent);
                storeBookId = jsonObject.getString("IDBook");
                String type = jsonObject.getString("type");
                if (type.equals("Enrichment")) {
                    String idBook = jsonObject.getString("BOOK");
                    JSONObject jsonObject1 = new JSONObject(idBook);
                    String bookUrl = jsonObject1.getString("downloadURL");
                    String storeBook = jsonObject1.getString("storeBook");
                    bookTitle = jsonObject1.getString("title");
                    String language = jsonObject1.getString("language");
                    String imageUrl = jsonObject1.getString("imageURL");
                    String template = jsonObject1.getString("template");
                    if (template.equals("0")) template = "6";
                    String categoryId = jsonObject1.getString("CID");
                    if (categoryId.equals("0")) categoryId = "1";
                    String descritpion = jsonObject1.getString("description");
                    String totalPages = jsonObject1.getString("totalPages");

                    bookDetails = enrichmentNo + "#" + storeBookId + "#" + bookUrl + "#" + bookTitle + "#" + descritpion + "#" + template + "#" + totalPages + "#" + jsonObject1.getString("storeID") + "#" + categoryId + "#" + language + "#" + jsonObject1.getString("downloadCompleted") + "#" + bookUrl + "#" + jsonObject1.getString("bookEditable") + "#" + jsonObject1.getString("price") + "#" + imageUrl + "#" + "free";
                }
                if (!checkStorBookExist(storeBookId)) {
                    cb.setChecked(false);
                    if (type.equals("Enrichment")) {
                        // ((BaseAdapter) parent.getAdapter()).notifyDataSetChanged();
                        AlertDialog.Builder builder = new AlertDialog.Builder(CloudEnrichDetails.this);
                        String alertMsg = getResources().getString(R.string.the_book) + " " + bookTitle + " " + getResources().getString(R.string.book_not_exist);
                        builder.setMessage(alertMsg);
                        builder.setCancelable(false);


                        final String BookDataDetails = bookDetails;
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final Dialog bookModalPopwindow = new Dialog(CloudEnrichDetails.this);
                                bookModalPopwindow.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                bookModalPopwindow.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                int mImageWidth =(int) getResources().getDimension(R.dimen.sharedbk_alert_width);
                                int mImageHeight=(int) getResources().getDimension(R.dimen.sharedbk_alert_heigth);


                                bookModalPopwindow.setContentView(CloudEnrichDetails.this.getLayoutInflater().inflate(R.layout.bookmodal_neww,null));
                                bookModalPopwindow.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                                bookModalPopwindow.getWindow().setLayout(Globals.getDeviceWidth(),  Globals.getDeviceHeight());
                                ViewGroup viewGroup = (RelativeLayout) bookModalPopwindow.findViewById(R.id.bookmodal_layout);
                                Typeface font = Typeface.createFromAsset(CloudEnrichDetails.this.getAssets(), "fonts/FiraSans-Regular.otf");
                                UserFunctions.changeFont(viewGroup, font);
                                TextView titleLabel = (TextView) bookModalPopwindow.findViewById(R.id.book_Label);
                                TextView languageLabel = (TextView) bookModalPopwindow.findViewById(R.id.langLabel);
                                TextView descriptionLabel = (TextView) bookModalPopwindow.findViewById(R.id.desLabel);
                                TextView authorLabel = (TextView) bookModalPopwindow.findViewById(R.id.authorLabel);
                                TextView totalPagesLabel = (TextView) bookModalPopwindow.findViewById(R.id.totalPagesLabel);
                                TextView txtSize = (TextView) bookModalPopwindow.findViewById(R.id.txtSize);
                                TextView txtCategory = (TextView) bookModalPopwindow.findViewById(R.id.txtCategory);
                                //TextView priceLabel =  (TextView) bookModalPopwindow.findViewById(R.id.priceLabel);
                                ImageView bookImage = (ImageView) bookModalPopwindow.findViewById(R.id.imageView1);
                                CircleButton back_btn = (CircleButton) bookModalPopwindow.findViewById(R.id.btn_back_bookmodel);
                                final String[] Bookdetails = BookDataDetails.split("#");
                                String priceContent = Bookdetails[1];
                                String bookTitleContent = Bookdetails[3];
                                String descriptionContent = Bookdetails[4];
                                String authorContent = Bookdetails[5];
                                if (authorContent.equals("null")) {
                                    authorContent = "";
                                }
                                String totalPagesContent = Bookdetails[6];
                                if (totalPagesContent.equals("null")) {
                                    totalPagesContent = "-";
                                }
                                String langContent = Bookdetails[9];
                                if (langContent.equals("null")) {
                                    langContent = "-";
                                }
                                String imageURL = Bookdetails[14];
                                Glide.with(CloudEnrichDetails.this).load(imageURL)
                                        .thumbnail(0.5f)
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .override(200,200)
                                        .into(bookImage);

                                //String size = Bookdetails[35];
                                //if (size.equals("null")) {
                                String size = "-";
                                // }
                                titleLabel.setText(bookTitleContent);
                                languageLabel.setText(langContent);
                                descriptionLabel.setText(descriptionContent);
                                authorLabel.setText("Semanoor");
                                totalPagesLabel.setText(totalPagesContent);
                                txtSize.setText(size);
                                back_btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        bookModalPopwindow.dismiss();
                                    }
                                });

                                final Button download = (Button) bookModalPopwindow.findViewById(R.id.downloadBtn);
                                download.setText(CloudEnrichDetails.this.getResources().getString(R.string.download));
                                bookModalPopwindow.show();
                                download.setBackgroundColor(CloudEnrichDetails.this.getResources().getColor(R.color.hovercolor));
                                download.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (!download.getText().toString().equals(CloudEnrichDetails.this.getResources().getString(R.string.downloaded)) && !download.getText().toString().equals(CloudEnrichDetails.this.getResources().getString(R.string.downloading))) {
                                             download.setText(CloudEnrichDetails.this.getResources().getString(R.string.downloading));
                                            GDriveExport gdrive = new GDriveExport(CloudEnrichDetails.this, BookDataDetails);
                                            gdrive.downloadBook();


                                        }
                                        v.invalidate();

                                    }
                                });



                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                        return;
                    } else {
                        cb.setChecked(false);
                        UserFunctions.DisplayAlertDialog(CloudEnrichDetails.this, R.string.please_download_book_store, R.string.download_book);
                        return;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!checkStorBookstatus(storeBookId)) {
                UserFunctions.DisplayAlertDialog(CloudEnrichDetails.this, R.string.alt_downloading, R.string.download_book);
                cb.setChecked(false);
                return;
            } else {  */
                if(cb_selectall.isChecked()){
                    map.put(webService.KEY_CLOUDENRSELECTED, "true");
                    selectedCloudEnrList.add(map);
                }else{
                    map.put(webService.KEY_CLOUDENRSELECTED, "false");
                    selectedCloudEnrList.remove(map);

                }

           // }
        }
        invalidatingViews(archieve_enrich,archieve);
    }



    private class deletingEnrichandResources extends AsyncTask<Void, Void, Void> {
        //JSONArray enrich_resource;
        String jsonString;
        ArrayList<CloudData> enrich_ResourceList;

        public deletingEnrichandResources(ArrayList<CloudData> archieve_enrich) {
            enrich_ResourceList=archieve_enrich;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //enrich_resource = new JSONArray();

        }

        @Override
        protected Void doInBackground(Void... params) {
            JSONArray enrich_resource = new JSONArray();
            for(int i=0;i<selectedCloudEnrList.size();i++){
                HashMap<String, String> hashmap = selectedCloudEnrList.get(i);
                String cloudEnrselected = hashmap.get(KEY_CLOUDENRSELECTED);
                String enrichmentNo = hashmap.get(KEY_CENRICHMENTS);
                String jsonContent = hashmap.get(KEY_CECONTENT);

//                CloudData data=enrich_ResourceList.get(i);
//                HashMap<String, String> map = data.getMap();
//                String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
//                // String enrichmentNo = map.get(webService.KEY_CENRICHMENTS);
//                String enrichmentNo = map.get(KEY_CENRICHMENTS);
                // String jsonContent = map.get(webService.KEY_CECONTENT);
                if(cloudEnrselected.equals("true")){
                    if(archieve){
                        deletearchieveEnrichments();

                    }else{
                        deleteEnrichments();

                    }
                    JSONObject id = new JSONObject();
                    try {
                        id.put("ID",enrichmentNo);
                        enrich_resource.put(id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put("deleteEnrichId",enrich_resource);
                jsonData.put("deleteType","");
                jsonString = jsonData.toString();

                // sendToSemaNotificationService(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            webService.deleteEnrichments(jsonString,emaild);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


           /* for(int i=0;i<enrich_ResourceList.size();i++) {
                CloudData data = enrich_ResourceList.get(i);
                HashMap<String, String> map = data.getMap();
                String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
                // String enrichmentNo = map.get(webService.KEY_CENRICHMENTS);
                String enrichmentNo = map.get(KEY_CENRICHMENTS);
                // String jsonContent = map.get(webService.KEY_CECONTENT);
                if (cloudEnrselected.equals("true")) {
                    for(int j=0;j<webService.enrichmentsList.size();j++) {
                        HashMap<String, String> resultData=webService.enrichmentsList;
                        if(resultData.get("ID").equals(enrichmentNo)){
                            if(archieve){
                                archieve_enrich.remove(i);
                            }else {
                                enrich_Resource.remove(i);
                            }
                        }
                    }
                }
            } */
            if(archieve) {
                invalidatingViews(archieve_enrich,archieve);
            } else{
                invalidatingViews(enrich_Resource,archieve);
            }
        }
    }
    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data){

    }

    public void deletearchieveEnrichments(){
        for(int i=0;i<archieve_enrich.size();i++) {
            CloudData data = archieve_enrich.get(i);
            HashMap<String, String> map = data.getMap();
            String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
            // String enrichmentNo = map.get(webService.KEY_CENRICHMENTS);
            String enrichmentNo = map.get(KEY_CENRICHMENTS);
            // String jsonContent = map.get(webService.KEY_CECONTENT);
            if (cloudEnrselected.equals("true")) {
                      archieve_enrich.remove(i);
            }
        }
    }
    public void deleteEnrichments(){
        for(int i=0;i<enrich_Resource.size();i++) {
            CloudData data = enrich_Resource.get(i);
            HashMap<String, String> map = data.getMap();
            String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
            // String enrichmentNo = map.get(webService.KEY_CENRICHMENTS);
            String enrichmentNo = map.get(KEY_CENRICHMENTS);
            // String jsonContent = map.get(webService.KEY_CECONTENT);
            if (cloudEnrselected.equals("true")) {
                    enrich_Resource.remove(i);

            }
        }
    }
    private void downloadallSelectedEnrichandResources(ArrayList<CloudData> archieve_enrich) {
        if (selectedCloudEnrList.size() > 0) {
            boolean exist = false;
            int selected = 0;
            String Content = null,msg=null;
            HashMap<String, String> map = null;
            for (int j = 0; j < archieve_enrich.size(); j++) {
                 CloudData content = archieve_enrich.get(j);
                 map = content.getMap();
                 String Enrselected = map.get(KEY_CLOUDENRSELECTED);
                 String enrichment = map.get(KEY_CENRICHMENTS);
                 Content = map.get(KEY_CECONTENT);
                 String storeBookId=null,bookTitle=null,bookDetails=null;
                 msg = map.get(KEY_CEMESSAGE);
                for (int i = 0; i < selectedCloudEnrList.size(); i++) {
                    HashMap<String, String> hashmap = selectedCloudEnrList.get(i);
                    String cloudEnrselected = hashmap.get(KEY_CLOUDENRSELECTED);
                    String enrichmentNo = hashmap.get(KEY_CENRICHMENTS);
                    String jsonContent = hashmap.get(KEY_CECONTENT);
                    if (enrichmentNo.equals(enrichment) && Enrselected.equals(cloudEnrselected) && Content.equals(jsonContent)) {

                        try {
                            final JSONObject jsonObj = new JSONObject(Content);
                            String type = jsonObj.getString("type");
                              // JSONObject jsonObject = new JSONObject(jsonContent);
                                storeBookId = jsonObj.getString("IDBook");
                                //type = jsonObject.getString("type");
                                if (type.equals("Enrichment")) {
                                    String idBook = jsonObj.getString("BOOK");
                                    JSONObject jsonObject1 = new JSONObject(idBook);
                                    String bookUrl = jsonObject1.getString("downloadURL");
                                    String storeBook = jsonObject1.getString("storeBook");
                                    bookTitle = jsonObject1.getString("title");
                                    String language = jsonObject1.getString("language");
                                    String imageUrl = jsonObject1.getString("imageURL");
                                    String template = jsonObject1.getString("template");
                                    if (template.equals("0")) template = "6";
                                    String categoryId = null;          // = jsonObject1.getString("CID");
                                    if(jsonObject1.getString("storeID").contains("M")){
                                        categoryId="1";
                                    }else if(jsonObject1.getString("storeID").contains("P")){
                                        categoryId="6";
                                    }
                                    if (categoryId.equals("0")) categoryId = "1";
                                    String descritpion = jsonObject1.getString("description");
                                    String totalPages = jsonObject1.getString("totalPages");
                                    String domainURL;
                                    if (jsonObject1.has("domainURL")) {
                                        domainURL = jsonObject1.getString("domainURL");
                                    }else {
                                        domainURL = "http://www.nooor.com/sboook/IPadSSboookService.asmx";
                                    }

                                    bookDetails = enrichment + "#" + storeBookId + "#" + bookUrl + "#" + bookTitle + "#" + descritpion + "#" + template + "#" + totalPages + "#" + jsonObject1.getString("storeID") + "#" + categoryId + "#" + language + "#" + jsonObject1.getString("downloadCompleted") + "#" + bookUrl + "#" + jsonObject1.getString("bookEditable") + "#" + jsonObject1.getString("price") + "#" + imageUrl + "#" + "free"+ "#" +domainURL;
                                }
                                if (!checkStorBookExist(storeBookId)) {
                                    // cb.setChecked(false);
                                    if (type.equals("Enrichment")) {

                                        AlertDialog.Builder builder = new AlertDialog.Builder(CloudEnrichDetails.this);
                                        String alertMsg = getResources().getString(R.string.the_book) + " " + bookTitle + " " + getResources().getString(R.string.book_not_exist);
                                        builder.setMessage(alertMsg);
                                        builder.setCancelable(false);


                                        final String BookDataDetails = bookDetails;
                                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                final Dialog bookModalPopwindow = new Dialog(CloudEnrichDetails.this);
                                                bookModalPopwindow.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                                bookModalPopwindow.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                                bookModalPopwindow.setContentView(CloudEnrichDetails.this.getLayoutInflater().inflate(R.layout.bookmodal_neww, null));
                                                bookModalPopwindow.getWindow().setLayout(Globals.getDeviceWidth(),  Globals.getDeviceHeight());
                                                bookModalPopwindow.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                                                ViewGroup viewGroup = (RelativeLayout) bookModalPopwindow.findViewById(R.id.bookmodal_layout);
                                                Typeface font = Typeface.createFromAsset(CloudEnrichDetails.this.getAssets(), "fonts/FiraSans-Regular.otf");
                                                UserFunctions.changeFont(viewGroup, font);
                                                viewGroup.setBackgroundResource(R.color.white);
                                                TextView titleLabel = (TextView) bookModalPopwindow.findViewById(R.id.book_Label);
                                                TextView languageLabel = (TextView) bookModalPopwindow.findViewById(R.id.langLabel);
                                                TextView descriptionLabel = (TextView) bookModalPopwindow.findViewById(R.id.desLabel);
                                                TextView authorLabel = (TextView) bookModalPopwindow.findViewById(R.id.authorLabel);
                                                TextView totalPagesLabel = (TextView) bookModalPopwindow.findViewById(R.id.totalPagesLabel);
                                                TextView txtSize = (TextView) bookModalPopwindow.findViewById(R.id.txtSize);
                                                TextView txtCategory = (TextView) bookModalPopwindow.findViewById(R.id.txtCategory);
                                                //TextView priceLabel =  (TextView) bookModalPopwindow.findViewById(R.id.priceLabel);
                                                ImageView bookImage = (ImageView) bookModalPopwindow.findViewById(R.id.imageView1);
                                                CircleButton back_btn = (CircleButton) bookModalPopwindow.findViewById(R.id.btn_back_bookmodel);
                                                final String[] Bookdetails = BookDataDetails.split("#");
                                                String priceContent = Bookdetails[1];
                                                String bookTitleContent = Bookdetails[3];
                                                String descriptionContent = Bookdetails[4];
                                                String authorContent = Bookdetails[5];
                                                if (authorContent.equals("null")) {
                                                    authorContent = "";
                                                }
                                                String totalPagesContent = Bookdetails[6];
                                                if (totalPagesContent.equals("null")) {
                                                    totalPagesContent = "-";
                                                }
                                                String langContent = Bookdetails[9];
                                                if (langContent.equals("null")) {
                                                    langContent = "-";
                                                }
                                                String imageURL = Bookdetails[14];
                                                if (imageURL != null) {
                                                    new saveImage(bookImage).execute(imageURL);
                                                }

                                                String size = "-";

                                                titleLabel.setText(bookTitleContent);
                                                languageLabel.setText(langContent);
                                                descriptionLabel.setText(descriptionContent);
                                                authorLabel.setText("Semanoor");
                                                totalPagesLabel.setText(totalPagesContent);
                                                txtSize.setText(size);
                                                back_btn.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        bookModalPopwindow.dismiss();
                                                    }
                                                });

                                                final Button download = (Button) bookModalPopwindow.findViewById(R.id.downloadBtn);
                                                boolean bookExist=false;
                                                for(int i=0;i<gridShelf.bookListArray.size();i++){
                                                    bookExist=false;
                                                    Book book= (Book) gridShelf.bookListArray.get(i);
                                                    if(book.is_bStoreBook()&&book.get_bStoreID().equals(Bookdetails[1])){
                                                        download.setText(book.getIsDownloadCompleted());
                                                        download.setBackgroundColor(getResources().getColor(R.color.hovercolor));
                                                        bookExist=true;
                                                        break;
                                                    }
                                                }
                                                if(!bookExist) {
                                                    download.setText(getResources().getString(R.string.download));
                                                    download.setBackgroundColor(getResources().getColor(R.color.hovercolor));
                                                }
                                                bookModalPopwindow.show();
                                                download.setBackgroundColor(CloudEnrichDetails.this.getResources().getColor(R.color.hovercolor));
                                                download.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        if (!download.getText().toString().equals(CloudEnrichDetails.this.getResources().getString(R.string.downloaded)) && !download.getText().toString().equals(CloudEnrichDetails.this.getResources().getString(R.string.downloading))) {
                                                            //downloadClicked();
                                                            //}else if(download.getText().toString().equals(CloudEnrichDetails.this.getResources().getString(R.string.download))){
                                                            download.setText(CloudEnrichDetails.this.getResources().getString(R.string.downloading));
                                                            GDriveExport gdrive = new GDriveExport(CloudEnrichDetails.this, BookDataDetails);
                                                            gdrive.downloadBook();
                                                            // download.setText(CloudEnrichDetails.this.getResources().getString(R.string.downloading));

                                                        }
                                                        v.invalidate();

                                                    }
                                                });



                                            }
                                        });
                                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        AlertDialog alert = builder.create();
                                        alert.show();
                                        return;
                                    } else {
                                        //cb.setChecked(false);
                                        UserFunctions.DisplayAlertDialog(CloudEnrichDetails.this, R.string.please_download_book_store, R.string.download_book);
                                        return;
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        if (!checkStorBookstatus(storeBookId)) {
                            UserFunctions.DisplayAlertDialog(CloudEnrichDetails.this, R.string.alt_downloading, R.string.download_book);
                            // cb.setChecked(false);
                            return;
                         }else {
                            exist = true;
                            selected = i;
                        }
                    }
                }
              }
                if (exist) {
                    try {
                        final JSONObject jsonObj = new JSONObject(Content);
                        String type = jsonObj.getString("type");

                        if (type.equals("Enrichment")) {
                            map.put(KEY_CLOUDENRSELECTED, "true");
                            //selectedCloudEnrList.add(map);
                            //enrich_Resource.remove(content);
                            ProgressDialog syncProgressDialog = ProgressDialog.show(CloudEnrichDetails.this, "", getResources().getString(R.string.syncing_please_wait), true);
                            ArrayList<Object> bookList = gridShelf.bookListArray;
                            ProgressDialog downloadProgDialog = new ProgressDialog(CloudEnrichDetails.this);
                           // GDriveExport gdrive=new GDriveExport(CloudEnrichDetails.this,msg);
                            new GDriveExport.EnrichmentDownloadFromGDrive(CloudEnrichDetails.this, downloadProgDialog, msg, jsonObj, bookList, selectedCloudEnrList, selected, webService.cloudEnrList, null, syncProgressDialog).execute();

                        }else if (type.equals("Resources")) {
                            map.put(KEY_CLOUDENRSELECTED, "true");
                            //selectedCloudEnrList.add(map);
                            //enrich_Resource.remove(content);
                            ProgressDialog syncProgressDialog = ProgressDialog.show(CloudEnrichDetails.this, "", getResources().getString(R.string.syncing_please_wait), true);
                            ArrayList<Object> bookList = gridShelf.bookListArray;
                            ProgressDialog downloadProgDialog = new ProgressDialog(CloudEnrichDetails.this);
                            //GDriveExport gdrive=new GDriveExport(CloudEnrichDetails.this,msg);
                            new GDriveExport.EnrichmentDownloadFromGDrive(CloudEnrichDetails.this, downloadProgDialog, msg, jsonObj, bookList, selectedCloudEnrList, selected, webService.cloudEnrList, null, syncProgressDialog).execute();

                        } else {
                            ProgressDialog syncProgressDialog = ProgressDialog.show(CloudEnrichDetails.this, "", getResources().getString(R.string.syncing_please_wait), true);
                            ArrayList<Object> bookList = gridShelf.bookListArray;
                            String storeBookId = jsonObj.getString("IDBook");
                            String contentXML = jsonObj.getString("ContentXMl");
                            Resources resource = new Resources(CloudEnrichDetails.this);
                            resource.setResourceId(10);
                            resource.new getResourceXmlFromUrl(db, webService, bookList, contentXML, storeBookId, selectedCloudEnrList, selected, webService.cloudEnrList, null, syncProgressDialog).execute();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    UserFunctions.DisplayAlertDialog(CloudEnrichDetails.this, R.string.select_enrichment, R.string.warning);
                    return;
                }


        } else {
            UserFunctions.DisplayAlertDialog(CloudEnrichDetails.this, R.string.select_enrichment, R.string.warning);
            return;
        }
    }
}

