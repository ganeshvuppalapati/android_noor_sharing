package com.semanoor.manahij;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptg.mindmap.xml.LoadImageFromServer;
import com.ptg.views.CircleButton;
import com.semanoor.manahij.mqtt.CallbackInterface;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.source_sboookauthor.Category;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.MyContacts;
import com.semanoor.source_sboookauthor.MyGroups;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.UserGroups;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krishna on 03-10-2016.
 */
public class Grp_SelectionAdapter {

    ExportEnrGroupActivity exportEnrGroup;
    Dialog exportEnrPopover;
    public ArrayList<UserGroups> userGroups;
    boolean my_contacts;
    private ArrayList<MyContacts> selectedContacts = new ArrayList<>();



    public Grp_SelectionAdapter(ExportEnrGroupActivity _context) {
        this.exportEnrGroup = _context;
    }


    /*
     * Displaying user shelfs (Groups and UserContacts)
     */

    public class GroupShelfAdapter extends RecyclerView.Adapter<GroupShelfAdapter.MyViewHolder> {
        ExportEnrGroupActivity exportEnrActivity;
        public ArrayList<Category> categoryListArray;
        // Dialog exportEnrPopover;

        public GroupShelfAdapter(ExportEnrGroupActivity context, ArrayList<Category> category) {
            exportEnrActivity = context;
            categoryListArray = category;
        }

        // private List<Movie> moviesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView tv_Title;
            RecyclerView export_shelf;
            Button btnPropertyAccord,btn_editShelf;
            boolean isShelfEdit;

            public MyViewHolder(View view) {
                super(view);
                export_shelf = (RecyclerView) view.findViewById(R.id.gridView1);
                btnPropertyAccord = (Button) view.findViewById(R.id.button1);
                tv_Title = (TextView) view.findViewById(R.id.text);
                btn_editShelf= (Button) view.findViewById(R.id.btn_edit);

                //title = (TextView) view.findViewById(R.id.title);
                // genre = (TextView) view.findViewById(R.id.genre);
                // year = (TextView) view.findViewById(R.id.year);
            }
        }


//    public MoviesAdapter(List<Movie> moviesList) {
//        this.moviesList = moviesList;
//    }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.export_enr_shelf, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            final Category category = categoryListArray.get(position);
            holder.tv_Title.setText(category.getCategoryName());
            Grp_SelectionAdapter enr = new Grp_SelectionAdapter(exportEnrActivity);
            //  ExportEnrItem grpItems = new ExportEnrItem(category, holder.export_shelf, Ex);
            //holder.export_shelf.setAdapter(horizontalAdapter);


            holder.export_shelf.setHasFixedSize(true);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(exportEnrActivity, LinearLayoutManager.HORIZONTAL, false);
            holder.export_shelf.setLayoutManager(mLayoutManager);
            // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            holder.export_shelf.setItemAnimator(new DefaultItemAnimator());



            if (category.isAccordExpanded()) {
                holder.btnPropertyAccord.setBackgroundResource(R.drawable.new_category_acordian_collapsed);
                ViewGroup.LayoutParams gridlayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.export_shelf.getLayoutParams();
                gridlayoutParams.height = (int) exportEnrActivity.getResources().getDimension(R.dimen.shelf_height) + (int) exportEnrActivity.getResources().getDimension(R.dimen.shelf_height);

                holder.export_shelf.setLayoutParams(gridlayoutParams);
                holder.export_shelf.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {

                    @Override
                    public void onLayoutChange(View v, int left, int top, int right,
                                               int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {

                    }
                });
                GridLayoutManager gridLayout = new GridLayoutManager(exportEnrActivity, 3, GridLayoutManager.VERTICAL, false);
                //llm.canScrollHorizontally();
                holder.export_shelf.setLayoutManager(gridLayout);
                holder.export_shelf.setHasFixedSize(true);
                holder.export_shelf.invalidate();
            } else {
                holder.btnPropertyAccord.setBackgroundResource(R.drawable.new_category_acordian_expanded);
                ViewGroup.LayoutParams gridlayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.export_shelf.getLayoutParams();
                int defaultHeight = (int) exportEnrActivity.getResources().getDimension(R.dimen.shelf_height);
                if (gridlayoutParams.height == defaultHeight){

                }else{
                    gridlayoutParams.height = (int) exportEnrActivity.getResources().getDimension(R.dimen.shelf_height);
                }
                holder.export_shelf.setLayoutParams(gridlayoutParams);
                LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(exportEnrActivity, LinearLayoutManager.HORIZONTAL, false);
                holder.export_shelf.setLayoutManager(horizontalLayoutManagaer);
                holder.export_shelf.setItemAnimator(new DefaultItemAnimator());
            }

            if (category.getCategoryId() == 0) {
                holder.export_shelf.setAdapter(enr.new UserGroupsList(category, holder.export_shelf, exportEnrActivity, exportEnrActivity.groupList));
            } else if (category.getCategoryId() == 1){
                holder.export_shelf.setAdapter(enr.new UserContactList(category, holder.export_shelf, exportEnrActivity, exportEnrActivity.myContacts,false));
            }else{
                holder.btn_editShelf.setVisibility(View.GONE);
                holder.export_shelf.setAdapter(enr.new UserContactList(category, holder.export_shelf, exportEnrActivity, exportEnrActivity.gmailContacts,true));
            }

            if(category.isHidden()){
                holder.btn_editShelf.setBackgroundResource(R.drawable.done);
            }else{
                holder.btn_editShelf.setBackgroundResource(R.drawable.edit_cloud);
            }

            holder.btnPropertyAccord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (category.isAccordExpanded()) {
                        holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_expanded_new);
                        category.setAccordExpanded(false);
                        notifyDataSetChanged();
                    } else {
                        holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_collapsed_new);
                        category.setAccordExpanded(true);
                        notifyDataSetChanged();
                    }
                }
            });

            holder.export_shelf.addOnItemTouchListener(new RecyclerTouchListener(exportEnrActivity, holder.export_shelf, new RecyclerTouchListener.ClickListener() {

                @Override
                public void onClick(View view, int position) {
                    if (position == 0) {

                    } else {

                    }
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));

           holder.btn_editShelf.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   //if (category.getCategoryId()!= 0) {
                       if (category.isHidden()) {
                           holder.btn_editShelf.setBackgroundResource(R.drawable.edit_cloud);
                           category.setHidden(false);
                       } else {
                           holder.btn_editShelf.setBackgroundResource(R.drawable.done);
                           category.setHidden(true);

                       }

                       if (category.getCategoryId() == 0) {
                           if (exportEnrActivity.grpSelected) {
                               ArrayList<MyContacts> groupsContactList = exportEnrActivity.selectGroup.getGroupsContactList();
                               Grp_SelectionAdapter enr = new Grp_SelectionAdapter(exportEnrActivity);
                               holder.export_shelf.setAdapter(enr.new UserContactList(category, holder.export_shelf, exportEnrActivity, groupsContactList,false));
                           } else {
                               holder.export_shelf.setAdapter(new UserGroupsList(category, holder.export_shelf, exportEnrActivity, exportEnrActivity.groupList));
                           }
                       } else {
                           // String query = "select * from tblUserContacts where groupsID='"+0+"'and userID='"+exportEnrActivity.email+"'";

                           holder.export_shelf.setAdapter(new UserContactList(category, holder.export_shelf, exportEnrActivity, exportEnrActivity.myContacts,false));
                       }
                  // }
               }
           });

        }

        @Override
        public int getItemCount() {
            return categoryListArray.size();   // moviesList.size();
        }

    }




    /*
     * Displaying user Groups
     */
    public class UserGroupsList extends RecyclerView.Adapter<UserGroupsList.MyViewHolder> {
        ExportEnrGroupActivity exportEnrActivity;
        public ArrayList<Category> categoryListArray;
        int Count = 0;
        private List<String> horizontalList;
        // private ArrayList<Book> categoryBooksList = new ArrayList<Book>();
        ArrayList<CloudData> Books = new ArrayList<CloudData>();
        ArrayList<MyGroups> userGroups;
        RecyclerView recyclerView;
        Category category;


        public UserGroupsList(Category catgory, RecyclerView export_shelf, ExportEnrGroupActivity exportActivity, ArrayList<MyGroups> Groups) {
            exportEnrActivity = exportActivity;
            this.category = catgory;
            //  Book book = null;
            this.recyclerView = export_shelf;
            this.userGroups = Groups;

        }

        // private List<Movie> moviesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            // public TextView title, year, genre;
            //public TextView txtView;
            ImageView bookImg;
            TextView tv_Title;
            RelativeLayout rl,rl_imaglayout;
            CheckBox cb_selectall;
            ImageView img_add;
            LinearLayout rl_layout;
            RelativeLayout layout_grpImages;
            View status;
            Button btn_text,update;
            CircleButton btn_delete,btn_edit;


            public MyViewHolder(View view) {
                super(view);
                rl = (RelativeLayout) view.findViewById(R.id.shelfbookLayout);
                ViewGroup.MarginLayoutParams margins = (ViewGroup.MarginLayoutParams) rl.getLayoutParams();
                margins.setMargins(0, 0, 0, 0);
                rl_imaglayout = (RelativeLayout) view.findViewById(R.id.relativeLayout1);
                rl_layout = (LinearLayout) view.findViewById(R.id.relativeLayout2);
                layout_grpImages = (RelativeLayout) view.findViewById(R.id.layout_add_grp);
                img_add = (ImageView) view.findViewById(R.id.img_add);
                bookImg = (ImageView) view.findViewById(R.id.imageView2);
                tv_Title = (TextView) view.findViewById(R.id.textView1);
                btn_text = (Button) view.findViewById(R.id.btn_text);
                cb_selectall = (CheckBox) view.findViewById(R.id.cb_selectall);
                btn_delete= (CircleButton) view.findViewById(R.id.btn_delete);
                btn_edit= (CircleButton) view.findViewById(R.id.btn_edit);
                update = (Button) view.findViewById(R.id.edit);
                status=(View)view.findViewById(R.id.btnisOnline);

            }
        }



        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shelf_enr_pages, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            MyGroups users = null;

            if (position == 0) {
                holder.rl_imaglayout.setVisibility(View.INVISIBLE);
                holder.layout_grpImages.setVisibility(View.VISIBLE);
                holder.tv_Title.setText(exportEnrActivity.getResources().getString(R.string.create_group_txt));

            } else {
                users = userGroups.get(position - 1);
                holder.bookImg.setBackgroundResource(R.drawable.group);
                holder.layout_grpImages.setVisibility(View.INVISIBLE);
                holder.rl_imaglayout.setVisibility(View.VISIBLE);
                holder.tv_Title.setText(users.getGroupName());
                //holder.btn_text.setVisibility(View.INVISIBLE);
                holder.btn_text.setText(users.getGroupsContactList().size()+"");
                holder.update.setVisibility(View.INVISIBLE);
                if (users.isGroupSelected()) {
                    holder.cb_selectall.setChecked(true);
                } else {
                    holder.cb_selectall.setChecked(false);

                }
                if(exportEnrActivity.enrichmentDetails==null &&exportEnrActivity.currentBook==null &&!exportEnrActivity.fromCloud) {
                    holder.cb_selectall.setVisibility(View.INVISIBLE);
                }
            }

            final MyGroups finalUsers = users;
            holder.status.setVisibility(View.INVISIBLE);
            holder.cb_selectall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   if(position!=0){
                       if (finalUsers.isGroupSelected()) {
                           holder.cb_selectall.setChecked(false);
                           selectingGroupsandContacts(exportEnrActivity,exportEnrActivity.groupList,position-1,false);

                       } else {
                           holder.cb_selectall.setChecked(true);
                           selectingGroupsandContacts(exportEnrActivity,exportEnrActivity.groupList,position-1,true);
                       }

                   }
                 }
            });

            if(category.isHidden()){
                holder.btn_delete.setVisibility(View.VISIBLE);
                holder.btn_edit.setVisibility(View.VISIBLE);
            }else{
                holder.btn_delete.setVisibility(View.GONE);
                holder.btn_edit.setVisibility(View.GONE);
            }
            final MyGroups finalUsers1 = users;
            holder.btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(exportEnrActivity);
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String query="delete from tblUserGroups where name='"+ finalUsers1.getGroupName() + "' and userID='" + exportEnrActivity.email + "'";
                            exportEnrActivity.db.executeQuery(query);
                            exportEnrActivity.db.executeQuery("delete from tblUserContacts where userID='" + exportEnrActivity.email + "' and groupsID='" + finalUsers1.getId() + "'");
                            exportEnrActivity.groupList.remove(finalUsers1);
                            notifyDataSetChanged();
                            dialog.cancel();
                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.setTitle(R.string.delete);
                    builder.setMessage(R.string.suredelete);
                    builder.show();
                    //finalUsers1
                }
            });
            holder.bookImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    exportEnrActivity.selectGroup = finalUsers;
                    exportEnrActivity.grpSelected = true;
                    //finalUsers.getGroupsContactList().size();
                    ArrayList<MyContacts> groupsContactList = finalUsers.getGroupsContactList();
                    Grp_SelectionAdapter enr = new Grp_SelectionAdapter(exportEnrActivity);
                    recyclerView.setAdapter(enr.new UserContactList(category, recyclerView, exportEnrActivity, groupsContactList,false));
                }
            });


            holder.img_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position == 0) {
                        alertDialog(exportEnrActivity, recyclerView, category, null, false);
                        //creatingGroup(exportEnrActivity, recyclerView, category);
                    } else {
                        exportEnrActivity.selectGroup = finalUsers;
                        exportEnrActivity.grpSelected = true;
                        //finalUsers.getGroupsContactList().size();
                        ArrayList<MyContacts> groupsContactList = finalUsers.getGroupsContactList();
                        Grp_SelectionAdapter enr = new Grp_SelectionAdapter(exportEnrActivity);
                        recyclerView.setAdapter(enr.new UserContactList(category, recyclerView, exportEnrActivity, groupsContactList,false));
                    }
                }
            });
            holder.btn_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog(exportEnrActivity, recyclerView, category,finalUsers1,true);
                }
            });

        }

        @Override
        public int getItemCount() {
            return userGroups.size() + 1;
        }

    }



    /*
     * Displaying user Contacts
     */

    public class UserContactList extends RecyclerView.Adapter<UserContactList.MyViewHolder> {
        ExportEnrGroupActivity exportEnrActivity;
        public ArrayList<Category> categoryListArray;
        int Count = 0;
        private List<String> horizontalList;
        ArrayList<MyContacts> userContacts;
        RecyclerView recyclerView;
        Category category;
        boolean hidePlus;
        private CallbackInterface mCallback;

        public UserContactList(Category catgory, RecyclerView export_shelf, ExportEnrGroupActivity exportActivity, ArrayList<MyContacts> contacts,boolean hidePlusBtn) {
            exportEnrActivity = exportActivity;
            this.category = catgory;
            //  Book book = null;
            this.recyclerView = export_shelf;
            this.userContacts = contacts;
            this.hidePlus = hidePlusBtn;
            try{
                mCallback = (CallbackInterface) exportActivity;
            }catch(ClassCastException ex){
                //.. should log the error or throw and exception
                Log.e("MyAdapter","Must implement the CallbackInterface in the Activity", ex);
            }
        }

        // private List<Movie> moviesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            // public TextView title, year, genre;
            //public TextView txtView;
            ImageView bookImg;
            TextView tv_Title;
            RelativeLayout rl,rl_imaglayout;
            CheckBox cb_selectall;
            ImageView img_add;
            LinearLayout rl_layout;
            RelativeLayout layout_grpImages;
            Button btn_text,update;
            CircleButton btn_delete,btn_edit;
            //FrameLayout frame_arr_layout,btn_edit_layout;
            View onlineStatus;
            public MyViewHolder(View view) {
                super(view);
                rl = (RelativeLayout) view.findViewById(R.id.shelfbookLayout);
                ViewGroup.MarginLayoutParams margins = (ViewGroup.MarginLayoutParams) rl.getLayoutParams();
                margins.setMargins(0, 0, 0, 0);

                layout_grpImages = (RelativeLayout) view.findViewById(R.id.layout_add_grp);
                rl_layout = (LinearLayout) view.findViewById(R.id.relativeLayout2);
                rl_imaglayout = (RelativeLayout) view.findViewById(R.id.relativeLayout1);
                img_add = (ImageView) view.findViewById(R.id.img_add);
                bookImg = (ImageView) view.findViewById(R.id.imageView2);
                btn_delete= (CircleButton) view.findViewById(R.id.btn_delete);
                //progressBar = (TextCircularProgressBar) view.findViewById(R.id.circular_progress);
                // progressBar.setVisibility(View.INVISIBLE);
                tv_Title = (TextView) view.findViewById(R.id.textView1);
                btn_text = (Button) view.findViewById(R.id.btn_text);
                cb_selectall = (CheckBox) view.findViewById(R.id.cb_selectall);
                btn_edit = (CircleButton) view.findViewById(R.id.btn_edit);
                onlineStatus = (View) view.findViewById(R.id.btnisOnline);
                update = (Button) view.findViewById(R.id.edit);



            }
        }




        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shelf_enr_pages, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            //Enrichments enrich = enrPageList.get(position);
            //String page = exportEnrActivity.getResources().getString(R.string.page);
            MyContacts users = null;
            holder. onlineStatus.setVisibility(View.VISIBLE);
            if (position == 0 && !hidePlus) {
                holder.rl_imaglayout.setVisibility(View.INVISIBLE);
                holder.layout_grpImages.setVisibility(View.VISIBLE);
                holder.tv_Title.setText(exportEnrActivity.getResources().getString(R.string.create_contact_txt));
            } else {
                if (hidePlus) {
                    users = userContacts.get(position);
                } else {
                    users = userContacts.get(position - 1);
                }
                holder.rl_imaglayout.setVisibility(View.VISIBLE);
                holder.layout_grpImages.setVisibility(View.INVISIBLE);
                // holder.layout_grpImages.setVisibility(View.VISIBLE);
                holder.tv_Title.setText(users.getUserName());
                holder.bookImg.setBackgroundResource(R.drawable.single);
                holder.btn_text.setVisibility(View.INVISIBLE);
                holder.update.setVisibility(View.VISIBLE);
                if (users.isSelected()) {
                    holder.cb_selectall.setChecked(true);
                } else {
                    holder.cb_selectall.setChecked(false);
                }
                if (exportEnrActivity.enrichmentDetails == null && exportEnrActivity.currentBook == null && !exportEnrActivity.fromCloud) {
                    holder.cb_selectall.setVisibility(View.INVISIBLE);
                }
                try {
                    if (users.isOnline() != null) {
                        switch (users.isOnline()) {
                            case "1":
                                holder.onlineStatus.setBackgroundResource(R.drawable.online_status);
                                break;
                            case "0":
                                holder.onlineStatus.setBackgroundResource(R.drawable.offline_status);
                                break;
                            case "2":
                                holder.onlineStatus.setBackgroundResource(R.drawable.busy_status);
                                break;
                            default:
                                holder.onlineStatus.setBackgroundResource(R.drawable.offline_status);
                                break;
                        }
                    }
                }catch (Exception e){
                    Log.e("eee",e.toString());
                }
            }
            if(category.isHidden()){
                holder.btn_delete.setVisibility(View.VISIBLE);
                holder.btn_edit.setVisibility(View.VISIBLE);
            }else{
                holder.btn_delete.setVisibility(View.GONE);
                holder.btn_edit.setVisibility(View.GONE);
            }

            final MyContacts finalUsers = users;
            holder.cb_selectall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position != 0) {
                        if (finalUsers.isSelected()) {
                            finalUsers.setSelected(false);
                            if (exportEnrActivity.grpSelected) {
                                //MyContacts users = exportEnrActivity.selectGroup.getGroupsContactList().get(position - 1);
                               // users.setSelected(false);
                               // exportEnrActivity.selectedContact.remove(users);
                                for(int i=0;i<exportEnrActivity.groupList.size();i++){
                                    MyGroups grp=exportEnrActivity.groupList.get(i);
                                    if(grp.getId()==exportEnrActivity.selectGroup.getId()){
                                        selectingGroupsandContacts(exportEnrActivity,exportEnrActivity.groupList,i,false);
                                        break;
                                    }
                                }

                            } else {
                                MyContacts users = exportEnrActivity.myContacts.get(position - 1);
                                users.setSelected(false);
                                exportEnrActivity.selectedContact.remove(users);
                            }
                        } else {
                            finalUsers.setSelected(true);
                            if (exportEnrActivity.grpSelected) {
                                for(int i=0;i<exportEnrActivity.groupList.size();i++){
                                    MyGroups grp=exportEnrActivity.groupList.get(i);
                                    if(grp.getId()==exportEnrActivity.selectGroup.getId()){
                                        selectingGroupsandContacts(exportEnrActivity,exportEnrActivity.groupList,i,true);
                                        break;
                                    }
                                }
//                                MyContacts users = exportEnrActivity.selectGroup.getGroupsContactList().get(position - 1);
//                                users.setSelected(true);
//                                exportEnrActivity.selectedContact.add(users);
                            } else {
                                MyContacts users = exportEnrActivity.myContacts.get(position - 1);
                                users.setSelected(true);
                                exportEnrActivity.selectedContact.add(users);
                            }
                        }

                    }
                }
            });

            final MyContacts finalUsers1 = users;
            holder.bookImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(mCallback != null){
                        if(finalUsers!=null) {
                            try {
                                if (finalUsers.isOnline() != null) {
                                    switch (finalUsers.isOnline()) {
                                        case "1":

                                            mCallback.onHandleSelection(finalUsers);
                                            break;
                                        case "0":
                                            callMethod(exportEnrActivity,finalUsers);
                                            break;
                                        case "2":
                                            callMethod(exportEnrActivity,finalUsers);
                                            break;

                                    }
                                }
                            }catch (Exception e){
                                Log.e("eee",e.toString());
                            }




                        }
                        else{ mCallback.onHandleSelection(null);}
                        // ManahijApp.getInstance().getPrefManager().addContactTopictoPublish(finalUsers2.getEmailId());
                    }
                }
            });
            holder.update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    creatingUserContacts(exportEnrActivity, recyclerView, category,true, finalUsers1,hidePlus);
                }
            });
        holder.img_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position == 0) {
                        if (category.getCategoryId() == 0){
                            showAddContactsDialog(category,recyclerView);
                        }else{
                            creatingUserContacts(exportEnrActivity, recyclerView, category,false,null,false);
                        }
                    }
                 }
            });

            final MyContacts Users = users;
            holder.btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(exportEnrActivity);
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            exportEnrActivity.db.executeQuery("delete from tblUserContacts where userID='" + exportEnrActivity.email + "' and id='" + Users.getId() + "'");
                            exportEnrActivity.myContacts.remove(Users);
                            notifyDataSetChanged();
                            dialog.cancel();
                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.setTitle(R.string.delete);
                    builder.setMessage(R.string.suredelete);
                    builder.show();
                }
            });
            holder.btn_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    creatingUserContacts(exportEnrActivity, recyclerView, category,true, finalUsers1,false);
                }
            });
        }


        @Override
        public int getItemCount() {
            if (hidePlus){
                return userContacts.size();
            }else{
                return userContacts.size() + 1;
            }
        }
    }


    public void callMethod(final Context exportEnrActivity, final MyContacts users) {
        final Dialog dialog = new Dialog(exportEnrActivity);
        if(users.isOnline!=null) {
            dialog.setContentView(R.layout.call_user);
            dialog.setCancelable(false);
            RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.onlineStatus);
            LinearLayout layout2 = (LinearLayout) dialog.findViewById(R.id.callingStatus);
            TextView ok = (TextView) dialog.findViewById(R.id.ok);
            TextView status = (TextView) dialog.findViewById(R.id.status);

            if (users.isOnline().equals("0")) {
                status.setText("User is Offline");
                layout2.setVisibility(View.GONE);
                layout.setVisibility(View.VISIBLE);
            } else if (users.isOnline().equals("2")) {
                layout2.setVisibility(View.GONE);
                layout.setVisibility(View.VISIBLE);
                status.setText("User is Busy");
            }
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });



            dialog.show();
        }

    }
    /*
     * DialogWindow for Creating and Updating Contacts
     */
    public void creatingUserContacts(final ExportEnrGroupActivity exportEnrActivity, final RecyclerView recyclerView, final Category category, final boolean isEditMode, final MyContacts users,boolean hideUpdateBtn) {
        final Dialog exportEnrPopover = new Dialog(exportEnrActivity);
        exportEnrPopover.getWindow().setBackgroundDrawable(new ColorDrawable(exportEnrActivity.getResources().getColor(R.color.semi_transparent)));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        exportEnrPopover.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exportEnrPopover.setContentView(exportEnrActivity.getLayoutInflater().inflate(R.layout.contact_creation, null));
        exportEnrPopover.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.3), (int) exportEnrActivity.getResources().getDimension(R.dimen.contact_create_dialog));
        TextView txtNooorTitle=(TextView) exportEnrPopover.findViewById(R.id.txtNooorTitle);
        final EditText et_name = (EditText) exportEnrPopover.findViewById(R.id.et_name);
        final EditText et_emailId = (EditText) exportEnrPopover.findViewById(R.id.et_emailId);
        Button btn_add_email = (Button) exportEnrPopover.findViewById(R.id.btn_add);
        CircleButton btn_back=(CircleButton)exportEnrPopover.findViewById(R.id.btn_back);
        if(isEditMode && users!=null){
            et_name.setText(users.getUserName());
            et_emailId.setText(users.getEmailId());
            txtNooorTitle.setText(exportEnrActivity.getResources().getString(R.string.update_contact));
            btn_add_email.setText(exportEnrActivity.getResources().getString(R.string.update));
            ManahijApp.getInstance().getPrefManager().addContactTopictoPublish(users.getEmailId());
            if (hideUpdateBtn){
                btn_add_email.setVisibility(View.GONE);
            }
        }

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportEnrPopover.dismiss();
            }
        });

        btn_add_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = et_name.getText().toString();
                String emailid = et_emailId.getText().toString();
                if (!username.equals("") && !username.equals("")) {
                    if (exportEnrActivity.grpSelected &&category.getCategoryId()==0) {
                        boolean exist = false;
                        if (!UserFunctions.CheckValidEmail(emailid)) {
                            UserFunctions.DisplayAlertDialog(exportEnrActivity, R.string.input_valid_mail_id, R.string.invalid_email_id);
                        } else {
                            exist = checkEmailId(exportEnrActivity.selectGroup.getGroupsContactList(), emailid,users);
                            if(!exist) {
                                if(isEditMode && users!=null){
                                    exportEnrActivity.db.executeQuery("update tblUserContacts set userName='" + username + "', emailID ='" + emailid + "' where userID='" + exportEnrActivity.email + "' and  id='" + users.getId() + "'");
                                    users.setEmailId(emailid);
                                    users.setUserName(username);
                                }else {
                                    exportEnrActivity.db.executeQuery("Insert into tblUserContacts(userName,emailID,userID,groupsID,enabled) values('" + username + "','" + emailid + "','" + exportEnrActivity.email + "','" + exportEnrActivity.selectGroup.getId() + "','" + 1 + "')");
                                    int Id = exportEnrActivity.db.getMaxUniqueRowID("tblUserContacts");
                                    ManahijApp.getInstance().getPrefManager().addContactTopictoPublish(emailid);
                                    MyContacts newContact = new MyContacts(exportEnrActivity);
                                    newContact.setLoggedInUserId(exportEnrActivity.email);
                                    newContact.setEmailId(emailid);
                                    newContact.setEnabled(1);
                                    newContact.setUserName(username);
                                    newContact.setId(Id);
                                    newContact.setGroupsID(exportEnrActivity.selectGroup.getId());
                                    newContact.setOnline("0");
                                    newContact.setSelected(false);
                                    exportEnrActivity.selectGroup.getGroupsContactList().add(newContact);
                                }

                                Grp_SelectionAdapter enr = new Grp_SelectionAdapter(exportEnrActivity);
                                recyclerView.setAdapter(enr.new UserContactList(category, recyclerView, exportEnrActivity, exportEnrActivity.selectGroup.getGroupsContactList(),false));
                            }else{
                                UserFunctions.DisplayAlertDialog(exportEnrActivity, R.string.mail_id_exist, R.string.email);
                            }
                                //exportEnrActivity.userContacts.userContactList.add(newContact);
                        }
                    } else {
                        boolean exist = false;
                        if (!UserFunctions.CheckValidEmail(emailid)) {
                            UserFunctions.DisplayAlertDialog(exportEnrActivity, R.string.input_valid_mail_id, R.string.invalid_email_id);
                        } else {
                            exist = checkEmailId(exportEnrActivity.myContacts, emailid,users);
                            if (exist) {
                                UserFunctions.DisplayAlertDialog(exportEnrActivity, R.string.mail_id_exist, R.string.email);
                            } else {
                                if(isEditMode && users!=null){
                                    exportEnrActivity.db.executeQuery("update tblUserContacts set userName='" + username + "', emailID ='" + emailid + "' where userID='" + exportEnrActivity.email + "' and  id='" + users.getId() + "'");
                                    users.setEmailId(emailid);
                                    users.setUserName(username);
                                }else {
                                    //"insert into groupBooks(groupName, categoryID) values('"+grpName+"', '" + fromBook.getCategoryId() + "')";
                                    exportEnrActivity.db.executeQuery("Insert into tblUserContacts(userName,emailID,userID,groupsID,enabled) values('" + username + "','" + emailid + "','" + exportEnrActivity.email + "','" + 0 + "','" + 1 + "')");
                                    int Id = exportEnrActivity.db.getMaxUniqueRowID("tblUserContacts");
                                    MyContacts newContact = new MyContacts(exportEnrActivity);
                                    newContact.setLoggedInUserId(exportEnrActivity.email);
                                    newContact.setEmailId(emailid);
                                    newContact.setEnabled(1);
                                    newContact.setUserName(username);
                                    newContact.setId(Id);
                                    newContact.setGroupsID(0);
                                    newContact.setSelected(false);
                                    newContact.setOnline("0");
                                    // exportEnrActivity.userContacts.userContactList.add(newContact);
                                    exportEnrActivity.myContacts.add(newContact);
                                }
                            }
                        }
                    }
                    recyclerView.getAdapter().notifyDataSetChanged();

                }
                exportEnrPopover.dismiss();
            }
        });
        exportEnrPopover.show();


    }


  /*  public void creatingGroup(final ExportEnrGroupActivity exportEnrActivity, final RecyclerView recyclerView, final Category category) {
        final Dialog exportEnrPopover = new Dialog(exportEnrActivity);
        exportEnrPopover.getWindow().setBackgroundDrawable(new ColorDrawable(exportEnrActivity.getResources().getColor(R.color.semi_transparent)));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        exportEnrPopover.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exportEnrPopover.setContentView(exportEnrActivity.getLayoutInflater().inflate(R.layout.grp_dialog, null));
        exportEnrPopover.getWindow().setLayout((int) exportEnrActivity.getResources().getDimension(R.dimen.grp_create_dialog_width),(int) exportEnrActivity.getResources().getDimension(R.dimen.grp_create_dialog));
        exportEnrPopover.setCanceledOnTouchOutside(false);
        final EditText et_name = (EditText) exportEnrPopover.findViewById(R.id.et_grpname);
        final Button btn_ok = (Button) exportEnrPopover.findViewById(R.id.btn_ok);
        Button btn_cancel = (Button) exportEnrPopover.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportEnrPopover.dismiss();
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String grpname = et_name.getText().toString();
                boolean exist = false;
                if (!grpname.equals("") && !grpname.equals("") && grpname.length() > 0) {
                    //"insert into groupBooks(groupName, categoryID) values('"+grpName+"', '" + fromBook.getCategoryId() + "')";
                    for (MyGroups newContact : exportEnrActivity.groupList) {
                        String group = newContact.getGroupName();
                        if (grpname.equals(group)) {
                            exist = true;
                            break;
                        }
                    }
                    if (exist) {
                        UserFunctions.DisplayAlertDialog(exportEnrActivity, R.string.group_exist, R.string.failed_add_group);
                    } else {
                        exportEnrActivity.db.executeQuery("Insert into tblUserGroups(name,userID,enabled) values('" + grpname + "','" + exportEnrActivity.email + "','" + 1 + "')");
                        int Id = exportEnrActivity.db.getMaxUniqueRowID("tblUserGroups");
                        MyGroups newContact = new MyGroups(exportEnrActivity);
                        newContact.setLoggedInUserId(exportEnrActivity.email);
                        newContact.setId(Id);
                        newContact.setGroupName(grpname);
                        newContact.setEnabled(1);
                        newContact.setGroupSelected(false);
                        exportEnrActivity.userContacts.groupList.add(newContact);
                        //exportEnrActivity.groupList.add(newContact);
                        Grp_SelectionAdapter enr = new Grp_SelectionAdapter(exportEnrActivity);
                        recyclerView.setAdapter(enr.new UserGroupsList(category, recyclerView, exportEnrActivity, exportEnrActivity.groupList));
                        exportEnrPopover.dismiss();
                    }
                    //recyclerView.getAdapter().notifyDataSetChanged();

                }

            }
        });
        exportEnrPopover.show();


    }  */

    /*
     * Alert Dialog for Creating Group
     */
    public void alertDialog(final ExportEnrGroupActivity exportEnrActivity, final RecyclerView recyclerView, final Category category, final MyGroups finalUsers1, final boolean isEditmode){
        AlertDialog.Builder builder = new AlertDialog.Builder(exportEnrActivity);
        builder.setTitle(exportEnrActivity.getResources().getString(R.string.group));
        if(isEditmode &&finalUsers1!=null){
            builder.setMessage(exportEnrActivity.getResources().getString(R.string.update_grp_name));
        }else {
            builder.setMessage(exportEnrActivity.getResources().getString(R.string.grp_name));
        }
        builder.setCancelable(false);
        LayoutInflater inflator =(exportEnrActivity).getLayoutInflater();
        View dialogView=inflator.inflate(R.layout.grpdialogwindow,null);
        builder.setView(dialogView);
        final EditText et_name = (EditText)dialogView.findViewById(R.id.et_grpname);
        if(isEditmode &&finalUsers1!=null){
            et_name.setText(finalUsers1.getGroupName());
        }
        builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String grpname = et_name.getText().toString();
                boolean exist = false;
                if (!grpname.equals("") && !grpname.equals("") && grpname.length() > 0) {
                    //"insert into groupBooks(groupName, categoryID) values('"+grpName+"', '" + fromBook.getCategoryId() + "')";
                    for (MyGroups newContact : exportEnrActivity.groupList) {
                        String group = newContact.getGroupName();
                        if(finalUsers1!=null && isEditmode ){
                            if(finalUsers1.getId()!=newContact.getId()) {
                                if (grpname.equals(group)) {
                                    exist = true;
                                    break;
                                }
                            }
                        }else{
                            if (grpname.equals(group)) {
                                exist = true;
                                break;
                            }
                        }
                    }
                    if (exist) {
                        UserFunctions.DisplayAlertDialog(exportEnrActivity, R.string.group_exist, R.string.failed_add_group);
                    } else {
                        if(finalUsers1!=null && isEditmode){
                            exportEnrActivity.db.executeQuery("update tblUserGroups set name='" + grpname + "' where userID='" + exportEnrActivity.email + "' and  id='" + finalUsers1.getId() + "'");
                            finalUsers1.setGroupName(grpname);
                           // recyclerView.setAdapter(enr.new UserGroupsList(category, recyclerView, exportEnrActivity, exportEnrActivity.groupList));
                        }else {
                            exportEnrActivity.db.executeQuery("Insert into tblUserGroups(name,userID,enabled) values('" + grpname + "','" + exportEnrActivity.email + "','" + 1 + "')");
                            int Id = exportEnrActivity.db.getMaxUniqueRowID("tblUserGroups");
                            MyGroups newContact = new MyGroups(exportEnrActivity);
                            newContact.setLoggedInUserId(exportEnrActivity.email);
                            newContact.setId(Id);
                            newContact.setGroupName(grpname);
                            newContact.setEnabled(1);
                            newContact.setGroupSelected(false);
                            exportEnrActivity.userContacts.groupList.add(newContact);
                            //exportEnrActivity.groupList.add(newContact);

                        }
                        Grp_SelectionAdapter enr = new Grp_SelectionAdapter(exportEnrActivity);
                        recyclerView.setAdapter(enr.new UserGroupsList(category, recyclerView, exportEnrActivity, exportEnrActivity.groupList));
                        dialog.cancel();
                    }
                    //recyclerView.getAdapter().notifyDataSetChanged();

                }
            }
        });
        builder.setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();

       // Dialog dialog=builder.create();
        //dialog.getWindow().setBackgroundDrawable(R.color.bookview_bluecolor);
        alert.show();
    }

   /*
    * Checking mail ID
    */
    public boolean checkEmailId(ArrayList<MyContacts> myContacts, String emailid, MyContacts users) {
        boolean exist = false;
        for (MyContacts checkmail : myContacts) {
            String mail = checkmail.getEmailId();
            if(users!=null) {
                if(users.getId()!=checkmail.getId()) {
                    if (emailid.equals(mail)) {
                        exist = true;
                        break;
                    }
                }
            }else{
                if (emailid.equals(mail)) {
                    exist = true;
                    break;
                }
            }
        }
        return exist;
    }

   /*
    * selecting GroupsandContacts
    */
    public void selectingGroupsandContacts(ExportEnrGroupActivity exportEnrActivity, ArrayList<MyGroups> groupList, int position, boolean b){
        MyGroups users=groupList.get(position);
        users.setGroupSelected(b);
        for(MyContacts contacts:groupList.get(position).getGroupsContactList()){
            contacts.setSelected(b);
            if(b){
                boolean exist=false;
                for(MyContacts contact:exportEnrActivity.selectedContact){
                  if(contact.getId()==contacts.getId()){
                      exist=true;
                      break;
                  }
                }
                if(!exist){
                    exportEnrActivity.selectedContact.add(contacts);
                }
            }else{
               // boolean exist=false;
                exportEnrActivity.selectedContact.remove(contacts);
            }
        }
    }

    public void exportEnrichmentstoGroups(){
        String mailid = null;
        boolean first=false;
        for(int i=0;i<exportEnrGroup.selectedContact.size();i++){
            MyContacts getemail=exportEnrGroup.selectedContact.get(i);
            if(i==0){
                mailid=getemail.getEmailId()+",";
                first=true;
            }else{
                if(i==exportEnrGroup.selectedContact.size()-1){
                    mailid=mailid.concat(getemail.getEmailId());
                }else {
                    mailid = mailid.concat(getemail.getEmailId() + ",");
                }
            }
        }
    }

    private void showAddContactsDialog(final Category category, final RecyclerView contactsRecycler){
        final Dialog addNewContactDialog = new Dialog(exportEnrGroup);
        addNewContactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addNewContactDialog.setContentView(exportEnrGroup.getLayoutInflater().inflate(R.layout.dialog_add_contacts, null));
        addNewContactDialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.2), (int)(Globals.getDeviceHeight()/1.5)); //1.5
        my_contacts = true;
        final RecyclerView recyclerView = (RecyclerView) addNewContactDialog.findViewById(R.id.gridView1);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(exportEnrGroup, LinearLayoutManager.VERTICAL, false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setAdapter(new ContactsAdapter());
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        final SegmentedRadioButton segmentedRadioGroup = (SegmentedRadioButton)addNewContactDialog.findViewById(R.id.segment_text);
        segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.my_contacts) {
                    my_contacts = true;
                    recyclerView.getAdapter().notifyDataSetChanged();
                } else if (checkedId == R.id.gmail_contacts) {
                    my_contacts = false;
                    recyclerView.getAdapter().notifyDataSetChanged();
                }
            }
        });
        CircleButton back_btn = (CircleButton) addNewContactDialog.findViewById(R.id.btnback);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewContactDialog.dismiss();
            }
        });
        Button add_btn = (Button) addNewContactDialog.findViewById(R.id.btnNext);
        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean exist;
                if (selectedContacts.size()>0) {
                    for (int i = 0; i < selectedContacts.size(); i++) {
                        MyContacts contacts = selectedContacts.get(i);
                        String emailid = contacts.getEmailId();
                        String username = contacts.getUserName();
                        exist = checkEmailId(exportEnrGroup.selectGroup.getGroupsContactList(), emailid, contacts);
                        if (!exist) {
                            exportEnrGroup.db.executeQuery("Insert into tblUserContacts(userName,emailID,userID,groupsID,enabled) values('" + username + "','" + emailid + "','" + exportEnrGroup.email + "','" + exportEnrGroup.selectGroup.getId() + "','" + 1 + "')");
                            int Id = exportEnrGroup.db.getMaxUniqueRowID("tblUserContacts");
                            MyContacts newContact = new MyContacts(exportEnrGroup);
                            newContact.setLoggedInUserId(exportEnrGroup.email);
                            newContact.setEmailId(emailid);
                            newContact.setEnabled(1);
                            newContact.setUserName(username);
                            newContact.setId(Id);
                            newContact.setGroupsID(exportEnrGroup.selectGroup.getId());
                            newContact.setSelected(false);
                            exportEnrGroup.selectGroup.getGroupsContactList().add(newContact);
                            Grp_SelectionAdapter enr = new Grp_SelectionAdapter(exportEnrGroup);
                            contactsRecycler.setAdapter(enr.new UserContactList(category, contactsRecycler, exportEnrGroup, exportEnrGroup.selectGroup.getGroupsContactList(), false));
                        } else {
                            //  UserFunctions.DisplayAlertDialog(exportEnrGroup, R.string.mail_id_exist, R.string.email);
                        }
                    }
                    addNewContactDialog.dismiss();
                }
            }
        });
        addNewContactDialog.show();
    }

    public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder> {



        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView title_txt;
            TextView email_txt;
            CheckBox checkBox;
            RelativeLayout checklist_layout;

            public MyViewHolder(View view) {
                super(view);
                checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
                title_txt = (TextView) view.findViewById(R.id.txt_username);
                email_txt = (TextView) view.findViewById(R.id.txt_mail);
                checklist_layout = (RelativeLayout) view.findViewById(R.id.checklist_layout);
            }
        }


        public ContactsAdapter() {

        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.contacts_checkbox_img, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            final MyContacts users;
            if (my_contacts) {
               users = exportEnrGroup.myContacts.get(position);
            }else{
                users = exportEnrGroup.gmailContacts.get(position);
            }
            if (checkSelected(users)){
                holder.checkBox.setChecked(true);
            }else{
                holder.checkBox.setChecked(false);
            }
            holder.title_txt.setText(users.getUserName());
            holder.email_txt.setText(users.getEmailId());
            holder.checklist_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.checkBox.isChecked()){
                        selectedContacts.remove(users);
                        holder.checkBox.setChecked(false);
                    }else{
                        selectedContacts.add(users);
                        holder.checkBox.setChecked(true);
                    }
                }
            });


        }

        @Override
        public int getItemCount() {
            if (my_contacts) {
                return exportEnrGroup.myContacts.size();
            }else{
                return exportEnrGroup.gmailContacts.size();
            }
        }
    }

    private boolean checkSelected(MyContacts contacts) {
        for (int i = 0; i < selectedContacts.size(); i++) {
            MyContacts user = selectedContacts.get(i);
            if (contacts.getEmailId().equalsIgnoreCase(user.getEmailId())) {
                return true;
            }

        }
        return false;
    }
}










