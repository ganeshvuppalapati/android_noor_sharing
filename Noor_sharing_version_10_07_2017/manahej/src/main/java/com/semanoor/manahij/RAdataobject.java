package com.semanoor.manahij;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by karthik on 18-09-2015.
 */
public class RAdataobject implements Serializable {
    private String name;
    private String path;
    private String pageId;
    private String type;
    private int Nodetype;
    private int indexNo;
    private ArrayList<RAdataobject> childobjlist=new ArrayList<RAdataobject>();


    public RAdataobject(String name, String path, int nodeType){
        this.name = name;
        this.path = path;
        this.Nodetype = nodeType;
        if (!this.path.equals("")) {
            this.pageId = path.substring(0, path.lastIndexOf('.'));
            this.pageId = pageId+name;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<RAdataobject> getChildobjlist() {
        return childobjlist;
    }

    public void setChildobjlist(ArrayList<RAdataobject> childobjlist) {
        this.childobjlist = childobjlist;
    }

    public int getNodetype() {
        return Nodetype;
    }

    public void setNodetype(int nodetype) {
        Nodetype = nodetype;
    }

    public int getIndexNo() {
        return indexNo;
    }

    public void setIndexNo(int indexNo) {
        this.indexNo = indexNo;
    }
}
