package com.semanoor.manahij;

import android.support.v7.widget.RecyclerView;

import com.semanoor.source_sboookauthor.Book;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by karthik on 15-09-2016.
 */
public class GroupBooks implements Serializable {
    private int categoryId;
    private String groupName;
    private int groupId;
    private int  bookId;
    private int position;
    private String IsDownloadCompleted;
    private Book book;
    private RecyclerView.ViewHolder viewHolder;

    private ArrayList<GroupBooks> childobjlist=new ArrayList<GroupBooks>();

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public ArrayList<GroupBooks> getChildobjlist() {
        return childobjlist;
    }

    public void setChildobjlist(ArrayList<GroupBooks> childobjlist) {
        this.childobjlist = childobjlist;
    }



    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getIsDownloadCompleted() {
        return IsDownloadCompleted;
    }

    public void setIsDownloadCompleted(String isDownloadCompleted) {
        IsDownloadCompleted = isDownloadCompleted;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public RecyclerView.ViewHolder getViewHolder() {
        return viewHolder;
    }

    public void setViewHolder(RecyclerView.ViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
}

