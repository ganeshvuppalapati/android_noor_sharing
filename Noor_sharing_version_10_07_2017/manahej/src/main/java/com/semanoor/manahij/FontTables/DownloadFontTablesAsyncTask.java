package com.semanoor.manahij.FontTables;

import android.os.AsyncTask;
import android.util.Log;

import com.semanoor.source_sboookauthor.Globals;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

import static android.content.ContentValues.TAG;

/**
 * Created by Mahaboob on 4/17/2017.
 */

//Downloading font files based on URL and storing in specified path
public class DownloadFontTablesAsyncTask extends AsyncTask<InputStream, Void, Boolean> {
    //final String appDirectoryName = "/NOOOOOOOR";

   final String appDirectoryName = Globals.TARGET_BASE_FONT_TABLES_PTH;
   // final String appDirectoryName = Environment.getExternalStorageDirectory() + "/NOOR";
    String fileName;
    File imageRoot;
    String jsonData;

    public DownloadFontTablesAsyncTask(String fileName) {
        this.fileName = fileName;
    }

    @Override
    protected Boolean doInBackground(InputStream... params) {
        InputStream inputStream = params[0];
        imageRoot = new File(appDirectoryName);

        try {
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            jsonData = convertUnicodeKeysToUpperCase(result.toString());


        } catch (Exception e) {

        }

        if (!imageRoot.exists())
            imageRoot.mkdir();
        File file = new File(imageRoot, fileName + ".json");
        OutputStream output = null;
        BufferedWriter writer = null;
        try {
            /*output = new FileOutputStream(file);
            byte[] buffer = new byte[1024]; // or other buffer size
            int read;
            while ((read = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, read);
            }
            output.flush();*/
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(jsonData);



        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (output != null) {
                    output.close();
                } else {
                    Log.d(TAG, "Output stream is null");
                }
                if(writer !=null){
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);


    }


    String subString;
    JSONObject finalJson = new JSONObject();
    JSONObject newJsonObj = new JSONObject();
    public String convertUnicodeKeysToUpperCase(String _unicodes) {
        try {
            JSONObject innerJsonObject;
            JSONObject fonttable = new JSONObject(_unicodes);

            Iterator iterator = fonttable.keys();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                JSONObject value = fonttable.getJSONObject(key);
                String uniCodes = value.toString();


                innerJsonObject = new JSONObject(uniCodes);
                Iterator innerJsonIterator = innerJsonObject.keys();

                while (innerJsonIterator.hasNext()) {
                    String unicodeKey = (String) innerJsonIterator.next();
                    String uniCodevalue = innerJsonObject.getString(unicodeKey);

                    subString = unicodeKey.toUpperCase();
                    subString = subString.replaceAll("X","x");
                   // Log.d("Values......", subString);
                    newJsonObj.putOpt(subString, uniCodevalue);
                }
                finalJson.put(key, newJsonObj);


            }

        } catch (Exception e) {
            Log.e("exception", e.toString());
        }

     //   Log.d("Unicodes...", "" + finalJson.toString());

        return finalJson.toString();
    }


}
