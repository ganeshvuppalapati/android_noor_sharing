package com.semanoor.manahij;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.OldDatabaseHandler;
import com.semanoor.source_sboookauthor.UserFunctions;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;

public class VersionUpdateDB {

	private Context context;
	private OldDatabaseHandler oldDb;
	private DatabaseHandler db;
	private UserFunctions userfunctions;
	public VersionUpdateDB(Context context, DatabaseHandler db, OldDatabaseHandler oldDb, UserFunctions userfn){
		this.context = context;
		this.db = db;
		this.oldDb = oldDb;
		this.userfunctions=userfn;
		updateOldDbElementsToNewDb();
	}

	public void updateOldDbElementsToNewDb(){
		ArrayList<String> oldDbbookList = oldDb.getAllBookContent();
		for (int i = 0; i < oldDbbookList.size(); i++) {
			String bookStr = oldDbbookList.get(i);
			String[] bookSplitStr = bookStr.split("\\|");
			String storeID =bookSplitStr[0];
			int totalPages =Integer.parseInt(bookSplitStr[1]); 
			int CID =Integer.parseInt(bookSplitStr[2]);
			int lastViewedPage=Integer.parseInt(bookSplitStr[3]);
			String title =bookSplitStr[4];
			String bLanguage =bookSplitStr[5];

			File sourceLocation = new File(Globals.TARGET_BASE_FILE_PATH+"."+storeID+"Book");
			char bookname=storeID.charAt(0);
			//File targetLocation;
			String book;
			if(!(storeID.charAt(0)=='M')){
				 book="M".concat(storeID);
				// targetLocation=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+book+"Book");
			}else{
				 book=storeID;
			    // targetLocation=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+book+"Book");
			}
			File targetLocation=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+book+"Book");
			if(!targetLocation.exists()){
				try {
					userfunctions.copyDirectory(sourceLocation,targetLocation);
					//coverImageForStoreBook(global.TARGET_BASE_BOOKS_DIR_PATH+storeID+"Book/Card.gif",Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID()+"/FreeFiles");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			//Book newBook =Shelf.addNewStoreBook(title,"description","Semanoor",6, totalPages, 0, 0, true, lastViewedPage, storeID, CID, "yes","yes", "yes", "yes", "yes","yes", "yes", "yes", "yes", "yes", "yes", "yes", "0","yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes",bLanguage, "yes", "yes", "0");
			db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,storeID,ShowPageNo,CID,Search,Bookmark,Copy,Flip,Navigation,Highlight,Note,GotoPage,IndexPage,Zoom,WebSearch,WikiSearch,UpdateCounts,google,youtube,semaBook,semaContent,yahoo,bing,ask,jazeera,blanguage,TranslationSearch,dictionarySearch,booksearch,StoreBookCreatedFromIpad) values('"+title+"','description','Semanoor','6','"+totalPages+"','0','0','1','true','"+lastViewedPage+"','"+book+"','false','"+CID+"','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','1','yes','yes','yes','yes','yes','yes','yes','yes','"+bLanguage+"','yes','yes','yes','"+"no"+"')");
			int lastBookId = db.getMaxUniqueRowID("books");
			UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH+lastBookId+"/FreeFiles");
			UserFunctions.makeFileWorldReadable(Globals.TARGET_BASE_BOOKS_DIR_PATH+lastBookId);
			UserFunctions.createNewDirectory(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+lastBookId);
			UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH+lastBookId+"/tempFiles");
			generatingImagesForstorebook(lastBookId,book);

			ArrayList<String> bookMark;
			String Bname=storeID;
			bookMark=oldDb.getAllBookMarkCount(storeID);
			for(int j=0;j<bookMark.size();j++){
				String currentBMark=bookMark.get(j);
				String Bmark[]=currentBMark.split("\\|");
				//int  BMID = Integer.parseInt(Bmark[0]);
				//String BName = storeID;
				int pageNo = Integer.parseInt(Bmark[1]);
				int TagId = Integer.parseInt(Bmark[2]);
				int TabNo = Integer.parseInt(Bmark[3]);
				if(!(storeID.charAt(0)=='M')){
				   Bname="M".concat(storeID);
				}
				String query = "insert into tblBookMark(BName,PageNo,TagID,TabNo) values('"+Bname+"','"+pageNo+"','0','0')";
				db.executeQuery(query);
			}
			Object[] notes=oldDb.loadNoteDivToArray(storeID);
			String[] PageNo = (String[]) notes[0];
			String[] SText = (String[]) notes[1];
			String[] TabNo = (String[]) notes[4];
			String[] Color = (String[]) notes[3];
			String[] SDesc = (String[]) notes[2];
			Integer[] Occurence =(Integer[]) notes[5];
			Integer[] NPos =(Integer[]) notes[6];
			String[] ProcessText = (String[]) notes[7];

			for(int tag =0; tag<SText.length; tag++){
				int pageNo = Integer.parseInt(PageNo[tag]); 
				String Stext=SText[tag];
				int Ocurence = Occurence[tag];
				String Sdesc=SDesc[tag];
				int Npos=NPos[tag];
				int Colour = Integer.parseInt(Color[tag]);
				String Processtext=ProcessText[tag];
				int tabNo = Integer.parseInt(TabNo[tag]); 
				if(!(storeID.charAt(0)=='M')){
					 Bname="M".concat(storeID);
				}
				String query = "insert into tblNote(BName,PageNo,SText,Occurence,SDesc,NPos,Color,ProcessSText,TabNo,Exported) values" +
						"('"+Bname+"','"+pageNo+"','"+Stext+"','"+Ocurence+"','"+Sdesc+"','"+Npos+"','"+Colour+"','"+Processtext+"','"+tabNo+"','"+"0"+"')";
				db.executeQuery(query);
			}
			Object[] highLights=oldDb.getallhighlights(storeID);
			String[] highlight_PageNo = (String[]) highLights[0];
			String[] highlight_SText = (String[]) highLights[1];
			Integer[] highlight_Occurence =(Integer[]) highLights[2];
			String[] highlight_TabNo = (String[]) highLights[3];

			for(int tag =0; tag<highlight_SText.length; tag++){
				int pageNo = Integer.parseInt(highlight_PageNo[tag]); 
				String Stext=highlight_SText[tag];
				int Ocurence = highlight_Occurence[tag];
				int tabNo = Integer.parseInt(highlight_TabNo[tag]); 
				if(!(storeID.charAt(0)=='M')){
					Bname="M".concat(storeID);
				}
				String query = "insert into tblHighlight(BName,PageNo,SText,Occurence,Color,TabNo) values('"+Bname+"', '"+pageNo+"', '"+Stext+"', '"+Ocurence+"', '', '"+tabNo+"')";
				db.executeQuery(query);
			}
		}
		Globals.olddb=true;
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(Globals.oldDatabase, Globals.olddb);
		editor.commit();
	}

	public void generatingImagesForstorebook(int uniqueId, String storeBookId){
		if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.gif").exists()){
			coverImageForStoreBook(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.gif",Globals.TARGET_BASE_BOOKS_DIR_PATH+uniqueId+"/FreeFiles/");
			frontThumbImageForStoreBook(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.gif",Globals.TARGET_BASE_BOOKS_DIR_PATH+uniqueId+"/FreeFiles/");
			frontImageForStoreBook(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.gif",Globals.TARGET_BASE_BOOKS_DIR_PATH+uniqueId+"/FreeFiles/");
		}
		else if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.png").exists()){
			coverImageForStoreBook(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.png",Globals.TARGET_BASE_BOOKS_DIR_PATH+uniqueId+"/FreeFiles/");
			frontThumbImageForStoreBook(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.png",Globals.TARGET_BASE_BOOKS_DIR_PATH+uniqueId+"/FreeFiles/");
			frontImageForStoreBook(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.png",Globals.TARGET_BASE_BOOKS_DIR_PATH+uniqueId+"/FreeFiles/");
		}
		else  if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.jpg").exists()){
			coverImageForStoreBook(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.jpg",Globals.TARGET_BASE_BOOKS_DIR_PATH+uniqueId+"/FreeFiles/");
			frontThumbImageForStoreBook(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.jpg",Globals.TARGET_BASE_BOOKS_DIR_PATH+uniqueId+"/FreeFiles/");
			frontImageForStoreBook(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/Card.jpg",Globals.TARGET_BASE_BOOKS_DIR_PATH+uniqueId+"/FreeFiles/");
		}
		if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/fpimages").exists()){
			copyingScreenShots(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/fpimages",Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+uniqueId);
		}
		if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/FreeFiles").exists()){
			userfunctions.createNewBooksDirAndCoptTemplates(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"Book/FreeFiles/",Globals.TARGET_BASE_BOOKS_DIR_PATH+uniqueId+"/FreeFiles/");
		}
	}

	public void frontImageForStoreBook(String bookImagePath,String freeFilesPath ){
		Bitmap bitmap = BitmapFactory.decodeFile(bookImagePath);
		String front_port_path = freeFilesPath+"front_P.png";
		if (!new File(front_port_path).exists()) {
			UserFunctions.saveBitmapImage(bitmap, front_port_path);
		}
	}

	public void frontThumbImageForStoreBook(String bookImagePath,String freeFilesPath ){
		Bitmap bitmap = BitmapFactory.decodeFile(bookImagePath);
		String front_thumb_path = freeFilesPath+"frontThumb.png";
		if (!new File(front_thumb_path).exists()) {
			Bitmap frontThumbBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(130, context), Globals.getDeviceIndependentPixels(170, context), true);
			UserFunctions.saveBitmapImage(frontThumbBitmap, front_thumb_path);
			String thumb_path = freeFilesPath+"thumb.png";
			UserFunctions.saveBitmapImage(frontThumbBitmap, thumb_path);
		}
	}

	public void coverImageForStoreBook(String bookImagePath,String freeFilesPath ){
		Bitmap bitmap = BitmapFactory.decodeFile(bookImagePath);
		String card_path = freeFilesPath+"card.png";
		if (!new File(card_path).exists()) {
			Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(200, context), Globals.getDeviceIndependentPixels(300, context), true);
			UserFunctions.saveBitmapImage(cardBitmap, card_path);
		}
	}

	public void copyingScreenShots(String srcPath, String destPath){
		File srcDir = new File(srcPath);
		File destDir = new File(destPath);
		if (!destDir.exists()) {
			destDir.mkdirs();
		}
		String[] path = srcDir.list();
		for(String file : path){
			String fromPath = srcDir+"/"+file;
			file=file.replace(".jpg", ".png");
			String toCopyPath = destDir+"/"+"thumbnail_"+file;
			Bitmap bitmap = BitmapFactory.decodeFile(fromPath);
			Bitmap frontThumbBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(130, context), Globals.getDeviceIndependentPixels(170, context), true);
			UserFunctions.saveBitmapImage(frontThumbBitmap, toCopyPath);
		}
	}
}
