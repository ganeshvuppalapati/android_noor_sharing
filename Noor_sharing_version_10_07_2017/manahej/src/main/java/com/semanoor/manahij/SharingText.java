package com.semanoor.manahij;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.WebDialog;
import com.semanoor.source_sboookauthor.PopoverView;

/**
 * Created by Krishna on 08-10-2015.
 */
public class SharingText {
    private Elesson elesson_Activity;
    private String[] share = {"Facebook", "Twitter"};
    PopoverView sharePopUp;
    LoginButton authButton;
    private GraphUser user;

    private boolean isFbSessionActive;
    Session session;
    boolean actionPerformed;


    public SharingText(Elesson elesson) {
        elesson_Activity = elesson;
    }

    public void showShareDialogWindow() {
        sharePopUp = new PopoverView(elesson_Activity, R.layout.elesson_advsearch);
        sharePopUp.setContentSizeForViewInPopover(new Point((int) elesson_Activity.getResources().getDimension(R.dimen.elesson_share_dialog_width), (int) elesson_Activity.getResources().getDimension(R.dimen.elesson_share_dialog_height)));
        sharePopUp.showPopoverFromRectInViewGroup(elesson_Activity.rootView, PopoverView.getFrameForView(elesson_Activity.btnShare), PopoverView.PopoverArrowDirectionUp, true);
        ListView advListView = (ListView) sharePopUp.findViewById(R.id.listViewAdv);
        RelativeLayout search_layout = (RelativeLayout) sharePopUp.findViewById(R.id.search_Layout);
        search_layout.setVisibility(View.GONE);
     //   advListView.setBackgroundColor(Color.parseColor("#367EAF"));
        advListView.setDividerHeight(2);
        advListView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1f));
        advListView.setAdapter(new shareAdapter(elesson_Activity, "Download E-Lesson" + elesson_Activity.elessonBookTitle));

        advListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v, final int position, long l) {
               /* session = Session.getActiveSession();
                if (session != null && session.isOpened()) {
                    isFbSessionActive = true;
                    Request.newMeRequest(session, new Request.GraphUserCallback() {

                        @Override
                        public void onCompleted(GraphUser _user, Response response) {
                            user = _user;
                            if (user != null && position == 0 && !actionPerformed) {
                                elesson_Activity.fb_userName = user.getName();
                                publishFeedDialog(elesson_Activity.fb_userName, "Download E-Lesson " + elesson_Activity.elessonBookTitle);
                            }
                        }
                    }).executeAsync();
                } else {
                    isFbSessionActive = false;
                    elesson_Activity.fb_userName = null;
                }
                if (position == 0) {
                    if (isFbSessionActive) {
                        if (elesson_Activity.fb_userName != null) {
                            publishFeedDialog(elesson_Activity.fb_userName, "Download E-Lesson " + elesson_Activity.elessonBookTitle);
                        }
                    } else {
                        authButton.performClick();
                        isFbSessionActive = false;
                    }
                } else {
                    actionPerformed = false;
                    TwitterDialog twitterDialog = new TwitterDialog(elesson_Activity);
                    twitterDialog.loginToTwitter("Download E-Lesson" + elesson_Activity.elessonBookTitle);
                    //new twitterLogin().execute();
                }*/
            }
        });
    }


    private class shareAdapter extends BaseAdapter {


        private String selectedText;
        private GraphUser user;
        Session session1;
        boolean isLoggedIn;

        public Integer[] mThumbIds = {
                R.drawable.elesson_facebook, R.drawable.elesson_twitter
        };

        public shareAdapter(Elesson elesson, String selectedText) {
            this.selectedText = selectedText;

            session1 = Session.getActiveSession();
            if (session1 != null && session1.isOpened()) {
                isFbSessionActive = true;
                Request.newMeRequest(session1, new Request.GraphUserCallback() {

                    @Override
                    public void onCompleted(GraphUser _user, Response response) {
                        user = _user;
                        if (user != null && !actionPerformed) {
                            elesson_Activity.fb_userName = user.getName();
                           // publishFeedDialog(elesson_Activity.fb_userName, "Download E-Lesson " + elesson_Activity.elessonBookTitle);
                        }
                    }
                }).executeAsync();
            } else {
                isFbSessionActive = false;
                elesson_Activity.fb_userName = null;
            }
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(elesson_Activity);
            isLoggedIn = prefs.getBoolean("is_twitter_loggedin", false);

        }

        @Override
        public int getCount() {
            return share.length;
        }

        @Override
        public Object getItem(int position) {

            return null;
        }

        @Override
        public long getItemId(int position) {

            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            if (convertView == null) {
                vi = elesson_Activity.getLayoutInflater().inflate(R.layout.elesson_share, null);
            }

            LinearLayout fblayout = (LinearLayout) vi.findViewById(R.id.fb_layout);
            fblayout.setVisibility(View.GONE);
          //  TextView txtSearchItem = (TextView) vi.findViewById(R.id.textViewAdv);
           // txtSearchItem.setVisibility(View.VISIBLE);
            ImageView imgSearchImg = (ImageView) vi.findViewById(R.id.imageViewAdv);
            authButton = (LoginButton) vi.findViewById(R.id.authButton);
            authButton.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
//            LinearLayout twitter_layout =(LinearLayout)vi.findViewById(R.id.twitter_layout);
            Button btn_textName=(Button)vi.findViewById(R.id.btn_sharing);
            Button btn_Logout=(Button)vi.findViewById(R.id.btn_logot);


            if(isFbSessionActive && position==0){
                btn_Logout.setVisibility(View.VISIBLE);
            }else  if(isLoggedIn && position==1){
                btn_Logout.setVisibility(View.VISIBLE);
            }else{
                btn_Logout.setVisibility(View.GONE);
            }

            btn_textName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(position==0) {
                        if (isFbSessionActive) {
                            if (elesson_Activity.fb_userName != null) {
                                publishFeedDialog(elesson_Activity.fb_userName, "Download E-Lesson " + elesson_Activity.elessonBookTitle);
                            }
                        } else {
                            //authButton.setReadPermissions(Arrays.asList("email"));
                            authButton.performClick();
                            isFbSessionActive = false;
                        }
                    }else {
                        actionPerformed = false;
                        TwitterDialog twitterDialog = new TwitterDialog(elesson_Activity);
                        twitterDialog.loginToTwitter("Download E-Lesson" + elesson_Activity.elessonBookTitle);
                    }
                    sharePopUp.dissmissPopover(true);

                }
            });
            btn_Logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(position==0) {
                        authButton.performClick();

                    }else {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(elesson_Activity);
                        // Access Token
                        //String access_token = prefs.getString(PREF_KEY_OAUTH_TOKEN, ""); //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(TwitterLogin.this);
                        SharedPreferences.Editor e = prefs.edit();
                        e.putString("oauth_token", "");
                        e.putString("oauth_token_secret", "");
                        e.putBoolean("is_twitter_loggedin", false);
                        e.putString("twitter_user_name", "");
                        e.clear();
                        e.commit();

                    }
                    sharePopUp.dissmissPopover(true);
                }
            });

            //imgSearchImg.setVisibility(View.VISIBLE);
           // txtSearchItem.setVisibility(View.VISIBLE);
           // imgSearchImg.setVisibility(View.VISIBLE);

            btn_textName.setText(share[position]);
            imgSearchImg.setImageResource(mThumbIds[position]);
            return vi;
        }
    }


    public void publishFeedDialog(String userName, String selectedText) {
        actionPerformed = true;
        Bundle parameters = new Bundle();
        //parameters.putString("link", "http://itunes.apple.com/us/app/manahij/id477095484?ls=1&mt=8");
        parameters.putString("link", "");
        parameters.putString("picture", "http://dev.sboook.com/manahijLogo.png");
        parameters.putString("name", userName + " has Shared");
        parameters.putString("caption", elesson_Activity.getResources().getString(R.string.app_name));
        parameters.putString("description", selectedText);
        parameters.putString("message", "Share your message!");

        // Invoke the dialog
        WebDialog feedDialog = (
                new WebDialog.FeedDialogBuilder(elesson_Activity,
                        Session.getActiveSession(),
                        parameters))
                .setOnCompleteListener(new WebDialog.OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values,
                                           FacebookException error) {
                        if (error == null) {
                            // When the story is posted, echo the success
                            // and the post Id.
                            final String postId = values.getString("post_id");
                            if (postId != null) {
                                Toast.makeText(elesson_Activity,
                                        "Posted successfully",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                // User clicked the Cancel button
                                Toast.makeText(elesson_Activity.getApplicationContext(),
                                        "Publish cancelled",
                                        Toast.LENGTH_SHORT).show();

                            }

                        } else if (error instanceof FacebookOperationCanceledException) {
                            // User clicked the "x" button
                            Toast.makeText(elesson_Activity.getApplicationContext(),
                                    "Publish cancelled",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Generic, ex: network error
                            Toast.makeText(elesson_Activity.getApplicationContext(),
                                    "Error Publish text",
                                    Toast.LENGTH_SHORT).show();
                        }
                        actionPerformed = false;
                    }
                })
                .build();
        feedDialog.show();
    }


}
