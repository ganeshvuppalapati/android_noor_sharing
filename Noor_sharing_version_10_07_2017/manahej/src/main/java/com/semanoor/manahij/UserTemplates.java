package com.semanoor.manahij;



import java.io.File;

import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.Templates;
import com.semanoor.source_sboookauthor.UserFunctions;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class UserTemplates extends Activity{
	int pageCount=3;
	public  int CAPTURE_GALLERY_TEMPLATE = 5500;

	int Position;
	int templateId;
	ListView templatesView;
	String freeFilesPath = Globals.TARGET_BASE_TEMPATES_PATH;
	ImageView iv_selected;
	Button btnchange, btnCreateTmp;
	DatabaseHandler db;
	boolean templatecreated=false;
	Templates template;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_templates);

		//templateId=(Integer) getIntent().getSerializableExtra("TemplateId");
		db = DatabaseHandler.getInstance(this);
		templateId = Integer.parseInt(db.getMaxId("templates"));
		templateId = templateId+1;
		if (templateId<1000){
			templateId = 1001;
		}
		
		UserFunctions.createNewDirectory(Globals.TARGET_BASE_TEMPATES_PATH+templateId); 
		
		RelativeLayout layout=(RelativeLayout) findViewById(R.id.PageView);
		templatesView=(ListView) findViewById(R.id.PagelistView);
		iv_selected=(ImageView) findViewById(R.id.bgImgView);
		btnchange=(Button) findViewById(R.id.btn_change);
		btnCreateTmp=(Button) findViewById(R.id.btn_createtmp);


		btnCreateTmp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				templatecreated = true;
				db.executeQuery("insert into templates(templateID,name,type,titleFont,titleSize,titleColor,authorFont,authorSize,authorColor,paraFont,paraColor,paraSize) values('"+templateId+"','UserTemplate','portrait','Verdana','54','232-219-184','Arial','25','232-219-184','Arial','56-56-48','25')");
				finish();
			}
		});

		btnchange.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				openGallery();
			}
		});

		if(new File(freeFilesPath+templateId+"/front_P.png").exists()&& new File(freeFilesPath+templateId+"/page_P.png").exists()&& new File(freeFilesPath+templateId+"/back_P.png").exists()){
			btnCreateTmp.setVisibility(View.VISIBLE);
		}else{
			btnCreateTmp.setVisibility(View.INVISIBLE);
			btnchange.setVisibility(View.INVISIBLE);
		}

		final NewTemplateAdapter adapter = new NewTemplateAdapter(pageCount, UserTemplates.this);
		templatesView.setAdapter(adapter);
		templatesView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View v, int position,long id) {
				// TODO Auto-generated method stub
				Position=position;
				//wantedPosition =position;
				int firstPosition = templatesView.getFirstVisiblePosition() - templatesView.getHeaderViewsCount();
				int calculatedPosition = Position - firstPosition;
				View currentSelectedListView = adapterView.getChildAt(calculatedPosition);

				if (currentSelectedListView != null && currentSelectedListView != v) {
					v.setBackgroundColor(Color.WHITE);
				}
				currentSelectedListView = v;
				v.setBackgroundColor(Color.parseColor("#604e3634"));
				btnchange.setVisibility(View.INVISIBLE);
				if(position==0){
					if(new File(freeFilesPath+templateId+"/front_P.png").exists()){
						Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/front_P.png");
						iv_selected.setImageBitmap(bitmap);
						btnchange.setVisibility(View.VISIBLE);
					}else{
						openGallery();
						//Position=position;
					}
				}else if(position==1){
					if(new File(freeFilesPath+templateId+"/page_P.png").exists()){
						Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/page_P.png");
						iv_selected.setImageBitmap(bitmap);
						btnchange.setVisibility(View.VISIBLE);
					}else{
						openGallery();
					}
				}else if(position==2){
					if(new File(freeFilesPath+templateId+"/back_P.png").exists()){
						Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/back_P.png");
						iv_selected.setImageBitmap(bitmap);
						btnchange.setVisibility(View.VISIBLE);
					}else{
						openGallery();
						//Position=position;
					}
				}
				templatesView.invalidateViews();
			}
		});

	}
	
	@Override
	public void finish() {
		if(!templatecreated){
			UserFunctions.DeleteDirectory(new File(freeFilesPath+templateId));
		}
		super.finish();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//View view = page.getCurrentView();
		if(requestCode == CAPTURE_GALLERY_TEMPLATE){
			if(resultCode == RESULT_OK){
				Uri selectedImage = data.getData();

				String picturePath = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
				if (picturePath != null) {
					if(Position==0){
						coverImageForBook(picturePath,freeFilesPath+templateId+"/card.png");
						coverImageForBook(picturePath,freeFilesPath+templateId+"/frontThumb.png");
						potraitImageForBook(picturePath,freeFilesPath+templateId+"/front_P.png");
						Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/front_P.png");
						iv_selected.setImageBitmap(bitmap);
						btnchange.setVisibility(View.VISIBLE);
					}else if(Position==1){
						coverImageForBook(picturePath,freeFilesPath+templateId+"/pageThumb.png");
						potraitImageForBook(picturePath,freeFilesPath+templateId+"/page_P.png");
						Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/page_P.png");
						iv_selected.setImageBitmap(bitmap);
						btnchange.setVisibility(View.VISIBLE);
					}else if(Position==2){
                        coverImageForBook(picturePath,freeFilesPath+templateId+"/backThumb.png");
						potraitImageForBook(picturePath,freeFilesPath+templateId+"/back_P.png");
						Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/back_P.png");
						iv_selected.setImageBitmap(bitmap);
						btnchange.setVisibility(View.VISIBLE);
					}
					templatesView.invalidateViews();
				}
			}else if(resultCode == RESULT_CANCELED){
				//System.out.println("Cancelled operation to pic image from gallery");
			}
		}
	}

	public void coverImageForBook(String sourceDir,String freeFilesPath ){
		Bitmap bitmap = BitmapFactory.decodeFile(sourceDir);
		Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(200, UserTemplates.this), Globals.getDeviceIndependentPixels(300,  UserTemplates.this), true);
		UserFunctions.saveBitmapImage(cardBitmap, freeFilesPath);
	}

	public void potraitImageForBook(String sourceDir,String freeFilesPath ){
		Bitmap bitmap = BitmapFactory.decodeFile(sourceDir);
		Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(1536, UserTemplates.this), Globals.getDeviceIndependentPixels(2048,  UserTemplates.this), true);
		UserFunctions.saveBitmapImage(cardBitmap, freeFilesPath);
	}


	private class NewTemplateAdapter extends BaseAdapter{
		int pagecount;
       
		public NewTemplateAdapter(int pageCount, UserTemplates userTemplates) {
			// TODO Auto-generated constructor stub
			pagecount=pageCount;
		}
		//@Override
		public int getCount() {

			return pagecount;
		}
		///@Override
		public Object getItem(int position) {

			return null;
		}

		//@Override
		public long getItemId(int position) {

			return 0;
		}

		//@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			View vi = convertView;
			if(convertView == null){
				vi = getLayoutInflater().inflate(R.layout.custom_tmp__image, null);
			}
			TextView tv = (TextView) vi.findViewById(R.id.tv_pagename);
			ImageView image=(ImageView)vi. findViewById(R.id.iv_page);
			//Button btn_del = (Button) vi.findViewById(R.id.btn_del);
			if(Position==position){
				vi.setBackgroundColor(Color.parseColor("#604e3634"));
			}else{
				vi.setBackgroundColor(Color.WHITE);
			}
			if(position==0){
				tv.setText(getResources().getString(R.string.cover_page));
				if(new File(freeFilesPath+templateId+"/frontThumb.png").exists()){
					Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/frontThumb.png");
					image.setImageBitmap(bitmap);
				}else{
					vi.setBackgroundColor(Color.WHITE);
				}
			}else if(position==1){
				tv.setText(getResources().getString(R.string.page));
				if(new File(freeFilesPath+templateId+"/pageThumb.png").exists()){
					Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/pageThumb.png");
					image.setImageBitmap(bitmap);
				}
			}else{
				tv.setText(getResources().getString(R.string.end_page));
				if(new File(freeFilesPath+templateId+"/backThumb.png").exists()){
					Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/backThumb.png");
					image.setImageBitmap(bitmap);
				}
				if(new File(freeFilesPath+templateId+"/front_P.png").exists()&& new File(freeFilesPath+templateId+"/page_P.png").exists()&& new File(freeFilesPath+templateId+"/back_P.png").exists()){
					btnCreateTmp.setVisibility(View.VISIBLE);
				}else{
					btnCreateTmp.setVisibility(View.INVISIBLE);
				}
			}
			return vi;
		}
	}
	
   public void onBackPressed()
	{
		if(!templatecreated){
			UserFunctions.DeleteDirectory(new File(freeFilesPath+templateId));
		}
		finish();
	}

	private void openGallery(){
		Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		galleryIntent.setType("image/*");
		startActivityForResult(galleryIntent, CAPTURE_GALLERY_TEMPLATE);
	}
}

