package com.semanoor.manahij;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import com.google.api.services.drive.model.File;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class CloudPreviewActivity extends AppCompatActivity {
    private String fileId,downloadUrl;
    private  WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloud_preview);
        fileId = getIntent().getStringExtra("fileId");
        downloadUrl = getIntent().getStringExtra("filePath");
       // new DownloadFile().execute(downloadUrl);
        webView = (WebView) findViewById(R.id.cloud_preview);
        String url = "docs.google.com/gview?embedded=true&url="+downloadUrl;
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.loadUrl(url);

    }

    private class DownloadFile extends AsyncTask<String, String, String> {
        boolean downloadSuccess;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                String imgDir;
                imgDir = Globals.TARGET_BASE_FILE_PATH;
                java.io.File fileImgDir = new java.io.File(imgDir);
                if (!fileImgDir.exists()) {
                    fileImgDir.mkdir();
                }
                REST.insertPermission(fileId, "default", "anyone", "reader");
                OutputStream output = new FileOutputStream(imgDir+"temp.pdf");
                if (downloadUrl == null){
                    downloadUrl = REST.downloadUrl(fileId);
                }
             //   REST.downloadFile(downloadUrl);
                downloadSuccess = true;
            } catch (Exception e) {
                downloadSuccess = false;
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (downloadSuccess) {
                String url = "docs.google.com/gview?embedded=true&url=file:///"+Globals.TARGET_BASE_FILE_PATH+"temp.pdf";
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setAllowFileAccess(true);
                webView.loadUrl(url);
            }
        }
    }
}
