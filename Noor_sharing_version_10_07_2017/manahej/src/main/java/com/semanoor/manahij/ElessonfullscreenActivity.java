package com.semanoor.manahij;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.File;
import java.util.ArrayList;


public class ElessonfullscreenActivity extends Activity implements View.OnClickListener {
    RAdataobject radataobj;
    ArrayList<RAdataobject> mainobjlist;
    ViewPager viewPager;
    String currentbookpath;
    String language;
    LinearLayout layout;
    RelativeLayout layout1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_elessonfullscreen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        layout = (LinearLayout) findViewById(R.id.closeLayout);
        layout1 = (RelativeLayout) findViewById(R.id.btnlayout);
        CircleButton btnClose = (CircleButton) findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
        Button btnPrev = (Button) findViewById(R.id.btn_Prev);
        btnPrev.setOnClickListener(this);
        Button btnNext = (Button) findViewById(R.id.btn_Next);
        btnNext.setOnClickListener(this);
        Button btnStart = (Button) findViewById(R.id.btn_Start);
        btnStart.setOnClickListener(this);
        Button btnLast = (Button) findViewById(R.id.btn_Last);
        btnLast.setOnClickListener(this);

        radataobj = (RAdataobject) getIntent().getSerializableExtra("currentpath");
        currentbookpath = getIntent().getStringExtra("currentbook");
        mainobjlist = (ArrayList<RAdataobject>) getIntent().getSerializableExtra("mainlist");
        language = getIntent().getStringExtra("Language");
        viewpagerAdapter pageradapter = new viewpagerAdapter(ElessonfullscreenActivity.this);
        viewPager = (ViewPager) findViewById(R.id.fullscreenview);
        viewPager.setAdapter(pageradapter);
        if (language.equals("ar")) {
            viewPager.setCurrentItem(mainobjlist.size() - radataobj.getIndexNo(), false);
        } else {
            viewPager.setCurrentItem(radataobj.getIndexNo() - 1, false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_elessonfullscreen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClose:
                finish();
                break;
            case R.id.btn_Prev:
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                break;
            case R.id.btn_Next:
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                break;
            case R.id.btn_Start:
                viewPager.setCurrentItem(mainobjlist.size() - 1);
                break;
            case R.id.btn_Last:
                viewPager.setCurrentItem(0);
                break;
        }

    }

    /*
    viewPager for fullscreen view
    */
    class viewpagerAdapter extends PagerAdapter {
        Context context;

        public viewpagerAdapter(Context c) {
            context = c;
        }

        public Object instantiateItem(View collection, final int position) {
            if (language.equals("en")) {
                radataobj = mainobjlist.get(position);
            } else {
                int arabPosition = mainobjlist.size() - position - 1;
                radataobj = mainobjlist.get(arabPosition);
            }

            String urlpath;
            if (radataobj.getPath().contains("http")) {
                urlpath = radataobj.getPath();
            } else {
                urlpath = "file:///" + currentbookpath + radataobj.getPath();
            }
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.fullscreenwebview, null);
            final TextView txtview = (TextView) view.findViewById(R.id.pagno_Txt);
            txtview.setText(radataobj.getIndexNo() + "/" + mainobjlist.size());
            if (layout1.isShown()) {
                txtview.setVisibility(View.VISIBLE);
            } else {
                txtview.setVisibility(View.GONE);
            }
            String file = currentbookpath + radataobj.getPath();
            String srcContent = UserFunctions.decryptFile(new File(file));
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ElessonfullscreenActivity.this);
            String str = pref.getString(Globals.IJSTversion,"");
            if (str.contains(".zip")) {
                str = str.replace(".zip", "");
            }
            if(srcContent!=null && srcContent.contains("file://///200.1.1.66/IJST/")) {
                if (!str.equals("") && new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + "IJST/"+str).exists()){
                    srcContent = srcContent.replace("file://///200.1.1.66/IJST/", "file:../../../IJST/"+str+"/");
                }else {
                    srcContent = srcContent.replace("file://///200.1.1.66/IJST/", "file:../../../IJST/");
                }
            }
            WebView webView = (WebView) view.findViewById(R.id.fullscreenview);
            webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewClient());
            webView.getSettings().setAllowContentAccess(true);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            webView.getSettings().setDomStorageEnabled(true);
            webView.setWebViewClient(new WebViewClient());
            if (radataobj.getPath().contains("http")) {
                webView.loadUrl(urlpath);
            } else {
                webView.loadDataWithBaseURL(urlpath, srcContent, "text/html", "utf-8", null);
            }
            webView.setOnTouchListener(new View.OnTouchListener() {
                @Override                                              //onTouch for visible and invisible buttons
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            break;
                        }
                        case MotionEvent.ACTION_UP: {
                            if (layout.isShown() && layout1.isShown()) {
                                layout.setVisibility(View.GONE);
                                layout1.setVisibility(View.GONE);
                                txtview.setVisibility(View.GONE);
                            } else {
                                layout.setVisibility(View.VISIBLE);
                                layout1.setVisibility(View.VISIBLE);
                                txtview.setVisibility(View.VISIBLE);
                            }
                            break;
                        }
                        default:
                            break;
                    }
                    return false;
                }
            });
            ((ViewPager) collection).addView(view, 0);
            return view;
        }

        @Override
        public int getCount() {
            return mainobjlist.size();
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            RelativeLayout view = (RelativeLayout) object;
            TextView txtview = (TextView) view.getChildAt(1);
            if (layout1.isShown()) {
                txtview.setVisibility(View.VISIBLE);
            } else {
                txtview.setVisibility(View.GONE);
            }
            super.setPrimaryItem(container, position, object);
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            /*if (!layout1.isShown()) {
                RelativeLayout view = (RelativeLayout) arg0;
                TextView txtview = (TextView) view.getChildAt(1);
                txtview.setVisibility(View.GONE);
                return view == (View) arg1;
            } else {
                return arg0 == ((View) arg1);
            }*/
            return arg0 == ((View) arg1);
        }

        @Override
        public void destroyItem(View arg0, int arg1, Object arg2) {
            ((ViewPager) arg0).removeView((View) arg2);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }
}
