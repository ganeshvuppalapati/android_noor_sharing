package com.semanoor.manahij;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;

/**
 * Created by karthik on 14-10-2015.
 */
class startAnimation extends Animation {
    float startAngle;
    float endAngle;
    View pieChart;

    public startAnimation(View piechart, float start,float end){
        startAngle=start;
        endAngle=end;
        pieChart=piechart;
        setDuration(5000);
        setInterpolator(new LinearInterpolator());
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        float currAngle=startAngle+((endAngle-startAngle)*interpolatedTime);
        endAngle=-currAngle;
    }
}