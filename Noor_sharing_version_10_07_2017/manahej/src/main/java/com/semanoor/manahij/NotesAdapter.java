package com.semanoor.manahij;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by semanoor on 3/27/2017.
 */

public class NotesAdapter extends BaseAdapter {

    ArrayList<String> notesList;
    Context context;

    public NotesAdapter(Context context, ArrayList notesList) {
        super();

        this.context = context;
        this.notesList = notesList;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return notesList.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    private class ViewHolder {
        TextView noteText;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(context);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.display_notes, null);
            holder = new ViewHolder();
            holder.noteText = (TextView) convertView.findViewById(R.id.noteText);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.noteText.setText(notesList.get(position));


        return convertView;
    }
}
