package com.semanoor.manahij;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.semanoor.sboookauthor_store.ListViewItem;
import com.semanoor.sboookauthor_store.MainTabs;
import com.semanoor.sboookauthor_store.SubTabs;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import pl.polidea.treeview.InMemoryTreeStateManager;
import pl.polidea.treeview.TreeBuilder;
import pl.polidea.treeview.TreeStateManager;

public class ElessonBookViewActivity extends Activity implements OnClickListener {
	
	private static Book currentBook;
	public TreeViewList treeView;
	private ProgressBar progressBar;
	private TextView txtTitle;
	private HorizontalScrollView btnScrollView;
	public LinearLayout linearTabsLayout;
	private WebView webView;
	public MultiDirectionSlidingDrawer mDrawer;
	private String currentBookPath;
	private ArrayList<MainTabs> mainTabsList = new ArrayList<MainTabs>();
	private SubTabs subTabs;
	private MainTabs mainTabs;
	private ListViewItem lists;
	private boolean isListItemEmpty;
	public MainTabs currentSelectedMainTab;
	private static final int LEVEL_NUMBER = 4;
    private TreeStateManager<Long> manager = null;
    private enum TreeType implements Serializable {
        SIMPLE,
        FANCY
    }
    private final Set<Long> selected = new HashSet<Long>();
    private boolean adsPurchased;
	private InterstitialAd interstitial;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		loadLocale();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_elesson_book_view);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		currentBook = (Book) getIntent().getSerializableExtra("Book");
		String bookName = currentBook.get_bStoreID();
		Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
		ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

		UserFunctions.changeFont(rootView,font);
		
		Button btn_library=(Button) findViewById(R.id.btnLibrary);
		btn_library.setOnClickListener(this);
		
		treeView = (TreeViewList) findViewById(R.id.mainTreeView);
		
		progressBar=(ProgressBar) findViewById(R.id.progressBar);
		txtTitle=(TextView) findViewById(R.id.txtTitle);
		
		btnScrollView=(HorizontalScrollView) findViewById(R.id.btnScrollView); 
		linearTabsLayout=(LinearLayout)btnScrollView.findViewById(R.id.ll_main_container);
		
		webView=(WebView)findViewById(R.id.webView1);
		mDrawer = (MultiDirectionSlidingDrawer) findViewById(R.id.left_drawer);
		currentBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookName+"Book/";
		parsingBookXml();
		loadMainTabs();
		linearTabsLayout.post(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				int width=linearTabsLayout.getWidth();
				btnScrollView.smoothScrollTo(width, 0);
			}
		});
		
		SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
		adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);

		if (!adsPurchased) {
			interstitial = new InterstitialAd(ElessonBookViewActivity.this);
			interstitial.setAdUnitId(Globals.INTERSTITIAL_AD_UNIT_ID);
			AdRequest adRequest = new AdRequest.Builder()
			//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
			//.addTestDevice("505F22CBB28695386696655EED14B98B") 
			.build();
			interstitial.loadAd(adRequest);
		}
	}
	
	private void loadMainTabs(){
		for (int i = 0; i < mainTabsList.size(); i++) {
			MainTabs mainTab = mainTabsList.get(i);
			mainTab.createMainTabs(i);
		}
		if (currentSelectedMainTab.getSubTabsList().size() > 0) {
			loadListItemView(currentSelectedMainTab.getSubTabsList());
		} else {
			loadWebView(currentSelectedMainTab.getTabPath());
		}
	}
	
	public void loadListItemView(ArrayList<SubTabs> subTabList){
		if (subTabList.size() == 0) {
			mDrawer.close();
			mDrawer.setVisibility(View.GONE);
			loadWebView(currentSelectedMainTab.getTabPath());
		} else {
			mDrawer.open();
			mDrawer.setVisibility(View.VISIBLE);
			ArrayList<Object> listItemObject = new ArrayList<Object>();
			Long selectedId = null;
			for (int i = 0; i < subTabList.size(); i++) {
				SubTabs subTabs = subTabList.get(i);
				listItemObject.add(subTabs);
				if (i==0) {
					selectedId = (long) (listItemObject.size() - 1);
				}
				for (int j = 0; j < subTabs.getListItemTabs().size(); j++) {
					ListViewItem listItem = subTabs.getListItemTabs().get(j);
					listItemObject.add(listItem);
					if (i==0 && j==0) {
						selectedId = (long) (listItemObject.size() - 1);
					}
					for (int l = 0; l < listItem.getListViewItem().size(); l++) {
						ListViewItem listItem1 = listItem.getListViewItem().get(l);
						listItemObject.add(listItem1);
						if (i==0 && l==0 && j==0) {
							selectedId = (long) (listItemObject.size() - 1);
						}
					}
				}
			}
			//lv_list.setAdapter(new listViewItemAdapter(listItemObject));
			manager = new InMemoryTreeStateManager<Long>();
			final TreeBuilder<Long> treeBuilder = new TreeBuilder<Long>(manager);
			for (int i = 0; i < listItemObject.size(); i++) {
				SubTabs subTab;
				ListViewItem listItem;
				if (listItemObject.get(i) instanceof SubTabs) {
					subTab = (SubTabs) listItemObject.get(i);
					treeBuilder.sequentiallyAddNextNode((long) i, subTab.getNodeType());
				} else {
					listItem = (ListViewItem) listItemObject.get(i);
					treeBuilder.sequentiallyAddNextNode((long) i, listItem.getNodeType());
				}

			}
			//Log.d(TAG, manager.toString());
			/*TreeType newTreeType = TreeType.SIMPLE;
			boolean newCollapsible = true;
			SimpleStandardAdapter simpleAdapter = new SimpleStandardAdapter(this, selectedId, manager,
					LEVEL_NUMBER, listItemObject);
			treeView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			treeView.setAdapter(simpleAdapter);
			treeView.setCollapsible(newCollapsible);*/
		}
	}
	
	public void loadWebView(String path){
		String[] str = path.split("/");
		String filePath;
		if(str[0].contains("http")){
			filePath=path;
		}else{ 
			filePath ="file:///"+currentBookPath+path;
		}
		String srcContent= UserFunctions.readFileFromPath(new File(filePath));
		srcContent.replace("file://///200.1.1.66/IJST/", "file:../../../IJST/");
		progressBar.setVisibility(View.VISIBLE);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		webView.getSettings().setAllowContentAccess(true);
		webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		webView.getSettings().setAllowFileAccess(true);
		webView.getSettings().setPluginState(PluginState.ON);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			webView.getSettings().setAllowFileAccessFromFileURLs(true);
			webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
		}
		//webView.clearCache(true);
		webView.getSettings().setDomStorageEnabled(true);
		webView.loadDataWithBaseURL(currentBookPath,srcContent,"text/html", "utf-8", null);
		webView.setWebViewClient(new WebViewClient(){

			@Override
			public void onPageFinished(WebView view, String url){
				progressBar.setVisibility(View.INVISIBLE);	
			}	
		});
	}
	
	public void parsingBookXml() {

		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			DefaultHandler handler = new DefaultHandler(){
				String currentNode = "";
				public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException
				{
					if(qName.contentEquals("Lesson"))
					{
						String Footertext = attributes.getValue("FooterText");
						String title = attributes.getValue("Title");
						txtTitle.setText(title);
						//footerText.setText(Footertext);

					}else if(qName.contains("MTab")){
						String pageTitle = attributes.getValue("Title");
						String path = attributes.getValue("Url");
						mainTabs=new MainTabs(ElessonBookViewActivity.this);
						mainTabs.setTabPath(path);
						mainTabs.setTabTitle(pageTitle);
						mainTabsList.add(mainTabs);
					}
					else if(qName.contains("STab")){
						String pageTitle = attributes.getValue("Title");
						String path = attributes.getValue("Url");

						subTabs=new SubTabs(ElessonBookViewActivity.this);
						subTabs.setTabPath(path);
						subTabs.setTabTitle(pageTitle);
						subTabs.setNodeType(0);
						mainTabs.getSubTabsList().add(subTabs);
					}
					else if(qName.contains("Item")){
						String Title = attributes.getValue("Title");
						String path = attributes.getValue("Url");
						String Type = attributes.getValue("Type");

						if(path.equals("")){
							isListItemEmpty=false;
						}

						if(!isListItemEmpty){
							lists=new ListViewItem(ElessonBookViewActivity.this);
							lists.setTabTitle(Title);
							lists.setTabPath(path);
							lists.settabType(Type);
							lists.setNodeType(1);
							if(path.equals("")){
								//lists.setTabDiff("listItem1");
								isListItemEmpty=true;
							}

							subTabs.getListItemTabs().add(lists);
						}else{
							ListViewItem listViewItem1=new ListViewItem(ElessonBookViewActivity.this);
							listViewItem1.setTabTitle(Title);
							listViewItem1.setTabPath(path);
							listViewItem1.settabType(Type);
							listViewItem1.setNodeType(2);
							lists.getListViewItem().add(listViewItem1);
						}

					}else if(qName.contentEquals("List")){

					}
				}

				/*@Override
					public void endElement(String uri, String localName, String qName) throws SAXException {

						if(qName.contains("MTab")){
							//System.out.println(qName);
							//System.out.println(maintabs.getSubTabsList());

                         }else if(qName.contains("STab")){
							//System.out.println(subtabs.getListItemTabs());

                        }else if(qName.contentEquals("List")){
                        	//System.out.println(subtabs.getListItemTabs());
						}
					}*/
			};

			//revert_book_hide :
			String filePath = currentBookPath+"Data/Lesson.xml";
			InputStream inputStream = new FileInputStream(filePath);
			Reader reader = new InputStreamReader(inputStream, "UTF-8");
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");
			saxParser.parse(is, handler);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Load Locale language from shared preference and change language in the application
	 */
	private void loadLocale() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String language = prefs.getString(Globals.languagePrefsKey, "en");
		changeLang(language);
	}
	
	/**
	 * change language in the application
	 * @param language
	 */
	private void changeLang(String language) {
		Locale myLocale = new Locale(language);
		Locale.setDefault(myLocale);
		android.content.res.Configuration config = new android.content.res.Configuration();
		config.locale = myLocale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.elesson_book_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnLibrary:{
				onBackPressed();
				break;
			}
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (Globals.isLimitedVersion() && !adsPurchased) {
			if (interstitial.isLoaded()) {
				interstitial.show();
			}
		}
		finish();
	}
}
