package com.semanoor.manahij;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mobeta.android.dslv.DragSortListView;
import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.Category;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.DownloadBackground;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.ShelfforcloudBook;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Krishna on 09-08-2016.
 */
public class CloudBooksActivity extends Activity {

    public ShelfforcloudBook bookShelf;
    public DragSortListView gridView;
    //String[] categoryList ={"Downloaded ","New Books","MiniBooks","mindmap","Media"};
    ArrayList<Category> Category;
    public ArrayList<Object> bookListArray = new ArrayList<Object>();
    public GridShelf gridShelf;
    public WebService webService;
    ArrayList<String> miniBooks;
    //ArrayList<String> EnrResList,newEnrResList,myenrResList;
    public ArrayList<HashMap<String, String>> EnrResList,newEnrResList,myenrResList;
    ArrayList<CloudData> miniBook,myBooks,newBooks,mindMapBooks,media;
    public DatabaseHandler db;
    public DownloadBackground background;
    public int storeViewActivityrequestCode = 3;
    public SlideMenuWithActivityGroup SlideMenuActivity;
    public String ScopeId,emaild;
    RelativeLayout shelfRootViewLayout;
    String themesPath =  Globals.TARGET_BASE_FILE_PATH+"Themes/current/";
    private AdView adView;
    private Groups groups;
    String id=null;
    public ProgressDialog syncProgressDialog;
    private SwipeRefreshLayout swipeContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.cloudbookslayout);
        gridShelf= (GridShelf) getIntent().getSerializableExtra("Shelf");
        if(getIntent().hasExtra("id")) {
            id = getIntent().getStringExtra("id");
        }
        syncProgressDialog = ProgressDialog.show(CloudBooksActivity.this,"", getResources().getString(R.string.fetching_files), true);
        shelfRootViewLayout = (RelativeLayout) findViewById(R.id.shelfRootViewLayout);
        UserFunctions.applyingBackgroundImage(themesPath+"inbox.png",shelfRootViewLayout, CloudBooksActivity.this);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContainer.setRefreshing(true);
                new loadCloudEnrichmentList().execute();
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        db = new DatabaseHandler(this);
        groups = Groups.getInstance();
        webService = new WebService(CloudBooksActivity.this, Globals.getNewCurriculumWebServiceURL());
        CircleButton btn_back= (CircleButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (Globals.isLimitedVersion()) {
            initializeAds();
        }
        Category=new ArrayList<Category>();
        for(int i=0;i<5;i++) {
            Category category = new Category();
            category.setCategoryId(i);
            if(i==0){
                category.setCategoryName(CloudBooksActivity.this.getResources().getString(R.string.downloaded_books));
            }else  if(i==1){
                category.setCategoryName(CloudBooksActivity.this.getResources().getString(R.string.not_downloaded_books));
            }else  if(i==2){
                category.setCategoryName(CloudBooksActivity.this.getResources().getString(R.string.created_books));
            }else  if(i==3){
                category.setCategoryName(CloudBooksActivity.this.getResources().getString(R.string.mindmap));
            }else  if(i==4){
                category.setCategoryName(CloudBooksActivity.this.getResources().getString(R.string.album));
            }
            //category.setCategoryName(categoryList[i]);
            category.setNewCatID(i);
            category.setHidden(false);
            Category.add(category);
        }
        new loadCloudEnrichmentList().execute();
    }

    public void loadGridView() {
        bookShelf = new ShelfforcloudBook(this,Category, miniBook,myBooks,newBooks,mindMapBooks,media);
        gridView = (DragSortListView) findViewById(R.id.myListView);
        gridView.setAdapter(bookShelf);
    }

    private  void loadLocale() {
        String data;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        ScopeId=prefs.getString(Globals.sUserIdKey, "");
       // String data = prefs.getString(ScopeId, "");
        emaild = prefs.getString(Globals.sUserEmailIdKey, "");

    }
    private class loadCloudEnrichmentList extends AsyncTask<Void, Void, Void> {
         String emaild;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CloudBooksActivity.this);
            emaild = prefs.getString(Globals.sUserEmailIdKey, "");
            if(id!=null){
                emaild=id;
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            webService.getCloudEnrichmentsFromUser(emaild, CloudBooksActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            swipeContainer.setRefreshing(false);
            miniBooks=new ArrayList<String>();
            miniBook=new ArrayList<CloudData>();
            myBooks=new ArrayList<CloudData>();
            newBooks=new ArrayList<CloudData>();
            mindMapBooks=new ArrayList<CloudData>();
            media=new ArrayList<CloudData>();
            if (webService.cloudEnrList != null && webService.cloudEnrList.size() > 0) {

                EnrResList=new ArrayList<HashMap<String, String>>();
                newEnrResList=new ArrayList<HashMap<String, String>>();
                myenrResList=new ArrayList<HashMap<String, String>>();

                for(int i=0;i<webService.cloudEnrList.size();i++){
                    HashMap<String, String> map = webService.cloudEnrList.get(i);
                    String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
                    String enrichmentNo=map.get(webService.KEY_CENRICHMENTS);
                    String jsonContent = map.get(webService.KEY_CECONTENT);
                    final String message = map.get(webService.KEY_CEMESSAGE);
                    String bookTitle = "";
                    try {
                        //HashMap<String, Object> values = (HashMap<String, Object>) jsonContent;
                        JSONObject jsonObject = new JSONObject(jsonContent);
                        String type=jsonObject.getString("type");
                        if(type.equals("sharedBook")) {
                            final String downloadFromPath = jsonObject.getString("downloadPath");
                            final String bookDetails = jsonObject.getString("bookDetails");
                            String data= enrichmentNo+"###"+message+"###"+jsonContent;
                            miniBooks.add(data);
                            CloudData book= addingDatatoArrayList(data,type,map,2);
                            miniBook.add(book);

                        }else if (type.equals("Resource") ||type.equals("Enrichment")) {
                            String storeBookId = jsonObject.getString("IDBook");
                             String data;
                             data= enrichmentNo+"###"+message+"###"+jsonContent;
                             EnrResList.add(map);
                        } else if(type.equals("Mindmap")){
                            String data= enrichmentNo+"###"+message+"###"+jsonContent;
                            CloudData book= addingDatatoArrayList(data,type,map,3);
                            mindMapBooks.add(book);

                         }else if(type.equals("Album")){
                            String data= enrichmentNo+"###"+message+"###"+jsonContent;
                            CloudData book= addingDatatoArrayList(data,type,map,4);
                            media.add(book);
                        }

                        } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                for(int i=0;i<EnrResList.size();i++){
                    HashMap<String, String> map =EnrResList.get(i);
                    String jsonContent = map.get(webService.KEY_CECONTENT);

                    try {
                        JSONObject jsonObject = new JSONObject(jsonContent);
                        String storeBookId = jsonObject.getString("IDBook");
                        String type=jsonObject.getString("type");
                        if(checkStorBookExist(storeBookId)){
                            myenrResList.add(map);
                        }else{
                            newEnrResList.add(map);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(myenrResList.size()>0){

                 for(int i=0;i<myenrResList.size();i++){
                     boolean exist = false;
                     HashMap<String, String> map=myenrResList.get(i);
                    String jsonContent = map.get(webService.KEY_CECONTENT);

                     try {
                         JSONObject jsonObject = new JSONObject(jsonContent);
                         String storeBookId = jsonObject.getString("IDBook");
                         String type=jsonObject.getString("type");


                            for(int j=0;j<myBooks.size();j++){
                                CloudData dataa=myBooks.get(j);
                                if(dataa.getStoreId().equals(storeBookId)){
                                     myBooks.remove(j);
                                     CloudData enrich=new CloudData();
                                     enrich.setType(type);
                                     enrich.setMap(map);
                                     enrich.setStoreId(storeBookId);
                                     if (type.equals("Enrichment")) {
                                         dataa.getEnrichments().add(enrich);
                                     }else {
                                         dataa.getResources().add(enrich);
                                     }
                                    myBooks.add(dataa);
                                    exist=true;
                                    break;
                                 }

                            }

                         if(!exist) {
                             CloudData dataa = new CloudData();
                             dataa.setStoreId(storeBookId);
                             dataa.setCategoryId(0);
                             CloudData enrich = new CloudData();
                             enrich.setType(type);
                             enrich.setMap(map);
                             enrich.setStoreId(storeBookId);
                             if (type.equals("Enrichment")) {
                                 dataa.getEnrichments().add(enrich);
                             } else {
                                 dataa.getResources().add(enrich);
                             }
                             myBooks.add(dataa);
                         }

                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                 }
                }
                if(newEnrResList.size()>0){

                    for(int i=0;i<newEnrResList.size();i++){
                        boolean exist = false;
                        HashMap<String, String> map=newEnrResList.get(i);
                        String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
                        String enrichmentNo=map.get(webService.KEY_CENRICHMENTS);
                        String jsonContent = map.get(webService.KEY_CECONTENT);

                        try {
                            JSONObject jsonObject = new JSONObject(jsonContent);
                            String storeBookId = jsonObject.getString("IDBook");
                            String type=jsonObject.getString("type");
                                for(int j=0;j<newBooks.size();j++){
                                    CloudData dataa=newBooks.get(j);
                                    if(dataa.getStoreId().equals(storeBookId)){
                                        newBooks.remove(j);
                                        CloudData enrich=new CloudData();
                                        enrich.setType(type);
                                        //enrich.setBookDetails(data);
                                        enrich.setMap(map);
                                        enrich.setStoreId(storeBookId);
                                        if (type.equals("Enrichment")) {
                                            dataa.getEnrichments().add(enrich);
                                        }else {
                                            dataa.getResources().add(enrich);
                                        }
                                        newBooks.add(dataa);
                                        exist=true;
                                        break;
                                    }
                                 }
                                if(!exist){
//                                    CloudData book= addingDatatoArrayList(data,type,map,3);
                                    CloudData book=new CloudData();
                                    book.setStoreId(storeBookId);
                                    book.setCategoryId(0);
                                    book.setMap(map);
                                    CloudData enrichment=new CloudData();
                                    enrichment.setType(type);
                                    enrichment.setMap(map);
                                    enrichment.setStoreId(storeBookId);
                                    if (type.equals("Enrichment")) {
                                        book.getEnrichments().add(enrichment);
                                    }else {
                                        book.getResources().add(enrichment);
                                    }
                                    newBooks.add(book);

                                }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (syncProgressDialog!=null) {
                syncProgressDialog.dismiss();
            }
            loadGridView();
         }
    }


    public boolean checkStorBookExist(String storeBookID){
        ArrayList<Object> bookList = gridShelf.bookListArray;
        for (int i = 0; i<bookList.size(); i++) {
            Book book = (Book) bookList.get(i);
            if (book.is_bStoreBook()) {
                if (book.get_bStoreID().equals(storeBookID)) {
                    return true;
                }
            }
        }
        return false;
    }
  private CloudData addingDatatoArrayList(String data, String type, HashMap<String, String> map, int i){
      CloudData book=new CloudData();
      book.setBookDetails(data);
      book.setType(type);
      book.setMap(map);
      book.setCategoryId(4);
     // media.add(book);

      return book;
  }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
        finish();
    }

    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data){
        if (requestCode == storeViewActivityrequestCode && data != null) {
            GridShelf grShelf = (GridShelf) data.getSerializableExtra("Shelf");
            gridShelf.bookListArray = grShelf.bookListArray;
            gridShelf.setNoOfBooks(grShelf.bookListArray.size());
            new loadCloudEnrichmentList().execute();
        }
    }
    private void initializeAds(){
        SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
        boolean adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);

        if (!adsPurchased) {
            adView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder()
                    //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    //.addTestDevice("505F22CBB28695386696655EED14B98B")
                    .build();
            adView.loadAd(adRequest);
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (!groups.getBannerAdsVisible()) {
                        adView.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }
}

