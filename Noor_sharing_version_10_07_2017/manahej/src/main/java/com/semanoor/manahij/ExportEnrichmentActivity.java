package com.semanoor.manahij;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.Note;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.GenerateHTML;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.UserGroups;
import com.semanoor.source_sboookauthor.WebService;
import com.semanoor.source_sboookauthor.Zip;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class ExportEnrichmentActivity extends Activity implements android.widget.CompoundButton.OnCheckedChangeListener {

    private ViewPager viewPager;
    public Book currentBook;
    private ArrayList<Enrichments> enrichmentListArray, enrichmentPageList;
    public  DatabaseHandler db;
    private WebService webService;
    private ArrayList<Enrichments> selectedEnrichedPageList;
    private int serviceType = 0;
    private String enrichXml = "<Enrichments>";
    boolean enrPublishSelected = false;
    boolean enrUpdateSelected = false;
    boolean enrStatusSelected = false;
    private String userName = "", password = "", resourceTitle;
    private ProgressDialog progressDialog;
    private EditText etUserName, etPassword;
    private ArrayList<Enrichments> publishEnrichTotalPageNoList;
    private ArrayList<Enrichments> exportedEnrichmentList;
    private ListView enrichStatusOrPagelistView, pageListView, lv_group;
    private ProgressBar enrStatusProgressBar;
    private boolean enrStatusLoaded = false;
    private ArrayList<Note> noteList, notePageList, noteTotalPageNoList, noteSelectedList;
    private ArrayList<Enrichments> enrichmentLinksList, enrichmentPageLinkList, enrichmentLinkTotalPageNoList, enrichmentSelectedLinkList,SelectedNoteListArray;
    private ArrayList<Enrichments> mindmapList, mindmapPageList, mindmapTotalPageNoList, mindmapSelectedList, enrichmentSelectedList;
    private boolean enrichmentTabSelected = true, noteTabSelected = false, enrichmentLinkSelected = false, groupSelected = false, emailSelected = false, mindmapTabSelected = false;
    public ArrayList<UserGroups> groups;
   // public UserCredentials userCredentials;
    public ArrayList<UserGroups> selectedGroup = new ArrayList<UserGroups>();
    public ArrayList<UserGroups> Groupdetails = new ArrayList<UserGroups>();
    EditText et_resourcetitle, et_mailid;
    RelativeLayout rl_groups;
    String mailid;
    boolean isprivate;
    Button btnPublish2;
    RelativeLayout rl_radiobutton;
    String filepath;
    String desc;
    File enrichZipFilePath;

    private InputStream chunkInputStream;
    private ByteArrayOutputStream chunkBaos;
    private byte[] chunkBytes;
    private int chunkBytesRead;
    private String chunkBookFlag;
    private int chunkNoOfExecution;
    String enrichXmlString;

    Switch btnswitch;

    RadioButton Radio_enr,Radio_links,Radio_mindmap,Radio_notes;
    public String login_UserName;
    public String scopeId;
    public int scopeType;
    public String email;
    HashMap dicEnrichments;

    private static final int MY_PERMISSIONS_REQUEST_GET_ACCOUNTS = 100001;
    public static int AUTH_CODE_REQUEST_CODE = 1004;
    private GDriveExport gDriveExport;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        scopeId = prefs.getString(Globals.sUserIdKey, "");
        login_UserName = prefs.getString(Globals.sUserNameKey, "");
        scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
        email = prefs.getString(Globals.sUserEmailIdKey, "");
        setContentView(R.layout.activity_export_enrichment);

        currentBook = (Book) getIntent().getSerializableExtra("Book");
        //userCredentials = (UserCredentials) getIntent().getSerializableExtra("UserCredentials");

        WindowManager.LayoutParams params = getWindow().getAttributes();
        //params.width = Globals.getDeviceIndependentPixels(450, this);
        //params.height = Globals.getDeviceIndependentPixels(400, this);
        params.width = (int) getResources().getDimension(R.dimen.info_Export_popupwidth);
        params.height = (int) getResources().getDimension(R.dimen.info_Export_popupheight);
        this.getWindow().setAttributes(params);
        //getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //params.setBackgroundColor(Color.TRANSPARENT);

        db = DatabaseHandler.getInstance(this);
        webService = new WebService(this, Globals.getNewCurriculumWebServiceURL());

        //enrichmentListArray = db.getAllEnrichmentTabListForTheBook(currentBook.getBookID(), this);

        enrichmentListArray = db.getAllEnrichmentTabListForExport(currentBook.getBookID(), ExportEnrichmentActivity.this);
        selectedEnrichedPageList = new ArrayList<Enrichments>();

        enrichmentLinksList = db.getAllEnrichmentLinksForExport(currentBook.getBookID(), ExportEnrichmentActivity.this);
        noteList = db.getAllNotesForExport(currentBook.get_bStoreID(), ExportEnrichmentActivity.this);
        mindmapList = db.getAllEnrichmentMindmapForExport(currentBook.getBookID(), ExportEnrichmentActivity.this);
        enrichmentSelectedList = new ArrayList<Enrichments>();
        enrichmentSelectedLinkList = new ArrayList<Enrichments>();
        noteSelectedList = new ArrayList<Note>();
        mindmapSelectedList = new ArrayList<Enrichments>();

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new ViewAdapter());
        viewPager.setOffscreenPageLimit(0);
        //viewPager.setCurrentItem(Integer.parseInt(currentPageNumber) - 1);
        viewPager.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton view, boolean isChecked) {
        if (view.getId() == R.id.switch1) {
            if (isChecked) {
                rl_groups.setVisibility(View.VISIBLE);
                et_mailid.setVisibility(View.INVISIBLE);
                lv_group.setVisibility(View.VISIBLE);
                groupSelected = true;
                emailSelected = false;
                isprivate = true;
            } else {
                rl_groups.setVisibility(View.GONE);
                groupSelected = false;
                emailSelected = false;
                isprivate = false;
            }

        }
    }

    private class ViewAdapter extends PagerAdapter {

        private ArrayList<Enrichments> enrichedPageList;

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            int resId = 0;
            View view = null;
            Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
            UserFunctions.changeFont(container,font);
            switch (position) {
                case 0:
                    resId = R.layout.export_enr_home_inflate;
                    view = inflater.inflate(resId, null);
                    Button btnPublishEnr = (Button) view.findViewById(R.id.btnPublishEnr);
                    btnPublishEnr.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (UserFunctions.isInternetExist(ExportEnrichmentActivity.this)) {
                                enrPublishSelected = true;
                                enrUpdateSelected = false;
                                enrStatusSelected = false;
                                enrichmentTabSelected = true;
                                noteTabSelected = false;
                                enrichmentLinkSelected = false;
                                mindmapTabSelected = false;
                                selectedEnrichedPageList.clear();
                                radioButtonSelection();
                                publishEnrichTotalPageNoList = getTotalNumberOfEnrichedPageList();
                                rl_radiobutton.setVisibility(View.VISIBLE);
                                //webService = new WebService(ExportEnrichmentActivity.this, Globals.getNewCurriculumWebServiceURL());
                                pageListView.setAdapter(new PageListAdapter(ExportEnrichmentActivity.this, publishEnrichTotalPageNoList, null, null, null));
                                viewPager.setCurrentItem(1, true);
                            } else {
                                UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportEnrichmentActivity.this, ExportEnrichmentActivity.this.getResources().getString(R.string.check_internet_connectivity), ExportEnrichmentActivity.this.getResources().getString(R.string.connection_error));
                            }
                        }
                    });

                    Button btnUpdateEnr = (Button) view.findViewById(R.id.btnUpdateEnr);
                    btnUpdateEnr.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (UserFunctions.isInternetExist(ExportEnrichmentActivity.this)) {
                                enrPublishSelected = false;
                                enrUpdateSelected = true;
                                enrStatusSelected = false;
                                selectedEnrichedPageList.clear();
                                enrichmentTabSelected = true;
                                noteTabSelected = false;
                                enrichmentLinkSelected = false;
                                mindmapTabSelected = false;
                                radioButtonSelection();
                               // webService = new WebService(ExportEnrichmentActivity.this, Globals.getNewCurriculumWebServiceURL());
                                publishEnrichTotalPageNoList = getTotalNumberOfEnrichedPageList();
                                pageListView.setAdapter(new PageListAdapter(ExportEnrichmentActivity.this, publishEnrichTotalPageNoList, null, null, null));
                                //viewPager.setCurrentItem(1, true);
                               // rl_radiobutton.setVisibility(View.GONE);
                                viewPager.setCurrentItem(1, true);
                            } else {
                                UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportEnrichmentActivity.this, ExportEnrichmentActivity.this.getResources().getString(R.string.check_internet_connectivity), ExportEnrichmentActivity.this.getResources().getString(R.string.connection_error));
                            }
                        }
                    });

                    Button btnEnrStatus = (Button) view.findViewById(R.id.btnEnrStatus);
                    btnEnrStatus.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (UserFunctions.isInternetExist(ExportEnrichmentActivity.this)) {
                                enrPublishSelected = false;
                                enrUpdateSelected = false;
                                enrStatusSelected = true;
                                selectedEnrichedPageList.clear();
                                exportedEnrichmentList = getExportedEnrichmentList();
                               // webService = new WebService(ExportEnrichmentActivity.this, Globals.ENRICHWEBSERVICEURL);
                                //pageListView.invalidateViews();

                                viewPager.setCurrentItem(2, false);
                            } else {
                                UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportEnrichmentActivity.this, ExportEnrichmentActivity.this.getResources().getString(R.string.check_internet_connectivity), ExportEnrichmentActivity.this.getResources().getString(R.string.connection_error));
                            }
                        }
                    });

                    Button btnCancel = (Button) view.findViewById(R.id.btn_cancel);
                    btnCancel.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });

                    if (Globals.ISGDRIVEMODE()) {
                        btnUpdateEnr.setVisibility(View.GONE);
                        btnEnrStatus.setVisibility(View.GONE);
                    } else {
                        btnUpdateEnr.setVisibility(View.VISIBLE);
                        btnEnrStatus.setVisibility(View.VISIBLE);
                    }

                    break;
                case 1:
                    resId = R.layout.exportenr_resources;
                    view = inflater.inflate(resId, null);

                    publishEnrichTotalPageNoList = getTotalNumberOfEnrichedPageList();
                    enrichmentLinkTotalPageNoList = getTotalNumberOfEnrichedLinkPageList();
                    noteTotalPageNoList = getTotalNumberOfNotePageList();
                    mindmapTotalPageNoList = getTotalNumberOfMindmapPageList();
                    SegmentedRadioButton segmentGroup = (SegmentedRadioButton) view.findViewById(R.id.segment_text);
                    Radio_enr = (RadioButton) segmentGroup.findViewById(R.id.segEnr);
                    Radio_notes = (RadioButton) segmentGroup.findViewById(R.id.segNotes);
                    Radio_links = (RadioButton) segmentGroup.findViewById(R.id.segLinks);
                    Radio_mindmap = (RadioButton) segmentGroup.findViewById(R.id.segMindmap);
                    radioButtonSelection();
                    pageListView = (ListView) view.findViewById(R.id.listView1);
                    rl_radiobutton = (RelativeLayout) view.findViewById(R.id.rl_radio_btn);
                    RelativeLayout progress_layout = (RelativeLayout) view.findViewById(R.id.progress_layout);
//                    progress_layout.setBackgroundColor(Color.parseColor("#3F3054"));
                    Button btnBack = (Button) view.findViewById(R.id.btn_back);
//                    Drawable img=ExportEnrichmentActivity.this.getResources().getDrawable(R.drawable.arrownext);
//                    btnBack.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    btnBack.setText(R.string.back);
//                    btnBack.setTextColor(Color.BLACK);
                    btnBack.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            viewPager.setCurrentItem(0, false);

                        }
                    });
                    segmentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (checkedId == R.id.segEnr) {
                                enrichmentTabSelected = true;
                                noteTabSelected = false;
                                enrichmentLinkSelected = false;
                                mindmapTabSelected = false;
                                publishEnrichTotalPageNoList = getTotalNumberOfEnrichedPageList();
                                pageListView.setAdapter(new PageListAdapter(ExportEnrichmentActivity.this, publishEnrichTotalPageNoList, null, null, null));
                            } else if (checkedId == R.id.segNotes) {
                                enrichmentTabSelected = false;
                                noteTabSelected = true;
                                enrichmentLinkSelected = false;
                                mindmapTabSelected = false;
                                noteTotalPageNoList = getTotalNumberOfNotePageList();
                                pageListView.setAdapter(new PageListAdapter(ExportEnrichmentActivity.this, null, null, noteTotalPageNoList, null));
                            } else if (checkedId == R.id.segLinks) {
                                enrichmentTabSelected = false;
                                noteTabSelected = false;
                                enrichmentLinkSelected = true;
                                mindmapTabSelected = false;
                                enrichmentLinkTotalPageNoList = getTotalNumberOfEnrichedLinkPageList();
                                pageListView.setAdapter(new PageListAdapter(ExportEnrichmentActivity.this, null, enrichmentLinkTotalPageNoList, null, null));
                            } else if (checkedId == R.id.segMindmap) {
                                enrichmentTabSelected = false;
                                noteTabSelected = false;
                                enrichmentLinkSelected = false;
                                mindmapTabSelected = true;
                                mindmapTotalPageNoList = getTotalNumberOfMindmapPageList();
                                pageListView.setAdapter(new PageListAdapter(ExportEnrichmentActivity.this, null, null, null, mindmapTotalPageNoList));
                            }
                        }
                    });
                    if (noteTabSelected) {
                        pageListView.setAdapter(new PageListAdapter(ExportEnrichmentActivity.this, null, null, noteTotalPageNoList, null));
                    } else if (mindmapTabSelected) {
                        pageListView.setAdapter(new PageListAdapter(ExportEnrichmentActivity.this, null, null, null, mindmapTotalPageNoList));
                    } else if (enrichmentLinkSelected) {
                        pageListView.setAdapter(new PageListAdapter(ExportEnrichmentActivity.this, null, enrichmentLinkTotalPageNoList, null, null));
                    } else if (enrichmentTabSelected) {
                        pageListView.setAdapter(new PageListAdapter(ExportEnrichmentActivity.this, publishEnrichTotalPageNoList, null, null, null));
                    }
                    ProgressBar enrichPage1bar = (ProgressBar) view.findViewById(R.id.enrich_progressbar);

                    pageListView.setOnItemClickListener(new OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                            /*if (enrUpdateSelected) {
                                Enrichments enrichment = publishEnrichTotalPageNoList.get(position);
                                enrichedPageList = getTotalNoOfEnrichedPages(enrichment.getEnrichmentPageNo());
                                enrichStatusOrPagelistView.setAdapter(new ExportEnrichPageListAdapter(enrichedPageList));
                                //enrichStatusOrPagelistView.setDividerHeight(2);
                            } else {*/
                                if (enrichmentTabSelected) {
                                    Enrichments enrichment = publishEnrichTotalPageNoList.get(position);
                                    enrichmentPageList = getTotalNoOfEnrichedPages(enrichment.getEnrichmentPageNo());
                                } else if (enrichmentLinkSelected) {
                                    Enrichments enrichment = enrichmentLinkTotalPageNoList.get(position);
                                    enrichmentPageLinkList = getTotalNoOfEnrichedLinkPages(enrichment.getEnrichmentPageNo());
                                } else if (mindmapTabSelected) {
                                    Enrichments enrichment = mindmapTotalPageNoList.get(position);
                                    mindmapPageList = getTotalNoOfMindmapPages(enrichment.getEnrichmentPageNo());
                                } else if (noteTabSelected) {
                                    Note note = noteTotalPageNoList.get(position);
                                    notePageList = getTotalNoOfNotePages(note.getPageNo());
                                }
                            //}
                            ((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
                            enrichStatusOrPagelistView.invalidateViews();
                            btnPublish2.setVisibility(View.VISIBLE);
                            viewPager.setCurrentItem(2, false);
                        }
                    });

                    break;

                case 2:
				/*if (enrStatusSelected) {
					exportedEnrichmentList = getExportedEnrichmentList();        //status
				} else {
					publishEnrichTotalPageNoList = getTotalNumberOfEnrichedPageList();      //Publish
				}*/
                    resId = R.layout.export_enr_listview;
                    view = inflater.inflate(resId, null);

                    enrStatusProgressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
//                    RelativeLayout rl_layout = (RelativeLayout) view.findViewById(R.id.rl_layout);
//                    rl_layout.setBackgroundColor(Color.parseColor("#3F3054"));
                    Button btnBack1 = (Button) view.findViewById(R.id.btnBack);
//                    Drawable img1=ExportEnrichmentActivity.this.getResources().getDrawable(R.drawable.arrownext);
//                    btnBack1.setCompoundDrawablesWithIntrinsicBounds(img1, null, null, null);
                    btnBack1.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (enrStatusSelected) {
                                enrStatusLoaded = false;
                                viewPager.setCurrentItem(0, false);
                            } else {
                                viewPager.setCurrentItem(1);
                            }
                        }
                    });

                    enrichStatusOrPagelistView = (ListView) view.findViewById(R.id.listView1);
                    enrichStatusOrPagelistView.setDividerHeight(2);
                    btnPublish2 = (Button) view.findViewById(R.id.btnPublish);
                    if (enrUpdateSelected) {
                        btnPublish2.setText("Update");
                    }
                    btnPublish2.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                             if (enrichmentLinkSelected) {
                                    if (enrichmentSelectedLinkList.size() > 0) {
                                         et_resourcetitle.setVisibility(View.VISIBLE);
                                         viewPager.setCurrentItem(3);
                                    } else {
                                        UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.no_enrichment_selected, R.string.select_enrichment_to_export);
                                    }
                                } else if (noteTabSelected) {
                                    if (noteSelectedList.size() > 0) {
                                         et_resourcetitle.setVisibility(View.VISIBLE);
                                         viewPager.setCurrentItem(3);

                                    } else {
                                        UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, "Note is not Selected", R.string.select_note_to_publish);
                                    }

                                } else if (mindmapTabSelected) {
                                    if (mindmapSelectedList.size() > 0) {
                                       et_resourcetitle.setVisibility(View.VISIBLE);
                                       viewPager.setCurrentItem(3);
                                    } else {
                                        UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.no_mindmap_selected, R.string.select_mindmap_to_export);
                                    }
                                } else {
                                    if (enrichmentSelectedList.size() > 0) {
                                         et_resourcetitle.setVisibility(View.VISIBLE);
                                          viewPager.setCurrentItem(3);
                                    } else {
                                        UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.no_enrichment_selected, R.string.select_enrichment_to_export);

                                    }
                                }
                            if (!enrichmentTabSelected) {
                                btnswitch.setEnabled(false);
                                isprivate=true;
                            } else {
                                btnswitch.setEnabled(true);
                                btnswitch.setOnCheckedChangeListener(ExportEnrichmentActivity.this);
                                isprivate=true;
                            }
                                if(!enrUpdateSelected) {
                                    if (groups == null) {
                                        groups = db.getMyGroups(scopeId, ExportEnrichmentActivity.this);
                                    }
                                    lv_group.setAdapter(new groupListAdapter(ExportEnrichmentActivity.this, groups));
                                }
                        }
                    });

                    if (!enrStatusSelected) {
                       // if (!enrUpdateSelected) {
                            enrichStatusOrPagelistView.setAdapter(new PageListSelectAdapter());
                            enrichStatusOrPagelistView.setDividerHeight(2);
                      //  }
                         enrichStatusOrPagelistView.setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View v,
                                                    int position, long id) {

                                    if (enrichmentTabSelected) {
                                        Enrichments enrichment = enrichmentPageList.get(position);
                                        if (enrichment.isEnrichmentSelected()) {
                                            enrichment.setEnrichmentSelected(false);
                                            enrichmentSelectedList.remove(enrichment);
                                        } else {
                                            enrichment.setEnrichmentSelected(true);
                                            enrichmentSelectedList.add(enrichment);
                                        }


                                    } else if (enrichmentLinkSelected) {
                                        Enrichments enrichment = enrichmentPageLinkList.get(position);
                                        if (enrichment.isEnrichmentSelected()) {
                                            enrichment.setEnrichmentSelected(false);
                                            enrichmentSelectedLinkList.remove(enrichment);
                                        } else {
                                            enrichment.setEnrichmentSelected(true);
                                            enrichmentSelectedLinkList.add(enrichment);
                                        }

                                    } else if (mindmapTabSelected) {
                                        Enrichments enrichment = mindmapPageList.get(position);
                                        if (enrichment.isEnrichmentSelected()) {
                                            enrichment.setEnrichmentSelected(false);
                                            mindmapSelectedList.remove(enrichment);
                                        } else {
                                            enrichment.setEnrichmentSelected(true);
                                            mindmapSelectedList.add(enrichment);
                                        }

                                    } else if (noteTabSelected) {
                                        Note note = notePageList.get(position);
                                        if (note.isNoteSelected()) {
                                            note.setNoteSelected(false);
                                            noteSelectedList.remove(note);
                                        } else {
                                            note.setNoteSelected(true);
                                            noteSelectedList.add(note);
                                        }
                                    }

                                ((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();

                            }
                        });
                    } else {
                        if (enrStatusSelected && !enrStatusLoaded) {
                            btnPublish2.setVisibility(View.GONE);
                            new getResponseForEnrichmentStatus().execute();
                        }
                    }


                    break;

                case 3:
                    resId = R.layout.export_enr_login;
                    view = inflater.inflate(resId, null);
                    et_resourcetitle = (EditText) view.findViewById(R.id.et_resource);
                   // et_resourcetitle.setTextColor(Color.WHITE);
                    //et_resourcetitle.setHintTextColor(Color.WHITE);
                    rl_groups = (RelativeLayout) view.findViewById(R.id.group_view);
                    SegmentedRadioButton Group_selection = (SegmentedRadioButton) rl_groups.findViewById(R.id.segment_text);
                    RadioButton Radio_group = (RadioButton) Group_selection.findViewById(R.id.seggroup);
                    RadioButton Radio_email = (RadioButton) Group_selection.findViewById(R.id.segemail);
                    lv_group = (ListView) rl_groups.findViewById(R.id.lv_groups);
                    lv_group.setDividerHeight(2);
                    et_mailid = (EditText) rl_groups.findViewById(R.id.et_mailid);
                    et_mailid.setVisibility(View.INVISIBLE);
                    //et_mailid.setTextColor(Color.WHITE);
                    rl_groups.setVisibility(View.VISIBLE);
                    btnswitch = (Switch) view.findViewById(R.id.switch1);
                    btnswitch.setChecked(true);
                    if (Globals.ISGDRIVEMODE()) {
                        btnswitch.setVisibility(View.GONE);
                    } else {
                        btnswitch.setVisibility(View.VISIBLE);
                    }
                    Button btnPublish = (Button) view.findViewById(R.id.btnPublish);
                    btnPublish.setVisibility(View.VISIBLE);
//                    TextView tv_title = (TextView) view.findViewById(R.id.textView1);
//                    tv_title.setTypeface(null, Typeface.BOLD);
                    //lv_group.setAdapter(new groupListAdapter(ExportEnrichmentActivity.this, groups));

                    Group_selection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (checkedId == R.id.seggroup) {
                                groupSelected = true;
                                emailSelected = false;
                                et_mailid.setVisibility(View.INVISIBLE);
                                lv_group.setVisibility(View.VISIBLE);
                            } else if (checkedId == R.id.segemail) {
                                groupSelected = false;
                                emailSelected = true;
                                et_mailid.setVisibility(View.VISIBLE);
                                lv_group.setVisibility(View.INVISIBLE);
                            }
                        }
                    });

                    lv_group.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            UserGroups group = groups.get(position);
                            if (group.isGroupSelected()) {
                                group.setGroupSelected(false);
                                selectedGroup.remove(group);

                            } else {
                                group.setGroupSelected(true);
                                selectedGroup.add(group);
                            }
                            lv_group.invalidateViews();

                        }
                    });

                    btnPublish.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //publishMethod();
                            resourceTitle = et_resourcetitle.getText().toString();
                           // SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ExportEnrichmentActivity.this);
                           // userName = sharedPref.getString(Globals.exportEnrichUserNameKey, "");
                            //password = sharedPref.getString(Globals.exportEnrichPasswordKey, "");
                            //userName=userCredentials.getUserName();
                            //password=userCredentials.getUserScopeId();
                          /*  if(enrUpdateSelected){
                                String str = getResources().getString(R.string.please_wait);
                                progressDialog = ProgressDialog.show(ExportEnrichmentActivity.this, "", str, true);
                                serviceType = 0;
                                new validateLoginAndPublishEnrichments().execute();
                            }else {  */
                                if (enrichmentLinkSelected) {
                                    publishmethod();
                                } else if (noteTabSelected) {
                                    publishmethod();
                                } else if (mindmapTabSelected) {
                                    publishmethod();
                                } else {
                                    publishmethod();
                                }
                            //}
                        }
                    });
                    Button btn_back = (Button) view.findViewById(R.id.btnBack);
//                    Drawable btn_img=ExportEnrichmentActivity.this.getResources().getDrawable(R.drawable.arrownext);
//                    btn_back.setCompoundDrawablesWithIntrinsicBounds(btn_img, null, null, null);

                    btn_back.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            et_mailid.getText().clear();
                            et_resourcetitle.getText().clear();
                            viewPager.setCurrentItem(2, false);
                        }
                    });
                    ImageView imgViewCover = (ImageView) view.findViewById(R.id.imgView_cover);
                    int BID = currentBook.getBookID();
                    String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/pageBG_1.png";
                    if (!new File(bookCardImagePath).exists()) {
                        bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/card.png";
                    }
                    Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
                    imgViewCover.setImageBitmap(bitmap);

                    break;

                default:
                    break;
            }

            ((ViewPager) container).addView(view, 0);
            return view;
        }
		
		/*@Override
		public void setPrimaryItem(ViewGroup container, int position,
				Object object) {
			super.setPrimaryItem(container, position, object);
			if (position == 1 || position == 3) {
				RelativeLayout parentView = (RelativeLayout) object;
				RelativeLayout childView = (RelativeLayout) parentView.getChildAt(0);
				Button btnPublish = (Button) childView.getChildAt(2);
				if (enrPublishSelected) {
					btnPublish.setText("Publish");
				} else {
					btnPublish.setText("Update");
				}
				if (position == 1) {
					RelativeLayout childView1 = (RelativeLayout) parentView.getChildAt(1);
					etUserName = (EditText) childView1.getChildAt(0);
					etPassword = (EditText) childView1.getChildAt(1);
				}
				btnPublish.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if (!UserFunctions.isInternetExist(ExportEnrichmentActivity.this)) {
							UserFunctions.DisplayAlertDialogNotFromStringsXML(ExportEnrichmentActivity.this, ExportEnrichmentActivity.this.getResources().getString(R.string.check_internet_connectivity), ExportEnrichmentActivity.this.getResources().getString(R.string.connection_error));
							return;
						}
						if (enrPublishSelected || enrUpdateSelected) {
							userName = etUserName.getText().toString();
							password = etPassword.getText().toString();
							if (userName.length() < 1 || password.length() < 1) {
								UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.forget_username_password, R.string.login_details);
								return;
							}
							if (selectedEnrichedPageList.size() > 0) {
								String str = getResources().getString(R.string.please_wait);
								progressDialog = ProgressDialog.show(ExportEnrichmentActivity.this, "", str, true);
								serviceType = 0;
								new validateLoginAndPublishEnrichments().execute();
							} else {
								UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.no_enrichment_selected, R.string.select_enrichment_to_export);
							}
						}
					}
				});
			} 
			
			//Load Only the Enriched Pages in list view 
			if (position == 3) {
				RelativeLayout parentView = (RelativeLayout) object;
				final ListView childListView = (ListView) parentView.getChildAt(1);
				childListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
				if (enrPublishSelected || enrUpdateSelected) {
					childListView.setAdapter(new ExportEnrichPageListAdapter(enrichedPageList));
					childListView.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> adapterView, View view,
								int position, long arg3) {
							Enrichments enrichments = enrichedPageList.get(position);
							if (enrichments.isEnrichmentSelected()) {
								enrichments.setEnrichmentSelected(false);
								selectedEnrichedPageList.remove(enrichments);
							} else {
								enrichments.setEnrichmentSelected(true);
								selectedEnrichedPageList.add(enrichments);
							}
							((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
						}
					});
				}
			}
			
			if (position == 2) {
				if (enrStatusSelected && !enrStatusLoaded) {
					new getResponseForEnrichmentStatus().execute();
				}
			}
		}*/

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

   private void radioButtonSelection() {
       if (mindmapTabSelected) {
           Radio_mindmap.setChecked(true);
           Radio_links.setChecked(false);
           Radio_notes.setChecked(false);
           Radio_enr.setChecked(false);
       } else if (enrichmentLinkSelected) {
           Radio_mindmap.setChecked(false);
           Radio_links.setChecked(true);
           Radio_notes.setChecked(false);
           Radio_enr.setChecked(false);
       } else if (noteTabSelected) {
           Radio_mindmap.setChecked(false);
           Radio_notes.setChecked(true);
           Radio_links.setChecked(false);
           Radio_enr.setChecked(false);
       } else {
           Radio_mindmap.setChecked(false);
           Radio_links.setChecked(false);
           Radio_notes.setChecked(false);
           Radio_enr.setChecked(true);
       }
   }
        /**
         * Get All The Exported Enriched Object List
         *
         * @return
         */
        private ArrayList<Enrichments> getExportedEnrichmentList() {
            ArrayList<Enrichments> exportedEnrList = new ArrayList<Enrichments>();
            for (Enrichments enrichments : enrichmentListArray) {
                if (enrichments.getEnrichmentExportValue() > 0) {
                    exportedEnrList.add(enrichments);
                }
            }
            return exportedEnrList;
        }

        /**
         * Get All the total number of Enriched page list
         *
         * @return
         */
        private ArrayList<Enrichments> getTotalNumberOfEnrichedPageList() {
            ArrayList<Enrichments> enrichPageNoList = new ArrayList<Enrichments>();
            for (Enrichments enrichment : enrichmentListArray) {
                boolean isEnrichPageNoExist = false;
                for (Enrichments filteredEnrichments : enrichPageNoList) {
                    if (filteredEnrichments.getEnrichmentPageNo() == enrichment.getEnrichmentPageNo()) {
                        isEnrichPageNoExist = true;
                        break;
                    } else {
                        isEnrichPageNoExist = false;
                    }
                }
                if (!isEnrichPageNoExist && enrPublishSelected && enrichment.getEnrichmentExportValue()==0) {
                    enrichPageNoList.add(enrichment);
                } else if (!isEnrichPageNoExist && enrUpdateSelected&& enrichment.getEnrichmentExportValue()!=0) {
                    enrichPageNoList.add(enrichment);
                }
            }
            return enrichPageNoList;
        }

        /**
         * Get All the total number of Enriched Link page list
         *
         * @return
         */
        private ArrayList<Enrichments> getTotalNumberOfEnrichedLinkPageList() {
            ArrayList<Enrichments> enrichPageNoList = new ArrayList<Enrichments>();
            for (Enrichments enrichment : enrichmentLinksList) {
                boolean isEnrichPageNoExist = false;
                for (Enrichments filteredEnrichments : enrichPageNoList) {
                    if (filteredEnrichments.getEnrichmentPageNo() == enrichment.getEnrichmentPageNo()) {
                        isEnrichPageNoExist = true;
                        break;
                    } else {
                        isEnrichPageNoExist = false;
                    }
                }
                if (!isEnrichPageNoExist  && enrPublishSelected && enrichment.getEnrichmentExportValue()==0) {
                    enrichPageNoList.add(enrichment);
                }else if (!isEnrichPageNoExist && enrUpdateSelected && enrichment.getEnrichmentExportValue()!=0) {
                    enrichPageNoList.add(enrichment);
                }
            }
            return enrichPageNoList;
        }

        /**
         * Get All the total number of Note page list
         *
         * @return
         */
        private ArrayList<Note> getTotalNumberOfNotePageList() {
            ArrayList<Note> notePageNoList = new ArrayList<Note>();
            for (Note note : noteList) {
                boolean isNotePageNoExist = false;
                for (Note filteredNote : notePageNoList) {
                    if (filteredNote.getPageNo() == note.getPageNo()) {
                        isNotePageNoExist = true;
                        break;
                    } else {
                        isNotePageNoExist = false;
                    }
                }
                if (!isNotePageNoExist && enrPublishSelected && note.getExportedId()==0) {
                    notePageNoList.add(note);
                }if (!isNotePageNoExist && enrUpdateSelected&& note.getExportedId()!=0) {
                    notePageNoList.add(note);
                }
            }
            return notePageNoList;
        }

        /**
         * Get All the total number of Mindmap page list
         *
         * @return
         */
        private ArrayList<Enrichments> getTotalNumberOfMindmapPageList() {
            ArrayList<Enrichments> mindmapPageNoList = new ArrayList<Enrichments>();
            for (Enrichments enrichment : mindmapList) {
                boolean isEnrichPageNoExist = false;
                for (Enrichments filteredEnrichments : mindmapPageNoList) {
                    if (filteredEnrichments.getEnrichmentPageNo() == enrichment.getEnrichmentPageNo()) {
                        isEnrichPageNoExist = true;
                        break;
                    } else {
                        isEnrichPageNoExist = false;
                    }
                }
                if (!isEnrichPageNoExist  && enrPublishSelected && enrichment.getEnrichmentExportValue()==0) {
                    mindmapPageNoList.add(enrichment);
                }if (!isEnrichPageNoExist && enrUpdateSelected && enrichment.getEnrichmentExportValue()!=0) {
                    mindmapPageNoList.add(enrichment);
                }
            }
            return mindmapPageNoList;
        }


        /**
         * Get the total number of enriched page no by passing the page number
         *
         * @param pageNo
         * @return
         */
        private ArrayList<Enrichments> getTotalNoOfEnrichedPages(int pageNo) {
            ArrayList<Enrichments> enrichedPageList = new ArrayList<Enrichments>();
            for (Enrichments enrichments : enrichmentListArray) {
                if (pageNo == enrichments.getEnrichmentPageNo() && enrichments.getEnrichmentExportValue() == 0 && enrPublishSelected) {
                    enrichedPageList.add(enrichments);
                } else if (pageNo == enrichments.getEnrichmentPageNo() && enrichments.getEnrichmentExportValue() != 0 && enrUpdateSelected) {
                    enrichedPageList.add(enrichments);
                }
            }
            return enrichedPageList;
        }

    }

    /**
     * Get the total number of mindmap page no by passing the page number
     *
     * @param pageNo
     * @return
     */
    private ArrayList<Enrichments> getTotalNoOfMindmapPages(int pageNo) {
        ArrayList<Enrichments> mindmapPageList = new ArrayList<Enrichments>();
        for (Enrichments enrichments : mindmapList) {
            if (pageNo == enrichments.getEnrichmentPageNo()) {
                mindmapPageList.add(enrichments);
            }
        }
        return mindmapPageList;
    }

    /**
     * Get the total number of  link page no by passing the page number
     *
     * @param pageNo
     * @return
     */
    private ArrayList<Enrichments> getTotalNoOfEnrichedLinkPages(int pageNo) {
        ArrayList<Enrichments> enrichedLinkPageList = new ArrayList<Enrichments>();
        for (Enrichments enrichments : enrichmentLinksList) {
            if (pageNo == enrichments.getEnrichmentPageNo()) {
                enrichedLinkPageList.add(enrichments);
            }
        }
        return enrichedLinkPageList;
    }

    /**
     * Get the total number of  note page no by passing the page number
     *
     * @param pageNo
     * @return
     */
    private ArrayList<Note> getTotalNoOfNotePages(int pageNo) {
        ArrayList<Note> notePageList = new ArrayList<Note>();
        for (Note note : noteList) {
            if (pageNo == note.getPageNo()) {
                notePageList.add(note);
            }
        }
        return notePageList;
    }

    /**
     * Get Response from Enrichment status from webservice
     *
     * @author Suriya
     */
    private class getResponseForEnrichmentStatus extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            enrStatusLoaded = true;
            enrStatusProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (exportedEnrichmentList.size() == 1) {
                Enrichments enrichment = exportedEnrichmentList.get(0);
                webService.checkEnrichedPage(enrichment.getEnrichmentExportValue());
            } else if (exportedEnrichmentList.size() > 1) {
                String enrichXML = "<Enrichments>";
                for (Enrichments enrichment : exportedEnrichmentList) {
                    enrichXML = enrichXML.concat("<enriched ID=\"" + enrichment.getEnrichmentExportValue() + "\"/>");
                }
                enrichXML = enrichXML.concat("</Enrichments>");
                webService.checkMultipleEnrichedPages(enrichXML);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            enrStatusProgressBar.setVisibility(View.GONE);
            if (exportedEnrichmentList.size() > 0) {
                enrichStatusOrPagelistView.setAdapter(new ExportPageListAdapter(exportedEnrichmentList));
            }
            super.onPostExecute(result);
        }

    }

    /**
     * Validate the login and publish or update enrichments
     *
     * @author Suriya
     */
    private class validateLoginAndPublishEnrichments extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            if (serviceType == 0) {
                ExportEnrichmentActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.setMessage("Signing In Please Wait");
                    }
                });
                webService.enrichPublishLogin(userName, password);
            } else if (serviceType == 1) {
                ExportEnrichmentActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.setMessage(getResources().getString(R.string.upload_please_wait));
                    }
                });
                if(enrichmentTabSelected) {
                    initUpload(enrichmentSelectedList);
                }else if(enrichmentLinkSelected){
                    //initUpload(enrichmentSelectedLinkList);
                    generatingXmlfornoteSelected();
                }else if(noteTabSelected){
                    generatingXmlfornoteSelected();
                  // initUpload(enrichmentSelectedLinkList)
                }else if(mindmapTabSelected){
                    genratingXmlforMindmapSelected();
                }
                File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp/zipFile222.zip");
                String enrichPageZipFileBase64 = convertFileToBase64Binary(enrichedPagesFilePath);
                String methodName = null;
                if (enrPublishSelected) {
                    methodName = "PublishEnriched_Step1";
                } else if (enrUpdateSelected) {
                    methodName = "UpdateEnriched_Step1";
                }
                webService.publishOrUpdateEnrichedPagesAsBytes(methodName, enrichedPagesFilePath.getName(), enrichPageZipFileBase64);
            } else if (serviceType == 2) {
                String methodName = null;
                if (enrPublishSelected) {
                    methodName = "PublishEnriched_Step3_Group";
                } else if (enrUpdateSelected) {
                    methodName = "UpdateEnriched_Step3_Group";
                }
                File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp/zipFile222.zip");
                webService.publishOrUpdateEnrichmentXml(methodName, enrichedPagesFilePath.getName(), enrichXml);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (serviceType == 0) {
                if (webService.enrichUserLoginHashMapDatails.size() > 0) {
                    String userCanPublish = webService.enrichUserLoginHashMapDatails.get(webService.KEY_ENRICH_USER_CANPUBLISH);
                    if (userCanPublish.contains("true")) {
                        serviceType = 1;
                        new validateLoginAndPublishEnrichments().execute();
                    } else {
                        progressDialog.dismiss();
                        UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.no_permission_publish_enrichment, R.string.oops);
                    }
                } else {
                    progressDialog.dismiss();
                    UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.went_wrong, R.string.oops);
                }
            } else if (serviceType == 1) {
                if (webService.enrichUploadBookResponse.contains("True")) {
                    serviceType = 2;
                    new validateLoginAndPublishEnrichments().execute();
                } else {
                    File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                    UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                    progressDialog.dismiss();
                    UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.went_wrong, R.string.oops);
                }
            } else if (serviceType == 2) {
               if (enrUpdateSelected) {
                    if (webService.enrichedPageUpdateResponseResult.size() > 0) {
                        if (webService.enrichedPageUpdateResponseResult.get(0).equals("true")) {
                            for (Enrichments enrichments : selectedEnrichedPageList) {
                                enrichments.setEnrichmentSelected(false);
                            }
                            selectedEnrichedPageList.clear();
                            progressDialog.dismiss();
                            File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                            UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                            UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.update_completed_successfully, R.string.updated);
                            viewPager.setCurrentItem(1);
                        } else {
                            progressDialog.dismiss();
                            File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                            UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                            UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.went_wrong, R.string.oops);
                            viewPager.setCurrentItem(1);
                        }
                    } else {
                        progressDialog.dismiss();
                        File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                        UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                        UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.went_wrong, R.string.oops);
                        viewPager.setCurrentItem(1);
                    }
                }
            }
        }

        /**
         * Generate all the selected enrichment html pages and and make it as zip and create an xml to prepare for publish or update.
         * @param enrichmentSelectedList
         */
        private void initUpload( ArrayList<Enrichments> enrichmentSelectedList) {
            String[] bookId=currentBook.get_bStoreID().split("M");
            //String storBookId = currentBook.get_bStoreID().replace("M", "");
            String storBookId=bookId[1];
            enrichXmlString = "<Enrichments>";
            if (Globals.ISGDRIVEMODE()) {
                dicEnrichments = new HashMap();
            }
            int i = 0;
           for (Enrichments enrichments : enrichmentSelectedList) {
                if (enrichments.isEnrichmentSelected()) {
                    new GenerateHTML(ExportEnrichmentActivity.this, db, currentBook, String.valueOf(enrichments.getEnrichmentPageNo()), null,false).generateEnrichedPageForExportAndZipIt(enrichments);
                    String fileName = currentBook.getBookID() + "-" + enrichments.getEnrichmentPageNo() + "-" + enrichments.getEnrichmentId() + ".zip";
                    if (enrPublishSelected) {
                        int idPage = enrichments.getEnrichmentPageNo() - 1;
                        enrichXmlString = enrichXmlString.concat("<Enriched IDUser='" + webService.enrichUserLoginHashMapDatails.get(webService.KEY_ENRICH_USER_ID) + "' IDBook='" + storBookId + "' IDPage='" + idPage + "' Title='" + enrichments.getEnrichmentTitle() + "' Password='' isPublished='" + webService.enrichUserLoginHashMapDatails.get(webService.KEY_ENRICH_USER_AUTHORIZE_DIRECT_PUBLISH_ID) + "' PublishToEnrichementsSite='true' file='" + fileName + "' />");
                        if (Globals.ISGDRIVEMODE()) {
                            HashMap<String, String> dicEnr = new HashMap<String, String>();
                            String storeID = "001";
                            String currentDateTime = UserFunctions.getCurrentDateTimeInString();
                            String enrIDGlobal = storeID + "-" + currentBook.getBookID() + "-" + idPage + "-" + enrichments.getEnrichmentId() + "-" + scopeId;
                            dicEnr.put("file1", fileName);
                            dicEnr.put("title", enrichments.getEnrichmentTitle());
                            dicEnr.put("pageNo", String.valueOf(idPage));
                            dicEnr.put("dateTime", currentDateTime);
                            dicEnr.put("enrichmentIDGlobal", enrIDGlobal);
                            String enrKey = "enrichment" + i;
                            dicEnrichments.put(enrKey, dicEnr);
                        }
                        i++;
                    } else if (enrUpdateSelected) {
                        enrichXmlString = enrichXmlString.concat("<Enriched ID='" + enrichments.getEnrichmentExportValue() + "' Title='" + enrichments.getEnrichmentTitle() + "' Password='' isPublished='" + webService.enrichUserLoginHashMapDatails.get(webService.KEY_ENRICH_USER_AUTHORIZE_DIRECT_PUBLISH_ID) + "' file='" + fileName + "' />");
                    }
                }
            }
            enrichXmlString = enrichXmlString.concat("</Enrichments>");
            if (selectedEnrichedPageList.size() > 0) {
                Zip zip = new Zip();
                  String sourcePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp/";
                String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp/zipFile222.zip";
                zip.zipFilesForEnrichments(new File(sourcePath), destPath);
            }
        }

    }

    /**
     * convert file to base64 binary
     *
     * @param file
     * @return
     */
    private String convertFileToBase64Binary(File file) {
        String base64Binary = null;
        byte[] byteArray = null;
        try {
            InputStream inputStream = new FileInputStream(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 8];
            int bytesRead = 0;
            //int i = 0;
            while ((bytesRead = inputStream.read(b)) != -1) {
                ////System.out.println("bytesRead: "+bytesRead+"i: "+i);
                baos.write(b, 0, bytesRead);
                //i++;
            }
            byteArray = baos.toByteArray();
            base64Binary = Base64.encodeToString(byteArray, Base64.DEFAULT).trim();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return base64Binary;
    }

    private class ExportEnrichPageListAdapter extends BaseAdapter {

        private ArrayList<Enrichments> totalEnrichedPageList;

        public ExportEnrichPageListAdapter(
                ArrayList<Enrichments> enrichedPageList) {
            totalEnrichedPageList = enrichedPageList;
        }

        @Override
        public int getCount() {
            return totalEnrichedPageList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.export_enr_select_inflate, null);
            }
            Enrichments enrichments = totalEnrichedPageList.get(position);
            TextView textView = (TextView) view.findViewById(R.id.textView1);
            textView.setText(enrichments.getEnrichmentTitle());
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
            if (enrichments.isEnrichmentSelected()) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            return view;
        }

    }

    private class ExportPageListAdapter extends BaseAdapter {

        private ArrayList<Enrichments> publishEnrichTotalPageNoList;

        public ExportPageListAdapter(
                ArrayList<Enrichments> _publishEnrichTotalPageNoList) {
            this.publishEnrichTotalPageNoList = _publishEnrichTotalPageNoList;
        }

        @Override
        public int getCount() {
            return publishEnrichTotalPageNoList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (enrStatusSelected) {
                if (view == null) {
                    view = getLayoutInflater().inflate(R.layout.export_enr_status, null);
                    ViewGroup viewGroup = (RelativeLayout) view.findViewById(R.id.export_ststus_layout);
                    Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
                    UserFunctions.changeFont(viewGroup,font);
                }
                TextView enr_pageNo = (TextView) view.findViewById(R.id.enr_pageNo);
                TextView enr_title = (TextView) view.findViewById(R.id.enr_title);
                TextView enr_status = (TextView) view.findViewById(R.id.enr_status);
                String resStr = getResources().getString(R.string.page);
                Enrichments enrichments = publishEnrichTotalPageNoList.get(position);
                for (HashMap<String, String> map : webService.enrichStatusHashMapList) {
                    if (enrichments.getEnrichmentExportValue() == Integer.parseInt(map.get(webService.KEY_ENRICH_STATUS_ID))) {
                        enr_pageNo.setText(resStr + " " + enrichments.getEnrichmentPageNo());
                        enr_title.setText(enrichments.getEnrichmentTitle());
                        int publishId = Integer.parseInt(map.get(webService.KEY_ENRICH_ISPUBLISHED));
                        if (publishId == 1) {
                            enr_status.setText(R.string.waiting_for_approval);
                        } else if (publishId == 2) {
                            enr_status.setText(R.string.rejected);
                        } else if (publishId == 3) {
                            enr_status.setText(R.string.approved);
                        }
                        break;
                    }
                }
            } else {
                if (view == null) {
                    view = getLayoutInflater().inflate(R.layout.export_enr_page_listview_inflate, null);
                    ViewGroup viewGroup = (RelativeLayout) view.findViewById(R.id.enrich_pagelist_layout);
                    Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
                    UserFunctions.changeFont(viewGroup,font);
                }
                Enrichments enrichments = publishEnrichTotalPageNoList.get(position);
                TextView textView = (TextView) view.findViewById(R.id.textView1);
                String resStr = getResources().getString(R.string.page);
                textView.setText(resStr + " " + enrichments.getEnrichmentPageNo());

                ImageView imgViewCover = (ImageView) view.findViewById(R.id.imageView1);
                String screenShotPath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + enrichments.getEnrichmentPageNo() + ".png";
                if (new File(screenShotPath).exists()) {
                    Bitmap bitmap = BitmapFactory.decodeFile(screenShotPath);
                    imgViewCover.setImageBitmap(bitmap);
                }
            }
            return view;
        }

    }


    private class PageListAdapter extends BaseAdapter {

        private ExportEnrichmentActivity bookView;
        private ArrayList<Enrichments> enrPageList;
        private ArrayList<Enrichments> enrLinkPageList;
        private ArrayList<Note> notePageList;
        private ArrayList<Enrichments> mindmapPageList;

        public PageListAdapter(ExportEnrichmentActivity bookView,
                               ArrayList<Enrichments> enrPageList, ArrayList<Enrichments> enrLinkPageList, ArrayList<Note> notePageList, ArrayList<Enrichments> mindmapPageList) {
            this.bookView = bookView;
            this.enrPageList = enrPageList;
            this.enrLinkPageList = enrLinkPageList;
            this.notePageList = notePageList;
            this.mindmapPageList = mindmapPageList;
        }

        @Override
        public int getCount() {
            if (enrichmentTabSelected) {
                return enrPageList.size();
            } else if (enrichmentLinkSelected) {
                return enrLinkPageList.size();
            } else if (mindmapTabSelected) {
                return mindmapPageList.size();
            } else {
                return notePageList.size();
            }

        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                LayoutInflater layoutinflater = (LayoutInflater) bookView.getSystemService(bookView.LAYOUT_INFLATER_SERVICE);
                view = layoutinflater.inflate(R.layout.export_enr_page_listview_inflate, null);
                Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
            }
            TextView text = (TextView) view.findViewById(R.id.textView1);
            String page = bookView.getResources().getString(R.string.page);
            ImageView imgViewCover = (ImageView) view.findViewById(R.id.imageView1);
            String screenShotPath;

            if (enrichmentTabSelected) {
                Enrichments enrichments = enrPageList.get(position);
                int pageno = enrichments.getEnrichmentPageNo() - 1;
                text.setText(page + " " + pageno);
                screenShotPath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + enrichments.getEnrichmentPageNo() + ".png";
                if (new File(screenShotPath).exists()) {
                    Bitmap bitmap = BitmapFactory.decodeFile(screenShotPath);
                    imgViewCover.setImageBitmap(bitmap);
                }
            } else if (enrichmentLinkSelected) {
                Enrichments enrichments = enrLinkPageList.get(position);
                int pageno = enrichments.getEnrichmentPageNo() - 1;
                text.setText(page + " " + pageno);
                screenShotPath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + enrichments.getEnrichmentPageNo() + ".png";
                if (new File(screenShotPath).exists()) {
                    Bitmap bitmap = BitmapFactory.decodeFile(screenShotPath);
                    imgViewCover.setImageBitmap(bitmap);
                }
            } else if (mindmapTabSelected) {
                Enrichments enrichments = mindmapPageList.get(position);
                int pageno = enrichments.getEnrichmentPageNo() - 1;
                text.setText(page + " " + pageno);
                screenShotPath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + enrichments.getEnrichmentPageNo() + ".png";
                if (new File(screenShotPath).exists()) {
                    Bitmap bitmap = BitmapFactory.decodeFile(screenShotPath);
                    imgViewCover.setImageBitmap(bitmap);
                }
            } else {
                Note note = notePageList.get(position);
                int pageno = note.getPageNo() - 1;
                text.setText(page + " " + pageno);
                screenShotPath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + note.getPageNo() + ".png";
                if (new File(screenShotPath).exists()) {
                    Bitmap bitmap = BitmapFactory.decodeFile(screenShotPath);
                    imgViewCover.setImageBitmap(bitmap);
                }
            }
            return view;
        }

    }


    private class PageListSelectAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (enrichmentTabSelected) {
                if (enrichmentPageList != null) {
                    return enrichmentPageList.size();
                } else {
                    return 0;
                }
            } else if (enrichmentLinkSelected) {
                if (enrichmentPageLinkList != null) {
                    return enrichmentPageLinkList.size();
                } else {
                    return 0;
                }
            } else if (mindmapTabSelected) {
                if (mindmapPageList != null) {
                    return mindmapPageList.size();
                } else {
                    return 0;
                }
            } else {
                if (notePageList != null) {
                    return notePageList.size();
                } else {
                    return 0;
                }
            }
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (enrichmentTabSelected || enrichmentLinkSelected || mindmapTabSelected) {
                //if(view == null){
                LayoutInflater layoutinflater = (LayoutInflater) ExportEnrichmentActivity.this.getSystemService(ExportEnrichmentActivity.this.LAYOUT_INFLATER_SERVICE);
                view = layoutinflater.inflate(R.layout.inflate_checklist, null);
                ViewGroup viewGroup = (RelativeLayout) view.findViewById(R.id.export_checklist_layout);
                //}
                Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(viewGroup,font);
                TextView txtTitle = (TextView) view.findViewById(R.id.textView1);
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
                txtTitle.setTextColor(Color.WHITE);
                if (enrichmentTabSelected) {
                    Enrichments enrichment = enrichmentPageList.get(position);
                    txtTitle.setText(enrichment.getEnrichmentTitle());
                    if (enrichment.isEnrichmentSelected()) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                } else if (mindmapTabSelected) {
                    Enrichments enrichment = mindmapPageList.get(position);
                    txtTitle.setText(enrichment.getEnrichmentTitle());
                    if (enrichment.isEnrichmentSelected()) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                } else {
                    Enrichments enrichment = enrichmentPageLinkList.get(position);
                    txtTitle.setText(enrichment.getEnrichmentTitle());
                    if (enrichment.isEnrichmentSelected()) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                }

            } else {
                //if(view == null){
                LayoutInflater layoutinflater = (LayoutInflater) ExportEnrichmentActivity.this.getSystemService(ExportEnrichmentActivity.this.LAYOUT_INFLATER_SERVICE);
                view = layoutinflater.inflate(R.layout.inflate_slct_users_res, null);
                ViewGroup viewGroup = (LinearLayout) view.findViewById(R.id.select_res_layout);
                //}
                Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(viewGroup,font);
                TextView txtTitle = (TextView) view.findViewById(R.id.textView1);
                TextView txtDesc = (TextView) view.findViewById(R.id.textView2);
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
                txtTitle.setTextColor(Color.WHITE);
                txtDesc.setTextColor(Color.WHITE);
                Note note = notePageList.get(position);
                txtTitle.setText(note.getsText());
                txtDesc.setText(note.getsDesc());
                if (note.isNoteSelected()) {
                    checkBox.setChecked(true);
                } else {
                    checkBox.setChecked(false);
                }
            }
            return view;
        }

    }

    private class groupListAdapter extends BaseAdapter {
        ArrayList<UserGroups> groups = new ArrayList<UserGroups>();
        private ExportEnrichmentActivity bookView;

        public groupListAdapter(ExportEnrichmentActivity bookView,
                                ArrayList<UserGroups> groups2) {
            groups = groups2;
            this.bookView = bookView;

        }

        @Override
        public int getCount() {

            return groups.size();
        }

        @Override
        public Object getItem(int position) {

            return null;
        }

        @Override
        public long getItemId(int position) {

            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            if (convertView == null) {
                Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
                vi = bookView.getLayoutInflater().inflate(R.layout.inflate_checklist, null);
            }
            TextView txtTitle = (TextView) vi.findViewById(R.id.textView1);
            txtTitle.setTextColor(Color.WHITE);
            CheckBox checkBox = (CheckBox) vi.findViewById(R.id.checkBox1);
            UserGroups group = groups.get(position);
            txtTitle.setText(group.getGroupName());
            //tv.setTextColor(Color.rgb(0, 0, 0));
            if (group.isGroupSelected()) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            return vi;
        }
    }


    public void publishmethod() {
        if (isprivate) {
            if (groupSelected) {

                if (selectedGroup.size() > 0) {
                    boolean first = false;
                    for (UserGroups groupslist : selectedGroup) {
                        Groupdetails = db.getselectedgroupdetails(groupslist.getGroupName(), groupslist.getUserScopeId(), ExportEnrichmentActivity.this);
                        for (UserGroups getemail : Groupdetails) {
                            if (!first) {
                                mailid = getemail.getEmailId() + ",";
                                first = true;
                            } else {
                                mailid = mailid.concat(getemail.getEmailId() + ",");
                            }
                        }
                        groupslist.setGroupSelected(false);
                    }
                    if (resourceTitle.trim().length() < 1) {
                        UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.resource_title, R.string.title);

                    } else {
                        String str = ExportEnrichmentActivity.this.getResources().getString(R.string.please_wait);
                        progressDialog = ProgressDialog.show(ExportEnrichmentActivity.this, "", str, true);
                        //new validateLoginAndPublishEnrichments().execute();
                        new validateLoginPublishEnrichments().execute();
                    }
                } else {
                    UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.group_selection, R.string.oops);
                }
            } else {
                mailid = et_mailid.getText().toString();
                if (!UserFunctions.CheckValidEmail(et_mailid.getText().toString())) {
                    UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.input_valid_mail_id, R.string.invalid_email_id);
                } else if (resourceTitle.trim().length() < 1) {
                    UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.resource_title, R.string.title);

                } else {
                    String str = ExportEnrichmentActivity.this.getResources().getString(R.string.please_wait);
                    progressDialog = ProgressDialog.show(ExportEnrichmentActivity.this, "", str, true);
                    new validateLoginPublishEnrichments().execute();
                }
            }
        } else {
            mailid = "";
            if (resourceTitle.trim().length() < 1) {
                UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.resource_title, R.string.title);
            } else {
                String str = ExportEnrichmentActivity.this.getResources().getString(R.string.please_wait);
                progressDialog = ProgressDialog.show(ExportEnrichmentActivity.this, "", str, true);
                new validateLoginPublishEnrichments().execute();
            }
        }

    }

    public void requestAccountsPermissionAndAuthorize(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, MY_PERMISSIONS_REQUEST_GET_ACCOUNTS);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED){
            gDriveExport.Authorize();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == AUTH_CODE_REQUEST_CODE){
            gDriveExport.Authorize();
        }
    }

    @Override
    public void onRequestPermissionsResult ( int requestCode, String[] permissions,
                                             int[] grantResults){
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_GET_ACCOUNTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission Granted
                    gDriveExport.Authorize();
                } else {
                    //Permission denied
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }
                return;
            }
        }
    }

    private class validateLoginPublishEnrichments extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute() {

            if(enrichmentLinkSelected){
                progressDialog.setMessage(getResources().getString(R.string.upload_please_wait));
                genratingXmlforLinksSelected();
                desc="Hello This is My Link";
                filepath=Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/resources.xml";
            }else if(noteTabSelected){
                progressDialog.setMessage(getResources().getString(R.string.upload_please_wait));
                generatingXmlfornoteSelected();
                desc="Hello This is My Note";
                filepath=Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/resources.xml";
            }else if (mindmapTabSelected){
                if (Globals.ISGDRIVEMODE()) {
                    progressDialog.setMessage(getResources().getString(R.string.upload_gdrive_please_wait));
                } else {
                    progressDialog.setMessage(getResources().getString(R.string.upload_please_wait));
                }
                genratingXmlforMindmapSelected();
                desc="Hello This is My Mindmap";
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
                String formattedDate = df.format(c.getTime());
                filepath=Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/"+formattedDate+".zip";
                Zip zip = new Zip();
                String sourcePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/";
                zip.zipFilesForEnrichments(new File(sourcePath), filepath);
            }else{
                if (Globals.ISGDRIVEMODE()) {
                    progressDialog.setMessage(getResources().getString(R.string.upload_gdrive_please_wait));
                } else {
                    progressDialog.setMessage(getResources().getString(R.string.upload_please_wait));
                }
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
                String formattedDate = df.format(c.getTime());
                filepath=Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/"+formattedDate+".zip";
                //filepath=Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/zipFile222.zip";
                generatinginitUpload(filepath);
            }

            enrichZipFilePath = new File(filepath);
        }

        @Override
        protected Void doInBackground(Void... params) {

            if (Globals.ISGDRIVEMODE()) {
                if (enrichmentTabSelected || mindmapTabSelected) {
                    String zipFileName = enrichZipFilePath.getName();
                    String type;
                    if (enrichmentTabSelected){
                        type = "Enrichment";
                    }else{
                        type = "Resource";
                    }
                    gDriveExport = new GDriveExport(ExportEnrichmentActivity.this, enrichZipFilePath.getPath(), zipFileName, mailid, enrichXmlString, dicEnrichments, resourceTitle, progressDialog,type);
                    if (Build.VERSION.SDK_INT >= 23) {
                        requestAccountsPermissionAndAuthorize();
                    } else {
                        gDriveExport.Authorize();
                    }
                } else {
                    gDriveExport = new GDriveExport(ExportEnrichmentActivity.this, mailid, enrichXmlString, resourceTitle, progressDialog);
                    ExportEnrichmentActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gDriveExport.new createJsonValueForResource().execute();
                        }
                    });
                }
            } else {
                if (enrichmentLinkSelected || noteTabSelected || enrichmentTabSelected || mindmapTabSelected) {
                    PublishEnrichmentsAsChunks();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }

        /**
         * Generate all the selected enrichment html pages and and make it as zip and create an xml to prepare for publish or update.
         */
        private void generatinginitUpload(String destPath){
             String[] bookId=currentBook.get_bStoreID().split("M");
             String storBookId=bookId[1];
           // String storBookId = currentBook.get_bStoreID().replace("M", "");
            enrichXmlString = "<Enrichments>";
            if (Globals.ISGDRIVEMODE()) {
                dicEnrichments = new HashMap();
            }
            int i = 0;
            for (Enrichments enrichments : enrichmentSelectedList) {
                if (enrichments.isEnrichmentSelected()) {
                    new GenerateHTML(ExportEnrichmentActivity.this, db, currentBook, String.valueOf(enrichments.getEnrichmentPageNo()), null,false).generateEnrichedPageForExportAndZipIt(enrichments);
                    String fileName = currentBook.getBookID()+"-"+enrichments.getEnrichmentPageNo()+"-"+enrichments.getEnrichmentId()+".zip";

                    int idPage = enrichments.getEnrichmentPageNo();
                    String bookname=currentBook.get_bStoreID().replace("M", "");
                    //bookname=bookname.replace("M", "");

                    enrichXmlString = enrichXmlString.concat("<Enriched IDUser='"+scopeId+"' IDBook='"+storBookId+"' IDPage='"+idPage+"' Title='"+enrichments.getEnrichmentTitle()+"' Password='' isPublished='3' PublishToEnrichementsSite='true' IsEditable='0' file='"+fileName+"' />");
                    if (Globals.ISGDRIVEMODE()) {
                        HashMap<String, String> dicEnr = new HashMap<String, String>();
                        String storeID = "001";
                        String currentDateTime = UserFunctions.getCurrentDateTimeInString();
                        String enrIDGlobal = storeID + "-" + currentBook.getBookID() + "-" + idPage + "-" + enrichments.getEnrichmentId() + "-" + scopeId;
                        dicEnr.put("file1", fileName);
                        dicEnr.put("title", enrichments.getEnrichmentTitle());
                        dicEnr.put("pageNo", String.valueOf(idPage));
                        dicEnr.put("dateTime", currentDateTime);
                        dicEnr.put("enrichmentIDGlobal", enrIDGlobal);
                        String enrKey = "enrichment" + i;
                        dicEnrichments.put(enrKey, dicEnr);
                    }
                    i++;
                }
            }
            enrichXmlString = enrichXmlString.concat("</Enrichments>");

            Zip zip = new Zip();
            String sourcePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/";
            //String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/zipFile222.zip";
            zip.zipFilesForEnrichments(new File(sourcePath), destPath);

        }


       /**
         * Initialize all values to publish book as chunks
         */
        private void PublishEnrichmentsAsChunks(){
            try {
                chunkInputStream = new FileInputStream(filepath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            chunkBaos = new ByteArrayOutputStream();
            chunkBytes = new byte[1024*1024];
            chunkBytesRead = 0;
            chunkNoOfExecution = 1;
            new publishEnrichmentsAsChunks().execute();
        }
    }


    /**
     * Async task class to publish book as chunks
     * @author
     *
     */
    private class publishEnrichmentsAsChunks extends AsyncTask<Void, Void, Void>{

        String bookZipBase64Chunks = null;
        byte[] byteArray = null;
        String base64Binary;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(enrichmentLinkSelected||noteTabSelected||mindmapTabSelected){
                base64Binary=Base64.encodeToString(enrichXmlString.getBytes(), Base64.DEFAULT);
            }else{
                convertFileToBase64BinaryChunks(enrichZipFilePath);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            //System.out.println(chunkBytesRead+"");
           // String storeBookId=currentBook.get_bStoreID().replace("M", "");

            String[] bookId=currentBook.get_bStoreID().split("M");
            String storeBookId=bookId[1];
            if (enrichmentLinkSelected||noteTabSelected||mindmapTabSelected){
                webService.publishResouce(0, scopeId, storeBookId, 0, resourceTitle, desc, 1, 5, base64Binary, mailid, isprivate);
            }
            else if (enrichmentTabSelected){
                if(chunkBytesRead > 0){
                    // String encode=URLEncodedUtils.isEncoded(entity)
                    //bookView.webService.Xmlparser.parseXml(enrichXmlString);
                    webService.newUploadBookAsChunks(scopeId, enrichZipFilePath.getName(), bookZipBase64Chunks, chunkBookFlag);
                }else{
                    webService.PublishNewEnrichedGroup(scopeId, enrichZipFilePath.getName(), enrichXmlString, isprivate, mailid);

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (webService.ResourceId!=0 && enrichmentLinkSelected){
                for(Enrichments enrich:enrichmentSelectedLinkList){
                    enrich.setEnrichmentExportValue(webService.ResourceId);
                    enrich.setEnrichmentSelected(false);
                    db.executeQuery("update enrichments set Exported='"+webService.ResourceId+"' where enrichmentID='"+enrich.getEnrichmentId()+"'");
                }
                UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.publish_completed_successfully, R.string.published);
                File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp");
                UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                progressDialog.dismiss();
            } else if (webService.ResourceId!=0 && mindmapTabSelected){
                for(Enrichments enrich:mindmapSelectedList){
                    enrich.setEnrichmentExportValue(webService.ResourceId);
                    enrich.setEnrichmentSelected(false);
                   db.executeQuery("update enrichments set Exported='"+webService.ResourceId+"' where enrichmentID='"+enrich.getEnrichmentId()+"'");
                }
                UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.publish_completed_successfully, R.string.published);
                File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp");
                UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                progressDialog.dismiss();
            }
            else if(webService.ResourceId!=0 && noteTabSelected){
                for (Note notes : noteSelectedList) {
                    notes.setExportedId(webService.ResourceId);
                    notes.setNoteSelected(false);
                    db.executeQuery("update tblNote set Exported='"+webService.ResourceId+"' where TID='"+notes.getId()+"'");

                }
                UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.publish_completed_successfully, R.string.published);
                File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp");
                UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                progressDialog.dismiss();

            }else if(enrichmentTabSelected){
                if (chunkBytesRead > 0){
                    if (webService.uploadJunkFileIpadResult != "") {
                        if (webService.uploadJunkFileIpadResult.equals("true")) {
                            chunkNoOfExecution++;
                            new publishEnrichmentsAsChunks().execute();
                        } else {
                            progressDialog.dismiss();
                            String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp";

                            File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp");
                            UserFunctions.DeleteDirectory(enrichedPagesFilePath);

                            UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.went_wrong, R.string.oops);
                        }
                    }
                }else{
                    if(webService.enrichedPageResponseServerIdList.size() > 0){
                        for (int i = 0; i < webService.enrichedPageResponseServerIdList.size(); i++) {
                            Enrichments enrichments = enrichmentSelectedList.get(i);
                            int enrichPageServerId = webService.enrichedPageResponseServerIdList.get(i);
                            enrichments.setEnrichmentExportValue(enrichPageServerId);
                            enrichments.setEnrichmentSelected(false);
                            db.executeQuery("update enrichments set Exported='"+enrichPageServerId+"' where enrichmentID='"+enrichments.getEnrichmentId()+"'");
                        }
                        //enrichmentSelectedList.clear();
                        progressDialog.dismiss();
                        File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp");
                        UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                        UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.publish_completed_successfully, R.string.published);

                    }else{
                        progressDialog.dismiss();
                        File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp");
                        UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                        UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.went_wrong, R.string.oops);

                    }
                }
            }
            else {
                progressDialog.dismiss();
                UserFunctions.DisplayAlertDialog(ExportEnrichmentActivity.this, R.string.went_wrong, R.string.oops);
            }
        }


        /**
         * convert file to base64 binary Chunks
         * @param file
         * @return
         */
        private void convertFileToBase64BinaryChunks(File file) {
            try {
                if ((chunkBytesRead = chunkInputStream.read(chunkBytes)) != -1) {
                    ////System.out.println("bytesRead: "+chunkBytesRead+"i: "+i);
                    chunkBaos.write(chunkBytes, 0, chunkBytesRead);     //81703
                    byteArray = chunkBaos.toByteArray();
                    bookZipBase64Chunks = Base64.encodeToString(byteArray, Base64.DEFAULT).trim();
                    if (chunkNoOfExecution == 1) {
                        chunkBookFlag = "Start";
                    } else if (chunkBytesRead < 1024*1024) {
                        chunkBookFlag = "End";
                    } else {

                        chunkBookFlag = "Middle";
                    }
                    chunkBaos.reset();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * generating xml for Notes
     * @return
     */
    private void generatingXmlfornoteSelected() {
        // TODO Auto-generated method stub
        int idPage;
        enrichXmlString = "<Notes>";
        for (Note notes : noteSelectedList) {
            if (notes.isNoteSelected()) {
                enrichXmlString =enrichXmlString.concat("<note>");
                idPage = notes.getPageNo();
                enrichXmlString =enrichXmlString.concat("\n"+currentBook.get_bStoreID()+"|"+idPage+"|"+notes.getsText()+"|"+notes.getOccurence()+"|"+notes.getsDesc()+"|"+notes.getnPos()+"|"+notes.getColor()+"|"+notes.getProcessStext()+"|"+notes.getTabNo()+"\n");
                enrichXmlString =enrichXmlString.concat("</note>");
            }
        }
        enrichXmlString =enrichXmlString.concat( "</Notes>");
    }

    /**
     * generating xml for Links
     * @return
     */
    private void genratingXmlforLinksSelected(){
        int idPage;
        enrichXmlString = "<Links>";
        for (Enrichments enrichments : enrichmentSelectedLinkList) {
            if (enrichments.isEnrichmentSelected()) {
                enrichXmlString =enrichXmlString.concat("<link>");
                idPage = enrichments.getEnrichmentPageNo();
                enrichXmlString =enrichXmlString.concat("\n"+enrichments.getEnrichmentId()+"|"+enrichments.getEnrichmentBid()+"|"+idPage+"|"+enrichments.getEnrichmentTitle()+"|"+enrichments.getEnrichmentType()+"|"+enrichments.getEnrichmentPath()+"|"+enrichments.getEnrichmentExportValue()+"\n");
                enrichXmlString =enrichXmlString.concat("</link>");
            }
        }
        enrichXmlString =enrichXmlString.concat("</Links>");
    }

    /**
     * generating xml for Mindmap
     * @return
     */
    private void genratingXmlforMindmapSelected(){
        int idPage;
        enrichXmlString = "<MindMaps>";
        if (Globals.ISGDRIVEMODE()) {
            dicEnrichments = new HashMap();
        }
        int i = 0;
        for (Enrichments enrichments : mindmapSelectedList) {
            if (enrichments.isEnrichmentSelected()) {
                enrichXmlString =enrichXmlString.concat("<mindmap>");
                String fileName = currentBook.getBookID() + "-" + enrichments.getEnrichmentPageNo() + "-" + enrichments.getEnrichmentId() + ".zip";
                idPage = enrichments.getEnrichmentPageNo();
                String enrPath = enrichments.getEnrichmentPath()+".xml";
                String mindMapPath = Globals.TARGET_BASE_MINDMAP_PATH+enrPath;
                zippingMindmapPageForExport(enrichments.getEnrichmentId(),mindMapPath,enrichments.getEnrichmentPageNo());
                String mindMapContent = UserFunctions.readFileFromPath(new File(mindMapPath));
                mindMapContent = mindMapContent.replace("\n", "");
                mindMapContent = mindMapContent.replace("<", "&lt;");
                mindMapContent = mindMapContent.replace(">", "&gt;");
                enrichXmlString =enrichXmlString.concat("\n"+enrichments.getEnrichmentId()+"|"+enrichments.getEnrichmentBid()+"|"+idPage+"|"+enrichments.getEnrichmentTitle()+"|"+enrichments.getEnrichmentType()+"|"+enrPath+"|"+enrichments.getEnrichmentExportValue()+"|"+mindMapContent+"\n");

                if (Globals.ISGDRIVEMODE()) {
                    HashMap<String, String> dicEnr = new HashMap<String, String>();
                    String storeID = "001";
                    String currentDateTime = UserFunctions.getCurrentDateTimeInString();
                    String enrIDGlobal = storeID + "-" + currentBook.getBookID() + "-" + idPage + "-" + enrichments.getEnrichmentId() + "-" + scopeId;
                    dicEnr.put("file1", fileName);
                    dicEnr.put("title", enrichments.getEnrichmentTitle());
                    dicEnr.put("pageNo", String.valueOf(idPage));
                    dicEnr.put("dateTime", currentDateTime);
                    dicEnr.put("enrichmentIDGlobal", enrIDGlobal);
                    String enrKey = "resource" + i;
                    dicEnrichments.put(enrKey, dicEnr);
                }
                enrichXmlString = enrichXmlString.concat("</mindmap>");
                i++;
            }
        }
        enrichXmlString =enrichXmlString.concat("</MindMaps>");
    }

    private void zippingMindmapPageForExport(int enrichId,String mindMapPath,int pageNumber){
        File file=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp");
        if (file.exists()){
            //file.delete();
            UserFunctions.deleteAllFilesFromDirectory(file);
        }
        String htmlFileDirPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/"+currentBook.getBookID()+"-"+pageNumber+"-"+enrichId;
        UserFunctions.createNewDirectory(htmlFileDirPath);
        String htmlFilePath = htmlFileDirPath+"/mindmap.xml";
        UserFunctions.copyFiles(mindMapPath,htmlFilePath);
        Zip zip = new Zip();
        String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/"+currentBook.getBookID()+"-"+pageNumber+"-"+enrichId+".zip";
        zip.zipFileAtPath(htmlFileDirPath, destPath);
        UserFunctions.DeleteDirectory(new File(htmlFileDirPath));
    }
}
