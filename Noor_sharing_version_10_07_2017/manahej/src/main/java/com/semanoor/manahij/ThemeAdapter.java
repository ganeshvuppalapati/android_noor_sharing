package com.semanoor.manahij;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.UserGroups;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Krishna on 14-10-2016.
 */

public class ThemeAdapter {
    ThemeActivity  exportEnrGroup;
    Dialog exportEnrPopover;
    public ArrayList<UserGroups> userGroups;



    public ThemeAdapter(ThemeActivity _context) {
        this.exportEnrGroup = _context;
    }


   /*
     * Displaying default and User themes list
     */

    public class ThemesItemAdapter extends RecyclerView.Adapter<ThemesItemAdapter.MyViewHolder> {
        ThemeActivity themeActivity;
        RecyclerView recycler;
        ArrayList<Theme> themesList;

         public ThemesItemAdapter( RecyclerView export_shelf, ThemeActivity themActivity, ArrayList<Theme> themes) {
             themeActivity=themActivity;
             recycler=export_shelf;
             themesList=themes;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView tv_Title;
            ImageView bookImg;
            CircleButton btn_delete;


            public MyViewHolder(View view) {
                super(view);

                bookImg = (ImageView) view.findViewById(R.id.imageView2);
                tv_Title = (TextView) view.findViewById(R.id.textView1);
                btn_delete=(CircleButton) view.findViewById(R.id.btn_delete);
            }
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.theme_images, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            final Theme themes = themesList.get(position);
            holder.tv_Title.setText(themes.getThemeName());
            String imageUrl= Globals.TARGET_BASE_FILE_PATH+"Themes/"+themes.getThemeID()+"/.shelf_bg.png";
            Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);
            holder.bookImg.setImageBitmap(bitmap);
            if(position!=0){
              holder.btn_delete.setVisibility(View.VISIBLE);
            }else{
               holder.btn_delete.setVisibility(View.GONE);
            }

            holder.btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(themeActivity);
                    builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            themeActivity.db.executeQuery("delete from Themes where themeID='" +themes.getThemeID() + "'");
                            themeActivity.themesList.remove(themes);
                            String newFolder= Globals.TARGET_BASE_FILE_PATH+"Themes/"+themes.getThemeID();
                            UserFunctions.DeleteDirectory(new File(newFolder));
                            recycler.getAdapter().notifyDataSetChanged();
                        }
                    });

                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();

                        }
                    });
                    builder.setTitle(R.string.delete_page);
                    builder.setMessage(R.string.suredelete);
                    builder.show();


                }
            });
            holder.bookImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                           ThemeAdapter enr = new ThemeAdapter(themeActivity);
                           //themeId= themes.getThemeID();
                           themeActivity.filePath = Globals.TARGET_BASE_FILE_PATH + "Themes/" + themes.getThemeID();
                           themeActivity.li_edit_image.setVisibility(View.VISIBLE);
                           if(themes.getThemeID()==0) {
                               themeActivity.rl_camera.setVisibility(View.INVISIBLE);
                               themeActivity.rl_galary.setVisibility(View.INVISIBLE);
                               themeActivity.rl_search.setVisibility(View.INVISIBLE);
                           }else{

                               themeActivity.rl_camera.setVisibility(View.VISIBLE);
                               themeActivity.rl_galary.setVisibility(View.VISIBLE);
                               themeActivity.rl_search.setVisibility(View.VISIBLE);
                           }
                           themeActivity.btn_add.setVisibility(View.INVISIBLE);
                           themeActivity.adapterFiles = themeActivity.getFilesList(themeActivity.filePath);

                           recycler.setAdapter(enr.new ThemesListAdapter(recycler, themeActivity, themeActivity.adapterFiles));
                           recycler.setHasFixedSize(true);
                           themeActivity.selectedFile=themeActivity.adapterFiles[0];
                           // String imageUrl= filePath+"/"+selectedFile.getName();
                           themeActivity.displayingImages(themeActivity.selectedFile.getAbsolutePath());
                           RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(themeActivity, LinearLayoutManager.HORIZONTAL, false);
                           recycler.setLayoutManager(mLayoutManager);
                           // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
                           recycler.setItemAnimator(new DefaultItemAnimator());
                }
            });

        }

        @Override
        public int getItemCount() {
            return themesList.size();
        }
    }

      /*
     * Displaying user shelfs (Groups and UserContacts)
     */

    public class ThemesListAdapter extends RecyclerView.Adapter<ThemesListAdapter.MyViewHolder> {
        ThemeActivity themeActivity;
        RecyclerView recycler;
        int themeId;
        ArrayList<Theme> themesList;
        File file;
        File[] adapterFiles;

        public ThemesListAdapter(RecyclerView export_shelf, ThemeActivity themActivity, File[] themes) {
            themeActivity=themActivity;
            recycler=export_shelf;
            adapterFiles=themes;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView tv_Title;
            ImageView bookImg;


            public MyViewHolder(View view) {
                super(view);
                // export_shelf = (RecyclerView) view.findViewById(R.id.gridView1);
                bookImg = (ImageView) view.findViewById(R.id.imageView2);
                tv_Title = (TextView) view.findViewById(R.id.textView1);

            }
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.theme_images, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            final File f=adapterFiles[position];
            final String fileName=f.getName();
               String[] split=f.getAbsolutePath().split(f.getName());
               String dotImages=split[0]+"."+f.getName();
               Bitmap bitmap = BitmapFactory.decodeFile(dotImages);
               Bitmap b=Bitmap.createScaledBitmap(bitmap,(int) themeActivity.getResources().getDimension(R.dimen.shelf_catimg_width),(int) themeActivity.getResources().getDimension(R.dimen.shelf_catimg_height),true);
               holder.bookImg.setImageBitmap(b);

               if(fileName.contains("cloud")){
                   holder.tv_Title.setText("Cloud");
               }else  if(fileName.contains("group")){
                   holder.tv_Title.setText("Group");
               }else  if(fileName.contains("inbox")){
                   holder.tv_Title.setText("Inbox");
               }else  if(fileName.contains("shelf")){
                   holder.tv_Title.setText("Shelf");
               }else  if(fileName.contains("store")){
                   holder.tv_Title.setText("Store");
               }else  if(fileName.contains("themes")){
                   holder.tv_Title.setText("Themes");
               }else {
                   String[] filename = new String[2];
                   if (fileName.contains("png")) {
                       filename = fileName.split(".png");
                   } else if (fileName.contains("jpg")) {
                       filename = fileName.split(".jpg");
                   }
                   if (filename[0].contains("_")) {
                       String[] file = filename[0].split("_");
                       //String extractName=file[0].replace(".","");
                       holder.tv_Title.setText(file[0]);
                   } else {
                       //String extractName=filename[0].replace(".","");
                       holder.tv_Title.setText(filename[0]);
                   }
               }

             holder.bookImg.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {

                     themeActivity.selectedFile=f;
                    // String imageUrl= filePath+"/"+selectedFile.getName();
                     themeActivity.displayingImages(f.getAbsolutePath());
                 }
             });

        }

        @Override
        public int getItemCount() {
            return adapterFiles.length;
        }
    }


}
