package com.semanoor.manahij;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Button;

import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.inappbilling.util.IabResult;
import com.semanoor.inappbilling.util.Inventory;
import com.semanoor.inappbilling.util.Purchase;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Suriya on 24/02/16.
 */
public class Subscription {

    private static Subscription instance;
    private Context ctxPurchase;
    public IabHelper mHelper;
    private boolean mSubscribedToBooks = false;
    private String storeBookId;
    public boolean isBookPurchased;
    private Enrichment enrichmentStore;
    //private WebService webService;

    public static synchronized Subscription getInstance(){
        if(instance == null){
            instance = new Subscription();
        }
        return instance;
    }

    public class SaveSubscriptionGroups extends AsyncTask<Void, Void, Void>{

        String emailId;
        long days;
        Purchase purchase;
        String response;
        Context context;
        ProgressDialog processDialog;

        public SaveSubscriptionGroups(Purchase purchase,Context context, Enrichment enrichmentStre){
            this.purchase = purchase;
            this.context = context;
            enrichmentStore = enrichmentStre;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            processDialog = UserFunctions.displayProcessDialog(context, context.getResources().getString(R.string.subscript_loading));
            String strTransDate = UserFunctions.milisecondstoDate(purchase.getPurchaseTime());
            String strEndDate = UserFunctions.milisecondstoEndDate(purchase.getPurchaseTime(),enrichmentStore.fa_enrich.visitedStore.getPeriod());
            long days = UserFunctions.dateDifference(strTransDate, strEndDate);
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            String emailId = sharedPrefs.getString(Globals.sUserEmailIdKey, "");
            //String emailId = "ksuriya@semasoft.co.in";
            this.emailId = emailId;
            this.days = days;
        }

        @Override
        protected Void doInBackground(Void... params) {
            saveSubscriptionGroup(emailId, days, enrichmentStore.fa_enrich.visitedStore.getInAppId(), UserFunctions.getMacAddress(context), purchase.getOriginalJson());
         //   saveSubscriptionGroup("karthik@semasoft.co.in", days, enrichmentStore.fa_enrich.visitedStore.getInAppId(), UserFunctions.getMacAddress(context), purchase.getOriginalJson());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //UserFunctions.alert(response, context);
            if (response != null && !response.equals("")) {
                //UserFunctions.alert(response, context);
                parseXml(response, context);
            }
            if (processDialog != null) {
                processDialog.dismiss();
            }
            if (context instanceof StoreViewActivity){
                ((StoreViewActivity)context).intialiseViewPager(((StoreViewActivity) context).mViewPager);
                ((StoreViewActivity)context).tabLayout.setupWithViewPager(((StoreViewActivity) context).mViewPager);
                ((StoreViewActivity)context).setUpIcon();
                ((StoreViewActivity)context).mViewPager.setCurrentItem(enrichmentStore.storeTabPosition);
            }
        }

        private void saveSubscriptionGroup(String emailId, long durationInDays, String productId, String macId, String activationCode) {
            int maskEmailID = 1;
            int activationType = 0;
            String group = productId;
            String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"+
                    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"+
                    "<soap:Body>\n"+
                    "<saveSubscribedGroup xmlns=\"http://semanoor.com.sa/\">\n"+
                    "<iDuration>"+durationInDays+"</iDuration>\n"+
                    "<sAppStoreID>"+productId+"</sAppStoreID>\n"+
                    "<sActivationEmailid>"+emailId+"</sActivationEmailid> \n"+
                    "<iActivationEmailid_MaskID>"+maskEmailID+"</iActivationEmailid_MaskID>\n"+
                    "<sActivationMac_ID>"+macId+"</sActivationMac_ID>\n"+
                    "<iActivation_Type>"+activationType+"</iActivation_Type>\n"+
                    "<sActivationcode>"+activationCode+"</sActivationcode>\n"+
                    "<Group>"+group+"</Group>\n"+
                    "</saveSubscribedGroup>\n"+
                    "</soap:Body>\n"+
                    "</soap:Envelope>\n";

            String soapAction = "http://semanoor.com.sa/saveSubscribedGroup";
            response = CallWebService(Globals.subscriptionWebserviceURL, soapAction, soapEnvelope);
            //System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + response);

        }
    }

    public class getSubscribedId extends AsyncTask<Void, Void, Void> {
        String emailId;
        String response;
        Context context;
        String inAppId;

        public getSubscribedId(Context context,String productId){
            this.context = context;
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            String emailId = sharedPrefs.getString(Globals.sUserEmailIdKey, "");
            //String emailId = "ksuriya@semasoft.co.in";
            this.emailId = emailId;
            this.inAppId = productId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            getSubscribedId(inAppId, emailId, UserFunctions.getMacAddress(context));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (response != null && !response.equals("")) {
                parseXml(response, context);
            }
            initializeIABHelper(context, true);
        }

        private void getSubscribedId(String productId, String emailId, String macId){
            int activationType = 0;
            int maskEmailId = 1;
            String activationCode = "";

            String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"+
                    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"+
                    "<soap:Body>\n"+
                    "<getSubScribedGroup xmlns=\"http://semanoor.com.sa/\">\n"+
                    "<iActivation_Type>"+activationType+"</iActivation_Type>\n"+
                    "<sAppStoreID>"+productId+"</sAppStoreID>\n"+
                    "<sEmailId>"+emailId+"</sEmailId> \n"+
                    "<iEmailMaskId>"+maskEmailId+"</iEmailMaskId>\n"+
                    "<sMac_ID>"+macId+"</sMac_ID>\n"+
                    "<sActivationcode>"+activationCode+"</sActivationcode>\n"+
                    "</getSubScribedGroup>\n"+
                    "</soap:Body>\n"+
                    "</soap:Envelope>\n";
            String soapAction = "http://semanoor.com.sa/getSubScribedGroup";
            response = CallWebService(Globals.subscriptionWebserviceURL, soapAction, soapEnvelope);
            //response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body><saveSubscribedGroupResponse xmlns=\"http://semanoor.com.sa/\"><saveSubscribedGroupResult>true:<Subscription><Sub ID=\"3\" AppstoreID=\"scoo1\" StartDate=\"2/27/2016 1:34:14 PM\" EndDate=\"3/27/2016 1:34:14 PM\" Duration=\"29\" NoOfDaysLeft=\"29\" /></Subscription></saveSubscribedGroupResult></saveSubscribedGroupResponse></soap:Body></soap:Envelope>";
        }
    }


    private String CallWebService(String url, String sOAPAction, String eNVELOPE){
        HttpURLConnection urlConnection = null;
        String returnResponse = "";
        try {
            URL urlToRequest = new URL(url);

            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(25000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            urlConnection.setRequestProperty("SOAPAction", sOAPAction);

            OutputStream out = urlConnection.getOutputStream();
            out.write(eNVELOPE.getBytes());
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

            } else if (statusCode != HttpURLConnection.HTTP_OK) {

            }

            InputStream in = urlConnection.getInputStream();
            returnResponse = UserFunctions.convertStreamToString(in);

            if (returnResponse == null || returnResponse.equals("")){
                return returnResponse;
            }

            returnResponse = returnResponse.replace("&lt;", "<").replace("&gt;", ">").replace("<?xml version='1.0' standalone='yes' ?>", "").replace("<?xml version=\"1.0\" encoding=\"utf-8\" ?>", "").replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");

        } catch (MalformedURLException e) {

        } catch (SocketTimeoutException e) {

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return returnResponse;
    }

    /*
    *  Parse Xml method for parsing the return string of Webservice calls
    */
    private void parseXml(String returnXml, Context context) {
        try {
            SAXParserFactory mySAXParserFactory = SAXParserFactory.newInstance();
            SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
            XMLReader myXMLReader = mySAXParser.getXMLReader();
            MyXmlHandler myRSSHandler = new MyXmlHandler(context);
            myXMLReader.setContentHandler(myRSSHandler);
            InputSource inputsource = new InputSource();
            inputsource.setEncoding("UTF-8");
            inputsource.setCharacterStream(new StringReader(returnXml));
            myXMLReader.parse(inputsource);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class MyXmlHandler extends DefaultHandler {

        private Context context;

        public MyXmlHandler(Context context){
            this.context = context;
        }

        String tempString;
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);
            if (localName.equals("Sub")) {
                String id = attributes.getValue("ID");
                String appStoreId = attributes.getValue("AppstoreID");
                String startDate = attributes.getValue("StartDate");
                String endDate = attributes.getValue("EndDate");
                String daysLeft = attributes.getValue("NoOfDaysLeft");
                updateSharedPreference(appStoreId, startDate, endDate, daysLeft, context);
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put("ID",id);
                hashMap.put("AppstoreID",appStoreId);
                hashMap.put("StartDate",startDate);
                hashMap.put("EndDate",endDate);
                hashMap.put("daysLeft",daysLeft);
                updateSubscriptionListToSharedPreferences(Globals.SUBSLIST,hashMap,context);
        }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            super.characters(ch, start, length);
            tempString = new String(ch, start, length);
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            super.endElement(uri, localName, qName);
            if (localName.equals("saveSubscribedGroupResult") || localName.equals("getSubScribedGroupResult")) {
                if (tempString.contains("false")){
                    UserFunctions.DisplayAlertDialog(context, R.string.subscription, R.string.receipt_validate);
                } else if (tempString.contains("exceed")) {
                    SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
                    SharedPreferences.Editor editor = sharedPreference.edit();
                    editor.putString(Globals.SUBSCRIPTENDDATEKEY, context.getResources().getString(R.string.exceed_device_limit));
                    editor.putString(Globals.SUBSCRIPTSTARTDATEKEY, "");
                    editor.putString(Globals.SUBSCRIPTDAYSLEFT, "0");
                    editor.commit();
                    if (context instanceof StoreViewActivity) {
                        if (enrichmentStore != null) {
                            enrichmentStore.updateSubsButton();
                        }
                    }
                }
            }
        }
    }

    private void updateSubscriptionListToSharedPreferences(String key,HashMap<String,String> hashMap,Context context){
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreference.edit();
        ArrayList<HashMap<String,String>> existList = null;
        try {
            existList = (ArrayList<HashMap<String,String>>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<HashMap<String,String>>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (UserFunctions.checkClientIdExist(existList,hashMap.get("AppstoreID"))) {
            for (int i=0;i<existList.size();i++){
                HashMap<String,String> hashMap1 = existList.get(i);
                if (hashMap.get("AppstoreID").equalsIgnoreCase(hashMap1.get("AppstoreID"))){
                    existList.remove(i);
                    existList.add(hashMap);
                }
            }
        }else{
            existList.add(hashMap);
        }
        try {
            editor.putString(key, ObjectSerializer.serialize(existList));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        editor.commit();
    }

    private void updateSharedPreference(String appId, String startDate, String endDate, String remainingDays, Context context){
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(Globals.SUBSCRIPTSTARTDATEKEY, startDate);
        editor.putString(Globals.SUBSCRIPTENDDATEKEY, endDate);
        editor.putString(Globals.SUBSCRIPTDAYSLEFT, remainingDays);
        editor.commit();
        if (context instanceof StoreViewActivity) {
            if (enrichmentStore != null) {
                enrichmentStore.updateSubsButton();
            }
        }
    }

    public void initializeIABHelper(final Context ctxPurchase, final boolean queryForSubscription){
        this.ctxPurchase = ctxPurchase;
        mHelper = new IabHelper(ctxPurchase, Globals.manahijBase64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(false);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        //Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                //Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    UserFunctions.complain("Problem setting up in-app billing: " + result, ctxPurchase);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                //Log.d(TAG, "Setup successful. Querying inventory.");
                if (queryForSubscription) {
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                }
            }
        });
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            //Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
             //   UserFunctions.complain("Failed to query inventory: " + result, ctxPurchase);
                return;
            }

            //Log.d(TAG, "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            // Do we have the premium upgrade?
			Purchase bookPurchase = inventory.getPurchase(storeBookId);
			isBookPurchased = (bookPurchase != null);
			//Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));

            // Do we have the infinite gas plan?
            Purchase subscriptionPurchase = inventory.getPurchase(Globals.SUBSCRIPTION_INAPP_PURCHASE_PRODUCTID);

            mSubscribedToBooks = (subscriptionPurchase != null);
            //Log.d(TAG, "User " + (mSubscribedToInfiniteGas ? "HAS" : "DOES NOT HAVE") + " infinite gas subscription.");
            if (mSubscribedToBooks) {
                mHelper.consumeAsync(subscriptionPurchase, mConsumeFinishedListener);
				/*String strTransDate = milisecondstoDate(subscriptionPurchase.getPurchaseTime());
				String strEndDate = milisecondstoEndDate(subscriptionPurchase.getPurchaseTime());
				//long days = milisecondsTodays(infiniteGasPurchase.getPurchaseTime());
				Subscription subscription = new Subscription(StoreTabViewFragment.this);
				subscription.new SaveSubscriptionGroups(subscriptionPurchase).execute();*/
            }

            // Check for gas delivery -- if we own gas, we should fill up the tank immediately
			/*Purchase gasPurchase = inventory.getPurchase(SKU_GAS);
			if (gasPurchase != null && verifyDeveloperPayload(gasPurchase)) {
				Log.d(TAG, "We have gas. Consuming it.");
				mHelper.consumeAsync(inventory.getPurchase(SKU_GAS), mConsumeFinishedListener);
				return;
			}*/

            //updateUi();
            //setWaitScreen(false);
            //Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isSuccess()) {
                ////System.out.println("Success");
                //UserFunctions.DisplayAlertDialog(fa_enrich, R.string.purchase_completed_successfully, R.string.purchased);
                //startDownload();
                if (purchase.getSku().equals(Globals.SUBSCRIPTION_INAPP_PURCHASE_PRODUCTID)) {
                    //UserFunctions.alert(purchase.getOriginalJson(), ctxPurchase);
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxPurchase);
                    int subscriptDays = Integer.parseInt(prefs.getString(Globals.SUBSCRIPTDAYSLEFT, "0"));
                    if (subscriptDays <= 0) {
                 //       new SaveSubscriptionGroups(purchase,ctxPurchase, null).execute();
                    }
                }
            } else {
                ////System.out.println("Failed");
                //UserFunctions.DisplayAlertDialog(fa_enrich, result.getMessage(), R.string.purchase_failed);
                UserFunctions.complain("Error while consuming: " + result, ctxPurchase);
            }
        }
    };

    /**
     * Check ForBooks Downloaded
     */
    public void checkForStoreBooksPurchased(String storeBookId){
        this.storeBookId = storeBookId;
        if (mHelper == null) return;
        mHelper.queryInventoryAsync(mGotInventoryListener);
    }

    /** Verifies the developer payload of a purchase. */
    public boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }
}
