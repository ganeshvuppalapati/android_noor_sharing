package com.semanoor.manahij;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.Zip;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Krishna on 04/08/15.
 */
public class MyFileManager {

    private String filesDirPath;
    private FileManagerActivity context;
    private File[] searchFilesList;

    public MyFileManager(String filesDirPath, FileManagerActivity Activity) {
        this.filesDirPath = filesDirPath;
        this.context = Activity;
    }

    /*
     * List of files in selected folders
     */
    public File[] getFilesList() {
        File f = new File(filesDirPath);
        File file = new File(filesDirPath);
        //mainFile.listFiles();
        // String[]filelist=f.list();
        if (context.folders.size() > 0) {
            context.folders.clear();
        }
        File[] currentFiles = f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return !file.isHidden();
            }
        });
        for (int i = 0; i < currentFiles.length; i++) {
            if (currentFiles[i].isDirectory()) {
                ClickedFilePath folders = new ClickedFilePath(context);
                folders.setFolderId(i);
                folders.setFolderPath(currentFiles[i].getPath());
                folders.setFolderTitle(currentFiles[i].getName());
                folders.setFolderSelected(false);
                folders.setFolder(true);
                folders.setFile(false);
                context.folders.add(folders);
            } else if (currentFiles[i].isFile()) {
                ClickedFilePath folders = new ClickedFilePath(context);
                folders.setFolderId(i);
                folders.setFolderPath(currentFiles[i].getPath());
                folders.setFolderTitle(currentFiles[i].getName());
                folders.setFolderSelected(false);
                folders.setFolder(false);
                folders.setFile(true);
                context.folders.add(folders);
            }

        }

        context.SelectedFile = null;
      /*  File[] unHiddenFiles=f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return !file.isHidden();
            }


        });*/
        return f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return !file.isHidden();
            }
        });
    }


    public File[] getSearchFilesList(ArrayList<String> searchArray) {
        File files;
        if (context.folders.size() > 0) {
            context.folders.clear();
        }
        File[] adapterFiles = new File[searchArray.size()];
        for (int i = 0; i < searchArray.size(); i++) {
            String file = filesDirPath + "/" + searchArray.get(i);
            files = new File(file);
            if (files.isDirectory()) {
                ClickedFilePath folders = new ClickedFilePath(context);
                folders.setFolderId(i);
                folders.setFolderPath(files.getPath());
                folders.setFolderTitle(files.getName());
                folders.setFolderSelected(false);
                folders.setFolder(true);
                folders.setFile(false);
                context.folders.add(folders);
            } else if (files.isFile()) {
                ClickedFilePath folders = new ClickedFilePath(context);
                folders.setFolderId(i);
                folders.setFolderPath(files.getPath());
                folders.setFolderTitle(files.getName());
                folders.setFolderSelected(false);
                folders.setFolder(false);
                folders.setFile(true);
                context.folders.add(folders);
            }
            adapterFiles[i] = files;
        }

        return adapterFiles;
    }

    /*
     *Dialog for creating folder
     */
    public void showFolderDialog() {
        final Dialog folderDialog = new Dialog(context);
        folderDialog.setTitle(context.getResources().getString(R.string.create_folder));
        folderDialog.setContentView(context.getLayoutInflater().inflate(R.layout.add_folder, null));
        folderDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(300, context), RelativeLayout.LayoutParams.WRAP_CONTENT);
        final EditText et_name = (EditText) folderDialog.findViewById(R.id.et_name);
        Button btn1 = (Button) folderDialog.findViewById(R.id.add);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String folderName = et_name.getText().toString();
                if (!folderName.equals("")) {
                    // File mydir = new File(filesDirPath + "/" + folderName);
                    context.userFunctions.createNewDirectory(filesDirPath + "/" + folderName);
                    context.gridView.setAdapter(context.new FileManagerGridAdapter(filesDirPath));
                }
                folderDialog.dismiss();
            }
        });

        folderDialog.show();
    }

    /*
     *Dialog to pick image,video and audio
     */
    public void showGalleryDialog() {
        final Dialog pickFilesFromGallery = new Dialog(context);
        //  pickFilesFromGallery.setTitle(context.getResources().getString(R.string.pick_file));
        pickFilesFromGallery.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pickFilesFromGallery.setContentView(context.getLayoutInflater().inflate(R.layout.add_image, null));
        pickFilesFromGallery.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, context), RelativeLayout.LayoutParams.WRAP_CONTENT);
        Button btn_Pick_Image = (Button) pickFilesFromGallery.findViewById(R.id.gallery);
        Button btn_Pick_Audio = (Button) pickFilesFromGallery.findViewById(R.id.audio);
        Button btn_Pick_Video = (Button) pickFilesFromGallery.findViewById(R.id.video);
        btn_Pick_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                //  context.startActivityForResult(intent, context.pick_up_image);
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                    galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                }
                context.startActivityForResult(galleryIntent, context.pick_up_image);
                pickFilesFromGallery.dismiss();
            }
        });
        btn_Pick_Video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent videoGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                videoGalleryIntent.setType("video/*");
                context.startActivityForResult(videoGalleryIntent, context.VIDEO_GALLERY);
                pickFilesFromGallery.dismiss();
            }
        });
        btn_Pick_Audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                context.startActivityForResult(intent, context.pick_Audio);
                //context.startActivityForResult(galleryIntent, context.pick_Audio);
                pickFilesFromGallery.dismiss();
            }
        });
        Button btn_cancel = (Button) pickFilesFromGallery.findViewById(R.id.btnCancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickFilesFromGallery.dismiss();
            }
        });
        pickFilesFromGallery.show();
    }

    /*
     * Image Preview
     */
    public void ImagePreview(File currentFile) {
        View iv_image = LayoutInflater.from(context).inflate(R.layout.preview_image, null);
        ImageView iv = (ImageView) iv_image.findViewById(R.id.imageView2);
        iv.setBackgroundResource(0);
        String fileName = filesDirPath + "/." + currentFile.getName();
        final Bitmap bitmap = BitmapFactory.decodeFile(currentFile.getPath());
        Bitmap b = bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(500, context), Globals.getDeviceIndependentPixels(500, context), false);
        iv.setImageBitmap(b);
        final PopupWindow popup = new PopupWindow();
        popup.setContentView(iv_image);
        popup.setWidth(Globals.getDeviceIndependentPixels(500, context));
        popup.setHeight(Globals.getDeviceIndependentPixels(500, context));
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new ColorDrawable(R.color.tabTransparent));
        popup.showAtLocation(iv_image, Gravity.CENTER, 0, 0);
        popup.setOutsideTouchable(false);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
            }
        });
    }

    /*

     *Audio preview
     */
    public void audioPreview(File currentFile) {
        //final Handler handler=new Handler();
        //boolean playAudio = false;
        View iv_image = LayoutInflater.from(context).inflate(R.layout.preview_audio, null);
        final Button play = (Button) iv_image.findViewById(R.id.btn_play);
        final SeekBar audio_progress = (SeekBar) iv_image.findViewById(R.id.seekBar);
        ImageView iv = (ImageView) iv_image.findViewById(R.id.imageView);

        final PopupWindow popup = new PopupWindow();
        popup.setContentView(iv_image);
        popup.setWidth(Globals.getDeviceIndependentPixels(500, context));
        popup.setHeight(Globals.getDeviceIndependentPixels(500, context));
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new ColorDrawable(R.color.white));
        popup.showAtLocation(iv_image, Gravity.CENTER, 0, 0);
        popup.setOutsideTouchable(false);
        final MediaPlayer mp = new MediaPlayer();

        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
                mp.stop();
            }
        });

        //  mc = new MediaController(FileManagerActivity.this);
        // mc.setAnchorView(iv);
        // mc.show();
        //MediaPlayer mediaPlayer = mp.create(FileManagerActivity.this, files[position].getAbsolutePath());
        try {
            mp.setDataSource(currentFile.getAbsolutePath());
            mp.prepare();
            audio_progress.setMax(mp.getDuration());
            //audio_progress.setProgress(mp.getDuration());
            //audio_progress.setMax(mp.get);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                audio_progress.setProgress(mediaPlayer.getCurrentPosition());
                // String p=context.getResources().getString(R.string.play).toString();
                if (play.getText().toString().equals(context.getResources().getString(R.string.stop).toString())) {
                    play.setText(context.getResources().getString(R.string.play));
                }

            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mp.isPlaying()) {
                    //mp.getSelectedTrack(mp.getCurrentPosition());
                    mp.start();
                    audio_progress.setProgress(mp.getCurrentPosition());
                    //audio_progress.setMax(mp.getDuration());
                    play.setSelected(true);
                    play.setText(context.getResources().getString(R.string.stop));
                    audio_progress.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            audio_progress.setProgress(mp.getCurrentPosition());
                        }
                    }, 1000);
                } else {
                    play.setText(context.getResources().getString(R.string.play));
                    mp.pause();
                    play.setSelected(false);
                    audio_progress.setProgress(mp.getCurrentPosition());
                    //audio_progress.setMax(mp.getDuration());

                }
                // mp.start();
            }
        });
        audio_progress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, final int i, boolean fromUser) {
                //mp.seekTo(i);
                audio_progress.setProgress(i);
                // progressUpdate(mp, audio_progress, play);
                //while(mp.getCurrentPosition())
                audio_progress.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        audio_progress.setProgress(mp.getCurrentPosition());
                    }
                }, 1000);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                System.out.println("Haiiiiiiiii");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int s = seekBar.getProgress();
                mp.seekTo(s);
                //audio_progress.setProgress();
            }
        });


    }

    /*
     *Cut,copy.paste and delete for selected file
     */
    public void FileOperation(boolean Copy, boolean Cut, boolean Paste, boolean Delete) {

        File[] files = getFilesList();
        File f = new File(filesDirPath + "/" + context.fileName);
        if (files.length > 0 && f.exists()) {
            if (Copy) {
                context.FilePath = context.copiedPath;
                context.copiedFileName = context.fileName;
                context.cutOperation = false;
                Toast.makeText(context, R.string.file_copied, Toast.LENGTH_SHORT).show();
            } else if (Cut) {
                context.FilePath = context.copiedPath;
                context.copiedFileName = context.fileName;
                context.cutOperation = true;
                // System.out.println("Helloo");
                Toast.makeText(context, R.string.file_cut, Toast.LENGTH_SHORT).show();
            } else if (Delete) {
                f.delete();
                File hiddenFile = new File(filesDirPath + "/." + context.fileName);
                hiddenFile.delete();
                //invalidateGridview();
                context.gridView.setAdapter(context.new FileManagerGridAdapter(filesDirPath));
                Toast.makeText(context, R.string.file_deleted, Toast.LENGTH_SHORT).show();
            }
        }
        if (Paste) {
            File file = new File(context.FilePath);
            File currentFile = new File(filesDirPath + "/" + context.copiedFileName);
            if (!currentFile.exists()) {
                if (context.cutOperation) {
                    String[] split = context.FilePath.split("/");
                    String fullName = context.FilePath.replace(split[split.length - 1], "/." + split[split.length - 1]);
                    context.userFunctions.copyFiles(fullName, filesDirPath + "/." + context.copiedFileName);
                    context.userFunctions.copyFiles(context.FilePath, filesDirPath + "/" + context.copiedFileName);
                    context.cutOperation = false;
                    file.delete();
                    File hiddenPath = new File(fullName);
                    hiddenPath.delete();
                    context.gridView.setAdapter(context.new FileManagerGridAdapter(filesDirPath));
                    //invalidateGridview();
                    Toast.makeText(context, R.string.file_pasted, Toast.LENGTH_SHORT).show();

                } else {
                    context.userFunctions.copyFiles(context.FilePath, filesDirPath + "/" + context.copiedFileName);
                    String[] split = context.FilePath.split("/");
                    String fullName = context.FilePath.replace(split[split.length - 1], "/." + split[split.length - 1]);
                    context.userFunctions.copyFiles(context.FilePath, filesDirPath + "/" + context.copiedFileName);
                    context.userFunctions.copyFiles(fullName, filesDirPath + "/." + context.copiedFileName);
                    context.gridView.setAdapter(context.new FileManagerGridAdapter(filesDirPath));
                    //invalidateGridview();
                    Toast.makeText(context, R.string.file_pasted, Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(context, R.string.file_exists, Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*
     *Cut,copy,paste,delete in multiple selection
     */
    public void multipleFileOperation(boolean Copy, boolean Cut, boolean Paste, boolean Delete) {
        if (Copy) {
            context.cutOperation = false;
            Toast.makeText(context, R.string.file_copied, Toast.LENGTH_SHORT).show();
            if (context.multipleSelection) {
                context.multipleSelection = false;
            }
            invalidateGridview();
            //context.gridView.setAdapter(context.new FileManagerGridAdapter(filesDirPath));
        } else if (Cut) {
            context.cutOperation = true;
            System.out.println("Helloo");
            Toast.makeText(context, R.string.file_cut, Toast.LENGTH_SHORT).show();
            if (context.multipleSelection) {
                context.multipleSelection = false;
            }
            invalidateGridview();
            //context.gridView.setAdapter(context.new FileManagerGridAdapter(filesDirPath));
        } else if (Delete) {
            for (ClickedFilePath currentDeleteFolder : context.selectedFolders) {
                File folderPath = new File(currentDeleteFolder.getFolderPath());
                if (folderPath.isDirectory()) {
                    context.userFunctions.DeleteDirectory(folderPath);
                } else {
                    folderPath.delete();
                }
            }
            if (context.multipleSelection) {
                context.multipleSelection = false;
            }
            invalidateGridviewAfterDeleting();
            context.selectedFolders.clear();

            Toast.makeText(context, R.string.file_deleted, Toast.LENGTH_SHORT).show();
        }

        if (Paste) {
            if (context.cutOperation) {
                for (ClickedFilePath currentDeleteFolder : context.selectedFolders) {
                    File folderPath = new File(currentDeleteFolder.getFolderPath());
                    File currentFile = new File(filesDirPath + "/" + currentDeleteFolder.getFolderTitle());
                    if (!currentFile.exists()) {
                        if (folderPath.isDirectory()) {
                            //context.userFunctions.createNewBooksDirAndCoptTemplates(currentDeleteFolder.getFolderPath(), filesDirPath + "/" + currentDeleteFolder.getFolderTitle());

                            try {
                                context.userFunctions.copyDirectory(folderPath, currentFile);
                                context.userFunctions.DeleteDirectory(folderPath);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } else {
                            String[] split = currentDeleteFolder.getFolderPath().split("/");
                            String fullName = currentDeleteFolder.getFolderPath().replace(split[split.length - 1], "." + split[split.length - 1]);
                            context.userFunctions.copyFiles(currentDeleteFolder.getFolderPath(), filesDirPath + "/" + currentDeleteFolder.getFolderTitle());
                            context.userFunctions.copyFiles(fullName, filesDirPath + "/." + currentDeleteFolder.getFolderTitle());
                            File hiddenFile = new File(fullName);
                            hiddenFile.delete();
                            folderPath.delete();

                        }
                    } else {
                        Toast.makeText(context, R.string.file_exists, Toast.LENGTH_SHORT).show();
                    }
                    context.cutOperation = false;
                }
                context.selectedFolders.clear();
                // context.gridView.setAdapter(context.new FileManagerGridAdapter(filesDirPath));
                Toast.makeText(context, R.string.file_pasted, Toast.LENGTH_SHORT).show();

            } else {
                for (ClickedFilePath currentDeleteFolder : context.selectedFolders) {
                    File folderPath = new File(currentDeleteFolder.getFolderPath());
                    File currentFile = new File(filesDirPath + "/" + currentDeleteFolder.getFolderTitle());
                    if (folderPath.isDirectory()) {
                        try {
                            context.userFunctions.copyDirectory(folderPath, currentFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        context.userFunctions.copyFiles(currentDeleteFolder.getFolderPath(), filesDirPath + "/" + currentDeleteFolder.getFolderTitle());
                        String[] split = currentDeleteFolder.getFolderPath().split("/");
                        String fullName = currentDeleteFolder.getFolderPath().replace(split[split.length - 1], "." + split[split.length - 1]);
                        context.userFunctions.copyFiles(fullName, filesDirPath + "/." + currentDeleteFolder.getFolderTitle());
                    }

                }
                context.btn_paste_File.setAlpha(0.5f);
                context.selectedFolders.clear();

                Toast.makeText(context, R.string.file_pasted, Toast.LENGTH_SHORT).show();
            }
            context.gridView.setAdapter(context.new FileManagerGridAdapter(filesDirPath));
        }
    }


    public void invalidateGridview() {
        File f = new File(filesDirPath);
        context.adapterFiles = f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return !file.isHidden();
            }
        });
        for (ClickedFilePath files : context.folders) {
            if (files.isFolderSelected()) {
                files.setFolderSelected(false);
            }
        }
        context.SelectedFile = null;
        context.gridView.invalidateViews();
    }

    public void invalidateGridviewAfterDeleting() {
        File f = new File(filesDirPath);
        context.adapterFiles = f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return !file.isHidden();
            }
        });
        for (ClickedFilePath folder : context.selectedFolders) {
            if (folder.isFolderSelected()) {
                folder.setFolderSelected(false);
                context.folders.remove(folder);
                //enrichmentSelectedList.remove(enrichment);
            }
        }
        context.SelectedFile = null;
        context.gridView.invalidateViews();
    }

    public void addingImagesInCurrentPage(File File) {
        if (File.toString().contains(".png") || File.toString().contains(".jpg") || File.toString().contains(".gif")) {
            context.setResult(context.RESULT_OK, context.getIntent().putExtra("Book", context.currentBook));
            context.setResult(context.RESULT_OK, context.getIntent().putExtra("imageType", "Image"));
            context.setResult(context.RESULT_OK, context.getIntent().putExtra("path", File.getPath()));
            context.finish();
            //  bookViewActivity.addNewImageRL();
        } else if (File.toString().contains(".mp4") || File.toString().contains(".mkv") || File.toString().contains(".3gp") || File.toString().contains(".avi") || File.toString().contains(".flv")) {
            context.setResult(context.RESULT_OK, context.getIntent().putExtra("Book", context.currentBook));
            context.setResult(context.RESULT_OK, context.getIntent().putExtra("imageType", "Video"));
            context.setResult(context.RESULT_OK, context.getIntent().putExtra("path", File.getPath()));
            context.finish();

        } else if (File.toString().contains(".mp3") || File.toString().contains(".m4a")) {
            context.setResult(context.RESULT_OK, context.getIntent().putExtra("Book", context.currentBook));
            context.setResult(context.RESULT_OK, context.getIntent().putExtra("imageType", "Audio"));
            context.setResult(context.RESULT_OK, context.getIntent().putExtra("path", File.getPath()));
            context.finish();
        }
    }

    public class lvimage extends AsyncTask<String, Void, Bitmap> {
        ImageView imagepath;
        ImageView img;


        public lvimage(ImageView imgView) {
            img = imgView;
        }

        protected Bitmap doInBackground(String... urls) {
            String URLpath = urls[0];
            Bitmap bmp = null;
            try {
                URL url = new URL(URLpath);
                URLConnection ucon = url.openConnection();
                ucon.connect();
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                //System.out.println(bmp.getWidth());;
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return bmp;

        }

        protected void onPostExecute(Bitmap result) {
            if (result == null) {

            } else {
                img.setVisibility(View.VISIBLE);
                Bitmap bit = result.createScaledBitmap(result, (int) context.getResources().getDimension(R.dimen.filemanager_folder_size), (int) context.getResources().getDimension(R.dimen.filemanager_folder_size), false);
                img.setImageBitmap(bit);

            }
        }
    }

    public class progressDownload extends AsyncTask<String, Integer, String> {
        public String DprogressString = "";
        public Boolean downloadSuccess;

        protected void onPreExecute() {
            super.onPreExecute();
            ////System.out.println("OnPreExecute downloading In internal Storage");
            //downloadProgDialog.show();
        }

        @Override
        protected String doInBackground(String... downloadurl) {

            final int TIMEOUT_CONNECTION = 5000;//5sec
            final int TIMEOUT_SOCKET = 30000;//30sec
            long bytes = 1024 * 1024;
            try {
                URL url = new URL(downloadurl[0]);
                //Open a connection to that URL.
                URLConnection ucon = url.openConnection();
                ucon.connect();
                int fileLength = ucon.getContentLength();
                ////System.out.println("filelength:"+fileLength);
                long startTime = System.currentTimeMillis();

                //this timeout affects how long it takes for the app to realize there's a connection problem
                ucon.setReadTimeout(TIMEOUT_CONNECTION);
                ucon.setConnectTimeout(TIMEOUT_SOCKET);
                //Define InputStreams to read from the URLConnection.
                // uses 3KB download buffer
                InputStream is = ucon.getInputStream();
                BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);

                // new File(file).delete();
                //Internal storage
                //FileOutputStream outStream = new FileOutputStream(DownloadPath);
                UserFunctions.createNewDirectory(filesDirPath+"/zipFiles");
                String bookPath = filesDirPath+"/zipFiles.zip";
                FileOutputStream outStream =new FileOutputStream(bookPath); //fa_enrich.openFileOutput(bookPath, fa_enrich.MODE_PRIVATE);
                byte[] buff = new byte[5 * 1024];
                //Read bytes (and store them) until there is nothing more to read(-1)
                int len;
                String totalMB, fileLengthMB;
                long total = 0;
                DecimalFormat twoDformat = new DecimalFormat("#.##");
                while ((len = inStream.read(buff)) != -1) {
                    total += len;
                    //////System.out.println("total:"+total);
                    totalMB = twoDformat.format((float) total / bytes);
                    fileLengthMB = twoDformat.format((float) fileLength / bytes);
                    DprogressString = totalMB + " MB" + " of " + fileLengthMB + " MB";
                    publishProgress((int) ((total * 100) / fileLength));
                    outStream.write(buff, 0, len);
                }
                //clean up
                outStream.flush();
                outStream.close();
                inStream.close();
                downloadSuccess = true;
                //downloadStatus = 0;
                ////System.out.println( "download completed in "+ ((System.currentTimeMillis() - startTime) / 1000)+ " sec");

            } catch (IOException e) {
                ////System.out.println("error while storing:"+e);
                downloadSuccess = false;
                //downloadStatus = 0;
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            //////System.out.println("OnprogressUpdate:"+progress[0]);
            //downloadProgDialog.setMessage(DprogressString);
           // downloadProgDialog.setProgress(progress[0]);

        }

        protected void onPostExecute(String result) {
            ////System.out.println("OnpostExecute");
            if (downloadSuccess) { //unzipping will be done when downloadSuccess is true.

                String bookpath = filesDirPath;
                UserFunctions.createNewDirectory(filesDirPath+"/Files");
                //boolean unzipSuccess = unzipDownloadfileNew(zipfile, unziplocation);
                UserFunctions.createNewDirectory(filesDirPath+"/Files");
                Boolean unzipSuccess= Zip.unzipDownloadfileNew(bookpath,filesDirPath+"/Files");
               //if unzipping success then need to store in database:
                if(unzipSuccess){
                  System.out.println(unzipSuccess);
                    //DB entry :
                }
                //Need to delete the zip file which is download
                File zipfilelocation = new File(filesDirPath+"/zipFiles."+".zip");  //Deleting zip file in the internal storage
                try{
                    zipfilelocation.delete();
                }catch(Exception e){
                    //////System.out.println("Failed to deletefile with Error "+e);
                }
            }
        }


    }
}

