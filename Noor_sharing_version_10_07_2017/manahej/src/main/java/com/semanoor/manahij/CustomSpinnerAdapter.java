package com.semanoor.manahij;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by semanoor on 31/05/17.
 */

public class CustomSpinnerAdapter extends ArrayAdapter<String> {
    private Context context;
    private String[] lineTypeText;
    private int[] lineTypeImages;
    LayoutInflater inflater;

    public CustomSpinnerAdapter(Context context, int textViewResourceId, String[] lineTypes, int[] images) {
        super(context, textViewResourceId, lineTypes);
        this.context = context;
        this.lineTypeText = lineTypes;
        lineTypeImages = images;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        // TODO Auto-generated method stub


        View row = inflater.inflate(R.layout.spinner_drawing_lines, parent, false);
        TextView label = (TextView) row.findViewById(R.id.lineType);
        label.setText(lineTypeText[position]);
        ImageView icon = (ImageView) row.findViewById(R.id.lineImage);
        //icon.setImageResource(R.drawable.dotted);
        icon.setImageResource(lineTypeImages[position]);


        return row;

    }

}
