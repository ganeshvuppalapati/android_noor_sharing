package com.semanoor.manahij;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.Templates;
import com.semanoor.source_sboookauthor.TextCircularProgressBar;
import com.semanoor.source_sboookauthor.UserFunctions;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by karthik on 11-01-2017.
 */

public class OnlineTemplatesGridAdapter extends RecyclerView.Adapter<OnlineTemplatesGridAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<OnlineTemplates> templatesListArray;
    private SharedPreferences sharedPreferences;
    private TextCircularProgressBar circularProgressBar;
    private boolean downloadTaskRunning;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView bookImg;
        TextCircularProgressBar textCircularProgressBar;

        public MyViewHolder(View view) {
            super(view);
            bookImg = (ImageView) view.findViewById(R.id.imageView2);
            textCircularProgressBar = (TextCircularProgressBar) view.findViewById(R.id.circular_progress);
        }
    }

    public OnlineTemplatesGridAdapter(Context ctx, ArrayList<OnlineTemplates> _templatesListArray) {
        this.context = ctx;
        this.templatesListArray = _templatesListArray;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.inflate_book_template, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        OnlineTemplates templates = templatesListArray.get(position);
        holder.itemView.setId(templates.getTemplateId());
        if (templates.getDownloadStatus().equals("Not Downloaded")){
            holder.bookImg.setAlpha(0.50f);
            holder.textCircularProgressBar.setVisibility(View.GONE);
        }else if (templates.getDownloadStatus().equals("Downloading")){
            if (getNextDownloadTemplateId() != -1){
                if (!downloadTaskRunning){
                    new downloadTemplate(getNextDownloadTemplateId()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,templates.getContentUrl());
                }
                if (templates.getTemplateId() == getNextDownloadTemplateId()){
                    setCircularProgressBar(holder.textCircularProgressBar);
                }
                holder.bookImg.setAlpha(0.50f);
                holder.textCircularProgressBar.setMax(100);
                holder.textCircularProgressBar.getCircularProgressBar().setCircleWidth(14);
                holder.textCircularProgressBar.setVisibility(View.VISIBLE);
            }
        }else{
            holder.bookImg.setAlpha(1.0f);
            holder.textCircularProgressBar.setVisibility(View.GONE);
        }
        if (templates.getDownloadStatus().equals("Downloaded") || templates.getDownloadStatus().equals("Created")){
            String freeFilesPath = Globals.TARGET_BASE_TEMPATES_PATH;
            Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templates.getTemplateId()+"/card.png");
            holder.bookImg.setImageBitmap(bitmap);
        }else {
            Glide.with(context).load(templates.getThumbUrl())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(200, 200)
                    .into(holder.bookImg);
        }
    }

    @Override
    public int getItemCount() {
        return templatesListArray.size();
    }

    private int getNextDownloadTemplateId(){
        ArrayList<Integer> existList = null;
        try {
            existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreferences.getString(Globals.DownloadingTemplates, ObjectSerializer.serialize(new ArrayList<Integer>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (existList.size()>0){
            return existList.get(0);
        }
        return -1;
    }

    public TextCircularProgressBar getCircularProgressBar() {
        return circularProgressBar;
    }

    public void setCircularProgressBar(TextCircularProgressBar circularProgressBar) {
        this.circularProgressBar = circularProgressBar;
    }

   private class downloadTemplate extends AsyncTask<String,Integer,String>{
        private int ID;
        private boolean downloadSuccess;

        public downloadTemplate(int templateId){
            this.ID = templateId;
            downloadTaskRunning = true;
        }

        @Override
        protected String doInBackground(String... downloadurl) {
            final int TIMEOUT_CONNECTION = 5000;//5sec
            final int TIMEOUT_SOCKET = 30000;//30sec
            long total = 0;
            int fileLength;
            try {
                if (UserFunctions.isInternetExist(context)) {
                    URL url = new URL(downloadurl[0]);
                    URLConnection ucon = url.openConnection();
                    File newFile = new File((Globals.TARGET_BASE_FILE_PATH) + ID);

                    if (newFile.exists()) {
                        total = newFile.length();
                        ucon.setRequestProperty("Range", "bytes=" + total + "-");
                        ucon.connect();
                    } else {
                        ucon.setRequestProperty("Range", "bytes=" + total + "-");
                        ucon.connect();
                    }
                    fileLength = ucon.getContentLength();
                    long startTime = System.currentTimeMillis();
                    ucon.setReadTimeout(TIMEOUT_CONNECTION);
                    ucon.setConnectTimeout(TIMEOUT_SOCKET);
                    InputStream is = ucon.getInputStream();
                    BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
                    String templateId= String.valueOf(ID);
                    FileOutputStream outStream = (total == 0) ? context.openFileOutput(templateId, Context.MODE_PRIVATE) : context.openFileOutput(templateId, Context.MODE_APPEND);
                    byte[] buff = new byte[5 * 1024];
                    int len;
                    while (!isCancelled() && (len = inStream.read(buff)) != -1) {
                        total += len;
                        publishProgress((int) ((total * 100) / fileLength));
                        outStream.write(buff, 0, len);
                    }
                    outStream.flush();
                    outStream.close();
                    inStream.close();
                    downloadSuccess = true;
                }
            }catch (IOException e) {
                downloadSuccess = false;
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            if (getCircularProgressBar()!=null) {
                getCircularProgressBar().setProgress(progress[0]);
                getCircularProgressBar().setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (downloadSuccess) {
                DatabaseHandler db = DatabaseHandler.getInstance(context);
                String tempPath, zipfile, unzippathWithSlash;
                tempPath = String.valueOf(ID);   //Changes after merge
                zipfile = Globals.TARGET_BASE_FILE_PATH.concat(tempPath);
                unzippathWithSlash = tempPath.concat("/");
                String unziplocation = Globals.TARGET_BASE_TEMPATES_PATH; //"/" is a must here..
                if (!new File(unziplocation+ID).exists()) {
                    boolean unzipSuccess = unzipDownloadfileNew(zipfile, unziplocation);
                    if (unzipSuccess) {
                        boolean add = true;
                        String name = "Template " + ID;
                        ArrayList<Templates> templateList = db.getAllTemplatesListBasedOnOrientation("portrait");
                        if (templateList != null) {
                            for (Templates templates : templateList) {
                                if (templates.getTemplateId() == ID) {
                                    add = false;
                                }
                            }
                        }
                        if (add) {
                            db.executeQuery("insert into templates(templateID,name,type,titleFont,titleSize,titleColor,authorFont,authorSize,authorColor,paraFont,paraColor,paraSize) values('" + ID + "','" + name + "','portrait','Verdana','54','232-219-184','Arial','25','232-219-184','Arial','56-56-48','25')");
                        }
                        removeFromSharedPreference(ID);
                        updateArrayList(ID);
                        notifyDataSetChanged();
                        final int nextId = getNextDownloadTemplateId();
                        if (nextId != -1) {
                            final String url = getUrl(nextId);
                            new android.os.Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    new downloadTemplate(nextId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
                                }
                            }, 1000);
                        } else {
                            downloadTaskRunning = false;
                        }
                        java.io.File fileDir = context.getFilesDir();   //Deleting zip file in the internal storage
                        File zipfilelocation = new java.io.File(fileDir, String.valueOf(ID));
                        try {
                            zipfilelocation.delete();
                        } catch (Exception e) {
                        }
                    }
                }else{
                    notifyDataSetChanged();
                }
            }
        }
    }
    private boolean unzipDownloadfileNew(String zipfilepath, String unziplocation) {
        ZipFile zipFile = null;
        List<FileHeader> headers = null;
        try {
            zipFile = new ZipFile(new File(zipfilepath));
            headers = zipFile.getFileHeaders();
        } catch (ZipException e) {
            e.printStackTrace();
        }

        if (headers != null) {
            for (int i = 0; i < headers.size(); i++) {
                FileHeader header = headers.get(i);
                header.setFileName(header.getFileName().replace("\\", "//"));
                //File  file = new File(eleessonPath+header.getFileName());
                try {
                    zipFile.extractFile(header, unziplocation);
                } catch (ZipException e) {
                    e.printStackTrace();
                }

            }
        }
        return true;
    }
    private String getUrl(int ID){
        for (int i=0;i<templatesListArray.size();i++){
            OnlineTemplates templates = templatesListArray.get(i);
            if (ID == templates.getTemplateId()){
                return templates.getContentUrl();
            }
        }
        return "";
    }
    private void removeFromSharedPreference(int Id){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        ArrayList<Integer> existList = null;
        try {
            existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreferences.getString(Globals.DownloadingTemplates, ObjectSerializer.serialize(new ArrayList<Integer>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i=0;i<existList.size();i++){
            int existId = existList.get(i);
            if (existId == Id){
                existList.remove(i);
                break;
            }
        }
        try {
            editor.putString(Globals.DownloadingTemplates, ObjectSerializer.serialize(existList));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        editor.commit();
    }
    private void updateArrayList(int Id){
        for (int i=0;i<templatesListArray.size();i++){
            OnlineTemplates templates = templatesListArray.get(i);
            if (Id == templates.getTemplateId()&& templates.getDownloadStatus().equals("Downloading")){
                templates.setDownloadStatus("Downloaded");
                break;
            }
        }
    }
}
