package com.semanoor.manahij;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.semanoor.source_manahij.NotesEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Bhavya on 05-10-2015.
 */
public class ElessonNote {
    private Elesson elesson;
    public boolean isPressed=true;
    ListView lv;
    public ArrayList<HashMap<String,String>> noteList;
    private NotesEditText  etNote;
    public EditText et;
    public String newTime;
    public boolean updateNoteList;
    HashMap<String,String> noteMap;

    public ElessonNote(Elesson elesson) {
        this.elesson = elesson;
    }

    public void LoadNotepopup(){
        elessonPopup();
    }

public void elessonPopup(){
    Dialog elessonNotepopup = new Dialog(elesson);
    elessonNotepopup.getWindow().setBackgroundDrawable(new ColorDrawable(elesson.getResources().getColor(R.color.semi_transparent)));
    elessonNotepopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
    elessonNotepopup.setContentView(elesson.getLayoutInflater().inflate(R.layout.elesson_note, null));
    elessonNotepopup.getWindow().setLayout((int) elesson.getResources().getDimension(R.dimen.e_notepopup_width), (int) elesson.getResources().getDimension(R.dimen.e_notepopup_height));
    noteList=elesson.db.getAllElessonNote(elesson.currentbook.get_bStoreID());
    final Button btn = (Button)elessonNotepopup.findViewById(R.id.button6);
    lv = (ListView)elessonNotepopup.findViewById(R.id.listView4);
    ListAdapter noteListAdapter = new noteListAdapter(elesson);
    lv.setAdapter(noteListAdapter);
    et = (EditText)elessonNotepopup.findViewById(R.id.editText2);
    lv.setVisibility(View.VISIBLE);
    et.setVisibility(View.GONE);
    elessonNotepopup.show();
    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            updateNoteList = true;
            noteMap = noteList.get(position);
            et.setText(noteMap.get("noteText"));
            lv.setVisibility(View.GONE);
            et.setVisibility(View.VISIBLE);
            btn.setText("<");
        }
    });

    lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            final HashMap<String, String> map = noteList.get(position);
            AlertDialog.Builder alert = new AlertDialog.Builder(elesson);
            alert.setTitle(R.string.delete);
            alert.setMessage(R.string.suredelete);
            alert.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String query = "delete from tblNoteElesson where id = '"+map.get("id")+"'";
                    elesson.db.executeQuery(query);
                    noteList=elesson.db.getAllElessonNote(elesson.currentbook.get_bStoreID());
                    lv.invalidateViews();
                    dialog.dismiss();
                }
            });
            alert.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
            return true;
        }
    });

    btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (lv.isShown()) {
                btn.setText("<");
                lv.setVisibility(View.GONE);
                et.setVisibility(View.VISIBLE);
                et.setText("");
            }else{
                btn.setText("+");
                lv.setVisibility(View.VISIBLE);
                et.setVisibility(View.GONE);
                if(updateNoteList) {
                    updateNoteList=false;
                    SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    newTime = sdfDateTime.format(new Date(System.currentTimeMillis()));
                    String query1 = "update tblNoteElesson set noteText='"+et.getText().toString()+"' where id = '"+noteMap.get("id")+"'";
                    elesson.db.executeQuery(query1);
                }else {
                    addElessonTexttoDb(et.getText().toString());
                }
                noteList=elesson.db.getAllElessonNote(elesson.currentbook.get_bStoreID());
                lv.invalidateViews();
            }
        }
    });
}
    public void addElessonTexttoDb(String et_text){
        if(!et_text.equals("")) {
            SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            newTime = sdfDateTime.format(new Date(System.currentTimeMillis()));
            String query = "insert into tblNoteElesson(lessonID,pageID,noteText,date,time,pageName) values('" + elesson.currentbook.get_bStoreID() + "','" + elesson.currentdataobj.getPageId() + "','" + et_text + "','" + newTime + "','" + "" + "','" + elesson.currentdataobj.getName() + "')";
            elesson.db.executeQuery(query);
        }
    }
    private class noteListAdapter extends BaseAdapter {

        private Context mContext;


        public noteListAdapter(Context context){
            mContext = context;
        }

        @Override
        public int getCount() {
            return noteList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if(convertView == null)
                vi = LayoutInflater.from(elesson).inflate(R.layout.note_text, null);

            TextView txt = (TextView)vi.findViewById(R.id.txt_one);
            TextView txt1 = (TextView)vi.findViewById(R.id.txt_two);

            HashMap<String,String> map = noteList.get(position);

            txt.setText(map.get("noteText"));
            txt1.setText(map.get("date"));

            return vi;
        }

    }
}