package com.semanoor.manahij;

import android.content.Context;

import java.io.File;
import java.io.FileFilter;

/**
 * Created by Krishna on 17-10-2016.
 */

public class Theme {
    private int themeID;
    private String themeName;
    private String createDate;
    private String foreGroundColor;
    private int category;
    Context context;
    private File[] adapterFiles;

    public Theme(Context contxt) {
        this.context=contxt;
    }

    public int getThemeID() {
        return themeID;
    }

    public void setThemeID(int themeID) {
        this.themeID = themeID;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getForeGroundColor() {
        return foreGroundColor;
    }

    public void setForeGroundColor(String foreGroundColor) {
        this.foreGroundColor = foreGroundColor;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }


    public File[] getAdapterFiles() {
        return adapterFiles;
    }

    public void setAdapterFiles(File[] adapterFiles) {
        this.adapterFiles = adapterFiles;
    }

    public File[] getFilesList(String filePath) {
        File f = new File(filePath);
        return f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return !file.isHidden();
            }
        });
    }
}
