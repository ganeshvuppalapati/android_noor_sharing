package com.semanoor.manahij;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.InputStreamContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.semanoor.source_sboookauthor.TextCircularProgressBar;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by karthik on 22-12-2016.
 */
public class DriveFileUpload {
    private static DriveFileUpload instance;
    private int progress;
    private Context context;
    private ArrayList<DriveFile> uploadingFilesList = new ArrayList<>();
    private TextCircularProgressBar textCircularProgressBar;
    private boolean uploadTakRunning;
    private boolean uploadScuccess;

    public static synchronized DriveFileUpload getInstance(){
        if(instance == null){
            instance = new DriveFileUpload();
        }
        return instance;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<DriveFile> getUploadingFilesList() {
        return uploadingFilesList;
    }

    public void setUploadingFilesList(ArrayList<DriveFile> uploadingFilesList) {
        this.uploadingFilesList = uploadingFilesList;
    }

    public boolean isUploadTakRunning() {
        return uploadTakRunning;
    }

    public void setUploadTakRunning(boolean uploadTakRunning) {
        this.uploadTakRunning = uploadTakRunning;
    }

    public class UploadFile extends AsyncTask<Void,Integer,String>{
        File destFile = null;

        @Override
        protected String doInBackground(Void... params) {
            if (uploadingFilesList.size()>0){
                uploadScuccess = false;
                DriveFile file = uploadingFilesList.get(0);
                java.io.File fil = file.getUploadFile();
                InputStream is = null;
                try {
                    is = new FileInputStream(fil);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                InputStreamContent mediaContent = new InputStreamContent(file.getMimeType(), is);
                mediaContent.setLength(fil.length());
                File body = new File();
                body.setTitle(file.getTitle());
                body.setMimeType(file.getMimeType());
                Drive.Files.Insert insert = REST.uploadFile(file.getParentId(), file.getTitle(), file.getMimeType(), mediaContent);
                final MediaHttpUploader uploader = insert.getMediaHttpUploader();
                uploader.setDirectUploadEnabled(false);
                uploader.setChunkSize(256*1024);
                uploader.setProgressListener(new MediaHttpUploaderProgressListener() {
                    @Override
                    public void progressChanged(MediaHttpUploader mediaHttpUploader) throws IOException {
                        switch (uploader.getUploadState()) {
                            case INITIATION_STARTED:
                                System.out.println("started");
                                break;
                            case INITIATION_COMPLETE:
                                final double percent1 = uploader.getProgress() * 100;
                                System.out.println("completed");
                                break;
                            case MEDIA_IN_PROGRESS:
                                final double percent = uploader.getProgress() * 100;
                                setProgress((int) percent);
                                if (textCircularProgressBar!=null && context instanceof CloudActivity) {
                                    ((CloudActivity)context).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            textCircularProgressBar.setProgress((int) percent);
                                        }
                                    });
                                }
                                break;
                            case MEDIA_COMPLETE:
                                setProgress(0);
                                uploadScuccess = true;
                                break;
                            case NOT_STARTED :
                                System.out.println("not started");
                                break;
                        }
                    }
                });
                try {
                    destFile = insert.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            final double percent = values[0];
            setProgress((int) percent);
            if (textCircularProgressBar!=null && context instanceof CloudActivity) {
                textCircularProgressBar.setProgress((int) percent);
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (uploadScuccess) {
                if (context instanceof CloudActivity) {
                    if (destFile!=null){
                        DriveFile file = uploadingFilesList.get(0);
                        int position = getParentIdPos(file.getParentId());
                        if (position!=-1) {
                            HashMap<Integer, Object> hashMap = ((CloudActivity) context).cloud.getCategoryFilesMap().get(position);
                            ArrayList<File> subList = (ArrayList<File>) hashMap.get(position);
                            for (int i=0;i<subList.size();i++){
                                File file1 = subList.get(i);
                                if (destFile.getTitle().equals(file1.getTitle())&& destFile.getId()!=null){
                                    subList.remove(i);
                                }
                            }
                            subList.add(destFile);
                        }
                    }
                    ((CloudActivity) context).gridView.invalidateViews();
                }
                uploadingFilesList.remove(0);
                if (uploadingFilesList.size() > 0) {
                    setUploadTakRunning(true);
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new UploadFile().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }, 1000);

                } else {
                    setUploadTakRunning(false);
                }
            }else{
                uploadingFilesList.remove(0);
                ((CloudActivity) context).gridView.invalidateViews();
                UserFunctions.DisplayAlertDialog(context,R.string.connection_error,R.string.upload_failed);
            }
        }
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public TextCircularProgressBar getTextCircularProgressBar() {
        return textCircularProgressBar;
    }

    public void setTextCircularProgressBar(TextCircularProgressBar textCircularProgressBar) {
        this.textCircularProgressBar = textCircularProgressBar;
    }
    private int getParentIdPos(String parentId){
        if (context instanceof CloudActivity){
            ArrayList<File> filesList = ((CloudActivity)context).cloud.getCategoryList();
            for (int i=0;i<filesList.size();i++){
                File file = filesList.get(i);
                if (file.getId()!=null && file.getId().equals(parentId)){
                    return i;
                }
            }
        }
        return -1;
    }
}
