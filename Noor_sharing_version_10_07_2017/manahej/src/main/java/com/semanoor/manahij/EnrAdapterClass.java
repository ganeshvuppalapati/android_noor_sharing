package com.semanoor.manahij;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptg.views.CircleButton;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.Note;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.Category;
import com.semanoor.source_sboookauthor.Globals;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krishna on 29-09-2016.
 */
public class EnrAdapterClass {
   Context context;
   Dialog exportEnrPopover;
    public EnrAdapterClass(Context _context) {
        this.context = _context;
    }


    public class ExportEnrAdapter extends RecyclerView.Adapter<ExportEnrAdapter.MyViewHolder> {
        ExportEnrActivity exportEnrActivity;
        public ArrayList<Category> categoryListArray;
        Dialog exportEnrPopover;

        public ExportEnrAdapter(ExportEnrActivity context, ArrayList<Category> category) {
            exportEnrActivity = context;
            categoryListArray = category;
        }

        // private List<Movie> moviesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView tv_Title;
            RecyclerView export_shelf;
            Button btnPropertyAccord;

            public MyViewHolder(View view) {
                super(view);
                export_shelf = (RecyclerView) view.findViewById(R.id.gridView1);
                btnPropertyAccord = (Button) view.findViewById(R.id.button1);
                tv_Title = (TextView) view.findViewById(R.id.text);
                //title = (TextView) view.findViewById(R.id.title);
                // genre = (TextView) view.findViewById(R.id.genre);
                // year = (TextView) view.findViewById(R.id.year);
            }
        }


//    public MoviesAdapter(List<Movie> moviesList) {
//        this.moviesList = moviesList;
//    }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.export_shelf, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            final Category category = categoryListArray.get(position);
            holder.tv_Title.setText(category.getCategoryName());
            EnrAdapterClass enr=new EnrAdapterClass(exportEnrActivity);
           // ExportEnrItem horizontalAdapter = new ExportEnrItem(category, holder.export_shelf, exportEnrActivity);
            //holder.export_shelf.setAdapter(horizontalAdapter);
            holder.export_shelf.setAdapter(enr.new ExportEnrItem(category, holder.export_shelf, exportEnrActivity));
            holder.export_shelf.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(exportEnrActivity, LinearLayoutManager.HORIZONTAL, false);
            holder.export_shelf.setLayoutManager(mLayoutManager);
            // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            holder.export_shelf.setItemAnimator(new DefaultItemAnimator());
//        Movie movie = moviesList.get(position);
//        holder.title.setText(movie.getTitle());
//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());

            if (category.isAccordExpanded()) {
                holder.btnPropertyAccord.setBackgroundResource(R.drawable.new_category_acordian_collapsed);
                // holder.scrollView.setVisibility(View.GONE);
                // holder.categoryGridView.setVisibility(View.VISIBLE);
                ViewGroup.LayoutParams gridlayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.export_shelf.getLayoutParams();
                //ViewGroup.LayoutParams imglayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.bgRackImageView.getLayoutParams();
                gridlayoutParams.height = (int) exportEnrActivity.getResources().getDimension(R.dimen.shelf_height) + (int) exportEnrActivity.getResources().getDimension(R.dimen.shelf_height);
                // imglayoutParams.height = holder.categoryGridView.getHeight();
                holder.export_shelf.setLayoutParams(gridlayoutParams);
                holder.export_shelf.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {

                    @Override
                    public void onLayoutChange(View v, int left, int top, int right,
                                               int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
//                    ViewGroup.LayoutParams imglayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.bgRackImageView.getLayoutParams();
//                    //System.out.println("GridView Height: "+v.getHeight());
//                    imglayoutParams.height = v.getHeight();
//                    holder.bgRackImageView.setLayoutParams(imglayoutParams);
                    }
                });
                GridLayoutManager gridLayout = new GridLayoutManager(exportEnrActivity, 3, GridLayoutManager.VERTICAL, false);
                //llm.canScrollHorizontally();
                holder.export_shelf.setLayoutManager(gridLayout);
                holder.export_shelf.setHasFixedSize(true);
                holder.export_shelf.invalidate();
            } else {
                holder.btnPropertyAccord.setBackgroundResource(R.drawable.new_category_acordian_expanded);
                // holder.categoryGridView.setVisibility(View.VISIBLE);
                ViewGroup.LayoutParams gridlayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.export_shelf.getLayoutParams();
                //ViewGroup.LayoutParams imglayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.bgRackImageView.getLayoutParams();
                gridlayoutParams.height = (int) exportEnrActivity.getResources().getDimension(R.dimen.shelf_height);
                // imglayoutParams.height = holder.categoryGridView.getHeight();
                holder.export_shelf.setLayoutParams(gridlayoutParams);
                LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(exportEnrActivity, LinearLayoutManager.HORIZONTAL, false);
                holder.export_shelf.setLayoutManager(horizontalLayoutManagaer);
                holder.export_shelf.setHasFixedSize(true);
                holder.export_shelf.invalidate();

                //new addBooksToScrollView(position, category, holder).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

            }
            holder.btnPropertyAccord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (category.isAccordExpanded()) {
                        holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_expanded_new);
                        category.setAccordExpanded(false);
                        notifyDataSetChanged();
                    } else {
                        holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_collapsed_new);
                        category.setAccordExpanded(true);
                        notifyDataSetChanged();
                    }
                }
            });

            holder.export_shelf.addOnItemTouchListener(new RecyclerTouchListener(exportEnrActivity, holder.export_shelf, new RecyclerTouchListener.ClickListener() {

                @Override
                public void onClick(View view, int position) {


                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));

        }

        @Override
        public int getItemCount() {
            return 4;   // moviesList.size();
        }

    }






    public class ExportEnrItem extends RecyclerView.Adapter<ExportEnrItem.MyViewHolder> {
        ExportEnrActivity exportEnrActivity;
        public ArrayList<Category> categoryListArray;
        int Count = 0;
        private List<String> horizontalList;
        // private ArrayList<Book> categoryBooksList = new ArrayList<Book>();
        ArrayList<CloudData> Books = new ArrayList<CloudData>();
        private ArrayList<Enrichments> enrPageList;
        private ArrayList<Note> notePageList;
        private Category category;
        RecyclerView recyclerView;

        public ExportEnrItem(Category catgory, RecyclerView export_shelf, ExportEnrActivity exportActivity) {
            exportEnrActivity = exportActivity;
            this.category = catgory;
            Book book = null;
            this.recyclerView = export_shelf;
            if (category.getCategoryId() == 0) {
                enrPageList = exportEnrActivity.enrichmentTotalPageNoList;
                Count = enrPageList.size();
            } else if (category.getCategoryId() == 1) {
                notePageList = exportEnrActivity.noteTotalPageNoList;
                Count = notePageList.size();
            } else if (category.getCategoryId() == 2) {
                enrPageList = exportEnrActivity.enrichmentLinkTotalPageNoList;
                Count = enrPageList.size();
            } else if (category.getCategoryId() == 3) {
                enrPageList = exportEnrActivity.mindmapTotalPageNoList;
                Count = enrPageList.size();
            }

        }

        // private List<Movie> moviesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            // public TextView title, year, genre;
            //public TextView txtView;
            ImageView bookImg;
            TextView tv_Title;
            RelativeLayout rl;
            CheckBox cb_selectall;
            // CircleButton btn_download;
            // CardView rl;
            //  TextCircularProgressBar progressBar;
            Button btn_text;


            public MyViewHolder(View view) {
                super(view);
                rl = (RelativeLayout) view.findViewById(R.id.shelfbookLayout);
                ViewGroup.MarginLayoutParams margins = (ViewGroup.MarginLayoutParams) rl.getLayoutParams();
                margins.setMargins(0, 0, 0, 0);
                bookImg = (ImageView) view.findViewById(R.id.imageView2);
                //progressBar = (TextCircularProgressBar) view.findViewById(R.id.circular_progress);
                // progressBar.setVisibility(View.INVISIBLE);
                tv_Title = (TextView) view.findViewById(R.id.textView1);
                btn_text = (Button) view.findViewById(R.id.btn_text);
                cb_selectall = (CheckBox) view.findViewById(R.id.cb_selectall);
                //title = (TextView) view.findViewById(R.id.title);
                // genre = (TextView) view.findViewById(R.id.genre);
                // year = (TextView) view.findViewById(R.id.year);
            }
        }


//    public MoviesAdapter(List<Movie> moviesList) {
//        this.moviesList = moviesList;
//    }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shelf_enr_pages, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            //Enrichments enrich = enrPageList.get(position);
            //String page = exportEnrActivity.getResources().getString(R.string.page);
            final String page = exportEnrActivity.getResources().getString(R.string.page);
            Enrichments enrich=null;
            Note note =null;
            if (category.getCategoryId() == 0 || category.getCategoryId() == 2 || category.getCategoryId() == 3) {

                enrich = enrPageList.get(position);
                int pageno = enrich.getEnrichmentPageNo() - 1;
                holder.tv_Title.setText(page + " " + pageno);
                holder.bookImg.setVisibility(View.VISIBLE);
                String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + exportEnrActivity.currentBook.getBookID() + "/thumbnail_" + enrich.getEnrichmentPageNo() + ".png";

                if(exportEnrActivity.currentBook.getTotalPages()-1==pageno){
                   // thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + exportEnrActivity.currentBook.getBookID() + "/thumbnail_" + enrich.getEnrichmentPageNo() + ".png";
                    thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + exportEnrActivity.currentBook.getBookID() + "/thumbnail_end.png";
                }
                Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
                holder.bookImg.setImageBitmap(bitmap);

                if (category.getCategoryId() == 0) {
                    ArrayList<Enrichments> enrichmentPageList = exportEnrActivity.getTotalNoOfEnrichedPages(enrich.getEnrichmentPageNo());
                    holder.btn_text.setText(enrichmentPageList.size() + "");
                    boolean selected=false;
                    for(int i=0;i<enrichmentPageList.size();i++){
                        selected=false;
                        Enrichments enrichh=enrichmentPageList.get(i);
                        if(enrichh.isEnrichmentSelected()){
                            selected=true;
                            break;
                        }
                    }
                    if(selected){
                        holder.cb_selectall.setChecked(true);
                    }else{
                        holder.cb_selectall.setChecked(false);
                    }
                } else if (category.getCategoryId() == 2) {
                    ArrayList<Enrichments> EnrichedLinkPageList = exportEnrActivity.getTotalNoOfEnrichedLinkPages(enrich.getEnrichmentPageNo());
                    holder.btn_text.setText(EnrichedLinkPageList.size() + "");
                    boolean selected=false;
                    for(int i=0;i<EnrichedLinkPageList.size();i++){
                        selected=false;
                        Enrichments enrichh=EnrichedLinkPageList.get(i);
                        if(enrichh.isEnrichmentSelected()){
                            selected=true;
                            break;
                        }
                    }
                    if(selected){
                        holder.cb_selectall.setChecked(true);
                    }else{
                        holder.cb_selectall.setChecked(false);
                    }
                } else if (category.getCategoryId() == 3) {
                    ArrayList<Enrichments> MindmapPageList = exportEnrActivity.getTotalNoOfMindmapPages(enrich.getEnrichmentPageNo());
                    holder.btn_text.setText(MindmapPageList.size() + "");
                    boolean selected=false;
                    for(int i=0;i<MindmapPageList.size();i++){
                        selected=false;
                        Enrichments enrichh=MindmapPageList.get(i);
                        if(enrichh.isEnrichmentSelected()){
                            selected=true;
                            break;
                        }
                    }
                    if(selected){
                        holder.cb_selectall.setChecked(true);
                    }else{
                        holder.cb_selectall.setChecked(false);
                    }
                }

            } else if (category.getCategoryId() == 1) {
                note = notePageList.get(position);
                int pageno = note.getPageNo() - 1;
                holder.tv_Title.setText(page + " " + pageno);
                String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + exportEnrActivity.currentBook.getBookID() + "/thumbnail_" + note.getPageNo() + ".png";
                Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
                holder.bookImg.setImageBitmap(bitmap);
                ArrayList<Note> noteList = exportEnrActivity.getTotalNoOfNotePages(note.getPageNo());
                holder.btn_text.setText(noteList.size() + "");
                boolean selected=false;
                for(int i=0;i<noteList.size();i++){
                    selected=false;
                    Note notee=noteList.get(i);
                    if(notee.isNoteSelected()){
                        selected=true;
                        break;
                    }
                }
                if(selected){
                    holder.cb_selectall.setChecked(true);
                }else{
                    holder.cb_selectall.setChecked(false);

                }
                // Count=exportEnrActivity.notePageList.size();
            }
            final Enrichments finalEnrich = enrich;
            final Note finalNote = note;
            holder.cb_selectall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<Enrichments> enrichmentPageList=null;
                    if (category.getCategoryId()!= 1) {
                        if(category.getCategoryId()==0) {
                            enrichmentPageList = exportEnrActivity.getTotalNoOfEnrichedPages(finalEnrich.getEnrichmentPageNo());
                        }else if(category.getCategoryId()==2){
                            enrichmentPageList = exportEnrActivity.getTotalNoOfEnrichedLinkPages(finalEnrich.getEnrichmentPageNo());
                        }else if(category.getCategoryId()==3){
                            enrichmentPageList = exportEnrActivity.getTotalNoOfMindmapPages(finalEnrich.getEnrichmentPageNo());
                        }
                        if(holder.cb_selectall.isChecked()){
                            for(int i=0;i<enrichmentPageList.size();i++){
                                Enrichments enrichh=enrichmentPageList.get(i);
                                enrichh.setEnrichmentSelected(true);
                                if(category.getCategoryId()==2){
                                    EnrichmentSelection(enrichh, exportEnrActivity.enrichmentDetails.getEnrichmentSelectedLinkList());
                                }else if(category.getCategoryId()==3){
                                    EnrichmentSelection(enrichh, exportEnrActivity.enrichmentDetails.getMindmapSelectedList());
                                }else if(category.getCategoryId()==0){
                                    EnrichmentSelection(enrichh, exportEnrActivity.enrichmentDetails.getEnrichmentSelectedList());
                                }

                            }
                            holder.cb_selectall.setChecked(true);
                        }else{
                            for(int i=0;i<enrichmentPageList.size();i++){
                                Enrichments enrichh=enrichmentPageList.get(i);
                                enrichh.setEnrichmentSelected(false);
                                if(category.getCategoryId()==2){
                                    EnrichmentSelection(enrichh, exportEnrActivity.enrichmentDetails.getEnrichmentSelectedLinkList());
                                }else if(category.getCategoryId()==3){
                                    EnrichmentSelection(enrichh, exportEnrActivity.enrichmentDetails.getMindmapSelectedList());
                                }else if(category.getCategoryId()==0){
                                    EnrichmentSelection(enrichh, exportEnrActivity.enrichmentDetails.getEnrichmentSelectedList());
                                }


                            }
                            holder.cb_selectall.setChecked(false);
                        }

                    }else if (category.getCategoryId() == 1) {
                        ArrayList<Note> noteList = exportEnrActivity.getTotalNoOfNotePages(finalNote.getPageNo());
                        if (holder.cb_selectall.isChecked()) {
                            for (int i = 0; i < noteList.size(); i++) {
                                Note note= noteList.get(i);
                                note.setNoteSelected(true);
                                NoteSelection(note, exportEnrActivity.enrichmentDetails.getNoteSelectedList());
                            }
                            // holder.cb_selectall.setChecked(false);
                            holder.cb_selectall.setChecked(true);
                        }else{
                            for (int i = 0; i < noteList.size(); i++) {
                                Note note= noteList.get(i);
                                note.setNoteSelected(false);
                                NoteSelection(note, exportEnrActivity.enrichmentDetails.getNoteSelectedList());
                            }
                            // holder.cb_selectall.setChecked(false);
                            holder.cb_selectall.setChecked(false);
                        }
                    }
                    notifyDataSetChanged();
                   // boolean selected=false;

                    //Count=enrPageList.size();

                }
            });

            holder.bookImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("yes");
                    Enrichments enrich;
                    EnrAdapterClass enr=new EnrAdapterClass(context);
                    if (category.getCategoryId() == 0) {
                        enrich = exportEnrActivity.enrichmentTotalPageNoList.get(position);
                        ArrayList<Enrichments> enrichmentPageList = exportEnrActivity.getTotalNoOfEnrichedPages(enrich.getEnrichmentPageNo());
                        enr.displayEnrPopUp(enrichmentPageList,true,null,recyclerView,category.getCategoryId());
                    } else if (category.getCategoryId() == 1) {
                        Note note = exportEnrActivity.noteTotalPageNoList.get(position);
                        ArrayList<Note> noteList = exportEnrActivity.getTotalNoOfNotePages(note.getPageNo());
                        //getTotalNoOfNotePages(note.getPageNo());
                        enr.displayEnrPopUp(null,false,noteList,recyclerView,category.getCategoryId());

                    } else if (category.getCategoryId() == 2) {
                        enrich = exportEnrActivity.enrichmentLinkTotalPageNoList.get(position);
                        ArrayList<Enrichments> EnrichedLinkPageList = exportEnrActivity.getTotalNoOfEnrichedLinkPages(enrich.getEnrichmentPageNo());

                        enr.displayEnrPopUp(EnrichedLinkPageList,true, null, recyclerView,category.getCategoryId());

                    } else if (category.getCategoryId() == 3) {
                        enrich = exportEnrActivity.mindmapTotalPageNoList.get(position);
                        ArrayList<Enrichments> MindmapPageList = exportEnrActivity.getTotalNoOfMindmapPages(enrich.getEnrichmentPageNo());
                        //Count=enrPageList.size();
                        enr.displayEnrPopUp(MindmapPageList,true, null, recyclerView,category.getCategoryId());
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return Count;   // moviesList.size();
        }

    }

    public void displayEnrPopUp(final ArrayList<Enrichments> EnrPageList, final boolean enrich, final ArrayList<Note> noteList, final RecyclerView export_shelf, final int categoryId) {
        exportEnrPopover = new Dialog(((ExportEnrActivity)context));
        exportEnrPopover.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.semi_transparent)));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        exportEnrPopover.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exportEnrPopover.setContentView(((ExportEnrActivity)context).getLayoutInflater().inflate(R.layout.export_enr_selectitems, null));
        exportEnrPopover.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.3), (int) context.getResources().getDimension(R.dimen.malzamahpopup_height));
        final RecyclerView recyclerView = (RecyclerView) exportEnrPopover.findViewById(R.id.recycler_view);
        EnrAdapterClass enr=new EnrAdapterClass(((ExportEnrActivity)context));
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
//          for(int i=0;i<EnrPageList.size();i++){
//
//          }
        CircleButton btn_back=(CircleButton)exportEnrPopover.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportEnrPopover.dismiss();
            }
        });
        recyclerView.setAdapter(enr.new ExportEnrDetails(EnrPageList,enrich,noteList,((ExportEnrActivity)context)));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, new RecyclerTouchListener.ClickListener() {


            @Override
            public void onClick(View view, int position) {
               if(enrich) {
                   Enrichments enrichment = EnrPageList.get(position);
                   if (enrichment.isEnrichmentSelected()) {
                       enrichment.setEnrichmentSelected(false);
                       //enrichmentSelectedList.remove(enrichment);
                   } else {
                       enrichment.setEnrichmentSelected(true);
                       // enrichmentSelectedList.add(enrichment);
                   }
                   if(categoryId==2){
                       EnrichmentSelection(enrichment, ((ExportEnrActivity) context).enrichmentDetails.getEnrichmentSelectedLinkList());
                   }else if(categoryId==3){
                       EnrichmentSelection(enrichment, ((ExportEnrActivity) context).enrichmentDetails.getMindmapSelectedList());
                   }else if(categoryId==0){
                       EnrichmentSelection(enrichment, ((ExportEnrActivity) context).enrichmentDetails.getEnrichmentSelectedList());
                   }
                 //  EnrichmentSelection(enrichment,SelectedEnrList);

               }else{
                   Note note = noteList.get(position);
                   if (note.isNoteSelected()) {

                       note.setNoteSelected(false);
                       //noteSelectedList.remove(note);
                   } else {
                       note.setNoteSelected(true);
                      // noteSelectedList.add(note);
                   }
                  NoteSelection(note, ((ExportEnrActivity) context).enrichmentDetails.getNoteSelectedList());
               }
                recyclerView.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        exportEnrPopover.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                export_shelf.getAdapter().notifyDataSetChanged();
            }
        });
            exportEnrPopover.show();
    }



    public class ExportEnrDetails extends RecyclerView.Adapter<ExportEnrDetails.MyViewHolder> {
        ExportEnrActivity exportEnrActivity;
        public ArrayList<Category> categoryListArray;
        int Count = 0;
        private List<String> horizontalList;
        // private ArrayList<Book> categoryBooksList = new ArrayList<Book>();
        ArrayList<CloudData> Books = new ArrayList<CloudData>();
        private ArrayList<Enrichments> enrPageList;
        private ArrayList<Note> notePageList;
        RecyclerView recyclerView;
        Boolean Enrichment;

        public ExportEnrDetails(ArrayList<Enrichments> enrichments, boolean enrich, ArrayList<Note> noteList, ExportEnrActivity context) {
            exportEnrActivity = context;
            Book book = null;
            Enrichment=enrich;

            if (Enrichment) {
                enrPageList = enrichments;
                Count = enrPageList.size();
            } else  {
                notePageList =noteList;
                Count = notePageList.size();
            }

        }

        // private List<Movie> moviesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            // public TextView title, year, genre;
            //public TextView txtView;
            ImageView bookImg;
            TextView tv_Title;
            RelativeLayout rl;
            CheckBox checkBox;
            TextView txtTitle;
            TextView txtDesc;
            CheckBox note_cb;
            // CircleButton btn_download;
            // CardView rl;
            //  TextCircularProgressBar progressBar;
            Button btn_text;


            public MyViewHolder(View view) {
                super(view);
                //rl = (RelativeLayout) view.findViewById(R.id.shelfbookLayout);
               // ViewGroup.MarginLayoutParams margins = (ViewGroup.MarginLayoutParams) rl.getLayoutParams();
               // margins.setMargins(0, 0, 0, 0);
                if(Enrichment) {
                    tv_Title = (TextView) view.findViewById(R.id.textView1);
                    checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
                }else{
                    txtTitle = (TextView) view.findViewById(R.id.textView1);
                    txtDesc = (TextView) view.findViewById(R.id.textView2);
                    note_cb = (CheckBox) view.findViewById(R.id.checkBox1);
                    bookImg=(ImageView) view.findViewById(R.id.iv_unread);
                }
                //title = (TextView) view.findViewById(R.id.title);
                // genre = (TextView) view.findViewById(R.id.genre);
                // year = (TextView) view.findViewById(R.id.year);
            }
        }


//    public MoviesAdapter(List<Movie> moviesList) {
//        this.moviesList = moviesList;
//    }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView;
            if (Enrichment) {
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.inflate_checklist, parent, false);
            }else{
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate( R.layout.inflate_slct_users_res ,parent,false);
            }
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            //Enrichments enrich = enrPageList.get(position);
            //String page = exportEnrActivity.getResources().getString(R.string.page);
            String page = exportEnrActivity.getResources().getString(R.string.page);
            if (Enrichment) {

                Enrichments enrich = enrPageList.get(position);
               // Enrichments enrichment = enrichmentPageList.get(position);
                holder.tv_Title.setTextColor(Color.BLACK);
                holder.tv_Title.setText(enrich.getEnrichmentTitle());
                if (enrich.isEnrichmentSelected()) {
                    holder.checkBox.setChecked(true);
                } else {
                    holder.checkBox.setChecked(false);
                }
               // int pageno = enrich.getEnrichmentPageNo() - 1;
               // holder.tv_Title.setText(page + " " + pageno);
              //  holder.bookImg.setVisibility(View.GONE);
                //String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + exportEnrActivity.currentBook.getBookID() + "/thumbnail_" + enrich.getEnrichmentPageNo() + ".png";
                //Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
                //holder.bookImg.setImageBitmap(bitmap);

            } else {
                Note note = notePageList.get(position);
                //int pageno = note.getPageNo() - 1;
                holder.txtTitle.setText(note.getsText());
                holder.txtDesc.setText(note.getsDesc());
                holder.txtTitle.setTextColor(Color.BLACK);
                holder.txtDesc.setTextColor(Color.BLACK);
                if (note.isNoteSelected()) {
                    holder.note_cb.setChecked(true);
                } else {
                    holder.note_cb.setChecked(false);
                }
                holder.bookImg.setVisibility(View.GONE);

            }

        }

        @Override
        public int getItemCount() {
            return Count;   // moviesList.size();
        }

    }

   public void EnrichmentSelection(Enrichments enrichh, ArrayList<Enrichments> enrichments){
        if (enrichh.isEnrichmentSelected()) {
                 boolean exist=false;
                 for (int i = 0; i < enrichments.size(); i++) {
                     Enrichments enrich = enrichments.get(i);
                     exist=false;
                     if (enrich.getEnrichmentId()== enrichh.getEnrichmentId()) {
                         exist=true;
                         break;
                     }
                 }
                 if(!exist){
                     enrichments.add(enrichh);
                 }


           } else {
             for (int i = 0; i < enrichments.size(); i++) {
                   Enrichments enrich = enrichments.get(i);
                   if (enrich.getEnrichmentId() == enrichh.getEnrichmentId()) {
                       enrichments.remove(i);
                   }
               }

        }

   }

    public void NoteSelection(Note note, ArrayList<Note> noteSelectedList){
        if (note.isNoteSelected()) {
                boolean exist=false;
                for (int i = 0; i < noteSelectedList.size(); i++) {
                    exist=false;
                    Note enrich_note = noteSelectedList.get(i);
                    if (enrich_note.getId() == note.getId()) {
                        exist=true;
                        break;

                    }
                }
            if(!exist){
                noteSelectedList.add(note);
            }


        } else {
            for (int i = 0; i < noteSelectedList.size(); i++) {
                Note enrich_note = noteSelectedList.get(i);
                if (enrich_note.getId() == note.getId()) {
                    noteSelectedList.remove(note);
                }
            }
        }
    }
}
