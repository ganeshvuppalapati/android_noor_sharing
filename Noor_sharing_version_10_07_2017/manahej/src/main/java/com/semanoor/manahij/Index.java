package com.semanoor.manahij;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.SegmentedRadioGroup;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Index extends ListActivity implements OnClickListener, android.widget.RadioGroup.OnCheckedChangeListener{
	
	private DatabaseHandler db;
	String filePath = new String();
	public String bookName;
	public ArrayList<String> indexArray = new ArrayList<String>(); 
	public ArrayList<String> bookMarkPno = new ArrayList<String>(); 
	public ArrayList<Integer> bookMarkTno = new ArrayList<Integer>();
	public ArrayList<Integer> indexPgNoArray = new ArrayList<Integer>();
	/* Enriched */
	public ArrayList<String> enrichPageArray = new ArrayList<String>();
	public ArrayList<String> finalArray = new ArrayList<String>();
	public ArrayList<String> finalListEnrichedArray = new ArrayList<String>();
	//private drawView drawLayer;
	/* Enriched */
	SegmentedRadioGroup segmentText;
	Toast mToast;
	int selectedSegment = 1;
	ListView lv;
	int pageCount;
	//int lngCategoryID;
	public int indexlistsize;
	public Boolean enriched=false;
	private LinearLayout rootviewLL;
	public Book currentBook;
	private GridView gridview;
	ArrayList<String> drawPno = new ArrayList<String>();
	ArrayList<String> drawPnoArray=new ArrayList<String>();
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		// No title Bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.index);
		
		db = new DatabaseHandler(this);
		segmentText = (SegmentedRadioGroup) findViewById(R.id.segment_text);
		segmentText.setOnCheckedChangeListener(this);
		
		//mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
		
		Intent i = getIntent();
		//bookName = i.getExtras().getString("string");
		//lngCategoryID = i.getExtras().getInt("lngID");
		currentBook = (Book) getIntent().getSerializableExtra("Book");
		
		
		bookName = currentBook.get_bStoreID();
		drawPno = getDrawingFiles();
		/* Enriched */
		if(bookName.contains("M"))
		enrichPageArray = i.getStringArrayListExtra("Enriched");
		/* Enriched */
		gridview=(GridView) findViewById(R.id.gridview);
		pageCount = currentBook.getTotalPages();
		////System.out.println("pageC:"+pageCount);
		
		selectBookMark();
		
		
		/* changes for enrich */
		if(!bookName.contains("M")){
		try {
			
			SAXParserFactory factory = SAXParserFactory.newInstance();
        	SAXParser saxParser = factory.newSAXParser();
        	
        	DefaultHandler handler = new DefaultHandler(){
        		public void startElement(String uri, String localName,String qName, 
                        Attributes attributes) throws SAXException {
        			if(qName != "Index")
            		{
        				String attrValue = attributes.getValue("Name");
        				String pgValue = attributes.getValue("Pg");
        				indexArray.add(attrValue);
        				indexPgNoArray.add(Integer.parseInt(pgValue));
            		}
        		}
        		
        	};
        	
			//File sdPath = Environment.getExternalStorageDirectory();
        	File sdPath = getFilesDir();
        	//revert_book_hide :
        	filePath = sdPath+"/."+bookName+"Book/Findex.xml";
        	//filePath = sdPath+"/"+bookName+"Book/Findex.xml";
        	InputStream inputStream = new FileInputStream(filePath);
        	Reader reader = new InputStreamReader(inputStream, "UTF-8");
        	
        	InputSource is = new InputSource(reader);
        	is.setEncoding("UTF-8");
        	
        	saxParser.parse(is, handler);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		}
		else{
			final ArrayList<String> enrich = new ArrayList<String>();
			for(int i1 = 0; i1 < enrichPageArray.size(); i1++){
				String[] arraySplit = enrichPageArray.get(i1).split("##");
				
				if(arraySplit.length > 1){
					
					for(int k=0;k<arraySplit.length;k++){
					  String[] Split = arraySplit[k].split("\\-"); 
					  
					  if(Split.length>1){
					  for(int j = 0; j < Split.length; j++){
						  //finalArray.add(i1+"|"+j);
						  if(j==0){
							  
						  }else{
						     finalArray.add(i1+"|"+k);
						  }
						 }
					  }else{
						  finalArray.add(i1+"|"+k);
					   }
					  }
					/*
					 * if(Split.length > 1){
					for(int k=0;k<Split.length;k++){
				    String[] arraySplit = Split[k].split("\\*"); 
					 for(int j = 0; j < arraySplit.length; j++){
						 finalArray.add(i1+"|"+k);
				 	 }
					}
				  }
					*/
				}
				else{
					finalArray.add(String.valueOf(i1));
				}
			}
		}
		
		loadEnrichedListArray();
		
		/* changes for enrich */
		lv = getListView();
		if(selectedSegment == 3){
		lv.setAdapter(new listAdapter(this));
		}else if(selectedSegment == 1){
			lv.setVisibility(View.GONE);
			gridview.setVisibility(View.VISIBLE);
			gridview.setAdapter(new indexthumbnailAdapter(this));
		    //pageCount= pageNumber.size();
		}
		//lv.setDividerHeight(2);
		//lv.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT,1f));
		else if(selectedSegment == 2){
			if(!bookName.contains("M")){
				//setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,indexArray));
				lv = getListView();
				lv.setAdapter(new indexlistadapter(this));
				indexlistsize=indexArray.size();
				enriched=false;
			}
			else{
				//setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,finalListEnrichedArray));
				lv = getListView();
				lv.setAdapter(new indexlistadapter(this));
				indexlistsize=finalListEnrichedArray.size();
				enriched=true;
			}
		}
		else if(selectedSegment == 4){
			//setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,finalListEnrichedArray));
			lv.setAdapter(new drawListAdapter(this));
		}
		//final ListView lv = getListView();
		//lv.setTextFilterEnabled(true);
		gridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long arg3) {
				// TODO Auto-generated method stub
				/*if(position==0){
					//System.out.println(indexPgNoArray.get(position));
				}*/
				//System.out.println(position+"pages");
				int pageno = position;
				//String[] pgValue = mahuMsg.split("\\|");
				Intent resuIntent = new Intent();
				resuIntent.putExtra("id", pageno+1);
				setResult(Activity.RESULT_OK, resuIntent);
				finish(); 
			}
		});
		lv.setOnItemClickListener(new OnItemClickListener() {

			
			//@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				if(selectedSegment == 2){
					if(enrichPageArray.size()>0){
						String mahuMsg = finalArray.get(position);
						String[] pgValue = mahuMsg.split("\\|");
						if(pgValue.length>1){
							Intent resuIntent = new Intent();
							resuIntent.putExtra("id", pgValue);
							setResult(Activity.RESULT_OK, resuIntent);
						}else{
						Intent resuIntent = new Intent();
						resuIntent.putExtra("id", Integer.parseInt(mahuMsg)+1);
						setResult(Activity.RESULT_OK, resuIntent);
						}
					}
					else{
						Intent resuIntent = new Intent();
						//resuIntent.putExtra("id", position+1);
						resuIntent.putExtra("id", indexPgNoArray.get(position));
						setResult(Activity.RESULT_OK, resuIntent);
					}
				}
				else if(selectedSegment == 3){
					 Intent resuIntent = new Intent();
					 String str1 = bookMarkPno.get(position);
					 String[] pgValue = str1.split("\\-");
					 if(!(pgValue.length>1)){
	                 String[] str = str1.split("Page ");
	                 int pNO = Integer.parseInt(str[1]) + 1;
	                 resuIntent.putExtra("id", pNO);
					 setResult(Activity.RESULT_OK, resuIntent);
					 }
					 else{
						 pgValue[0] = pgValue[0].replace("Page ", "");
						 BookView.bkmarkselected=true;
						 resuIntent.putExtra("id", pgValue);
						 setResult(Activity.RESULT_OK, resuIntent);
					 }
				}
				else if(selectedSegment == 4){

					Intent resuIntent = new Intent();
					String str1 = drawPno.get(position);
					str1 = str1.substring(0,str1.lastIndexOf(".png")); //page8-1
					String[] pgValue = str1.split("\\-");//page8,1
					if(!(pgValue.length>1)){
						String[] str = str1.split("Page ");
						int pNO = Integer.parseInt(str[1])+1;
						resuIntent.putExtra("id",pNO-1 );
						setResult(Activity.RESULT_OK, resuIntent);
					}
					else{
						String pageVal=pgValue[0].replace("Page ", "");

						int pNO=Integer.parseInt(pageVal)-1;
						pgValue[0] =""+pNO;
						resuIntent.putExtra("id", pgValue);
						setResult(Activity.RESULT_OK, resuIntent);
					}


				}
				finish(); 
			}
		});
		
		Button btnBack = (Button) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(this);
		
		RadioButton rdTblContent = (RadioButton) findViewById(R.id.segTblCon);
		RadioButton rdBookMark = (RadioButton) findViewById(R.id.segBookMark);
		btnBack.setText(getResources().getString(R.string.back));
		rdTblContent.setText(getResources().getString(R.string.table_of_contents));
		rdBookMark.setText(getResources().getString(R.string.bookmarks));
		
		rootviewLL = (LinearLayout) findViewById(R.id.indexView);
		//applyTheme(g.getSelectedTheme());
	}
private ArrayList<String> getDrawingFiles(){
		
		String path = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookName+"Book/"+"FreeFiles/Drawfiles/";
		
	    	File file = new File(path);
	    	
	    	File[] allfiles = file.listFiles();
	    	if(allfiles.length==0){
	    		return null;
	    	}
	    	else{
	    		for(int i=0;i<allfiles.length;i++){
	    			drawPnoArray.add(allfiles[i].getName());
	    		}
	    	}
	     	return drawPnoArray;
	}
/*public void applyTheme(int selectedTheme) {
		
		switch (selectedTheme) {
		case 1:
			rootviewLL.setBackgroundResource(R.drawable.shelvesbg_1);
			break;
		case 2:
			rootviewLL.setBackgroundResource(R.drawable.shelvesbg_2);
			break;
		case 3:
			rootviewLL.setBackgroundResource(R.drawable.shelvesbg_3);
			break;
		/*case 4:
			rootviewLL.setBackgroundResource(R.drawable.shelvesbg_4);
			break;
		case 5:
			rootviewLL.setBackgroundResource(R.drawable.shelvesbg_5);
			break;
		case 6:
			rootviewLL.setBackgroundResource(R.drawable.shelvesbg_6);
			break;

		default:
			rootviewLL.setBackgroundResource(R.drawable.shelvesbg_2);
			break;
		}
	}*/


	/* changes for enrich */
	private void loadEnrichedListArray() {
		// TODO Auto-generated method stub
		if(enrichPageArray.size() > 0){
			int pageInt;
			int enrichInt;
			String mahumsg = null;
			for(int i = 0; i<finalArray.size(); i++){
			String[] finalSplitArray = finalArray.get(i).split("\\|");
			if(finalSplitArray.length>1){
				pageInt = Integer.parseInt(finalSplitArray[0]);
				enrichInt = Integer.parseInt(finalSplitArray[1]);
				if(enrichInt == 0){
					String[] str = enrichPageArray.get(pageInt).split("##");
					//String[] str = enrichPageArray.get(pageInt).split("\\-");
					String[] str1 = str[enrichInt].split("U\\+202B");
					mahumsg = str1[0];
				}
				else{
					String[] str = enrichPageArray.get(pageInt).split("##");
					//String[] str = enrichPageArray.get(pageInt).split("\\-");
					String[] str1 = str[enrichInt].split("\\$\\$");
					mahumsg =str1[1]+" -";
				}
			}
			else{
				String[] str = enrichPageArray.get(Integer.parseInt(finalArray.get(i))).split("U\\+202B");
				mahumsg = str[0];
			}
			finalListEnrichedArray.add(mahumsg);
			}
		}
	}
	/* changes for enrich */

	private void selectBookMark() {
		SQLiteDatabase sql = db.getReadableDatabase();
		String query = "select PageNo,TagID,TabNo from tblBookMark where BName='"+bookName+"' order by PageNo and TabNo";
		Cursor c = sql.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			if(c.getInt(2)==0){
				bookMarkPno.add("Page "+(c.getInt(0)-1));
			}else{
				bookMarkPno.add("Page "+(c.getInt(0)-1)+"-"+c.getInt(2));
			}
			////System.out.println("bookMarkPNo:"+bookMarkPno);
			bookMarkTno.add(c.getInt(1));
			////System.out.println("bookMarkTNo:"+bookMarkTno);
			//bookMarkTabno.add(c.getInt(2));
			c.moveToNext();
		}
		c.close();
		sql.close();
	}
	
	//list adapter for custom index page
    public class indexlistadapter extends BaseAdapter
    {

		public indexlistadapter(Index index) 
		{
			// TODO Auto-generated constructor stub
		}

		public int getCount() 
		{
			// TODO Auto-generated method stub
			return indexlistsize;
		}

		public Object getItem(int position) 
		{
			// TODO Auto-generated method stub
			return null;
		}

		public long getItemId(int position) 
		{
			// TODO Auto-generated method stub  android:paddingRight="10dp"
			return 0;
		}

		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			// TODO Auto-generated method stub
			View indexlistview = convertView;
			if(convertView == null)
			indexlistview = getLayoutInflater().inflate(R.layout.indexlistview, null);
			TextView tv_indexlistview = (TextView) indexlistview.findViewById(R.id.tv_indexlistview);
			TextView tv_enrich = (TextView) indexlistview.findViewById(R.id.tv_enrich);
			if(enriched==true)
			{   
				if(finalListEnrichedArray.get(position).contains(" -")){
				   //System.out.println("enrichment");
				   tv_indexlistview.setVisibility(View.INVISIBLE);
				   tv_enrich.setVisibility(View.VISIBLE);
				   tv_enrich.setText(finalListEnrichedArray.get(position));
				 }else{
				   tv_enrich.setVisibility(View.INVISIBLE);
				   tv_indexlistview.setVisibility(View.VISIBLE);
				   tv_indexlistview.setText(finalListEnrichedArray.get(position));
			    }
			}
			else
			{
				tv_indexlistview.setText(indexArray.get(position));
			}
			return indexlistview;
		}
		
    }

    public class indexthumbnailAdapter extends BaseAdapter
    {

		public indexthumbnailAdapter(Index index) 
		{
			// TODO Auto-generated constructor stub
		}

		public int getCount() 
		{
			// TODO Auto-generated method stub
			return pageCount;
		}

		public Object getItem(int position) 
		{
			// TODO Auto-generated method stub
			return null;
		}

		public long getItemId(int position) 
		{
			// TODO Auto-generated method stub
			return 0;
		}

		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			// TODO Auto-generated method stub
			
			View indexlistview = convertView;
			if(convertView == null){
			indexlistview = getLayoutInflater().inflate(R.layout.thumbnailindex, null);
			}
			ImageView img_indexlistview = (ImageView) indexlistview.findViewById(R.id.imageView2);
			TextView tv_pageno= (TextView) indexlistview.findViewById(R.id.tv_pageno);
			int pageNumber=position+1;
			
			if( pageNumber == 1) {
				tv_pageno.setText(R.string.cover_page);
			    String thumbnailImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+"FreeFiles/frontThumb.png";
				//}
				Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
				img_indexlistview.setImageBitmap(bitmap);
					
			}else if (pageNumber ==pageCount) {
				String page = getResources().getString(R.string.end_page);
				tv_pageno.setText(page);
				String thumbnailImagePath=Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+currentBook.getBookID()+"/thumbnail_end.png";
				if(!new File(thumbnailImagePath).exists()){
					thumbnailImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+"FreeFiles/backThumb.png";
				}
				Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
				img_indexlistview.setImageBitmap(bitmap);
			}else{
				//int pno = position;
				int pno = pageNumber-1;
				//String page = getResources().getString(R.string.page);
				tv_pageno.setText(pno+" ");
				//System.out.println(position+"position");
				String thumbnailImagePath=Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+currentBook.getBookID()+"/thumbnail_"+pageNumber+".png";
				Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
				img_indexlistview.setImageBitmap(bitmap);
			 }
			return indexlistview;
		}

	 }
	private class listAdapter extends BaseAdapter{

		private Context mContext;
		
		 public Integer[] mThumbIds = {
	               R.drawable.bm1,
	               R.drawable.bm2,
	               R.drawable.bm3,
	               R.drawable.bm4
	        };
		 
		public listAdapter(Context context){
			mContext = context;
		}
		//@Override
		public int getCount() {
			// TODO Auto-generated method stub
			////System.out.println("idexSize:"+bookMarkPno.size());
			return bookMarkPno.size();
		}

		//@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		//@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		//@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View vi = convertView;
			if(convertView == null)
				vi = getLayoutInflater().inflate(R.layout.indexbookmark, null);
				TextView tv = (TextView)vi.findViewById(R.id.index_tv);
				String str = bookMarkPno.get(position);
				if(str.equals("Page 0"))
				{	
					tv.setText(getResources().getString(R.string.cover_page));
				}
				else if(str.equals("Page "+(pageCount-1))){
					tv.setText(getResources().getString(R.string.last_cover_page));
				}
				else{
					tv.setText(bookMarkPno.get(position));
				}
			  /*ImageView iv = (ImageView)vi.findViewById(R.id.index_iv);
				Bitmap bitmap = BitmapFactory.decodeResource(getResources(), 
	                    mThumbIds[bookMarkTno.get(position)-1]);
	    		iv.setImageBitmap(bitmap);*/
				return vi;
		}
		
	}
private class drawListAdapter extends BaseAdapter{
		
		private Context mContext;
		
		
		public drawListAdapter(Context context){
			mContext = context;
		}
		//@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return drawPnoArray.size();
		}

		//@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		//@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		//@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View vi = convertView;
			if(convertView == null)
			vi = getLayoutInflater().inflate(R.layout.indexpaint, null);
			TextView tv = (TextView)vi.findViewById(R.id.index_paint);
			
			//String str1 = drawPno.get(position);
			//str1 = str1.substring(0,str1.lastIndexOf(".png"));
			String str1 = drawPno.get(position);                     //page4-1.png
			 str1 = str1.substring(0,str1.lastIndexOf(".png"));       //page4-1
			 String[] pgValue = str1.split("\\-");                       //page4,1
			 if((pgValue.length>1)){                                 //true
				
            String[] str = pgValue[0].split("Page ");
            int pNO = Integer.parseInt(str[1])-1;
           // tv.setText("Page "+pNO);
            tv.setText("Page "+pNO+"-"+pgValue[1]);
			 }else{
				 String[] str = str1.split("Page ");
		            int pNO = Integer.parseInt(str[1])-1;
				 tv.setText("Page "+pNO);
			 }
					
	     	return vi;
		}
		
	}
	//@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.btnBack){
			finish();
		}
	}

	//@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		if(group == segmentText){
			if(checkedId == R.id.segTblCon){
				selectedSegment = 2;
				lv.setVisibility(View.VISIBLE);
				gridview.setVisibility(View.GONE);
		        //setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,indexArray));
				if(!bookName.contains("M")){
					//setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,indexArray));
					lv.setAdapter(new indexlistadapter(this));
					indexlistsize=indexArray.size();
					enriched=false;
				}
				else{
					//setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,finalListEnrichedArray));
					lv.setAdapter(new indexlistadapter(this));
					indexlistsize=finalListEnrichedArray.size();
					enriched=true;
				}
				//mToast.setText("TableContent");
				//mToast.show();
			}
			else if(checkedId == R.id.segBookMark){
				selectedSegment = 3;
				lv.setVisibility(View.VISIBLE);
				gridview.setVisibility(View.GONE);
		
				lv.setAdapter(new listAdapter(this));
				//mToast.setText("BookMark");
				//mToast.show();
			}
			else if(checkedId == R.id.segIndex){
				selectedSegment = 1;
				lv.setVisibility(View.GONE);
				gridview.setVisibility(View.VISIBLE);
		        gridview.setAdapter(new indexthumbnailAdapter(this));
			}
			else if(checkedId == R.id.segDrawing){
				selectedSegment = 4;
				lv.setVisibility(View.VISIBLE);
				gridview.setVisibility(View.GONE);
		
				lv.setAdapter(new drawListAdapter(this));
				
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		db.unbindDrawables(findViewById(R.id.indexView));
		System.gc();
	}
}
