package com.semanoor.manahij;

import android.view.View;
import android.widget.TextView;

import com.semanoor.source_sboookauthor.DatabaseHandler;

import java.util.ArrayList;

/**
 * Created by karthik on 30-11-2016.
 */
public class NoteData {
    private static NoteData ourInstance;
    private int notePageNo;
    private String noteTitle;
    private String noteDesc;
    private int noteTabNo;
    private int noteColor;
    private int noteType;



    private  View noTitle=null;
    private ArrayList<NoteData> noteDataArrayList = new ArrayList<>();

    public int getNotePageNo() {
        return notePageNo;
    }

    public void setNotePageNo(int notePageNo) {
        this.notePageNo = notePageNo;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public String getNoteDesc() {
        return noteDesc;
    }

    public void setNoteDesc(String noteDesc) {
        this.noteDesc = noteDesc;
    }

    public int getNoteTabNo() {
        return noteTabNo;
    }

    public void setNoteTabNo(int noteTabNo) {
        this.noteTabNo = noteTabNo;
    }

    public int getNoteColor() {
        return noteColor;
    }

    public void setNoteColor(int noteColor) {
        this.noteColor = noteColor;
    }

    public ArrayList<NoteData> getNoteDataArrayList() {
        return noteDataArrayList;
    }

    public void setNoteDataArrayList(ArrayList<NoteData> noteDataArrayList) {
        this.noteDataArrayList = noteDataArrayList;
    }

    public int getNoteType() {
        return noteType;
    }

    public void setNoteType(int noteType) {
        this.noteType = noteType;
    }
    public View getNoTitle() {
        return noTitle;
    }

    public void setNoTitle(View noTitle) {
        this.noTitle = noTitle;
    }

}
