package com.semanoor.manahij;

/**
 * Created by karthik on 11-01-2017.
 */

public interface ApiCallBackListener<T> {
    public void onSuccess(T object);
    public void onFailure(Exception e);
}

