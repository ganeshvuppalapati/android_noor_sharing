package com.semanoor.manahij.mqtt;



import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.semanoor.manahij.ExportEnrGroupActivity;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.manahij.mqtt.datamodels.OnlineStatus;
import com.semanoor.source_sboookauthor.DatabaseHandler;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by GaneshVeera on 22/05/17.
 */

public class NoorUserStatus extends AsyncTask<Void, Void, String> {
    JSONArray jsonArray;
    ArrayList<OnlineStatus> onlistList = new ArrayList<OnlineStatus>();
Context mContext;

    public NoorUserStatus(Context context,JSONArray mJsonArray) {
        this.jsonArray = mJsonArray;
        mContext=context;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constants.NOOR_USER_STATUS);
        try {
            JSONObject obj = new JSONObject();
            obj.put("identificationString", "0");
            obj.put("userObjects", jsonArray);
            httppost.setEntity(new StringEntity(obj.toString(), "UTF8"));
            httppost.setHeader("Content-type", "application/json");
            HttpResponse response = httpclient.execute(httppost);

            if(response!=null)
            return EntityUtils.toString(response.getEntity());
        } catch (ClientProtocolException e) {

        } catch (IOException e) {
            Log.e("noorstatus", e.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
        Log.e("res",aVoid.toString());
        parseData(aVoid);

    }

    private void parseData(String response) {


        try {
            JSONObject obj = new JSONObject(response);
            JSONArray userStatus = obj.getJSONArray("onlineUserStatus");
            for (int i=0;i<userStatus.length();i++){
                JSONObject inobj= userStatus.getJSONObject(i);
                String emailId = inobj.getString("emailId");
                String lastUpdatedTimeStamp = inobj.getString("lastUpdatedTimeStamp");
                String onlineStaus = inobj.getString("onlineStaus");
                OnlineStatus status = new OnlineStatus(emailId,onlineStaus);
                onlistList.add(status);

            }
           // callonline.response(onlistList);


            Intent intent = new Intent();
            intent.setAction(ExportEnrGroupActivity.MyBroadcastReceiver.ACTION);
            intent.putParcelableArrayListExtra("onlinlist",onlistList);
            mContext.sendBroadcast(intent);
        } catch (JSONException e) {
            Log.e("responseEx",e.toString());
        }

    }

   /* private void updateOnlineStatustoContacts(String emailId, String onlineStaus) {
        DatabaseHandler db = new DatabaseHandler(mContext);
        db.updateOnlineStatustoContacts(emailId,onlineStaus);
    }*/


}






