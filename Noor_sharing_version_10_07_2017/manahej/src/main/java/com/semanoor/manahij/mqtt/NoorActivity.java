package com.semanoor.manahij.mqtt;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.semanoor.manahij.ExportEnrGroupActivity;
import com.semanoor.manahij.MainActivity;
import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.R;
import com.semanoor.manahij.SlideMenuWithActivityGroup;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;

import org.json.JSONObject;

import java.util.Timer;

/**
 * Created by Pushpa on 13/06/17.
 */

public class NoorActivity extends FragmentActivity implements NetWorkBroadcastReceiver.ConnectivityReceiverListener {

    public Dialog callDialog, mqttdialog ;
    MQTTSubscriptionService service;
    public static final String FILTER = "noor.filter.call";
    private final int interval = 10000;
    private Handler handler = new Handler();
  boolean isNetwork=false;
    private DatabaseHandler db;
    ManahijApp app;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        service= new MQTTSubscriptionService();
        app=(ManahijApp) getApplicationContext();
        db = DatabaseHandler.getInstance(this);
       /* if(!ManahijApp.getInstance().isNetworkStatus()) {
            createNetErrorDialog();
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // MQTTSubscriptionService client = new MQTTSubscriptionService();
       // LocalBroadcastManager.getInstance(this).unregisterReceiver(msgreceiver);

    }

    @Override
    protected void onResume() {
      if(!ManahijApp.getInstance().isInternetAvailable()) {
           if(!isnetWrk)
          createNetErrorDialog();
      }
        ManahijApp.getInstance().setConnectivityListener(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(msgreceiver, new IntentFilter(FILTER));
       /* if (!app.getState()*//* ManahijApp.getInstance().getPrefManager().getIsinBackground()*//*) {
            if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                startService(new Intent(NoorActivity.this, CallConnectionStatusService.class));
            }
        }*/
        super.onResume();
    }

    @Override
    protected void onPause() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(msgreceiver);
        if(!app.getState()   /*ManahijApp.getInstance().getPrefManager().getIsinBackground()*/){
           // service.disConnectCall();
           // stopService(new Intent(NoorActivity.this, CallConnectionStatusService.class));
           // stopService(new Intent(NoorActivity.this, MQTTSubscriptionService.class));


        }
        super.onPause();
    }





    BroadcastReceiver msgreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          //  Log.e("camenoor","came");
            try {
                ManahijApp app = (ManahijApp)getApplicationContext();
                switch (intent.getStringExtra(Constants.KEYTYPE)) {

                    case Constants.KEYCALLREQUEST:

                         app.setCallState(true);
                        callConnectionDialog(intent.getStringExtra(Constants.KEYMESSAGE));
                        break;
                    case Constants.KEYCALLREQUESTR:
                        callDialog.dismiss();
                        app.setCallState(false);
                        Toast.makeText(context,"Call cancelled",Toast.LENGTH_LONG).show();
                        break;
                    case Constants.KEYCALLDISCONNECTIONREQUEST:
                        app.setCallState(false);
                        service.sendUsersList(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(),String.valueOf(1),getApplicationContext());
                        service.unSubScribeTopic(ManahijApp.getInstance().getPrefManager().getMainChannel(), getApplicationContext());
                        service.subscribeToNewTopic(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(), getApplicationContext());
                        ManahijApp.getInstance().getPrefManager().addIsMainChannel(false);
                        stopService(new Intent(context, CallConnectionStatusService.class));
                        break;
                    case "call_waiting":
                        JSONObject o = new JSONObject(intent.getStringExtra(Constants.KEYMESSAGE));

                        Toast.makeText(context,  o.getString("user_name")+" is in Waiting",Toast.LENGTH_SHORT).show();
                        break;
                    /*case "user_is_in_another_call":
                        callDialog.dismiss();
                        Toast.makeText(context,"User is in another call",Toast.LENGTH_LONG).show();
                        break;*/
                    case "status":


                        switch (intent.getExtras().getInt(Constants.MQTT_STATUS))
                        {   case 1:
                            if(mqttdialog!=null) {
                                mqttdialog.dismiss();
                                mqttdialog.cancel();
                            }
                            break;
                            case  0:
                                //failure
                                if (!ManahijApp.getInstance().isInternetAvailable()){
                                    if(!isnetWrk)
                                        createNetErrorDialog();
                                }else {
                                    connectionLostDailog();
                                }
                               // connectionLostDailog();
                            break;
                            case  2:
                                //connection lost
                              /* if (!ManahijApp.getInstance().isInternetAvailable()){
                                     if(!isnetWrk)
                                       createNetErrorDialog();
                                }
                                else {
                                   ((ManahijApp) getApplication()).startSubscriptionService();
                               }*/
                                if (!ManahijApp.getInstance().isInternetAvailable()){
                                    if(!isnetWrk)
                                        createNetErrorDialog();
                                }else {
                                    connectionLostDailog();
                                }
                               // connectionLostDailog();
                                break;
                        }
                     /*  if(isOnline()){
                            Toast.makeText(context,"online",Toast.LENGTH_LONG).show();
                            if(intent.getExtras().getInt(Constants.MQTT_STATUS)==1){
                                if(mqttdialog.isShowing()) {
                                    mqttdialog.dismiss();
                                    mqttdialog.cancel();
                                }
                            }else {
                                connectionLostDailog();
                            }
                        }else{
                            startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                        }*/
                        // Toast.makeText(context,"Status",Toast.LENGTH_LONG).show();
                        break;
                    /*case "NETWORK":
                        Log.e("camen","came");
                        JSONObject ob = new JSONObject();
                        isNetwork = ob.getBoolean("network_status");

                        *//*Intent in = new Intent(Intent.ACTION_MAIN);
                        intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
                        startActivity(in);*//*
                        if(isNetwork){
                            connectionLostDailog();
                        }else {
                            try {
                                Intent in = new Intent(Intent.ACTION_MAIN);
                                intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
                                startActivity(in);
                            }catch (Exception e){
                                Log.e("Ex at noconnection",e.toString());
                            }
                        }
                        break;*/
                }



            }
            catch (Exception e){
                Log.e("nooor",e.toString());
            }
        }
    };


    private void callConnectionDialog(String obj) {
        String topicname=null,publishTopic=null;


        try{
            JSONObject objj = new JSONObject(obj);
            topicname= objj.getString("user_name");
            publishTopic= objj.getString("topic_name");
            Log.e("ec",publishTopic+topicname);
        }catch (Exception e){
            Log.e("ex",e.toString());
        }
        callDialog = new Dialog(this);
        callDialog.setContentView(R.layout.call_request);
        callDialog.setCancelable(false);
        callDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView call = (TextView) callDialog.findViewById(R.id.callmsg);
        TextView callname = (TextView) callDialog.findViewById(R.id.callname);
        ImageView accept = (ImageView) callDialog.findViewById(R.id.accept);
        ImageView reject = (ImageView) callDialog.findViewById(R.id.reject);
        if(topicname!=null) {
            call.setText(topicname+" Calling you..");
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        String userName = prefs.getString(Globals.sUserNameKey, "");
        if(userName!=null){
            callname.setText(userName);
        }
        final String finalPublishTopic = publishTopic;
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                     ManahijApp app = (ManahijApp)getApplicationContext();
                    app.setCallState(true);
                    JSONObject obj= new JSONObject();
                    String sendtopic= ManahijApp.getInstance().getPrefManager().getSubscribeTopicName()+"_m";
                    obj.put("type",Constants.KEYCALLCONNECTION);
                    obj.put("action","call_accept");
                    obj.put("topic_to_subsribe",sendtopic);
                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    service.publishMsg(finalPublishTopic,obj.toString(),NoorActivity.this);
                    service.unSubScribeTopic(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(),NoorActivity.this);
                    service.subscribeToNewTopic(sendtopic,NoorActivity.this);
                    service.sendUsersList(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(),String.valueOf(2),NoorActivity.this);
                    ManahijApp.getInstance().getPrefManager().addMainChannel(sendtopic);
                    ManahijApp.getInstance().getPrefManager().addIsMainChannel(true);
                    db.updateInCall();
                    //callbackInterface.onStatusUpdate(true);
                    startService(new Intent(NoorActivity.this, CallConnectionStatusService.class));
                    if(!app.getMainState()) {
                        onBackPressed();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                callDialog.dismiss();
            }
        });
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    //startService(new Intent(MainActivity.this, CallConnectionStatusService.class));
                    JSONObject obj = new JSONObject();
                    obj.put("type", Constants.KEYCALLCONNECTION);
                    obj.put("action", "call_reject");
                    app.setCallState(false);
                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    service.publishMsg(finalPublishTopic, obj.toString(), getApplicationContext());
                }catch (Exception e){
                    e.printStackTrace();
                }
                callDialog.dismiss();
            }
        });

        callDialog.show();

    }



    private void connectionLostDailog() {

        mqttdialog = new Dialog(NoorActivity.this); // Context, this, etc.
        mqttdialog.setContentView(R.layout.connectionlostdailog);
        mqttdialog.setCancelable(false);
        Button cancel = (Button) mqttdialog.findViewById(R.id.cancelconnection);
        Button ok = (Button) mqttdialog.findViewById(R.id.connect);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ManahijApp) getApplication()).stopSubscriptionService();

                mqttdialog.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ((ManahijApp) getApplication()).stopSubscriptionService();

                ((ManahijApp) getApplication()).startSubscriptionService();
            /*    Runnable runnable = new Runnable(){
                    public void run() {
                    }
                };
                handler.postDelayed(runnable, interval);

*/
                mqttdialog.dismiss();
            }
        });
        mqttdialog.show();
    }

    public void onBackPressed() {
        Intent mainActivity = new Intent(/*Intent.ACTION_MAIN*/ this,MainActivity.class);
       // mainActivity.addCategory(Intent.CATEGORY_HOME);
        mainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainActivity);
        finish();
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
       // if(!app.isInternetAvailable()) {
            showSnack(isConnected);
           // app.setNetworkState(isConnected);
       // }
    }

    private void showSnack(boolean isConnected) {
        String message;
        if (isConnected) {
            stopService(new Intent(NoorActivity.this, MQTTSubscriptionService.class));
            startService(new Intent(NoorActivity.this, MQTTSubscriptionService.class));
            message = "Good! Connected to Internet";

        } else {
            message = "Sorry! Not connected to internet";
             if(!isnetWrk)
                createNetErrorDialog();

        }
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();

    }

    boolean isnetWrk=false;
    protected void createNetErrorDialog() {

        isnetWrk= true;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        AlertDialog alert = null;
       // final AlertDialog finalAlert = alert;
        builder.setMessage("You need internet connection for this app. Please turn on mobile network or Wi-Fi in Settings.")
                .setTitle("Unable to connect")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                              //  alert.dismiss();
                                isnetWrk=false;
                                Intent i = new Intent(Settings.ACTION_SETTINGS  /*Settings.ACTION_SETTINGS*/);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                /*Intent in = new Intent(Intent.ACTION_MAIN);
                                in.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
                                startActivity(in);*/
                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //finalAlert.cancel();
                             isnetWrk=false;
                            }
                        }
                );
        alert = builder.create();
        alert.show();
    }

}
