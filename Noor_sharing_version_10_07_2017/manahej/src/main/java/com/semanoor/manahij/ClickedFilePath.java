package com.semanoor.manahij;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

/**
 * Created by Krishna on 06-08-2015.
 */
public class ClickedFilePath {
    private int folderId;
    private String folderTitle;
    private boolean folderSelected;
    private Context context;
    private String folderPath;
    private boolean file;
    private boolean folder;
    private boolean isSelected;


    public ClickedFilePath(Context _context) {
        this.context = _context;
    }
    public int getFolderId() {
        return folderId;
    }

    public void setFolderId(int FolderId) {
        this.folderId = FolderId;
    }

    public String getFolderTitle() {
        return folderTitle;
    }

    public void setFolderTitle(String FolderTitle) {
        this.folderTitle = FolderTitle;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String FolderPath) {
        this.folderPath = FolderPath;
    }

    public boolean isFolderSelected() {
        return folderSelected;
    }

    public void setFolderSelected(boolean FolderSelected) {
        this.folderSelected = FolderSelected;
    }

    public Button createFolderTabs() {
        RelativeLayout layout = new RelativeLayout(context);
        layout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));

        final Button btnEnrichmentTab = new Button(context);
        //btnEnrichmentTab.setId(R.id.enrichmentId);
        btnEnrichmentTab.setTag(this);
        btnEnrichmentTab.setTextColor(context.getResources().getColor(R.color.white));

        btnEnrichmentTab.setText(this.folderTitle);
        btnEnrichmentTab.setSingleLine(true);
        btnEnrichmentTab.setEllipsize(TextUtils.TruncateAt.END);
       // btnEnrichmentTab.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) context.getResources().getDimension(R.dimen.enrichments_tex_size));
        btnEnrichmentTab.setBackgroundResource(R.drawable.btn_enrich_tab);
        //btnEnrichmentTab.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrownext,0);

        btnEnrichmentTab.setGravity(Gravity.CENTER);
        layout.addView(btnEnrichmentTab);
        btnEnrichmentTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FileManagerActivity)context).DisplayingFiles((Button)v);

            }
        });
        ((FileManagerActivity)context).ll_container.addView(layout, new RelativeLayout.LayoutParams((int)context.getResources().getDimension(R.dimen.enrichment_btn_width), (int)context.getResources().getDimension(R.dimen.enrichment_btn_height)));
        return btnEnrichmentTab;
    }


    public boolean isFile() {
        return file;
    }

    public void setFile(boolean file) {
        this.file = file;
    }

    public boolean isFolder() {
        return folder;
    }

    public void setFolder(boolean folder) {
        this.folder = folder;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

