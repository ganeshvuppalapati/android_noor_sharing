package com.semanoor.manahij;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfRenderer;
import android.menu.ActionItem;
import android.menu.QuickAction;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.artifex.mupdfdemo.Annotation;
import com.artifex.mupdfdemo.MuPDFPageView;
import com.artifex.mupdfdemo.MuPDFReaderView;
import com.artifex.mupdfdemo.MuPDFView;
import com.artifex.mupdfdemo.MyNoteDb;
import com.artifex.mupdfdemo.NotesEditText;
import com.artifex.mupdfdemo.PageView;
import com.artifex.mupdfdemo.SearchCallBack;
import com.artifex.mupdfdemo.SearchResult;
import com.artifex.mupdfdemo.SearchTaskResult;
import com.artifex.mupdfdemo.Selectionrectangle;
import com.ptg.mindmap.widget.AppDict;
import com.ptg.mindmap.widget.LoadMindMapContent;
import com.ptg.views.CircleButton;
import com.ptg.views.TwoDScrollView;
import com.semanoor.colorpicker.ColorsRead;
import com.semanoor.sboookauthor_store.BookMarkEnrichments;
import com.semanoor.sboookauthor_store.EnrichmentButton;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.CustomWebRelativeLayout;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.PopupColorPicker;
import com.semanoor.source_sboookauthor.TextCircularProgressBar;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static android.graphics.Bitmap.createBitmap;


public class pdfActivity extends Activity implements View.OnClickListener, PopoverView.PopoverViewDelegate, DialogInterface.OnDismissListener, Selectionrectangle, SearchCallBack, TextWatcher, searchBackGround {


    private int bookViewActivityrequestCode = 1001;
    private int indexActivityRequestCode = 1002;
    FrameLayout pdfView;
    EditText etAdv;
    TextCircularProgressBar progressBar;
    EditText searchEditText;
    private Dialog advSearchPopUp;
    private TextView tv_noresult;
    private ImageButton imgBtnBack;
    private int deviceWidth, deviceHeight;
    private EditText etSearch;
    private boolean isAdvancedSearch = false;
    public LinearLayout et_layout;
    //String pdfName = SAMPLE_FILE;
    String pdfName;
    Book currentBook;
    int sb_pNo;
    int pageCount;
    int currentPageNumber;
    SeekBar pdf_seekbar;
    TextView tvSeekBar;
    RelativeLayout designPageLayout, parentEnrRLayout, parentBkmrkLayout, back_layout, annotationLayout, acceptLayout;
    LinearLayout top_bar_layout;
    DatabaseHandler db;
    Button btnBookMark, btnEnrichEdit, btnEnrHome, annotation, btnViewing,search_img,arrow_btn,search_btn, completeAction, searchButton, addNote, noteCount, btnEnrResDownload, btnStudyCards, hideSearch;
    GridShelf gridShelf;
    LinearLayout ll_container, ll_bookmark_container,search_layout,icon_layout;
    WebView pdf_enrich;
    int currentEnrichmentTabId;
    public Enrichments currentBtnEnrichment;
    private Enrichments selectedEnr;
    String currentBookPath;
    int screenShotTime;
    ProgressBar progress;
    public boolean bkmInEditMode, bkmInDeleteMode;
    PopoverView advbkmarkpopup;
    ListView adv_bkmrktitle;
    ArrayList<BookMarkEnrichments> currentBookMarkEnrList;
    HorizontalScrollView sv, btn_bkmark_Sv;
    int arabicPage;
    private static final String SAMPLE_FILE = "big.pdf";
    private static final String FILE_PATH = "filepath";
    private static final String FILE_NAME = "filename";
    private static final String SEARCH_TEXT = "text";
    private PdfFragment fragment;

    Button btnBrush;
    String brushColor = null;
    ImageButton btn_BrushColor;
    Button buttonDone;
    RelativeLayout drawLayout;
    Button clearDraw;
    ColorsRead colosRead;
    Button mindmap, stamp;
    FrameLayout mindMapLayout, stampLayout,downloadLayout;


    private AlertDialog.Builder mAlertBuilder;
    private static final int ID_HIGHLIGHT = 1;
    private static final int ID_COPY = 2;
    private static final int ID_UNDERLINE = 3;
    private static final int ID_INK = 4;
    private static final int ID_STRIKE_OUT = 5;
    private static final int ID_SEARCH = 6;
    private static final int ID_NOTE = 7;
    private static final int ID_UNHILIGHT = 8;
    private static final int ID_SPEECH = 11;

    private static final int ID_UNSTRIKE_OUT = 9;
    private static final int ID_UNUNDERLINE = 10;

    ImageButton cancelSearch, searchBack, searchForward;
    EditText searchText;
    RelativeLayout searchLayout;
    QuickAction quickAction;
    QuickAction quickActionHilight;
    NotesEditText et;
    public String searchString = "";
    MuPDFView pageView;
    boolean success = false;
    String noteText = "";
    ActionItem highlightItem;
    boolean noteClikced = false;
    ActionItem underLineItem;
    ActionItem strikeItem;


    //color picker
    ImageButton preset_pencil;
    SeekBar brushSize, opacity;
    int brushThickness;
    int isSeekBarChanged = 0;
    AsyncTask searchTask;
    public TextToSpeech textToSpeech;
    String fileName = "";
    private RecyclerView CurrentRecyclerView,subRecyclerView,subRecylerView1;
    private EditText et_search;
    private RelativeLayout rootView;
    public GestureDetector gestureDetector;
    private boolean isLoadEnrInpopview;
    private View gestureView;
    private PopoverView popoverView;
    private RelativeLayout subTabsLayout,subTabLayout1,enr_layout;
    public RelativeLayout add_btn_layout;
    private RecyclerView clickableRecyclerView;
    private Button add_btn;
    private TreeStructureView treeStructureView;
    private int categoryId;
    private InternetSearch internetSearch;
    private CircleButton dropdown_btn;
    final static android.os.Handler mWebviewVisibility = new Handler();
    final Runnable mDoneNote = new Runnable() {
        public void run() {
            db.addPdfNotes(fileName, fragment.core.getPageNumber(), selectedRectangle, noteText);
            //	access.setMynotename(mFilenameView.getText().toString());
            //	access.setMyNotepg(pagenum+1);
        }
    };


    //Displaying Menu while selecting Text..........
    public void prepareMenu() {
        ActionItem coptItem = new ActionItem(ID_COPY, "Copy", getResources().getDrawable(com.artifex.mupdfdemo.R.drawable.ic_clipboard));
        highlightItem = new ActionItem(ID_HIGHLIGHT, "Highlight", getResources().getDrawable(com.artifex.mupdfdemo.R.drawable.ic_highlight));
        underLineItem = new ActionItem(ID_UNDERLINE, "Underline", getResources().getDrawable(com.artifex.mupdfdemo.R.drawable.ic_underline));
        ActionItem inkItem = new ActionItem(ID_INK, "Ink", getResources().getDrawable(com.artifex.mupdfdemo.R.drawable.ic_pen));
        strikeItem = new ActionItem(ID_STRIKE_OUT, "Strikeout", getResources().getDrawable(com.artifex.mupdfdemo.R.drawable.ic_strike));
        final ActionItem noteItem = new ActionItem(ID_NOTE, "Note", getResources().getDrawable(R.drawable.ic_pen));
        ActionItem searchItem = new ActionItem(ID_SEARCH, "Search", getResources().getDrawable(R.drawable.ic_pen));
        ActionItem speechItem = new ActionItem(ID_SPEECH, "Speech", getResources().getDrawable(R.drawable.ic_pen));

        highlightItem.setSticky(true);
        coptItem.setSticky(true);
        quickAction = new QuickAction(this, QuickAction.HORIZONTAL);

        //add action items into QuickAction
        quickAction.addActionItem(coptItem);
        quickAction.addActionItem(highlightItem);
        quickAction.addActionItem(underLineItem);
        quickAction.addActionItem(strikeItem);
        quickAction.addActionItem(noteItem);
        quickAction.addActionItem(searchItem);
        quickAction.addActionItem(speechItem);


        quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionid) {
                ActionItem actionItem = quickAction.getActionItem(pos);
                int actionId = actionItem.getActionId();
                pageView = (MuPDFView) fragment.mDocView.getDisplayedView();

                //here we can filter which action item was clicked with pos or actionId parameter
                if (actionId == ID_UNDERLINE) {
                    if (pageView != null)
                        success = pageView.markupSelection(Annotation.Type.UNDERLINE);
                    noteClikced = false;
                    clearSelection();
                } else if (actionId == ID_HIGHLIGHT) {
                    if (pageView != null)
                        success = pageView.markupSelection(Annotation.Type.HIGHLIGHT);
                    noteClikced = false;
                    clearSelection();
                } else if (actionId == ID_UNHILIGHT) {
                    Log.d("", "");
                    MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                    if (pageView != null)
                        pageView.deleteSelectedAnnotation();
                    clearSelection();
                    noteClikced = false;
                } else if (actionId == ID_STRIKE_OUT) {
                    if (pageView != null)
                        success = pageView.markupSelection(Annotation.Type.STRIKEOUT);
                    clearSelection();
                    noteClikced = false;
                } else if (actionId == ID_INK) {
                    completeAction.setVisibility(View.VISIBLE);
                    searchButton.setVisibility(View.GONE);
                    fragment.mDocView.setMode(MuPDFReaderView.Mode.Drawing);

                    clearSelection();
                    noteClikced = false;
                } else if (actionId == ID_COPY) {
                    MuPDFPageView pView = (MuPDFPageView) fragment.mDocView.getDisplayedView();
                    String copiedText = null;
                    String convertedCopiedText = null;
                    if (pageView != null)
                        copiedText = pageView.getSelectedText();
                    if (pView.doubleTap)
                        copiedText = pView.selectedWord;
                    if (!fragment.core.isEnglish()) {
                        if (copiedText != null && copiedText.length() > 0)
                            convertedCopiedText = fragment.core.convertEncodedToNoramalTextWithSelectedText(copiedText, currentPageNumber);

                    } else {
                        convertedCopiedText = copiedText;
                    }
                    int currentApiVersion = Build.VERSION.SDK_INT;
                    if (currentApiVersion >= Build.VERSION_CODES.HONEYCOMB) {
                        android.content.ClipboardManager cm = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

                        cm.setPrimaryClip(ClipData.newPlainText("MuPDF", convertedCopiedText));
                    } else {
                        android.text.ClipboardManager cm = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        cm.setText(convertedCopiedText);
                    }
                    //generatetext(fragment.core.getTextofPage(currentPageNumber));
                    clearSelection();
                    noteClikced = false;
                    // ص والتوثيق؛ تب
                } else if (actionId == ID_SEARCH) {


                    MuPDFPageView pView = (MuPDFPageView) fragment.mDocView.getDisplayedView();
                    if (pageView != null) {
                        searchLayout.setVisibility(View.VISIBLE);
                        top_bar_layout.setVisibility(View.INVISIBLE);
                        back_layout.setVisibility(View.VISIBLE);
                        SearchTaskResult.set(null);
                        searchString = "";
                        fragment.mDocView.resetupChildren();
                        PageView view1 = (PageView) fragment.mDocView.getDisplayedView();
                        String selectedtext = pageView.getSelectedText();
                        if (pView.doubleTap)
                            selectedtext = pView.selectedWord;
                        if (selectedtext.length() > 0) {
                            if (!fragment.core.isEnglish()) {
                                if (selectedtext.equals("")) {
                                    searchText.setText(fragment.core.convertEncodedToNoramalTextWithSelectedText(view1.getSelectedText2(), currentPageNumber));

                                    //   advancedSearch(fragment.core.convertEncodedToNoramalTextWithSelectedText(view1.getSelectedText2(), currentPageNumber));
                                    searchString = view1.getSelectedText2();
                                } else {
                                    searchText.setText(fragment.core.convertEncodedToNoramalTextWithSelectedText(selectedtext, currentPageNumber));
                                    // advancedSearch(fragment.core.convertEncodedToNoramalTextWithSelectedText(selectedtext, currentPageNumber));
                                    searchString = selectedtext;
                                }
                            } else {
                                searchText.setText(selectedtext);

                            /*if (selectedtext != null || !selectedtext.equals(""))
                                advancedSearch(selectedtext);*/
                            }
                            searchText.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    keyboard.showSoftInput(searchText, 0);
                                }
                            }, 300);

                        }
                        //fragment.core.convertEncodedToNoramalTextWithSelectedText(pageView.getSelectedText(),currentPageNumber);
                    }

                    //clearSelection();
                } else if (actionId == ID_NOTE)

                {
                    notePopUp();
                } else if (actionId == ID_SPEECH)

                {
                    MuPDFPageView pView = (MuPDFPageView) fragment.mDocView.getDisplayedView();
                    if (pageView != null) {

                        SearchTaskResult.set(null);
                        searchString = "";
                        fragment.mDocView.resetupChildren();
                        PageView view1 = (PageView) fragment.mDocView.getDisplayedView();
                        String selectedtext = pageView.getSelectedText();
                        if (pView.doubleTap)
                            selectedtext = pView.selectedWord;
                        if (selectedtext.length() > 0) {
                            if (!fragment.core.isEnglish()) {
                                if (selectedtext.equals("")) {
                                    // searchText.setText(fragment.core.convertEncodedToNoramalTextWithSelectedText(view1.getSelectedText2(), currentPageNumber));
                                    speakText(fragment.core.convertEncodedToNoramalTextWithSelectedText(view1.getSelectedText2(), currentPageNumber));
                                    //   advancedSearch(fragment.core.convertEncodedToNoramalTextWithSelectedText(view1.getSelectedText2(), currentPageNumber));
                                    //searchString = view1.getSelectedText2();
                                } else {
                                    // searchText.setText(fragment.core.convertEncodedToNoramalTextWithSelectedText(selectedtext, currentPageNumber));
                                    speakText(fragment.core.convertEncodedToNoramalTextWithSelectedText(selectedtext, currentPageNumber));
                                    // advancedSearch(fragment.core.convertEncodedToNoramalTextWithSelectedText(selectedtext, currentPageNumber));
                                    //searchString = selectedtext;
                                }
                            } else {
                                //searchText.setText(selectedtext);
                                speakText(selectedtext);

                            /*if (selectedtext != null || !selectedtext.equals(""))
                                advancedSearch(selectedtext);*/
                            }

                        }
                        //fragment.core.convertEncodedToNoramalTextWithSelectedText(pageView.getSelectedText(),currentPageNumber);
                    }

                }

                // fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);
                quickAction.dismiss();
            }
        });


        //ال�صكر والتقدير ل�صاحب ال�صمو امللكي الأمري خالد الفي�صل بن عبدالعزيز اآل �صعود، اأمري منطقة
        //set listnener for on dismiss event, this listener will be called only if QuickAction dialog was dismissed
        //by clicking the area outside the dialog.
        quickAction.setOnDismissListener(new QuickAction.OnDismissListener()

        {
            @Override
            public void onDismiss() {
       /* if(!noteClikced)
                clearSelection();*/
            }
        });
    }

    public void speakText(String text) {
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    ActionItem unhighlightItem;

    public void prepareHilightMenu() {

        unhighlightItem = new ActionItem(ID_UNHILIGHT, "UnHighlight", getResources().getDrawable(com.artifex.mupdfdemo.R.drawable.ic_clipboard));


        unhighlightItem.setSticky(true);
        quickActionHilight = new QuickAction(this, QuickAction.HORIZONTAL);

        //add action items into QuickAction
        quickActionHilight.addActionItem(unhighlightItem);

        //quickAction.addActionItem(inkItem);


        quickActionHilight.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction source, int pos, int actionId) {

                if (actionId == ID_UNHILIGHT) {
                    MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                    if (pageView != null)
                        pageView.deleteSelectedAnnotation();
                }
                quickActionHilight.dismiss();
            }
        });

        //set listnener for on dismiss event, this listener will be called only if QuickAction dialog was dismissed
        //by clicking the area outside the dialog.
        quickActionHilight.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {

                //clearSelection();
            }
        });
    }


    //This method will search based on page Number in document based on direction......
    public void search(int direction, String text) {

        if (text != null && text.length() > 0) {
            int displayPage = fragment.mDocView.getDisplayedViewIndex();
            SearchTaskResult r = SearchTaskResult.get();
            int searchPage = r != null ? r.pageNumber : -1;
            fragment.mSearchTask.go(text, direction, displayPage, searchPage);
        }
    }

    ArrayList<SearchResult> foundSearhList = new ArrayList<>();


    //This method will search entire document........
    public void searchAll(int direction, String text) {
        SearchResult searchResult = null;
        SearchTaskResult.set(null);
        fragment.mDocView.resetupChildren();
        foundSearhList.clear();
        if (text != null && text.length() > 0) {
            for (int i = 0; i < currentBook.getTotalPages(); i++) {
                int displayPage = 0;//fragment.mDocView.getDisplayedViewIndex();
                SearchTaskResult r = SearchTaskResult.get();
                int searchPage = 0;//r != null ? r.pageNumber : -1;
                searchResult = fragment.mSearchTask.searchResultList(text, direction, i, i);
                if (searchResult.getHitViews() != null && searchResult.getHitViews().length > 0)
                    foundSearhList.add(searchResult);
            }
        }
    }


    //This method will clear selected text.......
    public void clearSelection() {
        MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
        if (pageView != null) {
            pageView.deselectText();
            pageView.cancelDraw();
            fragment.mDocView.showSelectionHandles(false);
        }
        //fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
    }


    protected void onCreate(Bundle savedInstanceState) {
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        //getSupportActionBar().hide();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        //getActionBar().hide();
        // getWindow().addFlags(View.SYSTEM_UI_FLAG_FULLSCREEN);
        setContentView(R.layout.pdf_view);
        deviceWidth = Globals.getDeviceWidth();
        deviceHeight = Globals.getDeviceHeight();


        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    /*Intent intent = getIntent();
    Uri data = intent.getData();

    if (data != null && intent.getType().equals("application/pdf")) {

    }*/

        btnBrush = (Button) findViewById(R.id.btn_brush);
        btnBrush.setOnClickListener(this);
        buttonDone = (Button) findViewById(R.id.btn_done);
        buttonDone.setOnClickListener(this);
        clearDraw = (Button) findViewById(R.id.btn_clear);
        mindMapLayout = (FrameLayout) findViewById(R.id.mindMapLayout);
        stampLayout = (FrameLayout) findViewById(R.id.stampLayout);
        downloadLayout = (FrameLayout) findViewById(R.id.downloadLayout);
        clearDraw.setOnClickListener(this);
        brushSize = (SeekBar) findViewById(R.id.brush_size);
        opacity = (SeekBar) findViewById(R.id.opacity);

        hideButtons();
        mAlertBuilder = new AlertDialog.Builder(this);
        db = new DatabaseHandler(this);
        currentBook = (Book) getIntent().getSerializableExtra("Book");
        gridShelf = (GridShelf) getIntent().getSerializableExtra("Shelf");
        currentPageNumber = 0;
        TextView title_text = (TextView) findViewById(R.id.pdf_text);
        currentBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/";
        File srcDir = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book");
        String[] path = srcDir.list();
        if (path != null && path.length > 0) {
            for (String file : path) {
                String fromPath = srcDir + "/" + file;
                String[] str = file.split("\\.");
                //set Pdf Name as title
                //title_text.setText(str[0]);
                if (file.contains(".pdf")) {
                    pdfName = fromPath;
                    break;
                }
            }
        }


//Brush size listener in deawing Mode
        brushSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                isSeekBarChanged = 1;
                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                if (pageView != null)
                    pageView.saveDraw();

                try {
                    Thread.sleep(70);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                brushThickness = seekBar.getProgress();
                fragment.core.setInk_thickness(brushThickness);


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        //Opacity seekbar listener
        opacity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                if (pageView != null)
                    pageView.saveDraw();

                try {
                    Thread.sleep(70);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                seekBar.setMax(10);

                float opacity = seekBar.getProgress();


                fragment.core.setInk_opacity(opacity / 10);
                fragment.core.setInk_alpha(opacity * 25);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //userCredentials = (UserCredentials) getIntent().getSerializableExtra("UserCredentials");
        gestureDetector = new GestureDetector(pdfActivity.this,new GestureListener());
        add_btn_layout = (RelativeLayout) findViewById(R.id.add_layout);
        add_btn = (Button) findViewById(R.id.add_btn_tab);
        add_btn.setOnClickListener(this);
        rootView = (RelativeLayout) findViewById(R.id.rootView);
        search_layout = (LinearLayout) findViewById(R.id.internet_search_layout);
        icon_layout = (LinearLayout) findViewById(R.id.icon_layout);
        search_img = (Button) findViewById(R.id.search_image);
        search_img.setOnClickListener(this);
        arrow_btn = (Button) findViewById(R.id.arrow_btn);
        arrow_btn.setOnClickListener(this);
        et_search = (EditText) findViewById(R.id.et_search);
        search_btn = (Button) findViewById(R.id.btn_search);
        search_btn.setOnClickListener(this);
        CurrentRecyclerView = (RecyclerView) findViewById(R.id.gridView1);
        subRecyclerView = (RecyclerView) findViewById(R.id.gridView2);
        subRecylerView1 = (RecyclerView) findViewById(R.id.gridView3);
        subTabLayout1 = (RelativeLayout)findViewById(R.id.rl_tabs_layout2);
        subTabsLayout = (RelativeLayout) findViewById(R.id.rl_tabs_layout1);
        dropdown_btn = (CircleButton) findViewById(R.id.dropdown_btn);
        dropdown_btn.setOnClickListener(this);
        enr_layout = (RelativeLayout) findViewById(R.id.enr_layout);
        pdfView = (FrameLayout) findViewById(R.id.pdfView);
        btnBookMark = (Button) findViewById(R.id.btnBookMark);
        btnBookMark.setOnClickListener(this);
        CircleButton btn_library = (CircleButton) findViewById(R.id.btnLibrary);
        btn_library.setOnClickListener(this);
        Button btn_Index = (Button) findViewById(R.id.btnIndex);
        btn_Index.setOnClickListener(this);
        if (pdfName != null)
            fileName = pdfName.substring(pdfName.lastIndexOf("/") + 1, pdfName.length());
        top_bar_layout = (LinearLayout) findViewById(R.id.relativeLayout1);
        back_layout = (RelativeLayout) findViewById(R.id.frameLayout);
        designPageLayout = (RelativeLayout) findViewById(R.id.DesignPageLayout);
        parentEnrRLayout = (RelativeLayout) findViewById(R.id.rl_tabs_layout);
        ll_container = (LinearLayout) findViewById(R.id.ll_container);
        pdf_enrich = (WebView) findViewById(R.id.pdf_enrich);
        hideSearch = (Button) findViewById(R.id.hideSearch);

        pdf_enrich.getSettings().setLoadWithOverviewMode(false);
        pdf_enrich.getSettings().setUseWideViewPort(false);
        pdf_enrich.getSettings().setJavaScriptEnabled(true);
        pdf_enrich.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        pdf_enrich.clearCache(true);
        pdf_enrich.setWebViewClient(new WebViewClient());
        pdf_enrich.getSettings().setAllowContentAccess(true);
        pdf_enrich.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        pdf_enrich.getSettings().setAllowFileAccess(true);
        pdf_enrich.getSettings().setPluginState(WebSettings.PluginState.ON);
        pdf_enrich.getSettings().setDomStorageEnabled(true);
        // btnEnrHome = (Button) findViewById(R.id.btnEnrHome);
        searchButton = (Button) findViewById(R.id.searchButton);

        //addNote = (Button) findViewById(R.id.addNote);
        //noteCount = (Button) findViewById(R.id.noteCount);
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        searchText = (EditText) findViewById(R.id.searchText);
        cancelSearch = (ImageButton) findViewById(R.id.cancelSearch);
        // searchBack = (ImageButton) findViewById(R.id.searchBack);
        // searchForward = (ImageButton) findViewById(R.id.searchForward);
        btnEnrResDownload = (Button) findViewById(R.id.btnEnrResDownload);
        btnStudyCards = (Button) findViewById(R.id.btnStudyCards);

        searchButton.setOnClickListener(this);
        cancelSearch.setOnClickListener(this);
        btnStudyCards.setOnClickListener(this);
        // searchBack.setOnClickListener(this);
        btnStudyCards.setOnClickListener(this);
        hideSearch.setOnClickListener(this);
        internetSearch = new InternetSearch(search_layout,et_search,pdfActivity.this,search_img);

//        addNote.setOnClickListener(this);
        // noteCount.setOnClickListener(this);

        // btnEnrHome.setOnClickListener(this);

        searchText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    SearchTaskResult.set(null);
                    searchString = "";
                    clearSelection();
                    fragment.mDocView.resetupChildren();
                    hideKeyboard();

                    if (!fragment.core.isEnglish()) {

                        if (searchString != null && searchString.length() > 0) {
                            // search(1, searchString);
                            /*searchAll(0, searchString);
                            //showDialog(0, searchString);
                            initializeSearchPopUp(foundSearhList);*/
                            if (new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + "P" + currentBook.getBookID() + "Book/Search.xml").exists())
                                searchTask = new SearchBackgroundForAllPages(pdfActivity.this, searchString).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            else
                                fragment.core.showAlert(pdfActivity.this, "Font Tables Are Not Found");
                        } else {
                            String convertedText = fragment.core.convertTypedText(searchText.getText().toString(), currentPageNumber);
                            /*searchAll(0, convertedText);
                            //showDialog(0, convertedText);
                            initializeSearchPopUp(foundSearhList);*/
                            if (new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + "P" + currentBook.getBookID() + "Book/Search.xml").exists())
                                searchTask = new SearchBackgroundForAllPages(pdfActivity.this, searchText.getText().toString()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            else
                                fragment.core.showAlert(pdfActivity.this, "Font Tables Are Not Found");
                        }
                    } else {
                        //search(0, searchText.getText().toString());
                        searchAll(0, searchText.getText().toString());
                        // showDialog(0, searchText.getText().toString());

                        initializeSearchPopUp(foundSearhList);

                    }

                    return true;
                }
                return false;
            }

        });

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    internetSearch.searchClicked(new InternetSearchCallBack() {
                        @Override
                        public void onClick(String url, String title) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
                            new createAdvanceSearchTab(url, title).execute();
                        }
                    });
                }
                return false;
            }
        });
        annotationLayout = (RelativeLayout) findViewById(R.id.relativeLayout2);
        acceptLayout = (RelativeLayout) findViewById(R.id.checkLayout);
        annotation = (Button) findViewById(R.id.annotation);
        btnViewing = (Button) findViewById(R.id.btn_viewing);


        completeAction = (Button) findViewById(R.id.completeAction);

        annotation.setOnClickListener(this);
        completeAction.setOnClickListener(this);
        btnViewing.setOnClickListener(this);
        btnViewing.setSelected(true);
        parentBkmrkLayout = (RelativeLayout) findViewById(R.id.rl_bkmark_layout);
        ll_bookmark_container = (LinearLayout) findViewById(R.id.ll_bookmark_container);
        progress = (ProgressBar) findViewById(R.id.progressBar1);
        sv = (HorizontalScrollView) findViewById(R.id.btnScrollView);
        btn_bkmark_Sv = (HorizontalScrollView) findViewById(R.id.btn_bkmark_Sv);
        Button currentbkedit = (Button) findViewById(R.id.btn_popup);
        currentbkedit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                displaycurrentBookMarkTitle(v);
            }
        });
        if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
            //pdfView.arabicSelected = false;
        } else {
            // pdfView.arabicSelected = true;
        }
        if (pdfName != null && pdfName.length() > 0)
            openPdfWithFragment(srcDir);

        String screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + 1 + ".png";
       // if (!new File(screenShot).exists()) {
            CopyOpenPdfReader pdf = new CopyOpenPdfReader(pdfActivity.this);
            pdf.new thumbnailImages(pdfName, currentBook).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
      //  }
        new loadEnrichmentTabs(currentPageNumber,CurrentRecyclerView,currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        prepareMenu();
        prepareHilightMenu();


        pdf_seekbar = (SeekBar) findViewById(R.id.sb_pageslider);
        tvSeekBar = (TextView) findViewById(R.id.tv_pagenumber);
        btnEnrichEdit = (Button) findViewById(R.id.btnEnrichEdit);
        btnEnrichEdit.setOnClickListener(this);
        //        btnEnrichEdit.setVisibility(View.INVISIBLE);
        //pdfView.offsetLeftAndRight();
        pdf_seekbar.bringToFront();
        pdf_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, final int i, boolean fromUser) {
                //    pdfView.jumpTo(progress);
                // new takeAndSaveScreenshot(i-1).execute();
                System.out.println();

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int s = seekBar.getProgress();
                fragment.mDocView.setDisplayedViewIndex(seekBar.getProgress());
                /*if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
                    fragment.mDocView.setDisplayedViewIndex(seekBar.getProgress());
                } else {
                    fragment.mDocView.setDisplayedViewIndex(pageCount - seekBar.getProgress() + 1);
                }*/
            }
        });
        pdf_enrich.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        break;
                    case MotionEvent.ACTION_UP:
                        if (enr_layout.isShown()) {
                            enr_layout.setVisibility(View.GONE);
                        } else {
                            enr_layout.setVisibility(View.VISIBLE);
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                }
                return false;
            }
        });
    }

    public void onBackPressed() {
        //To clear highlights
        removeSelection();
        if (searchLayout.getVisibility() != View.VISIBLE) {

            if (fragment.core != null && fragment.core.hasChanges()) {
                DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == AlertDialog.BUTTON_POSITIVE) {
                            fragment.core.save();
                            db.savePdfNotes();
                        } else if (which == AlertDialog.BUTTON_NEGATIVE) {
                            db.deletePdfNotes();
                        }
                        db.executeQuery("update tblCategory set isHidden='false' where CID='6'and isHidden='true'");
                        setResult(RESULT_OK, getIntent().putExtra("Book", currentBook));
                        setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
                        fragment.core.onDestroy();
                        finish();

                    }
                };
                String filename = "";
                if (pdfName != null)
                    filename = pdfName.substring(pdfName.lastIndexOf("/") + 1, pdfName.length());
                AlertDialog alert = mAlertBuilder.create();
                alert.setTitle(filename);
                alert.setMessage(getString(com.artifex.mupdfdemo.R.string.document_has_changes_save_them_));
                alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(com.artifex.mupdfdemo.R.string.yes), listener);
                alert.setButton(AlertDialog.BUTTON_NEGATIVE, getString(com.artifex.mupdfdemo.R.string.no), listener);
                alert.show();
            } else {
                db.executeQuery("update tblCategory set isHidden='false' where CID='6'and isHidden='true'");
                setResult(RESULT_OK, getIntent().putExtra("Book", currentBook));
                setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
                if (fragment.core != null)
                    fragment.core.onDestroy();
                finish();
            }
        } else {
            searchLayout.setVisibility(View.GONE);
            top_bar_layout.setVisibility(View.VISIBLE);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.annotation_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setSeekBarTextValues(int pageNumber) {
      //  MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
        /*if (pageView == null) {

            if (fragment.mDocView != null)
                fragment.mDocView.showSelectionHandles(false);
        }*/
        if (pageNumber == 1) {
            tvSeekBar.setText(R.string.cover_page);
            //screenShotTime=1000;
        } else if (pageNumber == pageCount) {
            tvSeekBar.setText(R.string.end_page);
            //screenShotTime=500;
        } else {
            tvSeekBar.setText("" + (pageNumber - 1) + " of " + (pageCount - 2) + "");
            //screenShotTime=500;
        }
        if (textToSpeech == null) {
            Locale[] locales = Locale.getAvailableLocales();

            textToSpeech = new TextToSpeech(pdfActivity.this, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    textToSpeech.setLanguage(Locale.ENGLISH);

// at this point the localeList object will c
                }
            });
            if (textToSpeech != null && fragment.core.mDirection == 0)
                textToSpeech.setLanguage(Locale.ENGLISH);
            List<Locale> localeList = new ArrayList<Locale>();
            Locale arabicLical;
            for (Locale locale : locales) {
                int res = textToSpeech.isLanguageAvailable(locale);
                if (res == TextToSpeech.LANG_COUNTRY_AVAILABLE) {
                    localeList.add(locale);
                }
            }
        }


    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnBookMark) {
            if (!btnBookMark.isSelected()) {
                btnBookMark.setSelected(true);
                addBookMark(currentPageNumber, currentEnrichmentTabId);
            } else {
                btnBookMark.setSelected(false);
                removeBookMark(currentPageNumber, currentEnrichmentTabId);
            }
        } else if (v.getId() == R.id.btnEnrichEdit) {
            if (currentPageNumber == 1 || currentPageNumber == pageCount) {
                UserFunctions.DisplayAlertDialog(this, R.string.cover_page_cannot_enriched, R.string.cover_page);
            } else {
                Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
                bookViewIntent.putExtra("Book", currentBook);
                bookViewIntent.putExtra("currentPageNo", currentPageNumber);
                bookViewIntent.putExtra("enrichmentTabId", currentEnrichmentTabId);
                bookViewIntent.putExtra("Shelf", gridShelf);
                startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
            }
        } /*else if (v.getId() == R.id.btnEnrHome) {
            pdf_enrich.setVisibility(View.INVISIBLE);
            pdfView.setVisibility(View.VISIBLE);
            clickedBtnHomeEnrichment((Button) v, ll_container, currentPageNumber);
        } */ else if (v.getId() == R.id.btnLibrary) {

            if (fragment.mDocView != null && fragment.mDocView.getMode() != null && fragment.mDocView.getMode() == MuPDFReaderView.Mode.Drawing) {
                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                if (pageView != null) {
                    pageView.deselectText();
                    pageView.cancelDraw();
                }
                fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);
                completeAction.setSelected(false);
                searchButton.setVisibility(View.VISIBLE);
                drawLayout.setVisibility(View.GONE);
                top_bar_layout.setVisibility(View.VISIBLE);
            } else {
                onBackPressed();
            }
        } else if (v.getId() == R.id.btnIndex) {
            Intent i = new Intent(getApplicationContext(), Index_new.class);
            i.putExtra("Book", currentBook);
            startActivityForResult(i, indexActivityRequestCode);
        } else if (v.getId() == R.id.annotation) {
            if (fragment.mDocView.getMode() == MuPDFReaderView.Mode.Drawing) {
                AlertDialog.Builder builder = new AlertDialog.Builder(pdfActivity.this);
                builder.setTitle("Drawing");
                builder.setMessage("Do you want to save ?");
                AlertDialog alert = builder.create();
                alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(com.artifex.mupdfdemo.R.string.save),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                                if (pageView != null)
                                    pageView.saveDraw();
                                completeAction.setSelected(false);
                                annotation.setSelected(true);
                                btnViewing.setSelected(false);
                                fragment.mDocView.setMode(MuPDFReaderView.Mode.Selecting);
                            }
                        });
                alert.setButton(AlertDialog.BUTTON_NEGATIVE, getString(com.artifex.mupdfdemo.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();

                                if (pageView != null) {
                                    pageView.deselectText();
                                    pageView.cancelDraw();
                                }
                                completeAction.setSelected(false);
                                btnViewing.setSelected(false);
                                annotation.setSelected(true);
                                fragment.mDocView.setMode(MuPDFReaderView.Mode.Selecting);
                            }
                        });
                alert.show();
            } else if (fragment.mDocView.getMode() == MuPDFReaderView.Mode.Viewing) {
                annotation.setSelected(true);
                completeAction.setSelected(false);
                btnViewing.setSelected(false);
                fragment.mDocView.setMode(MuPDFReaderView.Mode.Selecting);
            } else {
                fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);
                completeAction.setSelected(false);
                btnViewing.setSelected(true);
                annotation.setSelected(false);
            }
        } else if (v.getId() == R.id.btn_done) {
            if (fragment.mDocView.getMode() == MuPDFReaderView.Mode.Drawing) {
                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                if (pageView != null)
                    pageView.saveDraw();


                btnViewing.setSelected(true);
                drawLayout.setVisibility(View.GONE);
                annotation.setSelected(false);
                completeAction.setSelected(false);
                fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);
                final MuPDFPageView pageView2 = (MuPDFPageView) fragment.mDocView.getDisplayedView();
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // this code will be executed after 2 seconds
                        pageView2.loadAnnotations();
                    }
                }, 1000);


            } else {
                fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);
                completeAction.setSelected(false);
                btnViewing.setSelected(true);
                annotation.setSelected(false);
            }


        } else if (v.getId() == R.id.btn_viewing) {
            if (fragment.mDocView.getMode() == MuPDFReaderView.Mode.Drawing) {
                AlertDialog.Builder builder = new AlertDialog.Builder(pdfActivity.this);
                builder.setTitle("Drawing");
                builder.setMessage("Do you want to save ?");
                AlertDialog alert = builder.create();
                alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(com.artifex.mupdfdemo.R.string.save),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                                if (pageView != null)
                                    pageView.saveDraw();
                                completeAction.setSelected(false);
                                annotation.setSelected(false);
                                btnViewing.setSelected(true);
                                fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);
                            }
                        });
                alert.setButton(AlertDialog.BUTTON_NEGATIVE, getString(com.artifex.mupdfdemo.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();

                                if (pageView != null) {
                                    pageView.deselectText();
                                    fragment.mDocView.showSelectionHandles(false);
                                    pageView.cancelDraw();
                                }
                                completeAction.setSelected(false);
                                btnViewing.setSelected(true);
                                annotation.setSelected(false);
                                fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);
                            }
                        });
                alert.show();
            } else {
                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();

                if (pageView != null) {
                    pageView.deselectText();
                    fragment.mDocView.showSelectionHandles(false);
                }
                completeAction.setSelected(false);
                annotation.setSelected(false);
                btnViewing.setSelected(true);

                fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);

            }
        } else if (v.getId() == R.id.completeAction) {
            boolean success = false;

            drawLayout = (RelativeLayout) findViewById(R.id.draw_view);

            if (fragment.mDocView.getMode() == MuPDFReaderView.Mode.Viewing) {
                completeAction.setSelected(true);
                btnViewing.setSelected(false);
                annotation.setSelected(false);
                drawLayout.setVisibility(View.VISIBLE);
                fragment.mDocView.setMode(MuPDFReaderView.Mode.Drawing);
            } else if (fragment.mDocView.getMode() == MuPDFReaderView.Mode.Selecting) {
                completeAction.setSelected(true);
                btnViewing.setSelected(false);
                annotation.setSelected(false);
                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                if (pageView != null) {
                    pageView.deselectText();
                    fragment.mDocView.showSelectionHandles(false);
                    pageView.cancelDraw();
                }
                drawLayout.setVisibility(View.VISIBLE);
                btnViewing.setVisibility(View.GONE);
                annotation.setVisibility(View.GONE);
                fragment.mDocView.setMode(MuPDFReaderView.Mode.Drawing);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(pdfActivity.this);
                builder.setTitle("Drawing");
                builder.setMessage("Do you want to save ?");
                AlertDialog alert = builder.create();
                alert.setButton(AlertDialog.BUTTON_POSITIVE, getString(com.artifex.mupdfdemo.R.string.save),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
                                if (pageView != null)
                                    pageView.saveDraw();
                                completeAction.setSelected(false);
                                btnViewing.setSelected(true);
                                annotation.setSelected(false);
                                fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);
                            }
                        });
                alert.setButton(AlertDialog.BUTTON_NEGATIVE, getString(com.artifex.mupdfdemo.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();

                                if (pageView != null) {
                                    pageView.deselectText();
                                    fragment.mDocView.showSelectionHandles(false);
                                    pageView.cancelDraw();
                                }
                                completeAction.setSelected(false);
                                btnViewing.setSelected(true);
                                annotation.setSelected(false);
                                fragment.mDocView.setMode(MuPDFReaderView.Mode.Viewing);
                            }
                        });
                alert.show();

            }



           /* boolean success = false;
            MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
            switch (mAcceptMode) {
                case CopyText:
                    if (pageView != null)
                        success = pageView.copySelection();
                    mTopBarMode = TopBarMode.More;
                    break;
                case Highlight:
                    if (pageView != null)
                        success = pageView.markupSelection(Annotation.Type.HIGHLIGHT);
                    mTopBarMode = TopBarMode.Annot;
                    break;
                case Underline:
                    if (pageView != null)
                        success = pageView.markupSelection(Annotation.Type.UNDERLINE);
                    mTopBarMode = TopBarMode.Annot;
                    break;

                case StrikeOut:
                    if (pageView != null)
                        success = pageView.markupSelection(Annotation.Type.STRIKEOUT);
                    break;
                case Ink:
                    if (pageView != null)
                        success = pageView.saveDraw();
                    searchButton.setVisibility(View.VISIBLE);
                    mTopBarMode = TopBarMode.Annot;
                    break;
            }
            fragment.mDocView.setMode(MuPDFReaderView.Mode.Selecting);
           *//* annotationLayout.setVisibility(View.INVISIBLE);
            acceptLayout.setVisibility(View.INVISIBLE);
            top_bar_layout.setVisibility(View.VISIBLE);*//*
            completeAction.setVisibility(View.GONE);*/
        } else if (v.getId() == R.id.btn_brush) {
          /*  MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
            if (pageView != null)
                pageView.saveDraw();

            showBrushSettingsDialog();
            if (isSeekBarChanged == 1)
                brushSize.setProgress(brushThickness);
            else
                brushSize.setProgress(6);*/

            initializeColorPicker();
        } else if (v.getId() == R.id.btn_clear) {
            MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
            if (pageView != null)
                pageView.cancelDraw();
        } else if (v.getId() == R.id.searchButton) {

            back_layout.setVisibility(View.VISIBLE);
            searchLayout.setVisibility(View.VISIBLE);
            back_layout.bringToFront();


        } else if (v.getId() == R.id.cancelSearch) {
            top_bar_layout.setVisibility(View.VISIBLE);
            back_layout.setVisibility(View.VISIBLE);
            searchLayout.setVisibility(View.INVISIBLE);
            searchText.setText("");
            SearchTaskResult.set(null);
            searchString = "";
            clearSelection();
            fragment.mDocView.resetupChildren();
            hideKeyboard();
        } else if (v.getId() == R.id.searchForward) {
            if (fragment.core.docDetails != null && fragment.core.creatorList.contains(fragment.core.docDetails.getDocumentInfo().getCreator())) {
                if (searchString != null && searchString.length() > 0)
                    search(1, searchString);
                else
                    fragment.core.convertTypedText(searchText.getText().toString(), currentPageNumber);
            } else {
                search(1, searchText.getText().toString());
            }

        } else if (v.getId() == R.id.searchBack) {
            if (fragment.core.docDetails != null && fragment.core.creatorList.contains(fragment.core.docDetails.getDocumentInfo().getCreator())) {
                if (searchString != null && searchString.length() > 0)
                    search(-1, searchString);
                else
                    fragment.core.convertTypedText(searchText.getText().toString(), currentPageNumber);
            } else
                search(-1, searchText.getText().toString());
        } else if (v.getId() == R.id.addNote) {
            notePopUp();
            Toast.makeText(getApplicationContext(), "C..P.." + fragment.core.getPageNumber(), Toast.LENGTH_LONG).show();
        } else if (v.getId() == R.id.noteCount) {
            displayNotesList();

        } else if (v.getId() == R.id.btnStudyCards) {
            openFlashNotes();
        } else if (v.getId() == R.id.hideSearch) {

            searchLayout.setVisibility(View.GONE);
            top_bar_layout.setVisibility(View.VISIBLE);
            removeSelection();
        }else if (v.getId() == R.id.add_btn_tab){
           // treeViewStructureList(v,true,0);
            viewTreeList(v,true,0);
        }else if (v.getId() == R.id.btn_search){
            internetSearch.searchClicked(new InternetSearchCallBack() {
                @Override
                public void onClick(String url, String title) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
                    new createAdvanceSearchTab(url, title).execute();
                }
            });
        }else if (v.getId() == R.id.search_image){
            internetSearch.showSearchPopUp(v);
        }else if (v.getId() == R.id.arrow_btn){
            internetSearch.showSearchPopUp(v);
        }else if (v.getId() == R.id.dropdown_btn){
            if (SubtabsAdapter.level3!=0) {
                if (currentBtnEnrichment.getEnrichmentId() == SubtabsAdapter.level3) {
                    isLoadEnrInpopview = true;
                    viewTreeList(v, false, currentBtnEnrichment.getEnrichmentId());
                }
            }
        }
    }

    /*
    * Checking for bookmarking page
    *
    */

    public String detectLanguage(String selectedText) {

        String alpha = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
        String[] alphaArray = alpha.split(",");
        for (int i = 0; i < alphaArray.length; i++) {
            String current = alphaArray[i];
            if (selectedText.contains(current)) {
                return "en";
            }
        }
        return "ar";
    }
    public void openFlashNotes() {

    }

    public void checkForBookMark(int pageNo, int enrTabId) {
        int count = db.getBookMarkCount(currentBook.get_bStoreID(), pageNo, enrTabId);
        if (count > 0) {
            btnBookMark.setSelected(true);
        } else {
            btnBookMark.setSelected(false);
        }
    }

    /*
     * Removing page BookMark
     */
    public void removeBookMark(int pageNo, int enrTabId) {
        String query = "delete from tblBookMark where BName='" + currentBook.get_bStoreID() + "' and PageNo='" + pageNo + "' and TabNo='" + enrTabId + "'";
        db.executeQuery(query);
    }

    /*
     * Adding Page BookMark
     */
    private void addBookMark(int pageNo, int enrTabId) {
        String query = "insert into tblBookMark(BName,PageNo,TagID,TabNo) values('" + currentBook.get_bStoreID() + "','" + pageNo + "','0','" + enrTabId + "')";
        db.executeQuery(query);
    }

    /*
    * Generating Thumbnail images for current page
    *
    */
    public void screenshot(Bitmap bitmap, int pageNo, int pageCount, int bookId) {
        String coverandendpage = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/";
        //String absPath=Environment.getExternalStorageDirectory().getAbsolutePath();
        // String coverandendpage=absPath+"/Download/";
        if (pageNo == 0) {
            String card_path = coverandendpage + "card.png";
            //if (!new File(card_path).exists()) {
            Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(200, this), Globals.getDeviceIndependentPixels(300, this), true);
            UserFunctions.saveBitmapImage(cardBitmap, card_path);
            // }
            int thumbnail_pageNo = pageNo + 1;
            String pageThumbPath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_" + thumbnail_pageNo + ".png";
            // String pageThumbPath= coverandendpage+"thumbnail_"+pageNo+".png";
            if (!new File(pageThumbPath).exists()) {
                Bitmap pageThumbBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(130, this), Globals.getDeviceIndependentPixels(170, this), true);
                UserFunctions.saveBitmapImage(pageThumbBitmap, pageThumbPath);
            }
            String front_thumb_path = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/frontThumb.png";
            Bitmap frontThumbBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(130, this), Globals.getDeviceIndependentPixels(170, this), true);
            UserFunctions.saveBitmapImage(frontThumbBitmap, front_thumb_path);

            Bitmap front_p = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceWidth(), Globals.getDeviceHeight(), true);
            UserFunctions.saveBitmapImage(front_p, Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/front_P.png");

        } else if (pageNo == pageCount - 1) {
            String back_port_path = coverandendpage + "back_P.png";
            if (!new File(back_port_path).exists()) {
                UserFunctions.saveBitmapImage(bitmap, back_port_path);
            }

            String pageThumbPath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_end.png";
            // String pageThumbPath= coverandendpage+"thumbnail_"+pageNo+".png";
            if (!new File(pageThumbPath).exists()) {
                Bitmap pageThumbBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(130, this), Globals.getDeviceIndependentPixels(170, this), true);
                UserFunctions.saveBitmapImage(pageThumbBitmap, pageThumbPath);
            }
        } else {
            int thumbnail_pageNo = pageNo + 1;
            String pageThumbPath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_" + thumbnail_pageNo + ".png";
            // String pageThumbPath= coverandendpage+"thumbnail_"+pageNo+".png";
            if (!new File(pageThumbPath).exists()) {
                Bitmap pageThumbBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(130, this), Globals.getDeviceIndependentPixels(170, this), true);
                UserFunctions.saveBitmapImage(pageThumbBitmap, pageThumbPath);

                Bitmap front_p = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceWidth(), Globals.getDeviceHeight(), true);
                UserFunctions.saveBitmapImage(front_p, Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/" + pageNo + ".png");
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == bookViewActivityrequestCode) {
            if (data != null) {
                if (data != null) {
                    Book book = (Book) data.getSerializableExtra("Book");
                    if (book.get_bStoreID().contains("P")) {
                        currentBook = book;
                        currentEnrichmentTabId = data.getIntExtra("enrichmentTabId",0);
                        String num = data.getStringExtra("currentPageNo");
                        int num1 = Integer.parseInt(num);
                        currentPageNumber = num1;
                        // pageCount = currentBook.getTotalPages();
                        new loadEnrichmentTabs(currentPageNumber,CurrentRecyclerView,currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }

            }

        } else if (requestCode == indexActivityRequestCode && data != null) {
            String[] pgval = data.getExtras().getStringArray("id");
            if(pgval == null){
                int value= data.getExtras().getInt("id");
                // if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
                fragment.mDocView.setDisplayedViewIndex(pageCount-value);
//                }else{
//                    int page= pageCount-value;
//                   fragment.mDocView.setDisplayedViewIndex(page);
//               }
            } else {
                int pageNumber = Integer.parseInt(pgval[0]);
                int enrTabId = Integer.parseInt(pgval[1]);
                currentEnrichmentTabId = enrTabId;
                currentPageNumber = pageNumber;
                //  if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
                //  fragment.mDocView.setDisplayedViewIndex(pageNumber);
//                }else{
//                    int page= pageCount-pageNumber-1;
//                    fragment.mDocView.setDisplayedViewIndex(page);
//                }
                new loadEnrichmentTabs(currentPageNumber,CurrentRecyclerView,currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    /*
     * Checking for enrichments
     */
    public void checkForEnrichments(int pageNo) {
        ll_container.removeViews(1, ll_container.getChildCount() - 1);
        ArrayList<EnrichmentButton> enrichmentTabList = db.getEnrichmentTabListForBookReadAct(currentBook.getBookID(), pageNo, pdfActivity.this, ll_container);
        if (enrichmentTabList.size() > 0) {
            parentEnrRLayout.setVisibility(View.VISIBLE);
        } else {
            parentEnrRLayout.setVisibility(View.GONE);
        }
    }

    /*
     * Enrichments button on Click
     *
     */
    public void clickedEnrichments(Enrichments enrButton) {
        if (designPageLayout.getChildCount() == 5) {
            designPageLayout.removeViewAt(4);
        }
        pdfView.setVisibility(View.INVISIBLE);
        pdf_enrich.setVisibility(View.VISIBLE);
        currentEnrichmentTabId = enrButton.getEnrichmentId();
        checkForBookMark(currentPageNumber, currentEnrichmentTabId);
        currentBtnEnrichment = enrButton;
        String filepath = null;
        pdf_enrich.getSettings().setLoadWithOverviewMode(true);
        pdf_enrich.getSettings().setUseWideViewPort(true);

        pdf_enrich.getSettings().setJavaScriptEnabled(true);
        pdf_enrich.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        pdf_enrich.clearCache(true);
        pdf_enrich.setWebViewClient(new WebViewClient());
        pdf_enrich.getSettings().setAllowContentAccess(true);
        pdf_enrich.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        pdf_enrich.getSettings().setAllowFileAccess(true);
        pdf_enrich.getSettings().setPluginState(WebSettings.PluginState.ON);
        pdf_enrich.getSettings().setDomStorageEnabled(true);
        // File  file = new File(currentBookPath+currentBtnEnrichment.getEnrichmentPath());
        if (currentBtnEnrichment != null && currentBtnEnrichment.getEnrichmentType().equals(Globals.downloadedEnrichmentType)) {
            if (Globals.ISGDRIVEMODE()) {
                //if (pageNumber == currentBtnEnrichment.getEnrichmentPageNo()) {
                File file = new File(currentBtnEnrichment.getEnrichmentPath());
                filepath = "file:///" + currentBtnEnrichment.getEnrichmentPath();
                if (!file.exists()) {
                    String modFilePath = currentBtnEnrichment.getEnrichmentPath().replace("/index.htm", "");
                    modFilePath = modFilePath.substring(0, modFilePath.lastIndexOf("/"));
                    modFilePath = modFilePath + "/index.htm";
                    file = new File(modFilePath);
                    filepath = "file:///" + modFilePath;
                }
            }
        } else {
            filepath = "file:///" + currentBookPath + currentBtnEnrichment.getEnrichmentPageNo() + "-" + currentBtnEnrichment.getEnrichmentId() + ".htm";
        }

        pdf_enrich.loadUrl(filepath);

    }


    /*
     * Selecting Enrichments button
     *
     */
    public void makeEnrichedBtnSlected(Button enrButton, LinearLayout enrTabsLayout) {
        for (int i = 0; i < enrTabsLayout.getChildCount(); i++) {
            View view = enrTabsLayout.getChildAt(i);
            if (view instanceof RelativeLayout) {
                RelativeLayout rlView = (RelativeLayout) view;
                View btnView = rlView.getChildAt(0);
                if (btnView.equals(enrButton)) {
                    btnView.setSelected(true);
                } else {
                    btnView.setSelected(false);
                }
            } else {
                if (view.equals(enrButton)) {
                    view.setSelected(true);
                } else {
                    view.setSelected(false);
                }
            }
        }
    }

    /*
     * Selecting Home Button
     */
    private void clickedBtnHomeEnrichment(Button view, LinearLayout ll_container, int viewPagerPageNo) {
        pdfView.setVisibility(View.VISIBLE);
        pdf_enrich.setVisibility(View.INVISIBLE);
        currentEnrichmentTabId = 0;
        currentBtnEnrichment = null;
        makeEnrichedBtnSlected(view, ll_container);
        checkForBookMark(viewPagerPageNo, currentEnrichmentTabId);

    }

    /*
     * Picking button from Reletive Layout in AdvSearch Enrichments
     *
     */
    /*
     * Selecting AdvSearch Enrichments & mindmap
     *
     */
    public void clickedAdvanceSearchtab(final Enrichments enrButton) {
        if (designPageLayout.getChildCount()==5){
            designPageLayout.removeViewAt(4);
        }
        parentEnrRLayout.setVisibility(View.VISIBLE);
        pdf_enrich.setVisibility(View.VISIBLE);
        pdfView.setVisibility(View.GONE);
        pdf_enrich.clearView();
        currentEnrichmentTabId = enrButton.getEnrichmentId();
        currentBtnEnrichment = enrButton;
        checkForBookMark(currentPageNumber, enrButton.getEnrichmentId());
        // checkForPaintAndApply(currentCanvasImgView, currentPageNumber, enrButton.getEnrichmentId());
        if (enrButton.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
            pdf_enrich.getSettings().setLoadWithOverviewMode(false);
            pdf_enrich.getSettings().setUseWideViewPort(false);
        } else {
            pdf_enrich.getSettings().setLoadWithOverviewMode(true);
            pdf_enrich.getSettings().setUseWideViewPort(true);
        }
        pdf_enrich.getSettings().setJavaScriptEnabled(true);
        pdf_enrich.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        pdf_enrich.clearCache(true);
        pdf_enrich.getSettings().setAllowContentAccess(true);
        pdf_enrich.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        pdf_enrich.getSettings().setAllowFileAccess(true);
        pdf_enrich.getSettings().setPluginState(WebSettings.PluginState.ON);
        pdf_enrich.getSettings().setDomStorageEnabled(true);
        if (enrButton.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
            String mindMapPath = Globals.TARGET_BASE_MINDMAP_PATH + enrButton.getEnrichmentPath() + ".xml";
            if (!new File(mindMapPath).exists()) {
                UserFunctions.DisplayAlertDialog(pdfActivity.this, R.string.mindmap_deleted_msg, R.string.mindmap_deleted);
            } else {
                pdf_enrich.setVisibility(View.GONE);
                progress.setVisibility(View.GONE);
                if (designPageLayout.getChildCount()==5){
                    designPageLayout.removeViewAt(4);
                }
                loadMindMap(enrButton.getEnrichmentPath(),designPageLayout,pdfActivity.this);
            }
        } else {
            pdf_enrich.loadUrl(enrButton.getEnrichmentPath());
        }
        progress.setVisibility(View.VISIBLE);

        if (enrButton.getEnrichmentType().equals("Search")) {
            pdf_enrich.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:{
                            break;
                        }
                        case MotionEvent.ACTION_UP:{
                            pdf_enrich.setWebViewClient(new CustomWebViewClient());
                            if (enr_layout.getVisibility() == View.VISIBLE) {
                                enr_layout.setVisibility(View.GONE);
                            } else {
                                enr_layout.setVisibility(View.VISIBLE);
                            }
                            break;
                        }
                        default:
                            break;
                    }
                    return false;
                }
            });
        }

        pdf_enrich.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                progress.setVisibility(View.INVISIBLE);
            }


        });
    }

    public void loadMindMap(final String title,final RelativeLayout layout,final Context context) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final RelativeLayout canvasView = new RelativeLayout(context);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                params.addRule(RelativeLayout.BELOW,enr_layout.getId());
                canvasView.setLayoutParams(params);
                final TwoDScrollView twoDScroll = new TwoDScrollView(context, canvasView);
                twoDScroll.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                twoDScroll.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                twoDScroll.setEnabled(false);
                canvasView.addView(twoDScroll);
                ViewGroup drawingView = new RelativeLayout(context);
                drawingView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                twoDScroll.addView(drawingView);
                //setSelectedView(viewEdit);
                drawingView.getLayoutParams().width = AppDict.DEFAULT_OFFSET;
                drawingView.getLayoutParams().height = AppDict.DEFAULT_OFFSET;
                String url = Globals.TARGET_BASE_MINDMAP_PATH + title + ".xml";
                LoadMindMapContent mindMapContent = new LoadMindMapContent(twoDScroll, drawingView, context);
                mindMapContent.loadXmlData(url, false);
                Rect rectf = new Rect();
                drawingView.getLocalVisibleRect(rectf);
                twoDScroll.calculateBounds();
                twoDScroll.scroll((int) (AppDict.DEFAULT_OFFSET - canvasView.getWidth() + 120) / 2,
                        (int) (AppDict.DEFAULT_OFFSET - canvasView.getHeight()) / 2);
                layout.addView(canvasView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            }
        });
    }


    /*
     * Deleting AdvSearch Enrichments
     */
    @Override
    public void setSearchString(String search) {
        int displayPage = fragment.mDocView.getDisplayedViewIndex();
        // search(0, search);
        /*if (search != null && search.length() > 0) {
            int displayPage = fragment.mDocView.getDisplayedViewIndex();
            SearchTaskResult r = SearchTaskResult.get();
            int searchPage = r != null ? r.pageNumber : -1;
            fragment.mSearchTask.go(search, 0, displayPage, displayPage);
        }*/
    }


    public class CustomWebViewClient extends WebViewClient {
        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            Log.d("WEB_ERROR", "error code:" + errorCode + ":" + description);
            String url = "";
            //createAdvancedSearchTab(advtitle, url).execute();
        }

        @Override
        // Method where you(get) load the 'URL' which you have clicked
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (currentBtnEnrichment.getEnrichmentType().equals(Globals.advancedSearchType)) {
                try {
                    if (url != null) {
                        //currentProgressbar.setVisibility(View.VISIBLE);
                        String path = url;
                        String[] adv_bkmrktype = path.split("/");
                        String url1 = adv_bkmrktype[2].replace(".", "/");
                        String[] type = url1.split("/");
                        for (int i = 0; i < type.length; i++) {
                            if (i == type.length - 2) {
                                // createAdvancedSearchTab(url,type[i]);
                                new createAdvanceSearchTab(url, type[i]).execute();
                                break;
                            }
                        }
                        return true;
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
            return true;
        }
    }

    /*
     * Selecting AdvSearch Enrichments or mindmap after deleting adsearch button
     */

    /*
    * Creating AdvSearch Enrichments Tab from bkmrk button
    */
    public void createAdvancedSearchTab(String url, String title) {
        title = title.replace("'", "''");
        db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path)values('" + currentBook.getBookID() + "','" + currentPageNumber + "','" + title + "','Search', '0','" + url + "')");
        int objUniqueId = db.getMaxUniqueRowID("enrichments");
        title = title.replace("''", "'");
        RelativeLayout layout = new RelativeLayout(this);
        layout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
//        EnrichmentButton enrichmentButton = new EnrichmentButton(this, Globals.advancedSearchType, objUniqueId, currentBook.getBookID(), currentPageNumber, title, ll_container.getChildCount() + 1, 0, url, ll_container);
//        clickedAdvanceSearchtab(enrichmentButton);
    }

    public class createAdvanceSearchTab extends AsyncTask<Void, Void, Void> {
        String Url;
        String title;

        public createAdvanceSearchTab(String url, String s) {
            Url = url;
            title = s;
        }

        @Override
        protected void onPreExecute() {
            title = title.replace("'", "''");
            int  objSequentialId = db.getMaximumSequentialId(currentPageNumber, currentBook.getBookID()) + 1;
            db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,SequentialID,categoryID)values('" + currentBook.getBookID() + "','" + currentPageNumber + "','" + title + "','Search', '0','" + Url + "','"+objSequentialId+"','"+categoryId+"')");
            int objUniqueId = db.getMaxUniqueRowID("enrichments");
            title = title.replace("''", "'");
            currentEnrichmentTabId = objUniqueId;
            Enrichments enrichments = db.getEnrichment(objUniqueId,objSequentialId,pdfActivity.this);
            currentBtnEnrichment = enrichments;
            if (clickableRecyclerView == null){
                SubtabsAdapter.level1 = objUniqueId;
                new loadEnrichmentTabs(currentPageNumber, CurrentRecyclerView, currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }else {
                if (clickableRecyclerView.getId() == R.id.gridView1) {
                    new loadEnrichmentTabs(currentPageNumber, CurrentRecyclerView, currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    if (currentBtnEnrichment.getCategoryID()!=0) {
                        new loadSubEnrichmentTabs(currentPageNumber, subRecyclerView, 0, currentBtnEnrichment.getCategoryID(), clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    }
                } else if (clickableRecyclerView.getId() == R.id.gridView2) {
                    new loadSubEnrichmentTabs(currentPageNumber, subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    new loadSubEnrichmentTabs(currentPageNumber, subRecylerView1, 0, currentBtnEnrichment.getCategoryID(), clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                } else {
                    SubtabsAdapter.level3 = objUniqueId;
                    new loadSubEnrichmentTabs(currentPageNumber, subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                }
            }
            if (enrichments!=null){
                clickedAdvanceSearchtab(enrichments);
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // if (parentEnrRLayout.getWidth()>ll_container.getWidth()) {
//            int enrTabsWidth1 = ll_container.getWidth();
//            sv.smoothScrollTo(enrTabsWidth1, 0);
            // }
        }
    }

    /*
      * Adding  AdvSearch Enrichments as bkmark
     */
   /* public void showAdvsearchEnrichDialog(final EnrichmentButton enrichmentButton) {
        final Dialog enrichDialog = new Dialog(this);
        enrichDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
        enrichDialog.setTitle(R.string.advsearch_bookmark);
        enrichDialog.setContentView(this.getLayoutInflater().inflate(R.layout.advsearch_bkmark_dialog, null));
        enrichDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, this), LinearLayout.LayoutParams.WRAP_CONTENT);
        final EditText editText = (EditText) enrichDialog.findViewById(R.id.enr_editText);
        editText.setText(enrichmentButton.getEnrichmentTitle());
        Button btnSave = (Button) enrichDialog.findViewById(R.id.enr_save);
        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String enrTitle = editText.getText().toString();
                enrTitle = enrTitle.replace("'", "''");
                db.executeQuery("insert into BookmarkedSearchTabs (Title, SearchTabId, PageNo,IsForAllPages,Path,Bid) values('" + enrTitle + "', '" + enrichmentButton.getEnrichmentId() + "', '" + enrichmentButton.getEnrichmentPageNo() + "','False','" + enrichmentButton.getEnrichmentPath() + "','" + currentBook.getBookID() + "')");
             //   addingBookMarkEnrichmnets(enrichmentButton, enrTitle);
                enrichDialog.setOnDismissListener(pdfActivity.this);
                enrichDialog.dismiss();

            }
        });

        enrichDialog.show();
    }*/

    /*
     * Creating  BookMark Enrichments
     */
  /*  public void addingBookMarkEnrichmnets(EnrichmentButton enrButton, String enrTitle) {
        //ArrayList<BookMarkEnrichments> currentbmrkEnrichmentList = bookMarkedAdvTabList.get(pageNumber);
        parentBkmrkLayout.setVisibility(View.VISIBLE);
        int objUniqueId = db.getMaxUniqueRowID("BookmarkedSearchTabs");
        BookMarkEnrichments bookmarkenrichment = new BookMarkEnrichments(pdfActivity.this);
        bookmarkenrichment.setEnrichmentId(objUniqueId);
        bookmarkenrichment.setEnrichmentPath(enrButton.getEnrichmentPath());
        bookmarkenrichment.setEnrichmentTitle(enrTitle);
        bookmarkenrichment.setEnrichmentPageNo(enrButton.getEnrichmentPageNo());
        bookmarkenrichment.setEnrichmentBid(currentBook.getBookID());
        bookmarkenrichment.setSearchTabId(enrButton.getEnrichmentId());

        bookmarkenrichment.createEnrichmentTabs(ll_bookmark_container);

    }*/

    /*
    * Adding  image to  AdvSearch Enrichments
    *
    */
    public class lvimage extends AsyncTask<String, Void, Bitmap> {
        ImageView imagepath;
        ImageView img;
        Button btn;
        int id;

        public lvimage(Button btnEnrichmentTab, int i) {
            // TODO Auto-generated constructor stub
            this.btn = btnEnrichmentTab;
            id = i;
        }

        protected Bitmap doInBackground(String... urls) {
            String URLpath = urls[0];
            Bitmap bmp = null;
            try {
                URL url = new URL(URLpath);
                URLConnection ucon = url.openConnection();
                ucon.connect();
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                //System.out.println(bmp.getWidth());;
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return bmp;

        }

        protected void onPostExecute(Bitmap result) {
            if (result == null) {

            } else {
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, result.getWidth(), getResources().getDisplayMetrics());
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, result.getHeight(), getResources().getDisplayMetrics());

                Bitmap b = Bitmap.createScaledBitmap(result, width, height, false);
                Drawable f = new BitmapDrawable(getResources(), b);
                String Path = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "bkmarkimages/";
                //String imgpath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BookView.bookName+"/";
                UserFunctions.createNewDirectory(Path);
                String correct = Path + id + ".png";
                btn.setCompoundDrawablesWithIntrinsicBounds(f, null, null, null);

                if (id != 0) {
                    UserFunctions.saveBitmapImage(b, correct);
                }
            }
        }
    }


    private void displaycurrentBookMarkTitle(View v) {
        currentBookMarkEnrList = db.getAllbookMarkTabListForTheBook(currentPageNumber, currentBook.getBookID(), pdfActivity.this);

        bkmInEditMode = false;
        bkmInDeleteMode = false;
        //final ArrayList<BookMarkEnrichments> currentbmrkList = bookMarkedAdvTabList.get(pageNumber);

        advbkmarkpopup = new PopoverView(pdfActivity.this, R.layout.advsearchtitles);
        advbkmarkpopup.setContentSizeForViewInPopover(new Point((int) getResources().getDimension(R.dimen.info_popover_width), (int) getResources().getDimension(R.dimen.info_popover_height)));
        advbkmarkpopup.setDelegate(this);
        advbkmarkpopup.showPopoverFromRectInViewGroup(designPageLayout, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionUp, true);
        adv_bkmrktitle = (ListView) advbkmarkpopup.findViewById(R.id.lv_bkmark);
        adv_bkmrktitle.setAdapter(new adv_bkmrkadapter(pdfActivity.this, currentBookMarkEnrList));
        final Button btn_edit = (Button) advbkmarkpopup.findViewById(R.id.btn_edit);
        final Button bt_del = (Button) advbkmarkpopup.findViewById(R.id.btn_del);

        btn_edit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!bkmInEditMode) {
                    bkmInEditMode = true;
                    adv_bkmrktitle.invalidateViews();
                    btn_edit.setText(getResources().getString(R.string.done));
                } else {
                    bkmInEditMode = false;
                    adv_bkmrktitle.invalidateViews();
                    btn_edit.setText(getResources().getString(R.string.edit));
                }
            }
        });

        bt_del.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!bkmInDeleteMode) {
                    bkmInDeleteMode = true;
                    adv_bkmrktitle.invalidateViews();
                    bt_del.setText(getResources().getString(R.string.done));
                } else {
                    bkmInDeleteMode = false;
                    adv_bkmrktitle.invalidateViews();
                    bt_del.setText(getResources().getString(R.string.delete));
                }
            }
        });
        adv_bkmrktitle.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View v, int position,
                                    long arg3) {
                BookMarkEnrichments bookMarkEnr = currentBookMarkEnrList.get(position);
                //createAdvancedSearchTab(bookMarkEnr.getEnrichmentPath(), bookMarkEnr.getEnrichmentTitle());
                new createAdvanceSearchTab(bookMarkEnr.getEnrichmentPath(), bookMarkEnr.getEnrichmentTitle()).execute();
                advbkmarkpopup.dissmissPopover(true);
            }
        });
    }

    public void updateNameToButton(int position, String newtext) {
        newtext = newtext.replace("''", "'");
        //RelativeLayout rlview = (RelativeLayout) currentbkmarkLayout.getChildAt(position);
        Button nextEnrichSelectView = (Button) ll_bookmark_container.getChildAt(position);
        nextEnrichSelectView.setText(newtext);
    }

   /* public void removingBkMrkButton(int position, int pageno, int id) {
        ll_bookmark_container.removeViewAt(position);
        db.executeQuery("delete from BookmarkedSearchTabs where Id='" + id + "' and PageNo='" + pageno + "' and Bid='" + currentBook.getBookID() + "'");
        adv_bkmrktitle.invalidateViews();
        if (ll_bookmark_container.getChildCount() < 1) {
            parentBkmrkLayout.setVisibility(View.GONE);
            advbkmarkpopup.dissmissPopover(true);
        }
    }*/

    /* private void setEnrichTabSelected(int enrTabId){
         int  enrTabsWidth1 = 0;
         RelativeLayout rlview = null;
         //get current page tab no : Its reassigns at viewpager instantiate
         for (int i = 0; i < ll_container.getChildCount(); i++) {
             Button btnEnrTab;
             View view = ll_container.getChildAt(i);
             if (view instanceof RelativeLayout) {
                 RelativeLayout rlView = (RelativeLayout) view;
                 EnrichmentButton enrButton = (EnrichmentButton) rlView.getChildAt(0);
                 enrTabsWidth1+=rlview.getWidth();
                 if(enrButton.getEnrichmentId()==enrTabId){
                     sv.smoothScrollTo(enrTabsWidth1, 0);
                 }
             } else {
                 EnrichmentButton enrichButton = (EnrichmentButton) view;
                 enrTabsWidth1+=enrichButton.getWidth();
                 if(enrichButton.getEnrichmentId()==enrTabId){
                     sv.smoothScrollTo(enrTabsWidth1, 0);
                 }
             }

         }
     }*/
    @Override
    public void popoverViewWillShow(PopoverView view) {

    }

    @Override
    public void popoverViewDidShow(PopoverView view) {

    }

    @Override
    public void popoverViewWillDismiss(PopoverView view) {

    }

    @Override
    public void popoverViewDidDismiss(PopoverView view) {

    }


    public void onDismiss(DialogInterface d) {
        // TODO Auto-generated method stub
        //  if (parentBkmrkLayout.getWidth()>currentparentbklayout.getWidth()) {
//        int enrTabsWidth1 = parentBkmrkLayout.getWidth();
//        btn_bkmark_Sv.smoothScrollTo(enrTabsWidth1, 0);
        // }
    }

    public void openPdfWithFragment(File srcDir) {
        fragment = new PdfFragment();

        Bundle args = new Bundle();
        args.putString(FILE_NAME, pdfName);
        args.putString(FILE_PATH, srcDir.getPath());
        fragment.setArguments(args);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.pdfView, fragment).commit();
    }

    public void onPageChanged(int page, int pagecount) {
        //if(currentPageNumber!=page) {
        if (pageView != null)
            clearSelection();
        if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
            currentPageNumber = page;
        } else {
            currentPageNumber = pageCount - page + 1;
            arabicPage = page;
            if (currentPageNumber < 0) {
                currentPageNumber = 1;
                if (pagecount != 0) {
                    page = pagecount;
                }
            }
        }
        //fragment.core.textLines(currentPageNumber - 1);
        pdf_enrich.setVisibility(View.INVISIBLE);
        pdfView.setVisibility(View.VISIBLE);
        pageCount = pagecount;
        if (!(currentBook.getTotalPages() == pagecount)) {
            db.executeQuery("update books set totalPages='" + pageCount + "' where BID='" + currentBook.getBookID() + "'");
            currentBook.setTotalPages(pageCount);
        }
        pdf_seekbar.setProgress(page - 1);
        pdf_seekbar.setMax(pageCount - 1);
        // new loadEnrichmentTabs(currentPageNumber, ll_container, parentEnrRLayout).execute();
        setSeekBarTextValues(currentPageNumber);
        //clickedBtnHomeEnrichment(btnEnrHome, ll_container, currentPageNumber);
        checkForBookMark(currentPageNumber, currentEnrichmentTabId);
        search_layout.setVisibility(View.GONE);
        new loadEnrichmentTabs(currentPageNumber,CurrentRecyclerView,currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public void setSelectedRectangle(PointF startPoint, PointF endPoint, RectF selectedArea) {
        selectedText = "";
        String processedText = "";
        MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
        if (pageView != null)
            selectedText = pageView.getSelectedText();

        MuPDFPageView pageView2 = (MuPDFPageView) fragment.mDocView.getDisplayedView();
        Annotation annotation = null;
        if (selectedText.equals(""))
            selectedText = pageView2.getSelectedText2();
        if (selectedText.equalsIgnoreCase("")) {
            clearSelection();
        }
        if (pageView2.mSelectBox != null) {
            if (pageView2.mAnnotations != null)
                for (int i = 0; i < pageView2.mAnnotations.length; i++) {
                    float annotLeft = pageView2.mAnnotations[i].left;
                    float annotRight = pageView2.mAnnotations[i].right;
                    float annotTop = pageView2.mAnnotations[i].top;
                    float annotBottom = pageView2.mAnnotations[i].bottom;

                    float drawnLeft = pageView2.mSelectBox.left;
                    float drawnRight = pageView2.mSelectBox.right;
                    float drawnTop = pageView2.mSelectBox.top;
                    float drawnBottom = pageView2.mSelectBox.bottom;


                    if (annotLeft > annotRight) {
                        float temp;
                        temp = annotLeft;
                        annotLeft = annotRight;
                        annotRight = temp;
                    }
                    if (annotTop > annotBottom) {
                        float temp;
                        temp = annotBottom;
                        annotBottom = annotTop;
                        annotTop = temp;
                    }

                    if (drawnLeft > drawnRight) {
                        float temp;
                        temp = drawnRight;
                        drawnRight = drawnLeft;
                        drawnLeft = temp;
                    }
                    if (drawnTop > drawnBottom) {
                        float temp;
                        temp = drawnBottom;
                        drawnBottom = drawnTop;
                        drawnTop = temp;
                    }
                    pageView2.mAnnotations[i].left = annotLeft;
                    pageView2.mAnnotations[i].right = annotRight;
                    pageView2.mAnnotations[i].top = annotTop;
                    pageView2.mAnnotations[i].bottom = annotBottom;

                    pageView2.mSelectBox.left = drawnLeft;
                    pageView2.mSelectBox.right = drawnRight;
                    pageView2.mSelectBox.top = drawnTop;
                    pageView2.mSelectBox.bottom = drawnBottom;


                    if (pageView2.mSelectBox.intersect(pageView2.mAnnotations[i])) {
                        annotation = pageView2.mAnnotations[i];
                        pageView2.setmSelectedAnnotationIndex(i);
                    }

                }

            if (annotation != null) {
                switch (annotation.type) {
                    case HIGHLIGHT:
                        highlightItem.getTextView().setText("UnHighlight");
                        highlightItem.setActionId(ID_UNHILIGHT);
                        strikeItem.getTextView().setText("Strikeout");
                        strikeItem.setActionId(ID_STRIKE_OUT);
                        underLineItem.getTextView().setText("Underline");
                        underLineItem.setActionId(ID_UNDERLINE);
                        break;
                    case UNDERLINE:
                        underLineItem.getTextView().setText("UnUnderline");
                        underLineItem.setActionId(ID_UNHILIGHT);
                        highlightItem.getTextView().setText("Highlight");
                        highlightItem.setActionId(ID_HIGHLIGHT);
                        strikeItem.getTextView().setText("Strikeout");
                        strikeItem.setActionId(ID_STRIKE_OUT);
                        break;
                    case STRIKEOUT:
                        strikeItem.getTextView().setText("UnStrike");
                        strikeItem.setActionId(ID_UNHILIGHT);
                        highlightItem.getTextView().setText("Highlight");
                        highlightItem.setActionId(ID_HIGHLIGHT);
                        underLineItem.getTextView().setText("Underline");
                        underLineItem.setActionId(ID_UNDERLINE);
                        break;
                    case INK:
                        break;
                }

            } else {
                highlightItem.getTextView().setText("Highlight");
                highlightItem.setActionId(ID_HIGHLIGHT);
                strikeItem.getTextView().setText("Strikeout");
                strikeItem.setActionId(ID_STRIKE_OUT);
                underLineItem.getTextView().setText("Underline");
                underLineItem.setActionId(ID_UNDERLINE);
            }
            processedText = selectedText;
            if (startPoint != null && processedText != null && processedText.trim().length() > 0 && endPoint != null) {
                PointF event;
                if (startPoint.y < endPoint.y) {
                    event = startPoint;
                } else {
                    event = endPoint;
                }

                quickAction.show(fragment.mDocView, event);
                quickAction.setAnimStyle(QuickAction.ANIM_REFLECT);


            }

        }
    }


    public void clearText() {
        MuPDFView pageView = (MuPDFView) fragment.mDocView.getDisplayedView();
        if (pageView != null) {
            pageView.deselectText();
            fragment.mDocView.showSelectionHandles(false);
            pageView.cancelDraw();
        }

    }

    public String selectedRectangle = "", selectedText = "";

    public void setMyNoteString(String value) {
        selectedRectangle = value;
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (searchTask != null)
            searchTask.cancel(true);
        if (fragment != null && fragment.mSearchTask != null)
            fragment.mSearchTask.stop();

    }

    public void onDestroy() {
        if (searchTask != null)
            searchTask.cancel(true);
        if (fragment.core != null)
            fragment.core.onDestroy();

        fragment.core = null;
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    //Removing annotations on click.........
    public void removeAnnotation(Annotation annotation) {
        PointF event;
        String text = "";
        MuPDFPageView pageView = (MuPDFPageView) fragment.mDocView.getDisplayedView();
        if (pageView != null && annotation != null) {
            switch (annotation.type) {
                case HIGHLIGHT:
                    unhighlightItem.getTextView().setText("UnHighlight");
                    break;
                case UNDERLINE:
                    unhighlightItem.getTextView().setText("Ununderline");
                    break;
                case STRIKEOUT:
                    unhighlightItem.getTextView().setText("UnStrike");
                    break;
                case INK:

                    unhighlightItem.getTextView().setText("Remove Draw");

                    break;
            }
            event = pageView.getPageToViewPoint(annotation.left + (annotation.width()) / 3, annotation.top - 20);
            quickActionHilight.showHilight(fragment.mDocView, event);
            quickActionHilight.setAnimStyle(QuickAction.ANIM_REFLECT);
        }
    }

    //This method removes selection pins .......
    @Override
    public void removeSelection() {
        searchText.setText("");
        SearchTaskResult.set(null);
        searchString = "";
        clearSelection();
        fragment.mDocView.resetupChildren();
        hideKeyboard();
    }


    public void changeBrushColor(View v) {
        final ArrayList<String> recentPaintColorList = getRecentColorsFromSharedPreference(Globals.paintColorKey);
        int color = 0xffffffff;
        new PopupColorPicker(pdfActivity.this, color, v, recentPaintColorList, false, new PopupColorPicker.ColorPickerListener() {
            int pickedColor = 0;
            String hexColor = null;

            @Override
            public void pickedColor(int color) {
                hexColor = String.format("#%06X", (0xFFFFFF & color));
                pickedColor = color;
                brushColor = hexColor;
                btn_BrushColor.setBackgroundColor(Color.parseColor(hexColor));

               /* try {

                } catch (Exception e) {
                    e.printStackTrace();
                }*/

            }

            @Override
            public void dismissPopWindow() {
               /* if (hexColor != null) {
                    bookViewReadActivity.setandUpdateRecentColorsToSharedPreferences(Globals.paintColorKey, recentPaintColorList, hexColor);
                }*/
                if (hexColor != null) {
                    try {
                        hexColor = hexColor.replaceAll("#", "#80");
                        fragment.core.setINK_COLOR(hexColor);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void pickedTransparentColor(String transparentColor) {

            }

            @Override
            public void pickColorFromBg() {

            }
        });
    }


    public ArrayList<String> getRecentColorsFromSharedPreference(String key) {
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
        ArrayList<String> recentColorList = new ArrayList<>();
      /*  try {
            recentColorList = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        recentColorList.add("#B0171F");
        recentColorList.add("#FF82AB");
        recentColorList.add("#0000FF");
        recentColorList.add("#000000");
        recentColorList.add("#006400");
        return recentColorList;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {


    }


    public String copySelectedText = "";


    Dialog searchPopup;
    ArrayList<String> resultFound;

    private void initializeSearchPopUp(ArrayList<SearchResult> _searchResult) {
        searchPopup = new Dialog(pdfActivity.this);
        searchPopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
        searchPopup.setTitle("Search Results");

        WindowManager.LayoutParams wmlp = searchPopup.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.LEFT;
        wmlp.x = 40;   //x position
        wmlp.y = 110;   //y position
        searchPopup.setContentView(R.layout.search_list_popup_for_pdf);
        searchPopup.getWindow().setLayout((int) ((deviceWidth / 4.8) * 3.5), (int) ((deviceHeight / 4) * 2.1));//resize popup
        searchPopup.getWindow().setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        tv_noresult = (TextView) searchPopup.findViewById(R.id.tv_noresult);
        final ListView lv = (ListView) searchPopup.findViewById(R.id.listViewSearch);
        etSearch = (EditText) searchPopup.findViewById(R.id.editTextSearch);
        imgBtnBack = (ImageButton) searchPopup.findViewById(R.id.imageButtonBack);

        if (_searchResult.size() == 0) {
            tv_noresult.setText("No Result Found");
        } else {
            imgBtnBack.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    searchPopup.dismiss();
                    advSearchPopUp.show();
                    etSearch.setVisibility(View.VISIBLE);
                    imgBtnBack.setVisibility(View.VISIBLE);
                    searchPopup.findViewById(R.id.footer).setVisibility(View.VISIBLE);
                }
            });
            lv.setDividerHeight(2);
            lv.setAdapter(new searchListAdapter(_searchResult));
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View arg1, int pageNum, long arg3) {
                    //searchPopup.dismiss();
                    // pdf_seekbar.setProgress(foundSearhList.get(pageNum).getPageNo());

                  /*  if (currentBook.getBookLangugae().equalsIgnoreCase("english"))
                        fragment.mDocView.setDisplayedViewIndex(pageNum);
                    else*/

                    int pageNumber = foundSearhList.get(pageNum).getPageNo();

                    if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
                        SearchTaskResult searchTaskResult = new SearchTaskResult(selectedText, pageNumber, foundSearhList.get(pageNum).getHitViews());
                        //  fragment.mDocView.setDisplayedViewIndex(pageNumber);
                        fragment.mSearchTask.onTextFound(searchTaskResult);
                        fragment.mDocView.showSelectionHandles(false);
                    } else {
                        int page = pageCount - pageNumber - 1;
                        //fragment.mDocView.setDisplayedViewIndex(page);
                        SearchTaskResult searchTaskResult = new SearchTaskResult(selectedText, page, foundSearhList.get(pageNum).getHitViews());
                        fragment.mSearchTask.onTextFound(searchTaskResult);
                        fragment.mDocView.showSelectionHandles(false);
                    }


                }
            });
        }
        searchPopup.show();
    }

    public class searchListAdapter extends BaseAdapter {
        ArrayList<SearchResult> searchResult = null;

        public searchListAdapter(ArrayList<SearchResult> _searchResult) {
            searchResult = _searchResult;
            // Collections.sort(searchResult);
        }

        @Override
        public int getCount() {
            return searchResult.size();
        }

        @Override
        public Object getItem(int position) {
            return searchResult.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = getLayoutInflater().inflate(R.layout.lng_txtfield, null);
            TextView tv = (TextView) vi.findViewById(R.id.textView1);
            tv.setText("Page " + searchResult.get(position).getPageNo());
            tv.setTextColor(Color.rgb(0, 0, 0));
            if (searchResult.isEmpty() == true)//changes-s
            {
                tv_noresult.setVisibility(View.VISIBLE);
            } else {
                tv_noresult.setVisibility(View.GONE);
            }
            return vi;
        }
    }

    public void notePopUp() {
        noteClikced = true;
        //	data=	mDocView.getSelectedItem().toString();
        final Dialog dialog = new Dialog(pdfActivity.this);
        dialog.setTitle("Something");
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(300, 300);
        View notePopupView = getLayoutInflater().inflate(R.layout.note_popup_pdf, null, false);
        dialog.setContentView(notePopupView);
        et = (NotesEditText) dialog.findViewById(R.id.ll_notes_et_container);
        ImageView iv_nColor1 = (ImageView) dialog.findViewById(R.id.iv_nColorSelect1);
        ImageView iv_nColor2 = (ImageView) dialog.findViewById(R.id.iv_nColorSelect2);
        ImageView iv_nColor3 = (ImageView) dialog.findViewById(R.id.iv_nColorSelect3);
        ImageView iv_nColor4 = (ImageView) dialog.findViewById(R.id.iv_nColorSelect4);
        ImageView iv_nColor5 = (ImageView) dialog.findViewById(R.id.iv_nColorSelect5);
        ImageView iv_nColor6 = (ImageView) dialog.findViewById(R.id.iv_nColorSelect6);
        Button btn_save = (Button) dialog.findViewById(R.id.btn_save);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        dialog.show();
        final RelativeLayout rl_notepop = (RelativeLayout) dialog.findViewById(R.id.rl_note_popup_small_container);

                  /*  dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {

                            InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                            inputmanager.hideSoftInputFromWindow(et.getWindowToken(), 0);
                           *//* mWebviewVisibility.post(mDoneNote);*//*
                        }
                    });*/
        iv_nColor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg1);
            }
        });
        iv_nColor2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg2);
            }
        });
        iv_nColor3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg3);
            }
        });
        iv_nColor4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg4);
            }
        });
        iv_nColor5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg5);
            }
        });
        iv_nColor6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg6);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelection();
                dialog.dismiss();
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageView != null) {
                    noteText = et.getText().toString();
                    success = pageView.markupSelection(Annotation.Type.ADDNOTE);
                    InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.\
                    inputmanager.hideSoftInputFromWindow(et.getWindowToken(), 0);
                    if (success)
                        mWebviewVisibility.post(mDoneNote);


                }
                dialog.dismiss();
            }
        });

    }


    public void displayNotesList() {
        ArrayList listOfNotes;
        MyNoteDb myNoteDb = new MyNoteDb(this);
        listOfNotes = myNoteDb.getNoteData(fragment.core.getBookName(), fragment.core.getPageNumber());
        ListView noteListView;
        NotesAdapter adapter;
        noteListView = (ListView) findViewById(R.id.listOfNotes);
        adapter = new NotesAdapter(getApplicationContext(), listOfNotes);
        noteListView.setAdapter(adapter);


    }


    @Override
    public void setSeekbar(int mpageCount, int language) {
        pageCount = mpageCount;
        if (language == 0) {
            currentBook.setBookLangugae("English");
            fragment.mDocView.setDisplayedViewIndex(0);
        } else {
            currentBook.setBookLangugae("Arabic");
            fragment.mDocView.setDisplayedViewIndex(pageCount - 1);

        }

        setSeekBarTextValues(currentPageNumber);

    }

    @Override
    public void onSearchCompleted(ArrayList<SearchResult> searchResult) {
        initializeSearchPopUp(searchResult);
    }

    private class SearchBackgroundForAllPages extends AsyncTask<Void, Integer, ArrayList<SearchResult>> {
        private searchBackGround searchCompleted;
        ProgressDialog dialog;
        String text;

        private SearchBackgroundForAllPages(searchBackGround searchCompleted, String _text) {

            this.searchCompleted = searchCompleted;
            text = _text;

        }

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(pdfActivity.this);
            dialog.setMessage("Searching...");
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setMax(currentBook.getTotalPages());
            dialog.setCancelable(false);
            dialog.show();


        }

        int i = 0;

        @Override
        protected ArrayList<SearchResult> doInBackground(Void... params) {
            int direction = 0;

            SearchResult searchResult = null;
            SearchTaskResult.set(null);

            foundSearhList.clear();

            if (text != null && text.length() > 0 && !this.isCancelled()) {
                for (i = 0; i < currentBook.getTotalPages(); i++) {
                    searchResult = fragment.mSearchTask.searchResultList(text, direction, i, i);


                    if (searchResult.getHitViews() != null && searchResult.getHitViews().length > 0)
                        foundSearhList.add(searchResult);
                    if (isCancelled()) {
                        dialog.dismiss();
                        break;
                    }


                    publishProgress(i);
                }
            }
            return foundSearhList;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {

            dialog.setProgress(values[0]);

        }


        @Override
        protected void onPostExecute(ArrayList<SearchResult> results) {
            dialog.dismiss();
            searchCompleted.onSearchCompleted(foundSearhList);
        }

    }


    //Initializing color picker in drawing mode
    RelativeLayout rootLayout;

    public void initializeColorPicker() {
        rootLayout = (RelativeLayout) findViewById(R.id.rootView);
        if (colosRead == null) {
            colosRead = new ColorsRead(this, fragment);
            colosRead.setX(deviceWidth / 2.5f);
            colosRead.setY(UserFunctions.toPx(90));
            rootLayout.addView(colosRead);
        }
        colosRead.setVisibility(View.VISIBLE);
        colosRead.showColorPicker();

    }

    //Hiding buttons in Mobile device
    public void hideButtons() {
        if (!Globals.isTablet()) {
            mindMapLayout.setVisibility(View.GONE);
            stampLayout.setVisibility(View.GONE);
            downloadLayout.setVisibility(View.GONE);

        }
    }
    public class loadEnrichmentTabs extends AsyncTask<Void, Void, Void>{

        ArrayList<Enrichments> enrichmentTabList;
        int pageNumber;
        RecyclerView recyclerview;
        int Id;

        public loadEnrichmentTabs(int pageNumber, RecyclerView recyclerView, int SelectecEnrId){
            this.pageNumber = pageNumber;
            this.recyclerview=recyclerView;
            this.Id=SelectecEnrId;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            enrichmentTabList = db.getEnrichmentTabListForBookReadAct1(currentBook.getBookID(), pageNumber, pdfActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            boolean selected = false;
            for(int i=0;i<enrichmentTabList.size();i++){
                Enrichments enrich=enrichmentTabList.get(i);
                selected=false;
                if(enrich.getEnrichmentId()==Id){
                    enrichmentTabList.remove(i);
                    enrich.setEnrichmentSelected(true);
                    enrichmentTabList.add(i,enrich);
                    selected=true;
                    break;
                }
            }
            if(selected&&Id!=0){
                Enrichments enrich=enrichmentTabList.get(0);
                enrich.setEnrichmentSelected(false);
            }
            ArrayList<Enrichments> topEnrList = new ArrayList<>();
            for (Enrichments enrichments : enrichmentTabList){
                if (enrichments.getCategoryID()==0){
                    topEnrList.add(enrichments);
                }
            }
            loadView(pageNumber,topEnrList,recyclerview);
        }
    }

    public class GestureListener extends GestureDetector.SimpleOnGestureListener{

        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (gestureView!=null) {
              //  treeViewStructureList(gestureView, false,selectedEnr.getEnrichmentId());
                viewTreeList(gestureView, false,selectedEnr.getEnrichmentId());
            }
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
        }
    }

    public class loadSubEnrichmentTabs extends AsyncTask<Void, Void, Void>{

        ArrayList<Enrichments> enrichmentTabList;
        int pageNumber;
        RecyclerView recyclerview;
        int Id;
        int categoryId;
        RecyclerView clickableRecycler;

        public loadSubEnrichmentTabs(int pageNumber, RecyclerView recyclerView, int SelectecEnrId,int catId,RecyclerView newRecycler){
            this.pageNumber = pageNumber;
            this.recyclerview=recyclerView;
            this.Id=SelectecEnrId;
            this.categoryId = catId;
            this.clickableRecycler = newRecycler;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            enrichmentTabList = db.getEnrichmentTabListForCatId(currentBook.getBookID(), pageNumber, pdfActivity.this,categoryId);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (enrichmentTabList.size()>0) {
                subTabsLayout.setVisibility(View.VISIBLE);
                if (recyclerview.getId() == R.id.gridView3) {
                    subTabLayout1.setVisibility(View.VISIBLE);
                }else{
                    subTabLayout1.setVisibility(View.GONE);
                }

                loadView(pageNumber,enrichmentTabList,recyclerview);
            }else{
                if (clickableRecycler.getId()==R.id.gridView1) {
                    subTabsLayout.setVisibility(View.GONE);
                }
                subTabLayout1.setVisibility(View.GONE);
            }
        }
    }

    public void loadView(int pageNumber,ArrayList<Enrichments> enrichmentTabList,RecyclerView recyclerview){
        SubtabsAdapter  adapter = new SubtabsAdapter(subTabsLayout,subTabLayout1,subRecyclerView,subRecylerView1,db,pdfActivity.this, pageNumber, enrichmentTabList, recyclerview, new OnclickCallBackInterface<Object>() {
            @Override
            public void onClick(Object object) {
                progress.setVisibility(View.GONE);
                CallBackObjects objects = (CallBackObjects) object;
                if (objects.doubleTap){
                    gestureView = objects.view;
                    isLoadEnrInpopview = objects.loadEnrPoview;
                  //  treeViewStructureList(gestureView, false,selectedEnr.getEnrichmentId());
                    viewPopUp(gestureView,currentBtnEnrichment.getEnrichmentId());
                  //  viewTreeList(gestureView, false,selectedEnr.getEnrichmentId());
                }else {
                    RecyclerView recyclerView = objects.recyclerView;
                    clickableRecyclerView = recyclerView;
                    if (recyclerView != null) {
                        int position = objects.adapterPosition;
                        Enrichments enrichment = objects.horizontalList.get(position);
                        if (recyclerView.getId() == R.id.gridView2){
                            SubtabsAdapter.level2 = enrichment.getEnrichmentId();
                        }else if (recyclerView.getId() == R.id.gridView3){
                            SubtabsAdapter.level3 = enrichment.getEnrichmentId();
                        }else{
                            SubtabsAdapter.level1 = enrichment.getEnrichmentId();
                        }
                        for (Enrichments enrich : objects.horizontalList) {
                            if (enrich.isEnrichmentSelected()) {
                                enrich.setEnrichmentSelected(false);
                                break;
                            }
                        }
                        if (recyclerView.getId() == R.id.gridView1 && position == 0 && enrichment.getCategoryID() == 0) {
                            if (designPageLayout.getChildCount() == 5) {
                                designPageLayout.removeViewAt(4);
                            }
                            fragment.mDocView.setDisplayedViewIndex(pageCount-currentPageNumber);
                        }
                        if (enrichment.getEnrichmentType().equals(Globals.advancedSearchType)) {

                            clickedAdvanceSearchtab(enrichment);
                        } else if (enrichment.getEnrichmentType().equals(Globals.onlineEnrichmentsType) || (enrichment.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB))) {
                            clickedAdvanceSearchtab(enrichment);
                        } else {
                            if (enrichment.getEnrichmentId() != 0) {
                                clickedEnrichments(enrichment);
                            }
                        }
                        enrichment.setEnrichmentSelected(true);
                        currentEnrichmentTabId = enrichment.getEnrichmentId();
                        currentBtnEnrichment = enrichment;
                        selectedEnr = currentBtnEnrichment;
                        if (recyclerView.getId() == R.id.gridView1) {
                            if (enrichment.getEnrichmentId() != 0) {
                                new loadSubEnrichmentTabs(currentPageNumber, subRecyclerView, 0, enrichment.getEnrichmentId(), recyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                            } else {
                                subTabsLayout.setVisibility(View.GONE);
                                subTabLayout1.setVisibility(View.GONE);
                            }
                        } else if (recyclerView.getId() == R.id.gridView2) {
                            new loadSubEnrichmentTabs(currentPageNumber, subRecylerView1, 0, enrichment.getEnrichmentId(), recyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                        }
                    }
                }
            }
        });
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(pdfActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerview.setNestedScrollingEnabled(false);
        recyclerview.setLayoutManager(horizontalLayoutManagaer);
        recyclerview.setAdapter(adapter);
        if (currentPageNumber == 1 || currentPageNumber == pageCount){
            parentEnrRLayout.setVisibility(View.GONE);
        }else{
            parentEnrRLayout.setVisibility(View.VISIBLE);
        }
    }
    private void renamePopup(){
        final Dialog groupDialog = new Dialog(pdfActivity.this);
        groupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        groupDialog.setTitle(R.string.title);
        groupDialog.setContentView(getLayoutInflater().inflate(R.layout.group_edit, null));
        groupDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.5), RelativeLayout.LayoutParams.WRAP_CONTENT);
        final EditText editText = (EditText) groupDialog.findViewById(R.id.enr_editText);
        Button btnSave = (Button) groupDialog.findViewById(R.id.enr_save);
        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (editText.getText().toString().length()>0){
                    db.executeQuery("update enrichments set Title='" + editText.getText().toString() + "' where enrichmentID='" + currentBtnEnrichment.getEnrichmentId() + "' and BID='"+currentBook.getBookID()+"'");
                    if (clickableRecyclerView.getId() == R.id.gridView1) {
                        new loadEnrichmentTabs(currentPageNumber,CurrentRecyclerView,currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        new loadSubEnrichmentTabs(currentPageNumber, subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    } else if (clickableRecyclerView.getId() == R.id.gridView2) {
                        new loadSubEnrichmentTabs(currentPageNumber, subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                        new loadSubEnrichmentTabs(currentPageNumber, subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    }else{
                        new loadSubEnrichmentTabs(currentPageNumber, subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    }
                    groupDialog.dismiss();
                }
            }
        });
        Button btnCancel = (Button) groupDialog.findViewById(R.id.enr_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                groupDialog.dismiss();
            }
        });
        groupDialog.show();
    }

    public void deletePopup(){
        AlertDialog.Builder builder = new AlertDialog.Builder(pdfActivity.this);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                db.executeQuery("delete from enrichments where enrichmentID = '" + currentBtnEnrichment.getEnrichmentId() + "'");
                if (clickableRecyclerView.getId() == R.id.gridView1 &&currentBtnEnrichment.getCategoryID() == 0) {
                    if (designPageLayout.getChildCount() == 5) {
                        designPageLayout.removeViewAt(4);
                    }
                    SubtabsAdapter.level1 = 0;
                    fragment.mDocView.setDisplayedViewIndex(pageCount-currentPageNumber);
                    subTabsLayout.setVisibility(View.GONE);
                    subTabLayout1.setVisibility(View.GONE);
                }else{
                    RecyclerView curRecycler;
                    int catID;
                    if (clickableRecyclerView.getId() == R.id.gridView2){
                        curRecycler = CurrentRecyclerView;
                        catID = SubtabsAdapter.level1;
                    }else{
                        curRecycler = clickableRecyclerView;
                        catID = SubtabsAdapter.level2;
                    }
                    new loadSubEnrichmentTabs(currentPageNumber,clickableRecyclerView,0,catID,curRecycler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                dialog.cancel();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setTitle(R.string.delete);
        builder.setMessage(R.string.suredelete);
        builder.show();
    }

    public void viewTreeList(View view, boolean isAddEnrich, final int catId){
        categoryId = catId;
        treeStructureView = new TreeStructureView(currentBook, db, currentPageNumber, popoverView, pdfActivity.this, rootView, new TreeStructureCallBack() {
            @Override
            public void onClick(TreeNode node, Object object,PopoverView popoverView) {
                MyTreeHolder.IconTreeItem treeItem = (MyTreeHolder.IconTreeItem) object;
                Enrichments enrichments3 = treeItem.enrich;
                if (enrichments3.getEnrichmentId()!=0){
                    currentBtnEnrichment = enrichments3;
                    if (enrichments3.getEnrichmentType().equals(Globals.advancedSearchType)) {

                        clickedAdvanceSearchtab(enrichments3);
                    } else if (enrichments3.getEnrichmentType().equals(Globals.onlineEnrichmentsType) || (enrichments3.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB))) {
                        clickedAdvanceSearchtab(enrichments3);
                    } else {
                        if (enrichments3.getEnrichmentId() != 0) {
                            clickedEnrichments(enrichments3);
                        }
                    }
                }
                node.setSelected(true);
                if (enrichments3.getEnrichmentTitle().equals("")) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    //  treeViewStructureList(gestureView, true, enrichments1.getCategoryID());
                    viewTreeList(gestureView, true, enrichments3.getCategoryID());
                } else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.internet_search))) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    search_layout.setVisibility(View.VISIBLE);
                }else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.rename))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    renamePopup();
                }else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.delete))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    deletePopup();
                }else if (enrichments3.getEnrichmentId()==0 && (enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.blank_page)) || enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.duplicate_page)))){
                    Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
                    bookViewIntent.putExtra("Book", currentBook);
                    bookViewIntent.putExtra("currentPageNo", currentPageNumber);
                    bookViewIntent.putExtra("enrichmentTabId", 0);
                    bookViewIntent.putExtra("Shelf", gridShelf);
                    if (enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.blank_page))) {
                        bookViewIntent.putExtra("createBlankEnr", "blank");
                    }else{
                        bookViewIntent.putExtra("createBlankEnr", "duplicate");
                    }
                    bookViewIntent.putExtra("categoryId",catId);
                    startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
                }else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.edit))){
                    Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
                    bookViewIntent.putExtra("Book", currentBook);
                    bookViewIntent.putExtra("currentPageNo", currentPageNumber);
                    bookViewIntent.putExtra("enrichmentTabId", currentBtnEnrichment.getEnrichmentId());
                    bookViewIntent.putExtra("Shelf", gridShelf);
                    bookViewIntent.putExtra("createBlankEnr", "edit");
                    bookViewIntent.putExtra("categoryId",catId);
                    startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
                }
            }
        });
        treeStructureView.treeViewStructureList(isLoadEnrInpopview,currentBtnEnrichment,view,isAddEnrich,catId);
    }

    private void viewPopUp(View view,final int catId){
        categoryId = catId;
        treeStructureView = new TreeStructureView(currentBook, db, currentPageNumber, popoverView, pdfActivity.this, rootView, new TreeStructureCallBack() {
            @Override
            public void onClick(TreeNode node, Object object, PopoverView popoverView) {
                Enrichments enrichments3 = (Enrichments) object;
                if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.rename))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    renamePopup();
                }else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.delete))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    deletePopup();
                }else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.edit))){
                    Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
                    bookViewIntent.putExtra("Book", currentBook);
                    bookViewIntent.putExtra("currentPageNo", currentPageNumber);
                    bookViewIntent.putExtra("enrichmentTabId", currentBtnEnrichment.getEnrichmentId());
                    bookViewIntent.putExtra("Shelf", gridShelf);
                    bookViewIntent.putExtra("createBlankEnr", "edit");
                    bookViewIntent.putExtra("categoryId",catId);
                    startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
                }
                if (enrichments3.getEnrichmentTitle().equals("")) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    //  treeViewStructureList(gestureView, true, enrichments1.getCategoryID());
                    viewTreeList(gestureView, true, enrichments3.getCategoryID());
                }
            }
        });
        treeStructureView.treeViewStructureListNew(view,currentBtnEnrichment);
    }
}

