package com.semanoor.manahij;

import android.Manifest;
import android.accounts.Account;
import android.annotation.TargetApi;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.LocalActivityManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.preference.PreferenceManager;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mobeta.android.dslv.DragSortListView;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.inappbilling.util.Purchase;
import com.semanoor.manahij.FontTables.FontTablesDB;
import com.semanoor.manahij.FontTables.ParseFontsJson;
import com.semanoor.manahij.FontTables.fonttablescallback;
import com.semanoor.manahij.mqtt.CallbackInterface;
import com.semanoor.manahij.mqtt.MQTTSubscriptionService;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.Resources;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.DownloadBackground;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.MyContacts;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.UserGroups;
import com.semanoor.source_sboookauthor.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.semanoor.manahij.mqtt.CallConnectionStatusService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import me.tangke.slidemenu.SlideMenu;
import me.tangke.slidemenu.SlideMenu.LayoutParams;


public class SlideMenuWithActivityGroup extends ActivityGroup  implements Serializable, fonttablescallback {
    public SlideMenu mSlideMenu;
	public DatabaseHandler db;
	private static final int INITIAL_PERMISSIONS_REQUEST = 54321;
	public static final String FILTER = "noor.subscribe";
	private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 777777;
	ViewPager viewPager;
	private UserFunctions userfn;
	private Globals global;
	//public UserCredentials userCredentials;
	public ArrayList<UserGroups> groups;
	public ArrayList<UserGroups> group_details;
	String groupname;
	boolean deletemode;
	public Enrichments enrichment;
	String language;
	private GoogleApiClient mGoogleApiClient;
	private boolean mIntentInProgress;
	private boolean mSignInClicked, googleSignInClicked, facebookSignInClicked;
	private Bitmap mIcon11 = null;
	private ConnectionResult mConnectionResult;
	private static final int RC_SIGN_IN = 9001;
	// Profile pic image size in pixels
	private static final int PROFILE_PIC_SIZE = 400;
	private ProgressBar googleFbProgressBar;

	private UiLifecycleHelper uiHelper;
	private String fbPhotoUrl;
	boolean openPdfReader;
	private boolean bookSearch;
	public int CAPTURE_GALLERY = 1003;
	Malzamah malzamah;
	public boolean isDeleteBookDir;
	public ImageView imgViewCover;
	public com.semanoor.source_sboookauthor.PopoverView PopoverView;
	private Uri selectedGalleryImageUri;
	WebService webService;
	boolean exist;
	public ViewAdapter loginAdapter;
	public GridShelf gridShelf;
	//public ArrayList<Object> bookListArray = new ArrayList<Object>();
	private int mSlideState;
	private Subscription subscription;
	private final static String SLIDE_STATE = "SlideState";
	//public UserCredentials userCredentials;
	public String userName;
	public String scopeId;
	public int scopeType;
	public String email;
	public DragSortListView gridview;
	//ArrayList<String>BookDetails=new ArrayList<String>();
	public ProgressDialog processPageProgDialog;
	boolean grp_delete;
	public ArrayList<UserGroups> User_groups;
	public boolean deleteMode = false;
    private int themesActivityRequest = 1;
	boolean fbEnrDownload;
	String msgFromBrowserUrl;
	private FirebaseAnalytics firebaseAnalytics;
	public IabHelper mHelper;
	FontTablesDB fonyTableDb;
	ParseFontsJson parseFontsJson;
	public Dialog dialog/*, mqttdialog*/ ;

	public static int  CONNECT_CALL_ESTABLISHMENT=236;
	MQTTSubscriptionService service ;
	CallbackInterface callbackInterface;
	ManahijApp app;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		Uri data = intent.getData();
		loadLocale();
//		if (data != null && intent.getType().equals("application/pdf")) {
//
//		}else{
			new getNewVersionCode().execute();
//		}
		app= (ManahijApp)getApplicationContext();
		app.setState(false);
		if(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName()!=null){
		callMQTT();}
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		boolean wipeData = prefs.getBoolean(Globals.wipeAllData, true);
		if (wipeData){
			clearApplicationData();
			String DB_PATH = getFilesDir().getParentFile().getPath()+"/databases/sboookAuthor.sqlite";
			if (new File(DB_PATH).exists()) {
				this.deleteDatabase(DB_PATH);
			}
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
			SharedPreferences.Editor editor = pref.edit();
			editor.putBoolean(Globals.wipeAllData,false);
			editor.commit();
		}
		service = new MQTTSubscriptionService();
		dialog = new Dialog(this);
		//Remove title Bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		if (Build.VERSION.SDK_INT >= 23) {
			initializePermissionForMarsh();
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {

			//If the draw over permission is not available open the settings screen
			//to grant the permission.
			Intent in = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
					Uri.parse("package:" + getPackageName()));
			startActivityForResult(in, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
		}
		String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
				Settings.Secure.ANDROID_ID);

		ManahijApp.getInstance().getPrefManager().addDeviceId(android_id);
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);
		db = DatabaseHandler.getInstance(this);
		if(alterTableGruopbooks(db)){
			System.out.println("column");
		}
		String insertQuery = "update books set domainURL='http://www.nooor.com/sboook/IPadSSboookService.asmx'";
		alterTableGroupbooks(insertQuery);
		alterTableAddClientId();
		alterTableAddBookDirection();
		alterTableEnrichments();
		alterTableAddCategorytId();
		alterNoteTable();
		alterNoteTableforPdf();
		firebaseAnalytics = FirebaseAnalytics.getInstance(this);
		firebaseAnalytics.setAnalyticsCollectionEnabled(true);
		firebaseAnalytics.setMinimumSessionDuration(20000);
		Bundle bundle1 = new Bundle();
		String name = "App Opened";
		bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
		firebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle1);

		String oldDbPath = this.getFilesDir().getParentFile().getPath() + "/databases/sbook.sqlite";

		//String packageName = getApplicationContext().getPackageName();
		String packageName = BuildConfig.APPLICATION_ID;
		//String packageName = Globals.sbaPackageName;

		if (packageName.equals(Globals.sbaPackageName)) {
			Globals.setLimitedVersion(false);
			Globals.setISGDRIVEMODE(false);
			Globals.setNewCurriculumWebServiceURL(Globals.sbaNewCurriculumWebServiceURL);
			Globals.setSubscriptionWebserviceURL(Globals.sbaSubscriptionURL);
			Globals.setStoreUrl(Globals.demoStoreJsonUrl);
			Globals.setCurrentProjPackageName(Globals.manahijPackageName);
		} else {
			Globals.setLimitedVersion(true);
			Globals.setISGDRIVEMODE(true);
			//Globals.setNewCurriculumWebServiceURL(Globals.sbaNewCurriculumWebServiceURL);
			Globals.setNewCurriculumWebServiceURL(Globals.manahijNewCurriculumWebServiceURL); //  <--manahijNewCurriculumWebServiceURL
			Globals.setSubscriptionWebserviceURL(Globals.manahijSubscriptionURL);
			Globals.setStoreUrl(Globals.storeJsonUrl);
			Globals.setCurrentProjPackageName(packageName);
		}

		/*if (!Globals.isLimitedVersion()) {
			checkValidation();
		}*/

		global = Globals.getInstance();

		subscription = Subscription.getInstance();
		boolean isLoggedIn = UserFunctions.checkLoginAndAlert(this, false);
		if (Globals.isLimitedVersion()) {
			if (isLoggedIn) {
					subscription.new getSubscribedId(this,"").execute();
			} else {
					subscription.initializeIABHelper(this, false);
			}
		}

		userfn = UserFunctions.getInstance();

		if (data != null && intent.getType()!=null && intent.getType().equals("application/pdf")&& !data.toString().contains("_display_name=")) {
			//CopyOpenpdfReader(data);
			new CopyOpenPdfReader(this, data, db, userfn);
		}
        setContentView(R.layout.activity_slidemenu);
		Globals.setTablet(isTablet(this));

        //Globals.setTablet(getResources().getBoolean(R.bool.isTablet));

		webService = new WebService(SlideMenuWithActivityGroup.this, Globals.getNewCurriculumWebServiceURL());
		db = DatabaseHandler.getInstance(this);
		userfn = UserFunctions.getInstance();
		global = Globals.getInstance();
		checkAndLogin();
		if (checkInternetConnection()) {
			new UpdateUserGroups(SlideMenuWithActivityGroup.this).execute();
		}else{
			String str = Globals.TARGET_BASE_FILE_PATH+"groups/0_groups.json";
			if ( new File(str).exists()){
				getGroupsList groupsList = new getGroupsList(SlideMenuWithActivityGroup.this);
				groupsList.loadGroupData("0_groups");
			}
		}
		//db.alterTableAddColumnDomainURL();
		//gridShelf = new GridShelf(SecondaryActivity.this, db, userfn);
		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = userfn.GetDeviceResolution(display);
		global.setDeviceHeight(displayMetrics.heightPixels);
		global.setDeviceWidth(displayMetrics.widthPixels);
		IntentFilter filter = new IntentFilter();
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(mNetworkStateIntentReceiver, filter);
		//bookListArray=global.bookListArray;

        //mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(SlideMenuWithActivityGroup.this).addOnConnectionFailedListener(this).addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN).build();

		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
				requestEmail().
				requestProfile().
				requestScopes(new Scope(Scopes.PLUS_LOGIN)).
				requestScopes(new Scope(Scopes.PLUS_ME)).
				requestScopes(new Scope("https://www.googleapis.com/auth/contacts.readonly")).
				build();
		mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage((MainActivity) this.getCurrentActivity(), new GoogleApiClient.OnConnectionFailedListener() {
			@Override
			public void onConnectionFailed(ConnectionResult connectionResult) {

			}
		}).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
		//mGoogleApiClient.connect();
		if (Globals.ISGDRIVEMODE()) {
			//new loadCloudEnrichmentList().execute();
		}else{
			if(((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).btn_text!=null) {
				((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).btn_text.setText(String.valueOf(0));
			}
		}

		if(intent != null && intent.getAction().equals("com.semanoor.manahij")){
			Bundle bundle = intent.getExtras();
			if(bundle != null){
				String msg= bundle.getString("msg_from_browser");
				System.out.println(msgFromBrowserUrl);
				msg=msg.replace("id=","");
				CheckingUserLogin(msg);
			}
        }
	}

	@TargetApi(Build.VERSION_CODES.M)
	private void initializePermissionForMarsh(){
		final List<String> permissionList = new ArrayList<String>();
		addPermission(permissionList, Manifest.permission.READ_EXTERNAL_STORAGE);
		addPermission(permissionList, Manifest.permission.WRITE_EXTERNAL_STORAGE);
		addPermission(permissionList, Manifest.permission.GET_ACCOUNTS);
		addPermission(permissionList, Manifest.permission.RECORD_AUDIO);
		addPermission(permissionList, Manifest.permission.INTERNET);
		addPermission(permissionList, Manifest.permission.CAMERA);
		if (permissionList.size() > 0) {
			requestPermissions(permissionList.toArray(new String[permissionList.size()]), INITIAL_PERMISSIONS_REQUEST);
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	private boolean addPermission(List<String> permissionsList, String permission){
		if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
			permissionsList.add(permission);
		}
		return true;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
			case INITIAL_PERMISSIONS_REQUEST: {

				for (int i = 0; i < permissions.length; i++) {
					String permission = permissions[i];
					int grantResult = grantResults[i];
					if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
						if (grantResult == PackageManager.PERMISSION_GRANTED) {
							fonyTableDb = new FontTablesDB(SlideMenuWithActivityGroup.this);
							parseFontsJson = new ParseFontsJson(this, this, fonyTableDb);
							parseFontsJson.parseJson();
						}
					}
				}


			}
		}
	}

	@Override
	public void onContentChanged() {
		super.onContentChanged();
		mSlideMenu = (SlideMenu) findViewById(R.id.slideMenu);

		final LocalActivityManager activityManager = getLocalActivityManager();

		View slide=getLayoutInflater().inflate(R.layout.activity_secondary,null);
		viewPager = (ViewPager)slide. findViewById(R.id.viewPager);
		//viewPager.setAdapter(new ViewAdapter());
		loginAdapter = new ViewAdapter();
		//loginAdapter.btnLoginLayout
		viewPager.setAdapter(loginAdapter);
		viewPager.setOffscreenPageLimit(2);
		//viewPager.setCurrentItem(Integer.parseInt(currentPageNumber) - 1);
		viewPager.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});
		RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		//slide.setLayoutParams(params);
		mSlideMenu.addView(slide, new LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.MATCH_PARENT,
				LayoutParams.ROLE_SECONDARY_MENU));
		/*View secondary = activityManager.startActivity("SecondaryActivity",
				new Intent(this, SecondaryActivity.class)).getDecorView();
		mSlideMenu.addView(secondary, new LayoutParams(
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				LayoutParams.ROLE_SECONDARY_MENU));*/
		Intent bookViewIntent = new Intent(getApplicationContext(), MainActivity.class);
		bookViewIntent.putExtra("SlidemenuActivity", SlideMenuWithActivityGroup.this);
		//View content = activityManager.startActivity("ContentActivity",new Intent(this, MainActivity.class)).getDecorView();
		View content = activityManager.startActivity("ContentActivity",bookViewIntent).getDecorView();
		mSlideMenu.addView(content, new LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT,
				LayoutParams.ROLE_CONTENT));
		mSlideMenu.setOnSlideStateChangeListener(new SlideMenu.OnSlideStateChangeListener() {
			@Override
			public void onSlideStateChange(int slideState) {
				mSlideState = slideState;
				updateText();
			}

			@Override
			public void onSlideOffsetChange(float offsetPercent) {
				updateText();
			}
		});




	}

	/*
    * Checking whether the user login or not
    */
	private void CheckingUserLogin(final String msg){
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SlideMenuWithActivityGroup.this);
		String emaild = preferences.getString(Globals.sUserEmailIdKey, "");
		if(!emaild.equals("")) {
			Intent activity = new Intent((MainActivity) this.getCurrentActivity(), CloudBooksActivity.class);
			//intent.putExtra("SlidemenuActivity",SlideMenuActivity);
			activity.putExtra("Shelf", ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf);
			activity.putExtra("id", msg);
			((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).startActivityForResult(activity, 3);
		}else{
			AlertDialog.Builder alertdialog = new AlertDialog.Builder(SlideMenuWithActivityGroup.this);
			alertdialog.setTitle(R.string.alert);
			alertdialog.setMessage(R.string.login_alert);
			alertdialog.setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					msgFromBrowserUrl=msg;
					fbEnrDownload=true;
					mSlideMenu.mCurrentContentOffset= -(int)getResources().getDimension(R.dimen.info_popover_loginwidth);
					mSlideMenu.invalidateMenuState();
					mSlideMenu.invalidate();
					mSlideMenu.requestLayout();
				}
			});
			alertdialog.setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					fbEnrDownload=false;
					dialog.cancel();

				}
			});
			alertdialog.show();



		}
	}
	private void updateText() {
		int slideStateRes;
		switch (mSlideState) {
			default:
			case SlideMenu.STATE_CLOSE:

				break;
			case SlideMenu.STATE_OPEN_LEFT:

				break;
			case SlideMenu.STATE_OPEN_RIGHT:
				refreshingUserData();
                break;
			case SlideMenu.STATE_DRAG:

				break;
			case SlideMenu.STATE_SCROLL:

				break;
		}
	}

	public void refreshingUserData(){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SlideMenuWithActivityGroup.this);
		int scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
		if (scopeType != 0) {
			viewPager.setCurrentItem(1);
			loginAdapter.btnLoginLayout.setVisibility(View.GONE);
			loginAdapter.loggedInSuccessLayout.setVisibility(View.VISIBLE);
			loginAdapter.usertxtView.setText(userName);
			if (mIcon11 != null) {
				loginAdapter.userImagView.setImageBitmap(mIcon11);
			}

			    if (scopeType == Globals.scopeTypeFacebook) {
				loginAdapter.btnlogout.setVisibility(View.GONE);
				loginAdapter.fbLogoutBtn.setVisibility(View.VISIBLE);
				if (mIcon11 == null && fbPhotoUrl != null) {
					Glide.with(SlideMenuWithActivityGroup.this).load(fbPhotoUrl)
							.thumbnail(0.5f)
							.crossFade()
							.diskCacheStrategy(DiskCacheStrategy.ALL)
							.override(200, 200)
							.into(loginAdapter.userImagView);
					//new LoadProfileImage(loginAdapter.userImagView).execute(fbPhotoUrl);
				}
			} else if (scopeType == Globals.scopeTypeGoogle) {
				String photoUrl = prefs.getString(Globals.sUserEmailPhotoUrl, "");
				if (!photoUrl.equals("")) {
					Glide.with(SlideMenuWithActivityGroup.this).load(photoUrl)
							.thumbnail(0.5f)
							.crossFade()
							.diskCacheStrategy(DiskCacheStrategy.ALL)
							.override(200, 200)
							.into(loginAdapter.userImagView);
					//new LoadProfileImage(loginAdapter.userImagView).execute(photoUrl);
				}
			}

			if (Globals.ISGDRIVEMODE()) {
				//new loadCloudEnrichmentList().execute();
			} else {
				new loadEnrichmentAndResourceByUserId().execute();
			}
		}else{
			if(((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).btn_text!=null) {
				((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).btn_text.setText(String.valueOf(0));
			}
		}
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	    mSlideState = savedInstanceState.getInt(SLIDE_STATE);
		//updateText();
	}


	private void loadLocale() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		language = prefs.getString(Globals.languagePrefsKey, "en");
		//oldDatabasePrefs = prefs.getBoolean(Globals.oldDatabase, Globals.olddb);
		//changeLang(language);
	}

	@Override
	public void response(String response) {
		try {
			int i = 0;
			JSONObject fonttable = new JSONObject(response);
			Iterator iterator = fonttable.keys();
			while (iterator.hasNext()) {
				String key = (String) iterator.next();
				JSONObject objkey = fonttable.getJSONObject(key);
				String fileName = objkey.optString("name");
				String version = objkey.optString("version");
				String url = objkey.optString("url");
				if (fonyTableDb.isFontAvailable(fileName)) {

				}
				//Download Json file from server when version changed......
				if (!fonyTableDb.addFontTables(fileName, version, url)) {
					parseFontsJson.getJsonDataFronServer(fileName, url);

				}
			}
		} catch (Exception e) {
			Log.e("exception", e.toString());
		}

	}


	public class ViewAdapter extends PagerAdapter {

		public RelativeLayout btnLoginLayout, loggedInSuccessLayout,grpLayout;
		public ImageView userImagView;
		public TextView usertxtView, txtEnrichment, txtResources, txtHeadTitle, txtCloudShare;
		public Button btnEnrichment, btnResources,btnGroups,btnSharedUsers, btnCloudShare,btnsync,btn_theme,btn_redeem;
		public ListView listViewEnrRes, listViewSelectEnrRes,listViewGroup,listViewEmailGroup,shared_listView;
		public ArrayList<Enrichments> selectedEnrichments = new ArrayList<Enrichments>();
		public ArrayList<Enrichments> unreadEnrichments = new ArrayList<Enrichments>();
		public ArrayList<Resources> unreadResources = new ArrayList<Resources>();
		public ArrayList<Resources> selectedResources = new ArrayList<Resources>();
		public ArrayList<Enrichments> delEnrList = new ArrayList<Enrichments>();
		public ArrayList<Resources> delResourcesList = new ArrayList<Resources>();
		public ArrayList<HashMap<String, String>> selectedCloudEnrList = new ArrayList<HashMap<String, String>>();
		private boolean resourcesClicked, enrichmentClicked,grpInEditMode,textChecking;
		private UsersSelectEnrResAdapter userSeleEnrresAdapter;
		private SharedUsersAdapter sharedAdapter;
		public ProgressBar progressBar;
		public Button btnlogout,btn_edit,btnDownload;
		public LoginButton fbLogoutBtn;
		@Override
		public int getCount() {
			return 10;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return (view == object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {

			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
			UserFunctions.changeFont(container,font);
			View view = null;
			switch (position) {
				case 0:
					view = inflater.inflate(R.layout.selection_view, null);
					Button btn_signin = (Button) view.findViewById(R.id.btn_signin);
					Button btn_lang = (Button) view.findViewById(R.id.btn_lang);
					Button btn_theme=(Button) view.findViewById(R.id.btn_theme);
					Button btn_nooor_plus = (Button) view.findViewById(R.id.btn_nooor_plus);
					btn_lang.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							DisplayLanguageAlertDialog(SlideMenuWithActivityGroup.this, R.string.CHANGE_LANGUAGE_MESSAGE, R.string.CHANGE_LANGUAGE_TITLE);

						}
					});

					btn_signin.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							if (Globals.ISGDRIVEMODE()) {
								googleSignInClicked = true;
								signInWithGLogin();
							} else {
								viewPager.setCurrentItem(1, true);
							}
						}
					});
					btn_theme.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent=new Intent(SlideMenuWithActivityGroup.this,ThemeActivity.class);
							intent.putExtra("Shelf", ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf);
							startActivityForResult(intent,themesActivityRequest);
						}
					});
					btn_nooor_plus.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(SlideMenuWithActivityGroup.this,NooorPlusActivity.class);
							startActivity(intent);
						}
					});
					break;
				case 1:
					view = inflater.inflate(R.layout.social_login, null);

					btnLoginLayout = (RelativeLayout) view.findViewById(R.id.loginBtnLayout);
					loggedInSuccessLayout = (RelativeLayout) view.findViewById(R.id.loggedInSuccessLayout);
					userImagView = (ImageView) view.findViewById(R.id.userImgView);
					usertxtView = (TextView) view.findViewById(R.id.txtUserName);
					progressBar = (ProgressBar) view.findViewById(R.id.progressBar2);
					btnEnrichment = (Button) view.findViewById(R.id.btnEnrichment);
					btnEnrichment.setEnabled(false);
					grpLayout = (RelativeLayout) view.findViewById(R.id.layout);
					if (!Globals.isTablet()){
						grpLayout.setVisibility(View.GONE);
					}
					btnGroups = (Button) view.findViewById(R.id.btnGroups);
					btn_redeem = (Button) view.findViewById(R.id.btn_redeem);
					btn_redeem.setVisibility(View.GONE);
					btnSharedUsers = (Button) view.findViewById(R.id.btnSharedUsers);
					btnsync= (Button) view.findViewById(R.id.btnsync);
					RelativeLayout cloudShareLayout = (RelativeLayout) view.findViewById(R.id.cloud_share_layout);
					RelativeLayout enrLayout = (RelativeLayout) view.findViewById(R.id.rl1);
					RelativeLayout resLayout = (RelativeLayout) view.findViewById(R.id.rl2);
					btnCloudShare = (Button) view.findViewById(R.id.btnCloudSharing);
					btnCloudShare.setEnabled(false);
					txtCloudShare = (TextView) view.findViewById(R.id.txtCloudSharing);
					btn_theme=(Button) view.findViewById(R.id.btn_theme);
					Button btn_mng_account=(Button) view.findViewById(R.id.btn_mng_account);
					Button btn_nooorplus=(Button) view.findViewById(R.id.btn_nooorplus);
					if (Globals.ISGDRIVEMODE()){
						btnsync.setVisibility(View.GONE);
						btnSharedUsers.setVisibility(View.GONE);
						cloudShareLayout.setVisibility(View.GONE);
						enrLayout.setVisibility(View.GONE);
						resLayout.setVisibility(View.GONE);
					} else {
						btnsync.setVisibility(View.VISIBLE);
						btnSharedUsers.setVisibility(View.VISIBLE);
						cloudShareLayout.setVisibility(View.GONE);
						enrLayout.setVisibility(View.VISIBLE);
						resLayout.setVisibility(View.VISIBLE);
					}
					btn_nooorplus.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(SlideMenuWithActivityGroup.this,NooorPlusActivity.class);
							startActivity(intent);
							/*Nooorplus noorPlus=new Nooorplus(SlideMenuWithActivityGroup.this);
							noorPlus.DisplayingNoorplusDialog();*/
						}
					 });
					btn_mng_account.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Uri uri = Uri.parse("http://school.nooor.com/ManageAccount/Login.html");
							Intent intent = new Intent(Intent.ACTION_VIEW, uri);
							startActivity(intent);
						}
					});
					btn_theme.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
						    Intent intent=new Intent(SlideMenuWithActivityGroup.this,ThemeActivity.class);
							intent.putExtra("Shelf", ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf);
							startActivityForResult(intent,themesActivityRequest);
						}
					});
					btn_redeem.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							redeemDialog();
						}
					});
					btnsync.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							new getUserGroupDetails(scopeId).execute();

						}
					});
					Button  btnBack3 = (Button) view.findViewById(R.id.btnBack);
					btnBack3.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							viewPager.setCurrentItem(0, true);
						}
					});

					btnCloudShare.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							viewPager.setCurrentItem(3, true);
							userSeleEnrresAdapter = new UsersSelectEnrResAdapter(webService.cloudEnrList);
							listViewSelectEnrRes.setAdapter(userSeleEnrresAdapter);
							listViewSelectEnrRes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
								@Override
								public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
									String bookDetails = null;
									HashMap<String, String> map = webService.cloudEnrList.get(position);
									String cloudEnrselected = map.get(webService.KEY_CLOUDENRSELECTED);
									String enrichmentNo=map.get(webService.KEY_CENRICHMENTS);
									String jsonContent = map.get(webService.KEY_CECONTENT);
									String bookTitle = "";
									try {
										//JSONObject jsonObject = new JSONObject(jsonContent);
										//String storeBookId = jsonObject.getString("IDBook");
										JSONObject jsonObject = new JSONObject(jsonContent);
										String storeBookId = jsonObject.getString("IDBook");
										String type=jsonObject.getString("type");
										if(type.equals("Enrichment")) {
											String idBook=jsonObject.getString("BOOK");
											JSONObject jsonObject1 = new JSONObject(idBook);
											String bookUrl = jsonObject1.getString("downloadURL");
											String storeBook = jsonObject1.getString("storeBook");
											bookTitle = jsonObject1.getString("title");
											String language = jsonObject1.getString("language");
											String imageUrl = jsonObject1.getString("imageURL");
											String template = jsonObject1.getString("template");
											if (template.equals("0")) template = "6";
											String categoryId = jsonObject1.getString("CID");
											if (categoryId.equals("0")) categoryId = "1";
											String descritpion = jsonObject1.getString("description");
											String totalPages = jsonObject1.getString("totalPages");

											bookDetails = enrichmentNo + "#" + storeBookId + "#" + bookUrl + "#" + bookTitle + "#" + descritpion + "#" + template + "#" + totalPages + "#" + jsonObject1.getString("storeID") + "#" + categoryId + "#" + language + "#" + jsonObject1.getString("downloadCompleted") + "#" + bookUrl + "#" + jsonObject1.getString("bookEditable") + "#" + jsonObject1.getString("price") + "#" + imageUrl + "#" + "free";
										}
                                        if (!checkStorBookExist(storeBookId)) {

											if (type.equals("Enrichment")) {
												((BaseAdapter) parent.getAdapter()).notifyDataSetChanged();
												AlertDialog.Builder builder = new AlertDialog.Builder(SlideMenuWithActivityGroup.this);
												String alertMsg = getResources().getString(R.string.the_book)+" "+bookTitle+" "+getResources().getString(R.string.book_not_exist);
												builder.setMessage(alertMsg);
												builder.setCancelable(false);
												//final String BookDetails = bookDetails;
												//final String finalBookDetails = bookDetails;
												final String BookDetails = bookDetails;
												builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

													@Override
													public void onClick(DialogInterface dialog, int which) {
														gridview = ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridView;
														gridShelf = ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf;
														GDriveExport gdrive = new GDriveExport(SlideMenuWithActivityGroup.this.getCurrentActivity(), BookDetails);
														gdrive.downloadBook();
														((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).loadGridView();


													}
												});
												builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

													@Override
													public void onClick(DialogInterface dialog, int which) {
														dialog.cancel();
													}
												});
												AlertDialog alert = builder.create();
												alert.show();
												return;
											}else{
												UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.please_download_book_store, R.string.download_book);
												return;
											}
										}
									} catch (JSONException e) {
										e.printStackTrace();
									}
									if (cloudEnrselected.equals("false")) {
										map.put(webService.KEY_CLOUDENRSELECTED, "true");
										selectedCloudEnrList.add(map);
									} else {
										map.put(webService.KEY_CLOUDENRSELECTED, "false");
										selectedCloudEnrList.remove(map);
									}
									((BaseAdapter) parent.getAdapter()).notifyDataSetChanged();
								}
							});
						}
					});

					btnEnrichment.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
						if(!progressBar.isShown()){
							enrichmentClicked = true;
							resourcesClicked = false;
							txtHeadTitle.setText(getResources().getString(R.string.downloadEnrich));
							viewPager.setCurrentItem(2, true);
							final UsersEnrichmentResourceAdapter userEnrResAdapter = new UsersEnrichmentResourceAdapter(webService.enrichmentList, null);
							listViewEnrRes.setAdapter(userEnrResAdapter);
							listViewEnrRes.setOnItemClickListener(new AdapterView.OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> adapterView, View view,
														int position, long arg3) {
									//this.bookListArray = db.getAllBookContent(this,"");
									Enrichments enrichment = userEnrResAdapter.filteredEnrItemList.get(position);
									boolean bookExist = false;
									for (int i = 0; i < ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray.size(); i++) {
										Book book = (Book) ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray.get(i);
										if (!(book.get_bStoreID() == null)) {
											int bookId = 0;
											String[] bookIdSplit = new String[0];
											if (book.get_bStoreID().contains("M")) {
												bookIdSplit = book.get_bStoreID().split("M");
												bookId = Integer.parseInt(bookIdSplit[1]);
											} else if (book.get_bStoreID().contains("Q")) {
												bookIdSplit = book.get_bStoreID().split("Q");
												bookId = Integer.parseInt(bookIdSplit[1]);
											} else if (book.get_bStoreID().contains("E")) {
												bookIdSplit = book.get_bStoreID().split("E");
												bookId = Integer.parseInt(bookIdSplit[1]);
											} else if (book.get_bStoreID().contains("R")) {
												bookIdSplit = book.get_bStoreID().split("R");
												bookId = Integer.parseInt(bookIdSplit[1]);
											}
											if (bookId == enrichment.getEnrichmentBid()) {
												enrichment.setEnrichmentStoreId(bookIdSplit[0]);
												enrichment.setDbBookId(book.getBookID());
												bookExist = true;
												break;
											}
										}
									}
									if (bookExist) {
										viewPager.setCurrentItem(3, true);
										userSeleEnrresAdapter = new UsersSelectEnrResAdapter(enrichment, null);
										listViewSelectEnrRes.setAdapter(userSeleEnrresAdapter);
										listViewSelectEnrRes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
											@Override
											public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
												if (!deleteMode) {
													deleteMode = true;
													view.setBackgroundColor(Color.parseColor("#C51162"));
													btnDownload.setText(R.string.delete);
												}else{
													deleteMode = false;
													btnDownload.setText(R.string.download);
													for (int i = 0; i < userSeleEnrresAdapter.enrichList.size(); i++) {
														Enrichments enrichments = userSeleEnrresAdapter.enrichList.get(i);
														enrichments.setEnrichmentSelected(false);
													}
													delEnrList.clear();
													userSeleEnrresAdapter.notifyDataSetChanged();
												}
												return false;
											}
										});
										listViewSelectEnrRes.setOnItemClickListener(new AdapterView.OnItemClickListener() {

											@Override
											public void onItemClick(AdapterView<?> adapterView, View view,
																	int position, long arg3) {
												Enrichments enrichment = userSeleEnrresAdapter.enrichList.get(position);
												if (!deleteMode) {
													if (enrichment.isEnrichmentSelected()) {
														enrichment.setEnrichmentSelected(false);
														selectedEnrichments.remove(enrichment);
													} else {
														enrichment.setEnrichmentSelected(true);
														if (!enrichment.isDownloaded()) {
															selectedEnrichments.add(enrichment);
														}
													}
												}else{
													if (enrichment.isEnrichmentSelected()) {
														enrichment.setEnrichmentSelected(false);
														delEnrList.remove(enrichment);
													} else {
														enrichment.setEnrichmentSelected(true);
														delEnrList.add(enrichment);
													}

												}

											  ((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
											}
										});
									} else {
										UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.please_download_book_store, R.string.download_book);
									}
								}

							});
						}
						}
					});
					txtEnrichment = (TextView) view.findViewById(R.id.txtEnrichment);
					btnResources = (Button) view.findViewById(R.id.btnResources);
					btnResources.setEnabled(false);
					btnResources.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if(!progressBar.isShown()) {
								enrichmentClicked = false;
								resourcesClicked = true;
								txtHeadTitle.setText(getResources().getString(R.string.downloadResource));
								viewPager.setCurrentItem(2, true);
								final UsersEnrichmentResourceAdapter userEnrResAdapter = new UsersEnrichmentResourceAdapter(null, webService.resourceList);
								listViewEnrRes.setAdapter(userEnrResAdapter);
								listViewEnrRes.setOnItemClickListener(new AdapterView.OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> adapterView, View view,
														int position, long arg3) {
									Resources resource = userEnrResAdapter.filteredResItemList.get(position);
									boolean bookExist = false;
									for (int i = 0; i < ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray.size(); i++) {
										Book book = (Book)((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray.get(i);
										if (!(book.get_bStoreID() == null)) {
											int bookId = 0;
											String[] bookIdSplit = new String[0];
											if(book.get_bStoreID().contains("M")){
												bookIdSplit = book.get_bStoreID().split("M");
												bookId = Integer.parseInt(bookIdSplit[1]);
											}else if(book.get_bStoreID().contains("Q")){
												bookIdSplit = book.get_bStoreID().split("Q");
												bookId = Integer.parseInt(bookIdSplit[1]);
											}else if(book.get_bStoreID().contains("E")){
												bookIdSplit = book.get_bStoreID().split("E");
												bookId = Integer.parseInt(bookIdSplit[1]);
											}else if(book.get_bStoreID().contains("R")){
												bookIdSplit = book.get_bStoreID().split("R");
												bookId = Integer.parseInt(bookIdSplit[1]);
											}
											if (bookId == resource.getResourceBid()) {
												resource.setResourceStoreId(bookIdSplit[0]);
												bookExist = true;
												break;
											}
										}
									}
									if (bookExist) {
										viewPager.setCurrentItem(3, true);
										userSeleEnrresAdapter = new UsersSelectEnrResAdapter(null, resource);
										listViewSelectEnrRes.setAdapter(userSeleEnrresAdapter);
										listViewSelectEnrRes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
											@Override
											public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
												if (!deleteMode) {
													deleteMode = true;
													refreshingDeleteMode();
													view.setBackgroundColor(Color.parseColor("#C51162"));
													btnDownload.setText(R.string.delete);
												}else{
													deleteMode = false;
													btnDownload.setText(R.string.download);
													for (int i = 0; i < userSeleEnrresAdapter.resList.size(); i++) {
														Resources resource = userSeleEnrresAdapter.resList.get(i);
														resource.setResourceSelected(false);
													}
													delResourcesList.clear();
													userSeleEnrresAdapter.notifyDataSetChanged();
												}
												return false;
											}
										});
										listViewSelectEnrRes.setOnItemClickListener(new AdapterView.OnItemClickListener() {

											@Override
											public void onItemClick(AdapterView<?> adapterView, View view,
																	int position, long arg3) {
												Resources resource = userSeleEnrresAdapter.resList.get(position);
												if (!deleteMode) {
													if (resource.isResourceSelected()) {
														resource.setResourceSelected(false);
														selectedResources.remove(resource);
													} else {
														resource.setResourceSelected(true);
														if (!resource.isResourceDownloaded()) {
															selectedResources.add(resource);
														}
													}
												}else{
													if (resource.isResourceSelected()) {
														resource.setResourceSelected(false);
														delResourcesList.remove(resource);
													} else {
														resource.setResourceSelected(true);
														delResourcesList.add(resource);
													}
												}
												((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
											}
										});
									} else {
										UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.please_download_book_store, R.string.download_book);
									}
								}
								});
							}
						}
					});
					txtResources = (TextView) view.findViewById(R.id.txtResources);
					btnGroups.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							/*viewPager.setCurrentItem(6, true);

							if(groups==null){

								groups = db.getMyGroups(scopeId, SlideMenuWithActivityGroup.this);
								User_groups = db.getMyGroups(scopeId, SlideMenuWithActivityGroup.this);
							}
							listViewGroup.setAdapter(new groupListAdapter());
							listViewGroup.invalidateViews();  */

							Intent intent = new Intent(SlideMenuWithActivityGroup.this, ExportEnrGroupActivity.class);
							//intent.putExtra("Book", currentBook);
							intent.putExtra("Shelf", ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf);
							app.setState(true);
							ManahijApp.getInstance().getPrefManager().addIsinBackground(true);
							startActivityForResult(intent,CONNECT_CALL_ESTABLISHMENT);
							//startActivity(intent);


						}
					});

					btnSharedUsers.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							viewPager.setCurrentItem(8, true);
						}
					});

					btnlogout = (Button) view.findViewById(R.id.btnLogout);
					Button lang_btn = (Button) view.findViewById(R.id.lang_btn);
					lang_btn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
                                 DisplayLanguageAlertDialog(SlideMenuWithActivityGroup.this, R.string.CHANGE_LANGUAGE_MESSAGE, R.string.CHANGE_LANGUAGE_TITLE);
						}
					});
					fbLogoutBtn = (LoginButton) view.findViewById(R.id.fbLogoutButton);
					fbLogoutBtn.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);

					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SlideMenuWithActivityGroup.this);
					final int scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
					if (scopeType != 0) {
						btnLoginLayout.setVisibility(View.GONE);
						loggedInSuccessLayout.setVisibility(View.VISIBLE);
						usertxtView.setText(userName);
						if (mIcon11 != null){
							userImagView.setImageBitmap(mIcon11);
						 }

						if (scopeType == Globals.scopeTypeFacebook) {
							btnlogout.setVisibility(View.GONE);
							fbLogoutBtn.setVisibility(View.VISIBLE);
							if (mIcon11 == null && fbPhotoUrl != null){
								Glide.with(SlideMenuWithActivityGroup.this).load(fbPhotoUrl)
										.thumbnail(0.5f)
										.crossFade()
										.diskCacheStrategy(DiskCacheStrategy.ALL)
										.override(200, 200)
										.into(loginAdapter.userImagView);
								//new LoadProfileImage(userImagView).execute(fbPhotoUrl);
							}
						} else if (scopeType == Globals.scopeTypeGoogle) {
							String photoUrl = prefs.getString(Globals.sUserEmailPhotoUrl, "");
							if (!photoUrl.equals("")) {
								Glide.with(SlideMenuWithActivityGroup.this).load(photoUrl)
										.thumbnail(0.5f)
										.crossFade()
										.diskCacheStrategy(DiskCacheStrategy.ALL)
										.override(200, 200)
										.into(loginAdapter.userImagView);
							//	new LoadProfileImage(userImagView).execute(photoUrl);
							}
						}

						if (Globals.ISGDRIVEMODE()) {
							new loadCloudEnrichmentList().execute();
						} else {
							new loadEnrichmentAndResourceByUserId().execute();
						}
					} else {
						btnLoginLayout.setVisibility(View.VISIBLE);
						loggedInSuccessLayout.setVisibility(View.GONE);
					}

					btnlogout.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							logout();
						}
					});

					googleFbProgressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
					googleFbProgressBar.setVisibility(View.GONE);

					final EditText userName = (EditText) view.findViewById(R.id.editText3);
					final EditText passWord = (EditText) view.findViewById(R.id.editText4);
					final ProgressBar progressBar2 = (ProgressBar) view.findViewById(R.id.progressBar2);

					Button btnSignIn = (Button) view.findViewById(R.id.button7);
					btnSignIn.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if (userName.getText().toString().isEmpty() || passWord.getText().toString().isEmpty()) {
								UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.forget_username_password, R.string.login_details);
							} else {
								if (UserFunctions.isInternetExist(SlideMenuWithActivityGroup.this)) {
									new SignIn(userName.getText().toString(), passWord.getText().toString(), Globals.scopeTypeSemanoor, progressBar2, "").execute();
								} else {
									UserFunctions.DisplayAlertDialogNotFromStringsXML(SlideMenuWithActivityGroup.this, SlideMenuWithActivityGroup.this.getResources().getString(R.string.check_internet_connectivity), SlideMenuWithActivityGroup.this.getResources().getString(R.string.connection_error));
								}
							}
						}
					});

					Button signUP = (Button) view.findViewById(R.id.button8);
					signUP.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							viewPager.setCurrentItem(5, true);
						}
					});

					Button btnGoogleLogin = (Button) view.findViewById(R.id.btn_gogle_sign_in);
					btnGoogleLogin.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							googleSignInClicked = true;
							signInWithGLogin();
						}
					});

					LoginButton authButton = (LoginButton) view.findViewById(R.id.fbAuthButton);
					authButton.setBackgroundResource(R.drawable.fb_login);
					authButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
					authButton.setText("");
					authButton.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
					authButton.setReadPermissions(Arrays.asList("email"));
					authButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							facebookSignInClicked = true;

						}
					});

					break;

				case 2:
					view = inflater.inflate(R.layout.users_enrichment_res, null);
					Button btnBackk = (Button) view.findViewById(R.id.btnBack);
					btnBackk.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
                            if(userSeleEnrresAdapter!=null &&loginAdapter.unreadEnrichments.size() > 0 ||userSeleEnrresAdapter!=null &&loginAdapter.unreadResources.size() > 0) {
								generatingJsonData(enrichmentClicked, resourcesClicked);
								userSeleEnrresAdapter=null;
							}
							viewPager.setCurrentItem(1, true);
						}
					});

					txtHeadTitle = (TextView) view.findViewById(R.id.textView1);

					listViewEnrRes = (ListView) view.findViewById(R.id.listView1);
					break;

				case 3:
					view = inflater.inflate(R.layout.select_users_enr_res, null);
					Button btnBackkk = (Button) view.findViewById(R.id.btnBack);
					btnBackkk.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if (Globals.ISGDRIVEMODE()) {
								viewPager.setCurrentItem(1, false);
							} else {
								if (deleteMode) {
									deleteMode = false;
									delEnrList.clear();
									delResourcesList.clear();
									btnDownload.setText(R.string.download);
									refreshingDeleteMode();
									listViewSelectEnrRes.invalidateViews();
								}
								selectedEnrichments.clear();
								selectedResources.clear();
								if (enrichmentClicked) {
									for (int i = 0; i < userSeleEnrresAdapter.enrichList.size(); i++) {
										Enrichments enrichment = userSeleEnrresAdapter.enrichList.get(i);
										enrichment.setEnrichmentSelected(false);
									}
								}else{
									for (int i = 0; i < userSeleEnrresAdapter.resList.size(); i++) {
										Resources resource = userSeleEnrresAdapter.resList.get(i);
										resource.setResourceSelected(false);
									}
								}
								viewPager.setCurrentItem(2, true);
							}
						}
					});
					Button btnSelectAll = (Button) view.findViewById(R.id.btnSelectAll);
					if (Globals.ISGDRIVEMODE()) {
						btnSelectAll.setVisibility(View.GONE);
					}
					btnSelectAll.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if (((Button) v).getText().equals(getResources().getString(R.string.selectall))) {
								((Button) v).setText(getResources().getString(R.string.select_none));
								if (enrichmentClicked) {
									for (int i = 0; i < userSeleEnrresAdapter.enrichList.size(); i++) {
										Enrichments enrichment = userSeleEnrresAdapter.enrichList.get(i);
										enrichment.setEnrichmentSelected(true);
										if (deleteMode){
											delEnrList.add(enrichment);
										}else {
											if (!enrichment.isDownloaded()) {
												selectedEnrichments.add(enrichment);
											}
										}
									}
									userSeleEnrresAdapter.notifyDataSetChanged();
								} else {
									for (int i = 0; i < userSeleEnrresAdapter.resList.size(); i++) {
										Resources resource = userSeleEnrresAdapter.resList.get(i);
										resource.setResourceSelected(true);
										if (deleteMode){
											delResourcesList.add(resource);
										}else{
											if (!resource.isResourceDownloaded());{
												selectedResources.add(resource);
											}
										}
									}
									userSeleEnrresAdapter.notifyDataSetChanged();
								}
							} else {
								((Button) v).setText(getResources().getString(R.string.selectall));
								if (enrichmentClicked) {
									for (int i = 0; i < userSeleEnrresAdapter.enrichList.size(); i++) {
										Enrichments enrichment = userSeleEnrresAdapter.enrichList.get(i);
										enrichment.setEnrichmentSelected(false);
										if (deleteMode){
											delEnrList.remove(enrichment);
										}else {
											selectedEnrichments.remove(enrichment);
										}
									}
									userSeleEnrresAdapter.notifyDataSetChanged();
								} else {
									for (int i = 0; i < userSeleEnrresAdapter.resList.size(); i++) {
										Resources resource = userSeleEnrresAdapter.resList.get(i);
										resource.setResourceSelected(false);
										if (deleteMode){
											delResourcesList.remove(resource);
										}else {
											selectedResources.remove(resource);
										}
									}
									userSeleEnrresAdapter.notifyDataSetChanged();
								}
							}
						}
					});
					btnDownload = (Button) view.findViewById(R.id.btnDownload);
					if (deleteMode){
						btnDownload.setText(R.string.delete);
					}else{
						btnDownload.setText(R.string.download);
					}
					btnDownload.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if (Globals.ISGDRIVEMODE()) {
								if (selectedCloudEnrList.size() > 0) {
										for (int i = 0; i < selectedCloudEnrList.size(); i++) {
										HashMap<String, String> map = selectedCloudEnrList.get(i);
										String jsonContent = map.get(webService.KEY_CECONTENT);
										final String message = map.get(webService.KEY_CEMESSAGE);
										try {
											final JSONObject jsonObj = new JSONObject(jsonContent);
											String type = jsonObj.getString("type");

											if (type.equals("Enrichment")) {
												GDriveExport gdrive=new GDriveExport(SlideMenuWithActivityGroup.this,message);
												ProgressDialog syncProgressDialog = ProgressDialog.show(SlideMenuWithActivityGroup.this, "", getResources().getString(R.string.syncing_please_wait), true);
                                                ArrayList<Object> bookList = ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray;
												ProgressDialog downloadProgDialog = new ProgressDialog(SlideMenuWithActivityGroup.this);
												new GDriveExport.EnrichmentDownloadFromGDrive(SlideMenuWithActivityGroup.this, downloadProgDialog, message, jsonObj, bookList, selectedCloudEnrList, i, webService.cloudEnrList, loginAdapter, syncProgressDialog).execute();
											}else if(type.equals("sharedBook")) {
												final int position=i;
												final GDriveExport gdrive=new GDriveExport(((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()),null);
												final String downloadFromPath = jsonObj.getString("downloadPath");
												final String bookDetails = jsonObj.getString("bookDetails");

												gdrive.saveBookDetails(bookDetails,downloadFromPath,jsonContent,selectedCloudEnrList, type);
												((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).loadGridView();
												String enrID = map.get(WebService.KEY_CENRICHMENTS);
												//DatabaseHandler db = DatabaseHandler.getInstance(ctx);
												db.executeQuery("insert into CloudObjects(objID)values('" + enrID + "')");
												//HashMap<String, String> selectedMap = selectedCloudEnrList.remove(i);
												webService.cloudEnrList.remove(map);
                                         	}
											else {
												ProgressDialog syncProgressDialog = ProgressDialog.show(SlideMenuWithActivityGroup.this, "", getResources().getString(R.string.syncing_please_wait), true);
                                                ArrayList<Object> bookList = ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray;
												String storeBookId = jsonObj.getString("IDBook");
												String contentXML = jsonObj.getString("ContentXMl");
												Resources resource = new Resources(SlideMenuWithActivityGroup.this);
												resource.setResourceId(10);
												resource.new getResourceXmlFromUrl(db, webService, bookList, contentXML, storeBookId, selectedCloudEnrList, i, webService.cloudEnrList, loginAdapter, syncProgressDialog).execute();
											}
										} catch (JSONException e) {
											e.printStackTrace();
										}
									}
									mSlideMenu.setCurrentOffset(0);

								}
							} else {
								if (!deleteMode){
									if (enrichmentClicked) {
										if (selectedEnrichments.size() > 0) {
											ProgressDialog downloadProgDialog = new ProgressDialog(SlideMenuWithActivityGroup.this);
											for (int i = 0; i < selectedEnrichments.size(); i++) {
												Enrichments enrichment = selectedEnrichments.get(i);
												enrichment.new EnrichmentDownloadAndUpdate(downloadProgDialog, userSeleEnrresAdapter.enrichList, selectedEnrichments, enrichment).execute();

												if (!db.sharedUsersList(scopeId, enrichment.getEnrichmentUserName())) {
													db.executeQuery("insert into sharedUsers(LoginID, UserName, Email, Blacklist,SharedType)values('" + scopeId + "','" + enrichment.getEnrichmentUserName() + "','" + enrichment.getEnrichmentEmailId() + "', 'no','Enrichment')");
												}
											}
											//socialPopoverView.dissmissPopover(true);
											mSlideMenu.setCurrentOffset(0);
										}
									} else {
										if (selectedResources.size() > 0) {
											ProgressDialog resourceProgressDialog = ProgressDialog.show(SlideMenuWithActivityGroup.this, "", getResources().getString(R.string.syncing_resource), true);
											boolean dismissProgressDialog = false;
											for (int i = 0; i < selectedResources.size(); i++) {
												if (i == selectedResources.size() - 1) {
													dismissProgressDialog = true;
												}
												Resources resource = selectedResources.get(i);
												resource.new getResourceXmlFromUrl(db, webService, ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray, resourceProgressDialog, 0, null, dismissProgressDialog).execute();
												//resource.getResourceXmlFromUrl(db, webService, gridShelf.bookListArray, 0);

												if (!db.sharedUsersList(scopeId, resource.getResourceUserName())) {
													db.executeQuery("insert into sharedUsers(LoginID, UserName, Email, Blacklist,SharedType)values('" + scopeId + "','" + resource.getResourceUserName() + "','" + resource.getResourceEmailId() + "', 'no','Resource')");
												}

											}
											//resourceProgressDialog.dismiss();
											userSeleEnrresAdapter.resList.clear();
											selectedResources.clear();
											//socialPopoverView.dissmissPopover(true);
											mSlideMenu.setCurrentOffset(0);
										}
									}
								}else {
									if (delEnrList.size() > 0 || delResourcesList.size()>0) {
										final Dialog deleteDialog = new Dialog(SlideMenuWithActivityGroup.this);
										deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(SlideMenuWithActivityGroup.this.getResources().getColor(R.color.semi_transparent)));
										deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
										deleteDialog.setContentView(SlideMenuWithActivityGroup.this.getLayoutInflater().inflate(R.layout.delete_enrichments_view, null));
										deleteDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.6), LinearLayout.LayoutParams.WRAP_CONTENT);
										Button delete_local = (Button) deleteDialog.findViewById(R.id.del_local_btn);
										delete_local.setOnClickListener(new View.OnClickListener() {
											@Override
											public void onClick(View v) {
												if (delEnrList.size()>0) {
													deleteEnrichmentsLocally(delEnrList);
													deleteDialog.dismiss();
													deleteMode = false;
													btnDownload.setText(R.string.download);
													delEnrList.clear();
													mSlideMenu.setCurrentOffset(0);
												}
											}
										});
										Button cancel_btn = (Button) deleteDialog.findViewById(R.id.cancel_btn);
										cancel_btn.setOnClickListener(new View.OnClickListener() {
											@Override
											public void onClick(View v) {
												deleteDialog.cancel();
											}
										});
										Button delete_permanent = (Button) deleteDialog.findViewById(R.id.del_permanent_btn);
										delete_permanent.setOnClickListener(new View.OnClickListener() {
											@Override
											public void onClick(View v) {
												if (delEnrList.size() > 0) {
													deleteEnrichmentsLocally(delEnrList);
												}
												String json = null;
												try {
													if (delEnrList.size()>0) {
														json = jsonString(delEnrList, null);
													}else{
														json = jsonString(null, delResourcesList);
													}
												} catch (JSONException e) {
													e.printStackTrace();
												}
												deleteDialog.dismiss();
												deleteMode = false;
												btnDownload.setText(R.string.download);
												mSlideMenu.setCurrentOffset(0);
												new deleteEnrichments(json, scopeId).execute();
												delEnrList.clear();
											}
										});
										deleteDialog.show();
									}
								}
							}
						}
					});
					listViewSelectEnrRes = (ListView) view.findViewById(R.id.listView1);

					break;

				case 4:
					view = inflater.inflate(R.layout.sem_login, null);
					Button btn=(Button)view. findViewById(R.id.btnBack);
					btn.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							viewPager.setCurrentItem(1, true);
						}
					});
					final EditText etUsername = (EditText) view.findViewById(R.id.et_userName);
					final EditText etPassword = (EditText) view.findViewById(R.id.et_password);
					final ProgressBar progressBar1 = (ProgressBar) view.findViewById(R.id.progressBar1);

					Button btnsignIn = (Button) view.findViewById(R.id.btnSigin);
					btnsignIn.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if (etUsername.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty()) {
								UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.forget_username_password, R.string.login_details);
							} else {
								if (UserFunctions.isInternetExist(SlideMenuWithActivityGroup.this)) {
									new SignIn(etUsername.getText().toString(), etPassword.getText().toString(), Globals.scopeTypeSemanoor, progressBar1, "").execute();
								} else {
									UserFunctions.DisplayAlertDialogNotFromStringsXML(SlideMenuWithActivityGroup.this, SlideMenuWithActivityGroup.this.getResources().getString(R.string.check_internet_connectivity), SlideMenuWithActivityGroup.this.getResources().getString(R.string.connection_error));
								}
							}
						}
					});

					Button btnSignUp = (Button) view.findViewById(R.id.btnSignUp);
					btnSignUp.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							viewPager.setCurrentItem(5, true);
						}
					});
					break;

				case 5:
					view = inflater.inflate(R.layout.sem_signup, null);
					final EditText etSignUpUsername = (EditText) view.findViewById(R.id.et_username);
					final EditText etSignupFullname = (EditText) view.findViewById(R.id.et_fullname);
					final EditText etSignUpEmail = (EditText) view.findViewById(R.id.et_email);
					final EditText etSignUpPassword = (EditText) view.findViewById(R.id.et_password);
					final EditText etSignUpRetypePwd = (EditText) view.findViewById(R.id.et_retypePassword);
					final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

					Button btnBack = (Button) view.findViewById(R.id.btnBack);
					btnBack.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							viewPager.setCurrentItem(1, true);
						}
					});

					Button btnSignUpp = (Button) view.findViewById(R.id.btnSignup);
					btnSignUpp.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if(etSignUpUsername.getText().toString().length()<2 || etSignUpUsername.getText().toString()==null || etSignUpUsername.getText().toString().length()>60) {
								UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.input_valid_user_name, R.string.invalid_userName);
							} else if (etSignupFullname.getText().toString().length()<2 || etSignupFullname.getText().toString()==null || etSignupFullname.getText().toString().length()>60) {
								UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.input_valid_fullname, R.string.invalid_fullname);
							} else if (etSignUpPassword.getText().toString().length()<2 || etSignUpPassword.getText().toString()==null || etSignUpPassword.getText().toString().length()>40) {
								UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.input_valid_password, R.string.invalid_password);
							} else if (!UserFunctions.CheckValidEmail(etSignUpEmail.getText().toString())) {
								UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.input_valid_mail_id, R.string.invalid_email_id);
							} else if (!etSignUpPassword.getText().toString().equals(etSignUpRetypePwd.getText().toString())) {
								UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.password_doesnt_match, R.string.check_password);
							} else if (!UserFunctions.isInternetExist(SlideMenuWithActivityGroup.this)) {
								UserFunctions.DisplayAlertDialogNotFromStringsXML(SlideMenuWithActivityGroup.this, SlideMenuWithActivityGroup.this.getResources().getString(R.string.check_internet_connectivity), SlideMenuWithActivityGroup.this.getResources().getString(R.string.connection_error));
							} else {
								ProgressDialog progressDialog = ProgressDialog.show(SlideMenuWithActivityGroup.this, "", getResources().getString(R.string.please_wait), true);
								new SignUp(etSignUpUsername.getText().toString(), etSignUpPassword.getText().toString(), etSignupFullname.getText().toString(), etSignUpEmail.getText().toString(), Globals.scopeTypeSemanoor, progressBar, progressDialog).execute();
							}
						}
					});
					break;
				case 6:
					view = inflater.inflate(R.layout.select_users_enr_groups, null);
					btn_edit = (Button) view.findViewById(R.id.btn_edit);
					Button btnDelete = (Button) view.findViewById(R.id.btndelete);
					listViewGroup = (ListView) view.findViewById(R.id.listView1);
					Button btnBack1 = (Button) view.findViewById(R.id.btnBack);
					// listViewGroup.setAdapter(new searchListAdapter(MainActivity.this,groups));

					btnBack1.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							//if(Globals.Gdrive){
							//	viewPager.setCurrentItem(9, true);
						//	}else {
							if (!Globals.ISGDRIVEMODE()) {
								if (grp_delete) {
									new SavingGroupDetails().execute();
								}
							}
							  // generatingJsonData();
								viewPager.setCurrentItem(1, true);
						//	}
						}
					});
					Button btnGoup = (Button) view.findViewById(R.id.btnGroup);
					btnGoup.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View arg0) {
							showGroupDialog();

						}
					});
					final Button btn_delete = (Button) view.findViewById(R.id.btndelete);
					btn_delete.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View arg0) {
							if(deletemode){
								deletemode=false;
								btn_delete.setText(getResources().getString(R.string.delete));
							}else{
								deletemode=true;
							}
							listViewGroup.invalidateViews();
						}
					});
					btn_edit.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							if(!grpInEditMode){
								grpInEditMode = true;
								btn_edit.setText(getResources().getString(R.string.done));
								listViewGroup.invalidateViews();
								exist=false;
							}else{
								grpInEditMode = false;
								textChecking=true;
								listViewGroup.invalidateViews();
							}
						}
					});
					listViewGroup.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> adapterView, View view,
												int position, long arg3) {
							UserGroups group = groups.get(position);
							group_details= db.getselectedgroupdetails(group.getGroupName(),group.getUserScopeId(),SlideMenuWithActivityGroup.this);
							groupname =group.getGroupName();
							listViewEmailGroup.setAdapter(new groupEmailAdapter(SlideMenuWithActivityGroup.this,group_details));
							viewPager.setCurrentItem(7, true);
							listViewEmailGroup.invalidateViews();

						}
					});
					break;
				case 7:
					view = inflater.inflate(R.layout.select_users_enr_groups, null);
					Button btnDelete1 = (Button) view.findViewById(R.id.btndelete);
					listViewEmailGroup = (ListView) view.findViewById(R.id.listView1);
					Button btnBack2 = (Button) view.findViewById(R.id.btnBack);
					Button btn_edit1 = (Button) view.findViewById(R.id.btn_edit);
					btn_edit1.setVisibility(View.INVISIBLE);
					btnBack2.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							viewPager.setCurrentItem(6, true);
							//System.out.println(group_details.size());
							if(group_details.size()==0){
								db.executeQuery("delete from userGroups where groupName='"+groupname+"' and userID='"+scopeId+"'");
								for(int i=0;i<groups.size();i++){
									UserGroups del_group=groups.get(i);
									if(del_group.getGroupName()==groupname){
										groups.remove(del_group);
									}
								}
							}
							deletemode=false;
							//listViewGroup.invalidateViews();
						}
					});
					Button btn_email = (Button) view.findViewById(R.id.btnGroup);
					btn_email.setText(getResources().getString(R.string.email_id));
					btn_email.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View arg0) {
							showemailsDialog();

						}
					});
					Button btn_delete1 = (Button) view.findViewById(R.id.btndelete);
					btn_delete1.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View arg0) {
							if(deletemode){
								deletemode=false;
							}else{
								deletemode=true;

							}
							listViewEmailGroup.invalidateViews();
						}
					});
					listViewEmailGroup.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> adapterView, View view,
												int position, long arg3) {
							UserGroups usrgroups=group_details.get(position);
							editingMemberDetails(usrgroups,position);
						}
					});
					break;

				case 8:
					view = inflater.inflate(R.layout.shared_users, null);
					shared_listView = (ListView) view.findViewById(R.id.listView1);
					ArrayList<SharedUsers> sharedList = db.getCountForSharedList(scopeId, SlideMenuWithActivityGroup.this);
					final SharedUsersAdapter adapter = new SharedUsersAdapter(sharedList, SlideMenuWithActivityGroup.this);
					shared_listView.setAdapter(adapter);
					adapter.notifyDataSetChanged();

					Button btnBack5 = (Button) view.findViewById(R.id.btnBack);
					btnBack5.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {

							viewPager.setCurrentItem(1, true);
						}
					});
					shared_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> adapterView, View view,
												int position, long arg3) {
							if(position==0){

							}
						}
					});
					break;
				case 9:
					view = inflater.inflate(R.layout.gdrive, null);
					ImageView userImagView = (ImageView) view.findViewById(R.id.userImgView);
					TextView usertxtView = (TextView) view.findViewById(R.id.txtUserName);
					ProgressBar progress = (ProgressBar) view.findViewById(R.id.progressBar2);
					Button btn_cloud_sharing = (Button) view.findViewById(R.id.btn_cloud_sharing);
					Button btnGroups = (Button) view.findViewById(R.id.btnGroups);
					Button btn_lang1 = (Button) view.findViewById(R.id.lang_btn);
					btnGroups.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							viewPager.setCurrentItem(6, true);
							SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SlideMenuWithActivityGroup.this);
							int scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
							if (scopeType != 0) {
							   groups=db.getMyGroups(scopeId,SlideMenuWithActivityGroup.this);
							   listViewGroup.setAdapter(new groupListAdapter());
							   listViewGroup.invalidateViews();
							}

						}
					});
					btn_cloud_sharing.setOnClickListener(new View.OnClickListener() {
					@Override
					 public void onClick(View v) {

					  }
				    });
					btn_lang1.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							DisplayLanguageAlertDialog(SlideMenuWithActivityGroup.this, R.string.CHANGE_LANGUAGE_MESSAGE, R.string.CHANGE_LANGUAGE_TITLE);

						}
					});
					break;
				default:
					break;
			}

			((ViewPager)container).addView(view, 0);
			return view;

		}
		@Override
		public void destroyItem(ViewGroup container, int position, Object object){
			((ViewPager)container).removeView((View) object);
		}
		@Override
		public void setPrimaryItem(ViewGroup container, int position,
								   Object object) {

		}
	}
    private void refreshingDeleteMode(){
		if(loginAdapter.resourcesClicked){
			for(Resources res:loginAdapter.userSeleEnrresAdapter.resList){
				if(res.isResourceSelected()){
					res.setResourceSelected(false);
				}
			}
		}if(loginAdapter.enrichmentClicked){
			for(Enrichments enrich:loginAdapter.userSeleEnrresAdapter.enrichList){
				if(enrich.isEnrichmentSelected()){
					enrich.setEnrichmentSelected(false);
				}

			}
		}
	}
	private void generatingJsonData(boolean enr,boolean res){
		String path=Globals.TARGET_BASE_FILE_PATH+scopeId+".txt";
		File f=new File(path);
		JSONObject jsonData = null;
		ArrayList<String>Enrichments = new ArrayList<String>();
		ArrayList<String>resources = new ArrayList<String>();
		JSONArray enrichJsonArray = null;
		JSONArray resourceJsonArray = null;
		if(f.exists()) {
			String sourceString = UserFunctions.readFileFromPath(f);
			if (sourceString != null) {
				//JSONArray enrichJsonArray = new JSONArray();

				try {
					jsonData = new JSONObject(sourceString);
					String enrichIds = jsonData.getString("enrichId");
					String userId = jsonData.getString("userID");
					if (userId.equals(scopeId)) {
						String resourceId = jsonData.getString("resourceId");
						enrichJsonArray = new JSONArray(enrichIds);
						for (int i = 0; i < enrichJsonArray.length(); i++) {
							JSONObject json = enrichJsonArray.getJSONObject(i);
							String id = json.getString("ID");
							Enrichments.add(id);
						}
						resourceJsonArray = new JSONArray(resourceId);
						for (int i = 0; i < resourceJsonArray.length(); i++) {
							JSONObject json = resourceJsonArray.getJSONObject(i);
							String id = json.getString("ID");
							resources.add(id);
						}
					}

					if (enr) {
						if (loginAdapter.unreadEnrichments.size() > 0) {
							boolean exist = false;
							// for (String enrichId : Enrichments) {
							for (Enrichments enrich : loginAdapter.unreadEnrichments) {
								//userScopeId=user.getUserScopeId();
								for (String enrichId1 : Enrichments) {
									if (!enrichId1.equals("" + enrich.getEnrichmentId())) {
										exist = false;
									} else {
										exist = true;
										break;
									}
								}
								if (!exist) {
									JSONObject data = new JSONObject();
									data.put("ID", "" + enrich.getEnrichmentId());
									enrichJsonArray.put(data);
									// }
								}
							}
							loginAdapter.unreadEnrichments.clear();

                        }
					} else if (res) {
						if (loginAdapter.unreadResources.size() > 0) {
							for (Resources resource : loginAdapter.unreadResources) {
								for (String resourceId : resources) {
									if (!resourceId.equals("" + resource.getResourceId())) {
										exist = false;
									} else {
										exist = true;
										break;
									}
								}
								if (!exist) {
									JSONObject data = new JSONObject();
									data.put("ID", "" + resource.getResourceId());
									resourceJsonArray.put(data);
								}
								//}
							}
							loginAdapter.unreadResources.clear();
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}else {
				jsonData = new JSONObject();
				enrichJsonArray = new JSONArray();
				resourceJsonArray = new JSONArray();
				try {
					if(enr) {
						for (Enrichments enrich : loginAdapter.unreadEnrichments) {
							JSONObject data = new JSONObject();
							data.put("ID", "" + enrich.getEnrichmentId());
						    enrichJsonArray.put(data);
						}
						if(loginAdapter.unreadEnrichments.size()>0){
							loginAdapter.unreadEnrichments.clear();
						}
					}else if(res) {
						for (Resources resource : loginAdapter.unreadResources) {
							JSONObject resourcejsonObject = new JSONObject();
							resourcejsonObject.put("ID", resource.getResourceId());
							resourceJsonArray.put(resourcejsonObject);
						}
						if(loginAdapter.unreadResources.size()>0){
							loginAdapter.unreadResources.clear();
						}
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			 if(enrichJsonArray.length()>0 ||resourceJsonArray.length()>0) {
				  try {
					  jsonData.put("userID", scopeId);
					  jsonData.put("enrichId", enrichJsonArray);
					  jsonData.put("resourceId", resourceJsonArray);
					  FileOutputStream fos = new FileOutputStream(path);
					  fos.write(jsonData.toString().getBytes());
					  fos.close();
				  } catch (IOException e) {
					  e.printStackTrace();
				  } catch (JSONException e) {
					  e.printStackTrace();
				  }

			  }
	}
	public boolean checkStorBookExist(String storeBookID){
		ArrayList<Object> bookList = ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray;
		for (int i = 0; i<bookList.size(); i++) {
			Book book = (Book) bookList.get(i);
			if (book.is_bStoreBook()) {
				if (book.get_bStoreID().equals(storeBookID)) {
					return true;
				}
			}
		}
		return false;
	}

	public void DisplayLanguageAlertDialog(Context ctx, int msg, int title) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (language.equals("ar")) {
					saveLocale("en");
				} else {
					saveLocale("ar");
				}
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void saveLocale(String language) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Globals.languagePrefsKey, language);
		editor.commit();
	}



	protected void onStart() {    //returning back to main
		super.onStart();

	}

	protected void onStop() {    //returning back to main
		super.onStop();
		/*if (global.userCredentials != null &&viewPager!=null &&loginAdapter!=null) {
			viewPager.setCurrentItem(1);
			loginAdapter.btnLoginLayout.setVisibility(View.GONE);
			loginAdapter.loggedInSuccessLayout.setVisibility(View.VISIBLE);
			loginAdapter.usertxtView.setText(global.userCredentials.getUserName());
			if (mIcon11 != null){
				loginAdapter.userImagView.setImageBitmap(mIcon11);
			}

			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SecondaryActivity.this);
			int scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
			if (scopeType == Globals.scopeTypeFacebook) {
				loginAdapter.btnlogout.setVisibility(View.GONE);
				loginAdapter.fbLogoutBtn.setVisibility(View.VISIBLE);
				if (mIcon11 == null && fbPhotoUrl != null){
					new LoadProfileImage(loginAdapter.userImagView).execute(fbPhotoUrl);
				}
			}

			new loadEnrichmentAndResourceByUserId().execute();
		} else if(viewPager!=null &&loginAdapter!=null){
			loginAdapter.btnLoginLayout.setVisibility(View.VISIBLE);
			loginAdapter.loggedInSuccessLayout.setVisibility(View.GONE);
		}  */
	}
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
		//outState.putFloat(OFFSET_PERCENT, mOffsetPercent);
		outState.putInt(SLIDE_STATE, mSlideState);
	}
	//GooglePlusLogin

	private void signInWithGLogin() {
		Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}

	private void handleSignInResult(GoogleSignInResult result){
		if (result.isSuccess()) {
			GoogleSignInAccount acct = result.getSignInAccount();
			String personIdentifier = acct.getId();
			String personName = acct.getDisplayName();
			Uri profilePic = acct.getPhotoUrl();
			String email = acct.getEmail();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = prefs.edit();

			if (profilePic != null) {
				editor.putString(Globals.sUserEmailPhotoUrl, profilePic.toString());
			}
            editor.commit();
			Account account = acct.getAccount();
		//	new getToken(account).execute();
			//new GetContactsTask(account).execute();


			if (googleSignInClicked) {
				new SignIn(personName, personIdentifier, Globals.scopeTypeGoogle, googleFbProgressBar, email).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				googleSignInClicked = false;
			}

			if (loginAdapter != null && loginAdapter.userImagView != null && profilePic != null) {
				Glide.with(SlideMenuWithActivityGroup.this).load(profilePic.toString())
						.thumbnail(0.5f)
						.crossFade()
						.diskCacheStrategy(DiskCacheStrategy.ALL)
						.override(200, 200)
						.into(loginAdapter.userImagView);
			//	new LoadProfileImage(loginAdapter.userImagView).execute(profilePic.toString());
			}
		} else {
			Toast.makeText(getApplicationContext(),"Person information is null", Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Background Async task to load user profile picture from url
	 * */
	private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public LoadProfileImage(ImageView bmImage) {
			this.bmImage = bmImage;
		}


		protected Bitmap doInBackground(String... urls) {
			if (mIcon11 == null) {
				String urldisplay = urls[0];
				/*try {
					InputStream in = new java.net.URL(urldisplay).openStream();
					mIcon11 = BitmapFactory.decodeStream(in);
				} catch (Exception e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}*/
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
		//	bmImage.setImageBitmap(result);
		}
	}

	//FaceBook
	// Called when session changes
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
						 Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	// When session is changed, this method is called from callback method
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {

		if (state.isOpened()) {
			Request.newMeRequest(session, new Request.GraphUserCallback() {
				@Override
				public void onCompleted(GraphUser user, Response response) {
					if (user != null) {
						String userName = user.getName();
						String userId = user.getId();

						String userEmail =user.getProperty("email").toString();
						fbPhotoUrl = "https://graph.facebook.com/"+userId+"/picture?type=large";
						if (facebookSignInClicked) {
							new SignIn(userName, userId, Globals.scopeTypeFacebook, googleFbProgressBar, userEmail).execute();
							facebookSignInClicked = false;
						}

						if (loginAdapter != null && loginAdapter.userImagView != null) {
							Glide.with(SlideMenuWithActivityGroup.this).load(fbPhotoUrl)
									.thumbnail(0.5f)
									.crossFade()
									.diskCacheStrategy(DiskCacheStrategy.ALL)
									.override(200, 200)
									.into(loginAdapter.userImagView);
							//new LoadProfileImage(loginAdapter.userImagView).execute(fbPhotoUrl);
						}
					}
				}
			}).executeAsync();
		} else if (state.isClosed()) {
			logout();
		}else if (state.equals("OPENING")) {
			session.openForRead(new Session.OpenRequest(this).setPermissions(Arrays.asList("email")));
		}
	}

	private void showGroupDialog(){
		final Dialog groupDialog = new Dialog(SlideMenuWithActivityGroup.this);
		groupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.semi_transparent)));
		groupDialog.setTitle(R.string.create_group);
		groupDialog.setContentView(this.getLayoutInflater().inflate(R.layout.group_edit, null));
		groupDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, this), RelativeLayout.LayoutParams.WRAP_CONTENT);
		final EditText editText = (EditText) groupDialog.findViewById(R.id.enr_editText);
		//editText.setText(enrichmentTitle);
		Button btnSave = (Button) groupDialog.findViewById(R.id.enr_save);
		btnSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean exist=false;
				String enrTitle = editText.getText().toString();
				if(enrTitle.length()>0){

					for(UserGroups checkgroups:groups){
						String group=checkgroups.getGroupName();
						if(enrTitle.equals(group)){
							exist=true;
						}
					}
					if(exist){
						UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string. group_exist, R.string.failed_add_group);
					}else{
						db.executeQuery("insert into userGroups(userID, groupName, userName, emailID, enabled)values('"+scopeId+"', '"+enrTitle+"', '', '', '1')");
						int objUniqueId = db.getMaxUniqueRowID("userGroups") + 1;
						UserGroups group= new UserGroups(SlideMenuWithActivityGroup.this);
						group.setUserScopeId(Integer.parseInt(scopeId));
						group.setEnabled(1);
						group.setGroupName(enrTitle);
						group.setEmailId("");
						group.setUserName("");
						group.setId(objUniqueId);
						groups.add(group);
                        grp_delete=false;
						User_groups.add(group);
						changingDetailsforMailIds();
						loginAdapter.listViewGroup.invalidateViews();
						groupDialog.dismiss();
					}
				}


			}
		});

		Button btnCancel = (Button) groupDialog.findViewById(R.id.enr_cancel);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				groupDialog.dismiss();
			}
		});
		groupDialog.show();
	}

	private void showemailsDialog(){
		final Dialog groupDialog = new Dialog(SlideMenuWithActivityGroup.this);
		groupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.semi_transparent)));
		groupDialog.setTitle(getResources().getString(R.string.enter_email));
		groupDialog.setContentView(this.getLayoutInflater().inflate(R.layout.email_edit, null));
		groupDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, this), RelativeLayout.LayoutParams.WRAP_CONTENT);
		final EditText editText = (EditText) groupDialog.findViewById(R.id.enr_editText);
		final EditText email_et = (EditText) groupDialog.findViewById(R.id.editText1);
		//editText.setddText(enrichmentTitle);
		Button btnSave = (Button) groupDialog.findViewById(R.id.enr_save);
		btnSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean exist = false;
				String userName = editText.getText().toString();
				String emailId = email_et.getText().toString();
				if (!UserFunctions.CheckValidEmail(emailId)) {
					UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.input_valid_mail_id, R.string.invalid_email_id);
				}else{
					int id = db.getMaxUniqueID(groupname);
					if (id != 0) {
						db.executeQuery("update userGroups set userName='" + userName + "',emailID='" + emailId + "'where ID='" + id + "'");
						group_details.remove(0);
						UserGroups group = new UserGroups(SlideMenuWithActivityGroup.this);
						group.setUserScopeId(Integer.parseInt(scopeId));
						group.setEnabled(1);
						group.setGroupName(groupname);
						group.setEmailId(emailId);
						group.setUserName(userName);
						group_details.add(group);
						groupDialog.dismiss();
						changingDetailsforMailIds();

					} else {
						for (UserGroups checkmail : group_details) {
							String mail = checkmail.getEmailId();
							if (emailId.equals(mail)) {
								exist = true;
								break;
							}
						}
						if (exist) {
							UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.mail_id_exist, R.string.email);
						} else {
							db.executeQuery("insert into userGroups(userID, groupName, userName, emailID, enabled)values('" +scopeId + "', '" + groupname + "', '" + userName + "', '" + emailId + "', '1')");
							UserGroups group = new UserGroups(SlideMenuWithActivityGroup.this);
							group.setUserScopeId(Integer.parseInt(scopeId));
							group.setEnabled(1);
							group.setGroupName(groupname);
							group.setEmailId(emailId);
							group.setUserName(userName);
							group_details.add(group);
							groupDialog.dismiss();
							changingDetailsforMailIds();
						}
					}
					loginAdapter.listViewEmailGroup.invalidateViews();
				}

			}
		});

		Button btnCancel = (Button) groupDialog.findViewById(R.id.enr_cancel);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				groupDialog.dismiss();
				loginAdapter.listViewEmailGroup.invalidateViews();
			}
		});
		groupDialog.show();
	}


	private class groupListAdapter extends BaseAdapter{
		//ArrayList<UserGroups> groupsdetails=new ArrayList<UserGroups>();
		String newtext;
		int selected;

		ArrayList<UserGroups> editedGrpDetails=new ArrayList<UserGroups>();
		/*public groupListAdapter(MainActivity mainActivity,
                ArrayList<UserGroups> groups2) {
            // TODO Auto-generated constructor stub
            groups=groups2;


        }*/
		//@Override
		public int getCount() {

			return groups.size();
		}
		///@Override
		public Object getItem(int position) {

			return null;
		}

		//@Override
		public long getItemId(int position) {

			return 0;
		}

		//@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			View vi = convertView;
			if (convertView == null) {
				Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
				UserFunctions.changeFont(parent,font);
				vi = getLayoutInflater().inflate(R.layout.groups_view, null);
			}
			final TextView tv = (TextView) vi.findViewById(R.id.tv_groupName);
			ImageView iv_next = (ImageView) vi.findViewById(R.id.iv_next);
			Button btn_del = (Button) vi.findViewById(R.id.btn_del);
			final EditText et_bkmarktitle = (EditText) vi.findViewById(R.id.et_bkmarktitle);
			final UserGroups group = groups.get(position);
			tv.setText(group.getGroupName());
			//tv.setTextColor(Color.rgb(0, 0, 0));
			if (loginAdapter.grpInEditMode) {
				tv.setVisibility(View.INVISIBLE);
				et_bkmarktitle.setVisibility(View.VISIBLE);
				et_bkmarktitle.setText(group.getGroupName());
				et_bkmarktitle.setBackgroundResource(R.drawable.border_white);
				//et_bkmrktitle.setEnabled(true);
				iv_next.setVisibility(View.GONE);
				et_bkmarktitle.setFocusableInTouchMode(true);
			} else {
				if (loginAdapter.textChecking) {
					for (int i = 0; i < editedGrpDetails.size(); i++) {
						UserGroups checkgroups = editedGrpDetails.get(i);
						for (UserGroups editedgrp : groups) {
							String gp = editedgrp.getGroupName();
							String[] split = checkgroups.getGroupName().split("\n");
							String[] split1 = editedgrp.getGroupName().split("\n");
							if (split[0].equals(split1[0]) && checkgroups.getId() != editedgrp.getId()) {
								exist = true;
							}
						}
						if (exist) {
							exist = false;
							loginAdapter.btn_edit.setText(getResources().getString(R.string.edit));
						} else {
							loginAdapter.listViewGroup.invalidateViews();
							loginAdapter.btn_edit.setText(getResources().getString(R.string.edit));
							loginAdapter.grpInEditMode = false;
							for (UserGroups grp : editedGrpDetails) {
								for (UserGroups g : groups) {
									if (grp.getId() == g.getId()) {
										db.executeQuery("update userGroups set groupName='" + grp.getGroupName() + "' where groupName='"+g.getGroupName()+"'");
										//db.executeQuery("insert into userGroups(userID, groupName, userName, emailID, enabled)values('"+userCredentials.getUserScopeId()+"', '"+enrTitle+"', '', '', '1')");
										changingDetails(g.getId(),g,grp.getGroupName());
										g.setGroupName(grp.getGroupName());
										tv.setVisibility(View.VISIBLE);
										et_bkmarktitle.setVisibility(View.INVISIBLE);
										iv_next.setVisibility(View.VISIBLE);
										tv.setText(grp.getGroupName());
										et_bkmarktitle.setText(grp.getGroupName());
									}
								}
							}
							loginAdapter.listViewGroup.invalidateViews();
							//groupDialog.dismiss();
							et_bkmarktitle.setBackgroundResource(R.drawable.edit_text_without_border);
							et_bkmarktitle.setFocusableInTouchMode(false);

						}
					}
					if (editedGrpDetails.size() > 0) {
						editedGrpDetails.clear();
					}
					loginAdapter.textChecking = false;
				} else if (!exist) {
					loginAdapter.listViewGroup.invalidateViews();
					//groupDialog.dismiss();
					loginAdapter.btn_edit.setText(getResources().getString(R.string.edit));
					et_bkmarktitle.setBackgroundResource(R.drawable.edit_text_without_border);

					et_bkmarktitle.setFocusableInTouchMode(false);
					tv.setVisibility(View.VISIBLE);
					et_bkmarktitle.setVisibility(View.INVISIBLE);
					tv.setText(group.getGroupName());
					iv_next.setVisibility(View.VISIBLE);
					et_bkmarktitle.setText(group.getGroupName());
				}
			}
			if (deletemode) {
				btn_del.setVisibility(View.VISIBLE);
				iv_next.setVisibility(View.INVISIBLE);
			} else {
				btn_del.setVisibility(View.INVISIBLE);
				if (!loginAdapter.grpInEditMode) {
					iv_next.setVisibility(View.VISIBLE);
				}
			}
			btn_del.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					UserGroups group1 = groups.get(position);
					db.executeQuery("delete from userGroups where groupName='" + group1.getGroupName() + "' and userID='" + group1.getUserScopeId() + "'");
					changingDetails(group1.getId(),null, group1.getGroupName());
					groups.remove(position);
					loginAdapter.listViewGroup.invalidateViews();
					grp_delete=true;
				}
			});

			et_bkmarktitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {

				@Override
				public void onFocusChange(View arg0, boolean arg1) {
					newtext = et_bkmarktitle.getText().toString();
					UserGroups group1 = groups.get(position);

					UserGroups gp = new UserGroups(SlideMenuWithActivityGroup.this);
					gp.setGroupName(newtext);
					gp.setEmailId(group1.getEmailId());
					gp.setId(group1.getId());
					//if(editedGrpDetails.size()>0) {
					for (int j = 0; j < editedGrpDetails.size(); j++) {
						UserGroups group = editedGrpDetails.get(j);
						if (group.getId() == gp.getId()) {
							editedGrpDetails.remove(j);
							editedGrpDetails.add(gp);
							exist = true;
						}
					}
					if (!exist) {
						editedGrpDetails.add(gp);
					}
					exist = false;
					if (loginAdapter.btn_edit.getText().toString().equals(getResources().getString(R.string.edit))) {
						loginAdapter.textChecking = true;
						loginAdapter.listViewGroup.invalidateViews();
					}
				}
			});
			return vi;
		}
	}

	private class groupEmailAdapter extends BaseAdapter{
		ArrayList<UserGroups> groups_list=new ArrayList<UserGroups>();

		public groupEmailAdapter(SlideMenuWithActivityGroup mainActivity,
								 ArrayList<UserGroups> groupsmails) {
			groups_list=groupsmails;

		}
		//@Override
		public int getCount() {

			return groups_list.size();
		}
		///@Override
		public Object getItem(int position) {

			return null;
		}

		//@Override
		public long getItemId(int position) {

			return 0;
		}

		//@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			View vi = convertView;
			if(convertView == null){
				Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
				UserFunctions.changeFont(parent,font);
				vi = getLayoutInflater().inflate(R.layout.groups_list, null);
			}
			TextView tv_username = (TextView) vi.findViewById(R.id.tv_username);
			TextView tv_email = (TextView) vi.findViewById(R.id.tv_mailid);
			Button btn_del = (Button) vi.findViewById(R.id.btn_del);
			UserGroups group=groups_list.get(position);
			tv_username.setText(group.getUserName());
			tv_email.setText(group.getEmailId());
			btn_del.setVisibility(View.GONE);
			//tv.setTextColor(Color.rgb(0, 0, 0));
			if(deletemode){
				btn_del.setVisibility(View.VISIBLE);
			}else{
				btn_del.setVisibility(View.INVISIBLE);
			}
			btn_del.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					UserGroups group1=groups_list.get(position);
					db.executeQuery("delete from userGroups where userName='"+group1.getUserName()+"' and emailID='"+group1.getEmailId()+"'");
					groups_list.remove(position);
					changingDetailsforMailIds();
					loginAdapter.listViewEmailGroup.invalidateViews();
				}
			});

			return vi;
		}
	}
	private class UsersSelectEnrResAdapter extends BaseAdapter {

		private Enrichments enrichment;
		private Resources resource;
		private ArrayList<Enrichments> enrichList;
		private ArrayList<Resources> resList;
		private ArrayList<HashMap<String, String>> cloudEnrList;

		public UsersSelectEnrResAdapter(Enrichments enrichment, Resources resource) {
			this.enrichment = enrichment;
			this.resource = resource;
			if (enrichment != null) {
				enrichList = new ArrayList<Enrichments>();
				getAllEnrichmentList();
			} else {
				resList = new ArrayList<Resources>();
				getAllResList();
			}
		}

		public UsersSelectEnrResAdapter(ArrayList<HashMap<String, String>> cloudEnrList) {
			this.cloudEnrList = cloudEnrList;
		}

		private void getAllEnrichmentList(){
			for (int i = 0; i < webService.enrichmentList.size(); i++) {
				Enrichments enr = webService.enrichmentList.get(i);
				if (enrichment.getEnrichmentBid() == enr.getEnrichmentBid()) {
					enr.setEnrichmentStoreId(enrichment.getEnrichmentStoreId());
					enr.setDbBookId(enrichment.getDbBookId());
					enrichList.add(enr);
				}
			}
		}

		private void getAllResList(){
			for (int i = 0; i < webService.resourceList.size(); i++) {
				Resources res = webService.resourceList.get(i);
				if (resource.getResourceBid() == res.getResourceBid()) {
					res.setResourceStoreId(resource.getResourceStoreId());
					resList.add(res);
				}
			}
		}

		@Override
		public int getCount() {
			if (Globals.ISGDRIVEMODE()) {
				return this.cloudEnrList.size();
			} else {
				if (enrichment != null) {
					return enrichList.size();
				} else if (resource != null) {
					return resList.size();
				}
			}
			return 0;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			Enrichments enrichments;
			Resources resource;
			if (view == null) {
				Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
				UserFunctions.changeFont(parent,font);
				view = getLayoutInflater().inflate(R.layout.inflate_slct_users_res, null);
			}
			TextView txtTitle = null, txtSharedBy = null;
			CheckBox checkBox = null;
			ImageView iv_unread= (ImageView)view. findViewById(R.id.iv_unread);
			txtSharedBy = (TextView) view.findViewById(R.id.textView2);
			txtTitle = (TextView) view.findViewById(R.id.textView1);
			checkBox = (CheckBox) view.findViewById(R.id.checkBox1);

			if (Globals.ISGDRIVEMODE()) {
				HashMap<String, String> map = cloudEnrList.get(position);
				String enrMsg = map.get(webService.KEY_CEMESSAGE);
				txtTitle.setText(enrMsg);
				String sharedBy = "";
				String dateTime = "";
				String jsonContent = map.get(webService.KEY_CECONTENT);
				try {
					JSONObject jsonObject = new JSONObject(jsonContent);
					sharedBy = jsonObject.getString("sharedBy");
					dateTime = jsonObject.getString("dateTime");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				String txtDateSharedBy = sharedBy+" "+dateTime;
				txtSharedBy.setText(txtDateSharedBy);
				String isSelected = map.get(webService.KEY_CLOUDENRSELECTED);
				if (isSelected.equals("false")) {
					checkBox.setChecked(false);
				} else {
					checkBox.setChecked(true);
				}
			} else {
				if (enrichment != null) {
					enrichments = enrichList.get(position);
					txtSharedBy.setText("Shared by: "+enrichments.getEnrichmentUserName());
					txtTitle.setText("page "+enrichments.getEnrichmentPageNo()+" - "+enrichments.getEnrichmentTitle());

					if(enrichments.isDownloaded()&&!deleteMode){
						iv_unread.setVisibility(View.INVISIBLE);
						checkBox.setVisibility(View.GONE);
						view.setBackgroundColor(Color.parseColor("#A54970"));
					}else if(!enrichments.isReadStatus()){
						iv_unread.setVisibility(View.VISIBLE);
						checkBox.setVisibility(View.VISIBLE);
						view.setBackgroundColor(Color.TRANSPARENT);
					}else {
						iv_unread.setVisibility(View.INVISIBLE);
						checkBox.setVisibility(View.VISIBLE);
						view.setBackgroundColor(Color.TRANSPARENT);
					}

					if (enrichments.isEnrichmentSelected()) {
						if (deleteMode){
							view.setBackgroundColor(Color.parseColor("#C51162"));
						}
						checkBox.setChecked(true);
					} else {
						if (deleteMode){
							view.setBackgroundColor(Color.TRANSPARENT);
						}
						checkBox.setChecked(false);
					}
				}
				else if (this.resource != null) {
					resource = resList.get(position);
					if (resource.isResourceDownloaded()&&!deleteMode){
						iv_unread.setVisibility(View.INVISIBLE);
						checkBox.setVisibility(View.INVISIBLE);
						view.setBackgroundColor(Color.parseColor("#A54970"));
					}else if(!resource.isReadStatus()){
						iv_unread.setVisibility(View.VISIBLE);
						checkBox.setVisibility(View.VISIBLE);
						view.setBackgroundColor(Color.TRANSPARENT);
					}else {
						iv_unread.setVisibility(View.INVISIBLE);
						checkBox.setVisibility(View.VISIBLE);
						view.setBackgroundColor(Color.TRANSPARENT);
					}

					txtSharedBy.setText("Shared by: "+resource.getResourceUserName());
					txtTitle.setText(resource.getResourceTitle());
					if (resource.isResourceSelected()) {
						if (deleteMode){
							view.setBackgroundColor(Color.parseColor("#C51162"));
						}
						checkBox.setChecked(true);
					} else {
						if (deleteMode){
							view.setBackgroundColor(Color.TRANSPARENT);
						}
						checkBox.setChecked(false);
					}
				}
			}

			return view;
		}

	}

	private class UsersEnrichmentResourceAdapter extends BaseAdapter {

		private ArrayList<Enrichments> enrichList;
		private ArrayList<Resources> resList;
		private ArrayList<Enrichments> filteredEnrItemList;
		private ArrayList<Resources> filteredResItemList;

		public UsersEnrichmentResourceAdapter(ArrayList<Enrichments> enrichList, ArrayList<Resources> resList){
			this.enrichList = enrichList;
			this.resList = resList;
			if (enrichList != null) {
				filteredEnrItemList = new ArrayList<Enrichments>();
				filterEnrList();
			} else {
				filteredResItemList = new ArrayList<Resources>();
				filterResList();
			}
		}

		private void filterEnrList(){
			for (int i = 0; i < enrichList.size(); i++) {
				Enrichments enrich = enrichList.get(i);
				boolean isBookExist = false;
				for (int j = 0; j < filteredEnrItemList.size(); j++) {
					Enrichments filterEnr = filteredEnrItemList.get(j);
					if (enrich.getEnrichmentBid() == filterEnr.getEnrichmentBid()) {
						isBookExist = true;
						break;
					} else {
						isBookExist = false;
					}
				}
				if (!isBookExist && enrich.getEnrichmentBookTitle() != null) {
					filteredEnrItemList.add(enrich);
				}
			}
		}

		private void filterResList(){
			for (int i = 0; i < resList.size(); i++) {
				Resources resource = resList.get(i);
				boolean isBookExist = false;
				for (int j = 0; j < filteredResItemList.size(); j++) {
					Resources filterRes = filteredResItemList.get(j);
					if (resource.getResourceBid() == filterRes.getResourceBid()) {
						isBookExist = true;
						break;
					} else {
						isBookExist = false;
					}
				}
				if (!isBookExist && resource.getResourceBookTitle() != null) {
					filteredResItemList.add(resource);
				}
			}
		}

		@Override
		public int getCount() {
			if (enrichList != null) {
				return filteredEnrItemList.size();
			} else {
				return filteredResItemList.size();
			}
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			Enrichments enrichments;
			Resources resource;
			if (view == null) {
				Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
				UserFunctions.changeFont(parent,font);
				view = getLayoutInflater().inflate(R.layout.enrich_page_text, null);
			}
			TextView textView = (TextView) view.findViewById(R.id.textView1);
			TextView tv_enrichsize= (TextView) view.findViewById(R.id.tv_enrichsize);
			if (enrichList != null) {
				enrichments = filteredEnrItemList.get(position);
				int value=filteringUnreadEnrichments(enrichments.getEnrichmentBookTitle());
				if(value>0) {
					tv_enrichsize.setVisibility(View.VISIBLE);
					tv_enrichsize.setText(String.valueOf(value));
				}else{
					tv_enrichsize.setVisibility(View.GONE);
				}
				textView.setText(enrichments.getEnrichmentBookTitle());
			} else {
				resource = filteredResItemList.get(position);
				int value=filteringUnreadResources(resource.getResourceBookTitle());
				if(value>0) {
					tv_enrichsize.setVisibility(View.VISIBLE);
					tv_enrichsize.setText(String.valueOf(value));
				}else{
					tv_enrichsize.setVisibility(View.GONE);
				}
				textView.setText(resource.getResourceBookTitle());
			}
			return view;
		}
	}
    private int filteringUnreadEnrichments(String bookTitle){
		int unreadEnrichSize = 0;
          for(Enrichments enrich:loginAdapter.unreadEnrichments){
			  if(bookTitle.equals(enrich.getEnrichmentBookTitle())){
				  unreadEnrichSize=unreadEnrichSize+1;
			  }
		  }
		return unreadEnrichSize;
	}
	private int filteringUnreadResources(String bookTitle){
		int unreadEnrichSize = 0;
		for(Resources resource:loginAdapter.unreadResources){
			if(bookTitle.equals(resource.getResourceBookTitle())){
				unreadEnrichSize=unreadEnrichSize+1;
			}
		}
		return unreadEnrichSize;
	}
	private class SharedUsersAdapter extends BaseAdapter {

		Context context;
		private ArrayList<SharedUsers> shareduserslist;

		public SharedUsersAdapter(ArrayList<SharedUsers> shareduserslist,Context _context){
			this.shareduserslist = shareduserslist;
			this.context = _context;
		}
		@Override
		public int getCount() {

			return shareduserslist.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View view = convertView;

			if (view == null) {
				Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
				UserFunctions.changeFont(parent,font);
				view = getLayoutInflater().inflate(R.layout.sharedusers_inflate_list, null);
			}
			final SharedUsers shareduser= shareduserslist.get(position);
			TextView textView = (TextView) view.findViewById(R.id.textView1);
			TextView textView1 = (TextView) view.findViewById(R.id.textView2);
			final ToggleButton btntoggle = (ToggleButton) view.findViewById(R.id.switch1);
			btntoggle.setTag(shareduser.getUsersId());
			textView.setText(shareduser.getSharedUsersName());
			textView1.setText(shareduser.getSharedUsersEmailId());


			if(shareduser.getSharedUsersBlacklist().equals("yes")){
				btntoggle.setChecked(false);
			}else{
				btntoggle.setChecked(true);
			}
			btntoggle.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					if(btntoggle.isChecked()){
						btntoggle.setChecked(true);
						db.executeQuery("update sharedUsers set Blacklist ='no' where Email ='"+shareduser.getSharedUsersEmailId()+"'");
					}else{
						btntoggle.setChecked(false);
						db.executeQuery("update sharedUsers set Blacklist ='yes' where Email ='"+shareduser.getSharedUsersEmailId()+"'");
					}
				}
			});

			return view;
		}
	}

	private class loadCloudEnrichmentList extends AsyncTask<Void, Void, Void> {

		String emaild;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (loginAdapter != null &&loginAdapter.progressBar!=null) {
				loginAdapter.progressBar.setVisibility(View.INVISIBLE);
			}
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SlideMenuWithActivityGroup.this);
			emaild = prefs.getString(Globals.sUserEmailIdKey, "");
		}

		@Override
		protected Void doInBackground(Void... params) {
			webService.getCloudEnrichmentsFromUser(emaild, SlideMenuWithActivityGroup.this);
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			 super.onPostExecute(aVoid);

			if (webService.cloudEnrList != null && webService.cloudEnrList.size() > 0&&!emaild.equals("")) {
				if(loginAdapter!=null &&loginAdapter.txtCloudShare!=null) {
					loginAdapter.txtCloudShare.setText(String.valueOf(webService.cloudEnrList.size()));
					loginAdapter.btnCloudShare.setEnabled(true);
				}
				((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).btn_text.setText(String.valueOf(webService.cloudEnrList.size()));
				((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).btn_text.setEnabled(true);
			} else {
				if(loginAdapter!=null &&loginAdapter.txtCloudShare!=null) {
					loginAdapter.txtCloudShare.setText(String.valueOf(0));
				}
				if(((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).btn_text!=null) {
					((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).btn_text.setText(String.valueOf(0));
				}
			}
			if(loginAdapter!=null&&loginAdapter.progressBar!=null) {
				loginAdapter.progressBar.setVisibility(View.GONE);
			}
		}

	}

	/**
	 * Load Enrichment and resource for the logged in user
	 * @author Suriya
	 *
	 */
	private class loadEnrichmentAndResourceByUserId extends AsyncTask<Void, Void, Void> {
		ArrayList<SharedUsers> sharedList;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			loginAdapter.progressBar.setVisibility(View.INVISIBLE);
			sharedList = db.getCountForSharedList(scopeId, SlideMenuWithActivityGroup.this);
		}

		@Override
		protected Void doInBackground(Void... params) {
			webService.downloadedResourceList = new ArrayList<Resources>();
			webService.downloadedEnrichmentList = new ArrayList<Enrichments>();
			webService.readEnrichments=new ArrayList<String>();
			webService.readResources=new ArrayList<String>();
			for (int i = 0; i < ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray.size(); i++) {
				Book book = (Book) ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray.get(i);
				if (book.is_bStoreBook()) {
					String xmlUpdateRecordsPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+book.get_bStoreID()+"Book/xmlForUpdateRecords.xml";
					if (new File(xmlUpdateRecordsPath).exists()) {
						webService.getDownloadedEnrichmentsAndResourcesFromXmlPath(xmlUpdateRecordsPath);
					}
				}
			}

			File f=new File(Globals.TARGET_BASE_FILE_PATH+scopeId+".txt");
			String sourceString=UserFunctions.readFileFromPath(f);
            if(sourceString!=null) {
				JSONArray store1 = null;
				try {
					JSONObject jsonData = new JSONObject(sourceString);
					String enrichIds = jsonData.getString("enrichId");
					String userId = jsonData.getString("userID");
					if (userId.equals(scopeId)) {
						String resourceId = jsonData.getString("resourceId");
						store1 = new JSONArray(enrichIds);
						for (int i = 0; i < store1.length(); i++) {
							JSONObject json = store1.getJSONObject(i);
							String id = json.getString("ID");
							webService.readEnrichments.add(id);
						}
						store1 = new JSONArray(resourceId);
						for (int i = 0; i < store1.length(); i++) {
							JSONObject json = store1.getJSONObject(i);
							String id = json.getString("ID");
							webService.readResources.add(id);
						}
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
//			}else {
//                webService.readEnrichments.add("67");
//				webService.readEnrichments.add("68");
//			}
			webService.getEnrichmentPagesByUserId(scopeId,sharedList);
			webService.getResourcesPagesByUserId(scopeId,sharedList);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			int enrich_values = 0;
			if(loginAdapter.unreadEnrichments.size()>0){
				loginAdapter.unreadEnrichments.clear();
			}
			if(loginAdapter.unreadResources.size()>0){
				loginAdapter.unreadResources.clear();
			}
			for(int i=0;i<webService.enrichmentList.size();i++){
				Enrichments enrich=webService.enrichmentList.get(i);
                if(!enrich.isReadStatus() &&!enrich.isDownloaded()){
					 loginAdapter.unreadEnrichments.add(enrich);
				}
			}

			loginAdapter.txtEnrichment.setText(String.valueOf(loginAdapter.unreadEnrichments.size()));
			if (webService.enrichmentList.size() > 0) {
				loginAdapter.btnEnrichment.setEnabled(true);
			}
			for(int i=0;i<webService.resourceList.size();i++){
				Resources enrich=webService.resourceList.get(i);
				if(!enrich.isReadStatus() &&!enrich.isResourceDownloaded()){
					loginAdapter.unreadResources.add(enrich);
				}
			}

			loginAdapter.txtResources.setText(String.valueOf(loginAdapter.unreadResources.size()));
			if (webService.resourceList.size() > 0) {
				loginAdapter.btnResources.setEnabled(true);
			}
			loginAdapter.progressBar.setVisibility(View.GONE);
		}
	}

	/**
	 * User SignIn
	 * @author Suriya
	 *
	 */
	private class SignIn extends AsyncTask<Void, Void, Void> {

		int scopeType;
		String userName, password;
		ProgressBar progressBar;
		ProgressDialog progressDialog;
		String email;
		private WebService wService;

		public SignIn(String userName, String password, int scopeType, ProgressBar progressBar, String email){
			this.scopeType = scopeType;
			this.userName = userName;
			this.password = password;
			this.progressBar = progressBar;
			this.email = email;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (progressBar != null) {
				progressBar.setVisibility(View.INVISIBLE);
			}
			//wService = new WebService(SlideMenuWithActivityGroup.this, Globals.oldCurriculumWebServiceURL);
			progressDialog = ProgressDialog.show(SlideMenuWithActivityGroup.this, "", getResources().getString(R.string.please_wait), true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			Thread.currentThread().setPriority(Process.THREAD_PRIORITY_BACKGROUND + Process.THREAD_PRIORITY_MORE_FAVORABLE);
			wService = new WebService(SlideMenuWithActivityGroup.this, Globals.oldCurriculumWebServiceURL);
			wService.signIn(userName, password, scopeType);
			SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(SlideMenuWithActivityGroup.this);
			SharedPreferences.Editor editor = sharedPreference.edit();
			editor.putString(Globals.exportEnrichUserNameKey, userName);
			editor.putString(Globals.exportEnrichPasswordKey, password);
			editor.commit();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (wService.newSignInResponse != "") {
				if (wService.newSignInResponse.equals("-1")) {
					if (this.scopeType != Globals.scopeTypeSemanoor) {
						new SignUp(userName, password, userName, email, scopeType, progressBar, progressDialog).execute();
					}
				} else {
					//Login Success
					loggedInSuccessfully(wService.newSignInResponse, userName, scopeType, progressDialog,email);
					if (progressBar != null) {
						progressBar.setVisibility(View.GONE);
					}
				}
			}
		}
	}

	/**
	 * User SignUp
	 * @author Suriya
	 *
	 */
	private class SignUp extends AsyncTask<Void, Void, Void> {

		int scopeType;
		String userName, password, fullName, emailId;
		ProgressBar progressBar;
		ProgressDialog progressDialog;
		WebService wService;

		public SignUp(String userName, String password, String fullName, String emailId, int scopeType, ProgressBar progressBar, ProgressDialog progressDialog) {
			this.scopeType = scopeType;
			this.userName = userName;
			this.password = password;
			this.fullName = fullName;
			this.emailId = emailId;
			this.progressBar = progressBar;
			this.progressDialog = progressDialog;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			wService = new WebService(SlideMenuWithActivityGroup.this, Globals.oldCurriculumWebServiceURL);
			wService.signUp(userName, password, emailId, fullName, scopeType);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (wService.newSignUpResponse != "") {
				String[] splitArray = wService.newSignUpResponse.split("~");
				String signUpSuccess = splitArray[0];
				String scopeId = splitArray[1];
				if (signUpSuccess.equals("true")) {
					//Sign Up Successful
					loggedInSuccessfully(scopeId, userName, scopeType, progressDialog,emailId);
				} else {
					//Sign Up Failed
					UserFunctions.DisplayAlertDialogNotFromStringsXML(SlideMenuWithActivityGroup.this, getString(R.string.login_failed), "");
					if (progressDialog != null) {
						progressDialog.dismiss();
					}
				}
			}
			if (progressBar != null) {
				progressBar.setVisibility(View.GONE);
			}
		}
	}

	private void loggedInSuccessfully(String scopeId, String userName, int scopeType, ProgressDialog progressDialog,String email){
		new RegPushNotification(SlideMenuWithActivityGroup.this, scopeId, userName, scopeType, true, progressDialog,email).execute();
	}

	public void updateCredentialsForLoginView(String scopeId, String userName, int scopeType, ProgressDialog progressDialog,String email){
		updateUserCredentials(scopeId, userName, scopeType,email);
		if (viewPager.getCurrentItem() != 1) {
			viewPager.setCurrentItem(1, true);
		}
		if (Globals.isLimitedVersion()) {
			subscription.new getSubscribedId(this,"").execute();
            new UpdateUserGroups(SlideMenuWithActivityGroup.this).execute();
			new GetNooorPlusSubscriptionDetails().execute();
		}
		loginAdapter.btnLoginLayout.setVisibility(View.GONE);
		loginAdapter.loggedInSuccessLayout.setVisibility(View.VISIBLE);
		loginAdapter.usertxtView.setText(userName);
		TopicName=email;
		initMQTT();
		if (scopeType == Globals.scopeTypeFacebook) {
			loginAdapter.btnlogout.setVisibility(View.GONE);
			loginAdapter.fbLogoutBtn.setVisibility(View.VISIBLE);
		} else {
			loginAdapter.btnlogout.setVisibility(View.VISIBLE);
			loginAdapter.fbLogoutBtn.setVisibility(View.GONE);
			if (Globals.ISGDRIVEMODE()) {
				new loadCloudEnrichmentList().execute();
				new getTblUserGroupDetails(scopeId,progressDialog).execute();
			} else {
				new getUserGroupDetails(scopeId).execute();
				new loadEnrichmentAndResourceByUserId().execute();
			}
		}
		if (!Globals.ISGDRIVEMODE()) {
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}
		if(fbEnrDownload){
			Intent activity = new Intent((MainActivity) this.getCurrentActivity(), CloudBooksActivity.class);
			//intent.putExtra("SlidemenuActivity",SlideMenuActivity);
			activity.putExtra("Shelf", ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf);
			activity.putExtra("id", msgFromBrowserUrl);
			((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).startActivityForResult(activity, 3);
		}
	}

	private void logout(){
		ProgressDialog progressDialog = ProgressDialog.show(SlideMenuWithActivityGroup.this, "", getResources().getString(R.string.please_wait), true);
		new RegPushNotification(SlideMenuWithActivityGroup.this, "", "", 0, false, progressDialog, "").execute();
		SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(SlideMenuWithActivityGroup.this);
		SharedPreferences.Editor editor = sharedPreference.edit();
		editor.putString(Globals.exportEnrichUserNameKey, "");
		editor.putString(Globals.exportEnrichPasswordKey, "");
		editor.putString(Globals.exportEnrichPasswordKey, "");
		editor.putString(Globals.SUBSCRIPTSTARTDATEKEY, "");
		editor.putString(Globals.SUBSCRIPTENDDATEKEY, "");
		editor.putString(Globals.SUBSCRIPTDAYSLEFT, "0");
		editor.putString(Globals.sUserEmailPhotoUrl, "");
        editor.putString(Globals.sAuthToken,"");
		editor.commit();
		editor.clear();
		if(((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).btn_text!=null) {
			((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).btn_text.setText(String.valueOf(0));
		}
	}

	public void updateCredentialsForLogoutView(String scope_Id, String user_Name, int scope_Type, ProgressDialog progressDialog,String user_Email){
		updateUserCredentials(scope_Id, user_Name, scope_Type,user_Email);
		scopeId=scope_Id;
		scopeType=scope_Type;
		userName=user_Name;
		email=user_Email;
		mIcon11=null;
		loginAdapter.usertxtView.setText(user_Name);
		loginAdapter.userImagView.setImageBitmap(null);
		loginAdapter.btnLoginLayout.setVisibility(View.VISIBLE);
		loginAdapter.loggedInSuccessLayout.setVisibility(View.GONE);
		viewPager.setCurrentItem(0, true);
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
		Groups updateGroups = Groups.getInstance();
		updateGroups.setAllToFalse();
		new GetNooorPlusSubscriptionDetails().execute();
		new UpdateUserGroups(SlideMenuWithActivityGroup.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		UserContactDetails.context=null;
		deletingGrpandContactDetails();
	}

	/**
	 * Update the login user credentials to Shared preferene
	 */
	private void updateUserCredentials(String scope_Id, String user_Name, int scope_Type,String user_Email){

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		int sscopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
		if (scope_Type == 0){
			if (sscopeType == Globals.scopeTypeGoogle){
				signOutFromGplus();
			} else if (sscopeType == Globals.scopeTypeFacebook){
				mIcon11 = null;
				fbPhotoUrl = null;
			}
		}
		scopeId=scope_Id;
		scopeType=scope_Type;
		userName=user_Name;
		email=user_Email;
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Globals.sUserIdKey, scopeId);
		editor.putString(Globals.sUserNameKey, userName);
		editor.putInt(Globals.sScopeTypeKey, scopeType);
		editor.putString(Globals.sUserEmailIdKey,email);
		editor.commit();
		ManahijApp.getInstance().getPrefManager().addSubscribeTopicName(email);
		checkAndLogin();
	}

	private void signOutFromGplus() {
		/*if (mGoogleApiClient.isConnected()) {
			mIcon11 = null;
			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			mGoogleApiClient.disconnect();
			mGoogleApiClient.connect();
		}*/
			Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
				@Override
				public void onResult(Status status) {
					//System.out.println("SignOut");
				}
			});
	}

	/**
	 * Check shared preference and login
	 */
	private void checkAndLogin(){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		 scopeId = prefs.getString(Globals.sUserIdKey, "");
		 userName = prefs.getString(Globals.sUserNameKey, "");
		 //scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
		 email = prefs.getString(Globals.sUserEmailIdKey, "");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RC_SIGN_IN) {
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
			handleSignInResult(result);
		} else if (requestCode == 64206) {
			uiHelper.onActivityResult(requestCode, resultCode, data);
			/*Session session = Session.getActiveSession();
			session.onActivityResult(this, requestCode, resultCode, data);
			if (session != null && (session.isOpened() || session.isClosed())) {
				onSessionStateChange(session, session.getState(), null);
			}*/
		} else if (requestCode == themesActivityRequest) {
            String themesPath =  Globals.TARGET_BASE_FILE_PATH+"Themes/current/";
			UserFunctions.applyingBackgroundImage(themesPath+"shelf_bg.png",((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).rl,SlideMenuWithActivityGroup.this);

		} if (mHelper!=null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		}
		else if (requestCode==CONNECT_CALL_ESTABLISHMENT&& data != null){
			if (resultCode == RESULT_OK) {
				MyContacts contacts = data.getParcelableExtra("response");
				if(contacts!=null) {
					creatingCallUI(this, contacts);
					Log.e("coming", "coming" + contacts.getEmailId() + contacts.isOnline);
				}
			}

		}
 	}
	private void editingMemberDetails(final UserGroups usrgroups, final int position){
		final Dialog groupDialog = new Dialog(SlideMenuWithActivityGroup.this);
		groupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.semi_transparent)));
		groupDialog.setTitle("Enter MailId");
		groupDialog.setContentView(this.getLayoutInflater().inflate(R.layout.email_edit, null));
		groupDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, this), RelativeLayout.LayoutParams.WRAP_CONTENT);
		final EditText editText = (EditText) groupDialog.findViewById(R.id.enr_editText);
		final EditText email_et = (EditText) groupDialog.findViewById(R.id.editText1);
		editText.setText(usrgroups.getUserName());
		email_et.setText(usrgroups.getEmailId());
		//editText.setddText(enrichmentTitle);
		Button btnSave = (Button) groupDialog.findViewById(R.id.enr_save);
		btnSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean exist = false;
				String userName = editText.getText().toString();
				String emailId = email_et.getText().toString();
				if (!UserFunctions.CheckValidEmail(emailId)) {
					UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.input_valid_mail_id, R.string.invalid_email_id);
				}else{

					for (UserGroups checkmail : group_details) {
						String mail = checkmail.getEmailId();
						if (emailId.equals(mail) && usrgroups.getId()!=checkmail.getId()) {
							exist = true;
							break;
						}
					}
					if (exist) {
						UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, R.string.mail_id_exist, R.string.email);
					} else {
						db.executeQuery("update userGroups set userName='" + userName + "',emailID='" + emailId + "'where ID='" + usrgroups.getId() + "'");
						group_details.remove(position);
						UserGroups group = new UserGroups(SlideMenuWithActivityGroup.this);
						group.setUserScopeId(usrgroups.getUserScopeId());
						group.setEnabled(usrgroups.getEnabled());
						group.setGroupName(usrgroups.getGroupName());
						group.setEmailId(emailId);
						group.setUserName(userName);
						group_details.add(group);
						groupDialog.dismiss();
						changingDetailsforMailIds();
					}
					//}
					loginAdapter.listViewEmailGroup.invalidateViews();
				}

			}
		});

		Button btnCancel = (Button) groupDialog.findViewById(R.id.enr_cancel);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				groupDialog.dismiss();
				loginAdapter.listViewEmailGroup.invalidateViews();
			}
		});
		groupDialog.show();
	}

	private void clearApplicationData(){
		File cache = getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists()){
			String[] child = appDir.list();
			for (String str : child){
				if (str.equals("files")){
					deleteDir(new File(appDir,str));
				}
			}
		}
	}
	private boolean deleteDir(File dir){
		if (dir!=null && dir.isDirectory()){
			String[] child = dir.list();
			for (int i = 0; i<child.length; i++){
				boolean success = deleteDir(new File(dir,child[i]));
				if (!success){
					return false;
				}
			}
		}
		return dir.delete();
	}
	public void checkValidation(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date now =new Date();
		String dateString = format.format(now);
		if (dateString.equals("2016-06-01")){
			android.os.Process.killProcess(android.os.Process.myPid());
		}
	}


	private class SavingGroupDetails extends AsyncTask<Void, Void, Void> {
		JSONObject jsonData;
		int userScopeId = 0;
		String saveuserGrpData;
		@Override
		protected void onPreExecute() {

			ArrayList<JSONObject> grps=new ArrayList<>();
			JSONArray jsonArray=new JSONArray();
			for(UserGroups user:User_groups){
				userScopeId=user.getUserScopeId();
				JSONObject groups=getGroupDetails(user);

				//HashMap<String, String> groups=generatingJsonDataForGroups(user);
				jsonArray.put(groups);
			}
			//JSONObject groups=getGroupDetails(user);
			//JSONObject group1=getGroupDetails(user);
			 jsonData=new JSONObject();
			try {
				jsonData.put("UserGroupsDetail",jsonArray);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		@Override
		protected Void doInBackground(Void... params) {
			String jsonString = jsonData.toString();
			WebService webService = new WebService(SlideMenuWithActivityGroup.this, Globals.sbaNewCurriculumWebServiceURL);
			webService.saveUserGroupDetails(String.valueOf(userScopeId),jsonString);
			return null;
		}


		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
//			for(UserGroups user:User_groups) {
//				userScopeId = user.getUserScopeId();
		   db.executeQuery("delete from userGroups where userID='" +userScopeId+ "'");

//			}

			if(User_groups.size()>0){
				User_groups.clear();
				User_groups=null;
			}
			if(groups.size()>0){
				groups.clear();
				groups=null;
			}
			creatingDetails();

		}
	}

	private class getUserGroupDetails extends AsyncTask<Void, Void, Void> {
        String  scopeId;
		ListView list;
		WebService webService;

		public getUserGroupDetails(String id) {
			scopeId=id;

		}

		@Override
		protected Void doInBackground(Void... params) {
			webService = new WebService(SlideMenuWithActivityGroup.this, Globals.sbaNewCurriculumWebServiceURL);
			webService.getUserGroupDetails(scopeId);
			db.executeQuery("delete from userGroups where userID='"+Integer.parseInt(scopeId)+"';");
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(groups!=null) {
				if (groups.size() > 0) {
					groups.clear();
					groups = null;
				}
			}
			if(User_groups!=null) {
				if (User_groups.size() > 0) {
					User_groups.clear();
					User_groups = null;
				}
			}
			try {

				if(webService.userGrpData!=null) {
					JSONObject json = new JSONObject(webService.userGrpData);
					String userId = json.getString("UserId");
					String UserGroup = json.getString("UserGroup");
					if (UserGroup != null ||!UserGroup.equals("null")) {
					    JSONArray store1 = new JSONArray(UserGroup);
						for (int i = 0; i < store1.length(); i++) {
							JSONObject jsonSub = store1.getJSONObject(i);
							String grpName = jsonSub.getString("GroupName");
							String emailIDs = jsonSub.getString("EmailIDs");
							String[] split = emailIDs.split(",");
							//group_names = db.getMyGroups(scopeId, SlideMenuWithActivityGroup.this);
							for (int j = 0; j < split.length; j++) {
								if (!split[j].equals(" ") || !split[j].equals("") || !split[j].equals("-")) {
									String[] data = split[j].split("-");
									// emails= db.getselectedgroupdetails(grpName,Integer.parseInt(scopeId),SlideMenuWithActivityGroup.this);
									if (data.length > 1) {
										boolean isexist = false;
										db.executeQuery("insert into userGroups(userID, groupName, userName, emailID, enabled)values('" + scopeId + "', '" + grpName + "', '" + data[0] + "', '" + data[1] + "', '1')");
									} else {
										db.executeQuery("insert into userGroups(userID, groupName, userName, emailID, enabled)values('" + scopeId + "', '" + grpName + "', '', '', '1')");
									}

								} else {
									boolean isexist = false;
									int id = db.getMaxUniqueID(grpName);
									db.executeQuery("insert into userGroups(userID, groupName, userName, emailID, enabled)values('" + scopeId + "', '" + grpName + "', '', '', '1')");

								}
							}
						}
						//	}
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			/*String absPath= Environment.getExternalStorageDirectory().getAbsolutePath();
			String coverandendpage=absPath+"/Download/sboookAuthor.sqlite";
			String DB_PATH = SlideMenuWithActivityGroup.this.getFilesDir().getParentFile().getPath()+"/databases/sboookAuthor.sqlite";
			UserFunctions.copyFiles(DB_PATH,coverandendpage);*/
			groups = db.getMyGroups(scopeId, SlideMenuWithActivityGroup.this);
			User_groups = db.getMyGroups(scopeId, SlideMenuWithActivityGroup.this);

		}

	}

	private JSONObject getGroupDetails(UserGroups user) {
	ArrayList<String> group_details= db.getselectedgroup(user.getGroupName(),user.getUserScopeId(),SlideMenuWithActivityGroup.this);
		String data = null;
		for(int i=0;i<group_details.size();i++){
			if(i==0){
				data=group_details.get(i);
			}else{
				String p=group_details.get(i);
				data=data+" , " +p;
			}
		}

		JSONObject groups=new JSONObject();
		try {
			if(user.isDelete()) {
				groups.put("isDelete","yes");
				groups.put("GroupName",user.getGroupName());
				groups.put("EmailIDs","  ");
			}else{
				groups.put("isDelete","no");
				groups.put("GroupName",user.getGroupName());
				if(data!=null) {
					groups.put("EmailIDs", data);
				}else{
					groups.put("EmailIDs"," ");
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

       return groups;
	}

	private void changingDetails(int position, UserGroups g, String groupName){
		grp_delete=true;
		if(g!=null) {
			for (UserGroups user : User_groups) {
				if (user.getId() == position && user.getGroupName().equals(groupName)) {
					user.setDelete(true);
                    break;
				}
				if(user.getId() == position && !user.getGroupName().equals(groupName)){
					if(!loginAdapter.grpInEditMode) {
						g.setGroupName(groupName);
						User_groups.add(g);
						break;
					}
				}
			}
		}else{
			for (UserGroups user : User_groups) {
				if (user.getId() == position && user.getGroupName().equals(groupName)) {
					user.setDelete(true);
					break;
				}
			}
		}
	}
	private void creatingDetails(){
		grp_delete=false;
	}
	private void changingDetailsforMailIds(){
		grp_delete=true;
    }
	private void deleteEnrichmentsLocally(ArrayList<Enrichments> delEnrList){
		for (int j = 0; j < delEnrList.size(); j++) {
			Enrichments enrichments = delEnrList.get(j);
			String path = null;
			Book currentBook = null;
			for (int i = 0; i < ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray.size(); i++) {
				currentBook = (Book) ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).gridShelf.bookListArray.get(i);
				if (currentBook.is_bStoreBook()) {
					if (currentBook.getBookTitle().equals(enrichments.getEnrichmentBookTitle())) {
						path = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/xmlForUpdateRecords.xml";
						if (new File(path).exists()) {
							break;
							//		webService.getDownloadedEnrichmentsAndResourcesFromXmlPath(xmlUpdateRecordsPath);
						}
					}
				}
			}
			if (path!=null) {
				try {
					DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

					Document document = documentBuilder.parse(new File(path));
					String tagName = "page" + (enrichments.getEnrichmentPageNo());
					Node mainPageNode = document.getElementsByTagName("pages").item(0);
					Node pageNode = document.getElementsByTagName(tagName).item(0);
					if (pageNode!=null) {
						NodeList nodes = pageNode.getChildNodes();
						for (int i = 0; i < nodes.getLength(); i++) {
							Node element = nodes.item(i);
							NamedNodeMap attribute = element.getAttributes();
							Node nodeAttr = attribute.getNamedItem("ID");
							String idValue = String.valueOf(enrichments.getEnrichmentId());
							String idvalue1 = nodeAttr.getNodeValue();

							if (idvalue1.equals(idValue)) {
								//System.out.println("nodevalue entered");
								db.executeQuery("delete from enrichments where BID='" + enrichments.getDbBookId() + "' and pageNO='" + enrichments.getEnrichmentPageNo() + "' and Type='" + Globals.downloadedEnrichmentType + "'");
								pageNode.removeChild(element);
								if (nodes.getLength() == 1) {
									mainPageNode.removeChild(pageNode);
								}
								String fileName = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/Enrichments/Page" + enrichments.getEnrichmentPageNo();
								File enrichFileDelete = new File(fileName + "/" + "Enriched" + idValue);
								if (enrichFileDelete.exists()) {
									UserFunctions.DeleteDirectory(enrichFileDelete);
								}
								File enrichFile = new File(fileName);
								File[] enrichFileArray = enrichFile.listFiles();
								if (enrichFileArray.length == 0) {
									if (enrichFile.exists()) {
										UserFunctions.DeleteDirectory(enrichFile);
									}
								}
								break;
							}
						}
					}

					// write the DOM object to the file
					TransformerFactory transformerFactory = TransformerFactory.newInstance();
					Transformer transformer = transformerFactory.newTransformer();
					DOMSource domSource = new DOMSource(document);

					StreamResult streamResult = new StreamResult(new File(path));
					transformer.transform(domSource, streamResult);

				} catch (ParserConfigurationException pce) {
					pce.printStackTrace();
				} catch (TransformerException tfe) {
					tfe.printStackTrace();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				} catch (SAXException sae) {
					sae.printStackTrace();
				}
			}
		}
	}
	private String jsonString(ArrayList<Enrichments> delenrList,ArrayList<Resources> delResList) throws JSONException {
		JSONArray jsonArray = new JSONArray();
		JSONObject object = null;
		if (delenrList!=null) {
			for (int i = 0; i < delenrList.size(); i++) {
				Enrichments enrichments = delenrList.get(i);
				object = new JSONObject();
				object.put("ID", enrichments.getEnrichmentId());
				jsonArray.put(object);
			}
		}else{
			for (int i = 0; i < delResList.size(); i++) {
				Resources resources = delResList.get(i);
				object = new JSONObject();
				object.put("ID", resources.getResourceId());
				jsonArray.put(object);
			}
		}
		JSONObject finalObject = new JSONObject();
		if (delenrList!=null){
			finalObject.put("deleteType","E");
		}else{
			finalObject.put("deleteType","R");
		}
		finalObject.put("deleteEnrichId",jsonArray);
		String jsonStr = finalObject.toString();
        return jsonStr;
	}
	private class deleteEnrichments extends AsyncTask<Void, Void, Void>{
		ProgressDialog progresdialog;
		String json;
		String scopeId;

		public deleteEnrichments(String jsonString,String scopeId){
			this.json = jsonString;
			this.scopeId = scopeId;
		}

		@Override
		protected void onPreExecute() {
			String progressString = "Deleting...";
			progresdialog = ProgressDialog.show(SlideMenuWithActivityGroup.this, "", progressString, true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			WebService webService = new WebService(SlideMenuWithActivityGroup.this, Globals.sbaNewCurriculumWebServiceURL);
			webService.deleteEnrichments(json,scopeId);
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			if (progresdialog.isShowing()){
				progresdialog.dismiss();
			}
		}
	}
	public BroadcastReceiver mNetworkStateIntentReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			int extraWifiState = 0;
			if (intent.getAction().equals(
					ConnectivityManager.CONNECTIVITY_ACTION)) {

				NetworkInfo info = intent.getParcelableExtra(
						ConnectivityManager.EXTRA_NETWORK_INFO);
				String typeName = info.getTypeName();
				String subtypeName = info.getSubtypeName();
				// //System.out.println("Network is up ******** "+typeName+":::"+subtypeName);

				if (checkInternetConnection() == true) {
					extraWifiState = 3;

				} else {
					extraWifiState = 1;
				}
			} else {
				extraWifiState = 3;
			}

			switch (extraWifiState) {
				case WifiManager.WIFI_STATE_DISABLED:
					DownloadBackground background = DownloadBackground.getInstance();
					if (background.isDownloadTaskRunning()){
						background.setDownloadTaskRunning(false);
					}
					((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).isInternetExists = true;
					break;

				case WifiManager.WIFI_STATE_ENABLED:
					if(((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).isInternetExists) {
						DownloadBackground background1 = DownloadBackground.getInstance();
						if (background1.getContext() instanceof MainActivity) {
							((MainActivity) background1.getContext()).loadGridView();
						}
//						if (gridview!=null){
//							gridview.invalidateViews();
//						}
					}
					break;
			}
		}
	};
	private boolean checkInternetConnection() {

		ConnectivityManager conMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

		// ARE WE CONNECTED TO THE NET

		if (conMgr.getActiveNetworkInfo() != null
				&& conMgr.getActiveNetworkInfo().isAvailable()
				&& conMgr.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			return false;
		}
	}
	private class getNewVersionCode extends AsyncTask<Void,String,String>{

		@Override
		protected String doInBackground(Void... params) {
			String newVersion = null;
			try{
				newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=com.semanoor.manahij&hl=en")
						.timeout(30000)
						.userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
						.referrer("http://www.google.com").get()
						.select("div[itemprop=softwareVersion]")
						.first()
						.ownText();
				return newVersion;
			}catch (Exception e){
				return newVersion;
			}
		}

		@Override
		protected void onPostExecute(String newVersion) {
			super.onPostExecute(newVersion);
			if (newVersion!=null && !newVersion.isEmpty()){
				if (!newVersion.equals(com.semanoor.manahij.BuildConfig.VERSION_NAME)){
					AlertDialog.Builder builder = new AlertDialog.Builder(SlideMenuWithActivityGroup.this);
					builder.setPositiveButton(R.string.upgrade_now, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent updateIntent = new Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/apps/details?id=com.semanoor.manahij&hl=en"));
							startActivity(updateIntent);
						}
					});

					builder.setNegativeButton(R.string.remind_later, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
					builder.setTitle(R.string.upgrade);
					builder.setMessage(R.string.upgrade_msg);
					builder.show();
				}
			}
		}

	}

	private void alterTableGroupbooks(String query){
		try {
			db.executeQuery("ALTER TABLE books ADD COLUMN domainUrl Text");
			db.executeQuery(query);
			//return false;
		}catch (SQLiteException e){
			//return true;
		}
	}
	private void alterTableEnrichments(){
		try {
			db.executeQuery("ALTER TABLE enrichments ADD COLUMN SequentialID INTEGER");
			//db.executeQuery(query);
			//return false;
		}catch (SQLiteException e){
			//return true;
		}
	}
	private boolean alterTableGruopbooks(DatabaseHandler db){
		try {
			db.executeQuery("ALTER TABLE books ADD COLUMN groupId INTEGER");
			db.executeQuery("update books set groupId='0'");
			return false;
		}catch (SQLiteException e){
			return true;
		}
	}
	private void alterTableAddClientId(){
		try {
			db.executeQuery("ALTER TABLE books ADD COLUMN clientID text");
			db.executeQuery("update books set clientID='0'");
			//return false;
		}catch (SQLiteException e){
			//return true;
		}
	}

	private void alterTableAddCategorytId(){
		try {
			db.executeQuery("ALTER TABLE enrichments ADD COLUMN categoryID INTEGER");
			db.executeQuery("update enrichments set categoryID='0'");
			//return false;
		}catch (SQLiteException e){
			//return true;
		}
	}

	private void alterNoteTable(){
		try {
			db.executeQuery("ALTER TABLE tblNote ADD COLUMN NoteType INTEGER");
			db.executeQuery("ALTER TABLE tblNote ADD COLUMN CallStatus INTEGER");
			db.executeQuery("update tblNote set NoteType='1'");
			//return false;
		}catch (SQLiteException e){
			//return true;
		}
	}

	private void alterNoteTableforPdf(){
		try {
			db.executeQuery("ALTER TABLE tblNote ADD COLUMN NoteText text");
			db.executeQuery("ALTER TABLE tblNote ADD COLUMN Flag text");
			db.executeQuery("ALTER TABLE tblNote ADD COLUMN Rect text");
		}catch (SQLiteException e){

		}
	}


	private void alterTableAddBookDirection(){
		try {
			db.executeQuery("ALTER TABLE books ADD COLUMN bookDirection text");
			db.executeQuery("update books set bookDirection='ltr'");
			//return false;
		}catch (SQLiteException e){
			//return true;
		}
	}

	private String newWebService(String url,String urlParameter){
		HttpURLConnection urlConnection = null;
		StringBuilder sb = new StringBuilder();
		String returnResponse = "";
		try {
			URL urlToRequest = new URL(url);

			urlConnection = (HttpURLConnection) urlToRequest.openConnection();
			urlConnection.setConnectTimeout(10000);
			urlConnection.setReadTimeout(25000);
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			OutputStream os = urlConnection.getOutputStream();
			BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(os, "UTF-8"));
			writer.write(urlParameter);
			writer.flush();
			writer.close();
			os.close();
			int statusCode = urlConnection.getResponseCode();
			if (statusCode == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
				String line = null;
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}
				br.close();
				System.out.println("" + sb.toString());
			} else {
				System.out.println(urlConnection.getResponseMessage());
			}

		} catch (MalformedURLException e) {

		} catch (SocketTimeoutException e) {

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return sb.toString();
	}
	public class callWebserviceForFRedeem extends AsyncTask<Void, Void, String> {
		String webServiceUrl;
		JSONObject json;
		String jsonData;
		String redeemCode;
		ProgressDialog progressDialog;

		public callWebserviceForFRedeem(String url,String code){
			this.webServiceUrl = url;
			this.redeemCode = code;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(SlideMenuWithActivityGroup.this,"", getResources().getString(R.string.please_wait), true);
		}

		@Override
		protected String doInBackground(Void... params) {
			String urlParameter = getJsonData(redeemCode,scopeId);
			jsonData = newWebService(webServiceUrl,urlParameter);
			jsonData = jsonData.replaceAll("\\\\r\\\\n", "");
			jsonData = jsonData.replace("\"{", "");
			jsonData = jsonData.replace("\"", "");
			jsonData = jsonData.replace("}\"", "");
			jsonData = jsonData.replace("{","");
			jsonData = jsonData.replace("}","");
			return jsonData;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (progressDialog!=null){
				progressDialog.dismiss();
			}
			if (jsonData == null || jsonData.equals("")) {
				return;
			}
			String[] split = jsonData.split(",");
			if (split.length>1){
				String message = split[0];
				String success = split[1];
				String[] getMessage = message.split(":");
				String[] ifSuccess = success.split(":");
				if (getMessage.length>1 && ifSuccess.length>1){
					if (ifSuccess[1].equals("true")){
                        new UpdateUserGroups(SlideMenuWithActivityGroup.this).execute();
						UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, getMessage[1], R.string.redeem);
					}else{
						UserFunctions.DisplayAlertDialog(SlideMenuWithActivityGroup.this, getMessage[1], R.string.redeem);
					}
				}
			}
		}
	}
	private String getJsonData(String code,String userId){
		JSONObject student2 = new JSONObject();
		try {
			student2.put("RedeemCode", code);
			student2.put("UserId", userId);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return student2.toString();
	}
	private void redeemDialog(){
		final Dialog groupDialog = new Dialog(SlideMenuWithActivityGroup.this);
		groupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		groupDialog.setContentView(getLayoutInflater().inflate(R.layout.redeem_dialog, null));
		groupDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.5), RelativeLayout.LayoutParams.WRAP_CONTENT);
		final EditText editText = (EditText) groupDialog.findViewById(R.id.enr_editText);
		Button btnSave = (Button) groupDialog.findViewById(R.id.enr_save);
		btnSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (editText.getText().toString().length()>0){
					groupDialog.dismiss();
					String url = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/SaveUserRedeemDetails";
					new callWebserviceForFRedeem(url,editText.getText().toString()).execute();
				}
			}
		});
		Button btnCancel = (Button) groupDialog.findViewById(R.id.enr_cancel);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				groupDialog.dismiss();
			}
		});
		groupDialog.show();
	}

  private class getTblUserGroupDetails extends AsyncTask<Void, Void, Void> {
		String  scopeId;
		ListView list;
		ProgressDialog progressDialog;
	    WebService webService;
		public getTblUserGroupDetails(String id, ProgressDialog prgresDialog) {
			scopeId=id;
			progressDialog=prgresDialog;
		}

		@Override
		protected Void doInBackground(Void... params) {
			webService = new WebService(SlideMenuWithActivityGroup.this, Globals.getNewCurriculumWebServiceURL());
			webService.getUserGroupDetails(scopeId);
//			db.executeQuery("delete from userGroups where userID='"+Integer.parseInt(scopeId)+"';");
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(groups!=null) {
				if (groups.size() > 0) {
					groups.clear();
					groups = null;
				}
			}
			if(User_groups!=null) {
				if (User_groups.size() > 0) {
					User_groups.clear();
					User_groups = null;
				}
			}
			try {

				if(webService.userGrpData!=null) {
					JSONObject json = new JSONObject(webService.userGrpData);
					//String userId = json.getString("userId");
					//String UserGroup = json.getString("group");
					String Contacts = json.getString("contacts");
					UpdatingUserGrpDetails(json.getString("groups"));

					JSONObject jsonContactData = new JSONObject(Contacts);
					//String contactsList=jsonContactData.getString("contact");
					savingContactDetails(jsonContactData.getString("contact"),0);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			groups = db.getMyGroups(scopeId, SlideMenuWithActivityGroup.this);
			User_groups = db.getMyGroups(scopeId, SlideMenuWithActivityGroup.this);

			if (progressDialog != null) {
				progressDialog.dismiss();
			}

		}

	}
     private void deletingGrpandContactDetails(){
		 db.executeQuery("delete from tblUserGroups");
		 db.executeQuery("delete from tblUserContacts");
		 db.executeQuery("delete from sqlite_sequence where name='tblUserGroups'");
		 db.executeQuery("delete from sqlite_sequence where name='tblUserContacts'");
	 }
	private void UpdatingUserGrpDetails(String grpDetails){
		JSONObject json;
		JSONArray store1 = null;
		try {
			json = new JSONObject(grpDetails);
			String group=json.getString("group");
			store1 = new JSONArray(group);
			//String groupData=store1.getString("group");

		 for (int i = 0; i < store1.length(); i++) {
			 JSONObject jsonSub = store1.getJSONObject(i);
			 String grpName = jsonSub.getString("name");
			 String userId = jsonSub.getString("userId");
			 int enabled = jsonSub.getInt("enabled");
			 db.executeQuery("Insert into tblUserGroups(name,userID,enabled) values('" + grpName + "','" + userId + "','" + enabled + "')");
			// db.executeQuery("update tblUserGroups set name='" + grpName + "' where userID='" +userId + "' and  id='" + enabled + "'");
			 String contacts = jsonSub.getString("contacts");
			 int groupId =db.getMaxUniqueRowID("tblUserGroups");
             savingContactDetails(contacts,groupId);
		 }
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	private  void savingContactDetails(String contactDetails,int groupId){
		JSONArray josnData;
		try {
			josnData = new JSONArray(contactDetails);
		for (int i = 0; i < josnData.length(); i++) {
			JSONObject jsonSub = josnData.getJSONObject(i);
			String userName = jsonSub.getString("userName");
			String emailId = jsonSub.getString("emailId");
			int enabled = jsonSub.getInt("enabled");
			String userId = jsonSub.getString("userId");
			db.executeQuery("Insert into tblUserContacts(userName,emailID,userID,groupsID,enabled) values('" + userName + "','" + emailId + "','" + userId + "','" + groupId+ "','" + enabled + "')");
			//int Id = db.getMaxUniqueRowID("tblUserContacts");
          }
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public boolean isTablet(Context ctx){
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width=dm.widthPixels;
		int height=dm.heightPixels;
		double wi=(double)width/(double)dm.xdpi;
		double hi=(double)height/(double)dm.ydpi;
		double x = Math.pow(wi,2);
		double y = Math.pow(hi,2);
		double screenInches = Math.sqrt(x+y);
		if (screenInches>=6.5){
			return true;
		}else{
			return false;
		}
	}
	public class GetNooorPlusSubscriptionDetails extends AsyncTask<Void, Void, Void> {

		String emailId;
		long days;
		String response;
		ProgressDialog processDialog;
		private String productId = "np001";
		private SharedPreferences sharedPrefs;

		public GetNooorPlusSubscriptionDetails() {
			sharedPrefs = PreferenceManager.getDefaultSharedPreferences(SlideMenuWithActivityGroup.this);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			String emailId = sharedPrefs.getString(Globals.sUserEmailIdKey, "");
			//String emailId = "ksuriya@semasoft.co.in";
			this.emailId = emailId;
			this.days = days;
		}

		@Override
		protected Void doInBackground(Void... params) {
			String url = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetAndroidSubScribedGroup";
			String urlParameter = getJsonDataForNooorPlus(emailId,UserFunctions.getMacAddress(SlideMenuWithActivityGroup.this),productId);
			response = newWebService(url,urlParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			if (response != null && !response.equals("")) {
				HashMap<String, Object> jsonList = null;
				try {
					JSONObject array = new JSONObject(response);
					jsonList = (HashMap<String, Object>) toMap(array);
					for (int i = 0; i < jsonList.size(); i++) {
						boolean success = (boolean) jsonList.get("Success");
						if (success) {
							ArrayList<Object> subsList = (ArrayList<Object>) jsonList.get("Subscription");
							String endDate = "";
							if (subsList.size()>0) {
								HashMap<String, Object> getSubscription = (HashMap<String, Object>) subsList.get(subsList.size() - 1);
								endDate = (String) getSubscription.get("EndDate");
							}
							SharedPreferences.Editor editor = sharedPrefs.edit();
							editor.putString(Globals.nooorPlusEndDate, endDate);
							editor.commit();
							break;
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			if (processDialog != null) {
				processDialog.dismiss();
			}
		}

		private Map<String, Object> toMap(JSONObject object) throws JSONException {
			Map<String, Object> map = new HashMap<String, Object>();
			Iterator<String> keysIter = object.keys();
			while (keysIter.hasNext()) {
				String key = keysIter.next();
				Object value = object.get(key);
				if (value instanceof JSONArray) {
					value = toList((JSONArray) value);
				} else if (value instanceof JSONObject) {
					value = toMap((JSONObject) value);
				}
				map.put(key, value);
			}
			return map;
		}

		private List<Object> toList(JSONArray array) throws JSONException {
			List<Object> list = new ArrayList<Object>();
			for (int i = 0; i < array.length(); i++) {
				Object value = array.get(i);
				if (value instanceof JSONArray) {
					value = toList((JSONArray) value);
				} else if (value instanceof JSONObject) {
					value = toMap((JSONObject) value);
				}
				list.add(value);
			}
			return list;
		}
	}
	private String getJsonDataForNooorPlus(String emailId,String macId,String productId){
		String activationType = "0";
		String maskEmailId = "1";
		String activationCode = "";
		JSONObject student2 = new JSONObject();
		try {
			student2.put("ActivationCode", activationCode);
			student2.put("ActivationMac_ID", macId);
			student2.put("ActivationType", activationType);
			student2.put("AppStoreID",productId);
			student2.put("EmailId",emailId);
			student2.put("EmailMaskId",maskEmailId);
			student2.put("Group","");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return student2.toString();
	}


	/*private void storeData(String serverIp, String portNumber, String clientIdentity) {
		Log.e("status","data");
		try {
			int status = 0;
			if (serverIp.equals(SERVER_IP)) {
				status = 1;
			} else if (portNumber.equals(SERVER_PORT))
				status = 2;
			else if (clientIdentity.equals(CLIENT_IDENTITY))
				status = 3;
			if (status != 3) {
				storeDataInToPrefs(serverIp, portNumber, clientIdentity);
				try {
					Intent intent = getIntent();
					this.startActivity(intent);
				//	initMQTT();

				} catch (Exception e) {
					Log.e("stro", e.toString());
				}
			}
		}catch (Exception e){
			Log.e("stat",e.toString());
		}
	}*/
	/*private void storeDataInToPrefs(String serverIp, String portNumber, String clientIdentity) {
		try {
			SharedPreferences mSharedPreferences = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
			SharedPreferences.Editor mEditor = mSharedPreferences.edit();
			mEditor.putString(Constants.SERVER_IP, serverIp);
			mEditor.putString(Constants.SERVER_PORT, portNumber);
			mEditor.putString(Constants.CLIENT_IDENTITY, clientIdentity);
			mEditor.commit();
		}catch (Exception e){
			Log.e("storin pref",e.toString());
		}

	}
*/
	protected String getSaltString() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 5) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}
	private void callMQTT(){
		subscribeToInAppCommunication();
		String randomstring= getSaltString();

		ManahijApp.getInstance().getPrefManager().addClientName( "noor"+randomstring);

		try {
			Intent intent = getIntent();
			this.startActivity(intent);
			//	initMQTT();

		} catch (Exception e) {
			Log.e("stro", e.toString());
		}
		((ManahijApp) getApplication()).startSubscriptionService();
		//storeData("174.129.30.141","1883","noor"+randomstring);
		/*getServerData();
		if (!SERVER_IP.isEmpty() && !CLIENT_IDENTITY.isEmpty()) {
			if (SERVER_PORT.isEmpty()) {
				SERVER_PORT = "1883";
			}
			((ManahijApp) getApplication()).startSubscriptionService();
		}*/
		//initMQTT();
	}
	private void initMQTT() {
		try {



			if(TopicName==null||TopicName.isEmpty()) {
				if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
					TopicName = ManahijApp.getInstance().getPrefManager().getMainChannel();
					//startService(new Intent(SlideMenuWithActivityGroup.this, CallConnectionStatusService.class));
				} else {
					TopicName = ManahijApp.getInstance().getPrefManager().getSubscribeTopicName();
				}
			}

			if (TopicName!=null&&!TopicName.isEmpty()){
				Log.e("init",TopicName);
				if(ManahijApp.getInstance().isInternetAvailable()) {
					subscribeToNewTopic(TopicName);
					sendUsersList(String.valueOf(1));
				}
			}
		}catch (Exception e){
			Log.e("init",e.toString());
		}

	}

	private void subscribeToInAppCommunication() {

		try {
			IntentFilter mIntentFilter = new IntentFilter();
			mIntentFilter.addAction(Constants.BROADCAST_ACTION_MQTT_STATUS);
			this.registerReceiver(mInAppCommunicator, mIntentFilter);
		}catch (Exception e){
			Log.e("subc",e.toString());
		}
	}
	/*private void getServerData() {
		SharedPreferences mSharedPreferences = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);
		SERVER_IP = mSharedPreferences.getString(Constants.SERVER_IP, "");
		SERVER_PORT = mSharedPreferences.getString(Constants.SERVER_PORT, "");
		CLIENT_IDENTITY = mSharedPreferences.getString(Constants.CLIENT_IDENTITY, "");

	}*/
	public static String SERVER_IP;
	public static String SERVER_PORT;
	public static String CLIENT_IDENTITY;

	public int MQTT_STATUS = 0;


	BroadcastReceiver mInAppCommunicator = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(Constants.BROADCAST_ACTION_MQTT_STATUS)) {
				final int status = intent.getExtras().getInt(Constants.MQTT_STATUS);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {

							updateMqttConnectioStatus(status);
						}catch (Exception e){
							Log.e("reciever",e.toString());
						}
					}
				});
			}
		}

	};

	private void subscribeToNewTopic(String name) {
		Log.e("in subscribe","checking");
		if (MQTT_STATUS == 1) {
			Intent mIntent = new Intent(Constants.BROADCAST_ACTION_SUBSCRIBE_TO_SINGLE_TOPIC);
			mIntent.putExtra(Constants.TOPIC_NAME, name);
			mIntent.putExtra(Constants.TOPIC_SUBSCRIPTION_STATUS, 1);
			sendBroadcast(mIntent);
		}
	}

	private void updateMqttConnectioStatus(int status) {
		MQTT_STATUS = status;
		//updatedUiStatus();
		if (status == 0) {

			if(ManahijApp.getInstance().isInternetAvailable()) {
			sendUsersList(String.valueOf(0));}
			//  restartSubscriptionService();
			//connectionLostDailog();

		} else if (status == 1) {
			//sendUsersList(String.valueOf(1));
			//mqttdialog.dismiss();
			initMQTT();
		}
	}

/*	private void connectionLostDailog() {

		 mqttdialog = new Dialog(SlideMenuWithActivityGroup.this); // Context, this, etc.
		mqttdialog.setContentView(R.layout.connectionlostdailog);
		mqttdialog.setCancelable(false);
		Button cancel = (Button) mqttdialog.findViewById(R.id.cancelconnection);
		Button ok = (Button) mqttdialog.findViewById(R.id.connect);

		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((ManahijApp) getApplication()).stopSubscriptionService();

				mqttdialog.dismiss();
			}
		});
		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// ((ManahijApp) getApplication()).stopSubscriptionService();
				Log.e("came","came");
				stopService(new Intent(SlideMenuWithActivityGroup.this, MQTTSubscriptionService.class));
				startService(new Intent(SlideMenuWithActivityGroup.this, MQTTSubscriptionService.class));
				*//*((ManahijApp) getApplication()).startSubscriptionService();
				callMQTT();*//*
				mqttdialog.dismiss();
			}
		});
		mqttdialog.show();
	}*/


	public  String TopicName;
	/*private void updatedUiStatus() {
		try {
			if (MQTT_STATUS == 0){
				sendUsersList(String.valueOf(0));
				Log.e("not connected","not connected");}
			else {
				sendUsersList(String.valueOf(1));
				Log.e("connected", "connected");
				// SharedPreferences pref = getSharedPreferences(Constants.SHARINGNAME, MODE_PRIVATE);

				// SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(SlideMenuWithActivityGroup.this);
				if(TopicName==null)
					if(ManahijApp.getInstance().getPrefManager().getISMainChannel())
						TopicName= ManahijApp.getInstance().getPrefManager().getMainChannel();     *//*prefs.getString(Globals.sUserEmailIdKey, ""); *//**//*pref.getString(Constants.MYTOPICNAME,null);*//*
					else TopicName= ManahijApp.getInstance().getPrefManager().getSubscribeTopicName();

				subscribeToNewTopic(TopicName);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}*/
	public void sendUsersList(String status){
		if(ManahijApp.getInstance().isInternetAvailable()) {
			Log.e("publish", "noor users" + status);
			if (!status.isEmpty()) {
				String mainStatus = null;
				if (status.equals("1")) {
					if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
						mainStatus = String.valueOf(2);
					} else {
						mainStatus = status;
					}
				} else {
					mainStatus = status;
				}

				Intent mIntent = new Intent();
				mIntent.setAction(Constants.BROADCAST_ACTION_PUBLISH);
				mIntent.putExtra(Constants.PUBLISH_TOPIC, Constants.NOORUSERSTATUS);
				String msg = ManahijApp.getInstance().getPrefManager().getSubscribeTopicName() + ":" + mainStatus;
				Log.e("msg", msg);

				byte[] data;
				String base64msg = "";
				try {
					data = msg.getBytes();
					base64msg = Base64.encodeToString(data, Base64.DEFAULT);
					base64msg = base64msg.replace("\n", "");
					Log.e("Base 64 ", base64msg);

				} catch (Exception e) {

					e.printStackTrace();

				}


				mIntent.putExtra(Constants.PUBLISH_MSG, base64msg);
				sendBroadcast(mIntent);
			/*try{
				//ManahijApp.getInstance().getPrefManager().addContactTopictoPublish(users.getEmailId());
				// MQTTSubscriptionService service = new MQTTSubscriptionService();
				JSONObject obj = new JSONObject();
				obj.put("type",Constants.KEYCALLCONNECTION);
				obj.put("action","call_request");
				obj.put("user_name",userName);
				obj.put("topic_name",ManahijApp.getInstance().getPrefManager().getSubscribeTopicName());
				obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());


				service.publishMsg(ManahijApp.getInstance().getPrefManager().getContactTopictoPublish(),obj.toString(),SlideMenuWithActivityGroup.this);
				//  Log.e("req",users.getEmailId()+obj.toString());
          *//*
            service. sendUsersList(users.getEmailId(),String.valueOf(2),exportEnrActivity);*//*
			}catch (Exception e){
				Log.e("ex",e.toString());
			}*/
			}

		}

	}


	public void creatingCallUI(final Context exportEnrActivity,  final MyContacts users) {
		///final Dialog dialog = new Dialog(exportEnrActivity); // Context, this, etc.
		//Log.e("coming","comingusers"+users.getEmailId()+users.isOnline);
		if(users.isOnline!=null) {
			dialog.setContentView(R.layout.call_user);
			dialog.setCancelable(false);
			RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.onlineStatus);
			LinearLayout layout2 = (LinearLayout) dialog.findViewById(R.id.callingStatus);
			TextView ok = (TextView) dialog.findViewById(R.id.ok);
			ImageView cancel = (ImageView) dialog.findViewById(R.id.canclestatus);
			TextView status = (TextView) dialog.findViewById(R.id.status);
			TextView usname = (TextView) dialog.findViewById(R.id.usname);
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

			String userName = prefs.getString(Globals.sUserNameKey, "");
			if (userName != null) {
				usname.setText(users.getUserName());
			}
			if (users.isOnline().equals("0")) {
				status.setText("User is Offline");
				layout2.setVisibility(View.GONE);
				layout.setVisibility(View.VISIBLE);
			} else if (users.isOnline().equals("2")) {
				layout2.setVisibility(View.GONE);
				layout.setVisibility(View.VISIBLE);
				status.setText("User is Busy");
			} else {
				try {
					ManahijApp.getInstance().getPrefManager().addContactTopictoPublish(users.getEmailId());
					// MQTTSubscriptionService service = new MQTTSubscriptionService();
					JSONObject obj = new JSONObject();
					obj.put("type", Constants.KEYCALLCONNECTION);
					obj.put("action", "call_request");
					obj.put("user_name", userName);
					obj.put("topic_name", ManahijApp.getInstance().getPrefManager().getSubscribeTopicName());
					obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());


					service.publishMsg(users.getEmailId(), obj.toString(), exportEnrActivity);
					//  Log.e("req",users.getEmailId()+obj.toString());
          /*
            service. sendUsersList(users.getEmailId(),String.valueOf(2),exportEnrActivity);*/
				} catch (Exception e) {
					Log.e("excall", e.toString());
				}
				dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
				layout.setVisibility(View.GONE);
				layout2.setVisibility(View.VISIBLE);
			}
			ok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					app.setCallState(true);
					dialog.dismiss();
				}
			});

			cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						ManahijApp.getInstance().getPrefManager().addContactTopictoPublish(users.getEmailId());
						// MQTTSubscriptionService service = new MQTTSubscriptionService();
						app.setCallState(false);
						JSONObject obj = new JSONObject();
						obj.put("type", Constants.KEYCALLCONNECTION);
						obj.put("action", "call_cancel");
						obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
						service.publishMsg(users.getEmailId(),obj.toString(),exportEnrActivity);

					} catch (Exception e) {
						Log.e("ex", e.toString());
					}
					dialog.dismiss();
				}
			});

			dialog.show();
		}

	}
	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();
		app.setState(false);
		ManahijApp.getInstance().getPrefManager().addIsinBackground(false);
		LocalBroadcastManager.getInstance(this).registerReceiver(msgreceiver, new IntentFilter(FILTER));
		IntentFilter mIntentFilter = new IntentFilter();
		mIntentFilter.addAction(Constants.BROADCAST_ACTION_MQTT_STATUS);
		this.registerReceiver(mInAppCommunicator,mIntentFilter);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
		/*if(!  app.getState()){
			stopService(new Intent(SlideMenuWithActivityGroup.this, CallConnectionStatusService.class));
		}*/
		LocalBroadcastManager.getInstance(this).unregisterReceiver(msgreceiver);
		this.unregisterReceiver(mInAppCommunicator);
	}

	BroadcastReceiver msgreceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
			/*	switch (intent.getStringExtra(Constants.KEYTYPE)) {
					case Constants.KEYCALLCONNECTIONREQUEST:*/
						String message = intent.getStringExtra(Constants.KEYMESSAGE);
						JSONObject object = new JSONObject(message);
						/*if(object.getString("action").equals("call_accept")){
							// callbackInterface.onStatusUpdate(true);
							String subscribetopicname=   object.getString("topic_to_subsribe");
							ManahijApp.getInstance().getPrefManager().addMainChannel(subscribetopicname);
							ManahijApp.getInstance().getPrefManager().addIsMainChannel(true);
							service.unSubScribeTopic(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(),context);
							service.subscribeToNewTopic(subscribetopicname,context);
							service.sendUsersList(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(),String.valueOf(2),context);
							startService(new Intent(SlideMenuWithActivityGroup.this, CallConnectionStatusService.class));
							dialog.dismiss();
							Toast.makeText(context,"Call connected",Toast.LENGTH_LONG).show();
						}else {
							// startService(new Intent(SlideMenuWithActivityGroup.this, CallConnectionStatusService.class));
							// callbackInterface.onStatusUpdate(true);
							dialog.dismiss();
							Toast.makeText(context,"Call Rejected",Toast.LENGTH_LONG).show();
						}*/
                    switch (object.getString("action")){

						case "call_accept":
							String subscribetopicname=   object.getString("topic_to_subsribe");
							ManahijApp.getInstance().getPrefManager().addMainChannel(subscribetopicname);
							ManahijApp.getInstance().getPrefManager().addIsMainChannel(true);
							service.unSubScribeTopic(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(),context);
							service.subscribeToNewTopic(subscribetopicname,context);
							service.sendUsersList(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(),String.valueOf(2),context);
							startService(new Intent(SlideMenuWithActivityGroup.this, CallConnectionStatusService.class));
							dialog.dismiss();
							db.updateInCall();
							app.setCallState(true);
							Toast.makeText(context,"Call connected",Toast.LENGTH_LONG).show();
							break;
						case "call_reject":
							dialog.dismiss();

							app.setCallState(false);
							Toast.makeText(context,"Call Rejected",Toast.LENGTH_LONG).show();
					          break;
						case "user_is_in_another_call":
							dialog.dismiss();
							Toast.makeText(context,"User is in another call",Toast.LENGTH_LONG).show();
							break;
					}




						//break;
				//}

              /*  if(intent.getStringExtra(Constants.SubscribeSTATUS).equals("subscribed"))
                    sendUsersList(String.valueOf(1));*/

			}catch (Exception e){
				Log.e("Erecive",e.toString());
			}


		}
	};

	@Override
	public void onDestroy() {
		super.onDestroy();

		uiHelper.onDestroy();
		if (subscription.mHelper != null) subscription.mHelper.dispose();
		subscription.mHelper = null;
		subscription = null;
		if(ManahijApp.getInstance().isInternetAvailable()) {
			sendUsersList(String.valueOf(0));
			service.unSubScribeTopic(ManahijApp.getInstance().getPrefManager().getMainChannel(), this);
		}
		((ManahijApp) getApplication()).stopSubscriptionService();
		unregisterReceiver(mNetworkStateIntentReceiver);
       /* CallConnectionStatusService se= new CallConnectionStatusService();
        se.stopSelf();*/
		/*
		* uiHelper.onDestroy();
		if (subscription.mHelper != null) subscription.mHelper.dispose();
		subscription.mHelper = null;
		subscription = null;
		unregisterReceiver(mNetworkStateIntentReceiver);
		*
		*
		* */
	}
}



