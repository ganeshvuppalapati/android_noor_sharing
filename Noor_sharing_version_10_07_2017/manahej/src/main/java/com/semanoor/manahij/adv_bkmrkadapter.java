package com.semanoor.manahij;


import java.io.File;
import java.util.ArrayList;

import com.semanoor.sboookauthor_store.BookMarkEnrichments;
import com.semanoor.source_sboookauthor.Globals;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import android.widget.TextView;

public class adv_bkmrkadapter extends BaseAdapter {


	private Context context;
	ArrayList<BookMarkEnrichments> bookMarkTabsList;
	private boolean done_editing;
	boolean bkInEditMode;

	public adv_bkmrkadapter(Context context, ArrayList<BookMarkEnrichments> bookMarkTabsList)
	{

		/*if(context instanceof  BookViewReadActivity) {
			this.BookViewReadActivity = (BookViewReadActivity) context;
		}else{
			this.pdfContext= (pdfActivity) context;
		}*/
		this.context=context;
		this.bookMarkTabsList = bookMarkTabsList;
	}

	@Override
	public int getCount() {
		return bookMarkTabsList.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder{
		public ImageView imgView;
		public EditText etTxtView;
		public TextView textview;
		public Button  btn_del;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)

	{
		View view = convertView;
		final ViewHolder holder;
		if (convertView == null) {
			if (context instanceof pdfActivity) {
				//this.pdfContext= (pdfActivity) context;
				view = ((pdfActivity)context).getLayoutInflater().inflate(R.layout.advbkmarktitles, null);
             } else {
				view = ((BookViewReadActivity)context).getLayoutInflater().inflate(R.layout.advbkmarktitles, null);
             }
			//view = context.getLayoutInflater().inflate(R.layout.advbkmarktitles, null);
			holder=new ViewHolder();
			holder.imgView=(ImageView)view.findViewById(R.id.iv_advsearch);
			holder.etTxtView=(EditText)view.findViewById(R.id.et_bkmarktitle);
			holder.btn_del=(Button)view.findViewById(R.id.btn_del);
			holder.textview=(TextView)view.findViewById(R.id.tv_title);
			view.setTag(holder);

		} else {
			holder=(ViewHolder)view.getTag();
		}
		if (context instanceof pdfActivity) {
			if (((pdfActivity)context).bkmInDeleteMode) {
				holder.btn_del.setVisibility(View.VISIBLE);
			} else {
				holder.btn_del.setVisibility(View.INVISIBLE);
			}
		}else{
			if (((BookViewReadActivity)context).bkmInDeleteMode) {
				holder.btn_del.setVisibility(View.VISIBLE);
			} else {
				holder.btn_del.setVisibility(View.INVISIBLE);
			}
		}
		  holder.btn_del.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//bookMarkTitles.get(position).getEnrichmentTitle()
				int id=bookMarkTabsList.get(position).getEnrichmentId();
				int pageno=bookMarkTabsList.get(position).getEnrichmentPageNo();
				bookMarkTabsList.remove(position);
				String Path;
				if (context instanceof pdfActivity) {
				//	((pdfActivity)context).removingBkMrkButton(position, pageno, id);
					 Path = Globals.TARGET_BASE_BOOKS_DIR_PATH+((pdfActivity)context).currentBook.getBookID()+"/"+"bkmarkimages/";

				}else{
					((BookViewReadActivity)context).removingBkMrkButton(position, pageno, id);
					 Path = Globals.TARGET_BASE_BOOKS_DIR_PATH+((BookViewReadActivity)context).currentBook.getBookID()+"/"+"bkmarkimages/";

				}

				String correct= Path+id+".png";
				
				File file=new File(correct);
				if (file.exists()) {
					file.delete();
				}
			}
		});
		if (context instanceof pdfActivity) {
			bkInEditMode=((pdfActivity)context).bkmInEditMode;
		} else {
			bkInEditMode=((BookViewReadActivity)context).bkmInEditMode;
		}
		    ////System.out.println(holder.etTxtView.getText().toString()+"text");
		if(bkInEditMode){
			holder.textview.setVisibility(View.INVISIBLE);
			holder.etTxtView.setVisibility(View.VISIBLE);
			holder.etTxtView.setText(bookMarkTabsList.get(position).getEnrichmentTitle());
			holder.etTxtView.setBackgroundResource(R.drawable.border);
			//et_bkmrktitle.setEnabled(true);
			holder.etTxtView.setFocusableInTouchMode(true);
			done_editing=true;
			
		}else{
			holder.etTxtView.setBackgroundResource(R.drawable.edit_text_without_border);
		   
			    holder.etTxtView.setFocusableInTouchMode(false);
				holder.textview.setVisibility(View.VISIBLE);
				holder.etTxtView.setVisibility(View.INVISIBLE);
                holder.textview.setText(bookMarkTabsList.get(position).getEnrichmentTitle());
				holder.etTxtView.setText(bookMarkTabsList.get(position).getEnrichmentTitle());

				holder.imgView.setVisibility(View.INVISIBLE);
			    String enrichPath;
			if (context instanceof pdfActivity) {
				 enrichPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ((pdfActivity)context).currentBook.getBookID() + "/" + "bkmarkimages/";
			}else{
				enrichPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ((BookViewReadActivity)context).currentBook.getBookID() + "/" + "bkmarkimages/";
			}
				String objContent= enrichPath+(bookMarkTabsList.get(position).getEnrichmentId())+".png";
				
				File file1=new File(objContent);
				if (file1.exists()) {
					Bitmap bitmap = BitmapFactory.decodeFile(objContent);
					holder.imgView.setImageBitmap(bitmap);
					holder.imgView.setVisibility(View.VISIBLE);
				}
			
		   }  
		   holder.etTxtView.setOnFocusChangeListener(new OnFocusChangeListener() {

			   @Override
			   public void onFocusChange(View arg0, boolean arg1) {
				   //System.out.println(position+"position");
				   //System.out.println(holder.etTxtView.getText().toString()+"text");
				   String newtext = holder.etTxtView.getText().toString();
				   bookMarkTabsList.get(position).setEnrichmentTitle(newtext);
				   String title = bookMarkTabsList.get(position).getEnrichmentTitle();
				   title = title.replace("'", "''");
				   if (context instanceof pdfActivity) {
					   ((pdfActivity)context).db.executeQuery("update  BookmarkedSearchTabs set Title='" + title + "'where Id='" + bookMarkTabsList.get(position).getEnrichmentId() + "'and PageNo= '" + bookMarkTabsList.get(position).getEnrichmentPageNo() + "' ");
					   ((pdfActivity)context).updateNameToButton(position, newtext);
				   }else{
					   ((BookViewReadActivity)context).db.executeQuery("update  BookmarkedSearchTabs set Title='" + title + "'where Id='" + bookMarkTabsList.get(position).getEnrichmentId() + "'and PageNo= '" + bookMarkTabsList.get(position).getEnrichmentPageNo() + "' ");
					   ((BookViewReadActivity)context).updateNameToButton(position, newtext);

				   }
			   }
		   });
		return view;
	}

}



