package com.semanoor.manahij;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.Display;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobeta.android.dslv.DragSortListView;
import com.mobeta.android.dslv.DragSortListView.DropListener;
import com.mobeta.android.dslv.DragSortListView.RemoveListener;
import com.ptg.mindmap.widget.AppDict;
import com.ptg.mindmap.widget.LoadMindMapContent;
import com.ptg.views.CircleButton;
import com.ptg.views.TwoDScrollView;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.manahij.MultiDirectionSlidingDrawer.OnDrawerCloseListener;
import com.semanoor.manahij.MultiDirectionSlidingDrawer.OnDrawerOpenListener;
import com.semanoor.paint.BrushPreset;
import com.semanoor.paint.PainterCanvas;
import com.semanoor.sboookauthor_store.BookMarkEnrichments;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.sboookauthor_store.RenderHTMLObjects;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.ChangePageBackground;
import com.semanoor.source_sboookauthor.ClearObjects;
import com.semanoor.source_sboookauthor.CustomDesignScrollView;
import com.semanoor.source_sboookauthor.CustomImageRelativeLayout;
import com.semanoor.source_sboookauthor.CustomWebRelativeLayout;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.GenerateHTML;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.MindMapDialog;
import com.semanoor.source_sboookauthor.Page;
import com.semanoor.source_sboookauthor.PageAdapter;
import com.semanoor.source_sboookauthor.PageTemplateAdapter;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.PopoverView.PopoverViewDelegate;
import com.semanoor.source_sboookauthor.PopupColorPicker;
import com.semanoor.source_sboookauthor.PopupColorPicker.ColorPickerListener;
import com.semanoor.source_sboookauthor.Quiz;
import com.semanoor.source_sboookauthor.UndoRedoObjectData;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.advAdapter;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Random;

public class BookViewActivity extends Activity implements OnClickListener, OnTouchListener, OnItemClickListener, DropListener, RemoveListener, OnItemLongClickListener, PopoverViewDelegate, OnGlobalLayoutListener {

    public Book currentBook;
    public DragSortListView pageListView;
    public ImageView bgImgView;
    public RelativeLayout designPageLayout, pageView, rootView, keyboardToolbar, mainDesignView;
    public CustomDesignScrollView designScrollPageView;
    public PageAdapter pageAdapter;
    public DatabaseHandler db;
    public String currentPageNumber = "1";
    private UserFunctions userFunctions;
    public int pageCount;
    public Page page;
    public Globals global;

    private View currentSelectedListView;
    int gestureStartPointX = 0, gestureStartPointY = 0;
    int gestureEndPointX = 0, gestureEndPointY = 0;

    //Background variables
    private Button btnAddImageView, btnAddWebView, btnAddVideo, btnAddBackground, btnDraw, btnPen, btnBrush, btnEarser, btnClearDrawing, btnDeleteObject, btnPreview, btnClear, btnEnrich, btnAddAudio, btnAddPageNo, btnAddWidgets, pageView_handle, btnFileManger,btnCloudManager;
    Button btnAddPage;
    public CircleButton btnUndo, btnRedo,btnBack;
    public ImageButton btn_BrushColor;
    public Button btnPaintUndo, btnPaintRedo;
    public MultiDirectionSlidingDrawer mDrawer;
    private RelativeLayout drawingToolbar, objectsToolbar;
    private Button drawFinished;
    //private Button btnApplyAll, btnApply, btnCancel;
    Point changeBackgroundPoint, clearallobjects, drawPnt, widgetPnt;
    private int PAGE_BACKGROUND_GALLERY = 3;
    private int ActivityFileManager = 145;
    private int cloudActivity_callback = 143;

    private SeekBar mBrushSize, mBrushBlurRadius;
    private Spinner mBrushBlurStyle;

    private Dialog brushSettingsDialog;
    public MatrixCursor cursor;
    public boolean listViewOnLongClick;

    //Keyboard toolbar buttons
    public Button btnKeyToolbar_pickColor, btnKeyToolbar_pickFont;
    private TextView tvKeyToolbar_fontSize;
    private String audio_outputFile = null;
    private Button btn_record, Search;

    private MediaRecorder mRecorder;

    private GestureDetector gestureDetector;

    private int mLastHeightDifference;

    public boolean pickColorFromBgView = false;

    ImageView pickClrImgView;
    public String pickedColorFor;
    public WebView wv_advSearch;
    public RelativeLayout rlTabsLayout, bkmarkLayout,enr_layout;
    public LinearLayout enrTabsLayout, bookmark_container;
    public HorizontalScrollView enrScrollView;
    public HorizontalScrollView bookmrk_sv;
    public ArrayList<Enrichments> enrichmentTabListArray;
    public ArrayList<BookMarkEnrichments> enrichmentbkmrkList;
    public TwoDScrollView twoDScroll;
    public ViewGroup drawingView;
    public RelativeLayout canvasView;
    Button btnEnrHome;
    Button btn_bkmrkpopup;
    public boolean isEnrichmentCreated;

    private PageTemplateAdapter pageTemplateListAdapter;

    private DrawerLayout drawerLayout;
    public ArrayList<Integer> enrichTabCountListAllPages;

    public ProgressBar progressBar;

    public static boolean ineditmode;
    public static boolean deletemode;
    PopoverView advbkmarkpopup;
    public static boolean bkmrk_dismiss;
    ListView lv_bkmrktitle;
    static Dialog sharePopUp;
    String advtitle;
    public RelativeLayout topBarLayout,backBtnLayout;
    public RelativeLayout undoRedoLayout;
    public RelativeLayout mindMapSliderlayout;
    private boolean isInPaintMode;
    private int navigateEnrichId;
    public GridShelf gridShelf;
    String brushColor = null;
    public boolean mindMapEdited = false;

    public boolean backClicked=false;
    public boolean titleTextClicked=false;
    public RenderHTMLObjects webView;
    public Subscription subscription;
    public static String ITEM_SKU;
    public IabHelper mHelper;
    private int WebviewRequestCode = 9;
    public Groups groups;
    public View mDrapView;
    private GestureDetector mGestureDetector;
    public RecyclerView recyclerView;
    private RecyclerView subRecyclerView,subRecylerView1;
    public String isCreateBlankEnr;
    private int catId = 0;
    private boolean isLoadEnrInpopview;
    private View gestureView;
    private PopoverView popoverView;
    private RelativeLayout subTabsLayout,subTabLayout1;
    public RelativeLayout add_btn_layout;
    private Button add_btn,search_img,arrow_btn,search_btn;
    private EditText et_search;
    private LinearLayout search_layout,icon_layout;
    public Enrichments currentBtnEnrichment;
    private RecyclerView clickableRecyclerView;
    private TreeStructureView treeStructureView;
    private String selectedSearch = "Google";
    private int categoryID;
    private InternetSearch internetSearch;
    private CircleButton dropdown_btn;
    private SeekBar opacity_seekbar,brushsize_seekbar;
    private Button btn_brush_new,btn_eraser_new,btn_clear_new;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title Bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.book_view);
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
        ViewGroup rootView_font = (ViewGroup) findViewById(android.R.id.content).getRootView();

        UserFunctions.changeFont(rootView_font,font);
        currentBook = (Book) getIntent().getSerializableExtra("Book");
        gridShelf = (GridShelf) getIntent().getSerializableExtra("Shelf");
        // int pageNumber = getIntent().getIntExtra("currentPageNo", 1);
        int pageNumber = getIntent().getIntExtra("currentPageNo", 1);
        currentPageNumber = String.valueOf(pageNumber);
        navigateEnrichId = getIntent().getIntExtra("enrichmentTabId", 0);
        isCreateBlankEnr = getIntent().getStringExtra("createBlankEnr");
        catId = getIntent().getIntExtra("categoryId",0);

        //set screen orientation based on the current book orientation value
        if (currentBook.getBookOrientation() == Globals.portrait) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
//        ComponentName prev=this.getCallingActivity();
//        System.out.println(prev);
//        activity=prev.getClass();
        groups = Groups.getInstance();
        db = DatabaseHandler.getInstance(this);
        userFunctions = UserFunctions.getInstance();
        global = Globals.getInstance();
        subscription = Subscription.getInstance();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        global.setDeviceWidth(size.x);
        global.setDeviceHeight(size.y);

        pageCount = currentBook.getTotalPages();
        add_btn_layout = (RelativeLayout) findViewById(R.id.add_layout);
        add_btn = (Button) findViewById(R.id.add_btn_tab);
        add_btn.setOnClickListener(this);
        enr_layout = (RelativeLayout) findViewById(R.id.enr_layout);
        if (Integer.parseInt(currentPageNumber) != 1 || Integer.parseInt(currentPageNumber) != currentBook.getTotalPages()-1) {
            enr_layout.setVisibility(View.GONE);
        }
        rlTabsLayout = (RelativeLayout) findViewById(R.id.rl_tabs_layout);
        subTabLayout1 = (RelativeLayout) findViewById(R.id.rl_tabs_layout2);
        subTabsLayout = (RelativeLayout) findViewById(R.id.rl_tabs_layout1);
        subRecyclerView = (RecyclerView) findViewById(R.id.gridView2);
        subRecylerView1 = (RecyclerView) findViewById(R.id.gridView3);
        dropdown_btn = (CircleButton) findViewById(R.id.dropdown_btn);
        dropdown_btn.setOnClickListener(this);
        enrTabsLayout = (LinearLayout) findViewById(R.id.enr_linearLayout);
        search_layout = (LinearLayout) findViewById(R.id.internet_search_layout);
        icon_layout = (LinearLayout) findViewById(R.id.icon_layout);
        search_img = (Button) findViewById(R.id.search_image);
        search_img.setOnClickListener(this);
        arrow_btn = (Button) findViewById(R.id.arrow_btn);
        arrow_btn.setOnClickListener(this);
        et_search = (EditText) findViewById(R.id.et_search);
        search_btn = (Button) findViewById(R.id.btn_search);
        search_btn.setOnClickListener(this);
        enrScrollView = (HorizontalScrollView) findViewById(R.id.enr_scrollView);
        recyclerView= (RecyclerView)findViewById(R.id.gridView);
        btnEnrHome = (Button) findViewById(R.id.btnEnrHome);
        btnEnrHome.setOnClickListener(this);

        internetSearch = new InternetSearch(search_layout,et_search,BookViewActivity.this,search_img);

        bkmarkLayout = (RelativeLayout) findViewById(R.id.rl_bkmark_layout);
        bookmrk_sv = (HorizontalScrollView) findViewById(R.id.btn_bkmark_Sv);
        bookmark_container = (LinearLayout) findViewById(R.id.ll_bookmark_container);
        btn_bkmrkpopup = (Button) findViewById(R.id.btn_popup);
        btn_bkmrkpopup.setOnClickListener(this);
        backBtnLayout = (RelativeLayout) findViewById(R.id.relLayout);
        //bookmrk_sv.setHorizontalFadingEdgeEnabled(false);

		mainDesignView = (RelativeLayout) findViewById(R.id.mainDesignView);
		wv_advSearch=(WebView)findViewById(R.id.wv_advsearch);
		wv_advSearch.getSettings().setLoadWithOverviewMode(true);
		wv_advSearch.getSettings().setUseWideViewPort(true);
		wv_advSearch.getSettings().setJavaScriptEnabled(true);
        bgImgView = (ImageView) findViewById(R.id.bgImgView);
        pageView = (RelativeLayout) findViewById(R.id.PageView);
        designScrollPageView = (CustomDesignScrollView) findViewById(R.id.designScrollView);
        designPageLayout = (RelativeLayout) findViewById(R.id.DesignPageLayout);
        designPageLayout.setOnTouchListener(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        pageView_handle = (Button) findViewById(R.id.page_handler);
        pageView_handle.setOnClickListener(this);
        Button btn_lvHide = (Button) findViewById(R.id.btn_lv_close);
        Button btn_import = (Button)findViewById(R.id.btn_importPage);
        Button button_search = (Button) findViewById(R.id.button_search);
        button_search.setOnClickListener(this);
        btn_lvHide.setOnClickListener(this);
        btn_import.setOnClickListener(this);
        webView = (RenderHTMLObjects)findViewById(R.id.webView1);
        /*if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab()) {
			mainDesignView.removeView(designPageLayout);
			designScrollPageView.setVisibility(View.VISIBLE);
			designScrollPageView.addView(designPageLayout);
		}*/

        designPageLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                Globals.setDesignPageWidth(designPageLayout.getMeasuredWidth());
                Globals.setDesignPageHeight(designPageLayout.getMeasuredHeight());
                Globals.setMainDesignPageWidth(mainDesignView.getMeasuredWidth());
                Globals.setMainDesignPageHeight(mainDesignView.getMeasuredHeight());
            }
        });

        btnUndo = (CircleButton) findViewById(R.id.btn_undo);
        btnUndo.setOnClickListener(this);
        btnRedo = (CircleButton) findViewById(R.id.btn_redo);
        btnRedo.setOnClickListener(this);

        pageListView = (DragSortListView) findViewById(R.id.PagelistView);
        String[] cols = {"name"};
        int[] ids = {R.id.text};

        if (currentBook.is_bStoreBook()) {
            enrichTabCountListAllPages = new ArrayList<Integer>();
        } else {
            enrichTabCountListAllPages = new ArrayList<Integer>();
        }
        cursor = new MatrixCursor(new String[]{"_id", "name"});
        String strPage = getResources().getString(R.string.page);
        for (int i = 0; i < pageCount; i++) {
            if (i == 0) {
                cursor.newRow()
                        .add(i)
                        .add(R.string.cover_page);
            } else if (i == pageCount - 1) {
                cursor.newRow()
                        .add(i)
                        .add(R.string.end_page);
            } else {
                cursor.newRow()
                        .add(i)
                        .add(strPage + ": " + i);
            }
            if (currentBook.is_bStoreBook()) {
                int enrichTabCount = db.getCountForEnrichmentTabList(currentBook.getBookID(), i + 1);
                enrichTabCountListAllPages.add(enrichTabCount);
            } else {
                int enrichTabCount = db.getCountForEnrichmentTabList(currentBook.getBookID(), i + 1);
                enrichTabCountListAllPages.add(enrichTabCount);
            }
        }

        mindMapSliderlayout = (RelativeLayout) findViewById(R.id.mindmap_sliderview);
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setMax(255);
        seekBar.setProgress(255);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                bgImgView.setAlpha((float) progress / 255);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        ImageButton btnEdit = (ImageButton) findViewById(R.id.btn_edit);
        btnEdit.setOnClickListener(this);


        pageAdapter = new PageAdapter(this, R.layout.page_view, null, cols, ids, 0);
        pageAdapter.changeCursor(cursor);
        //currentPageNumber = String.valueOf(currentBook.get_lastViewedPage());
        checkForEnrichments(Integer.parseInt(currentPageNumber), navigateEnrichId);
        checkForBkmrkEnrichments(Integer.parseInt(currentPageNumber));
        page = (Page) new Page(this, db, global, currentPageNumber, navigateEnrichId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        pageListView.setAdapter(pageAdapter);
        pageListView.setOnItemClickListener(this);
        pageListView.setDropListener(this);
        pageListView.setRemoveListener(this);
        if (!currentBook.is_bStoreBook()) {
            pageListView.setOnItemLongClickListener(this);
        }
        pageListView.smoothScrollToPosition(Integer.parseInt(currentPageNumber) - 1);
        pageListView.setSelection(Integer.parseInt(currentPageNumber) - 1);
        btnAddImageView = (Button) findViewById(R.id.btn_add_image);
        btnAddImageView.setOnClickListener(this);
        btnAddVideo = (Button) findViewById(R.id.btn_add_video);
        btnAddVideo.setOnClickListener(this);
        btnAddWebView = (Button) findViewById(R.id.btn_add_text);
        btnAddWebView.setOnClickListener(this);
        btnAddPage = (Button) findViewById(R.id.btn_addPage);
        btnAddPage.setOnClickListener(this);
        btnAddBackground = (Button) findViewById(R.id.btn_add_background);
        btnAddBackground.setOnClickListener(this);
        btnBack = (CircleButton) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(this);
        btnDraw = (Button) findViewById(R.id.btn_draw);
        btnDraw.setOnClickListener(this);
        btnAddWidgets = (Button) findViewById(R.id.btn_add_widgets);
        btnAddWidgets.setOnClickListener(this);
        btnPen = (Button) findViewById(R.id.btn_pen);
        btnPen.setOnClickListener(this);
        btnBrush = (Button) findViewById(R.id.btn_brush);
        btnBrush.setOnClickListener(this);
        btnEarser = (Button) findViewById(R.id.btn_eraser);
        btnEarser.setOnClickListener(this);
        btnClearDrawing = (Button) findViewById(R.id.btn_clear);
        btnClearDrawing.setOnClickListener(this);
        btnPaintUndo = (Button) findViewById(R.id.btn_paint_undo);
        btnPaintUndo.setOnClickListener(this);
        btnPaintRedo = (Button) findViewById(R.id.btn_paint_redo);
        btnPaintRedo.setOnClickListener(this);
        objectsToolbar = (RelativeLayout) findViewById(R.id.toolBar_view);
        drawingToolbar = (RelativeLayout) findViewById(R.id.draw_view);
        drawFinished = (Button) findViewById(R.id.btn_done);
        drawFinished.setOnClickListener(this);
        btnDeleteObject = (Button) findViewById(R.id.btn_delete_object);
        btnDeleteObject.setOnClickListener(this);
        btnPreview = (Button) findViewById(R.id.btn_preview);
        String query = "select * from objects where BID='"+currentBook.getBookID()+"'order by SequentialID";
        ArrayList<Object> objectList = db.getAllObjectsFromPage(query);
//        if(!currentBook.is_bStoreBook() && objectList.size()>2&& currentBook.getDownloadURL()!=null) {
//            btnPreview.setVisibility(View.GONE);
//        }else{
            btnPreview.setVisibility(View.VISIBLE);
        //}
       // btnPreview.setVisibility(View.GONE);
        btn_brush_new = (Button) findViewById(R.id.btn_brush_new);
        btn_brush_new.setOnClickListener(this);
        btn_eraser_new = (Button) findViewById(R.id.btn_eraser_new);
        btn_eraser_new.setOnClickListener(this);
        btn_clear_new = (Button) findViewById(R.id.btn_clear_new);
        btn_clear_new.setOnClickListener(this);
        opacity_seekbar = (SeekBar) findViewById(R.id.opacity);
        brushsize_seekbar = (SeekBar) findViewById(R.id.brush_size);
        brushsize_seekbar.setProgress(5);
        opacity_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                page.canvasView.setPresetBlur(1,progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        brushsize_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress > 0) {
                    if (fromUser) {
                        page.canvasView.setPresetSize(seekBar.getProgress());
                    }
                } else {
                    brushsize_seekbar.setProgress(1);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 0) {
                    page.canvasView.setPresetSize(seekBar.getProgress());
                }
            }
        });
        btnPreview.setOnClickListener(this);
        btnClear = (Button) findViewById(R.id.btn_clear_page);
        btnClear.setOnClickListener(this);
        btnEnrich = (Button) findViewById(R.id.btnEnrich);
        btnEnrich.setOnClickListener(this);
        btnAddAudio = (Button) findViewById(R.id.btn_add_audio);
        btnAddAudio.setOnClickListener(this);
        btnAddPageNo = (Button) findViewById(R.id.btn_add_page_no);
        btnAddPageNo.setOnClickListener(this);
        Search = (Button) findViewById(R.id.Search);
        Search.setOnClickListener(this);
        Search.setVisibility(View.GONE);
        btnFileManger = (Button) findViewById(R.id.btnFileManager);
        btnFileManger.setOnClickListener(this);
        btnCloudManager= (Button) findViewById(R.id.btnCloudManager);
        btnCloudManager.setOnClickListener(this);
        if (Integer.parseInt(currentPageNumber) == 1 || Integer.parseInt(currentPageNumber) == pageCount) {
            btnAddPageNo.setEnabled(false);
        }

        onWindowFocusChanged(true);

        //Disable apply all for cover and end pages

        keyboardToolbar = (RelativeLayout) findViewById(R.id.keyboard_toolbar);
        rootView = (RelativeLayout) findViewById(R.id.root_view);
        rootView.setOnTouchListener(this);
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(this);
        btnKeyToolbar_pickColor = (Button) findViewById(R.id.btn_keyboardToolbar_pickColor);
        btnKeyToolbar_pickColor.setOnClickListener(this);
        btnKeyToolbar_pickFont = (Button) findViewById(R.id.btn_keyboardToolbar_pickFont);
        btnKeyToolbar_pickFont.setOnClickListener(this);
        Button btnKeyToolbar_fontIncrease = (Button) findViewById(R.id.btn_keyboardToolbar_fontIncrease);
        btnKeyToolbar_fontIncrease.setOnClickListener(this);
        tvKeyToolbar_fontSize = (TextView) findViewById(R.id.tv_keyboardToolbar_fontSize);
        Button btnKeyToolbar_fontDecrease = (Button) findViewById(R.id.btn_keyboardToolbar_fontDecrease);
        btnKeyToolbar_fontDecrease.setOnClickListener(this);
        Button btnKeyToolbar_textAlignLeft = (Button) findViewById(R.id.btn_keyboardToolbar_textAlign_left);
        btnKeyToolbar_textAlignLeft.setOnClickListener(this);
        Button btnKeyToolbar_textAlignCenter = (Button) findViewById(R.id.btn_keyboardToolbar_textAlign_center);
        btnKeyToolbar_textAlignCenter.setOnClickListener(this);
        Button btnKeyToolbar_textAlignRight = (Button) findViewById(R.id.btn_keyboardToolbar_textAlign_right);
        btnKeyToolbar_textAlignRight.setOnClickListener(this);
        Button btnKeyToolbar_textAlignJustify = (Button) findViewById(R.id.btn_keyboardToolbar_textAlign_justify);
        btnKeyToolbar_textAlignJustify.setOnClickListener(this);
        Button btnKeyToolbar_orderedList = (Button) findViewById(R.id.btn_keyboardToolbar_orderedList);
        btnKeyToolbar_orderedList.setOnClickListener(this);
        Button btnKeyToolbar_unOrderedList = (Button) findViewById(R.id.btn_keyboardToolbar_unorderedList);
        btnKeyToolbar_unOrderedList.setOnClickListener(this);
        Button btnKeyToolbar_done = (Button) findViewById(R.id.btn_keyboardToolbar_done);
        btnKeyToolbar_done.setOnClickListener(this);

        gestureDetector = new GestureDetector(this, new GestureListener());

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setScrimColor(Color.TRANSPARENT);
        drawerLayout.setOnTouchListener(this);

        mDrawer = (MultiDirectionSlidingDrawer) findViewById(R.id.bottom_drawer);
        undoRedoLayout = (RelativeLayout) findViewById(R.id.undo_redo_layout);
        topBarLayout = (RelativeLayout) findViewById(R.id.top_bar);
        mDrawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {

            @Override
            public void onDrawerOpened() {
                if (!isInPaintMode) {
                    undoRedoLayout.setVisibility(View.VISIBLE);
                    topBarLayout.setVisibility(View.VISIBLE);
                    backBtnLayout.setVisibility(View.VISIBLE);
                    page.displayAlphaSliderView();
                }
            }
        });

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    internetSearch.searchClicked(new InternetSearchCallBack() {
                        @Override
                        public void onClick(String url, String title) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
                            new createAdvanceSearchTab(url, title).execute();
                        }
                    });
                }
                return false;
            }
        });

        mDrawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {

            @Override
            public void onDrawerClosed() {
//                if (enrichmentbkmrkList.size()>0){
//                    bkmarkLayout.setVisibility(View.GONE);
//                }
                undoRedoLayout.setVisibility(View.GONE);
                topBarLayout.setVisibility(View.GONE);
                backBtnLayout.setVisibility(View.GONE);
                mindMapSliderlayout.setVisibility(View.GONE);
            }
        });

        showPageListView();
        mDrawer.open();

        enableOrDisableToolbarButtons();
        if (isCreateBlankEnr!=null && !isCreateBlankEnr.equals("")){
            enr_layout.setVisibility(View.GONE);
            pageView_handle.setVisibility(View.GONE);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }


        btnAddPage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
         //       if(!Globals.isLimitedVersion()) {
                    final PopoverView pageImportpopup = new PopoverView(BookViewActivity.this, R.layout.export_page);
                    pageImportpopup.setContentSizeForViewInPopover(new Point((int) getResources().getDimension(R.dimen.page_exportDialog_width), (int) getResources().getDimension(R.dimen.page_exportDialog_height)));

                    // popoverView.setContentSizeForViewInPopover(new Point((int) 150, 120));
                    //popoverView.setDelegate(BookViewActivity.this);
                    pageImportpopup.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionDown, true);
                    Button btn_addPage = (Button) pageImportpopup.findViewById(R.id.btn_addPage);
                    Button btn_import = (Button) pageImportpopup.findViewById(R.id.btn_import);
                    btn_import.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pageImportpopup.dissmissPopover(true);
                            PageExportDialog page_Export = new PageExportDialog(BookViewActivity.this, db, gridShelf);
                            page_Export.showBookDialog();
                        }
                    });
                    btn_addPage.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pageImportpopup.dissmissPopover(true);
                            addingNewPage();
                        }
                    });
           //     }
                return true;
            }
        });
    }

    /**
     * enableOrdisableToolbar buttons
     */
    private void enableOrDisableToolbarButtons() {
        if (currentBook.is_bStoreBook() || currentBook.getbCategoryId() == 6) {
            if (navigateEnrichId == 0) {
                disableToolbarButtons();
            } else {
                enableToolbarButtons();
            }
            for (Enrichments enrichments : enrichmentTabListArray) {
                if (enrichments.getEnrichmentId() == navigateEnrichId) {
                    if (enrichments.getEnrichmentType().equals("Search") || enrichments.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
                        disableToolbarButtons();
                        break;
                    }
                }
            }
            if (currentBook.is_bStoreBook()) {
                btnAddPage.setVisibility(View.INVISIBLE);
            }
            btnAddPageNo.setVisibility(View.GONE);
            hidePageListView();
            btnEnrich.setVisibility(View.GONE);
        } else {
            btnEnrich.setVisibility(View.GONE);
        }
        if (isCreateBlankEnr != null && !isCreateBlankEnr.equals("")) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final int maxEnrichId = db.getMaxUniqueRowID("enrichments") + 1;
                    //pageTemplateListAdapter.addDefaultTemplate(position+1, Integer.parseInt(currentPageNumber), maxEnrichId);
                    addPageNumberObject(Integer.parseInt(currentPageNumber), maxEnrichId);
                    if (isCreateBlankEnr.equals("blank")) {
                        createEnrichments("blank", maxEnrichId);
                    } else if (isCreateBlankEnr.equals("duplicate")) {
                        createEnrichments("duplicate", maxEnrichId);
                    }
                    //popoverView.dissmissPopover(true);
                    int enrTabCount = enrichTabCountListAllPages.get(Integer.parseInt(currentPageNumber) - 1);
                    enrTabCount = enrTabCount + 1;
                    enrichTabCountListAllPages.set(Integer.parseInt(currentPageNumber) - 1, enrTabCount);
                    enableToolbarButtons();
                    pageListView.invalidateViews();
                }
            });
        }
    }

    /**
     * Disable all the toolbar buttons
     */
    public void disableToolbarButtons() {
        btnAddWebView.setEnabled(false);
        btnAddImageView.setEnabled(false);
        btnAddVideo.setEnabled(false);
        btnDraw.setEnabled(false);
        btnAddWidgets.setEnabled(false);
        btnAddBackground.setEnabled(false);
        btnClear.setEnabled(false);
        btnAddAudio.setEnabled(false);
        btnDeleteObject.setEnabled(false);
        btnAddPageNo.setEnabled(false);
        if (currentBook.get_bStoreID()!=null &&currentBook.is_bStoreBook() ||currentBook.get_bStoreID()!=null&&currentBook.get_bStoreID().contains("C")||currentBook.get_bStoreID()!=null &&currentBook.get_bStoreID().contains("P")) {
            btnAddPage.setEnabled(false);
            btnAddPage.setVisibility(View.INVISIBLE);
        }
        btnFileManger.setEnabled(false);
    }

    /**
     * Enable All the toolbar buttons
     */
    public void enableToolbarButtons() {
        btnAddWebView.setEnabled(true);
        btnAddImageView.setEnabled(true);
        btnAddVideo.setEnabled(true);
        btnDraw.setEnabled(true);
        btnAddWidgets.setEnabled(true);
        btnAddBackground.setEnabled(true);
        btnClear.setEnabled(true);
        btnAddAudio.setEnabled(true);
        btnDeleteObject.setEnabled(true);
        btnAddPageNo.setEnabled(true);
        btnFileManger.setEnabled(true);
    }

    /**
     * Keyboard toolbar listener
     */
    @Override
    public void onGlobalLayout() {
        if (page.currentView instanceof CustomWebRelativeLayout) {
            possiblyResizeChildOfContent();
        }
    }

    /**
     * possibly make the rootview to move above the keyboard
     */
    private void possiblyResizeChildOfContent() {
        int usableHeightNow = computeUsableHeight(designPageLayout);
        int usableheightSansKeyboard = designPageLayout.getRootView().getHeight();
        int heightDifference = (usableheightSansKeyboard - usableHeightNow);
        if (heightDifference > (usableheightSansKeyboard / 4) && heightDifference != mLastHeightDifference) {
            //Keyboard in Vissible
            if (((CustomWebRelativeLayout) (page.currentView)).getObjType().equals("WebText") || ((CustomWebRelativeLayout) (page.currentView)).getObjType().equals(Globals.OBJTYPE_TITLETEXT) || ((CustomWebRelativeLayout) (page.currentView)).getObjType().equals(Globals.OBJTYPE_AUTHORTEXT)) {
                keyboardToolbar.setVisibility(View.VISIBLE);
                keyboardToolbar.setY(usableHeightNow - keyboardToolbar.getHeight());
                updateKeyboardToolBarButtons();
            }
            int currentTouchYPos = ((CustomWebRelativeLayout) (page.currentView)).prevY;
            Rect keyboardRect = new Rect(0, usableHeightNow - (keyboardToolbar.getHeight() * 2), rootView.getRootView().getWidth(), (usableHeightNow - keyboardToolbar.getHeight()) + (heightDifference + keyboardToolbar.getHeight()));
            Rect currentViewRect = new Rect((int) page.currentView.getX(), (int) page.currentView.getY(), (int) (page.currentView.getX() + page.currentView.getWidth()), (int) (page.currentView.getY() + page.currentView.getHeight()));
            if (currentTouchYPos > keyboardRect.top) {
                //System.out.println("Views Intersected");
                //Keyboard and current view intersected and should make the rootview to move up
                if (((CustomWebRelativeLayout) (page.currentView)).getObjScalePageToFit().equals("no")) {
                    int val1 = (int) (page.currentView.getY() + page.currentView.getHeight());
                    int val2 = usableHeightNow - (keyboardToolbar.getHeight() * 2);
                    int currentViewHeihtDiff = val1 - val2;
                    if (val1 > usableheightSansKeyboard - (keyboardToolbar.getHeight() * 2)) {
                        //if current view Y position more than rootview height minus toolbar height, then make sure that rootview not placed above keyboard toolbar
                        designPageLayout.setY(-(currentViewHeihtDiff - keyboardToolbar.getHeight()));
                    } else {
                        designPageLayout.setY(-currentViewHeihtDiff);
                    }
                } else {
                    designPageLayout.setY(-(heightDifference));
                }
            } else if (currentViewRect.intersect(keyboardRect)) {
                designPageLayout.setY(-keyboardToolbar.getHeight());
            }
            mLastHeightDifference = heightDifference;
            ((CustomWebRelativeLayout) page.currentView).prevObjContent = ((CustomWebRelativeLayout) page.currentView).getObjectContent();
        } else if (heightDifference != mLastHeightDifference) {
            //Keyboard in Vissible
            ((CustomWebRelativeLayout) page.currentView).isURObjContent = true;
            ((CustomWebRelativeLayout) page.currentView).getAndSetWebViewContent();
            ((CustomWebRelativeLayout) page.currentView).getAndSetSelectionIsActive();
            keyboardToolbar.setVisibility(View.GONE);
            mLastHeightDifference = heightDifference;
            keyboardToolbar.setY(usableHeightNow);
            designPageLayout.setY(0);
            ((CustomWebRelativeLayout) page.currentView).getAndUpdateWebViewContentHeight(((CustomWebRelativeLayout) page.currentView).webView);
        }
    }

    /**
     * get window visible frame height
     *
     * @param view
     * @return
     */
    private int computeUsableHeight(View view) {
        Window w = getWindow();
		/*Rect rect1=new Rect();
		w.getDecorView().getWindowVisibleDisplayFrame(rect1);
		int defaultHeight=rect1.top;*/
        ////System.out.println(defaultHeight+"defaultHeight");
        ////System.out.println(exactcontextbarheight+"contextbarheight");
        int cont = w.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        //int exactcontextbarheight=(cont-rect.top)+rect.top;
        Rect rect = new Rect();
        view.getWindowVisibleDisplayFrame(rect);
        return (rect.bottom - rect.top) - (cont - rect.top);
    }

    @Override
    public void onActionModeStarted(ActionMode mode) {
        // TODO Auto-generated method stub
        super.onActionModeStarted(mode);
        //System.out.println("Context menu started");
        if (page.currentView instanceof CustomWebRelativeLayout) {
            ((CustomWebRelativeLayout) page.currentView).setOnTouchListenerToNullForWebView();
            //Globals.showKeyboard(this);
        }
    }

    @Override
    public void onActionModeFinished(ActionMode mode) {
        // TODO Auto-generated method stub
        super.onActionModeFinished(mode);
        //System.out.println("Context menu finished");
        if (page.currentView instanceof CustomWebRelativeLayout) {
            ((CustomWebRelativeLayout) page.currentView).setOnTouchListenerFroWebView();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_add_image: {
                if (groups.isImageController()) {
                    hidePageListView();
                    int imageWidth = (int) getResources().getDimension(R.dimen.bookview_initial_image_width);
                    int imageHeight = (int) getResources().getDimension(R.dimen.bookview_initial_image_width);
                    int imageXpos = designPageLayout.getWidth() / 2 - imageWidth / 2;
                    int imageYpos = designPageLayout.getHeight() / 2 - imageHeight / 2;
                    String imageType = "Image";
                    String objFormat = "_defaultbg.png";
                    addNewImageRL(imageWidth, imageHeight, imageXpos, imageYpos, imageType, objFormat, null);
                }else{
                    showNooorPlusDialog();
                }
                break;
            }
            case R.id.btn_add_video: {
                if (groups.isVideoController()) {
                    hidePageListView();
                    int imageWidth = (int) getResources().getDimension(R.dimen.bookview_initial_video_height);
                    int imageHeight = (int) getResources().getDimension(R.dimen.bookview_initial_video_height);
                    int imageXpos = designPageLayout.getWidth() / 2 - imageWidth / 2;
                    int imageYpos = designPageLayout.getHeight() / 2 - imageHeight / 2;
                    String imageType = "YouTube";
                    String objFormat = ".mp4";
                    addNewImageRL(imageWidth, imageHeight, imageXpos, imageYpos, imageType, objFormat, null);
                }else{
                    showNooorPlusDialog();
                }
                break;
            }
            case R.id.btn_add_text: {
                if (groups.isTextController()) {
                    hidePageListView();
                    int initialWebTextWidth = (int) getResources().getDimension(R.dimen.bookview_initial_text_width);
                    int initialWebTextHeight = (int) getResources().getDimension(R.dimen.bookview_initial_text_height);
                    int initialWebTextXpos = designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
                    int initialWebTextYpos = designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
                    String location = initialWebTextXpos + "|" + initialWebTextYpos;
                    String size = initialWebTextWidth + "|" + initialWebTextHeight;
                    String objType = "WebText";
                    addNewWebRL(location, size, objType, null);
                }else{
                    showNooorPlusDialog();
                }
                break;
            }

            case R.id.btn_add_audio: {
                if (groups.isSoundRecorder()) {
                    hidePageListView();
                    showAudioPopOverView(v);
                }else{
                    showNooorPlusDialog();
                }
                break;
            }

            case R.id.btn_add_page_no: {
                hidePageListView();
                final int initPageNoSize = (int) getResources().getDimension(R.dimen.bookview_initial_text_page_no_size);
                final int initPageNoPos = (int) getResources().getDimension(R.dimen.bookview_initial_text_page_no_pos);
                final PopoverView popoverView = new PopoverView(this, R.layout.popover_page_no);
                popoverView.setContentSizeForViewInPopover(new Point((int) getResources().getDimension(R.dimen.bookview_page_no_popover_width), (int) getResources().getDimension(R.dimen.bookview_page_no_popover_height)));
                popoverView.setDelegate(this);
                popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionDown, true);
                Button btn_lu = (Button) popoverView.findViewById(R.id.btn_lu);
                Button btn_cu = (Button) popoverView.findViewById(R.id.btn_cu);
                Button btn_ru = (Button) popoverView.findViewById(R.id.btn_ru);
                Button btn_ld = (Button) popoverView.findViewById(R.id.btn_ld);
                Button btn_cd = (Button) popoverView.findViewById(R.id.btn_cd);
                Button btn_rd = (Button) popoverView.findViewById(R.id.btn_rd);

                btn_lu.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int initialWebTextXpos = initPageNoPos;
                        int initialWebTextYpos = initPageNoPos;
                        showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
                        popoverView.dissmissPopover(true);
                    }
                });
                btn_cu.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int initialWebTextXpos = designPageLayout.getWidth() / 2 - initPageNoSize / 2;
                        int initialWebTextYpos = initPageNoPos;
                        showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
                        popoverView.dissmissPopover(true);
                    }
                });
                btn_ru.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int initialWebTextXpos = (designPageLayout.getWidth() - initPageNoPos) - initPageNoSize;
                        int initialWebTextYpos = initPageNoPos;
                        showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
                        popoverView.dissmissPopover(true);
                    }
                });
                btn_ld.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int initialWebTextXpos = initPageNoPos;
                        int initialWebTextYpos = (designPageLayout.getHeight() - initPageNoPos) - initPageNoSize;
                        showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
                        popoverView.dissmissPopover(true);
                    }
                });
                btn_cd.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int initialWebTextXpos = designPageLayout.getWidth() / 2 - initPageNoSize / 2;
                        int initialWebTextYpos = (designPageLayout.getHeight() - initPageNoPos) - initPageNoSize;
                        showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
                        popoverView.dissmissPopover(true);
                    }
                });
                btn_rd.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        int initialWebTextXpos = (designPageLayout.getWidth() - initPageNoPos) - initPageNoSize;
                        int initialWebTextYpos = (designPageLayout.getHeight() - initPageNoPos) - initPageNoSize;
                        showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
                        popoverView.dissmissPopover(true);
                    }
                });
                break;
            }

            case R.id.btn_addPage: {
                addingNewPage();
               /*popoverView.dissmissPopover(true);
				}
			});*/

                break;
            }
            case R.id.btn_importPage:{
                PageExportDialog page_Export = new PageExportDialog(BookViewActivity.this, db, gridShelf);
                page_Export.showBookDialog();
                break;
            }
            case R.id.button_search:{
                if (groups.isSearchInWeb()) {
                    if (!currentBook.is_bStoreBook()){
                        hidePageListView();
                        Intent web_intent = new Intent(BookViewActivity.this,WebBookActivity.class);
                        web_intent.putExtra("Shelf", gridShelf);
                        web_intent.putExtra("book",currentBook);
                        web_intent.putExtra("From","FromBook");
                        startActivityForResult(web_intent, WebviewRequestCode);
                    }else {
                        advanceSearchPopup();
                    }
                }else{
                    showNooorPlusDialog();
                }
                break;
            }
            case R.id.btn_add_background: {
                hidePageListView();
                ChangePageBackground changeBackground = new ChangePageBackground(this);
                changeBackground.callPageBgPopwidnow(btnAddBackground);
                //takePageBackgroundFromGallery();
                //Update in the DB.
                break;
            }
            case R.id.btn_back: {
                onBackPressed();
                break;
            }
            case R.id.btn_delete_object: {
                hidePageListView();
                deleteSelectedObject();
                break;
            }
            case R.id.btn_draw: {
                hidePageListView();
                showShapesPopUpWindow();
                break;
            }
            case R.id.btn_add_widgets: {
                hidePageListView();
                showWidgetsPopUpWindow();
                break;
            }
            case R.id.btn_done: {
                isInPaintMode = false;
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                pageView_handle.setVisibility(View.VISIBLE);
                new saveDrawnTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                drawingToolbar.setVisibility(View.GONE);
                objectsToolbar.setVisibility(View.VISIBLE);
                backBtnLayout.setVisibility(View.VISIBLE);
                topBarLayout.setVisibility(View.VISIBLE);
                undoRedoLayout.setVisibility(View.VISIBLE);
                btnBack.setVisibility(View.VISIBLE);
                page.displayAlphaSliderView();
                designScrollPageView.setEnableScrolling(true);
                if (btnPen.isSelected() || btnEarser.isSelected()) {
                    btnPen.setSelected(false);
                    btnEarser.setSelected(false);
                }
                if (currentBook.is_bStoreBook()) {
                    if (!currentBook.isStoreBookCreatedFromTab()) {
                        designScrollPageView.setEnableScrolling(true);
                    }
//                    if (enrichmentTabListArray.size() > 0) {
//                        rlTabsLayout.setVisibility(View.VISIBLE);
//                    }
//                    if (enrichmentbkmrkList.size() > 0) {
//                        bkmarkLayout.setVisibility(View.VISIBLE);
//                    }
                }
                break;
            }
            case R.id.btn_pen: {
                if (!btnPen.isSelected()) {
                    btnPen.setSelected(true);
                }
                btnEarser.setSelected(false);
                page.canvasView.getThread().activate();
                page.canvasView.setPresetPorterMode(null);
                break;
            }
            case R.id.btn_brush: {
                showBrushSettingsDialog();
                break;
            }
            case R.id.btn_eraser: {
                if (!btnEarser.isSelected()) {
                    btnEarser.setSelected(true);
                }
                btnPen.setSelected(false);
                page.canvasView.setPresetPorterMode(new PorterDuffXfermode(Mode.CLEAR));
                break;
            }
            case R.id.btn_clear: {
                page.canvasView.clearView();
                break;
            }
            case R.id.btn_paint_undo: {
                page.canvasView.paintUndo();
                break;
            }
            case R.id.btn_paint_redo: {
                page.canvasView.paintRedo();
                break;
            }
            case R.id.btn_preview: {
                hidePageListView();
                page.cancel(true);
                if (page.currentView != null) {
                    page.currentView.clearFocus();
                }
                page.saveAllObjectsInCurrentPage(currentPageNumber);

                Intent previewIntent = new Intent(getApplicationContext(), PreviewActivity.class);
                previewIntent.putExtra("Book", currentBook);
                previewIntent.putExtra("currentPageNo", currentPageNumber);
                Globals.deletedObjectList = page.deletedObjectsListArray;
                startActivity(previewIntent);
                break;
            }
            case R.id.btn_clear_page: {
                hidePageListView();
                ClearObjects clearobjects = new ClearObjects(this);
                clearobjects.clearObjectsPopwidnow(btnClear);
                break;
            }
            //KeyBoard Toolbar Buttons
            case R.id.btn_keyboardToolbar_pickColor: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {

                        page.currentView.clearFocus();
                    }
                    ((CustomWebRelativeLayout) page.currentView).getAndSetSelectionIsActive();
                    if (currentBook.getBookOrientation() == Globals.portrait) {
                        ((CustomWebRelativeLayout) page.currentView).showPopOverColorPickerDialogToChangeFontColor(v);
                    } else {
                        page.hideKeyboard();
                        ((CustomWebRelativeLayout) page.currentView).showPopUpColorPickerDialog(null);
                    }
                }
                break;
            }
            case R.id.btn_keyboardToolbar_pickFont: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        page.currentView.clearFocus();
                    }
                    ((CustomWebRelativeLayout) page.currentView).getAndSetSelectionIsActive();
                    if (currentBook.getBookOrientation() == Globals.portrait) {
                        ((CustomWebRelativeLayout) page.currentView).showPopOverFontPickerDialog(v);
                    } else {
                        page.hideKeyboard();
                        ((CustomWebRelativeLayout) page.currentView).showFontListDialog();
                    }
                }
                break;
            }
            case R.id.btn_keyboardToolbar_fontIncrease: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        page.currentView.clearFocus();
                    }
                    ((CustomWebRelativeLayout) page.currentView).getAndSetSelectionIsActive();
                    int font_size = Integer.parseInt(tvKeyToolbar_fontSize.getText().toString());
                    if (font_size <= 72) {
                        font_size++;
                        ((CustomWebRelativeLayout) page.currentView).changeFontSize(font_size, "content");
                        tvKeyToolbar_fontSize.setText(String.valueOf(font_size));
                    }
                }
                break;
            }
            case R.id.btn_keyboardToolbar_fontDecrease: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        page.currentView.clearFocus();
                    }
                    ((CustomWebRelativeLayout) page.currentView).getAndSetSelectionIsActive();
                    int font_size = Integer.parseInt(tvKeyToolbar_fontSize.getText().toString());
                    if (font_size >= 8) {
                        font_size--;
                        ((CustomWebRelativeLayout) page.currentView).changeFontSize(font_size, "content");
                        tvKeyToolbar_fontSize.setText(String.valueOf(font_size));
                    }
                }
                break;
            }
            case R.id.btn_keyboardToolbar_textAlign_left: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        page.currentView.clearFocus();
                    }
                    ((CustomWebRelativeLayout) page.currentView).textAlign("Left", "content");
                }
                break;
            }
            case R.id.btn_keyboardToolbar_textAlign_center: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        page.currentView.clearFocus();
                    }
                    ((CustomWebRelativeLayout) page.currentView).textAlign("Center", "content");
                }
                break;
            }
            case R.id.btn_keyboardToolbar_textAlign_right: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        page.currentView.clearFocus();
                    }
                    ((CustomWebRelativeLayout) page.currentView).textAlign("Right", "content");
                }
                break;
            }
            case R.id.btn_keyboardToolbar_textAlign_justify: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        page.currentView.clearFocus();
                    }
                    ((CustomWebRelativeLayout) page.currentView).textAlign("Justify", "content");
                }
                break;
            }
            case R.id.btn_keyboardToolbar_orderedList: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        page.currentView.clearFocus();
                    }
                    ((CustomWebRelativeLayout) page.currentView).insertOrderedList();
                }
                break;
            }
            case R.id.btn_keyboardToolbar_unorderedList: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                        page.currentView.clearFocus();
                    }
                    ((CustomWebRelativeLayout) page.currentView).insertUnOrderedList();
                }
                break;
            }
            case R.id.btn_keyboardToolbar_done: {
                if (page.currentView instanceof CustomWebRelativeLayout) {
                    page.hideKeyboard();
                }
                break;
            }
            case R.id.btnEnrich: {
                if (Integer.parseInt(currentPageNumber) == 1 || Integer.parseInt(currentPageNumber) == pageCount) {
                    UserFunctions.DisplayAlertDialog(this, R.string.cover_page_cannot_enriched, R.string.cover_page);
                } else {
                    hidePageListView();
                    showEnrichDialog(v);
                }
                break;
            }
            case R.id.btnEnrHome: {
                if (page.enrichedPageId != 0) {
                    wv_advSearch.setVisibility(View.INVISIBLE);
                    if (mainDesignView.getChildCount() == 4) {
                        mainDesignView.removeViewAt(3);
                    }

//				canvasView.setVisibility(View.INVISIBLE);
//				twoDScroll.setVisibility(View.INVISIBLE);
//				drawingView.setVisibility(View.INVISIBLE);
                    bgImgView.setVisibility(View.VISIBLE);
                    designScrollPageView.setVisibility(View.VISIBLE);
                    makeEnrichedBtnSlected(v);
                    instantiateNewPage(Integer.parseInt(currentPageNumber), 0);
                    if (keyboardToolbar.getVisibility()==View.VISIBLE){
                        keyboardToolbar.setVisibility(View.GONE);
                    }
                }
                break;
            }
            case R.id.page_handler: {
                showPageListView();
                break;
            }
            case R.id.btn_lv_close: {
                hidePageListView();
                break;
            }
            case R.id.btnEnrichmentTab: {
                final Enrichments enrich = (Enrichments) v.getTag();
                if (enrich.getEnrichmentType().equals("Search")) {
                    if (mainDesignView.getChildCount() == 4) {
                        mainDesignView.removeViewAt(3);
                    }
                    enrTabsLayout.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    wv_advSearch.setVisibility(View.VISIBLE);
                    designScrollPageView.setVisibility(View.INVISIBLE);
                    wv_advSearch.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                    wv_advSearch.getSettings().setJavaScriptEnabled(true);
                    wv_advSearch.setWebChromeClient(new WebChromeClient());
                    wv_advSearch.getSettings().setAllowContentAccess(true);
                    wv_advSearch.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                    wv_advSearch.getSettings().setAllowFileAccess(true);
                    wv_advSearch.getSettings().setPluginState(PluginState.ON);
                    wv_advSearch.getSettings().setDomStorageEnabled(true);
                    wv_advSearch.setWebViewClient(new WebViewClient());
                    advtitle = enrich.getEnrichmentTitle();
                    if (enrich.getEnrichmentType().equals("Search")) {
                        if (enrich.getEnrichmentTitle().equals("BookPage")){
                            String[] split = enrich.getEnrichmentPath().split("/");
                            String filePath = "file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/";
                            File hyperPage = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/"+split[split.length-1]);
                            String srcString = UserFunctions.decryptFile(hyperPage);
                            wv_advSearch.loadDataWithBaseURL(filePath, srcString, "text/html", "utf-8", null);
                        }else{
                            wv_advSearch.loadUrl(enrich.getEnrichmentPath());
                        }
                    }
                    wv_advSearch.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onPageFinished(WebView view, String url) {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
                    if (enrich.getEnrichmentType().equals("Search")) {
                        wv_advSearch.setOnTouchListener(new OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                wv_advSearch.setWebViewClient(new CustomWebViewClient());
                                return false;
                            }
                        });
                    }
                    bgImgView.setVisibility(View.INVISIBLE);

                } else if (enrich.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
                    wv_advSearch.setVisibility(View.INVISIBLE);
                    if (mainDesignView.getChildCount() == 4) {
                        mainDesignView.removeViewAt(3);
                    }
                    loadMindMap(enrich.getEnrichmentPath(),mainDesignView,BookViewActivity.this);
                    if (progressBar.getVisibility()==View.VISIBLE){
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                } else {
                    if (mainDesignView.getChildCount() == 4) {
                        mainDesignView.removeViewAt(3);
                    }
                    designScrollPageView.setVisibility(View.VISIBLE);
                    bgImgView.setVisibility(View.VISIBLE);
                }

                if (page.enrichedPageId != enrich.getEnrichmentId()) {
                    makeEnrichedBtnSlected(v);
                    instantiateNewPage(Integer.parseInt(currentPageNumber), enrich.getEnrichmentId());
                    if (enrich.getEnrichmentType().equals("Search") || enrich.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
                        disableToolbarButtons();
                    } else {
                        enableToolbarButtons();
                    }
                }
//                mDrapView = v;                                      //Rearranging tabs
//                ClipData data = ClipData.newPlainText("", "");
//                MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(
//                        v);
//                v.startDrag(data, shadowBuilder, v, 0);
                if (keyboardToolbar.getVisibility()==View.VISIBLE){
                    keyboardToolbar.setVisibility(View.GONE);
                }
                break;
            }
            case R.id.btn_undo: {
                page.undo();
                break;
            }
            case R.id.btn_redo: {
                page.redo();
                break;
            }
            case R.id.btn_popup: {
                ineditmode = false;
                deletemode = false;

                advbkmarkpopup = new PopoverView(BookViewActivity.this, R.layout.advsearchtitles);
                advbkmarkpopup.setContentSizeForViewInPopover(new Point((int) getResources().getDimension(R.dimen.info_popover_width), (int) getResources().getDimension(R.dimen.web_info_height)));
                advbkmarkpopup.setDelegate(BookViewActivity.this);
                advbkmarkpopup.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(btn_bkmrkpopup), PopoverView.PopoverArrowDirectionUp, true);
                lv_bkmrktitle = (ListView) advbkmarkpopup.findViewById(R.id.lv_bkmark);
                lv_bkmrktitle.setAdapter(new bkmrk_adapter(BookViewActivity.this, enrichmentbkmrkList));
                final Button btn_edit = (Button) advbkmarkpopup.findViewById(R.id.btn_edit);
                final Button bt_del = (Button) advbkmarkpopup.findViewById(R.id.btn_del);
                lv_bkmrktitle.setOnItemClickListener(new OnItemClickListener() {

                    @Override

                    public void onItemClick(AdapterView<?> adapterView, View v, int position, long arg3) {
                        new CreatingAdvsearchEnrichments(enrichmentbkmrkList.get(position).getEnrichmentTitle(), enrichmentbkmrkList.get(position).getEnrichmentPath()).execute();
                        //CreatingAdvsearchEnrichments(enrichmentbkmrkList.get(position).getEnrichmentTitle(),enrichmentbkmrkList.get(position).getEnrichmentPath());
                        bkmrk_dismiss = true;
                        advbkmarkpopup.dissmissPopover(true);
                    }
                });
                btn_edit.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        if (!ineditmode) {
                            ineditmode = true;
                            lv_bkmrktitle.invalidateViews();
                            btn_edit.setText(getResources().getString(R.string.done));

                        } else {
                            ineditmode = false;
                            lv_bkmrktitle.invalidateViews();
                            btn_edit.setText(getResources().getString(R.string.edit));

                        }
                    }
                });
                bt_del.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        if (!deletemode) {
                            deletemode = true;
                            lv_bkmrktitle.invalidateViews();
                            bt_del.setText(getResources().getString(R.string.done));
                        } else {
                            deletemode = false;
                            lv_bkmrktitle.invalidateViews();
                            bt_del.setText(getResources().getString(R.string.delete));

                        }
                    }
                });

                break;
            }
            case R.id.Search: {
                if (groups.isSearchInWeb()) {
                    if (!currentBook.is_bStoreBook()){
                        hidePageListView();
                        Intent web_intent = new Intent(BookViewActivity.this,WebBookActivity.class);
                        web_intent.putExtra("Shelf", gridShelf);
                        web_intent.putExtra("book",currentBook);
                        web_intent.putExtra("From","FromBook");
                        startActivityForResult(web_intent, WebviewRequestCode);
                    }else {
                        advanceSearchPopup();
                    }
                }else{
                    showNooorPlusDialog();
                }
                break;
            }
            case R.id.btn_edit: {
                MindMapDialog mindMapDialog = new MindMapDialog(BookViewActivity.this, false);
                String pageBgPath = page.pageBgMindMapContent;
                mindMapDialog.openMindmapActivity("", "", pageBgPath,Globals.mindMapLastBook);
                break;
            }
            case R.id.btnFileManager: {
                Intent intent = new Intent(getApplicationContext(), FileManagerActivity.class);
                intent.putExtra("Book", currentBook);
                startActivityForResult(intent, ActivityFileManager);
                break;
            }
            case R.id.btnCloudManager:{
                if(checkLoginAndAlert(BookViewActivity.this,true)) {
                    Intent cloud_intent = new Intent(BookViewActivity.this, CloudActivity.class);
                    cloud_intent.putExtra("Book", currentBook);
                    cloud_intent.putExtra("PageNumber",currentPageNumber);
                    startActivityForResult(cloud_intent,cloudActivity_callback);
                }
                break;
            }
            case R.id.add_btn_tab:{
              //  treeViewStructureList(v,true,0);
                viewTreeList(v,true,0);
                break;
            }case R.id.btn_search:{
                internetSearch.searchClicked(new InternetSearchCallBack() {
                    @Override
                    public void onClick(String url, String title) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
                        new createAdvanceSearchTab(url, title).execute();
                    }
                });
                break;
            }case R.id.search_image:{
                internetSearch.showSearchPopUp(v);
              //  showSearchPopUp(v);
                break;
            }case R.id.arrow_btn:{
                internetSearch.showSearchPopUp(v);
              //  showSearchPopUp(v);
                break;
            }case R.id.dropdown_btn:{
                if (SubtabsAdapter.level3!=0) {
                    if (currentBtnEnrichment.getEnrichmentId() == SubtabsAdapter.level3) {
                        isLoadEnrInpopview = true;
                        viewTreeList(v, false, currentBtnEnrichment.getEnrichmentId());
                    }
                }
                break;
            }case R.id.btn_brush_new:{
                changeBrushColor(v);
                break;
            }case R.id.btn_eraser_new:{
                page.canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                break;
            }case R.id.btn_clear_new:{
               page.canvasView.clearView();
                break;
            }
            default:
                break;
        }
    }


  /*  public class MyDragShadowBuilder extends View.DragShadowBuilder {        for Rearranging the tabs

        // The drag shadow image, defined as a drawable thing
        private Drawable shadow;

        // Defines the constructor for myDragShadowBuilder
        public MyDragShadowBuilder(View v) {

            // Stores the View parameter passed to myDragShadowBuilder.

            super(v);

            // Creates a draggable image that will fill the Canvas provided by the system.
            // shadow = new ColorDrawable(Color.LTGRAY);
            shadow=(Drawable)BookViewActivity.this.getResources().getDrawable(R.drawable.tab_enr_dark);
//            shadow = new Drawable(Sample.this.getResources().getDrawable(R.drawable.tab_enr_dark));
        }

//        @Override
//        public void onDrawShadow(Canvas canvas) {
//            canvas.scale(1.5F, 1.5F);
//            super.onDrawShadow(canvas);
//        }

        // Defines a callback that sends the drag shadow dimensions and touch point back to the
        // system.
        @Override
        public void onProvideShadowMetrics (Point size, Point touch) {
            // Defines local variables
            int width, height;

            // Sets the width of the shadow to half the width of the original View
            width = getView().getWidth();

            // Sets the height of the shadow to half the height of the original View
            height = getView().getHeight();

            // The drag shadow is a ColorDrawable. This sets its dimensions to be the same as the
            // Canvas that the system will provide. As a result, the drag shadow will fill the
            // Canvas.
            shadow.setBounds(0, 0, width, height);

            // Sets the size parameter's width and height values. These get back to the system
            // through the size parameter.
            size.set(width, height);

//            shadowSize.set((int) (view.getWidth() * 1.5F),
//                        (int) (view.getHeight() * 1.5F));
//               shadowTouchPoint.set(shadowSize.x / 2, shadowSize.y / 2);
            // Sets the touch point's position to be in the middle of the drag shadow

            touch.set(width / 2, height / 2);
        }

        // Defines a callback that draws the drag shadow in a Canvas that the system constructs
        // from the dimensions passed in onProvideShadowMetrics().
        @Override
        public void onDrawShadow(Canvas canvas) {
            //  canvas.scale(1.5F, 1.5F);
            // Draws the ColorDrawable in the Canvas passed in from the system.
            shadow.draw(canvas);
        }
    }   */

    public static boolean checkLoginAndAlert(Context ctx, boolean withAlert){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        String scopeId = prefs.getString(Globals.sUserIdKey, "");
        int scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
        if (scopeType > 0 && !scopeId.equals("")) {
            return true;
        } else {
            if (withAlert) {
                UserFunctions.DisplayAlertDialog(ctx,R.string.please_login_main, R.string.login);
            }
            return false;
        }
    }

    private void addingNewPage(){
        if (!wv_advSearch.isShown()) {
            if (!currentBook.is_bStoreBook() || currentBook.get_bStoreID().contains("C")) {
                new takeAndSaveScreenshot(currentPageNumber).execute();
            }
        } else {
            wv_advSearch.setVisibility(View.INVISIBLE);
        }

        int oldPageCountVal = pageCount;
        pageCount = pageCount + 1;
        currentBook.setTotalPages(pageCount);

        int pageNumber = pageCount - 1;

        //update the cursor
        int cursorValue = pageNumber - 1;
        cursor.newRow().add(cursorValue).add("Page: " + cursorValue);
        pageAdapter.changeCursor(cursor);
        pageAdapter.notifyDataSetChanged();

        db.executeQuery("update books set totalPages='" + pageCount + "' where BID='" + currentBook.getBookID() + "'");
        db.executeQuery("update objects set pageNO='" + pageCount + "' where BID='" + currentBook.getBookID() + "' and pageNO='" + oldPageCountVal + "'");

        //new takeAndSaveScreenshot(currentPageNumber).execute();

        //pageTemplateListAdapter.addDefaultTemplate(position+1, pageNumber, 0);
        addPageNumberObject(pageNumber, 0);
        wv_advSearch.setVisibility(View.INVISIBLE);
//				canvasView.setVisibility(View.INVISIBLE);
//				twoDScroll.setVisibility(View.INVISIBLE);
//				drawingView.setVisibility(View.INVISIBLE);
        bgImgView.setVisibility(View.VISIBLE);

        if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("C")) {
            enrichTabCountListAllPages.add(0);
            int oldPageCount = pageCount - 1;
            File oldPage = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/" + oldPageCount + ".htm");
            File newPage = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/" + pageCount + ".htm");
            oldPage.renameTo(newPage);
        }

        checkForEnrichments(pageNumber, 0);
        instantiateNewPage(pageNumber, 0);

        //new takeAndSaveScreenshot(String.valueOf(pageNumber)).execute();
        pageListView.invalidateViews();
        pageListView.smoothScrollToPosition(pageNumber - 1);
    }
    public void addPageNumberObject(int pageNumber, int enrichPageId) {
        if (!currentBook.is_bStoreBook() && currentBook.is_bShowPageNo()) {
            //Add Page Number Object
            int initPageNoSize = (int) getResources().getDimension(R.dimen.bookview_initial_text_page_no_size);
            String objType = Globals.objType_pageNoText;
            String location2 = currentBook.getPageNoXPos() + "|" + currentBook.getPageNoYPos();
            String size2 = initPageNoSize + "|" + initPageNoSize;
            String objContent2 = currentBook.get_bPageNoContent();
            int objSequentialId2 = 3;
            addWebTextToDB(location2, size2, objContent2, objSequentialId2, pageNumber, enrichPageId, objType);
        }
    }

    private void addWebTextToDB(String location, String size, String objContent, int objSequentialId, int pageNumber, int enrichPageId, String objType) {
        int objBID = currentBook.getBookID();
        String objSclaPageToFit = "no";
        boolean objLocked = false;
        db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + objBID + "', '" + pageNumber + "', '" + location + "', '" + size + "', '" + objContent + "', '" + objSequentialId + "', '" + objSclaPageToFit + "', '" + objLocked + "', '" + enrichPageId + "')");
    }

    public static String detectLanguage(String selectedText) {

        String alpha = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
        String[] alphaArray = alpha.split(",");
        for (int i = 0; i < alphaArray.length; i++) {
            String current = alphaArray[i];
            if (selectedText.contains(current)) {
                return "en";
            }
        }
        return "ar";
    }

    public class CustomWebViewClient extends WebViewClient {
        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            Log.d("WEB_ERROR", "error code:" + errorCode + ":" + description);
            String url = "";
         //   new CreatingAdvsearchEnrichments(advtitle, url).execute();
        }

        @Override
        // Method where you(get) load the 'URL' which you have clicked
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            try {
                if (url != null) {
                    String path = url;
                    String[] adv_bkmrktype = path.split("/");
                    String url1 = adv_bkmrktype[2].replace(".", "/");
                    String[] type = url1.split("/");
                    for (int i = 0; i < type.length; i++) {
                        if (i == type.length - 2) {
                            new createAdvanceSearchTab(url, type[i]).execute();
                            break;
                        }
                    }
                    return true;
                } else {
                    return false;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

    }

    /**
     * show Add Page number dialog view
     *
     * @param initialWebTextXpos
     * @param initialWebTextYpos
     */
    public void showAddPageNoDialogView(final int initialWebTextXpos, final int initialWebTextYpos) {
        final int initPageNoSize = (int) getResources().getDimension(R.dimen.bookview_initial_text_page_no_size);
        AlertDialog.Builder builder = new AlertDialog.Builder(BookViewActivity.this);
        builder.setMessage(R.string.page_number_position);
        builder.setPositiveButton(R.string.apply, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String location = initialWebTextXpos + "|" + initialWebTextYpos;
                String size = initPageNoSize + "|" + initPageNoSize;
                String objType = Globals.objType_pageNoText;
                if (page.pageNumberObject == null) {
                    CustomWebRelativeLayout webRl = addNewWebRL(location, size, objType, null);

                    UndoRedoObjectData uRObjData = new UndoRedoObjectData();
                    uRObjData.setuRObjDataUniqueId(webRl.getObjectUniqueId());
                    uRObjData.setuRObjDataCustomView(webRl);
                    uRObjData.setuRObjDataLocation(webRl.getObjLocation());
                    uRObjData.setuRObjDataSize(webRl.getObjSize());
                    uRObjData.setuRObjDataType(webRl.getObjType());
                    uRObjData.setuRObjDataContent(webRl.getObjectContent());
                    uRObjData.setuRPageNoObjDataStatus("apply");
                    page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_DELETED, Globals.OBJECT_PREVIOUS_STATE);
                    page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CREATED, Globals.OBJECT_PRESENT_STATE);
                } else {
                    UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
                    uRPrevObjData.setuRObjDataUniqueId(page.pageNumberObject.getObjectUniqueId());
                    uRPrevObjData.setuRPageNoObjDataStatus("apply");
                    uRPrevObjData.setuRObjDataXPos((int) page.pageNumberObject.getX());
                    uRPrevObjData.setuRObjDataYPos((int) page.pageNumberObject.getY());
                    uRPrevObjData.setuRObjDataType(Globals.objType_pageNoText);
                    page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_MOVED, Globals.OBJECT_PREVIOUS_STATE);

                    updatePosPageNumberInPage(location);

                    UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                    uRCurrObjData.setuRObjDataUniqueId(page.pageNumberObject.getObjectUniqueId());
                    uRCurrObjData.setuRPageNoObjDataStatus("apply");
                    uRCurrObjData.setuRObjDataXPos(initialWebTextXpos);
                    uRCurrObjData.setuRObjDataYPos(initialWebTextYpos);
                    uRCurrObjData.setuRObjDataType(Globals.objType_pageNoText);
                    page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_MOVED, Globals.OBJECT_PRESENT_STATE);
                }
                dialog.cancel();
            }
        });

        builder.setNeutralButton(R.string.apply_all, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String location = initialWebTextXpos + "|" + initialWebTextYpos;
                String size = initPageNoSize + "|" + initPageNoSize;
                String objType = Globals.objType_pageNoText;
                if (page.pageNumberObject == null) {
                    CustomWebRelativeLayout webRl = addNewWebRL(location, size, objType, null);
                    addPageNoObjectToAllPage(location, size, objType, null);

                    UndoRedoObjectData uRObjData = new UndoRedoObjectData();
                    uRObjData.setuRObjDataUniqueId(webRl.getObjectUniqueId());
                    uRObjData.setuRObjDataCustomView(webRl);
                    uRObjData.setuRObjDataLocation(webRl.getObjLocation());
                    uRObjData.setuRObjDataSize(webRl.getObjSize());
                    uRObjData.setuRObjDataType(webRl.getObjType());
                    uRObjData.setuRObjDataContent(webRl.getObjectContent());
                    uRObjData.setuRPageNoObjDataStatus("applyAll");
                    page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_DELETED, Globals.OBJECT_PREVIOUS_STATE);
                    page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CREATED, Globals.OBJECT_PRESENT_STATE);
                } else {
                    UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
                    uRPrevObjData.setuRObjDataUniqueId(page.pageNumberObject.getObjectUniqueId());
                    uRPrevObjData.setuRPageNoObjDataStatus("applyAll");
                    uRPrevObjData.setuRObjDataXPos((int) page.pageNumberObject.getX());
                    uRPrevObjData.setuRObjDataYPos((int) page.pageNumberObject.getY());
                    uRPrevObjData.setuRObjDataType(Globals.objType_pageNoText);
                    page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_MOVED, Globals.OBJECT_PREVIOUS_STATE);
                    addPageNoObjectToAllPage(location, size, objType, null);
                    //updatePosPageNumberInAllPages(location);

                    UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                    uRCurrObjData.setuRObjDataUniqueId(page.pageNumberObject.getObjectUniqueId());
                    uRCurrObjData.setuRPageNoObjDataStatus("applyAll");
                    uRCurrObjData.setuRObjDataXPos(initialWebTextXpos);
                    uRCurrObjData.setuRObjDataYPos(initialWebTextYpos);
                    uRCurrObjData.setuRObjDataType(Globals.objType_pageNoText);
                    page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_MOVED, Globals.OBJECT_PRESENT_STATE);
                }
                dialog.cancel();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    /**
     * Make Enrich Button to be selected
     *
     * @param v
     */
    public void makeEnrichedBtnSlected(View v) {
        for (int i = 0; i < enrTabsLayout.getChildCount(); i++) {
            Button btnEnrTab;
            if (i == 0) {
                btnEnrTab = (Button) enrTabsLayout.getChildAt(i);
            } else {
                RelativeLayout rlview = (RelativeLayout) enrTabsLayout.getChildAt(i);
                btnEnrTab = (Button) rlview.getChildAt(0);
            }
            if (btnEnrTab == v) {
                v.setSelected(true);
            } else {
                btnEnrTab.setSelected(false);
            }
        }
    }

    /**
     * Deletes the selected object
     */
    public void deleteSelectedObject() {
        if (page.currentView != null) {
            if (page.currentView instanceof CustomWebRelativeLayout && ((CustomWebRelativeLayout) page.currentView).getObjType().equals(Globals.objType_pageNoText)) {
                Builder builder = new AlertDialog.Builder(this);
                builder.setPositiveButton(R.string.apply, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CustomWebRelativeLayout webRl = page.pageNumberObject;
                        page.deleteCurrentSelectedView(webRl);

                        UndoRedoObjectData uRObjData = new UndoRedoObjectData();
                        uRObjData.setuRObjDataUniqueId(webRl.getObjectUniqueId());
                        uRObjData.setuRObjDataCustomView(webRl);
                        uRObjData.setuRObjDataLocation(webRl.getObjLocation());
                        uRObjData.setuRObjDataSize(webRl.getObjSize());
                        uRObjData.setuRObjDataType(webRl.getObjType());
                        uRObjData.setuRObjDataContent(webRl.getObjectContent());
                        uRObjData.setuRPageNoObjDataStatus("apply");
                        page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CREATED, Globals.OBJECT_PREVIOUS_STATE);
                        page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_DELETED, Globals.OBJECT_PRESENT_STATE);
                        dialog.cancel();
                        page.pageNumberObject = null;
                    }
                });
                builder.setNeutralButton(R.string.apply_all, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        CustomWebRelativeLayout webRl = page.pageNumberObject;
                        page.deleteCurrentSelectedView(webRl);
                        page.deletePageNumberObjectFromAllPages();

                        UndoRedoObjectData uRObjData = new UndoRedoObjectData();
                        uRObjData.setuRObjDataUniqueId(webRl.getObjectUniqueId());
                        uRObjData.setuRObjDataCustomView(webRl);
                        uRObjData.setuRObjDataLocation(webRl.getObjLocation());
                        uRObjData.setuRObjDataSize(webRl.getObjSize());
                        uRObjData.setuRObjDataType(webRl.getObjType());
                        uRObjData.setuRObjDataContent(webRl.getObjectContent());
                        uRObjData.setuRPageNoObjDataStatus("applyAll");
                        page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CREATED, Globals.OBJECT_PREVIOUS_STATE);
                        page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_DELETED, Globals.OBJECT_PRESENT_STATE);
                        dialog.cancel();
                        page.pageNumberObject = null;
                    }
                });

                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.setTitle(R.string.delete);
                builder.setMessage(R.string.suredelete);
                builder.show();
            } else {
                Builder builder = new AlertDialog.Builder(this);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (page.handlerArea9!=null){
                            page.handlerArea9.setVisibility(View.GONE);
                        }
                        page.deleteCurrentSelectedView(page.currentView);
                        dialog.cancel();
                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setTitle(R.string.delete);
                builder.setMessage(R.string.suredelete);
                builder.show();
            }
        } else {
            UserFunctions.DisplayAlertDialog(this, R.string.select_item_delete, R.string.warning);
        }
    }

    /**
     * Show Dialog for Enrichment
     *
     * @param view
     */
    private void showEnrichDialog(View v) {
        final Dialog enrichDialog = new Dialog(this);

        enrichDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        enrichDialog.setTitle(R.string.enrich);
        enrichDialog.setContentView(this.getLayoutInflater().inflate(R.layout.enrich_dialog, null));
        enrichDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, BookViewActivity.this), LayoutParams.WRAP_CONTENT);
        enrichDialog.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isEnrichmentCreated && enrTabsLayout.getWidth() > designPageLayout.getWidth()) {
                    int enrTabsWidth = enrTabsLayout.getWidth();
                    enrScrollView.smoothScrollTo(enrTabsWidth, 0);
                    isEnrichmentCreated = false;
                }
            }
        });

        Button btnDuplicate = (Button) enrichDialog.findViewById(R.id.btnDuplicate);
        btnDuplicate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int maxEnrichId = db.getMaxUniqueRowID("enrichments") + 1;
                createEnrichments("duplicate", maxEnrichId);
                enrichDialog.dismiss();
                int enrTabCount = enrichTabCountListAllPages.get(Integer.parseInt(currentPageNumber) - 1);
                enrTabCount = enrTabCount + 1;
                enrichTabCountListAllPages.set(Integer.parseInt(currentPageNumber) - 1, enrTabCount);
                pageListView.invalidateViews();
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        });
        Button btnBlank = (Button) enrichDialog.findViewById(R.id.btnBlank);
        btnBlank.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

				int maxEnrichId = db.getMaxUniqueRowID("enrichments") + 1;
				//pageTemplateListAdapter.addDefaultTemplate(position+1, Integer.parseInt(currentPageNumber), maxEnrichId);
				addPageNumberObject(Integer.parseInt(currentPageNumber), maxEnrichId);
				createEnrichments("blank", maxEnrichId);
				//popoverView.dissmissPopover(true);
				int enrTabCount = enrichTabCountListAllPages.get(Integer.parseInt(currentPageNumber) - 1);
				enrTabCount = enrTabCount + 1;
				enrichTabCountListAllPages.set(Integer.parseInt(currentPageNumber) - 1, enrTabCount);
				pageListView.invalidateViews();
				enrichDialog.dismiss();
            }
        });
        Button btnCancel = (Button) enrichDialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                enrichDialog.dismiss();
            }
        });
        TextView tv_title = (TextView) enrichDialog.findViewById(R.id.textView1);
        if (currentBook.get_bStoreID().contains("P")) {
            btnDuplicate.setVisibility(View.GONE);
            tv_title.setText(getResources().getString(R.string.blank_page));
        } else {
            tv_title.setText(getResources().getString(R.string.duplicate_page_or_blank_page));
        }
        enrichDialog.show();
    }

    /**
     * Create New Enrichments based on the type
     *
     * @param enrType
     */
    public void createEnrichments(final String enrType, final int maxEnrichId) {
        int enrichSequenceId = enrichmentTabListArray.size() + 1;
        String enrichTitle = "Enrichment" + enrichSequenceId;
        for(Enrichments enrich:enrichmentTabListArray){
           if(enrich.isEnrichmentSelected()){
               enrich.setEnrichmentSelected(false);
               break;
           }
        }
        final Enrichments enrichments = new Enrichments(BookViewActivity.this);
        enrichments.setEnrichmentId(maxEnrichId);
        enrichments.setEnrichmentBid(currentBook.getBookID());
        int  objSequentialId = db.getMaximumSequentialId(Integer.parseInt(currentPageNumber), currentBook.getBookID()) + 1;
        enrichments.setEnrichmentSequenceId(objSequentialId);
        enrichments.setEnrichmentPageNo(Integer.parseInt(currentPageNumber));
        enrichments.setEnrichmentTitle(enrichTitle);
        enrichments.setEnrichmentType(enrType);
        enrichments.setEnrichmentExportValue(0);
        enrichments.setEnrichmentSelected(true);
        if (enrType.equals("duplicate")) {
            enrichments.createNewDuplicateEnrichments();
        } else if (enrType.equals("blank")) {
            enrichments.createBlankEnrichments(catId);
        }
        if (clickableRecyclerView!=null) {
            if (clickableRecyclerView.getId() == R.id.gridView2) {
                SubtabsAdapter.level3 = enrichments.getEnrichmentId();
            } else if (clickableRecyclerView.getId() == R.id.gridView) {
                SubtabsAdapter.level2 = enrichments.getEnrichmentId();
            } else if (catId == 0){
                SubtabsAdapter.level1 = enrichments.getEnrichmentId();
            }
        }else{
            SubtabsAdapter.level1 = enrichments.getEnrichmentId();
        }
        enrichmentTabListArray.add(enrichments);
        if (enrichmentTabListArray.size()> 1) {
//            if (rlTabsLayout.getVisibility() != View.VISIBLE) {
//                rlTabsLayout.setVisibility(View.VISIBLE);
//            }
        }else{
            rlTabsLayout.setVisibility(View.INVISIBLE);
        }
        if (mainDesignView.getChildCount()==4){
            mainDesignView.removeViewAt(3);
        }
       // Button enrichBtnCreated = enrichments.createEnrichmentTabs();
       // makeEnrichedBtnSlected(enrichBtnCreated);
        wv_advSearch.setVisibility(View.INVISIBLE);
        bgImgView.setVisibility(View.VISIBLE);
        isEnrichmentCreated = true;
        if (isCreateBlankEnr!=null && !isCreateBlankEnr.equals("")){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    page = null;
                    page = (Page) new Page(BookViewActivity.this, db, global, currentPageNumber, maxEnrichId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    page.setPageType(enrType);
                }
            });
        }else{
            instantiateNewPage(Integer.parseInt(currentPageNumber), maxEnrichId);
        }
        if (recyclerView!=null){
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    /**
     * Check for Enrichment tabs in the page
     *
     * @param pageNo
     */
    public void checkForEnrichments(int pageNo, int navigateEnrId) {
        //if (currentBook.is_bStoreBook()) {
        //Clear All Enrichment Buttons Except Home Button
        enrTabsLayout.removeViews(1, enrTabsLayout.getChildCount() - 1);
        enrichmentTabListArray = db.getAllEnrichmentTabList(currentBook.getBookID(), pageNo, this);
        wv_advSearch.setVisibility(View.INVISIBLE);
        bgImgView.setVisibility(View.VISIBLE);
        designScrollPageView.setVisibility(View.VISIBLE);
        if (mainDesignView.getChildCount() == 4) {
            mainDesignView.removeViewAt(3);
        }
        rlTabsLayout.setVisibility(View.INVISIBLE);
        if (enrichmentTabListArray.size() > 1) {
            for(Enrichments enrich:enrichmentTabListArray){
                //if(enrich.isEnrichmentSelected()){
                    enrich.setEnrichmentSelected(false);
               // }
                if(enrich.getEnrichmentId()==navigateEnrId){
                    enrich.setEnrichmentSelected(true);
                    break;
                }
            }
            if (navigateEnrId == 0) {
                btnEnrHome.setSelected(true);
            }
            if (rlTabsLayout.getVisibility() != View.VISIBLE) {
                rlTabsLayout.setVisibility(View.VISIBLE);
            }

        } else {
            rlTabsLayout.setVisibility(View.VISIBLE);
        }

        ArrayList<Enrichments> topEnrList = new ArrayList<>();
        for (Enrichments enrichments : enrichmentTabListArray){
            if (enrichments.getCategoryID()==0){
                topEnrList.add(enrichments);
            }
        }
        loadView(topEnrList,recyclerView);
//        ItemTouchHelper.Callback callback = new MovieTouchHelper(adapter);
//        ItemTouchHelper helper = new ItemTouchHelper(callback);
//        helper.attachToRecyclerView(recyclerView);
        //}
    }

    public class CreatingAdvsearchEnrichments extends AsyncTask<Void, Void, Void> {
        String title;
        String enrichmentPath;

		public CreatingAdvsearchEnrichments(String Title, String url) {
			// TODO Auto-generated constructor stub
			this.title=Title;
			this.enrichmentPath=url;
			progressBar.setVisibility(View.VISIBLE);
            for(Enrichments enrich:enrichmentTabListArray){
                if(enrich.isEnrichmentSelected()) {
                    enrich.setEnrichmentSelected(false);
                    break;
                }
            }
		}
		@Override
	    protected void onPreExecute() {
		 super.onPreExecute();
		 String bookXmlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/"+"Book.xml";
	        //int enrichSequenceId = enrichmentTabListArray.size()+1;
			//String enrichTitle = "Enrichment"+enrichSequenceId;
			title=title.replace("'", "''");
            int  objSequentialId = db.getMaximumSequentialId(Integer.parseInt(currentPageNumber), currentBook.getBookID()) + 1;
			db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,SequentialID,categoryID)values('"+currentBook.getBookID()+"', '"+currentPageNumber+"', '"+title+"', 'Search', '0','"+enrichmentPath+"','" + objSequentialId + "','"+currentBtnEnrichment.getEnrichmentId()+"')");
			int maxEnrichId = db.getMaxUniqueRowID("enrichments");
			Enrichments enrichments = new Enrichments(BookViewActivity.this);
			enrichments.setEnrichmentId(maxEnrichId);
			enrichments.setEnrichmentBid(currentBook.getBookID());
			enrichments.setEnrichmentSequenceId(objSequentialId);
			enrichments.setEnrichmentPageNo(Integer.parseInt(currentPageNumber));
			enrichments.setEnrichmentTitle(title.replace( "''", "'"));
			enrichments.setEnrichmentPath(enrichmentPath);
			enrichments.setEnrichmentType("Search");
            enrichments.setEnrichmentSelected(true);
			enrichments.setEnrichmentExportValue(0);
			enrichmentTabListArray.add(enrichments);
			if(currentBook.is_bStoreBook()){
			      enrichments.UpdateAdvSearchintoBookXml(bookXmlPath);
			}
			//Button enrichBtnCreated = enrichments.createEnrichmentTabs();
			//makeEnrichedBtnSlected(enrichBtnCreated);
            if (mainDesignView.getChildCount() == 4) {
                mainDesignView.removeViewAt(3);
            }
			//System.out.println(enrichmentTabListArray.size());
			if (enrichmentTabListArray.size()>0) {
				if (rlTabsLayout.getVisibility() != 0) {
					rlTabsLayout.setVisibility(View.VISIBLE);
				}
			}
			wv_advSearch.setVisibility(View.VISIBLE);
			wv_advSearch.getSettings().setDomStorageEnabled(true);
			wv_advSearch.getSettings().setLoadWithOverviewMode(true);
			wv_advSearch.getSettings().setUseWideViewPort(true);
			wv_advSearch.getSettings().setJavaScriptEnabled(true);
			wv_advSearch.setWebViewClient(new WebViewClient());
            if (title.equals("BookPage")){
                String[] split = enrichmentPath.split("/");
                String filePath = "file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/";
                File hyperPage = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/"+split[split.length-1]);
                String srcString = UserFunctions.decryptFile(hyperPage);
                wv_advSearch.loadDataWithBaseURL(filePath, srcString, "text/html", "utf-8", null);
            }else{
                wv_advSearch.loadUrl(enrichmentPath);
            }
			bgImgView.setVisibility(View.INVISIBLE);
			designScrollPageView.setVisibility(View.INVISIBLE);
			instantiateNewPage(Integer.parseInt(currentPageNumber), maxEnrichId);
			isEnrichmentCreated = true;

            disableToolbarButtons();
            //System.out.println(enrichTabCountListAllPages);
            int enrTabCount = enrichTabCountListAllPages.get(Integer.parseInt(currentPageNumber) - 1);
            enrTabCount = enrTabCount + 1;
            enrichTabCountListAllPages.set(Integer.parseInt(currentPageNumber) - 1, enrTabCount);
            pageListView.invalidateViews();
            wv_advSearch.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            recyclerView.getAdapter().notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (enrTabsLayout.getWidth() > designPageLayout.getWidth()) {
                int enrTabsWidth = enrTabsLayout.getWidth();
                enrScrollView.smoothScrollTo(enrTabsWidth, 0);
            }
            if (keyboardToolbar.getVisibility()==View.VISIBLE){
                keyboardToolbar.setVisibility(View.GONE);
            }
        }
    }


    public void creatingBookmarkButton(String enrTitle, Enrichments advsearchenrich) {
        // TODO Auto-generated method stub
       // Enrichments advsearchenrich = (Enrichments) btnEnr.getTag();
        db.executeQuery("insert into BookmarkedSearchTabs (Title, SearchTabId, PageNo,IsForAllPages,Path,Bid) values('" + enrTitle + "', '" + advsearchenrich.getEnrichmentId() + "', '" + advsearchenrich.getEnrichmentPageNo() + "','False','" + advsearchenrich.getEnrichmentPath() + "','" + currentBook.getBookID() + "')");
        enrTitle = enrTitle.replace("''", "'");
        int objUniqueId = db.getMaxUniqueRowID("BookmarkedSearchTabs");

        BookMarkEnrichments bookmarkenrichment = new BookMarkEnrichments(BookViewActivity.this);
        bookmarkenrichment.setEnrichmentId(objUniqueId);
        bookmarkenrichment.setEnrichmentPath(advsearchenrich.getEnrichmentPath());
        bookmarkenrichment.setEnrichmentTitle(enrTitle);
        bookmarkenrichment.setEnrichmentPageNo(advsearchenrich.getEnrichmentPageNo());
        bookmarkenrichment.setEnrichmentBid(currentBook.getBookID());
        bookmarkenrichment.setSearchTabId(advsearchenrich.getEnrichmentId());
        enrichmentbkmrkList.add(bookmarkenrichment);

        Button enrichBtnCreated = bookmarkenrichment.createBookMarkTabs();

    }

    /**
     * Check for BkMrkEnrichment tabs in the page
     *
     * @param pageNo
     */
    private void checkForBkmrkEnrichments(int pageNo) {
        //if (currentBook.is_bStoreBook()) {
        //enrichmentbkmrkList.clear();
        bookmark_container.removeViews(0, bookmark_container.getChildCount());
        enrichmentbkmrkList = db.getAllbookMarkTabListForTheBook(pageNo, currentBook.getBookID(), BookViewActivity.this);

        if (enrichmentbkmrkList.size() > 0) {
            bkmarkLayout.setVisibility(View.VISIBLE);

            for (BookMarkEnrichments bkmrkenrichments : enrichmentbkmrkList) {
                bkmrkenrichments.createBookMarkTabs();
            }
        } else {
            bkmarkLayout.setVisibility(View.GONE);
        }
        //}
    }

    public void updateNameToButton(int position, String newtext) {
        newtext = newtext.replace("''", "'");
        //System.out.println(bookmark_container.getChildCount()+"child");
        //RelativeLayout view = (RelativeLayout) bookmark_container.getChildAt(position);
        // RelativeLayout rlview = (RelativeLayout) bookmark_container.getChildAt(position);
        Button nextEnrichSelectView = (Button) bookmark_container.getChildAt(position);
        nextEnrichSelectView.setText(newtext);

    }

    public void removingBkMrkButton(int position, int pageno, int id) {
        bookmark_container.removeViewAt(position);
        lv_bkmrktitle.invalidateViews();
        db.executeQuery("delete from BookmarkedSearchTabs where Id='" + id + "' and PageNo='" + pageno + "' and Bid='" + currentBook.getBookID() + "'");
        if (bookmark_container.getChildCount() < 1) {
            bkmarkLayout.setVisibility(View.GONE);
            bkmrk_dismiss = true;
            advbkmarkpopup.dissmissPopover(true);
        }

    }

    /**
     * showWidgets dropdown
     */
    private void showWidgetsPopUpWindow() {
        final PopoverView popView = new PopoverView(BookViewActivity.this, R.layout.widgets_dropdown);
        popView.setContentSizeForViewInPopover(new Point(Globals.getDeviceIndependentPixels(65, BookViewActivity.this), Globals.getDeviceIndependentPixels(320, BookViewActivity.this)));
        popView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(btnAddWidgets), PopoverView.PopoverArrowDirectionDown, true);

        Button btnAddWebTable = (Button) popView.findViewById(R.id.btn_add_table);
        btnAddWebTable.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (groups.isTables()) {
                    showWebTableAlertDialog();
                    popView.dissmissPopover(true);
                }else{
                    showNooorPlusDialog();
                }
            }
        });

        Button btnAddIframe = (Button) popView.findViewById(R.id.btn_add_iframe);
        btnAddIframe.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (groups.isIframe()) {
                    int imageWidth = (int) getResources().getDimension(R.dimen.bookview_initial_iframe_width);
                    int imageHeight = (int) getResources().getDimension(R.dimen.bookview_initial_iframe_height);
                    int imageXpos = designPageLayout.getWidth() / 2 - imageWidth / 2;
                    int imageYpos = designPageLayout.getHeight() / 2 - imageHeight / 2;
                    String imageType = "IFrame";
                    String objFormat = ".png";
                    addNewImageRL(imageWidth, imageHeight, imageXpos, imageYpos, imageType, objFormat, null);
                    popView.dissmissPopover(true);
                }else{
                    showNooorPlusDialog();
                }
            }
        });

        Button btnAddEmbedObject = (Button) popView.findViewById(R.id.btn_add_embed);
        btnAddEmbedObject.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (groups.isEmbedCode()) {
                    int imageWidth = (int) getResources().getDimension(R.dimen.bookview_initial_embed_width);
                    int imageHeight = (int) getResources().getDimension(R.dimen.bookview_initial_embed_height);
                    int imageXpos = designPageLayout.getWidth() / 2 - imageWidth / 2;
                    int imageYpos = designPageLayout.getHeight() / 2 - imageHeight / 2;
                    String imageType = "EmbedCode";
                    String objFormat = ".png";
                    addNewImageRL(imageWidth, imageHeight, imageXpos, imageYpos, imageType, objFormat, null);
                    popView.dissmissPopover(true);
                }else{
                    showNooorPlusDialog();
                }
            }
        });

        Button btnAddQuiz = (Button) popView.findViewById(R.id.btn_add_quiz);
        btnAddQuiz.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (groups.isQuiz()) {
                    showQuizPopover(false, null, 0, 0, null, null, getResources().getString(R.string.app_language));
                    popView.dissmissPopover(true);
                }else{
                    showNooorPlusDialog();
                }
            }
        });

        Button btnAddMindMap = (Button) popView.findViewById(R.id.btn_add_mindmap);
        btnAddMindMap.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (groups.isMindmapEditor()) {
                    MindMapDialog mindMapDialog = new MindMapDialog(BookViewActivity.this, false);
                    mindMapDialog.showMindMapDialog("");
                    popView.dissmissPopover(true);
                }else{
                    showNooorPlusDialog();
                }
            }
        });

    }


    public void showQuizPopover(final boolean isFromWebView, final String quesId, final int quesuniqueId, final int quesSectionNo, final WebView webview, Button btnInfo, final String language) {
        final PopoverView popoverView = new PopoverView(BookViewActivity.this, R.layout.quiz_popoverview);
        popoverView.setContentSizeForViewInPopover(new Point((int) getResources().getDimension(R.dimen.bookview_quiz_popover_width), (int) getResources().getDimension(R.dimen.bookview_quiz_popover_height)));
        if (!isFromWebView) {
            popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(btnAddWidgets), PopoverView.PopoverArrowDirectionDown, true);
        } else {
            popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(btnInfo), PopoverView.PopoverArrowDirectionUp, true);
        }
        Button btnQuizType1 = (Button) popoverView.findViewById(R.id.btnQuizType1);
        btnQuizType1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Quiz quiz = new Quiz(BookViewActivity.this, db);
                if (!isFromWebView) {
                    hidePageListView();
                    int initialWebTextWidth = (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_width);
                    int initialWebTextHeight = (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_height);
                    int initialWebTextXpos = designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
                    int initialWebTextYpos = designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
                    String location = initialWebTextXpos + "|" + initialWebTextYpos;
                    String size = initialWebTextWidth + "|" + initialWebTextHeight;
                    String objType = Globals.OBJTYPE_QUIZWEBTEXT;

                    int objUniqueId = db.getMaxUniqueRowID("objects") + 1;
                    String objContent = quiz.getTypeOneTemplate(objUniqueId, true, language);
                    addNewWebRL(location, size, objType, objContent);
                } else {
                    quiz.webQuizTypeOneSelected(quesId, quesuniqueId, quesSectionNo, webview, language);
                }
                popoverView.dissmissPopover(true);
            }
        });

        Button btnQuizType2 = (Button) popoverView.findViewById(R.id.btnQuizType2);
        btnQuizType2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Quiz quiz = new Quiz(BookViewActivity.this, db);
                if (!isFromWebView) {
                    hidePageListView();
                    int initialWebTextWidth = (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_width);
                    int initialWebTextHeight = (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_height);
                    int initialWebTextXpos = designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
                    int initialWebTextYpos = designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
                    String location = initialWebTextXpos + "|" + initialWebTextYpos;
                    String size = initialWebTextWidth + "|" + initialWebTextHeight;
                    String objType = Globals.OBJTYPE_QUIZWEBTEXT;

                    int objUniqueId = db.getMaxUniqueRowID("objects") + 1;
                    String objContent = quiz.getTypeTwoTemplate(objUniqueId, true, language);
                    addNewWebRL(location, size, objType, objContent);
                } else {
                    quiz.webQuizTypeTwoSelected(quesId, quesuniqueId, quesSectionNo, webview, language);
                }
                popoverView.dissmissPopover(true);
            }
        });

        Button btnQuizType3 = (Button) popoverView.findViewById(R.id.btnQuizType3);
        btnQuizType3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Quiz quiz = new Quiz(BookViewActivity.this, db);
                if (!isFromWebView) {
                    hidePageListView();
                    int initialWebTextWidth = (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_width);
                    int initialWebTextHeight = (int) getResources().getDimension(R.dimen.bookview_initial_quiz_three_text_height);
                    int initialWebTextXpos = designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
                    int initialWebTextYpos = designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
                    String location = initialWebTextXpos + "|" + initialWebTextYpos;
                    String size = initialWebTextWidth + "|" + initialWebTextHeight;
                    String objType = Globals.OBJTYPE_QUIZWEBTEXT;
                    int objUniqueId = db.getMaxUniqueRowID("objects") + 1;
                    String objContent = quiz.getTypeThreeTemplate(objUniqueId, true, language);
                    addNewWebRL(location, size, objType, objContent);
                } else {
                    quiz.webQuizTypeThreeSelected(quesId, quesuniqueId, quesSectionNo, webview, language);
                }
                popoverView.dissmissPopover(true);
            }
        });

        Button btnQuizType4 = (Button) popoverView.findViewById(R.id.btnQuizType4);
        btnQuizType4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Quiz quiz = new Quiz(BookViewActivity.this, db);
                if (!isFromWebView) {
                    hidePageListView();
                    int initialWebTextWidth = (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_width);
                    int initialWebTextHeight = (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_height);
                    int initialWebTextXpos = designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
                    int initialWebTextYpos = designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
                    String location = initialWebTextXpos + "|" + initialWebTextYpos;
                    String size = initialWebTextWidth + "|" + initialWebTextHeight;
                    String objType = Globals.OBJTYPE_QUIZWEBTEXT;
                    int objUniqueId = db.getMaxUniqueRowID("objects") + 1;
                    String objContent = quiz.getTypeFourTemplate(objUniqueId, true, language);
                    addNewWebRL(location, size, objType, objContent);
                } else {
                    quiz.webQuizTypeFourSelected(quesId, quesuniqueId, quesSectionNo, webview, language);
                }
                popoverView.dissmissPopover(true);
            }
        });
    }

    /**
     * show shapes drop down window
     */
    private void showShapesPopUpWindow() {
        final PopoverView popView = new PopoverView(BookViewActivity.this, R.layout.draw_dropdown);
        popView.setContentSizeForViewInPopover(new Point(Globals.getDeviceIndependentPixels(50, BookViewActivity.this), Globals.getDeviceIndependentPixels(270, BookViewActivity.this)));
        popView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(btnDraw), PopoverView.PopoverArrowDirectionDown, true);

        Button btn_draw_view = (Button) popView.findViewById(R.id.btn_draw_view);
        btn_draw_view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (groups.isDrawingEditor()) {
                    isInPaintMode = true;
                    enr_layout.setVisibility(View.GONE);
                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    objectsToolbar.setVisibility(View.INVISIBLE);
                    drawingToolbar.setVisibility(View.VISIBLE);
                    backBtnLayout.setVisibility(View.GONE);
                    topBarLayout.setVisibility(View.GONE);
                    undoRedoLayout.setVisibility(View.GONE);
                    mindMapSliderlayout.setVisibility(View.GONE);
                    designScrollPageView.setEnableScrolling(false);
                    btnBack.setVisibility(View.GONE);
                    btnPen.setSelected(true);
                    if (page.canvasImageView == null) {
                        int imageWidth = designPageLayout.getWidth();
                        int imageHeight = designPageLayout.getHeight();
                        int imageXpos = 0;
                        int imageYpos = 0;
                        String imageType = "DrawnImage";
                        String objFormat = ".png";
                        addNewImageRL(imageWidth, imageHeight, imageXpos, imageYpos, imageType, objFormat, null);
                    }

                    page.canvasImageView.setVisibility(View.INVISIBLE);

                    PainterCanvas canvasView = new PainterCanvas(BookViewActivity.this, page.canvasImageView.getObjectPathContent(), null);
                    page.addCanvasViewLayout(canvasView);

                    page.canvasView.setPresetPorterMode(null);
                    pageView_handle.setVisibility(View.GONE);
                    hidePageListView();
                    popView.dissmissPopover(true);
                }else{
                    showNooorPlusDialog();
                }
            }
        });

        Button btn_square = (Button) popView.findViewById(R.id.btn_square);
        btn_square.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int initialWebTextWidth = (int) getResources().getDimension(R.dimen.bookview_initial_text_square_size);
                int initialWebTextHeight = (int) getResources().getDimension(R.dimen.bookview_initial_text_square_size);
                int initialWebTextXpos = designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
                int initialWebTextYpos = designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
                String location = initialWebTextXpos + "|" + initialWebTextYpos;
                String size = initialWebTextWidth + "|" + initialWebTextHeight;
                String objType = "TextSquare";
                addNewWebRL(location, size, objType, null);
            }
        });

        Button btn_rectangle = (Button) popView.findViewById(R.id.btn_rectangle);
        btn_rectangle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int initialWebTextWidth = (int) getResources().getDimension(R.dimen.bookview_initial_text_rectangle_width);
                int initialWebTextHeight = (int) getResources().getDimension(R.dimen.bookview_initial_text_rectangle_height);
                int initialWebTextXpos = designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
                int initialWebTextYpos = designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
                String location = initialWebTextXpos + "|" + initialWebTextYpos;
                String size = initialWebTextWidth + "|" + initialWebTextHeight;
                String objType = "TextRectangle";
                addNewWebRL(location, size, objType, null);
            }
        });

        Button btn_circle = (Button) popView.findViewById(R.id.btn_circle);
        btn_circle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                int initialWebTextWidth = (int) getResources().getDimension(R.dimen.bookview_initial_text_circle_size);
                int initialWebTextHeight = (int) getResources().getDimension(R.dimen.bookview_initial_text_circle_size);
                int initialWebTextXpos = designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
                int initialWebTextYpos = designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
                String location = initialWebTextXpos + "|" + initialWebTextYpos;
                String size = initialWebTextWidth + "|" + initialWebTextHeight;
                String objType = "TextCircle";
                addNewWebRL(location, size, objType, null);
            }
        });

    }

    /**
     * update the keyboard toolbar buttons for the current webtext properties
     */
    public void updateKeyboardToolBarButtons() {
        if (page.currentView instanceof CustomWebRelativeLayout) {
            if ((((CustomWebRelativeLayout) page.currentView).fontColor) != null && !(((CustomWebRelativeLayout) page.currentView).fontColor).equals("transparent")) {
                btnKeyToolbar_pickColor.setBackgroundColor(Color.parseColor(((CustomWebRelativeLayout) page.currentView).fontColor));
            }
            btnKeyToolbar_pickFont.setText(((CustomWebRelativeLayout) page.currentView).fontFamily);
            tvKeyToolbar_fontSize.setText(String.valueOf(((CustomWebRelativeLayout) page.currentView).fontSize));
        }
    }

    /**
     * Show Audio Popover view to record Audio
     *
     * @param view
     */
    private void showAudioPopOverView(View view) {
        final PopoverView popoverView = new PopoverView(this, R.layout.audio_popoverview);
        popoverView.setContentSizeForViewInPopover(new Point((int) getResources().getDimension(R.dimen.bookview_audio_popover_width), (int) getResources().getDimension(R.dimen.bookview_audio_popover_height)));
        popoverView.setDelegate(this);
        popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(view), PopoverView.PopoverArrowDirectionDown, true);
        final TextView tv_record = (TextView) popoverView.findViewById(R.id.tv_start_record);
        btn_record = (Button) popoverView.findViewById(R.id.btn_audio_record);

        btn_record.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!btn_record.isSelected()) { //Start Recording
                    File fileImgDir = new File(Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR);
                    if (!fileImgDir.exists()) {
                        fileImgDir.mkdir();
                    }
                    audio_outputFile = Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR + "SBA_AUDIO.m4a";

                    mRecorder = new MediaRecorder();
                    mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                    mRecorder.setOutputFile(audio_outputFile);
                    mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                    mRecorder.setAudioSamplingRate(44100);
                    mRecorder.setAudioChannels(2);

                    try {
                        mRecorder.prepare();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mRecorder.start();

                    btn_record.setSelected(true);
                    tv_record.setText(R.string.stop_recording);
                    ((Chronometer) popoverView.findViewById(R.id.audio_chronometer)).setBase(SystemClock.elapsedRealtime());
                    ((Chronometer) popoverView.findViewById(R.id.audio_chronometer)).start();
                } else { //Stop Recording
                    btn_record.setSelected(false);
                    tv_record.setText(R.string.start_recording);
                    ((Chronometer) popoverView.findViewById(R.id.audio_chronometer)).stop();
                    popoverView.dissmissPopover(true);
                }
            }
        });
    }

    /**
     * Ask the user whether to use the recording
     *
     * @param popoverView
     */
    private void useThisRecording(final PopoverView popoverView) {
        AlertDialog.Builder alertdialog = new AlertDialog.Builder(BookViewActivity.this);
        alertdialog.setTitle(R.string.recording_finished);
        alertdialog.setMessage(R.string.use_this_recording);
        alertdialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                int imageWidth = (int) getResources().getDimension(R.dimen.bookview_initial_audio_size);
                int imageHeight = (int) getResources().getDimension(R.dimen.bookview_initial_audio_size);
                int imageXpos = designPageLayout.getWidth() / 2 - imageWidth / 2;
                int imageYpos = designPageLayout.getHeight() / 2 - imageHeight / 2;
                String imageType = "Audio";
                String objFormat = ".m4a";
                addNewImageRL(imageWidth, imageHeight, imageXpos, imageYpos, imageType, objFormat, null);
                audio_outputFile = null;
            }
        });
        alertdialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                File audioFilePath = new File(audio_outputFile);
                audioFilePath.delete();
                audio_outputFile = null;
            }
        });
        alertdialog.show();
    }

    @Override
    public void popoverViewWillShow(PopoverView view) {

    }

    @Override
    public void popoverViewDidShow(PopoverView view) {

    }

    @Override
    public void popoverViewWillDismiss(PopoverView view) {
        if (bkmrk_dismiss) {
            if (enrTabsLayout.getWidth() > designPageLayout.getWidth()) {
                int enrTabsWidth = enrTabsLayout.getWidth();
                enrScrollView.smoothScrollTo(enrTabsWidth, 0);
                isEnrichmentCreated = false;
            }
        } else {
            if (mRecorder != null) {
                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
            }
            if (audio_outputFile != null) {
                File file = new File(audio_outputFile);
                if (file.exists()) {
                    useThisRecording(view);
                }
            }
        }
    }

    @Override
    public void popoverViewDidDismiss(PopoverView view) {
        if (bkmrk_dismiss) {
            if (bookmark_container.getWidth() > (bkmarkLayout.getWidth())) {
                int enrTabsWidth1 = bookmark_container.getWidth();
                bookmrk_sv.smoothScrollTo(enrTabsWidth1, 0);
            }
        }
    }


    /**
     * Show webTable AlertDialog to input Rows and Columns
     */
    private void showWebTableAlertDialog() {
        View promptView = getLayoutInflater().inflate(R.layout.inflate_webtable_alertdialog, null);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setView(promptView);
        final EditText editText_row = (EditText) promptView.findViewById(R.id.editText_row);
        final EditText editText_column = (EditText) promptView.findViewById(R.id.editText_column);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!editText_row.getText().toString().equals("") && !editText_column.getText().toString().equals("")) {
                    editText_row.getBackground().setColorFilter(getResources().getColor(R.color.skyblue), Mode.SRC_ATOP);
                    editText_column.getBackground().setColorFilter(getResources().getColor(R.color.skyblue), Mode.SRC_ATOP);
                    int rows = Integer.parseInt(editText_row.getText().toString());
                    int columns = Integer.parseInt(editText_column.getText().toString());
                    int initialWebTextWidth = (int) getResources().getDimension(R.dimen.bookview_initial_table_width);
                    int paddingTopHeight = Globals.getDeviceIndependentPixels(25, BookViewActivity.this);//25 pixel to place info and delete button.
                    int initialWebTextHeight = (rows * Globals.getDeviceIndependentPixels(35, BookViewActivity.this)) + paddingTopHeight;
                    int initialWebTextXpos = designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
                    int initialWebTextYpos = designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
                    String location = initialWebTextXpos + "|" + initialWebTextYpos;
                    String size = initialWebTextWidth + "|" + initialWebTextHeight + "|" + rows + "|" + columns;
                    String objType = "WebTable";
                    addNewWebRL(location, size, objType, null);
                } else {
						/*if (editText_row.getText().toString().equals("")) {
							editText_row.getBackground().setColorFilter(getResources().getColor(R.color.red), Mode.SRC_ATOP);
						}
						if (editText_column.getText().toString().equals("")) {
							editText_column.getBackground().setColorFilter(getResources().getColor(R.color.red), Mode.SRC_ATOP);
						}*/
                    Toast.makeText(BookViewActivity.this, R.string.error_msg, Toast.LENGTH_SHORT).show();
                }


            }
        });
        alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setTitle("Table");
        alertDialog.show();
    }

    public void setPreset(View v) {
        switch (v.getId()) {
            case R.id.preset_pencil:
                page.canvasView.setPreset(new BrushPreset(BrushPreset.PENCIL, page.canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    page.canvasView.setPresetPorterMode(new PorterDuffXfermode(Mode.CLEAR));
                }
                break;
            case R.id.preset_brush:
                page.canvasView.setPreset(new BrushPreset(BrushPreset.BRUSH, page.canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    page.canvasView.setPresetPorterMode(new PorterDuffXfermode(Mode.CLEAR));
                }
                break;
            case R.id.preset_marker:
                page.canvasView.setPreset(new BrushPreset(BrushPreset.MARKER, page.canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    page.canvasView.setPresetPorterMode(new PorterDuffXfermode(Mode.CLEAR));
                }
                break;
            case R.id.preset_pen:
                page.canvasView.setPreset(new BrushPreset(BrushPreset.PEN, page.canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    page.canvasView.setPresetPorterMode(new PorterDuffXfermode(Mode.CLEAR));
                }
                break;
        }

        updateControls();
    }

    private void updateControls() {
        mBrushSize.setProgress((int) page.canvasView.getCurrentPreset().size);
        if (page.canvasView.getCurrentPreset().blurStyle != null) {
            mBrushBlurStyle.setSelection(page.canvasView.getCurrentPreset().blurStyle
                    .ordinal() + 1);
            mBrushBlurRadius.setProgress(page.canvasView.getCurrentPreset().blurRadius);
        } else {
            mBrushBlurStyle.setSelection(0);
            mBrushBlurRadius.setProgress(0);
        }
    }

    /**
     * @author ShowBrushSettingsDialog
     */
    private void showBrushSettingsDialog() {
        Dialog brushSettingsDialog = new Dialog(this);
        brushSettingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        brushSettingsDialog.setTitle(R.string.brush_settings);
        brushSettingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.brush_settings_dialog, null));
        brushSettingsDialog.getWindow().setLayout((int) (global.getDeviceWidth() / 1.5), (int) (global.getDeviceHeight() / 1.5));
        mBrushSize = (SeekBar) brushSettingsDialog.findViewById(R.id.brush_size);
        btn_BrushColor = (ImageButton) brushSettingsDialog.findViewById(R.id.brush_color);

        if (brushColor != null && brushColor.equals(page.canvasView.getCurrentPreset())) {
            btn_BrushColor.setBackgroundColor(Color.parseColor(brushColor));
        } else {
            btn_BrushColor.setBackgroundColor(page.canvasView.getCurrentPreset().color);
        }
        mBrushSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 0) {
                    page.canvasView.setPresetSize(seekBar.getProgress());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (progress > 0) {
                    if (fromUser) {
                        page.canvasView.setPresetSize(seekBar.getProgress());
                    }
                } else {
                    mBrushSize.setProgress(1);
                }
            }
        });
        mBrushBlurRadius = (SeekBar) brushSettingsDialog.findViewById(R.id.brush_blur_radius);
        mBrushBlurRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                updateBlurSeek(seekBar.getProgress());

                if (seekBar.getProgress() > 0) {
                    setBlur();
                } else {
                    page.canvasView.setPresetBlur(null, 0);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (fromUser) {
                    updateBlurSeek(progress);
                    if (progress > 0) {
                        setBlur();
                    } else {
                        page.canvasView.setPresetBlur(null, 0);
                    }
                }
            }
        });
        mBrushBlurStyle = (Spinner) brushSettingsDialog.findViewById(R.id.brush_blur_style);
        mBrushBlurStyle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View v,
                                       int position, long id) {
                if (id > 0) {
                    updateBlurSpinner(id);
                    setBlur();
                } else {
                    mBrushBlurRadius.setProgress(0);
                    page.canvasView.setPresetBlur(null, 0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        updateControls();
        brushSettingsDialog.show();
    }

    private void updateBlurSeek(int progress) {
        if (progress > 0) {
            if (mBrushBlurStyle.getSelectedItemId() < 1) {
                mBrushBlurStyle.setSelection(1);
            }
        } else {
            mBrushBlurStyle.setSelection(0);
        }
    }

    private void setBlur() {
        page.canvasView.setPresetBlur((int) mBrushBlurStyle.getSelectedItemId(),
                mBrushBlurRadius.getProgress());
    }

    private void updateBlurSpinner(long blur_style) {
        if (blur_style > 0 && mBrushBlurRadius.getProgress() < 1) {
            mBrushBlurRadius.setProgress(1);
        }
    }

    /**
     * @author showColorPickerDialogtochangeDrawingcolor
     */
    public void changeBrushColor(View v) {
        final ArrayList<String> recentPaintColorList = this.page.getRecentColorsFromSharedPreference(Globals.paintColorKey);
        new PopupColorPicker(this, page.canvasView.getCurrentPreset().color, v, recentPaintColorList, false, new ColorPickerListener() {
            int pickedColor = 0;
            String hexColor = null;

            @Override
            public void pickedColor(int color) {
                hexColor = String.format("#%06X", (0xFFFFFF & color));
                pickedColor = color;
                brushColor = hexColor;
              //  btn_BrushColor.setBackgroundColor(Color.parseColor(hexColor));
            }

            @Override
            public void dismissPopWindow() {
                page.canvasView.setPresetColor(pickedColor);
                if (hexColor != null) {
                    page.setandUpdateRecentColorsToSharedPreferences(Globals.paintColorKey, recentPaintColorList, hexColor);
                }
            }

            @Override
            public void pickedTransparentColor(String transparentColor) {

            }

            @Override
            public void pickColorFromBg() {

            }
        });
    }

    public class image extends AsyncTask<String, Void, Bitmap> {
        ImageView imagepath;
        ImageView img;
        Button btn;
        int id;

        public image(Button btnEnrichmentTab, int i) {
            // TODO Auto-generated constructor stub
            this.btn = btnEnrichmentTab;
            id = i;
        }

        protected Bitmap doInBackground(String... urls) {
            String URLpath = urls[0];
            Bitmap bmp = null;
            try {
                URL url = new URL(URLpath);
                URLConnection ucon = url.openConnection();
                ucon.connect();
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                //System.out.println(bmp.getWidth());;
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return bmp;

        }

        protected void onPostExecute(Bitmap result) {
            if (result == null) {

            } else {
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, result.getWidth(), getResources().getDisplayMetrics());
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, result.getHeight(), getResources().getDisplayMetrics());
                Bitmap b = Bitmap.createScaledBitmap(result, width, height, false);
                Drawable f = new BitmapDrawable(getResources(), b);

                String imgpath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "bkmarkimages/";
                UserFunctions.createNewDirectory(imgpath);
                String correct = imgpath + (id) + ".png";  //6
                btn.setCompoundDrawablesWithIntrinsicBounds(f, null, null, null);

                if (id != 0) {
                    UserFunctions.saveBitmapImage(b, correct);
                }
            }
        }
    }

    /**
     * @author saveDrawnImagetoFilesDir
     */
    private class saveDrawnTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            page.canvasImageView.setUndoPreviousDataForDrawnImage("previous");
        }

        @Override
        protected String doInBackground(Void... params) {
            saveBitmap(page.canvasView.objContentPath);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            page.canvasImageView.setUndoPreviousDataForDrawnImage("present");
            page.canvasImageView.setBackgroundImageForDrawing();
            page.canvasImageView.setVisibility(View.VISIBLE);
            designPageLayout.removeView(page.canvasView);
            page.bringViewToFront(page.canvasImageView, 0);
        }
    }

    /**
     * @param pictureName
     * @author saveBitmapImage
     */
    private void saveBitmap(String pictureName) {
        try {
            page.canvasView.saveBitmap(pictureName);
            page.canvasView.changed(false);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

	/**
	 * insert page number object to all the pages
	 * @param location
	 * @param size
	 * @param objType
	 */
	public void addPageNoObjectToAllPage(String location, String size, String objType, String _objContent) {
		int width = Integer.parseInt(size.split("\\|")[0]);
		int height = Integer.parseInt(size.split("\\|")[1]);
		int xPos = Integer.parseInt(location.split("\\|")[0]);
		int yPos = Integer.parseInt(location.split("\\|")[1]);
		int pxWidth = Globals.getPixelValue(width, this);
		int pxHeight = Globals.getPixelValue(height, this);
		String[] rgbFontColor = currentBook.getTemplate().getTemplateParaColor().split("-");
		String objContent;
		if (_objContent == null) {
			objContent = "<div id=\"content\" contenteditable=\"false\" style=\"background-color: transparent; font-size: 30px; color: rgb("+rgbFontColor[0]+", "+rgbFontColor[1]+", "+rgbFontColor[2]+"); direction: ltr; text-align: center; width: "+pxWidth+"px; height: "+pxHeight+"px; box-sizing:border-box;padding-top:15px;\">";
		} else {
			objContent = _objContent;
			objContent = objContent.replace("'", "''");
		}
		for (int i = 2; i < pageCount ; i++) {
			int count = db.getPageNumberObjectUniqueRowId(i, currentBook.getBookID(), Globals.objType_pageNoText);
			if (count == 0) {
				int objBID = currentBook.getBookID();
				int objSequentialId = db.getMaximumSequentialIdValue(i, currentBook.getBookID()) + 1;
				String objSclaPageToFit = "no";
				boolean objLocked = true;
				db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+objType+"', '"+objBID+"', '"+i+"', '"+location+"', '"+size+"', '"+objContent+"', '"+objSequentialId+"', '"+objSclaPageToFit+"', '"+objLocked+"', '"+page.enrichedPageId+"')");
			}
			if(currentPageNumber.equals(i+"")){
				page.pageNumberObject.setObjLocation(location);
				page.pageNumberObject.setX(xPos);
				page.pageNumberObject.setY(yPos);

                //Remove the current handler area from the view and set the handler area to the newly added view
				page.removeHandlerArea();
				page.setCurrentView(page.pageNumberObject);
				page.makeSelectedArea(page.pageNumberObject);
			}
		}
		db.executeQuery("update objects set location='"+location+"' where objType='"+Globals.objType_pageNoText+"' and BID='"+currentBook.getBookID()+"'");
		currentBook.set_bShowPageNo(true);
		currentBook.setPageNoXPos(xPos);
		currentBook.setPageNoYPos(yPos);
		currentBook.set_bPageNoContent(objContent);
		boolean showPageNo = true;
		db.executeQuery("update books set ShowPageNo='" + showPageNo + "' and PageNoPos='" + location + "' and PageNoContent='" + objContent + "' where BID='" + currentBook.getBookID() + "'");
		objContent = objContent.replace("''", "'");
	}

    /**
     * this update page number to the current page
     *
     * @param location
     */
    public void updatePosPageNumberInPage(String location) {
        page.pageNumberObject.setObjLocation(location);
        int xPos = Integer.parseInt(location.split("\\|")[0]);
        int yPos = Integer.parseInt(location.split("\\|")[1]);
        page.pageNumberObject.setX(xPos);
        page.pageNumberObject.setY(yPos);

        //Remove the current handler area from the view and set the handler area to the newly added view
        page.removeHandlerArea();
        page.setCurrentView(page.pageNumberObject);
        page.makeSelectedArea(page.pageNumberObject);

        db.executeQuery("update objects set location='" + location + "' where objType='" + Globals.objType_pageNoText + "' and BID='" + currentBook.getBookID() + "' and pageNO='" + currentPageNumber + "'");
    }

    /**
     * update page number position in all pages
     *
     * @param location
     */
    public void updatePosPageNumberInAllPages(String location) {
        page.pageNumberObject.setObjLocation(location);
        int xPos = Integer.parseInt(location.split("\\|")[0]);
        int yPos = Integer.parseInt(location.split("\\|")[1]);
        page.pageNumberObject.setX(xPos);
        page.pageNumberObject.setY(yPos);
        db.executeQuery("update objects set location='" + location + "' where objType='" + Globals.objType_pageNoText + "' and BID='" + currentBook.getBookID() + "'");
        boolean showPageNo = true;
        currentBook.set_bShowPageNo(showPageNo);
        currentBook.setPageNoXPos(xPos);
        currentBook.setPageNoYPos(yPos);

        //Remove the current handler area from the view and set the handler area to the newly added view
        page.removeHandlerArea();
        page.setCurrentView(page.pageNumberObject);
        page.makeSelectedArea(page.pageNumberObject);

        db.executeQuery("update books set ShowPageNo='" + showPageNo + "' and PageNoPos='" + location + "' where BID='" + currentBook.getBookID() + "'");
    }

    /**
     * add new Web relative Layout
     *
     * @param location
     * @param size
     * @param objType
     */
    public CustomWebRelativeLayout addNewWebRL(String location, String size, String objType, String _objContent) {
        int objUniqueId = db.getMaxUniqueRowID("objects") + 1;
        int objBID = currentBook.getBookID();
        String objPageNo = currentPageNumber;
        String objContent;
        boolean objLocked = false;
        if (objType.equals(Globals.objType_pageNoText)) {
            int width = Integer.parseInt(size.split("\\|")[0]);
            int height = Integer.parseInt(size.split("\\|")[1]);
            int pxWidth = Globals.getPixelValue(width, this);
            int pxHeight = Globals.getPixelValue(height, this);
            String[] rgbFontColor = currentBook.getTemplate().getTemplateParaColor().split("-");
            objLocked = true;
            if (_objContent == null) {
                objContent = "<div id=\"content\" contenteditable=\"false\" style=\"background-color: transparent; font-size: 30px; color: rgb(" + rgbFontColor[0] + ", " + rgbFontColor[1] + ", " + rgbFontColor[2] + "); direction: ltr; text-align: center; width: " + pxWidth + "px; height: " + pxHeight + "px; box-sizing:border-box;padding-top:15px;\">";
            } else {
                objContent = _objContent;
                objContent = objContent.replace("'", "''");
            }
        } else if (objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
            objContent = _objContent;
            objContent = objContent.replace("'", "''");
        } else if (objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
            objContent = _objContent;
        } else {
            objLocked = false;
            objContent = "";
        }
        int objSequentialId = page.objectListArray.size() + 1;
        String objSclaPageToFit = "no";

        db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + objBID + "', '" + objPageNo + "', '" + location + "', '" + size + "', '" + objContent + "', '" + objSequentialId + "', '" + objSclaPageToFit + "', '" + objLocked + "', '" + page.enrichedPageId + "')");
        objContent = objContent.replace("''", "'");

        CustomWebRelativeLayout webRl = new CustomWebRelativeLayout(this, objType, objBID, objPageNo, location, size, objContent, objUniqueId, objSequentialId, objSclaPageToFit, objLocked, page.enrichedPageId, null);
        if (objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
            webRl.Language = getResources().getString(R.string.app_language);
        }

        page.addWebViewLayout(webRl);
        if (!objType.equals(Globals.objType_pageNoText)) {
            UndoRedoObjectData uRObjData = new UndoRedoObjectData();
            uRObjData.setuRObjDataUniqueId(webRl.getObjectUniqueId());
            uRObjData.setuRObjDataCustomView(webRl);
            page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_DELETED, Globals.OBJECT_PREVIOUS_STATE);
            page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CREATED, Globals.OBJECT_PRESENT_STATE);
        }

        //Remove the current handler area from the view and set the handler area to the newly added view
        page.removeHandlerArea();
        page.setCurrentView(webRl);
        page.makeSelectedArea(webRl);
        if (objType.equals("WebText")) {
            if (currentBook.getBookDirection().equals("ltr")) {
                webRl.textAlign("Left", "content");
            } else {
                webRl.textAlign("Right", "content");
            }
        }
        return webRl;
    }

    /**
     * @param imageWidth
     * @param imageHeight
     * @param imageYpos
     * @param imageXpos
     * @param imageType
     * @param objFormat
     * @author addNewImageRelativeLayout
     */
    public void addNewImageRL(int imageWidth, int imageHeight, int imageXpos, int imageYpos, String imageType, String objFormat, String path) {
        String objContent;
        int initialImageWidth = imageWidth;
        int initialImageHeight = imageHeight;
        int initialImageXpos = imageXpos;
        int initialImageYpos = imageYpos;
        String location = initialImageXpos + "|" + initialImageYpos;
        String size = initialImageWidth + "|" + initialImageHeight;

        //Insert objects before getting uniqueID
        //db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit) values(\"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\");");

        int objUniqueId = db.getMaxUniqueRowID("objects") + 1;
        String objType = imageType;
        int objBID = currentBook.getBookID();
        String objPageNo = currentPageNumber;
        String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + objBID + "/" + "images/";
        File fileImgDir = new File(imgDir);
        if (!fileImgDir.exists()) {
            fileImgDir.mkdir();
            UserFunctions.makeFileWorldReadable(imgDir);
        }

        if (objType.equals("IFrame")) {
            objContent = "http://www.nooor.com";
        } else if (objType.equals("EmbedCode")) {
            objContent = "<div style=\"font-size:20px;color:blue; text-align:center;background-color:#F5F5F5;\">Embed the code and loaded!!</div>";
        } else if (objType.equals("Audio")) {
            File audio_play_default_path = new File(imgDir + "Default_Audio_Play.png");
            File audio_stop_default_path = new File(imgDir + "Default_Audio_Stop.png");
            if (!(audio_play_default_path.exists() || audio_stop_default_path.exists())) {
                UserFunctions.copyAsset(getAssets(), "Default_Audio_Play.png", audio_play_default_path.toString());
                UserFunctions.copyAsset(getAssets(), "Default_Audio_Stop.png", audio_stop_default_path.toString());
            }
            objContent = imgDir + objUniqueId + objFormat;

            if (audio_outputFile != null) {
                UserFunctions.copyFiles(audio_outputFile, objContent);
                File audioFilePath = new File(audio_outputFile);
                audioFilePath.delete();
            } else {
                UserFunctions.copyFiles(path, objContent);
            }
            UserFunctions.makeFileWorldReadable(objContent);
        }
        else {
            objContent = imgDir +objUniqueId + objFormat;
        }

        int objSequentialId = page.objectListArray.size() + 1;
        String objSclaPageToFit = "no";
        if (objType.equals("Image") && path == null) {
            UserFunctions.copyAsset(getAssets(), "defaultbg.png", objContent);
        } else if (objType.equals("Image") && path != null) {
            UserFunctions.copyFiles(path, objContent);
        }
        if (objType.equals("YouTube") && path != null) {
            //UserFunctions.copyAsset(getAssets(), "defaultbg.png", objContent);
            UserFunctions.copyFiles(path, objContent);
            UserFunctions.makeFileWorldReadable(objContent);
            String Imagepath = Globals.TARGET_BASE_BOOKS_DIR_PATH + objBID + "/images/" + objUniqueId + ".png";
            Bitmap videoThumbnail = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
            UserFunctions.saveBitmapImage(videoThumbnail, Imagepath);
        } else if (objType.equals("YouTube") && path == null) {
            UserFunctions.copyAsset(getAssets(), "defaultbg.png", objContent);
        }
        boolean objLocked = false;

        db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + objBID + "', '" + objPageNo + "', '" + location + "', '" + size + "', '" + objContent.replace("'", "''") + "', '" + objSequentialId + "', '" + objSclaPageToFit + "', '" + objLocked + "', '" + page.enrichedPageId + "')");

        CustomImageRelativeLayout imgRl = new CustomImageRelativeLayout(this, objType, objBID, objPageNo, location, size, objContent, objUniqueId, objSequentialId, objSclaPageToFit, objLocked, page.enrichedPageId, null);
        page.addImageViewLayout(imgRl);

        UndoRedoObjectData uRObjData = new UndoRedoObjectData();
        uRObjData.setuRObjDataUniqueId(imgRl.getObjectUniqueId());
        uRObjData.setuRObjDataCustomView(imgRl);
        page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_DELETED, Globals.OBJECT_PREVIOUS_STATE);
        page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CREATED, Globals.OBJECT_PRESENT_STATE);

        //Remove the current handler area from the view and set the handler area to the newly added view
        page.removeHandlerArea();
        page.setCurrentView(imgRl);
        page.makeSelectedArea(imgRl);
    }

    @Override
    protected void onPause() {
        super.onPause();
        page.cancel(true);
        if (page.currentView != null) {
            page.currentView.clearFocus();
        }
        page.saveAllObjectsInCurrentPage(currentPageNumber);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void onBackPressed() {
        SubtabsAdapter.level1 = 0;
        backClicked=true;
        page.saveAllObjectsInCurrentPage(currentPageNumber);
        generateEnrichHtml();
        UserFunctions.copyHTMLPageToMalzamah(currentBook);
        page.deleteAllObjectsFromDb(currentPageNumber);
        db.executeQuery("update books set LastViewedPage='" + currentPageNumber + "' where BID='" + currentBook.getBookID() + "'");
        currentBook.set_lastViewedPage(Integer.parseInt(currentPageNumber));
        setResult(RESULT_OK, getIntent().putExtra("Book", currentBook));
        setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
        setResult(RESULT_OK, getIntent().putExtra("mindMap", mindMapEdited));
        setResult(RESULT_OK, getIntent().putExtra("currentPageNo", currentPageNumber));
        setResult(RESULT_OK, getIntent().putExtra("enrichmentTabId", navigateEnrichId));

      if(currentBook.is_bStoreBook()||!titleTextClicked) {
          finish();
       }
    }
//	public void onBackPressed(){
//		page.saveAllObjectsInCurrentPage(currentPageNumber);
//		generateEnrichHtml();
//		UserFunctions.copyHTMLPageToMalzamah(currentBook);
//		page.deleteAllObjectsFromDb(currentPageNumber);
//		db.executeQuery("update books set LastViewedPage='" + currentPageNumber + "' where BID='" + currentBook.getBookID() + "'");
//		currentBook.set_lastViewedPage(Integer.parseInt(currentPageNumber));
//		setResult(RESULT_OK, getIntent().putExtra("Book", currentBook));
//		setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
//		finish();
//	}

    /**
     * This Method will Generate only Enriched html
     */
    private void generateEnrichHtml() {
        if (currentBook.is_bStoreBook()||currentBook.get_bStoreID()!=null&&currentBook.get_bStoreID().contains("P")) {
            if (currentBook.get_bStoreID().contains("C")) {
                String htmlFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/";
                new GenerateHTML(BookViewActivity.this, db, currentBook, currentPageNumber, enrichmentTabListArray,false).generatePage(htmlFilePath, page.deletedObjectsListArray);
            } else {
                if (enrichmentTabListArray.size() > 0) {
                    new GenerateHTML(BookViewActivity.this, db, currentBook, currentPageNumber, enrichmentTabListArray,false).generateOnlyEnrichedPages(page.deletedObjectsListArray);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        ////System.out.println("onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        ////System.out.println("onResume");
        super.onResume();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
        //Highlight the Current Selection View
        if (position != Integer.parseInt(currentPageNumber) - 1 && page.getStatus() == AsyncTask.Status.FINISHED) {
            subTabsLayout.setVisibility(View.GONE);
            subTabLayout1.setVisibility(View.GONE);
            SubtabsAdapter.level1 = SubtabsAdapter.level2 = SubtabsAdapter.level3 = 0;
            generateEnrichHtml();
            int wantedPosition = Integer.parseInt(currentPageNumber) - 1;
            int firstPosition = pageListView.getFirstVisiblePosition() - pageListView.getHeaderViewsCount();
            int calculatedPosition = wantedPosition - firstPosition;
            currentSelectedListView = adapterView.getChildAt(calculatedPosition);

            if (currentSelectedListView != null && currentSelectedListView != view) {

                UnHighlightCurrentSlectedView(currentSelectedListView);
            }
            currentSelectedListView = view;
            highlightCurrentSlectedView(currentSelectedListView);

            if(!currentBook.is_bStoreBook()&&currentBook.getbCategoryId()!=6 || currentBook.get_bStoreID().contains("C")) {
                  if (!wv_advSearch.isShown()) {
                        new takeAndSaveScreenshot(currentPageNumber).execute();
                    }
            }

            int pageNumber = position + 1;
            checkForEnrichments(pageNumber, 0);
            checkForBkmrkEnrichments(pageNumber);
            instantiateNewPage(pageNumber, 0);
            listViewOnLongClick = false;
            pageAdapter.notifyDataSetChanged();
            if (Integer.parseInt(currentPageNumber) != 1 && Integer.parseInt(currentPageNumber) != currentBook.getTotalPages()) {
                enr_layout.setVisibility(View.VISIBLE);
            }else{
                enr_layout.setVisibility(View.GONE);
            }
       //     pageListView.invalidateViews();
        }
    }

    /**
     * @param from
     * @param to
     * @author RearrangeListViewItem
     */
    @Override
    public void drop(int from, int to) {
        if (from != pageCount - 1 && from != 0 && to != 0 && to != pageCount - 1) {
            int fromPageNumber = from + 1; //2
            int toPageNumber = to + 1; //4

            page.swapePageScreenshotsAndBackground(fromPageNumber, toPageNumber);
            String filePath=Globals.TARGET_BASE_BOOKS_DIR_PATH +currentBook.getBookID()+"/FreeFiles/"+"malzamah.json";
            if(new File(filePath).exists()) {
                updatingJsonFile(filePath, 0, true, fromPageNumber, toPageNumber);
            }
            db.executeQuery("update objects set pageNO = CASE pageNO when '" + fromPageNumber + "' then '" + toPageNumber + "' when '" + toPageNumber + "' then '" + fromPageNumber + "' else pageNO end where BID='" + currentBook.getBookID() + "'");
            db.executeQuery("update enrichments set pageNO = CASE pageNO when '" + fromPageNumber + "' then '" + toPageNumber + "' when '" + toPageNumber + "' then '" + fromPageNumber + "' else pageNO end where BID='" + currentBook.getBookID() + "'");

            wv_advSearch.setVisibility(View.INVISIBLE);
//			canvasView.setVisibility(View.INVISIBLE);
//			twoDScroll.setVisibility(View.INVISIBLE);
//			drawingView.setVisibility(View.INVISIBLE);
            bgImgView.setVisibility(View.VISIBLE);
            btnEnrHome.setSelected(true);
            makeEnrichedBtnSlected(btnEnrHome);
            instantiateNewPage(toPageNumber, 0);
            pageListView.invalidateViews();
        } else {
            Toast.makeText(this, R.string.cover_end_page_cannot_swap, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * @param which
     * @author RemoveListViewItem
     */
    @Override
    public void remove(int which) {
        //System.out.println("remove");
        final int deletePageNumber = which + 1;
        if (pageCount > 3) {
            Builder builder = new AlertDialog.Builder(this);
            builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pageCount = pageCount - 1;
                    currentBook.setTotalPages(pageCount);
                    page.deletePage(currentBook.getBookID(), deletePageNumber);
                    String filePath=Globals.TARGET_BASE_BOOKS_DIR_PATH +currentBook.getBookID()+"/FreeFiles/"+"malzamah.json";
                   // JSONArray  jsonArray = new JSONArray();
                    if(new File(filePath).exists()){
                        updatingJsonFile(filePath,deletePageNumber,false,0,0);
                    }
                    instantiateNewPage(Integer.parseInt(currentPageNumber), 0);
                    pageListView.invalidateViews();
                }
            });

            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    pageAdapter.notifyDataSetChanged();
                }
            });
            builder.setTitle(R.string.delete_page);
            builder.setMessage(R.string.suredelete);
            builder.show();
        } else {
            Toast.makeText(this, R.string.cannot_be_deleted, Toast.LENGTH_SHORT).show();
            pageAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
                                   long arg3) {
        listViewOnLongClick = true;
        pageListView.invalidateViews();
        return true;
    }

    private void updatingJsonFile(String filePath, int deletePageNumber,boolean pageSwap,int fromPageNumber,int toPageNumber){
        File file=new File(filePath);
        String jsonStr=null;
        JSONArray jsonarray = null;
        try {
            FileInputStream stream = new FileInputStream(file);
            FileChannel fc = stream.getChannel();
            try {
                stream = new FileInputStream(file);
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                jsonStr = Charset.defaultCharset().decode(bb).toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }

            JSONObject jsonObject=new JSONObject(jsonStr);
            JSONArray data=jsonObject.getJSONArray("malzamah");
            //String  d= data.toString().replace("[","");
            jsonarray = new JSONArray();
            if(pageSwap) {
                for (int i = 0; i < data.length(); i++) {
                    JSONObject jsonSub = data.getJSONObject(i);
                    int pageNo = jsonSub.getInt("page");
                    String sourceHtmlPath=jsonSub.getString("htmlPath");
                    if (pageNo == fromPageNumber) {
                        String query = "update malzamah set pageNO='"+toPageNumber+"' where sourceHtmlPath='"+sourceHtmlPath+"' and BID='"+currentBook.getBookID()+"'";
                        db.executeQuery(query);
                        jsonSub.put("page", toPageNumber);
                    }
                    if (pageNo == toPageNumber) {
                        String query = "update malzamah set pageNO='"+fromPageNumber+"' where sourceHtmlPath='"+sourceHtmlPath+"' and BID='"+currentBook.getBookID()+"'";
                        db.executeQuery(query);
                        jsonSub.put("page", fromPageNumber);
                    }
                    jsonarray.put(jsonSub);
                }
            }else {
                for (int i = 0; i < data.length(); i++) {
                    JSONObject jsonSub = data.getJSONObject(i);
                    int pageNo = jsonSub.getInt("page");
                    String sourceHtmlPath=jsonSub.getString("htmlPath");
                    if(pageNo==deletePageNumber){
                        db.executeQuery("delete from malzamah where pageNO='"+pageNo+"' and sourceHtmlPath='"+sourceHtmlPath+"' and BID='"+currentBook.getBookID()+"'");

                       // String query = "update malzamah set pageNO='"+pageNo+"' where sourceHtmlPath='"+sourceHtmlPath+"' and BID='"+currentBook.getBookID()+"'";
                        //db.executeQuery(query);
                    }
                    if (pageNo!= deletePageNumber) {
                        int pNo = pageNo - 1;
                        if (pageNo > deletePageNumber) {
                            jsonSub.put("page", pNo);
                        }
                        jsonarray.put(jsonSub);
                    }
                }
            }
            JSONObject jsonData=new JSONObject();

            jsonData.put("malzamah",jsonarray);
            FileWriter jsonFile=new FileWriter(new File(filePath));
            jsonFile.write(jsonData.toString());
            jsonFile.flush();
            jsonFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    /**
     * Instantiate New Page
     *
     * @param pageNumber
     */
    public void instantiateNewPage(int pageNumber, int enrichedId) {
        if (page.getStatus() == AsyncTask.Status.FINISHED) {
            if (page.currentView != null) {
                page.currentView.clearFocus();
            }
            if ((pageNumber == 1 || pageNumber == pageCount) || currentBook.is_bStoreBook()) {
                btnAddPageNo.setEnabled(false);
            } else {
                btnAddPageNo.setEnabled(true);
            }
            if (currentBook.is_bStoreBook()) {
                btnAddPageNo.setVisibility(View.GONE);
            }
            //Save All Objects in Current Page and Remove All the views
            page.saveAllObjectsInCurrentPage(currentPageNumber);
            page.deleteAllObjectsFromDb(currentPageNumber);
            designPageLayout.removeViews(1, designPageLayout.getChildCount() - 1);

            //Set Design page and imgview to device height
            page.extendDesignPageLayout(Globals.getDeviceHeight());

            String pageType = swapScrollViews(enrichedId);

            if (currentBook.is_bStoreBook() && !currentBook.get_bStoreID().contains("C") && enrichedId == 0 &&currentBook.get_bStoreID().contains("P")) {
                disableToolbarButtons();
            } else if (currentBook.is_bStoreBook() && !currentBook.get_bStoreID().contains("C") && enrichedId != 0) {
                enableToolbarButtons();
            } else if (!currentBook.is_bStoreBook()&&currentBook.getbCategoryId()!=6) {
                enableToolbarButtons();
            } else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("C") && enrichedId == 0 || currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("C") && enrichedId != 0) {
                enableToolbarButtons();
            } else {
                disableToolbarButtons();
            }

            navigateEnrichId = enrichedId;
            //Increment the Current Page Number and load the new Page
            //int pageNumber = pageCount - 1;
            currentPageNumber = String.valueOf(pageNumber);

            page = null;
            page = (Page) new Page(this, db, global, currentPageNumber, enrichedId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
            page.setPageType(pageType);
        }
    }

    /**
     * swap scroll view for duplicate and blank enriched page
     *
     * @param enrichedId
     * @return
     */
    private String swapScrollViews(int enrichedId) {
        String pageType = null;
        //if (currentBook.is_bStoreBook()) {
        for (Enrichments enrichments : enrichmentTabListArray) {
            if (enrichedId == enrichments.getEnrichmentId()) {
                if (enrichments.getEnrichmentType().equals("duplicate")) {
					/*if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab()) {
						mainDesignView.removeView(designPageLayout);
						designScrollPageView.removeView(designPageLayout);
						mainDesignView.removeView(designScrollPageView);
						mainDesignView.addView(designScrollPageView, 0);
						designScrollPageView.setVisibility(View.VISIBLE);
						designScrollPageView.addView(designPageLayout);
					}*/
                    pageType = "duplicate";
                } else {
					/*if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab()) {
						designScrollPageView.removeView(designPageLayout);
						mainDesignView.removeView(designScrollPageView);
						mainDesignView.removeView(designPageLayout);
						mainDesignView.addView(designPageLayout, 0);
					}*/
                    pageType = "blank";
                }
                break;
            }
        }
        //}
        return pageType;
    }

    /**
     * Save screenshot of the current page
     *
     * @author Suriya
     */
    public class takeAndSaveScreenshot extends AsyncTask<Void, Void, Void> {
        String screenshot_path, pNumber;
        Bitmap bitmap;

        public takeAndSaveScreenshot(String currentPageNumber) {
            this.pNumber = currentPageNumber;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            int pageNo = Integer.parseInt(pNumber);
            if (pageNo != pageCount) {
                screenshot_path = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_" + pageNo + ".png";
            } else {
                screenshot_path = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_end.png";
            }
            bitmap = takeScreenShot();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (bitmap != null) {
                try {
                    int width = (int) getResources().getDimension(R.dimen.inflate_pageview_width);
                    int height = (int) getResources().getDimension(R.dimen.inflate_pageview_height);
                    Bitmap scaleBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(width, BookViewActivity.this), Globals.getDeviceIndependentPixels(height, BookViewActivity.this), true);
                    saveBitmapImage(scaleBitmap, screenshot_path);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pageListView.invalidateViews();
        }

        public void saveBitmapImage(Bitmap bitmap, String path) {
            File imagePath = new File(path);

            FileOutputStream fos;
            try{
                fos = new FileOutputStream(imagePath);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.flush();
                fos.close();
            }catch(FileNotFoundException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }
        }

        public Bitmap takeScreenShot() {
            designPageLayout.destroyDrawingCache();
            designPageLayout.setDrawingCacheEnabled(true);
            designPageLayout.setDrawingCacheQuality(RelativeLayout.DRAWING_CACHE_QUALITY_HIGH);
            designPageLayout.buildDrawingCache();
            Bitmap bitmap = designPageLayout.getDrawingCache();
            return bitmap;
        }
    }

    /**
     * @param view
     * @author HighlightView
     */
    public void highlightCurrentSlectedView(View view) {
        view.setBackgroundColor(getResources().getColor(R.color.darkColor));
    }

    /**
     * @param view
     * @author UnhighlightView
     */
    public void UnHighlightCurrentSlectedView(View view) {
        view.setBackgroundColor(Color.TRANSPARENT);
    }

    /**
     * Paste Image in the view
     *
     * @param imgRl
     */
    private void pasteImageRl(CustomImageRelativeLayout imgRl) {
        int toastMsg = 0;
        String objContent = null;
        int initialImageWidth = imgRl.getWidth();
        int initialImageHeight = imgRl.getHeight();
        int initialImageXpos = designPageLayout.getWidth() / 2 - initialImageWidth / 2;
        int initialImageYpos = designPageLayout.getHeight() / 2 - initialImageHeight / 2;
        String location = initialImageXpos + "|" + initialImageYpos;
        String size = initialImageWidth + "|" + initialImageHeight;

        int objUniqueId = db.getMaxUniqueRowID("objects") + 1;
        String objType = imgRl.getObjectType();
        int objBID = currentBook.getBookID();
        String objPageNo = currentPageNumber;
        String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + objBID + "/" + "images/";
        File fileImgDir = new File(imgDir);
        if (!fileImgDir.exists()) {
            fileImgDir.mkdir();
            UserFunctions.makeFileWorldReadable(imgDir);
        }

        if (objType.equals("Image")) {
            toastMsg = R.string.image_pasted;
            String copiedObjPathContent = imgRl.getObjectPathContent();
            if (new File(copiedObjPathContent).exists()) {
                objContent = imgDir + objUniqueId + ".png";
                UserFunctions.copyFiles(copiedObjPathContent, objContent);
            }
        } else if (objType.equals("YouTube")) {
            toastMsg = R.string.video_pasted;
            String copiedObjPathContent = imgRl.getObjectPathContent();
            if (copiedObjPathContent.contains("data/data")) {
                if (new File(copiedObjPathContent).exists()) {
                    objContent = imgDir + objUniqueId + ".mp4";
                    UserFunctions.copyFiles(copiedObjPathContent, objContent);
                    UserFunctions.makeFileWorldReadable(objContent);
                }
            } else {
                objContent = copiedObjPathContent;
            }

            // Copy Image if exist
            String copiedImgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + imgRl.getObjectBID() + "/" + "images/" + imgRl.getObjectUniqueId() + ".png";
            if (new File(copiedImgPath).exists()) {
                String imgObjContent = imgDir + objUniqueId + ".png";
                UserFunctions.copyFiles(copiedImgPath, imgObjContent);
            }
        } else if (objType.equals("Audio")) {
            toastMsg = R.string.audio_pasted;
            String copiedObjPathContent = imgRl.getObjectPathContent();
            if (new File(copiedObjPathContent).exists()) {
                objContent = imgDir + objUniqueId + ".m4a";
                UserFunctions.copyFiles(copiedObjPathContent, objContent);
                UserFunctions.makeFileWorldReadable(objContent);
            }
        } else if (objType.equals("IFrame")) {
            toastMsg = R.string.iframe_pasted;
            objContent = imgRl.getObjectPathContent();
        } else if (objType.equals("EmbedCode")) {
            toastMsg = R.string.embed_view_pasted;
            objContent = imgRl.getObjectPathContent();
        }

        int objSequentialId = page.objectListArray.size() + 1;
        String objSclaPageToFit = "no";
        boolean objLocked = imgRl.isObjLocked();

        db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + objBID + "', '" + objPageNo + "', '" + location + "', '" + size + "', '" + objContent + "', '" + objSequentialId + "', '" + objSclaPageToFit + "', '" + objLocked + "', '" + page.enrichedPageId + "')");

        CustomImageRelativeLayout custImgRl = new CustomImageRelativeLayout(this, objType, objBID, objPageNo, location, size, objContent, objUniqueId, objSequentialId, objSclaPageToFit, objLocked, page.enrichedPageId, null);
        page.addImageViewLayout(custImgRl);

        UndoRedoObjectData uRObjData = new UndoRedoObjectData();
        uRObjData.setuRObjDataUniqueId(custImgRl.getObjectUniqueId());
        uRObjData.setuRObjDataCustomView(custImgRl);
        page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_DELETED, Globals.OBJECT_PREVIOUS_STATE);
        page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CREATED, Globals.OBJECT_PRESENT_STATE);

        //Remove the current handler area from the view and set the handler area to the newly added view
        page.removeHandlerArea();
        page.setCurrentView(custImgRl);
        page.makeSelectedArea(custImgRl);

        Toast.makeText(this, toastMsg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Paste WebText and WebTable to the view
     *
     * @param webRL
     */
    private void pasteWebRl(CustomWebRelativeLayout webRL) {
        String toastMsg = null;
        String objType = webRL.getObjType();
        int initialWebTextWidth = webRL.getWidth();
        int initialWebTextHeight = webRL.getHeight();
        int initialWebTextXpos = designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
        int initialWebTextYpos = designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
        String location = initialWebTextXpos + "|" + initialWebTextYpos;
        String size = null;
        if (objType.equals("TextCircle")) {
            toastMsg = "Circle Pasted";
            size = initialWebTextWidth + "|" + initialWebTextHeight;
        } else if (objType.equals("WebTable")) {
            toastMsg = "Table Pasted";
            int rows = webRL.getmWebTableRow();
            int columns = webRL.getmWebTableColumn();
            size = initialWebTextWidth + "|" + initialWebTextHeight + "|" + rows + "|" + columns;
        } else {
            toastMsg = "Text Pasted";
            size = initialWebTextWidth + "|" + initialWebTextHeight;
        }

        int objUniqueId = db.getMaxUniqueRowID("objects") + 1;
        int objBID = currentBook.getBookID();
        String objPageNo = currentPageNumber;
        String objContent = webRL.getObjectContent();
        objContent = objContent.replace("'", "''");
        int objSequentialId = page.objectListArray.size() + 1;
        String objSclaPageToFit = "no";
        boolean objLocked = webRL.isObjLocked();

        db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + objBID + "', '" + objPageNo + "', '" + location + "', '" + size + "', '" + objContent + "', '" + objSequentialId + "', '" + objSclaPageToFit + "', '" + objLocked + "', '" + page.enrichedPageId + "')");
        objContent = objContent.replace("''", "'");

        CustomWebRelativeLayout custWebRl = new CustomWebRelativeLayout(this, objType, objBID, objPageNo, location, size, objContent, objUniqueId, objSequentialId, objSclaPageToFit, objLocked, page.enrichedPageId, null);
        page.addWebViewLayout(custWebRl);

        UndoRedoObjectData uRObjData = new UndoRedoObjectData();
        uRObjData.setuRObjDataUniqueId(custWebRl.getObjectUniqueId());
        uRObjData.setuRObjDataCustomView(custWebRl);
        page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_DELETED, Globals.OBJECT_PREVIOUS_STATE);
        page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CREATED, Globals.OBJECT_PRESENT_STATE);

        //Remove the current handler area from the view and set the handler area to the newly added view
        page.removeHandlerArea();
        page.setCurrentView(custWebRl);
        page.makeSelectedArea(custWebRl);

        Toast.makeText(this, toastMsg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Call this method to paste the view copied to the clipboard
     */
    public void pasteViews() {
        View clipboardView = Globals.getClipboardView();
        if (clipboardView instanceof CustomImageRelativeLayout) {
            pasteImageRl((CustomImageRelativeLayout) clipboardView);
        } else if (clipboardView instanceof CustomWebRelativeLayout) {
            pasteWebRl((CustomWebRelativeLayout) clipboardView);
        }
    }

    /**
     * Gesture listener class for the double tap event
     *
     * @author Suriya
     */
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            ////System.out.println("DoubleTap");
            pasteViews();
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            // TODO Auto-generated method stub
            System.out.println("single tap");
            if (Integer.parseInt(currentPageNumber) != 1 && Integer.parseInt(currentPageNumber) != currentBook.getTotalPages()) {
                if (enr_layout.getVisibility() == View.VISIBLE) {
                    enr_layout.setVisibility(View.GONE);
                } else {
                    if (isCreateBlankEnr == null) {
                        enr_layout.setVisibility(View.VISIBLE);
                    }
                }
            }
            return true;
        }


    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int action = event.getAction();
        int positionX = (int) event.getX();
        int positionY = (int) event.getY();

        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                gestureStartPointX = (int) event.getRawX();
                gestureStartPointY = (int) event.getRawY();

                if (pickColorFromBgView) {
                    hidePageListView();
                    new bgThrea(positionX, positionY).execute();
                } else {
                    if (page.currentView != null) {
                        if (page.currentView instanceof CustomWebRelativeLayout) {
                            if (((CustomWebRelativeLayout) page.currentView).webView.actionMode != null) {
                                ((CustomWebRelativeLayout) page.currentView).webView.clearFocus();
                                ((CustomWebRelativeLayout) page.currentView).webView.actionMode = null;
                            }
                            if (((CustomWebRelativeLayout) page.currentView).webView.callBack != null){
                                ((CustomWebRelativeLayout) page.currentView).webView.clearFocus();
                                ((CustomWebRelativeLayout) page.currentView).webView.callBack = null;
                            }
                        }
                        page.removeHandlerArea();
                    }
                }
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                if (pickColorFromBgView) {
                    new bgThrea(positionX, positionY).execute();
                }
                break;
            }

            case MotionEvent.ACTION_UP: {
                gestureEndPointX = (int) event.getRawX();
                gestureEndPointY = (int) event.getRawY();

			/*int pageViewX = (int) pageView.getX();
			if (pageViewX == 0) {
				if (gestureEndPointX < 300 && (gestureStartPointX - gestureEndPointX > 25)) {
					hidePageListView();
				}
			}else{
				if (gestureEndPointX < 300 && (gestureEndPointX - gestureStartPointX > 25)) {
					showPageListView();
				}
			}*/
                if (pickColorFromBgView) {
                    setPickedColorForObjects();
                    rootView.removeView(pickClrImgView);
                    pickClrImgView = null;
                    pickColorFromBgView = false;
                }
                break;
            }

            default:
                break;
        }
        return gestureDetector.onTouchEvent(event);
    }

    /**
     * Set Picked for the objects from the view
     */
    private void setPickedColorForObjects() {
        ColorDrawable drawable = (ColorDrawable) pickClrImgView.getBackground();
        int color = drawable.getColor();
        String pickedColor = String.format("#%06X", (0xFFFFFF & color));
        if (((CustomWebRelativeLayout) page.currentView).getObjType().equals("WebText") || ((CustomWebRelativeLayout) (page.currentView)).getObjType().equals(Globals.OBJTYPE_TITLETEXT) || ((CustomWebRelativeLayout) (page.currentView)).getObjType().equals(Globals.OBJTYPE_AUTHORTEXT)) {
            if (pickedColorFor.equals("FontColor")) {
                ((CustomWebRelativeLayout) page.currentView).changeFontColor(pickedColor, "content");
                ArrayList<String> recentFontColorList = page.getRecentColorsFromSharedPreference(Globals.fontColorKey);
                page.setandUpdateRecentColorsToSharedPreferences(Globals.fontColorKey, recentFontColorList, pickedColor);
            } else if (pickedColorFor.equals("BackgroundColor")) {
                ((CustomWebRelativeLayout) page.currentView).changeBackgroundColor(pickedColor, "content");
                ArrayList<String> recentBackgroundColorList = page.getRecentColorsFromSharedPreference(Globals.backgroundColorKey);
                page.setandUpdateRecentColorsToSharedPreferences(Globals.backgroundColorKey, recentBackgroundColorList, pickedColor);
            }
        } else if (((CustomWebRelativeLayout) page.currentView).getObjType().equals("WebTable")) {
            if (pickedColorFor.equals("FontColor")) {
                ((CustomWebRelativeLayout) page.currentView).changeFontColor(pickedColor, "SBATable");
                ArrayList<String> recentFontColorList = page.getRecentColorsFromSharedPreference(Globals.webTablefontColorKey);
                page.setandUpdateRecentColorsToSharedPreferences(Globals.webTablefontColorKey, recentFontColorList, pickedColor);
            } else if (pickedColorFor.equals("BackgroundColor")) {
                ((CustomWebRelativeLayout) page.currentView).changeBackgroundColor(pickedColor, "SBATable");
                ArrayList<String> recentWebTableBackgroundColorList = page.getRecentColorsFromSharedPreference(Globals.webTablebackgroundColorKey);
                page.setandUpdateRecentColorsToSharedPreferences(Globals.webTablebackgroundColorKey, recentWebTableBackgroundColorList, pickedColor);
            }
        }
    }

    private class bgThrea extends AsyncTask<Void, Void, Void> {

        int x, y;
        int pixel;

        public bgThrea(int eventX, int eventY) {
            x = eventX;
            y = eventY;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //System.out.println("eventX:"+x+"eventY:"+y);
            if (pickClrImgView == null) {
                rootView.setDrawingCacheEnabled(true);
                rootView.buildDrawingCache();
                pickClrImgView = new ImageView(getApplicationContext());
                pickClrImgView.setLayoutParams(new LayoutParams(Globals.getDeviceIndependentPixels(100, BookViewActivity.this), Globals.getDeviceIndependentPixels(100, BookViewActivity.this)));
                rootView.addView(pickClrImgView);
            }
            if (y <= pickClrImgView.getHeight() + Globals.getDeviceIndependentPixels(50, BookViewActivity.this)) {
                pickClrImgView.setX(x - Globals.getDeviceIndependentPixels(50, BookViewActivity.this));
                pickClrImgView.setY(y + (pickClrImgView.getHeight() + Globals.getDeviceIndependentPixels(50, BookViewActivity.this)));
            } else {
                pickClrImgView.setX(x - Globals.getDeviceIndependentPixels(50, BookViewActivity.this));
                pickClrImgView.setY(y - (pickClrImgView.getHeight() + Globals.getDeviceIndependentPixels(50, BookViewActivity.this)));
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (pickClrImgView != null) {
                Bitmap bitmap = rootView.getDrawingCache();
                pixel = bitmap.getPixel(x, y);
                //System.out.println("pixel:"+pixel);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pickClrImgView != null) {
                pickClrImgView.setBackgroundColor(pixel);
            }
        }

    }

	/*@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		if (newConfig.hardKeyboardHidden == Configuration.KEYBOARDHIDDEN_NO) {
			Toast.makeText(this, "KeyBoard Visible", Toast.LENGTH_SHORT).show();
		} else if (newConfig.hardKeyboardHidden == Configuration.KEYBOARDHIDDEN_YES) {
			Toast.makeText(this, "KeyBoard Hidden", Toast.LENGTH_SHORT).show();
		}
	}*/

    /**
     * @author hidePageListView
     */
    public void hidePageListView() {
        drawerLayout.closeDrawer(Gravity.START);
		/*if (pageView.getX() == 0) {
			pageView.setX(-pageListView.getWidth());

			TranslateAnimation ta = new TranslateAnimation(-pageView.getX(), 0, 0, 0);
			ta.setDuration(200);
			pageView.startAnimation(ta);
		}*/
    }

    /**
     * @author showPageListView
     */
    public void showPageListView() {
        drawerLayout.openDrawer(Gravity.LEFT);
		/*TranslateAnimation ta = new TranslateAnimation(pageView.getX(), 0, 0, 0);
		ta.setDuration(200);
		pageView.startAnimation(ta);

		pageView.setX(0);*/
    }

    /**
     * Method to catch onActivityResult for picking images/videos from gallery and camera.
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        View view = page.getCurrentView();
        //	View vieww = page.getCurrentView();
        if (view != null && view instanceof CustomImageRelativeLayout) {
            CustomImageRelativeLayout imageRLayout = (CustomImageRelativeLayout) view;
            if (requestCode == imageRLayout.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    ////System.out.println("imagePath:"+imageRLayout.imageFileUri.getPath());
                    imageRLayout.setImageBackground(imageRLayout.imageFileUri.getPath());
                    imageRLayout.setUndoRedoForImageContentChanged(imageRLayout.imageFileUri.getPath(), null);
                    imageRLayout.SaveUriFile(imageRLayout.imageFileUri.getPath());
                } else if (resultCode == RESULT_CANCELED) {
                    //System.out.println("User cancelled the image capture");
                } else {
                    //System.out.println("Image capture failed");
                }
            }else if (requestCode == imageRLayout.CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE){
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String videoPath = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
                    Bitmap videoThumbnail = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);
                    imageRLayout.setUndoRedoForImageContentChanged(videoPath, videoThumbnail);
                    if (videoThumbnail != null) {
                        imageRLayout.copyThumbnnailImageForVideo(videoThumbnail);
                    }
                    imageRLayout.SaveUriForVideoFile(videoPath);
                } else if (resultCode == RESULT_CANCELED) {
                    //System.out.println("Cancelled operation to pic video from gallery");
                }
            }
            else if (requestCode == imageRLayout.CAPTURE_GALLERY) {
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
					/*String[] filePathColumn ={MediaStore.Images.Media.DATA};
					Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    String picturePath = null;
					if (cursor.moveToFirst()) {
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        picturePath = cursor.getString(columnIndex);
                    }
					cursor.close();*/
                    String picturePath = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
                    if (picturePath != null) {
                        imageRLayout.setImageBackground(picturePath);
                        imageRLayout.setUndoRedoForImageContentChanged(picturePath, null);
                        imageRLayout.SaveUriFile(picturePath);
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    //System.out.println("Cancelled operation to pic image from gallery");
                }
            } else if (requestCode == imageRLayout.VIDEO_GALLERY) {
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
					/*String[] filePathColumn ={MediaStore.Images.Media.DATA};
					Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
					cursor.moveToFirst();
					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					String videoPath = cursor.getString(columnIndex);
					cursor.close();*/
                    String videoPath = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
                    Bitmap videoThumbnail = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);
                    imageRLayout.setUndoRedoForImageContentChanged(videoPath, videoThumbnail);
                    if (videoThumbnail != null) {
                        imageRLayout.copyThumbnnailImageForVideo(videoThumbnail);
                    }
                    imageRLayout.SaveUriForVideoFile(videoPath);
                } else if (resultCode == RESULT_CANCELED) {
                    //System.out.println("Cancelled operation to pic video from gallery");
                }
            }
        }
        if (requestCode == PAGE_BACKGROUND_GALLERY) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
				/*String[] filePathColumn ={MediaStore.Images.Media.DATA};
				Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				backgroundSrcImage = cursor.getString(columnIndex);*/
                String backgroundSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
                //ApplyPageBackground(selectedImagePath);
                showBackgroundDialog(backgroundSrcImage, Globals.OBJTYPE_PAGEBG);
				/*if(Integer.parseInt(currentPageNumber) == 1 || Integer.parseInt(currentPageNumber) == pageCount){
					btnApplyAll.setVisibility(View.GONE);
				}else{
					btnApplyAll.setVisibility(View.VISIBLE);
				}*/
                //cursor.close();

            } else if (resultCode == RESULT_CANCELED) {
                //System.out.println("Cancelled operation to pic image for page background");
            }
        }
        if (view != null && view instanceof CustomWebRelativeLayout) {
            CustomWebRelativeLayout webRLayout = (CustomWebRelativeLayout) view;

            if (requestCode == webRLayout.CAPTURE_FROM_GALLERY) {
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String quiz_ImageName = webRLayout.quizImageFileName;
                    String quiz_imgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/images/" + "" + quiz_ImageName + ".png";
                    String picturePath = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
                    if (picturePath != null) {
                        //webRLayout.webView.clearCache(true);
                        webRLayout.SaveUriFile(picturePath, quiz_imgPath);
                        String bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID();
                        int randomNoToRefresh = new Random().nextInt();

                        CustomWebRelativeLayout.loadJSScript("javascript:document.getElementById('" + quiz_ImageName + "').src=\"/" + bookDir + "/images/" + quiz_ImageName + ".png?" + randomNoToRefresh + "\"", webRLayout.webView);
                    }
                }
            }
            if (requestCode == webRLayout.CAPTURE_WEB_IMAGE_ACTIVITY_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    String quiz_CamImageName = webRLayout.quizImageFileName;
                    String quiz_imgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/images/" + "" + quiz_CamImageName + ".png";
                    webRLayout.SaveUriFile(webRLayout.imageFileUri.getPath(), quiz_imgPath);
                    String bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID();
                    int randomNoToRefresh = new Random().nextInt();

                    CustomWebRelativeLayout.loadJSScript("javascript:document.getElementById('" + quiz_CamImageName + "').src=\"/" + bookDir + "/images/" + quiz_CamImageName + ".png?" + randomNoToRefresh + "\"", webRLayout.webView);

                } else if (resultCode == RESULT_CANCELED) {

                    //System.out.println("User cancelled the image capture");
                } else {
                    //System.out.println("Image capture failed");
                }
            }
        }
        if (requestCode == MindMapDialog.MindMapActivityRequestCode) {
            if (data != null) {
                boolean isOpenMindMapDialog = data.getExtras().getBoolean("MIOpenDialog", false);
                if (isOpenMindMapDialog) {
                    Globals.mindMapFileNameLastOpened = null;
                    String orphanNode = data.getExtras().getString("MIOrphanNodeText", "");
                    MindMapDialog mindMapDialog = new MindMapDialog(BookViewActivity.this, false);
                    mindMapDialog.showMindMapDialog(orphanNode);
                }
                String mindMapTitle = data.getExtras().getString("MITitle");
                if (mindMapTitle != null && mindMapTitle != "") {
                    UserFunctions.saveMindMapScreenShotForPageBg(mindMapTitle, db, currentBook, gridShelf);
                }
                instantiateNewPage(Integer.parseInt(currentPageNumber), page.enrichedPageId);
            }
        }
        if (requestCode == WebviewRequestCode){
            if (resultCode == RESULT_OK){
                boolean addPage = false;
                Book book = (Book) data.getSerializableExtra("book");
                GridShelf grShelf = (GridShelf) data.getSerializableExtra("Shelf");
                gridShelf = grShelf;
                currentBook = book;
                if (pageCount<currentBook.getTotalPages()){
                    int count = currentBook.getTotalPages()-pageCount;
                    for (int i=0;i<count;i++){
                        int pageNumber = pageCount;
                        int cursorValue = pageNumber - 1;
                        cursor.newRow().add(cursorValue).add("Page: " + cursorValue);
                        pageCount++;
                        addPage = true;
                    }
                }
                pageCount = currentBook.getTotalPages();
                pageAdapter.changeCursor(cursor);
                instantiateNewPage(Integer.parseInt(currentPageNumber), page.enrichedPageId);
                pageAdapter.notifyDataSetChanged();
                pageListView.invalidateViews();
                if (addPage){
                    UserFunctions.DisplayAlertDialog(BookViewActivity.this,R.string.add_webpage,R.string.add_page);
                }
            }
        }
        if (requestCode == cloudActivity_callback){
            if (resultCode == RESULT_OK){
                int pageNumber = data.getExtras().getInt("PageNumber",1);
                instantiateNewPage(pageNumber,0);
            }
        }
        if (requestCode == ActivityFileManager) {
            if (resultCode == RESULT_OK) {
                Book book = (Book) data.getSerializableExtra("Book");
                String imageType = data.getExtras().getString("imageType");
                String path = data.getExtras().getString("path");
                currentBook = book;
                //designScrollPageView.is
                //if(currentBook.is_bStoreBook() && page.enrichedPageId!=0  ||!currentBook.is_bStoreBook()) {
                if (imageType.equals("Image")) {
                    hidePageListView();
                    int imageWidth = (int) getResources().getDimension(R.dimen.bookview_initial_image_width);
                    int imageHeight = (int) getResources().getDimension(R.dimen.bookview_initial_image_height);
                    int imageXpos = designPageLayout.getWidth() / 2 - imageWidth / 2;
                    int imageYpos = designPageLayout.getHeight() / 2 - imageHeight / 2;
                    String Type = "Image";
                    String objFormat = ".png";
                    addNewImageRL(imageWidth, imageHeight, imageXpos, imageYpos, Type, objFormat, path);
                } else if (imageType.equals("Video")) {
                    hidePageListView();
                    int imageWidth = (int) getResources().getDimension(R.dimen.bookview_initial_video_width);
                    int imageHeight = (int) getResources().getDimension(R.dimen.bookview_initial_video_height);
                    int imageXpos = designPageLayout.getWidth() / 2 - imageWidth / 2;
                    int imageYpos = designPageLayout.getHeight() / 2 - imageHeight / 2;
                    String image = "YouTube";
                    String objFormat = ".mp4";
                    addNewImageRL(imageWidth, imageHeight, imageXpos, imageYpos, image, objFormat, path);

                } else if (imageType.equals("Audio")) {
                    hidePageListView();
                    int imageWidth = (int) getResources().getDimension(R.dimen.bookview_initial_audio_size);
                    int imageHeight = (int) getResources().getDimension(R.dimen.bookview_initial_audio_size);
                    int imageXpos = designPageLayout.getWidth() / 2 - imageWidth / 2;
                    int imageYpos = designPageLayout.getHeight() / 2 - imageHeight / 2;
                    String Type = "Audio";
                    String objFormat = ".m4a";
                    addNewImageRL(imageWidth, imageHeight, imageXpos, imageYpos, Type, objFormat, path);

                }
                //}
            }
        }
        if (mHelper!=null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public void showBackgroundDialog(final String backgroundSrcImage, final String objType) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.page_background);
        builder.setPositiveButton(R.string.apply, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                page.undoRedoForBackgroundImageContent("apply", Globals.OBJECT_PREVIOUS_STATE);

                ApplyPageBackground(backgroundSrcImage, "ScaleToFit", objType);

                page.undoRedoForBackgroundImageContent("apply", Globals.OBJECT_PRESENT_STATE);
                dialog.cancel();
            }
        });
        if (Integer.parseInt(currentPageNumber) == 1 || Integer.parseInt(currentPageNumber) == pageCount || currentBook.is_bStoreBook() || objType.equals(Globals.OBJTYPE_MINDMAPBG)) {

        } else {
            builder.setNeutralButton(R.string.apply_all, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    page.undoRedoForBackgroundImageContent("applyAll", Globals.OBJECT_PREVIOUS_STATE);

                    ApplyAllPageBackground(backgroundSrcImage, "ScaleToFit");

                    page.undoRedoForBackgroundImageContent("applyAll", Globals.OBJECT_PRESENT_STATE);
                    dialog.cancel();
                }
            });
        }
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    /**
     * this shows the dialog to apply background and apply all
     */
	/*private void showBackgroundDialog(){
		final Dialog backgroundDialog = new Dialog(this);
		backgroundDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		backgroundDialog.setContentView(R.layout.dialog_page);
		btnApply = (Button) backgroundDialog.findViewById(R.id.applyBtn);
		btnApply.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				page.undoRedoForBackgroundImageContent("apply", Globals.OBJECT_PREVIOUS_STATE);
				
				ApplyPageBackground(backgroundSrcImage, "ScaleToFit");
				
				page.undoRedoForBackgroundImageContent("apply", Globals.OBJECT_PRESENT_STATE);
				
				backgroundDialog.dismiss();
			}
		});
		btnApplyAll = (Button) backgroundDialog.findViewById(R.id.applyallBtn);
		btnApplyAll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				page.undoRedoForBackgroundImageContent("applyAll", Globals.OBJECT_PREVIOUS_STATE);
				
				ApplyAllPageBackground(backgroundSrcImage, "ScaleToFit");
				
				page.undoRedoForBackgroundImageContent("applyAll", Globals.OBJECT_PRESENT_STATE);
				
				backgroundDialog.dismiss();
			}
		});
		btnCancel = (Button) backgroundDialog.findViewById(R.id.cancelBtn);
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				backgroundDialog.dismiss();
			}
		});
		backgroundDialog.setCanceledOnTouchOutside(false);
		backgroundDialog.show();
	}*/

    /**
     * Apply page background to single page
     *
     * @param srcImage
     */
    public void ApplyPageBackground(String srcImage, String size, String objType) {

        String destPath = page.pageBG_PATH;
        String pageno = page.currentPageStr;
        //Delete pageColor background
        DeleteCurrentPageBackground();

        /*if(objType.equals(Globals.OBJTYPE_MINDMAPBG)){
            if(navigateEnrichId==0) {
                destPath = "data/data/com.semanoor.SboookAuthor/files/books/11/FreeFiles/" + Globals.OBJTYPE_MINDMAPBG + "_" + pageno + ".png";
            }else{
                destPath = "data/data/com.semanoor.SboookAuthor/files/books/18/FreeFiles/"+Globals.OBJTYPE_MINDMAPBG +"_"+pageno +"-"+navigateEnrichId+".png";
            }
           page.pageBG_PATH=destPath;
        }*/
        if (currentBook.is_bStoreBook() && page.storePageWebRl != null) {
            page.storePageWebRl.getParentTableBgForStoreWebRl();
        }
        ChangePageBackground(srcImage, destPath, pageno, size, objType);
    }

    /**
     * Apply all page background for the image taken from the gallery
     *
     * @param srcImage
     */
    public void ApplyAllPageBackground(String srcImage, String size) {

        //Remove all pageBG and pageColor in DB and from freeFiles
        DeleteApplyApplyAllPageBackgrounds();
        String destPath = page.pageBG_ALL_PATH;
        String pageno = String.valueOf(0);
        ChangePageBackground(srcImage, destPath, pageno, size, Globals.OBJTYPE_PAGEBG);
    }

    /**
     * Method to delete current page background in "Remove" background as well as PageColor
     */
    public void DeleteCurrentPageBackground() {

        String deleteFilePath = page.pageBG_PATH;
        File deleteFile = new File(deleteFilePath);
        if (deleteFile.exists()) {
            deleteFile.delete();
            String PageBGdbQuery = "delete from objects where objType = '" + page.pageBG_objType + "' and BID = '" + currentBook.getBookID() + "'and pageNO = '" + page.currentPageStr + "' and EnrichedID = '" + page.enrichedPageId + "'";
            db.executeQuery(PageBGdbQuery);
        }
        //Deleting PageColor entries from the DB
        String PageBGdbQuery = "delete from objects where objType = '" + page.pageColor_objType + "' and BID = '" + currentBook.getBookID() + "'and pageNO = '" + page.currentPageStr + "' and EnrichedID = '" + page.enrichedPageId + "'";
        db.executeQuery(PageBGdbQuery);
    }

    /**
     * Method to delete All page background in "Remove All" (both Image background and color for All will be deleted)
     */
    public void DeleteOnlyAllPageBackgrounds() {

        String deleteFilePath = page.pageBG_ALL_PATH;
        File deleteFile = new File(deleteFilePath);
        if (deleteFile.exists()) {
            deleteFile.delete();
            String PageBGdbQuery = "delete from objects where (objType = '" + Globals.OBJTYPE_PAGEBG + "' or objType = '" + Globals.OBJTYPE_MINDMAPBG + "') and BID = '" + currentBook.getBookID() + "'and pageNO = 0";
            db.executeQuery(PageBGdbQuery);
        }
        //Deleting PageColor entries from the DB
        String PageBGdbQuery = "delete from objects where objType = 'PageColor' and BID = '" + currentBook.getBookID() + "'and pageNO = 0";
        db.executeQuery(PageBGdbQuery);
    }

    /**
     * Method to delete all page background images and all entries in the DB for PageBG and PageColor
     */
    public void DeleteApplyApplyAllPageBackgrounds() {

        String deletefilePath;
        for (int i = 2; i < pageCount; i++) {  //It starts from 2 as apply all works for pages in between Start and End pages.
            deletefilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/FreeFiles/" + "pageBG_" + i + ".png";
            File deleteFile = new File(deletefilePath);
            if (deleteFile.exists()) {
                deleteFile.delete();
            }
        }
        //Deleting PageBG entries from the DB
        String PageBGdbQuery = "delete from objects where (objType = '" + Globals.OBJTYPE_MINDMAPBG + "' or objType = '" + Globals.OBJTYPE_MINDMAPBG + "') and BID = '" + currentBook.getBookID() + "' and pageNO NOT LIKE 1 and pageNO NOT LIKE '" + pageCount + "' "; //delete from objects where objType = "PageBG" and BID = '2'
        db.executeQuery(PageBGdbQuery);
        //Deleting PageColor entries from the DB
        String PageColordbQuery = "delete from objects where objType = 'PageColor' and BID = '" + currentBook.getBookID() + "' and pageNO NOT LIKE 1 and pageNO NOT LIKE '" + pageCount + "' ";
        db.executeQuery(PageColordbQuery);
    }

    /**
     * Method to Change the page background by copying the selected images from gallery to freeFiles, entry in the DB and setting the page Background image.
     *
     * @param srcImage
     * @param destPath
     * @param pageNo
     */
    private void ChangePageBackground(String srcImage, String destPath, String pageNo, String _size, String objType) {

        //Copy page background
        UserFunctions.copyFiles(srcImage, destPath);
        //Entry in the DB
        //String objType = page.pageBG_objType;
        page.pageBG_objType = objType;
        int objBID = currentBook.getBookID();
        String location = "0|0";
        String size = _size;
        String content;
        if (objType.equals(Globals.OBJTYPE_MINDMAPBG)) {
            String mindMapFilename = new File(srcImage).getName();
            String[] splitMindmapFileName = mindMapFilename.split("\\.");
            content = splitMindmapFileName[0];
            page.pageBgMindMapContent = content;
        } else {
            content = destPath;
        }
        //int objSequentialId = page.objectListArray.size() + 1;
        int objSequentialId = 0;
        String scalePageToFit = "no";
        db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, EnrichedID) values('" + "" + "', '" + objType + "', '" + objBID + "', '" + pageNo + "', '" + location + "', '" + size + "', '" + content + "', '" + objSequentialId + "', '" + scalePageToFit + "', '" + page.enrichedPageId + "')");

        //set as page background.
        page.setBackgroundImage(pageNo, false);
    }

    /**
     * Dialog to shows rotate clock or anticlockwise
     */
    public void showRotateDialog() {

        final Dialog rotateDialog = new Dialog(this);
        rotateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        rotateDialog.setContentView(R.layout.dialog_rotate);
        rotateDialog.setCanceledOnTouchOutside(false);
        rotateDialog.show();
        //Checks about the background image.
        final String finalPagePath;
        final String pageNumber;
        String pagePath = page.pageBG_PATH;
        String pageAllPath = page.pageBG_ALL_PATH;
        File pagefile = new File(pagePath);
        File pageAllfile = new File(pageAllPath);
        if (pagefile.exists()) {
            finalPagePath = pagePath;
            pageNumber = page.currentPageStr;
        } else {
            if (pageAllfile.exists()) {
                finalPagePath = pageAllPath;
                pageNumber = String.valueOf(0);
            } else {
                finalPagePath = pagePath;
                pageNumber = page.currentPageStr;
            }
        }
        ImageButton hideBtn = (ImageButton) rotateDialog.findViewById(R.id.hideBtn);
        hideBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                rotateDialog.dismiss();
            }
        });
        ImageButton rotateLeft = (ImageButton) rotateDialog.findViewById(R.id.leftImgBtn);
        rotateLeft.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
                uRPrevObjData.setuRPageNoObjDataStatus("Rotated");
                uRPrevObjData.setuRObjectDataRotatedAngle(-90);
                uRPrevObjData.setuRObjDataContent(finalPagePath);
                page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PREVIOUS_STATE);

                //update DB.
                page.rotateBackground(finalPagePath, pageNumber, 90, true);

                UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                uRCurrObjData.setuRPageNoObjDataStatus("Rotated");
                uRCurrObjData.setuRObjectDataRotatedAngle(90);
                uRCurrObjData.setuRObjDataContent(finalPagePath);
                page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);

                rotateDialog.dismiss();
            }
        });
        ImageButton rotateRight = (ImageButton) rotateDialog.findViewById(R.id.rightImgBtn);
        rotateRight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
                uRPrevObjData.setuRPageNoObjDataStatus("Rotated");
                uRPrevObjData.setuRObjectDataRotatedAngle(90);
                uRPrevObjData.setuRObjDataContent(finalPagePath);
                page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PREVIOUS_STATE);

                //update DB.
                page.rotateBackground(finalPagePath, pageNumber, -90, true);

                UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                uRCurrObjData.setuRPageNoObjDataStatus("Rotated");
                uRCurrObjData.setuRObjectDataRotatedAngle(-90);
                uRCurrObjData.setuRObjDataContent(finalPagePath);
                page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);

                rotateDialog.dismiss();
            }
        });
    }

    public void showColorDialog(final int backgroundColor) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.page_background);
        builder.setPositiveButton(R.string.apply, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                page.undoRedoForBackgroundImageContent("apply", Globals.OBJECT_PREVIOUS_STATE);

                //Update in the db
                DeleteCurrentPageBackground();
                pageBackgroundColorEntryinDB(backgroundColor, page.currentPageStr);

                UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                String currBgImgPath = page.pageBackgroundColor;
                uRCurrObjData.setuRObjPickedColorValue(true);
                uRCurrObjData.setuRPageNoObjDataStatus("apply");
                uRCurrObjData.setuRObjDataContent(currBgImgPath);
                page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);
                dialog.cancel();
            }
        });
        if (Integer.parseInt(currentPageNumber) == 1 || Integer.parseInt(currentPageNumber) == pageCount || currentBook.is_bStoreBook()) {

        } else {
            builder.setNeutralButton(R.string.apply_all, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    page.undoRedoForBackgroundImageContent("applyAll", Globals.OBJECT_PREVIOUS_STATE);

                    //Update in the db
                    DeleteApplyApplyAllPageBackgrounds();
                    pageBackgroundColorEntryinDB(backgroundColor, "0");

                    UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                    String currBgImgPath = page.pageBackgroundColor;
                    uRCurrObjData.setuRObjPickedColorValue(true);
                    uRCurrObjData.setuRPageNoObjDataStatus("applyAll");
                    uRCurrObjData.setuRObjDataContent(currBgImgPath);
                    page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);
                    dialog.cancel();
                }
            });
        }
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Revert back and apply background
                page.setBackgroundImage(page.currentPageStr, true);
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    /**
     * dialog for color background
     */

	/*public void showColorDialog(final int backgroundColor){

		final Dialog  colorDialog = new Dialog(this);
		colorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		colorDialog.setContentView(R.layout.dialog_page);
		colorDialog.setCanceledOnTouchOutside(false);
		colorDialog.show();
		Button btnApply = (Button) colorDialog.findViewById(R.id.applyBtn);
		btnApply.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				page.undoRedoForBackgroundImageContent("apply", Globals.OBJECT_PREVIOUS_STATE);
				
				//Update in the db
				DeleteCurrentPageBackground();
				pageBackgroundColorEntryinDB(backgroundColor, page.currentPageStr);
				
				UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
				String currBgImgPath = page.pageBackgroundColor;
				uRCurrObjData.setuRObjPickedColorValue(true);
				uRCurrObjData.setuRPageNoObjDataStatus("apply");
				uRCurrObjData.setuRObjDataContent(currBgImgPath);
				page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);
				
				colorDialog.dismiss();

				
			}
		});
		Button btnApplyAll = (Button) colorDialog.findViewById(R.id.applyallBtn);
		btnApplyAll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				page.undoRedoForBackgroundImageContent("applyAll", Globals.OBJECT_PREVIOUS_STATE);

				//Update in the db
				DeleteApplyApplyAllPageBackgrounds();
				pageBackgroundColorEntryinDB(backgroundColor, "0");

				UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
				String currBgImgPath = page.pageBackgroundColor;
				uRCurrObjData.setuRObjPickedColorValue(true);
				uRCurrObjData.setuRPageNoObjDataStatus("applyAll");
				uRCurrObjData.setuRObjDataContent(currBgImgPath);
				page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);

				colorDialog.dismiss();
			}
		});
		Button btnCancel = (Button) colorDialog.findViewById(R.id.cancelBtn);
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Revert back and apply background
				page.setBackgroundImage(page.currentPageStr);
				colorDialog.dismiss();
			}
		});
		if(Integer.parseInt(currentPageNumber) == 1 || Integer.parseInt(currentPageNumber) == pageCount){
			btnApplyAll.setVisibility(View.GONE);
		}
	}*/
    public void pageBackgroundColorEntryinDB(int pageColor, String pageNo) {
        //Entry in the DB
        String objType = page.pageColor_objType;
        page.pageBackgroundColor = String.valueOf(pageColor);
        int objBID = currentBook.getBookID();
        String location = "0|0";
        String size = "ScaleToFit";
        String content = String.valueOf(pageColor);
        int objSequentialId = page.objectListArray.size() + 1;
        String scalePageToFit = "no";
        db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, EnrichedID) values('" + "" + "', '" + objType + "', '" + objBID + "', '" + pageNo + "', '" + location + "', '" + size + "', '" + content + "', '" + objSequentialId + "', '" + scalePageToFit + "', '" + page.enrichedPageId + "')");
    }

    public void removeBackgroundDialog() {
        boolean isRemoveAll = false;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.page_background);
        if (currentBook.is_bStoreBook() || CheckForApplyRemoveBackground()) {
            isRemoveAll = false;
        } else {
            if (CheckForApplyallRemoveAll()) {
                isRemoveAll = true;
            }
        }
        if (!isRemoveAll) {
            builder.setPositiveButton(R.string.remove, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    page.undoRedoForBackgroundImageContent("apply", Globals.OBJECT_PREVIOUS_STATE);

                    //Delete in the db
                    DeleteCurrentPageBackground();
                    page.setBackgroundImage(page.currentPageStr, false);

                    if (currentBook.is_bStoreBook() && page.storePageWebRl != null) {
                        page.storePageWebRl.resetParentTableBgForStoreWebRl();
                    }

                    mindMapSliderlayout.setVisibility(View.GONE);

                    UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                    uRCurrObjData.setuRPageNoObjDataStatus("remove");
                    page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);
                    dialog.cancel();
                }
            });
        } else {
            builder.setNeutralButton(R.string.remove_all, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    page.undoRedoForBackgroundImageContent("applyAll", Globals.OBJECT_PREVIOUS_STATE);

                    //Delete in the db
                    DeleteOnlyAllPageBackgrounds();
                    page.setBackgroundImage(page.currentPageStr, false);

                    mindMapSliderlayout.setVisibility(View.GONE);

                    UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                    uRCurrObjData.setuRPageNoObjDataStatus("removeAll");
                    page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);
                    dialog.cancel();
                }
            });
        }
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

	/**
	 *  dialog to remove color or page background
	 */
	/*public void removeBackgroundDialog(){

		final Dialog  removeDialog = new Dialog(this);
		removeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		removeDialog.setContentView(R.layout.dialog_remove);
		removeDialog.setCanceledOnTouchOutside(false);
		removeDialog.show();
		Button btnRemove = (Button) removeDialog.findViewById(R.id.removeBtn);
		btnRemove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				page.undoRedoForBackgroundImageContent("apply", Globals.OBJECT_PREVIOUS_STATE);
				
				//Delete in the db
				DeleteCurrentPageBackground();
				page.setBackgroundImage(page.currentPageStr);
				
				UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
				uRCurrObjData.setuRPageNoObjDataStatus("remove");
				page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);
				removeDialog.dismiss();
			}
		});
		Button btnRemoveAll = (Button) removeDialog.findViewById(R.id.removeAllBtn);
		btnRemoveAll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				page.undoRedoForBackgroundImageContent("applyAll", Globals.OBJECT_PREVIOUS_STATE);
				
				//Delete in the db
				DeleteOnlyAllPageBackgrounds();
				page.setBackgroundImage(page.currentPageStr);
				removeDialog.dismiss();
				
				UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
				uRCurrObjData.setuRPageNoObjDataStatus("removeAll");
				page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);
				removeDialog.dismiss();
			}
		});
		Button btnCancel = (Button) removeDialog.findViewById(R.id.cancelBtn);
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				removeDialog.dismiss();
			}
		});
		if(CheckForApplyRemoveBackground()){
			btnRemoveAll.setVisibility(View.GONE);
		}
		else{
			if(CheckForApplyallRemoveAll()){
				btnRemove.setVisibility(View.GONE);

			}
		}
	}*/
    public Boolean CheckForApplyallRemoveAll() {

        String bgALLImagePath = page.pageBG_ALL_PATH;
        String pageAllColorValue = db.getPageBackgroundColor(currentBook.getBookID(), "PageColor", "0", page.enrichedPageId);
        File file = new File(bgALLImagePath);
        if (file.exists()) {
            return true;
        } else if (!pageAllColorValue.equals("NoEntry")) {

            return true;
        } else {
            return false;
        }
    }

    public Boolean CheckForApplyRemoveBackground() {

        String bgImagePath = page.pageBG_PATH;
        String objType = page.pageColor_objType;
        String pageColorValue = db.getPageBackgroundColor(currentBook.getBookID(), objType, page.currentPageStr, page.enrichedPageId);
        File file = new File(bgImagePath);
        if (file.exists()) {
            return true;
        } else if (!pageColorValue.equals("NoEntry")) {

            return true;
        } else {
            return false;
        }
    }



    /**
     * Method to catch the window focus for buttons position
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        int[] location = new int[2];
        btnAddBackground.getLocationOnScreen(location);
        changeBackgroundPoint = new Point();
        changeBackgroundPoint.x = location[0];
        changeBackgroundPoint.y = location[1];

        int[] btnClearlocation = new int[2];
        btnClear.getLocationOnScreen(btnClearlocation);
        clearallobjects = new Point();
        clearallobjects.x = btnClearlocation[0];
        clearallobjects.y = btnClearlocation[1];

        int[] btnDrawLocation = new int[2];
        btnDraw.getLocationOnScreen(btnDrawLocation);
        drawPnt = new Point();
        drawPnt.x = btnDrawLocation[0];
        drawPnt.y = btnDrawLocation[1];

        int[] btnWidgetLocation = new int[2];
        btnAddWidgets.getLocationOnScreen(btnWidgetLocation);
        widgetPnt = new Point();
        widgetPnt.x = btnWidgetLocation[0];
        widgetPnt.y = btnWidgetLocation[1];

        super.onWindowFocusChanged(hasFocus);
    }

	

    public void loadMindMap(String title, RelativeLayout layout, Context context) {
        final RelativeLayout canvasView = new RelativeLayout(context);
        canvasView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        final TwoDScrollView twoDScroll = new TwoDScrollView(context,canvasView);
        twoDScroll.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        twoDScroll.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        twoDScroll.setEnabled(false);
        canvasView.addView(twoDScroll);
        ViewGroup drawingView = new RelativeLayout(context);
        drawingView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        twoDScroll.addView(drawingView);
        //setSelectedView(viewEdit);
        drawingView.getLayoutParams().width = AppDict.DEFAULT_OFFSET;
        drawingView.getLayoutParams().height = AppDict.DEFAULT_OFFSET;
        String url = Globals.TARGET_BASE_MINDMAP_PATH + title + ".xml";
        LoadMindMapContent mindMapContent = new LoadMindMapContent(twoDScroll, drawingView, context);
        mindMapContent.loadXmlData(url, false);
        Rect rectf = new Rect();
        drawingView.getLocalVisibleRect(rectf);
        twoDScroll.calculateBounds();
        twoDScroll.post(new Runnable() {
            @Override
            public void run() {
                twoDScroll.scrollTo((int) (AppDict.DEFAULT_OFFSET - canvasView.getWidth() + 120) / 2,
                        (int) (AppDict.DEFAULT_OFFSET - canvasView.getHeight()) / 2);
            }
        });
        layout.addView(canvasView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
    public void hideToolBarLayout(){
       mDrawer.close();
    }

    public void advanceSearchPopup(){
        final Dialog advSearchPopUp = new Dialog(BookViewActivity.this);
        advSearchPopUp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
        advSearchPopUp.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        advSearchPopUp.setContentView(BookViewActivity.this.getLayoutInflater().inflate(R.layout.advsearch, null));
        ListView advListView = (ListView) advSearchPopUp.findViewById(R.id.listViewAdv);
        final EditText etAdv = (EditText) advSearchPopUp.findViewById(R.id.editTextAdv);
        etAdv.setText("");
        advSearchPopUp.getWindow().setLayout((int) (global.getDeviceWidth() / 1.8), (int) (global.getDeviceHeight() / 1.8));
        advListView.setDividerHeight(1);
        advListView.setAdapter(new advAdapter(BookViewActivity.this, Globals.advSearch));
        advSearchPopUp.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                // TODO Auto-generated method stub
                //SearchClicked=false;
                if (enrTabsLayout.getWidth() > designPageLayout.getWidth()) {
                    int enrTabsWidth = enrTabsLayout.getWidth();
                    enrScrollView.smoothScrollTo(enrTabsWidth, 0);

                }
            }
        });
        advListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, final int position,
                                    long arg3) {
                // TODO Auto-generated method stub
                final String url;
                String advSearchtext;
                switch (position) {

//                    case 0:
//                        url = "http://projects.nooor.com/SemaBooksSearch/BkSearch.aspx?Srch=" + etAdv.getText().toString() + "";
//                        advSearchtext = etAdv.getText().toString();
//                        BookViewActivity.this.runOnUiThread(new Runnable() {
//
//                            @Override
//                            public void run() {
//                                new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
//                                advSearchPopUp.dismiss();
//                            }
//                        });
//                        advSearchPopUp.dismiss();
//                        break;
//
//                    case 1:
//                        url = "http://www.nooor.com/search/" + etAdv.getText().toString() + "/";
//                        advSearchtext = etAdv.getText().toString();
//                        //clickedAdvancedSearch(advSearch[position], url);
//                        BookViewActivity.this.runOnUiThread(new Runnable() {
//
//                            @Override
//                            public void run() {
//                                new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
//                                advSearchPopUp.dismiss();
//                            }
//                        });
//                        break;

                    case 0:
                        url = "http://www.google.com/search?q=" + etAdv.getText().toString() + "";
                        advSearchtext = etAdv.getText().toString();
                        BookViewActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
                                advSearchPopUp.dismiss();
                            }
                        });
                        advSearchPopUp.dismiss();
                        break;

                    case 1:
                        String text = etAdv.getText().toString();
                        String language = detectLanguage(text);

                        String percentEscpe = Uri.encode(text);
                        if (language.equals("ar")) {
                            url = "http://ar.wikipedia.org/wiki/" + percentEscpe + "";
                        } else {
                            url = "http://en.wikipedia.org/wiki/" + percentEscpe + "";
                        }
                        advSearchtext = etAdv.getText().toString();
                        BookViewActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
                                advSearchPopUp.dismiss();
                            }
                        });
                        advSearchPopUp.dismiss();
                        break;

                    case 2:
                        url = "http://m.youtube.com/#/results?q=" + etAdv.getText().toString() + "";
                        advSearchtext = etAdv.getText().toString();
                        BookViewActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
                                advSearchPopUp.dismiss();
                            }
                        });
                        advSearchPopUp.dismiss();
                        break;

                    case 3:
                        url = "http://search.yahoo.com/search?p=" + etAdv.getText().toString() + "";
                        advSearchtext = etAdv.getText().toString();
                        BookViewActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
                                advSearchPopUp.dismiss();
                            }
                        });
                        advSearchPopUp.dismiss();
                        break;

                    case 4:
                        url = "http://www.bing.com/search?q=" + etAdv.getText().toString() + "";
                        advSearchtext = etAdv.getText().toString();
                        BookViewActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
                                advSearchPopUp.dismiss();
                            }
                        });
                        advSearchPopUp.dismiss();
                        break;

                    case 5:
                        url = "http://www.ask.com/web?q=" + etAdv.getText().toString() + "";
                        advSearchtext = etAdv.getText().toString();
                        BookViewActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
                                advSearchPopUp.dismiss();
                            }
                        });
                        advSearchPopUp.dismiss();
                        break;

                    default:
                        break;
                }
            }

        });
        advSearchPopUp.show();
    }

    public  void setlayoutvisibilityInvisible(View v,boolean status){
        for (int i = 0, j = enrTabsLayout.getChildCount(); i < j; i++) {
            Button btnEnrTab;
            RelativeLayout rlview = null;
            if (i == 0) {
                btnEnrTab = (Button) enrTabsLayout.getChildAt(i);
            } else {
                rlview = (RelativeLayout) enrTabsLayout.getChildAt(i);
                btnEnrTab = (Button) rlview.getChildAt(0);
            }
            if (btnEnrTab ==v) {
                if(status){
                    rlview.setVisibility(View.VISIBLE);
                    btnEnrTab.setAlpha(1F);
                }else {
                    rlview.setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    public View getselectedItemPositionInlayout(LinearLayout enrTabsLayout, View vieww){
        for (int i = 0, j = enrTabsLayout.getChildCount(); i < j; i++) {
            Button btnEnrTab;
            RelativeLayout rlview = null;
            if (i == 0) {
                btnEnrTab = (Button) enrTabsLayout.getChildAt(i);
            } else {
                rlview = (RelativeLayout) enrTabsLayout.getChildAt(i);
                btnEnrTab = (Button) rlview.getChildAt(0);
            }
            if (btnEnrTab ==vieww) {
                return rlview;
            }
        }
        return null;
    }


    private void enrichmentButtonOnClickOperation(Enrichments enrich){
        if (enrich.getEnrichmentType().equals("Search")) {
            if (mainDesignView.getChildCount() == 4) {
                mainDesignView.removeViewAt(3);
            }
            wv_advSearch.clearView();
            enrTabsLayout.setVisibility(View.VISIBLE);
            enrTabsLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            wv_advSearch.setVisibility(View.VISIBLE);
            designScrollPageView.setVisibility(View.INVISIBLE);
            wv_advSearch.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            wv_advSearch.getSettings().setJavaScriptEnabled(true);
            wv_advSearch.setWebChromeClient(new WebChromeClient());
            wv_advSearch.getSettings().setAllowContentAccess(true);
            wv_advSearch.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            wv_advSearch.getSettings().setAllowFileAccess(true);
            wv_advSearch.getSettings().setPluginState(PluginState.ON);
            wv_advSearch.getSettings().setDomStorageEnabled(true);
            wv_advSearch.setWebViewClient(new WebViewClient());
            advtitle = enrich.getEnrichmentTitle();
            if (enrich.getEnrichmentType().equals("Search")) {
                if (enrich.getEnrichmentTitle().equals("BookPage")){
                    String[] split = enrich.getEnrichmentPath().split("/");
                    String filePath = "file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/";
                    File hyperPage = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/"+split[split.length-1]);
                    String srcString = UserFunctions.decryptFile(hyperPage);
                    wv_advSearch.loadDataWithBaseURL(filePath, srcString, "text/html", "utf-8", null);
                }else{
                    wv_advSearch.loadUrl(enrich.getEnrichmentPath());
                }
            }
            wv_advSearch.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            if (enrich.getEnrichmentType().equals("Search")) {
                wv_advSearch.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:{
                                break;
                            }
                            case MotionEvent.ACTION_UP:{
                                wv_advSearch.setWebViewClient(new CustomWebViewClient());
                                if (Integer.parseInt(currentPageNumber) != 1 && Integer.parseInt(currentPageNumber) != currentBook.getTotalPages()) {
                                    if (enr_layout.getVisibility() == View.VISIBLE) {
                                        enr_layout.setVisibility(View.GONE);
                                    } else {
                                        if (isCreateBlankEnr == null) {
                                            enr_layout.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
                                break;
                            }
                            default:
                                break;
                        }
                        return false;
                    }
                });
            }
            bgImgView.setVisibility(View.INVISIBLE);

        } else if (enrich.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
            wv_advSearch.setVisibility(View.INVISIBLE);
            if (mainDesignView.getChildCount() == 4) {
                mainDesignView.removeViewAt(3);
            }
            loadMindMap(enrich.getEnrichmentPath(),mainDesignView,BookViewActivity.this);
            if (progressBar.getVisibility()==View.VISIBLE){
                progressBar.setVisibility(View.INVISIBLE);
            }
        } else {
            if (mainDesignView.getChildCount() == 4) {
                mainDesignView.removeViewAt(3);
            }
             if(wv_advSearch.getVisibility()==View.VISIBLE){
                 wv_advSearch.setVisibility(View.INVISIBLE);
             }
          //  wv_advSearch.setVisibility(View.INVISIBLE);
            designScrollPageView.setVisibility(View.VISIBLE);
            bgImgView.setVisibility(View.VISIBLE);
        }

        if (page.enrichedPageId != enrich.getEnrichmentId()) {
           // makeEnrichedBtnSlected(v);
            instantiateNewPage(Integer.parseInt(currentPageNumber), enrich.getEnrichmentId());
            if (enrich.getEnrichmentType().equals("Search") || enrich.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
                disableToolbarButtons();
            } else {
                enableToolbarButtons();
            }
        }
//                mDrapView = v;                                      //Rearranging tabs
//                ClipData data = ClipData.newPlainText("", "");
//                MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(
//                        v);
//                v.startDrag(data, shadowBuilder, v, 0);
        if (keyboardToolbar.getVisibility()==View.VISIBLE){
            keyboardToolbar.setVisibility(View.GONE);
        }
         //recyclerView.getAdapter().notifyDataSetChanged();
    }


    private void deletingSearchEnrichments(Enrichments enrich){
       //Enrichments id = (Enrichments) btnEnrichmentTab.getTag();

       /* for (int j = 0; j < enrTabsLayout.getChildCount(); j++) {
            if (j == 0) {
                Button btnEnr = (Button)  enrTabsLayout.getChildAt(j);
            } else {
                RelativeLayout rlview = (RelativeLayout) enrTabsLayout.getChildAt(j);
                Button btnEnrTab = (Button) rlview.getChildAt(0);
                final Enrichments enrich = (Enrichments) btnEnrTab.getTag();

                if (enrich.getEnrichmentId() == id.getEnrichmentId()) {
                    enrTabsLayout.removeView(rlview);
                }
            }
        }  */

        // ((BookViewActivity)context).enrTabsLayout.removeView(id.getEnrichmentSequenceId());
        String deleteEnrQuery = "delete from enrichments where enrichmentID = '" + enrich.getEnrichmentId() + "'";
        db.executeQuery(deleteEnrQuery);
        int enrichid = enrich.getEnrichmentId();
//        for (Enrichments enrichments : enrichmentTabListArray) {
//            if (enrichments.getEnrichmentId() == id.getEnrichmentId()) {
                enrichmentTabListArray.remove(enrich);
//                break;
//            }
//        }
        int tagValue = 0;
        if (enrichmentTabListArray.size() == 1) {
            wv_advSearch.setVisibility(View.INVISIBLE);
            bgImgView.setVisibility(View.VISIBLE);
            designScrollPageView.setVisibility(View.VISIBLE);
            tagValue = 0;
        } else {
            for (int j = 0; j < enrichmentTabListArray.size(); j++) {
//                if (j == 0) {
//                    Button btnEnr = (Button) enrTabsLayout.getChildAt(j);
//                } else {
                    if (j == enrichmentTabListArray.size() - 1) {
                      //  RelativeLayout rlview = (RelativeLayout) enrTabsLayout.getChildAt(enrTabsLayout.getChildCount() - 1);
                       // Button btnEnrTab = (Button) rlview.getChildAt(0);
                        Enrichments enrichment =  enrichmentTabListArray.get(j);
                        //makeEnrichedBtnSlected(btnEnrTab);
                        tagValue = enrichment.enrichmentId;
                        enrichment.setEnrichmentSelected(true);
                        if (enrich.getEnrichmentType().equals("Search")) {
                            wv_advSearch.setVisibility(View.VISIBLE);
                            wv_advSearch.setWebViewClient(new WebViewClient());
                            if (enrichment.getEnrichmentTitle().equals("BookPage")){
                                String[] split = enrichment.getEnrichmentPath().split("/");
                                String filePath = "file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/";
                                File hyperPage = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/"+split[split.length-1]);
                                String srcString = UserFunctions.decryptFile(hyperPage);
                                wv_advSearch.loadDataWithBaseURL(filePath, srcString, "text/html", "utf-8", null);
                            }else{
                                wv_advSearch.loadUrl(enrich.getEnrichmentPath());
                            }
                            bgImgView.setVisibility(View.INVISIBLE);
                            designScrollPageView.setVisibility(View.INVISIBLE);

                        } else if (enrich.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
                             designScrollPageView.setVisibility(View.INVISIBLE);
                             wv_advSearch.setVisibility(View.INVISIBLE);
                             bgImgView.setVisibility(View.INVISIBLE);

                        } else {
                            designScrollPageView.setVisibility(View.VISIBLE);
                            wv_advSearch.setVisibility(View.INVISIBLE);
                             bgImgView.setVisibility(View.VISIBLE);
                        }
                   }
               // }
            }
        }
        checkForEnrichments(Integer.parseInt(currentPageNumber), tagValue);
        instantiateNewPage(Integer.parseInt(currentPageNumber), tagValue);
        int enrTabCount = enrichTabCountListAllPages.get(Integer.parseInt(currentPageNumber) - 1);
        enrTabCount = enrTabCount - 1;
        if (currentBook.is_bStoreBook()) {
            enrich.deleteEnrichmentEntryInBookXml(enrichid);
        }
        enrichTabCountListAllPages.set(Integer.parseInt(currentPageNumber) - 1, enrTabCount);
        pageListView.invalidateViews();
        recyclerView.getAdapter().notifyDataSetChanged();

    }

    public class loadSubEnrichmentTabs extends AsyncTask<Void, Void, Void>{

        ArrayList<Enrichments> enrichmentTabList;
        int pageNumber;
        RecyclerView recyclerview;
        int Id;
        int categoryId;
        RecyclerView clickableRecycler;

        public loadSubEnrichmentTabs(int pageNumber, RecyclerView recyclerView, int SelectecEnrId,int catId,RecyclerView newRecycler){
            this.pageNumber = pageNumber;
            this.recyclerview=recyclerView;
            this.Id=SelectecEnrId;
            this.categoryId = catId;
            this.clickableRecycler = newRecycler;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            enrichmentTabList = db.getEnrichmentTabListForCatId(currentBook.getBookID(), pageNumber, BookViewActivity.this,categoryId);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (enrichmentTabList.size()>0) {
                subTabsLayout.setVisibility(View.VISIBLE);
                if (recyclerview.getId() == R.id.gridView3) {
                    subTabLayout1.setVisibility(View.VISIBLE);
                }else{
                    subTabLayout1.setVisibility(View.GONE);
                }
                loadView(enrichmentTabList,recyclerview);

//                ItemTouchHelper.Callback callback = new MovieTouchHelper(adapter);
//                ItemTouchHelper helper = new ItemTouchHelper(callback);
//                helper.attachToRecyclerView(recyclerview);
            }else{
                if (clickableRecycler.getId()==R.id.gridView) {
                    subTabsLayout.setVisibility(View.GONE);
                }
                subTabLayout1.setVisibility(View.GONE);
            }
        }
    }

    public void loadView(ArrayList<Enrichments> topEnrList,RecyclerView currentRecycler){
        final SubtabsAdapter  adapter = new SubtabsAdapter(subTabsLayout,subTabLayout1,subRecyclerView,subRecylerView1,db,BookViewActivity.this, Integer.parseInt(currentPageNumber), topEnrList, currentRecycler, new OnclickCallBackInterface<Object>() {
            @Override
            public void onClick(Object object) {
                CallBackObjects objects = (CallBackObjects) object;
                if (objects.doubleTap){
                    gestureView = objects.view;
                    isLoadEnrInpopview = objects.loadEnrPoview;
                  //  treeViewStructureList(gestureView, false,currentBtnEnrichment.getEnrichmentId());
                    viewPopUp(gestureView,currentBtnEnrichment.getEnrichmentId());
                  //  viewTreeList(gestureView, false,currentBtnEnrichment.getEnrichmentId());
                }else {
                    RecyclerView recyclerView = objects.recyclerView;
                    clickableRecyclerView = recyclerView;
                    if (recyclerView != null) {
                        int position = objects.adapterPosition;
                        Enrichments enrichment = objects.horizontalList.get(position);
                        if (clickableRecyclerView.getId() == R.id.gridView2){
                            SubtabsAdapter.level2 = enrichment.getEnrichmentId();
                        }else if (clickableRecyclerView.getId() == R.id.gridView3){
                            SubtabsAdapter.level3 = enrichment.getEnrichmentId();
                        }else{
                            SubtabsAdapter.level1 = enrichment.getEnrichmentId();
                        }
                        for(Enrichments enrich:objects.horizontalList){
                            if(enrich.isEnrichmentSelected()){
                                enrich.setEnrichmentSelected(false);
                                break;
                            }
                        }
                        if (enrichment.getEnrichmentType().equals(Globals.advancedSearchType)) {
                            enrichmentButtonOnClickOperation(enrichment);
                        } else if (enrichment.getEnrichmentType().equals(Globals.onlineEnrichmentsType) || (enrichment.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB))) {
                            enrichmentButtonOnClickOperation(enrichment);
                        } else {
                            enrichmentButtonOnClickOperation(enrichment);
                            if (currentBook.is_bStoreBook() && !currentBook.get_bStoreID().contains("C") && enrichment.getEnrichmentId() == 0 &&currentBook.get_bStoreID().contains("P")) {
                                disableToolbarButtons();
                            } else if (currentBook.is_bStoreBook() && !currentBook.get_bStoreID().contains("C") && enrichment.getEnrichmentId() != 0) {
                                enableToolbarButtons();
                            } else if (!currentBook.is_bStoreBook()&&currentBook.getbCategoryId()!=6) {
                                enableToolbarButtons();
                            } else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("C") && enrichment.getEnrichmentId() == 0 || currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("C") && enrichment.getEnrichmentId() != 0) {
                                enableToolbarButtons();
                            } else {
                                disableToolbarButtons();
                            }
                        }
                        enrichment.setEnrichmentSelected(true);
                        currentBtnEnrichment = enrichment;
                        if (recyclerView.getId()== R.id.gridView) {
                            if (enrichment.getEnrichmentId() != 0) {
                                new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, enrichment.getEnrichmentId(),recyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                            } else {
                                subTabsLayout.setVisibility(View.GONE);
                                subTabLayout1.setVisibility(View.GONE);
                            }
                        }else if (recyclerView.getId()== R.id.gridView2){
                            new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, enrichment.getEnrichmentId(),recyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                        }
                    }
                }
            }
        });
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(BookViewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        currentRecycler.setNestedScrollingEnabled(false);
        currentRecycler.setLayoutManager(horizontalLayoutManagaer);
        currentRecycler.setAdapter(adapter);
    }

    public void viewTreeList(View view, boolean isAddEnrich, final int categoryId){
        categoryID = categoryId;
        treeStructureView = new TreeStructureView(currentBook, db, Integer.parseInt(currentPageNumber), popoverView, BookViewActivity.this, rootView, new TreeStructureCallBack() {
            @Override
            public void onClick(TreeNode node, Object object,PopoverView popoverView) {
                MyTreeHolder.IconTreeItem treeItem = (MyTreeHolder.IconTreeItem) object;
                Enrichments enrichments3 = treeItem.enrich;
                if (enrichments3.getEnrichmentId()!=0){
                    search_layout.setVisibility(View.GONE);
                    currentBtnEnrichment = enrichments3;
                    if (enrichments3.getEnrichmentType().equals(Globals.advancedSearchType)) {
                        enrichmentButtonOnClickOperation(enrichments3);
                    } else if (enrichments3.getEnrichmentType().equals(Globals.onlineEnrichmentsType) || (enrichments3.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB))) {
                        enrichmentButtonOnClickOperation(enrichments3);
                    } else {
                        enrichmentButtonOnClickOperation(enrichments3);
                        if (currentBook.is_bStoreBook() && !currentBook.get_bStoreID().contains("C") && enrichments3.getEnrichmentId() == 0 &&currentBook.get_bStoreID().contains("P")) {
                            disableToolbarButtons();
                        } else if (currentBook.is_bStoreBook() && !currentBook.get_bStoreID().contains("C") && enrichments3.getEnrichmentId() != 0) {
                            enableToolbarButtons();
                        } else if (!currentBook.is_bStoreBook()&&currentBook.getbCategoryId()!=6) {
                            enableToolbarButtons();
                        } else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("C") && enrichments3.getEnrichmentId() == 0 || currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("C") && enrichments3.getEnrichmentId() != 0) {
                            enableToolbarButtons();
                        } else {
                            disableToolbarButtons();
                        }
                    }
                }else if(enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.blank_page))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    int maxEnrichId = db.getMaxUniqueRowID("enrichments") + 1;
                    //pageTemplateListAdapter.addDefaultTemplate(position+1, Integer.parseInt(currentPageNumber), maxEnrichId);
                    addPageNumberObject(Integer.parseInt(currentPageNumber), maxEnrichId);
                    catId = categoryId;
                    createEnrichments("blank", maxEnrichId);
                    //popoverView.dissmissPopover(true);
                    int enrTabCount = enrichTabCountListAllPages.get(Integer.parseInt(currentPageNumber) - 1);
                    enrTabCount = enrTabCount + 1;
                    enrichTabCountListAllPages.set(Integer.parseInt(currentPageNumber) - 1, enrTabCount);
                    pageListView.invalidateViews();
                    if (catId==0){
                        ArrayList<Enrichments> enrichmentTabList = db.getAllEnrichmentTabList(currentBook.getBookID(), Integer.parseInt(currentPageNumber), BookViewActivity.this);
                        ArrayList<Enrichments> topEnrList = new ArrayList<>();
                        for (Enrichments enrichments : enrichmentTabList){
                            if (enrichments.getCategoryID()==0){
                                topEnrList.add(enrichments);
                            }
                        }
                        loadView(topEnrList,recyclerView);
                    }else if (clickableRecyclerView!=null && clickableRecyclerView.getId()== R.id.gridView) {
                        if (catId != 0) {
                            new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, catId,clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                        } else {
                            subTabsLayout.setVisibility(View.GONE);
                            subTabLayout1.setVisibility(View.GONE);
                        }
                    }else if (clickableRecyclerView!=null && clickableRecyclerView.getId()== R.id.gridView2){
                        new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, catId,clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    }
                }else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.internet_search))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    search_layout.setVisibility(View.VISIBLE);
                }else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.rename))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    renamePopup();
                }else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.delete))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    deletePopup();
                }
                if (enrichments3.getEnrichmentTitle().equals("")) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    //  treeViewStructureList(gestureView, true, enrichments1.getCategoryID());
                    viewTreeList(gestureView, true, enrichments3.getCategoryID());
                }
            }
        });
        treeStructureView.treeViewStructureList(isLoadEnrInpopview,currentBtnEnrichment,view,isAddEnrich,categoryId);
    }

    private void renamePopup(){
        final Dialog groupDialog = new Dialog(BookViewActivity.this);
        groupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        groupDialog.setTitle(R.string.title);
        groupDialog.setContentView(getLayoutInflater().inflate(R.layout.group_edit, null));
        groupDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.5), RelativeLayout.LayoutParams.WRAP_CONTENT);
        final EditText editText = (EditText) groupDialog.findViewById(R.id.enr_editText);
        Button btnSave = (Button) groupDialog.findViewById(R.id.enr_save);
        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (editText.getText().toString().length()>0){
                    db.executeQuery("update enrichments set Title='" + editText.getText().toString() + "' where enrichmentID='" + currentBtnEnrichment.getEnrichmentId() + "' and BID='"+currentBook.getBookID()+"'");
                    if (clickableRecyclerView.getId() == R.id.gridView) {
                        enrichmentTabListArray = db.getAllEnrichmentTabList(currentBook.getBookID(), Integer.parseInt(currentPageNumber), BookViewActivity.this);
                        ArrayList<Enrichments> topEnrList = new ArrayList<>();
                        for (Enrichments enrichments : enrichmentTabListArray){
                            if (enrichments.getCategoryID()==0){
                                topEnrList.add(enrichments);
                            }
                        }
                        loadView(topEnrList,recyclerView);
                        new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    } else if (clickableRecyclerView.getId() == R.id.gridView2) {
                        new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                        new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    }else{
                        new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    }
                    groupDialog.dismiss();
                }
            }
        });
        Button btnCancel = (Button) groupDialog.findViewById(R.id.enr_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                groupDialog.dismiss();
            }
        });
        groupDialog.show();
    }

    public void deletePopup(){
        AlertDialog.Builder builder = new AlertDialog.Builder(BookViewActivity.this);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                db.executeQuery("delete from enrichments where enrichmentID = '" + currentBtnEnrichment.getEnrichmentId() + "'");
                if (clickableRecyclerView.getId() == R.id.gridView &&currentBtnEnrichment.getCategoryID() == 0) {
                    subTabsLayout.setVisibility(View.GONE);
                    subTabLayout1.setVisibility(View.GONE);
                    SubtabsAdapter.level1 = 0;
                    ArrayList<Enrichments> topEnrList = new ArrayList<>();
                    enrichmentTabListArray = db.getAllEnrichmentTabList(currentBook.getBookID(), Integer.parseInt(currentPageNumber), BookViewActivity.this);
                    for (Enrichments enrichments : enrichmentTabListArray){
                        if (enrichments.getCategoryID()==0){
                            topEnrList.add(enrichments);
                        }
                    }
                    if (topEnrList.size()>0){
                        enrichmentButtonOnClickOperation(topEnrList.get(0));
                    }
                    loadView(topEnrList,recyclerView);
                }else if (!isLoadEnrInpopview){
                    RecyclerView curRecycler;
                    int catID;
                    if (clickableRecyclerView.getId() == R.id.gridView2){
                        curRecycler = recyclerView;
                        catID = SubtabsAdapter.level1;
                    }else{
                        curRecycler = clickableRecyclerView;
                        catID = SubtabsAdapter.level2;
                    }
                    new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber),clickableRecyclerView,0,catID,curRecycler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                dialog.cancel();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setTitle(R.string.delete);
        builder.setMessage(R.string.suredelete);
        builder.show();
    }


    public class createAdvanceSearchTab extends AsyncTask<Void, Void, Void> {
        String Url;
        String title;

        public createAdvanceSearchTab(String url, String s) {
            Url = url;
            title = s;
        }

        @Override
        protected void onPreExecute() {
            title = title.replace("'", "''");
            int  objSequentialId = db.getMaximumSequentialId(Integer.parseInt(currentPageNumber), currentBook.getBookID()) + 1;
            db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,SequentialID,categoryID)values('" + currentBook.getBookID() + "','" + currentPageNumber + "','" + title + "','Search', '0','" + Url + "','"+objSequentialId+"','"+categoryID+"')");
            int objUniqueId = db.getMaxUniqueRowID("enrichments");
            title = title.replace("''", "'");
            navigateEnrichId = objUniqueId;
            Enrichments enrichments = db.getEnrichment(objUniqueId,objSequentialId,BookViewActivity.this);
            currentBtnEnrichment = enrichments;
            if (clickableRecyclerView == null){
                SubtabsAdapter.level1 = objUniqueId;
                ArrayList<Enrichments> enrichmentTabList = db.getAllEnrichmentTabList(currentBook.getBookID(), Integer.parseInt(currentPageNumber), BookViewActivity.this);
                ArrayList<Enrichments> topEnrList = new ArrayList<>();
                for (Enrichments enrich : enrichmentTabList){
                    if (enrich.getCategoryID()==0){
                        topEnrList.add(enrich);
                    }
                }
                loadView(topEnrList,recyclerView);
            }else {
                if (clickableRecyclerView.getId() == R.id.gridView) {
                    ArrayList<Enrichments> enrichmentTabList = db.getAllEnrichmentTabList(currentBook.getBookID(), Integer.parseInt(currentPageNumber), BookViewActivity.this);
                    ArrayList<Enrichments> topEnrList = new ArrayList<>();
                    for (Enrichments enrich : enrichmentTabList){
                        if (enrich.getCategoryID()==0){
                            topEnrList.add(enrich);
                        }
                    }
                    loadView(topEnrList,recyclerView);
                    if (currentBtnEnrichment.getCategoryID()!=0) {
                        new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, currentBtnEnrichment.getCategoryID(), clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    }
                } else if (clickableRecyclerView.getId() == R.id.gridView2) {
                    new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, currentBtnEnrichment.getCategoryID(), clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                } else {
                    SubtabsAdapter.level3 = objUniqueId;
                    new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                }
            }
            if (enrichments!=null){
                enrichmentButtonOnClickOperation(enrichments);
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void showNooorPlusDialog(){
        Intent intent = new Intent(BookViewActivity.this,NooorPlusActivity.class);
        startActivity(intent);
    }

    private void viewPopUp(View view,final int categoryId){
        categoryID = categoryId;
        treeStructureView = new TreeStructureView(currentBook, db, Integer.parseInt(currentPageNumber), popoverView, BookViewActivity.this, rootView, new TreeStructureCallBack() {
            @Override
            public void onClick(TreeNode node, Object object, PopoverView popoverView) {
                Enrichments enrichments3 = (Enrichments) object;
                if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.rename))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    renamePopup();
                }else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.delete))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    deletePopup();
                }
                if (enrichments3.getEnrichmentTitle().equals("")) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    //  treeViewStructureList(gestureView, true, enrichments1.getCategoryID());
                    viewTreeList(gestureView, true, enrichments3.getCategoryID());
                }
            }
        });
        treeStructureView.treeViewStructureListNew(view,currentBtnEnrichment);
    }
}
