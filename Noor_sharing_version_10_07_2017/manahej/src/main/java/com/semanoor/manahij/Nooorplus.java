package com.semanoor.manahij;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.api.services.drive.model.User;
import com.google.gson.JsonObject;
import com.ptg.views.CircleButton;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.inappbilling.util.IabResult;
import com.semanoor.inappbilling.util.Inventory;
import com.semanoor.inappbilling.util.Purchase;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Krishna on 11-04-2017.
 */

public class Nooorplus {
    private SlideMenuWithActivityGroup context;
    LinearLayout  dotsLayout;
    TextView[] dots;
    String ITEM_SKU;
    String response;
    private Button btn_subscribe;
    private ProgressDialog processDialog;
    public Nooorplus(SlideMenuWithActivityGroup _context) {
        this.context = _context;
    }

    public void DisplayingNoorplusDialog(){
        final Dialog info_dialog = new Dialog(context);
        info_dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        info_dialog.setContentView(context.getLayoutInflater().inflate(R.layout.activity_noorplus, null));
        if (Globals.isTablet()) {
            info_dialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.5));
        }else{
            info_dialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        }
        ViewPager viewPager = (ViewPager)info_dialog. findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout)info_dialog. findViewById(R.id.layoutDots);
        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        btn_subscribe=(Button)info_dialog. findViewById(R.id.btn_subscribe);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String endDate = prefs.getString(Globals.nooorPlusEndDate, "");
        if (!endDate.equals("")) {
            if (dateDiff(endDate)){
                btn_subscribe.setText(endDate);
            }
        }
        CircleButton btn_back=(CircleButton)info_dialog. findViewById(R.id.btn_back);
        btn_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // new SaveSubscriptionGroups(context).execute();
              subscribePurchase();
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                info_dialog.dismiss();
            }
        });
        addBottomDots(0);
        info_dialog.show();


    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.welcome_slide1, container, false);
            RelativeLayout layout= (RelativeLayout)view. findViewById(R.id.img_layout);
            if(position==0){
                layout.setBackgroundResource(R.drawable.nplus1);
            }else if(position==1){
                layout.setBackgroundResource(R.drawable.nplus2);
            }else if(position==2){
                layout.setBackgroundResource(R.drawable.nplus3);
            }else {
                layout.setBackgroundResource(R.drawable.nplus4);
            }
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);


        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addBottomDots(int currentPage) {
        dots = new TextView[4];

        int[] colorsActive = context.getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = context.getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(context);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }
    /**
     * SubScription
     */
    private void subscribePurchase(){
//        if(!rootedDevice){
        //ITEM_SKU = Globals.ADS_INAPP_PURCHASE_PRODUCTID;
        ITEM_SKU="np001";
       // ITEM_SKU="l1093";
        context.mHelper = new IabHelper(context, Globals.manahijBase64EncodedPublicKey);
        context.mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    UserFunctions.complain("Problem setting up in-app billing: " + result, context);
                    return;
                }
                // Have we been disposed of in the meantime? If so, quit.
                if (context.mHelper == null) return;

                context.mHelper.launchPurchaseFlow(context, ITEM_SKU,10001, mPurchaseFinishedListener);
               // context.mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {
            if (result.isFailure()) {
                return;
            }

                /*
                 * Check for items we own. Notice that for each purchase, we check
                 * the developer payload to see if it's correct! See
                 * verifyDeveloperPayload().
                 */

            // // Check for gas delivery -- if we own gas, we should fill up the
            // tank immediately
            Purchase gasPurchase = inventory.getPurchase(ITEM_SKU);
            if (gasPurchase != null) {
                processDialog = UserFunctions.displayProcessDialog(context, context.getResources().getString(R.string.consuming_product));
                context.mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                        mConsumeFinishedListener);
                return;
            }
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            // if we were disposed of in the meantime, quit.
            if (context.mHelper == null) return;

            if (result.isFailure()) {
                int response = result.getResponse();
                if (response == 7) {
                   // UserFunctions.DisplayAlertDialog(context, R.string.item_already_owned, R.string.restoring_purchase);
                    context.mHelper.queryInventoryAsync(mGotInventoryListener);

                } else {
                   // UserFunctions.complain("Error purchasing: " + result, fa_enrich);
                }
                return;
            }

           if (purchase.getSku().equals(ITEM_SKU)){
               processDialog = UserFunctions.displayProcessDialog(context, context.getResources().getString(R.string.consuming_product));
               context.mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                //UserFunctions.alert("Thank you for subscribing", fa_enrich);
            }


        }
    };
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            if (context.mHelper == null) return;
            if (processDialog!=null && processDialog.isShowing()){
                processDialog.dismiss();
            }

            if (result.isSuccess()) {
                 new SaveSubscriptionGroups(purchase).execute();
            } else {
                UserFunctions.complain("Error while consuming: " + result, context);
            }
        }
    };

  public class SaveSubscriptionGroups extends AsyncTask<Void, Void, Void> {

        String emailId;
        long days;
        Purchase purchase;
        String response;
        ProgressDialog processDialog;

        public SaveSubscriptionGroups(Purchase purchaseDetails) {
            this.purchase = purchaseDetails;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            processDialog = UserFunctions.displayProcessDialog(context, context.getResources().getString(R.string.subscript_loading));
           // String strTransDate = UserFunctions.milisecondstoDate(purchase.getPurchaseTime());
           // String strEndDate = UserFunctions.milisecondstoEndDate(purchase.getPurchaseTime(), 2);
           // long days = UserFunctions.dateDifference(strTransDate, strEndDate);
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            String emailId = sharedPrefs.getString(Globals.sUserEmailIdKey, "");
            //String emailId = "ksuriya@semasoft.co.in";
            this.emailId = emailId;
            this.days = days;
        }

        @Override
        protected Void doInBackground(Void... params) {
           // String url1 = "http://kaladi.semanoorsoft.com/NoorReaderPermissionService/PermissionService/SaveAndroidSubscribedGroup";
            String url="http://school.nooor.com/NooorReaderPermissionService/PermissionService/SaveAndroidSubscribedGroup";
           // response = makePostRequest(url1,getJsonData(emailId,UserFunctions.getMacAddress(context),ITEM_SKU,purchase));
            response=newWebService(url,UserFunctions.getMacAddress(context),emailId,ITEM_SKU,purchase);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            "EXP_MSG_NOORPLUS" ="Your Nooor Plus expires on";
//            "EXP_MSG_NOORPLUS" ="ينتهي الاشتراك بنووور بتاريخ";
            if (response != null && !response.equals("")) {
                HashMap<String,Object> jsonList = null;
                try {
                    JSONObject array = new JSONObject(response);
                    jsonList = (HashMap<String,Object>) toMap(array);
                    for (int i=0;i<jsonList.size();i++){
                       boolean success = (boolean) jsonList.get("Success");
                        if (success){
                            new UpdateUserGroups(context).execute();
                            ArrayList<Object> subsList = (ArrayList<Object>) jsonList.get("Subscription");
                            HashMap<String,Object> getSubscription = (HashMap<String, Object>) subsList.get(subsList.size()-1);
                            String endDate = (String) getSubscription.get("EndDate");
                            btn_subscribe.setText(endDate);
                            SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
                            SharedPreferences.Editor editor = sharedPreference.edit();
                            editor.putString(Globals.nooorPlusEndDate, endDate);
                            editor.commit();
                            break;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (processDialog != null) {
                processDialog.dismiss();
            }
        }

      private Map<String, Object> toMap(JSONObject object) throws JSONException {
          Map<String, Object> map = new HashMap<String, Object>();
          Iterator<String> keysIter = object.keys();
          while (keysIter.hasNext()) {
              String key = keysIter.next();
              Object value = object.get(key);
              if (value instanceof JSONArray) {
                  value = toList((JSONArray) value);
              } else if (value instanceof JSONObject) {
                  value = toMap((JSONObject) value);
              }
              map.put(key, value);
          }
          return map;
      }

      private List<Object> toList(JSONArray array) throws JSONException {
          List<Object> list = new ArrayList<Object>();
          for (int i = 0; i < array.length(); i++) {
              Object value = array.get(i);
              if (value instanceof JSONArray) {
                  value = toList((JSONArray) value);
              } else if (value instanceof JSONObject) {
                  value = toMap((JSONObject) value);
              }
              list.add(value);
          }
          return list;
      }
    }



    private String newWebService(String url, String macId, String emailId, String productId,Purchase purchase){
        HttpURLConnection urlConnection = null;
        StringBuilder sb = new StringBuilder();
        String returnResponse = "";
        try {
            URL urlToRequest = new URL(url);

            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(25000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            OutputStream os = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getJsonData(emailId,macId,productId,purchase));
            writer.flush();
            writer.close();
            os.close();
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                System.out.println("" + sb.toString());
            } else {
                System.out.println(urlConnection.getResponseMessage());
            }

        } catch (MalformedURLException e) {

        } catch (SocketTimeoutException e) {

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return sb.toString();
    }

    private String getJsonData(String emailId,String macId,String productId,Purchase purchase){
        String activationType = "0";
        String maskEmailId = "1";
        String activationCode = purchase.getOriginalJson();
        byte[] data = new byte[0];
        try {
            data = activationCode.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String base64ActivationCode = Base64.encodeToString(data, Base64.DEFAULT).trim();
        base64ActivationCode = base64ActivationCode.replace("\n","");

// Receiving side
//        byte[] data = Base64.decode(base64, Base64.DEFAULT);
//        String text = new String(data, "UTF-8");
        String group = productId;
        JSONObject student2 = new JSONObject();
        try {
            student2.put("ActivationCode", base64ActivationCode);
            student2.put("ActivationMac_ID", macId);
            student2.put("ActivationType", activationType);
            student2.put("AppStoreID",productId);
            student2.put("EmailId",emailId);
            student2.put("EmailMaskId",maskEmailId);
            student2.put("Group","");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return student2.toString();
    }

    private String CallWebService(String url, String sOAPAction, String eNVELOPE){
        HttpURLConnection urlConnection = null;
        String returnResponse = "";
        try {
            URL urlToRequest = new URL(url);

            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(25000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            urlConnection.setRequestProperty("SOAPAction", sOAPAction);

            OutputStream out = urlConnection.getOutputStream();
            out.write(eNVELOPE.getBytes());
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

            } else if (statusCode != HttpURLConnection.HTTP_OK) {

            }

            InputStream in = urlConnection.getInputStream();
            returnResponse = UserFunctions.convertStreamToString(in);

            if (returnResponse == null || returnResponse.equals("")){
                return returnResponse;
            }

            returnResponse = returnResponse.replace("&lt;", "<").replace("&gt;", ">").replace("<?xml version='1.0' standalone='yes' ?>", "").replace("<?xml version=\"1.0\" encoding=\"utf-8\" ?>", "").replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");

        } catch (MalformedURLException e) {

        } catch (SocketTimeoutException e) {

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return returnResponse;
    }
    private boolean dateDiff(String endDate){
        String strThatDay = endDate;
        String[] split = strThatDay.split(" ");
        if (split.length>1){
            String desDate = split[0]+" "+split[1];
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date d = null;
            try {
                d = formatter.parse(desDate);//catch exception
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Calendar today = Calendar.getInstance();
            Calendar thatDay = Calendar.getInstance();
            thatDay.setTime(d);
            String startDate = UserFunctions.milisecondstoDate(today.getTimeInMillis());
            String lastDate = UserFunctions.milisecondstoDate(thatDay.getTimeInMillis());
            long difff = UserFunctions.dateDifference(startDate,lastDate);
            if (difff>0){
                return true;

            }
        }
        return false;
    }

}
