package com.semanoor.manahij;

/**
 * Created by karthik on 15-05-2017.
 */

public interface InternetSearchCallBack {
    public void onClick(String url,String title);
}
