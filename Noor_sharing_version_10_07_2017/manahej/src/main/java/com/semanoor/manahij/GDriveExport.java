package com.semanoor.manahij;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveRequest;
import com.google.api.services.drive.DriveRequestInitializer;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.google.api.services.drive.model.Permission;
import com.semanoor.sboookauthor_store.BookStorage;
import com.semanoor.sboookauthor_store.DownloadCounts;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.sboookauthor_store.RenderHTMLObjects;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.DownloadBackground;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.HTMLObjectHolder;
import com.semanoor.source_sboookauthor.Templates;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;
import com.semanoor.source_sboookauthor.Zip;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Suriya on 07/03/16.
 */
public class GDriveExport {

    private String filePath, zipFileName, mailId, enrichXmlString, title, isEditable;
    private Activity context;
    private AccountManager accountManager;
    private Account mAccount;
    private static Drive drive;
    private HashMap dicEnrichments;
    private boolean isFileUploaded;
    private ProgressDialog progressDialog, downloadProgDialog;
    ;
    private String fromEmailId;
    public int EWhenDownloadBook = 0;
    String EBookIDCode;
    public int downloadStatus = 0;
    String BookUrl;

    // View layout_view;
    String bookDetails;
    RenderHTMLObjects webview;
    Book currentBook;
    String zipPath;
    boolean connected = false;
    String enrichmentType;
    String sharedName;
    String mMain;

    public GDriveExport(Activity context, String filePath, String zipFileName, String mailId, String enrichXmlString, HashMap dicEnrichments, String title, ProgressDialog progressDialog,String type) {
        this.context = context;
        this.filePath = filePath;
        //this.filePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/Download/android-quickstart-master.zip";
        this.zipFileName = zipFileName;
        this.mailId = mailId;
        this.enrichXmlString = enrichXmlString;
        this.title = title;
        this.dicEnrichments = dicEnrichments;
        this.progressDialog = progressDialog;
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.fromEmailId = sharedPrefs.getString(Globals.sUserEmailIdKey, "");
        this.sharedName = sharedPrefs.getString(Globals.sUserNameKey, "");
        this.enrichmentType = type;
    }
    public GDriveExport(Activity context, String book,String Main) {
        this.context = context;
        this.bookDetails = book;
        mMain=Main;
    }

    public GDriveExport(Activity context, String mailid, String enrichXmlString, String resourceTitle, ProgressDialog progresdialog) {
        this.context = context;
        this.mailId = mailid;
        this.enrichXmlString = enrichXmlString;
        this.title = resourceTitle;
        this.progressDialog = progresdialog;
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.fromEmailId = sharedPrefs.getString(Globals.sUserEmailIdKey, "");
        this.sharedName = sharedPrefs.getString(Globals.sUserNameKey, "");
    }

    public GDriveExport(Activity context, String book) {
        this.context = context;
        this.bookDetails = book;
        mMain="";
    }

    public GDriveExport(Activity context, String filePath, String zipFileName, String mailId, String title, ProgressDialog progressDialog, Book book, String file, String editable) {
        this.context = context;
        this.filePath = filePath;
        //this.filePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/Download/android-quickstart-master.zip";
        this.zipFileName = zipFileName;
        this.mailId = mailId;
        this.title = title;
        this.progressDialog = progressDialog;
        currentBook = book;
        zipPath = file;
        isEditable = editable;
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.fromEmailId = sharedPrefs.getString(Globals.sUserEmailIdKey, "");
        this.sharedName = sharedPrefs.getString(Globals.sUserNameKey, "");
    }

    public void Authorize() {
        accountManager = AccountManager.get(context);
        Account[] account = accountManager.getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        if (account != null && account.length == 0) {
            UserFunctions.alert("Authorization Failed", context);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            return;
        }
        for (int i = 0; i < account.length; i++) {
            if (account[i].name.equals(fromEmailId)) {
                mAccount = account[i];
                String name = mAccount.name;
                //accountManager.getAuthToken(mAccount, "oauth2:https://www.googleapis.com/auth/drive", new Bundle(), true, new OnTokenAcquired(), null);
                HttpTransport httpTransport = new NetHttpTransport();
                JacksonFactory jsonFactory = new JacksonFactory();
              /*  GoogleAccountCredential credential =
                        GoogleAccountCredential.usingOAuth2(context, DriveScopes.DRIVE);
                credential.setSelectedAccountName(mAccount.name);
                // Trying to get a token right away to see if we are authorized
                //credential.getToken();
                Drive.Builder b = new Drive.Builder(httpTransport, jsonFactory, credential);
                b.setDriveRequestInitializer(new DriveRequestInitializer() {
                    @Override
                    protected void initializeDriveRequest(DriveRequest<?> driveRequest) throws IOException {
                        //DriveRequest request = driveRequest;
                        //request.setPrettyPrint(true);
                        //request.setKey(Globals.gDriveClientId);
                        //request.setOauthToken(token);
                    }
                });*/

                drive = new Drive.Builder(httpTransport,jsonFactory,
                        GoogleAccountCredential.usingOAuth2(context, DriveScopes.DRIVE)
                                .setSelectedAccountName(name)
                ).build();
                new uploadFileTask(false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                break;
            }
        }
    }

    private class OnTokenAcquired implements AccountManagerCallback<Bundle> {

        @Override
        public void run(AccountManagerFuture<Bundle> result) {
            try{
                final String token = result.getResult().getString(AccountManager.KEY_AUTHTOKEN);
                HttpTransport httpTransport = new NetHttpTransport();
                JacksonFactory jsonFactory = new JacksonFactory();
                GoogleAccountCredential credential =
                        GoogleAccountCredential.usingOAuth2(context, DriveScopes.DRIVE_FILE);
                credential.setSelectedAccountName(mAccount.name);
                // Trying to get a token right away to see if we are authorized
                //credential.getToken();
                Drive.Builder b = new Drive.Builder(httpTransport, jsonFactory, credential);
                b.setDriveRequestInitializer(new DriveRequestInitializer(){
                    @Override
                    protected void initializeDriveRequest(DriveRequest<?> driveRequest) throws IOException {
                        //DriveRequest request = driveRequest;
                        //request.setPrettyPrint(true);
                        //request.setKey(Globals.gDriveClientId);
                        //request.setOauthToken(token);
                    }
                });

                drive = b.build();
                new uploadFileTask(false).execute();
                //uploadFile(false);
            } catch (AuthenticatorException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (OperationCanceledException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadFile(boolean useDirectUpload)
    {
        File fileMetadata = new File();
        fileMetadata.setTitle(zipFileName);
        //fileMetadata.setDescription("A Test File");
        fileMetadata.setMimeType("application/zip");

        FileContent mediaContent = new FileContent("application/zip", new java.io.File(filePath));

        Drive.Files.Insert insert = null;
        try {
            insert = drive.files().insert(fileMetadata, mediaContent);
        } catch (IOException e) {
            e.printStackTrace();
        }
        MediaHttpUploader uploader = insert.getMediaHttpUploader();
        uploader.setDirectUploadEnabled(useDirectUpload);
        uploader.setProgressListener(new FileUploadProgressListener());
        try {
            File uploadedFile = insert.execute();
            Permission permission = insertPermission(drive, uploadedFile.getId(), "default", "anyone", "reader");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class uploadFileTask extends AsyncTask<Void, Void, Void> {
        boolean useDirectUpload, isUserAuthException = false;
        File uploadedFile;

        public uploadFileTask(boolean useDirectUpload){
            this.useDirectUpload = useDirectUpload;
        }

        @Override
        protected Void doInBackground(Void... params) {
            isFileUploaded = false;
            com.google.api.services.drive.model.File body = null;
            FileContent mediaContentt = null;
            Drive.Files.Insert insert = null;
            Drive.Files.Insert mediaInsert = null;
            File mediaFile = null;
            String fileId;
            if (currentBook != null && !currentBook.is_bStoreBook()){
                if (!currentBook.is_bStoreBook() &&currentBook.get_bStoreID() != null && currentBook.get_bStoreID().charAt(0) == 'I') {
                    fileId = getDriveFolderId("Mindmaps");
                }else if (!currentBook.is_bStoreBook() &&currentBook.get_bStoreID() != null && currentBook.get_bStoreID().charAt(0) == 'A') {
                    fileId = getDriveFolderId("Albums");
                } else{
                    fileId = getDriveFolderId("Books");
                }
            }else{
                fileId = getDriveFolderId("Enrichments");
            }
            if (currentBook != null && !currentBook.is_bStoreBook()) {
                String source;
                if (!currentBook.is_bStoreBook() &&currentBook.get_bStoreID() != null && (currentBook.get_bStoreID().charAt(0) == 'I' || currentBook.get_bStoreID().charAt(0) == 'A')) {
                    source = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "FreeFiles/card.png";
                }else{
                    source = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "FreeFiles/frontThumb.png";
                }
                Uri fileUri = Uri.fromFile(new java.io.File(source));
                java.io.File filecontent = new java.io.File(fileUri.getPath());
                mediaInsert = createFile(fileId, filecontent.getName(), "image/png", filecontent);
                try {
                    mediaFile = mediaInsert.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
          insert = createFile(fileId, zipFileName, "application/zip", new java.io.File(filePath));


            MediaHttpUploader uploader = insert.getMediaHttpUploader();
            uploader.setDirectUploadEnabled(useDirectUpload);
            uploader.setProgressListener(new FileUploadProgressListener());
            try {
                uploadedFile = insert.execute();
                Permission permission = insertPermission(drive,uploadedFile.getId(), "default", "anyone", "reader");
                if (currentBook != null) {
                    Permission permission1 = insertPermission(drive,mediaFile.getId(), "default", "anyone", "reader");
                }
               if (context instanceof ExportEnrGroupActivity) {
                    if (currentBook != null) {
                        String type;
                        if (!currentBook.is_bStoreBook() &&currentBook.get_bStoreID() != null && currentBook.get_bStoreID().charAt(0) == 'I') {
                            type = "Mindmap";
                        }else if (!currentBook.is_bStoreBook() &&currentBook.get_bStoreID() != null && currentBook.get_bStoreID().charAt(0) == 'A') {
                            type = "Album";
                        } else{
                            type = "sharedBook";
                        }
                        if (type.equals("sharedBook")) {
                            jsonData(uploadedFile.getWebContentLink(), uploadedFile.getFileSize(), mediaFile.getWebContentLink(), type);
                        }else{
                            jsonDataForMediaAndMindmap(uploadedFile.getWebContentLink(), uploadedFile.getFileSize(), mediaFile.getWebContentLink(), type);
                        }
                    }else{
                        createJsonValues(uploadedFile.getWebContentLink(), uploadedFile.getFileSize());
                    }
                } else {
                    createJsonValues(uploadedFile.getWebContentLink(), uploadedFile.getFileSize());
                }
            } catch (UserRecoverableAuthIOException e) {
                isUserAuthException = true;
                if (context instanceof BookViewReadActivity) {
                    context.startActivityForResult(e.getIntent(), BookViewReadActivity.AUTH_CODE_REQUEST_CODE);
                } else if (context instanceof ExportEnrichmentActivity) {
                    context.startActivityForResult(e.getIntent(), ExportEnrichmentActivity.AUTH_CODE_REQUEST_CODE);
                } else if (context instanceof MainActivity) {
                    context.startActivityForResult(e.getIntent(), 1003);
                } else if (context instanceof ExportEnrGroupActivity) {
                    context.startActivityForResult(e.getIntent(), ExportEnrGroupActivity.AUTH_CODE_REQUEST_CODE);
                }
            } catch (IOException e) {
                isFileUploaded = false;
                e.printStackTrace();
            }
            /*String downloadLink = "https://docs.google.com/a/semasoft.co.in/uc?id=0B-HK8gA_mVjCWEh5OUttRXVzaEk&export=download";
            createJsonValues(downloadLink);*/
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isUserAuthException) {
                return;
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            if (isFileUploaded) {
               if (context instanceof ExportEnrGroupActivity) {
                    if ((((ExportEnrGroupActivity) context).Share)) {
                        String absPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
                      	//String path= "http://192.168.0.52/SemaLibrary/share.html?id="+ mailId;;       just for testing
                        String Url =Globals.enrShareUrl + mailId+context.getResources().getString(R.string.share_enrichment);
                       // ((ExportEnrGroupActivity) context).share(Url);
                        ((ExportEnrGroupActivity) context).shareIntent(Url);
                    }else{
                        UserFunctions.alert(context.getResources().getString(R.string.upload_success), context);
                    }
                }else {
                    UserFunctions.alert(context.getResources().getString(R.string.upload_success), context);
                }
            } else {
                UserFunctions.alert(context.getResources().getString(R.string.upload_failed), context);
            }
            if (zipPath!=null){
                java.io.File enrichZipFilePath = new java.io.File(zipPath);
                enrichZipFilePath.delete();
            }
        }
    }

    private void jsonData(String downloadUrl, Long size, String coverImageUrl,String type) {
        String storeId = "001";
        String zipFilename = this.zipFileName;
        String currentDateTime = UserFunctions.getCurrentDateTimeInString();
        //String xmlString = enrichXmlString;
        //String zipFilename = this.zipFileName;
        String bookId = null, bookTitle = null;
        bookId = currentBook.get_bStoreID();
        bookTitle = currentBook.getBookTitle();
        JSONArray jsonArray = new JSONArray();
        JSONObject groups = bookDetails();

        JSONArray pageDetails = jsonpageDetails();

        JSONObject jsonData = new JSONObject();

        try {
            jsonData.put("sharedBy", fromEmailId);
            jsonData.put("type", type);
            jsonData.put("coverImagePath", coverImageUrl);
            jsonData.put("dateTime", currentDateTime);
            jsonData.put("downloadPath", downloadUrl);
            jsonData.put("bookDetails", groups);
            jsonData.put("book", pageDetails);
            jsonData.put("sharedName",sharedName);
            jsonData.put("masterFileName", zipFilename);
            jsonData.put("fileSize", size);
            String jsonString = jsonData.toString();
            sendToSemaNotificationService(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void jsonDataForMediaAndMindmap(String downloadUrl, Long size, String coverImageUrl,String type) {
        String bookId = null, bookTitle = null;
        String zipFilename = this.zipFileName;
        String currentDateTime = UserFunctions.getCurrentDateTimeInString();
        bookId = currentBook.get_bStoreID();
        bookTitle = currentBook.getBookTitle();
        JSONArray jsonArray = new JSONArray();
        //JSONObject groups = bookDetails();

        //JSONArray pageDetails = jsonpageDetails();

        JSONObject jsonData = new JSONObject();

        try {
            jsonData.put("bookTitle",currentBook.getBookTitle());
            jsonData.put("sharedBy", fromEmailId);
            jsonData.put("type", type);
            jsonData.put("dateTime", currentDateTime);
            jsonData.put("downloadPath", downloadUrl);
            jsonData.put("IDBook",String.valueOf(currentBook.get_bStoreID()));
            jsonData.put("coverImagePath", coverImageUrl);
            jsonData.put("catID", String.valueOf(currentBook.getbCategoryId()));
            jsonData.put("sharedTo", mailId);
            jsonData.put("sharedName",sharedName);
            jsonData.put("masterFileName", zipFilename);
            jsonData.put("fileSize", size);
            String jsonString = jsonData.toString();
            sendToSemaNotificationService(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONArray jsonpageDetails() {
        ArrayList<Object> objectList;
        JSONArray pageDetails = new JSONArray();
        for (int i = 1; i < currentBook.getTotalPages() + 1; i++) {
            String query = "select * from objects where pageNO='"+String.valueOf(i)+"' and EnrichedID='"+0+"' and BID='"+currentBook.getBookID()+"'order by SequentialID";
           if(context instanceof  MainActivity) {
               objectList = ((MainActivity) context).db.getAllObjectsFromPage(query);
           }else{
               objectList = ((ExportEnrGroupActivity) context).db.getAllObjectsFromPage(query);
           }
            for (int j = 0; j < objectList.size(); j++) {
                HTMLObjectHolder htmlObject = (HTMLObjectHolder) objectList.get(j);
                String objType = htmlObject.getObjType();
                JSONObject page = new JSONObject();
                try {
                    if( objType.equals("WebTable") ) {
                        page.put("size",htmlObject.getObjSize());
                    }else {
                        page.put("size", htmlObject.getObjWidth() + "|" + htmlObject.getObjHeight());
                    }
                    page.put("content", htmlObject.getObjContentPath());
                    page.put("rowID", String.valueOf(htmlObject.getObjUniqueRowId()));
                    page.put("SeqID", String.valueOf(htmlObject.getObjSequentialId()));
                    page.put("objType", htmlObject.getObjType());
                    page.put("pageNo", htmlObject.getObjPageNo());
                    if (objType.equals("WebText") || objType.equals("WebTable") || objType.equals("TextSquare") || objType.equals("TextRectangle") || objType.equals("TextCircle") || objType.equals(Globals.objType_pageNoText) || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT) || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT) ) {
                       page.put("location", htmlObject.getObjXPos() + "|"+htmlObject.getObjYPos());
                    }else{
                        page.put("location", htmlObject.getObjXPos() + "|" + htmlObject.getObjYPos()+"|0.0000");
                    }
                    page.put("ScalePageToFit", htmlObject.getObjScalePageToFit());
                    page.put("objID", String.valueOf(htmlObject.getObjBID()));
                    pageDetails.put(page);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return pageDetails;
    }

    private JSONObject bookDetails() {
        JSONObject groups = new JSONObject();
        try {
            groups.put("template", String.valueOf(currentBook.get_bTemplateId()));
            groups.put("bIndex", String.valueOf(currentBook.getBookIndex()));
            groups.put("showPageNo", String.valueOf(currentBook.get_lastViewedPage()));
            groups.put("StoreBook", "no");
            groups.put("storeID", "no");
            groups.put("title", currentBook.getBookTitle());
            groups.put("MLastViewedPage", String.valueOf(currentBook.get_lastViewedPage()));
            groups.put("isEditable", isEditable);
            groups.put("CID", String.valueOf(currentBook.getbCategoryId()));
            groups.put("totalPages", String.valueOf(currentBook.getTotalPages()));
            groups.put("Description", currentBook.get_bDescription());
            groups.put("PageBGValue", String.valueOf(currentBook.get_bPagebgvalue()));
            groups.put("blanguage", currentBook.getBookLangugae());
            groups.put("oriantation", String.valueOf(currentBook.getBookOrientation()));
            groups.put("Author", currentBook.getBookAuthorName());
            String domainUrl=null;
            if(context instanceof  BookViewReadActivity){
                domainUrl =((BookViewReadActivity) context).db.getStoreBookDomainUrl(currentBook.get_bStoreID(),currentBook.getBookID());
            }else  if(context instanceof  MainActivity){
                domainUrl =((MainActivity) context).db.getStoreBookDomainUrl(currentBook.get_bStoreID(),currentBook.getBookID());
            } if(context instanceof  ExportEnrGroupActivity){
                domainUrl =((ExportEnrGroupActivity) context).db.getStoreBookDomainUrl(currentBook.get_bStoreID(),currentBook.getBookID());
            }

            groups.put("domainURL",domainUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return groups;
    }

    public class createJsonValueForResource extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            isFileUploaded = false;
            String storeId = "001";
            String bookId = null;
            String bookTitle = null;
            HashMap<String, String> bookMap = null;

            if (context instanceof BookViewReadActivity) {
                bookTitle= ((BookViewReadActivity) context).currentBook.getBookTitle();
                int updateCount= ((BookViewReadActivity) context).db.getupdateCountsFromDb(((BookViewReadActivity) context).currentBook.getBookID());
                bookMap = getBookInfo(((BookViewReadActivity) context).currentBook,updateCount);
                bookId = ((BookViewReadActivity) context).currentBook.get_bStoreID();
            } else if (context instanceof ExportEnrGroupActivity) {
                bookTitle= ((ExportEnrGroupActivity) context).currentBook.getBookTitle();
                int updateCount= ((ExportEnrGroupActivity) context).db.getupdateCountsFromDb(((ExportEnrGroupActivity) context).currentBook.getBookID());
                bookMap = getBookInfo(((ExportEnrGroupActivity) context).currentBook,updateCount);
                bookId = ((ExportEnrGroupActivity) context).currentBook.get_bStoreID();
            } else if (context instanceof ExportEnrichmentActivity) {
                bookTitle= ((ExportEnrichmentActivity) context).currentBook.getBookTitle();
                bookId = ((ExportEnrichmentActivity) context).currentBook.get_bStoreID();
                int updateCount= ((ExportEnrichmentActivity) context).db.getupdateCountsFromDb(((ExportEnrichmentActivity) context).currentBook.getBookID());
                bookMap = getBookInfo(((ExportEnrichmentActivity) context).currentBook,updateCount);
            }
            String currentDateTime = UserFunctions.getCurrentDateTimeInString();
            HashMap mainMap = new HashMap();
            mainMap.put("BOOK",bookMap);
            mainMap.put("sharedBy", fromEmailId);
            mainMap.put("storeID", storeId);
            mainMap.put("sharedTo", mailId);
            mainMap.put("bookTitle", bookTitle);
            mainMap.put("dateTime", currentDateTime);
            mainMap.put("IDBook", bookId);
            mainMap.put("sharedTitle",title);
            mainMap.put("ContentXMl", enrichXmlString);
            mainMap.put("type", "Resource");

            JSONObject jsonObj = new JSONObject(mainMap);
            String jsonString = jsonObj.toString();
            sendToSemaNotificationService(jsonString);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isFileUploaded) {
                UserFunctions.alert(context.getResources().getString(R.string.upload_success), context);
            } else {
                UserFunctions.alert(context.getResources().getString(R.string.upload_failed), context);
            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

    private void createJsonValues(String downloadUrl, Long fileSize){
        String storeId = "001";

        int page;
        String currentDateTime = UserFunctions.getCurrentDateTimeInString();
        Enrichments enrichments = null;
        //String xmlString = enrichXmlString;
        String zipFilename = this.zipFileName;
        String bookId = null, bookTitle = null;
        if (context instanceof BookViewReadActivity) {
            bookId = ((BookViewReadActivity)context).currentBook.get_bStoreID();
            bookTitle = ((BookViewReadActivity)context).currentBook.getBookTitle();
        } else if(context instanceof ExportEnrichmentActivity) {
            bookId = ((ExportEnrichmentActivity)context).currentBook.get_bStoreID();
            bookTitle = ((ExportEnrichmentActivity)context).currentBook.getBookTitle();
        }else  if (context instanceof ExportEnrGroupActivity) {
            bookId = ((ExportEnrGroupActivity)context).currentBook.get_bStoreID();
            bookTitle = ((ExportEnrGroupActivity)context).currentBook.getBookTitle();
            enrichments =((ExportEnrGroupActivity)context).enrichmentDetails.enrichmentSelectedList.get( ((ExportEnrGroupActivity)context).enrichmentDetails.enrichmentSelectedList.size()-1);
        }

        HashMap mainMap = new HashMap();
        mainMap.put("sharedBy", this.fromEmailId);
        mainMap.put("downloadPath", downloadUrl);
        mainMap.put("dateTime", currentDateTime);
        mainMap.put("storeID", storeId);
        mainMap.put("sharedTo", mailId);
        mainMap.put("masterFileName", zipFilename);
        mainMap.put("IDBook", bookId);
        mainMap.put("bookTitle", bookTitle);
        mainMap.put("type", enrichmentType);
        if(enrichments!=null) {
            mainMap.put("sharedPage", enrichments.getEnrichmentPageNo());
        }else{
            mainMap.put("sharedPage", 2);
        }
        mainMap.put("sharedTitle",title);
        mainMap.put("sharedName",sharedName);
        mainMap.put("fileSize",fileSize);

        HashMap<String, String> bookMap = null;
        if (context instanceof BookViewReadActivity) {
            int updateCount= ((BookViewReadActivity) context).db.getupdateCountsFromDb(((BookViewReadActivity) context).currentBook.getBookID());
            bookMap = getBookInfo(((BookViewReadActivity) context).currentBook,updateCount);
        } else if (context instanceof ExportEnrichmentActivity) {
            int updateCount= ((ExportEnrichmentActivity) context).db.getupdateCountsFromDb(((ExportEnrichmentActivity) context).currentBook.getBookID());
            bookMap = getBookInfo(((ExportEnrichmentActivity) context).currentBook,updateCount);
        }else if (context instanceof ExportEnrGroupActivity) {
            int updateCount= ((ExportEnrGroupActivity) context).db.getupdateCountsFromDb(((ExportEnrGroupActivity) context).currentBook.getBookID());
            bookMap = getBookInfo(((ExportEnrGroupActivity) context).currentBook,updateCount);
        }

        mainMap.put("BOOK", bookMap);
       if(enrichmentType.equals("Enrichment")) {
           mainMap.put("enrichments", dicEnrichments);
       }else{
            mainMap.put("ContentXMl", enrichXmlString);
       }
        JSONObject jsonObj = new JSONObject(mainMap);
        String jsonString = jsonObj.toString();
        sendToSemaNotificationService(jsonString);
    }

    private HashMap getBookInfo(Book book, int updateCount){
        HashMap<String, String> bookMap = new HashMap<String, String>();
        bookMap.put("BID", String.valueOf(book.getBookID()));
        bookMap.put("title", book.getBookTitle());
        bookMap.put("description", book.get_bDescription());
        bookMap.put("author", book.getBookAuthorName());
        bookMap.put("bIndex", String.valueOf(book.getBookIndex()));
        bookMap.put("template", String.valueOf(book.get_bTemplateId()));
        bookMap.put("orientation", String.valueOf(book.getBookOrientation()));
        bookMap.put("totalPages", String.valueOf(book.getTotalPages()));
        bookMap.put("pageBGValue", String.valueOf(book.get_bPagebgvalue()));
        bookMap.put("updateCounts", String.valueOf(updateCount));
        String isStoreBook = "no";
        if (book.is_bStoreBook()) {
            isStoreBook = "yes";
        }
        bookMap.put("storeBook", isStoreBook);
        bookMap.put("storeID", book.get_bStoreID());
        bookMap.put("showPageNo", "");
        bookMap.put("CID", String.valueOf(book.getbCategoryId()));
        bookMap.put("LastViewedPage", String.valueOf(book.get_lastViewedPage()));
        bookMap.put("language", book.getBookLangugae());
        String storeBookCreatedFromIpad = "no";
        if (book.isStoreBookCreatedFromTab()) {
            storeBookCreatedFromIpad = "yes";
        }
        bookMap.put("storebookFromIpad", storeBookCreatedFromIpad);
        bookMap.put("downloadCompleted", "downloaded");
        bookMap.put("downloadURL", book.getDownloadURL());
        bookMap.put("bookEditable", book.getIsEditable());
        bookMap.put("isMalzamah", book.getIsMalzamah());
        bookMap.put("imageURL", book.getImageURL());
        bookMap.put("price", book.getPrice());
        String domainUrl=null;
        if(context instanceof BookViewReadActivity) {
           domainUrl = ((BookViewReadActivity) context).db.getStoreBookDomainUrl(book.get_bStoreID(), book.getBookID());
        }else{
            domainUrl = ((ExportEnrGroupActivity) context).db.getStoreBookDomainUrl(book.get_bStoreID(), book.getBookID());
        }
        bookMap.put("domainURL",domainUrl);
        return bookMap;
    }

    private void sendToSemaNotificationService(String jsonContent) {
        try {
            byte[] data = jsonContent.getBytes("UTF-8");
            String encodedJson = Base64.encodeToString(data, Base64.NO_WRAP);
            WebService webService = new WebService(context, Globals.getNewCurriculumWebServiceURL());
            String response = webService.newCloudEnrichmentsFromUser(fromEmailId, mailId, title, encodedJson);
            parseXml(response, context);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /*
    *  Parse Xml method for parsing the return string of Webservice calls
    */
    private void parseXml(String returnXml, Context context) {
        try {
            SAXParserFactory mySAXParserFactory = SAXParserFactory.newInstance();
            SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
            XMLReader myXMLReader = mySAXParser.getXMLReader();
            MyXmlHandler myRSSHandler = new MyXmlHandler();
            myXMLReader.setContentHandler(myRSSHandler);
            InputSource inputsource = new InputSource();
            inputsource.setEncoding("UTF-8");
            inputsource.setCharacterStream(new StringReader(returnXml));
            myXMLReader.parse(inputsource);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class MyXmlHandler extends DefaultHandler {

        String tempString;
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);

        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            super.characters(ch, start, length);
            tempString = new String(ch, start, length);
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            super.endElement(uri, localName, qName);
            if (localName.equals("NewCloudEnrichmentsResult")) {
                if (tempString.equals("True") || tempString.equals("true")) {
                    isFileUploaded = true;
                } else {
                    isFileUploaded = false;
                }
            }
        }
    }

    public static class FileUploadProgressListener implements MediaHttpUploaderProgressListener {

        @Override
        public void progressChanged(MediaHttpUploader uploader) throws IOException {
            switch (uploader.getUploadState()) {
                case INITIATION_STARTED:
                    //View.header2("Upload Initiation has started.");
                    break;
                case INITIATION_COMPLETE:
                    //View.header2("Upload Initiation is Complete.");
                    break;
                case MEDIA_IN_PROGRESS:
                    //View.header2("Upload is In Progress: " + NumberFormat.getPercentInstance().format(uploader.getProgress()));
                    break;
                case MEDIA_COMPLETE:
                    //View.header2("Upload is Complete!");
                    break;
            }
        }
    }

    private static Permission insertPermission(Drive service, String fileId, String value, String type, String role) {
        Permission newPermisssion = new Permission();
        newPermisssion.setValue(value);
        newPermisssion.setType(type);
        newPermisssion.setRole(role);
        try {
            return service.permissions().insert(fileId, newPermisssion).execute();
        } catch (IOException e) {
            System.out.println("An Error occured" +e);
        }
        return null;
    }

    public static class EnrichmentDownloadFromGDrive extends AsyncTask<Void, Integer, Void> {

        String message, bookTitle, sharedBy;
        String enrStoreBookId;
        String enrMasterFileName;
        public String dProgressString="";
        String downloadFromPath;
        public boolean downloadSuccess;
        ProgressDialog downloadProgDialog;
        String enrFileToPath;
        Context ctx;
        JSONObject jsonObj;
        String bookDir,type;
        ArrayList<Object> bookList;
        ArrayList<HashMap<String, String>> selectedCloudEnrList;
        ArrayList<HashMap<String, String>> cloudEnrList;
        int selectedCloudEnrPosition;
        SlideMenuWithActivityGroup.ViewAdapter loginAdapter;
        ProgressDialog syncProgressDialog;

        public EnrichmentDownloadFromGDrive(Context ctx, ProgressDialog downloadProgDialog, String message, JSONObject jsonObj, ArrayList<Object> bookList, ArrayList<HashMap<String, String>> selectedCloudEnrList, int selectedCloudEnrPosition, ArrayList<HashMap<String, String>> cloudEnrList, SlideMenuWithActivityGroup.ViewAdapter loginAdapter, ProgressDialog syncProgressDialog){
            this.message = message;
            this.downloadProgDialog = downloadProgDialog;
            this.jsonObj = jsonObj;
            this.bookList = bookList;
            this.selectedCloudEnrList = selectedCloudEnrList;
            this.selectedCloudEnrPosition = selectedCloudEnrPosition;
            this.cloudEnrList = cloudEnrList;
            this.loginAdapter = loginAdapter;
            this.syncProgressDialog = syncProgressDialog;
            this.ctx = ctx;
            try {
                enrStoreBookId = jsonObj.getString("IDBook");
                bookTitle = jsonObj.getString("bookTitle");
                sharedBy = jsonObj.getString("sharedBy");
                downloadFromPath = jsonObj.getString("downloadPath");
                if(jsonObj.has("masterFileName")) {
                    enrMasterFileName = jsonObj.getString("masterFileName").replace(".zip", "");
                }
                type = jsonObj.getString("type");
                if(enrMasterFileName==null||enrMasterFileName.equals("")) {
                    enrMasterFileName = "ZipFile";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (selectedCloudEnrList.size() == 1) {
                downloadProgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                downloadProgDialog.setMessage("");
                downloadProgDialog.getWindow().setTitleColor(Color.rgb(222, 223, 222));
                downloadProgDialog.setIndeterminate(false);
                downloadProgDialog.setCancelable(false);
                downloadProgDialog.setCanceledOnTouchOutside(false);
                downloadProgDialog.setTitle(message);
                downloadProgDialog.setProgress(0);
                downloadProgDialog.setMax(100);
                downloadProgDialog.show();
            }

            bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+enrStoreBookId+"Book/";
            enrFileToPath = bookDir+type+"/";
            if (!new java.io.File(enrFileToPath).exists()) {
                new java.io.File(enrFileToPath).mkdirs();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            final int TIMEOUT_CONNECTION = 5000;//5sec
            final int TIMEOUT_SOCKET = 30000;//30sec
            long bytes = 1024 * 1024;
            try{
                URL url = new URL(downloadFromPath);
                //Open a connection to that URL.
                URLConnection ucon = url.openConnection();
                ucon.connect();
                int   fileLength = ucon.getContentLength();
                //////System.out.println("filelength:"+fileLength);
                long startTime = System.currentTimeMillis();

                //this timeout affects how long it takes for the app to realize there's a connection problem
                ucon.setReadTimeout(TIMEOUT_CONNECTION);
                ucon.setConnectTimeout(TIMEOUT_SOCKET);
                //Define InputStreams to read from the URLConnection.
                // uses 3KB download buffer
                InputStream is = ucon.getInputStream();
                BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
                //Internal storage
                String filePath = enrFileToPath+enrMasterFileName+".zip";
                FileOutputStream outStream = new FileOutputStream(filePath);
                byte[] buff = new byte[5 * 1024];
                //Read bytes (and store them) until there is nothing more to read(-1)
                int len;
                String totalMB,fileLengthMB;
                long total = 0;
                DecimalFormat twoDformat = new DecimalFormat("#.##");
                while ((len = inStream.read(buff)) != -1)
                {
                    total += len;
                    ////////System.out.println("total:"+total);
                    totalMB=twoDformat.format((float)total/bytes);
                    fileLengthMB= twoDformat.format((float)fileLength/bytes);
                    dProgressString=totalMB+" MB"+" of "+fileLengthMB+" MB";
                    publishProgress((int)((total*100)/fileLength));
                    outStream.write(buff,0,len);
                }
                //clean up
                outStream.flush();
                outStream.close();
                inStream.close();
                downloadSuccess = true;
                ////System.out.println( "download completed in "+ ((System.currentTimeMillis() - startTime) / 1000)+ " sec");

            }
            catch (IOException e){
                ////System.out.println("error while storing:"+e);
                downloadSuccess = false;
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress){
            super.onProgressUpdate(progress);
            if(downloadProgDialog != null && selectedCloudEnrList.size() == 1){
                ////System.out.println("OnprogressUpdate:"+progress[0]);
                downloadProgDialog.setMessage(dProgressString);
                downloadProgDialog.setProgress(progress[0]);
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (downloadSuccess) {
                String zipfile = enrFileToPath+enrMasterFileName + ".zip";
                String unziplocation = enrFileToPath+enrMasterFileName + "/"; //"/" is a must here..
                Boolean unzipSuccess = null;
                if(type.equals("Enrichment")) {
                    unzipSuccess = Zip.unzipDownloadfileNew(zipfile, unziplocation); //if unzipping success then need to store in database:
                }else if(type.equals("Resource")){
                    String url = Globals.TARGET_BASE_MINDMAP_PATH;
                    unzipSuccess = Zip.unzipDownloadfileNew(zipfile, url); //if unzipping success then need to store in database:
                    if (ctx instanceof CloudEnrichDetails) {
                        UserFunctions.copyAsset( ((CloudEnrichDetails) ctx).getAssets(), "semamindmap.min.js", Globals.TARGET_BASE_MINDMAP_PATH + "semamindmap.min.js");
                        UserFunctions.copyAsset( ((CloudEnrichDetails) ctx).getAssets(), "style.min.css", Globals.TARGET_BASE_MINDMAP_PATH + "style.min.css");
                    }
                }
                if(unzipSuccess){
                    new java.io.File(zipfile).delete();
                    //DB entry :
                    String enrichmentStr = null;

                     //   if(jsonObj.has("enrichments")){
                        if(type.equals("Enrichment")) {
                           // enrichmnetDetails(ctx,contentXml,enrStoreBookId,sharedBy,enrFileToPath,enrMasterFileName,bookDir);
                            try {
                             enrichmentStr = jsonObj.getString("enrichments");

                             JSONObject enrJson = new JSONObject(enrichmentStr);
                            int enrJsonEnrCount = enrJson.length();
                        for (int j = 0; j < enrJsonEnrCount; j++) {
                            String enrJsonContent = enrJson.getString("enrichment" + j);
                            JSONObject jsonObjEnr = new JSONObject(enrJsonContent);
                            String enrTitle = jsonObjEnr.getString("title");
                            String enrPageNo = jsonObjEnr.getString("pageNo");
                            String enrFileName = jsonObjEnr.getString("file1");
                            String enrichmentIDGlobal = jsonObjEnr.getString("enrichmentIDGlobal");
                            String categoryId=jsonObjEnr.getString("categoryId");
                            String[] splitEnrIDGlobal = enrichmentIDGlobal.split("-");
                            String[] splitStoreBookId = splitEnrIDGlobal[1].split("M");
                            //String storeBookId = splitStoreBookId[1];

                            Enrichments enrichment = new Enrichments(ctx);
                            enrichment.setEnrichmentId(Integer.parseInt(splitEnrIDGlobal[3]));
                            enrichment.setEnrichmentPageNo(Integer.parseInt(enrPageNo));
                            enrichment.setEnrichmentTitle(enrTitle);
                            enrichment.setEnrichmentBookTitle(bookTitle);
                            enrichment.setEnrichmentStoreBookId(enrStoreBookId);
                            enrichment.setEnrichmentBid(getBookIDFromStoreId(enrStoreBookId));
                            enrichment.setEnrichmentEmailId(sharedBy);

                            String dateString = UserFunctions.getCurrentDateTimeInString();
                            String zipFile = enrFileToPath + enrMasterFileName + "/" + enrFileName;
                            String unzipLoc = bookDir + "Enrichments/Page" + enrPageNo + "/Enriched" + splitEnrIDGlobal[3] + "_" + dateString + "/";
                            Boolean unzipFileSuccess = Zip.unzipDownloadfileNew(zipFile, unzipLoc);
                         //   if (unzipFileSuccess) {
                                String deleteDir = enrFileToPath + enrMasterFileName + "/";
                                UserFunctions.DeleteDirectory(new java.io.File(deleteDir));
                                UserFunctions.traverseAndMakeFileWorldReadable(new java.io.File(bookDir + "Enrichments"));
                                String elePath = unzipLoc + enrFileName.replace(".zip", "") + "/index.htm";
                                enrichment.createDownloadEnrichmentEntryInBookXmlForGdrive(elePath);
                                enrichment.checkAndUpdateDbForUpdateRecords(elePath);
                          //  }
                          }
                       } catch (JSONException e) {
                                e.printStackTrace();
                            }
                    }  /* else  if(jsonObj.has("resources")){
                            enrichmentStr = jsonObj.getString("resources");
                            //resources
                            JSONObject enrJson = new JSONObject(enrichmentStr);
                            int enrJsonEnrCount = enrJson.length();
                            for (int j = 0; j < enrJsonEnrCount; j++) {
                                String enrJsonContent = enrJson.getString("resource" + j);
                                JSONObject jsonObjEnr = new JSONObject(enrJsonContent);
                                String enrTitle = jsonObjEnr.getString("title");
                                String enrPageNo = jsonObjEnr.getString("pageNo");
                                String enrFileName = jsonObjEnr.getString("file");
                                String enrichmentIDGlobal = jsonObjEnr.getString("enrichmentIDGlobal");
                                String[] splitEnrIDGlobal = enrichmentIDGlobal.split("-");
                                String[] splitStoreBookId = splitEnrIDGlobal[1].split("M");


                                Enrichments enrichment = new Enrichments(ctx);
                                enrichment.setEnrichmentId(Integer.parseInt(splitEnrIDGlobal[3]));
                                enrichment.setEnrichmentPageNo(Integer.parseInt(enrPageNo));
                                enrichment.setEnrichmentTitle(enrTitle);
                                enrichment.setEnrichmentBookTitle(bookTitle);
                                enrichment.setEnrichmentStoreBookId(enrStoreBookId);
                                enrichment.setEnrichmentBid(getBookIDFromStoreId(enrStoreBookId));
                                enrichment.setEnrichmentEmailId(sharedBy);

                                String dateString = UserFunctions.getCurrentDateTimeInString();
                                //String zipFile = Globals.TARGET_BASE_MINDMAP_PATH+ enrFileName;
                               // String unzipLoc = bookDir + "Enrichments/Page" + enrPageNo + "/Enriched" + splitEnrIDGlobal[3] + "_" + dateString + "/";
                                String url = Globals.TARGET_BASE_MINDMAP_PATH;
                                Boolean unzipFileSuccess = Zip.unzipDownloadfileNew(zipfile, url);
                                if (unzipFileSuccess) {
                                    String deleteDir = enrFileToPath + enrMasterFileName + "/";
                                    new java.io.File(zipfile).delete();
                                    UserFunctions.DeleteDirectory(new java.io.File(deleteDir));

                                    UserFunctions.traverseAndMakeFileWorldReadable(new java.io.File(Globals.TARGET_BASE_MINDMAP_PATH));
                                    String fname=enrFileName.replace(".zip","");
                                    String fileName=  Globals.TARGET_BASE_MINDMAP_PATH+ fname+"/mindmap.xml";
                                    UserFunctions.copyFiles(fileName,Globals.TARGET_BASE_MINDMAP_PATH+enrTitle+".xml");
                                    UserFunctions.DeleteDirectory(new java.io.File(Globals.TARGET_BASE_MINDMAP_PATH+ fname));
                                    java.io.File  f=new java.io.File( Globals.TARGET_BASE_MINDMAP_PATH+ enrFileName);
                                    if(f.exists()) {
                                        f.delete();
                                    }
                                 String query = "insert into enrichments(BID, pageNO, Title, Type, Exported, Path) values ('"+enrichment.getEnrichmentBid()+"', '"+enrichment.getEnrichmentPageNo()+"', '"+enrTitle+"', '"+Globals.OBJTYPE_MINDMAPTAB+"', '10', '"+enrTitle+"')";
                                ((CloudEnrichDetails) ctx).db.executeQuery(query);
                                }
                            }
                   } */ else if(type.equals("Resource")){
//                            String contentXml= null;
//                            try {
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
                             Document doc=null;
                                DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
                                try{
                                    String  contentXml = jsonObj.getString("ContentXMl");
                                    DocumentBuilder db=dbf.newDocumentBuilder();
                                    InputSource is=new InputSource();
                                    is.setCharacterStream(new StringReader(contentXml));
                                    doc=db.parse(is);
                                    NodeList nl=doc.getElementsByTagName("MindMaps");
                                    Node pagesNode = doc.getElementsByTagName("MindMaps").item(0);

                                    NodeList pagesnodesList = pagesNode.getChildNodes();
                                    String title=null;
                                    String page=null;
                                    for(int i = 0;i< pagesnodesList.getLength(); i++){
                                        // Node hhj=pagesnodesList.item(i);
                                        Node element = pagesnodesList.item(i);
                                        NamedNodeMap attribute = element.getAttributes();
                                        Node nodeAttr = attribute.getNamedItem("IDPage");
                                        Node nodetitle = attribute.getNamedItem("Title");
                                        Node nodetype = attribute.getNamedItem("file");
                                        title=nodetitle.getNodeValue();
                                        page=nodeAttr.getNodeValue();

                                    String idBook = jsonObj.getString("BOOK");
                                    JSONObject json=new JSONObject(idBook);
                                    String Bid=json.getString("BID");
                                    String dateString = UserFunctions.getCurrentDateTimeInString();
                                    String enrFileName=Bid+".zip";
                                    String zipFile = Globals.TARGET_BASE_MINDMAP_PATH+ enrFileName;
                                    // String unzipLoc = bookDir + "Enrichments/Page" + enrPageNo + "/Enriched" + splitEnrIDGlobal[3] + "_" + dateString + "/";
                                    String url = Globals.TARGET_BASE_MINDMAP_PATH;
                                    //Boolean unzipFileSuccess = Zip.unzipDownloadfileNew(zipFile, url);
                                    //if (unzipFileSuccess) {
                                        //String deleteDir = enrFileToPath + enrMasterFileName + "/";
                                        //new java.io.File(zipfile).delete();
                                        //UserFunctions.DeleteDirectory(new java.io.File(deleteDir));

                                        UserFunctions.traverseAndMakeFileWorldReadable(new java.io.File(Globals.TARGET_BASE_MINDMAP_PATH));
//                                        String fname = enrFileName.replace(".zip", "");
//                                        java.io.File Path = new java.io.File(Globals.TARGET_BASE_MINDMAP_PATH+enrMasterFileName);
                                        //java.io.File[] mindMapFiles = Path.listFiles();
//                                        for (java.io.File file : mindMapFiles) {
//                                            String filaName = file.getName();
//                                            UserFunctions.copyFiles(file.getPath(), Globals.TARGET_BASE_MINDMAP_PATH+ filaName);
//
//                                        }
                                        //UserFunctions.DeleteDirectory(Path);
//                                        String fileName = Globals.TARGET_BASE_MINDMAP_PATH +enrMasterFileName+ "/"+title+".xml";
//                                        UserFunctions.copyFiles(fileName, Globals.TARGET_BASE_MINDMAP_PATH + title + ".xml");
//                                        UserFunctions.DeleteDirectory(new java.io.File(Globals.TARGET_BASE_MINDMAP_PATH + fname));
//                                        java.io.File f = new java.io.File(Globals.TARGET_BASE_MINDMAP_PATH + enrFileName);
//                                        if (f.exists()) {
//                                            f.delete();
//                                        }

                                      //}
                                        String query = "insert into enrichments(BID, pageNO, Title, Type, Exported, Path,categoryID) values ('" + getBookIDFromStoreId(enrStoreBookId) + "', '" + page + "', '" + title + "', '" + Globals.OBJTYPE_MINDMAPTAB + "', '10', '" + title + "','0')";
                                        ((CloudEnrichDetails) ctx).db.executeQuery(query);
                                    }
                                }catch (Exception e){

                                }

                            }
                }
                if(selectedCloudEnrList.size()>selectedCloudEnrPosition) {
                    HashMap<String, String> selectedMap = selectedCloudEnrList.get(selectedCloudEnrPosition);
                    String enrID = selectedMap.get(WebService.KEY_CENRICHMENTS);
                    DatabaseHandler db = DatabaseHandler.getInstance(ctx);
                    db.executeQuery("insert into CloudObjects(objID)values('" + enrID + "')");
                    if (cloudEnrList != null) {
                        cloudEnrList.remove(selectedMap);
                    }
                    if (ctx instanceof BookViewReadActivity) {
                        ((BookViewReadActivity) ctx).webService.cloudEnrList.remove(selectedMap);
                    } else if (ctx instanceof CloudEnrichDetails) {
                        String type = null;
                        // type = jsonObj.getString("type");
//                        if (type.equals("Enrichment")) {
                            for(int i=0;i< ((CloudEnrichDetails) ctx).enrich_Resource.size();i++){
                                CloudData data=((CloudEnrichDetails) ctx).enrich_Resource.get(i);
                                if(data.getMap().equals(selectedMap)){
                                    ((CloudEnrichDetails) ctx).enrich_Resource.remove(i);
                                }
                            }

                        ((CloudEnrichDetails) ctx).lv.invalidateViews();
                    }
                }
            }

            if (selectedCloudEnrPosition == selectedCloudEnrList.size()-1 ) {
                selectedCloudEnrList.clear();
                if (loginAdapter != null) {
                    loginAdapter.listViewSelectEnrRes.invalidateViews();
                    loginAdapter.txtCloudShare.setText(String.valueOf(cloudEnrList.size()));
                }
                if (ctx instanceof BookViewReadActivity) {
                    ((BookViewReadActivity) ctx).enrResDownload.popoverView.dissmissPopover(true);
                    ((BookViewReadActivity) ctx).enrResDownload.updateBtnEnrResCount();
                }
                syncProgressDialog.dismiss();
            }
            if (downloadProgDialog != null) {
                downloadProgDialog.dismiss();
            }
        }

        private  void  enrichmnetDetails(Context ctx, String xml, String enrStoreBookId, String sharedBy, String enrFileToPath, String enrMasterFileName, String contentXml){
            // String contentXml=  jsonObj.getString("ContentXMl");
            Document doc=null;
            DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(xml));
                doc = db.parse(is);
                //NodeList nl = doc.getElementsByTagName("MindMaps");
                Node pagesNode = doc.getElementsByTagName("Enrichments").item(0);

                NodeList pagesnodesList = pagesNode.getChildNodes();
                String BookTitle = null;
                String page = null;
                for (int i = 0; i < pagesnodesList.getLength(); i++) {
                    // Node hhj=pagesnodesList.item(i);
                    Node element = pagesnodesList.item(i);
                    NamedNodeMap attribute = element.getAttributes();
                    Node pageNo = attribute.getNamedItem("IDPage");
                    Node nodetitle = attribute.getNamedItem("bookTitle");
                    Node fileName = attribute.getNamedItem("file");
                    Node bookId=attribute.getNamedItem("IDBook");
                    Node Title=attribute.getNamedItem("Title");
                    Node EnrichmentID=attribute.getNamedItem("EnrichmentID");
                    BookTitle = nodetitle.getNodeValue();
                    page = pageNo.getNodeValue();

                    Enrichments enrichment = new Enrichments(ctx);
                    enrichment.setEnrichmentId(Integer.parseInt(EnrichmentID.getNodeValue()));
                    enrichment.setEnrichmentPageNo(Integer.parseInt(pageNo.getNodeValue()));
                    enrichment.setEnrichmentTitle(Title.getNodeValue());
                    enrichment.setEnrichmentBookTitle(nodetitle.getNodeValue());
                    enrichment.setEnrichmentStoreBookId(enrStoreBookId);
                    enrichment.setEnrichmentBid(getBookIDFromStoreId(enrStoreBookId));
                    enrichment.setEnrichmentEmailId(sharedBy);

                    String dateString = UserFunctions.getCurrentDateTimeInString();
                    String zipFile = enrFileToPath + enrMasterFileName + "/" + fileName.getNodeValue();
                    String unzipLoc = bookDir + "Enrichments/Page" + pageNo.getNodeValue() + "/Enriched" +  enrichment.getEnrichmentId() + "_" + dateString + "/";
                    Boolean unzipFileSuccess = Zip.unzipDownloadfileNew(zipFile, unzipLoc);
                    if (unzipFileSuccess) {
                        String deleteDir = enrFileToPath + enrMasterFileName + "/";
                        UserFunctions.DeleteDirectory(new java.io.File(deleteDir));
                        UserFunctions.traverseAndMakeFileWorldReadable(new java.io.File(bookDir + "Enrichments"));
                        String elePath = unzipLoc + fileName.getNodeValue().replace(".zip", "") + "/index.htm";
                        enrichment.createDownloadEnrichmentEntryInBookXmlForGdrive(elePath);
                        enrichment.checkAndUpdateDbForUpdateRecords(elePath);
                    }
                }

            }catch(Exception e){

            }

        }
        private int getBookIDFromStoreId(String bookStoreId){
            for (int i = 0; i<bookList.size(); i++) {
                Book book = (Book) bookList.get(i);
                if (book.is_bStoreBook() && book.get_bStoreID().equals(bookStoreId)) {
                    return book.getBookID();
                }
            }
            return 0;
        }
    }



    public void startDownload(RenderHTMLObjects webView) {
        //((SlideMenuWithActivityGroup)context).gridView=gridview;
        //((SlideMenuWithActivityGroup)context).gridShelf=Shelf;
        webview=webView;
        String book[]=bookDetails.split("#");
       // layout_view=view;
        EBookIDCode = book[1];
        BookUrl=book[2];
        //shelfPopWindow.dismiss();
        //if(!rootedDevice){
        downloadProgDialog = new ProgressDialog(context);
        downloadProgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        //if(language == 1){
        downloadProgDialog.setTitle(context.getResources().getString(R.string.downloading));
			/*}
			else{
				downloadProgDialog.setTitle("√ò¬¨√ò¬ß√ò¬±√ô≈† √ò¬ß√ô‚Äû√ò¬™√ò¬≠√ô‚Ä¶√ô≈†√ô‚Äû");
			}*/
        downloadProgDialog.setMessage("");
        downloadProgDialog.getWindow().setTitleColor(Color.rgb(222, 223, 222));
        downloadProgDialog.setIndeterminate(false);
        downloadProgDialog.setMax(100);
        downloadProgDialog.setCancelable(false);
        downloadProgDialog.setCanceledOnTouchOutside(false);
        //downloadProgDialog.show();
			/*SettingsDialog = new Dialog(fa_enrich);
			SettingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
			SettingsDialog.setTitle("Settings");
			SettingsDialog.setContentView(fa_enrich.getLayoutInflater().inflate(R.layout.preview_inflate, null));
			SettingsDialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.5), (int)(Globals.getDeviceHeight()/1.5));*/
        download();
    }
    public void download() {
        //String BookDownloadPath ="/sdcard/Manahij/"+BookID;
        ////System.out.println("BookDownloadPath:"+BookDownloadPath);
        //com.semanoor.manahij.ManahijActivity.shelfDestroy=0;			//Called this to delete the contents of country, university, categories array's etc..
        ////System.out.println("shelfdestroy in download: "+ com.semanoor.manahij.ManahijActivity.shelfDestroy);
        //int updateDValue = 0;			 //Changes after merge
        //StoreDatabase.updateEnrichStoreDestroy(updateDValue);
        EWhenDownloadBook = 1;
        /***************************************************************/
        Boolean internalStorageAvailability = BookStorage.checkInternalStorageAvailability();
		/*Boolean internal;
			if(sampleTest==0){
			 internal = true;
			 sampleTest++;
			}else{
				internal = false;
			}
			//Boolean internal = false;*/
        if (internalStorageAvailability) {
            //String internalPath = Environment.getDataDirectory().getPath()+"/"+BookID+"Book";
            //Store to internal memory
            downloadStatus = 1;

            new progressDownload().execute(BookUrl);
        } else {
            try {
                //changes for fixing crash issue (try/catch)
                Toast.makeText(context.getApplicationContext(), R.string.storage_full, Toast.LENGTH_SHORT).show();
            } catch (RuntimeException e) {
            }
        }
        ////System.out.println("dismiss popupwindow");
    }

    private class progressDownload extends AsyncTask<String, Integer, String> {
        public String DprogressString = "";
        public Boolean downloadSuccess;

        protected void onPreExecute() {
            super.onPreExecute();
            ////System.out.println("OnPreExecute downloading In internal Storage");
            downloadProgDialog.show();
        }

        @Override
        protected String doInBackground(String... downloadurl) {

            final int TIMEOUT_CONNECTION = 5000;//5sec
            final int TIMEOUT_SOCKET = 30000;//30sec
            long bytes = 1024 * 1024;
            try {
                URL url = new URL(downloadurl[0]);
                //Open a connection to that URL.
                URLConnection ucon = url.openConnection();
                ucon.connect();
                int fileLength = ucon.getContentLength();
                ////System.out.println("filelength:"+fileLength);
                long startTime = System.currentTimeMillis();

                //this timeout affects how long it takes for the app to realize there's a connection problem
                ucon.setReadTimeout(TIMEOUT_CONNECTION);
                ucon.setConnectTimeout(TIMEOUT_SOCKET);
                //Define InputStreams to read from the URLConnection.
                // uses 3KB download buffer
                InputStream is = ucon.getInputStream();
                BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);

                // new File(file).delete();
                //Internal storage
                //FileOutputStream outStream = new FileOutputStream(DownloadPath);
                String bookPath = EBookIDCode;
                FileOutputStream outStream = context.openFileOutput(bookPath, context.MODE_PRIVATE);
                byte[] buff = new byte[5 * 1024];
                //Read bytes (and store them) until there is nothing more to read(-1)
                int len;
                String totalMB, fileLengthMB;
                long total = 0;
                DecimalFormat twoDformat = new DecimalFormat("#.##");
                while ((len = inStream.read(buff)) != -1) {
                    total += len;
                    //////System.out.println("total:"+total);
                    totalMB = twoDformat.format((float) total / bytes);
                    fileLengthMB = twoDformat.format((float) fileLength / bytes);
                    DprogressString = totalMB + " MB" + " of " + fileLengthMB + " MB";
                    publishProgress((int) ((total * 100) / fileLength));
                    outStream.write(buff, 0, len);
                }
                //clean up
                outStream.flush();
                outStream.close();
                inStream.close();
                downloadSuccess = true;
                downloadStatus = 0;
                ////System.out.println( "download completed in "+ ((System.currentTimeMillis() - startTime) / 1000)+ " sec");

            } catch (IOException e) {
                ////System.out.println("error while storing:"+e);
                downloadSuccess = false;
                downloadStatus = 0;
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            //////System.out.println("OnprogressUpdate:"+progress[0]);
            downloadProgDialog.setMessage(DprogressString);
            downloadProgDialog.setProgress(progress[0]);

        }

        protected void onPostExecute(String result) {
            ////System.out.println("OnpostExecute");
            if (downloadSuccess) {
                String bookpath = EBookIDCode;   //Changes after merge

                // String book="M".concat(EBookIDCode);
                //book=EBookIDCode.replaceFirst(EBookIDCode, book);
                ////System.out.println("book"+book);

                String zipfile = Globals.TARGET_BASE_FILE_PATH.concat(bookpath);
                String unzippathWithSlash = EBookIDCode.concat("Book/");
                //String unzippathWithOutSlash=bookpath.concat("Book");
                //File file1 = fa_enrich.getDir(unzippathWithOutSlash, Context.MODE_PRIVATE);
                String unziplocation = Globals.TARGET_BASE_BOOKS_DIR_PATH.concat(unzippathWithSlash); //"/" is a must here..
                boolean unzipSuccess = unzipDownloadfileNew(zipfile, unziplocation); //if unzipping success then need to store in database:
                if (unzipSuccess) {
                }
                //RenderHTMLObjects webView = (RenderHTMLObjects) layout_view.findViewById(R.id.webView1);
                setUpProgressDialogForProcessingPages();
                // addNewStoreBook(String bTitle, String bDescription, String bAuthor, int bTemplate, int bTotalPages, int bPageBgValue, int bOrientation, Boolean isStoreBook, int lastViewedPage, String bStoreID, int categoryId, String lSearch, String lBookmark, String lCopy, String lFlip, String lNavigation, String lHighlight, String lNote, String lGoToPage, String lIndexPage, String lZoom, String lWebSearch, String lWikiSearch, String lUpdateCounts, String lGoogle, String lYoutube, String lNoor, String lNoorBook, String lYahoo, String lBing, String lAsk, String lJazira, String lBookLanguage, String lTranslationSearch, String lDictionarySearch, String lBookSearch, String isDownloadCompleted, String downloadURL, String isEditable, String price, String imgURL, String purchaseType)
                String book[] = bookDetails.split("#");
                Book newBook = ((SlideMenuWithActivityGroup) context).gridShelf.addNewStoreBook(book[3], book[4], "Semanoor", Integer.parseInt(book[5]), Integer.parseInt(book[6]), 0, Globals.portrait, true, 0, book[7], Integer.parseInt(book[8]), "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "1", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", book[9], "yes", "yes", "yes", book[10], book[11], book[12], book[13], book[14], book[15],book[16],"0");
                //    bookDetails =enrichmentNo+"#"+storeBookId +"#"+bookUrl+"#"+ bookTitle + "#"+descritpion+"#"+ template + "#"+ totalPages + "#"+jsonObject1.getString("storeID")+"#"+categoryId+"#"+                                                                                                                                                                             language + "#" +jsonObject1.getString("downloadCompleted")+"#"+bookUrl+"#"+jsonObject1.getString("bookEditable")+"#"+jsonObject1.getString("price")+"#"+imageUrl + "#"+"free";
                // fa_enrich.downloadedBooksArray.add(bookDetails[0]);
                ((SlideMenuWithActivityGroup) context).gridview.invalidateViews();
                String storename = newBook.get_bStoreID();
                String sourceDir;
                String freeFilesPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.getBookID() + "/FreeFiles/";
                if (storename.contains("M")) {
                    sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/Card.gif";
                    UserFunctions.makeFileWorldReadable(Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/");
                    if (new java.io.File(sourceDir).exists()) {
                        coverImageForStoreBook(sourceDir, freeFilesPath);
                    } else {
                        new saveImage(book[14], sourceDir, freeFilesPath).execute();
                    }
                    webview.setInitialValues1(book[1] + "Book", Integer.parseInt(book[6]), newBook, ((SlideMenuWithActivityGroup) context));
                    webview.processHtmlPages();
                }
                DownloadCounts.downloadCountsWebservice(EBookIDCode); //To call webservice download counts webservice.
                //Need to delete the zip file which is download
                java.io.File fileDir = ((SlideMenuWithActivityGroup) context).getFilesDir();   //Deleting zip file in the internal storage
                java.io.File zipfilelocation = new java.io.File(fileDir, EBookIDCode);
                // File file =  new File(zipfile);
                try {
                    zipfilelocation.delete();
                } catch (Exception e) {
                    ////System.out.println("Failed to deletefile with Error "+e);
                }
            }
        }

    }

    public class saveImage extends AsyncTask<Void, Void, Void> {
        String sourcePath, destPath;
        String freeFilesPath;

        public saveImage(String sourcePath, String destPath, String freeFilesPath) {
            this.sourcePath = sourcePath;
            this.destPath = destPath;
            this.freeFilesPath = freeFilesPath;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                URL url = new URL(sourcePath);
                Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                UserFunctions.saveBitmapImage(bitmap, destPath);
                String card_path = freeFilesPath + "card.png";
                if (!new java.io.File(card_path).exists()) {
                    Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(200, context), Globals.getDeviceIndependentPixels(300, context), true);
                    UserFunctions.saveBitmapImage(cardBitmap, card_path);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(context instanceof MainActivity) {
                ((MainActivity) context).gridView.invalidateViews();
            }
        }

    }

    public void coverImageForStoreBook(String bookImagePath, String freeFilesPath) {
        if(new java.io.File(bookImagePath).exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(bookImagePath);
            String card_path = freeFilesPath + "card.png";
            if (!new java.io.File(card_path).exists()) {
                Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(200, context), Globals.getDeviceIndependentPixels(300, context), true);
                UserFunctions.saveBitmapImage(cardBitmap, card_path);
            }
        }
//        if (processDialog != null) {
//            processDialog.dismiss();
//        }
    }
    private void setUpProgressDialogForProcessingPages() {
        ((SlideMenuWithActivityGroup)context).processPageProgDialog = new ProgressDialog(context);
        ((SlideMenuWithActivityGroup)context).processPageProgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        ((SlideMenuWithActivityGroup)context).processPageProgDialog.setTitle("Processing Files ...");
        ((SlideMenuWithActivityGroup)context).processPageProgDialog.setMessage("");
        ((SlideMenuWithActivityGroup)context).processPageProgDialog.getWindow().setTitleColor(Color.rgb(222, 223, 222));
        ((SlideMenuWithActivityGroup)context).processPageProgDialog.setIndeterminate(false);
        ((SlideMenuWithActivityGroup)context).processPageProgDialog.setMax(100);
        ((SlideMenuWithActivityGroup)context).processPageProgDialog.setCancelable(false);
        ((SlideMenuWithActivityGroup)context).processPageProgDialog.setCanceledOnTouchOutside(false);
        ((SlideMenuWithActivityGroup)context).processPageProgDialog.show();
    }
    private boolean unzipDownloadfileNew(String zipfilepath, String unziplocation) {
        //String eleessonPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/SboookAuthor/normalbooks1/";
        ZipFile zipFile = null;
        List<FileHeader> headers = null;
        try {
            zipFile = new ZipFile(new java.io.File(zipfilepath));
            headers = zipFile.getFileHeaders();
        } catch (ZipException e) {
            e.printStackTrace();
            if (downloadProgDialog != null &&downloadProgDialog.isShowing()) {
                downloadProgDialog.dismiss();
            }
            downloadProgDialog = null;
        }

        if (headers != null) {
            for (int i = 0; i < headers.size(); i++) {
                FileHeader header = headers.get(i);
                header.setFileName(header.getFileName().replace("\\", "//"));
                //File  file = new File(eleessonPath+header.getFileName());
                try {
                    zipFile.extractFile(header, unziplocation);
                } catch (ZipException e) {
                    e.printStackTrace();

                    if(downloadProgDialog != null &&downloadProgDialog.isShowing()) {
                        downloadProgDialog.dismiss();
                    }
                    downloadProgDialog = null;
                }

            }
        }
        if (downloadProgDialog != null &&downloadProgDialog.isShowing()) {
            downloadProgDialog.dismiss();
        }
        downloadProgDialog = null;
        return true;
    }
    public void downloadBook(){

        ////System.out.println("currentBook: "+CurrentBook);
        String[] BookDetails = bookDetails.split("#");
        String BookID = BookDetails[0];

        EWhenDownloadBook = 1;
        /***************************************************************/
        Boolean internalStorageAvailability = BookStorage.checkInternalStorageAvailability();
        if (internalStorageAvailability) {
            downloadStatus = 1;
            EBookIDCode = BookID;
            String isDownloadCompleted = "downloading";
            String book[]=bookDetails.split("#");
            Book newBook=null;
            if(context instanceof CloudEnrichDetails) {
                 newBook = ((CloudEnrichDetails) context).gridShelf.addNewStoreBook(book[3], book[4], "Semanoor", Integer.parseInt(book[5]), Integer.parseInt(book[6]), 0, Globals.portrait, true, 0, book[7], Integer.parseInt(book[8]), "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "1", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", book[9], "yes", "yes", "yes", isDownloadCompleted, book[11], book[12], book[13], book[14], book[15],book[16],"0");
            }else  if(context instanceof MainActivity) {
                newBook = ((MainActivity) context).gridShelf.addNewStoreBook(book[3], book[4], "Semanoor", Integer.parseInt(book[5]), Integer.parseInt(book[6]), 0, Globals.portrait, true, 0, book[7], Integer.parseInt(book[8]), "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "1", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", book[9], "yes", "yes", "yes", isDownloadCompleted, book[11], book[12], book[13], book[14], book[15], book[16],"0");
            } else if(context instanceof BookViewActivity) {
                newBook = ((BookViewActivity) context).gridShelf.addNewStoreBook(book[3], book[4], "Semanoor", Integer.parseInt(book[5]), Integer.parseInt(book[6]), 0, Globals.portrait, true, 0, book[7], Integer.parseInt(book[8]), "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "1", "yes", "yes", "yes", "yes", "yes", "yes", "yes", "yes", book[9], "yes", "yes", "yes", isDownloadCompleted, book[11], book[12], book[13], book[14], book[15],book[16],"0");
            }
            String sourceDir;
            String freeFilesPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.getBookID() + "/FreeFiles/";
            sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/Card.gif";
            new saveImage(book[6], sourceDir, freeFilesPath).execute();
            final DownloadBackground background= DownloadBackground.getInstance();

            if(mMain.equals("MQTT")) {
                callBacktoMain(book[7]);
            }
            ArrayList<Book> downloadArray;
            if (background.getList().size()>0){
                downloadArray = background.getList();
                downloadArray.add(newBook);
                background.setList(downloadArray);
            }else{
                downloadArray = new ArrayList<>();
                downloadArray.add(newBook);
                background.setDownLoadBook(newBook);
                background.setList(downloadArray);
                background.setTextCircularProgressBar(null);
                background.setContext(context);
                background.setDownloadTaskRunning(true);
                background.new downloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,newBook.getDownloadURL());
            }
        }else {
            try {
                //changes for fixing crash issue (try/catch)
                Toast.makeText(context.getApplicationContext(), R.string.storage_full, Toast.LENGTH_SHORT).show();
            } catch (RuntimeException e) {
            }
        }
    }
    public void callBacktoMain(String bookID){
        Intent intent = new Intent();
        intent.setAction(MainActivity.MyBroadcastReceiver.ACTION);
        intent.putExtra("bookId",bookID);
        context.sendBroadcast(intent);
    }
    public void saveBookDetails(String bookContent, String downloadFromPath, String jsonContent, ArrayList<HashMap<String, String>> selectedCloudEnrList, String type) {
        Book newBook = null;
        int lastBookIndex;
        Book lastBook;
        String templateId = null,showpageNo= null,title= null,lastViewdPage= null,isEditable= null,CID= null,totalPages= null,description= null,pageBgValue= null,bLanguage= null,orientation= null,author= null;
        Templates templates = null;
        if (((CloudBooksActivity)context).gridShelf.bookListArray.isEmpty()) {
            lastBookIndex = 0;
        } else {
            lastBook = (Book) ((CloudBooksActivity)context).gridShelf.bookListArray.get(((CloudBooksActivity)context).gridShelf.bookListArray.size() - 1);
            lastBookIndex = lastBook.getBookIndex();
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonContent);
            String bookType=jsonObj.getString("type");
            String sharedBy=jsonObj.getString("sharedBy");
            if(jsonObj.has("catID")){
                CID = jsonObj.getString("catID");
                title=jsonObj.getString("bookTitle");
            }

            //final String downloadFromPath = jsonObj.getString("downloadPath");
            if(jsonObj.has("bookDetails")) {
                final String bookDetails = jsonObj.getString("bookDetails");
                JSONObject enrJson = new JSONObject(bookDetails);
                templateId = enrJson.getString("template");
                templateId = enrJson.getString("bIndex");
                showpageNo = enrJson.getString("showPageNo");
                title = enrJson.getString("title");
                lastViewdPage = enrJson.getString("MLastViewedPage");
                isEditable = enrJson.getString("isEditable");
                CID = enrJson.getString("CID");
                totalPages = enrJson.getString("totalPages");
                description = enrJson.getString("Description");
                pageBgValue = enrJson.getString("PageBGValue");
                bLanguage = enrJson.getString("blanguage");
                orientation = enrJson.getString("oriantation");
                author = enrJson.getString("Author");
                templates = ((CloudBooksActivity)context).gridShelf.getTemplates(Integer.parseInt(templateId), Integer.parseInt(orientation),context);
            }
            String bStoreID = null;


            boolean isStoreBookCreatedFromTab = false;
            boolean isStoreBook = false;
            boolean showPageNo = false;

            //mindmap  query
            if(type.equals("Mindmap")) {
                author=sharedBy;
                templateId="6";
                orientation="0";
                lastViewdPage="0";
                CID =String.valueOf(Globals.mindMapCatId);
                bLanguage = "English";
                isEditable="no";
                description="MindMap";
                totalPages="1";
                int mindmapBook = ((CloudBooksActivity) context).db.getMaxUniqueRowID("books");
                bStoreID="I"+mindmapBook + 1;
                ((CloudBooksActivity) context).db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,storeID,ShowPageNo,CID,blanguage,StoreBookCreatedFromIpad,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,clientID,bookDirection) " +
                        "values('" + title + "','" + description + "','" + sharedBy + "','" +"0"+ "','" + Integer.parseInt(totalPages) + "','0','" + Integer.parseInt(orientation) + "','" + lastBookIndex + 1 + "','" + isStoreBook + "','" + Integer.parseInt(lastViewdPage) + "','I"+ mindmapBook + 1 + "','no','" + Integer.parseInt(CID) + "','" + bLanguage + "','no','downloading','" + downloadFromPath + "','" + isEditable + "','no','0.00','','created','0','ltr')");
                ((CloudBooksActivity) context).db.executeQuery("update tblCategory set isHidden='false' where CID='"+Integer.parseInt(CID)+"'and isHidden='true'");
            }else if(type.equals("sharedBook")) {
                // normalBook query
                ((CloudBooksActivity) context).db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,ShowPageNo,CID,StoreBookCreatedFromIpad,blanguage,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,clientID,bookDirection) values('" + title + "','" + description + "','" + author + "','" + Integer.parseInt(templateId) + "','" + Integer.parseInt(totalPages) + "','" + "0" + "','" + Integer.parseInt(orientation) + "','" + lastBookIndex + 1 + "','" + isStoreBook + "','" + Integer.parseInt(lastViewdPage) + "','" + false + "','" + Integer.parseInt(CID) + "','" + "no" + "', '" + bLanguage + "','downloading','" + downloadFromPath + "','" + isEditable + "','no','0.00','','created','0','ltr')");
                ((CloudBooksActivity) context).db.executeQuery("update tblCategory set isHidden='false' where CID='"+Integer.parseInt(CID)+"'and isHidden='true'");

            }else if(type.equals("Album")) {
                author=sharedBy;
                templateId="0";
                orientation="0";
                lastViewdPage="0";
                bLanguage = "English";
                isEditable="no";
                totalPages="1";
                CID=String.valueOf(Globals.myMediaCatId);
                description="MyMedia";
                int mindmapBook = ((CloudBooksActivity) context).db.getMaxUniqueRowID("books");
                bStoreID="A"+mindmapBook + 1;
                // ((CloudBooksActivity) context).db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,ShowPageNo,CID,StoreBookCreatedFromIpad,blanguage,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,clientID) values('" + title + "','" + description + "','" + author + "','" + Integer.parseInt(templateId) + "','" + Integer.parseInt(totalPages) + "','" + "0" + "','" + Integer.parseInt(orientation) + "','" + lastBookIndex + 1 + "','" + isStoreBook + "','" + Integer.parseInt(lastViewdPage) + "','" + false + "','" + Integer.parseInt(CID) + "','" + "no" + "', '" + bLanguage + "','downloading','" + downloadFromPath + "','" + isEditable + "','no','0.00','','created','0')");
                 ((CloudBooksActivity) context).db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,storeID,ShowPageNo,CID,blanguage,StoreBookCreatedFromIpad,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,clientID,bookDirection) " +
                        "values('" + title + "','" + description + "','" + sharedBy + "','" +"0" + "','" + 1+ "','0','" + Integer.parseInt(orientation) + "','" + lastBookIndex + 1 + "','" + isStoreBook + "','" + Integer.parseInt(lastViewdPage) + "','A"+ mindmapBook + 1 + "','no','" + Integer.parseInt(CID) + "','" + bLanguage + "','no','downloading','" + downloadFromPath + "','" + isEditable + "','no','0.00','','created','0','ltr')");
                ((CloudBooksActivity) context).db.executeQuery("update tblCategory set isHidden='false' where CID='"+Integer.parseInt(CID)+"'and isHidden='true'");

            }
            int bookId = ((CloudBooksActivity)context).db.getMaxUniqueRowID("books");
            // lastBookId = bookId;
            newBook = new Book(bookId, title, description, author, Integer.parseInt(templateId), Integer.parseInt(totalPages), 0, Integer.parseInt(orientation), lastBookIndex + 1, false, Integer.parseInt(lastViewdPage), bStoreID, null, false, null, null, templates, isStoreBookCreatedFromTab, Integer.parseInt(CID), bLanguage, "downloading", downloadFromPath, isEditable, "no", "0.00", "", "created",0,"0","ltr");
            ((CloudBooksActivity)context).gridShelf.bookListArray.add(0, newBook);
            ((CloudBooksActivity)context).gridShelf.setNoOfBooks(((CloudBooksActivity)context).gridShelf.bookListArray.size());
            ((CloudBooksActivity)context).background=DownloadBackground.getInstance();
            final ArrayList<Book> downloadArray;
            final ArrayList<String> Stringlist;

            if (((CloudBooksActivity)context).background.getList().size()>0){           //not enter
                downloadArray = ((CloudBooksActivity)context).background.getList();
                Stringlist= ((CloudBooksActivity)context).background.getString();
                ((CloudBooksActivity)context).background.setSelectedCloudEnrList(selectedCloudEnrList);
                downloadArray.add(newBook);
                Stringlist.add(jsonContent);
                ((CloudBooksActivity)context).background.setList(downloadArray);
                ((CloudBooksActivity)context).background.setString(Stringlist);
            }else {
                downloadArray = new ArrayList<>();
                Stringlist = new ArrayList<>();
                downloadArray.add(newBook);
                Stringlist.add(jsonContent);
                ((CloudBooksActivity)context).background.setDownLoadBook(newBook);
                ((CloudBooksActivity)context).background.setList(downloadArray);
                ((CloudBooksActivity)context).background.setString(Stringlist);
                ((CloudBooksActivity)context).background.setSelectedCloudEnrList(selectedCloudEnrList);
                ((CloudBooksActivity)context).background.setTextCircularProgressBar(null);
                ((CloudBooksActivity)context).background.setContext(((CloudBooksActivity)context));
                ((CloudBooksActivity)context).background.setDownloadTaskRunning(true);
                ((CloudBooksActivity)context).background.new downloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, newBook.getDownloadURL());

            }
            } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    public String replaceExistingImages(String srcPath, int objUniqueId, MainActivity context, String cont, int fromId) {
        String content = cont;
        String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + fromId + "/" + "images/";

        java.io.File srcDir = new java.io.File(srcPath);
        java.io.File destDir = new java.io.File(srcPath);
        //String imgPath ="q"+sectionId+"_img_"+uniqueId+""+".png";
        String filePath = null;
        int sectionId = 0, prevSectionId = 0, uniqueId = 0;
        String[] path = srcDir.list();
        for (String file : path) {
            if (file.contains("q")) {

                if (file.contains("_img_a_")) {
                    context.db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                    sectionId = context.db.getMaxUniqueRowID("tblMultiQuiz");
                    String[] split = file.split("_img_a_");
                    String s = split[0].replace("q","");
                    String[] p = split[1].split("\\.");
                    uniqueId = Integer.parseInt(p[0]);
                    prevSectionId = Integer.parseInt(s);

                    filePath = "q" + sectionId + "_img_a_" + objUniqueId + "" + ".png";

                } else if (file.contains("_img_b_")) {
                    filePath = "q" + sectionId + "_img_b_" + objUniqueId + "" + ".png";

                } else if (file.contains("_img_c_")) {
                    filePath = "q" + sectionId + "_img_c_" + objUniqueId + "" + ".png";
                } else if (file.contains("_img_d_")) {
                    filePath = "q" + sectionId + "_img_d_" + objUniqueId + "" + ".png";
                } else if (file.contains("_img_e_")) {
                    filePath = "q" + sectionId + "_img_e_" + objUniqueId + "" + ".png";
                } else if (file.contains("_img_f_")) {
                    filePath = "q" + sectionId + "_img_f_" + objUniqueId + "" + ".png";
                }else if (file.contains("_img_")) {
                    String[] split = file.split("_img_");
                    String s = split[0].replace("q","");
                    String[] p = split[1].split("\\.");
                    uniqueId = Integer.parseInt(p[0]);
                    prevSectionId = Integer.parseInt(s);

                    context.db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                    sectionId = context.db.getMaxUniqueRowID("tblMultiQuiz");

                    filePath = "q" + sectionId + "_img_" + objUniqueId + "" + ".png";
                }

                String fromPath = srcDir + "/" + file;
                String toCopyPath = destDir + "/" + filePath;

                java.io.File ExistingPath = new java.io.File(fromPath);
                java.io.File newPath = new java.io.File(toCopyPath);
                ExistingPath.renameTo(newPath);
                if (content.contains(imgDir)) {
                    content = content.replace(imgDir, srcPath);
                }
                if(content.contains(prevSectionId+"")||content.contains(uniqueId+"")) {
                    //content = content.replace(prevSectionId + "", sectionId + "");
                    //content = content.replace(uniqueId + "", objUniqueId + "");
                    //copyFiles(fromPath,toCopyPath);
                    content = content.replace(prevSectionId+"_"+uniqueId,sectionId+"_"+objUniqueId);
                    content = content.replace("_"+uniqueId+"_"+prevSectionId,"_"+objUniqueId+"_"+sectionId);
                    content = content.replace("q"+prevSectionId+"_result_"+uniqueId,"q"+sectionId+"_result_"+objUniqueId);
                    content = content.replace("q"+prevSectionId,"q"+sectionId);
                    content = content.replace("_"+uniqueId,"_"+objUniqueId);
                    content = content.replace("'btnCheckAnswer"+prevSectionId,"'btnCheckAnswer"+sectionId);
                    content = content.replace("Title"+prevSectionId,"Title"+sectionId);
                    content = content.replace("_"+uniqueId+":"+prevSectionId,"_"+objUniqueId+":"+sectionId);

                }
            }

        }
        return content;

    }
    private String getDriveFolderId(String folderName){
        ArrayList<File> filesList = new ArrayList<>();
        filesList = search("root", "Nooor", null);
        if (filesList.size() > 0) {
            for (File file : filesList) {
                ArrayList<File> fileArrayList = search(file.getId(), folderName, null);
                if (fileArrayList.size()>0 && checkFolderExist(fileArrayList,folderName)) {
                    for (File file1 : fileArrayList) {
                        if (file1.getTitle().equals(folderName)) {
                            return file1.getId();
                        }else{
                            return "";
                        }
                    }
                }else{
                    File createCloudFile = createFolder(file.getId(), folderName);
                    return createCloudFile.getId();
                }
                break;
            }
        } else {
            File createFile = createFolder(null, "Nooor");
            File createCloudFile = createFolder(createFile.getId(), folderName);
            return createCloudFile.getId();
        }
        return "";
    }
    private boolean checkFolderExist(ArrayList<File> fileList,String folderName){
        for (File file1 : fileList) {
            if (file1.getTitle().equals(folderName)) {
                return true;
            }
        }
        return false;
    }
    private ArrayList<File> search(String prnId, String titl, String mime) {
        ArrayList<File> gfs = new ArrayList<>();
        if (drive != null) try {
            // add query conditions, build query
            String qryClause = "'me' in owners and ";
            if (prnId != null) qryClause += "'" + prnId + "' in parents and ";
            if (titl != null) qryClause += "title = '" + titl + "' and ";
            if (mime != null) qryClause += "mimeType = '" + mime + "' and ";
            qryClause = qryClause.substring(0, qryClause.length() - " and ".length());
            Drive.Files.List qry = drive.files().list().setQ(qryClause);
            String npTok = null;
            if (qry != null) do {
                FileList gLst = qry.execute();
                if (gLst != null) {
                    for (File gFl : gLst.getItems()) {
                        if (gFl.getLabels().getTrashed()) continue;
                        gfs.add(gFl);
                    }                                                                 //else UT.lg("failed " + gFl.getTitle());
                    npTok = gLst.getNextPageToken();
                    qry.setPageToken(npTok);
                }
            } while (npTok != null && npTok.length() > 0);                     //UT.lg("found " + vlss.size());
        } catch (Exception e) { UT.le(e); }
        return gfs;
    }

    /************************************************************************************************
     * create file/folder in GOODrive
     * @param prnId  parent's ID, (null or "root") for root
     * @param titl  file name
     * @return      file id  / null on fail
     */
    private File createFolder(String prnId, String titl) {
        String rsId = null;
        File meta = null;
        File gFl = null;
        if (drive != null && titl != null) try {
            meta= new File();
            meta.setParents(Collections.singletonList(new ParentReference().setId(prnId == null ? "root" : prnId)));
            meta.setTitle(titl);
            meta.setMimeType(UT.MIME_FLDR);

            try { gFl = drive.files().insert(meta).execute();
            } catch (Exception e) { UT.le(e); }
            if (gFl != null && gFl.getId() != null) {
                rsId = gFl.getId();
            }
            meta.setId(rsId);
        } catch (Exception e) { UT.le(e); }
        return gFl;
    }

    /************************************************************************************************
     * create file/folder in GOODrive
     * @param prnId  parent's ID, (null or "root") for root
     * @param titl  file name
     * @param mime  file mime type
     * @param file  file (with content) to create
     * @return      file id  / null on fail
     */
    private Drive.Files.Insert createFile(String prnId, String titl, String mime, java.io.File file) {
        String rsId = null;
        File meta=null;
        File gFl = null;
        Drive.Files.Insert insert = null;
        if (drive != null && titl != null && mime != null && file != null) try {
            meta= new File();
            meta.setParents(Collections.singletonList(new ParentReference().setId(prnId == null ? "root" : prnId)));
            meta.setTitle(titl);
            meta.setMimeType(mime);
            insert = drive.files().insert(meta, new FileContent(mime, file));
            if (gFl != null)
                rsId = gFl.getId();
            meta.setId(rsId);
        } catch (Exception e) {
            UT.le(e);
        }
        return insert;
    }


}

