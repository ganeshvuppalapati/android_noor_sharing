package com.semanoor.manahij;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by karthik on 24-09-2016.
 */
public class Clients {

    private static Clients instance;
    private ArrayList<Object> jsonMap = new ArrayList<Object>();

    public static synchronized Clients getInstance(){
        if(instance == null){
            instance = new Clients();
        }
        return instance;
    }

    public ArrayList<Object> getJsonMap() {
        return jsonMap;
    }

    public void setJsonMap(ArrayList<Object> jsonMap) {
        this.jsonMap = jsonMap;
    }

    public void getClientsList(String downloadurl,String scopeID){
        JSONObject json;
        String jsonData;
        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        try {
            URL urlToRequest = new URL(downloadurl);
            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(25000);
            urlConnection.setRequestProperty("accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
            } else {

            }
            String returnResponse = sb.toString();
            jsonData = returnResponse;
            jsonData = jsonData.replaceAll("\\\\r\\\\n", "");
            jsonData = jsonData.replace("\"{", "{");
            jsonData = jsonData.replace("}\",", "},");
            jsonData = jsonData.replace("}\"", "}");
            createAndSaveFile(scopeID+"_clients",jsonData);
            readJsonData(scopeID+"_clients");
            System.out.println("success");

        } catch (MalformedURLException e) {

        } catch (SocketTimeoutException e) {

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }
    private Map<String,Object> toMap(JSONObject object) throws JSONException {
        Map<String,Object> map = new HashMap<String,Object>();
        Iterator<String> keysIter = object.keys();
        while (keysIter.hasNext()){
            String key = keysIter.next();
            Object value = object.get(key);
            if (value instanceof JSONArray){
                value = toList((JSONArray) value);
            }else if (value instanceof JSONObject){
                value = toMap((JSONObject)value);
            }
            map.put(key,value);
        }
        return map;
    }
    private List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0;i<array.length();i++){
            Object value = array.get(i);
            if (value instanceof JSONArray){
                value = toList((JSONArray) value);
            }else if (value instanceof JSONObject){
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
    public void createAndSaveFile(String fileName,String jsonResponse){
        File newFile = new File(Globals.TARGET_BASE_FILE_PATH+"clients");
        if (newFile.exists()){
            File[] files = newFile.listFiles();
            if (files.length>0){
                for (int i=0;i<files.length;i++){
                    files[i].delete();
                }
            }
        }else{
            newFile.mkdir();
        }
        try{
            FileWriter file = new FileWriter(Globals.TARGET_BASE_FILE_PATH+"clients/"+fileName+".json");
            file.write(jsonResponse);
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String readJsonData(String fileName){
        JSONArray jsonArray;
        String str = "";
        try {
            File file = new File(Globals.TARGET_BASE_FILE_PATH+"clients/"+ fileName + ".json");
            FileInputStream fis = new FileInputStream(file);
            int size = fis.available();
            byte[] buffer = new byte[size];
            fis.read(buffer);
            fis.close();
            String response = new String(buffer);
            str = response;
            jsonArray = new JSONArray(response);
            setJsonMap((ArrayList<Object>) toList(jsonArray));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void updateSubscriptionListToSharedPreferences(String key,HashMap<String,String> hashMap,Context context){
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreference.edit();
        ArrayList<HashMap<String,String>> existList = null;
        try {
            existList = (ArrayList<HashMap<String,String>>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<HashMap<String,String>>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (UserFunctions.checkClientIdExist(existList,hashMap.get("AppstoreID"))) {
            for (int i=0;i<existList.size();i++){
                HashMap<String,String> hashMap1 = existList.get(i);
                if (hashMap.get("AppstoreID").equalsIgnoreCase(hashMap1.get("AppstoreID"))){
                    existList.remove(i);
                    existList.add(hashMap);
                }
            }
        }else{
            existList.add(hashMap);
        }
        try {
            editor.putString(key, ObjectSerializer.serialize(existList));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        editor.commit();
    }
}