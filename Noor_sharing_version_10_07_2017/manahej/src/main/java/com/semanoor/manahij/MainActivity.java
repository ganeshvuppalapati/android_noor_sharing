package com.semanoor.manahij;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mobeta.android.dslv.DragSortController;
import com.mobeta.android.dslv.DragSortListView;
import com.ptg.mindmap.widget.MindMapActivity;
import com.ptg.mindmap.widget.MindMapPlayerActivity;
import com.ptg.views.CircleButton;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.inappbilling.util.IabResult;
import com.semanoor.inappbilling.util.Purchase;
import com.semanoor.manahij.mqtt.CallConnectionStatusService;
import com.semanoor.manahij.mqtt.MQTTSubscriptionService;
import com.semanoor.manahij.mqtt.NoorActivity;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.sboookauthor_store.RenderHTMLObjects;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.BookExportDialog;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.DownloadBackground;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.MindMapDialog;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.OldDatabaseHandler;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.SegmentedRadioGroup;
import com.semanoor.source_sboookauthor.TemplateGridListAdapter;
import com.semanoor.source_sboookauthor.Templates;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.UserGroups;
import com.semanoor.source_sboookauthor.WebService;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class MainActivity extends NoorActivity implements OnClickListener, OnCheckedChangeListener,PopoverView.PopoverViewDelegate,DragSortListView.DropListener {

	private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 100001;
	private static final int INITIAL_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 54321;

	//Instance Variables
	public DatabaseHandler db;
	public OldDatabaseHandler oldDb;
	private UserFunctions userfn;
	private Globals global;
	private Button btnNewBook, btnLang, btn_fb_login,btnAddBook,btnSearch;
	private RelativeLayout create_books_layout,create_mindmap_layout,create_webbook_layout,create_flascard_layout;
	private HorizontalScrollView add_books_layout;
	private CircleButton btnImportBook;
	private ImageView titleText;
	public Button btnEditBook;
	private Button myBookbtn,mindMapBtn,albumBtn,addShelf_btn,web_book_btn,flashcard_btn;
	private SegmentedRadioGroup btnSegmentControl;
	public DragSortListView gridView;
	public GridShelf gridShelf;
	private int currentBookClickedPosition;
	private int bookViewActivityrequestCode = 1;
	private int storeViewActivityrequestCode = 3;
	private int webBookActivityRequestCode = 9;
	private int COVER_BACKGROUND_GALLERY = 2;
	public  int CAPTURE_GALLERY_TEMPLATE = 5500;
	public int RACK_CAPTURE_GALLERY = 10;
	private int userTemplateRequestCode=5;
	private int mediaAlbumActivityRequestCode = 6;
	//private RelativeLayout shelfBgLayout;
	private Locale myLocale;
	private TemplateGridListAdapter templateGridAdapter;
	RelativeLayout rl;
	private ViewPager templatesViewPager;
	private Dialog addNewBookDialog;
	public WebService webService;
    //private ViewAdapter loginAdapter;
	private PopoverView socialPopoverView;
	public ArrayList<UserGroups> groups;
	public ArrayList<UserGroups> group_details;
	String groupname;
	boolean deletemode;
	public Enrichments enrichment;
	String language;
	boolean oldDatabasePrefs;
	Button btn_cleartext,btnArrange,btnEnrResDownload,btn_text,btn_cloud;
    EditText et_search;
	RelativeLayout et_layout,enr_relativeLayout;
	FrameLayout frame_layout,frame_layout1,cloud_layout,frame_edit_layout,frame_add_layout;
	public GestureDetector gestureDetector;

	private GoogleApiClient mGoogleApiClient;
	private boolean mIntentInProgress;
	private boolean mSignInClicked, googleSignInClicked, facebookSignInClicked;
	private Bitmap mIcon11 = null;
	private ConnectionResult mConnectionResult;
	private static final int RC_SIGN_IN = 0;
	// Profile pic image size in pixels
	private static final int PROFILE_PIC_SIZE = 400;
	private ProgressBar googleFbProgressBar;

	//private UiLifecycleHelper uiHelper;
	private String fbPhotoUrl;
	boolean openPdfReader;
	private boolean bookSearch;
	public int CAPTURE_GALLERY = 1003;
	Malzamah malzamah;
	public boolean isDeleteBookDir;
	public ImageView imgViewCover;
	public PopoverView PopoverView;
	private Uri selectedGalleryImageUri;
	boolean exist;
	public boolean editmode;
	public MatrixCursor cursor;
	public DragSortController controller;
    public SlideMenuWithActivityGroup SlideMenuActivity;
	private boolean isPressed = true;
	private Subscription subscription;
	private IabHelper mHelper;
	public RenderHTMLObjects webView;
	public HashMap<String,Integer> progress;
	public boolean isInternetExists = false;
	public DownloadBackground background;
	String themesPath =  Globals.TARGET_BASE_FILE_PATH+"Themes/current/";
	public Groups group;
	public boolean Search;
	private SharedPreferences sharedPreference;
	public ArrayList<OnlineTemplates> templatesList = new ArrayList<>();
	private boolean onlineTemplates = true;
	private String freeFilesPath = Globals.TARGET_BASE_TEMPATES_PATH;
	private int templateId;
	private int selectedPos;
	private boolean templatecreated = false;
	private ListView templatesView;
	private ImageView iv_selected;
	public static Context mContext;
	boolean isDownload=false;
	MyBroadcastReceiver  receiver ;
	Dialog callDialog ;
	int lastViewdPage;
	MQTTSubscriptionService service ;
	String bookId="";
	ManahijApp app;
	public static final String FILTER = "noor.filter";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = MainActivity.this;
		loadLocale();


		//Remove title Bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		db = DatabaseHandler.getInstance(this);

		String oldDbPath = this.getFilesDir().getParentFile().getPath() + "/databases/sbook.sqlite";
		//checking

		String packageName = BuildConfig.APPLICATION_ID;

		global = Globals.getInstance();


		setContentView(R.layout.activity_main);
		Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
		ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

		UserFunctions.changeFont(rootView,font);

		SlideMenuActivity = (SlideMenuWithActivityGroup) getIntent().getSerializableExtra("SlidemenuActivity");
		Globals.setTablet(isTablet(this));
		group = Groups.getInstance();
		service = new MQTTSubscriptionService();
		receiver = new MyBroadcastReceiver();
		/*ManahijApp */app=(ManahijApp) getApplicationContext();
		app.setVideoActivityContext(this);
		app.setState(false);
		app.setMainState(true);
		ManahijApp.getInstance().getPrefManager().addIsinBackground(false);
		callDialog = new Dialog(this);
		userfn = UserFunctions.getInstance();

		if (!Globals.isTablet()){
			String query="select count(*) from books where CID='"+2+"'";
			int Count =db.checkCategoryExist(query);
			db.executeQuery("update tblCategory set isHidden='false' where CID='1'and isHidden='true'");
			if(Count==0) {
				db.executeQuery("update tblCategory set isHidden='true' where CID='2'and isHidden='false'");
			}
		}else{
			db.executeQuery("update tblCategory set isHidden='false' where CID='5'and isHidden='true'");
		}
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		//checkAndLogin();
		//Load All System Fonts
		UserFunctions.copyFontAssetFolder(this.getAssets(), "fonts", Globals.TARGET_BASE_FONT_PATH);
		UserFunctions.loadSystemFonts();
        gestureDetector = new GestureDetector(MainActivity.this,new GestureListener());
		sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);

		//getDevice width and height and set it to global
		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = userfn.GetDeviceResolution(display);
		global.setDeviceHeight(displayMetrics.heightPixels);
		global.setDeviceWidth(displayMetrics.widthPixels);
		rl = (RelativeLayout) findViewById(R.id.shelfRootViewLayout);
		et_search = (EditText) findViewById(R.id.et_search);
		et_layout = (RelativeLayout) findViewById(R.id.et_layout);
		enr_relativeLayout = (RelativeLayout) findViewById(R.id.enrResDownloadLayout);
		frame_layout = (FrameLayout) findViewById(R.id.frame_layout);
		frame_edit_layout = (FrameLayout) findViewById(R.id.frame_arr_layout);
		frame_add_layout = (FrameLayout) findViewById(R.id.add_shelf_layout);
		cloud_layout = (FrameLayout) findViewById(R.id.cloud_layout);
		btn_cloud = (Button) findViewById(R.id.btn_cloud);
		btn_cloud.setOnClickListener(this);


		//Copy Template Folder from Asset ToFiles Dir
		if (Globals.isTablet()) {
			userfn.copyTemplatesDirFromAssetToFilesDir(this, Globals.TARGET_BASE_TEMPATES_PATH, "templates");
		}
		userfn.copyThemesFromAssetToFilesDir(this, Globals.TARGET_BASE_FILE_PATH+"Themes/", "themes");

		userfn.createNewDirectory(Globals.TARGET_BASE_FILE_PATH + "UserLibrary");

		// create Help Book when the application is installed for the first time
		if (!userfn.AppInstalled(this)) {

			UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH);
			UserFunctions.makeFileWorldReadable(Globals.TARGET_BASE_BOOKS_DIR_PATH);

		}
		if (!oldDatabasePrefs && new File(oldDbPath).exists()) {
			oldDb = OldDatabaseHandler.getInstance(this);
			new VersionUpdateDB(this, db, oldDb, userfn);
		}


		if (!(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + "commonfiles")).exists()) {
			userfn.copyAsset(this.getAssets(), "commonfiles.zip", Globals.TARGET_BASE_FILE_PATH + "commonfiles");
			userfn.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + "commonfiles");
			Boolean unzipSuccess = unZipDownloadedFile(Globals.TARGET_BASE_FILE_PATH + "commonfiles", Globals.TARGET_BASE_BOOKS_DIR_PATH + "commonfiles");
			File zipfile = new File(Globals.TARGET_BASE_FILE_PATH + "commonfiles");
			zipfile.delete();
		}

		if (!(new File(Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + "1.png")).exists()) {
			String shelfRackImgPath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH;
			File fileImgDir = new File(shelfRackImgPath);
			if (!fileImgDir.exists()) {
				fileImgDir.mkdir();
			}
			if (Globals.isLimitedVersion()){
				userfn.copyAsset(this.getAssets(), "1.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+"1.png");
				userfn.copyAsset(this.getAssets(), "2.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+"2.png");
				userfn.copyAsset(this.getAssets(), "2.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+"3.png");
				userfn.copyAsset(this.getAssets(), "3.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+"4.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+"5.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+"6.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+"7.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+"8.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+"-1.png");
			}else {
				userfn.copyAsset(this.getAssets(), "1.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + "1.png");
				userfn.copyAsset(this.getAssets(), "2.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + "2.png");
				userfn.copyAsset(this.getAssets(), "3.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + "3.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + "4.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + "5.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + "6.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + "7.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + "8.png");
				userfn.copyAsset(this.getAssets(), "4.png", Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + "-1.png");
			}
		}

		//Instantiating UIControl Elements
		create_books_layout = (RelativeLayout) findViewById(R.id.create_book_layout);
		create_mindmap_layout = (RelativeLayout) findViewById(R.id.create_mindmap_layout);
		create_webbook_layout = (RelativeLayout) findViewById(R.id.create_web_book);
		create_flascard_layout = (RelativeLayout) findViewById(R.id.create_flashcard_layout);
		flashcard_btn = (Button) findViewById(R.id.create_flashcard_btn);
		flashcard_btn.setOnClickListener(this);
		web_book_btn = (Button) findViewById(R.id.create_webbook_btn);
		web_book_btn.setOnClickListener(this);
		myBookbtn = (Button) findViewById(R.id.create_book_btn);
		myBookbtn.setOnClickListener(this);
		mindMapBtn = (Button) findViewById(R.id.create_mindmap_btn);
		mindMapBtn.setOnClickListener(this);
		albumBtn = (Button) findViewById(R.id.create_album_btn);
		albumBtn.setOnClickListener(this);
		addShelf_btn = (Button) findViewById(R.id.create_shelf_btn);
		addShelf_btn.setOnClickListener(this);
		add_books_layout = (HorizontalScrollView) findViewById(R.id.toolBarLayout);
		btnAddBook = (Button) findViewById(R.id.add_shelf);
		btnAddBook.setOnClickListener(this);
		btnNewBook = (Button) findViewById(R.id.btn_new);
		btnNewBook.setOnClickListener(this);
		btnImportBook = (CircleButton) findViewById(R.id.btn_import);
		btnImportBook.setOnClickListener(this);
		btnSegmentControl = (SegmentedRadioGroup) findViewById(R.id.btnSegmentControl);
		btnSegmentControl.setOnCheckedChangeListener(this);
		frame_layout1 = (FrameLayout) findViewById(R.id.frame_layout1);
		btn_fb_login = (Button) findViewById(R.id.btn_facebklogin);
		btn_fb_login.setOnClickListener(this);
		btnLang = (Button) findViewById(R.id.btn_lang);
		btnLang.setOnClickListener(this);
		btn_cleartext = (Button) findViewById(R.id.btn_cleartext);
		btn_cleartext.setVisibility(View.GONE);
		btnSearch = (Button) findViewById(R.id.btn_search);
		btnSearch.setOnClickListener(this);
		btnArrange = (Button) findViewById(R.id.btn_arrange);
		btnArrange.setOnClickListener(this);
		titleText = (ImageView) findViewById(R.id.txtNooorTitle);
		if (Globals.isTablet()){
			titleText.setVisibility(View.VISIBLE);
		}else{
			titleText.setVisibility(View.GONE);
		}
		webView = (RenderHTMLObjects)findViewById(R.id.webView1);
		TextView switch_txt = (TextView) findViewById(R.id.textView15);
		Switch switch1 = (Switch) findViewById(R.id.switch1);
		btnEnrResDownload = (Button) findViewById(R.id.btnEnrResDownload);
		btnEnrResDownload.setOnClickListener(this);
		btn_text= (Button) findViewById(R.id.btn_text);
		UserFunctions.applyingBackgroundImage(themesPath+"shelf_bg.png",rl, MainActivity.this);
		if (!Globals.isTablet()){
			switch1.setVisibility(View.GONE);
			switch_txt.setVisibility(View.GONE);
		}
		String bookname = et_search.getText().toString();
		//et_search.
		switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton view, boolean isChecked) {
				if (view.getId() == R.id.switch1) {
					if (isChecked) {
						bookSearch = true;

					} else {
						bookSearch = false;
					}
				}
			}
		});

		et_search.addTextChangedListener(new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.toString().length() == 0) {
					btn_cleartext.setVisibility(View.GONE);
				} else {
					btn_cleartext.setVisibility(View.VISIBLE);
				}
				if (et_search.getText().toString().equals("") && !bookSearch) {
					gridShelf.bookListArray = db.getAllBookContent(gridShelf, et_search.getText().toString());

					gridView.invalidateViews();
					et_search.setFocusableInTouchMode(false);
					et_search.setFocusable(false);
					et_search.setFocusableInTouchMode(true);
					et_search.setFocusable(true);
					et_search.clearFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(rl.getWindowToken(), 0);
				}
			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});

		btn_cleartext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				et_search.getText().clear();
				loadGridView();
			}
		});

		et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					if (!et_search.getText().toString().equals("") && !bookSearch) {
						if (group.getShelfSearchInsideBookEnable()) {
							gridShelf.bookListArray = db.getAllBookContent(gridShelf, et_search.getText().toString());

							btn_cleartext.setVisibility(View.VISIBLE);
							//loadGridView();
							gridView.invalidateViews();
							et_search.setFocusableInTouchMode(false);
							et_search.setFocusable(false);
							et_search.clearFocus();
							et_search.setFocusableInTouchMode(true);
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(rl.getWindowToken(), 0);
						}else{
							showNooorPlusDialog();
						}
					} else if (!et_search.getText().toString().equals("") && bookSearch) {
						if (group.getShelfSearchForBookEnable()) {
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(rl.getWindowToken(), 0);
							BookSearchDialog searchDialog = new BookSearchDialog(MainActivity.this);
							searchDialog.new checkingForCoverPage(et_search.getText().toString()).execute();
						}else{
							showNooorPlusDialog();
						}
					}

				}

				return false;
			}
		});
		int themeCount=db.getThemeCount(0);
		if(themeCount==0){
			Calendar c = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String formattedDate = df.format(c.getTime());
			String query = "insert into Themes(themeID,themeName,createDate,foregroundColor,category) values('0','Default','"+formattedDate+"','1-1-1','"+1+"')";
			db.executeQuery(query);
		}
		filterViewsBasedOnLimitedVersion();
		this.loadGridView();

		//mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN).build();
	}

	/**
	 * Display Alert Dialog for changing language
	 * @param ctx
	 * @param msg
	 * @param title
	 */
	public void DisplayLanguageAlertDialog(Context ctx, int msg, int title) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (language.equals("ar")) {
					saveLocale("en");
				} else {
					saveLocale("ar");
				}
				android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void filterViewsBasedOnLimitedVersion() {
		if (!Globals.isTablet()) {
			btnNewBook.setVisibility(View.GONE);
			create_books_layout.setVisibility(View.GONE);
			create_mindmap_layout.setVisibility(View.GONE);
			create_webbook_layout.setVisibility(View.GONE);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Load Locale language from shared preference and change language in the application
	 */
	private void loadLocale() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		language = prefs.getString(Globals.languagePrefsKey, "en");
		oldDatabasePrefs = prefs.getBoolean(Globals.oldDatabase, Globals.olddb);
		changeLang(language);
	}

	/**
	 * change language in the application
	 * @param language
	 */
	private void changeLang(String language) {
		myLocale = new Locale(language);
		Locale.setDefault(myLocale);
		android.content.res.Configuration config = new android.content.res.Configuration();
		config.locale = myLocale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
	}

	/**
	 * save locale language to shared preference
	 * @param language
	 */
	private void saveLocale(String language) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Globals.languagePrefsKey, language);
		editor.commit();
	}

	public Boolean unZipDownloadedFile(String zipfilepath, String unziplocation) {
		InputStream is;
		ZipInputStream zis;
		try {
			is = new FileInputStream(zipfilepath);
			zis = new ZipInputStream(new BufferedInputStream(is));
			ZipEntry ze;
			while ((ze = zis.getNextEntry()) != null) {
				String zipEntryName = ze.getName();

				File file = new File(unziplocation + "/" + zipEntryName);
				if (file.exists()) {

					byte[] buffer = new byte[1024];
					FileOutputStream fout = new FileOutputStream(file);
					BufferedOutputStream baos = new BufferedOutputStream(fout, 1024);
					int count;
					while ((count = zis.read(buffer, 0, 1024)) != -1) {
						baos.write(buffer, 0, count);
					}
					baos.flush();
					baos.close();
				} else {

					if (ze.isDirectory()) {

						file.mkdirs();
						continue;
					}

					file.getParentFile().mkdirs();   //Horror line..............
					byte[] buffer = new byte[1024];
					FileOutputStream fout = new FileOutputStream(file);
					BufferedOutputStream baos = new BufferedOutputStream(fout, 1024);
					int count;
					while ((count = zis.read(buffer, 0, 1024)) != -1) {
						baos.write(buffer, 0, count);
					}
					baos.flush();
					baos.close();
				}
			}
			zis.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * @author LoadGridView
	 *
	 */
	public void loadGridView() {
		if (et_search.getText().toString().equals("")) {
			btn_cleartext.setVisibility(View.INVISIBLE);
		}
		gridShelf = new GridShelf(this, db, userfn);
		gridView = (DragSortListView) findViewById(R.id.myListView);
		gridView.setAdapter(gridShelf);
		gridView.setDropListener(this);
		controller = new DragSortController(gridView);
		controller.setDragHandleId(R.id.drag_handle);
		controller.setRemoveEnabled(false);
		controller.setSortEnabled(true);
		controller.setDragInitMode(DragSortController.ON_DRAG);
		gridView.setFloatViewManager(controller);
		gridView.setOnTouchListener(controller);
//		gridView.setDragEnabled(true);
		global.bookListArray=gridShelf.bookListArray;
		gridView.setHorizontalScrollBarEnabled(false);
		if (isDownload){
			if (checkStorBookstatus(bookId)) {
				bookClicked(bookId,"",false,null);
			}

		}
		if (SlideMenuActivity.openPdfReader) {
			for (int j = 0; j < gridShelf.bookListArray.size(); j++) {
				Book book = (Book) gridShelf.bookListArray.get(j);
				int lastBook = db.getMaxUniqueRowID("books");
				if (book.getbCategoryId() == 6 && book.getBookID() == lastBook) {
					SlideMenuActivity.openPdfReader = false;
					break;
				}
			}
		}
	}

	private boolean checkForBookSubscribeAndOpen(Book currentBook){
		if (currentBook.getPurchaseType().equals("free") || currentBook.getPurchaseType().equals("created") || currentBook.getPurchaseType().equals("purchased")) {
			return true;
		} else if (currentBook.getPurchaseType().equals("subscribed")){
			if (UserFunctions.checkLoginAndAlert(MainActivity.this, true)) {
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
				ArrayList<HashMap<String,String>> existList = null;
				try {
					existList = (ArrayList<HashMap<String,String>>) ObjectSerializer.deserialize(prefs.getString(Globals.SUBSLIST, ObjectSerializer.serialize(new ArrayList<HashMap<String,String>>())));
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (UserFunctions.checkClientIdExist(existList,currentBook.getClientID())){
					for (int i=0;i<existList.size();i++){
						HashMap<String,String> hashMap = existList.get(i);
						if (hashMap.get("AppstoreID").equals(currentBook.getClientID())) {
							int subscriptDays = Integer.parseInt(hashMap.get("daysLeft"));
							if (subscriptDays > 0) {
								return true;
							} else {
								UserFunctions.DisplayAlertDialog(MainActivity.this, R.string.renew_subscript, R.string.subscription);
								return false;
							}
						}
					}
				}else {
					UserFunctions.DisplayAlertDialog(MainActivity.this, R.string.renew_subscript, R.string.subscription);
					return false;
				}
			}
		}
		return false;
	}

	public void bookClicked(int position) {
//		String absPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
//		String path= absPath+"/Download/";
//		UserFunctions.copyFiles("data/data/"+ Globals.getCurrentProjPackageName() +"/databases/sboookAuthor.sqlite",path+"sboookAuthor.sqlite");
		Book currentBook = (Book) gridShelf.bookListArray.get(position);
		currentBookClickedPosition = position;
		if (!checkForBookSubscribeAndOpen(currentBook)) {
			return;
		}
		if (currentBook.is_bStoreBook() && (currentBook.get_bStoreID().contains("M"))) {
			Intent bookViewIntent = new Intent(getApplicationContext(), BookViewReadActivity.class);
			bookViewIntent.putExtra("Book", currentBook);
			//bookViewIntent.putExtra("UserCredentials", SlideMenuActivity.userCredentials);
			bookViewIntent.putExtra("currentPageNo", currentBook.get_lastViewedPage());
			bookViewIntent.putExtra("Shelf", gridShelf);
			startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
		} else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("E")) {
			Intent bookViewIntent = new Intent(getApplicationContext(), Elesson.class);
			bookViewIntent.putExtra("Book", currentBook);
			//bookViewIntent.putExtra("UserCredentials", SlideMenuActivity.userCredentials);
			startActivityForResult(bookViewIntent, bookViewActivityrequestCode);

		} else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("Q")) {
			Intent bookViewIntent = new Intent(getApplicationContext(), QuizBookViewActivity.class);
			bookViewIntent.putExtra("Book", currentBook);
			//bookViewIntent.putExtra("UserCredentials", SlideMenuActivity.userCredentials);
			bookViewIntent.putExtra("Shelf", gridShelf);
			startActivityForResult(bookViewIntent, bookViewActivityrequestCode);

		} else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'R') {
			Intent bookViewIntent = new Intent(getApplicationContext(), RzooomBookViewActivity.class);
			bookViewIntent.putExtra("Book", currentBook);
			//bookViewIntent.putExtra("UserCredentials", SlideMenuActivity.userCredentials);
			startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
		} else if (currentBook.get_bStoreID()!=null && currentBook.get_bStoreID().contains("P")) {
			Intent bookViewIntent = new Intent(getApplicationContext(), pdfActivity.class);
			bookViewIntent.putExtra("Book", currentBook);
			bookViewIntent.putExtra("Shelf", gridShelf);
			startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
		} else if (!currentBook.is_bStoreBook() && currentBook.get_bStoreID() != null && currentBook.get_bStoreID().contains("A")) {
			//if (currentBook.get_bStoreID().charAt(0) == 'A') {
				Intent bookViewIntent = new Intent(getApplicationContext(), MediaBookViewActivity.class);
				bookViewIntent.putExtra("Book", currentBook);
				bookViewIntent.putExtra("currentPageNo", currentBook.get_lastViewedPage());
				bookViewIntent.putExtra("Shelf", gridShelf);
				startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
			//}
		} else if (currentBook.get_bStoreID() != null &&currentBook.get_bStoreID().length()>1&& (currentBook.get_bStoreID().charAt(0) == 'I' || currentBook.get_bStoreID().charAt(1) == 'I')) {
			if (!currentBook.is_bStoreBook()) {
				File Path = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/mindMaps");
				File[] mindMapFiles = Path.listFiles();
				String fileName=null;
				for (File file : mindMapFiles) {
					 fileName = file.getName().replace(".xml","");
                }
				Intent intent = new Intent(getApplicationContext(), MindMapActivity.class);
				intent.putExtra("MIRootNodeText", "");
				intent.putExtra("MIOrphanNodeText", "");
				intent.putExtra("MindMapTitle",fileName);
				intent.putExtra("book", currentBook);
				startActivityForResult(intent, 1004);
			}else if (currentBook.get_bStoreID().charAt(1) == 'I') {
				Intent intent = new Intent(getApplicationContext(), MindMapPlayerActivity.class);
				intent.putExtra("book", currentBook);
				startActivity(intent);
			 }
			}  else {
			   Intent bookViewIntent;
			   String query = "select * from objects where BID='"+currentBook.getBookID()+"'order by SequentialID";
               ArrayList<Object> objectList = db.getAllObjectsFromPage(query);
			   if(objectList.size()>2&& currentBook.getDownloadURL()!=null) {
				  // bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
				   bookViewIntent = new Intent(getApplicationContext(), PreviewActivity.class);
			   }else{
				   bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
			   }
				bookViewIntent.putExtra("Book", currentBook);
				bookViewIntent.putExtra("currentPageNo", String.valueOf(currentBook.get_lastViewedPage()));
			   // bookViewIntent.putExtra("CurrentPageNumber", currentBook.get_lastViewedPage());
				bookViewIntent.putExtra("Shelf", gridShelf);
				startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
			}
		}


		@Override
		public void onSaveInstanceState (Bundle outState){
			super.onSaveInstanceState(outState);
			//uiHelper.onSaveInstanceState(outState);
		}

		@Override
		protected void onActivityResult ( int requestCode, int resultCode, Intent data){
			if (mHelper != null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
				super.onActivityResult(requestCode, resultCode, data);
			}
			if (requestCode == bookViewActivityrequestCode && data != null) {
			
				Book book = (Book) data.getSerializableExtra("Book");
				GridShelf grShelf = (GridShelf) data.getSerializableExtra("Shelf");
				boolean mindMapedited = data.getBooleanExtra("mindMap", false);
				int mindMapAdded = grShelf.bookListArray.size() - gridShelf.bookListArray.size();
				if (grShelf != null) {
					gridShelf.bookListArray = grShelf.bookListArray;
					gridShelf.setNoOfBooks(grShelf.bookListArray.size());
				}
				for (int i = 0; i < gridShelf.bookListArray.size(); i++) {
					Book currentBook = (Book) gridShelf.bookListArray.get(i);
					if (currentBook.getBookID() == book.getBookID()) {
						gridShelf.bookListArray.remove(i);
						gridShelf.bookListArray.add(i, book);
						gridView.invalidateViews();
						break;
					}
				}
				if (gridShelf.downloadInProgressArray().size()>0 || mindMapedited){
					loadGridView();
				}
				gridShelf.reloadCategoryListArray();
			} else if (requestCode == storeViewActivityrequestCode && data != null) {
				GridShelf grShelf = (GridShelf) data.getSerializableExtra("Shelf");
			//	progress = (HashMap<String, Integer>) data.getSerializableExtra("progress");
				gridShelf.bookListArray = grShelf.bookListArray;
				gridShelf.setNoOfBooks(grShelf.bookListArray.size());
				gridShelf.reloadCategoryListArray();
				gridView.invalidateViews();
//				if (downloadList>0){
					loadGridView();
			//	}
			} else if (requestCode == COVER_BACKGROUND_GALLERY && data != null) {
				if (resultCode == RESULT_OK) {
					Uri selectedImage = data.getData();
					String backgroundSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
					String toPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + gridShelf.currentBook.getBookID() + "/FreeFiles/pageBG_1.png";
					if (!new File(toPath).exists()) {
						db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit) values('" + "" + "', '" + "PageBG" + "', '" + gridShelf.currentBook.getBookID() + "', '" + 1 + "', '" + "0|0" + "', '" + "ScaleToFit" + "', '" + toPath + "', '" + 0 + "', '" + "no" + "')");
					}
					UserFunctions.copyFiles(backgroundSrcImage, toPath);
					gridView.invalidateViews();
				}
			} else if (requestCode == RACK_CAPTURE_GALLERY && data != null) {
				if (resultCode == RESULT_OK) {
					selectedGalleryImageUri = data.getData();
					if (Build.VERSION.SDK_INT >= 23) {
						requestReadExternalStoragePermission();
					} else {
						changeRackBgImage();
					}
				}
			} else if (requestCode == userTemplateRequestCode && data != null) {
				if (resultCode == RESULT_OK) {
					Templates usertemplate = (Templates) data.getSerializableExtra("Template");
					if (usertemplate != null) {
						gridShelf.templatesPortraitListArray.add(usertemplate);
					}
				}
			} else if (requestCode == RC_SIGN_IN) {
				if (resultCode != RESULT_OK) {
					mSignInClicked = false;
				}

				mIntentInProgress = false;

			} else if (requestCode == 64206) {
			//	uiHelper.onActivityResult(requestCode, resultCode, data);
			/*Session session = Session.getActiveSession();
			session.onActivityResult(this, requestCode, resultCode, data);
			if (session != null && (session.isOpened() || session.isClosed())) {
				onSessionStateChange(session, session.getState(), null);
			}*/
			} else if (requestCode == 1004) {
				if (data != null) {
					boolean isOpenMindMapDialog = data.getExtras().getBoolean("MIOpenDialog", false);
					if (isOpenMindMapDialog) {
						Globals.mindMapFileNameLastOpened = null;
						String orphanNode = data.getExtras().getString("MIOrphanNodeText", "");
						MindMapDialog mindMapDialog = new MindMapDialog(MainActivity.this, false);
						mindMapDialog.showMindMapDialog(orphanNode);
					}
					String mindMapTitle = data.getExtras().getString("MITitle");
					if (mindMapTitle != null && mindMapTitle != "") {
//					UserFunctions.saveMindMapScreenShotForPageBg(mindMapTitle, db, currentBook, gridShelf);
					}
					gridShelf.reloadCategoryListArray();
				//	if (gridShelf.downloadInProgressArray().size()>0){
						loadGridView();
				//	}
				}
			} else if (requestCode == CAPTURE_GALLERY) {
				if (resultCode == RESULT_OK) {
					Uri selectedImage = data.getData();
					String picturePath = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
					if (picturePath != null) {
						Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
						BookSearchDialog bookSearch = new BookSearchDialog(MainActivity.this);
						bookSearch.imagePicked(bitmap);
					}
				}
			} else if (requestCode == mediaAlbumActivityRequestCode && data != null) {
				GridShelf grShelf = (GridShelf) data.getSerializableExtra("Shelf");
				boolean isAlbumCreated = data.getBooleanExtra("IsAlbumCreated", false);
				if (isAlbumCreated) {
					gridShelf.bookListArray = grShelf.bookListArray;
					gridShelf.setNoOfBooks(grShelf.bookListArray.size());
					gridShelf.reloadCategoryListArray();
					gridView.invalidateViews();
				}
				if (gridShelf.downloadInProgressArray().size()>0){
					loadGridView();
				}
			}else if (requestCode == DirectoryPickerActivity.PICK_DIRECTORY && resultCode == RESULT_OK) {
				Bundle extras = data.getExtras();
				String fileSourcePath = extras.getString(DirectoryPickerActivity.SOURCE_PATH);
				String fileDestpath = extras.getString(DirectoryPickerActivity.CHOSEN_DIRECTORY)+"/"+new File(fileSourcePath).getName();
				BookExportDialog exportDialog=new BookExportDialog(MainActivity.this,gridShelf.currentBook);
				exportDialog.new saveFileToUserDefinedPath(fileSourcePath, fileDestpath).execute();
			}else if(requestCode == 1003 && data != null){
				BookExportDialog exportDialog=new BookExportDialog(MainActivity.this,gridShelf.currentBook);
				exportDialog.Authorize();
			}else if(requestCode == 1 && data != null){

			}
			else if (requestCode == webBookActivityRequestCode && data!=null){
				GridShelf grShelf = (GridShelf) data.getSerializableExtra("Shelf");
				gridShelf.bookListArray = grShelf.bookListArray;
				boolean isAlbumCreated = data.getBooleanExtra("IsWebBookCreated", false);
				if (isAlbumCreated) {
					gridShelf.setNoOfBooks(grShelf.bookListArray.size());
					gridShelf.reloadCategoryListArray();
					gridView.invalidateViews();
				}
				if (gridShelf.downloadInProgressArray().size()>0){
					loadGridView();
				}
			}
			else if (requestCode == 10001 && data != null){
				BookExportDialog exportDialog=new BookExportDialog(MainActivity.this,gridShelf.currentBook);
                if (exportDialog.mHelper != null) {
					if (!exportDialog.mHelper.handleActivityResult(requestCode,
							resultCode, data)) {
						//System.out.println("OnActivityResult");
						super.onActivityResult(requestCode, resultCode, data);
					}
				}
			}if(requestCode == CAPTURE_GALLERY_TEMPLATE){
				if(resultCode == RESULT_OK){
					Uri selectedImage = data.getData();

					String picturePath = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
					if (picturePath != null && templatesView !=null && iv_selected!=null) {
						if(selectedPos==0){
							coverImageForBook(picturePath,freeFilesPath+templateId+"/card.png");
							coverImageForBook(picturePath,freeFilesPath+templateId+"/frontThumb.png");
							potraitImageForBook(picturePath,freeFilesPath+templateId+"/front_P.png");
							Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/front_P.png");
							iv_selected.setImageBitmap(bitmap);
						}else if(selectedPos==1){
							coverImageForBook(picturePath,freeFilesPath+templateId+"/pageThumb.png");
							potraitImageForBook(picturePath,freeFilesPath+templateId+"/page_P.png");
							Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/page_P.png");
							iv_selected.setImageBitmap(bitmap);
						}else if(selectedPos==2){
							coverImageForBook(picturePath,freeFilesPath+templateId+"/backThumb.png");
							potraitImageForBook(picturePath,freeFilesPath+templateId+"/back_P.png");
							Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/back_P.png");
							iv_selected.setImageBitmap(bitmap);
						}
						templatesView.invalidateViews();
					}
				}else if(resultCode == RESULT_CANCELED){
					//System.out.println("Cancelled operation to pic image from gallery");
				}
			}
		}

		@Override
		public void onRequestPermissionsResult ( int requestCode, String[] permissions,
		int[] grantResults){
			switch (requestCode) {
				case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
					if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
						//Permission Granted
						changeRackBgImage();
					} else {
						//Permission denied
					}
					return;
				}
				case INITIAL_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {

				}
			}
		}


	public void requestReadExternalStoragePermission(){
		if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			/*if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

			} else {*/
			ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
			//}
		} else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
			changeRackBgImage();
		}
	}

	private void changeRackBgImage(){
		String bgRackSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, selectedGalleryImageUri);
		String shelfRackImgPath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH;
		File fileImgDir = new File(shelfRackImgPath);
		if (!fileImgDir.exists()) {
			fileImgDir.mkdir();
		}
		String destRackImgPath = shelfRackImgPath+gridShelf.categoryRack.getCategoryId()+".png";
		gridShelf.categoryRack.setCategoryImagePath(destRackImgPath);
		String query = "update tblCategory set CBgImagePath='"+gridShelf.categoryRack.getCategoryId()+".png"+"' where CID='"+gridShelf.categoryRack.getCategoryId()+"'";
		db.executeQuery(query);
		UserFunctions.copyFiles(bgRackSrcImage, destRackImgPath);
		gridView.invalidateViews();
	}

	/**
	 * @author On Click Listener
	 *
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_new:
			//	showAddNewBookDialog();
				break;
			case R.id.create_book_btn:
				if (group.getShelfCreateNewBookEnable()){
					//showAddNewBookDialog();
					showTemplatesDialog();
				}else{
					showNooorPlusDialog();
				}
				break;
			case R.id.create_mindmap_btn:
				if (group.getShelfMindmapEnable()) {
					MindMapDialog dialog = new MindMapDialog(MainActivity.this, false);
					dialog.showMindMapDialog("");
				}else{
					showNooorPlusDialog();
				}
				break;
			case R.id.create_album_btn:
				if (group.getShelfAlbumEnable()) {
					showAddNewMediaAlbumActivity();
				}else{
					showNooorPlusDialog();
				}
				break;
			case R.id.create_shelf_btn:
				gridShelf.addCategory();
				break;

			case R.id.add_shelf:
				if (add_books_layout.getVisibility()==View.VISIBLE){
					btnAddBook.setBackgroundResource(R.drawable.ic_add_page);
					Animation anim= AnimationUtils.loadAnimation(this, R.anim.toolbar_anim_down);
					add_books_layout.setAnimation(anim);
					add_books_layout.setVisibility(View.GONE);
				}else{
					btnAddBook.setBackgroundResource(R.drawable.ic_add_page_pink);
					Animation anim= AnimationUtils.loadAnimation(this, R.anim.toolbar_anim_up);
					add_books_layout.setAnimation(anim);
					add_books_layout.setVisibility(View.VISIBLE);
				}
				break;

//			case R.id.btn_edit:
//				gridShelf.editBooks();
//				gridView.invalidateViews();
//				break;

			case R.id.btn_import:
			    Intent storeViewIntent = new Intent(getApplicationContext(), StoreTabViewFragment.class);
				storeViewIntent.putExtra("Shelf", gridShelf);
				startActivityForResult(storeViewIntent, storeViewActivityrequestCode);
				break;

			case R.id.btn_lang:
				DisplayLanguageAlertDialog(this, R.string.CHANGE_LANGUAGE_MESSAGE, R.string.CHANGE_LANGUAGE_TITLE);
				break;
			case R.id.btn_facebklogin:

				isPressed = !isPressed;

				if (!isPressed) {

					SlideMenuActivity.mSlideMenu.mCurrentContentOffset= -(int)getResources().getDimension(R.dimen.info_popover_loginwidth);
					SlideMenuActivity.mSlideMenu.invalidateMenuState();
					SlideMenuActivity.mSlideMenu.invalidate();
					SlideMenuActivity.mSlideMenu.requestLayout();
					SlideMenuActivity.refreshingUserData();


				} else {

					SlideMenuActivity.mSlideMenu.close(true);

				}
//				mGoogleApiClient.connect();
//				socialPopoverView = new PopoverView(this, R.layout.activity_export_enrichment);
//				socialPopoverView.setContentSizeForViewInPopover(new Point((int) getResources().getDimension(R.dimen.info_popover_loginwidth), (int) getResources().getDimension(R.dimen.info_popover_loginheight)));
//				socialPopoverView.setDelegate(MainActivity.this);
//				socialPopoverView.showPopoverFromRectInViewGroup(rl, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionUp, true);
//				viewPager = (ViewPager) socialPopoverView.findViewById(R.id.viewPager);
//				loginAdapter = new ViewAdapter();
//				viewPager.setAdapter(loginAdapter);
//				viewPager.setOffscreenPageLimit(2);
//				//viewPager.setCurrentItem(Integer.parseInt(currentPageNumber) - 1);
//				viewPager.setOnTouchListener(new OnTouchListener() {
//					@Override
//					public boolean onTouch(View v, MotionEvent event) {
//						return true;
//					}
//				});
				break;
			case R.id.btn_search:
				if (et_layout.getVisibility()==View.INVISIBLE){
//					frame_layout.setBackgroundColor(Color.parseColor("#AA4871"));
					if (Globals.isTablet()){
						cloud_layout.setVisibility(View.INVISIBLE);
						frame_edit_layout.setVisibility(View.INVISIBLE);
						enr_relativeLayout.setVisibility(View.INVISIBLE);
					}else{
						cloud_layout.setVisibility(View.INVISIBLE);
						frame_edit_layout.setVisibility(View.INVISIBLE);
						frame_add_layout.setVisibility(View.INVISIBLE);
						enr_relativeLayout.setVisibility(View.INVISIBLE);
					}
					Search=true;
					et_layout.setVisibility(View.VISIBLE);
					titleText.setVisibility(View.GONE);
//					btnEditBook.setVisibility(View.GONE);
				} else {
					if (Globals.isTablet()){
						cloud_layout.setVisibility(View.VISIBLE);
						frame_edit_layout.setVisibility(View.VISIBLE);
						enr_relativeLayout.setVisibility(View.VISIBLE);
					}else{
						cloud_layout.setVisibility(View.VISIBLE);
						frame_edit_layout.setVisibility(View.VISIBLE);
						frame_add_layout.setVisibility(View.VISIBLE);
						enr_relativeLayout.setVisibility(View.VISIBLE);
					}
					Search=false;
					et_layout.setVisibility(View.INVISIBLE);
					if (Globals.isTablet()) {
						titleText.setVisibility(View.VISIBLE);
					}
					gridView.invalidateViews();
//					frame_layout.setBackgroundColor(Color.parseColor("#3F3054"));
//					btnEditBook.setVisibility(View.VISIBLE);
				}
				break;
			case R.id.btn_arrange:
				if (editmode){
					btnArrange.setBackgroundResource(R.drawable.ic_edit);
					editmode = false;
				}else {
					btnArrange.setBackgroundResource(R.drawable.done);
					editmode = true;
				}
				gridView.invalidateViews();
//				RearrangeShelf rearrangeShelf = new RearrangeShelf(MainActivity.this, gridShelf.categoryListArray);
//				rearrangeShelf.displayRearrangeShelfPopUp();
				break;
			case R.id.btnEnrResDownload:
				if (group.isShelfinboxEnable()) {
					Intent intent = new Intent(MainActivity.this, CloudBooksActivity.class);
					intent.putExtra("Shelf", gridShelf);
					startActivityForResult(intent, storeViewActivityrequestCode);
				}else{
					showNooorPlusDialog();
				}
				break;
			case R.id.btn_cloud:
				if(checkLoginAndAlert(MainActivity.this,true)) {
					Intent cloud_intent = new Intent(MainActivity.this, CloudActivity.class);
					startActivity(cloud_intent);
				}
				break;
			case R.id.create_webbook_btn:
				if (group.getShelfCreateNewBookEnable()){
					Intent web_intent = new Intent(MainActivity.this,WebBookActivity.class);
					web_intent.putExtra("Shelf", gridShelf);
					web_intent.putExtra("From","FromShelf");
					startActivityForResult(web_intent, webBookActivityRequestCode);
				}else{
					showNooorPlusDialog();
				}
				break;
			case R.id.create_flashcard_btn:
				Intent intent = new Intent(MainActivity.this,FlashCardActivity.class);
				intent.putExtra("fromBook",false);
				startActivity(intent);
				break;
			default:
				break;
		}

	}

	@Override
	public void drop(int from, int to) {
		System.out.println(from);
		int frmCID = from + 1;
		int toCID = to + 1;
		com.semanoor.source_sboookauthor.Category category = gridShelf.categoryListArray.get(from);
		com.semanoor.source_sboookauthor.Category category1 = gridShelf.categoryListArray.get(to);
		//categoryListArray.add(to, category);
		if (from > to) {
			db.executeQuery("update tblCategory set newCID=newCID+1 where newCID>='"+category1.getNewCatID()+"' and newCID<'"+category.getNewCatID()+"'");
		} else {
			db.executeQuery("update tblCategory set newCID=newCID-1 where newCID<='"+category1.getNewCatID()+"' and newCID>'"+category.getNewCatID()+"'");
		}
		db.executeQuery("update tblCategory set newCID='"+category1.getNewCatID()+"' where CID='"+category.getCategoryId()+"'");
		gridShelf.categoryListArray = gridShelf.reloadCategoryListArray();
//		gridShelf.notifyDataSetChanged();
		gridView.invalidateViews();
	}

	//GooglePlusLogin


/**
 * Update the login user credentials to Shared preferene
 * @param scopeId
 * @param userName
 * @param scopeType
 */


/**
 * Check shared preference and login
 */
/*private void checkAndLogin(){
	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
	String scopeId = prefs.getString(Globals.sUserIdKey, "");
	String userName = prefs.getString(Globals.sUserNameKey, "");
	int scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
	String userEmailId = prefs.getString(Globals.sUserEmailIdKey, "");
	if (scopeType != 0) {
		userCredentials = new UserCredentials(scopeId, userName, scopeType);
	} else {
		userCredentials = null;
	}
}  */


	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		if (group == btnSegmentControl) {
			if (checkedId == R.id.segSlideShelf) {

			}
			else if (checkedId == R.id.segGridShelf) {
			}
		}
    }

	/**
	 * @author Show Portrait and Landscape popup window
	 *
	 */
	/*private void showLandscapeAndPortraitPopUpWindow(){
		int popupWidth = 300;
		int popupHeight = 200;

		View view = this.getLayoutInflater().inflate(R.layout.add_new_book_dialog, null);

		final PopupWindow popup = new PopupWindow(this);
		popup.setContentView(view);
		popup.setWidth(popupWidth);
		popup.setHeight(popupHeight);
		popup.setFocusable(true);

		popup.showAtLocation(view, Gravity.CENTER, 0, 0);

		Button btnPortrait = (Button) view.findViewById(R.id.btnPortrait);
		btnPortrait.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				gridShelf.addNewBook(Globals.portrait);
				gridView.invalidateViews();
				popup.dismiss();
			}
		});

		Button btnLandscape = (Button) view.findViewById(R.id.btnLandscape);
		btnLandscape.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				gridShelf.addNewBook(Globals.landscape);
				gridView.invalidateViews();
				popup.dismiss();
			}
		});
	}*/

   public void showAddNewBookDialog(){
		final Dialog addNewBookDialog = new Dialog(this);
		//addNewBookDialog.setTitle(R.string.create_new_book);
		addNewBookDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		addNewBookDialog.setContentView(this.getLayoutInflater().inflate(R.layout.add_new_book_dialog, null));
		addNewBookDialog.getWindow().setLayout((int)(global.getDeviceWidth()/1.3), (int)(global.getDeviceHeight()/1.5));

		final SegmentedRadioGroup btnSegmentGroup = (SegmentedRadioGroup) addNewBookDialog.findViewById(R.id.btn_segment_group);

		final GridView templateGrid = (GridView) addNewBookDialog.findViewById(R.id.gridView1);
	   Button btn = (Button) addNewBookDialog.findViewById(R.id.book_btn);
	   btn.setVisibility(View.GONE);
	   CircleButton back_btn = (CircleButton) addNewBookDialog.findViewById(R.id.btnback);
	   back_btn.setOnClickListener(new OnClickListener() {
		   @Override
		   public void onClick(View v) {
			   addNewBookDialog.dismiss();
		   }
	   });
		templateGridAdapter = new TemplateGridListAdapter(this, gridShelf.templatesPortraitListArray,false);
		templateGrid.setAdapter(templateGridAdapter);
		templateGrid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position,
									long arg3) {
				int templateId = view.getId();
				if (position == gridShelf.templatesPortraitListArray.size()) {
					Intent bookViewIntent = new Intent(getApplicationContext(), UserTemplates.class);
					startActivityForResult(bookViewIntent, userTemplateRequestCode);
					addNewBookDialog.dismiss();
				} else {
					int orientationIndex = btnSegmentGroup.indexOfChild(addNewBookDialog.findViewById(btnSegmentGroup.getCheckedRadioButtonId()));
					gridShelf.addNewBook(orientationIndex, templateId);
					gridView.invalidateViews();
					addNewBookDialog.dismiss();
				}
			}

		});

		btnSegmentGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId == R.id.btn_segment_portrait) {
					templateGridAdapter = new TemplateGridListAdapter(MainActivity.this, gridShelf.templatesPortraitListArray,false);
					templateGrid.setAdapter(templateGridAdapter);
				} else if (checkedId == R.id.btn_segment_landscape) {
					templateGridAdapter = new TemplateGridListAdapter(MainActivity.this, gridShelf.templatesLandscapeListArray,false);
					templateGrid.setAdapter(templateGridAdapter);
				}
			}
		});

		addNewBookDialog.show();
	}

	public void showAddNewMediaAlbumActivity(){
		Intent myMediaIntent = new Intent(getApplicationContext(), MediaAlbumCreatorActivity.class);
		myMediaIntent.putExtra("Shelf", gridShelf);
		startActivityForResult(myMediaIntent, mediaAlbumActivityRequestCode);
	}

	protected void onStart() {
		super.onStart();
	}

	protected void onStop() {
		super.onStop();

	}

	@Override
	public void popoverViewWillShow(PopoverView view) {

	}

	@Override
	public void popoverViewDidShow(PopoverView view) {

	}

	@Override
	public void popoverViewWillDismiss(PopoverView view) {
		if(malzamah!=null && isDeleteBookDir) {
			malzamah.deleteCretaedBookDir();
		}
		if (PopoverView!= null){
			PopoverView = null;
		}

    }

	@Override
	public void popoverViewDidDismiss(PopoverView view) {

	}

	public void selectedPageInBook(int pageNum, int bookId, String typedText, String bookStoreId) {
		for (int i = 0; i < gridShelf.bookListArray.size(); i++) {
			Book currentBook = (Book) gridShelf.bookListArray.get(i);
			if (currentBook.getBookID() == bookId && currentBook.get_bStoreID().equals(bookStoreId)) {
				currentBookClickedPosition = i;
				Intent bookViewIntent = new Intent(getApplicationContext(), BookViewReadActivity.class);
				bookViewIntent.putExtra("Book", currentBook);
				//bookViewIntent.putExtra("UserCredentials", SlideMenuActivity.userCredentials);
				bookViewIntent.putExtra("currentPageNo", pageNum);
				bookViewIntent.putExtra("Shelf", gridShelf);
				startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
				break;
			}
		}
	}

    public class GestureListener extends GestureDetector.SimpleOnGestureListener{

		@Override
		public boolean onDown(MotionEvent e) {
			return true;
		}

		@Override
		public boolean onDoubleTap(MotionEvent e) {
			gridShelf.editTitle(gridShelf.editCategory);
			return true;
		}
	}

	/**
	 * SubScription
	 */
	public void subscribePurchase(final String subscribeProductId){
		subscription = Subscription.getInstance();
		mHelper = new IabHelper(this, Globals.manahijBase64EncodedPublicKey);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

			@Override
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					// Oh noes, there was a problem.
					UserFunctions.complain("Problem setting up in-app billing: " + result, MainActivity.this);
					return;
				}

				// Have we been disposed of in the meantime? If so, quit.
				if (mHelper == null) return;

				mHelper.launchPurchaseFlow(MainActivity.this, subscribeProductId, 10001, myPurchaseFinishedListener, "");
			}
		});

	}

	IabHelper.OnIabPurchaseFinishedListener myPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			// if we were disposed of in the meantime, quit.
			if (mHelper == null) return;

			//Log.d(TAG, "Purchase successful.");
			if (result.isFailure()) {
				//UserFunctions.complain("Error purchasing: " + result, BookViewReadActivity.this);
				return;
			}

			if (purchase.getSku().equals("")){
				mHelper.consumeAsync(purchase, myConsumeFinishedListener);
				//UserFunctions.alert("Thank you for subscribing", fa_enrich);
			}
		}
	};

	IabHelper.OnConsumeFinishedListener myConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		@Override
		public void onConsumeFinished(Purchase purchase, IabResult result) {

			if (mHelper == null) return;

			if (result.isSuccess()) {

				if (purchase.getSku().equals("")) {

				}
			} else {
				UserFunctions.complain("Error while consuming: " + result, MainActivity.this);
			}
		}
	};

	public static boolean checkLoginAndAlert(Context ctx, boolean withAlert){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		String scopeId = prefs.getString(Globals.sUserIdKey, "");
		int scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
		if (scopeType > 0 && !scopeId.equals("")) {
			return true;
		} else {
			if (withAlert) {
				UserFunctions.DisplayAlertDialog(ctx,R.string.please_login_main, R.string.login);
			}
			return false;
		}
	}
	private void showTemplatesDialog(){
		addNewBookDialog = new Dialog(MainActivity.this);
		//addNewBookDialog.setTitle(R.string.create_new_book);
		addNewBookDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		addNewBookDialog.setContentView(getLayoutInflater().inflate(R.layout.template_layout, null));
		addNewBookDialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.3), (int)(Globals.getDeviceHeight()/1.5)); //1.5
		templatesViewPager = (ViewPager)addNewBookDialog.findViewById(R.id.viewPager);
		ViewPagerAdapter viewPageradapter = new ViewPagerAdapter();
		templatesViewPager.setAdapter(viewPageradapter);
		templatesViewPager.setOffscreenPageLimit(1);
		addNewBookDialog.show();
	}
	private void addTemplateIdToSharedPreferences(String key,int ID){
		SharedPreferences.Editor editor = sharedPreference.edit();
		ArrayList<Integer> existList = null;
		try {
			existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<Integer>())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		existList.add(ID);
		try {
			editor.putString(key, ObjectSerializer.serialize(existList));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		editor.commit();
	}
	private boolean isDownloading(int Id){
		ArrayList<Integer> existList = null;
		try {
			existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.DownloadingTemplates, ObjectSerializer.serialize(new ArrayList<Integer>())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (int i=0;i<existList.size();i++){
			int existId = existList.get(i);
			if (existId == Id){
				return true;
			}
		}
		return false;
	}
	private void updateTemplateList(int position){
		for (int i=0;i<templatesList.size();i++){
			OnlineTemplates templates = templatesList.get(i);
			if (position == i){
				templates.setDownloadStatus("Downloading");
				break;
			}
		}
	}
	private boolean isDownloaded(int Id){
		for (int i=0;i<templatesList.size();i++){
			OnlineTemplates templates = templatesList.get(i);
			if (Id == templates.getTemplateId()&& (templates.getDownloadStatus().equals("Downloaded") || templates.getDownloadStatus().equals("Created"))){
				return true;
			}
		}
		return false;
	}
	private void parseAndSaveTemplatesList(ArrayList<Object> templates){
		templatesList.clear();
		for (int i=0;i<templates.size();i++){
			HashMap<String,Object> hashMap = (HashMap<String, Object>) templates.get(i);
			OnlineTemplates templates1 = new OnlineTemplates();
			templates1.setTemplateName((String) hashMap.get("name"));
			templates1.setTemplateId((Integer) hashMap.get("ID"));
			templates1.setThumbUrl((String) hashMap.get("ThumbURL"));
			templates1.setContentUrl((String) hashMap.get("contentURL"));
			if (isDownloading(templates1.getTemplateId())){
				templates1.setDownloadStatus("Downloading");
			}else {
				if (new File(Globals.TARGET_BASE_TEMPATES_PATH + templates1.getTemplateId()).exists()) {
					templates1.setDownloadStatus("Downloaded");
				} else {
					templates1.setDownloadStatus("Not Downloaded");
				}
			}
			templatesList.add(templates1);
		}
	}
	private void apiCallBackTask(final ProgressBar progressBar, final OnlineTemplatesGridAdapter adapter, final RecyclerView templateGrid){
		templateGrid.setVisibility(View.INVISIBLE);
		ApiCallBackTask apiCallBackTask = new ApiCallBackTask(MainActivity.this,Globals.templatesUrl,null, new ApiCallBackListener<Object>() {

			@Override
			public void onSuccess(Object object) {
				progressBar.setVisibility(View.GONE);
				templateGrid.setVisibility(View.VISIBLE);
				if (onlineTemplates) {
					templatesList.clear();
					if (object != null) {
						Map<String, Object> jsonMap = (Map<String, Object>) object;
						ArrayList<Object> mapObject = (ArrayList<Object>) jsonMap.get("Templates");
						parseAndSaveTemplatesList(mapObject);
					} else {
						String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
						File file = new File(templatePath);
						File[] path = file.listFiles();
						for (File tempFile : path) {
							OnlineTemplates templates1 = new OnlineTemplates();
							templates1.setTemplateName("Template " + tempFile.getName());
							templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
							templates1.setThumbUrl("");
							templates1.setContentUrl("");
							if (Integer.parseInt(tempFile.getName())<1000){
								templates1.setDownloadStatus("Downloaded");
								templatesList.add(templates1);
							}else{
								templates1.setDownloadStatus("Created");
							}
						}
					}
					templateGrid.setAdapter(adapter);
				}
			}

			@Override
			public void onFailure(Exception e) {
				templatesList.clear();
				templateGrid.setVisibility(View.VISIBLE);
				progressBar.setVisibility(View.GONE);
				String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
				File file = new File(templatePath);
				File[] path = file.listFiles();
				for(File tempFile : path){
					OnlineTemplates templates1 = new OnlineTemplates();
					templates1.setTemplateName("Template "+tempFile.getName());
					templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
					templates1.setThumbUrl("");
					templates1.setContentUrl("");
					if (Integer.parseInt(tempFile.getName())<1000){
						templates1.setDownloadStatus("Downloaded");
						templatesList.add(templates1);
					}else{
						templates1.setDownloadStatus("Created");
					}
				}
				templateGrid.setAdapter(adapter);
			}
		});
		apiCallBackTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	public boolean isTablet(Context ctx){
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width=dm.widthPixels;
		int height=dm.heightPixels;
		double wi=(double)width/(double)dm.xdpi;
		double hi=(double)height/(double)dm.ydpi;
		double x = Math.pow(wi,2);
		double y = Math.pow(hi,2);
		double screenInches = Math.sqrt(x+y);
		if (screenInches>=6.5){
			return true;
		}else{
			return false;
		}
	}

	private void showNooorPlusDialog(){
		Intent intent = new Intent(MainActivity.this,NooorPlusActivity.class);
		startActivity(intent);
	}

	private class ViewPagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return (view == object);
		}

		@Override
		public Object instantiateItem(final ViewGroup container, int position) {
			LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = null;
			switch (position){
				case 0:
					view = inflater.inflate(R.layout.add_new_book_dialog, null);
					final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
					progressBar.setVisibility(View.VISIBLE);
					final OnlineTemplatesGridAdapter adapter = new OnlineTemplatesGridAdapter(MainActivity.this,templatesList);
					final RecyclerView templateGrid = (RecyclerView) view.findViewById(R.id.gridView1);
					SegmentedRadioButton segmentedRadioGroup = (SegmentedRadioButton)view.findViewById(R.id.segment_text);
					final RadioButton online_temp = (RadioButton)view.findViewById(R.id.online);
					Button create_btn = (Button) view.findViewById(R.id.btnNext);
					create_btn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							if (!onlineTemplates) {
								online_temp.setChecked(true);
								onlineTemplates = true;
								progressBar.setVisibility(View.VISIBLE);
								templateGrid.setVisibility(View.INVISIBLE);
								apiCallBackTask(progressBar, adapter, templateGrid);
							}
							templatesViewPager.setCurrentItem(1,true);
						}
					});
					segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
						@Override
						public void onCheckedChanged(RadioGroup group, int checkedId) {
							if (checkedId == R.id.online) {
								onlineTemplates = true;
								progressBar.setVisibility(View.VISIBLE);
								templateGrid.setVisibility(View.INVISIBLE);
								apiCallBackTask(progressBar,adapter,templateGrid);
							} else if (checkedId == R.id.create) {
								onlineTemplates = false;
								templateGrid.setVisibility(View.VISIBLE);
								templatesList.clear();
								progressBar.setVisibility(View.GONE);
								String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
								File file = new File(templatePath);
								File[] path = file.listFiles();
								for(File tempFile : path){
									OnlineTemplates templates1 = new OnlineTemplates();
									templates1.setTemplateName("Template "+tempFile.getName());
									templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
									templates1.setThumbUrl("");
									templates1.setContentUrl("");
									if (Integer.parseInt(tempFile.getName())<1000){
										templates1.setDownloadStatus("Downloaded");
									}else{
										templates1.setDownloadStatus("Created");
										templatesList.add(templates1);
									}
								}
								final OnlineTemplatesGridAdapter tempAdapter = new OnlineTemplatesGridAdapter(MainActivity.this,templatesList);
								templateGrid.setAdapter(tempAdapter);
							}
						}
					});
					GridLayoutManager gridLayout = new GridLayoutManager(MainActivity.this, getResources().getInteger(R.integer.grid_column), GridLayoutManager.VERTICAL, false);
					templateGrid.setLayoutManager(gridLayout);
					templateGrid.setItemAnimator(new DefaultItemAnimator());
					templateGrid.addOnItemTouchListener(new RecyclerTouchListener(MainActivity.this, templateGrid, new RecyclerTouchListener.ClickListener() {
						@Override
						public void onClick(View view, int position) {
							OnlineTemplates templates = templatesList.get(position);
							if (!isDownloading(templates.getTemplateId()) && !isDownloaded(templates.getTemplateId())){
								addTemplateIdToSharedPreferences(Globals.DownloadingTemplates,templates.getTemplateId());
								updateTemplateList(position);
								adapter.notifyDataSetChanged();
							}else if (isDownloaded(templates.getTemplateId())){
								gridShelf.addNewBook(0, templates.getTemplateId());
								gridView.invalidateViews();
								if (addNewBookDialog!=null){
									addNewBookDialog.dismiss();
								}
							}
						}

						@Override
						public void onLongClick(View view, int position) {

						}
					}));
					apiCallBackTask(progressBar,adapter,templateGrid);
					Button btn = (Button) view.findViewById(R.id.book_btn);
					btn.setVisibility(View.GONE);
					CircleButton back_btn = (CircleButton) view.findViewById(R.id.btnback);
					back_btn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							if (addNewBookDialog!=null){
								addNewBookDialog.dismiss();
							}
						}
					});
					break;
				case 1:
					view = inflater.inflate(R.layout.user_templates, null);
					templateId = Integer.parseInt(db.getMaxId("templates"));
					templateId = templateId+1;
					if (templateId<1000){
						templateId = 1001;
					}
					UserFunctions.createNewDirectory(Globals.TARGET_BASE_TEMPATES_PATH+templateId);
					RelativeLayout layout=(RelativeLayout) view.findViewById(R.id.PageView);
                    Button change_btn = (Button) view.findViewById(R.id.btn_change);
                    change_btn.setVisibility(View.GONE);
					templatesView=(ListView) view.findViewById(R.id.PagelistView);
					iv_selected=(ImageView) view.findViewById(R.id.bgImgView);
					final Button btnCreateTmp=(Button) view.findViewById(R.id.btn_createtmp);
					CircleButton close_btn = (CircleButton) view.findViewById(R.id.btn_close);
					close_btn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							if(!templatecreated){
								UserFunctions.DeleteDirectory(new File(freeFilesPath+templateId));
							}
							templatesViewPager.setCurrentItem(0,true);
						}
					});

					btnCreateTmp.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							templatecreated = true;
							if (!new File(freeFilesPath+templateId+"/page_P.png").exists()) {
								coverImageForBook(freeFilesPath +1+ "/pageThumb.png", freeFilesPath + templateId + "/pageThumb.png");
								potraitImageForBook(freeFilesPath +1+ "/page_P.png", freeFilesPath + templateId + "/page_P.png");
							}
							db.executeQuery("insert into templates(templateID,name,type,titleFont,titleSize,titleColor,authorFont,authorSize,authorColor,paraFont,paraColor,paraSize) values('"+templateId+"','UserTemplate','portrait','Verdana','54','232-219-184','Arial','25','232-219-184','Arial','56-56-48','25')");
							templatesViewPager.setCurrentItem(0,true);
						}
					});

					if(new File(freeFilesPath+templateId+"/front_P.png").exists()&& new File(freeFilesPath+templateId+"/back_P.png").exists()){
						btnCreateTmp.setVisibility(View.VISIBLE);
					}else{
						btnCreateTmp.setVisibility(View.INVISIBLE);
					}

					final NewTemplateAdapter templateAdapter = new NewTemplateAdapter(3,btnCreateTmp);
					templatesView.setAdapter(templateAdapter);
					templatesView.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> adapterView, View v, int position,long id) {
							// TODO Auto-generated method stub
							if (selectedPos == position){
								openGallery();
							}else{
								selectedPos=position;
								int firstPosition = templatesView.getFirstVisiblePosition() - templatesView.getHeaderViewsCount();
								int calculatedPosition = selectedPos - firstPosition;
								View currentSelectedListView = adapterView.getChildAt(calculatedPosition);

								if (currentSelectedListView != null && currentSelectedListView != v) {
									v.setBackgroundColor(Color.WHITE);
								}
								currentSelectedListView = v;
								v.setBackgroundColor(Color.parseColor("#604e3634"));
								if(position==0){
									if(new File(freeFilesPath+templateId+"/front_P.png").exists()){
										Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/front_P.png");
										iv_selected.setImageBitmap(bitmap);
									}else{
										openGallery();
									}
								}else if(position==1){
									if(new File(freeFilesPath+templateId+"/page_P.png").exists()){
										Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/page_P.png");
										iv_selected.setImageBitmap(bitmap);
									}else{
										openGallery();
									}
								}else if(position==2){
									if(new File(freeFilesPath+templateId+"/back_P.png").exists()){
										Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/back_P.png");
										iv_selected.setImageBitmap(bitmap);
									}else{
										openGallery();
									}
								}
								templatesView.invalidateViews();
							}
						}
					});
					break;
			}
			((ViewPager) container).addView(view, 0);
			return view;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}
	}
	public void coverImageForBook(String sourceDir,String freeFilesPath ){
		Bitmap bitmap = BitmapFactory.decodeFile(sourceDir);
		Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(200, MainActivity.this), Globals.getDeviceIndependentPixels(300,  MainActivity.this), true);
		UserFunctions.saveBitmapImage(cardBitmap, freeFilesPath);
	}

	public void potraitImageForBook(String sourceDir,String freeFilesPath ){
		Bitmap bitmap = BitmapFactory.decodeFile(sourceDir);
		Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(1536, MainActivity.this), Globals.getDeviceIndependentPixels(2048,  MainActivity.this), true);
		UserFunctions.saveBitmapImage(cardBitmap, freeFilesPath);
	}


	private class NewTemplateAdapter extends BaseAdapter{
		int pagecount;
		Button btnCreateTmp;

		public NewTemplateAdapter(int pageCount,Button createBtn) {
			// TODO Auto-generated constructor stub
			pagecount=pageCount;
			this.btnCreateTmp = createBtn;
		}
		//@Override
		public int getCount() {
			return pagecount;
		}
		//@Override
		public Object getItem(int position) {
			return null;
		}

	//	@Override
		public long getItemId(int position) {
			return 0;
		}

		//@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			View vi = convertView;
			if(convertView == null){
				vi = getLayoutInflater().inflate(R.layout.custom_tmp__image, null);
			}
			TextView tv = (TextView) vi.findViewById(R.id.tv_pagename);
			ImageView image=(ImageView)vi. findViewById(R.id.iv_page);
			//Button btn_del = (Button) vi.findViewById(R.id.btn_del);
			if(selectedPos==position){
				vi.setBackgroundColor(Color.parseColor("#604e3634"));
			}else{
				vi.setBackgroundColor(Color.WHITE);
			}
			if(position==0){
				tv.setText(getResources().getString(R.string.cover_page));
				if(new File(freeFilesPath+templateId+"/frontThumb.png").exists()){
					Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/frontThumb.png");
					image.setImageBitmap(bitmap);
				}else{
					vi.setBackgroundColor(Color.WHITE);
				}
			}else if(position==1){
				tv.setText(getResources().getString(R.string.page));
				if(new File(freeFilesPath+templateId+"/pageThumb.png").exists()){
					Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/pageThumb.png");
					image.setImageBitmap(bitmap);
				}
			}else{
				tv.setText(getResources().getString(R.string.end_page));
				if(new File(freeFilesPath+templateId+"/backThumb.png").exists()){
					Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+templateId+"/backThumb.png");
					image.setImageBitmap(bitmap);
				}
			}
			if(new File(freeFilesPath+templateId+"/front_P.png").exists()&& new File(freeFilesPath+templateId+"/back_P.png").exists()){
				btnCreateTmp.setVisibility(View.VISIBLE);
			}else{
				btnCreateTmp.setVisibility(View.INVISIBLE);
			}

			return vi;
		}
	}

	private void openGallery(){
		Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		galleryIntent.setType("image/*");
		startActivityForResult(galleryIntent, CAPTURE_GALLERY_TEMPLATE);
	}


	public void bookClicked(String bookId, final String bookUrll, boolean publish,Book book) {




		Book currentBook = book;

		if(currentBook==null)
			currentBook = db.getAllBooks(bookId);
		try {




			if(currentBook!=null) {
				if(!currentBook.is_bStoreBook()){
					publish=false;
				}


				if(publish){
					try {



						if(ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
							String TopicName = ManahijApp.getInstance().getPrefManager().getMainChannel();
							Log.e("book", TopicName + "openbook");
							JSONObject obj = new JSONObject();
							obj.put("type", Constants.KEYBOOK);
							obj.put("action",Constants.KEYOPENBOOK);
							String lastviewed = String.valueOf(currentBook.get_lastViewedPage());
							obj.put("book_id", bookId);
							obj.put("last_viewed_page", lastviewed);
							obj.put("url", bookUrll);
							obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
							service.publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(), obj.toString(), MainActivity.this);
						}
					}catch (Exception e){
						Log.e("bookpublish",e.toString());
					}
				}


				if (!checkForBookSubscribeAndOpen(currentBook)) {
					return;
				}
				if(currentBook.is_bStoreBook()){
					lastViewdPage = currentBook.get_lastViewedPage();
					if(!publish) {
						SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
						if (!ManahijApp.getInstance().getPrefManager().getlastViewedPage().equals("") || !(ManahijApp.getInstance().getPrefManager().getlastViewedPage() == null)) {
							if (lastViewdPage != Integer.parseInt(ManahijApp.getInstance().getPrefManager().getlastViewedPage())) {

								lastViewdPage = Integer.parseInt(ManahijApp.getInstance().getPrefManager().getlastViewedPage());
							}
						}
					}

				}
				app.setState(true);
				ManahijApp.getInstance().getPrefManager().addIsinBackground(true);
				if (currentBook.is_bStoreBook() && (currentBook.get_bStoreID().contains("M"))) {



					Intent bookViewIntent = new Intent(getApplicationContext(), BookViewReadActivity.class);
					bookViewIntent.putExtra("Book", currentBook);
					bookViewIntent.putExtra("currentPageNo", lastViewdPage);
					bookViewIntent.putExtra("Shelf", gridShelf);

					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);

				} else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("E")) {
					Intent bookViewIntent = new Intent(getApplicationContext(), Elesson.class);
					bookViewIntent.putExtra("Book", currentBook);
					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);


				} else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().contains("Q")) {

					Intent bookViewIntent = new Intent(getApplicationContext(), QuizBookViewActivity.class);
					bookViewIntent.putExtra("Book", currentBook);
					bookViewIntent.putExtra("Shelf", gridShelf);
					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);

				} else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'R') {

					Intent bookViewIntent = new Intent(getApplicationContext(), RzooomBookViewActivity.class);
					bookViewIntent.putExtra("Book", currentBook);
					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
				} else if (currentBook.get_bStoreID() != null && currentBook.get_bStoreID().contains("P")) {

					Intent bookViewIntent = new Intent(getApplicationContext(), pdfActivity.class);
					bookViewIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					bookViewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					bookViewIntent.putExtra("Book", currentBook);
					bookViewIntent.putExtra("Shelf", gridShelf);
					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
				} else if (!currentBook.is_bStoreBook() && currentBook.get_bStoreID() != null && currentBook.get_bStoreID().contains("A")) {


					Intent bookViewIntent = new Intent(getApplicationContext(), MediaBookViewActivity.class);
					bookViewIntent.putExtra("Book", currentBook);
					bookViewIntent.putExtra("currentPageNo", currentBook.get_lastViewedPage());
					bookViewIntent.putExtra("Shelf", gridShelf);
					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
					//}
				} else if (currentBook.get_bStoreID() != null && currentBook.get_bStoreID().length() > 1 && (currentBook.get_bStoreID().charAt(0) == 'I' || currentBook.get_bStoreID().charAt(1) == 'I')) {
					if (!currentBook.is_bStoreBook()) {
						File Path = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/mindMaps");
						File[] mindMapFiles = Path.listFiles();
						String fileName = null;
						for (File file : mindMapFiles) {
							fileName = file.getName().replace(".xml", "");
						}
						Intent intent = new Intent(getApplicationContext(), MindMapActivity.class);
						intent.putExtra("MIRootNodeText", "");
						intent.putExtra("MIOrphanNodeText", "");
						intent.putExtra("MindMapTitle", fileName);
						intent.putExtra("book", currentBook);
						startActivityForResult(intent, 1004);
					} else if (currentBook.get_bStoreID().charAt(1) == 'I') {
						Intent intent = new Intent(getApplicationContext(), MindMapPlayerActivity.class);
						intent.putExtra("book", currentBook);
						startActivity(intent);
					}
				} else {

					Intent bookViewIntent;
					String query = "select * from objects where BID='" + currentBook.getBookID() + "'order by SequentialID";
					ArrayList<Object> objectList = db.getAllObjectsFromPage(query);
					if (objectList.size() > 2 && currentBook.getDownloadURL() != null) {
						bookViewIntent = new Intent(getApplicationContext(), PreviewActivity.class);
					} else {
						bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
					}
					bookViewIntent.putExtra("Book", currentBook);
					bookViewIntent.putExtra("currentPageNo", String.valueOf(currentBook.get_lastViewedPage()));
					bookViewIntent.putExtra("Shelf", gridShelf);
					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
				}

			}
			else {


				try {


					JSONObject object = new JSONObject(bookUrll);
					String storeBookId= object.getString("book_id");
					String bookUrl= object.getString("download_url");
					String bookTitle= object.getString("book_title");
					String descritpion= object.getString("b_description");
					String totalPages= object.getString("total_pages");
					String categoryId= object.getString("b_category_id");
					String language= object.getString("book_langugae");
					String bookEditable= object.getString("is_editable");
					String Price= object.getString("price");
					String imageUrl= object.getString("image_url");
					String domainURL= object.getString("domain_uri");


					String bookDetails = totalPages + "#" + storeBookId + "#" + bookUrl + "#" + bookTitle + "#" + descritpion + "#" + 6 + "#" + totalPages + "#" +storeBookId /*jsonObject1.getString("storeID")*/ + "#" + categoryId + "#" + language + "#" + "downloading" /*jsonObject1.getString("downloadCompleted")*/ + "#" + bookUrl + "#" + bookEditable  /*jsonObject1.getString("bookEditable")*/ + "#" + Price + "#" + imageUrl + "#" + "free"+ "#" +domainURL;


					GDriveExport gdrive = new GDriveExport(MainActivity.this, bookDetails,"MQTT");
					gdrive.downloadBook();

				}catch (Exception e){
					Log.e("download exception",e.toString());
				}

				Toast.makeText(MainActivity.this,"Please Wait",Toast.LENGTH_LONG).show();
			}
		}catch (Exception e){
			Log.e("ex",e.toString());
		}
	}

	public boolean checkStorBookstatus(String storeBookID){
		ArrayList<Object> bookList = this.gridShelf.bookListArray;
		for (int i = 0; i<bookList.size(); i++) {
			Book book = (Book) bookList.get(i);
			if (book.is_bStoreBook()) {
				if (book.get_bStoreID().equals(storeBookID)&&!book.getIsDownloadCompleted().equals("downloading")) {
					return true;
				}
			}
		}
		return false;
	}



	public class MyBroadcastReceiver extends BroadcastReceiver {
		public static final String ACTION = "com.example.ACTION_SOMETHING";
		@Override
		public void onReceive(Context context, Intent intent) {
			isDownload=true;
			bookId = intent.getStringExtra("bookId");
			gridShelf.reloadCategoryListArray();
			gridView.invalidateViews();
			loadGridView();
		}
	}
	@Override
	public void onResume() {
		super.onResume();
		InAppCommunication();
		app.setMainState(true);




		app.setState(false);
		ManahijApp.getInstance().getPrefManager().addIsinBackground(false);
		this.registerReceiver(receiver, new IntentFilter(MyBroadcastReceiver.ACTION));
	}
	private void InAppCommunication() {
		LocalBroadcastManager.getInstance(this).registerReceiver(msgreceiver, new IntentFilter(FILTER));

	}
	@Override
	public void onPause() {
		super.onPause();
		this.unregisterReceiver(receiver);
		UnregisterInAppCommunication();
          app.setMainState(false);


       app.setState(false);
	}





	BroadcastReceiver msgreceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {


			try {
				String message = intent.getStringExtra(Constants.KEYMESSAGE);
				JSONObject object = new JSONObject(message);

				switch (object.getString("action")) {
					case Constants.KEYOPENBOOK:
						lastViewdPage = Integer.parseInt(object.getString("last_viewed_page"));
						ManahijApp.getInstance().getPrefManager().addlastViewedPage(object.getString("last_viewed_page"));
						bookClicked(object.getString("book_id"), object.getString("url"), false, null);
						break;
					case Constants.KEYPAGECHANGE:
						lastViewdPage = Integer.parseInt(object.getString("pagenumber"));
						ManahijApp.getInstance().getPrefManager().addlastViewedPage(object.getString("pagenumber"));
						bookClicked(object.getString("book_id"), object.getString("url"), false, null);
						Log.e("e","pagechange");
						break;

				}



			}
			catch (Exception e){
				Log.e("rc",e.toString());
			}
		}
	};




	private void UnregisterInAppCommunication() {
		LocalBroadcastManager.getInstance(this).unregisterReceiver(msgreceiver);


	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		try {
			JSONObject object = new JSONObject();
			object.put("type", Constants.KEYCALLCONNECTION);
			object.put("action",Constants.KEYCALLDISCONNECTIONREQUEST);
			object.put("device_id",ManahijApp.getInstance().getPrefManager().getDeviceID());

			service.publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(), object.toString(), MainActivity.this);
			service.sendUsersList(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(), String.valueOf(0),MainActivity.this);
			service.unSubScribeTopic(ManahijApp.getInstance().getPrefManager().getMainChannel(), MainActivity.this);
			service.subscribeToNewTopic(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(), MainActivity.this);
			ManahijApp.getInstance().getPrefManager().addIsMainChannel(false);
			stopService(new Intent(MainActivity.this, CallConnectionStatusService.class));
			app.setCallState(false);
		}catch (Exception e){
			Log.e("MainDEstroy",e.toString());
		}
	}
}
