package com.semanoor.manahij;

/**
 * Created by karthik on 19-08-2016.
 */
import android.app.Activity;
import android.content.ContentValues;
import android.os.AsyncTask;

import com.google.android.gms.drive.query.Query;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAuthIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.ChildList;
import com.google.api.services.drive.model.ChildReference;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.Property;
import com.semanoor.source_sboookauthor.Globals;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class REST { private REST() {}
    interface ConnectCBs {
        void onConnFail(Exception ex);
        void onConnOK();
    }
    private static Drive mGOOSvc;
    private static ConnectCBs mConnCBs;
    private static boolean mConnected;

    /************************************************************************************************
     * initialize Google Drive Api
     * @param act   activity context
     */
    static boolean init(Activity act){                    //UT.lg( "REST init " + email);
        if (act != null) try {
            String email = UT.AM.getEmail();
            if (email != null) {
                mConnCBs = (ConnectCBs)act;
                mGOOSvc = new Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(),
                        GoogleAccountCredential.usingOAuth2(UT.acx, DriveScopes.DRIVE)
                                .setSelectedAccountName(email)
                ).build();
                return true;
            }
        } catch (Exception e) {UT.le(e);}
        return false;
    }
    /**
     * connect
     */
    static void connect() {
        if (UT.AM.getEmail() != null && mGOOSvc != null) {
            mConnected = false;
            new connectGdrive().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }
    /**
     * disconnect    disconnects GoogleApiClient
     */
    static void disconnect() {}

    /************************************************************************************************
     * find file/folder in GOODrive
     * @param prnId   parent ID (optional), null searches full drive, "root" searches Drive root
     * @param titl    file/folder name (optional)
     * @param mime    file/folder mime type (optional)
     * @return        arraylist of found objects
     */
    static ArrayList<File> search(String prnId, String titl, String mime,boolean myFiles) {
        ArrayList<File> gfs = new ArrayList<>();
        if (mGOOSvc != null && mConnected) try {
            // add query conditions, build query
            String qryClause = "'me' in owners and ";
            if (prnId != null) qryClause += "'" + prnId + "' in parents and ";
            if (titl != null) qryClause += "title = '" + titl + "' and ";
            if (mime != null) qryClause += "mimeType = '" + mime + "' and ";
            qryClause = qryClause.substring(0, qryClause.length() - " and ".length());
            Drive.Files.List qry;
            if (myFiles){
               qry = mGOOSvc.files().list().setQ(qryClause);
            }else{
               qry = mGOOSvc.files().list().setQ("sharedWithMe");
            }
            String npTok = null;
            if (qry != null) do {
                FileList gLst = qry.execute();
                if (gLst != null) {
                    for (File gFl : gLst.getItems()) {
                        if (gFl.getLabels().getTrashed()) continue;
                        gfs.add(gFl);
                    }                                                                 //else UT.lg("failed " + gFl.getTitle());
                    npTok = gLst.getNextPageToken();
                    qry.setPageToken(npTok);
                }
            } while (npTok != null && npTok.length() > 0);                     //UT.lg("found " + vlss.size());
        } catch (Exception e) { UT.le(e); }
        return gfs;
    }

    static ArrayList<File> subFiles(String prnId) {
        ArrayList<File> gfs = new ArrayList<>();
        if (mGOOSvc != null && mConnected) try {
            // add query conditions, build query
            String qryClause = "";
            if (prnId != null) qryClause += "'" + prnId + "' in parents and ";
            qryClause = qryClause.substring(0, qryClause.length() - " and ".length());
            Drive.Files.List qry;
            qry = mGOOSvc.files().list().setQ(qryClause);
            String npTok = null;
            if (qry != null) do {
                FileList gLst = qry.execute();
                if (gLst != null) {
                    for (File gFl : gLst.getItems()) {
                        if (gFl.getLabels().getTrashed()) continue;
                        gfs.add(gFl);
                    }                                                                 //else UT.lg("failed " + gFl.getTitle());
                    npTok = gLst.getNextPageToken();
                    qry.setPageToken(npTok);
                }
            } while (npTok != null && npTok.length() > 0);                     //UT.lg("found " + vlss.size());
        } catch (Exception e) { UT.le(e); }
        return gfs;
    }

    /************************************************************************************************
     * create file/folder in GOODrive
     * @param prnId  parent's ID, (null or "root") for root
     * @param titl  file name
     * @return      file id  / null on fail
     */
    static File createFolder(String prnId, String titl) {
        String rsId = null;
        File meta = null;
        File gFl = null;
        if (mGOOSvc != null && mConnected && titl != null) try {
            meta= new File();
            meta.setParents(Collections.singletonList(new ParentReference().setId(prnId == null ? "root" : prnId)));
            meta.setTitle(titl);
            meta.setMimeType(UT.MIME_FLDR);

            try { gFl = mGOOSvc.files().insert(meta).execute();
            } catch (Exception e) { UT.le(e); }
            if (gFl != null && gFl.getId() != null) {
                rsId = gFl.getId();
            }
            meta.setId(rsId);
        } catch (Exception e) { UT.le(e); }
        return gFl;
    }

    /************************************************************************************************
     * create file/folder in GOODrive
     * @param prnId  parent's ID, (null or "root") for root
     * @param titl  file name
     * @param mime  file mime type
     * @param file  file (with content) to create
     * @return      file id  / null on fail
     */
    static File createFile(String prnId, String titl, String mime, java.io.File file) {
        String rsId = null;
        File meta=null;
        File gFl = null;
        if (mGOOSvc != null && mConnected && titl != null && mime != null && file != null) try {
            meta= new File();
            meta.setParents(Collections.singletonList(new ParentReference().setId(prnId == null ? "root" : prnId)));
            meta.setTitle(titl);
            meta.setMimeType(mime);
            gFl = mGOOSvc.files().insert(meta, new FileContent(mime, file)).execute();
            if (gFl != null)
                rsId = gFl.getId();
            meta.setId(rsId);
        } catch (Exception e) {
            UT.le(e);
        }
        return gFl;
    }

     static void downloadFile(String downloadUrl,String fileID,OutputStream outputStream) {
        if (mGOOSvc != null && mConnected) {
            try {
                if (downloadUrl!=null) {
                  mGOOSvc.files().get(fileID)
                            .executeMediaAndDownloadTo(outputStream);
                }else{
                    mGOOSvc.files().export(fileID, "application/pdf")
                            .executeMediaAndDownloadTo(outputStream);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /************************************************************************************************
     * get file contents
     * @param resId  file driveId
     * @return       file's content  / null on fail
     */
    static String downloadUrl(String resId) {
        if (mGOOSvc != null && mConnected && resId != null) try {
            File gFl = mGOOSvc.files().get(resId).setFields("downloadUrl").execute();
            if (gFl != null){
                return gFl.getDownloadUrl();
            }
        } catch (Exception e) { UT.le(e); }
        return null;
    }

    static void download(String fileId){
        if (mGOOSvc != null && mConnected && fileId != null) try {
            OutputStream outputStream = new FileOutputStream(Globals.TARGET_BASE_FILE_PATH+"temp.pdf");
            mGOOSvc.files().export(fileId, "application/pdf")
                    .executeMediaAndDownloadTo(outputStream);
            System.out.println("fine");
        } catch (Exception e) { UT.le(e); }
    }

    /*
     * @param resId  file  id
     * @param titl  new file name (optional)
     * @param mime  new mime type (optional, "application/vnd.google-apps.folder" indicates folder)
     * @param file  new file content (optional)
     * @return      file id  / null on fail
     */
    static File update(String resId, String titl, String mime, String desc, java.io.File file,Permission permission){
        File gFl = null;
        if (mGOOSvc != null && mConnected && resId != null) try {
            File meta = new File();
            if (titl != null) meta.setTitle(titl);
            if (mime != null) meta.setMimeType(mime);
            if (desc != null) meta.setDescription(desc);
            if (permission != null) meta.setUserPermission(permission);

            if (file == null)
                gFl = mGOOSvc.files().patch(resId, meta).execute();
            else
                gFl = mGOOSvc.files().update(resId, meta, new FileContent(mime, file)).execute();

        } catch (Exception e) { UT.le(e); }
        return gFl == null ? null : gFl;
    }

    /**
     * FILE / FOLDER type object inquiry
     * @param cv oontent values
     * @return TRUE if FOLDER, FALSE otherwise
     */
    static boolean isFolder(ContentValues cv) {
        String mime = cv.getAsString(UT.MIME);
        return mime != null && UT.MIME_FLDR.equalsIgnoreCase(mime);
    }
    static File getFile(String id){
        File meta=null;
        if (mGOOSvc != null && mConnected) try {
            meta = mGOOSvc.files().get(id).execute();
        } catch (Exception e) { UT.le(e); }
        return meta;
    }
     static void insertPermission(String fileId, String value, String type, String role) {
        Permission newPermisssion = new Permission();
        newPermisssion.setValue(value);
        newPermisssion.setType(type);
        newPermisssion.setRole(role);
        try {
            mGOOSvc.permissions().insert(fileId, newPermisssion).execute();
        } catch (IOException e) {
            System.out.println("An Error occured" +e);
        }
    }
    static void moveFiles(File sourcefile,File targetfile){
        try {
            List<ParentReference> parentRef = sourcefile.getParents();
            ParentReference parentReference = parentRef.get(0);
            String fileId = parentReference.getId();
            mGOOSvc.files().update(sourcefile.getId(),null)
                    .setAddParents(targetfile.getId())
                    .setRemoveParents(fileId)
                    .setFields("id ,parents")
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static File copyFile(String fileId,String title,String parentId){
        File copiedFile = new File();
        copiedFile.setTitle(title);
        ParentReference parentReference = new ParentReference();
        parentReference.setId(parentId);
        List<ParentReference> listParent = new ArrayList<>();
        listParent.add(parentReference);
        copiedFile.setParents(listParent);
        try {
            return mGOOSvc.files().copy(fileId,copiedFile).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    static void deleteFile(String fileId){
        try {
            mGOOSvc.files().delete(fileId).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static class connectGdrive extends AsyncTask<Void,Void,Exception>{

        @Override
        protected Exception doInBackground(Void... params) {
            try {
                // GoogleAuthUtil.getToken(mAct, email, DriveScopes.DRIVE_FILE);   SO 30122755
                mGOOSvc.files().get("root").setFields("title").execute();
                mConnected = true;
            } catch (UserRecoverableAuthIOException uraIOEx) {  // standard authorization failure - user fixable
                return uraIOEx;
            } catch (GoogleAuthIOException gaIOEx) {  // usually PackageName /SHA1 mismatch in DevConsole
                return gaIOEx;
            } catch (IOException e) {   // '404 not found' in FILE scope, consider connected
                if (e instanceof GoogleJsonResponseException) {
                    if (404 == ((GoogleJsonResponseException) e).getStatusCode())
                        mConnected = true;
                }
            } catch (Exception e) {  // "the name must not be empty" indicates
                UT.le(e);           // UNREGISTERED / EMPTY account in 'setSelectedAccountName()' above
            }
            return null;
        }

        @Override
        protected void onPostExecute(Exception e) {
            super.onPostExecute(e);
            if (mConnected) {
                mConnCBs.onConnOK();
            } else {  // null indicates general error (fatal)
                mConnCBs.onConnFail(e);
            }
        }
    }
    static Drive.Files.Insert uploadFile(String prnId, String titl, String mime, InputStreamContent file) {
        File meta=null;
        Drive.Files.Insert gFl = null;
        if (mGOOSvc != null && mConnected && titl != null && mime != null && file != null) try {
            meta= new File();
            meta.setParents(Collections.singletonList(new ParentReference().setId(prnId == null ? "root" : prnId)));
            meta.setTitle(titl);
            meta.setMimeType(mime);
            gFl = mGOOSvc.files().insert(meta, file);
        } catch (Exception e) {
            UT.le(e);
        }
        return gFl;
    }
}