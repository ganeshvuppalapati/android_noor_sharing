package com.semanoor.manahij.mqtt.datamodels;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.semanoor.manahij.MainActivity;

/**
 * Created by semanoor on 18/04/17.
 */

public class MyPreferenceManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    Context _context;


    public MyPreferenceManager(Context context) {
        this._context = context;
        pref =  PreferenceManager.getDefaultSharedPreferences(context);
        editor = pref.edit();
    }
  public void addContactTopictoPublish(String emailId){
      editor.putString(Constants.OTHERTOPICNAME,emailId);
      editor.commit();
  }
    public String getContactTopictoPublish(){
        return pref.getString(Constants.OTHERTOPICNAME, null);
    }

    public void addIsMainChannel(boolean channel){
        editor.putBoolean(Constants.ISMAINCHANNEL,channel);
        editor.commit();
    }
    public boolean getISMainChannel(){
        return pref.getBoolean(Constants.ISMAINCHANNEL,false);
    }


    public void addIsinBackground(boolean isbackground){
        editor.putBoolean(Constants.ISBACKGROUND,isbackground);
        editor.commit();
    }
    public boolean getIsinBackground(){
        return pref.getBoolean(Constants.ISBACKGROUND,false);
    }
    public void addMainChannel(String channel){
        editor.putString(Constants.MAINCHANNEL,channel);
        editor.commit();
    }
    public String getMainChannel(){
        return pref.getString(Constants.MAINCHANNEL, null);
    }

    public void setBookOpend(boolean open){
        editor.putBoolean("Bookopend",open);
        editor.commit();
    }
    public boolean isBookOpend(){
        return pref.getBoolean("Bookopend", false);
    }
    public void addSubscribeTopicName(String emailId){
        editor.putString(Constants.SUBSCRIBETOPICNAME,emailId);
        editor.commit();
    }
    public String getSubscribeTopicName(){
        return pref.getString(Constants.SUBSCRIBETOPICNAME, null);
    }
    public void addClientName(String client){
        editor.putString(Constants.CLIENT_IDENTITY,client);
        editor.commit();
    }
    public String getClientName(){
        return pref.getString(Constants.CLIENT_IDENTITY, null);
    }
    public void addDeviceId(String androidId){
        editor.putString(Constants.ANDROID_ID,androidId);
        editor.commit();
    }
    public String getDeviceID(){
        return pref.getString(Constants.ANDROID_ID, null);
    }
    public void clear() {
        editor.clear();
        editor.commit();
    }

    public void addlastViewedPage(String lastiewdnumber) {

        editor.putString(Constants.lastviewdpage,lastiewdnumber);
        editor.commit();
    }
    public String getlastViewedPage(){
        return pref.getString(Constants.lastviewdpage, null);
    }
}
