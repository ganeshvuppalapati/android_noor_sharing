package com.semanoor.manahij;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.advAdapter;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Krishna on 05-10-2015.
 */
public class AdvanceSearchDialog {


    String[] advSearch = {"Google", "Wikipedia", "Youtube", "Yahoo", "Bing", "Ask"};
    Elesson elesson;
   // ArrayList<String> SearchList = new ArrayList<String>();
    ArrayList<HashMap<String,String>> SearchList = new ArrayList<HashMap<String,String>>();
    public AdvanceSearchDialog(Elesson elesson_Activity) {
        elesson = elesson_Activity;

    }

    /*
     *  Global Search
     */
    public void showGlobalSearchWindow() {
        final PopoverView popView = new PopoverView(elesson, R.layout.elesson_advsearch);
        popView.setContentSizeForViewInPopover(new Point((int) elesson.getResources().getDimension(R.dimen.elesson_search_width), (int) elesson.getResources().getDimension(R.dimen.elesson_search_height)));
        popView.showPopoverFromRectInViewGroup(elesson.rootView, PopoverView.getFrameForView(elesson.btnGlobalSearch), PopoverView.PopoverArrowDirectionUp, true);
        ListView advListView = (ListView) popView.findViewById(R.id.listViewAdv);
        final EditText etAdv = (EditText) popView.findViewById(R.id.editTextAdv);

        advListView.setDividerHeight(1);
        // advListView.setAdapter(new advAdapter(elesson));
        advListView.setAdapter(new advAdapter(elesson, advSearch));
        advListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long l) {
                String text = etAdv.getText().toString();
                String percentEscpe = Uri.encode(text);
                String url = null;
//                if (position == 0) {
//                    url = "http://projects.nooor.com/SemaBooksSearch/BkSearch.aspx?Srch=" + percentEscpe + "";
//                }
                 if (position == 0) {
                    url = "http://www.google.com/search?q=" + percentEscpe + "";

                } else if (position == 1) {
                    String language = detectLanguage(text);
                    if (language.equals("ar")) {
                        url = "http://ar.wikipedia.org/wiki/" + percentEscpe + "";
                    } else {
                        url = "http://en.wikipedia.org/wiki/" + percentEscpe + "";
                    }
                } else if (position == 2) {
                    url = "http://m.youtube.com/#/results?q=" + percentEscpe + "";

                } else if (position == 3) {
                    url = "http://search.yahoo.com/search?p=" + percentEscpe + "";

                } else if (position == 4) {
                    url = "http://www.bing.com/search?q=" + percentEscpe + "";

                } else if (position == 5) {
                    url = "http://www.ask.com/web?q=" + percentEscpe + "";
                }
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                elesson.startActivity(intent);
                popView.dissmissPopover(true);
            }
        });
    }

    /*
     *  search Dialog for searching the text
     */
    public void wordSearchDialog() {
        final PopoverView popView = new PopoverView(elesson, R.layout.elesson_advsearch);
        popView.setContentSizeForViewInPopover(new Point((int) elesson.getResources().getDimension(R.dimen.elesson_search_width),(int) elesson.getResources().getDimension(R.dimen.elesson_search_height)));
        popView.showPopoverFromRectInViewGroup(elesson.rootView, PopoverView.getFrameForView(elesson.btn_Search), PopoverView.PopoverArrowDirectionUp, true);
        final ListView advListView = (ListView) popView.findViewById(R.id.listViewAdv);
        final EditText etAdv = (EditText) popView.findViewById(R.id.editTextAdv);
        if (SearchList.size() > 0) {
            SearchList.clear();
        }
        advListView.setAdapter(new bookSearchAdapter(elesson));
        etAdv.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filteringSearchContent(etAdv.getText().toString());
                advListView.invalidateViews();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        advListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long l) {
                HashMap<String, String> map = SearchList.get(position);
                String Url=map.get("Url");
                String Id=Url.substring(0, Url.lastIndexOf('.'));
                elesson.selectingPageInListView(Id);
                popView.dissmissPopover(true);
            }
        });
    }


    /*
     * Filtering the Search text
     */
    public void filteringSearchContent(String text) {
        if (SearchList.size() > 0) {
            SearchList.clear();
        }
        for (int i = 0; i < elesson.BookSearch.size(); i++) {
            HashMap<String, String> map = elesson.BookSearch.get(i);
                                              String Data = map.get("Data");
            String Url = map.get("Url");
            if (Data.toLowerCase().contains(text.toLowerCase())) {
                for (int j = 0; j < elesson.previewlist.size(); j++) {
                    RAdataobject previewlist = elesson.previewlist.get(j);
                    if (previewlist.getPath().equals(Url)) {
                        map.put("Name",previewlist.getName());
                        SearchList.add(map);
                    }
                }
            }

        }

    }

    /*
     * Displaying pages containing Search text
     */
    private class bookSearchAdapter extends BaseAdapter {


        public bookSearchAdapter(Elesson elesson) {
        }

        //@Override
        public int getCount() {

            return SearchList.size();
        }

        //@Override
        public Object getItem(int position) {

            return null;
        }

        //@Override
        public long getItemId(int position) {

            return 0;
        }

        //@Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            if (convertView == null) {
                vi = elesson.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
            }
            TextView textView = (TextView) vi.findViewById(R.id.textView1);
            HashMap<String, String> map =SearchList.get(position);
            textView.setText(map.get("Name"));

            return vi;
        }

    }

    /*
     * Detecting the keyboard language
     */
    public static String detectLanguage(String selectedText) {

        String alpha = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
        String[] alphaArray = alpha.split(",");
        for (int i = 0; i < alphaArray.length; i++) {
            String current = alphaArray[i];
            if (selectedText.contains(current)) {
                return "en";
            }
        }
        return "ar";
    }

    /*
     *  Adding page related content in array list
    */
    public void processXML() {
        if (elesson.BookSearch.size() > 0) {
            elesson.BookSearch.clear();
        }
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {
                public void startElement(String uri, String localName, String qName,
                                         Attributes attributes) throws SAXException {
                    if (qName.contains("Find")) {
                        HashMap<String, String> map = new HashMap<>();
                        String attrValue = attributes.getValue("Data");
                        String attrUrl = attributes.getValue("Url");
                        // String pageId=attrUrl.substring(0,attrUrl.lastIndexOf('.'));
                        if (attrValue != null) {
                            map.put("Url", attrUrl);
                            map.put("Data", attrValue);
                            elesson.BookSearch.add(map);
                        }
                    }
                }

            };

            String filePath = elesson.currentbookpath + "Data/FxdTls/Search.xml";
            //filePath = sdPath+"/"+bookName+"Book/Search.xml";
            InputStream inputStream = new FileInputStream(filePath);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");

            saxParser.parse(is, handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
