package com.semanoor.manahij;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

/**
 * Created by karthik on 12-09-2016.
 */
public class CustomRecyclerView extends RecyclerView {
    Context context;
    public CustomRecyclerView(Context context) {
        super(context);
        this.context = context;
    }
    public CustomRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public CustomRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (context instanceof MainActivity){
            if (!((MainActivity) context).editmode){
                return super.onInterceptTouchEvent(event);
            }
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (context instanceof MainActivity){
            if (!((MainActivity) context).editmode){
                return super.onTouchEvent(ev);
            }
        }
        return false;
    }
}