package com.semanoor.manahij;

import java.util.ArrayList;

/**
 * Created by karthik on 08-10-2016.
 */

public class StoreCategory {
    private int id;
    private int active;
    private String arbName;
    private String engName;
    private String createdOn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getArbName() {
        return arbName;
    }

    public void setArbName(String arbName) {
        this.arbName = arbName;
    }

    public String getEngName() {
        return engName;
    }

    public void setEngName(String engName) {
        this.engName = engName;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

}
