package com.semanoor.manahij;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ptg.views.CircleButton;
import com.semanoor.manahij.mqtt.CallConnectionStatusService;
import com.semanoor.manahij.mqtt.CallbackInterface;
import com.semanoor.manahij.mqtt.MQTTSubscriptionService;
import com.semanoor.manahij.mqtt.NoorActivity;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.MyContacts;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FlashCardActivity extends NoorActivity implements View.OnClickListener{

    public static final String FILTER = "noor.flash.filter";
    private DatabaseHandler db;
    private CardContainer mCardContainer;
    private Book currentBook;
    public ArrayList<NoteData> noteDataArrayList;
    private ArrayList<NoteData> tempNoteDataArrayList = new ArrayList<>();
   // private boolean allPageClicked;
    private int noteSelection;
    private TextView tv_StudyCardsAlert;
    private int CC;
    private ViewPager noteViewPager;
    private Button note_countbtn1,note_countbtn2,note_countbtn3,notwell_btn,well_btn,vrywell_btn,count_btn;
    private ImageView noteColor1,noteColor2,noteColor3,noteColor4,noteColor5,noteColor6;
    private int currentPageNumber;
    public Integer[] cardImgId = {R.color.yellow,R.color.green,R.color.skyblue,R.color.pink,R.color.lavender,R.color.white};
    private SimpleCardStackAdapter adapter;
    private boolean fromBook;
    private String storeId;
     MQTTSubscriptionService service;
    RadioButton btnCurrnt,btnAll,inCall;
    String mainChannel=null;
    RelativeLayout card_layout;

    SegmentedRadioButton segmentedRadioGroup;
    RadioGroup.OnCheckedChangeListener checkedChangeListener;
  ManahijApp app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        app= (ManahijApp)getApplicationContext();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_flash_card);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = (int) (Globals.getDeviceWidth() / 1.2);
        params.height = (int) (Globals.getDeviceHeight() / 1.4);
        this.getWindow().setAttributes(params);
        db = DatabaseHandler.getInstance(this);
        service = new MQTTSubscriptionService();
        mainChannel=ManahijApp.getInstance().getPrefManager().getMainChannel();

        currentBook = (Book) getIntent().getSerializableExtra("Book");
        currentPageNumber = getIntent().getIntExtra("pagenumber",0);
        fromBook = getIntent().getBooleanExtra("fromBook",false);
        noteViewPager = (ViewPager) findViewById(R.id.viewPager);
        if (currentBook!=null){
            storeId = currentBook.get_bStoreID();
        }else{
            storeId = "0";
        }
        noteDataArrayList = db.loadNoteDivToArray(storeId);
        for (int i=0;i<noteDataArrayList.size();i++){
            tempNoteDataArrayList.add(noteDataArrayList.get(i));
        }
        ViewPagerNoteAdapter viewPageradapter = new ViewPagerNoteAdapter();
        noteViewPager.setAdapter(viewPageradapter);
        noteViewPager.setOffscreenPageLimit(1);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.all_btn){
            publichNotesBasedOnColors(0);
            loadAllNotesInCurrentPage();
        } else if (view.getId() == R.id.imgBtnC1){
            publichNotesBasedOnColors(1);
            loadNotesBasedOnColors(1);
        } else if (view.getId() == R.id.imgBtnC2){
            publichNotesBasedOnColors(2);
            loadNotesBasedOnColors(2);
        } else if (view.getId() == R.id.imgBtnC3){
            publichNotesBasedOnColors(3);
            loadNotesBasedOnColors(3);
        } else if (view.getId() == R.id.imgBtnC4){
            publichNotesBasedOnColors(4);
            loadNotesBasedOnColors(4);
        } else if (view.getId() == R.id.imgBtnC5){
            publichNotesBasedOnColors(5);
            loadNotesBasedOnColors(5);
        } else if (view.getId() == R.id.imgBtnC6){
            publichNotesBasedOnColors(6);
            loadNotesBasedOnColors(6);
        }else if (view.getId() == R.id.count_btn1){
            loadNotesBasedOnType(1,true);
        }else if (view.getId() == R.id.count_btn2){
            loadNotesBasedOnType(2, true);
        }else if (view.getId() == R.id.count_btn3){
            loadNotesBasedOnType(3, true);
        }else if (view.getId() == R.id.notwell_btn){
            wellReview(1,true);

        }else if (view.getId() == R.id.well_btn){


            wellReview(2, true);

        }else if (view.getId() == R.id.verywell_btn){

            wellReview(3, true);

        }
    }

    private void wellReview(int val, boolean b) {
      if(b){
          try {


                  if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                      JSONObject object = new JSONObject();
                      object.put("type", Constants.FLASHCARD);
                      object.put("action", Constants.KEYWELLREVIEW);
                      object.put("review_id", val);
                      object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                      service.publishMsg(mainChannel, object.toString(), FlashCardActivity.this);

              }
          }catch (Exception e){
              Log.e("ex",e.toString());
          }
      }

        if (noteDataArrayList.size()>0) {
            int position = mCardContainer.cardPosition;
            if (position == -1) {
                position = 0;
            }
            NoteData data = noteDataArrayList.get(position);
            if (data.getNoteType() != val) {
                data.setNoteType(val);
                String query = "update tblNote set NoteType='" + val + "'  where BName='" + storeId + "' and PageNo='" + data.getNotePageNo() + "' and TabNo='" + data.getNoteTabNo() + "' and SText='" + data.getNoteTitle() + "' and SDesc='" + data.getNoteDesc() + "'";
                db.executeQuery(query);
                setBackgroudColorForButtons(position);
                updateButtons();
            }
        }
    }

    private void publichNotesBasedOnColors(int i) {
        try{
                if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                    JSONObject obj = new JSONObject();
                    obj.put("type", Constants.FLASHCARD);
                    obj.put("action", Constants.FLASHCOLORSELECION);
                    obj.put("color_id", i);
                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    service.publishMsg(mainChannel, obj.toString(), FlashCardActivity.this);
                }

        }catch (Exception e){
Log.e("color",e.toString());
        }
    }





    public void publishFlshAnimation(int cardPosition, float e1, float e2, float velocityX, float velocityY) {

        try{

                if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                    JSONObject obj = new JSONObject();
                    obj.put("type", Constants.FLASHCARD);
                    obj.put("action", Constants.FLASHANIMATION);
                    obj.put("card_position", cardPosition);
                    obj.put("x1", e1);
                    obj.put("x2", e2);
                    obj.put("velocity_x", velocityX);
                    obj.put("velocity_y", velocityY);
                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    service.publishMsg(mainChannel, obj.toString(), FlashCardActivity.this);
                }

        }catch (Exception e){
            Log.e("color",e.toString());
        }

    }




    public class ViewPagerNoteAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = null;
            switch (position){
                case 0:
                    view = inflater.inflate(R.layout.card_layout, null);
                    ArrayList<NoteData> noteDatas = db.loadNoteDivToArray(storeId);
                    noteDataArrayList.clear();
                    noteDataArrayList = noteDatas;

                    mCardContainer = (CardContainer)view.findViewById(R.id.layoutview);
                    mCardContainer.cardPosition = 0;
                    tv_StudyCardsAlert=(TextView)view.findViewById(R.id.txt);
                   segmentedRadioGroup = (SegmentedRadioButton) view.findViewById(R.id.segment_text);
             checkedChangeListener=  new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (group == segmentedRadioGroup){

                                if (checkedId == R.id.currentPage) {
                                    //allPageClicked = false;
                                    noteSelection=1;

                                    cardSelection(1);

                                } else if (checkedId == R.id.all_page) {
                                   // allPageClicked = true;
                                    noteSelection=2;
                                    cardSelection(2);

                                }else if(checkedId == R.id.inCall){
                                    noteSelection=3;
                                    cardSelection(3);
                                }
                            }
                        }
                    };

                    if (!fromBook){
                        segmentedRadioGroup.setVisibility(View.GONE);
                    }

                    btnCurrnt = (RadioButton)view.findViewById(R.id.currentPage) ;
                    btnAll = (RadioButton)view.findViewById(R.id.all_page) ;
                    inCall = (RadioButton)view.findViewById(R.id.inCall) ;
                    if(!ManahijApp.getInstance().getPrefManager().getISMainChannel()){
                        inCall.setVisibility(View.GONE);
                        StudyCardsAdapter(noteDatas);
                    }else {
                        inCall.setChecked(true);
                        noteSelection=3;
                        ArrayList<NoteData> noteData = db.loadNoteInCallDivToArray(storeId);
                        StudyCardsAdapter(noteData);
                    }
                    segmentedRadioGroup.setOnCheckedChangeListener(checkedChangeListener);
                    initializeDialogLayouts(view);

                    updateButtons();
                    if (noteDataArrayList.size()>0) {
                        setBackgroudColorForButtons(0);
                    }
                    count_btn.setText(""+noteDataArrayList.size());
                    Button add_btn = (Button) view.findViewById(R.id.btnNext);
                    add_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try {

                                if(ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                                    JSONObject object = new JSONObject();
                                    object.put("type", Constants.FLASHCARD);
                                    object.put("action", Constants.KEYADDFLASHCARD);
                                    object.put("add_flash_card", 1);
                                    object.put("all_pages", noteSelection);
                                    object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                                    service.publishMsg(mainChannel, object.toString(), FlashCardActivity.this);
                                }

                            } catch (JSONException e) {
                                Log.e("flashselection",e.toString());
                            }
                            noteViewPager.setCurrentItem(1,true);
                        }
                    });
                    CircleButton back_btn = (CircleButton) view.findViewById(R.id.btn_back);
                    back_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
             try {

                 if(ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                     JSONObject object = new JSONObject();
                     object.put("type", Constants.FLASHCARD);
                     object.put("action", Constants.KEYCLOSEFLASHCARD);
                     object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                     service.publishMsg(mainChannel, object.toString(), FlashCardActivity.this);

                 }
               }catch (Exception e){

                  }
                            finish();
                        }
                    });

                    break;
                case 1:
                    view = inflater.inflate(R.layout.add_flashcard_layout, null);
                    CircleButton back_btn1 = (CircleButton) view.findViewById(R.id.btn_back);
                    back_btn1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                               // if(ManahijApp.getInstance().isNetworkStatus()) {
                                    if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                                        JSONObject object = new JSONObject();
                                        object.put("type", Constants.FLASHCARD);
                                        object.put("action", Constants.KEYADDFLASHCARD);
                                        object.put("add_flash_card", 0);
                                        object.put("all_pages", noteSelection);
                                        object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                                        service.publishMsg(mainChannel, object.toString(), FlashCardActivity.this);
                                   // }
                                }
                            } catch (JSONException e) {
                                Log.e("flashselection",e.toString());
                            }
                            noteViewPager.setCurrentItem(0,true);
                            loadStudyCards(noteSelection);
                        }
                    });
                    card_layout = (RelativeLayout) view.findViewById(R.id.card_container);
                    final EditText title_txt = (EditText) view.findViewById(R.id.title_edittext);
                    final EditText desc_txt = (EditText) view.findViewById(R.id.description_edittext);
                    Button turn_btn = (Button) view.findViewById(R.id.btn_turn);
                    turn_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FlipAnimation flipAnimation = new FlipAnimation(title_txt, desc_txt);

                            if (title_txt.getVisibility() == View.GONE)
                            {
                                flipAnimation.reverse();
                            }
                            try{
                               // if(ManahijApp.getInstance().isNetworkStatus()) {
                                if(ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                                    JSONObject obj = new JSONObject();
                                    obj.put("type", Constants.FLASHCARD);
                                    obj.put("action", Constants.KEYFLASHNOTETURN);
                                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                                    service.publishMsg(mainChannel, obj.toString(), FlashCardActivity.this);
                               // }
                                }
                            }catch (Exception e){

                            }

                            card_layout.startAnimation(flipAnimation);
                        }
                    });
                    Button btn_add = (Button) view.findViewById(R.id.btnNext);
                    btn_add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (title_txt.getText().length()>0){
                                int colorValue = CC+1;
                                try{
                                  //  if(ManahijApp.getInstance().isNetworkStatus()) {
                                        if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                                            JSONObject obj = new JSONObject();
                                            obj.put("type", Constants.FLASHCARD);
                                            obj.put("action", Constants.KEYFLASHADDNOTE);
                                            obj.put("color", colorValue);
                                            obj.put("description", desc_txt.getText().toString());
                                            obj.put("title", title_txt.getText().toString());
                                            obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                                            service.publishMsg(mainChannel, obj.toString(), FlashCardActivity.this);
                                       // }
                                    }
                                }catch (Exception e){

                                }
                                addNote(storeId,title_txt.getText().toString(),desc_txt.getText().toString(),colorValue);


                                // updateNotesCount();
                                title_txt.setText("");
                                desc_txt.setText("");
                            }
                        }
                    });
                    ImageView iv_nColor1 = (ImageView) view.findViewById(R.id.imgBtnC1);
                    iv_nColor1.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 0;
                            publishAddColortoLayout(CC);
                            card_layout.setBackgroundResource(R.drawable.notepop_bg1);
                        }
                    });
                    ImageView iv_nColor2 = (ImageView) view.findViewById(R.id.imgBtnC2);
                    iv_nColor2.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 1;
                            publishAddColortoLayout(CC);
                            card_layout.setBackgroundResource(R.drawable.notepop_bg2);
                        }
                    });
                    ImageView iv_nColor3 = (ImageView) view.findViewById(R.id.imgBtnC3);
                    iv_nColor3.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 2;
                            publishAddColortoLayout(CC);
                            card_layout.setBackgroundResource(R.drawable.notepop_bg3);
                        }
                    });
                    ImageView iv_nColor4 = (ImageView) view.findViewById(R.id.imgBtnC4);
                    iv_nColor4.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 3;
                            publishAddColortoLayout(CC);
                            card_layout.setBackgroundResource(R.drawable.notepop_bg4);
                        }
                    });
                    ImageView iv_nColor5 = (ImageView) view.findViewById(R.id.imgBtnC5);
                    iv_nColor5.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 4;
                            publishAddColortoLayout(CC);
                            card_layout.setBackgroundResource(R.drawable.notepop_bg5);
                        }
                    });
                    ImageView iv_nColor6 = (ImageView) view.findViewById(R.id.imgBtnC6);
                    iv_nColor6.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 5;
                            publishAddColortoLayout(CC);
                            card_layout.setBackgroundResource(R.drawable.notepop_bg6);
                        }
                    });
                    break;
            }
            ((ViewPager) container).addView(view, 0);
            return view;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    private void cardSelection(int noteSelection) {

        try {
            if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                    JSONObject object = new JSONObject();
                    object.put("type", Constants.FLASHCARD);
                    object.put("action", Constants.KEYFLASHSELECTION);
                    object.put("all_pages", noteSelection);
                    object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    service.publishMsg(mainChannel, object.toString(), FlashCardActivity.this);

            }
            if (noteSelection == 3)
                inCallAdapter();
            else  loadStudyCards(noteSelection);
        } catch (JSONException e) {
            Log.e("flashselection",e.toString());
        }
    }

    private void addNote(String storeId, String title, String decription, int colorValue) {


        NoteData data = new NoteData();
        data.setNoteColor(colorValue);
        data.setNoteType(1);
        data.setNoteTabNo(0);
        data.setNotePageNo(0);
        data.setNoteTitle(title);
        data.setNoteDesc(decription);
        noteDataArrayList.add(data);
        String query;
        if(ManahijApp.getInstance().getPrefManager().getISMainChannel()){
            query = "insert into tblNote(BName,PageNo,SText,Occurence,SDesc,NPos,Color,ProcessSText,TabNo,Exported,NoteType,CallStatus) values" +
                    "('"+storeId+"','0','"+title+"','0','"+decription+"','0','"+colorValue+"','0','0','"+"0"+"','1','1')";
        }else {
            query = "insert into tblNote(BName,PageNo,SText,Occurence,SDesc,NPos,Color,ProcessSText,TabNo,Exported,NoteType,CallStatus) values" +
                    "('"+storeId+"','0','"+title+"','0','"+decription+"','0','"+colorValue+"','0','0','"+"0"+"','1','0')";
        }
        db.executeQuery(query);
    }

    private void publishAddColortoLayout(int CC) {

        try{
            if(ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                JSONObject obj = new JSONObject();
                obj.put("type", Constants.FLASHCARD);
                obj.put("action", Constants.KEYFLASHCOLORSELECION);
                obj.put("color_id", CC);
                obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                service.publishMsg(mainChannel, obj.toString(), FlashCardActivity.this);
            }
        }catch (Exception e){

        }
    }

    private void initializeDialogLayouts(View dialog){
        count_btn = (Button) dialog.findViewById(R.id.btn_text);
        note_countbtn1 = (Button) dialog.findViewById(R.id.count_btn1);
        note_countbtn1.setOnClickListener(this);
        note_countbtn2 = (Button) dialog.findViewById(R.id.count_btn2);
        note_countbtn2.setOnClickListener(this);
        note_countbtn3 = (Button) dialog.findViewById(R.id.count_btn3);
        note_countbtn3.setOnClickListener(this);
        notwell_btn = (Button) dialog.findViewById(R.id.notwell_btn);
        notwell_btn.setOnClickListener(this);
        well_btn = (Button) dialog.findViewById(R.id.well_btn);
        well_btn.setOnClickListener(this);
        vrywell_btn = (Button) dialog.findViewById(R.id.verywell_btn);
        vrywell_btn.setOnClickListener(this);
        Button all_btn = (Button) dialog.findViewById(R.id.all_btn);
        all_btn.setOnClickListener(this);
        noteColor1 = (ImageView) dialog.findViewById(R.id.imgBtnC1);
        noteColor1.setOnClickListener(this);
        noteColor2 = (ImageView) dialog.findViewById(R.id.imgBtnC2);
        noteColor2.setOnClickListener(this);
        noteColor3 = (ImageView) dialog.findViewById(R.id.imgBtnC3);
        noteColor3.setOnClickListener(this);
        noteColor4 = (ImageView) dialog.findViewById(R.id.imgBtnC4);
        noteColor4.setOnClickListener(this);
        noteColor5 = (ImageView) dialog.findViewById(R.id.imgBtnC5);
        noteColor5.setOnClickListener(this);
        noteColor6 = (ImageView) dialog.findViewById(R.id.imgBtnC6);
        noteColor6.setOnClickListener(this);
    }

    private void loadAllNotesInCurrentPage(){
        ArrayList<NoteData> allnotesInCurrentPage = new ArrayList<>();
        for (int i=0;i<tempNoteDataArrayList.size();i++){
            NoteData data = tempNoteDataArrayList.get(i);
            if (noteSelection==2){
                allnotesInCurrentPage.add(data);
            }else if(noteSelection==1){
                if (data.getNotePageNo() == currentPageNumber) {
                    allnotesInCurrentPage.add(data);
                }
            }
        }
        if(noteSelection==3){
            allnotesInCurrentPage = db.loadNoteInCallDivToArray(storeId);

        }
        noteDataArrayList.clear();
        noteDataArrayList = allnotesInCurrentPage;

        mCardContainer.cardPosition = 0;
        StudyCardsAdapter(allnotesInCurrentPage);
        updateButtons();
        if (noteDataArrayList.size()>0) {
            setBackgroudColorForButtons(0);
        }
    }
    private void loadNotesBasedOnColors(int colorValue){
        ArrayList<NoteData> allNotesBasedOnColors = new ArrayList<>();



        if(noteSelection==3){
            tempNoteDataArrayList=db.loadNoteInCallDivToArray(storeId);
        for (int i=0;i<tempNoteDataArrayList.size();i++){
            NoteData data = tempNoteDataArrayList.get(i);
            if (data.getNoteColor() == colorValue) {
                allNotesBasedOnColors.add(data);
            }
        }}else {
            for (int i = 0; i < tempNoteDataArrayList.size(); i++) {
                NoteData data = tempNoteDataArrayList.get(i);
                if (data.getNoteColor() == colorValue) {
                    allNotesBasedOnColors.add(data);
                }
            }
        }
        noteDataArrayList.clear();
        noteDataArrayList = allNotesBasedOnColors;
        mCardContainer.cardPosition = 0;

        StudyCardsAdapter(allNotesBasedOnColors);
        updateButtons();
        if (noteDataArrayList.size()>0) {
            setBackgroudColorForButtons(0);
        }
    }
    private void loadNotesBasedOnType(int type, boolean b){
  if(b) {
      try {
          if(ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
              JSONObject obj = new JSONObject();
              obj.put("type", Constants.FLASHCARD);
              obj.put("action", Constants.KEYNOTESBASEDONTYPE);
              obj.put("note_type", type);
              obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
              service.publishMsg(mainChannel, obj.toString(), FlashCardActivity.this);
          }

      } catch (Exception e) {
          Log.e("color", e.toString());
      }
  }

        ArrayList<NoteData> allNotesBasedOnTpye = new ArrayList<>();
        if(noteSelection==3){
            tempNoteDataArrayList=db.loadNoteInCallDivToArray(storeId);
            for (int i=0;i<tempNoteDataArrayList.size();i++){
                NoteData data = tempNoteDataArrayList.get(i);
                if (data.getNoteColor() == type) {
                    allNotesBasedOnTpye.add(data);
                }
            }
        }else {

            for (int i = 0; i < noteDataArrayList.size(); i++) {
                NoteData data = noteDataArrayList.get(i);
                if (data.getNoteType() == type) {
                    allNotesBasedOnTpye.add(data);
                }
            }
        }
        noteDataArrayList.clear();
        noteDataArrayList = allNotesBasedOnTpye;

        mCardContainer.cardPosition = 0;
        StudyCardsAdapter(allNotesBasedOnTpye);
        updateButtons();
        if (noteDataArrayList.size()>0) {
            setBackgroudColorForButtons(0);
        }
    }
    private void updateButtons(){
        int notwellCount = 0,wellCount = 0,verywellCount = 0;
        for (int i=0;i<noteDataArrayList.size();i++){
            NoteData data = noteDataArrayList.get(i);
            if (data.getNoteType()==1){
                notwellCount++;
            }else if (data.getNoteType()==2){
                wellCount++;
            }else if (data.getNoteType()==3){
                verywellCount++;
            }
        }
        note_countbtn1.setText(String.valueOf(notwellCount));
        note_countbtn2.setText(String.valueOf(wellCount));
        note_countbtn3.setText(String.valueOf(verywellCount));
        count_btn.setText(""+noteDataArrayList.size());
    }
    public void setBackgroudColorForButtons(int cardPosition){
        int type = 0;
        if (cardPosition < noteDataArrayList.size()) {
            NoteData data = noteDataArrayList.get(cardPosition);
            type = data.getNoteType();
            if (type == 1) {
                notwell_btn.setBackgroundResource(R.drawable.white_border_hovercolor);
                well_btn.setBackgroundResource(R.drawable.black_background);
                vrywell_btn.setBackgroundResource(R.drawable.black_background);
            } else if (type == 2) {
                notwell_btn.setBackgroundResource(R.drawable.black_background);
                well_btn.setBackgroundResource(R.drawable.white_border_hovercolor);
                vrywell_btn.setBackgroundResource(R.drawable.black_background);
            } else if (type == 3) {
                notwell_btn.setBackgroundResource(R.drawable.black_background);
                well_btn.setBackgroundResource(R.drawable.black_background);
                vrywell_btn.setBackgroundResource(R.drawable.white_border_hovercolor);
            }
        }
    }
    private void loadStudyCards(int allPage){
        switch (allPage){
            case 2:
                noteDataArrayList.clear();
                tempNoteDataArrayList.clear();
                noteDataArrayList = db.loadNoteDivToArray(storeId);
                for (int i=0;i<noteDataArrayList.size();i++){
                    tempNoteDataArrayList.add(noteDataArrayList.get(i));
                }

                StudyCardsAdapter(noteDataArrayList);
                updateButtons();
                if (noteDataArrayList.size()>0) {
                    setBackgroudColorForButtons(0);
                }
                break;
            case 1:
                noteDataArrayList.clear();
                tempNoteDataArrayList.clear();
                noteDataArrayList = db.loadNotesInCurrentPage(storeId,currentPageNumber);
                for (int i=0;i<noteDataArrayList.size();i++){
                    tempNoteDataArrayList.add(noteDataArrayList.get(i));
                }

                StudyCardsAdapter(noteDataArrayList);
                updateButtons();
                if (noteDataArrayList.size()>0) {
                    setBackgroudColorForButtons(0);
                }
                break;
            case 3:

                noteDataArrayList.clear();
                tempNoteDataArrayList.clear();
                noteDataArrayList = db.loadNoteInCallDivToArray(storeId);
                for (int i=0;i<noteDataArrayList.size();i++){
                    tempNoteDataArrayList.add(noteDataArrayList.get(i));
                }

                StudyCardsAdapter(noteDataArrayList);
                updateButtons();
                if (noteDataArrayList.size()>0) {
                    setBackgroudColorForButtons(0);
                }
                break;
        }


    }
    public void StudyCardsAdapter(ArrayList<NoteData> dataArrayList){
        //Calling the Simple Card class :
        if(dataArrayList.size()==0){
            tv_StudyCardsAlert.setVisibility(View.VISIBLE);
            tv_StudyCardsAlert.setText(getResources().getString(R.string.study_cards_alert));
        }else{
            tv_StudyCardsAlert.setVisibility(View.GONE);
        }

        Log.e("flashselection","adapter");
        adapter = new SimpleCardStackAdapter(FlashCardActivity.this, this, dataArrayList);

        android.content.res.Resources r = this.getResources();
        for(int tag =0; tag<dataArrayList.size(); tag++){
            NoteData noteData = dataArrayList.get(tag);
            int pageNo = noteData.getNotePageNo();
            int tabNo = noteData.getNoteTabNo();
            int color = noteData.getNoteColor();
            String finalPageNo;
            if(tabNo > 0){

                finalPageNo = ""+pageNo + "-" +tabNo;
            }
            else{
                finalPageNo = ""+pageNo;
            }
            if(pageNo <= 0){
                adapter.add(new CardModel("Cover page", noteData.getNoteTitle(), r.getDrawable(R.drawable.turn_bg)));

            }else{
                adapter.add(new CardModel("Page "+finalPageNo, noteData.getNoteTitle(), r.getDrawable(R.drawable.turn_bg)));
            }

        }

        mCardContainer.setAdapter(adapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
       // ManahijApp.getInstance().getPrefManager().addIsinBackground(true);
        LocalBroadcastManager.getInstance(this).registerReceiver(flshreceiver, new IntentFilter(FILTER));

    }

    BroadcastReceiver flshreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                JSONObject ojb= new JSONObject(intent.getStringExtra(Constants.KEYMESSAGE));

                switch (ojb.getString("action")){

                    case  Constants.KEYFLASHSELECTION:
                        if(ojb.getInt("all_pages")==3){
                            noteSelection=3;
                            segmentedRadioGroup.setOnCheckedChangeListener(null);
                            inCallAdapter();
                            inCall.setChecked(true);
                            segmentedRadioGroup.setOnCheckedChangeListener(checkedChangeListener);

                        }else if(ojb.getInt("all_pages")==2){
                            noteSelection=2;
                            segmentedRadioGroup.setOnCheckedChangeListener(null);
                            btnAll.setChecked(true);
                            loadStudyCards(2);
                            segmentedRadioGroup.setOnCheckedChangeListener(checkedChangeListener);

                        }else {
                            noteSelection=1;
                            segmentedRadioGroup.setOnCheckedChangeListener(null);
                            btnCurrnt.setChecked(true);
                            loadStudyCards(1);
                            segmentedRadioGroup.setOnCheckedChangeListener(checkedChangeListener);

                        }


                        break;
                    case  Constants.KEYADDFLASHCARD:

                        if( ojb.getString("add_flash_card").equals("0")) {
                            noteViewPager.setCurrentItem(0, true);
                            loadStudyCards(noteSelection);
                        }else {
                            noteViewPager.setCurrentItem(1, true);
                        }
                        break;
                    case  Constants.FLASHCOLORSELECION:
                             if(ojb.getInt("color_id")==0){
                                 loadAllNotesInCurrentPage();
                             } else {
                                 loadNotesBasedOnColors(ojb.getInt("color_id"));
                             }

                        break;
                    case  Constants.KEYCLOSEFLASHCARD:
                             finish();

                        break;
                    case  Constants.KEYFLASHCOLORSELECION:
                               switch (ojb.getInt("color_id")){
                                   case 0:
                                       CC = 0;
                                       card_layout.setBackgroundResource(R.drawable.notepop_bg1);
                                       break;
                                   case 1:
                                       CC = 1;
                                       card_layout.setBackgroundResource(R.drawable.notepop_bg2);
                                       break;
                                   case 2:
                                       CC = 2;
                                       card_layout.setBackgroundResource(R.drawable.notepop_bg3);
                                       break;
                                   case 3:
                                       CC = 3;
                                       card_layout.setBackgroundResource(R.drawable.notepop_bg4);
                                       break;
                                   case 4:
                                       CC = 4;
                                       card_layout.setBackgroundResource(R.drawable.notepop_bg5);
                                       break;
                                   case 5:
                                       CC = 5;
                                       card_layout.setBackgroundResource(R.drawable.notepop_bg6);
                                       break;

                               }

                        break;


                    case  Constants.KEYFLASHADDNOTE:

                        addNote(storeId,ojb.getString("title"),ojb.getString("description"),ojb.getInt("color"));

                        Toast.makeText(FlashCardActivity.this,"Flash Note Added",Toast.LENGTH_SHORT).show();
                        break;

                    case  Constants.KEYFLASHNOTETURN:
                        Toast.makeText(FlashCardActivity.this,"Flash Note Turned",Toast.LENGTH_SHORT).show();
                        break;
                    case  Constants.KEYFLASHTURN:

                        adapter.onUpturn(ojb.getBoolean("flip"));
                        break;

                    case  Constants.KEYWELLREVIEW:
                        wellReview(ojb.getInt("review_id"),false);
                        break;
                    case  Constants.KEYNOTESBASEDONTYPE:
                        loadNotesBasedOnType(ojb.getInt("note_type"),false);
                        break;
                    case  Constants.FLASHANIMATION:
                        mCardContainer.mtopCardAnimation(ojb.getInt("card_position"),ojb.getInt("x1"),ojb.getInt("x2"),ojb.getInt("velocity_x"),ojb.getInt("velocity_y"));

                        break;
                    case  "close_inCall":
                         db.updateInCall();
                        inCall.setVisibility(View.GONE);
                        btnAll.setChecked(true);
                        break;
                }

            }catch (Exception e){
                Log.e("Flash",e.toString());
            }


        }
    };

    private void inCallAdapter() {
        ArrayList<NoteData> noteDatas = db.loadNoteInCallDivToArray(storeId);
        noteDataArrayList = noteDatas;
        StudyCardsAdapter(noteDatas);
    }

    @Override
    protected void onPause() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(flshreceiver);
        app.setState(false);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if(ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                JSONObject object = new JSONObject();
                object.put("type", Constants.FLASHCARD);
                object.put("action", Constants.KEYCLOSEFLASHCARD);
                object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                service.publishMsg(mainChannel, object.toString(), FlashCardActivity.this);
            }
        }catch (Exception e){

        }
    }
}
