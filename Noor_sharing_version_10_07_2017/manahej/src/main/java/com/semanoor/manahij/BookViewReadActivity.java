package com.semanoor.manahij;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.WebDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.ptg.mindmap.widget.AppDict;
import com.ptg.mindmap.widget.LoadMindMapContent;
import com.ptg.mindmap.widget.VideoPopup;
import com.ptg.views.CircleButton;
import com.ptg.views.TwoDScrollView;
import com.semanoor.colorpicker.ColorsRead;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.inappbilling.util.IabResult;
import com.semanoor.inappbilling.util.Purchase;
import com.semanoor.manahij.mqtt.CallConnectionStatusService;
import com.semanoor.manahij.mqtt.MQTTSubscriptionService;
import com.semanoor.manahij.mqtt.NoorActivity;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.paint.BrushPreset;
import com.semanoor.paint.PainterCanvas;
import com.semanoor.sboookauthor_store.BookMarkEnrichments;
import com.semanoor.sboookauthor_store.EnrichmentButton;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.CustomViewPager;
import com.semanoor.source_manahij.EnrResDownload;
import com.semanoor.source_manahij.ExportEnrResViewAdapter;
import com.semanoor.source_manahij.NotesEditText;
import com.semanoor.source_manahij.OnlineEnrichments;
import com.semanoor.source_manahij.SemaWebView;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.HyperLinkDialog;
import com.semanoor.source_sboookauthor.MindMapDialog;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.PopupColorPicker;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import twitter4j.Twitter;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;


public class BookViewReadActivity extends NoorActivity implements View.OnClickListener, TextWatcher, PopoverView.PopoverViewDelegate, DialogInterface.OnDismissListener, ColorsRead.selectedColor {

    private int bookViewActivityrequestCode = 1001;
    private int indexActivityRequestCode = 1002;
    public int CAPTURE_GALLERY = 1003;
    public static final int MY_PERMISSIONS_REQUEST_GET_ACCOUNTS = 100001;
    public static int AUTH_CODE_REQUEST_CODE = 1010;
    Path mPath;
    public DatabaseHandler db;
    public Book currentBook;
    private CustomViewPager viewPager;
    public int currentPageNumber = 1;
    public int pageCount;
    private String currentBookPath;
    private String style;

    private String jsFilePath = "file:///android_asset/getSelection.js";
    private String jsAndroidSelection = "file:///android_asset/android.selection.js";
    private String jsjquery = "file:///android_asset/jquery-1.8.3.js";
    private String jsRange = "file:///android_asset/rangy-core.js";
    private String jsRangeSerializer = "file:///android_asset/rangy-serializer.js";
    private String jsJpnText = "file:///android_asset/jpntext.js";
    public SeekBar seekBar;
    public TextView tvSeekBar;
    public static Rect mNoteBounds = null;
    public RelativeLayout rootviewRL;
    public int currentEnrichmentTabId;
    public boolean isTablet = false;
    private int deviceWidth, deviceHeight;
    private SemaWebView currentWebView;
    public LinearLayout currentEnrTabsLayout;
    public LinearLayout currentBkmarkLayout, et_layout;
    private RelativeLayout mindMapView;
    private TwoDScrollView twoDScrollView;
    private ViewGroup drawingView;

    //Note Initialize
    private Dialog notePopUp;
    private View notePopupView;
    private RelativeLayout noteRelativeLayout;
    private RelativeLayout noteColorSelectionContainer;
    private ListView noteList;
    private ArrayList<String> pages = new ArrayList<String>();
    private ArrayList<String> pagesValue = new ArrayList<String>();
    private String SN = "";
    private TextView txtNote;
    private Button btnDone, btnCancel;
    private ImageView imgBtnC1, imgBtnC2, imgBtnC3, imgBtnC4, imgBtnC5, imgBtnC6;
    private int CC;
    private NotesEditText et, etNote;
    public Integer[] cardImgId = {R.color.yellow, R.color.green, R.color.skyblue, R.color.pink, R.color.lavender, R.color.white};
    final static Handler mWebviewVisibility = new Handler();
    private View view_notepop;
    private Dialog notepop;
    private RelativeLayout rl_notepop;
    private FrameLayout poparrow_bottomcontainer, poparrow_topcontainer;
    //private ImageView iv_toparrow_left,iv_toparrow_right,iv_toparrow_center,iv_bottomarrow_left,iv_bottomarrow_right,iv_bottomarrow_center;
    private String noteSelText;

    //Share
    public Twitter mTwitter;
    public RequestToken mRequestToken;
    private String[] share = {"Facebook", "Twitter", "Email"};
    private Dialog sharePopUp;

    private String[] advSearch = {"Book", "Google", "Wikipedia", "Youtube", "Yahoo", "Bing", "Ask"};
    // public ArrayList<String> searchIndexArray = new ArrayList<String>();
    public ArrayList<String> resultFound = new ArrayList<String>();
    private Dialog searchPopup;
    private EditText etSearch;
    private Dialog advSearchPopUp;
    private TextView tv_noresult;
    private EditText etAdv;
    private EditText searchEditText;
    private ImageButton imgBtnBack;
    private ImageView clearSearchBox;

    private Button btnBookMark;
    private boolean isAdvancedSearch;

    private WebView currentAdvanceSearchWebView;
    public RelativeLayout currentParentEnrRLayout, currentParentBklayout, subTabsLayout, subTabLayout1, enr_layout;
    private RelativeLayout designPageLayout, drawingToolbar;
    private LinearLayout objectsToolbar;
    private ProgressBar currentProgressbar;
    public boolean bkmInEditMode, bkmInDeleteMode;
    public RecyclerView CurrentRecyclerView, subRecyclerView, subRecylerView1;

    private PopoverView advbkmarkpopup;
    private ArrayList<BookMarkEnrichments> currentBookMarkEnrList;
    private ListView adv_bkmrktitle;

    private Button btnMarker, btnPen, btn_done, btnBrush, btnEarser, btnClearDrawing, btnStudyCards, btnIndex, btnHideAd, btnEnrResExport, btnMindmap, btnEnrichEdit, btn_print;
    public Button btnPaintUndo, btnPaintRedo, btnEnrResDownload, btnMalzamah, btn_text, btn_text_cardcount;
    private CircleButton search_btn;
    private String drawnFilesDir, objContent;
    private ImageView currentCanvasImgView;
    public PainterCanvas canvasView;
    private SeekBar mBrushSize, mBrushBlurRadius, opacity_seekbar, brushsize_seekbar, thickNess, opacity;
    private Button btn_brush_new, btn_eraser_new, btn_clear_new;
    private Spinner mBrushBlurStyle, lineTypeInDrawing;
    private LinearLayout toolbar_layout2, toolbar_layout3, toolbar_layout4, toolbar_layout5, toolbar_layout6, toolbar_layout7, toolbar_layout8, toolbar_layout9, toolbar_layout10, toolbar_enrreslayout;

    public static Object[] noteStudycards;
    private Dialog showStudyCardsDialog;
    private TextView tv_StudyCardsAlert;
    private CardContainer mCardContainer;
    private boolean isSetViewPagerItem;
    // public UserCredentials userCredentials;

    private boolean adsPurchased;
    private InterstitialAd interstitial;
    private AdView adView;
    public ViewPager exportEnrViewPager, noteViewPager;
    public WebService webService;
    public EnrResDownload enrResDownload;
    boolean isUpdate = false;
    private Malzamah malzamah;
    public GridShelf gridShelf;
    private ToggleButton btnSwitchOnlineEnr;
    public HorizontalScrollView sv, currentBkmarkScroll;
    ImageButton btn_BrushColor;
    String brushColor = null;
    public static final int WEBVIEW_REQUEST_CODE = 100;
    public RelativeLayout searchLayout;
    public boolean mindMapEdited = false;
    private RelativeLayout rl;
    CircleButton btnLibrary;
    private RelativeLayout enrResDownloadLayout;

    String webSelectedText;
    String tts_language;
    private TextToSpeech tts;

    public String userName;
    public String scopeId;
    public int scopeType;
    public String email;
    private Subscription subscription;
    private IabHelper mHelper;
    public ArrayList<HashMap<String, String>> existList;
    public Groups groups;
    private int InboxRequestCode = 55443;

    public Dialog exportEnrPopover;
    private ExportEnrResViewAdapter exportEnrResViewAdapter;
    public View mDrapView;
    public ArrayList<NoteData> noteDataArrayList;
    boolean allPageClicked = true;
    public ArrayList<NoteData> tempNoteDataArrayList = new ArrayList<>();
    SimpleCardStackAdapter adapter;
    private Button note_countbtn1, note_countbtn2, note_countbtn3, notwell_btn, well_btn, vrywell_btn, count_btn;
    private ImageView noteColor1, noteColor2, noteColor3, noteColor4, noteColor5, noteColor6;
    private EditText descText, titleText;
    private boolean isLoadEnrInpopview;
    private View gestureView;
    private PopoverView popoverView;
    private Button add_btn;
    public Enrichments currentBtnEnrichment;
    private RecyclerView clickableRecyclerView;
    private TreeStructureView treeStructureView;
    private Button search_img, arrow_btn, adv_search_btn;
    private EditText et_search, colorPickerEditText;
    private LinearLayout search_layout, icon_layout;
    private String selectedSearch = "Google";
    private int categoryID;
    private CircleButton dropdown_btn;
    public RelativeLayout add_btn_layout;
    public ArrayList<String> searchIndexArray = new ArrayList<String>();
    public ArrayList<String> searchBridgeArray = new ArrayList<String>();
    public ArrayList<String> searchMappingArray = new ArrayList<String>();
    private InternetSearch internetSearch;
    public int recCurrentNumber = 1, CurrentNumber = 1;
    public boolean isRecieve = false, isPagesele = false, isPage = false;
    private String mNotetext = "";
    String TopicName;
    MQTTSubscriptionService ss;
    public static final String FILTER = "noor.bookf.filter";
    ImageButton colorPickerImgView;
    int colorCode;
    String finalHex;
    ColorsRead colosRead;
    ManahijApp app;

    //ViewPagerAdapter viewPageradapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // No title Bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        loadLocale();

        setContentView(R.layout.activity_book_view_read);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/FiraSans-Regular.otf");
        ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

        UserFunctions.changeFont(rootView, font);
        app = (ManahijApp) getApplicationContext();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Globals.setDeviceWidth(size.x);
        Globals.setDeviceHeight(size.y);
        app.setState(false);
        ManahijApp.getInstance().getPrefManager().addIsinBackground(false);
        db = new DatabaseHandler(this);
        currentBook = (Book) getIntent().getSerializableExtra("Book");
        // userCredentials = (UserCredentials) getIntent().getSerializableExtra("UserCredentials");
        gridShelf = (GridShelf) getIntent().getSerializableExtra("Shelf");

        int pageNumber = getIntent().getIntExtra("currentPageNo", 1);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(BookViewReadActivity.this);
        try {
            existList = (ArrayList<HashMap<String, String>>) ObjectSerializer.deserialize(prefs.getString(Globals.SUBSLIST, ObjectSerializer.serialize(new ArrayList<HashMap<String, String>>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        groups = Groups.getInstance();
        currentPageNumber = pageNumber;
        pageCount = currentBook.getTotalPages();
        currentBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/";
        style = getNoteStyle();

        RelativeLayout lay = (RelativeLayout) findViewById(R.id.lay);
        lay.bringToFront();
        rootviewRL = (RelativeLayout) findViewById(R.id.rootView);
        isTablet = getResources().getBoolean(R.bool.isTablet);
        deviceWidth = Globals.getDeviceWidth();
        deviceHeight = Globals.getDeviceHeight();
        webService = new WebService(BookViewReadActivity.this, Globals.getNewCurriculumWebServiceURL());

        drawnFilesDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/" + "FreeFiles/Drawfiles/";
        UserFunctions.createNewDirectory(drawnFilesDir);
        searchLayout = (RelativeLayout) findViewById(R.id.relativeLay);
        copyNoteImage();
        ss = new MQTTSubscriptionService();
        TopicName = ManahijApp.getInstance().getPrefManager().getMainChannel();   /*pref.getString(Constants.OTHERTOPICNAME, null);*/
        ManahijApp.getInstance().getPrefManager().setBookOpend(true);
        if (Globals.isLimitedVersion()) {
           // initializeAds();
        }
        noteDataArrayList = db.loadNoteDivToArray(currentBook.get_bStoreID());
        for (int i = 0; i < noteDataArrayList.size(); i++) {
            tempNoteDataArrayList.add(noteDataArrayList.get(i));
        }
        intializeCustomViewPager();
        initializeNotePopUp();
        initializesmallNotePop();
        initializeSearchPopUp();
        initializeSearchEdittext();

        colorPickerImgView = (ImageButton) findViewById(R.id.colorPickerImgView);
        colorPickerImgView.setOnClickListener(this);
        colorPickerEditText = (EditText) findViewById(R.id.colorPickerEditText);

        seekBar = (SeekBar) findViewById(R.id.sb_pageslider);
        thickNess = (SeekBar) findViewById(R.id.thickness);
        opacity = (SeekBar) findViewById(R.id.opacity);


        colorPickerEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    String enteredColorCode = colorPickerEditText.getText().toString();

                    validateEnteredColor(enteredColorCode, true);


                    handled = true;
                }
                return handled;
            }
        });
        final GestureDetector gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            public boolean onDoubleTap(MotionEvent e) {


                if (colosRead.getVisibility() != View.VISIBLE) {


                    colorPickerEditText.setEnabled(true);
                    colorPickerEditText.setFocusable(true);
                    colorPickerEditText.setClickable(false);
                    colorPickerEditText.setFocusableInTouchMode(true);
                    colorPickerEditText.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);


                }

                return true;
            }
        });
        colorPickerEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return gestureDetector.onTouchEvent(motionEvent);
            }
        });


        //Brush size listener in deawing Mode

        thickNess.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (seekBar.getProgress() > 0) {
                    publishThickness(true, seekBar.getProgress());
                }

               /* try {
                    MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();
                    String topicName = ManahijApp.getInstance().getPrefManager().getContactTopictoPublish();
                    JSONObject obj = new JSONObject();
                    obj.put("type", Constants.THICKNESS);
                    obj.put(Constants.THICKNESS, thickness);
                    service.publishMsg(topicName, obj.toString(), BookViewReadActivity.this);
                } catch (Exception e) {
                    Log.e("Exception", e.toString());
                }*/
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 0) {
                    canvasView.setPresetSize(seekBar.getProgress());
                }
            }
        });


        //Opacity seekbar listener
        opacity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (seekBar.getProgress() > 0) {
                    publishOpacity(true, seekBar.getProgress());
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        lineTypeInDrawing = (Spinner) findViewById(R.id.lineTypeSpinner);
        lineTypeInDrawing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    // MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();
                    // if(ManahijApp.getInstance().getNetworkState()) {
                    if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                        JSONObject obj = new JSONObject();
                        obj.put("type", Constants.DRAWING);
                        obj.put("action", Constants.DRAWING_LINE_TYPE);
                        obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                        obj.put(Constants.DRAWING_LINE_TYPE, i);
                        ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
                        // }
                    }
                } catch (Exception e) {
                    Log.e("Exception", e.toString());
                }
                switch (i) {
                    case 0:
                        canvasView.setLineType(BrushPreset.NORMAL_LINE);
                        break;
                    case 1:

                        canvasView.setLineType(BrushPreset.DOTTED_LINE);
                        break;
                    case 2:
                        canvasView.setLineType(BrushPreset.DASHED_LINE);
                        break;
                    case 3:
                        canvasView.setLineType(BrushPreset.BARCODE_LINE);

                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        seekBar = (SeekBar) findViewById(R.id.sb_pageslider);
        tvSeekBar = (TextView) findViewById(R.id.tv_pagenumber);
        seekBar.setMax(pageCount - 1);
        setSeekBarValues();
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                /*if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
                    setSeekBarTextValues(i - 1);
                } else {
                    setSeekBarTextValues(pageCount - i);
                }*/


                try {
                   // Log.e("ViewPager", "" + i + "p" + recCurrentNumber);
                    JSONObject obj = new JSONObject();

                    String domainURL = "http://www.nooor.com/semaenrichments/IPadSSboookService.asmx";
                    JSONObject bookDetails = new JSONObject();
                    bookDetails.put("book_id", currentBook.get_bStoreID());
                    bookDetails.put("download_url", currentBook.getDownloadURL());
                    bookDetails.put("book_title", currentBook.getBookTitle());
                    bookDetails.put("b_description", currentBook.get_bDescription());
                    bookDetails.put("total_pages", currentBook.getTotalPages());
                    bookDetails.put("bstore_id", currentBook.get_bStoreID());
                    bookDetails.put("b_category_id", currentBook.getbCategoryId());
                    bookDetails.put("book_langugae", currentBook.getBookLangugae());
                    //bookDetails.put("IsDownloadCompleted", currentBook.getIsDownloadCompleted());
                    bookDetails.put("is_editable", currentBook.getIsEditable());
                    bookDetails.put("price", currentBook.getPrice());
                    bookDetails.put("image_url", currentBook.getImageURL());
                   // bookDetails.put("type", "free");
                    bookDetails.put("domain_uri", domainURL);

                    if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
                        if (!isPagesele) {
                            try {
                                if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                                    obj.put("type", Constants.KEYBOOK);
                                    obj.put("action", Constants.KEYPAGECHANGE);
                                    obj.put("book_id", currentBook.get_bStoreID());
                                    obj.put("url", bookDetails.toString());
                                    obj.put("page_number", (i - 1));
                                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                                    ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
                                }

                            } catch (Exception e) {
                                Log.e("ViewPager", "" + e.toString());
                            }
                        }

                        if (isPagesele)
                            setSeekBarTextValues(recCurrentNumber);
                        else
                            setSeekBarTextValues(i - 1);
                    } else {
                        if (!isPagesele) {
                            try {
                                if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                                    obj.put("type", Constants.KEYBOOK);
                                    obj.put("action", Constants.KEYPAGECHANGE);
                                    obj.put("book_id", currentBook.get_bStoreID());
                                    obj.put("url", bookDetails.toString());
                                    // obj.put("type", Constants.KEYPAGECHANGE);
                                    obj.put("page_number", (pageCount - i));
                                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                                    ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
                                }
                            } catch (Exception e) {
                                Log.e("ViewPager", "" + e.toString());
                            }
                        }
                        if (isPagesele)
                            setSeekBarTextValues(recCurrentNumber);
                        else
                            setSeekBarTextValues(pageCount - i);
                    }
                } catch (Exception e) {
                    Log.e("ViewPager", "" + e.toString());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                viewPager.setCurrentItem(seekBar.getProgress());
            }
        });

        btnEnrichEdit = (Button) findViewById(R.id.btnEnrichEdit);
        btnEnrichEdit.setOnClickListener(this);
        btnBookMark = (Button) findViewById(R.id.btnBookMark);
        btnBookMark.setOnClickListener(this);
        designPageLayout = (RelativeLayout) findViewById(R.id.DesignPageLayout);
        objectsToolbar = (LinearLayout) findViewById(R.id.relativeLayout1);
        drawingToolbar = (RelativeLayout) findViewById(R.id.draw_view);
        toolbar_layout2 = (LinearLayout) findViewById(R.id.bv_layout2);
        toolbar_layout3 = (LinearLayout) findViewById(R.id.bv_layout3);
        toolbar_layout4 = (LinearLayout) findViewById(R.id.bv_layout4);
        toolbar_layout5 = (LinearLayout) findViewById(R.id.bv_layout5);
        toolbar_layout6 = (LinearLayout) findViewById(R.id.bv_layout6);
        toolbar_layout7 = (LinearLayout) findViewById(R.id.bv_layout7);
        toolbar_layout8 = (LinearLayout) findViewById(R.id.bv_layout8);
        toolbar_layout9 = (LinearLayout) findViewById(R.id.bv_layout9);
        toolbar_layout10 = (LinearLayout) findViewById(R.id.bv_layout10);
        toolbar_enrreslayout = (LinearLayout) findViewById(R.id.bv_enrreslayout);

        btnMarker = (Button) findViewById(R.id.btnMarker);
        btnMarker.setOnClickListener(this);
        btnPen = (Button) findViewById(R.id.btn_pen);
        btnPen.setOnClickListener(this);
        btn_done = (Button) findViewById(R.id.btn_done);
        btn_done.setOnClickListener(this);
        btnBrush = (Button) findViewById(R.id.btn_brush);
        btnBrush.setOnClickListener(this);
        btnEarser = (Button) findViewById(R.id.btn_eraser);
        btnEarser.setOnClickListener(this);
        btnClearDrawing = (Button) findViewById(R.id.btn_clear);
        btnClearDrawing.setOnClickListener(this);
        btnPaintUndo = (Button) findViewById(R.id.btn_paint_undo);
        btnPaintUndo.setOnClickListener(this);
        btnPaintRedo = (Button) findViewById(R.id.btn_paint_redo);
        btnPaintRedo.setOnClickListener(this);
        btnStudyCards = (Button) findViewById(R.id.btnStudyCards);
        btnStudyCards.setOnClickListener(this);
        btnIndex = (Button) findViewById(R.id.btnIndex);
        btnIndex.setOnClickListener(this);
        btnEnrResExport = (Button) findViewById(R.id.btnEnrResExport);
        btnEnrResExport.setOnClickListener(this);
        btnEnrResDownload = (Button) findViewById(R.id.btnEnrResDownload);
        btnEnrResDownload.setOnClickListener(this);
        btn_text = (Button) findViewById(R.id.btn_text);
        btn_text_cardcount = (Button) findViewById(R.id.btn_text_cardcount);
        //   updateNotesCount();
        btnMindmap = (Button) findViewById(R.id.btnMindmap);
        btnMindmap.setOnClickListener(this);
        btnMalzamah = (Button) findViewById(R.id.btnMalzamah);
        btnMalzamah.setOnClickListener(this);
        btnLibrary = (CircleButton) findViewById(R.id.btnLibrary);
        btnLibrary.setOnClickListener(this);
        btn_print = (Button) findViewById(R.id.btn_print);
        btn_print.setOnClickListener(this);
        enrResDownloadLayout = (RelativeLayout) findViewById(R.id.enrResDownloadLayout);
        btnSwitchOnlineEnr = (ToggleButton) findViewById(R.id.switch2);
        //btn_brush_new = (Button) findViewById(R.id.btn_brush_new);
        //btn_brush_new.setOnClickListener(this);
        //    btn_eraser_new = (Button) findViewById(R.id.btn_eraser_new);
        // btn_eraser_new.setOnClickListener(this);
        // btn_clear_new = (Button) findViewById(R.id.btn_clear_new);
        // btn_clear_new.setOnClickListener(this);
        //opacity_seekbar = (SeekBar) findViewById(R.id.opacity);
        // brushsize_seekbar = (SeekBar) findViewById(R.id.brush_size);
        // brushsize_seekbar.setProgress(5);
        /*opacity_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // seekBar.setMax(10);
                // float opacity = seekBar.getProgress();
                canvasView.setPresetBlur(1, progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });*/
       /* brushsize_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress > 0) {
                    if (fromUser) {
                        canvasView.setPresetSize(seekBar.getProgress());
                    }
                } else {
                    brushsize_seekbar.setProgress(1);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 0) {
                    canvasView.setPresetSize(seekBar.getProgress());
                }
            }
        });*/
        btnSwitchOnlineEnr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (UserFunctions.checkLoginAndSubscription(BookViewReadActivity.this, true, existList, currentBook.getClientID(), false, currentBook.getPurchaseType())) {
                    if (isChecked) {
                        if (scopeType != 0) {
                            new OnlineEnrichments(BookViewReadActivity.this).execute();
                            btnSwitchOnlineEnr.setBackgroundResource(R.drawable.ic_enr_sync_btn);
                        } else {
                            UserFunctions.DisplayAlertDialog(BookViewReadActivity.this, R.string.please_login_download_publish, R.string.please_login);
                            btnSwitchOnlineEnr.setChecked(false);
                            btnSwitchOnlineEnr.setBackgroundResource(R.drawable.ic_enr_sync_online);
                        }
                    } else {
                        if (scopeType != 0) {
                            if (db.getOnlineEnrCount(currentBook.getBookID()) != 0) {
                                db.executeQuery("delete from enrichments where BID='" + currentBook.getBookID() + "'and Type='" + Globals.onlineEnrichmentsType + "'");
                                loadViewPager();
                            }
                        } else {
                            UserFunctions.DisplayAlertDialog(BookViewReadActivity.this, R.string.please_login_download_publish, R.string.please_login);
                            btnSwitchOnlineEnr.setChecked(false);
                            btnSwitchOnlineEnr.setBackgroundResource(R.drawable.ic_enr_sync_online);
                        }
                    }
                }
            }
        });
        if (scopeType != 0 && currentBook.get_bStoreID().charAt(1) == 'M' && Globals.isTablet()) {
            toolbar_enrreslayout.setVisibility(View.VISIBLE);
            enrResDownload = (EnrResDownload) new EnrResDownload(BookViewReadActivity.this).execute();
        } else {
            toolbar_enrreslayout.setVisibility(View.GONE);
        }

        filterToolbarButtons();

        tts = new TextToSpeech(BookViewReadActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.UK);
                }
            }
        });
    }

    private void filterToolbarButtons() {
        toolbar_layout8.setVisibility(View.GONE);
        if (!Globals.isTablet()) {
            toolbar_layout5.setVisibility(View.GONE);
            toolbar_layout6.setVisibility(View.GONE);
            toolbar_layout7.setVisibility(View.GONE);
            toolbar_layout8.setVisibility(View.GONE);
            toolbar_layout9.setVisibility(View.GONE);
            toolbar_layout10.setVisibility(View.GONE);
            toolbar_enrreslayout.setVisibility(View.GONE);
        } else if (currentBook.get_bStoreID().contains("C")) {
            toolbar_layout9.setVisibility(View.GONE);
            toolbar_layout6.setVisibility(View.GONE);
            toolbar_enrreslayout.setVisibility(View.GONE);
            toolbar_layout7.setVisibility(View.GONE);
        }
    }

    public void setViewPagerItem(int pageNumber, int enrichmentTabId) {
        currentEnrichmentTabId = enrichmentTabId;
        isSetViewPagerItem = true;
        if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
            viewPager.setCurrentItem((pageNumber - 1), false);
        } else {
            viewPager.setCurrentItem(pageCount - pageNumber, false);
        }
    }

    private void intializeCustomViewPager() {
        viewPager = (CustomViewPager) findViewById(R.id.viewPager);
        loadViewPager();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
                    currentPageNumber = viewPager.getCurrentItem() + 1;
                } else {
                    currentPageNumber = pageCount - viewPager.getCurrentItem();
                }
                setSeekBarValues();

                clearCurrentBookMarkEnrichments();
                /*if (arg0 == 0 || arg0 == pageCount - 1) {
                    displayBookMark(pageNumber, tabNo);
                }*/
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                /*loadEnrichmentObjectList(pageNumber);
                displayBookMark(pageNumber, tabNo);
                displayBMark(pageNumber);*/
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    private void clearCurrentBookMarkEnrichments() {
        if (currentBookMarkEnrList != null) {
            currentBookMarkEnrList.clear();
            currentBookMarkEnrList = null;
        }
    }

    public void checkForBookMark(int pageNo, int enrTabId) {
        int count = db.getBookMarkCount(currentBook.get_bStoreID(), pageNo, enrTabId);
        if (count > 0) {
            btnBookMark.setSelected(true);
        } else {
            btnBookMark.setSelected(false);
        }
    }

    public void loadViewPager() {
        viewPager.setAdapter(new ViewPagerAdapter());
        if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
            viewPager.setCurrentItem((currentPageNumber - 1), false);
        } else {
            viewPager.setCurrentItem(pageCount - currentPageNumber, false);
        }
        isPagesele = false;
    }

    /**
     * SubScription
     */
    public void subscribePurchase(final String subscribeProductId) {
        subscription = Subscription.getInstance();
        mHelper = new IabHelper(this, Globals.manahijBase64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    UserFunctions.complain("Problem setting up in-app billing: " + result, BookViewReadActivity.this);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                mHelper.launchPurchaseFlow(BookViewReadActivity.this, subscribeProductId, 10001, myPurchaseFinishedListener, "");
            }
        });

    }

    IabHelper.OnIabPurchaseFinishedListener myPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            //Log.d(TAG, "Purchase successful.");
            if (result.isFailure()) {
                //UserFunctions.complain("Error purchasing: " + result, BookViewReadActivity.this);
                return;
            }

            if (purchase.getSku().equals("")) {
                mHelper.consumeAsync(purchase, myConsumeFinishedListener);
                //UserFunctions.alert("Thank you for subscribing", fa_enrich);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener myConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isSuccess()) {
                ////System.out.println("Success");
                //UserFunctions.DisplayAlertDialog(fa_enrich, R.string.purchase_completed_successfully, R.string.purchased);
                //startDownload();
                if (purchase.getSku().equals("")) {
                    //UserFunctions.alert(purchase.getOriginalJson(), BookViewReadActivity.this);
                    //       subscription.new SaveSubscriptionGroups(purchase, BookViewReadActivity.this, null).execute();
                }
            } else {
                ////System.out.println("Failed");
                //UserFunctions.DisplayAlertDialog(fa_enrich, result.getMessage(), R.string.purchase_failed);
                UserFunctions.complain("Error while consuming: " + result, BookViewReadActivity.this);
            }
        }
    };

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnEnrichEdit) {
            if (UserFunctions.checkLoginAndSubscription(this, false, existList, currentBook.getClientID(), groups.isBookReaderEnrichment(), currentBook.getPurchaseType())) {
                if (currentPageNumber == 1 || currentPageNumber == pageCount) {
                    UserFunctions.DisplayAlertDialog(this, R.string.cover_page_cannot_enriched, R.string.cover_page);
                } else {
                    Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
                    bookViewIntent.putExtra("Book", currentBook);
                    bookViewIntent.putExtra("currentPageNo", currentPageNumber);
                    if (currentBtnEnrichment != null && currentBtnEnrichment.getEnrichmentType().equals(Globals.downloadedEnrichmentType)) {
                        currentEnrichmentTabId = 0;
                    }
                    bookViewIntent.putExtra("enrichmentTabId", currentEnrichmentTabId);
                    bookViewIntent.putExtra("Shelf", gridShelf);
                    app.setState(true);
                    ManahijApp.getInstance().getPrefManager().addIsinBackground(true);

                    startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
                }
            }
        } else if (view.getId() == R.id.btnBookMark) {
            if (UserFunctions.checkLoginAndSubscription(this, false, existList, currentBook.getClientID(), groups.getBookReaderBookmarkEnable(), currentBook.getPurchaseType())) {
                if (!btnBookMark.isSelected()) {
                    btnBookMark.setSelected(true);
                    addBookMark(currentPageNumber, currentEnrichmentTabId);
                } else {
                    btnBookMark.setSelected(false);
                    removeBookMark(currentPageNumber, currentEnrichmentTabId);
                }
            }
        } else if (view.getId() == R.id.btnMarker) {
            // if (UserFunctions.checkLoginAndSubscription(this, false, existList, currentBook.getClientID(), groups.getBookReaderDrawingEnable(), currentBook.getPurchaseType())) {
            //clearMode = false;
           // thickNess.setProgress(10);
            //opacity.setProgress(10);
            //opacity.setMax(50);
          //  thickNess.setMax(40);
            drawingUI(true);

            // }
        } else if (view.getId() == R.id.btn_done) {
            btnDrawingDone(true);
            //}
        } else if (view.getId() == R.id.btn_pen) {
            selectPen(true);
        } else if (view.getId() == R.id.btn_brush) {
            showBrushSettingsDialog();
        } else if (view.getId() == R.id.colorPickerImgView) {


            openColorPicker(true);
            hideEditTextFocus();
            hideKeyboard(colorPickerEditText);


        } else if (view.getId() == R.id.btn_eraser) {
            eraseDraw(true);
        } else if (view.getId() == R.id.btn_paint_undo) {
            canvasView.paintUndo();
        } else if (view.getId() == R.id.btn_paint_redo) {
            canvasView.paintRedo();
        } else if (view.getId() == R.id.btn_clear) {
            clearDraw(true);
        } else if (view.getId() == R.id.btnStudyCards) {
            if (UserFunctions.checkLoginAndSubscription(this, false, existList, currentBook.getClientID(), groups.getBookReaderFlashCardEnable(), currentBook.getPurchaseType())) {
                // showStudyCardsDialogg();


                try {
                    //if(ManahijApp.getInstance().getNetworkState()) {
                    if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {

                        JSONObject obj = new JSONObject();

                        obj.put("type", Constants.KEYBOOK);
                        obj.put("action", Constants.KEYOPENFLASHCARD);
                        obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                        ss.publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(), obj.toString(), BookViewReadActivity.this);
                    }
                    // }
                } catch (Exception e) {
                    Log.e("flash", e.toString());
                }
                Intent intent = new Intent(BookViewReadActivity.this, FlashCardActivity.class);
                intent.putExtra("Book", currentBook);
                intent.putExtra("pagenumber", currentPageNumber);
                intent.putExtra("fromBook", true);
                app.setState(true);
                ManahijApp.getInstance().getPrefManager().addIsinBackground(true);
                startActivity(intent);
            }
        } else if (view.getId() == R.id.btnIndex) {
            if (UserFunctions.checkLoginAndSubscription(this, false, existList, currentBook.getClientID(), false, currentBook.getPurchaseType())) {
                Intent i = new Intent(getApplicationContext(), Index_new.class);
                i.putExtra("Book", currentBook);
                app.setState(true);
                ManahijApp.getInstance().getPrefManager().addIsinBackground(true);
                startActivityForResult(i, indexActivityRequestCode);
            }
        } else if (view.getId() == R.id.btnLibrary) {
            try {
                if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                    JSONObject object = new JSONObject();
                    object.put("type", Constants.KEYBOOK);
                    object.put("action", Constants.KEYBOOKCLOSE);
                    object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    ss.publishMsg(TopicName, object.toString(), BookViewReadActivity.this);
                }
            } catch (Exception e) {
                Log.e("ex", e.toString());
            }

            onBackPressed();
        } else if (view.getId() == R.id.btnEnrResExport) {
            if (UserFunctions.checkLoginAndSubscription(this, true, existList, currentBook.getClientID(), false, currentBook.getPurchaseType())) {
                if (scopeType != 0) {
                    Intent intent = new Intent(BookViewReadActivity.this, ExportEnrActivity.class);
                    intent.putExtra("Book", currentBook);
                    intent.putExtra("Shelf", gridShelf);
                    app.setState(true);
                    ManahijApp.getInstance().getPrefManager().addIsinBackground(true);
                    startActivity(intent);
                    // startActivity();
                    // exportpopup();
                } else {
                    UserFunctions.DisplayAlertDialog(BookViewReadActivity.this, R.string.please_login_download_publish, R.string.please_login);
                }
            }
        } else if (view.getId() == R.id.btnEnrResDownload) {

            if (UserFunctions.checkLoginAndSubscription(this, true, existList, currentBook.getClientID(), false, currentBook.getPurchaseType())) {
                if (scopeType != 0) {
                    // enrResDownload.enrichPopup();
                    Intent intent = new Intent(BookViewReadActivity.this, CloudBooksActivity.class);
                    //intent.putExtra("SlidemenuActivity",SlideMenuActivity);
                    intent.putExtra("Shelf", gridShelf);
                    app.setState(true);
                    ManahijApp.getInstance().getPrefManager().addIsinBackground(true);
                    startActivityForResult(intent, InboxRequestCode);
                } else {
                    UserFunctions.DisplayAlertDialog(BookViewReadActivity.this, R.string.please_login_download_publish, R.string.please_login);
                }
            }
        } else if (view.getId() == R.id.btnMalzamah) {
            if (UserFunctions.checkLoginAndSubscription(this, false, existList, currentBook.getClientID(), groups.getBookReaderMalzamahEnable(), currentBook.getPurchaseType())) {
                malzamah = new Malzamah(BookViewReadActivity.this);
                malzamah.displayMalzamahPopUp();
            }
        } else if (view.getId() == R.id.btnMindmap) {
            if (UserFunctions.checkLoginAndSubscription(this, false, existList, currentBook.getClientID(), groups.getBookReaderMindmapEnable(), currentBook.getPurchaseType())) {
                MindMapDialog mindMapDialog = new MindMapDialog(BookViewReadActivity.this, false);
                mindMapDialog.showMindMapDialog("");
            }
        } else if (view.getId() == R.id.btn_print) {
            if (UserFunctions.checkLoginAndSubscription(this, false, existList, currentBook.getClientID(), groups.getBookReaderMindmapEnable(), currentBook.getPurchaseType())) {
                createWebPrintJob(currentWebView);
            }
        } else if (view.getId() == R.id.all_btn) {
            loadAllNotesInCurrentPage();
        } else if (view.getId() == R.id.imgBtnC1) {
            loadNotesBasedOnColors(1);
        } else if (view.getId() == R.id.imgBtnC2) {
            loadNotesBasedOnColors(2);
        } else if (view.getId() == R.id.imgBtnC3) {
            loadNotesBasedOnColors(3);
        } else if (view.getId() == R.id.imgBtnC4) {
            loadNotesBasedOnColors(4);
        } else if (view.getId() == R.id.imgBtnC5) {
            loadNotesBasedOnColors(5);
        } else if (view.getId() == R.id.imgBtnC6) {
            loadNotesBasedOnColors(6);
        } else if (view.getId() == R.id.count_btn1) {
            loadNotesBasedOnType(1);
        } else if (view.getId() == R.id.count_btn2) {
            loadNotesBasedOnType(2);
        } else if (view.getId() == R.id.count_btn3) {
            loadNotesBasedOnType(3);
        } else if (view.getId() == R.id.notwell_btn) {
            if (noteDataArrayList.size() > 0) {
                int position = mCardContainer.cardPosition;
                if (position == -1) {
                    position = 0;
                }
                NoteData data = noteDataArrayList.get(position);
                if (data.getNoteType() != 1) {
                    data.setNoteType(1);
                    String query = "update tblNote set NoteType='1' where BName='" + currentBook.get_bStoreID() + "' and PageNo='" + data.getNotePageNo() + "' and TabNo='" + data.getNoteTabNo() + "' and SText='" + data.getNoteTitle() + "' and SDesc='" + data.getNoteDesc() + "'";
                    db.executeQuery(query);
                    setBackgroudColorForButtons(position);
                    updateButtons();
                }
            }
        } else if (view.getId() == R.id.well_btn) {
            if (noteDataArrayList.size() > 0) {
                int position = mCardContainer.cardPosition;
                if (position == -1) {
                    position = 0;
                }
                NoteData data = noteDataArrayList.get(position);
                if (data.getNoteType() != 2) {
                    data.setNoteType(2);
                    String query = "update tblNote set NoteType='2' where BName='" + currentBook.get_bStoreID() + "' and PageNo='" + data.getNotePageNo() + "' and TabNo='" + data.getNoteTabNo() + "' and SText='" + data.getNoteTitle() + "' and SDesc='" + data.getNoteDesc() + "'";
                    db.executeQuery(query);
                    setBackgroudColorForButtons(position);
                    updateButtons();
                }
            }
        } else if (view.getId() == R.id.verywell_btn) {
            if (noteDataArrayList.size() > 0) {
                int position = mCardContainer.cardPosition;
                if (position == -1) {
                    position = 0;
                }
                NoteData data = noteDataArrayList.get(position);
                if (data.getNoteType() != 3) {
                    data.setNoteType(3);
                    String query = "update tblNote set NoteType='3' where BName='" + currentBook.get_bStoreID() + "' and PageNo='" + data.getNotePageNo() + "' and TabNo='" + data.getNoteTabNo() + "' and SText='" + data.getNoteTitle() + "' and SDesc='" + data.getNoteDesc() + "'";
                    db.executeQuery(query);
                    setBackgroudColorForButtons(position);
                    updateButtons();
                }
            }
        } else if (view.getId() == R.id.btn_search) {
            internetSearch.searchClicked(new InternetSearchCallBack() {
                @Override
                public void onClick(String url, String title) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(rl.getWindowToken(), 0);
                    new createAdvanceSearchTab(url, title).execute();
                }
            });
        } else if (view.getId() == R.id.search_image) {
            internetSearch.showSearchPopUp(view);
        } else if (view.getId() == R.id.arrow_btn) {
            internetSearch.showSearchPopUp(view);
        } else if (view.getId() == R.id.btn_brush_new) {
            changeBrushColor(view);
        } else if (view.getId() == R.id.btn_eraser_new) {
            canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        } else if (view.getId() == R.id.btn_clear_new) {
            canvasView.clearView();
            File enrichFile = new File(objContent);
            if (enrichFile.exists()) {
                enrichFile.delete();
            }
        }
    }

    /**
     * Export Enrichment/resource PopUp
     */
    public void exportpopup() {
        PopoverView exportEnrPopover = new PopoverView(BookViewReadActivity.this, R.layout.export_viewpager);
        int width = (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_width);
        int height = (int) getResources().getDimension(R.dimen.bookview_initial_quiz_text_height);
        exportEnrPopover.setContentSizeForViewInPopover(new Point(width, height));
        exportEnrPopover.setDelegate(this);
        exportEnrPopover.showPopoverFromRectInViewGroup(this.rootviewRL, PopoverView.getFrameForView(btnEnrResExport), PopoverView.PopoverArrowDirectionUp, true);

        exportEnrViewPager = (ViewPager) exportEnrPopover.findViewById(R.id.viewPager);
        exportEnrResViewAdapter = new ExportEnrResViewAdapter(BookViewReadActivity.this);
        exportEnrViewPager.setAdapter(exportEnrResViewAdapter);
        exportEnrViewPager.setOffscreenPageLimit(2);
        exportEnrViewPager.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    public void setPreset(View v) {
        switch (v.getId()) {
            case R.id.preset_pencil:
                canvasView.setPreset(new BrushPreset(BrushPreset.PENCIL, canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                }
                break;
            case R.id.preset_brush:
                canvasView.setPreset(new BrushPreset(BrushPreset.BRUSH, canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                }
                break;
            case R.id.preset_marker:
                canvasView.setPreset(new BrushPreset(BrushPreset.MARKER, canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                }
                break;
            case R.id.preset_pen:
                canvasView.setPreset(new BrushPreset(BrushPreset.PEN, canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                }
                break;
        }

        updateControls();
    }

    private void updateControls() {
        mBrushSize.setProgress((int) canvasView.getCurrentPreset().size);
        if (canvasView.getCurrentPreset().blurStyle != null) {
            mBrushBlurStyle.setSelection(canvasView.getCurrentPreset().blurStyle
                    .ordinal() + 1);
            mBrushBlurRadius.setProgress(canvasView.getCurrentPreset().blurRadius);
        } else {
            mBrushBlurStyle.setSelection(0);
            mBrushBlurRadius.setProgress(0);
        }
    }

    public void checkForPaintAndApply(ImageView cvsImageView, int pageNo, int enrId) {
        if (enrId > 0) {
            objContent = drawnFilesDir + "Page" + " " + pageNo + "-" + enrId + ".png";
        } else {
            objContent = drawnFilesDir + "Page" + " " + pageNo + ".png";
        }
        setBackgroundImageForDrawing(cvsImageView);
    }

    public void advSearchTab(EnrichmentButton enrButton) {
        RelativeLayout parentLayout = (RelativeLayout) enrButton.getParent();
        int index = currentEnrTabsLayout.indexOfChild(parentLayout);
        if (currentEnrTabsLayout.getChildCount() > 1) {
            View v = currentEnrTabsLayout.getChildAt(index);
            if (v instanceof RelativeLayout) {
                RelativeLayout rlView = (RelativeLayout) v;
                EnrichmentButton advButton = (EnrichmentButton) rlView.getChildAt(0);
                //  clickedAdvanceSearchtab(advButton);
            }
        }
    }

    @Override
    public void selectedColor(String selcetedColor) {
        setSelectedColorToDraw(selcetedColor, true);
        if (popupMessage != null && popupMessage.isShowing())
            popupMessage.dismiss();

    }

    /**
     * @author saveDrawnImagetoFilesDir
     */
    private class saveDrawnTask extends AsyncTask<Void, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(BookViewReadActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage(getResources().getString(R.string.processing));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            saveBitmap(canvasView.objContentPath);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setBackgroundImageForDrawing(currentCanvasImgView);
            for (int i = 0; i < designPageLayout.getChildCount(); i++) {
                View childView = designPageLayout.getChildAt(i);
                if (childView == canvasView) {
                    designPageLayout.removeView(canvasView);
                    break;
                }
            }
            // designPageLayout.removeView(canvasView);
            doneFromDrawingMode();
            progressDialog.dismiss();
        }
    }

    /**
     * @author setBackgroundImageForDrawing
     */
    public void setBackgroundImageForDrawing(ImageView cvsImageView) {
        if (new File(objContent).exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(objContent);
            cvsImageView.setVisibility(View.VISIBLE);
            cvsImageView.setImageBitmap(bitmap);
        } else {
            cvsImageView.setImageBitmap(null);
            cvsImageView.setVisibility(View.GONE);
        }
    }

    /**
     * @param pictureName
     * @author saveBitmapImage
     */
    private void saveBitmap(String pictureName) {
        try {
            canvasView.saveBitmap(pictureName);
            canvasView.changed(false);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private void doneFromDrawingMode() {
        drawingToolbar.setVisibility(View.GONE);
        objectsToolbar.setVisibility(View.VISIBLE);
        seekBar.setVisibility(View.VISIBLE);
        tvSeekBar.setVisibility(View.VISIBLE);
        searchLayout.setVisibility(View.VISIBLE);
        btnLibrary.setVisibility(View.VISIBLE);

        if (btnPen.isSelected() || btnEarser.isSelected()) {
            btnPen.setSelected(false);
            btnEarser.setSelected(false);
        }
    }

    /**
     * @author ShowBrushSettingsDialog
     */
    private void showBrushSettingsDialog() {
        Dialog brushSettingsDialog = new Dialog(this);
        brushSettingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        brushSettingsDialog.setTitle(R.string.brush_settings);
        brushSettingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.brush_settings_dialog, null));
        //brushSettingsDialog.getWindow().setLayout((int)(g.getDeviceWidth()/1.5), (int)(g.getDeviceHeight()/1.5));
        brushSettingsDialog.getWindow().setLayout((int) getResources().getDimension(R.dimen.drawing_brush_dialog), (int) getResources().getDimension(R.dimen.drawing_brush_dialog_height));
        mBrushSize = (SeekBar) brushSettingsDialog.findViewById(R.id.brush_size);
        btn_BrushColor = (ImageButton) brushSettingsDialog.findViewById(R.id.brush_color);
        if (brushColor != null && brushColor.equals(canvasView.getCurrentPreset())) {
            btn_BrushColor.setBackgroundColor(Color.parseColor(brushColor));
        } else {
            btn_BrushColor.setBackgroundColor(canvasView.getCurrentPreset().color);
        }
        mBrushSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 0) {
                    canvasView.setPresetSize(seekBar.getProgress());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (progress > 0) {
                    if (fromUser) {
                        canvasView.setPresetSize(seekBar.getProgress());
                    }
                } else {
                    mBrushSize.setProgress(1);
                }
            }
        });
        mBrushBlurRadius = (SeekBar) brushSettingsDialog.findViewById(R.id.brush_blur_radius);
        mBrushBlurRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                updateBlurSeek(seekBar.getProgress());

                if (seekBar.getProgress() > 0) {
                    setBlur();
                } else {
                    canvasView.setPresetBlur(null, 0);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (fromUser) {
                    updateBlurSeek(progress);
                    if (progress > 0) {
                        setBlur();
                    } else {
                        canvasView.setPresetBlur(null, 0);
                    }
                }
            }
        });
        mBrushBlurStyle = (Spinner) brushSettingsDialog.findViewById(R.id.brush_blur_style);
        mBrushBlurStyle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View v,
                                       int position, long id) {
                if (id > 0) {
                    updateBlurSpinner(id);
                    setBlur();
                } else {
                    mBrushBlurRadius.setProgress(0);
                    canvasView.setPresetBlur(null, 0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
        updateControls();
        brushSettingsDialog.show();
    }

    private void updateBlurSeek(int progress) {
        if (progress > 0) {
            if (mBrushBlurStyle.getSelectedItemId() < 1) {
                mBrushBlurStyle.setSelection(1);
            }
        } else {
            mBrushBlurStyle.setSelection(0);
        }
    }

    private void setBlur() {
        canvasView.setPresetBlur((int) mBrushBlurStyle.getSelectedItemId(),
                mBrushBlurRadius.getProgress());
    }

    private void updateBlurSpinner(long blur_style) {
        if (blur_style > 0 && mBrushBlurRadius.getProgress() < 1) {
            mBrushBlurRadius.setProgress(1);
        }
    }

    /**
     * @author showColorPickerDialogtochangeDrawingcolor
     */
    public void changeBrushColor(View v) {
        final ArrayList<String> recentPaintColorList = this.getRecentColorsFromSharedPreference(Globals.paintColorKey);
        new PopupColorPicker(this, canvasView.getCurrentPreset().color, v, recentPaintColorList, false, new PopupColorPicker.ColorPickerListener() {
            int pickedColor = 0;
            String hexColor = null;

            @Override
            public void pickedColor(int color) {
                hexColor = String.format("#%06X", (0xFFFFFF & color));
                pickedColor = color;
                brushColor = hexColor;
                //   btn_BrushColor.setBackgroundColor(Color.parseColor(hexColor));
            }

            @Override
            public void dismissPopWindow() {
                canvasView.setPresetColor(pickedColor);
                if (hexColor != null) {
                    setandUpdateRecentColorsToSharedPreferences(Globals.paintColorKey, recentPaintColorList, hexColor);
                }
            }

            @Override
            public void pickedTransparentColor(String transparentColor) {

            }

            @Override
            public void pickColorFromBg() {

            }
        });
    }

    /**
     * getRecentColorsFromSharedPreferences
     *
     * @param key
     * @return
     */
    public ArrayList<String> getRecentColorsFromSharedPreference(String key) {
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
        ArrayList<String> recentColorList = null;
        try {
            recentColorList = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (recentColorList.size() == 0) {
            recentColorList.add("#B0171F");
            recentColorList.add("#FF82AB");
            recentColorList.add("#0000FF");
            recentColorList.add("#000000");
            recentColorList.add("#006400");
        }
        return recentColorList;
    }

    /**
     * setandUpdateRecentColorsToSharedPreferences
     *
     * @param key
     * @param recentColorList
     * @param recentColor
     */
    public void setandUpdateRecentColorsToSharedPreferences(String key, ArrayList<String> recentColorList, String recentColor) {
        ArrayList<String> tempArrayList = new ArrayList<String>();
        for (int i = 0; i < recentColorList.size(); i++) {
            if (i == 0) {
                tempArrayList.add(recentColor);
            } else {
                tempArrayList.add(recentColorList.get(i - 1));
            }
        }
        recentColorList.clear();
        recentColorList = tempArrayList;

        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreference.edit();
        try {
            editor.putString(key, ObjectSerializer.serialize(recentColorList));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    public void removeBookMark(int pageNo, int enrTabId) {
        String query = "delete from tblBookMark where BName='" + currentBook.get_bStoreID() + "' and PageNo='" + pageNo + "' and TabNo='" + enrTabId + "'";
        db.executeQuery(query);
    }

    private void addBookMark(int pageNo, int enrTabId) {
        String query = "insert into tblBookMark(BName,PageNo,TagID,TabNo) values('" + currentBook.get_bStoreID() + "','" + pageNo + "','0','" + enrTabId + "')";
        db.executeQuery(query);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (searchEditText.getText().toString().equals("")) {

        } else {

        }
    }

    @Override
    public void popoverViewWillShow(PopoverView view) {

    }

    @Override
    public void popoverViewDidShow(PopoverView view) {

    }

    @Override
    public void popoverViewWillDismiss(PopoverView view) {

    }

    @Override
    public void popoverViewDidDismiss(PopoverView view) {

    }

    private class ViewPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return pageCount;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final int viewPagerPageNo;
            if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
                viewPagerPageNo = position + 1;
            } else {
                viewPagerPageNo = pageCount - position;
            }
            View view = getLayoutInflater().inflate(R.layout.bookview_inflate, null);
            final SemaWebView viewPagerWebView = (SemaWebView) view.findViewById(R.id.bTWebView1);
            viewPagerWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            viewPagerWebView.clearCache(true);
            viewPagerWebView.getSettings().setJavaScriptEnabled(true);
            viewPagerWebView.getSettings().setAllowContentAccess(true);
            viewPagerWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            viewPagerWebView.getSettings().setAllowFileAccess(true);
            viewPagerWebView.setWebChromeClient(new WebChromeClient());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                viewPagerWebView.getSettings().setAllowFileAccessFromFileURLs(true);
                viewPagerWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
            }
            viewPagerWebView.getSettings().setPluginState(WebSettings.PluginState.ON);

            final ImageView viewPagerCanvasImgView = (ImageView) view.findViewById(R.id.bgImgView);
            RelativeLayout parentEnrRLayout = (RelativeLayout) view.findViewById(R.id.rl_tabs_layout);
            final LinearLayout ll_container = (LinearLayout) view.findViewById(R.id.ll_container);
            RelativeLayout parentBkEnrRLayout = (RelativeLayout) view.findViewById(R.id.rl_bkmark_layout);
            LinearLayout ll_bkContainer = (LinearLayout) view.findViewById(R.id.ll_bookmark_container);
            final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.gridView1);
            Button currentbkedit = (Button) view.findViewById(R.id.btn_popup);
            currentbkedit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    displaycurrentBookMarkTitle(v);
                }
            });
            Button btnEnrHome = (Button) view.findViewById(R.id.btnEnrHome);
            btnEnrHome.setSelected(true);
            btnEnrHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickedBtnHomeEnrichment((Button) view, ll_container, viewPagerWebView, viewPagerPageNo, viewPagerCanvasImgView);
                }
            });

            viewPagerWebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onScaleChanged(WebView view, float oldScale, float newScale) {
                    ((SemaWebView) view).mTextSelectionSupport.onScaleChanged(oldScale, newScale);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    loadHighlightForViewPager((SemaWebView) view, viewPagerPageNo, currentEnrichmentTabId);
                    loadNoteForViewPager((SemaWebView) view, viewPagerPageNo);
                    loadAdvancedSearch((SemaWebView) view);
                    loadNormalSearch((SemaWebView) view);
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    String[] searchResults = url.split("/");
                    if (searchResults.length > 3) {
                        if (searchResults[3].equalsIgnoreCase("E")) {
                            ((SemaWebView) view).addNoteFromPopMenu = false;
                            String vc = "E|" + searchResults[4];
                            addNote("", vc, (SemaWebView) view);
                        }

                    }
                    return true;
                }
            });
            loadHighlightAndNoteForViewPager(viewPagerWebView, viewPagerPageNo, currentEnrichmentTabId);
            checkForPaintAndApply(viewPagerCanvasImgView, viewPagerPageNo, currentEnrichmentTabId);
            if (search_layout != null) {
                search_layout.setVisibility(View.GONE);
            }
            new loadEnrichmentTabs(viewPagerPageNo, recyclerView, currentEnrichmentTabId, parentEnrRLayout, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);


           /* enrichmentTabList = db.getEnrichmentTabListForBookReadAct1(currentBook.getBookID(), currentPageNumber, BookViewReadActivity.this);
            HorizontalAdapter  adapter = new HorizontalAdapter(viewPagerPageNo,enrichmentTabList);
            LinearLayoutManager horizontalLayoutManagaer
                    = new LinearLayoutManager(BookViewReadActivity.this, LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(horizontalLayoutManagaer);
            recyclerView.setAdapter(adapter);
            ItemTouchHelper.Callback callback = new MovieTouchHelper(adapter);
            ItemTouchHelper helper = new ItemTouchHelper(callback);
            helper.attachToRecyclerView(recyclerView);*/

            //   new loadBookmarkedEnrichmentTabs(viewPagerPageNo, ll_bkContainer, parentBkEnrRLayout).execute();
            ((ViewPager) container).addView(view, 0);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            rl = (RelativeLayout) object;
            SemaWebView currentSemaWebView = (SemaWebView) rl.getChildAt(0);
            WebView advSearchWebView = (WebView) rl.getChildAt(1);
            viewPager.webView = currentSemaWebView;
            currentWebView = currentSemaWebView;
            currentAdvanceSearchWebView = (WebView) rl.getChildAt(1);
            currentCanvasImgView = (ImageView) rl.getChildAt(2);
            enr_layout = (RelativeLayout) rl.getChildAt(4);

            currentParentEnrRLayout = (RelativeLayout) enr_layout.getChildAt(0);
            if (currentPageNumber == 1 || currentPageNumber == pageCount) {
                currentParentEnrRLayout.setVisibility(View.GONE);
            }
            subTabsLayout = (RelativeLayout) enr_layout.getChildAt(1);
            subTabLayout1 = (RelativeLayout) enr_layout.getChildAt(2);
            CurrentRecyclerView = (RecyclerView) currentParentEnrRLayout.getChildAt(0);
            subRecyclerView = (RecyclerView) subTabsLayout.getChildAt(0);
            subRecylerView1 = (RecyclerView) subTabLayout1.getChildAt(0);
            RelativeLayout dropdown_layout = (RelativeLayout) subTabLayout1.getChildAt(1);
            dropdown_btn = (CircleButton) dropdown_layout.getChildAt(0);
            dropdown_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SubtabsAdapter.level3 != 0) {
                        if (currentBtnEnrichment.getEnrichmentId() == SubtabsAdapter.level3) {
                            isLoadEnrInpopview = true;
                            viewTreeList(v, false, currentBtnEnrichment.getEnrichmentId());
                        }
                    }
                }
            });
            add_btn_layout = (RelativeLayout) currentParentEnrRLayout.getChildAt(1);
            add_btn = (Button) add_btn_layout.getChildAt(0);
            add_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // treeViewStructureList(v,true,0);
                    viewTreeList(v, true, 0);
                }
            });
            sv = (HorizontalScrollView) currentParentEnrRLayout.getChildAt(2);
            currentEnrTabsLayout = (LinearLayout) sv.getChildAt(0);
            currentProgressbar = (ProgressBar) rl.getChildAt(5);

            search_layout = (LinearLayout) enr_layout.getChildAt(4);
            LinearLayout layout1 = (LinearLayout) search_layout.getChildAt(0);
            LinearLayout layout2 = (LinearLayout) layout1.getChildAt(0);
            icon_layout = (LinearLayout) layout2.getChildAt(0);
            search_img = (Button) icon_layout.getChildAt(1);
            search_img.setOnClickListener(BookViewReadActivity.this);
            arrow_btn = (Button) icon_layout.getChildAt(0);
            arrow_btn.setOnClickListener(BookViewReadActivity.this);
            et_search = (EditText) layout2.getChildAt(1);
            RelativeLayout relativeLayout1 = (RelativeLayout) search_layout.getChildAt(1);
            FrameLayout frameLayout = (FrameLayout) relativeLayout1.getChildAt(0);
            adv_search_btn = (Button) frameLayout.getChildAt(0);
            adv_search_btn.setOnClickListener(BookViewReadActivity.this);
            internetSearch = new InternetSearch(search_layout, et_search, BookViewReadActivity.this, search_img);
            currentParentBklayout = (RelativeLayout) enr_layout.getChildAt(3);
            currentBkmarkScroll = (HorizontalScrollView) currentParentBklayout.getChildAt(0);
            currentBkmarkLayout = (LinearLayout) currentBkmarkScroll.getChildAt(0);

            if (Globals.isLimitedVersion() && !adsPurchased && adView != null) {
                if (!groups.getBannerAdsVisible()) {
                    adView.setVisibility(View.VISIBLE);
                    btnHideAd.setVisibility(View.VISIBLE);
                }
            }

            if (currentEnrichmentTabId > 0 && isSetViewPagerItem) {
                isSetViewPagerItem = false;

            } else {

            }
            if (CurrentRecyclerView.getAdapter() != null) {
                CurrentRecyclerView.getAdapter().notifyDataSetChanged();
            }

            checkForBookMark(currentPageNumber, currentEnrichmentTabId);

            et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        internetSearch.searchClicked(new InternetSearchCallBack() {
                            @Override
                            public void onClick(String url, String title) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(rl.getWindowToken(), 0);
                                new createAdvanceSearchTab(url, title).execute();
                            }
                        });
                    }
                    return false;
                }
            });

            currentWebView.setWebViewClient(new WebViewClient() {

                @Override
                public void onScaleChanged(WebView view, float oldScale, float newScale) {
                    ((SemaWebView) view).mTextSelectionSupport.onScaleChanged(oldScale, newScale);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    currentProgressbar.setVisibility(View.GONE);
                    loadHighlightForViewPager((SemaWebView) view, currentPageNumber, currentEnrichmentTabId);
                    loadNoteForViewPager((SemaWebView) view, currentPageNumber);
                    loadAdvancedSearch((SemaWebView) view);
                    loadNormalSearch((SemaWebView) view);
                                                  /*  for(int i=0;i< CurrentRecyclerView.getAdapter().getItemCount();i++){
                                                        RelativeLayout viewas =(RelativeLayout)CurrentRecyclerView.getChildAt(i);
                                                        System.out.println(viewas);
                                                    }*/
                }

                @Override
                public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
                    String[] searchResults = url.split("/");
                    if (searchResults.length > 3 && !url.contains("hyper")) {
                        if (searchResults[3].equalsIgnoreCase("E")) {
                            ((SemaWebView) view).addNoteFromPopMenu = false;
                            String vc = "E|" + searchResults[4];
                            addNote("", vc, (SemaWebView) view);
                        }
                    } else if (url.contains("hyperurl://") || url.contains("hyperpage://") || url.contains("hyperfile://")) {

                        String hyperlink_url[] = url.split("\\?");
                        if (hyperlink_url[0].contains("hyperurl://")) {

                            String hyperLinkUrl;
                            String urlLowerCase = hyperlink_url[0].toLowerCase();
                            if (!urlLowerCase.contains("http://")) {
                                hyperLinkUrl = urlLowerCase.replace("hyperurl://", "http://");
                            } else {
                                hyperLinkUrl = urlLowerCase.replace("hyperurl://", "");
                            }

                            new createAdvanceSearchTab(hyperLinkUrl, hyperLinkUrl).execute();
                        } else if (hyperlink_url[0].contains("hyperpage://")) {
                            String hyperLink_pageNo = hyperlink_url[0].replace("file:///android_asset/hyperpage://", "file:///");
                            String hyperLink = hyperlink_url[0].replace("hyperpage://", "file:///");
                            // String page_path = url.replace("file:///data/data/com.semanoor.SboookAuthor/files/books/1/hyperlink_page://", "file:///");
                            new createAdvanceSearchTab(hyperLink, "BookPage").execute();

                        } else if (hyperlink_url[0].contains("hyperfile://")) {
                            String[] split = hyperlink_url[0].split("/");
                            String hyperLink = hyperlink_url[0].replace("hyperfile://", "");
                            String imgPath = split[split.length - 1];
                            if (imgPath.contains(".png") || imgPath.contains(".jpg") || imgPath.contains(".gif")) {
                                // shouldOverride = true;
                                HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(BookViewReadActivity.this, null);
                                hyperLinkDialog.imageDialogWindow(hyperLink, true, false);
                                //return shouldOverride;
                                //	hyperlink_dialog.dismiss();
                            } else if (imgPath.contains(".mp4") || imgPath.contains(".mkv") || imgPath.contains(".3gp") || imgPath.contains(".avi") || imgPath.contains(".flv")) {
                                // shouldOverride = true;
                                HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(BookViewReadActivity.this, null);
                                hyperLinkDialog.videoHyperLink(hyperLink);
                                //return shouldOverride;
                                //hyperlink_dialog.dismiss();
                            } else if (imgPath.contains(".mp3") || imgPath.contains(".m4a")) {
                                // shouldOverride = true;
                                HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(BookViewReadActivity.this, null);
                                hyperLinkDialog.audioHyperLink(hyperLink);
                                //return shouldOverride;
                            }
                        }
                    }
                    return true;
                }
            });
            advSearchWebView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            break;
                        }
                        case MotionEvent.ACTION_UP: {
                            currentAdvanceSearchWebView.setWebViewClient(new CustomWebViewClient());
                            hideEnrichmentAndBookmarkTabs();
                            break;
                        }
                        default:
                            break;
                    }
                    return false;
                }
            });

            currentSemaWebView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            break;
                        }
                        case MotionEvent.ACTION_UP: {
                            hideEnrichmentAndBookmarkTabs();
                            break;
                        }
                        default:
                            break;
                    }
                    return false;
                }
            });

        }

        private void loadNormalSearch(SemaWebView webView) {
            if (searchEditText != null && searchEditText.getText().toString().trim() != "") {
                String searchText = searchEditText.getText().toString().trim();
                int i = 0;
                //searchText = searchText.replaceAll("\'\'", "\'").replaceAll("\"", "dqzd");
                webView.loadJSScript("javascript:Highlight('" + searchText + "','" + i + "', \"searchhighlight\");", webView);
            }
        }

        private void loadAdvancedSearch(SemaWebView webView) {
            if (etAdv != null && etAdv.getText().toString() != "") {
                String searchText = etAdv.getText().toString().trim();
                int i = 0;
                //searchText = searchText.replaceAll("\'\'", "\'").replaceAll("\"", "dqzd");
                webView.loadJSScript("javascript:Highlight('" + searchText + "','" + i + "', \"searchhighlight\");", webView);
                etAdv.setText("");
            }
        }
    }

    public void hideEnrichmentAndBookmarkTabs() {
        if (seekBar.isShown()) {
//            currentParentEnrRLayout.setVisibility(View.GONE);
            enr_layout.setVisibility(View.GONE);
            currentParentBklayout.setVisibility(View.GONE);
            seekBar.setVisibility(View.GONE);
            tvSeekBar.setVisibility(View.GONE);
            search_btn.setVisibility(View.GONE);
            et_layout.setVisibility(View.GONE);
        } else {
            if (CurrentRecyclerView.getAdapter().getItemCount() > 1) {
                currentParentEnrRLayout.setVisibility(View.VISIBLE);
            }
            if (currentBkmarkLayout.getChildCount() > 0) {
                currentParentBklayout.setVisibility(View.VISIBLE);
            }
            enr_layout.setVisibility(View.VISIBLE);
            seekBar.setVisibility(View.VISIBLE);
            tvSeekBar.setVisibility(View.VISIBLE);
            search_btn.setVisibility(View.VISIBLE);
        }
    }

    private void displaycurrentBookMarkTitle(View v) {
        if (currentBookMarkEnrList == null) {
            currentBookMarkEnrList = db.getAllbookMarkTabListForTheBook(currentPageNumber, currentBook.getBookID(), BookViewReadActivity.this);
        }
        bkmInEditMode = false;
        bkmInDeleteMode = false;
        //final ArrayList<BookMarkEnrichments> currentbmrkList = bookMarkedAdvTabList.get(pageNumber);

        advbkmarkpopup = new PopoverView(BookViewReadActivity.this, R.layout.advsearchtitles);
        advbkmarkpopup.setContentSizeForViewInPopover(new Point((int) getResources().getDimension(R.dimen.info_popover_width), (int) getResources().getDimension(R.dimen.info_popover_height)));
        advbkmarkpopup.setDelegate(BookViewReadActivity.this);
        advbkmarkpopup.showPopoverFromRectInViewGroup(designPageLayout, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionUp, true);
        adv_bkmrktitle = (ListView) advbkmarkpopup.findViewById(R.id.lv_bkmark);
        adv_bkmrktitle.setAdapter(new adv_bkmrkadapter(BookViewReadActivity.this, currentBookMarkEnrList));
        final Button btn_edit = (Button) advbkmarkpopup.findViewById(R.id.btn_edit);
        final Button bt_del = (Button) advbkmarkpopup.findViewById(R.id.btn_del);

        btn_edit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!bkmInEditMode) {
                    bkmInEditMode = true;
                    adv_bkmrktitle.invalidateViews();
                    btn_edit.setText(getResources().getString(R.string.done));
                } else {
                    bkmInEditMode = false;
                    adv_bkmrktitle.invalidateViews();
                    btn_edit.setText(getResources().getString(R.string.edit));
                }
            }
        });

        bt_del.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!bkmInDeleteMode) {
                    bkmInDeleteMode = true;
                    adv_bkmrktitle.invalidateViews();
                    bt_del.setText(getResources().getString(R.string.done));
                } else {
                    bkmInDeleteMode = false;
                    adv_bkmrktitle.invalidateViews();
                    bt_del.setText(getResources().getString(R.string.delete));
                }
            }
        });
        adv_bkmrktitle.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View v, int position,
                                    long arg3) {
                BookMarkEnrichments bookMarkEnr = currentBookMarkEnrList.get(position);
                // createAdvancedSearchTab(bookMarkEnr.getEnrichmentPath(), bookMarkEnr.getEnrichmentTitle());
                new createAdvanceSearchTab(bookMarkEnr.getEnrichmentPath(), bookMarkEnr.getEnrichmentTitle()).execute();
                advbkmarkpopup.dissmissPopover(true);
            }
        });
    }

    public void removingBkMrkButton(int position, int pageno, int id) {
        currentBkmarkLayout.removeViewAt(position);
        db.executeQuery("delete from BookmarkedSearchTabs where Id='" + id + "' and PageNo='" + pageno + "' and Bid='" + currentBook.getBookID() + "'");
        adv_bkmrktitle.invalidateViews();
        if (currentBkmarkLayout.getChildCount() < 1) {
            currentParentBklayout.setVisibility(View.GONE);
            advbkmarkpopup.dissmissPopover(true);
        }
    }

    public void updateNameToButton(int position, String newtext) {
        newtext = newtext.replace("''", "'");
        //RelativeLayout rlview = (RelativeLayout) currentbkmarkLayout.getChildAt(position);
        Button nextEnrichSelectView = (Button) currentBkmarkLayout.getChildAt(position);
        nextEnrichSelectView.setText(newtext);
    }

    private class loadBookmarkedEnrichmentTabs extends AsyncTask<Void, Void, Void> {

        ArrayList<BookMarkEnrichments> bkEnrichmentTabList;
        int pageNumber;
        LinearLayout ll_bkContainer;
        RelativeLayout parentBkEnrRLayout;

        public loadBookmarkedEnrichmentTabs(int pageNumber, LinearLayout ll_bkContainer, RelativeLayout parentBkEnrRLayout) {
            this.pageNumber = pageNumber;
            this.ll_bkContainer = ll_bkContainer;
            this.parentBkEnrRLayout = parentBkEnrRLayout;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            bkEnrichmentTabList = db.getAllbookMarkTabListForTheBook(pageNumber, currentBook.getBookID(), BookViewReadActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            for (int i = 0; i < bkEnrichmentTabList.size(); i++) {
                parentBkEnrRLayout.setVisibility(View.VISIBLE);
                BookMarkEnrichments bkEnrTabs = bkEnrichmentTabList.get(i);
                bkEnrTabs.createEnrichmentTabs(ll_bkContainer);
            }
            super.onPostExecute(aVoid);
        }
    }

    public class loadEnrichmentTabs extends AsyncTask<Void, Void, Void> {

        ArrayList<Enrichments> enrichmentTabList;
        int pageNumber;
        RecyclerView recyclerview;
        int Id;
        RelativeLayout layout;
        boolean deleteTab;

        public loadEnrichmentTabs(int pageNumber, RecyclerView recyclerView, int SelectecEnrId, RelativeLayout parentEnrRLayout, boolean delete) {
            this.pageNumber = pageNumber;
            this.recyclerview = recyclerView;
            this.Id = SelectecEnrId;
            this.layout = parentEnrRLayout;
            this.deleteTab = delete;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            layout.setVisibility(View.INVISIBLE);
            //enrichmentLayout.removeViews(1, enrichmentLayout.getChildCount()-1);
            // recyclerview.removeViews(1,recyclerview.getChildCount()-1);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            enrichmentTabList = db.getEnrichmentTabListForBookReadAct1(currentBook.getBookID(), pageNumber, BookViewReadActivity.this);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // if (enrichmentTabList.size() > 0){
            boolean selected = false;
            for (int i = 0; i < enrichmentTabList.size(); i++) {
                Enrichments enrich = enrichmentTabList.get(i);
                selected = false;
                if (enrich.getEnrichmentId() == Id) {
                    enrichmentTabList.remove(i);
                    enrich.setEnrichmentSelected(true);
                    enrichmentTabList.add(i, enrich);
                    currentEnrichmentTabId = enrich.getEnrichmentId();
                    selected = true;
                    break;
                }
            }
            if (selected && Id != 0) {
                Enrichments enrich = enrichmentTabList.get(0);
                enrich.setEnrichmentSelected(false);
            }
            if (enrichmentTabList.size() > 1) {
                layout.setVisibility(View.VISIBLE);
            } else {
                layout.setVisibility(View.INVISIBLE);
            }
            ArrayList<Enrichments> topEnrList = new ArrayList<>();
            for (Enrichments enrichments : enrichmentTabList) {
                if (enrichments.getCategoryID() == 0) {
                    topEnrList.add(enrichments);
                }
            }
            if (SubtabsAdapter.level1 == 0 && topEnrList.size() > 0 && deleteTab) {
                clickedEnrichments(topEnrList.get(0));
            }
            loadView(topEnrList, recyclerview);
            currentParentEnrRLayout.setVisibility(View.VISIBLE);
        }
    }

    private void clickedBtnHomeEnrichment(Button view, LinearLayout ll_container, SemaWebView viewPagerWebView, int viewPagerPageNo, ImageView viewPagerCanvasImgView) {
        if (rl.getChildCount() == 7) {
            rl.removeViewAt(6);
        }
        currentAdvanceSearchWebView.setVisibility(View.INVISIBLE);
        currentWebView.setVisibility(View.VISIBLE);
        currentEnrichmentTabId = 0;
        currentBtnEnrichment = null;
        makeEnrichedBtnSlected(view, ll_container);
        checkForBookMark(currentPageNumber, currentEnrichmentTabId);
        checkForPaintAndApply(viewPagerCanvasImgView, viewPagerPageNo, currentEnrichmentTabId);
        loadHighlightAndNoteForViewPager(viewPagerWebView, viewPagerPageNo, currentEnrichmentTabId);
    }

    public void clickedEnrichments(Enrichments enrButton) {
        if (rl.getChildCount() == 7) {
            rl.removeViewAt(6);
        }
        currentAdvanceSearchWebView.setVisibility(View.INVISIBLE);
        currentWebView.setVisibility(View.VISIBLE);
        currentEnrichmentTabId = enrButton.getEnrichmentId();
        currentBtnEnrichment = enrButton;
        // makeEnrichedBtnSlected(enrButton, currentEnrTabsLayout);
        checkForBookMark(currentPageNumber, currentEnrichmentTabId);
        checkForPaintAndApply(currentCanvasImgView, currentPageNumber, currentEnrichmentTabId);
        loadHighlightAndNoteForViewPager(currentWebView, enrButton.getEnrichmentPageNo(), enrButton.getEnrichmentId());
    }

    public class createAdvanceSearchTab extends AsyncTask<Void, Void, Void> {
        String Url;
        String title;

        public createAdvanceSearchTab(String url, String s) {
            Url = url;
            title = s;
        }

        @Override
        protected void onPreExecute() {
            title = title.replace("'", "''");
            int objSequentialId = db.getMaximumSequentialId(currentPageNumber, currentBook.getBookID()) + 1;
            db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,SequentialID,categoryID)values('" + currentBook.getBookID() + "','" + currentPageNumber + "','" + title + "','Search', '0','" + Url + "','" + objSequentialId + "','" + categoryID + "')");
            int objUniqueId = db.getMaxUniqueRowID("enrichments");
            title = title.replace("''", "'");
            currentEnrichmentTabId = objUniqueId;
            Enrichments enrichments = db.getEnrichment(objUniqueId, objSequentialId, BookViewReadActivity.this);
            currentBtnEnrichment = enrichments;
            if (clickableRecyclerView == null) {
                SubtabsAdapter.level1 = objUniqueId;
                new loadEnrichmentTabs(currentPageNumber, CurrentRecyclerView, currentEnrichmentTabId, currentParentEnrRLayout, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                if (clickableRecyclerView.getId() == R.id.gridView1) {
                    new loadEnrichmentTabs(currentPageNumber, CurrentRecyclerView, currentEnrichmentTabId, currentParentEnrRLayout, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    if (currentBtnEnrichment.getCategoryID() != 0) {
                        new loadSubEnrichmentTabs(currentPageNumber, subRecyclerView, 0, currentBtnEnrichment.getCategoryID(), clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    }
                } else if (clickableRecyclerView.getId() == R.id.gridView2) {
                    new loadSubEnrichmentTabs(currentPageNumber, subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    new loadSubEnrichmentTabs(currentPageNumber, subRecylerView1, 0, currentBtnEnrichment.getCategoryID(), clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                } else {
                    SubtabsAdapter.level3 = objUniqueId;
                    new loadSubEnrichmentTabs(currentPageNumber, subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                }
            }
            if (enrichments != null) {
                clickedAdvanceSearchtab(enrichments);
            }


        }

        @Override
        protected Void doInBackground(Void... voids) {

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // if (currentEnrTabsLayout.getWidth()>currentParentEnrRLayout.getWidth()) {
            // int  enrTabsWidth1 = currentEnrTabsLayout.getWidth();
            // sv.smoothScrollTo(enrTabsWidth1, 0);
            // }
        }
    }
   /* public void createAdvancedSearchTab(String url, String title){
        title = title.replace("'","''");
        db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path)values('" + currentBook.getBookID() + "','" + currentPageNumber + "','" + title + "','Search', '0','" + url + "')");
        int objUniqueId = db.getMaxUniqueRowID("enrichments");
        title = title.replace("''","'");
        RelativeLayout layout=new RelativeLayout(this);
        layout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        EnrichmentButton enrichmentButton = new EnrichmentButton(this, Globals.advancedSearchType, objUniqueId, currentBook.getBookID(), currentPageNumber, title, currentEnrTabsLayout.getChildCount()+1, 0, url, currentEnrTabsLayout);
        clickedAdvanceSearchtab(enrichmentButton);
        if (currentEnrTabsLayout.getWidth()>currentParentEnrRLayout.getWidth()) {
            int  enrTabsWidth1 = currentEnrTabsLayout.getWidth();
            sv.smoothScrollTo(enrTabsWidth1, 0);
        }
    }*/

    public void clickedAdvanceSearchtab(final Enrichments enrButton) {
        currentParentEnrRLayout.setVisibility(View.VISIBLE);
        currentWebView.setVisibility(View.INVISIBLE);
        currentEnrichmentTabId = enrButton.getEnrichmentId();
        currentBtnEnrichment = enrButton;
        // makeEnrichedBtnSlected(enrButton, currentEnrTabsLayout);
        checkForBookMark(currentPageNumber, enrButton.getEnrichmentId());
        checkForPaintAndApply(currentCanvasImgView, currentPageNumber, enrButton.getEnrichmentId());
        if (enrButton.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
            String mindMapPath = Globals.TARGET_BASE_MINDMAP_PATH + enrButton.getEnrichmentPath() + ".xml";
            if (!new File(mindMapPath).exists()) {
                UserFunctions.DisplayAlertDialog(BookViewReadActivity.this, R.string.mindmap_deleted_msg, R.string.mindmap_deleted);
                // removeEnrButtonAndSlctEnrButton(enrButton);
            } else {
                currentWebView.setVisibility(View.INVISIBLE);
                currentAdvanceSearchWebView.setVisibility(View.INVISIBLE);
                viewPager.webView.setVisibility(View.INVISIBLE);
                if (rl.getChildCount() == 7) {
                    rl.removeViewAt(6);
                }
                loadMindMap(enrButton.getEnrichmentPath(), rl, BookViewReadActivity.this);
                if (currentProgressbar.getVisibility() == View.VISIBLE) {
                    currentProgressbar.setVisibility(View.INVISIBLE);
                }
            }
        } else if (enrButton.getEnrichmentType().equals(Globals.downloadedEnrichmentType)) {
            clickedEnrichments(enrButton);
        } else {
            if (rl.getChildCount() == 7) {
                rl.removeViewAt(6);
            }
            currentAdvanceSearchWebView.clearView();
            currentAdvanceSearchWebView.setVisibility(View.VISIBLE);
            viewPager.webView.setVisibility(View.INVISIBLE);
            currentAdvanceSearchWebView.getSettings().setLoadWithOverviewMode(true);
            currentAdvanceSearchWebView.getSettings().setUseWideViewPort(true);
            currentAdvanceSearchWebView.getSettings().setJavaScriptEnabled(true);
            currentAdvanceSearchWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            currentAdvanceSearchWebView.clearCache(true);
            currentAdvanceSearchWebView.getSettings().setAllowContentAccess(true);
            currentAdvanceSearchWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            currentAdvanceSearchWebView.getSettings().setAllowFileAccess(true);
            currentAdvanceSearchWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
            currentAdvanceSearchWebView.getSettings().setDomStorageEnabled(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                currentAdvanceSearchWebView.getSettings().setAllowFileAccessFromFileURLs(true);
                currentAdvanceSearchWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
            }
            if (enrButton.getEnrichmentTitle().equals("BookPage")) {
                String[] split = enrButton.getEnrichmentPath().split("/");
                String filePath = "file:///" + Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/";
                File hyperPage = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/" + split[split.length - 1]);
                String srcString = UserFunctions.decryptFile(hyperPage);
                currentAdvanceSearchWebView.loadDataWithBaseURL(filePath, srcString, "text/html", "utf-8", null);
            } else {
                currentAdvanceSearchWebView.loadUrl(enrButton.getEnrichmentPath());
            }
            currentProgressbar.setVisibility(View.VISIBLE);
            currentAdvanceSearchWebView.setWebViewClient(new WebViewClient() {

                @Override
                public void onPageFinished(WebView view, String url) {
                    currentProgressbar.setVisibility(View.INVISIBLE);
                }

           /* @Override
             public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (enrButton.getEnrichmentType().equals(Globals.advancedSearchType)) {
                    try {
                        if (url != null) {
                            String path = url;
                            currentProgressbar.setVisibility(View.VISIBLE);
                            String[] adv_bkmrktype = path.split("/");
                            String url1 = adv_bkmrktype[2].replace(".", "/");
                            String[] type = url1.split("/");
                            for (int i = 0; i < type.length; i++) {
                                if (i == type.length - 2) {
                                    createAdvancedSearchTab(type[i], url);
                                    break;
                                }
                            }
                            return true;
                        } else {
                            return false;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }*/
            });
        }
    }

    public class CustomWebViewClient extends WebViewClient {
        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            Log.d("WEB_ERROR", "error code:" + errorCode + ":" + description);
            String url = "";
            //createAdvancedSearchTab(advtitle, url).execute();
        }

        @Override
        // Method where you(get) load the 'URL' which you have clicked
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (currentBtnEnrichment.getEnrichmentType().equals(Globals.advancedSearchType)) {
                try {
                    if (url != null) {
                        //currentProgressbar.setVisibility(View.VISIBLE);
                        String path = url;
                        String[] adv_bkmrktype = path.split("/");
                        String url1 = adv_bkmrktype[2].replace(".", "/");
                        String[] type = url1.split("/");
                        for (int i = 0; i < type.length; i++) {
                            if (i == type.length - 2) {
                                // createAdvancedSearchTab(url,type[i]);
                                new createAdvanceSearchTab(url, type[i]).execute();
                                break;
                            }
                        }
                        return true;
                    } else {
                        return false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

            return true;
        }
    }

    public void showAdvsearchEnrichDialog(final EnrichmentButton enrichmentButton) {
        final Dialog enrichDialog = new Dialog(this);
        enrichDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
        enrichDialog.setTitle(R.string.advsearch_bookmark);
        enrichDialog.setContentView(this.getLayoutInflater().inflate(R.layout.advsearch_bkmark_dialog, null));
        enrichDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, this), LinearLayout.LayoutParams.WRAP_CONTENT);
        final EditText editText = (EditText) enrichDialog.findViewById(R.id.enr_editText);
        editText.setText(enrichmentButton.getEnrichmentTitle());
        Button btnSave = (Button) enrichDialog.findViewById(R.id.enr_save);
        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String enrTitle = editText.getText().toString();
                enrTitle = enrTitle.replace("'", "''");
                db.executeQuery("insert into BookmarkedSearchTabs (Title, SearchTabId, PageNo,IsForAllPages,Path,Bid) values('" + enrTitle + "', '" + enrichmentButton.getEnrichmentId() + "', '" + enrichmentButton.getEnrichmentPageNo() + "','False','" + enrichmentButton.getEnrichmentPath() + "','" + currentBook.getBookID() + "')");
                addingBookMarkEnrichmnets(enrichmentButton, enrTitle);
                //advsearchbkmark = true;
                enrichDialog.setOnDismissListener(BookViewReadActivity.this);
                enrichDialog.dismiss();
            }
        });

        enrichDialog.show();
    }

    public void addingBookMarkEnrichmnets(EnrichmentButton enrButton, String enrTitle) {
        //ArrayList<BookMarkEnrichments> currentbmrkEnrichmentList = bookMarkedAdvTabList.get(pageNumber);
        currentParentBklayout.setVisibility(View.VISIBLE);
        int objUniqueId = db.getMaxUniqueRowID("BookmarkedSearchTabs");

        BookMarkEnrichments bookmarkenrichment = new BookMarkEnrichments(BookViewReadActivity.this);
        bookmarkenrichment.setEnrichmentId(objUniqueId);
        bookmarkenrichment.setEnrichmentPath(enrButton.getEnrichmentPath());
        bookmarkenrichment.setEnrichmentTitle(enrTitle);
        bookmarkenrichment.setEnrichmentPageNo(enrButton.getEnrichmentPageNo());
        bookmarkenrichment.setEnrichmentBid(currentBook.getBookID());
        bookmarkenrichment.setSearchTabId(enrButton.getEnrichmentId());
        //currentbmrkEnrichmentList.add(bookmarkenrichment);

        bookmarkenrichment.createEnrichmentTabs(currentBkmarkLayout);
        //makeBookmarkEnrichedBtnSlected(enrichBtnCreated);
    }

    public class lvimage extends AsyncTask<String, Void, Bitmap> {
        ImageView imagepath;
        ImageView img;
        Button btn;
        int id;

        public lvimage(Button btnEnrichmentTab, int i) {
            // TODO Auto-generated constructor stub
            this.btn = btnEnrichmentTab;
            id = i;
        }

        protected Bitmap doInBackground(String... urls) {
            String URLpath = urls[0];
            Bitmap bmp = null;
            try {
                URL url = new URL(URLpath);
                URLConnection ucon = url.openConnection();
                ucon.connect();
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                //System.out.println(bmp.getWidth());;
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return bmp;

        }

        protected void onPostExecute(Bitmap result) {
            if (result == null) {

            } else {
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, result.getWidth(), getResources().getDisplayMetrics());
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, result.getHeight(), getResources().getDisplayMetrics());

                Bitmap b = Bitmap.createScaledBitmap(result, width, height, false);
                Drawable f = new BitmapDrawable(getResources(), b);
                String Path = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "bkmarkimages/";
                //String imgpath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BookView.bookName+"/";
                UserFunctions.createNewDirectory(Path);
                String correct = Path + id + ".png";
                btn.setCompoundDrawablesWithIntrinsicBounds(f, null, null, null);

                if (id != 0) {
                    UserFunctions.saveBitmapImage(b, correct);
                }
            }
        }
    }

    public void deleteAdvancedEnrichTab(Enrichments enrichmentButton, Enrichments nextSelected) {
        removeBookMark(enrichmentButton.getEnrichmentPageNo(), enrichmentButton.getEnrichmentId());
        db.executeQuery("delete from enrichments where enrichmentID='" + enrichmentButton.getEnrichmentId() + "' and pageNO='" + enrichmentButton.getEnrichmentPageNo() + "' and Type='" + enrichmentButton.getEnrichmentType() + "'");
        currentEnrichmentTabId = nextSelected.getEnrichmentId();
        currentBtnEnrichment = nextSelected;
        new loadEnrichmentTabs(enrichmentButton.getEnrichmentPageNo(), CurrentRecyclerView, nextSelected.getEnrichmentId(), currentParentEnrRLayout, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        // removeEnrButtonAndSlctEnrButton(enrichmentButton);
    }

    public void deleteDownloadedEnrichTab(Enrichments button, Enrichments nextSelected) {
        deleteAdvancedEnrichTab(button, nextSelected);
    }

    public void deleteXmlEntryInUpdateRecordsXml(String path, Enrichments button) {
        String enrichValue = String.valueOf(button.getEnrichmentId());
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            Document document = documentBuilder.parse(new File(path));
            String tagName = "page" + (button.getEnrichmentPageNo());
            Node mainPageNode = document.getElementsByTagName("pages").item(0);
            Node pageNode = document.getElementsByTagName(tagName).item(0);
            NodeList nodes = pageNode.getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                Node element = nodes.item(i);
                NamedNodeMap attribute = element.getAttributes();
                Node nodeAttr = attribute.getNamedItem("ID");

                String[] idSplit = button.getEnrichmentPath().split("/");
                String idValue = idSplit[2];
                String idvalue1 = "Enriched" + nodeAttr.getNodeValue();

                if (idvalue1.equals(idValue)) {
                    //System.out.println("nodevalue entered");
                    pageNode.removeChild(element);
                    if (nodes.getLength() == 1) {
                        mainPageNode.removeChild(pageNode);
                    }
                    String fileName = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/Enrichments/Page" + button.getEnrichmentPageNo();
                    File enrichFileDelete = new File(fileName + "/" + idValue);
                    if (enrichFileDelete.exists()) {
                        UserFunctions.DeleteDirectory(enrichFileDelete);
                    }
                    File enrichFile = new File(fileName);
                    File[] enrichFileArray = enrichFile.listFiles();
                    if (enrichFileArray.length == 0) {
                        if (enrichFile.exists()) {
                            UserFunctions.DeleteDirectory(enrichFile);
                        }
                    }
                    break;
                }
            }

            // write the DOM object to the file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);

            StreamResult streamResult = new StreamResult(new File(path));
            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        }
    }

    /*private void removeEnrButtonAndSlctEnrButton(Enrichments enrichmentButton){
        int index;
        if (enrichmentButton.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)){
            index = currentEnrTabsLayout.indexOfChild(enrichmentButton);
            currentEnrTabsLayout.removeView(enrichmentButton);
        } else {
            RelativeLayout parentLayout = (RelativeLayout) enrichmentButton.getParent();
            index = currentEnrTabsLayout.indexOfChild(parentLayout);
            currentEnrTabsLayout.removeView(parentLayout);
        }
        if (enrichmentButton.isSelected()) {
            if (currentEnrTabsLayout.getChildCount() > 1) {
                View view = currentEnrTabsLayout.getChildAt(index - 1);
                if (view instanceof RelativeLayout) {
                    RelativeLayout rlView = (RelativeLayout) view;
                    EnrichmentButton enrButton = (EnrichmentButton) rlView.getChildAt(0);
                    clickedAdvanceSearchtab(enrButton);
                }else if (view instanceof EnrichmentButton){
                    if (((EnrichmentButton) view).getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)){
                        clickedAdvanceSearchtab((EnrichmentButton) view);
                    }else {
                        clickedEnrichments((EnrichmentButton) view);
                    }
                } else {
                    clickedBtnHomeEnrichment((Button) view, currentEnrTabsLayout, currentWebView, currentPageNumber, currentCanvasImgView);
                }
            } else {
                currentParentEnrRLayout.setVisibility(View.GONE);
                View view = currentEnrTabsLayout.getChildAt(0);
                clickedBtnHomeEnrichment((Button) view, currentEnrTabsLayout, currentWebView, currentPageNumber, currentCanvasImgView);
            }
        }
    }  */

    private Object[] getCurrentEnrichmentTabId(LinearLayout enrTabsLayout) {
        int enrichmentId = 0;
        Object[] object = new Object[2];
        for (int i = 0; i < enrTabsLayout.getChildCount(); i++) {
            View view = enrTabsLayout.getChildAt(i);
            if (view instanceof EnrichmentButton && view.isSelected()) {
                enrichmentId = ((EnrichmentButton) view).getEnrichmentId();
                object[0] = enrichmentId;
                object[1] = view;
                return object;
            } else if (view instanceof RelativeLayout) {
                RelativeLayout rlView = (RelativeLayout) view;
                EnrichmentButton advButton = (EnrichmentButton) rlView.getChildAt(0);
                if (advButton instanceof EnrichmentButton && advButton.isSelected()) {
                    enrichmentId = advButton.getEnrichmentId();
                    object[0] = enrichmentId;
                    object[1] = advButton;
                    return object;
                }

            }
            if (i != 0) {
                if (view instanceof RelativeLayout) {
                    RelativeLayout rlView = (RelativeLayout) view;
                } else {
                    EnrichmentButton enrichButton = (EnrichmentButton) view;
                    enrichButton.setOnTouchListener(mOnTouchListener);
                    enrichButton.setOnDragListener(mOnDragListener);
                }
            }

        }
        object[0] = enrichmentId;
        object[1] = null;
        return object;
    }

    /**
     * Make Enrich Button to be selected
     *
     * @param enrButton
     * @param enrTabsLayout
     */
    public void makeEnrichedBtnSlected(Button enrButton, LinearLayout enrTabsLayout) {
        for (int i = 0; i < enrTabsLayout.getChildCount(); i++) {
            View view = enrTabsLayout.getChildAt(i);
            if (view instanceof RelativeLayout) {
                RelativeLayout rlView = (RelativeLayout) view;
                View btnView = rlView.getChildAt(0);
                if (btnView.equals(enrButton)) {
                    btnView.setSelected(true);
                } else {
                    btnView.setSelected(false);
                }
            } else {
                if (view.equals(enrButton)) {
                    view.setSelected(true);
                } else {
                    view.setSelected(false);
                }
            }
        }
    }

    public void loadHighlightForViewPager(final SemaWebView view, int pageNumber, int enrichmentTabId) {
        final String[] sTextArray = db.getStext(currentBook.get_bStoreID(), pageNumber, enrichmentTabId);
        final int[] occcurenceArray = db.getOccurence(currentBook.get_bStoreID(), pageNumber, enrichmentTabId);
        for (int i = 0; i < occcurenceArray.length; i++) {
            view.loadJSScript("javascript:Highlight('" + sTextArray[i].replaceAll("\'\'", "\'").replaceAll("\"", "dqzd") + "', '" + occcurenceArray[i] + "', \"highlight\")", view);
        }
    }

    private void setSeekBarValues() {
        if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
            seekBar.setProgress(currentPageNumber - 1);
        } else {
            seekBar.setProgress(pageCount - currentPageNumber);
        }
        setSeekBarTextValues(currentPageNumber);
    }

    private void setSeekBarTextValues(int pageNumber) {
        if (pageNumber == 1) {
            tvSeekBar.setText(R.string.cover_page);
        } else if (pageNumber == pageCount) {
            tvSeekBar.setText(R.string.end_page);
        } else {
            tvSeekBar.setText("" + (pageNumber - 1) + "/" + (pageCount - 2) + "");
        }
    }

    public void loadHighlightAndNoteForViewPager(SemaWebView webView, int pageNumber, int tabId) {
        String pageNo;
        String filePath = "";
        File file = null;
        String sourceString = "";
        boolean isStoreBookFile = false;
        if (currentBtnEnrichment != null && currentBtnEnrichment.getEnrichmentType().equals(Globals.downloadedEnrichmentType)) {
            if (Globals.ISGDRIVEMODE()) {
                if (pageNumber == currentBtnEnrichment.getEnrichmentPageNo()) {
                    file = new File(currentBtnEnrichment.getEnrichmentPath());
                    filePath = "file:///" + currentBtnEnrichment.getEnrichmentPath();
                    if (!file.exists()) {
                        String modFilePath = currentBtnEnrichment.getEnrichmentPath().replace("/index.htm", "");
                        modFilePath = modFilePath.substring(0, modFilePath.lastIndexOf("/"));
                        modFilePath = modFilePath + "/index.htm";
                        file = new File(modFilePath);
                        filePath = "file:///" + modFilePath;
                        //    filePath = "file:///" + Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/";
                    }
                } else {
                    pageNo = String.valueOf(pageNumber);
                    file = new File(currentBookPath + pageNo + ".htm");
                    filePath = "file:///" + currentBookPath;
                    isStoreBookFile = true;
                }
            } else {
                if (pageNumber == currentBtnEnrichment.getEnrichmentPageNo()) {
                    file = new File(currentBookPath + currentBtnEnrichment.getEnrichmentPath());
                    filePath = "file:///" + currentBookPath + currentBtnEnrichment.getEnrichmentPath();
                } else {
                    pageNo = String.valueOf(pageNumber);
                    file = new File(currentBookPath + pageNo + ".htm");
                    filePath = "file:///" + currentBookPath;
                    isStoreBookFile = true;
                }
                //filePath = "file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/";
            }

        } else {
            if (tabId > 0) {
                String enrBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/";
                pageNo = pageNumber + "-" + tabId;
                if (new File(enrBookPath + pageNo + ".htm").exists()) {
                    file = new File(enrBookPath + pageNo + ".htm");
                    filePath = "file:///" + Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/";
                    //filePath = "file:///" + enrBookPath + pageNo + ".htm";
                } else {
                    pageNo = String.valueOf(pageNumber);
                    file = new File(currentBookPath + pageNo + ".htm");
                    filePath = "file:///" + currentBookPath;
                    isStoreBookFile = true;
                    //filePath = "file:///" + currentBookPath + pageNo + ".htm";
                }
            } else {
                pageNo = String.valueOf(pageNumber);
                file = new File(currentBookPath + pageNo + ".htm");
                filePath = "file:///" + currentBookPath;
                isStoreBookFile = true;
                //filePath = "file:///" + currentBookPath + pageNo + ".htm";
            }
        }
        if (isStoreBookFile) {
            sourceString = UserFunctions.decryptFile(file);
        } else {
            sourceString = readFileFromSDCard(file);
        }
        sourceString = loadNoteDiv(sourceString, pageNumber, tabId, webView);
        loadNoteDiv(sourceString, pageNumber, tabId, webView);
        sourceString = sourceString.replaceAll("(?:</title>)", "</title>" + style);
        sourceString = sourceString.replaceAll("file:///data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + currentBook.getBookID() + "/", "");
        sourceString = sourceString.replaceAll("../FreeFiles", "FreeFiles");
        sourceString = sourceString.replaceFirst("<head>", "<head><script src=" + jsFilePath + "></script><script src=" + jsAndroidSelection + "></script><script src=" + jsjquery + "></script><script src=" + jsRange + "></script><script src=" + jsRangeSerializer + "></script>");
        webView.loadDataWithBaseURL(filePath, sourceString, "text/html", "utf-8", null);
    }

    private static String getNoteStyle() {
        String colorValue = "<style type=\"text/css\">.highlight {background-color:#FFCCCC;color:inherit;text-decoration:inherit}";
        colorValue = colorValue + ".Color1 {background-color:#F1E9B8;color:inherit;text-decoration:inherit}";
        colorValue = colorValue + ".Color2 {background-color:#D6F2AF;color:inherit;text-decoration:inherit}";
        colorValue = colorValue + ".Color3 {background-color:#B1D4F5;color:inherit;text-decoration:inherit}"; //#66CCFF
        colorValue = colorValue + ".Color4 {background-color:#FEC2E5;color:inherit;text-decoration:inherit}";
        colorValue = colorValue + ".Color5 {background-color:#E2C4EE;color:inherit;text-decoration:inherit}";
        colorValue = colorValue + ".Color6 {background-color:#F2F2F2;color:inherit;text-decoration:inherit}";
        colorValue = colorValue + ".searchhighlight {background-Color:#CC9966;color:inherit;text-decoration:inherit}";
        colorValue = colorValue + "</style>";
        return colorValue;
    }

    public String loadNoteDiv(String sourceString, int pageNumber, int tabId, SemaWebView webView) {
        Object[] str = db.loadNoteDivToDatabase(currentBook.get_bStoreID(), pageNumber, tabId);
        String[] TID = (String[]) str[0];
        String[] SText = (String[]) str[1];
        String[] Ocuurences = (String[]) str[2];
        String[] NPos = (String[]) str[3];
        String[] CID = (String[]) str[4];
        for (int i = 0; i < SText.length; i++) {
            String pos = NPos[i];
            if (!pos.equals("0")) {
                String noteKeyword = NoteKeywordLoadDiv(SText[i], Ocuurences[i], NPos[i], sourceString, TID[i], CID[i]);
                sourceString = sourceString.replace("</body>", "" + noteKeyword + "</body>");
            }
        }
        return sourceString;
    }

    private static String NoteKeywordLoadDiv(String stext, String Occurences,
                                             String YPos, String sourceString2, String TID, String CID) {
        String noteImagePath = Environment.getDataDirectory().getPath().concat("/data/" + Globals.getCurrentProjPackageName() + "/files/").concat("BookMarkImage/stick_samll.png");
        String lNoteDiv = "<div style=z-index:101;position:absolute;width:46;height:46;right:5;top:" + YPos + ";background-image:url(" + noteImagePath + ");background-repeat:no-repeat; onclick=window.location=\"/E/" + TID + "\";>&nbsp;</div>";
        return lNoteDiv;
    }

    private void NoteKeywordLoad(String stext, String Occurences,
                                 String YPos, String sourceString2, String TID, String CID, SemaWebView webView) {
        String ColorValue = "";
        if (CID.contentEquals("1")) {
            ColorValue = "color1";
        } else if (CID.contentEquals("2")) {
            ColorValue = "color2";
        } else if (CID.contentEquals("3")) {
            ColorValue = "color3";
        } else if (CID.contentEquals("4")) {
            ColorValue = "color4";
        } else if (CID.contentEquals("5")) {
            ColorValue = "color5";
        } else if (CID.contentEquals("6")) {
            ColorValue = "color6";
        }
        //webView.loadUrl("javascript:Highlight('"+stext+"', '"+Occurences+"', '"+ColorValue+"')");
        webView.loadJSScript("javascript:Highlight('" + stext + "', '" + Occurences + "', '" + ColorValue + "')", webView);
    }


    private static String readFileFromSDCard(File file) {
        if (!file.exists()) {
            //changes for empty mainpage in book
            String filePath = "file:///android_asset/nomainpage.html";
            String sourceString = "<html> <head> <title>Main Page Not Found</title> </head> <body><!-- inullpageexists --> </body> </html>";
            return sourceString;
            //throw new RuntimeException("File not found");
        }

        BufferedReader reader = null;
        StringBuilder builder = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line + "\n");//bug fixed
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

    /**
     * Load Locale language from shared preference and change language in the application
     */
    private void loadLocale() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String language = prefs.getString(Globals.languagePrefsKey, "en");
        scopeId = prefs.getString(Globals.sUserIdKey, "");
        userName = prefs.getString(Globals.sUserNameKey, "");
        scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
        email = prefs.getString(Globals.sUserEmailIdKey, "");
        changeLang(language);
    }

    /**
     * change language in the application
     *
     * @param language
     */
    private void changeLang(String language) {
        Locale myLocale = new Locale(language);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    public static String getSelectedPosition(String selText) {

        selText = selText.replaceAll("</span>", "");
        selText = selText.replaceAll("<span style=\"background-Color:#FFCCCC\">", "");
        selText = selText.replaceAll("<span style=\"background-Color:#F6E993\">", "");
        selText = selText.replaceAll("<span style=\"background-Color:#C1E391\">", "");
        selText = selText.replaceAll("<span style=\"background-Color:#B1D8FF\">", "");
        selText = selText.replaceAll("<span style=\"background-Color:#FCACDA\">", "");
        selText = selText.replaceAll("<span style=\"background-Color:#E0B4F0\">", "");
        selText = selText.replaceAll("<span style=\"background-Color:#F2F2F2\">", "");

        String[] splres = selText.split("\\|");

        String result1 = splres[0];

        String encodedString = selText.replaceAll("\"", "\"\"");
        encodedString = encodedString.replaceAll("\'", "\'\'");
        encodedString = encodedString.replaceAll("\r", " ");
        encodedString = encodedString.replaceAll("\n", " ");
        encodedString = encodedString.replaceAll("\t", " ");

        String retVal = encodedString;

        String str = retVal + "|" + result1;
        return str;
    }

    public int getHiighCount(String text) {
        int highlightCount = db.getHighlightCount(currentBook.get_bStoreID(), currentPageNumber, text);
        return highlightCount;
    }

    public int getNoteCount(String text) {
        int noteCount = db.getNoteCountFromDB(currentBook.get_bStoreID(), currentPageNumber, text);
        return noteCount;
    }

    public void addHighlights(String iVal, SemaWebView webView) {
        String[] searchResults = iVal.split("\\|");
        String sTextTemp = searchResults[3].trim();
        String query = "insert into tblHighlight(BName,PageNo,SText,Occurence,Color,TabNo) values('" + currentBook.get_bStoreID() + "', '" + currentPageNumber + "', '" + sTextTemp + "', '" + searchResults[1] + "', '', '" + currentEnrichmentTabId + "')";
        db.executeQuery(query);
        loadHighlightForViewPager(webView, currentPageNumber, currentEnrichmentTabId);
    }

    public void removeHighlights(String iVal, SemaWebView webView) {
        String[] searchResults = iVal.split("\\|");
        String sTextTemp = searchResults[3].trim();
        String query = "delete from tblHighlight where BName='" + currentBook.get_bStoreID() + "' and PageNo='" + currentPageNumber + "' and TabNo='" + currentEnrichmentTabId + "' and SText='" + sTextTemp + "' and Occurence='" + searchResults[1] + "'";
        db.executeQuery(query);
        loadHighlightAndNoteForViewPager(webView, currentPageNumber, currentEnrichmentTabId);
    }

    private void initializeNotePopUp() {
        notePopUp = new Dialog(this);
        notePopUp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        notePopUp.getWindow().setBackgroundDrawable(new ColorDrawable(BookViewReadActivity.this.getResources().getColor(R.color.semi_transparent)));
        notePopUp.getWindow().setLayout(Globals.getDeviceIndependentPixels(300, BookViewReadActivity.this), RelativeLayout.LayoutParams.WRAP_CONTENT);

        //notePopUp = new PopupWindow(this);
        notePopupView = getLayoutInflater().inflate(R.layout.note_popup, null, false);
        noteRelativeLayout = (RelativeLayout) notePopupView.findViewById(R.id.noteRelativeLayout);
        noteColorSelectionContainer = (RelativeLayout) notePopupView.findViewById(R.id.ll_color_selection_container);
        notePopUp.setContentView(notePopupView);
//        notePopUp.setBackgroundDrawable(null);
//        notePopUp.setOutsideTouchable(false);
//        notePopUp.setFocusable(true);
//        notePopUp.setTouchable(true);
        noteList = (ListView) notePopupView.findViewById(R.id.notelistView);

        noteList.setDividerHeight(2);

        noteList.setAdapter(new noteListAdapter(this));
        noteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                String TESN = pagesValue.get(position);
                SN = TESN;
                String[] searchResults = SN.split("\\|");
                if (searchResults[0].contentEquals("E")) {

                    noteColorSelectionContainer.setVisibility(View.VISIBLE);
                    txtNote.setVisibility(View.GONE);
                    btnDone.setVisibility(View.VISIBLE);
                    btnCancel.setVisibility(View.GONE);
                    etNote.setVisibility(View.VISIBLE);
                    noteList.setVisibility(View.GONE);
                    String[] searchResultsText = db.getRecord(searchResults[1]).split("\\|");
                    noteSelText = searchResultsText[0];
                    etNote.setText(searchResultsText[0]);
                    CC = Integer.parseInt(searchResultsText[1]) - 1;
                    etNote.requestFocus();
                    Editable text = etNote.getText();
                    etNote.setSelection(etNote.getText().length());

                    InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                    inputmanager.showSoftInput(etNote, inputmanager.SHOW_IMPLICIT);
                    // noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
                    noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
                    notePopUp.show();

                }
            }

        });

        txtNote = (TextView) notePopupView.findViewById(R.id.txtNote);
        btnDone = (Button) notePopupView.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                //inputmanager.hideSoftInputFromWindow(etNote.getWindowToken(), 0);
                Log.e("str", "came");
                isUpdate = true;
                String[] searchResults = SN.split("\\|");
                String[] searchResultsText = db.getRecord(searchResults[1]).split("\\|");
                noteSelText = searchResultsText[0];
                mWebviewVisibility.post(mDoneNote);


                notePopUp.dismiss();
            }
        });
        btnCancel = (Button) notePopupView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                notePopUp.dismiss();
            }
        });
        imgBtnC1 = (ImageView) notePopupView.findViewById(R.id.imgBtnC1);
        imgBtnC1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                CC = 0;

                //noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
                noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
            }
        });
        imgBtnC2 = (ImageView) notePopupView.findViewById(R.id.imgBtnC2);
        imgBtnC2.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                CC = 1;

                //noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
                noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
            }
        });
        imgBtnC3 = (ImageView) notePopupView.findViewById(R.id.imgBtnC3);
        imgBtnC3.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                CC = 2;

                //noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
                noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
            }
        });
        imgBtnC4 = (ImageView) notePopupView.findViewById(R.id.imgBtnC4);
        imgBtnC4.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                CC = 3;

                //noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
                noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
            }
        });
        imgBtnC5 = (ImageView) notePopupView.findViewById(R.id.imgBtnC5);
        imgBtnC5.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                CC = 4;

                //noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
                noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
            }
        });
        imgBtnC6 = (ImageView) notePopupView.findViewById(R.id.imgBtnC6);
        imgBtnC6.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                CC = 5;

                //noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
                noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
            }
        });

        LinearLayout customEditTxtContainer = (LinearLayout) notePopupView.findViewById(R.id.customEditTxtContainer);
        LinearLayout.LayoutParams textViewLayoutParams2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, deviceHeight / 4);
        etNote = new NotesEditText(this, null);
        etNote.setText("");
        etNote.setTextColor(Color.rgb(0, 0, 0));
        etNote.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.store_header_text_size));
        etNote.setLineSpacing(8, 1);
        etNote.setBackgroundResource(R.drawable.transparent);
        etNote.setLayoutParams(textViewLayoutParams2);
        etNote.setGravity(Gravity.TOP);
        etNote.setCursorVisible(true);
        etNote.requestFocus();
        customEditTxtContainer.addView(etNote);
    }

    private void initializesmallNotePop() {
        //small note popup
        notepop = new Dialog(this);
        notepop.requestWindowFeature(Window.FEATURE_NO_TITLE);
        notepop.getWindow().setBackgroundDrawable(new ColorDrawable(BookViewReadActivity.this.getResources().getColor(R.color.semi_transparent)));
        if (Globals.isTablet()) {
            notepop.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.4));
        } else {
            notepop.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        }        // notepop = new PopupWindow(this);
        view_notepop = this.getLayoutInflater().inflate(R.layout.add_flashcard_layout, null, false);
        notepop.setContentView(view_notepop);
        rl_notepop = (RelativeLayout) view_notepop.findViewById(R.id.card_container);
        rl_notepop.setFocusable(true);
        titleText = (EditText) view_notepop.findViewById(R.id.title_edittext);
        titleText.setTag(titleText.getKeyListener());
        titleText.setKeyListener(null);
        descText = (EditText) view_notepop.findViewById(R.id.description_edittext);
        Button turn_btn = (Button) view_notepop.findViewById(R.id.btn_turn);
        turn_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlipAnimation flipAnimation = new FlipAnimation(titleText, descText);

                if (titleText.getVisibility() == View.GONE) {
                    flipAnimation.reverse();
                }
                rl_notepop.startAnimation(flipAnimation);
            }
        });
        CircleButton back_btn = (CircleButton) view_notepop.findViewById(R.id.btn_back);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notepop.dismiss();
            }
        });
        Button add_btn = (Button) view_notepop.findViewById(R.id.btnNext);
        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                inputmanager.hideSoftInputFromWindow(descText.getWindowToken(), 0);
                notepop.dismiss();
            }
        });
        ImageView iv_nColor1 = (ImageView) view_notepop.findViewById(R.id.imgBtnC1);
        ImageView iv_nColor2 = (ImageView) view_notepop.findViewById(R.id.imgBtnC2);
        ImageView iv_nColor3 = (ImageView) view_notepop.findViewById(R.id.imgBtnC3);
        ImageView iv_nColor4 = (ImageView) view_notepop.findViewById(R.id.imgBtnC4);
        ImageView iv_nColor5 = (ImageView) view_notepop.findViewById(R.id.imgBtnC5);
        ImageView iv_nColor6 = (ImageView) view_notepop.findViewById(R.id.imgBtnC6);
        notepop.setOnDismissListener(new DialogInterface.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                inputmanager.hideSoftInputFromWindow(descText.getWindowToken(), 0);
                isUpdate = false;
                mWebviewVisibility.post(mDoneNote);
            }
        });

        //custom_et.setKeyListener(null); //used for making et to readonly
        iv_nColor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CC = 0;
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg1);
                /*iv_toparrow_left.setBackgroundResource(R.drawable.arrow_1);
                iv_toparrow_center.setBackgroundResource(R.drawable.arrow_1);
                iv_toparrow_right.setBackgroundResource(R.drawable.arrow_1);
                iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_1);
                iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_1);
                iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_1);*/

            }
        });
        iv_nColor2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CC = 1;
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg2);
                /*iv_toparrow_left.setBackgroundResource(R.drawable.arrow_2);
                iv_toparrow_center.setBackgroundResource(R.drawable.arrow_2);
                iv_toparrow_right.setBackgroundResource(R.drawable.arrow_2);
                iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_2);
                iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_2);
                iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_2);*/
            }
        });
        iv_nColor3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CC = 2;
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg3);
                /*iv_toparrow_left.setBackgroundResource(R.drawable.arrow_3);
                iv_toparrow_center.setBackgroundResource(R.drawable.arrow_3);
                iv_toparrow_right.setBackgroundResource(R.drawable.arrow_3);
                iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_3);
                iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_3);
                iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_3);*/
            }
        });
        iv_nColor4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CC = 3;
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg4);
                /*iv_toparrow_left.setBackgroundResource(R.drawable.arrow_4);
                iv_toparrow_center.setBackgroundResource(R.drawable.arrow_4);
                iv_toparrow_right.setBackgroundResource(R.drawable.arrow_4);
                iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_4);
                iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_4);
                iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_4);*/
            }
        });
        iv_nColor5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CC = 4;
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg5);
                /*iv_toparrow_left.setBackgroundResource(R.drawable.arrow_5);
                iv_toparrow_center.setBackgroundResource(R.drawable.arrow_5);
                iv_toparrow_right.setBackgroundResource(R.drawable.arrow_5);
                iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_5);
                iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_5);
                iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_5);*/
            }
        });
        iv_nColor6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CC = 5;
                rl_notepop.setBackgroundResource(R.drawable.notepop_bg6);
                /*iv_toparrow_left.setBackgroundResource(R.drawable.arrow_6);
                iv_toparrow_center.setBackgroundResource(R.drawable.arrow_6);
                iv_toparrow_right.setBackgroundResource(R.drawable.arrow_6);
                iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_6);
                iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_6);
                iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_6);*/
            }
        });
    }

    //Note
    public void addNote(String iVal, String vc, SemaWebView webView) {
        int popWidth = 0;
        int popHeight = 0;
        noteSelText = iVal;

        if (isTablet) {
            if (webView.addNoteFromPopMenu) {
                //int coordsX = Math.round(mNoteBounds.left);//mScale);
                //int coordsY = Math.round(mNoteBounds.bottom);//mScale);

                displaySmallNotesPopup();
            } else {
                int notepopW = (int) (deviceWidth / 1.8); //2.5
                int notepopH = (int) (deviceHeight / 3.5);
                // popWidth = 65*deviceWidth/100;     //1600   //1040

                // popHeight = deviceHeight/3;       // 2464      //821
                notePopUp.getWindow().setLayout(notepopW, notepopH);
                // notePopUp.show();
                // notePopUp.showAtLocation(noteRelativeLayout, Gravity.CENTER, 0, 0);
                //  notePopUp.update(popWidth, popHeight);
            }
        } else {
            if (webView.addNoteFromPopMenu) {
                //int coordsX = Math.round(mNoteBounds.left);//mScale);
                //int coordsY = Math.round(mNoteBounds.bottom);//mScale);

                displaySmallNotesPopup();
            } else {
//                popWidth = 65*deviceWidth/100;
//                popHeight = deviceHeight/3;
                int notepopW = (int) (deviceWidth / 1.8); //2.5
                int notepopH = (int) (deviceHeight / 3.5);
                notePopUp.getWindow().setLayout(notepopW, notepopH);
                // notePopUp.show();
                // notePopUp.showAtLocation(noteRelativeLayout, Gravity.CENTER, 0, 0);
                // notePopUp.update(popWidth, popHeight);
            }

        }

        loadNote(vc);
    }

    public void loadNote(String vc) {
        SN = vc;

        //if(!(lngCategoryID == 1)){
        btnDone.setText(getResources().getString(R.string.done));
        btnCancel.setText(getResources().getString(R.string.cancel));
        txtNote.setText(getResources().getString(R.string.select_item_view_note));

        //}
        txtNote.setVisibility(View.GONE);
        btnDone.setVisibility(View.VISIBLE);
        btnCancel.setVisibility(View.GONE);
        etNote.setVisibility(View.VISIBLE);
        noteList.setVisibility(View.GONE);
        String[] searchResults = vc.split("\\|");
        if (searchResults[0].equalsIgnoreCase("E")) {
            String MultRecs = db.getMultipleRecord(searchResults[1]);
            pages = db.selectPageRecords(MultRecs);

            pagesValue = db.selectPageValuesRecord(MultRecs);

            if (pages.size() == 1) {

                noteColorSelectionContainer.setVisibility(View.VISIBLE);
                txtNote.setVisibility(View.GONE);
                btnDone.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.GONE);
                etNote.setVisibility(View.VISIBLE);
                noteList.setVisibility(View.GONE);
                String[] searchResultsText = db.getRecord(searchResults[1]).split("\\|");
                etNote.setText(searchResultsText[0]);
                CC = Integer.parseInt(searchResultsText[1]) - 1;
                etNote.requestFocus();
                Editable text = etNote.getText();
                etNote.setSelection(etNote.getText().length());
                InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                inputmanager.showSoftInput(etNote, inputmanager.SHOW_IMPLICIT);
                // noteRelativeLayout.setBackgroundResource(stickImgId[CC]);
                noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
                notePopUp.show();

                /*noteColorSelectionContainer.setVisibility(View.VISIBLE);
                txtNote.setVisibility(View.GONE);
                btnDone.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.GONE);
                etNote.setVisibility(View.VISIBLE);
                noteList.setVisibility(View.GONE);
                String[] searchResultsText = db.getRecord(searchResults[1]).split("\\|");
                etNote.setText(searchResultsText[0]);
                CC = Integer.parseInt(searchResultsText[1])-1;
                etNote.setSelection(etNote.getText().length());
                InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                inputmanager.showSoftInput(etNote, inputmanager.SHOW_IMPLICIT);

                noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
                notePopUp.show();*/
            } else {
                noteColorSelectionContainer.setVisibility(View.GONE);
                txtNote.setVisibility(View.VISIBLE);
                btnDone.setVisibility(View.GONE);
                btnCancel.setVisibility(View.VISIBLE);
                etNote.setVisibility(View.GONE);
                noteList.setVisibility(View.VISIBLE);
                noteRelativeLayout.setBackgroundResource(cardImgId[0]);
                notePopUp.show();
            }
        } else {
            noteColorSelectionContainer.setVisibility(View.VISIBLE);
            CC = 0;
            etNote.setText("");
            //   et.setText("");
            String[] searchresults = SN.split("\\|");
            titleText.setText(searchresults[1].trim());
            noteRelativeLayout.setBackgroundResource(cardImgId[CC]);
            rl_notepop.setBackgroundResource(R.drawable.notepop_bg1);
            //notePopUp.show();
            /*iv_toparrow_left.setBackgroundResource(R.drawable.arrow_1);
            iv_toparrow_center.setBackgroundResource(R.drawable.arrow_1);
            iv_toparrow_right.setBackgroundResource(R.drawable.arrow_1);
            iv_bottomarrow_left.setBackgroundResource(R.drawable.arrow_flip_1);
            iv_bottomarrow_center.setBackgroundResource(R.drawable.arrow_flip_1);
            iv_bottomarrow_right.setBackgroundResource(R.drawable.arrow_flip_1);*/
        }

        InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm1.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private void displaySmallNotesPopup() {
        if (Globals.isTablet()) {
            notepop.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.4));
        } else {
            notepop.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        }
        notepop.show();
    }

    final Runnable mDoneNote = new Runnable() {
        public void run() {/*
            String[] searchresults = SN.split("\\|");
            addNotetoDatabase(searchresults);
            if(searchresults[0].contentEquals("A")){
                SReloadOne(currentBook.get_bStoreID(), currentPageNumber, 0);
            }
            else{
                SReloadOne("B", 0, 1);
            }*/


            Log.e("ed", "note");

            String noteText = "";
            if (isTablet) {
                noteText = descText.getText().toString();
            } else {
                noteText = descText.getText().toString();
            }

            if (!isRecieve) {
                isRecieve = false;
                try {


                    if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                        JSONObject obj = new JSONObject();
                        obj.put("type", Constants.KEYBOOK);
                        obj.put("action", Constants.NOTE);
                        obj.put("selected_text", noteSelText);
                        if (isUpdate)
                            obj.put("note_text", etNote.getText());
                        else obj.put("note_text", noteText);
                        obj.put("note_query", SN);
                        obj.put("color", CC);
                        obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                        ss.publishMsg(TopicName, obj.toString() /*"textselect," + noteSelText + "," + SN + "<" + noteText+"<"+CC*/, BookViewReadActivity.this);
                    }
                } catch (Exception e) {
                    Log.e("ViewPager", "" + e.toString());
                }
            } else {
                noteText = mNotetext;
                // SN=mVC;
                // noteSelText=mnoteSelText;
            }


            String[] searchresults = SN.split("\\|");

            addNotetoDatabase(searchresults, noteText);
            // addNotetoDatabase(searchresults,noteText);
            if (searchresults[0].contentEquals("A")) {
                SReloadOne(currentBook.get_bStoreID(), currentPageNumber, 0);
            } else {
                SReloadOne("B", 0, 1);
            }
        }
    };

    public void SReloadOne(String BID, int PN, int RS) {
        if (RS == 0) {
            String[] searchresults = noteSelText.split("\\|");
            int sameLinePosition = db.getSameLinePosition(currentBook.get_bStoreID(), currentPageNumber, Integer.parseInt(searchresults[2]), currentEnrichmentTabId);
            if (sameLinePosition > 0) {
                updateNote(searchresults[0] + "|" + searchresults[1] + "|" + sameLinePosition);
            } else {
                updateNote(searchresults[0] + "|" + searchresults[1] + "|" + sameLinePosition);
            }
        }
        loadHighlightAndNoteForViewPager(currentWebView, currentPageNumber, currentEnrichmentTabId);
    }

    private void updateNote(String iVal) {
        String[] searchresults = iVal.split("\\|");
        String StextTemp = searchresults[0].trim();
        String query = "update tblNote set NPos='" + searchresults[2] + "' where BName='" + currentBook.get_bStoreID() + "' and PageNo='" + currentPageNumber + "' and TabNo='" + currentEnrichmentTabId + "' and SText='" + StextTemp + "' and Occurence='" + searchresults[1] + "'";
        db.executeQuery(query);
    }

    /* public void addNotetoDatabase(String[] searchresults) {
         //not using
         String noteText = "";
         if(isTablet){
             noteText = descText.getText().toString();
         }else{
             noteText = descText.getText().toString();
         }
         String StextTemp = searchresults[1].trim();
         String decriptedNoteText = convertEncodedToNoramalTextWithSelectedText(StextTemp, currentPageNumber);


         int colorVal = CC + 1;
         if (searchresults[0].contentEquals("A")) {
             String query = "insert into tblNote(BName,PageNo,SText,Occurence,SDesc,NPos,Color,ProcessSText,TabNo,Exported,NoteText,Flag,Rect) values" +
                     "('" + currentBook.get_bStoreID() + "','" + currentPageNumber + "','" + StextTemp + "','" + searchresults[2] + "','" + noteText + "','" + searchresults[3] + "','" + colorVal + "','" + searchresults[4] + "','" + currentEnrichmentTabId + "','" + "0" + "','" + decriptedNoteText + "','','')";
             db.executeQuery(query);
         } else {
             String query = "update tblNote set SDesc='" + etNote.getText() + "',Color='" + colorVal + "' where TID='" + searchresults[1] + "'";
             db.executeQuery(query);
         }
     }*/
    public void addNotetoDatabase(String[] searchresults, String noteText) {
        /*String noteText = "";
        if(isTablet){
            noteText = descText.getText().toString();
        }else{
            noteText = descText.getText().toString();
        }*/
        String StextTemp = searchresults[1].trim();
        int colorVal = CC + 1;
        if (searchresults[0].contentEquals("A")) {
            NoteData data = new NoteData();
            data.setNoteColor(colorVal);
            data.setNoteType(1);
            data.setNoteTabNo(currentEnrichmentTabId);
            data.setNotePageNo(currentPageNumber);
            data.setNoteTitle(StextTemp);
            data.setNoteDesc(noteText);
            noteDataArrayList.add(data);
            String query = "insert into tblNote(BName,PageNo,SText,Occurence,SDesc,NPos,Color,ProcessSText,TabNo,Exported,NoteType,CallStatus) values" +
                    "('" + currentBook.get_bStoreID() + "','" + currentPageNumber + "','" + StextTemp + "','" + searchresults[2] + "','" + noteText + "','" + searchresults[3] + "','" + colorVal + "','" + searchresults[4] + "','" + currentEnrichmentTabId + "','" + "0" + "','1','0')";
           Log.e("values", currentBook.get_bStoreID()+currentPageNumber+StextTemp+searchresults[2]+noteText+searchresults[3]+colorVal+ searchresults[4]+currentEnrichmentTabId+ "0" +'1'+'0');
            db.executeQuery(query);
        } else {
            String query;
            ///noteText=etNote.getText().toString();0M5345من أصل1king5881 من أصل 0010

            if (isRecieve && !isUpdate) {
                query = "update tblNote set SDesc='" + noteText + "',Color='" + colorVal + "' where SDesc='" + noteSelText + "'";

            } else {
                query = "update tblNote set SDesc='" + etNote.getText() + "',Color='" + colorVal + "' where TID='" + searchresults[1] + "'";
            }

            db.executeQuery(query);
            if (noteDataArrayList != null) {
                noteDataArrayList.clear();
                noteDataArrayList = db.loadNoteDivToArray(currentBook.get_bStoreID());
            }
        }
        //  updateNotesCount();
        descText.setText("");
    }

    public void loadNoteForViewPager(SemaWebView view, int pNumber) {
        Object[] str = db.loadNoteDivToDatabase(currentBook.get_bStoreID(), pNumber, currentEnrichmentTabId);
        final String[] TID = (String[]) str[0];
        final String[] SText = (String[]) str[1];
        final String[] Ocuurences = (String[]) str[2];
        final String[] NPos = (String[]) str[3];
        final String[] CID = (String[]) str[4];
        for (int i = 0; i < SText.length; i++) {
            NoteKeywordLoadForViewPager(SText[i].replaceAll("\'\'", "\'").replaceAll("\"", "dqzd"), Ocuurences[i], NPos[i], TID[i], CID[i], view);
        }
    }

    public String convertEncodedToNoramalTextWithSelectedText(String selectedString, int pageno)

    {
        String selectedText = selectedString;


        selectedText = selectedText.replaceAll("\n", "").replaceAll("\r", "").replaceAll(" ", "");

        if (new File(currentBookPath + "FreeFiles/searchMapping.xml").exists()) {
            pageno = pageno - 1;
            if (searchMappingArray.isEmpty()) {
                searchMappingArray = UserFunctions.processXML(currentBookPath + "FreeFiles/searchMapping.xml");
            }
            // ArrayList<String> smContent = processXML("FreeFiles/searchMapping.xml");
            String selectedPAge = searchMappingArray.get(pageno);
            int index = selectedPAge.indexOf(selectedText);
            String indexToString = "" + index;

            //   ArrayList<String>  sbContent = processXML("FreeFiles/searchBridging.xml");
            if (searchBridgeArray.isEmpty()) {
                searchBridgeArray = UserFunctions.processXML(currentBookPath + "FreeFiles/searchBridging.xml");
            }
            String selectedPageBridge = searchBridgeArray.get(pageno);
            String[] sbContentArray = selectedPageBridge.split(",");
            ArrayList<String> stringList = new ArrayList<String>(Arrays.asList(sbContentArray));
            int indexOfSearch = stringList.indexOf(indexToString);


            if (searchIndexArray.isEmpty()) {
                searchIndexArray = UserFunctions.processXML(currentBookPath + "Search.xml");
            }

            String currentPage = searchIndexArray.get(pageno - 1);
            int index1 = indexOfSearch * 6;
            int index2 = (indexOfSearch + selectedText.length()) * 6;
            String searchString = "";
            if (currentPage.length() > index1 && currentPage.length() > index2 && index1 > 0 && index2 > 0)
                searchString = currentPage.substring(index1, index2);


            /*Spanned sample = Html.fromHtml(searchString);

            searchString = StringEscapeUtils.unescapeJava(searchString);*/

            searchString = UserFunctions.convertUnicode2String(searchString);
            searchString = UserFunctions.reverseString(searchString);
            return searchString;
        }
        return selectedText;
    }

    public void removeNote(String IVal, SemaWebView webView) {
        String[] searchResults = IVal.split("\\|");
        String sTextTemp = searchResults[3].trim();
        String query = "delete from tblNote where BName='" + currentBook.get_bStoreID() + "' and PageNo='" + currentPageNumber + "' and TabNo='" + currentEnrichmentTabId + "' and SText='" + sTextTemp + "' and Occurence='" + searchResults[1] + "'";
        db.executeQuery(query);
        loadHighlightAndNoteForViewPager(webView, currentPageNumber, currentEnrichmentTabId);
    }

    private void NoteKeywordLoadForViewPager(String stext, String Occurences,
                                             String YPos, String TID, String CID, SemaWebView view) {

        String ColorValue = "";
        if (CID.contentEquals("1")) {
            ColorValue = "color1";
        } else if (CID.contentEquals("2")) {
            ColorValue = "color2";
        } else if (CID.contentEquals("3")) {
            ColorValue = "color3";
        } else if (CID.contentEquals("4")) {
            ColorValue = "color4";
        } else if (CID.contentEquals("5")) {
            ColorValue = "color5";
        } else if (CID.contentEquals("6")) {
            ColorValue = "color6";
        }
        view.loadJSScript("javascript:Highlight('" + stext + "', '" + Occurences + "', '" + ColorValue + "')", view);
    }

    public class noteListAdapter extends BaseAdapter {

        public noteListAdapter(BookViewReadActivity bookView) {
        }

        public int getCount() {
            return pages.size();
        }

        public Object getItem(int position) {

            return null;
        }

        public long getItemId(int position) {

            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = getLayoutInflater().inflate(R.layout.notelist_txtfield, null);
            TextView tv = (TextView) vi.findViewById(R.id.textView1);
            tv.setText((CharSequence) pages.get(position));
            return vi;
        }

    }

    public void textToSpeech(String selectedText, SemaWebView webView) {
        //selectedText = webSelectedText;
        //new speakToME(webSelectedText, webView).execute();
        webSelectedText = selectedText;
        tts_language = detectLanguage(webSelectedText);

        String percentEscpe = Uri.encode(webSelectedText);
        if (tts_language.equals("ar")) {
            TtsAlertDialog();
        } else {
            speakOut();
        }
    }

    public void speakOut() {
        if (tts_language != null && !tts_language.equals("ar")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(webSelectedText, TextToSpeech.QUEUE_FLUSH, null, null);
            } else {
                tts.speak(webSelectedText, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    public void TtsAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BookViewReadActivity.this);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        builder.setTitle(R.string.warning);
        builder.setMessage(R.string.tts_alert);
        builder.show();
    }

    public class speakToME extends AsyncTask<Void, Void, Void> {

        String webSelectedText;
        SemaWebView webView;

        public speakToME(String webSelectedText, SemaWebView webView) {
            this.webSelectedText = webSelectedText;
            this.webView = webView;
        }

        @Override
        protected Void doInBackground(Void... params) {
            String language = detectLanguage(webSelectedText);
            //////System.out.println("language:"+language);

            String percentEscpe = Uri.encode(webSelectedText);
            try {
                if (language.equals("ar")) {

                    ByteArrayOutputStream bais = readBytes(new URL("http://translate.google.com/translate_tts?tl=ar&ie=utf-8&q=" + percentEscpe + ""));
                    // webView.playMp3(bais.toByteArray());
                } else {
                    ByteArrayOutputStream bais = readBytes(new URL("http://translate.google.com/translate_tts?tl=en&q=" + percentEscpe + ""));
                    // webView.playMp3(bais.toByteArray());
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {

                e.printStackTrace();
            }

            return null;
        }

        public String detectLanguage(String selectedText) {

            String alpha = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
            String[] alphaArray = alpha.split(",");
            for (int i = 0; i < alphaArray.length; i++) {
                String current = alphaArray[i];
                if (selectedText.contains(current)) {
                    return "en";
                }
            }
            return "ar";
        }

        public ByteArrayOutputStream readBytes(URL url) throws IOException {
            ByteArrayOutputStream bais = new ByteArrayOutputStream();
            InputStream is = null;
            try {
                is = url.openStream();
                byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
                int n;
                while ((n = is.read(byteChunk)) > 0) {
                    bais.write(byteChunk, 0, n);
                }
            } catch (IOException e) {
                System.err.printf("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
                e.printStackTrace();
                // Perform any other exception handling that's appropriate.
            } finally {
                if (is != null) {
                    is.close();
                }
            }
            return bais;
        }
    }

    public void share(final String webSelectedText, final SemaWebView webView) {
        //selectedText = webSelectedText;
        sharePopUp = new Dialog(this);
        sharePopUp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
        sharePopUp.setTitle("Share via");

        sharePopUp.setContentView(getLayoutInflater().inflate(R.layout.share, null));
        ListView advListView = (ListView) sharePopUp.findViewById(R.id.listViewShare);
        advListView.setDividerHeight(2);
        advListView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1f));
        advListView.setAdapter(new shareAdapter(this, webSelectedText));
        advListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        if (webView.allowPublish(200)) {
                            //mHandler.post(mUpdateResults);
                        }
                        break;

                    case 1:
                        if (webView.allowPublish(117)) {
//                            Intent i=new Intent(BookViewReadActivity.this,Tw.class);
//                            startActivity(i);

                            //new twitterLogin().execute();
                            TwitterDialog twitterDialog = new TwitterDialog(BookViewReadActivity.this);
                            twitterDialog.loginToTwitter(webSelectedText);

                        }

                        break;

                    case 2:
                        if (webView.allowPublish(200)) {
                            Intent i = new Intent(android.content.Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"recipient@example.com"});
                            i.putExtra(android.content.Intent.EXTRA_CC, new String[]{""});
                            i.putExtra(android.content.Intent.EXTRA_BCC, new String[]{""});
                            i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Note From Manahij");
                            i.putExtra(android.content.Intent.EXTRA_TEXT, webSelectedText);

                            try {
                                startActivity(Intent.createChooser(i, "Send mail...."));
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(BookViewReadActivity.this, "There are no email clients installed", Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;

                    default:
                        break;
                }
            }


           /* class twitterLogin extends AsyncTask<Void, Void, Void> {

                @Override
                protected Void doInBackground(Void... params) {


                    ConfigurationBuilder confbuilder = new ConfigurationBuilder();
                    Configuration conf = confbuilder
                            .setOAuthConsumerKey(Globals.CONSUMER_KEY)
                            .setOAuthConsumerSecret(Globals.CONSUMER_SECRET)
                            .build();
                    mTwitter = new TwitterFactory(conf).getInstance();
                    mTwitter.setOAuthAccessToken(null);
                    try {
                        mRequestToken = mTwitter.getOAuthRequestToken(Globals.CALLBACK_URL);
                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    super.onPostExecute(result);
                    if (mRequestToken != null) {
                        Intent intent = new Intent(BookViewReadActivity.this, TwitterLogin.class);
                        intent.putExtra(Globals.IEXTRA_AUTH_URL, mRequestToken.getAuthorizationURL());
                        startActivityForResult(intent, 1);
                    }
                    sharePopUp.dismiss();
                }

            }*/
        });
        sharePopUp.show();
    }

    private class shareAdapter extends BaseAdapter {

        private boolean isFbSessionActive;
        private GraphUser user;
        Session session;
        private String selectedText;

        public Integer[] mThumbIds = {
                R.drawable.facebook, R.drawable.twitter, R.drawable.emailiphone
        };

        public shareAdapter(BookViewReadActivity bookView, String selectedText) {
            this.selectedText = selectedText;
            session = Session.getActiveSession();
            if (session != null && session.isOpened()) {
                isFbSessionActive = true;
                Request.newMeRequest(session, new Request.GraphUserCallback() {

                    @Override
                    public void onCompleted(GraphUser _user, Response response) {
                        user = _user;
                    }
                }).executeAsync();
            } else {
                isFbSessionActive = false;
            }
        }

        @Override
        public int getCount() {
            return share.length;
        }

        @Override
        public Object getItem(int position) {

            return null;
        }

        @Override
        public long getItemId(int position) {

            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            if (convertView == null) {
                vi = getLayoutInflater().inflate(R.layout.advpopgrid, null);
            }
            TextView txtSearchItem = (TextView) vi.findViewById(R.id.textViewAdv);
            txtSearchItem.setTextColor(Color.BLACK);
            ImageView imgSearchImg = (ImageView) vi.findViewById(R.id.imageViewAdv);
            LinearLayout fbLayout = (LinearLayout) vi.findViewById(R.id.fb_layout);
            final LoginButton authButton = (LoginButton) vi.findViewById(R.id.authButton);
            authButton.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);

            authButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sharePopUp.dismiss();
                }
            });
            Button btnShare = (Button) vi.findViewById(R.id.button1);
            btnShare.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (user != null) {
                        publishFeedDialog(user.getName(), selectedText);
                    }
                }
            });

            if (position == 0) {
                if (isFbSessionActive) {
                    btnShare.setVisibility(View.VISIBLE);
                } else {
                    btnShare.setVisibility(View.GONE);
                }
                fbLayout.setVisibility(View.VISIBLE);
                txtSearchItem.setVisibility(View.GONE);
                imgSearchImg.setVisibility(View.GONE);
            } else {
                fbLayout.setVisibility(View.GONE);
                txtSearchItem.setVisibility(View.VISIBLE);
                imgSearchImg.setVisibility(View.VISIBLE);
            }

            txtSearchItem.setText(share[position]);
            imgSearchImg.setImageResource(mThumbIds[position]);
            return vi;
        }

    }

    private void publishFeedDialog(String userName, String selectedText) {
        Bundle parameters = new Bundle();
        //parameters.putString("link", "http://itunes.apple.com/us/app/manahij/id477095484?ls=1&mt=8");
        parameters.putString("link", "");
        parameters.putString("picture", "http://dev.sboook.com/manahijLogo.png");
        parameters.putString("name", userName + " has Shared");
        parameters.putString("caption", getResources().getString(R.string.app_name));
        parameters.putString("description", selectedText);
        parameters.putString("message", "Share your message!");

        // Invoke the dialog
        WebDialog feedDialog = (
                new WebDialog.FeedDialogBuilder(this,
                        Session.getActiveSession(),
                        parameters))
                .setOnCompleteListener(new WebDialog.OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values,
                                           FacebookException error) {
                        if (error == null) {
                            // When the story is posted, echo the success
                            // and the post Id.
                            final String postId = values.getString("post_id");
                            if (postId != null) {
                                Toast.makeText(BookViewReadActivity.this,
                                        "Posted successfully",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                // User clicked the Cancel button
                                Toast.makeText(BookViewReadActivity.this.getApplicationContext(),
                                        "Publish cancelled",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else if (error instanceof FacebookOperationCanceledException) {
                            // User clicked the "x" button
                            Toast.makeText(BookViewReadActivity.this.getApplicationContext(),
                                    "Publish cancelled",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Generic, ex: network error
                            Toast.makeText(BookViewReadActivity.this.getApplicationContext(),
                                    "Error Publish text",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .build();
        feedDialog.show();
    }

    private void initializeSearchPopUp() {
        searchPopup = new Dialog(BookViewReadActivity.this);
        searchPopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
        searchPopup.setTitle("Search Results");

        searchPopup.setContentView(getLayoutInflater().inflate(R.layout.search_list_popup, null));
        searchPopup.getWindow().setLayout((int) ((deviceWidth / 4) * 3.1), (int) ((deviceHeight / 4) * 2.1));//resize popup
        tv_noresult = (TextView) searchPopup.findViewById(R.id.tv_noresult);
        final ListView lv = (ListView) searchPopup.findViewById(R.id.listViewSearch);

        etSearch = (EditText) searchPopup.findViewById(R.id.editTextSearch);
        imgBtnBack = (ImageButton) searchPopup.findViewById(R.id.imageButtonBack);
        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                searchPopup.dismiss();
                advSearchPopUp.show();
                etSearch.setVisibility(View.VISIBLE);
                imgBtnBack.setVisibility(View.VISIBLE);
                searchPopup.findViewById(R.id.footer).setVisibility(View.VISIBLE);
            }
        });
        lv.setDividerHeight(2);
        lv.setAdapter(new searchListAdapter(this));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                String str1 = resultFound.get(arg2);
                String[] str = str1.split("page ");
                //pageNumber = Integer.parseInt(str[1]);
                final int page = Integer.parseInt(str[1]) + 1;
                searchPopup.dismiss();
                if (currentPageNumber != page) {
                    currentEnrichmentTabId = 0;
//                    isSetViewPagerItem = true;
                }
                currentPageNumber = page;
                loadViewPager();
            }
        });
    }

    private class searchListAdapter extends BaseAdapter {

        public searchListAdapter(BookViewReadActivity bookView) {

        }

        @Override
        public int getCount() {

            return resultFound.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = getLayoutInflater().inflate(R.layout.lng_txtfield, null);
            TextView tv = (TextView) vi.findViewById(R.id.textView1);
            tv.setText(resultFound.get(position));
            tv.setTextColor(Color.rgb(0, 0, 0));
            if (resultFound.isEmpty() == true)//changes-s
            {
                tv_noresult.setVisibility(View.VISIBLE);
            } else {
                tv_noresult.setVisibility(View.GONE);
            }
            return vi;
        }
    }

    public void advancedSearch(final String selectedText) {
        advSearchPopUp = new Dialog(BookViewReadActivity.this);
        advSearchPopUp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
        advSearchPopUp.setTitle("Search via");
        advSearchPopUp.setContentView(getLayoutInflater().inflate(R.layout.advsearch, null));
        ListView advListView = (ListView) advSearchPopUp.findViewById(R.id.listViewAdv);
        etAdv = (EditText) advSearchPopUp.findViewById(R.id.editTextAdv);
        advSearchPopUp.getWindow().setLayout((int) ((deviceWidth / 4) * 3.1), (int) ((deviceHeight / 4) * 2.1));//resize popup
        advListView.setDividerHeight(2);
        advListView.setAdapter(new advAdapter(this));
        advListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String url = null;
                String advSearchtext;
                switch (position) {
                    case 0:
                        isAdvancedSearch = true;
                        advSearchPopUp.dismiss();
                        resultFound.clear();

                        etSearch.setText(etAdv.getText().toString());
                        etSearch.setVisibility(View.GONE);

                        if (searchIndexArray.isEmpty()) {
                            processXML();
                        }
                        serachTableReload(selectedText);
                        searchPopup.show();
                        break;

//                    case 1:
//                        url = "http://projects.nooor.com/SemaBooksSearch/BkSearch.aspx?Srch=" + etAdv.getText().toString() + "";
//                        advSearchtext = etAdv.getText().toString();
//                        //clickedAdvancedSearch(advSearch[position], url);
//                        //new clickedAdvancedSearchTabs(advSearch[position], url).execute();
//                        //advSearchPopUp.setOnDismissListener(BookViewReadActivity.this);
//                       // createAdvancedSearchTab(url, advSearch[position]);
//                        new createAdvanceSearchTab(url, advSearch[position]).execute();
//                        advSearchPopUp.dismiss();
//                        break;
//
//                    case 2:
//                        url = "http://www.nooor.com/search/" + etAdv.getText().toString() + "/";
//                        advSearchtext = etAdv.getText().toString();
//                        //clickedAdvancedSearch(advSearch[position], url);
//                        //new clickedAdvancedSearchTabs(advSearch[position], url).execute();
//                        //advSearchPopUp.setOnDismissListener(BookViewReadActivity.this);
//                        //createAdvancedSearchTab(url, advSearch[position]);
//                        new createAdvanceSearchTab(url, advSearch[position]).execute();
//                        advSearchPopUp.dismiss();
//                        break;

                    case 1:
                        url = "http://www.google.com/search?q=" + etAdv.getText().toString() + "";
                        advSearchtext = etAdv.getText().toString();
                        //clickedAdvancedSearch(advSearch[position], url);
                        //new clickedAdvancedSearchTabs(advSearch[position], url).execute();
                        //advSearchPopUp.setOnDismissListener(BookViewReadActivity.this);
                        // createAdvancedSearchTab(url, advSearch[position]);
                        new createAdvanceSearchTab(url, advSearch[position]).execute();
                        advSearchPopUp.dismiss();
                        break;

                    case 2:
                        String text = etAdv.getText().toString();
                        String language = detectLanguage(text);

                        String percentEscpe = Uri.encode(text);
                        if (language.equals("ar")) {
                            url = "http://ar.wikipedia.org/wiki/" + percentEscpe + "";
                        } else {
                            url = "http://en.wikipedia.org/wiki/" + percentEscpe + "";
                        }
                        advSearchtext = etAdv.getText().toString();
                        //clickedAdvancedSearch( advSearch[position], url);
                        //new clickedAdvancedSearchTabs(advSearch[position], url).execute();
                        //advSearchPopUp.setOnDismissListener(BookViewReadActivity.this);
                        //createAdvancedSearchTab(url, advSearch[position]);
                        new createAdvanceSearchTab(url, advSearch[position]).execute();
                        advSearchPopUp.dismiss();
                        break;

                    case 3:
                        url = "http://m.youtube.com/#/results?q=" + etAdv.getText().toString() + "";
                        advSearchtext = etAdv.getText().toString();
                        //clickedAdvancedSearch(advSearch[position], url);
                        //new clickedAdvancedSearchTabs(advSearch[position], url).execute();
                        //advSearchPopUp.setOnDismissListener(BookViewReadActivity.this);
                        //createAdvancedSearchTab(url, advSearch[position]);
                        new createAdvanceSearchTab(url, advSearch[position]).execute();
                        advSearchPopUp.dismiss();
                        break;

                    case 4:
                        url = "http://search.yahoo.com/search?p=" + etAdv.getText().toString() + "";
                        advSearchtext = etAdv.getText().toString();
                        //clickedAdvancedSearch(advSearch[position], url);
                        //new clickedAdvancedSearchTabs(advSearch[position], url).execute();
                        //advSearchPopUp.setOnDismissListener(BookViewReadActivity.this);
                        //createAdvancedSearchTab(url, advSearch[position]);
                        new createAdvanceSearchTab(url, advSearch[position]).execute();
                        advSearchPopUp.dismiss();
                        break;

                    case 5:
                        url = "http://www.bing.com/search?q=" + etAdv.getText().toString() + "";
                        advSearchtext = etAdv.getText().toString();
                        //clickedAdvancedSearch( advSearch[position], url);
                        //new clickedAdvancedSearchTabs(advSearch[position], url).execute();
                        //new clickedAdvancedSearchTabs(advSearch[position], url).execute();
                        //advSearchPopUp.setOnDismissListener(BookViewReadActivity.this);
                        //createAdvancedSearchTab(url, advSearch[position]);
                        new createAdvanceSearchTab(url, advSearch[position]).execute();
                        advSearchPopUp.dismiss();
                        break;

                    case 8:
                        url = "http://www.ask.com/web?q=" + etAdv.getText().toString() + "";
                        advSearchtext = etAdv.getText().toString();
                        //clickedAdvancedSearch(advSearch[position], url);
                        //new clickedAdvancedSearchTabs(advSearch[position], url).execute();
                        //advSearchPopUp.setOnDismissListener(BookViewReadActivity.this);
                        //createAdvancedSearchTab(url, advSearch[position]);
                        new createAdvanceSearchTab(url, advSearch[position]).execute();
                        advSearchPopUp.dismiss();
                        break;

                    default:
                        break;
                }
            }
        });

      /*  if (!Globals.isTablet()) {
            isAdvancedSearch = true;
            //searchEditText.setText(selectedText);
            etAdv.setText(selectedText);
            //advSearchPopUp.show();

            //etSearch.setVisibility(View.VISIBLE);
            //imgBtnBack.setVisibility(View.VISIBLE);
            //searchPopup.findViewById(R.id.footer).setVisibility(View.VISIBLE);

            resultFound.clear();

            if(searchIndexArray.isEmpty()){
                processXML();
            }
            serachTableReload(selectedText);

            searchPopup.show();

            if(resultFound.isEmpty()==true)
            {
                tv_noresult.setVisibility(View.VISIBLE);
            }

            etSearch.setVisibility(View.GONE);
            imgBtnBack.setVisibility(View.GONE);
            searchPopup.findViewById(R.id.footer).setVisibility(View.GONE);
        } else {*/
        etAdv.setText(selectedText);
        advSearchPopUp.show();

        etSearch.setVisibility(View.VISIBLE);
        imgBtnBack.setVisibility(View.VISIBLE);
        searchPopup.findViewById(R.id.footer).setVisibility(View.VISIBLE);
//        }
    }

    public static String detectLanguage(String selectedText) {

        String alpha = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9";
        String[] alphaArray = alpha.split(",");
        for (int i = 0; i < alphaArray.length; i++) {
            String current = alphaArray[i];
            if (selectedText.toLowerCase().contains(current)) {
                return "en";
            }
        }
        return "ar";
    }

    private class advAdapter extends BaseAdapter {
        //private Context mContext;

        public Integer[] mThumbIds = {
                R.drawable.book, R.drawable.google1, R.drawable.wikipedia1, R.drawable.youtube1, R.drawable.yahoo_icon,
                R.drawable.bing_icon, R.drawable.ask
        };

        public advAdapter(BookViewReadActivity bookView) {

        }

        //@Override
        public int getCount() {

            return advSearch.length;
        }

        //@Override
        public Object getItem(int position) {

            return null;
        }

        //@Override
        public long getItemId(int position) {

            return 0;
        }

        //@Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            if (convertView == null) {
                vi = getLayoutInflater().inflate(R.layout.advpopgrid, null);
            }
            LinearLayout fblayout = (LinearLayout) vi.findViewById(R.id.fb_layout);
            fblayout.setVisibility(View.GONE);
            TextView txtSearchItem = (TextView) vi.findViewById(R.id.textViewAdv);
            txtSearchItem.setVisibility(View.VISIBLE);
            ImageView imgSearchImg = (ImageView) vi.findViewById(R.id.imageViewAdv);
            imgSearchImg.setVisibility(View.VISIBLE);
            txtSearchItem.setText(advSearch[position]);
            imgSearchImg.setImageResource(mThumbIds[position]);
            return vi;
        }

    }

    private void serachTableReload(String searchText) {
        String withoutVowels = escapeHarakat(searchText);
        resultFound.clear();//changes-s
        for (int i = 0; i < searchIndexArray.size(); i++) {
            String fileContent = searchIndexArray.get(i);
            if (fileContent.contains(withoutVowels)) {
                if (currentBook.get_bStoreID().contains("M")) {
                    resultFound.add("page " + (i + 1));
                } else {
                    resultFound.add("page " + i);
                }
            }
        }
    }

    private String escapeHarakat(String searchText) {

        if (searchText.contains("َ")) {
            searchText = searchText.replaceAll("َ", "");
        }
        if (searchText.contains("ً")) {
            searchText = searchText.replaceAll("ً", "");
        }
        if (searchText.contains("ُ")) {
            searchText = searchText.replaceAll("ُ", "");
        }
        if (searchText.contains("ٌ")) {
            searchText = searchText.replaceAll("ٌ", "");
        }
        if (searchText.contains("ِ")) {
            searchText = searchText.replaceAll("ِ", "");
        }
        if (searchText.contains("ٍ")) {
            searchText = searchText.replaceAll("ٍ", "");
        }
        if (searchText.contains("ْ")) {
            searchText = searchText.replaceAll("ْ", "");
        }
        if (searchText.contains("ّ")) {
            searchText = searchText.replaceAll("ّ", "");
        }
        return searchText;
    }

    public void processXML() {
        searchIndexArray.clear();
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {
                public void startElement(String uri, String localName, String qName,
                                         Attributes attributes) throws SAXException {
                    if (qName != "Search") {
                        String attrValue = attributes.getValue("Data");
                        searchIndexArray.add(attrValue);
                    }
                }

            };

            File sdPath = getFilesDir();
            //revert_book_hide :
            String filePath = currentBookPath + "Search.xml";
            //filePath = sdPath+"/"+bookName+"Book/Search.xml";
            InputStream inputStream = new FileInputStream(filePath);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");

            saxParser.parse(is, handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeSearchEdittext() {
        et_layout = (LinearLayout) findViewById(R.id.et_layout);
        search_btn = (CircleButton) findViewById(R.id.btnTabs);
        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_layout.isShown()) {
                    et_layout.setVisibility(View.GONE);
                } else {
                    et_layout.setVisibility(View.VISIBLE);
                }
            }
        });
        searchEditText = (EditText) findViewById(R.id.search_box);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        searchEditText.addTextChangedListener(this);
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            //@Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    resultFound.clear();

                    if (searchIndexArray.isEmpty()) {
                        processXML();
                    }
                    serachTableReload(searchEditText.getText().toString());

                    searchPopup.show();

                    InputMethodManager inputmanager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                    inputmanager.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);

                    if (resultFound.isEmpty() == true) {
                        tv_noresult.setVisibility(View.VISIBLE);
                    }
                    searchEditText.clearFocus();
                    etSearch.setVisibility(View.GONE);
                    imgBtnBack.setVisibility(View.GONE);
                    searchPopup.findViewById(R.id.footer).setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });
    }

    /***
     * Method to display study cards :
     */
    public void showStudyCardsDialogg() {
        //Declaring and defining the Dialog
        showStudyCardsDialog = new Dialog(this);
        showStudyCardsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        showStudyCardsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        showStudyCardsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.note_viewpager_layout, null));
        if (Globals.isTablet()) {
            showStudyCardsDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.4));
        } else {
            showStudyCardsDialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        }
        noteViewPager = (ViewPager) showStudyCardsDialog.findViewById(R.id.viewPager);
        ViewPagerNoteAdapter viewPageradapter = new ViewPagerNoteAdapter();
        noteViewPager.setAdapter(viewPageradapter);
        noteViewPager.setOffscreenPageLimit(1);
        noteViewPager.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        showStudyCardsDialog.show();
    }

    private void initializeDialogLayouts(View dialog) {
        count_btn = (Button) dialog.findViewById(R.id.btn_text);
        note_countbtn1 = (Button) dialog.findViewById(R.id.count_btn1);
        note_countbtn1.setOnClickListener(this);
        note_countbtn2 = (Button) dialog.findViewById(R.id.count_btn2);
        note_countbtn2.setOnClickListener(this);
        note_countbtn3 = (Button) dialog.findViewById(R.id.count_btn3);
        note_countbtn3.setOnClickListener(this);
        notwell_btn = (Button) dialog.findViewById(R.id.notwell_btn);
        notwell_btn.setOnClickListener(this);
        well_btn = (Button) dialog.findViewById(R.id.well_btn);
        well_btn.setOnClickListener(this);
        vrywell_btn = (Button) dialog.findViewById(R.id.verywell_btn);
        vrywell_btn.setOnClickListener(this);
        Button all_btn = (Button) dialog.findViewById(R.id.all_btn);
        all_btn.setOnClickListener(this);
        noteColor1 = (ImageView) dialog.findViewById(R.id.imgBtnC1);
        noteColor1.setOnClickListener(this);
        noteColor2 = (ImageView) dialog.findViewById(R.id.imgBtnC2);
        noteColor2.setOnClickListener(this);
        noteColor3 = (ImageView) dialog.findViewById(R.id.imgBtnC3);
        noteColor3.setOnClickListener(this);
        noteColor4 = (ImageView) dialog.findViewById(R.id.imgBtnC4);
        noteColor4.setOnClickListener(this);
        noteColor5 = (ImageView) dialog.findViewById(R.id.imgBtnC5);
        noteColor5.setOnClickListener(this);
        noteColor6 = (ImageView) dialog.findViewById(R.id.imgBtnC6);
        noteColor6.setOnClickListener(this);
    }

    public void studyCardNavigatePage(int navigatePage, int navigateTab) {
        //dimiss study cards
        // load Index code :
        showStudyCardsDialog.dismiss();

        //loadingfromIndexpage = true;
        //loadingfromcurlpage = true;
        //tabNo =navigateTab;
        currentPageNumber = navigatePage;
        currentEnrichmentTabId = navigateTab;
        //   isSetViewPagerItem = true;
        loadViewPager();
        /*if(tabNo>0){
            //enriched = true;
            //Get the epath :
            String[] currentPageContents;
            ////System.out.println("Current page number:"+myValue);
            currentPageContents = enrichPageArray.get(navigatePage).split("\\|");
            if(currentPageContents.length > 1){
                ////System.out.println("CurrentPageContents:"+currentPageContents[1]);
                String[] enrichments = currentPageContents[1].split("##");
                epath = enrichments[tabNo].split("\\$\\$")[0];
            }
            passingPageNumberValues(navigatePage);

        }else{
            passingPageNumberValues(navigatePage);
        }*/
    }

    public void StudyCardsAdapter(ArrayList<NoteData> dataArrayList) {
        //Calling the Simple Card class :
        if (dataArrayList.size() == 0) {
            tv_StudyCardsAlert.setVisibility(View.VISIBLE);
            tv_StudyCardsAlert.setText(getResources().getString(R.string.study_cards_alert));
        } else {
            tv_StudyCardsAlert.setVisibility(View.GONE);
        }
        adapter = new SimpleCardStackAdapter(getApplicationContext(), this, dataArrayList);
        android.content.res.Resources r = this.getResources();
        for (int tag = 0; tag < dataArrayList.size(); tag++) {
            NoteData noteData = dataArrayList.get(tag);
            int pageNo = noteData.getNotePageNo();
            int tabNo = noteData.getNoteTabNo();
            int color = noteData.getNoteColor();
            String finalPageNo;
            if (tabNo > 0) {

                finalPageNo = "" + pageNo + "-" + tabNo;
            } else {
                finalPageNo = "" + pageNo;
            }
            if (pageNo <= 0) {
                adapter.add(new CardModel("Cover page", noteData.getNoteTitle(), r.getDrawable(R.drawable.turn_bg)));

            } else {
                adapter.add(new CardModel("Page " + finalPageNo, noteData.getNoteTitle(), r.getDrawable(R.drawable.turn_bg)));
            }

        }

        mCardContainer.setAdapter(adapter);
    }

    public void copyNoteImage() {
        Bitmap noteIcon = BitmapFactory.decodeResource(getResources(), R.drawable.stick_samll);
        BitmapFactory.Options bmOptions;
        bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 1;

        String bookMarkPath = Environment.getDataDirectory().getPath().concat("/data/" + Globals.getCurrentProjPackageName() + "/files/").concat("BookMarkImage/");
        File wallpaperDirectory = new File(bookMarkPath);
        wallpaperDirectory.mkdirs();
        OutputStream outStream = null;

        File file = new File(wallpaperDirectory, "stick_samll.png");

        try {
            outStream = new FileOutputStream(file);
            noteIcon.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();


        } catch (IOException e) {
            e.printStackTrace();

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_book_view_read, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initializeAds() {
        SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
        adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);

        if (!adsPurchased) {
            interstitial = new InterstitialAd(BookViewReadActivity.this);
            interstitial.setAdUnitId(Globals.INTERSTITIAL_AD_UNIT_ID);
            btnHideAd = (Button) findViewById(R.id.btnHideAd);
            btnHideAd.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BookViewReadActivity.this);
                    builder.setTitle(R.string.ad_remover_title);
                    builder.setMessage(R.string.ad_remover_message);
                    builder.setCancelable(false);
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startAdsPurchase();
                        }
                    });
                    builder.setNeutralButton(R.string.hide, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            btnHideAd.setVisibility(View.GONE);
                            adView.setVisibility(View.GONE);
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
            adView = (AdView) findViewById(R.id.adView);
           /* AdRequest adRequest = new AdRequest.Builder()

                    //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    //.addTestDevice("505F22CBB28695386696655EED14B98B")
                    .build();
            adView.loadAd(adRequest);

            adView.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    Log.e("close", "closed");
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                }

                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                    Log.e("open", "opened");
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (!groups.getBannerAdsVisible()) {
                        adView.setVisibility(View.VISIBLE);

                        btnHideAd.setVisibility(View.VISIBLE);
                    }
                }
            });

            interstitial.loadAd(adRequest);*/
                /* adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (!groups.getBannerAdsVisible()) {
                        adView.setVisibility(View.VISIBLE);

                        btnHideAd.setVisibility(View.VISIBLE);
                    }
                }
            });*/
        }
    }

    /**
     * Initiate the purchase
     */
    private void startAdsPurchase() {
        //ITEM_SKU = "bookfeature";
        //ITEM_SKU = inAppProductId;
        mHelper = new IabHelper(BookViewReadActivity.this, Globals.manahijBase64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    UserFunctions.complain("Problem setting up in-app billing: " + result, BookViewReadActivity.this);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                mHelper.launchPurchaseFlow(BookViewReadActivity.this, Globals.ADS_INAPP_PURCHASE_PRODUCTID, 10001, mPurchaseFinishedListener, "");
            }
        });
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                String message = result.getMessage();
                int response = result.getResponse();
                //System.out.println(result+":"+response);
                if (response == 7) {
                    UserFunctions.DisplayAlertDialog(BookViewReadActivity.this, R.string.item_already_owned, R.string.restoring_purchase);
                    disableAds();
                } else {

                }
                return;
            }

            if (purchase.getSku().equals(Globals.ADS_INAPP_PURCHASE_PRODUCTID)) {
                disableAds();
            }
        }
    };

    /*public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {

        @Override
        public void onQueryInventoryFinished(IabResult result, Inventory inv) {
            if (result.isFailure()) {
                ////System.out.println("Failed");
                UserFunctions.DisplayAlertDialogNotFromStringsXML(BookViewReadActivity.this, result.getMessage(), "");
            } else {
                mHelper.consumeAsync(inv.getPurchase(Globals.ADS_INAPP_PURCHASE_PRODUCTID), mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {

        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess()) {
                ////System.out.println("Success");
                //UserFunctions.DisplayAlertDialog(BookViewReadActivity.this, R.string.purchase_completed_successfully, R.string.purchased);
                disableAds();
            } else {
                ////System.out.println("Failed");
                UserFunctions.DisplayAlertDialog(BookViewReadActivity.this, result.getMessage(), R.string.purchase_failed);
            }
        }
    };*/

    private void disableAds() {
        SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        editor.putBoolean(Globals.ADS_DISPLAY_KEY, true);
        editor.commit();
        adView.setVisibility(View.GONE);
        btnHideAd.setVisibility(View.GONE);
        adsPurchased = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    public void onBackPressed() {
        String query = "update books set LastViewedPage='" + currentPageNumber + "' where storeID='" + currentBook.get_bStoreID() + "'";
        db.executeQuery(query);
        currentBook.set_lastViewedPage(currentPageNumber);
        setResult(RESULT_OK, getIntent().putExtra("Book", currentBook));
        setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
        setResult(RESULT_OK, getIntent().putExtra("mindMap", mindMapEdited));
        if (Globals.isLimitedVersion() && !adsPurchased && interstitial != null) {
            if (interstitial.isLoaded() && !groups.getInterstitialAdsVisible()) {
                interstitial.show();

            }
           /* adView.setVisibility(View.GONE);
            btnHideAd.setVisibility(View.GONE);*/

          /*  new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    adView.setVisibility(View.GONE);
                    btnHideAd.setVisibility(View.GONE);
                }
            }, 1000);*/
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper != null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == 64206) {
            Session session = Session.getActiveSession();
            session.onActivityResult(this, requestCode, resultCode, data);
            if (session != null && session.isOpened()) {
                Request.newMeRequest(session, new Request.GraphUserCallback() {

                    @Override
                    public void onCompleted(GraphUser _user, Response response) {
                        if (_user != null) {
                            publishFeedDialog(_user.getName(), currentWebView.selectedText);
                        }
                    }
                }).executeAsync();
            }
            /*if (session != null &&
                    (session.isOpened() || session.isClosed()) ) {
				//onSessionStateChange(session, session.getState(), null);
			}*/
        } else if (requestCode == bookViewActivityrequestCode) {
            if (data != null) {
                Book book = (Book) data.getSerializableExtra("Book");
                if (book.is_bStoreBook() && book.get_bStoreID().contains("C")) {
                    currentBook = book;
                    pageCount = currentBook.getTotalPages();
                }
            }
            loadViewPager();
        } else if (requestCode == InboxRequestCode && data != null) {
            GridShelf grShelf = (GridShelf) data.getSerializableExtra("Shelf");
            gridShelf = grShelf;
        } else if (requestCode == indexActivityRequestCode && data != null) {
            String[] pgval = data.getExtras().getStringArray("id");
            if (pgval == null) {
                int value = data.getExtras().getInt("id");
                currentEnrichmentTabId = 0;
                currentPageNumber = value;
                isSetViewPagerItem = true;
                setSeekBarValues();
                loadViewPager();
                if(currentPageNumber==pageCount||currentPageNumber==1){
                    try {
                        JSONObject obj = new JSONObject();
                        if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                            obj.put("type", Constants.KEYBOOK);
                            obj.put("action", Constants.KEYPAGECHANGE);
                            obj.put("book_id", currentBook.get_bStoreID());

                            // obj.put("type", Constants.KEYPAGECHANGE);
                            if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
                                if(currentPageNumber==pageCount){
                                    obj.put("pagenumber", pageCount);
                                }else {
                                    obj.put("pagenumber", -1);
                                }

                            }
                            else {
                                if(currentPageNumber==1) {
                                    obj.put("pagenumber", currentPageNumber);
                                }else {
                                    obj.put("pagenumber", (pageCount));
                                }
                            }
                         /*   if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
                                seekBar.setProgress(currentPageNumber - 1);
                            } else {
                                seekBar.setProgress(pageCount - currentPageNumber);
                            }*/
                            obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                            ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
                        }
                    } catch (Exception e) {
                        Log.e("ViewPager", "" + e.toString());
                    }

                }
            } else {
                int pageNumber = Integer.parseInt(pgval[0]);
                int enrTabId = Integer.parseInt(pgval[1]);
                currentEnrichmentTabId = enrTabId;
                currentPageNumber = pageNumber + 1;
                isSetViewPagerItem = true;
                loadViewPager();
            }
        } else if (requestCode == CAPTURE_GALLERY) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                String picturePath = UserFunctions.getPathFromGalleryForPickedItem(this, selectedImage);
                if (picturePath != null) {
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                    malzamah.imagePicked(bitmap);
                }
            }
        } else if (requestCode == MindMapDialog.MindMapActivityRequestCode) {
            if (data != null) {
                boolean isOpenMindMapDialog = data.getExtras().getBoolean("MIOpenDialog", false);
                if (isOpenMindMapDialog) {
                    Globals.mindMapFileNameLastOpened = null;
                    String orphanNode = data.getExtras().getString("MIOrphanNodeText", "");
                    MindMapDialog mindMapDialog = new MindMapDialog(BookViewReadActivity.this, false);
                    mindMapDialog.showMindMapDialog(orphanNode);
                }
                String mindMapTitle = data.getExtras().getString("MITitle");
                if (mindMapTitle != null && mindMapTitle != "") {
                    UserFunctions.saveMindMapScreenShotForPageBg(mindMapTitle, db, currentBook, gridShelf);
                }
            }
        } else if (requestCode == WEBVIEW_REQUEST_CODE && data != null) {
            String verifier = data.getExtras().getString(Globals.IEXTRA_OAUTH_VERIFIER);
            if (!verifier.equals("Exit")) {
                final String selectedText = data.getExtras().getString("text");
                try {
                    AccessToken accessToken = mTwitter.getOAuthAccessToken(mRequestToken, verifier);

                    long userID = accessToken.getUserId();
                    final User user = mTwitter.showUser(userID);
                    String username = user.getName();

                    TwitterDialog twitterDialog = new TwitterDialog(BookViewReadActivity.this);
                    twitterDialog.saveTwitterInfo(accessToken);
                    twitterDialog.twitterReadActivityAlertDialog(selectedText);
                    //twitterDialog.sharingText(selectedText);


                } catch (Exception e) {
                    Log.e("Twitter Login Failed", e.getMessage());
                }
            } else {
                String selectedText = data.getExtras().getString("text");
            }
        } else if (requestCode == AUTH_CODE_REQUEST_CODE) {
            if (exportEnrResViewAdapter != null) {
                exportEnrResViewAdapter.gDriveExport.Authorize();
            }
        }

    }

    public void onDismiss(DialogInterface d) {
        //  if (parentBkmrkLayout.getWidth()>currentparentbklayout.getWidth()) {
        int enrTabsWidth1 = currentBkmarkLayout.getWidth();
        currentBkmarkScroll.smoothScrollTo(enrTabsWidth1, 0);
        // }
    }

    public void loadMindMap(final String title, final RelativeLayout layout, final Context context) {
        ((BookViewReadActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final RelativeLayout canvasView = new RelativeLayout(context);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                params.addRule(RelativeLayout.BELOW, enr_layout.getId());
                canvasView.setLayoutParams(params);
                final TwoDScrollView twoDScroll = new TwoDScrollView(context, canvasView);
                twoDScroll.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                twoDScroll.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                twoDScroll.setEnabled(false);
                canvasView.addView(twoDScroll);
                ViewGroup drawingView = new RelativeLayout(context);
                drawingView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                twoDScroll.addView(drawingView);
                //setSelectedView(viewEdit);
                drawingView.getLayoutParams().width = AppDict.DEFAULT_OFFSET;
                drawingView.getLayoutParams().height = AppDict.DEFAULT_OFFSET;
                String url = Globals.TARGET_BASE_MINDMAP_PATH + title + ".xml";
                LoadMindMapContent mindMapContent = new LoadMindMapContent(twoDScroll, drawingView, context);
                mindMapContent.loadXmlData(url, false);
                Rect rectf = new Rect();
                drawingView.getLocalVisibleRect(rectf);
                twoDScroll.calculateBounds();
//                twoDScroll.post(new Runnable() {
//                    @Override
//                    public void run() {
                twoDScroll.scroll((int) (AppDict.DEFAULT_OFFSET - canvasView.getWidth() + 120) / 2,
                        (int) (AppDict.DEFAULT_OFFSET - canvasView.getHeight()) / 2);
                //  }
//                });
                layout.addView(canvasView, params);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_GET_ACCOUNTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission Granted
                    if (exportEnrResViewAdapter != null) {
                        exportEnrResViewAdapter.gDriveExport.Authorize();
                    }
                } else {
                    //Permission denied
                    if (exportEnrResViewAdapter.progresdialog != null) {
                        exportEnrResViewAdapter.progresdialog.dismiss();
                    }
                }
                return;
            }
        }
    }


    private boolean checkBookSubscribedForClient(String productID) {
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(BookViewReadActivity.this);
        ArrayList<HashMap<String, String>> existList = null;
        try {
            existList = (ArrayList<HashMap<String, String>>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.SUBSLIST, ObjectSerializer.serialize(new ArrayList<HashMap<String, String>>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (UserFunctions.checkClientIdExist(existList, productID)) {
            return true;
        } else {
            return false;
        }
    }

    public View.OnDragListener mOnDragListener = new View.OnDragListener() {
        int sequenceId, changedEnrId;
        EnrichmentButton enrichButton;

        //int enrTabsWidth=0;
        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // Do nothing
                    // mDrapView=v;
                    //  enrTabsWidth = 300;
                    enrichButton = (EnrichmentButton) mDrapView;
                    sequenceId = enrichButton.getEnrichmentSequenceId();
                    System.out.println(sequenceId);

                    System.out.println("Hellow");
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setAlpha(0.5F);
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    mDrapView.setVisibility(View.INVISIBLE);
                    View vieww = (View) event.getLocalState();
                    // for changing selected item in selected places
                    EnrichmentButton draggedView = (EnrichmentButton) mDrapView;

                    for (int i = 0, j = currentEnrTabsLayout.getChildCount(); i < j; i++) {
                        if (i != 0) {
                            EnrichmentButton enrich = (EnrichmentButton) currentEnrTabsLayout.getChildAt(i);
                            //   enrTabsWidth+=enrich.getWidth();
                            if (currentEnrTabsLayout.getChildAt(i) == v) {
                                // 当前位置
                                //currentEnrTabsLayout.removeView(vieww);
                                //currentEnrTabsLayout.addView(vieww, i);
                                //view.setVisibility(View.VISIBLE);
//                              if(mDrapView!=v) {
                                EnrichmentButton alterEnrich = (EnrichmentButton) v;
                                changedEnrId = alterEnrich.getEnrichmentSequenceId();
                                sequenceId = enrichButton.getEnrichmentSequenceId();
                                enrichButton.setEnrichmentSequenceId(changedEnrId);
                                draggedView.setEnrichmentSequenceId(changedEnrId);
                                alterEnrich.setEnrichmentSequenceId(sequenceId);
                                //sv.smoothScrollTo(enrTabsWidth, 0);
                                System.out.println(sequenceId);
                                // if(enrich.getEnrichmentSequenceId()!= draggedView.getEnrichmentSequenceId()){

                                db.executeQuery("update enrichments set SequentialID='" + changedEnrId + "'where enrichmentID='" + enrichButton.getEnrichmentId() + "'");
                                db.executeQuery("update enrichments set SequentialID='" + sequenceId + "'where enrichmentID='" + alterEnrich.getEnrichmentId() + "'");
                                // }
                                currentEnrTabsLayout.removeView(vieww);
                                currentEnrTabsLayout.addView(vieww, i);
                                break;
                            }

                        }
                    }
                    break;
                case DragEvent.ACTION_DROP:
                    View view = (View) event.getLocalState();
                    // mDrapView.setVisibility(View.INVISIBLE);
                    for (int i = 0, j = currentEnrTabsLayout.getChildCount(); i < j; i++) {

                        if (currentEnrTabsLayout.getChildAt(i) == v) {
                            // 当前位置
                            view.setVisibility(View.VISIBLE);
                            EnrichmentButton alterEnrich = (EnrichmentButton) v;
                            changedEnrId = alterEnrich.getEnrichmentSequenceId();    // 3
                            //rearrangingTabs(view,changedEnrId,i);
                            sequenceId = enrichButton.getEnrichmentSequenceId();
                            enrichButton.setEnrichmentSequenceId(changedEnrId);
                            alterEnrich.setEnrichmentSequenceId(sequenceId);
                            System.out.println(sequenceId);
                            db.executeQuery("update enrichments set SequentialID='" + changedEnrId + "'where enrichmentID='" + enrichButton.getEnrichmentId() + "'");
                            db.executeQuery("update enrichments set SequentialID='" + sequenceId + "'where enrichmentID='" + alterEnrich.getEnrichmentId() + "'");

                            currentEnrTabsLayout.removeView(view);
                            currentEnrTabsLayout.addView(view, i);
                            currentEnrTabsLayout.invalidate();
                            // enrTabsWidth=300;
                            break;
                        }
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setAlpha(1F);
                    v.invalidate();
                    mDrapView.setVisibility(View.VISIBLE);

                    break;
                case DragEvent.ACTION_DRAG_LOCATION:
                    System.out.println(sequenceId);
                    int enrTabsWidth1 = 0;
                    // int  enrTabsWidth2= (int) BookViewReadActivity.this.getResources().getDimension(R.dimen.enrichmen t_btn_width);
                   /* if (currentEnrTabsLayout.getWidth() > designPageLayout.getWidth()) {
                        enrTabsWidth = currentEnrTabsLayout.getWidth();
                        sv.smoothScrollTo(enrTabsWidth, 0);*/

                default:
                    break;
            }
            return true;
        }
    };
    public View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mDrapView = v;

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:

                    break;
                case MotionEvent.ACTION_DOWN:
//                   ((BookViewReadActivity) context).mDrapView = view;
//                   ClipData data = ClipData.newPlainText("", "");
//                   EnrichmentButton.MyDragShadowBuilder shadowBuilder = new EnrichmentButton.MyDragShadowBuilder(
//                           view);
//                   view.startDrag(data, shadowBuilder, view, 0);

//                    mDrapView = v;
//                    ClipData data = ClipData.newPlainText("", "");
//                    MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(
//                            v,BookViewReadActivity.this);
//                    v.startDrag(data, shadowBuilder, v, 0);
                    break;
            }

            return false;
        }
    };

    //2
    private void rearrangingTabs(View view, int position, int count) {
        int sequenceId, currentId;
        EnrichmentButton enrich = (EnrichmentButton) view;
        sequenceId = enrich.getEnrichmentSequenceId();   //3
        //4
        currentId = enrich.getEnrichmentSequenceId();
        if (sequenceId > position) {
            for (int i = position, j = currentEnrTabsLayout.getChildCount(); i < j; i++) {
                RelativeLayout rlview = null;

                if (i != 0) {
                    // rlview = (RelativeLayout) currentEnrTabsLayout.getChildAt(i);
                    EnrichmentButton alterEnrich = (EnrichmentButton) currentEnrTabsLayout.getChildAt(i);
                    // EnrichmentButton alterEnrich = (EnrichmentButton)currentEnrTabsLayout.getChildAt(i);

                    if (currentId > alterEnrich.getEnrichmentSequenceId()) {

                        EnrichmentButton Enrich = (EnrichmentButton) currentEnrTabsLayout.getChildAt(i + 1);
                        int id = alterEnrich.getEnrichmentSequenceId() + 1;
                        // sequenceId = sequenceId - 1;
                        alterEnrich.setEnrichmentSequenceId(id);
                        db.executeQuery("update enrichments set SequentialID='" + id + "'where enrichmentID='" + alterEnrich.getEnrichmentId() + "'");
                        // sequenceId = sequenceId - 1;
                    }
                }

            }
        } else {
            for (int i = sequenceId, j = count + 1; i < j; i++) {
                RelativeLayout rlview = null;

                if (i != 0) {
                    // rlview = (RelativeLayout) currentEnrTabsLayout.getChildAt(i);
                    EnrichmentButton alterEnrich = (EnrichmentButton) currentEnrTabsLayout.getChildAt(i);
                    // EnrichmentButton alterEnrich = (EnrichmentButton)currentEnrTabsLayout.getChildAt(i);

                    if (currentId < alterEnrich.getEnrichmentSequenceId()) {
                        int id = alterEnrich.getEnrichmentSequenceId() - 1;
                        alterEnrich.setEnrichmentSequenceId(id);
                        // sequenceId = sequenceId - 1;
                        db.executeQuery("update enrichments set SequentialID='" + id + "'where enrichmentID='" + alterEnrich.getEnrichmentId() + "'");
                        // sequenceId = sequenceId - 1;
                    }
                }

            }
        }
        db.executeQuery("update enrichments set SequentialID='" + position + "'where enrichmentID='" + enrich.getEnrichmentId() + "'");


    }


    private void loadAllNotesInCurrentPage() {
        ArrayList<NoteData> allnotesInCurrentPage = new ArrayList<>();
        for (int i = 0; i < tempNoteDataArrayList.size(); i++) {
            NoteData data = tempNoteDataArrayList.get(i);
            if (allPageClicked) {
                allnotesInCurrentPage.add(data);
            } else {
                if (data.getNotePageNo() == currentPageNumber) {
                    allnotesInCurrentPage.add(data);
                }
            }
        }
        noteDataArrayList.clear();
        noteDataArrayList = allnotesInCurrentPage;
        mCardContainer.cardPosition = 0;
        StudyCardsAdapter(allnotesInCurrentPage);
        updateButtons();
        if (noteDataArrayList.size() > 0) {
            setBackgroudColorForButtons(0);
        }
    }

    private void loadNotesBasedOnColors(int colorValue) {
        ArrayList<NoteData> allNotesBasedOnColors = new ArrayList<>();
        for (int i = 0; i < tempNoteDataArrayList.size(); i++) {
            NoteData data = tempNoteDataArrayList.get(i);
            if (data.getNoteColor() == colorValue) {
                allNotesBasedOnColors.add(data);
            }
        }
        noteDataArrayList.clear();
        noteDataArrayList = allNotesBasedOnColors;
        mCardContainer.cardPosition = 0;
        StudyCardsAdapter(allNotesBasedOnColors);
        updateButtons();
        if (noteDataArrayList.size() > 0) {
            setBackgroudColorForButtons(0);
        }
    }

    private void loadNotesBasedOnType(int type) {
        ArrayList<NoteData> allNotesBasedOnTpye = new ArrayList<>();
        for (int i = 0; i < noteDataArrayList.size(); i++) {
            NoteData data = noteDataArrayList.get(i);
            if (data.getNoteType() == type) {
                allNotesBasedOnTpye.add(data);
            }
        }
        noteDataArrayList.clear();
        noteDataArrayList = allNotesBasedOnTpye;
        mCardContainer.cardPosition = 0;
        StudyCardsAdapter(allNotesBasedOnTpye);
        updateButtons();
        if (noteDataArrayList.size() > 0) {
            setBackgroudColorForButtons(0);
        }
    }

    private void updateButtons() {
        int notwellCount = 0, wellCount = 0, verywellCount = 0;
        for (int i = 0; i < noteDataArrayList.size(); i++) {
            NoteData data = noteDataArrayList.get(i);
            if (data.getNoteType() == 1) {
                notwellCount++;
            } else if (data.getNoteType() == 2) {
                wellCount++;
            } else if (data.getNoteType() == 3) {
                verywellCount++;
            }
        }
        note_countbtn1.setText(String.valueOf(notwellCount));
        note_countbtn2.setText(String.valueOf(wellCount));
        note_countbtn3.setText(String.valueOf(verywellCount));
        count_btn.setText("" + noteDataArrayList.size());
    }

    public void setBackgroudColorForButtons(int cardPosition) {

        int type = 0;
        if (cardPosition < noteDataArrayList.size()) {
            NoteData data = noteDataArrayList.get(cardPosition);
            type = data.getNoteType();
            if (type == 1) {
                notwell_btn.setBackgroundResource(R.drawable.white_border_hovercolor);
                well_btn.setBackgroundResource(R.drawable.black_background);
                vrywell_btn.setBackgroundResource(R.drawable.black_background);
            } else if (type == 2) {
                notwell_btn.setBackgroundResource(R.drawable.black_background);
                well_btn.setBackgroundResource(R.drawable.white_border_hovercolor);
                vrywell_btn.setBackgroundResource(R.drawable.black_background);
            } else if (type == 3) {
                notwell_btn.setBackgroundResource(R.drawable.black_background);
                well_btn.setBackgroundResource(R.drawable.black_background);
                vrywell_btn.setBackgroundResource(R.drawable.white_border_hovercolor);
            }
        }
    }

    private void updateNoteType(int position, int type) {
        NoteData data = noteDataArrayList.get(position);
        data.setNoteType(type);
    }

    public class ViewPagerNoteAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = null;
            switch (position) {
                case 0:
                    view = inflater.inflate(R.layout.card_layout, null);
                    ArrayList<NoteData> noteDatas = db.loadNoteDivToArray(currentBook.get_bStoreID());
                    noteDataArrayList.clear();
                    noteDataArrayList = noteDatas;
                    mCardContainer = (CardContainer) view.findViewById(R.id.layoutview);
                    mCardContainer.cardPosition = 0;
                    tv_StudyCardsAlert = (TextView) view.findViewById(R.id.txt);
                    final SegmentedRadioButton segmentedRadioGroup = (SegmentedRadioButton) view.findViewById(R.id.segment_text);
                    segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (group == segmentedRadioGroup) {
                                if (checkedId == R.id.currentPage) {
                                    allPageClicked = false;
                                    loadStudyCards(allPageClicked);
                                } else if (checkedId == R.id.all_page) {
                                    allPageClicked = true;
                                    loadStudyCards(allPageClicked);
                                }
                            }
                        }
                    });
                    initializeDialogLayouts(view);
                    StudyCardsAdapter(noteDatas);
                    updateButtons();
                    if (noteDataArrayList.size() > 0) {
                        setBackgroudColorForButtons(0);
                    }
                    count_btn.setText("" + noteDataArrayList.size());
                    Button add_btn = (Button) view.findViewById(R.id.btnNext);
                    add_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            noteViewPager.setCurrentItem(1, true);
                        }
                    });
                    CircleButton back_btn = (CircleButton) view.findViewById(R.id.btn_back);
                    back_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showStudyCardsDialog.dismiss();
                        }
                    });

                    break;
                case 1:
                    view = inflater.inflate(R.layout.add_flashcard_layout, null);
                    CircleButton back_btn1 = (CircleButton) view.findViewById(R.id.btn_back);
                    back_btn1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            noteViewPager.setCurrentItem(0, true);
                            loadStudyCards(allPageClicked);
                        }
                    });
                    final RelativeLayout card_layout = (RelativeLayout) view.findViewById(R.id.card_container);
                    final EditText title_txt = (EditText) view.findViewById(R.id.title_edittext);
                    final EditText desc_txt = (EditText) view.findViewById(R.id.description_edittext);
                    Button turn_btn = (Button) view.findViewById(R.id.btn_turn);
                    turn_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FlipAnimation flipAnimation = new FlipAnimation(title_txt, desc_txt);

                            if (title_txt.getVisibility() == View.GONE) {
                                flipAnimation.reverse();
                            }
                            card_layout.startAnimation(flipAnimation);
                        }
                    });
                    Button btn_add = (Button) view.findViewById(R.id.btnNext);
                    btn_add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (title_txt.getText().length() > 0) {
                                int colorValue = CC + 1;
                                NoteData data = new NoteData();
                                data.setNoteColor(colorValue);
                                data.setNoteType(1);
                                data.setNoteTabNo(0);
                                data.setNotePageNo(0);
                                data.setNoteTitle(title_txt.getText().toString());
                                data.setNoteDesc(desc_txt.getText().toString());
                                noteDataArrayList.add(data);
                                String query = "insert into tblNote(BName,PageNo,SText,Occurence,SDesc,NPos,Color,ProcessSText,TabNo,Exported,NoteType,CallStatus) values" +
                                        "('" + currentBook.get_bStoreID() + "','0','" + title_txt.getText().toString() + "','0','" + desc_txt.getText().toString() + "','0','" + colorValue + "','0','0','" + "0" + "','1','0')";
                                db.executeQuery(query);
                                // updateNotesCount();
                                title_txt.setText("");
                                desc_txt.setText("");
                            }
                        }
                    });
                    ImageView iv_nColor1 = (ImageView) view.findViewById(R.id.imgBtnC1);
                    iv_nColor1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CC = 0;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg1);
                        }
                    });
                    ImageView iv_nColor2 = (ImageView) view.findViewById(R.id.imgBtnC2);
                    iv_nColor2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CC = 1;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg2);
                        }
                    });
                    ImageView iv_nColor3 = (ImageView) view.findViewById(R.id.imgBtnC3);
                    iv_nColor3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CC = 2;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg3);
                        }
                    });
                    ImageView iv_nColor4 = (ImageView) view.findViewById(R.id.imgBtnC4);
                    iv_nColor4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CC = 3;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg4);
                        }
                    });
                    ImageView iv_nColor5 = (ImageView) view.findViewById(R.id.imgBtnC5);
                    iv_nColor5.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CC = 4;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg5);
                        }
                    });
                    ImageView iv_nColor6 = (ImageView) view.findViewById(R.id.imgBtnC6);
                    iv_nColor6.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CC = 5;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg6);
                        }
                    });
                    break;
            }
            ((ViewPager) container).addView(view, 0);
            return view;
        }

//        @Override
//        public void setPrimaryItem(ViewGroup container, int position, Object object) {
//            if (position==0){
//                loadStudyCards(allPageClicked);
//            }
//        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    private void loadStudyCards(boolean allPage) {
        if (allPage) {
            noteDataArrayList.clear();
            tempNoteDataArrayList.clear();
            noteDataArrayList = db.loadNoteDivToArray(currentBook.get_bStoreID());
            for (int i = 0; i < noteDataArrayList.size(); i++) {
                tempNoteDataArrayList.add(noteDataArrayList.get(i));
            }
            StudyCardsAdapter(noteDataArrayList);
            updateButtons();
            if (noteDataArrayList.size() > 0) {
                setBackgroudColorForButtons(0);
            }
        } else {
            noteDataArrayList.clear();
            tempNoteDataArrayList.clear();
            noteDataArrayList = db.loadNotesInCurrentPage(currentBook.get_bStoreID(), currentPageNumber);
            for (int i = 0; i < noteDataArrayList.size(); i++) {
                tempNoteDataArrayList.add(noteDataArrayList.get(i));
            }
            StudyCardsAdapter(noteDataArrayList);
            updateButtons();
            if (noteDataArrayList.size() > 0) {
                setBackgroudColorForButtons(0);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void createWebPrintJob(WebView webView) {
        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();
        String jobName = getString(R.string.app_name) + " " + currentBook.getBookTitle() + currentPageNumber;
        printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());
    }
  /*  private void updateNotesCount(){
        if (noteDataArrayList.size()>0){
            btn_text_cardcount.setBackgroundResource(R.drawable.circular_red_button);
        }
        btn_text_cardcount.setText(String.valueOf(noteDataArrayList.size()));
    }*/


    public class loadSubEnrichmentTabs extends AsyncTask<Void, Void, Void> {

        ArrayList<Enrichments> enrichmentTabList;
        int pageNumber;
        RecyclerView recyclerview;
        int Id;
        int categoryId;
        RecyclerView clickableRecycler;

        public loadSubEnrichmentTabs(int pageNumber, RecyclerView recyclerView, int SelectecEnrId, int catId, RecyclerView newRecycler) {
            this.pageNumber = pageNumber;
            this.recyclerview = recyclerView;
            this.Id = SelectecEnrId;
            this.categoryId = catId;
            this.clickableRecycler = newRecycler;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            enrichmentTabList = db.getEnrichmentTabListForCatId(currentBook.getBookID(), pageNumber, BookViewReadActivity.this, categoryId);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (enrichmentTabList.size() > 0) {
                subTabsLayout.setVisibility(View.VISIBLE);
                if (recyclerview.getId() == R.id.gridView3) {
                    subTabLayout1.setVisibility(View.VISIBLE);
                } else {
                    subTabLayout1.setVisibility(View.GONE);
                }
                loadView(enrichmentTabList, recyclerview);


            } else {
                if (clickableRecycler.getId() == R.id.gridView1) {
                    subTabsLayout.setVisibility(View.GONE);
                }
                subTabLayout1.setVisibility(View.GONE);
            }
        }
    }


    public void loadView(ArrayList<Enrichments> topEnrList, RecyclerView currentRecycler) {
        final SubtabsAdapter adapter = new SubtabsAdapter(subTabsLayout, subTabLayout1, subRecyclerView, subRecylerView1, db, BookViewReadActivity.this, currentPageNumber, topEnrList, currentRecycler, new OnclickCallBackInterface<Object>() {
            @Override
            public void onClick(Object object) {
                CallBackObjects objects = (CallBackObjects) object;
                if (objects.doubleTap) {
                    gestureView = objects.view;
                    isLoadEnrInpopview = objects.loadEnrPoview;
                    //  treeViewStructureList(gestureView, false,currentBtnEnrichment.getEnrichmentId());
                    //  viewTreeList(gestureView, false,currentBtnEnrichment.getEnrichmentId());
                    viewPopUp(gestureView, currentBtnEnrichment.getEnrichmentId());
                } else {
                    RecyclerView recyclerView = objects.recyclerView;
                    clickableRecyclerView = recyclerView;
                    if (recyclerView != null) {
                        int position = objects.adapterPosition;
                        Enrichments enrichment = objects.horizontalList.get(position);
                        if (clickableRecyclerView.getId() == R.id.gridView2) {
                            SubtabsAdapter.level2 = enrichment.getEnrichmentId();
                        } else if (clickableRecyclerView.getId() == R.id.gridView3) {
                            SubtabsAdapter.level3 = enrichment.getEnrichmentId();
                        } else {
                            SubtabsAdapter.level1 = enrichment.getEnrichmentId();
                        }
                        for (Enrichments enrich : objects.horizontalList) {
                            if (enrich.isEnrichmentSelected()) {
                                enrich.setEnrichmentSelected(false);
                                break;
                            }
                        }
                        if (enrichment.getEnrichmentType().equals(Globals.advancedSearchType)) {

                            clickedAdvanceSearchtab(enrichment);
                        } else if (enrichment.getEnrichmentType().equals(Globals.onlineEnrichmentsType) || (enrichment.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB))) {
                            clickedAdvanceSearchtab(enrichment);
                        } else {
                            clickedEnrichments(enrichment);
                        }
                        enrichment.setEnrichmentSelected(true);
                        currentEnrichmentTabId = enrichment.getEnrichmentId();
                        currentBtnEnrichment = enrichment;
                        if (recyclerView.getId() == R.id.gridView1) {
                            if (enrichment.getEnrichmentId() != 0) {
                                new loadSubEnrichmentTabs(currentPageNumber, subRecyclerView, 0, enrichment.getEnrichmentId(), recyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                            } else {
                                subTabsLayout.setVisibility(View.GONE);
                                subTabLayout1.setVisibility(View.GONE);
                            }
                        } else if (recyclerView.getId() == R.id.gridView2) {
                            new loadSubEnrichmentTabs(currentPageNumber, subRecylerView1, 0, enrichment.getEnrichmentId(), recyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                        }
                    }
                }
            }
        });
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(BookViewReadActivity.this, LinearLayoutManager.HORIZONTAL, false);
        currentRecycler.setNestedScrollingEnabled(false);
        currentRecycler.setLayoutManager(horizontalLayoutManagaer);
        currentRecycler.setAdapter(adapter);
    }

    public void viewTreeList(View view, boolean isAddEnrich, final int categoryId) {
        categoryID = categoryId;
        treeStructureView = new TreeStructureView(currentBook, db, currentPageNumber, popoverView, BookViewReadActivity.this, rootviewRL, new TreeStructureCallBack() {
            @Override
            public void onClick(TreeNode node, Object object, PopoverView popoverView) {
                MyTreeHolder.IconTreeItem treeItem = (MyTreeHolder.IconTreeItem) object;
                Enrichments enrichments1 = treeItem.enrich;
                if (enrichments1.getEnrichmentId() != 0) {
                    search_layout.setVisibility(View.GONE);
                    if (rl.getChildCount() == 7) {
                        rl.removeViewAt(6);
                    }
                    currentBtnEnrichment = enrichments1;
                    if (enrichments1.getEnrichmentType().equals(Globals.advancedSearchType)) {
                        clickedAdvanceSearchtab(enrichments1);
                    } else if (enrichments1.getEnrichmentType().equals(Globals.onlineEnrichmentsType) || (enrichments1.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB))) {
                        clickedAdvanceSearchtab(enrichments1);
                    } else {
                        clickedEnrichments(enrichments1);
                    }
                }
                node.setSelected(true);
                if (enrichments1.getEnrichmentTitle().equals("")) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    //    treeViewStructureList(gestureView, true, enrichments1.getCategoryID());
                    viewTreeList(gestureView, true, enrichments1.getCategoryID());
                } else if (enrichments1.getEnrichmentId() == 0 && enrichments1.getEnrichmentTitle().equals(getResources().getString(R.string.internet_search))) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    search_layout.setVisibility(View.VISIBLE);
                } else if (enrichments1.getEnrichmentId() == 0 && (enrichments1.getEnrichmentTitle().equals(getResources().getString(R.string.blank_page)) || enrichments1.getEnrichmentTitle().equals(getResources().getString(R.string.duplicate_page)))) {
                    if (UserFunctions.checkLoginAndSubscription(BookViewReadActivity.this, false, existList, currentBook.getClientID(), groups.isBookReaderEnrichment(), currentBook.getPurchaseType())) {
                        Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
                        bookViewIntent.putExtra("Book", currentBook);
                        bookViewIntent.putExtra("currentPageNo", currentPageNumber);
                        if (currentBtnEnrichment != null && currentBtnEnrichment.getEnrichmentType().equals(Globals.downloadedEnrichmentType)) {
                            currentEnrichmentTabId = 0;
                        }
                        if (enrichments1.getEnrichmentTitle().equals(getResources().getString(R.string.blank_page))) {
                            bookViewIntent.putExtra("createBlankEnr", "blank");
                        } else {
                            bookViewIntent.putExtra("createBlankEnr", "duplicate");
                        }
                        bookViewIntent.putExtra("categoryId", categoryId);
                        bookViewIntent.putExtra("enrichmentTabId", 0);
                        bookViewIntent.putExtra("Shelf", gridShelf);
                        startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
                    }
                } else if (enrichments1.getEnrichmentId() == 0 && enrichments1.getEnrichmentTitle().equals(getResources().getString(R.string.rename))) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    renamePopup();
                } else if (enrichments1.getEnrichmentId() == 0 && enrichments1.getEnrichmentTitle().equals(getResources().getString(R.string.delete))) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    deletePopup();
                } else if (enrichments1.getEnrichmentId() == 0 && enrichments1.getEnrichmentTitle().equals(getResources().getString(R.string.edit))) {
                    Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
                    bookViewIntent.putExtra("Book", currentBook);
                    bookViewIntent.putExtra("currentPageNo", currentPageNumber);
                    bookViewIntent.putExtra("enrichmentTabId", currentBtnEnrichment.getEnrichmentId());
                    bookViewIntent.putExtra("Shelf", gridShelf);
                    bookViewIntent.putExtra("createBlankEnr", "edit");
                    bookViewIntent.putExtra("categoryId", categoryId);
                    app.setState(true);
                    ManahijApp.getInstance().getPrefManager().addIsinBackground(true);
                    startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
                }
            }
        });
        treeStructureView.treeViewStructureList(isLoadEnrInpopview, currentBtnEnrichment, view, isAddEnrich, categoryId);
    }

    private void renamePopup() {
        final Dialog groupDialog = new Dialog(BookViewReadActivity.this);
        groupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        groupDialog.setTitle(R.string.title);
        groupDialog.setContentView(getLayoutInflater().inflate(R.layout.group_edit, null));
        groupDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.5), RelativeLayout.LayoutParams.WRAP_CONTENT);
        final EditText editText = (EditText) groupDialog.findViewById(R.id.enr_editText);
        Button btnSave = (Button) groupDialog.findViewById(R.id.enr_save);
        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (editText.getText().toString().length() > 0) {
                    db.executeQuery("update enrichments set Title='" + editText.getText().toString() + "' where enrichmentID='" + currentBtnEnrichment.getEnrichmentId() + "' and BID='" + currentBook.getBookID() + "'");
                    if (clickableRecyclerView.getId() == R.id.gridView1) {
                        new loadEnrichmentTabs(currentPageNumber, CurrentRecyclerView, currentEnrichmentTabId, currentParentEnrRLayout, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        new loadSubEnrichmentTabs(currentPageNumber, subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    } else if (clickableRecyclerView.getId() == R.id.gridView2) {
                        new loadSubEnrichmentTabs(currentPageNumber, subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                        new loadSubEnrichmentTabs(currentPageNumber, subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    } else {
                        new loadSubEnrichmentTabs(currentPageNumber, subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                    }
                    groupDialog.dismiss();
                }
            }
        });
        Button btnCancel = (Button) groupDialog.findViewById(R.id.enr_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                groupDialog.dismiss();
            }
        });
        groupDialog.show();
    }

    public void deletePopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BookViewReadActivity.this);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                db.executeQuery("delete from enrichments where enrichmentID = '" + currentBtnEnrichment.getEnrichmentId() + "'");
                if (clickableRecyclerView.getId() == R.id.gridView1 && currentBtnEnrichment.getCategoryID() == 0) {
                    subTabsLayout.setVisibility(View.GONE);
                    subTabLayout1.setVisibility(View.GONE);
                    SubtabsAdapter.level1 = 0;
                    new loadEnrichmentTabs(currentPageNumber, CurrentRecyclerView, currentEnrichmentTabId, currentParentEnrRLayout, true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    RecyclerView curRecycler;
                    int catID;
                    if (clickableRecyclerView.getId() == R.id.gridView2) {
                        curRecycler = CurrentRecyclerView;
                        catID = SubtabsAdapter.level1;
                    } else {
                        curRecycler = clickableRecyclerView;
                        catID = SubtabsAdapter.level2;
                    }
                    new loadSubEnrichmentTabs(currentPageNumber, clickableRecyclerView, 0, catID, curRecycler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                dialog.cancel();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setTitle(R.string.delete);
        builder.setMessage(R.string.suredelete);
        builder.show();
    }

    private void viewPopUp(View view, final int categoryId) {
        categoryID = categoryId;
        treeStructureView = new TreeStructureView(currentBook, db, currentPageNumber, popoverView, BookViewReadActivity.this, rootviewRL, new TreeStructureCallBack() {
            @Override
            public void onClick(TreeNode node, Object object, PopoverView popoverView) {
                Enrichments enrichments3 = (Enrichments) object;
                if (enrichments3.getEnrichmentId() == 0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.rename))) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    renamePopup();
                } else if (enrichments3.getEnrichmentId() == 0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.delete))) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    deletePopup();
                } else if (enrichments3.getEnrichmentId() == 0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.edit))) {
                    Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
                    bookViewIntent.putExtra("Book", currentBook);
                    bookViewIntent.putExtra("currentPageNo", currentPageNumber);
                    bookViewIntent.putExtra("enrichmentTabId", currentBtnEnrichment.getEnrichmentId());
                    bookViewIntent.putExtra("Shelf", gridShelf);
                    bookViewIntent.putExtra("createBlankEnr", "edit");
                    bookViewIntent.putExtra("categoryId", categoryId);
                    app.setState(true);
                    ManahijApp.getInstance().getPrefManager().addIsinBackground(true);
                    startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
                }
                if (enrichments3.getEnrichmentTitle().equals("")) {
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    //  treeViewStructureList(gestureView, true, enrichments1.getCategoryID());
                    viewTreeList(gestureView, true, enrichments3.getCategoryID());
                }
            }
        });
        treeStructureView.treeViewStructureListNew(view, currentBtnEnrichment);
    }


    @Override
    protected void onResume() {
        super.onResume();
        ManahijApp.getInstance().getPrefManager().setBookOpend(true);
        app.setState(false);
        ManahijApp.getInstance().getPrefManager().addIsinBackground(false);

     /*   if (!app.getState()  *//*ManahijApp.getInstance().getPrefManager().getIsinBackground()*//*) {
            if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                startService(new Intent(BookViewReadActivity.this, CallConnectionStatusService.class));
            }
        }*/
        LocalBroadcastManager.getInstance(this).registerReceiver(msgreceiver, new IntentFilter(FILTER));

    }

    float receivedDeviceWidth, receivedDeviceeHeight;
    BroadcastReceiver msgreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            currentBook = (Book) getIntent().getSerializableExtra("Book");
            try {
                JSONObject ojb = new JSONObject(intent.getStringExtra(Constants.KEYMESSAGE));
                switch (ojb.getString("action")) {

                    //case "pagechange":
                    case Constants.KEYPAGECHANGE:
                        recCurrentNumber = Integer.parseInt(ojb.getString("pagenumber"));
                        isPagesele = true;
                        if (currentBook.getBookLangugae().equalsIgnoreCase("English")){
                            currentPageNumber = recCurrentNumber+2;
                        }else {
                            currentPageNumber = recCurrentNumber;
                        }
                       // setSeekBarTextValues(currentPageNumber);
                        setSeekBarValues();
                        loadViewPager();
                        //
                       //
                        break;
                    //case "textselect":
                    case Constants.NOTE:

                        isRecieve = true;
                        noteSelText = ojb.getString("selected_text");
                        SN = ojb.getString("note_query");
                        mNotetext = ojb.getString("note_text");
                        CC = Integer.parseInt(ojb.getString("color"));
                        isUpdate = false;
                        mWebviewVisibility.post(mDoneNote);
                        break;

                    case Constants.HIGHLIGHT:
                        addHighlights(ojb.getString("selected_text"), viewPager.webView);
                        break;
                       /*
                        String textSelect = intent.getStringExtra(Constants.KEYANNOTATION);
                        JSONObject object= new JSONObject(textSelect);
                        switch (object.getString("annotaion")){
                            case "highlight":
                                addHighlights(object.getString("selText"), viewPager.webView);
                                break;
                            case "note":
                                isRecieve =true;
                                noteSelText=object.getString("noteSelText");
                                SN=object.getString("SN");
                                mNotetext=object.getString("noteText");
                                CC= Integer.parseInt(object.getString("color"));
                                isUpdate=false;
                                mWebviewVisibility.post(mDoneNote);
                                break;
                        }


                        break;*/
                    case Constants.KEYOPENFLASHCARD:
                        // JSONObject oj =  new JSONObject(intent.getStringExtra(Constants.KEYMESSAGE));
                        Intent in = new Intent(BookViewReadActivity.this, FlashCardActivity.class);
                        in.putExtra("Book", currentBook);
                        in.putExtra("pagenumber", currentPageNumber);
                        in.putExtra("fromBook", true);
                        app.setState(true);
                        ManahijApp.getInstance().getPrefManager().addIsinBackground(true);
                        startActivity(in);
                        break;
                    case Constants.KEYBOOKCLOSE:
                        onBackPressed();
                        break;
                    case Constants.ENABLE_DRAWING:

                        drawingUI(false);
                        break;

                    case Constants.ACTION_DOWN:
                        getScreenResolution();
                        mPath = new Path();
                       /* String actionDownDrawPoints = intent.getStringExtra(Constants.ACTION_DOWN);
                        JSONObject actionDownObj = new JSONObject(actionDownDrawPoints);
                        int x1 = actionDownObj.getInt(Constants.x);
                        int y1 = actionDownObj.getInt(Constants.y);
                        receivedDeviceWidth = actionDownObj.getInt(Constants.DEVICE_WIDTH);
                        receivedDeviceeHeight = actionDownObj.getInt(Constants.DEVICE_HEIGHT);*/


                        int x1 = ojb.getInt(Constants.x);
                        int y1 = ojb.getInt(Constants.y);
                        receivedDeviceWidth = ojb.getInt(Constants.DEVICE_WIDTH);
                        receivedDeviceeHeight = ojb.getInt(Constants.DEVICE_HEIGHT);
                        float t5 = ((x1 / receivedDeviceWidth) * 100);
                        float t6 = ((y1 / receivedDeviceeHeight) * 100);
                        float t7 = (t5 * currentDeviceWidth) / 100;
                        float t8 = (t6 * currentDeviceHeight) / 100;
                        mPath.moveTo((int) t7, (int) t8);
                        if (canvasView != null)
                            canvasView.getThread().drawBegin();

                        break;
                    case Constants.ACTION_MOVE:
                        String drawPoints = intent.getStringExtra(Constants.ACTION_MOVE);
                        /*JSONObject actionMoveObj = new JSONObject(drawPoints);
                        int x = actionMoveObj.getInt(Constants.x);
                        int y = actionMoveObj.getInt(Constants.y);
*/


                        int x = ojb.getInt(Constants.x);
                        int y = ojb.getInt(Constants.y);
                        float t3 = ((x / receivedDeviceWidth) * 100);
                        float t4 = ((y / receivedDeviceeHeight) * 100);
                        float t1 = (t3 * currentDeviceWidth) / 100;
                        float t2 = (t4 * currentDeviceHeight) / 100;

                        mPath.lineTo((int) t1, (int) t2);
                        if (canvasView != null)
                            canvasView.getThread().draw1(mPath);
                        break;
                    case Constants.CLEAR_DRAW:
                        clearDraw(false);
                        break;

                    case Constants.OPACITY:
                        /*String _opacity = intent.getStringExtra(Constants.OPACITY);
                        JSONObject opacityJsonObj = new JSONObject(_opacity);*/
                        publishOpacity(false, ojb.getInt(Constants.OPACITY));
                        break;
                    case Constants.THICKNESS:
                      /*  String _thickNess = intent.getStringExtra(Constants.THICKNESS);
                        JSONObject thickNessJsonObj = new JSONObject(_thickNess);*/
                        publishThickness(false, ojb.getInt(Constants.THICKNESS));
                        break;
                    case Constants.DRAWING_LINE_TYPE:
                        lineTypeInDrawing.setSelection(ojb.getInt(Constants.DRAWING_LINE_TYPE));
                        break;
                    case Constants.DRAWING_BTN_DONE:
                        btnDrawingDone(false);
                        break;
                    case Constants.BTN_ERASER:
                        eraseDraw(false);
                        break;
                    case Constants.DRAW_PEN:
                        selectPen(false);
                        break;
                    case Constants.COLOR_CODE:
                       /* String _colorCode = intent.getStringExtra(Constants.COLOR_CODE);
                        JSONObject colorCodeJsonObj = new JSONObject(_colorCode);*/
                        String colorCodeReceived = ojb.getString(Constants.COLOR_CODE);
                        validateEnteredColor(colorCodeReceived, false);
                        break;

                    case Constants.SET_SELECTED_COLOR_TO_DRAW:
                       /* String selectedColor = intent.getStringExtra(Constants.SET_SELECTED_COLOR_TO_DRAW);
                        JSONObject selectedColorJsonObj = new JSONObject(selectedColor);*/
                        setSelectedColorToDraw(ojb.getString(Constants.SET_SELECTED_COLOR_TO_DRAW), false);
                        break;
                    /*case Constants.BACK_BUTTON_CLICKED:
                        onBackPressed(false);
                        break;*/
                }


            } catch (Exception e) {
                Log.e("EXc at book reciever", e.toString());
            }


        }
    };

    @Override
    protected void onPause() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        ManahijApp.getInstance().getPrefManager().setBookOpend(false);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(msgreceiver);

       /* if(!app.getState()  *//*ManahijApp.getInstance().getPrefManager().getIsinBackground()*//*){
            stopService(new Intent(BookViewReadActivity.this, CallConnectionStatusService.class));

            ss.disConnectCall();
        }*/
        app.setState(false);
        super.onPause();
    }

    /**********************************
     * DRAWING FUNCRTIONS
     ************************************/
    private void drawingUI(boolean value) {
        if (value) {
            try {

                //  MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();
                JSONObject obj = new JSONObject();
                obj.put("type", Constants.DRAWING);
                obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                obj.put("action", Constants.ENABLE_DRAWING);
                ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
            } catch (Exception e) {
                Log.e("Exception", e.toString());
            }
        }

        if (UserFunctions.checkLoginAndSubscription(this, false, existList, currentBook.getClientID(), groups.getBookReaderDrawingEnable(), currentBook.getPurchaseType())) {
            lineTypeInDrawing.setAdapter(new CustomSpinnerAdapter(BookViewReadActivity.this, R.layout.spinner_drawing_lines, lineTypeArrayElements(), lineTypeImages()));
            objectsToolbar.setVisibility(View.INVISIBLE);
            drawingToolbar.setVisibility(View.VISIBLE);
            seekBar.setVisibility(View.GONE);
            tvSeekBar.setVisibility(View.GONE);
            searchLayout.setVisibility(View.GONE);
            btnLibrary.setVisibility(View.GONE);
            btnPen.setSelected(true);
            if (currentEnrichmentTabId > 0) {
                objContent = drawnFilesDir + "Page" + " " + currentPageNumber + "-" + currentEnrichmentTabId + ".png";
            } else {
                objContent = drawnFilesDir + "Page" + " " + currentPageNumber + ".png";
            }
            currentCanvasImgView.setVisibility(View.GONE);
            canvasView = new PainterCanvas(BookViewReadActivity.this, objContent, null);
            canvasView.setLineType(BrushPreset.NORMAL_LINE);
            designPageLayout.addView(canvasView);
            canvasView.setPresetPorterMode(null);


        }/*canvasView.getThread().drawBegin();
                canvasView.getThread().draw(2, 3);
                canvasView.getThread().drawEnd();*/
    }

    public String[] lineTypeArrayElements() {
        String[] lineTypes = {"Normal", "Dotted", "Dashed", "Barcode"};
        return lineTypes;
    }

    public int[] lineTypeImages() {
        int[] images = {R.drawable.dotted, R.drawable.dotted, R.drawable.dashedline, R.drawable.barcode};
        return images;
    }

    float currentDeviceWidth, currentDeviceHeight;

    private void getScreenResolution() {
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        currentDeviceWidth = metrics.widthPixels;
        currentDeviceHeight = metrics.heightPixels;


    }

    private void clearDraw(boolean value) {
        canvasView.clearView();
        File enrichFile = new File(objContent);
        if (enrichFile.exists()) {
            enrichFile.delete();
        }
        if (value) {
            try {
                //MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();

                JSONObject obj = new JSONObject();
                obj.put("type", Constants.DRAWING);
                obj.put("action", Constants.CLEAR_DRAW);
                obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
            } catch (Exception e) {
                Log.e("Exception", e.toString());
            }
        }
    }

    private void validateEnteredColor(String enteredColorCode, boolean value) {

        String HEX_PATTERN = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$";
        Pattern pattern = Pattern.compile(HEX_PATTERN);
        Matcher matcher;
        if (enteredColorCode.equals(" ") || enteredColorCode != null) {
            if (enteredColorCode.length() == 6) {
                byte[] b = new byte[0];
                try {
                    matcher = pattern.matcher("#" + enteredColorCode);
                    if (matcher.matches()) {
                        colorPickerEditText.setText("#" + enteredColorCode);
                        finalHex = "#" + enteredColorCode;
                        //This is final color after validation...................
                        colorCode = Color.parseColor(finalHex);
                        if (colosRead != null)
                            colosRead.finalHex = finalHex;
                        colorPickerImgView.setBackgroundColor(colorCode);

                        if (canvasView != null)
                            canvasView.setPresetColor(colorCode);
                        // Constants.sendMessageToReceiverWithMQTT2(BookViewReadActivity.this, Constants.COLOR_CODE, sendColorCodeToReceiver);
                        hideKeyboard(colorPickerEditText);
                    } else {
                        Toast.makeText(getBaseContext(), "Invalid Color Code", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getBaseContext(), "Enter Six Charecters", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getBaseContext(), "Enter Color Code", Toast.LENGTH_LONG).show();
        }

        if (value) {
            try {
                //  MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();

                JSONObject obj = new JSONObject();
                obj.put("type", Constants.DRAWING);
                obj.put("action", Constants.COLOR_CODE);

                obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                obj.put("color_code", enteredColorCode);
                ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
            } catch (Exception e) {
                Log.e("Exception", e.toString());
            }
        }
    }

    private void hideKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            editText.setSelection(editText.getText().length());
        }
    }

    private void publishOpacity(boolean value, int opacityValue) {
        if (opacityValue > 0) {
            if (canvasView != null)
                canvasView.setPresetBlur(1, opacityValue);
            if (value) {
                try {
                    //  MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();

                    JSONObject obj = new JSONObject();
                    obj.put("type", Constants.DRAWING);
                    obj.put("action", Constants.OPACITY);
                    obj.put("opacity", opacityValue);
                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
                } catch (Exception e) {
                    Log.e("Exception", e.toString());
                }
            }
        }
    }

    private void publishThickness(boolean value, int thickNessValue) {
        if (thickNessValue > 0) {
            if (canvasView != null)
                canvasView.setPresetSize(thickNessValue);
            if (value) {
                try {
                    // MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();

                    JSONObject obj = new JSONObject();
                    obj.put("type", Constants.DRAWING);
                    obj.put("action", Constants.THICKNESS);
                    obj.put("thickness", thickNessValue);
                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
                } catch (Exception e) {
                    Log.e("Exception", e.toString());
                }
            }
        }
    }

    private void btnDrawingDone(boolean value) {
        new saveDrawnTask().execute();
        if (value) {
            try {
                //  MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();
                // if(ManahijApp.getInstance().getNetworkState()) {
                if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                    JSONObject obj = new JSONObject();
                    obj.put("type", Constants.DRAWING);
                    obj.put("action", Constants.DRAWING_BTN_DONE);
                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
                }
                //  }
            } catch (Exception e) {
                Log.e("Exception", e.toString());
            }
        }
    }

    private void eraseDraw(boolean value) {

        if (value) {
            try {
                // MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();
                //if(ManahijApp.getInstance().getNetworkState()) {
                if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                    JSONObject obj = new JSONObject();
                    obj.put("type", Constants.DRAWING);
                    obj.put("action", Constants.BTN_ERASER);
                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
                    // }
                }
            } catch (Exception e) {
                Log.e("Exception", e.toString());
            }
        }

        if (!btnEarser.isSelected()) {
            btnEarser.setSelected(true);
        }
        btnPen.setSelected(false);

        btnEarser.setVisibility(View.GONE);
        btnPen.setVisibility(View.VISIBLE);
        canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));


    }

    private void selectPen(boolean value) {

        if (value) {
            try {
                //MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();
                //  if(ManahijApp.getInstance().getNetworkState()) {
                if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                    JSONObject obj = new JSONObject();
                    obj.put("type", Constants.DRAWING);
                    obj.put("action", Constants.DRAW_PEN);
                    obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                    ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
                    // }
                }
            } catch (Exception e) {
                Log.e("Exception", e.toString());
            }
        }
        if (!btnPen.isSelected()) {
            btnPen.setSelected(true);
        }
        btnEarser.setSelected(false);
        btnPen.setVisibility(View.GONE);
        btnEarser.setVisibility(View.VISIBLE);
        canvasView.getThread().activate();
        canvasView.setPresetPorterMode(null);

        //canvasView.getThread().activate();
    }

    private void setSelectedColorToDraw(String selcetedColor, boolean value) {
        if (canvasView != null) {
            canvasView.setPresetColor(Color.parseColor(selcetedColor));
            colorPickerEditText.setText("" + selcetedColor);
            colorPickerImgView.setBackgroundColor(Color.parseColor(selcetedColor));
        }
        if (value) {
            try {
                //MQTTSubscriptionService service = MQTTSubscriptionService.getInstance();
                String topicName = ManahijApp.getInstance().getPrefManager().getContactTopictoPublish();
                JSONObject obj = new JSONObject();
                obj.put("type", Constants.DRAWING);
                obj.put("action", Constants.SET_SELECTED_COLOR_TO_DRAW);
                obj.put(Constants.SET_SELECTED_COLOR_TO_DRAW, selcetedColor);
                obj.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                ss.publishMsg(TopicName, obj.toString(), BookViewReadActivity.this);
            } catch (Exception e) {
                Log.e("Exception", e.toString());
            }
        }
    }

    PopupWindow popupMessage;

    private void initiateColorPickerPopupWindow() {
        try {
            if (colosRead == null) {
                if (finalHex != null) {
                    colosRead = new ColorsRead(BookViewReadActivity.this, finalHex);
                    colosRead.currView = colosRead.updatelastColorBox(finalHex, true);
                } else {
                    colosRead = new ColorsRead(BookViewReadActivity.this, "");
                    colosRead.updatelastColorBox("", false);
                }
                colosRead.setX(deviceWidth / 2.5f);
                colosRead.setY(UserFunctions.toPx(90));
            } else {
                if (colosRead.finalHex != null) {
                    if (colosRead.currView != null) {
                        colosRead.prevSeleced = colosRead.currView;
                        colosRead.prevSeleced.checkBox.setVisibility(View.GONE);
                    }
                    colosRead.currView = colosRead.updatelastColorBox(finalHex, true);
                } else
                    colosRead.updatelastColorBox("", false);
                colosRead.setVisibility(View.VISIBLE);

                colosRead.invalidate();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideEditTextFocus() {
        colorPickerEditText.setFocusable(false);
        colorPickerEditText.setClickable(true);
    }

    public void popupInit() {
        popupMessage = new PopupWindow(colosRead, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        popupMessage.setContentView(colosRead);
        popupMessage.showAtLocation(designPageLayout, Gravity.TOP, deviceWidth / 4, 60);
    }

    private void openColorPicker(boolean value) {
        initiateColorPickerPopupWindow();
        popupInit();

    }


}