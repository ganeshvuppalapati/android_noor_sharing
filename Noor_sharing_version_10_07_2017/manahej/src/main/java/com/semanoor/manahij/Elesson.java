package com.semanoor.manahij;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.ptg.views.CircleButton;
import com.semanoor.paint.BrushPreset;
import com.semanoor.paint.PainterCanvas;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.PopupColorPicker;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.Statistics;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectStreamException;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import pl.polidea.treeview.InMemoryTreeStateManager;
import pl.polidea.treeview.TreeBuilder;
import pl.polidea.treeview.TreeStateManager;
import twitter4j.Twitter;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class Elesson extends Activity implements View.OnClickListener {
    public static int ELESSONREPORT_REQUEST_ID=1001;
    public Book currentbook;
    public String currentbookpath;
    public ArrayList<RAdataobject> mainlistobj = new ArrayList<RAdataobject>();
    public ArrayList<RAdataobject> previewlist = new ArrayList<RAdataobject>();
    public ArrayList<HashMap<String, String>> favouritesArray;
    private Boolean islistitemempty = false;
    public TreeViewList treeView;
   // public String[] pageText;
    public TextView tv_StudyCardsAlert;
    private CardContainer mCardContainer;
    TreeStateManager<Long> treeStateManager = null;
    private WebView webView;
    private Dialog showStudyCardsDialog;
    public MultiDirectionSlidingDrawer mDrawer;
    public static final int LEVEL_NUMBER = 4;
    private ProgressBar progressBar;
    private boolean timeInprogress = false;
    public ElessonNote elessonNote;
    // RelativeLayout rl_treeView;

    LinearLayout drawingToolbar;
    private boolean animation;
    public RAdataobject currentdataobj;
    private String strLang;

    private RelativeLayout rlToolbar;
    private LinearLayout btmLayout;
    private TextView txtTitle;

    public DatabaseHandler db;
    public String elessonBookTitle;
    Button btnIndex,fullscreen,btnIndicator,btnBookmark,btn_Search,btnGlobalSearch,btnHideBtmBar;
    public Button btnShare, btnInfo, btnPrint, btnFeedback;
    public CircleButton btn_note, btn_cards, btnDraw, btnEarser, btnPen, btn_brush, btnClearDrawing, btn_paint_undo, btn_paint_redo;

    ImageView bgImgView;
    String objContent;
    RelativeLayout designPageLayout, rootView;
    public PainterCanvas canvasView;
    private SeekBar mBrushSize, mBrushBlurRadius;
    private Spinner mBrushBlurStyle;
    ImageButton btn_BrushColor;
    String brushColor = null;
    public long startTime;
    public int elapsedTime;
    Date date;
    private String timeDiff;
    Statistics statistics;
    ArrayList<HashMap<String, String>> BookSearch = new ArrayList<HashMap<String, String>>();
    ArrayList<Object> listItemObject = new ArrayList<Object>();
    AdvanceSearchDialog adv_dialog;
    String fb_userName;
    public Twitter mTwitter;
    public RequestToken mRequestToken;
    public static final int WEBVIEW_REQUEST_CODE = 100;
    public static Object[] elessonNotecards;
    public Integer[] cardImgId = {R.color.yellow,R.color.green,R.color.skyblue,R.color.pink,R.color.lavender,R.color.white};
    private PopupWindow infoPopupWindow;
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String PREF_USER_NAME = "twitter_user_name";
    EditText mShareEditText;
    private DrawerLayout drawerView;
    private  ImageView semaLogo;
    public RelativeLayout relativeLayout;
    private String newVersion = "";
    private DownloadManager manager;
    private long downloadID;
    private int downloadCount = 1;
    private Button updateBtn;
    public Groups groups;
    public ArrayList<HashMap<String,String>> existList;
    public ArrayList<NoteData> noteDataArrayList;
    boolean allPageClicked = true;
    public ArrayList<NoteData> tempNoteDataArrayList = new ArrayList<>();
    SimpleCardStackAdapter	adapter;
    private Button note_countbtn1,note_countbtn2,note_countbtn3,notwell_btn,well_btn,vrywell_btn,count_btn;
    private ImageView noteColor1,noteColor2,noteColor3,noteColor4,noteColor5,noteColor6;
    private EditText descText ,titleText;
    private int CC;
    private ViewPager noteViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_elesson);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        db = new DatabaseHandler(this);
        statistics = new Statistics(Elesson.this);
        manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Query q = new DownloadManager.Query();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(Elesson.this);
        downloadID = pref.getLong(Globals.downloadID,0L);
        if (downloadID!=0L) {
            q.setFilterById(downloadID);
            Cursor cursor = manager.query(q);
            cursor.moveToFirst();
            downloadCount = cursor.getCount();
            if (cursor.getCount()>0) {
                int cursorInt = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                    replaceIJSTfiles();
                }
            }else{
                downloadID=0L;
            }
            cursor.close();
        }
        if (downloadID == 0L) {
            new updateIJST().execute(Globals.IJSTupdateUrl);
        }
        registerReceiver(onComplete,new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
        ViewGroup rootView_font = (ViewGroup) findViewById(android.R.id.content).getRootView();

        groups = Groups.getInstance();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Elesson.this);
        try {
            existList = (ArrayList<HashMap<String,String>>) ObjectSerializer.deserialize(prefs.getString(Globals.SUBSLIST, ObjectSerializer.serialize(new ArrayList<HashMap<String,String>>())));
        } catch (IOException e) {
            e.printStackTrace();
        }

        UserFunctions.changeFont(rootView_font,font);
        rootView = (RelativeLayout) findViewById(R.id.root_view);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        treeView = (TreeViewList) findViewById(R.id.mainTreeView);
        webView = (WebView) findViewById(R.id.webView1);
        semaLogo= (ImageView) findViewById(R.id.semaImage);
        drawingToolbar = (LinearLayout) findViewById(R.id.drawer_layout);
        designPageLayout = (RelativeLayout) findViewById(R.id.DesignPageLayout);
        btn_Search = (Button) findViewById(R.id.btnSearch);
        btnGlobalSearch = (Button) findViewById(R.id.btnGlobalSearch);
        relativeLayout = (RelativeLayout)findViewById(R.id.relativeLayout1);

        currentbook = (Book) getIntent().getSerializableExtra("Book");
        String bookName = currentbook.get_bStoreID();
        currentbookpath = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookName + "Book/";
        txtTitle = (TextView) findViewById(R.id.textView8);

        elessonNote = new ElessonNote(this);

        noteDataArrayList = db.loadNoteDivToArray(currentbook.get_bStoreID());
        for (int i=0;i<noteDataArrayList.size();i++){
            tempNoteDataArrayList.add(noteDataArrayList.get(i));
        }

        checkLanguage();
        //UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookName + "Book/" + "Drawfiles");
       // drawnFilesDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookName + "Book/" + "Drawfiles/";
        if (new File(currentbookpath + "Data/Lesson.xml").exists()) {
            XMLparse();
        }else{
           jsonParse();
        }
        loadtreelistview();
        if (!new File(currentbookpath + "Data/statistics.xml").exists()) {
            statistics.statisticsXml();
        }

        btnIndex = (Button) findViewById(R.id.btnIndex);
        btnIndex.setOnClickListener(this);
        fullscreen = (Button) findViewById(R.id.btnFullScreen);
        btnDraw = (CircleButton) findViewById(R.id.btnDraw);
        bgImgView = (ImageView) findViewById(R.id.bgImgView);
        btnEarser = (CircleButton) findViewById(R.id.btn_eraser);
        btn_brush = (CircleButton) findViewById(R.id.btn_brush);
        btn_paint_undo = (CircleButton) findViewById(R.id.btn_paint_undo);
        btn_paint_redo = (CircleButton) findViewById(R.id.btn_paint_redo);
        btnPen = (CircleButton) findViewById(R.id.btnPen);
        btnClearDrawing = (CircleButton) findViewById(R.id.btnClear);
        btnShare = (Button) findViewById(R.id.btnShare);
        btnHideBtmBar = (Button) findViewById(R.id.btnHideBtmBar);
        btnInfo = (Button) findViewById(R.id.btnInfo);
        btnPrint = (Button) findViewById(R.id.btnPrint);
        btnFeedback = (Button) findViewById(R.id.btnFeedback);
        Button btnBack = (Button) findViewById(R.id.btnback);
        drawerView = (DrawerLayout) findViewById(R.id.drawer_View);
        drawerView.setScrimColor(Color.TRANSPARENT);

        btnClearDrawing.setOnClickListener(this);
        btnPen.setOnClickListener(this);
        btnDraw.setOnClickListener(this);
        fullscreen.setOnClickListener(this);
        btn_Search.setOnClickListener(this);
        btnGlobalSearch.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        btnHideBtmBar.setOnClickListener(this);
        btnInfo.setOnClickListener(this);
        btnPrint.setOnClickListener(this);
        btnFeedback.setOnClickListener(this);
        btnBack.setOnClickListener(this);

        btnBookmark = (Button) findViewById(R.id.btnBookmark);
        btnBookmark.setOnClickListener(this);
        Button btnStatistics = (Button) findViewById(R.id.btnStatistics);
        btnStatistics.setOnClickListener(this);
        btnIndicator = (Button) findViewById(R.id.btnIndicator);
        btnIndicator.setOnClickListener(this);
        btn_note = (CircleButton) findViewById(R.id.btnNote);
        btn_note.setOnClickListener(this);
        btn_cards = (CircleButton)findViewById(R.id.btnFlashcard);
        btn_cards.setOnClickListener(this);
        rlToolbar = (RelativeLayout) findViewById(R.id.toolbar);
        btmLayout = (LinearLayout) findViewById(R.id.linearLayout1);
        btnEarser.setOnClickListener(this);
        btn_brush.setOnClickListener(this);
        btn_paint_undo.setOnClickListener(this);
        btn_paint_redo.setOnClickListener(this);

        addFavouritesToTheArray();

        RAdataobject rAdataobject = (RAdataobject) previewlist.get(0);
        loadWebView(rAdataobject);
        drawerView.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                relativeLayout.setBackgroundResource(R.color.hovercolor);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                relativeLayout.setBackgroundResource(R.color.index_blue);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    /**
     * Checks Book Language
     */
    private void checkLanguage() {
        String elessonHtmlPath = currentbookpath + "Index.html";
        if (!new File(elessonHtmlPath).exists()){
            elessonHtmlPath = currentbookpath + "index.html";
        }
        String elessonHtmlData = UserFunctions.readFileFromPath(new File(elessonHtmlPath));
        if (elessonHtmlData != null && elessonHtmlData.contains(getResources().getString(R.string.elesson_arabic_string))) {
            strLang = "ar";
        } else {
            strLang = "en";
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_elesson, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
/*
load the tree list content
 */

    public void loadtreelistview() {
        Long selectedid = null;

        // mDrawer.open();
        // mDrawer.setVisibility(View.VISIBLE);
//        treeStateManager = new InMemoryTreeStateManager<Long>();
//        TreeBuilder<Long> builder = new TreeBuilder<Long>(treeStateManager);
        for (int i = 0; i < mainlistobj.size(); i++) {
            RAdataobject mainraobjdata = mainlistobj.get(i);
            listItemObject.add(mainraobjdata);
            if (!mainraobjdata.getPath().equals("")) {
                previewlist.add(mainraobjdata);
                mainraobjdata.setIndexNo(previewlist.size());
            }
            if (i == 0) {
                selectedid = (long) listItemObject.size() - 1;
            }
            if (mainraobjdata.getChildobjlist().size() > 0) {
                for (int j = 0; j < mainraobjdata.getChildobjlist().size(); j++) {
                    RAdataobject subraobjdata = mainraobjdata.getChildobjlist().get(j);
                    listItemObject.add(subraobjdata);
                    if (!subraobjdata.getPath().equals("")) {
                        previewlist.add(subraobjdata);
                        subraobjdata.setIndexNo(previewlist.size());
                    }
                    if (i == 0 && j == 0) {
                        selectedid = (long) listItemObject.size() - 1;
                    }
                    if (subraobjdata.getChildobjlist().size() > 0) {
                        for (int k = 0; k < subraobjdata.getChildobjlist().size(); k++) {
                            RAdataobject itemobjdata = subraobjdata.getChildobjlist().get(k);
                            listItemObject.add(itemobjdata);
                            if (!itemobjdata.getPath().equals("")) {
                                previewlist.add(itemobjdata);
                                itemobjdata.setIndexNo(previewlist.size());
                            }
                            if (i == 0 && j == 0 && k == 0) {
                                selectedid = (long) listItemObject.size() - 1;
                            }
                            if (itemobjdata.getChildobjlist().size() > 0) {
                                for (int l = 0; l < itemobjdata.getChildobjlist().size(); l++) {
                                    RAdataobject listitemobjdata = itemobjdata.getChildobjlist().get(l);
                                    listItemObject.add(listitemobjdata);
                                    if (!listitemobjdata.getPath().equals("")) {
                                        previewlist.add(listitemobjdata);
                                        listitemobjdata.setIndexNo(previewlist.size());
                                    }
                                    if (i == 0 && j == 0 && k == 0 && l == 0) {
                                        selectedid = (long) listItemObject.size() - 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        treeStateManager = new InMemoryTreeStateManager<Long>();
        TreeBuilder<Long> builder = new TreeBuilder<Long>(treeStateManager);
        for (int i = 0; i < listItemObject.size(); i++) {
            RAdataobject raobjdata = (RAdataobject) listItemObject.get(i);
            builder.sequentiallyAddNextNode((long) i, raobjdata.getNodetype());
        }
        boolean collapsible = true;
        SimpleStandardAdapter simpleStandardAdapter = new SimpleStandardAdapter(this, selectedid, treeStateManager, LEVEL_NUMBER, listItemObject);
        treeView.setAdapter(simpleStandardAdapter);
        treeView.setCollapsible(collapsible);
        treeView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    /**
     * load webview for elesson
     */
    public void loadWebView(final RAdataobject dataObj) {
        progressBar.setVisibility(View.VISIBLE);
        String filepath;
        if (dataObj.getPath().equals("")) {
            semaLogo.setVisibility(View.VISIBLE);
            semaLogo.setImageResource(R.drawable.semanoor);
            webView.setVisibility(View.INVISIBLE);
            btn_note.setEnabled(false);
            btnBookmark.setEnabled(false);
            btnBookmark.setAlpha(0.5f);
            btnDraw.setEnabled(false);
            progressBar.setVisibility(View.INVISIBLE);
        }else {
            semaLogo.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            btn_note.setEnabled(true);
            btnBookmark.setEnabled(true);
            btnBookmark.setAlpha(1f);
            btnDraw.setEnabled(true);
        }
        if (dataObj.getPath().contains("http")) {
            filepath = dataObj.getPath();
        } else {
            filepath = "file:///" + currentbookpath + dataObj.getPath();
        }

        String file = currentbookpath + dataObj.getPath();
        String srcContent=null;
        if (!dataObj.getPath().equals("")){
            srcContent = UserFunctions.decryptFile(new File(file));
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
            String str = pref.getString(Globals.IJSTversion,"");
            if (str.contains(".zip")) {
                str = str.replace(".zip", "");
            }
            if(srcContent!=null && srcContent.contains("file://///200.1.1.66/IJST/")) {
                if (!str.equals("") && new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + "IJST/"+str).exists()){
                    srcContent = srcContent.replace("file://///200.1.1.66/IJST/", "file:../../../IJST/"+str+"/");
                }else {
                    srcContent = srcContent.replace("file://///200.1.1.66/IJST/", "file:../../../IJST/");
                }
            }
            startMeasuring();
        }
        currentdataobj = dataObj;
        checkAndApplyPaint(bgImgView, currentdataobj.getIndexNo());
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        //webView.clearCache(true);
        webView.getSettings().setDomStorageEnabled(true);

        checkAndDisplayFavourite();

        webView.setWebViewClient(new WebViewClient());
        if (!dataObj.getPath().equals("")) {
            if (dataObj.getPath().contains("http")) {
                webView.loadUrl(filepath);
            } else {
                webView.loadDataWithBaseURL(filepath, srcContent, "text/html", "utf-8", null);
            }
        }
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    String thumbPath = currentbookpath + currentdataobj.getPageId() + "_thumb.png";
                    new captureWebViewScreenShot(thumbPath).execute();
                }
            }
         });
    }


    private class captureWebViewScreenShot extends AsyncTask<Void, Void, Bitmap>
    {
        String path;

        public captureWebViewScreenShot(String path){
            this.path = path;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                Thread.sleep(1000);
                //webView.layout(0, 0, webView.getMeasuredWidth(), webView.getMeasuredHeight());
                webView.setDrawingCacheEnabled(true);
                webView.buildDrawingCache();
                Bitmap b = Bitmap.createBitmap(webView.getMeasuredWidth(),webView.getMeasuredHeight(),Bitmap.Config.ARGB_8888);

                Canvas c = new Canvas(b);
                Paint paint = new Paint();
                int iHeight = b.getHeight();
                c.drawBitmap(b, 0, iHeight, paint);
                webView.draw(c);
                return b;
            }
            //catch (InterruptedException e){}
            catch (Exception e) {}
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap b) {
            FileOutputStream fos = null;
            try{
                fos = new FileOutputStream(path);
                if(fos != null){
                    b.compress(Bitmap.CompressFormat.PNG,100,fos);
                    fos.close();
                }
            }catch (Exception e){

            }
        }
    }

    /**
     * parsing book XML
     */
    public void XMLparse() {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            DefaultHandler handler = new DefaultHandler() {
                String currentNode = "";

                RAdataobject mainraobjdata;
                RAdataobject subraobjdata;
                RAdataobject listraobjdata;
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                    if (qName.contentEquals("Lesson")) {
                        elessonBookTitle = attributes.getValue("Title");
                        txtTitle.setText(elessonBookTitle);
                    } else if (qName.contains("MTab")) {
                        String pageTitle = attributes.getValue("Title");
                        String path = attributes.getValue("Url");
                        mainraobjdata = new RAdataobject(pageTitle, path, 0);
                    } else if (qName.contains("STab")) {
                        String pageTitle = attributes.getValue("Title");
                        String path = attributes.getValue("Url");
                        subraobjdata = new RAdataobject(pageTitle, path, 1);
                        mainraobjdata.getChildobjlist().add(subraobjdata);
                    } else if (qName.contains("Item")) {
                        String Title = attributes.getValue("Title");
                        String path = attributes.getValue("Url");
                        String Type = attributes.getValue("Type");
                        if (Type.equals("7")) {
                            islistitemempty = false;
                        }
                        if (!islistitemempty) {
                            listraobjdata = new RAdataobject(Title, path, 2);
                            listraobjdata.setType(Type);
                            if (Type.equals("7")) {
                                islistitemempty = true;
                            }
                            subraobjdata.getChildobjlist().add(listraobjdata);
                        } else {
                            RAdataobject listitemraobjdata = new RAdataobject(Title, path, 3);
                            listitemraobjdata.setType(Type);
                            listraobjdata.getChildobjlist().add(listitemraobjdata);
                        }
                    }
                }

                @Override
                public void endElement(String uri, String localName, String qName) throws SAXException {
                    if (qName.contains("MTab")) {
                        mainlistobj.add(mainraobjdata);
                    }
                }
            };

            String filePath = currentbookpath + "Data/Lesson.xml";
            InputStream inputStream = new FileInputStream(filePath);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");
            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");
            saxParser.parse(is, handler);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void disablebuttons() {
        btnIndex.setEnabled(false);
        btnIndex.setAlpha(0.5f);
        btnIndicator.setEnabled(false);
        btnHideBtmBar.setEnabled(false);
        btnHideBtmBar.setAlpha(0.5f);
        btnIndicator.setBackgroundResource(R.drawable.e_arr_index_up);
        rlToolbar.setVisibility(View.GONE);
        txtTitle.setVisibility(View.VISIBLE);
    }

    private void enablebuttons() {
        btnIndex.setEnabled(true);
        btnIndex.setAlpha(1f);
        btnIndicator.setEnabled(true);
        btnHideBtmBar.setEnabled(true);
        btnHideBtmBar.setAlpha(1f);
        btnIndicator.setBackgroundResource(R.drawable.e_arr_index_down);
        rlToolbar.setVisibility(View.VISIBLE);
        txtTitle.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnIndex:
                /*if (!treeView.isShown()) {
                    Animation anim = AnimationUtils.loadAnimation(Elesson.this, R.anim.slide_in_right);
                    treeView.startAnimation(anim);
                    treeView.setVisibility(View.VISIBLE);
                } else {
                    Animation anim = AnimationUtils.loadAnimation(Elesson.this, R.anim.slide_out_right);
                    treeView.startAnimation(anim);
                    treeView.setVisibility(View.GONE);
                }*/
                if (drawerView.isDrawerOpen(Gravity.END)){
                    drawerView.closeDrawers();
                    relativeLayout.setBackgroundResource(R.color.index_blue);
                } else {
                    drawerView.openDrawer(Gravity.END);
                    relativeLayout.setBackgroundResource(R.color.hovercolor);
                }
                break;
            case R.id.btnFullScreen:
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),groups.getElessonPowerPointEnable(),currentbook.getPurchaseType())) {
                    Intent fullscrnintent = new Intent(Elesson.this, ElessonfullscreenActivity.class);
                    fullscrnintent.putExtra("currentpath", currentdataobj);
                    fullscrnintent.putExtra("mainlist", previewlist);
                    fullscrnintent.putExtra("currentbook", currentbookpath);
                    fullscrnintent.putExtra("Language", strLang);
                    startActivity(fullscrnintent);
                }
                break;
            case R.id.btnIndicator:
                if (rlToolbar.isShown()) {
                    rlToolbar.setVisibility(View.GONE);
                    Animation anim=AnimationUtils.loadAnimation(this, R.anim.toolbar_anim_elesson_down);
                    rlToolbar.setAnimation(anim);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            txtTitle.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    ObjectAnimator animation=ObjectAnimator.ofFloat(btnIndicator,"rotation",0,180);
                    animation.setDuration(500);
                    animation.setRepeatCount(0);
                    animation.start();
//                    btnIndicator.setBackgroundResource(R.drawable.e_arr_index_up);

                } else {
                    txtTitle.setVisibility(View.GONE);
                    Animation anim=AnimationUtils.loadAnimation(this, R.anim.toolbar_anim_elesson_up);
                    rlToolbar.setAnimation(anim);
                    ObjectAnimator animation=ObjectAnimator.ofFloat(btnIndicator,"rotation",180,360);
                    animation.setDuration(500);
                    animation.setRepeatCount(0);
                    animation.start();
//                    btnIndicator.setBackgroundResource(R.drawable.e_arr_index_down);
                    rlToolbar.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.btnStatistics:
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),groups.getElessonStatisticsEnable(),currentbook.getPurchaseType())) {
                    stopMeasuringAndUpdateXml();
                    Intent reportIntent = new Intent(Elesson.this, ElessonReportActivity.class);
                    reportIntent.putExtra("BookTitle", elessonBookTitle);
                    reportIntent.putExtra("currentBookPath", currentbookpath);
                    reportIntent.putExtra("favList", favouritesArray);
                    reportIntent.putExtra("mainlist", previewlist);
                    reportIntent.putExtra("Book", currentbook);
                    startActivityForResult(reportIntent, ELESSONREPORT_REQUEST_ID);
                }
                break;
            case R.id.btnBookmark:
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),false,currentbook.getPurchaseType())) {
                    selectFavorite();
                }
                break;
            case R.id.btnDraw:
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),groups.getElessonDrawingEnable(),currentbook.getPurchaseType())) {
                    if (!drawingToolbar.isShown()) {
                        drawerView.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        drawingToolbar.setVisibility(View.VISIBLE);
                        disablebuttons();
                        btnPen.setSelected(true);
                        if (btnEarser.isSelected()) {
                            btnEarser.setSelected(false);
                        }
                        // designScrollPageView.setEnableScrolling(false);
                        int pageNo = currentdataobj.getIndexNo();
                        //objContent = drawnFilesDir + "Page" + " " + pageNo + ".png";
                        objContent = currentbookpath + currentdataobj.getPageId() + ".png";
                        canvasView = new PainterCanvas(Elesson.this, objContent, null);
                        designPageLayout.addView(canvasView);
                        canvasView.setPresetPorterMode(null);
                        bgImgView.setVisibility(View.GONE);
                    } else {
                        new saveDrawnTask().execute();
                        drawerView.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                        enablebuttons();
                    }
                }
                break;
            case R.id.btn_brush: {
                showBrushSettingsDialog();
                break;
            }
            case R.id.btn_eraser: {
                if (!btnEarser.isSelected()) {
                    btnEarser.setSelected(true);
                }
                btnPen.setSelected(false);
                canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                break;
            }
            case R.id.btnNote: {
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),groups.getElessonNoteEnable(),currentbook.getPurchaseType())) {
                    elessonNote.LoadNotepopup();
                }
                break;
            }
            case R.id.btnFlashcard: {
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),groups.getElessonFlashcardEnable(),currentbook.getPurchaseType())) {
                 /*   showStudyCardsDialog = new Dialog(this);
                    showStudyCardsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
                    showStudyCardsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    showStudyCardsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.card_layout, null));
                    showStudyCardsDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), ActionBar.LayoutParams.MATCH_PARENT);
                    mCardContainer = (CardContainer) showStudyCardsDialog.findViewById(R.id.layoutview);

                    tv_StudyCardsAlert = (TextView) showStudyCardsDialog.findViewById(R.id.txt);
                    studycardAdapter();
                    showStudyCardsDialog.show();*/
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                    showStudyCardsDialogg();
                }
                break;
            }

            case R.id.btnPen: {
                if (!btnPen.isSelected()) {
                    btnPen.setSelected(true);
                }
                btnEarser.setSelected(false);
                canvasView.getThread().activate();
                canvasView.setPresetPorterMode(null);
                break;
            }
            case R.id.btnClear: {
                canvasView.clearView();
                File enrichFile = new File(objContent);
                if (enrichFile.exists()) {
                    enrichFile.delete();
                }
                break;
            }
            case R.id.btn_paint_undo: {
                canvasView.paintUndo();
                break;
            }
            case R.id.btn_paint_redo: {
                canvasView.paintRedo();
                break;
            }
            case R.id.btnSearch: {
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),groups.getElessonSearchEnable(),currentbook.getPurchaseType())) {
                    if (adv_dialog == null) {
                        adv_dialog = new AdvanceSearchDialog(Elesson.this);
                        adv_dialog.processXML();
                    }
                    adv_dialog.wordSearchDialog();
                }
                break;
            }
            case R.id.btnGlobalSearch: {
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),groups.getElessonSearchInWebEnable(),currentbook.getPurchaseType())) {
                    AdvanceSearchDialog adv_dialog = new AdvanceSearchDialog(Elesson.this);
                    adv_dialog.showGlobalSearchWindow();
                }
                break;
            }
            case R.id.btnShare: {
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),false,currentbook.getPurchaseType())) {
                    SharingText sharingText = new SharingText(Elesson.this);
                    sharingText.showShareDialogWindow();
                }

                break;
            }
            case R.id.btnHideBtmBar:{
                if (btmLayout.isShown()) {
                    Animation anim=AnimationUtils.loadAnimation(this, R.anim.toolbar_anim_down);
                    btmLayout.setAnimation(anim);
                    btmLayout.setVisibility(View.GONE);
                } else {
                    Animation anim=AnimationUtils.loadAnimation(this, R.anim.toolbar_anim_up);
                    btmLayout.setAnimation(anim);
                    btmLayout.setVisibility(View.VISIBLE);
                }
                break;
            }
            case R.id.btnInfo:{
                displayBtnInfoPopup();
                break;
            }
            case R.id.btnPrint:{
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),groups.getElessonPrintEnable(),currentbook.getPurchaseType())) {
                    createWebPrintJob(webView);
                }
                break;
            }
            case R.id.btnFeedback:{
                if (UserFunctions.checkLoginAndSubscription(this,false,existList,currentbook.getClientID(),groups.getElessonFeedbackEnable(),currentbook.getPurchaseType())) {
                    try {
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"feedback@semanoor.com", "info@semanoor.com"});
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Elesson: " + elessonBookTitle + " feedback");
                        String bodyText = getString(R.string.lesson_title_colon) + " " + elessonBookTitle + " \n" + " " + getString(R.string.lesson_author_colon) + "Semanoor" + " \n -------------------------- \n";
                        bodyText = bodyText + getString(R.string.user_email_colon) + " \n " + getString(R.string.user_name_colon) + " \n " + getString(R.string.user_message_colon) + " ";
                        emailIntent.putExtra(Intent.EXTRA_TEXT, bodyText);
                        emailIntent.setType("text/plain");
                        startActivity(emailIntent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        //There are no mail client installed on users device.

                    }
                }
                break;
            }
            case R.id.btnback:{
                finish();
                break;
            }
            case R.id.all_btn:{
                loadAllNotesInCurrentPage();
                break;
            }
            case R.id.imgBtnC1:{
                loadNotesBasedOnColors(1);
                break;
            }
            case R.id.imgBtnC2:{
                loadNotesBasedOnColors(2);
                break;
            }
            case  R.id.imgBtnC3:{
                loadNotesBasedOnColors(3);
                break;
            }
            case  R.id.imgBtnC4:{
                loadNotesBasedOnColors(4);
                break;
            }
            case  R.id.imgBtnC5:{
                loadNotesBasedOnColors(5);
                break;
            }
            case R.id.imgBtnC6:{
                loadNotesBasedOnColors(6);
                break;
            }
            case R.id.count_btn1:{
                loadNotesBasedOnType(1);
                break;
            }
            case R.id.count_btn2:{
                loadNotesBasedOnType(2);
                break;
            }
            case R.id.count_btn3:{
                loadNotesBasedOnType(3);
                break;
            }
            case R.id.notwell_btn:{
                if (noteDataArrayList.size()>0) {
                    int position = mCardContainer.cardPosition;
                    if (position == -1) {
                        position = 0;
                    }
                    NoteData data = noteDataArrayList.get(position);
                    if (data.getNoteType() != 1) {
                        data.setNoteType(1);
                        String query = "update tblNote set NoteType='1' where BName='" + currentbook.get_bStoreID() + "' and PageNo='" + data.getNotePageNo() + "' and TabNo='" + data.getNoteTabNo() + "' and SText='" + data.getNoteTitle() + "' and SDesc='" + data.getNoteDesc() + "'";
                        db.executeQuery(query);
                        setBackgroudColorForButtons(position);
                        updateButtons();
                    }
                }
                break;
            }
            case R.id.well_btn:{
                if (noteDataArrayList.size()>0) {
                    int position = mCardContainer.cardPosition;
                    if (position == -1) {
                        position = 0;
                    }
                    NoteData data = noteDataArrayList.get(position);
                    if (data.getNoteType() != 2) {
                        data.setNoteType(2);
                        String query = "update tblNote set NoteType='2' where BName='" + currentbook.get_bStoreID() + "' and PageNo='" + data.getNotePageNo() + "' and TabNo='" + data.getNoteTabNo() + "' and SText='" + data.getNoteTitle() + "' and SDesc='" + data.getNoteDesc() + "'";
                        db.executeQuery(query);
                        setBackgroudColorForButtons(position);
                        updateButtons();
                    }
                }
                break;
            }
            case R.id.verywell_btn:{
                if (noteDataArrayList.size()>0) {
                    int position = mCardContainer.cardPosition;
                    if (position == -1) {
                        position = 0;
                    }
                    NoteData data = noteDataArrayList.get(position);
                    if (data.getNoteType() != 3) {
                        data.setNoteType(3);
                        String query = "update tblNote set NoteType='3' where BName='" + currentbook.get_bStoreID() + "' and PageNo='" + data.getNotePageNo() + "' and TabNo='" + data.getNoteTabNo() + "' and SText='" + data.getNoteTitle() + "' and SDesc='" + data.getNoteDesc() + "'";
                        db.executeQuery(query);
                        setBackgroudColorForButtons(position);
                        updateButtons();
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    public void setPreset(View v) {
        switch (v.getId()) {
            case R.id.preset_pencil:
                canvasView.setPreset(new BrushPreset(BrushPreset.PENCIL, canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                }
                break;
            case R.id.preset_brush:
                canvasView.setPreset(new BrushPreset(BrushPreset.BRUSH, canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                }
                break;
            case R.id.preset_marker:
                canvasView.setPreset(new BrushPreset(BrushPreset.MARKER, canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                }
                break;
            case R.id.preset_pen:
                canvasView.setPreset(new BrushPreset(BrushPreset.PEN, canvasView
                        .getCurrentPreset().color));
                if (btnEarser.isSelected()) {
                    canvasView.setPresetPorterMode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                }
                break;
        }

        updateControls();
    }

    /*
     * Brush settings dialog for drawing
     */
    private void showBrushSettingsDialog() {
        Dialog brushSettingsDialog = new Dialog(this);
        brushSettingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        brushSettingsDialog.setTitle(R.string.brush_settings);
        brushSettingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.brush_settings_dialog, null));
        //brushSettingsDialog.getWindow().setLayout((int)(g.getDeviceWidth()/1.5), (int)(g.getDeviceHeight()/1.5));
        brushSettingsDialog.getWindow().setLayout((int) getResources().getDimension(R.dimen.drawing_brush_dialog), (int) getResources().getDimension(R.dimen.elesson_brush_dialog_height));
        mBrushSize = (SeekBar) brushSettingsDialog.findViewById(R.id.brush_size);
        btn_BrushColor = (ImageButton) brushSettingsDialog.findViewById(R.id.brush_color);

        if (brushColor != null && brushColor.equals(canvasView.getCurrentPreset())) {
            btn_BrushColor.setBackgroundColor(Color.parseColor(brushColor));
        } else {
            btn_BrushColor.setBackgroundColor(canvasView.getCurrentPreset().color);
        }

        mBrushSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 0) {
                    canvasView.setPresetSize(seekBar.getProgress());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (progress > 0) {
                    if (fromUser) {
                        canvasView.setPresetSize(seekBar.getProgress());
                    }
                } else {
                    mBrushSize.setProgress(1);
                }
            }
        });
        mBrushBlurRadius = (SeekBar) brushSettingsDialog.findViewById(R.id.brush_blur_radius);
        mBrushBlurRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                updateBlurSeek(seekBar.getProgress());

                if (seekBar.getProgress() > 0) {
                    setBlur();
                } else {
                    canvasView.setPresetBlur(null, 0);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                if (fromUser) {
                    updateBlurSeek(progress);
                    if (progress > 0) {
                        setBlur();
                    } else {
                        canvasView.setPresetBlur(null, 0);
                    }
                }
            }
        });
        mBrushBlurStyle = (Spinner) brushSettingsDialog.findViewById(R.id.brush_blur_style);
        mBrushBlurStyle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View v,
                                       int position, long id) {
                if (id > 0) {
                    updateBlurSpinner(id);
                    setBlur();
                } else {
                    mBrushBlurRadius.setProgress(0);
                    canvasView.setPresetBlur(null, 0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
        updateControls();
        brushSettingsDialog.show();
    }

    private void updateControls() {
        mBrushSize.setProgress((int) canvasView.getCurrentPreset().size);
        if (canvasView.getCurrentPreset().blurStyle != null) {
            mBrushBlurStyle.setSelection(canvasView.getCurrentPreset().blurStyle
                    .ordinal() + 1);
            mBrushBlurRadius.setProgress(canvasView.getCurrentPreset().blurRadius);
        } else {
            mBrushBlurStyle.setSelection(0);
            mBrushBlurRadius.setProgress(0);
        }
    }

    private void updateBlurSpinner(long blur_style) {
        if (blur_style > 0 && mBrushBlurRadius.getProgress() < 1) {
            mBrushBlurRadius.setProgress(1);
        }
    }

    private void updateBlurSeek(int progress) {
        if (progress > 0) {
            if (mBrushBlurStyle.getSelectedItemId() < 1) {
                mBrushBlurStyle.setSelection(1);
            }
        } else {
            mBrushBlurStyle.setSelection(0);
        }
    }

    private void setBlur() {
        canvasView.setPresetBlur((int) mBrushBlurStyle.getSelectedItemId(),
                mBrushBlurRadius.getProgress());
    }

    private void checkAndApplyPaint(ImageView cvsImageView, int pageNo) {
        //objContent = drawnFilesDir + "Page" + " " + pageNo + ".png";
        objContent = currentbookpath + currentdataobj.getPageId()+".png";
        setBackgroundImageForDrawing(cvsImageView);

    }


    /**
     * @author saveDrawnImagetoFilesDir
     */
    private class saveDrawnTask extends AsyncTask<Void, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Elesson.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage(getResources().getString(R.string.processing));
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            saveBitmap(canvasView.objContentPath);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setBackgroundImageForDrawing(bgImgView);
            designPageLayout.removeView(canvasView);
            drawingToolbar.setVisibility(View.GONE);
            progressDialog.dismiss();
        }
    }

    /**
     * @param pictureName
     * @author saveBitmapImage
     */
    private void saveBitmap(String pictureName) {
        try {
            canvasView.saveBitmap(pictureName);
            canvasView.changed(false);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @author setBackgroundImageForDrawing
     */
    public void setBackgroundImageForDrawing(ImageView cvsImageView) {
        if (new File(objContent).exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(objContent);
            cvsImageView.setVisibility(View.VISIBLE);
            cvsImageView.setImageBitmap(bitmap);
        } else {
            cvsImageView.setImageBitmap(null);
            cvsImageView.setVisibility(View.GONE);
        }
    }


    /**
     * @author showColorPickerDialogtochangeDrawingcolor
     */
    public void changeBrushColor(View v) {
        final ArrayList<String> recentPaintColorList = this.getRecentColorsFromSharedPreference(Globals.paintColorKey);
        new PopupColorPicker(this, canvasView.getCurrentPreset().color, v, recentPaintColorList, false, new PopupColorPicker.ColorPickerListener() {
            int pickedColor = 0;
            String hexColor = null;

            @Override
            public void pickedColor(int color) {
                hexColor = String.format("#%06X", (0xFFFFFF & color));
                pickedColor = color;
                brushColor = hexColor;
                btn_BrushColor.setBackgroundColor(Color.parseColor(hexColor));
            }

            @Override
            public void dismissPopWindow() {
                canvasView.setPresetColor(pickedColor);
                if (hexColor != null) {
                    setandUpdateRecentColorsToSharedPreferences(Globals.paintColorKey, recentPaintColorList, hexColor);
                }
            }

            @Override
            public void pickedTransparentColor(String transparentColor) {

            }

            @Override
            public void pickColorFromBg() {

            }
        });
    }

    /**
     * setandUpdateRecentColorsToSharedPreferences
     *
     * @param key
     * @param recentColorList
     * @param recentColor
     */
    public void setandUpdateRecentColorsToSharedPreferences(String key, ArrayList<String> recentColorList, String recentColor) {
        ArrayList<String> tempArrayList = new ArrayList<String>();
        for (int i = 0; i < recentColorList.size(); i++) {
            if (i == 0) {
                tempArrayList.add(recentColor);
            } else {
                tempArrayList.add(recentColorList.get(i - 1));
            }
        }
        recentColorList.clear();
        recentColorList = tempArrayList;

        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreference.edit();
        try {
            editor.putString(key, ObjectSerializer.serialize(recentColorList));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    /**
     * getRecentColorsFromSharedPreferences
     *
     * @param key
     * @return
     */
    public ArrayList<String> getRecentColorsFromSharedPreference(String key) {
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
        ArrayList<String> recentColorList = null;
        try {
            recentColorList = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (recentColorList.size() == 0) {
            recentColorList.add("#B0171F");
            recentColorList.add("#FF82AB");
            recentColorList.add("#0000FF");
            recentColorList.add("#000000");
            recentColorList.add("#006400");
        }
        return recentColorList;
    }

    private void selectFavorite() {
        HashMap<String, String> map = new HashMap<>();
        map.put("PageTitle", currentdataobj.getName());
        map.put("pageID", currentdataobj.getPageId());
        if (favouritesArray.contains(map)) {
            String query = "delete from tblElessonBmark where lessonID='" + currentbook.get_bStoreID() + "' and pageID='" + currentdataobj.getPageId() + "'";
            db.executeQuery(query);
            btnBookmark.setBackgroundResource(R.drawable.e_favourite);
        } else {
            String query = "insert into tblElessonBmark(lessonID,pageID,pageTitle) values('" + currentbook.get_bStoreID() + "','" + currentdataobj.getPageId() + "','" + currentdataobj.getName() + "')";
            db.executeQuery(query);
            btnBookmark.setBackgroundResource(R.drawable.e_favourite_fill);
        }
        addFavouritesToTheArray();
    }

    private void addFavouritesToTheArray() {
        favouritesArray = db.getAllElessonBookmark(currentbook.get_bStoreID());
    }

    private void checkAndDisplayFavourite() {
        HashMap<String, String> map = new HashMap<>();
        map.put("PageTitle", currentdataobj.getName());
        map.put("pageID", currentdataobj.getPageId());
        if (favouritesArray.contains(map)) {
            btnBookmark.setBackgroundResource(R.drawable.e_favourite_fill);
        } else {
            btnBookmark.setBackgroundResource(R.drawable.e_favourite);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        startMeasuring();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopMeasuringAndUpdateXml();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startMeasuring();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopMeasuringAndUpdateXml();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
    }

    public void startMeasuring() {
        startTime = Calendar.getInstance().getTimeInMillis();
    }

    public void stopMeasuringAndUpdateXml() {
        long endTime = Calendar.getInstance().getTimeInMillis();
        long totalElapsedTime = endTime - startTime;
        elapsedTime = (int) TimeUnit.MILLISECONDS.toSeconds(totalElapsedTime);
        if (currentdataobj != null) {
            statistics.readStatisticsXml();
        }
    }

    /*
     *  Selecting the page in treeListview
     */
    public void selectingPageInListView(String Id) {
        //String name = SearchList.get(position);
        for (int i = 0; i < listItemObject.size(); i++) {
            RAdataobject subraobjdata = (RAdataobject) listItemObject.get(i);
            String path = subraobjdata.getPath();
            if (!path.equals("")) {
                String pageId = path.substring(0, path.lastIndexOf('.'));
                //  String Id=Url.substring(0, Url.lastIndexOf('.'));
                if (pageId.equals(Id)) {
                    SimpleStandardAdapter simpleStandardAdapter = new SimpleStandardAdapter(Elesson.this, (long) i, treeStateManager, LEVEL_NUMBER, listItemObject);
                    treeView.setAdapter(simpleStandardAdapter);
                    treeView.setCollapsible(true);
                    treeView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    treeView.invalidateViews();
                    loadWebView(subraobjdata);
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 64206) {
            Session session = Session.getActiveSession();
            session.onActivityResult(this, requestCode, resultCode, data);
            if (session != null && session.isOpened()) {
                Request.newMeRequest(session, new Request.GraphUserCallback() {

                    @Override
                    public void onCompleted(GraphUser _user, Response response) {
                        if (_user != null) {
                            fb_userName=_user.getName();
                            SharingText sharingText = new SharingText(Elesson.this);
                            sharingText.publishFeedDialog(_user.getName(), "Download E-Lesson "+elessonBookTitle);
                        }
                    }
                }).executeAsync();
            }
        }else if(requestCode==WEBVIEW_REQUEST_CODE && data!=null){
            String verifier = data.getExtras().getString(Globals.IEXTRA_OAUTH_VERIFIER);
            if(!verifier.equals("Exit")) {

                String selectedText = data.getExtras().getString("text");
                try {
                    AccessToken accessToken = mTwitter.getOAuthAccessToken(mRequestToken, verifier);
                    long userID = accessToken.getUserId();
                    final User user = mTwitter.showUser(userID);
                    String username = user.getName();

                    TwitterDialog twitterDialog = new TwitterDialog(Elesson.this);
                    twitterDialog.saveTwitterInfo(accessToken);
                    twitterDialog.twitterShareAlertDialog(selectedText);
                    //twitterDialog.sharingText(selectedText);
                } catch (Exception e) {
                    Log.e("Twitter Login Failed", e.getMessage());
                }
            }else{
                String selectedText = data.getExtras().getString("text");
            }
        }else if(requestCode == ELESSONREPORT_REQUEST_ID) {
            if(resultCode == RESULT_OK){
                String myValue = data.getExtras().getString("pageID");
                selectingPageInListView(myValue);
            }
        }
    }

    public void displayBtnInfoPopup() {
        View view = getLayoutInflater().inflate(R.layout.elesson_info, null);
        TextView txtLessonName = (TextView) view.findViewById(R.id.txtLessonName);
        txtLessonName.setText(elessonBookTitle);
        TextView txtAbout = (TextView) view.findViewById(R.id.txtAboutUs);
        txtAbout.setText(currentbook.get_bDescription());
        updateBtn = (Button) view.findViewById(R.id.updateIJST_btn);
        updateBtn.setVisibility(View.GONE);
       /* if (downloadID==0 || downloadCount==0){
            updateBtn.setEnabled(true);
        }else{
            updateBtn.setEnabled(false);
        }
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new updateIJST().execute("http://www.nooor.com/ijst/ijst.txt");
            }
        });*/
        infoPopupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        infoPopupWindow.setContentView(view);
        infoPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        infoPopupWindow.setOutsideTouchable(true);
        infoPopupWindow.setFocusable(true);
        infoPopupWindow.showAsDropDown(btnInfo, 0, 0);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void createWebPrintJob(WebView webView){
        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();
        String jobName = getString(R.string.app_name)+" "+currentdataobj.getName();
        printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());
    }

    /*
     *  Looping json object to hashmap
     */

    private Map<String,Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String,Object> retMap = new HashMap<String,Object>();
        if (json!=JSONObject.NULL){
            retMap = toMap(json);
        }
        return retMap;
    }
    private Map<String,Object> toMap(JSONObject object) throws JSONException {
        Map<String,Object> map = new HashMap<String,Object>();
        Iterator<String> keysIter = object.keys();
        while (keysIter.hasNext()){
            String key = keysIter.next();
            Object value = object.get(key);
            if (value instanceof JSONArray){
                value = toList((JSONArray) value);
            }else if (value instanceof JSONObject){
                value = toMap((JSONObject)value);
            }
            map.put(key,value);
        }
        return map;
    }
    private List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0;i<array.length();i++){
            Object value = array.get(i);
            if (value instanceof JSONArray){
                value = toList((JSONArray) value);
            }else if (value instanceof JSONObject){
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    /*
     * Parsing new Elesson json file
     */

    private void jsonParse(){
        String jsonData = null;
        File jsonFile = new File(currentbookpath + "Data/Lesson.json");
        String returnResponse = null;
        RAdataobject mainraobjdata = null;
        RAdataobject subraobjdata;
        RAdataobject listraobjdata;
        try {
            returnResponse = UserFunctions.convertJsonFileToString(jsonFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        jsonData = returnResponse;
        jsonData = jsonData.replaceAll("\\\\r\\\\n","");
        jsonData = jsonData.replace("\"{", "{");
        jsonData = jsonData.replace("}\",", "},");
        jsonData = jsonData.replace("}\"", "}");
        try {
            JSONObject json = new JSONObject(jsonData);
            HashMap<String,Object> jsonMap = (HashMap<String, Object>) jsonToMap(json);
            HashMap<String,Object> lessonHash = (HashMap<String, Object>) jsonMap.get("ELesson");
            HashMap<String,Object> structureHash = (HashMap<String, Object>) lessonHash.get("Structure");
            HashMap<String,Object> nodeHash = (HashMap<String, Object>) structureHash.get("node");
            elessonBookTitle = (String) nodeHash.get("title");
            txtTitle.setText(elessonBookTitle);
            HashMap<String,Object> mediaHash = (HashMap<String, Object>) nodeHash.get("media");
            String url = (String) mediaHash.get("url");
            String type = (String) mediaHash.get("type");

            ArrayList<HashMap<String,Object>> childHash = (ArrayList<HashMap<String, Object>>) nodeHash.get("children");
            if (childHash.size()>0) {
                for (int main = 0; main < childHash.size(); main++) {
                    HashMap<String,Object> child = childHash.get(main);
                    String title = (String) child.get("title");
                    HashMap<String, Object> mediaHash1 = (HashMap<String, Object>) child.get("media");
                    String url1 = (String) mediaHash1.get("url");
                    mainraobjdata = new RAdataobject(title, url1, 0);
                    ArrayList<HashMap<String,Object>> childHash1 = (ArrayList<HashMap<String, Object>>) child.get("children");
                    if (childHash1.size() > 0) {
                        for (int sub = 0; sub < childHash1.size(); sub++) {
                            HashMap<String,Object> child1 = childHash1.get(sub);
                            String title1 = (String) child1.get("title");
                            HashMap<String, Object> mediaHash2 = (HashMap<String, Object>) child1.get("media");
                            String url2 = (String) mediaHash2.get("url");
                            subraobjdata = new RAdataobject(title1, url2, 1);
                            mainraobjdata.getChildobjlist().add(subraobjdata);
                            ArrayList<HashMap<String,Object>> childHash2 = (ArrayList<HashMap<String, Object>>) child1.get("children");
                            if (childHash2.size() > 0) {
                                for (int list = 0; list < childHash2.size(); list++) {
                                    HashMap<String,Object> child2 = childHash2.get(list);
                                    String title2 = (String) child2.get("title");
                                    HashMap<String, Object> mediaHash3 = (HashMap<String, Object>) child2.get("media");
                                    String url3 = (String) mediaHash3.get("url");
                                    String type3;
                                    Object typeObj = mediaHash3.get("type");
                                    if (typeObj instanceof Integer){
                                        type3 = String.valueOf(typeObj);
                                    }else{
                                        type3 = (String) mediaHash3.get("type");
                                    }
                                    if (type3.equals("7")) {
                                        islistitemempty = false;
                                    }
                                    if (!islistitemempty) {
                                        listraobjdata = new RAdataobject(title2, url3, 2);
                                        listraobjdata.setType(type3);
                                        if (type3.equals("7")) {
                                            islistitemempty = true;
                                        }
                                        subraobjdata.getChildobjlist().add(listraobjdata);
                                        ArrayList<HashMap<String,Object>> childHash3 = (ArrayList<HashMap<String,Object>>) child2.get("children");
                                        if (childHash3.size() > 0) {
                                            for (int listItem = 0; listItem < childHash3.size(); listItem++) {
                                                HashMap<String,Object> child3 = childHash3.get(listItem);
                                                String title3 = (String) child3.get("title");
                                                HashMap<String, Object> mediaHash4 = (HashMap<String, Object>) child3.get("media");
                                                String url4 = (String) mediaHash4.get("url");
                                                String type4;
                                                Object typeObj1 = mediaHash4.get("type");
                                                if (typeObj1 instanceof Integer){
                                                    type4 = String.valueOf(typeObj1);
                                                }else{
                                                    type4 = (String) mediaHash4.get("type");
                                                }
                                                RAdataobject listItemobj = new RAdataobject(title3, url4, 3);
                                                listItemobj.setType(type4);
                                                listraobjdata.getChildobjlist().add(listItemobj);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    mainlistobj.add(mainraobjdata);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private boolean isDownloadManangerAvailable(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.GINGERBREAD){
            return true;
        }
        return false;
    }
    private class updateIJST extends AsyncTask<String,Integer,String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = new ProgressDialog(Elesson.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Checking for update..");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            try {

                URL urlToRequest = new URL(params[0]);
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setConnectTimeout(10000);
                urlConnection.setReadTimeout(25000);

                int statusCode = urlConnection.getResponseCode();
                if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

                } else if (statusCode != HttpURLConnection.HTTP_OK) {

                }

                InputStream in = urlConnection.getInputStream();

                //	InputStream in=getAssets().open("storeList-demo.json");

                String returnResponse = UserFunctions.convertStreamToString(in);
                if (returnResponse == null && returnResponse.equals("")) {
                    return null;
                }
                if (returnResponse.contains("\n")){
                    returnResponse = returnResponse.replace("\n","");
                }
                newVersion = returnResponse;


            } catch (MalformedURLException e) {

            } catch (SocketTimeoutException e) {

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            File file = new File(Environment.getExternalStorageDirectory()+"/IJST");
            if (file.exists()){
                File[] files = file.listFiles();
                if (files.length>0){
                    for (int i=0;i<files.length;i++){
                        if (files[i].isFile()){
                            files[i].delete();
                        }else{
                            UserFunctions.DeleteDirectory(files[i]);
                        }
                    }
                }
            }else{
                file.mkdir();
            }
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(Elesson.this);
            String oldVersion = pref.getString(Globals.IJSTversion,"");
            if (!newVersion.equals("") && !newVersion.equals(oldVersion)){
                String url = "http://www.nooor.com/ijst/"+newVersion;
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.setTitle(getString(R.string.elesson_update));
                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB){
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                }
                request.setDestinationInExternalPublicDir("/IJST",newVersion);
                long downloadId = manager.enqueue(request);
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Elesson.this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putLong(Globals.downloadID, downloadId);
                downloadID = downloadId;
                editor.commit();
             //   updateBtn.setEnabled(false);
            }
        }
    }
   private BroadcastReceiver onComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                replaceIJSTfiles();
                loadWebView(currentdataobj);
            }
        }
    };
    public Boolean unZipDownloadedFile(String zipfilepath, String unziplocation) {
        InputStream is;
        ZipInputStream zis;
        try {
            is = new FileInputStream(zipfilepath);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                String zipEntryName = ze.getName();
                //zipEntryName = zipEntryName.replace("\\", "/");
                ////System.out.println("zipEntryName: "+zipEntryName);
                File file = new File(unziplocation + "/" + zipEntryName);
                if (file.exists()) {
                    ////System.out.println("file exists at the given path:" +file);
                    byte[] buffer = new byte[1024];
                    FileOutputStream fout = new FileOutputStream(file);
                    BufferedOutputStream baos = new BufferedOutputStream(fout, 1024);
                    int count;
                    while ((count = zis.read(buffer, 0, 1024)) != -1) {
                        baos.write(buffer, 0, count);
                    }
                    baos.flush();
                    baos.close();
                } else {

                    if (ze.isDirectory()) {
                        ////System.out.println("Ze is an directory. It will create directory and exit");
                        file.mkdirs();
                        continue;
                    }

                    file.getParentFile().mkdirs();   //Horror line..............
                    byte[] buffer = new byte[1024];
                    FileOutputStream fout = new FileOutputStream(file);
                    BufferedOutputStream baos = new BufferedOutputStream(fout, 1024);
                    int count;
                    while ((count = zis.read(buffer, 0, 1024)) != -1) {
                        baos.write(buffer, 0, count);
                    }
                    baos.flush();
                    baos.close();
                }
            }
            zis.close();
            return true;
        } catch (Exception e) {
            ////System.out.println("unzipping error"+e);
            e.printStackTrace();
            return false;
        }

    }
    private void replaceIJSTfiles(){
        File ijstFile = new File(Environment.getExternalStorageDirectory() + "/IJST");
        if (ijstFile.exists()) {
            File[] files = ijstFile.listFiles();
            if (files.length>0) {
                String ijstFileName = files[0].getName();
                File ijstFile1 = new File(Environment.getExternalStorageDirectory() + "/IJST/" + ijstFileName);

                File newFile = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + "IJST");
                File[] existFiles = newFile.listFiles();
                if (existFiles.length > 0) {
                    for (int i = 0; i < existFiles.length; i++) {
                        if (existFiles[i].isFile()) {
                            existFiles[i].delete();
                        } else {
                            UserFunctions.DeleteDirectory(existFiles[i]);
                        }
                    }
                }
                boolean unZip = unZipDownloadedFile(ijstFile1.getAbsolutePath(), Globals.TARGET_BASE_BOOKS_DIR_PATH + "IJST");
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Elesson.this);
                SharedPreferences.Editor editor = preferences.edit();
                String fileName = ijstFile1.getName();
                editor.putString(Globals.IJSTversion, fileName);
                editor.putLong(Globals.downloadID,0L);
                editor.commit();
                ijstFile1.delete();
//                if (updateBtn!=null){
//                    updateBtn.setEnabled(true);
//                }
            }
        }
    }
    public void showStudyCardsDialogg(){
        //Declaring and defining the Dialog
        showStudyCardsDialog = new Dialog(this);
        showStudyCardsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        showStudyCardsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        showStudyCardsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.note_viewpager_layout, null));
        WindowManager.LayoutParams lp=showStudyCardsDialog.getWindow().getAttributes();
        //set transparency of background
        lp.dimAmount=1.0f;  // dimAmount between 0.0f and 1.0f, 1.0f is completely dark
        showStudyCardsDialog.getWindow().setAttributes(lp);
        showStudyCardsDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        if (Globals.isTablet()) {
            showStudyCardsDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.4));
        }else{
            showStudyCardsDialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        }
        noteViewPager = (ViewPager)showStudyCardsDialog.findViewById(R.id.viewPager);
        ViewPagerNoteAdapter viewPageradapter = new ViewPagerNoteAdapter();
        noteViewPager.setAdapter(viewPageradapter);
        noteViewPager.setOffscreenPageLimit(1);
        noteViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        showStudyCardsDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            }
        });
        showStudyCardsDialog.show();
    }
    public class ViewPagerNoteAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = null;
            switch (position){
                case 0:
                    view = inflater.inflate(R.layout.card_layout, null);
                    ArrayList<NoteData> noteDatas = db.loadNoteDivToArray(currentbook.get_bStoreID());
                    noteDataArrayList.clear();
                    noteDataArrayList = noteDatas;
                    mCardContainer = (CardContainer)view.findViewById(R.id.layoutview);
                    mCardContainer.cardPosition = 0;
                    tv_StudyCardsAlert=(TextView)view.findViewById(R.id.txt);
                    final SegmentedRadioButton segmentedRadioGroup = (SegmentedRadioButton) view.findViewById(R.id.segment_text);
                    segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (group == segmentedRadioGroup){
                                if(checkedId == R.id.currentPage){
                                    allPageClicked = false;
                                    loadStudyCards(allPageClicked);
                                }else if (checkedId == R.id.all_page){
                                    allPageClicked = true;
                                    loadStudyCards(allPageClicked);
                                }
                            }
                        }
                    });
                    initializeDialogLayouts(view);
                    StudyCardsAdapter(noteDatas);
                    updateButtons();
                    if (noteDataArrayList.size()>0) {
                        setBackgroudColorForButtons(0);
                    }
                    Button add_btn = (Button) view.findViewById(R.id.btnNext);
                    add_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            noteViewPager.setCurrentItem(1,true);
                        }
                    });
                    CircleButton back_btn = (CircleButton) view.findViewById(R.id.btn_back);
                    back_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showStudyCardsDialog.dismiss();
                        }
                    });

                    break;
                case 1:
                    view = inflater.inflate(R.layout.add_flashcard_layout, null);
                    CircleButton back_btn1 = (CircleButton) view.findViewById(R.id.btn_back);
                    back_btn1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            noteViewPager.setCurrentItem(0,true);
                            loadStudyCards(allPageClicked);
                        }
                    });
                    final RelativeLayout card_layout = (RelativeLayout) view.findViewById(R.id.card_container);
                    final EditText title_txt = (EditText) view.findViewById(R.id.title_edittext);
                    final EditText desc_txt = (EditText) view.findViewById(R.id.description_edittext);
                    Button turn_btn = (Button) view.findViewById(R.id.btn_turn);
                    turn_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FlipAnimation flipAnimation = new FlipAnimation(title_txt, desc_txt);

                            if (title_txt.getVisibility() == View.GONE)
                            {
                                flipAnimation.reverse();
                            }
                            card_layout.startAnimation(flipAnimation);
                        }
                    });
                    Button btn_add = (Button) view.findViewById(R.id.btnNext);
                    btn_add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (title_txt.getText().length()>0){
                                int colorValue = CC+1;
                                NoteData data = new NoteData();
                                data.setNoteColor(colorValue);
                                data.setNoteType(1);
                                data.setNoteTabNo(0);
                                data.setNotePageNo(currentdataobj.getIndexNo());
                                data.setNoteTitle(title_txt.getText().toString());
                                data.setNoteDesc(desc_txt.getText().toString());
                                noteDataArrayList.add(data);
                                String query = "insert into tblNote(BName,PageNo,SText,Occurence,SDesc,NPos,Color,ProcessSText,TabNo,Exported,NoteType) values" +
                                        "('"+currentbook.get_bStoreID()+"','"+currentdataobj.getIndexNo()+"','"+title_txt.getText().toString()+"','0','"+desc_txt.getText().toString()+"','0','"+colorValue+"','0','0','"+"0"+"','1')";
                                db.executeQuery(query);
                                title_txt.setText("");
                                desc_txt.setText("");
                            }
                        }
                    });
                    ImageView iv_nColor1 = (ImageView) view.findViewById(R.id.imgBtnC1);
                    iv_nColor1.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 0;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg1);
                        }
                    });
                    ImageView iv_nColor2 = (ImageView) view.findViewById(R.id.imgBtnC2);
                    iv_nColor2.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 1;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg2);
                        }
                    });
                    ImageView iv_nColor3 = (ImageView) view.findViewById(R.id.imgBtnC3);
                    iv_nColor3.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 2;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg3);
                        }
                    });
                    ImageView iv_nColor4 = (ImageView) view.findViewById(R.id.imgBtnC4);
                    iv_nColor4.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 3;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg4);
                        }
                    });
                    ImageView iv_nColor5 = (ImageView) view.findViewById(R.id.imgBtnC5);
                    iv_nColor5.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 4;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg5);
                        }
                    });
                    ImageView iv_nColor6 = (ImageView) view.findViewById(R.id.imgBtnC6);
                    iv_nColor6.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            CC = 5;
                            card_layout.setBackgroundResource(R.drawable.notepop_bg6);
                        }
                    });
                    break;
            }
            ((ViewPager) container).addView(view, 0);
            return view;
        }

//        @Override
//        public void setPrimaryItem(ViewGroup container, int position, Object object) {
//            if (position==0){
//                loadStudyCards(allPageClicked);
//            }
//        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }
    private void loadStudyCards(boolean allPage){
        if (allPage){
            noteDataArrayList.clear();
            tempNoteDataArrayList.clear();
            noteDataArrayList = db.loadNoteDivToArray(currentbook.get_bStoreID());
            for (int i=0;i<noteDataArrayList.size();i++){
                tempNoteDataArrayList.add(noteDataArrayList.get(i));
            }
            StudyCardsAdapter(noteDataArrayList);
            updateButtons();
            if (noteDataArrayList.size()>0) {
                setBackgroudColorForButtons(0);
            }
        }else{
            noteDataArrayList.clear();
            tempNoteDataArrayList.clear();
            noteDataArrayList = db.loadNotesInCurrentPage(currentbook.get_bStoreID(),currentdataobj.getIndexNo());
            for (int i=0;i<noteDataArrayList.size();i++){
                tempNoteDataArrayList.add(noteDataArrayList.get(i));
            }
            StudyCardsAdapter(noteDataArrayList);
            updateButtons();
            if (noteDataArrayList.size()>0) {
                setBackgroudColorForButtons(0);
            }
        }
    }
    private void loadAllNotesInCurrentPage(){
        ArrayList<NoteData> allnotesInCurrentPage = new ArrayList<>();
        for (int i=0;i<tempNoteDataArrayList.size();i++){
            NoteData data = tempNoteDataArrayList.get(i);
            if (allPageClicked){
                allnotesInCurrentPage.add(data);
            }else {
                if (data.getNotePageNo() == currentdataobj.getIndexNo()) {
                    allnotesInCurrentPage.add(data);
                }
            }
        }
        noteDataArrayList.clear();
        noteDataArrayList = allnotesInCurrentPage;
        mCardContainer.cardPosition = 0;
        StudyCardsAdapter(allnotesInCurrentPage);
        updateButtons();
        if (noteDataArrayList.size()>0) {
            setBackgroudColorForButtons(0);
        }
    }
    private void loadNotesBasedOnColors(int colorValue){
        ArrayList<NoteData> allNotesBasedOnColors = new ArrayList<>();
        for (int i=0;i<tempNoteDataArrayList.size();i++){
            NoteData data = tempNoteDataArrayList.get(i);
            if (data.getNoteColor() == colorValue) {
                allNotesBasedOnColors.add(data);
            }
        }
        noteDataArrayList.clear();
        noteDataArrayList = allNotesBasedOnColors;
        mCardContainer.cardPosition = 0;
        StudyCardsAdapter(allNotesBasedOnColors);
        updateButtons();
        if (noteDataArrayList.size()>0) {
            setBackgroudColorForButtons(0);
        }
    }
    private void loadNotesBasedOnType(int type){
        ArrayList<NoteData> allNotesBasedOnTpye = new ArrayList<>();
        for (int i=0;i<noteDataArrayList.size();i++){
            NoteData data = noteDataArrayList.get(i);
            if (data.getNoteType()==type){
                allNotesBasedOnTpye.add(data);
            }
        }
        noteDataArrayList.clear();
        noteDataArrayList = allNotesBasedOnTpye;
        mCardContainer.cardPosition = 0;
        StudyCardsAdapter(allNotesBasedOnTpye);
        updateButtons();
        if (noteDataArrayList.size()>0) {
            setBackgroudColorForButtons(0);
        }
    }
    private void updateButtons(){
        int notwellCount = 0,wellCount = 0,verywellCount = 0;
        for (int i=0;i<noteDataArrayList.size();i++){
            NoteData data = noteDataArrayList.get(i);
            if (data.getNoteType()==1){
                notwellCount++;
            }else if (data.getNoteType()==2){
                wellCount++;
            }else if (data.getNoteType()==3){
                verywellCount++;
            }
        }
        note_countbtn1.setText(String.valueOf(notwellCount));
        note_countbtn2.setText(String.valueOf(wellCount));
        note_countbtn3.setText(String.valueOf(verywellCount));
    }
    public void setBackgroudColorForButtons(int cardPosition){
        int type = 0;
        if (cardPosition < noteDataArrayList.size()) {
            NoteData data = noteDataArrayList.get(cardPosition);
            type = data.getNoteType();
            if (type == 1) {
                notwell_btn.setBackgroundResource(R.drawable.white_border_hovercolor);
                well_btn.setBackgroundResource(R.drawable.black_background);
                vrywell_btn.setBackgroundResource(R.drawable.black_background);
            } else if (type == 2) {
                notwell_btn.setBackgroundResource(R.drawable.black_background);
                well_btn.setBackgroundResource(R.drawable.white_border_hovercolor);
                vrywell_btn.setBackgroundResource(R.drawable.black_background);
            } else if (type == 3) {
                notwell_btn.setBackgroundResource(R.drawable.black_background);
                well_btn.setBackgroundResource(R.drawable.black_background);
                vrywell_btn.setBackgroundResource(R.drawable.white_border_hovercolor);
            }
        }
    }
    public void StudyCardsAdapter(ArrayList<NoteData> dataArrayList){
        //Calling the Simple Card class :
        if(dataArrayList.size()==0){
            tv_StudyCardsAlert.setVisibility(View.VISIBLE);
            tv_StudyCardsAlert.setText(getResources().getString(R.string.study_cards_alert));
        }else{
            tv_StudyCardsAlert.setVisibility(View.GONE);
        }
        adapter = new SimpleCardStackAdapter(getApplicationContext(), this, dataArrayList);
        android.content.res.Resources r = this.getResources();
        for(int tag =0; tag<dataArrayList.size(); tag++){
            NoteData noteData = dataArrayList.get(tag);
            int pageNo = noteData.getNotePageNo();
            int tabNo = noteData.getNoteTabNo();
            int color = noteData.getNoteColor();
            String finalPageNo;
            if(tabNo > 0){
                finalPageNo = ""+pageNo + "-" +tabNo;
            }
            else{
                finalPageNo = ""+pageNo;
            }
            if(pageNo <= 0){
                adapter.add(new CardModel("Cover page", noteData.getNoteTitle(), r.getDrawable(R.drawable.turn_bg)));

            }else{
                adapter.add(new CardModel("Page "+finalPageNo, noteData.getNoteTitle(), r.getDrawable(R.drawable.turn_bg)));
            }

        }

        mCardContainer.setAdapter(adapter);
    }
    public void studycardAdapter(){
        elessonNotecards = db.loadElessonNoteDivToArray(currentbook.get_bStoreID());
        String[] pageText = (String[]) elessonNotecards[0];
        android.content.res.Resources r = this.getResources();

        ElessonCardStackAdapter adapter = new ElessonCardStackAdapter(getApplicationContext(),this,elessonNotecards);

        for(int tag =0; tag<pageText.length; tag++){
            adapter.add(new CardModel("",pageText[tag],r.getDrawable(R.drawable.turn_bg)));
        }
        if(adapter.isEmpty()){
            tv_StudyCardsAlert.setVisibility(View.VISIBLE);
            tv_StudyCardsAlert.setText(getResources().getString(R.string.study_cards_alert));
        }
        mCardContainer.setAdapter(adapter);
    }
    private void initializeDialogLayouts(View dialog){
        count_btn = (Button) dialog.findViewById(R.id.btn_text);
        note_countbtn1 = (Button) dialog.findViewById(R.id.count_btn1);
        note_countbtn1.setOnClickListener(this);
        note_countbtn2 = (Button) dialog.findViewById(R.id.count_btn2);
        note_countbtn2.setOnClickListener(this);
        note_countbtn3 = (Button) dialog.findViewById(R.id.count_btn3);
        note_countbtn3.setOnClickListener(this);
        notwell_btn = (Button) dialog.findViewById(R.id.notwell_btn);
        notwell_btn.setOnClickListener(this);
        well_btn = (Button) dialog.findViewById(R.id.well_btn);
        well_btn.setOnClickListener(this);
        vrywell_btn = (Button) dialog.findViewById(R.id.verywell_btn);
        vrywell_btn.setOnClickListener(this);
        Button all_btn = (Button) dialog.findViewById(R.id.all_btn);
        all_btn.setOnClickListener(this);
        noteColor1 = (ImageView) dialog.findViewById(R.id.imgBtnC1);
        noteColor1.setOnClickListener(this);
        noteColor2 = (ImageView) dialog.findViewById(R.id.imgBtnC2);
        noteColor2.setOnClickListener(this);
        noteColor3 = (ImageView) dialog.findViewById(R.id.imgBtnC3);
        noteColor3.setOnClickListener(this);
        noteColor4 = (ImageView) dialog.findViewById(R.id.imgBtnC4);
        noteColor4.setOnClickListener(this);
        noteColor5 = (ImageView) dialog.findViewById(R.id.imgBtnC5);
        noteColor5.setOnClickListener(this);
        noteColor6 = (ImageView) dialog.findViewById(R.id.imgBtnC6);
        noteColor6.setOnClickListener(this);
    }
}
