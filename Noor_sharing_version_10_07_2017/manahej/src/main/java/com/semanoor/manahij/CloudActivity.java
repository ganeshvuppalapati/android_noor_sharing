package com.semanoor.manahij;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dragsortadapter.DragSortAdapter;
import com.dragsortadapter.NoForegroundShadowBuilder;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.ParentReference;
import com.google.api.services.drive.model.User;
import com.mobeta.android.dslv.DragSortController;
import com.mobeta.android.dslv.DragSortListView;
import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.TextCircularProgressBar;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class CloudActivity extends Activity implements DragSortListView.DropListener,View.OnClickListener,REST.ConnectCBs {

    public DragSortListView gridView;
    DragSortController controller;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 105;
    public NooorCloud cloud;
    public listAdapter adapter;
    public HorizontalAdapter horizontalAdapter;
    private int PICK_IMAGE = 555;
    private int PICK_VIDEO = 666;
    private int PICK_AUDIO = 777;
    private int CAPTURE_VIDEO = 888;
    private int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 999;
    private int GROUP_REQ = 1005;
    RelativeLayout add_files_layout,shelfRootViewLayout;
    Button btnAddBook,create_file,create_folder,create_category;
    private boolean createFolder = false;
    private boolean createFile = false;
    public ProgressDialog syncProgressDialog;
    ArrayList<File> filesList;
    private static final int REQ_CONNECT = 2;
    private static final int REQ_ACCPICK = 5;
    private boolean reLoad = true;
    public int totalScrolly = 0;
    private int fromPos;
    public GestureDetector gestureDetector;
    private int currentShelfPos;
    private ArrayList<Integer> editModeList = new ArrayList<>();
    String themesPath =  Globals.TARGET_BASE_FILE_PATH+"Themes/current/";
    private Uri imageFileUri;
    private String audio_outputFile;
    private MediaRecorder mRecorder;
    private Book book;
    private DatabaseHandler db;
    private int pageNumber;
    private  ProgressBar progressBar;
    private DriveFileUpload driveFileUpload;
    private Button drive_btn,edit_btn,filter_btn,done_btn,delete_btn,share_btn,duplicate_btn;
    private SegmentedRadioButton segmentedRadioGroup;
    private boolean myFiles = true;
    private boolean edit_mode;
    private RelativeLayout relativeLayout;
    private ArrayList<File> selectedList = new ArrayList<>();
    private ArrayList<File> sharedUserList = new ArrayList<>();
    private RelativeLayout rootView;
    private int seletedPosition = -1;
    private SwipeRefreshLayout swipeContainer;
    boolean showprogresssBar = true;
    private TextView title_txt;
    private FrameLayout add_shelf_layout;
    private RelativeLayout back_btn_layout;
    private ArrayList<Integer> expandList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cloud);
        book = (Book) getIntent().getSerializableExtra("Book");
        if (book!=null) {
            pageNumber = Integer.parseInt(getIntent().getStringExtra("PageNumber"));
        }
        cloud = NooorCloud.getInstance();
        cloud.setContext(CloudActivity.this);
        db = DatabaseHandler.getInstance(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        rootView = (RelativeLayout) findViewById(R.id.shelfRootViewLayout);
        UT.init(this);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(CloudActivity.this);
        String email = prefs.getString(Globals.sUserEmailIdKey, "");
        UT.AM.setEmail(email);
        if (!REST.init(this)) {

        }
        title_txt = (TextView) findViewById(R.id.txtNooorTitle);
        add_shelf_layout = (FrameLayout) findViewById(R.id.add_shelf_layout);
        back_btn_layout = (RelativeLayout) findViewById(R.id.imp_layout);
        if (Globals.isTablet()){
            title_txt.setVisibility(View.VISIBLE);
            back_btn_layout.setVisibility(View.VISIBLE);
        }else{
            title_txt.setVisibility(View.INVISIBLE);
            back_btn_layout.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) add_shelf_layout.getLayoutParams();
            layoutParams.setMargins(5,0,0,0);
            add_shelf_layout.setLayoutParams(layoutParams);
        }
        String audio_file = audio_outputFile = Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR + "SBA_AUDIO_CLOUD.m4a";
        if (new java.io.File(audio_file).exists()){
            new java.io.File(audio_file).delete();
        }
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContainer.setRefreshing(true);
                showprogresssBar = false;
                new getFilesFromGdrive().execute();
                new getSharedFilesFromGdrive().execute();
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        segmentedRadioGroup = (SegmentedRadioButton)findViewById(R.id.segment_text);
        segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group == segmentedRadioGroup){
                    if(checkedId == R.id.my_files){
                        myFiles = true;
                        if (cloud!=null){
                            cloud.getCategoryList().clear();
                        }
                        new getFilesFromGdrive().execute();
                    }else if (checkedId == R.id.shared_files){
                        myFiles = false;
                        if (cloud!=null){
                            cloud.getCategoryList().clear();
                        }
                        new getFilesFromGdrive().execute();
                    }
                }
            }
        });
        delete_btn = (Button) findViewById(R.id.delete_btn);
        delete_btn.setOnClickListener(this);
        share_btn = (Button) findViewById(R.id.share_btn);
        share_btn.setOnClickListener(this);
        duplicate_btn = (Button) findViewById(R.id.duplicate_btn);
        duplicate_btn.setOnClickListener(this);
        done_btn = (Button) findViewById(R.id.done_btn);
        done_btn.setOnClickListener(this);
        filter_btn = (Button) findViewById(R.id.filter_btn);
        filter_btn.setOnClickListener(this);
        relativeLayout = (RelativeLayout) findViewById(R.id.edit_share_delete_layout);
        edit_btn = (Button) findViewById(R.id.edit_btn);
        edit_btn.setOnClickListener(this);
        drive_btn = (Button) findViewById(R.id.drive_btn);
        drive_btn.setOnClickListener(this);
        Spinner spinAuthorTitle = (Spinner) findViewById(R.id.sort_btn);
        CustomAdapter<String> spinnerAdapter = new CustomAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, new String[] {"Last Modified",getResources().getString(R.string.name)});
        spinAuthorTitle.setAdapter(spinnerAdapter);
        spinAuthorTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View view,
                                       int position, long id) {
                if (position == 0) {
                    if (filesList!=null) {
                        new SortByNameAndLastModified(position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                } else {
                    if (filesList!=null) {
                        new SortByNameAndLastModified(position).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
        driveFileUpload = DriveFileUpload.getInstance();
        gestureDetector = new GestureDetector(CloudActivity.this,new GestureListener());
        shelfRootViewLayout = (RelativeLayout) findViewById(R.id.shelfRootViewLayout);
        btnAddBook = (Button) findViewById(R.id.add_shelf);
        btnAddBook.setOnClickListener(this);
        create_file = (Button) findViewById(R.id.create_file);
        create_file.setOnClickListener(this);
        create_folder = (Button) findViewById(R.id.create_folder);
        create_folder.setOnClickListener(this);
        CircleButton btnLibrary = (CircleButton) findViewById(R.id.btn_import);
        btnLibrary.setOnClickListener(this);
        create_category = (Button) findViewById(R.id.create_category);
        create_category.setOnClickListener(this);
        add_files_layout = (RelativeLayout) findViewById(R.id.toolBarLayout);
        gridView = (DragSortListView) findViewById(R.id.myListView);
        adapter = new listAdapter(CloudActivity.this);
        gridView.setDropListener(this);
        controller = new DragSortController(gridView);
        controller.setDragHandleId(R.id.drag_handle);
        controller.setRemoveEnabled(false);
        controller.setSortEnabled(true);
        controller.setDragInitMode(DragSortController.ON_DRAG);
        gridView.setFloatViewManager(controller);
        gridView.setOnTouchListener(controller);
        UserFunctions.applyingBackgroundImage(themesPath+"cloud_bg.png",shelfRootViewLayout,CloudActivity.this);
    }

    @Override
    public void drop(int from, int to) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_shelf:
                if (add_files_layout.getVisibility()==View.VISIBLE){
                    btnAddBook.setBackgroundResource(R.drawable.add_authoring);
                    Animation anim= AnimationUtils.loadAnimation(CloudActivity.this, R.anim.toolbar_anim_down);
                    add_files_layout.setAnimation(anim);
                    add_files_layout.setVisibility(View.GONE);
                }else{
                    btnAddBook.setBackgroundResource(R.drawable.add_authoring_click);
                    Animation anim= AnimationUtils.loadAnimation(CloudActivity.this, R.anim.toolbar_anim_up);
                    add_files_layout.setAnimation(anim);
                    add_files_layout.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.create_file:
                createFile = true;
                addFilesDialog();
                break;
            case R.id.create_folder:
                createFile = false;
                createFolder=true;
                showDialog(null);
                break;
            case R.id.create_category:
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(CloudActivity.this);
                new createFolderorFile("root","New Folder","application/vnd.google-apps.folder",true).execute();
             //   cloud.getFilesFromGdrive();
                break;
            case R.id.btn_import:
                onBackPressed();
                break;
            case R.id.drive_btn:
                Intent updateIntent = new Intent(Intent.ACTION_VIEW,Uri.parse("https://drive.google.com/drive/u/0/my-drive"));
                startActivity(updateIntent);
                break;
            case R.id.edit_btn:
                edit();
                break;
            case R.id.filter_btn:
                showPopover(v);
                break;
            case R.id.done_btn:
                edit();
                break;
            case R.id.delete_btn:
                if (selectedList.size()>0) {
                    new deleteFile(null, false, selectedList,-1).execute();
                }else{
                    UserFunctions.alert(getResources().getString(R.string.select_file),CloudActivity.this);
                }
                break;
            case R.id.share_btn:
                if (selectedList.size()>0) {
                    reLoad = false;
                    Intent intent = new Intent(CloudActivity.this, ExportEnrGroupActivity.class);
                    intent.putExtra("fromCloud", true);
                    startActivityForResult(intent, GROUP_REQ);
                }else{
                    UserFunctions.alert(getResources().getString(R.string.select_file),CloudActivity.this);
                }
                break;
            case R.id.duplicate_btn:
                if (selectedList.size()>0) {
                    showListDialog();
                }else{
                    UserFunctions.alert(getResources().getString(R.string.select_file),CloudActivity.this);
                }
                break;
        }
    }

    @Override
    public void onConnFail(Exception ex) {
        if (ex instanceof UserRecoverableAuthIOException) {                        UT.lg("connFail - has res");
            startActivityForResult((((UserRecoverableAuthIOException) ex).getIntent()), REQ_CONNECT);
        }
    }

    @Override
    public void onConnOK() {
        new getFilesFromGdrive().execute();
        new getSharedFilesFromGdrive().execute();
    }



    public class listAdapter extends BaseAdapter {

        public listAdapter(Context context) {

        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        public class ViewHolder {
            public TextView txtTitle;
            public Button btnDelCategory;
            public Button btnPropertyAccord;
            public ImageView btnArrangeCategory;
            public ImageView bgRackImageView;
            public int position;
            public FrameLayout btnFrameDelCategory;
            public FrameLayout btnFramePropAccord;
            public RelativeLayout drag_layout;
            public RelativeLayout shelf_layout;
            public RecyclerView recyclerView;
            public ProgressBar progressBar;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return cloud.getCategoryList().size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;

            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.cloud_shelf_layout, null);
                Typeface font = Typeface.createFromAsset(getAssets(), "fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent, font);
                holder = new ViewHolder();
                holder.btnFramePropAccord = (FrameLayout) view.findViewById(R.id.frameButton1);
                holder.btnFrameDelCategory = (FrameLayout) view.findViewById(R.id.frameButton2);
                holder.progressBar = (ProgressBar) view.findViewById(R.id.progressBar2);
                //  holder.drag_layout = (FrameLayout) view.findViewById(R.id.frameButton3);
                holder.shelf_layout = (RelativeLayout) view.findViewById(R.id.shelf_layout);
                //    holder.scrollHideBtn = (Button) view.findViewById(R.id.drag_handle_img);
                holder.drag_layout = (RelativeLayout) view.findViewById(R.id.drag_handle);
                holder.txtTitle = (TextView) view.findViewById(R.id.text);
                holder.recyclerView = (RecyclerView) view.findViewById(R.id.scrollView1);
                holder.btnDelCategory = (Button) view.findViewById(R.id.button2);
                holder.btnPropertyAccord = (Button) view.findViewById(R.id.button1);
                holder.bgRackImageView = (ImageView) view.findViewById(R.id.bg_rack_image);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            if (holder.recyclerView.getTag()==null){
                holder.recyclerView.setTag(position);
            }
            if (position==cloud.getCategoryList().size()-1){
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.shelf_layout.getLayoutParams();
                layoutParams.setMargins(0,0,0,0);
            }
            final File metadata = cloud.getCategoryList().get(position);
            holder.txtTitle.setText(metadata.getTitle());
            holder.txtTitle.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    currentShelfPos = position;
                    return gestureDetector.onTouchEvent(event);
                }
            });
            if (isViewExpanded(position)){
                holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_collapsed_new);
            }else{
                holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_expanded_new);
            }
            holder.btnFrameDelCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cloud.getCategoryList().size()>1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(CloudActivity.this);
                        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (position!=0) {
                                    new deleteFile(metadata, true, null,position).execute();
                                }else{
                                    UserFunctions.alert(getResources().getString(R.string.cannot_be_deleted),CloudActivity.this);
                                }
                                dialog.cancel();
                            }
                        });
                        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.setTitle(R.string.delete);
                        builder.setMessage(R.string.suredelete);
                        builder.show();
                    }
                }
            });
            holder.btnDelCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cloud.getCategoryList().size()>1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(CloudActivity.this);
                        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (position!=0) {
                                    new deleteFile(metadata, true, null,position).execute();
                                }else{
                                    UserFunctions.alert(getResources().getString(R.string.cannot_be_deleted),CloudActivity.this);
                                }
                                dialog.cancel();
                            }
                        });
                        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.setTitle(R.string.delete);
                        builder.setMessage(R.string.suredelete);
                        builder.show();
                    }
                }
            });
            holder.btnFramePropAccord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<Integer, Object> hashMap = cloud.getCategoryFilesMap().get(position);
                    ArrayList<File> fileArrayList = (ArrayList<File>) hashMap.get(position);
                    if (fileArrayList.size()>3){
                        if (isViewExpanded(position)){
                            for (int i = 0; i < expandList.size(); i++) {
                                if (position == expandList.get(i)) {
                                    expandList.remove(i);
                                    holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_expanded_new);
                                    break;
                                }
                            }
                        }else{
                            holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_collapsed_new);
                            expandList.add(position);
                        }
                        gridView.invalidateViews();
                    }
                }
            });
            holder.btnPropertyAccord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<Integer, Object> hashMap = cloud.getCategoryFilesMap().get(position);
                    ArrayList<File> fileArrayList = (ArrayList<File>) hashMap.get(position);
                    if (fileArrayList.size()>3){
                        if (isViewExpanded(position)){
                            for (int i = 0; i < expandList.size(); i++) {
                                if (position == expandList.get(i)) {
                                    expandList.remove(i);
                                    holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_expanded_new);
                                    break;
                                }
                            }
                        }else{
                            holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_collapsed_new);
                            expandList.add(position);
                        }
                        gridView.invalidateViews();
                    }
                }
            });
            if (position < cloud.getCategoryFilesMap().size()) {
                HashMap<Integer, Object> hashMap = cloud.getCategoryFilesMap().get(position);
                ArrayList<File> fileArrayList = (ArrayList<File>) hashMap.get(position);
                HorizontalAdapter adapter;
                if (fileArrayList != null) {
                    if (checkPrevFolderExist(position)) {
                        adapter = new HorizontalAdapter(fileArrayList, fileArrayList.size() + 1, holder.recyclerView);
                    } else {
                        adapter = new HorizontalAdapter(fileArrayList, fileArrayList.size(), holder.recyclerView);
                    }

                } else {
                    adapter = new HorizontalAdapter(new ArrayList<File>(), 0, holder.recyclerView);
                }
                if (isViewExpanded(position)) {
                    ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
                    gridlayoutParams.height = (int) getResources().getDimension(R.dimen.shelf_height) + (int) getResources().getDimension(R.dimen.shelf_height);
                    holder.recyclerView.setLayoutParams(gridlayoutParams);
                    GridLayoutManager gridLayout = new GridLayoutManager(CloudActivity.this, getResources().getInteger(R.integer.recycler_grid_column), GridLayoutManager.VERTICAL, false);
                    holder.recyclerView.setLayoutManager(gridLayout);
                } else {
                    LinearLayoutManager horizontalLayoutManagaer
                            = new LinearLayoutManager(CloudActivity.this, LinearLayoutManager.HORIZONTAL, false);
                    ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
                    gridlayoutParams.height = (int) getResources().getDimension(R.dimen.shelf_height);
                    holder.recyclerView.setLayoutParams(gridlayoutParams);
                    holder.recyclerView.setNestedScrollingEnabled(false);
                    holder.recyclerView.setLayoutManager(horizontalLayoutManagaer);
                    holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
                }
                holder.recyclerView.setAdapter(adapter);
            }
            return view;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }

    private boolean isViewExpanded(int position) {
        boolean expand = false;
        for (int i = 0; i < expandList.size(); i++) {
            if (position == expandList.get(i)) {
                expand = true;
                break;
            }
        }
        return expand;
    }

    public class HorizontalAdapter extends DragSortAdapter<HorizontalAdapter.MyViewHolder> {

        ArrayList<File> fileList;
        RecyclerView mRecyclerView;
        int size;

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        public int getPositionForId(long id) {
            return fileList.indexOf((int) id);
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public boolean move(int fromPosition, int toPosition) {
           if (fromPos != toPosition && toPosition != -1) {
               if (checkPrevFolderExist((Integer) mRecyclerView.getTag())){
                   fromPos=fromPos-1;
                   File sourceFile = fileList.get(fromPos);
                   if (sourceFile.getId()!=null) {
                       if (toPosition == 0) {
                           new getPreviousParentFile((Integer) mRecyclerView.getTag(), sourceFile).execute();
                       } else {
                           toPosition = toPosition - 1;
                           File targetFile = fileList.get(toPosition);
                           if (targetFile.getId() != null) {
                               if (targetFile.getMimeType().equals("application/vnd.google-apps.folder")) {
                                   new movingTask((Integer) mRecyclerView.getTag(), sourceFile, targetFile).execute();
                               } else {
                                   groupingFiles((Integer) mRecyclerView.getTag(), sourceFile, targetFile);
                               }
                           }
                       }
                   }
               }else {
                   File sourceFile = fileList.get(fromPos);
                   File targetFile = fileList.get(toPosition);
                   if (targetFile.getId()!=null && sourceFile.getId()!=null) {
                       if (targetFile.getMimeType().equals("application/vnd.google-apps.folder")) {
                           new movingTask((Integer) mRecyclerView.getTag(), sourceFile, targetFile).execute();
                       } else {
                           groupingFiles((Integer) mRecyclerView.getTag(), sourceFile, targetFile);
                       }
                   }
               }
           }
            gridView.invalidateViews();
            return false;
        }

        public class MyViewHolder extends DragSortAdapter.ViewHolder{
            public TextView txtView;
            public ImageView imgView;
            public CheckBox checkBox;
            public TextCircularProgressBar textCircularProgressBar;

            public MyViewHolder(DragSortAdapter adapter, View itemView) {
                super(adapter,itemView);
                txtView = (TextView) itemView.findViewById(R.id.textView1);
                imgView = (ImageView) itemView.findViewById(R.id.imageView2);
                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox1);
                textCircularProgressBar = (TextCircularProgressBar) itemView.findViewById(R.id.circular_progress);
            }


            @Override public View.DragShadowBuilder getShadowBuilder(View itemView, Point touchPoint) {
                return new NoForegroundShadowBuilder(itemView, touchPoint);
            }
        }


        public HorizontalAdapter(ArrayList<File> fileArrayList,int count, RecyclerView rView) {
            super(rView);
            this.fileList = fileArrayList;
            this.size = count;
            mRecyclerView = rView;
        }



        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cloud_file, parent, false);
            if (itemView.getTag()==null){
                itemView.setTag(parent.getTag());
            }
            MyViewHolder holder = new MyViewHolder(this,itemView);
            return holder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            File file = null;
            View view = holder.itemView;
            final int shelfPos = (Integer) view.getTag();
            if (holder.getAdapterPosition() == 0) {
                if (checkPrevFolderExist(shelfPos)) {
                    holder.imgView.setImageResource(R.drawable.cloud_back);
                    holder.txtView.setText("");
                } else {
                    file = fileList.get(holder.getAdapterPosition());
                    if (file.getId() != null && !file.getId().equals("")) {
                        holder.textCircularProgressBar.setVisibility(View.INVISIBLE);
                        holder.txtView.setText(file.getTitle());
                        if (file.getMimeType().equals("application/vnd.google-apps.folder")) {
                            holder.imgView.setImageResource(R.drawable.folder_cloud);
                        } else {
                            if (file.getMimeType().contains("image")) {
                                Glide.with(CloudActivity.this).load(file.getThumbnailLink())
                                        .thumbnail(0.5f)
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .override(200, 200)
                                        .into(holder.imgView);
                            } else if (file.getMimeType().contains("video")) {
                                if (file.getId() == null) {
                                    String path = file.getWebContentLink();
                                    Bitmap videoThumbnail = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
                                    holder.imgView.setImageBitmap(videoThumbnail);
                                } else {
                                    Glide.with(CloudActivity.this).load(file.getThumbnailLink())
                                            .thumbnail(0.5f)
                                            .crossFade()
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .override(200, 200)
                                            .into(holder.imgView);
                                }
                            } else if (file.getMimeType().contains("audio")) {
                                holder.imgView.setImageResource(R.drawable.cloud_audio);
                            } else {
                                if (file.getMimeType().equals("application/pdf")){
                                    holder.imgView.setImageResource(R.drawable.cloud_pdfs);
                                }else if (file.getMimeType().equals("application/vnd.google-apps.presentation")){
                                    holder.imgView.setImageResource(R.drawable.gpresentation);
                                }else if (file.getMimeType().equals("application/vnd.google-apps.spreadsheet")){
                                    holder.imgView.setImageResource(R.drawable.gspreadsheet);
                                }else if (file.getMimeType().equals("application/vnd.google-apps.document")){
                                    holder.imgView.setImageResource(R.drawable.cloud_doc);
                                }else if (file.getMimeType().equals("text/plain")){
                                    holder.imgView.setImageResource(R.drawable.gtxt);
                                }else if (file.getTitle().contains(".zip")){
                                    holder.imgView.setImageResource(R.drawable.zip);
                                }else{
                                    holder.imgView.setImageResource(R.drawable.unkown);
                                }
                            }
                        }
                    }else{
                        holder.imgView.setImageResource(R.drawable.unkown);
                        holder.txtView.setText("");
                        holder.textCircularProgressBar.setMax(100);
                        holder.textCircularProgressBar.getCircularProgressBar().setCircleWidth(20);
                        holder.textCircularProgressBar.setVisibility(View.VISIBLE);
                        driveFileUpload.setContext(CloudActivity.this);
                        driveFileUpload.setTextCircularProgressBar(holder.textCircularProgressBar);
                    }
                }
            } else {
                if (checkPrevFolderExist(shelfPos)) {
                    file = fileList.get(holder.getAdapterPosition() - 1);
                } else {
                    file = fileList.get(holder.getAdapterPosition());
                }
                if (file.getId()!=null && !file.getId().equals("")) {
                    holder.txtView.setText(file.getTitle());
                    if (file.getMimeType().equals("application/vnd.google-apps.folder")) {
                        holder.imgView.setImageResource(R.drawable.folder_cloud);
                    } else {
                        if (file.getMimeType().contains("image")) {
                            Glide.with(CloudActivity.this).load(file.getThumbnailLink())
                                    .thumbnail(0.5f)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .override(200, 200)
                                    .into(holder.imgView);
                        } else if (file.getMimeType().contains("video")) {
                            if (file.getId() == null) {
                                String path = file.getWebContentLink();
                                Bitmap videoThumbnail = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
                                holder.imgView.setImageBitmap(videoThumbnail);
                            } else {
                                Glide.with(CloudActivity.this).load(file.getThumbnailLink())
                                        .thumbnail(0.5f)
                                        .crossFade()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .override(200, 200)
                                        .into(holder.imgView);
                            }
                        } else if (file.getMimeType().contains("audio")) {
                            holder.imgView.setImageResource(R.drawable.default_audio_stop);
                        } else {
                            if (file.getMimeType().equals("application/pdf")){
                                holder.imgView.setImageResource(R.drawable.cloud_pdfs);
                            }else if (file.getMimeType().equals("application/vnd.google-apps.presentation")){
                                holder.imgView.setImageResource(R.drawable.gpresentation);
                            }else if (file.getMimeType().equals("application/vnd.google-apps.spreadsheet")){
                                holder.imgView.setImageResource(R.drawable.gspreadsheet);
                            }else if (file.getMimeType().equals("application/vnd.google-apps.document")){
                                holder.imgView.setImageResource(R.drawable.cloud_doc);
                            }else if (file.getMimeType().equals("text/plain")){
                                holder.imgView.setImageResource(R.drawable.gtxt);
                            }else if (file.getTitle().contains(".zip")){
                                holder.imgView.setImageResource(R.drawable.zip);
                            }else{
                                holder.imgView.setImageResource(R.drawable.unkown);
                            }
                        }
                    }
                }else{
                    holder.imgView.setImageResource(R.drawable.unknown);
                    holder.txtView.setText("");
                    holder.textCircularProgressBar.setMax(100);
                    holder.textCircularProgressBar.getCircularProgressBar().setCircleWidth(20);
                    holder.textCircularProgressBar.setVisibility(View.VISIBLE);
                    driveFileUpload.setContext(CloudActivity.this);
                    driveFileUpload.setTextCircularProgressBar(holder.textCircularProgressBar);
                }
            }

            if (edit_mode){
                if (file!=null) {
                    if (file.getId() != null && !file.getId().equals("")) {
                        holder.checkBox.setVisibility(View.VISIBLE);
                    }
                }
            }else{
                holder.checkBox.setVisibility(View.INVISIBLE);
            }

            final File finalFile = file;

        /*  holder.editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalFile.getId()!=null) {
                        int pos;
                        if (checkPrevFolderExist(shelfPos)) {
                            pos = position - 1;
                        } else {
                            pos = position;
                        }
                        editDialog(shelfPos, pos, false);
                    }
                }
            });*/

            holder.imgView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if ((checkPrevFolderExist((Integer) mRecyclerView.getTag()))) {
                        if (holder.getAdapterPosition() != 0) {
                            Globals.shelfPosition = shelfPos;
                            fromPos = holder.getAdapterPosition();
                            holder.startDrag();
                        }
                    } else {
                        Globals.shelfPosition = shelfPos;
                        fromPos = holder.getAdapterPosition();
                        holder.startDrag();
                    }
                    return true;
                }
            });

            holder.imgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    View view = holder.itemView;
                    final int shelfPos = (Integer) view.getTag();
                    if (edit_mode) {
                        if (finalFile!=null && !finalFile.getId().equals("")) {
                            if (holder.checkBox.isChecked()) {
                                holder.checkBox.setChecked(false);
                                for (int i = 0; i < selectedList.size(); i++) {
                                    File selectedFile = selectedList.get(i);
                                    if (finalFile.getId().equals(selectedFile.getId())) {
                                        selectedList.remove(i);
                                        break;
                                    }
                                }
                            } else {
                                holder.checkBox.setChecked(true);
                                selectedList.add(finalFile);
                            }
                        }
                    } else {
                        if (holder.getAdapterPosition() == 0 && checkPrevFolderExist(shelfPos)) {
                            new backClickedTask(shelfPos, mRecyclerView).execute();
                        } else {
                            if (finalFile.getId()!=null) {
                                if (finalFile.getMimeType().equals("application/vnd.google-apps.folder")) {
                                    File metadata;
                                    if (checkPrevFolderExist(shelfPos)) {
                                        metadata = fileList.get(holder.getAdapterPosition() - 1);
                                    } else {
                                        metadata = fileList.get(holder.getAdapterPosition());
                                    }
                                    new folderClickedTask(metadata, shelfPos, mRecyclerView).execute();
                                } else {
                                    if (finalFile.getMimeType().contains("video")) {
                                        //   showVideoDialog(finalFile);
                                        String url = "https://drive.google.com/file/d/" + finalFile.getId() + "/preview";
                                        new insertPermissionAndLoadVideo(finalFile).execute();
                                    } else if (finalFile.getMimeType().contains("image")) {
                                        new insertPermissionAndLoadVideo(finalFile).execute();
                                    } else if (finalFile.getMimeType().contains("audio")) {
                                        new insertPermissionAndLoadVideo(finalFile).execute();
                                    } else {
                                        if (isSupportedFile(finalFile.getMimeType()) && finalFile.getAlternateLink()!=null){
                                            Intent updateIntent = new Intent(Intent.ACTION_VIEW,Uri.parse(finalFile.getAlternateLink()));
                                            startActivity(updateIntent);
                                           // new DownloadFile(finalFile.getDownloadUrl(),finalFile.getId()).execute();
                                        }else{
                                            UserFunctions.alert(getResources().getString(R.string.unsupported_file),CloudActivity.this);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return size;
        }
    }

  /*  @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addScope(Drive.SCOPE_APPFOLDER) // required for App Folder sample
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        mGoogleApiClient.connect();
    }*/

    /**
     * Handles resolution callbacks.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CONNECT && resultCode == RESULT_OK){
            REST.connect();
        }else if (requestCode == REQ_ACCPICK && resultCode == RESULT_OK){
            if (data != null && data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME) != null) {
                UT.AM.setEmail(data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME));
            }
        }
        else if (requestCode == CAPTURE_VIDEO && resultCode == RESULT_OK){
            if (data!=null){
                ArrayList<Uri> uriList = new ArrayList<>();
                createFile = true;
                Uri selectedUri = data.getData();
                uriList.add(selectedUri);
                showDialogInUIthread(uriList);
            }
        }else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
       //     if (data != null){
                ArrayList<Uri> uriList = new ArrayList<>();
                createFile = true;
              //  Uri selectedUri = data.getData();
                uriList.add(imageFileUri);
            showDialogInUIthread(uriList);
     //   }
        }
        else if (requestCode == PICK_IMAGE && resultCode == RESULT_OK){
            if (data!=null) {
                ArrayList<Uri> uriList = new ArrayList<>();
                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.JELLY_BEAN){
                    if(data.getClipData()!=null) {
                        createFile = true;
                        ClipData mclipData = data.getClipData();
                        for (int i = 0; i < mclipData.getItemCount(); i++) {
                            ClipData.Item item = mclipData.getItemAt(i);
                            Uri uri = item.getUri();
                            //selectedGalleryImageUri = uri;
                            String pickedSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, uri);
                            if (Build.VERSION.SDK_INT >= 23) {
                                requestReadExternalStoragePermission(uriList,uri);
                            } else {
                                uriList.add(uri);
                            }
                        }
                        showDialogInUIthread(uriList);
                    }else if(data.getData()!=null){
                        createFile = true;
                        Uri selectedImage = data.getData();
                        uriList.add(selectedImage);
                        showDialogInUIthread(uriList);
                    }
                }else {
                    createFile = true;
                    Uri selectedImage = data.getData();
                    uriList.add(selectedImage);
                    showDialogInUIthread(uriList);
                }
            }
        }else if (requestCode == PICK_VIDEO && resultCode == RESULT_OK){
            Uri selectedVideo = data.getData();
            ArrayList<Uri> uriList = new ArrayList<>();
            uriList.add(selectedVideo);
            showDialogInUIthread(uriList);
        }else if (requestCode == PICK_AUDIO && resultCode == RESULT_OK){
            Uri selectedAudio = data.getData();
            ArrayList<Uri> uriList = new ArrayList<>();
            uriList.add(selectedAudio);
            showDialogInUIthread(uriList);
        }else if (requestCode == GROUP_REQ && resultCode == RESULT_OK){
            edit();
            ArrayList<String> selectedContact = (ArrayList<String>) data.getSerializableExtra("contactList");
            new insertPermission(selectedContact).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private class insertPermission extends AsyncTask<Void,Void,Void>{
        ArrayList<String> contacts;

        public insertPermission(ArrayList<String> contactList){
            this.contacts = contactList;
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i1=0;i1<selectedList.size();i1++){
                File file = selectedList.get(i1);
                String fileId = file.getId();
                for (int i=0;i<contacts.size();i++){
                    REST.insertPermission(fileId,contacts.get(i),"user","reader");
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            reLoad = false;
            selectedList.clear();
            UserFunctions.alert(getResources().getString(R.string.share_success),CloudActivity.this);
        }
    }
//
//    /**
//     * Called when activity gets invisible. Connection to Drive service needs to
//     * be disconnected as soon as an activity is invisible.
//     */
//    @Override
//    protected void onPause() {
//        if (mGoogleApiClient != null) {
//            mGoogleApiClient.disconnect();
//        }
//        super.onPause();
//    }
//    public GoogleApiClient getGoogleApiClient() {
//        return mGoogleApiClient;
//    }
    private class getFilesFromGdrive extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (showprogresssBar) {
                progressBar.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.INVISIBLE);
            }else{
                showprogresssBar = true;
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            getFileList();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressBar!=null){
                progressBar.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);
                gridView.setAdapter(adapter);
            }
        }
    }

    public String getMimeType(Uri uri){
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }
    public void showDialog(final ArrayList<Uri> uriList){
        final Dialog dialog = new Dialog(CloudActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloud_list_layout);
        dialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.5));

        CircleButton button = (CircleButton) dialog.findViewById(R.id.btn_import);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ListView listView = (ListView) dialog.findViewById(R.id.listView1);
        listCategory category = new listCategory(CloudActivity.this);
        listView.setAdapter(category);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
                dialog.dismiss();
                final File data = cloud.getCategoryList().get(position+1);
                if (createFile) {
                    reLoad=false;
                    for (int i=0;i<uriList.size();i++) {
                        Uri uri = uriList.get(i);
                        String path = UserFunctions.getPathFromGalleryForPickedItem(CloudActivity.this,uri);
                        java.io.File file = new java.io.File(path);
                        DriveFile driveFile = new DriveFile();
                        driveFile.setParentId(data.getId());
                        driveFile.setTitle(file.getName());
                        driveFile.setMimeType(getMimeType(uri));
                        driveFile.setUploadFile(file);
                        driveFileUpload.getUploadingFilesList().add(driveFile);
                        if (!driveFileUpload.isUploadTakRunning()){
                            driveFileUpload.new UploadFile().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            driveFileUpload.setUploadTakRunning(true);
                        }
                        HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(position+1);
                        ArrayList<File> subList = (ArrayList<File>) hashMap.get(position+1);
                        File newFile = new File();
                        newFile.setTitle(file.getName());
                        newFile.setMimeType(getMimeType(uri));
                        subList.add(newFile);
                        gridView.invalidateViews();
                     //   new createFolderorFile(data.getId(),file.getName(),getMimeType(uri),file,false,position+1).execute();
                    }
                    createFile = false;
                }else{
                    final Dialog groupDialog = new Dialog(CloudActivity.this);
                    groupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
                    groupDialog.setTitle(R.string.folder_name);
                    groupDialog.setContentView(getLayoutInflater().inflate(R.layout.group_edit, null));
                    groupDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.5), RelativeLayout.LayoutParams.WRAP_CONTENT);
                    final EditText editText = (EditText) groupDialog.findViewById(R.id.enr_editText);
                    Button btnSave = (Button) groupDialog.findViewById(R.id.enr_save);
                    btnSave.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (editText.getText().toString().length()>0){
//                                cloud.createFolder(data.getDriveId().encodeToString(),editText.getText().toString());
                                new createFolderorFile(data.getId(),editText.getText().toString(),"application/vnd.google-apps.folder",null,false,position+1).execute();
                                groupDialog.dismiss();
                            }
                        }
                    });
                    Button btnCancel = (Button) groupDialog.findViewById(R.id.enr_cancel);
                    btnCancel.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            groupDialog.dismiss();
                        }
                    });
                    groupDialog.show();
                }
            }
        });
        TextView txtNoCategories = (TextView) dialog.findViewById(R.id.textView1);
        txtNoCategories.setVisibility(View.GONE);
        dialog.show();
    }
    public class listCategory extends BaseAdapter {
        Context ctx;
        ArrayList<File> categoryList = new ArrayList<>();

        public listCategory(Context context) {
            this.ctx=context;
            for (int i=0;i<cloud.getCategoryList().size();i++){
                File file = cloud.getCategoryList().get(i);
                if (i!=0){
                    categoryList.add(file);
                }
            }
        }

        public class ViewHolder {
            public TextView txtTitle;

        }

        @Override
        public int getCount() {
            return cloud.getCategoryList().size()-1;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;

            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.inflate_text_view, null);
                Typeface font = Typeface.createFromAsset(getAssets(), "fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent, font);
                holder = new ViewHolder();
                holder.txtTitle = (TextView)view.findViewById(R.id.textView1);
                holder.txtTitle.setTextColor(Color.BLACK);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            File metadata = categoryList.get(position);
            holder.txtTitle.setText(metadata.getTitle());
            return view;
        }
    }
    public void requestReadExternalStoragePermission(ArrayList<Uri> uriList,Uri uri) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            /*if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

			} else {*/
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            //}
        } else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            uriList.add(uri);
        }
    }

    private boolean checkPrevFolderExist(int shelfPos){
        if (cloud.getPrevDriveContent().size()>0) {
            for (int i = 0; i < cloud.getPrevDriveContent().size(); i++) {
                HashMap<Integer, Object> prevMap = cloud.getPrevDriveContent().get(i);
                ArrayList<HashMap<Integer, Object>> prevList = (ArrayList<HashMap<Integer, Object>>) prevMap.get(shelfPos);
                if (prevList != null && prevList.size()>0) {
                    return true;
                }
            }
        }
        return false;
    }
    public void backClicked(final int shelfPos, final RecyclerView recyclerView){
        if (cloud.getPrevDriveContent().size()>0) {
            for (int i = 0; i < cloud.getPrevDriveContent().size(); i++) {
                HashMap<Integer, Object> prevMap = cloud.getPrevDriveContent().get(i);
                final ArrayList<HashMap<Integer, Object>> prevList = (ArrayList<HashMap<Integer, Object>>) prevMap.get(shelfPos);
                if (prevList != null&&prevList.size()>0) {
                    HashMap<Integer,Object> dataObj = prevList.get(prevList.size()-1);
                    ArrayList<File> metadataList = (ArrayList<File>) dataObj.get(shelfPos);
                    prevList.remove(prevList.size()-1);
                    cloud.getCategoryList().remove(shelfPos);
                    HorizontalAdapter adapter;
//                    if (checkPrevFolderExist(shelfPos)){
//                        adapter = new HorizontalAdapter(metadataList,metadataList.size()+1,this,recyclerView);
//                    }else{
//                        adapter = new HorizontalAdapter(metadataList,metadataList.size(),this,recyclerView);
//                    }
                    LinearLayoutManager horizontalLayoutManagaer
                            = new LinearLayoutManager(CloudActivity.this, LinearLayoutManager.HORIZONTAL, false);
                 //   horizontalLayoutManagaer.setStackFromEnd(true);
                    // vertical_recycler_view.setLayoutManager(horizontalLayoutManagaer);
                    recyclerView.setLayoutManager(horizontalLayoutManagaer);
                  //  recyclerView.setAdapter(adapter);
                    break;
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void addFilesDialog(){
        final Dialog dialog = new Dialog(CloudActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_files);
        dialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.2), (int)(Globals.getDeviceHeight()/1.5));
        CircleButton button = (CircleButton) dialog.findViewById(R.id.btn_import);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button img_btn = (Button) dialog.findViewById(R.id.image_file);
        img_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reLoad=false;
                dialog.dismiss();
                MediaPickerDialog("image");
            }
        });
        Button video_btn = (Button) dialog.findViewById(R.id.video_file);
        video_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reLoad=false;
                dialog.dismiss();
                MediaPickerDialog("video");
            }
        });
        Button audio_btn = (Button) dialog.findViewById(R.id.audio_file);
        audio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reLoad=false;
                MediaPickerDialog("audio");
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (cloud.getPrevDriveContent()!=null) {
            cloud.getPrevDriveContent().clear();
        }
        if (book!=null) {
            setResult(RESULT_OK, getIntent().putExtra("Book", book));
            setResult(RESULT_OK, getIntent().putExtra("PageNumber", pageNumber));
        }
        finish();
    }
    private void getFileList(){
        ArrayList<File> rootFolderList = new ArrayList<>();
        filesList = new ArrayList<>();
        filesList = REST.search("root", null, null,myFiles);
        if (filesList.size() > 0) {
            if (cloud.getCategoryFilesMap().size() > 0) {
                cloud.getCategoryFilesMap().clear();
            }
            if (cloud.getPrevDriveContent().size()>0){
                cloud.getPrevDriveContent().clear();
            }
            if (!myFiles){
                if (seletedPosition!=-1){
                    for (int i1=0;i1<filesList.size();i1++){
                        File file = filesList.get(i1);
                        File sharedFile = sharedUserList.get(seletedPosition);
                        List<User> ownerList = sharedFile.getOwners();
                        List<User> ownerList1 = file.getOwners();
                        if (ownerList!=null && ownerList.size()>0 && ownerList1!=null && ownerList1.size()>0){
                            User user = ownerList.get(0);
                            User user1 = ownerList1.get(0);
                            if (user!=null && user1!=null && !user.getEmailAddress().equals(user1.getEmailAddress())){
                                filesList.remove(i1);
                            }
                        }
                    }
                }
            }
            File meta= new File();
            meta.setParents(Collections.singletonList(new ParentReference().setId("root")));
            meta.setTitle("Root Files");
            meta.setMimeType(UT.MIME_FLDR);
            rootFolderList.add(meta);
            for (File file : filesList){
                if (file.getMimeType().equals("application/vnd.google-apps.folder")) {
                    rootFolderList.add(file);
                }
            }
            cloud.setCategoryList(rootFolderList);
            for (int i = 0; i < cloud.getCategoryList().size(); i++) {
                if (i==0){
                    ArrayList<File> rootFileList = new ArrayList<>();
                    for (File file : filesList){
                        if (!file.getMimeType().equals("application/vnd.google-apps.folder")) {
                           rootFileList.add(file);
                        }
                    }
                    HashMap<Integer, Object> hashMap = new HashMap<>();
                    hashMap.put(i, rootFileList);
                    cloud.getCategoryFilesMap().add(hashMap);
                }else {
                    ArrayList<File> subFileList = new ArrayList<>();
                    File subFile = cloud.getCategoryList().get(i);
                    for (int i1=0;i1<driveFileUpload.getUploadingFilesList().size();i1++){
                        DriveFile driveFile = driveFileUpload.getUploadingFilesList().get(i1);
                        if (driveFile.getParentId().equals(subFile.getId())){
                            File newFile = new File();
                            newFile.setTitle(driveFile.getTitle());
                            newFile.setMimeType(driveFile.getMimeType());
                            subFileList.add(newFile);
                        }
                    }
                    ArrayList<File> cloudFileList = REST.subFiles(subFile.getId());
                    for (File cloudFile : cloudFileList){
                        subFileList.add(cloudFile);
                    }
                    HashMap<Integer, Object> hashMap = new HashMap<>();
                    hashMap.put(i, subFileList);
                    cloud.getCategoryFilesMap().add(hashMap);
                }
            }
        }else{
            cloud.setCategoryList(rootFolderList);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (reLoad) {
            REST.connect();
            reLoad = false;
        }
    }
    @Override
    protected void onPause() {  super.onPause();
        REST.disconnect();
    }
    private class createFolderorFile extends AsyncTask<Void,Void,File>{
        private String folderId;
        private String folderTitle;
        private String folderMimeType;
        private java.io.File uploadFile;
        private boolean isCategory;
        private int position;
        private ProgressDialog dialog;

        public createFolderorFile(String id, String title, String mimeType, java.io.File file,boolean createCategory,int shelfPosition){
            this.folderId = id;
            this.folderTitle = title;
            this.folderMimeType = mimeType;
            this.uploadFile = file;
            this.isCategory = createCategory;
            this.position = shelfPosition;
        }

        public createFolderorFile(String id, String title, String mimeType,boolean createCategory){
            this.folderId = id;
            this.folderTitle = title;
            this.folderMimeType = mimeType;
            this.isCategory = createCategory;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (folderMimeType.contains("video")||folderMimeType.contains("audio")||folderMimeType.contains("image")) {
                dialog = ProgressDialog.show(CloudActivity.this,"","Uploading please wait...", true);
               /* File file = new File();
                file.setTitle(folderTitle);
                file.setMimeType(folderMimeType);
                file.setWebContentLink(uploadFile.getAbsolutePath());
                HashMap<Integer, Object> hashMap = cloud.getCategoryFilesMap().get(position);
                ArrayList<File> subList = (ArrayList<File>) hashMap.get(position);
                subList.add(0, file);
                gridView.invalidateViews();*/
            }

        }

        @Override
        protected File doInBackground(Void... params) {
            File file =null;
            if (folderMimeType.equals("application/vnd.google-apps.folder")) {
               file = REST.createFolder(folderId, folderTitle);
            }else{
               file = REST.createFile(folderId,folderTitle,folderMimeType,uploadFile);
            }
            return file;
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            if (dialog!=null && dialog.isShowing()){
                dialog.dismiss();
            }
            if (!isCategory){
                if (imageFileUri!=null) {
                    if (new java.io.File(imageFileUri.getPath()).exists()) {
                        new java.io.File(imageFileUri.getPath()).delete();
                    }
                }
                String audio_file = audio_outputFile = Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR + "SBA_AUDIO_CLOUD.m4a";
                if (new java.io.File(audio_file).exists()){
                    new java.io.File(audio_file).delete();
                }
            }
            if (file!=null){
                if (isCategory) {
                    cloud.getCategoryList().add(file);
                    HashMap<Integer,Object> hashMap = new HashMap<>();
                    ArrayList<File> arrayList = new ArrayList<>();
                    hashMap.put(cloud.getCategoryFilesMap().size(),arrayList);
                    cloud.getCategoryFilesMap().add(hashMap);
                    gridView.invalidateViews();
                    gridView.smoothScrollToPosition(cloud.getCategoryList().size()-1);
                }else{
                    HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(position);
                    ArrayList<File> subList = (ArrayList<File>) hashMap.get(position);
                    subList.add(file);
                    gridView.invalidateViews();
                }
            }else{
                Toast.makeText(CloudActivity.this, R.string.upload_failed,Toast.LENGTH_SHORT).show();
            }
        }
    }
    private class folderClickedTask extends AsyncTask<Void,Void,Void>{
        private File newFile;
        private int shelfPosition;
        private RecyclerView view;
        ArrayList<File> subFileList;

        public folderClickedTask(File file,int shelfPos,RecyclerView recyclerView){
            this.newFile = file;
            this.shelfPosition = shelfPos;
            this.view = recyclerView;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(shelfPosition);
            ArrayList<File> subList = (ArrayList<File>) hashMap.get(shelfPosition);
            if (!checkPrevFolderExist(shelfPosition)) {
                ArrayList<HashMap<Integer, Object>> prevList = new ArrayList<HashMap<Integer, Object>>();
                HashMap<Integer, Object> prevMap = new HashMap<Integer, Object>();
                ArrayList<File> prevSubList = new ArrayList<>();
                for (int i=0;i<subList.size();i++){
                    File file = subList.get(i);
                    prevSubList.add(file);
                }
                prevMap.put(shelfPosition,prevSubList);
                prevList.add(prevMap);
                HashMap<Integer, Object> prevMap1 = new HashMap<Integer, Object>();
                prevMap1.put(shelfPosition,prevList);
                cloud.getPrevDriveContent().add(prevMap1);
            }else{
                for (int i=0;i<cloud.getPrevDriveContent().size();i++){
                    HashMap<Integer, Object> prevMap = cloud.getPrevDriveContent().get(i);
                    ArrayList<HashMap<Integer, Object>> prevList = (ArrayList<HashMap<Integer, Object>>) prevMap.get(shelfPosition);
                    if (prevList!=null) {
                        HashMap<Integer, Object> prevMap1 = new HashMap<Integer, Object>();
                        ArrayList<File> prevSubList = new ArrayList<>();
                        for (int i1=0;i1<subList.size();i1++){
                            File file = subList.get(i1);
                            prevSubList.add(file);
                        }
                        prevMap1.put(shelfPosition,prevSubList);
                        prevList.add(prevMap1);
                        break;
                    }
                }
            }
            subList.clear();
            gridView.invalidateViews();
        }

        @Override
        protected Void doInBackground(Void... params) {
            subFileList = REST.subFiles(newFile.getId());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            cloud.getCategoryList().remove(shelfPosition);
            cloud.getCategoryList().add(shelfPosition,newFile);
            HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(shelfPosition);
            ArrayList<File> subList = (ArrayList<File>) hashMap.get(shelfPosition);
            subList.clear();
            for (int i=0;i<subFileList.size();i++){
                File file = subFileList.get(i);
                subList.add(file);
            }
            gridView.invalidateViews();
        }
    }
    private class backClickedTask extends AsyncTask<Void,Void,File>{

        private int shelfPosition;
        private RecyclerView rView;
        ArrayList<File> metadataList;

        public backClickedTask(int shelfpos,RecyclerView recyclerView){
            this.shelfPosition = shelfpos;
            this.rView = recyclerView;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(shelfPosition);
            ArrayList<File> subList = (ArrayList<File>) hashMap.get(shelfPosition);
            subList.clear();
            gridView.invalidateViews();
        }


        @Override
        protected File doInBackground(Void... params) {
            if (cloud.getPrevDriveContent().size()>0) {
                for (int i = 0; i < cloud.getPrevDriveContent().size(); i++) {
                    HashMap<Integer, Object> prevMap = cloud.getPrevDriveContent().get(i);
                    final ArrayList<HashMap<Integer, Object>> prevList = (ArrayList<HashMap<Integer, Object>>) prevMap.get(shelfPosition);
                    if (prevList != null&&prevList.size()>0) {
                        HashMap<Integer,Object> dataObj = prevList.get(prevList.size()-1);
                        metadataList = (ArrayList<File>) dataObj.get(shelfPosition);
                        prevList.remove(prevList.size()-1);
                        File file = metadataList.get(0);
                        List<ParentReference> list = file.getParents();
                        for (ParentReference parentReference:list){
                            String id = parentReference.getId();
                            File file1 = REST.getFile(id);
                            metadataList = REST.subFiles(file1.getId());
                            return file1;
                        }
                        break;
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            if (file!=null) {
                cloud.getCategoryList().remove(shelfPosition);
                cloud.getCategoryList().add(shelfPosition,file);
                HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(shelfPosition);
                ArrayList<File> subList = (ArrayList<File>) hashMap.get(shelfPosition);
                subList.clear();
                for (int i=0;i<metadataList.size();i++){
                    File file1 = metadataList.get(i);
                    subList.add(file1);
                }
                gridView.invalidateViews();
            }
        }
    }
    public void showImageDialog(final File file, String type) {
        final Dialog dialog = new Dialog(CloudActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.preview_cloud_image);
        if (Globals.isTablet()) {
            dialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.5));
        }else{
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
        Button download_btn = (Button) dialog.findViewById(R.id.download_btn);
        if (book!=null){
            download_btn.setVisibility(View.VISIBLE);
        }
        download_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadFileFromURL(file).execute(file.getWebContentLink());
            }
        });
        ImageView iv = (ImageView) dialog.findViewById(R.id.imageView2);
        final Button play = (Button) dialog.findViewById(R.id.btn_play);
        final SeekBar audio_progress = (SeekBar) dialog.findViewById(R.id.seekBar);
        ImageView iv_audio = (ImageView) dialog.findViewById(R.id.imageView);
        if (type.equals("image")){
            play.setVisibility(View.GONE);
            audio_progress.setVisibility(View.GONE);
            iv_audio.setVisibility(View.GONE);
            iv.setVisibility(View.VISIBLE);
        }else{
            play.setVisibility(View.VISIBLE);
            audio_progress.setVisibility(View.VISIBLE);
            iv_audio.setVisibility(View.VISIBLE);
            iv.setVisibility(View.GONE);
        }
        CircleButton btn_back=(CircleButton)dialog.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if (type.equals("image")) {
            new loadImage(iv).execute(file.getWebContentLink());
        }else{
            final MediaPlayer mp = new MediaPlayer();
            mp.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                    audio_progress.setSecondaryProgress(percent);
                }
            });

            try {
              //  mp.setDataSource("http://www.storychannels.com//Demo/img/uploads/Story/Audio/50/Audio_03_William Boyd.mp3");
                mp.setDataSource(file.getWebContentLink());
                mp.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mp.stop();
                }
            });

            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    audio_progress.setProgress(mediaPlayer.getCurrentPosition());
                    // String p=context.getResources().getString(R.string.play).toString();
                    if (play.getText().toString().equals(getResources().getString(R.string.stop).toString())) {
                        play.setText(getResources().getString(R.string.play));
                    }

                }
            });

            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final int mediaFileLength1 = mp.getDuration();
                    if (!mp.isPlaying()) {
                        //mp.getSelectedTrack(mp.getCurrentPosition());
                        mp.start();
                        audio_progress.setProgress(mp.getCurrentPosition());
                        //audio_progress.setMax(mp.getDuration());
                        play.setSelected(true);
                        play.setText(getResources().getString(R.string.stop));
                        audio_progress.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                audio_progress.setProgress((int)(((float)mp.getCurrentPosition()/mediaFileLength1*100)));
                            }
                        }, 1000);
                    } else {
                        play.setText(getResources().getString(R.string.play));
                        mp.pause();
                        play.setSelected(false);
                        audio_progress.setProgress(mp.getCurrentPosition());
                        //audio_progress.setMax(mp.getDuration());

                    }
                    // mp.start();
                }
            });
            audio_progress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, final int i, boolean fromUser) {
                    //mp.seekTo(i);
                    audio_progress.setProgress(i);
                    // progressUpdate(mp, audio_progress, play);
                    //while(mp.getCurrentPosition())
                    audio_progress.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            audio_progress.setProgress(mp.getCurrentPosition());
                        }
                    }, 1000);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int s = seekBar.getProgress();
                    mp.seekTo(s);
                    //audio_progress.setProgress();
                }
            });
        }
        dialog.show();
    }

    private class insertPermissionAndLoadVideo extends AsyncTask<Void,Void,String>{
        private File videoFile;
      ProgressDialog dialog;

        public insertPermissionAndLoadVideo(File file){
            this.videoFile = file;
        }

      @Override
      protected void onPreExecute() {
          super.onPreExecute();
          dialog = ProgressDialog.show(CloudActivity.this,"", "Preparing file", true);
      }

      @Override
        protected String doInBackground(Void... params) {
            String url = null;
          if (videoFile.getId()!=null) {
              REST.insertPermission(videoFile.getId(), "default", "anyone", "reader");
          }
            return url;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (dialog.isShowing()){
                dialog.dismiss();
            }
            if (videoFile.getMimeType().contains("video")){
                Intent intent = new Intent(CloudActivity.this,VideoPreviewOline.class);
                if (videoFile.getId()!=null){
                    intent.putExtra("boolean",false);
                }else{
                    intent.putExtra("boolean",true);
                }
                intent.putExtra("url",videoFile.getWebContentLink());
                startActivity(intent);
            }else if (videoFile.getMimeType().contains("image")){
                showImageDialog(videoFile,"image");
            }else{
                showImageDialog(videoFile,"audio");
            }
        }
    }
    private class movingTask extends AsyncTask<Void,Void,Void>{
        File sourceFile;
        File targetFile;
        int position;

        public movingTask(int shelfPos,File sourceFileId,File targetFileId){
            this.sourceFile = sourceFileId;
            this.targetFile = targetFileId;
            this.position = shelfPos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(position);
            ArrayList<File> subList = (ArrayList<File>) hashMap.get(position);
            subList.remove(sourceFile);
            gridView.invalidateViews();
        }

        @Override
        protected Void doInBackground(Void... params) {
            REST.moveFiles(sourceFile,targetFile);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void groupingFiles(final int shelfPos, final File sourceFile, final File targetFile){
        final Dialog groupDialog = new Dialog(CloudActivity.this);
        groupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        groupDialog.setTitle(R.string.folder_name);
        groupDialog.setContentView(getLayoutInflater().inflate(R.layout.group_edit, null));
        groupDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.5), RelativeLayout.LayoutParams.WRAP_CONTENT);
        final EditText editText = (EditText) groupDialog.findViewById(R.id.enr_editText);
        //editText.setText(enrichmentTitle);
        Button btnSave = (Button) groupDialog.findViewById(R.id.enr_save);
        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (editText.getText().toString().length()>0){
                    new GroupingFilesTask(shelfPos,sourceFile,targetFile,editText.getText().toString()).execute();
                    groupDialog.dismiss();
                }
            }
        });
        Button btnCancel = (Button) groupDialog.findViewById(R.id.enr_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                groupDialog.dismiss();
            }
        });
        groupDialog.show();
    }
    private class GroupingFilesTask extends AsyncTask<Void,Void,File>{
        private int position;
        private File sourceFile;
        private File destFile;
        private String name;
        File folder;

        public GroupingFilesTask(int shelfPos,File srcFile,File targetFile,String folderName){
            this.sourceFile = srcFile;
            this.destFile = targetFile;
            this.position = shelfPos;
            this.name = folderName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(position);
            ArrayList<File> subList = (ArrayList<File>) hashMap.get(position);
            subList.remove(sourceFile);
            subList.remove(destFile);
            gridView.invalidateViews();
        }

        @Override
        protected File doInBackground(Void... params) {
            File data = cloud.getCategoryList().get(position);
            folder = REST.createFolder(data.getId(),name);
            REST.moveFiles(sourceFile,folder);
            REST.moveFiles(destFile,folder);
            return folder;
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(position);
            ArrayList<File> subList = (ArrayList<File>) hashMap.get(position);
            subList.add(file);
            gridView.invalidateViews();
        }
    }
    public class GestureListener extends GestureDetector.SimpleOnGestureListener{

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (currentShelfPos!=0) {
                editDialog(currentShelfPos, 0, true);
            }
            return true;
        }
    }

    private void editDialog(final int position, final int bookPos, final boolean isCatEdit) {
         final File file;
        if (isCatEdit) {
            file = cloud.getCategoryList().get(position);
        }else{
            HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(position);
            ArrayList<File> subList = (ArrayList<File>) hashMap.get(position);
            file = subList.get(bookPos);
        }
        View view1 = getLayoutInflater().inflate(R.layout.category_property_listview, null);
        AlertDialog.Builder popView = new AlertDialog.Builder(CloudActivity.this);
        popView.setView(view1);
        popView.setCancelable(false);
        popView.setTitle("Edit title");
        final EditText et_cat = (EditText) view1.findViewById(R.id.editText1);
        et_cat.setText(file.getTitle());
        popView.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String title = et_cat.getText().toString();
                if (!file.getTitle().equals(title)){
                    new RenameFiletask(title,file.getId(),position,bookPos,isCatEdit).execute();
                    dialog.cancel();
                }
            }
        });
        popView.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = popView.create();
        alert.show();
    }

    private class RenameFiletask extends AsyncTask<Void,Void,File>{
        private String fileName;
        private String fileId;
        private int position;
        private int bookposition;
        private boolean categoryEdit;

        public RenameFiletask(String title,String id,int pos,int bookpos,boolean catEdit){
            this.fileName = title;
            this.fileId = id;
            this.position = pos;
            this.bookposition = bookpos;
            this.categoryEdit = catEdit;
        }

        @Override
        protected File doInBackground(Void... params) {
            return REST.update(fileId,fileName,null,null,null,null);
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            if (file!=null){
                if (categoryEdit) {
                    cloud.getCategoryList().remove(position);
                    cloud.getCategoryList().add(position, file);
                    gridView.invalidateViews();
                } else {
                    HashMap<Integer, Object> hashMap = cloud.getCategoryFilesMap().get(position);
                    ArrayList<File> subList = (ArrayList<File>) hashMap.get(position);
                    subList.remove(bookposition);
                    subList.add(bookposition, file);
                    gridView.invalidateViews();
                }
            }
        }
    }
    private class getPreviousParentFile extends AsyncTask<Void,Void,File>{
        private int shelfPosition;
        private File srcFile;

        public getPreviousParentFile(int position , File sourceFile){
            this.shelfPosition = position;
            this.srcFile = sourceFile;
        }

        @Override
        protected File doInBackground(Void... params) {
            if (cloud.getPrevDriveContent().size()>0) {
                for (int i = 0; i < cloud.getPrevDriveContent().size(); i++) {
                    HashMap<Integer, Object> prevMap = cloud.getPrevDriveContent().get(i);
                    final ArrayList<HashMap<Integer, Object>> prevList = (ArrayList<HashMap<Integer, Object>>) prevMap.get(shelfPosition);
                    if (prevList != null&&prevList.size()>0) {
                        HashMap<Integer,Object> dataObj = prevList.get(prevList.size()-1);
                        ArrayList<File> metadataList = (ArrayList<File>) dataObj.get(shelfPosition);
                        File file = metadataList.get(0);
                        List<ParentReference> list = file.getParents();
                        for (ParentReference parentReference:list){
                            String id = parentReference.getId();
                            return REST.getFile(id);
                        }
                        break;
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            if (file!=null) {
                new movingTask(shelfPosition, srcFile, file).execute();
            }
        }
    }
    private class deleteFile extends AsyncTask<Void,Void,Void>{
        private File delFile;
        private boolean isCategoryFile;
        private ArrayList<File> multipleFiles;
        private int shelfPos;

        public deleteFile(File file,boolean isCategory,ArrayList<File> filesList,int position){
            this.delFile = file;
            this.isCategoryFile = isCategory;
            this.multipleFiles = filesList;
            this.shelfPos = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isCategoryFile){
                cloud.getCategoryList().remove(shelfPos);
                cloud.getCategoryFilesMap().remove(shelfPos);
                progressBar.setVisibility(View.VISIBLE);
            }else{
                for (int i=0;i<multipleFiles.size();i++) {
                    String fileID = multipleFiles.get(i).getId();
                    for (int i2=0;i2<cloud.getCategoryFilesMap().size();i2++) {
                        HashMap<Integer, Object> hashMap = cloud.getCategoryFilesMap().get(i2);
                        ArrayList<File> subList = (ArrayList<File>) hashMap.get(i2);
                        for (int i1 = 0; i1 < subList.size(); i1++) {
                            File subFile = subList.get(i1);
                            if (fileID.equals(subFile.getId())) {
                                subList.remove(i1);
                            }
                        }
                    }
                }
            }
            gridView.invalidateViews();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (isCategoryFile) {
                REST.deleteFile(delFile.getId());
            }else{
                for (int i=0;i<multipleFiles.size();i++){
                    REST.deleteFile(multipleFiles.get(i).getId());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isCategoryFile){
                showprogresssBar = false;
                new getFilesFromGdrive().execute();
            }
            selectedList.clear();
        }
    }
   /* private boolean checkIsEditMode(int shelfPos){
        for (int i1=0;i1<editModeList.size();i1++){
            int value = editModeList.get(i1);
            if (value==shelfPos){
                return true;
            }
        }
        return false;
    }*/
    private void showListDialog(){
        final Dialog dialog = new Dialog(CloudActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloud_list_layout);
        dialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.5));

        ListView listView = (ListView) dialog.findViewById(R.id.listView1);
        listCategory category = new listCategory(CloudActivity.this);
        listView.setAdapter(category);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
                dialog.dismiss();
                new duplicateFileTask(position+1,selectedList).execute();
            }
        });
        TextView txtNoCategories = (TextView) dialog.findViewById(R.id.textView1);
        txtNoCategories.setVisibility(View.GONE);
        CircleButton button = (CircleButton) dialog.findViewById(R.id.btn_import);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private class duplicateFileTask extends AsyncTask<Void,Void,Void>{

        private int shelfPosition;
        ArrayList<File> duplicateFilesList = new ArrayList<>();

        public duplicateFileTask(int shelfPos,ArrayList<File> filesList){
            this.shelfPosition = shelfPos;
            this.duplicateFilesList = filesList;
        }

        @Override
        protected Void doInBackground(Void... params) {
            File parentFile = cloud.getCategoryList().get(shelfPosition);
            for (int i=0;i<duplicateFilesList.size();i++){
                File file = duplicateFilesList.get(i);
                REST.copyFile(file.getId(),file.getTitle(),parentFile.getId());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            HashMap<Integer,Object> hashMap = cloud.getCategoryFilesMap().get(shelfPosition);
            ArrayList<File> subList = (ArrayList<File>) hashMap.get(shelfPosition);
            for (int i=0;i<duplicateFilesList.size();i++){
                File file = duplicateFilesList.get(i);
                subList.add(file);
            }
            gridView.invalidateViews();
            selectedList.clear();
        }
    }

    private class loadImage extends AsyncTask<String,String,Bitmap>{
        ImageView imageView;

        public loadImage(ImageView imgView){
            this.imageView = imgView;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try{
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(params[0]).getContent());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap!=null && imageView!=null){
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    private void MediaPickerDialog(final String type){
        final Dialog media_dialog = new Dialog(CloudActivity.this);
        media_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        media_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        media_dialog.setContentView(getLayoutInflater().inflate(R.layout.cloud_pick_mediadialog, null));
        media_dialog.getWindow().setLayout((int) (Globals.getDeviceWidth()/1.3), RelativeLayout.LayoutParams.WRAP_CONTENT);
        Button cameraBtn = (Button) media_dialog.findViewById(R.id.camera_btn);
        if (type.equals("audio")){
            cameraBtn.setText(R.string.record_audio);
        }
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("image")){
                    media_dialog.dismiss();
                    callCameraToTakePhoto();
                }else if (type.equals("video")){
                    media_dialog.dismiss();
                    callCameraToTakeVideo();
                }else if (type.equals("audio")){
                    media_dialog.dismiss();
                    showAudioPopOverView();
                }
            }
        });
        Button galleryBtn = (Button) media_dialog.findViewById(R.id.gallery_btn);
        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("image")){
                    media_dialog.dismiss();
                    ImageGallery();
                }else if (type.equals("video")){
                    media_dialog.dismiss();
                    videoGallery();
                }else if (type.equals("audio")){
                    media_dialog.dismiss();
                    audioGallery();
                }
            }
        });
        media_dialog.show();
    }
    private void ImageGallery(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        if(Build.VERSION.SDK_INT>Build.VERSION_CODES.JELLY_BEAN){
            galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
        }
        startActivityForResult(galleryIntent, PICK_IMAGE);
    }
    private void videoGallery(){
        Intent videoGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        videoGalleryIntent.setType("video/*");
        startActivityForResult(videoGalleryIntent, PICK_VIDEO);
    }
    private void audioGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_AUDIO);
    }
    private boolean isDeviceSupportCamera() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }
    public void callCameraToTakeVideo() {
        if (!isDeviceSupportCamera()) {
            Toast.makeText(CloudActivity.this, "Sorry! Your device doesn't support camera", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            startActivityForResult(intent, CAPTURE_VIDEO);
        }
    }
    public void callCameraToTakePhoto(){

        //Checking camera availability
        if(!isDeviceSupportCamera()){
            Toast.makeText(CloudActivity.this, "Sorry! Your device doesn't support camera", Toast.LENGTH_LONG).show();
        }else{
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            imageFileUri = Uri.fromFile(getOutputMediaFile(MEDIA_TYPE_IMAGE));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }
    private java.io.File getOutputMediaFile(int type){

        java.io.File mediaStorageDir = new java.io.File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "SboookAuthor");
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("CamCamera", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String ImageName ="cloud1";
        java.io.File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new java.io.File(mediaStorageDir.getPath() + java.io.File.separator +"image_"+ ImageName + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new java.io.File(mediaStorageDir.getPath() + java.io.File.separator +"VID_"+ ImageName + ".mp4");
        }
        else {
            return null;
        }
        return mediaFile;
    }
    private void showAudioPopOverView() {
        final Dialog popoverView = new Dialog(this);
        popoverView.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
        popoverView.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popoverView.setContentView(getLayoutInflater().inflate(R.layout.audio_popoverview, null));
        popoverView.getWindow().setLayout((int) getResources().getDimension(R.dimen.bookview_audio_popover_width), (int) getResources().getDimension(R.dimen.bookview_audio_popover_height));
        final TextView tv_record = (TextView) popoverView.findViewById(R.id.tv_start_record);
        final Button btn_record = (Button) popoverView.findViewById(R.id.btn_audio_record);

        btn_record.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!btn_record.isSelected()) { //Start Recording
                    java.io.File fileImgDir = new java.io.File(Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR);
                    if (!fileImgDir.exists()) {
                        fileImgDir.mkdir();
                    }
                    audio_outputFile = Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR + "SBA_AUDIO_CLOUD.m4a";

                    mRecorder = new MediaRecorder();
                    mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                    mRecorder.setOutputFile(audio_outputFile);
                    mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                    mRecorder.setAudioSamplingRate(44100);
                    mRecorder.setAudioChannels(2);

                    try {
                        mRecorder.prepare();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mRecorder.start();

                    btn_record.setSelected(true);
                    tv_record.setText(R.string.stop_recording);
                    ((Chronometer) popoverView.findViewById(R.id.audio_chronometer)).setBase(SystemClock.elapsedRealtime());
                    ((Chronometer) popoverView.findViewById(R.id.audio_chronometer)).start();
                } else { //Stop Recording
                    btn_record.setSelected(false);
                    tv_record.setText(R.string.start_recording);
                    ((Chronometer) popoverView.findViewById(R.id.audio_chronometer)).stop();
                    popoverView.dismiss();
                }
            }
        });
        popoverView.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (mRecorder != null) {
                    mRecorder.stop();
                    mRecorder.release();
                    mRecorder = null;
                }
                if (audio_outputFile != null) {
                    java.io.File file = new java.io.File(audio_outputFile);
                    if (file.exists()) {
                        useThisRecording();
                    }
                }
            }
        });
        popoverView.show();
    }
    private void useThisRecording() {
        AlertDialog.Builder alertdialog = new AlertDialog.Builder(CloudActivity.this);
        alertdialog.setTitle(R.string.recording_finished);
        alertdialog.setMessage(R.string.use_this_recording);
        alertdialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                ArrayList<Uri> uriList = new ArrayList<>();
                uriList.add(Uri.fromFile(new java.io.File(audio_outputFile)));
                showDialog(uriList);
            }
        });
        alertdialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                java.io.File audioFilePath = new java.io.File(audio_outputFile);
                audioFilePath.delete();
            }
        });
        alertdialog.show();
    }
    private class DownloadFileFromURL extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        File driveFile;
        boolean downloadSuccess;

        public DownloadFileFromURL(File file){
            driveFile = file;
        }
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(CloudActivity.this);
            pDialog.setMessage("Downloading file. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();
                String imgDir;
                imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID() + "/" + "images/";
                java.io.File fileImgDir = new java.io.File(imgDir);
                if (!fileImgDir.exists()) {
                    fileImgDir.mkdir();
                }
                int objUniqueId = db.getMaxUniqueRowID("objects") + 1;
                String objFormat = "";
                if (driveFile.getMimeType().contains("image")){
                    objFormat = ".png";
                }else if (driveFile.getMimeType().contains("video")){
                    objFormat = ".mp4";
                }else if (driveFile.getMimeType().contains("audio")){
                    objFormat = ".m4a";
                    java.io.File audio_play_default_path = new java.io.File(imgDir + "Default_Audio_Play.png");
                    java.io.File audio_stop_default_path = new java.io.File(imgDir + "Default_Audio_Stop.png");
                    if (!(audio_play_default_path.exists() || audio_stop_default_path.exists())) {
                        UserFunctions.copyAsset(getAssets(), "Default_Audio_Play.png", audio_play_default_path.toString());
                        UserFunctions.copyAsset(getAssets(), "Default_Audio_Stop.png", audio_stop_default_path.toString());
                    }
                }
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                OutputStream output = new FileOutputStream(imgDir+objUniqueId+objFormat);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress(""+(int)((total*100)/lenghtOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                downloadSuccess = true;

            } catch (Exception e) {
                downloadSuccess = false;
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            if (pDialog!=null && pDialog.isShowing()){
                pDialog.dismiss();
            }
            if (downloadSuccess){
                updateObjects(driveFile);
            }
        }
    }
    private void updateObjects(File file){
        String objContent;
        int initialImageWidth =(int) getResources().getDimension(R.dimen.bookview_initial_video_width);
        int initialImageHeight = (int) getResources().getDimension(R.dimen.bookview_initial_video_height);
        int initialImageXpos = Globals.getDeviceWidth()/ 2 - initialImageWidth / 2;
        int initialImageYpos = Globals.getDesignPageHeight()/ 2 / 2 - initialImageHeight / 2;
        String location = initialImageXpos + "|" + initialImageYpos;
        String size = initialImageWidth + "|" + initialImageHeight;
        int objUniqueId = db.getMaxUniqueRowID("objects") + 1;
        String objType = "";
        String objFormat = "";
        if (file.getMimeType().contains("image")){
            objType = "Image";
            objFormat = ".png";
        }else if (file.getMimeType().contains("video")){
            objType = "YouTube";
            objFormat = ".mp4";
        }else if (file.getMimeType().contains("audio")){
            objType = "Audio";
            objFormat = ".m4a";
        }
        int objBID = book.getBookID();
        int  objSequentialId = db.getMaximumSequentialIdValue(pageNumber, objBID) + 1;
        String objPageNo = String.valueOf(pageNumber);
        String objSclaPageToFit = "no";
        String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + objBID + "/" + "images/";
        objContent = imgDir +objUniqueId + objFormat;
        boolean objLocked = false;
        db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + objBID + "', '" + objPageNo + "', '" + location + "', '" + size + "', '" + objContent.replace("'", "''") + "', '" + objSequentialId + "', '" + objSclaPageToFit + "', '" + objLocked + "', '" + 0 + "')");
    }
    private static class CustomAdapter<T> extends ArrayAdapter<String> {
        public CustomAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText("");
            return view;
        }
    }
    private void sortBy(int pos){
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        if (filesList!=null) {
            ArrayList<File> sortList = filesList;
            if (pos==0) {
                Collections.sort(sortList, new Comparator<File>() {
                    @Override
                    public int compare(File lhs, File rhs) {
                        String date1 = String.valueOf(lhs.getModifiedDate());
                        String date2 = String.valueOf(rhs.getModifiedDate());
                        try {
                            Date firstDate = dateFormat.parse(date1);
                            Date secondDate = dateFormat.parse(date2);
                            return firstDate.compareTo(secondDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        return 0;
                    }
                });
            }else {
                Collections.sort(sortList, new Comparator<File>() {
                    @Override
                    public int compare(File s1, File s2) {
                        return s1.getTitle().compareToIgnoreCase(s2.getTitle());
                    }
                });
            }
            ArrayList<File> rootFolderList = new ArrayList<>();
            if (sortList.size() > 0) {
                if (cloud.getCategoryFilesMap().size() > 0) {
                    cloud.getCategoryFilesMap().clear();
                }
                if (cloud.getPrevDriveContent().size() > 0) {
                    cloud.getPrevDriveContent().clear();
                }
                File meta = new File();
                meta.setParents(Collections.singletonList(new ParentReference().setId("root")));
                meta.setTitle("Root Files");
                meta.setMimeType(UT.MIME_FLDR);
                rootFolderList.add(meta);
                for (File file : sortList) {
                    if (file.getMimeType().equals("application/vnd.google-apps.folder")) {
                        rootFolderList.add(file);
                    }
                }
            }
            cloud.setCategoryList(rootFolderList);
            loadSubFiles(sortList,pos);
        }
    }
    private class SortByNameAndLastModified extends AsyncTask<Void,Void,Void>{
        int position;

        public SortByNameAndLastModified(int pos){
            this.position = pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.INVISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            sortBy(position);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);
            gridView.invalidateViews();
        }
    }

  /*  private class LoaaSubFilesTask extends AsyncTask<Void,Void,Void>{
        int position;
        ArrayList<File> arrayList;

        public LoaaSubFilesTask(ArrayList<File> fileArrayList,int pos){
            this.position = pos;
            this.arrayList = fileArrayList;
        }

        @Override
        protected Void doInBackground(Void... params) {
            loadSubFiles(arrayList,position);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            gridView.invalidateViews();
        }
    }*/
    private void loadSubFiles(ArrayList<File> fileArrayList,int pos){
        for (int i = 0; i < cloud.getCategoryList().size(); i++) {
            if (i == 0) {
                ArrayList<File> rootFileList = new ArrayList<>();
                for (File file : fileArrayList) {
                    if (!file.getMimeType().equals("application/vnd.google-apps.folder")) {
                        rootFileList.add(file);
                    }
                }
                if (pos == 1) {
                    Collections.sort(rootFileList, new Comparator<File>() {
                        @Override
                        public int compare(File s1, File s2) {
                            return s1.getTitle().compareToIgnoreCase(s2.getTitle());
                        }
                    });
                }
                HashMap<Integer, Object> hashMap = new HashMap<>();
                hashMap.put(i, rootFileList);
                cloud.getCategoryFilesMap().add(hashMap);
            } else {
                File subFile = cloud.getCategoryList().get(i);
                ArrayList<File> subFileList = REST.subFiles(subFile.getId());
                if (pos == 1) {
                    Collections.sort(subFileList, new Comparator<File>() {
                        @Override
                        public int compare(File s1, File s2) {
                            return s1.getTitle().compareToIgnoreCase(s2.getTitle());
                        }
                    });
                }
                HashMap<Integer, Object> hashMap = new HashMap<>();
                hashMap.put(i, subFileList);
                cloud.getCategoryFilesMap().add(hashMap);
            }
        }
    }
    private void edit(){
        if (edit_mode){
            edit_mode = false;
            relativeLayout.setVisibility(View.GONE);
            drive_btn.setVisibility(View.VISIBLE);
            segmentedRadioGroup.setVisibility(View.VISIBLE);
            gridView.invalidateViews();
        }else{
            edit_mode = true;
            relativeLayout.setVisibility(View.VISIBLE);
            drive_btn.setVisibility(View.INVISIBLE);
            segmentedRadioGroup.setVisibility(View.INVISIBLE);
            gridView.invalidateViews();
        }
    }
    private void showDialogInUIthread(final ArrayList<Uri> uriList){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showDialog(uriList);
            }
        });
    }
    private boolean isSupportedFile(String mimeType){
        String[] filesMimeType = {"application/pdf",
                "application/vnd.google-apps.presentation",
                "application/vnd.google-apps.spreadsheet",
                "application/vnd.google-apps.document",
                "text/plain",
                "application/vnd.oasis.opendocument.text",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "application/msword",
                "doc"};
        for (String str : filesMimeType){
            if (mimeType.equals(str)){
                return true;
            }
        }
        return false;
    }
    private class DownloadFile extends AsyncTask<String, String, String> {
        boolean downloadSuccess;
        private String downloadUrl;
        private String fileId;
        private ProgressDialog dialog;

        public DownloadFile(String url,String Id){
            this.downloadUrl = url;
            this.fileId = Id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(CloudActivity.this,"","please wait...", true);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            long total = 0;
            try {
                String imgDir;
                imgDir = Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR;
                java.io.File fileImgDir = new java.io.File(imgDir);
                if (!fileImgDir.exists()) {
                    fileImgDir.mkdir();
                }
                REST.insertPermission(fileId, "default", "anyone", "reader");
                OutputStream output = new FileOutputStream(imgDir+"temp.pdf");
                if (downloadUrl == null){
                    downloadUrl = REST.downloadUrl(fileId);
                }
                REST.downloadFile(downloadUrl,fileId,output);
                downloadSuccess = true;
            } catch (Exception e) {
                downloadSuccess = false;
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (dialog!=null){
                dialog.dismiss();
            }
            if (downloadSuccess && downloadUrl != null) {
                String url = "docs.google.com/gview?embedded=true&url=file:///"+Globals.TARGET_BASE_FILE_PATH+"temp.pdf";
                Intent updateIntent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                startActivity(updateIntent);
            }
        }
    }
    private void showPopover(View view){
        final PopoverView popoverView = new PopoverView(CloudActivity.this, R.layout.categories_list_view);
        popoverView.setContentSizeForViewInPopover(new Point((int) getResources().getDimension(R.dimen.shelf_info_view_width), (int) getResources().getDimension(R.dimen.shelf_info_view_height)));
        popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(view), PopoverView.PopoverArrowDirectionAny, true);

        ListView listView = (ListView) popoverView.findViewById(R.id.listView1);
        listView.setAdapter(new listSharedUser(CloudActivity.this));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                popoverView.dissmissPopover(true);
                if (seletedPosition==position){
                    seletedPosition = -1;
                    if (cloud!=null){
                        cloud.getCategoryList().clear();
                    }
                    new getFilesFromGdrive().execute();
                }else {
                    seletedPosition = position;
                    if (myFiles) {
                        segmentedRadioGroup.check(R.id.shared_files);
                    } else {
                        if (cloud != null) {
                            cloud.getCategoryList().clear();
                        }
                        new getFilesFromGdrive().execute();
                    }
                }
            }
        });
        TextView txtNoCategories = (TextView) popoverView.findViewById(R.id.textView1);
        if (sharedUserList.size()>0){
            txtNoCategories.setVisibility(View.GONE);
        }else{
            txtNoCategories.setText(getResources().getString(R.string.no_sharedusers));
        }
    }
    public class listSharedUser extends BaseAdapter {
        Context ctx;

        public listSharedUser(Context context) {
            this.ctx=context;
            filterSharedList();
        }

        public class ViewHolder {
            public TextView txtTitle;
            public CheckBox checkBox;
        }

        @Override
        public int getCount() {
            return sharedUserList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;

            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.inflate_checklist, null);
                Typeface font = Typeface.createFromAsset(getAssets(), "fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent, font);
                holder = new ViewHolder();
                holder.checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
                holder.txtTitle = (TextView)view.findViewById(R.id.textView1);
                holder.txtTitle.setTextColor(Color.BLACK);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            if (seletedPosition!=-1 && position == seletedPosition){
                holder.checkBox.setChecked(true);
            }else{
                holder.checkBox.setChecked(false);
            }
            File metadata = sharedUserList.get(position);
            List<User> ownersList = metadata.getOwners();
            if (ownersList.size()>0){
                User user = ownersList.get(0);
                if (user!=null){
                    holder.txtTitle.setText(user.getDisplayName());
                }
            }
            return view;
        }
    }
    private class getSharedFilesFromGdrive extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... params) {
           sharedUserList = REST.search("root",null,null,false);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            swipeContainer.setRefreshing(false);
        }
    }
    private void filterSharedList(){
        ArrayList<File> newSharedList = new ArrayList<>();
        for (int i=0;i<sharedUserList.size();i++){
            File file = sharedUserList.get(i);
            List<User> ownersList = file.getOwners();
            if (ownersList.size()>0){
                User user = ownersList.get(0);
                if (user != null){
                    if (!checkSameUser(user,newSharedList)) {
                        newSharedList.add(file);
                    }
                }
            }
        }
        sharedUserList = newSharedList;
    }
    private boolean checkSameUser(User user , ArrayList<File> fileList){
        for (int i=0;i<fileList.size();i++){
            File file = fileList.get(i);
            List<User> ownersList = file.getOwners();
            if (ownersList.size()>0){
                User user1 = ownersList.get(0);
                if (user1 != null){
                    if (user.getEmailAddress().equals(user1.getEmailAddress())){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
