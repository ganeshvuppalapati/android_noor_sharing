package com.semanoor.manahij;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by karthik on 23-03-2017.
 */


public class ItemDecoration extends RecyclerView.ItemDecoration {
    private int spacing;

    public ItemDecoration(Context ctx, @DimenRes int spacing) {
        this.spacing = ctx.getResources().getDimensionPixelSize(spacing);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.set(spacing, spacing, spacing, spacing);
    }
}