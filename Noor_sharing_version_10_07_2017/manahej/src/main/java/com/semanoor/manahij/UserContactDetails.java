package com.semanoor.manahij;

import android.app.Activity;

import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.MyContacts;
import com.semanoor.source_sboookauthor.MyGroups;

import java.util.ArrayList;

/**
 * Created by Krishna on 06-10-2016.
 */
public class UserContactDetails {

    private static UserContactDetails instance;

    public  ArrayList<MyGroups> groupList;
    public ArrayList<MyContacts> userContactList;

    public static Activity context;
    DatabaseHandler db;
    public static String loggedInemailId;

    private UserContactDetails() {
        if (groupList == null && context != null) {
            db = DatabaseHandler.getInstance(context);
            groupList = db.getMyGroupsList(loggedInemailId, context);
            String query2 = "select * from tblUserContacts where groupsID='" +0+ "'and userID='" + loggedInemailId + "'";
            userContactList = db.getContactDetails(query2, context);
         }
    }
    public static synchronized UserContactDetails getInstance(Activity contxt,String emailId) {
        //if (instance== null) {
        if (context == null) {
            context=contxt;
            loggedInemailId=emailId;
            instance = new UserContactDetails();
        }
        return instance;
    }


    public Activity getContext() {
        return context;
    }

    public void setContext(Activity context) {
        this.context = context;
    }

    public ArrayList<MyGroups> getGroupList() {
        return groupList;
    }

    public void setGroupList(ArrayList<MyGroups> groupList) {
        this.groupList = groupList;
    }



    public String getLoggedInemailId() {
        return loggedInemailId;
    }

    public void setLoggedInemailId(String loggedInemailId) {
        this.loggedInemailId = loggedInemailId;
    }

    public void setUserContactList(ArrayList<MyContacts> userContactList) {
        this.userContactList = userContactList;
    }

    public ArrayList<MyContacts> getUserContactList() {
        return userContactList;
    }
}
