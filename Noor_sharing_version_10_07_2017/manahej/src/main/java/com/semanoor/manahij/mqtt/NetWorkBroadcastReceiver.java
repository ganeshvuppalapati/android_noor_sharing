package com.semanoor.manahij.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.mqtt.datamodels.Constants;

import org.json.JSONObject;

/**
 * Created by Pushpa on 16/06/17.
 */
public class NetWorkBroadcastReceiver extends BroadcastReceiver {

   // boolean isConnected = false;
    public static ConnectivityReceiverListener connectivityReceiverListener;

    public NetWorkBroadcastReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent arg1) {

     /*   String status = NetworkUtil.getConnectivityStatusString(context);
       // Toast.makeText(context,status,Toast.LENGTH_LONG).show();

        if(status.equals("Not connected to Internet")){
          isConnected=true;
        }*/
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
      boolean  isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();

        /*if( activeNetwork != null && activeNetwork.isConnectedOrConnecting()){

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        isConnected = ManahijApp.getInstance().isNetworkStatus();
                    }catch (Exception e){
                        Log.e("Mainhered",e.toString());
                    }
                }
            });
            thread.start();

        }*/
        if (connectivityReceiverListener != null) {
            connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
        }


    }


    public static boolean isConnected() {
        ConnectivityManager
                cm = (ConnectivityManager) ManahijApp.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
      /*  return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();*/
/*

if( activeNetwork != null
        && activeNetwork.isConnectedOrConnecting()){

    return ManahijApp.getInstance().isNetworkStatus();
}
       return false;
*/

        if (cm != null)
        {
            NetworkInfo[] info = cm.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }



        }
         return false;
    }


    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }





 /*   Context mContext;
    int i =0;
    @Override
    public void onReceive(Context context, Intent intent) {
        WifiManager wifiManager = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
       // NetworkInfo mdat = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
          try {
              JSONObject obj = new JSONObject();
              obj.put("network_status", mWifi.isConnected());
              if (mWifi.isConnected()) {
                  Toast.makeText(context, "Connected ", Toast.LENGTH_LONG).show();
                  context.sendBroadcast(
                          new Intent(NoorActivity.FILTER).putExtra(Constants.KEYTYPE, "NETWORK").putExtra(Constants.KEYMESSAGE, obj.toString()));
              }else {
                  Toast.makeText(context, "No NetWork ", Toast.LENGTH_LONG).show();
                  context.sendBroadcast(
                          new Intent(NoorActivity.FILTER).putExtra(Constants.KEYTYPE, "NETWORK").putExtra(Constants.KEYMESSAGE, obj.toString()));
              }
          }catch (Exception e){
              Log.e("e",e.toString());
          }
*/

       /* SharedPreferences pref = context.getSharedPreferences("Network",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        i++;


        mContext=context;
        if (mWifi.isConnected()) {
            try {
                Intent intent1 = new Intent(NoorActivity.FILTER);
                context.sendBroadcast(intent1);
                editor.putInt("mynumber", 0);
                editor.commit();
                callNetWork(true);
            }catch (Exception e){
                Log.e("e",e.toString());
            }
        }
        else {
            try {
                int mynum = pref.getInt("mynumber",0);
                mynum = mynum + i;
                editor.putInt("mynumber", mynum);
                editor.commit();
                Log.e("numberb","n"+pref.getInt("mynumber",0));
                if(pref.getInt("mynumber",0)==1) {
                    Log.e("number","n"+pref.getInt("mynumber",0));
                    callNetWork(false);
                }
            }catch (Exception e){
                Log.e("number",e.toString());
            }

        }
*/




/*
        NetworkInfo networkInfo = intent
                .getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if (networkInfo != null) {
            Log.d("NETWORK_STATE", "Type : " + networkInfo.getType() + "State : " + networkInfo.getState());


            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {

                //get the different network states
                if (networkInfo.getState() == NetworkInfo.State.CONNECTING || networkInfo.getState() ==    NetworkInfo.State.CONNECTED) {
                    Log.e("NETWROEK","s");
                }
                else {
                    Log.e("NETWROEK","s2");
                }
            }
        }





        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
         Log.e("NETWROEK","ss"+isConnected+isWiFi);
        if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {


        }*/
 //   }

   /* private void callNetWork(boolean b) {


        try{

                JSONObject obj = new JSONObject();
                obj.put("network_status", b);
                if(b) {
                    Toast.makeText(mContext, "NetWork Connected", Toast.LENGTH_SHORT).show();
                    Intent intent1 = new Intent(NoorActivity.FILTER);
                    intent1.  putExtra(Constants.KEYTYPE, "NETWORK");
                    mContext.sendBroadcast(intent1);
                   mContext.sendBroadcast(
                           new Intent(NoorActivity.FILTER).putExtra(Constants.KEYTYPE, "NETWORK").putExtra(Constants.KEYMESSAGE,obj.toString()));
                }
                else {
                    Intent intent1 = new Intent(NoorActivity.FILTER);
                    intent1.  putExtra(Constants.KEYTYPE, "NETWORK");
                    mContext.sendBroadcast(intent1);
                    Toast.makeText(mContext, "No NetWork ", Toast.LENGTH_LONG).show();
                    mContext.sendBroadcast(
                             new Intent(NoorActivity.FILTER).putExtra(Constants.KEYTYPE, "NETWORK").putExtra(Constants.KEYMESSAGE,obj.toString()));
                }



    }catch (Exception e){
            Log.e("Ex",e.toString());
        }

    }*/


}
