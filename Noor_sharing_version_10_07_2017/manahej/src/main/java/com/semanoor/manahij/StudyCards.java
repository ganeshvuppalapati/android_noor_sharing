package com.semanoor.manahij;

import java.util.ArrayList;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Window;

import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;

public class StudyCards extends Activity {

    /**
     * This variable is the container that will host our cards
     */
	private CardContainer mCardContainer;
	public SimpleCardStackAdapter adapter;
	public static Object[] noteStudycards;
	public static DatabaseHandler db;
	public static String bookName; 
	public static Book currentBook;
	public BookView bookView;
	private boolean loadingfromIndexpage;
	public static int tabNo=0;
	public static ArrayList<String> enrichPageArray = new ArrayList<String>();
	public static String epath;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.card_layout);
		bookView=new BookView();
		db = new DatabaseHandler(this);
		mCardContainer = (CardContainer)findViewById(R.id.layoutview);
		currentBook = (Book) getIntent().getSerializableExtra("Book");

		bookName = currentBook.get_bStoreID();
		
	//	noteStudycards = db.loadNoteDivToArray(bookName);
		String[] PageNo = (String[]) noteStudycards[0];
		String[] SText = (String[]) noteStudycards[1];
		String[] TabNo = (String[]) noteStudycards[4];
//Calling the Simple Card class :        
	//	adapter = new SimpleCardStackAdapter(getApplicationContext(), this,noteStudycards);
		
		Resources r = getResources();
		for(int tag =0; tag<SText.length; tag++){
			int pageNo = Integer.parseInt(PageNo[tag]) - 1;
			int tabNo = Integer.parseInt(TabNo[tag]); 
			String finalPageNo;
			if(tabNo > 0){
				
				finalPageNo = ""+pageNo + "-" +tabNo;
			}
			else{
				finalPageNo = ""+pageNo;
			}
			if(pageNo <= 0){
				adapter.add(new CardModel("Cover page", SText[tag], r.getDrawable(R.drawable.turn_bg)));
			}else{
				adapter.add(new CardModel("Page "+finalPageNo, SText[tag], r.getDrawable(R.drawable.turn_bg)));
			}
			
		}

//		if(adapter.getCount()==0){
//			adapter.notifyDataSetInvalidated();
//		}
		mCardContainer.setAdapter(adapter);		
	}
	public void studyCardNavigatePage(int navigatePage,int navigateTab){
		//dimiss study cards
		// load Index code :
		
		
			loadingfromIndexpage = true;
			//loadingfromcurlpage = true;
			tabNo =navigateTab;
			if(bookView.tabNo>0){
				//enriched = true;
				//Get the epath :
				String[] currentPageContents;
				//System.out.println("Current page number:"+myValue);
				currentPageContents = enrichPageArray.get(navigatePage).split("\\|"); 
				if(currentPageContents.length > 1){
					//System.out.println("CurrentPageContents:"+currentPageContents[1]);
					String[] enrichments = currentPageContents[1].split("#");
					epath = enrichments[tabNo].split("\\+")[0];
				}
				bookView.passingPageNumberValues(navigatePage);

			}else{
				bookView.passingPageNumberValues(navigatePage);
			}
			
	}
}

