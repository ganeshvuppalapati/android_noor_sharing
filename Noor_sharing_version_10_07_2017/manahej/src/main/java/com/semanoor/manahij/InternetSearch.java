package com.semanoor.manahij;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

/**
 * Created by karthik on 15-05-2017.
 */

public class InternetSearch {

    private LinearLayout search_layout;
    private EditText et_search;
    private Activity context;
    private String selectedSearch;
    private Button search_img;

    public InternetSearch(LinearLayout layout, EditText et_text,Activity ctx,Button imageView){
        this.search_layout = layout;
        this.et_search = et_text;
        this.context = ctx;
        this.search_img = imageView;
        setSelectedSearch("Google");
    }

    public void showSearchPopUp(View v){
        final View popView = context.getLayoutInflater().inflate(R.layout.pdf_advsearch_popup, null, false);
        final PopupWindow popwindow = new PopupWindow(context);
        popwindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popwindow.setContentView(popView);
        popwindow.setFocusable(true);
        popwindow.setTouchable(true);
        popwindow.showAsDropDown(v, 0, 0);
        popwindow.update((int) context.getResources().getDimension(R.dimen.add_tabbtn_width), ActionBar.LayoutParams.WRAP_CONTENT);
        final ListView lv = (ListView) popView.findViewById(R.id.listView1);
        lv.setAdapter(new SearchAdapter(context));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){
                    search_img.setBackgroundResource(R.drawable.google1);
                    setSelectedSearch("Google");
                    popwindow.dismiss();
                }else if (position == 1){
                    search_img.setBackgroundResource(R.drawable.wikipedia1);
                    setSelectedSearch("Wiki");
                    popwindow.dismiss();
                }else if (position == 2){
                    search_img.setBackgroundResource(R.drawable.youtube1);
                    setSelectedSearch("Youtube");
                    popwindow.dismiss();
                }else if (position == 3){
                    search_img.setBackgroundResource(R.drawable.yahoo_icon);
                    setSelectedSearch("Yahoo");
                    popwindow.dismiss();
                }else if (position == 4){
                    search_img.setBackgroundResource(R.drawable.bing_icon);
                    setSelectedSearch("Bing");
                    popwindow.dismiss();
                }else if (position == 5){
                    search_img.setBackgroundResource(R.drawable.ask);
                    setSelectedSearch("Ask");
                    popwindow.dismiss();
                }
            }
        });
    }

    public String getSelectedSearch() {
        return selectedSearch;
    }

    public void setSelectedSearch(String selectedSearch) {
        this.selectedSearch = selectedSearch;
    }

    private class SearchAdapter extends BaseAdapter {
        Context ctx;
        Integer[] mThumbIds;

        public SearchAdapter(Context context) {
            this.ctx=context;
            mThumbIds = new Integer[] {
                    R.drawable.google1, R.drawable.wikipedia1, R.drawable.youtube1, R.drawable.yahoo_icon,
                    R.drawable.bing_icon, R.drawable.ask
            };
        }

        public class ViewHolder {
            public ImageView imageView;
        }

        @Override
        public int getCount() {
            return mThumbIds.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;

            if (view == null) {
                view = context.getLayoutInflater().inflate(R.layout.pdf_searchlayout_inflate, null);
                holder = new ViewHolder();
                holder.imageView = (ImageView) view.findViewById(R.id.imageViewAdv);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.imageView.setImageResource(mThumbIds[position]);
            return view;
        }
    }

    public void searchClicked(InternetSearchCallBack internetSearchCallBack){
        String url;
        String title;
        if (!et_search.getText().toString().equals("") ) {
            if (getSelectedSearch().equals("Google")) {
                url = "http://www.google.com/search?q=" + et_search.getText().toString() + "";
                title = "Google";
            } else if (getSelectedSearch().equals("Wiki")) {
                title = "Wikipedia";
                String text = et_search.getText().toString();
                String language = detectLanguage(text);

                String percentEscpe = Uri.encode(text);
                if (language.equals("ar")) {
                    url = "http://ar.wikipedia.org/wiki/" + percentEscpe + "";
                } else {
                    url = "http://en.wikipedia.org/wiki/" + percentEscpe + "";
                }
            } else if (getSelectedSearch().equals("Youtube")) {
                title = "Youtube";
                url = "http://m.youtube.com/#/results?q=" + et_search.getText().toString() + "";
            } else if (getSelectedSearch().equals("Yahoo")) {
                title = "Yahoo";
                url = "http://search.yahoo.com/search?p=" + et_search.getText().toString() + "";
            } else if (getSelectedSearch().equals("Bing")) {
                title = "Bing";
                url = "http://www.bing.com/search?q=" + et_search.getText().toString() + "";
            } else {
                title = "Ask";
                url = "http://www.ask.com/web?q=" + et_search.getText().toString() + "";
            }
            search_layout.setVisibility(View.GONE);
            internetSearchCallBack.onClick(url,title);
        }
    }
    public static String detectLanguage(String selectedText) {

        String alpha = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
        String[] alphaArray = alpha.split(",");
        for (int i = 0; i < alphaArray.length; i++) {
            String current = alphaArray[i];
            if (selectedText.contains(current)) {
                return "en";
            }
        }
        return "ar";
    }
}
