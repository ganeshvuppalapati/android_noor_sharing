package com.semanoor.manahij;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.HTMLObjectHolder;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.QuizSequenceObject;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Krishna on 04-05-2016.
 */
public class PageExportDialog {

    private String filesDirPath;
   // private BookViewActivity bookViewActivity;
    private File[] searchFilesList;
    DatabaseHandler db;
    GridShelf gridShelf;
    ArrayList<Object> StoreBooks;
    Book SelectedBook;
    boolean isBookSelected;
    ArrayList<PageNumbers> selectedPages = new ArrayList<PageNumbers>();
    ArrayList<PageNumbers> pages;
    Dialog addNewPageDialog;
    WebView wv_pages;
    GridView grid_pages;
    boolean webViewSelected;
    boolean allowed=false;
    boolean not_allowed=false;
    boolean all=false;
    SegmentedRadioButton btnSegmentGroup;
    Context ctx;
    ArrayList<String>malzamahBookDetails;
    public PageExportDialog(Context Activity, DatabaseHandler db, GridShelf gridshelf) {
        this.db = db;
        //this.bookViewActivity = Activity;
        this.gridShelf = gridshelf;
        this.ctx=Activity;
    }

//    public PageExportDialog(BookViewReadActivity context,DatabaseHandler database,GridShelf gridShlf) {
//        gridShelf=gridShlf;
//        this.ctx=
//
//    }


    public void showBookDialog() {
        addNewPageDialog = new Dialog((BookViewActivity)ctx);
        addNewPageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(((BookViewActivity)ctx).getResources().getColor(R.color.semi_transparent)));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        addNewPageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addNewPageDialog.setContentView(((BookViewActivity)ctx).getLayoutInflater().inflate(R.layout.export_pagedialog, null));
        addNewPageDialog.getWindow().setLayout((int) (((BookViewActivity)ctx).global.getDeviceWidth() / 1.1), (int) (((BookViewActivity)ctx).global.getDeviceHeight() / 1.5));

        btnSegmentGroup = (SegmentedRadioButton) addNewPageDialog.findViewById(R.id.btn_segment_group);
        // btnSegmentGroup.setVisibility(View.GONE);
        grid_pages = (GridView) addNewPageDialog.findViewById(R.id.gridView1);
        CircleButton btn_back = (CircleButton) addNewPageDialog.findViewById(R.id.btn_back);
        Button btn_add_page = (Button) addNewPageDialog.findViewById(R.id.btn_add_page);
        final TextView tv_title = (TextView) addNewPageDialog.findViewById(R.id.tv_title);
        wv_pages = (WebView) addNewPageDialog.findViewById(R.id.wv_preview);
        wv_pages.getSettings().setLoadWithOverviewMode(true);
        wv_pages.getSettings().setUseWideViewPort(true);
        wv_pages.getSettings().setJavaScriptEnabled(true);
        wv_pages.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        wv_pages.clearCache(true);
        wv_pages.setWebViewClient(new WebViewClient());
        wv_pages.getSettings().setAllowContentAccess(true);
        wv_pages.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv_pages.getSettings().setAllowFileAccess(true);
        wv_pages.getSettings().setPluginState(WebSettings.PluginState.ON);
        wv_pages.getSettings().setDomStorageEnabled(true);

        tv_title.setText(R.string.import_pages);
        // isBookSelected=false;
        //templateGridAdapter = new TemplateGridListAdapter(this, gridShelf.templatesPortraitListArray);
        all=false;
        allowed=true;
        not_allowed=false;
        grid_pages.setAdapter(new StoreBookDialog(false));
        btnSegmentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.seg_ll) {
                    all=true;
                    allowed=false;
                    not_allowed=false;
                    grid_pages.setAdapter(new StoreBookDialog(false));
                }else if (checkedId == R.id.seg_allowed) {
                    all=false;
                    allowed=true;
                    not_allowed=false;
                    grid_pages.setAdapter(new StoreBookDialog(false));
                }else if (checkedId == R.id.seg_not_allowed) {
                    all=false;
                    allowed=false;
                    not_allowed=true;
                    grid_pages.setAdapter(new StoreBookDialog(false));
                }
            }
        });
        grid_pages.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position,
                                    long arg3) {
                if (isBookSelected) {
                    PageNumbers page = pages.get(position);
                    if (page.isChecked()) {
                        page.setChecked(false);
                        for (int i = 0; i < selectedPages.size(); i++) {
                            PageNumbers select = selectedPages.get(i);
                            if (select.getBookID() == page.getBookID() && select.getpageNumber() == page.getpageNumber()) {
                                selectedPages.remove(i);
                                break;
                            }
                        }
                    } else {
                        if(page.getpageNumber()!=0) {
                            page.setChecked(true);
                            selectedPages.add(page);
                        }
                    }
                    //grid_pages.invalidateViews();
                    ((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
                } else {
                    Book book=(Book) StoreBooks.get(position);
                    if(checkBookSubscribedForClient(book.getClientID())) {
                        SelectedBook = (Book) StoreBooks.get(position);
                        tv_title.setText(SelectedBook.getBookTitle());
                        // isBookSelected=true;
                        grid_pages.setAdapter(new StoreBookDialog(true));
                    }
                    ((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
                }
            }

        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wv_pages.isShown()) {
                    btnSegmentGroup.setVisibility(View.VISIBLE);
                    grid_pages.setVisibility(View.VISIBLE);
                    wv_pages.setVisibility(View.GONE);
                } else {
                    if (isBookSelected) {
                        btnSegmentGroup.setVisibility(View.VISIBLE);
                        grid_pages.setAdapter(new StoreBookDialog(false));
                        grid_pages.invalidate();
                        tv_title.setText(R.string.import_pages);
                    }else{
                        addNewPageDialog.dismiss();
                    }
                }
            }
        });
        btn_add_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedPages.size()>0) {
                    ((BookViewActivity)ctx).new takeAndSaveScreenshot(((BookViewActivity)ctx).currentPageNumber).execute();
                    int oldPageCountVal = ((BookViewActivity)ctx).pageCount;
                    // bookViewActivity.pageCount = bookViewActivity.pageCount + selectedPages.size();
                    // bookViewActivity.currentBook.setTotalPages(bookViewActivity.pageCount);

                    int pageNumber = ((BookViewActivity)ctx).pageCount - 1;

                    //update the cursor

                    new AddingPagestoExistingNormalBook(selectedPages,((BookViewActivity)ctx).currentBook).execute();
                    ((BookViewActivity)ctx).rlTabsLayout.setVisibility(View.INVISIBLE);
                    ((BookViewActivity)ctx).bkmarkLayout.setVisibility(View.INVISIBLE);
                    ((BookViewActivity)ctx).wv_advSearch.setVisibility(View.INVISIBLE);
     //				canvasView.setVisibility(View.INVISIBLE);
  //				twoDScroll.setVisibility(View.INVISIBLE);
  //				drawingView.setVisibility(View.INVISIBLE);
                    ((BookViewActivity)ctx).bgImgView.setVisibility(View.VISIBLE);

}
                //bookViewActivity.instantiateNewPage(pageNumber, 0);

                //new takeAndSaveScreenshot(String.valueOf(pageNumber)).execute();
               // bookViewActivity.pageListView.invalidateViews();
                //bookViewActivity.pageListView.smoothScrollToPosition(pageNumber - 1);

            }
        });
        addNewPageDialog.show();

    }

    public class StoreBookDialog extends BaseAdapter {

        String[] filesList;

        String filePath;


        public  StoreBookDialog(boolean bookSelected) {
            isBookSelected = bookSelected;
        }


        @Override
        public int getCount() {
            // if(!importMedia) {
            if (!isBookSelected) {
                return getAllStoreBooks();
            } else {
                return getAllStoreBookPages();
            }
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                //view = bookViewActivity.getLayoutInflater().inflate(R.layout.inflate_book_template, null);
                view = ((BookViewActivity)ctx).getLayoutInflater().inflate(R.layout.export_pagethumbnail, null);
            }
            RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.layout);
            ImageView bookImg = (ImageView) view.findViewById(R.id.grid_item_image);
            TextView tv_pageno = (TextView) view.findViewById(R.id.tv_pageno);
            CheckBox checkbox = (CheckBox) view.findViewById(R.id.checkBox);
            ImageView preview_layout = (ImageView) view.findViewById(R.id.btn_preview);

            if (!isBookSelected) {
                String FilesPath = Globals.TARGET_BASE_BOOKS_DIR_PATH;
                Book book = (Book) StoreBooks.get(position);
                Bitmap bitmap = BitmapFactory.decodeFile(FilesPath + book.getBookID() + "/FreeFiles/card.png");
                if (bitmap==null) {
                    String thumbnailImagePath =FilesPath + book.getBookID()+"/"+"FreeFiles/frontThumb.png";
                    bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
                }
                if(!checkBookSubscribedForClient(book.getClientID())) {        // if(!book.getPurchaseType().equals("subscribed")) {
                    layout.setAlpha(0.5f);
                }else{
                    layout.setAlpha(1.0f);
                }
                bookImg.setImageBitmap(bitmap);
                tv_pageno.setText(book.getBookTitle());
                checkbox.setVisibility(View.GONE);
                preview_layout.setVisibility(View.GONE);
            } else {
                final PageNumbers page=pages.get(position);
                String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + SelectedBook.getBookID();
                String coverAndEndPage = Globals.TARGET_BASE_BOOKS_DIR_PATH + SelectedBook.getBookID() + "/" + "FreeFiles/";
                if (position == 0) {
                    String coverPage = coverAndEndPage + "card.png";
                    if (!new File(coverPage).exists()) {
                        coverPage = coverAndEndPage + "frontThumb.png";
                    }
                    if (new File(coverPage).exists()) {
                        Bitmap bitmap = BitmapFactory.decodeFile(coverPage);
                        bookImg.setImageBitmap(bitmap);
                    }
                    tv_pageno.setText(R.string.cover_page);
                } else if (position == SelectedBook.getTotalPages() - 1) {
                    String backThumb = thumbnailImagePath + "/thumbnail_end.png";
                    if (!new File(backThumb).exists()) {
                        backThumb = coverAndEndPage + "backThumb.png";
                    }
                    if (new File(backThumb).exists()) {
                        Bitmap bitmap = BitmapFactory.decodeFile(backThumb);
                        bookImg.setImageBitmap(bitmap);
                    }
                    tv_pageno.setText(R.string.end_page);
                } else {
                    int pNo=position+1;
                    String Pages = thumbnailImagePath + "/thumbnail_"+pNo+ ".png";

                    if (new File(Pages).exists()) {
                        Bitmap bitmap = BitmapFactory.decodeFile(Pages);
                        bookImg.setImageBitmap(bitmap);
                    }
                    tv_pageno.setText("Page: "+pNo);
                }
                preview_layout.setVisibility(View.VISIBLE);
                if (page.isChecked()) {
                    if (position!= 0) {
                        checkbox.setChecked(true);
                        layout.setBackgroundResource(R.color.hovercolor);
                    }
                } else {
                    checkbox.setChecked(false);
                    layout.setBackgroundResource(R.drawable.list_selector_background_disabled);
                    //layout.setAlpha(0.40f);
                }
                preview_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!wv_pages.isShown() && !page.getStoreId().contains("P")) {
                            System.out.println("Hellooo");
                            wv_pages.setVisibility(View.VISIBLE);
                            grid_pages.setVisibility(View.GONE);
                            btnSegmentGroup.setVisibility(View.GONE);
                            int pageNo = position + 1;
                            File htmlFile=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + SelectedBook.get_bStoreID() + "Book/" + pageNo + ".htm");
                            String srcString = UserFunctions.decryptFile(htmlFile);
                            String filePath ="file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+SelectedBook.get_bStoreID()+"Book/";
                            wv_pages.loadDataWithBaseURL(filePath, srcString, "text/html", "utf-8", null);
                           // wv_pages.loadUrl();
                        }
                    }
                });

            }
            return view;

        }
    }


    private int getAllStoreBooks() {
        StoreBooks = new ArrayList<Object>();
        for (int i = 0; i < gridShelf.bookListArray.size(); i++) {
            Book storeBook = (Book) gridShelf.bookListArray.get(i);
            if(storeBook.getIsDownloadCompleted().equals("downloaded")) {
                if (all && storeBook.is_bStoreBook()  ) {
                    if(storeBook.get_bStoreID().contains("M")||storeBook.get_bStoreID().contains("P")) {
                        StoreBooks.add(storeBook);
                    }
                } else if (allowed && storeBook.is_bStoreBook() && checkBookSubscribedForClient(storeBook.getClientID())) {
                    if(storeBook.get_bStoreID().contains("M") ||storeBook.get_bStoreID().contains("P")) {
                        StoreBooks.add(storeBook);
                    }
                }else if (storeBook.is_bStoreBook() && not_allowed && !checkBookSubscribedForClient(storeBook.getClientID())) {
                    if(storeBook.get_bStoreID().contains("M") ||storeBook.get_bStoreID().contains("P")) {
                        StoreBooks.add(storeBook);
                    }
                }
            }
        }
        return StoreBooks.size();
    }

    private int getAllStoreBookPages(){
        pages=new ArrayList<PageNumbers>();

        if(SelectedBook!=null){
            for(int i=0;i<SelectedBook.getTotalPages();i++){
                int pNo=i+1;
                boolean exist = false;
                PageNumbers page=new PageNumbers(((BookViewActivity)ctx));
                page.setpageNumber(pNo);
                page.setBookID(SelectedBook.getBookID());
                page.setBook(SelectedBook);
                for (PageNumbers pageselected : selectedPages) {
                    if(pageselected.getpageNumber()==page.getpageNumber() && page.getBookID()==pageselected.getBookID()) {
                        page.setChecked(true);
                        exist=true;
                        break;
                    }
                }
                if(!exist){
                    page.setChecked(false);
                }
                page.setStoreId(SelectedBook.get_bStoreID());
                pages.add(page);

            }
        }
       /*if(pages.size()>0 &&selectedPages.size()>0){
           for(PageNumbers Page:pages){
               for (PageNumbers pageselected : selectedPages) {
                if(pageselected.getpageNumber()==Page.getpageNumber()) {
                    Page.setChecked(true);
                }
              }
           }
        }*/
        return pages.size();
    }
    private boolean checkBookSubscribedForClient(String productID){
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(((BookViewActivity)ctx));
        ArrayList<HashMap<String,String>> existList = null;
        try {
            existList = (ArrayList<HashMap<String,String>>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.SUBSLIST, ObjectSerializer.serialize(new ArrayList<HashMap<String,String>>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (UserFunctions.checkClientIdExist(existList,productID)){
            return true;
        }else{
            return false;
        }
    }

    public  class AddingPagestoExistingNormalBook extends AsyncTask<Void, Void, Void> {

       // Book currentbook;
        ArrayList<PageNumbers> PageList;
        int totalPages;
        int existingPages;
        JSONArray jsonArray;
        Book SelectedBook;
        String filePath;
        boolean selectedispdf;
        private ProgressDialog progresdialog;
        public AddingPagestoExistingNormalBook(ArrayList<PageNumbers> selectedPageList, Book currentBook) {
            PageList = selectedPageList;
            SelectedBook=currentBook;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(ctx instanceof  BookViewReadActivity) {
                progresdialog = ProgressDialog.show( ((BookViewReadActivity)ctx), "", ((BookViewReadActivity)ctx).getResources().getString(R.string.please_wait), true);
            }else if(ctx instanceof  MainActivity) {
                progresdialog = ProgressDialog.show( ((MainActivity)ctx), "", ((MainActivity)ctx).getResources().getString(R.string.please_wait), true);
            }
            filePath=Globals.TARGET_BASE_BOOKS_DIR_PATH +SelectedBook.getBookID()+"/FreeFiles/"+"malzamah.json";
            jsonArray = new JSONArray();
            malzamahBookDetails=new ArrayList<String>();
            if(new File(filePath).exists()){
                jsonArray= readJsonFile(filePath);
               }
            //progresdialog = ProgressDialog.show(context, "", context.getResources().getString(R.string.please_wait), true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            //String[]str=bookDetails.split("##");
            existingPages=SelectedBook.getTotalPages();
            totalPages=existingPages+PageList.size();
            int pageNo = 0;
            String bookXml = null;


            int bookId= SelectedBook.getBookID();
            String query = "update books set totalPages='"+totalPages+"' where BID='"+bookId+"'";
            db.executeQuery(query);

            db.executeQuery("update objects set pageNO='" + totalPages + "' where BID='" + bookId + "' and pageNO='" + existingPages + "'");
            db.executeQuery("update books set isMalzamah='yes' where BID='"+bookId+"'");
            SelectedBook.setIsMalzamah("yes");
            File f=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId+ "/Book.xml");
            if(f.exists()){
                String sourceString= UserFunctions.readFileFromPath(f);
                sourceString=sourceString.replace("<Cover2..","");
                String[] actualContent=sourceString.split("<Cover2");
                bookXml=actualContent[0];
            }
            //changeEndpageFiles(bookId,existingPages,totalPages);
            //File f=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH +"C"+bookId + "Book/Book.xml");
            // String sourceString= UserFunctions.readFileFromPath(f);
            //sourceString=sourceString.replace("<Cover2..","");
           // String[] actualContent=sourceString.split("<Cover2");
           // String bookXml=actualContent[0];
            for (int i = 0; i < PageList.size(); i++) {
                PageNumbers selectedPage = PageList.get(i);
                pageNo = existingPages+i;
               // UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentbook.get_bStoreID() + "Book/fpimages/" + selectedPage.getpageNumber() + ".jpg", Globals.TARGET_BASE_BOOKS_DIR_PATH + "C" + bookId + "Book/fpimages/" + pageNo + ".jpg");
               // UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentbook.get_bStoreID() + "Book/" + selectedPage.getpageNumber() + ".htm", Globals.TARGET_BASE_BOOKS_DIR_PATH + "C" + bookId + "Book/" + pageNo + ".htm");
                //File file = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH +"C"+bookId + "Book/" + pageNo + ".htm");
                //changeHtmlFile(file, bookId,selectedPage.getStoreId(),selectedPage.getBookID());
                int pNo = selectedPage.getpageNumber();


                JSONObject jsonObject=AddingPagestojsonArray(selectedPage,pageNo,bookId);
                if(bookXml!=null&&!bookXml.contains("")) {
                    bookXml = generateBookXML(totalPages, pageNo, bookXml, Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId + "/Book.xml");
                }
                jsonArray.put(jsonObject);
                File file = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH +selectedPage.getStoreId()+ "Book/" +pNo+ ".htm");
                if(file.exists()) {
                    changeHtmlFile(file, bookId, selectedPage.getStoreId(), selectedPage.getBookID());
                }
                //new SavingPageDetails(selectedPage).execute();
                if(selectedPage.getStoreId().contains("P")) {
                    //createWebview(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + selectedPage.getBookID() + "/thumbnail_" + pNo + ".png", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/" + pageNo + ".pdf",pageNo);
                    savingObjectsIntoDb(pageNo,bookId);
                    UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedPage.getBookID() +"/FreeFiles/"+pNo+ ".png", Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId +"/FreeFiles/" + pageNo +".png");
                    generatecoverPageHtml(pageNo, Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId+"/"+pageNo+".htm");
                    selectedispdf=true;
                }else {
                    copyingImageInToNewBookPath(pNo, bookId, pageNo, selectedPage.getBookID(), selectedPage.getStoreId());
                }
                UserFunctions.copyFiles(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + selectedPage.getBookID() + "/thumbnail_" + pNo + ".png", Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_" + pageNo + ".png");
                if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+selectedPage.getStoreId()+"Book/FreeFiles/css").exists()){
                    try {
                        UserFunctions.copyDirectory(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+selectedPage.getStoreId()+"Book/FreeFiles/css"),new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId+"/FreeFiles/"+selectedPage.getStoreId()+"/css"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

             }
            JSONArray bookDetailsJson=getBookDetailsJson();
            JSONObject jsonData=new JSONObject();
            try {
               // String absPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
              //  String path= absPath+"/Download/";

                jsonData.put("malzamah",jsonArray);
                jsonData.put("bookDetails", bookDetailsJson);
                FileWriter file=new FileWriter(new File(filePath));
                file.write(jsonData.toString());
                file.flush();
                file.close();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String flipPath = "fpimages/" + totalPages + ".jpg";
            if(ctx instanceof  BookViewActivity) {
                ((BookViewActivity)ctx).pageCount = totalPages;
            }else{
                //((BookViewReadActivity)ctx).pageCount = totalPages;
            }
            //currentBook.setTotalPages(bookViewActivity.pageCount);

            SelectedBook.setTotalPages(totalPages);
            if(bookXml!=null) {
                creatingFile(bookXml, Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/Book.xml");
            }
//                    }
               // }
//            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            int pageNumber = existingPages;

            if(ctx instanceof BookViewActivity) {
                //update the cursor
                for (int i = existingPages; i < totalPages; i++) {
                    int cursorValue = i;
                    ((BookViewActivity) ctx).cursor.newRow().add(cursorValue).add("Page: " + cursorValue);
                    ((BookViewActivity) ctx).pageAdapter.changeCursor(((BookViewActivity) ctx).cursor);
                }
                ((BookViewActivity) ctx).currentPageNumber = String.valueOf(pageNumber);
                ((BookViewActivity) ctx).pageAdapter.notifyDataSetChanged();
                ((BookViewActivity) ctx).addPageNumberObject(pageNumber, 0);
               // ((BookViewActivity) ctx).checkForEnrichments(pageNumber, 0);
                ((BookViewActivity) ctx).instantiateNewPage(pageNumber, 0);
                // bookViewActivity.instantiateNewPage(cursorValue, 0);
                //bookViewActivity.pageListView.invalidateViews();
                ((BookViewActivity) ctx).pageListView.invalidateViews();
                //pageListView.invalidateViews();
                ((BookViewActivity) ctx).pageListView.smoothScrollToPosition(pageNumber - 1);
                 addNewPageDialog.dismiss();
            } else if(ctx instanceof BookViewReadActivity) {
                progresdialog.dismiss();
                UserFunctions.DisplayAlertDialog(((BookViewReadActivity) ctx), R.string.malzamah_updated, R.string.done);
                // popoverView.dissmissPopover(true);
                ((BookViewReadActivity)ctx).exportEnrPopover.dismiss();
            }else if(ctx instanceof  MainActivity) {
                progresdialog.dismiss();
                UserFunctions.DisplayAlertDialog(((MainActivity) ctx), R.string.malzamah_updated, R.string.done);
                ((MainActivity)ctx).PopoverView.dissmissPopover(true);
            }


        }
    }
    private JSONArray getBookDetailsJson(){
        JSONArray jsonData = new JSONArray();
        for(int i=0;i<malzamahBookDetails.size();i++){
            String data=malzamahBookDetails.get(i);
            String split[]=data.split("##");
            JSONObject jsonSub=new JSONObject();
            try {
                jsonSub.put("description", split[0]);
                jsonSub.put("purchaseType",split[1]);
                jsonSub.put("author",split[2]);
                jsonSub.put("totalPages",split[3]);
                jsonSub.put("title",split[4]);
                jsonSub.put("price",split[5]);
                jsonSub.put("storeID",split[6]);
                jsonSub.put("domainURL",split[7]);
                jsonSub.put("language",split[8]);
                jsonSub.put("imageURL",split[9]);
                jsonSub.put("isEditable",split[10]);
                jsonSub.put("downloadURL",split[11]);
                jsonData.put(jsonSub);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonData;
    }
    private JSONArray readJsonFile(String filePath){
        File file=new File(filePath);
        String jsonStr=null;
        JSONArray jsonarray = null;
        try {
            FileInputStream stream = new FileInputStream(file);
            FileChannel fc = stream.getChannel();
            try {
                stream = new FileInputStream(file);
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                jsonStr = Charset.defaultCharset().decode(bb).toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }

            JSONObject jsonObject=new JSONObject(jsonStr);
            JSONArray data=jsonObject.getJSONArray("malzamah");
            JSONArray bookDetails=jsonObject.getJSONArray("bookDetails");
            malzamahBookDetails=getBookDetails(bookDetails);
            //String  d= data.toString().replace("[","");
            jsonarray = new JSONArray();
            for(int i=0;i<data.length();i++){
                JSONObject jsonSub = data.getJSONObject(i);
                jsonarray.put(jsonSub);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

            return jsonarray;
    }

    private ArrayList<String> getBookDetails(JSONArray bookDetails){
        ArrayList<String>malzamahBookDetails=new ArrayList<String>();
        for(int i=0;i<bookDetails.length();i++){
            try {
                JSONObject jsonSub = bookDetails.getJSONObject(i);

                String book= jsonSub.get("description")+"##"+jsonSub.get("purchaseType")+"##"+jsonSub.get("author")+"##"+jsonSub.get("totalPages")+"##"+jsonSub.get("title")+"##"+jsonSub.get("price")+"##"+jsonSub.get("storeID")+"##"+jsonSub.get("domainURL")+"##"+jsonSub.get("language")+"##"+jsonSub.get("imageURL")+"##"+jsonSub.get("isEditable")+"##"+jsonSub.get("downloadURL");
                malzamahBookDetails.add(book);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return malzamahBookDetails;
    }
    private JSONObject AddingPagestojsonArray(PageNumbers selectedPage, int pageNo, int bookId) {
        String domainUrl=null;
        JSONObject groups = new JSONObject();
        int pNo=selectedPage.getpageNumber();
        try {
            groups.put("thumbPath", "Book_" + selectedPage.getBookID() + "/thumbnail_" + pNo + ".png");
            groups.put("BID", bookId);
            groups.put("htmlPath",selectedPage.getStoreId()+"Book/"+pNo+".htm");
            groups.put("page",pageNo);
            if(ctx instanceof BookViewActivity) {
                ((BookViewActivity) ctx).db.executeQuery("insert into malzamah(BID,pageNo, sourceStoreID,sourceHtmlPath,sourceThumbPath)" +
                        "values('" + bookId + "','" + pageNo + "','" + selectedPage.getStoreId() + "','" + selectedPage.getStoreId() + "Book/" + pNo + ".htm" + "','" + "Book_" + selectedPage.getBookID() + "/thumbnail_" + pNo + ".png" + "')");
                domainUrl=((BookViewActivity) ctx).db.getStoreBookDomainUrl(selectedPage.getStoreId(),selectedPage.getBookID());

            }else if(ctx instanceof BookViewReadActivity){
                ((BookViewReadActivity) ctx).db.executeQuery("insert into malzamah(BID,pageNo, sourceStoreID,sourceHtmlPath,sourceThumbPath)" +
                        "values('" + bookId + "','" + pageNo + "','" + selectedPage.getStoreId() + "','" + selectedPage.getStoreId() + "Book/" + pNo + ".htm" + "','" + "Book_" + selectedPage.getBookID() + "/thumbnail_" + pNo + ".png" + "')");
                domainUrl=((BookViewReadActivity) ctx).db.getStoreBookDomainUrl(selectedPage.getStoreId(),selectedPage.getBookID());
            }
            boolean bookExist=false;
            String bookDetails=selectedPage.getBook().get_bDescription()+"##"+selectedPage.getBook().getPurchaseType()+"##"+selectedPage.getBook().getBookAuthorName()+"##"+selectedPage.getBook().getTotalPages()+"##"+selectedPage.getBook().getBookTitle()+"##"+selectedPage.getBook().getPrice()+"##"+selectedPage.getBook().get_bStoreID()+"##"+domainUrl+"##"+selectedPage.getBook().getBookLangugae()+"##"+selectedPage.getBook().getImageURL()+"##"+selectedPage.getBook().getIsEditable()+"##"+selectedPage.getBook().getDownloadURL();
            if(malzamahBookDetails!=null&&malzamahBookDetails.size()>0){
                for(int m=0;m<malzamahBookDetails.size();m++){
                    String data=malzamahBookDetails.get(0);
                    String split[]=data.split("##");
                    if(split[6].equals(selectedPage.getBook().get_bStoreID())){
                        bookExist=true;
                        break;
                    }
                }
            }
            if(!bookExist){
                malzamahBookDetails.add(bookDetails);
            }
         } catch (JSONException e){
            e.printStackTrace();
        }
        return groups;
    }

    private void copyingImageInToNewBookPath(int pageNumber, int newBookId, int pageNo,int bookID,String StoreId) {
        ArrayList<HTMLObjectHolder> originalStoreObjectList = null;
        int maxObjUniqueId=0;
        ArrayList<QuizSequenceObject> enrQuizSequenceObjectArray = null;
        String pNo = String.valueOf(pageNumber);
        maxObjUniqueId = db.getMaxUniqueRowID("objects");
        originalStoreObjectList =db.getAllObjectsFromHomePageToCreateEnrichment(pNo, bookID, 0);

        for (HTMLObjectHolder htmlObjectHolder : originalStoreObjectList) {
            maxObjUniqueId = maxObjUniqueId + 1;
            int BID = htmlObjectHolder.getObjBID();
            int objPageNo = Integer.parseInt(htmlObjectHolder.getObjPageNo());
            String objLocation = htmlObjectHolder.getObjXPos() + "|" + htmlObjectHolder.getObjYPos();
            String objSize;
            String objType = htmlObjectHolder.getObjType();
            String objContent = htmlObjectHolder.getObjContentPath();
            int objSequentialId = htmlObjectHolder.getObjSequentialId();
            String objScalePageTofit = htmlObjectHolder.getObjScalePageToFit();
            String objLock = htmlObjectHolder.getObjLocked();
            int objUniqueId = htmlObjectHolder.getObjUniqueRowId();
            int objEnrichId = htmlObjectHolder.getObjEnrichID();
            if (objType.equals(Globals.OBJTYPE_PAGEBG) || objType.equals(Globals.OBJTYPE_MINDMAPBG)) {
                objSize = htmlObjectHolder.getObjSize();
            } else {
                objSize = htmlObjectHolder.getObjWidth() + "|" + htmlObjectHolder.getObjHeight();
            }
            // String objContent = htmlObjectHolder.getObjContentPath();
            UserFunctions.createNewDirectory( Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images");
            String newObjContent = null;
            if (objType.equals("WebText") || objType.equals("TextSquare") || objType.equals("TextRectangle") || objType.equals("TextCircle") || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT) || objType.equals(Globals.objType_pageNoText) || objType.equals("WebTable") || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
                if (objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
                    ArrayList<QuizSequenceObject> homeQuizSequenceObjectArray = db.getQuizSequenceArray(objUniqueId);

                    for (int i = 0; i < homeQuizSequenceObjectArray.size(); i++) {
                        db.executeQuery("insert into tblMultiQuiz(quizId) values('" + maxObjUniqueId + "')");

                    }

                    enrQuizSequenceObjectArray =db.getQuizSequenceArray(maxObjUniqueId);
                    for (int j = 0; j < enrQuizSequenceObjectArray.size(); j++) {
                        QuizSequenceObject homeQuizSequenceObject = homeQuizSequenceObjectArray.get(j);
                        int homeQuizUniqueId = homeQuizSequenceObject.getUniqueid();
                        int homeQuizSectionId = homeQuizSequenceObject.getSectionId();
                        QuizSequenceObject enrQuizSequenceObject = enrQuizSequenceObjectArray.get(j);
                        int enrQuizUniqueId = enrQuizSequenceObject.getUniqueid();
                        int enrQuizSectionId = enrQuizSequenceObject.getSectionId();

                        if (objContent.contains("q"+homeQuizSectionId+"_img_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_a_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_a_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_a_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_b_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_b_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_b_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_c_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_c_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_c_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_d_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_d_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_d_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_e_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_e_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_e_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_f_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_f_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_f_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }

                        objContent = objContent.replace("previousquestion:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "previousquestion:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
                        objContent = objContent.replace("deletequestion:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "deletequestion:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
                        objContent = objContent.replace("addquestions:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "addquestions:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
                        objContent = objContent.replace("nextquestion:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "nextquestion:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("Title"+homeQuizSectionId+"", "Title"+enrQuizSectionId+"");
                        objContent = objContent.replace("quizTitle_"+homeQuizUniqueId+"", "quizTitle_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("pTitle_"+homeQuizUniqueId+"", "pTitle_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("quiz_question_"+homeQuizUniqueId+"", "quiz_question_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("pQuiz_"+homeQuizUniqueId+"", "pQuiz_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("quiz_options_"+homeQuizUniqueId+"", "quiz_options_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_result_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_result_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_a_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_a_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"a_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"a_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("btnCheckAnswer"+homeQuizSectionId+"_"+homeQuizUniqueId+"", "btnCheckAnswer"+enrQuizSectionId+"_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_b_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_b_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"b_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"b_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_c_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_c_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"c_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"c_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_d_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_d_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"d_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"d_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_e_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_e_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"e_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"e_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_f_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_f_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"f_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"f_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("quiz_checkanswer_"+homeQuizUniqueId+"", "quiz_checkanswer_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("btnNext"+homeQuizSectionId+"_"+homeQuizUniqueId+"", "btnNext"+enrQuizSectionId+"_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_a_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_a_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_b_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_b_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_c_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_c_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_d_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_d_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_e_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_e_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_f_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_f_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("results_container_"+homeQuizUniqueId+"", "results_container_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("results_"+homeQuizUniqueId+"", "results_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("quiz_result_cont_"+homeQuizUniqueId+"", "quiz_result_cont_"+enrQuizUniqueId+"");

                        objContent=objContent.replace("/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + BID + "/", "/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + newBookId + "/");

                        newObjContent = objContent.replace("'", "''");
                    }
                } else {

                    objContent=objContent.replace("/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + BID + "/", "/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + newBookId + "/");
                    newObjContent = objContent.replace("'", "''");
                }
            }else  if (objType.equals("Image") || objType.equals("DrawnImage")) {
                newObjContent = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBookId + "/images/" + maxObjUniqueId + ".png";
                UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + newBookId + "/images");
                UserFunctions.copyFiles(objContent, newObjContent);
                // bookView.db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + newBookId + "', '" + pageNo + "', '" + objLocation + "', '" + objSize + "', '" + newObjContent + "', '" + objSequentialId + "', '" + objScalePageTofit + "', '" + objLock + "', '0')");

            }
            else if (objType.equals("EmbedCode")) {
                newObjContent = objContent.replace("'", "''");
            } else if (objType.equals("Audio")) {
                newObjContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/"+maxObjUniqueId+".m4a";
                UserFunctions.copyFiles(objContent, newObjContent);
            } else if (objType.equals("IFrame")) {
                newObjContent = objContent;
            } else if (objType.equals("YouTube")) {
                if (objContent.contains("youtube.com")) {
                    newObjContent = objContent;
                } else {
                    newObjContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/"+maxObjUniqueId+".mp4";
                    UserFunctions.copyFiles(objContent, newObjContent);
                    String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/"+maxObjUniqueId+".png";
                    String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/"+objUniqueId+".png";
                    UserFunctions.copyFiles(homeImgContent, enrImgContent);
                }
            }
            if(objScalePageTofit.contains("css")){
                String[] split=objScalePageTofit.split("##");
                String main="FreeFiles/"+StoreId+"/css/main.css";
                String normalize="FreeFiles/"+StoreId+"/css/normalize.css";
                objScalePageTofit=objScalePageTofit.replace("FreeFiles/css/normalize.css",normalize);
                objScalePageTofit=objScalePageTofit.replace("FreeFiles/css/main.css",main);
            }
           db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + newBookId + "', '" + pageNo + "', '" + objLocation + "', '" + objSize + "', '" + newObjContent + "', '" + objSequentialId + "', '" + objScalePageTofit + "', '" + objLock + "', '0')");


        }

    }

    private void changeHtmlFile(File file, int bookId,String storeId,int storeBookId) {
        // String sourceString = readFileFromSDCard(file);
        String sourceString = UserFunctions.decryptFile(file);
        sourceString.replace("file:///data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + storeId + "/", "file:///data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + bookId + "/");
        copyingImages(sourceString, bookId, storeId,storeBookId);

    }

    private void copyingImages(String sourceSting, int bookId,String storeId,int storeBookId) {
        if (sourceSting.contains("src")) {
            String[] splitStr = sourceSting.split("src");
            if (splitStr.length > 1) {
                for (int i = 0; i < splitStr.length; i++) {
                    if (i > 0) {
                        String[] path1 = splitStr[i].split("\"");
                        String[] folderPath = path1[1].split("/");
                        if(folderPath.length>1) {
                            if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeBookId +"/" + folderPath[0]).exists()) {
                                UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + folderPath[0]);
                                if (folderPath.length > 2) {
                                    //UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + "C" +bookId+"Book/"+ folderPath[0]+"/"+folderPath[1]);
                                    UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + folderPath[0] + "/" + folderPath[1]);
                                }
                            }
                           // UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH +storeId + "Book/" + path1[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + "C" + bookId + "Book/" + path1[1]);
                           // UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + folderPath[0]);
                            if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"/"+ path1[1]).exists()) {
                                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + path1[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + path1[1]);
                            }
                        }else{
                            //UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + path1[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + "C" + bookId + "Book/" + path1[1]);
                            if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"/"+ path1[1]).exists()) {
                                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + path1[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + path1[1]);
                            }
                        }

                    }
                }
            }
        }
        if (sourceSting.contains("background")) {
            String[] splitStr = sourceSting.split("background");
            if (splitStr.length > 1) {
                for (int i = 0; i < splitStr.length; i++) {
                    if (i > 0) {
                        String[] p = splitStr[i].split("\"");
                        String[] folderPath = p[1].split("/");  //FreeFiles/front.png
                        if(folderPath.length>1) {
                            if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeBookId +"/" + folderPath[0]).exists()) {
                                //UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + "C" + bookId + "Book/" + folderPath[0]);
                                UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + folderPath[0]);
                                if (folderPath.length > 2) {
                                    //UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + "C" +bookId+"Book/"+ folderPath[0]+"/"+folderPath[1]);
                                    UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + folderPath[0] + "/" + folderPath[1]);
                                }
                            }
                           // UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + p[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + "C" + bookId + "Book/" + p[1]);
                            if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"/"+ p[1]).exists()) {
                                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + p[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + p[1]);
                            }
                        }else{
                            if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+storeBookId+"/"+ p[1]).exists()) {
                                // UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + p[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + "C" + bookId + "Book/" + p[1]);
                                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + p[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + p[1]);
                            }
                        }
                    }
                }
            }
        }
    }

    private String generateBookXML(int totalPages,int pageNo, String bookXml, String flipPath){
        String xml;
        if (pageNo == 1) {
            xml = bookXml.concat("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Book><Cover1 Flip=\""+flipPath+"\" Path=\"1.htm\" ETitle=\"Cover 1\" ATitle=\"1 الغلاف\"/>");
        }else if (pageNo == totalPages) {
            xml = bookXml.concat("<Cover2 Flip=\""+flipPath+"\" Path=\""+pageNo+".htm\" ETitle=\"Cover 2\"  ATitle=\"2 الغلاف\"/></Book>");
        } else {
            int tempPageNo = pageNo - 1;
            xml = bookXml.concat("<P"+tempPageNo+" Flip=\""+flipPath+"\" Path=\""+pageNo+".htm\"  ATitle=\""+tempPageNo+" صفحة\" ETitle=\"Page"+tempPageNo+"\"/>");

        }
        return xml;
    }


    private void generatecoverPageHtml(int pageNo, String path) {
        String html = "<html>\n" +
                "<head>\n" +
                "<meta http-equiv='Content-Type' content='text/html'; charset='utf-8'>\n" +
                "<title>Cover Page</title>\n" +
                "<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n" +
                "</meta>\n" +
                "</head>\n" +
                " <body topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" bottommargin=\"0\" marginheight=\"0\" marginwidth=\"0\">\n" +
                "<div id='bgImage' class=\"bgImage\" style='background-image:url(FreeFiles/"+pageNo+".png); background-repeat: no-repeat; -webkit-background-size:100%;width:100%; height:100%; left:0px; top:0px; position:absolute;' >\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";
        try {
            FileWriter fileWriter = new FileWriter(path);
            fileWriter.write(html);
            fileWriter.close();
            File file = new File(path);
            //UserFunctions.encryptFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void creatingFile(String sourceString,String path){
        try {
            FileWriter fileWriter = new FileWriter(path);
            fileWriter.write(sourceString);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   private void savingObjectsIntoDb(int pageNo, int newBookId){

        int BID = newBookId;
        int objPageNo = pageNo;
        String objLocation = 0+ "|" + 0;
        String objSize;
        String objType = "WebText";
        String objContent = "<div id=\"PDFcontent\"contenteditable=\"false\" style=\"background-color:transparent; box-sizing:border-box; padding:0px;\"><img src=\"FreeFiles/"+pageNo+".png\" width=\"100%\" height=\"100%\"></div>";
        int objSequentialId = db.getMaximumSequentialIdValue(pageNo, newBookId) + 1;
        String objScalePageTofit = "yes##0px##0px##0px##0px##undefined##undefined";
        String objLock = "false";
        //int objUniqueId = htmlObjectHolder.getObjUniqueRowId();
        int objEnrichId = 0;
        int width=Globals.getDeviceWidth();
        int height=Globals.getDeviceHeight();
        objSize=width+"|"+height;
        db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + newBookId + "', '" + pageNo + "', '" + objLocation + "', '" + objSize + "', '" + objContent + "', '" + objSequentialId + "', '" + objScalePageTofit + "', '" + objLock + "', '0')");

    }


}