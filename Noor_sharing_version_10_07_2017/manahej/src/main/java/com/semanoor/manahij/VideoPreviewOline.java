package com.semanoor.manahij;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

public class VideoPreviewOline extends Activity {

    String url;
    boolean isLocal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.preview_drive_video);
        url = getIntent().getStringExtra("url");
        isLocal = getIntent().getBooleanExtra("boolean",false);
        final VideoView iv_preview = (VideoView) findViewById(R.id.videoView);
        Button btn_back=(Button)findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
        MediaController mediaController = new MediaController(VideoPreviewOline.this);
        if (!isLocal) {
            Uri uri = Uri.parse(url);
            iv_preview.setVideoURI(uri);
        }else{
            iv_preview.setVideoPath(url);
        }
        iv_preview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                int width = mp.getVideoWidth();
                int height = mp.getVideoHeight();
                if (width > height) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                }
                iv_preview.start();
            }
        });
        iv_preview.setMediaController(mediaController);
        iv_preview.setZOrderOnTop(true);
        mediaController.setAnchorView(iv_preview);
    }
}
