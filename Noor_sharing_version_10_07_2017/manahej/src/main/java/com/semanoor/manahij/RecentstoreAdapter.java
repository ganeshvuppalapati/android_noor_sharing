package com.semanoor.manahij;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by karthik on 22-06-2016.
 */
public class RecentstoreAdapter extends BaseAdapter {
    String Store_ImagesPath = Globals.TARGET_BASE_FILE_PATH + "storeImages";
    Context _context;
    ArrayList<StoreList> recent_storeList;

    public RecentstoreAdapter(Context context, ArrayList<StoreList> recent_storeListData){
        this._context = context;
        this.recent_storeList = recent_storeListData;
    }

    public int getCount() {
        return recent_storeList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }


    class ViewHolder {
        ImageView img_storeView;
        TextView tv_storeName;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        //View indexlistview = convertView;
        ViewHolder holder;
        View view = convertView;
        if (convertView == null) {
            Typeface font = Typeface.createFromAsset(_context.getAssets(), "fonts/FiraSans-Regular.otf");
            UserFunctions.changeFont(parent, font);
            if (_context instanceof StoreTabViewFragment){
                view = ((StoreTabViewFragment)_context).getLayoutInflater().inflate(R.layout.recent_storeitems, null);
            }else{
                view = ((StoreViewActivity)_context).getLayoutInflater().inflate(R.layout.recent_storeitems, null);
            }

            holder = new ViewHolder();
            assert view != null;
            holder.img_storeView = (ImageView) view.findViewById(R.id.iv_store);
            holder.tv_storeName = (TextView) view.findViewById(R.id.tv_storename);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (view != null) {
            StoreList store = recent_storeList.get(position);
            Glide.with(_context).load(store.getThumbUrl())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.img_storeView);
            holder.tv_storeName.setText(store.getNameEn());
        }
        return view;
    }
}