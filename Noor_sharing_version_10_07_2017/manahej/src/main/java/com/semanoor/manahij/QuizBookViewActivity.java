package com.semanoor.manahij;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.ptg.views.CircleButton;
import com.semanoor.source_manahij.QuizEnrichment;
import com.semanoor.source_manahij.QuizPage;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Locale;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class QuizBookViewActivity extends Activity implements OnSeekBarChangeListener{

	private Book currentBook;
	private String currentBookPath;
	public static String bookName; 
	private ArrayList<QuizPage> quizPageList;
	private ViewPager viewPager;
	public LinearLayout enrTabsLayout;
	private WebView currentWebView;
	static String filePath = new String();
	public LinearLayout currentEnrTabsLayout;
	private int currentPageNumber=1;
	public static int pageCount;
	private Button libraryButton;
	private int sb_pNo;
	public GridShelf gridShelf;

	/* Page Slider */
	public static SeekBar sb;
	public static TextView tv_pageNumber;
	
	private boolean adsPurchased;
	private InterstitialAd interstitial;
	
	private DatabaseHandler db;
	private int lastViewedPageNo;
	private CircleButton back_Btn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		loadLocale();
		setContentView(R.layout.activity_quiz_book_view);

		gridShelf = (GridShelf) getIntent().getSerializableExtra("Shelf");

	    db = DatabaseHandler.getInstance(this);
		Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
		ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

		UserFunctions.changeFont(rootView,font);
		libraryButton = (Button)findViewById(R.id.btnLibrary);
		back_Btn= (CircleButton) findViewById(R.id.btn_back);
		back_Btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		libraryButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		currentBook = (Book) getIntent().getSerializableExtra("Book");
		currentPageNumber = currentBook.get_lastViewedPage();
		if (currentPageNumber == 0) {
			currentPageNumber = 1;
		}
		bookName = currentBook.get_bStoreID()+"Book";
		currentBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookName+"/";
		pageCount = currentBook.getTotalPages();

		parseQuizBookXml();

		viewPager = (ViewPager) findViewById(R.id.viewPager);
		tv_pageNumber=(TextView) findViewById(R.id.tv_pagenumber);
		sb = (SeekBar)findViewById(R.id.sb_pageslider);
		sb.setOnSeekBarChangeListener(this);
		sb.setMax(pageCount-1);
		sb.setProgress(currentPageNumber-1);
		viewPager.setAdapter(new ViewAdapter());
		viewPager.setCurrentItem(currentPageNumber-1);
		
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				currentPageNumber = arg0+1;
				lastViewedPageNo = arg0+1;
				sb.setProgress(arg0);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				
			}
		});
		
		SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
		adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);

		if (!adsPurchased) {
			interstitial = new InterstitialAd(QuizBookViewActivity.this);
			interstitial.setAdUnitId(Globals.INTERSTITIAL_AD_UNIT_ID);
			AdRequest adRequest = new AdRequest.Builder()
			//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
			//.addTestDevice("505F22CBB28695386696655EED14B98B") 
			.build();
			interstitial.loadAd(adRequest);
		}
	}
	
	/**
	 * Load Locale language from shared preference and change language in the application
	 */
	private void loadLocale() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String language = prefs.getString(Globals.languagePrefsKey, "en");
		changeLang(language);
	}
	
	/**
	 * change language in the application
	 * @param language
	 */
	private void changeLang(String language) {
		Locale myLocale = new Locale(language);
		Locale.setDefault(myLocale);
		android.content.res.Configuration config = new android.content.res.Configuration();
		config.locale = myLocale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
	}

	private class ViewAdapter extends PagerAdapter{

		private HorizontalScrollView enrScrollView;
		private RelativeLayout rlTabsLayout;

		@Override
		public int getCount() {
			return quizPageList.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return (view == object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			currentPageNumber = position+1;
			View view = getLayoutInflater().inflate(R.layout.quiz_inflate, null);
			rlTabsLayout = (RelativeLayout) view.findViewById(R.id.rl_tabs_layout);
			enrScrollView = (HorizontalScrollView) view.findViewById(R.id.enr_scrollView);
			enrTabsLayout = (LinearLayout) view.findViewById(R.id.enr_linearLayout);
			final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
			progressBar.setVisibility(View.VISIBLE);

			WebView webView = (WebView) view.findViewById(R.id.quiz_webView);
			webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			webView.getSettings().setJavaScriptEnabled(true);
			webView.setWebViewClient(new WebViewClient(){
				@Override
				public void onPageFinished(WebView view, String url) {
					super.onPageFinished(view, url);
					progressBar.setVisibility(View.GONE);
				}
			});
			webView.setWebChromeClient(new WebChromeClient());
			webView.getSettings().setAllowContentAccess(true);
			webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
			webView.getSettings().setAllowFileAccess(true);
			webView.getSettings().setPluginState(PluginState.ON);
			webView.getSettings().setLoadWithOverviewMode(true);
			webView.getSettings().setUseWideViewPort(true);
			webView.getSettings().setSupportZoom(true);
			webView.getSettings().setDomStorageEnabled(true);
			webView.getSettings().setBuiltInZoomControls(true);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
				webView.getSettings().setAllowFileAccessFromFileURLs(true);
				webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
			}
			//webView.getSettings().setDisplayZoomControls(false);
			/*webView.setWebViewClient(new WebViewClient(){
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					return true;
					//return super.shouldOverrideUrlLoading(view, url);
				}
			});*/
			QuizPage quizPage = quizPageList.get(position);
			ArrayList<QuizEnrichment> quizEnrichmentList = quizPage.getQuizEnrichmentList();

			QuizEnrichment enrichments = quizEnrichmentList.get(0);
			String enrichmentPath = "file:///"+currentBookPath+enrichments.getQuizEnrichmentPath();
			loadEnrichments(quizEnrichmentList);
			File file = new File(currentBookPath+enrichments.getQuizEnrichmentPath());
			String sourceString = UserFunctions.decryptFile(file);
			String enrPath = enrichments.getQuizEnrichmentPath().substring(0,enrichments.getQuizEnrichmentPath().lastIndexOf("/"));
			//webView.clearCache(true);
			webView.loadDataWithBaseURL("file:///"+currentBookPath+enrPath+"/", sourceString, "text/html", "utf-8", null);
			((ViewPager)container).addView(view, 0);
			return view;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object){	
			((ViewPager)container).removeView((View) object);
		}

		@Override
		public void setPrimaryItem(ViewGroup container, int position,
				Object object) {
			super.setPrimaryItem(container, position, object);
			RelativeLayout view = (RelativeLayout) object;
			currentWebView = (WebView) view.getChildAt(1);
			final RelativeLayout rl = (RelativeLayout) view.getChildAt(0);
			HorizontalScrollView sv = (HorizontalScrollView) rl.getChildAt(0);
			currentEnrTabsLayout = (LinearLayout) sv.getChildAt(0);
			currentWebView.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					//final int action = event.getAction();
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:{
						break;
					}
					case MotionEvent.ACTION_UP:{
						if(sb.getVisibility()==View.VISIBLE){
							tv_pageNumber.setVisibility(View.INVISIBLE);
							sb.setVisibility(View.INVISIBLE);
							//rl.setVisibility(View.INVISIBLE);
						}else{
							tv_pageNumber.setVisibility(View.VISIBLE);
							sb.setVisibility(View.VISIBLE);
							//rl.setVisibility(View.VISIBLE);
						}
						break;
					}
					default:
						break;
					}
					return false;
				}
			});
			//currentDisplayedPageNo = position + 1;
		}
	}
	
	public void loadEnricmentPage(String path){
		String enrichmentPath = "file:///"+currentBookPath+path;
		currentWebView.loadUrl(enrichmentPath);
	}

	/**
	 * check for enrichment tabs and load it in enrichment list array
	 * @param quizEnrichmentList
	 */
	public void loadEnrichments(ArrayList<QuizEnrichment> quizEnrichmentList) {
		for (int i = 0; i < quizEnrichmentList.size(); i++) {
			QuizEnrichment enrichments = quizEnrichmentList.get(i);
			enrichments.createEnrichmentTabs(QuizBookViewActivity.this,i);
		}
	}

	/**
	 * Make Enrich Button to be selected
	 * @param v
	 */
	public void makeEnrichedBtnSlected(View v){
		for (int i = 0; i < currentEnrTabsLayout.getChildCount(); i++) {
			View view = currentEnrTabsLayout.getChildAt(i);
			if (view == v) {
				v.setSelected(true);
			} else {
				view.setSelected(false);
			}
		}
	}

	public void parseQuizBookXml() {

		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			DefaultHandler handler = new DefaultHandler(){
				String currentNode = "";
				QuizPage quizPage;
				ArrayList<QuizEnrichment> quizEnrichmentList;
				public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException 
				{
					if(qName.contentEquals("Book")){
						quizPageList = new ArrayList<QuizPage>();
					}
					if(qName.contains("P"))
					{
						String ETitle = attributes.getValue("ETitle");
						String ATitle = attributes.getValue("ATitle");
						quizPage  = new QuizPage();
						quizPage.setQuizATitle(ATitle);
						quizPage.setQuizETitle(ETitle);
						quizEnrichmentList = new ArrayList<QuizEnrichment>();
					}else if(qName.contains("Enr")){
						String enrTitle = attributes.getValue("Title");
						String enrPath = attributes.getValue("Path");
						QuizEnrichment quizEnrichment = new QuizEnrichment();
						quizEnrichment.setQuizEnrichmentPath(enrPath);
						quizEnrichment.setQuizEnrichmentTitle(enrTitle);

						quizEnrichmentList.add(quizEnrichment);
					}

				}

				@Override
				public void endElement(String uri, String localName, String qName) throws SAXException {

					if(qName.contains("P")){
						quizPage.setQuizEnrichmentList(quizEnrichmentList);
						quizPageList.add(quizPage);
					}
				}
			};

			File sdPath = getFilesDir();
			//revert_book_hide :
			filePath = currentBookPath+"Book.xml";
			InputStream inputStream = new FileInputStream(filePath);
			Reader reader = new InputStreamReader(inputStream, "UTF-8");

			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");

			saxParser.parse(is, handler);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/* Page Slider - Begin */
	@Override
	public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) 
	{
		sb_pNo = arg1;
		tv_pageNumber.setText(""+(arg1+1)+"/"+(pageCount)+"");
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) 
	{

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) 
	{	
		viewPager.setCurrentItem(sb_pNo);
	}
	/* Page Slider - End */
	
	@Override
	public void onBackPressed() {
		String storeBookId = bookName.replace("Book", "");
		String query = "update books set LastViewedPage='"+lastViewedPageNo+"' where storeID='"+storeBookId+"'";
		db.executeQuery(query);
		currentBook.set_lastViewedPage(lastViewedPageNo);
		//Intent intent = getIntent();
		//intent.putExtra("Book", currentBook);
		setResult(RESULT_OK, getIntent().putExtra("Book", currentBook));
		setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
		//setResult(RESULT_OK, intent);
		if (Globals.isLimitedVersion() && !adsPurchased) {
			if (interstitial.isLoaded()) {
				interstitial.show();
			}
		}
		finish();
	}
}
