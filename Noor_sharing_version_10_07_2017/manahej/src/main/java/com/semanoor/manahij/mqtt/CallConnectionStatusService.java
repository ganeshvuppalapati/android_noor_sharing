package com.semanoor.manahij.mqtt;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.FlashCardActivity;
import com.semanoor.manahij.SlideMenuWithActivityGroup;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.Globals;
import com.twilio.video.AudioTrack;
import com.twilio.video.LocalAudioTrack;
import com.twilio.video.LocalVideoTrack;

import com.twilio.video.CameraCapturer;
import com.twilio.video.ConnectOptions;
import com.twilio.video.LocalAudioTrack;
import com.twilio.video.LocalParticipant;
import com.twilio.video.LocalVideoTrack;
import com.twilio.video.Participant;
import com.twilio.video.Room;
import com.twilio.video.RoomState;
import com.twilio.video.TwilioException;
import com.twilio.video.Video;
import com.twilio.video.VideoRenderer;
import com.twilio.video.VideoTrack;
import com.twilio.video.VideoView;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;

import static com.semanoor.manahij.mqtt.datamodels.Constants.TWILIO_ACCESS_TOKEN_URL;


/**
 * Created by GaneshVeera on 31/05/17.
 */

public class CallConnectionStatusService extends Service implements View.OnClickListener {

    private static final String TAG = "CallStatusService";
    public static final String FILTER = "noor.vedio.filter";
    private WindowManager mWindowManager;
    View mFloatingView;
    ManahijApp context;

    //video calling paramaters
    private AudioManager audioManager;
    private int previousAudioMode;
    private boolean previousMicrophoneMute;
    private LocalAudioTrack localAudioTrack;
    private CameraCapturer cameraCapturer;
    private LocalVideoTrack localVideoTrack;
    private VideoView primaryVideoView;
    private VideoRenderer localVideoView;

    private String accessToken;
   MQTTSubscriptionService service;
    boolean isvedio=true,isAudio=true ;
    int mStatus;
    //android1
     //private static final String TWILIO_ACCESS_TOKEN ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzhmMzI0MzlmZWU1Zjk2MTZhN2Q3ZWZkYmJhNWM1OTlmLTE0OTYzMTg2ODkiLCJpc3MiOiJTSzhmMzI0MzlmZWU1Zjk2MTZhN2Q3ZWZkYmJhNWM1OTlmIiwic3ViIjoiQUMzYTI4ODlkYTQyNzVjMWJjMWFjYzMzYmM5MWZmYjNkYiIsImV4cCI6MTQ5NjMyMjI4OSwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiYW5kcm9pZDEiLCJ2aWRlbyI6eyJyb29tIjoiYW5kcm9pZF8xMjMifX19.0ZVnDqha25byLgBZUyrxUV3SE8pwiqPQmlIEKGAb1Hs";
//android2
    private static  String TWILIO_ACCESS_TOKEN ="";



    private Room room;
    private LocalParticipant localParticipant;
    private boolean disconnectedFromOnDestroy;
    private String participantIdentity;
    //end

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        service = new MQTTSubscriptionService();
        mFloatingView = LayoutInflater.from(this).inflate(R.layout.activity_call_connection_status, null);
        primaryVideoView = (VideoView) mFloatingView.findViewById(R.id.primary_video_view);
        primaryVideoView.setVisibility(View.GONE);
        init();
        setTouchlistener();

        //video calling

        context =(ManahijApp )getApplicationContext();
        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        context.getVideoActivityContext().setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        createAudioAndVideoTracks();
       // setAccessToken();
        //  configureAudio(true);

        LocalBroadcastManager.getInstance(this).registerReceiver(msgreceiver, new IntentFilter(FILTER));

    }

    ImageView txtVideoCall, textAudioCall, txtAudioDisconnection, txtCallDisconnection;
   // View connection_status;
    public void init() {
        txtVideoCall = (ImageView) mFloatingView.findViewById(R.id.video_call);
        textAudioCall = (ImageView) mFloatingView.findViewById(R.id.audio_call);
       // txtAudioDisconnection = (ImageView) mFloatingView.findViewById(R.id.audio_disconnect);
        txtCallDisconnection = (ImageView) mFloatingView.findViewById(R.id.call_disconnect);
       // connection_status = (View) mFloatingView.findViewById(R.id.connection_status);

        txtVideoCall.setOnClickListener(this);
        textAudioCall.setOnClickListener(this);
         //txtAudioDisconnection.setOnClickListener(this);
        txtCallDisconnection.setOnClickListener(this);
      //  connection_status.setOnClickListener(this);

    }

    public void setTouchlistener() {
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        //Specify the view position
        /*
        *   Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = GetDeviceResolution(display);
		global.setDeviceHeight(displayMetrics.heightPixels);
		global.setDeviceWidth(displayMetrics.widthPixels);
        */
        params.gravity = Gravity.BOTTOM | Gravity.RIGHT;        //Initially view will be added to top-left corner
        params.x = -10;
        params.y = -10 ;

        //Add the view to the window
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mFloatingView, params);
        mFloatingView.findViewById(R.id.root_container).setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        //remember the initial position.
                        if(params.x<0) {
                            params.x= (-1)*params.x;
                            initialX = params.x;
                           // Log.e("lessX",""+initialX);
                        }else {
                            initialX = params.x;
                            //Log.e("X",""+initialX);
                        }


                        if(params.y<0){
                            params.y= (-1)*params.y;
                            initialY = params.y;
                           // Log.e("lessX",""+initialY);
                        }else {
                            initialY = params.y;
                          //  Log.e("Y",""+initialY);
                        }

                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                       // Log.e("X+Y",""+initialTouchX+"Y"+initialTouchY);
                        return true;
                    case MotionEvent.ACTION_UP:
                        int Xdiff = (int) (event.getRawX() - initialTouchX);
                        int Ydiff = (int) (event.getRawY() - initialTouchY);

                        //The check for Xdiff <10 && YDiff< 10 because sometime elements moves a little while clicking.
                        //So that is click event.
                        if (Xdiff > 10 && Ydiff > 10) {
                            if (true/*isViewCollapsed()*/) {
                                //When user clicks on the image view of the collapsed layout,
                                //visibility of the collapsed layout will be changed to "View.GONE"
                                //and expanded view will become visible.
                                //collapsedView.setVisibility(View.GONE);
                                // videoViewFrame.setVisibility(View.VISIBLE);
                            }
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        //Calculate the X and Y coordinates of the view.
                        params.x = initialX + (int) initialTouchX  - (int)(event.getRawX());
                        params.y = initialY + (int) initialTouchY - (int) (event.getRawY());
                      //  Log.e("X++Y",""+ params.x +"Y"+params.y);
                        //Update the layout with new X & Y coordinate
                        mWindowManager.updateViewLayout(mFloatingView, params);
                        return true;
                }
                return false;
            }
        });
    }
    public void setTouchlistener2() {
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        //Specify the view position
        /*
        *   Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = GetDeviceResolution(display);
		global.setDeviceHeight(displayMetrics.heightPixels);
		global.setDeviceWidth(displayMetrics.widthPixels);
        */
        params.gravity = Gravity.BOTTOM | Gravity.RIGHT;        //Initially view will be added to top-left corner
        params.x = 0;
        params.y = -100 ;

        //Add the view to the window
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mFloatingView, params);
        mFloatingView.findViewById(R.id.root_container).setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;

                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        int Xdiff = (int) (event.getRawX() - initialTouchX);
                        int Ydiff = (int) (event.getRawY() - initialTouchY);

                        //The check for Xdiff <10 && YDiff< 10 because sometime elements moves a little while clicking.
                        //So that is click event.
                        if (Xdiff < 10 && Ydiff < 10) {
                            if (true/*isViewCollapsed()*/) {
                                //When user clicks on the image view of the collapsed layout,
                                //visibility of the collapsed layout will be changed to "View.GONE"
                                //and expanded view will become visible.
                                //collapsedView.setVisibility(View.GONE);
                                // videoViewFrame.setVisibility(View.VISIBLE);
                            }
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        //Calculate the X and Y coordinates of the view.
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);

                        //Update the layout with new X & Y coordinate
                        mWindowManager.updateViewLayout(mFloatingView, params);
                        return true;
                }
                return false;
            }
        });
    }
    private void configureAudio(boolean enable) {
        if (enable) {
            previousAudioMode = audioManager.getMode();
            // Request audio focus before making any device switch.
            audioManager.requestAudioFocus(null, AudioManager.STREAM_VOICE_CALL,
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
            /*
             * Use MODE_IN_COMMUNICATION as the default audio mode. It is required
             * to be in this mode when playout and/or recording starts for the best
             * possible VoIP performance. Some devices have difficulties with
             * speaker mode if this is not set.
             */
            audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            /*
             * Always disable microphone mute during a WebRTC call.
             */
            previousMicrophoneMute = audioManager.isMicrophoneMute();
            audioManager.setMicrophoneMute(false);
        } else {
            audioManager.setMode(previousAudioMode);
            audioManager.abandonAudioFocus(null);
            audioManager.setMicrophoneMute(previousMicrophoneMute);
        }
    }

    private void createAudioAndVideoTracks() {
        // Share your microphone
        localAudioTrack = LocalAudioTrack.create(this, true);

        // Share your camera
        cameraCapturer = new CameraCapturer(this, CameraCapturer.CameraSource.FRONT_CAMERA);
        localVideoTrack = LocalVideoTrack.create(this, true, cameraCapturer);
        primaryVideoView.setMirror(true);
        localVideoTrack.addRenderer(primaryVideoView);
        localVideoView = primaryVideoView;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.video_call:

                try {

                if(isvedio) {
                    primaryVideoView.setVisibility(View.VISIBLE);
                    //if(ManahijApp.getInstance().getNetworkState()) {

                        JSONObject object = new JSONObject();
                        object.put("type", Constants.KEYVEDIOCALL);
                        object.put("action", "video_call_start");
                        object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                        service.publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(), object.toString(), getApplicationContext());
                   // }

                  /*  if (room == null)
                        new GenerateToken(true).execute();
                    else
                        Toast.makeText(context, "In Call", Toast.LENGTH_SHORT).show();
*/
                    isTokenGenaration(true );
                    isvedio=false;
                }else {


                    //if(ManahijApp.getInstance().getNetworkState()) {

                            JSONObject object = new JSONObject();
                            object.put("type", Constants.KEYVEDIOCALL);
                            object.put("action", "video_call_hide");
                            object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                            service.publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(), object.toString(), getApplicationContext());
                       // }
                    primaryVideoView.setVisibility(View.GONE);
                    isvedio=true;
                }
                }catch (Exception e){
                    Log.e("vedio",e.toString());
                }

            break;
            case R.id.audio_call:



                try {

                    if(isAudio) {

                        JSONObject object = new JSONObject();
                        object.put("type", Constants.KEYVEDIOCALL);
                        object.put("action", "audio_call_start");
                        object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                        service.publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(), object.toString(), getApplicationContext());
                        primaryVideoView.setVisibility(View.GONE);
                       /* if(room==null) {
                            primaryVideoView.setVisibility(View.GONE);
                            new GenerateToken(false).execute();
                        }
                        else{
                            primaryVideoView.setVisibility(View.GONE);
                        }*/

                        isTokenGenaration(false );

                        isAudio=false;
                    }else {



                        JSONObject object = new JSONObject();
                        object.put("type", Constants.KEYVEDIOCALL);
                        object.put("action", "audio_end");
                        object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
                        service.publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(), object.toString(), getApplicationContext());
                        primaryVideoView.setVisibility(View.GONE);
                        if(room!=null){
                            room.disconnect();
                        }
                        isAudio=true;
                    }
                }catch (Exception e){
                    Log.e("vedio",e.toString());
                }







                break;
           /* case R.id.audio_disconnect:
                if (localAudioTrack != null) {
                    boolean enable = !localAudioTrack.isEnabled();
                    localAudioTrack.enable(enable);

                }
                break;*/
          /*  case R.id.connection_status:

                if(mStatus==0) {

                    connection_status.setVisibility(View.VISIBLE);
                    connection_status.setBackgroundResource(R.drawable.offline_status);
                    stopService(new Intent(CallConnectionStatusService.this, MQTTSubscriptionService.class));
                    startService(new Intent(CallConnectionStatusService.this, MQTTSubscriptionService.class));
                    Log.e("c","c");
                }
                else {
                    //connection_status.setBackgroundResource(R.drawable.online_status);
                    connection_status.setVisibility(View.GONE);
                    Toast.makeText(context, " Call Connected", Toast.LENGTH_SHORT).show();
                }
                break;*/

            case R.id.call_disconnect:

                try {
                    //if(ManahijApp.getInstance().getNetworkState()) {

                            JSONObject object = new JSONObject();
                            object.put("type", Constants.KEYCALLCONNECTION);
                            object.put("action", Constants.KEYCALLDISCONNECTIONREQUEST);
                            object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());

                            service.publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(), object.toString(), getApplicationContext());
                            service.sendUsersList(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(), String.valueOf(1), getApplicationContext());
                            service.unSubScribeTopic(ManahijApp.getInstance().getPrefManager().getMainChannel(), getApplicationContext());
                            service.subscribeToNewTopic(ManahijApp.getInstance().getPrefManager().getSubscribeTopicName(), getApplicationContext());
                       // }
                    ManahijApp.getInstance().getPrefManager().addIsMainChannel(false);
                    ManahijApp app = (ManahijApp)getApplicationContext();
                    app.setCallState(false);

                }catch (Exception e){
                    Log.e("call service",e.toString());
                }
                setDisconnection();
                stopSelf();
                break;
        }

    }

    private void isTokenGenaration(boolean b) {
        if (room == null)
            new GenerateToken(true).execute();
    }

    public void setDisconnection(){
        if (room != null && room.getState() != RoomState.DISCONNECTED) {
            room.disconnect();
            //disconnectedFromOnDestroy = true;
        }

        /*
         * Release the local audio and video tracks ensuring any memory allocated to audio
         * or video is freed.
         */
        if (localAudioTrack != null) {
            localAudioTrack.release();
            localAudioTrack = null;
        }
        if (localVideoTrack != null) {
            localVideoTrack.release();
            localVideoTrack = null;
        }
    }
    private void setAccessToken(String accessToken) {
        // OPTION 1- Generate an access token from the getting started portal
        // https://www.twilio.com/console/video/dev-tools/testing-tools
        this.accessToken = accessToken;

        // OPTION 2- Retrieve an access token from your own web app
        // retrieveAccessTokenfromServer();
    }
    private void connectToRoom(String roomName,boolean typeofCall) {
        configureAudio(true);
        ConnectOptions.Builder connectOptionsBuilder = new ConnectOptions.Builder(accessToken)
                .roomName(roomName);

        /*
         * Add local audio track to connect options to share with participants.
         */
        if (localAudioTrack != null) {
            connectOptionsBuilder
                    .audioTracks(Collections.singletonList(localAudioTrack));
        }

        /*
         * Add local video track to connect options to share with participants.
         */
        if (localVideoTrack != null&&typeofCall) {
            connectOptionsBuilder.videoTracks(Collections.singletonList(localVideoTrack));
        }
        room = Video.connect(this, connectOptionsBuilder.build(), roomListener());
        // setDisconnectAction();
    }


    private Room.Listener roomListener() {
        return new Room.Listener() {
            @Override
            public void onConnected(Room room) {
                localParticipant = room.getLocalParticipant();
                // videoStatusTextView.setText("Connected to " + room.getName());
                //setTitle(room.getName());

                for (Participant participant : room.getParticipants()) {
                    addParticipant(participant);
                    break;
                }
            }

            @Override
            public void onConnectFailure(Room room, TwilioException e) {
                // videoStatusTextView.setText("Failed to connect");
                configureAudio(false);
            }

            @Override
            public void onDisconnected(Room room, TwilioException e) {
                localParticipant = null;
                // videoStatusTextView.setText("Disconnected from " + room.getName());
                CallConnectionStatusService.this.room = null;
                // Only reinitialize the UI if disconnect was not called from onDestroy()
                if (!disconnectedFromOnDestroy) {
                    configureAudio(false);
                    // intializeUI();
                    moveLocalVideoToPrimaryView();
                }
            }

            @Override
            public void onParticipantConnected(Room room, Participant participant) {
                addParticipant(participant);

            }

            @Override
            public void onParticipantDisconnected(Room room, Participant participant) {
                removeParticipant(participant);
            }

            @Override
            public void onRecordingStarted(Room room) {
                /*
                 * Indicates when media shared to a Room is being recorded. Note that
                 * recording is only available in our Group Rooms developer preview.
                 */
                Log.d(TAG, "onRecordingStarted");
            }

            @Override
            public void onRecordingStopped(Room room) {
                /*
                 * Indicates when media shared to a Room is no longer being recorded. Note that
                 * recording is only available in our Group Rooms developer preview.
                 */
                Log.d(TAG, "onRecordingStopped");
            }
        };
    }

    /*
    * Called when participant joins the room
    */
    private void addParticipant(Participant participant) {
        /*
         * This app only displays video for one additional participant per Room
         */
       /* if (thumbnailVideoView.getVisibility() == View.VISIBLE) {
            Snackbar.make(connectActionFab,
                    "Multiple participants are not currently support in this UI",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }*/
        participantIdentity = participant.getIdentity();
        // videoStatusTextView.setText("Participant "+ participantIdentity + " joined");

        /*
         * Add participant renderer
         */
        if (participant.getVideoTracks().size() > 0) {
            addParticipantVideo(participant.getVideoTracks().get(0));
        }

        /*
         * Start listening for participant events
         */
        participant.setListener(participantListener());
    }
    private void addParticipantVideo(VideoTrack videoTrack) {
        moveLocalVideoToThumbnailView();
        primaryVideoView.setMirror(false);
        videoTrack.addRenderer(primaryVideoView);
    }

    private void moveLocalVideoToThumbnailView() {
        //if (thumbnailVideoView.getVisibility() == View.GONE)
        {
            //   thumbnailVideoView.setVisibility(View.VISIBLE);
            if(localVideoTrack!=null)
            localVideoTrack.removeRenderer(primaryVideoView);
            //localVideoTrack.addRenderer(thumbnailVideoView);
            // localVideoView = thumbnailVideoView;
            // thumbnailVideoView.setMirror(cameraCapturer.getCameraSource() == CameraCapturer.CameraSource.FRONT_CAMERA);
        }
    }
    private void moveLocalVideoToPrimaryView() {
        //if (thumbnailVideoView.getVisibility() == View.VISIBLE)
        {
            //localVideoTrack.removeRenderer(thumbnailVideoView);
            // thumbnailVideoView.setVisibility(View.GONE);
            if(localVideoTrack!=null) {
                localVideoTrack.addRenderer(primaryVideoView);
                localVideoView = primaryVideoView;
                primaryVideoView.setMirror(cameraCapturer.getCameraSource() == CameraCapturer.CameraSource.FRONT_CAMERA);
            }
        }
    }
    private Participant.Listener participantListener() {
        return new Participant.Listener() {
            @Override
            public void onAudioTrackAdded(Participant participant, AudioTrack audioTrack) {
                //videoStatusTextView.setText("onAudioTrackAdded");
            }

            @Override
            public void onAudioTrackRemoved(Participant participant, AudioTrack audioTrack) {
                // videoStatusTextView.setText("onAudioTrackRemoved");
            }

            @Override
            public void onVideoTrackAdded(Participant participant, VideoTrack videoTrack) {
                //videoStatusTextView.setText("onVideoTrackAdded");
                addParticipantVideo(videoTrack);
            }

            @Override
            public void onVideoTrackRemoved(Participant participant, VideoTrack videoTrack) {
                // videoStatusTextView.setText("onVideoTrackRemoved");
                removeParticipantVideo(videoTrack);
            }

            @Override
            public void onAudioTrackEnabled(Participant participant, AudioTrack audioTrack) {

            }

            @Override
            public void onAudioTrackDisabled(Participant participant, AudioTrack audioTrack) {

            }

            @Override
            public void onVideoTrackEnabled(Participant participant, VideoTrack videoTrack) {

            }

            @Override
            public void onVideoTrackDisabled(Participant participant, VideoTrack videoTrack) {

            }
        };
    }

    private void removeParticipantVideo(VideoTrack videoTrack) {
        videoTrack.removeRenderer(primaryVideoView);
    }


    /*
     * Called when participant leaves the room
     */
    private void removeParticipant(Participant participant) {
        //  videoStatusTextView.setText("Participant "+participant.getIdentity()+ " left.");
        if (!participant.getIdentity().equals(participantIdentity)) {
            return;
        }

        /*
         * Remove participant renderer
         */
        if (participant.getVideoTracks().size() > 0) {
            removeParticipantVideo(participant.getVideoTracks().get(0));
        }
        participant.setListener(null);
        moveLocalVideoToPrimaryView();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFloatingView != null) mWindowManager.removeView(mFloatingView);
        if (room != null && room.getState() != RoomState.DISCONNECTED) {
            room.disconnect();
            //disconnectedFromOnDestroy = true;
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(msgreceiver);
        /*
         * Release the local audio and video tracks ensuring any memory allocated to audio
         * or video is freed.
         */
        if (localAudioTrack != null) {
            localAudioTrack.release();
            localAudioTrack = null;
        }
        if (localVideoTrack != null) {
            localVideoTrack.release();
            localVideoTrack = null;
        }
    }
























    public class  GenerateToken extends AsyncTask<Void,Void,String> {
        String paramUsername =ManahijApp.getInstance().getPrefManager().getSubscribeTopicName();
        boolean typeOfcall;
        String paramPassword ="";
        public GenerateToken(boolean type){
            String m_androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            paramPassword=m_androidId;
            typeOfcall=type;
        }


        @Override
        protected String doInBackground(Void... params) {
            // http://183.82.117.211:8080/SemaPresence/gentoken?identity=testerarka@gmail.com&device=ghjklk;jjcbvn

            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(TWILIO_ACCESS_TOKEN_URL+"identity=" + paramUsername + "&device=" + paramPassword);
            try {
                HttpResponse httpResponse = httpClient.execute(httpGet);
                InputStream inputStream = httpResponse.getEntity().getContent();


                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();

                String bufferedStrChunk = null;

                while((bufferedStrChunk = bufferedReader.readLine()) != null){
                    stringBuilder.append(bufferedStrChunk);
                }

                return stringBuilder.toString();

            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String response) {

            super.onPostExecute(response);
            if(response!=null&&response.length()>0) {
                try {
                    // {"identity":"testerarka@gmail.com","token":"eyJjdHkiOiJ0d2lsaW8tZnBhO3Y9MSIsInR5cCI6IkpXVCIsImFsZyI6IkhTMjU2In0.eyJpc3MiOiJTS2UwM2U3NDkwMmY5YWI5ZTNhMWEwZGQ4MjQxNzk5NDYxIiwiZXhwIjoxNDk2NjQ2MjA4LCJncmFudHMiOnsicnRjIjp7ImNvbmZpZ3VyYXRpb25fcHJvZmlsZV9zaWQiOiJWU2I4ODRhOTU3YzkxMzk3YzNiOWUyMDBkYTNjMTZhMTUwIn0sImlkZW50aXR5IjoidGVzdGVyYXJrYUBnbWFpbC5jb20ifSwianRpIjoiU0tlMDNlNzQ5MDJmOWFiOWUzYTFhMGRkODI0MTc5OTQ2MS0xNDk2NjQyNTYwIiwic3ViIjoiQUMxMzVjZGI4NjBjMjM0Y2Q2ZDM3M2E3NDM3M2IzNmFhNyJ9.vWXqNKkuBoSf2paUHK1zfqFu0HrqrxfhJrXigxhY-7I"}
                    JSONObject object = new JSONObject(response);
                    String identity = object.getString("identity");
                    TWILIO_ACCESS_TOKEN = object.getString("token");
                    setAccessToken(TWILIO_ACCESS_TOKEN);

                    connectToRoom(ManahijApp.getInstance().getPrefManager().getMainChannel(),typeOfcall);

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }else{

            }

        }


    }


    BroadcastReceiver msgreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {

               /* final int status = intent.getExtras().getInt(Constants.MQTT_STATUS);
                //if(status!=-1) {
                mStatus = status;
                if (status == 0) {
                    connection_status.setVisibility(View.VISIBLE);
                    connection_status.setBackgroundResource(R.drawable.offline_status);
                   *//* ((ManahijApp) getApplication()).stopSubscriptionService();
                    ((ManahijApp) getApplication()).startSubscriptionService();*//*
                } else {
                    //connection_status.setBackgroundResource(R.drawable.online_status);
                    connection_status.setVisibility(View.GONE);
                }*/
                JSONObject ojb= new JSONObject(intent.getStringExtra(Constants.KEYMESSAGE));
                switch (ojb.getString("action")){

                             case "video_call_start":
                             primaryVideoView.setVisibility(View.VISIBLE);
                            /* if (room == null)
                                 new GenerateToken(true).execute();
                             else
                                 Toast.makeText(context, "In Call", Toast.LENGTH_SHORT).show();*/
                                 isTokenGenaration(true );
                             break;
                             case "video_call_hide":
                                 primaryVideoView.setVisibility(View.GONE);
                             break;


                              case "audio_call_start":
                              primaryVideoView.setVisibility(View.GONE);
                               /*if (room == null)
                                new GenerateToken(true).execute();
                                else
                                 Toast.makeText(context, "In Call", Toast.LENGTH_SHORT).show();*/
                                  isTokenGenaration(false );
                                 break;
                              case "audio_end":
                                  if(room!=null){
                                      room.disconnect();
                                  }
                              primaryVideoView.setVisibility(View.GONE);
                              break;


                         /*   case "status":

                                if ( ojb.getInt(Constants.MQTT_STATUS) == 0) {
                                    connection_status.setVisibility(View.VISIBLE);
                                    connection_status.setBackgroundResource(R.drawable.offline_status);

                                } else {

                                    connection_status.setVisibility(View.GONE);
                                }

                            break;*/

                }





            }catch (Exception e){
                Log.e("EXc",e.toString());
            }


        }
    };
}
