package com.semanoor.manahij;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ptg.views.CircleButton;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.HTMLObjectHolder;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.QuizSequenceObject;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.SegmentedRadioGroup;
import com.semanoor.source_sboookauthor.Templates;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Suriya on 03/06/15.
 */
public class Malzamah implements PopoverView.PopoverViewDelegate {

    private Context context;
    ArrayList<PageNumbers> selectedPageList = new ArrayList<PageNumbers>();
    ArrayList<PageNumbers> currentBookPageList = new ArrayList<PageNumbers>();
    private ViewPager viewPager;
    public boolean resourceSelected = false;
    private ProgressDialog progresdialog;
    PopoverView popoverView;
    private ImageView imgViewCover;
    private ArrayList<String> malzamahBookList;
    int wantedPosition;
    boolean clicked;
    String selectedBook;
    boolean isDeleteBookDir = false;
  //  public String bookId;
     boolean coverPageSelected;
    String templateImageId;
    ArrayList<String>malzamahBookDetails;
    JSONArray jsonBookDetails;
    public ArrayList<OnlineTemplates> templatesList = new ArrayList<>();
    private boolean onlineTemplates = true;
    private SharedPreferences sharedPreference;

    public Malzamah(Context context){
         this.context = context;
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
         if(context instanceof BookViewReadActivity) {
             templateImageId=null;
             for (int i = 0; i < ((BookViewReadActivity)context).pageCount - 2; i++) {
                 PageNumbers pages = new PageNumbers((BookViewReadActivity)context);
                 pages.setpageNumber(i + 2);
                 pages.setChecked(false);
                 pages.setBookID(((BookViewReadActivity)context).currentBook.getBookID());
                 pages.setStoreId(((BookViewReadActivity)context).currentBook.get_bStoreID());
                 pages.setBook(((BookViewReadActivity)context).currentBook);
                 currentBookPageList.add(pages);
            }
         }else if(context instanceof  MainActivity){

         }
    }

    public void displayMalzamahPopUp(){
        ((BookViewReadActivity)context).exportEnrPopover = new Dialog(((BookViewReadActivity)context));
        ((BookViewReadActivity)context).exportEnrPopover.getWindow().setBackgroundDrawable(new ColorDrawable(((BookViewReadActivity)context).getResources().getColor(R.color.semi_transparent)));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        ((BookViewReadActivity)context).exportEnrPopover.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ((BookViewReadActivity)context).exportEnrPopover.setContentView(((BookViewReadActivity)context).getLayoutInflater().inflate(R.layout.enrich_viewpager, null));
        ((BookViewReadActivity)context).exportEnrPopover.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.3),(int) context.getResources().getDimension(R.dimen.malzamahpopup_height));

       /* popoverView = new PopoverView(((BookViewReadActivity)context), R.layout.enrich_viewpager);
        int width =  (int) context.getResources().getDimension(R.dimen.malzamahpopup_width);
        int height =
        popoverView.setContentSizeForViewInPopover(new Point(width, height));
        popoverView.setDelegate(this);
        popoverView.showPopoverFromRectInViewGroup(((BookViewReadActivity)context).rootviewRL, PopoverView.getFrameForView(((BookViewReadActivity)context).btnMalzamah), PopoverView.PopoverArrowDirectionUp, true); */


        viewPager = (ViewPager)((BookViewReadActivity)context).exportEnrPopover.findViewById(R.id.viewPager);
        ViewPagerAdapter viewPageradapter = new ViewPagerAdapter();
        viewPager.setAdapter(viewPageradapter);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        ((BookViewReadActivity)context).exportEnrPopover.show();
    }

    private class ViewPagerAdapter extends PagerAdapter{

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = null;
            switch (position){
                case 0:
                    view = inflater.inflate(R.layout.malzamah_pagelist, null);
                    ViewGroup viewGroup = (RelativeLayout) view.findViewById(R.id.malzamah_layout);
                    Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
                    UserFunctions.changeFont(viewGroup,font);
                    CircleButton back_btn = (CircleButton) view.findViewById(R.id.btn_back);
                    back_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((BookViewReadActivity)context).exportEnrPopover.dismiss();
                        }
                    });
                    final FrameLayout nextLayout = (FrameLayout) view.findViewById(R.id.add_shelf_layout);
                    final ListView listView = (ListView) view.findViewById(R.id.listView3);
                    final Button btn_next = (Button) view.findViewById(R.id.btnNext);
                    if (selectedPageList.size() > 0) {
                        nextLayout.setVisibility(View.VISIBLE);
                    } else {
                        nextLayout.setVisibility(View.INVISIBLE);
                    }
                    listView.setAdapter(new ListAdapter());
                    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            PageNumbers pageNumbers = currentBookPageList.get(position);
                            if (pageNumbers.isChecked()) {
                                pageNumbers.setChecked(false);
                                selectedPageList.remove(pageNumbers);
                            } else {
                                pageNumbers.setChecked(true);
                                selectedPageList.add(pageNumbers);
                            }
                            if (selectedPageList.size() > 0) {
                                nextLayout.setVisibility(View.VISIBLE);
                            } else {
                                nextLayout.setVisibility(View.INVISIBLE);
                            }

                            ((BaseAdapter) parent.getAdapter()).notifyDataSetChanged();
                        }
                    });
                    btn_next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            viewPager.setCurrentItem(1);
                        }
                    });
                    break;
                case 1:
                    view = inflater.inflate(R.layout.malzamah_creation, null);
                    ViewGroup viewGroup1 = (RelativeLayout) view.findViewById(R.id.malzamah_createlayout);
                    Typeface font1 = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
                    UserFunctions.changeFont(viewGroup1,font1);
                    CircleButton btn_previous = (CircleButton) view.findViewById(R.id.btn_previous);
                    Button btn_Create = (Button) view.findViewById(R.id.btn_createbook);
                    final EditText et_bookName = (EditText) view.findViewById(R.id.et_book_name);
                    final EditText et_Description = (EditText) view.findViewById(R.id.et_description);
                 //   ToggleButton btnswitch = (ToggleButton) view.findViewById(R.id.switch_Res);
                    malzamahBookList = ((BookViewReadActivity)context).db.getMalzamahBooksList();
                    btn_previous.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            viewPager.setCurrentItem(0);
                        }
                    });
                 /*   btnswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                resourceSelected = true;
                            } else {
                                resourceSelected = false;
                            }
                        }
                    });*/
                    btn_Create.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String title = et_bookName.getText().toString();
                            String description = et_Description.getText().toString();
                            if (title.isEmpty()) {
                                et_bookName.requestFocus();
                                et_bookName.getBackground().setColorFilter(context.getResources().getColor(R.color.red), PorterDuff.Mode.SRC_ATOP);
                            } else {
                                isDeleteBookDir = false;
                                if(coverPageSelected) {
                                    new CreateMalzamahBook(title, description, selectedPageList, ((BookViewReadActivity) context).currentBook).execute();
                                }else{
                                    showAddNewBookDialog(true,title, description, selectedPageList, ((BookViewReadActivity) context).currentBook);

                                }
                            }
                        }
                    });
                    imgViewCover = (ImageView) view.findViewById(R.id.imgView_cover);
                    int BID = ((BookViewReadActivity)context).currentBook.getBookID();
                    String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/pageBG_1.png";
                    if (!new File(bookCardImagePath).exists()) {
                        bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/card.png";
                    }
                    Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
                    imgViewCover.setImageBitmap(bitmap);
                    imgViewCover.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            templateImageId=null;
                            showAddNewBookDialog(false, null, null, null, null);
                            //Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                            //galleryIntent.setType("image/*");
                            // ((BookViewReadActivity)context).startActivityForResult(galleryIntent, ((BookViewReadActivity)context).CAPTURE_GALLERY);
                        }
                    });
                    Button btn_add = (Button) view.findViewById(R.id.add_btn_malzamah);
                    final ListView lv_bookImages= (ListView) view.findViewById(R.id.lv_books);
                    lv_bookImages.setAdapter(new MalzBookListAdapter());
                    lv_bookImages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            clicked = true;
                            wantedPosition = position;
                            int firstPosition = lv_bookImages.getFirstVisiblePosition() - lv_bookImages.getHeaderViewsCount();
                            int calculatedPosition = wantedPosition - firstPosition;
                            View currentSelectedListView = parent.getChildAt(calculatedPosition);

                            if (currentSelectedListView != null && currentSelectedListView != view) {
                                view.setBackgroundColor(Color.TRANSPARENT);
                            }
                            currentSelectedListView = view;
                            view.setBackgroundColor(Color.parseColor("#604e3634"));
                            selectedBook = malzamahBookList.get(wantedPosition);
                            lv_bookImages.invalidateViews();
                        }
                    });
                    btn_add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (selectedBook != null){
                                PageExportDialog page_Export = new PageExportDialog(((BookViewReadActivity)context), ((BookViewReadActivity)context).db, ((BookViewReadActivity)context).gridShelf);
                               // page_Export.showBookDialog();

                                String[] str=selectedBook.split("##");
                                int bookId= Integer.parseInt(str[0]);
                                for(int i=0;i<((BookViewReadActivity)context).gridShelf.bookListArray.size();i++){
                                    Book book =(Book)((BookViewReadActivity)context).gridShelf.bookListArray.get(i);
                                    if(bookId==book.getBookID()){
                                        page_Export.new AddingPagestoExistingNormalBook(selectedPageList,book).execute();
                                        break;
                                    }
                                }
                                  //new AddingPagestoExistingMalzamahBook(selectedBook,((BookViewReadActivity)context).currentBook,selectedPageList).execute();
                            } else {
                                Toast.makeText(context, R.string.select_malzamah_books_message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    break;
                case 2:

                    break;
                case 3:

                    break;
            }
            ((ViewPager) container).addView(view, 0);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    public  class AddingPagestoExistingMalzamahBook extends AsyncTask<Void, Void, Void>{

        String bookDetails;
        Book currentbook;
        ArrayList<PageNumbers> PageList;
        JSONArray jsonArray;

        String filePath;
        String[]str;
        int bookId;
        public AddingPagestoExistingMalzamahBook(String selectedBook, Book currentBook, ArrayList<PageNumbers> selectedPageList) {
            bookDetails=selectedBook;
            currentbook=currentBook;
            PageList=selectedPageList;
            str=bookDetails.split("##");
            bookId= Integer.parseInt(str[0]);
            filePath=Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId+"/FreeFiles/"+"malzamah.json";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progresdialog = ProgressDialog.show(context, "", context.getResources().getString(R.string.please_wait), true);
            jsonArray = new JSONArray();
            jsonBookDetails= new JSONArray();
            malzamahBookDetails=new ArrayList<String>();
            if(new File(filePath).exists()){
                jsonArray= readJsonFile(filePath);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            int existingPages = Integer.parseInt(str[6]);
            int totalPages = existingPages + PageList.size();
            int pageNo = 0;
            String bookXml = null;
            changeEndpageFiles(bookId, existingPages, totalPages);
            File f = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/Book.xml");
            if (f.exists()) {
                String sourceString = UserFunctions.readFileFromPath(f);
                //sourceString=sourceString.replace("<Cover2..","");
                String[] actualContent = sourceString.split("<Cover2");

                 bookXml = actualContent[0];
            }
            for (int i = 0; i < PageList.size(); i++) {
                PageNumbers selectedPage = PageList.get(i);
                pageNo = existingPages+i;
                File file = null;
                //if(!(context instanceof BookViewActivity)) {
                    UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentbook.get_bStoreID() + "Book/fpimages/" + selectedPage.getpageNumber() + ".jpg", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/fpimages/" + pageNo + ".jpg");
                    //UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentbook.get_bStoreID() + "Book/" + selectedPage.getpageNumber() + ".htm", Globals.TARGET_BASE_BOOKS_DIR_PATH +  bookId + "/" + pageNo + ".htm");
                    File oldFile=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentbook.get_bStoreID() + "Book/" + selectedPage.getpageNumber() + ".htm");
                   // String htmlString=UserFunctions.decryptFile(oldFile);
                    String htmlString=UserFunctions.readFileFromPath(oldFile);
                    creatingFile(htmlString,Globals.TARGET_BASE_BOOKS_DIR_PATH +  bookId + "/" + pageNo + ".htm");
                    file = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/" + pageNo + ".htm");
               // }else if(context instanceof BookViewActivity){

               // }
                JSONObject jsonObject=AddingPagestojsonArray(selectedPage,pageNo, bookId);
                jsonArray.put(jsonObject);
                changeHtmlFile(file, bookId,selectedPage.getStoreId(),selectedPage.getBookID());
                copyingImageInToNewBookPath(selectedPage.getpageNumber(), bookId, pageNo,currentbook.getBookID(),selectedPage.getStoreId());
                UserFunctions.copyFiles(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentbook.getBookID() + "/thumbnail_" + selectedPage.getpageNumber() + ".png", Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_" + pageNo + ".png");

                String  flipPath="fpimages/"+ pageNo +".jpg";
                //bookXml=actualContent[0];
                if(bookXml!=null) {
                    bookXml = generateBookXML(totalPages, pageNo, bookXml, flipPath);
                }

                /*
                * for css Books
                */
                if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+selectedPage.getStoreId()+"Book/FreeFiles/css").exists() && !new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+ bookId+"/FreeFiles/"+selectedPage.getStoreId()+"/css").exists()){
                    try {
                        UserFunctions.copyDirectory(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+selectedPage.getStoreId()+"Book/FreeFiles/css"),new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId+"/FreeFiles/"+selectedPage.getStoreId()+"/css"));
                        UserFunctions.copyDirectory(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+selectedPage.getStoreId()+"Book/FreeFiles/css"),new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId+"/FreeFiles/"+selectedPage.getStoreId()+"/css"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            String  flipPath="fpimages/"+ totalPages +".jpg";
            if(bookXml!=null) {
                bookXml = generateBookXML(totalPages, totalPages, bookXml, flipPath);
            }
            String query = "update books set totalPages='"+totalPages+"' where storeID='"+bookId+"'and BID='"+bookId+"'";

            JSONArray bookDetailsJson=getBookDetailsJson();
            JSONObject jsonData=new JSONObject();
            try {
                // String absPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
                //  String path= absPath+"/Download/";

                jsonData.put("malzamah",jsonArray);
                jsonData.put("bookDetails", bookDetailsJson);
                FileWriter file=new FileWriter(new File(filePath));
                file.write(jsonData.toString());
                file.flush();
                file.close();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(context instanceof BookViewReadActivity) {
                ((BookViewReadActivity) context).db.executeQuery(query);
            }else if(context instanceof MainActivity){
                ((MainActivity) context).db.executeQuery(query);
            }
            if(bookXml!=null) {
                creatingFile(bookXml, Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/Book.xml");
            }
           // UserFunctions.createNewBooksDirAndCoptTemplates(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/");
            if(context instanceof BookViewReadActivity) {
                for (int i = 0; i < ((BookViewReadActivity) context).gridShelf.bookListArray.size(); i++) {
                    Book book = (Book) ((BookViewReadActivity) context).gridShelf.bookListArray.get(i);
                    if (book.getBookID() == bookId && book.getIsMalzamah().equals("yes")) {
                        book.setTotalPages(totalPages);
                        break;
                    }
                }
            } else if(context instanceof MainActivity) {
                for (int i = 0; i < ((MainActivity) context).gridShelf.bookListArray.size(); i++) {
                    Book book = (Book) ((MainActivity) context).gridShelf.bookListArray.get(i);
                    if (book.getBookID() == bookId && book.getIsMalzamah().equals("yes")) {
                        book.setTotalPages(totalPages);
                        break;
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progresdialog.dismiss();
            if(context instanceof BookViewReadActivity) {
                UserFunctions.DisplayAlertDialog(context, R.string.malzamah_updated, R.string.done);
               // popoverView.dissmissPopover(true);
                ((BookViewReadActivity)context). exportEnrPopover.dismiss();
            }else if(context instanceof MainActivity) {
                UserFunctions.DisplayAlertDialog(((MainActivity) context), R.string.malzamah_updated, R.string.done);
                ((MainActivity)context).PopoverView.dissmissPopover(true);
            }
        }
    }


    public class CreateMalzamahBook extends AsyncTask<Void, Void, Void>{

        String title, description;
        ArrayList<PageNumbers> PageList;
        Book selectedBook;
        JSONArray jsonArray;
        ArrayList<String> bookDetails;
        String filePath;
        public  CreateMalzamahBook(String title, String description, ArrayList<PageNumbers> selectedPageList, Book book){
            this.title = title;
            this.description = description;
            PageList=selectedPageList;
            selectedBook=book;

        }

        @Override
        protected void onPreExecute() {
            jsonArray = new JSONArray();
            malzamahBookDetails=new ArrayList<String>();
            progresdialog = ProgressDialog.show(context, "", context.getResources().getString(R.string.please_wait), true);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            int lastBookId = 0;
            Book lastBook = null;
            Templates template = null;
            if(context instanceof BookViewReadActivity) {
                lastBook = (Book) ((BookViewReadActivity)context).gridShelf.bookListArray.get(((BookViewReadActivity)context).gridShelf.bookListArray.size()-1);
                lastBookId = ((BookViewReadActivity) context).db.getMaxUniqueRowID("books");
            }else  if(context instanceof MainActivity) {
                lastBook = (Book) ((MainActivity)context).gridShelf.bookListArray.get(((MainActivity)context).gridShelf.bookListArray.size()-1);
                lastBookId = ((MainActivity) context).db.getMaxUniqueRowID("books");
            }
            String bookXml = "";
            int bookId = lastBookId + 1;
            int totalPages= PageList.size()+2;
            filePath=Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId+"/FreeFiles/"+"malzamah.json";
           // int i=(BookViewReadActivity)context).gridShelf.bookListArray

            int bIndex  = lastBook.getBookIndex();
            //int bIndex = selectedBook.getBookIndex()+1;
            int categoryId = 2;
            String domainUrl=null;
            //System.out.println(bookView.currentBook.get_bOffsetWidthAndHeight());
            if(context instanceof BookViewReadActivity) {
                domainUrl=((BookViewReadActivity) context).db.getStoreBookDomainUrl(selectedBook.get_bStoreID(),selectedBook.getBookID());
                ((BookViewReadActivity) context).db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,OffsetWidthAndHeight,ShowPageNo,CID,Search,Bookmark,Copy,Flip,Navigation,Highlight,Note,GotoPage,IndexPage,Zoom,WebSearch,WikiSearch,UpdateCounts,google,youtube,semaBook,semaContent,yahoo,bing,ask,jazeera,blanguage,TranslationSearch,dictionarySearch,booksearch,StoreBookCreatedFromIpad,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,domainURL,clientID,bookDirection) " +
                        "values('" + title +"','" +description+"','Semanoor','6','" + totalPages + "','0','" + Globals.portrait + "','" + bIndex + "','false','1','" + selectedBook.get_bOffsetWidthAndHeight() + "', 'false','" + categoryId + "','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','1','yes','yes','yes','yes','yes','yes','yes','yes','" + selectedBook.getBookLangugae() + "','yes','yes','yes','" + selectedBook.isStoreBookCreatedFromTab() + "','downloaded','','yes','yes','0.00','','created','"+domainUrl+"','"+selectedBook.getClientID()+"','ltr')");
                template = ((BookViewReadActivity) context).gridShelf.getTemplates(6, 0,context);

            }else  if(context instanceof MainActivity) {
                domainUrl=((MainActivity) context).db.getStoreBookDomainUrl(selectedBook.get_bStoreID(),selectedBook.getBookID());
                ((MainActivity) context).db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,OffsetWidthAndHeight,ShowPageNo,CID,Search,Bookmark,Copy,Flip,Navigation,Highlight,Note,GotoPage,IndexPage,Zoom,WebSearch,WikiSearch,UpdateCounts,google,youtube,semaBook,semaContent,yahoo,bing,ask,jazeera,blanguage,TranslationSearch,dictionarySearch,booksearch,StoreBookCreatedFromIpad,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,domainURL,clientID,bookDirection) " +
                        "values('" + title + "','" + description + "','Semanoor','6','" + totalPages + "','0','" + Globals.portrait + "','" + bIndex + "','false','1','" + selectedBook.get_bOffsetWidthAndHeight() + "', 'false','" + categoryId + "','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','1','yes','yes','yes','yes','yes','yes','yes','yes','" + selectedBook.getBookLangugae() + "','yes','yes','yes','" + selectedBook.isStoreBookCreatedFromTab() + "','downloaded','','yes','yes','0.00','','created','"+domainUrl+"','"+selectedBook.getClientID()+"','ltr')");
                template = ((MainActivity) context).gridShelf.getTemplates(6, 0,context);
            }
            Book newBook = new Book(bookId, title, description, "Semanoor", 6, totalPages, 0, Globals.portrait, bIndex, false, 1, null, selectedBook.get_bOffsetWidthAndHeight(), false, null, null, template, selectedBook.isStoreBookCreatedFromTab(), categoryId, selectedBook.getBookLangugae(), "downloaded", "", "yes", "yes", "0.00", "", "created",0,"0","ltr");
             //Book newBook = new Book(bookId, title, description, "Semanoor", 6, totalPages, 0, Globals.portrait, bIndex, false, 1, ""+bookId, selectedBook.get_bOffsetWidthAndHeight(), false, null, null, template, selectedBook.isStoreBookCreatedFromTab(), categoryId, selectedBook.getBookLangugae(), "downloaded", "", "yes", "yes", "0.00", "", "created");

            if(context instanceof BookViewReadActivity) {
                ((BookViewReadActivity) context).gridShelf.bookListArray.add(0, newBook);
                ((BookViewReadActivity) context).gridShelf.setNoOfBooks(((BookViewReadActivity) context).gridShelf.bookListArray.size());
             }else  if(context instanceof MainActivity) {
                ((MainActivity) context).gridShelf.bookListArray.add(0, newBook);
                ((MainActivity) context).gridShelf.setNoOfBooks(((MainActivity) context).gridShelf.bookListArray.size());
             }
            UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId);
            UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles");
            UserFunctions.createNewDirectory(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId);
            UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/tempFiles");
//            String absPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
//            String path= absPath+"/Download/";
            //copying images

            if (!new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/card.png").exists() &&templateImageId==null) {
               // if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles").exists())
                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedBook.getBookID() + "/FreeFiles/card.png", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/card.png");
                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedBook.getBookID() + "/FreeFiles/front_P.png", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/front_P.png");
                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedBook.getBookID() + "/FreeFiles/frontThumb.png", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/frontThumb.png");
                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedBook.getBookID() + "/FreeFiles/thumb.png", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/thumb.png");
                //UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookView.currentBook.getBookID() + "/FreeFiles/back_P.png", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/back_P.png");
                //UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookView.currentBook.getBookID() + "/FreeFiles/backThumb.png", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/backThumb.png");
                UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/fpimages");
                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedBook.get_bStoreID() + "Book/fpimages/1.jpg", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/fpimages/1.jpg");
               // UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/FreeFiles/card.png", path+ "card.png");
            }else{
                int templateId=Integer.parseInt(templateImageId);
                UserFunctions.createNewBooksDirAndCoptTemplates(Globals.TARGET_BASE_TEMPATES_PATH+templateId, Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId+"/FreeFiles");
                String fpImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId + "/fpimages/1.jpg";
                UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/fpimages");
                Bitmap bitmap = BitmapFactory.decodeFile(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/front_P.png");
                creatingBitMapImage(bitmap, fpImagePath, 758, 964);
                //UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeBookId + "Book/fpimages/" + storeBookTotalPages + ".jpg", Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId + "/fpimages/" + totalPages + ".jpg");
                Bitmap b = BitmapFactory.decodeFile(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/back_P.png");
                String flipPath= Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId + "/fpimages/"+ totalPages +".jpg";
                creatingBitMapImage(b, flipPath, 758, 964);
                generatecoverPageHtml(totalPages, Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId +"/" + totalPages + ".htm");
            }

            // UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookView.currentBook.get_bStoreID() + "Book/Book.xml", Globals.TARGET_BASE_BOOKS_DIR_PATH + "C" + bookId + "Book/Book.xml");
            UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedBook.get_bStoreID() + "Book/FIndex.xml", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/FIndex.xml");
            UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedBook.get_bStoreID() + "Book/Search.xml", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/Search.xml");

            generatecoverPageHtml(1,Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId +"/1.htm");
            String flipPath="fpimages/1.jpg";
            bookXml = generateBookXML(totalPages,1,bookXml,flipPath);

            //copying html pages
            for (int i = 0; i < PageList.size(); i++) {
                PageNumbers selectedPage = PageList.get(i);
                int pageNo = i + 2;
                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedPage.getStoreId() + "Book/fpimages/" + selectedPage.getpageNumber() + ".jpg", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/fpimages/" + pageNo + ".jpg");
                //UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedPage.getStoreId() + "Book/" + selectedPage.getpageNumber() + ".htm", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/" + pageNo + ".htm");
                File oldFile=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + selectedPage.getStoreId() + "Book/" + selectedPage.getpageNumber() + ".htm");
               // String sourceString=UserFunctions.decryptFile(oldFile);
                String sourceString=UserFunctions.readFileFromPath(oldFile);
                creatingFile(sourceString,Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/" + pageNo + ".htm");
                File file = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId +"/" + pageNo + ".htm");
                changeHtmlFile(file, bookId,selectedPage.getStoreId(),selectedPage.getBookID());
                copyingImageInToNewBookPath(selectedPage.getpageNumber(), bookId, pageNo,selectedPage.getBookID(), selectedPage.getStoreId());
                UserFunctions.copyFiles(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + selectedPage.getBookID() + "/thumbnail_" + selectedPage.getpageNumber() + ".png", Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_" + pageNo + ".png");
                flipPath="fpimages/"+ pageNo +".jpg";
                bookXml = generateBookXML(totalPages,pageNo,bookXml,flipPath);
                JSONObject jsonObject=AddingPagestojsonArray(selectedPage,pageNo,bookId);
               // bookDetails=getJsonStoreBookDetails();
                jsonArray.put(jsonObject);

                if (resourceSelected) {
                    int count;
                    Object[] str = new Object[6];
                    if(context instanceof BookViewReadActivity) {
                        count = ((BookViewReadActivity) context).db.getBookMarkCount(selectedPage.getStoreId(), selectedPage.getpageNumber(), 0);
                        if (count > 0) {
                            String query = "insert into tblBookMark(BName,PageNo,TagID,TabNo) values('"+ bookId + "','" + pageNo + "','0','0')";
                            ((BookViewReadActivity) context).db.executeQuery(query);
                        }
                        str= ((BookViewReadActivity)context).db.getAllNotesFromDb(selectedPage.getStoreId(), selectedPage.getpageNumber(), 0);
                    }else  if(context instanceof MainActivity) {
                        count = ((MainActivity) context).db.getBookMarkCount(selectedPage.getStoreId(), selectedPage.getpageNumber(), 0);
                        if (count > 0) {
                            String query = "insert into tblBookMark(BName,PageNo,TagID,TabNo) values('"+ bookId + "','" + pageNo + "','0','0')";
                            ((MainActivity) context).db.executeQuery(query);
                        }
                        str= ((MainActivity)context).db.getAllNotesFromDb(selectedPage.getStoreId(), selectedPage.getpageNumber(), 0);
                    }

                    String[] TID = (String[]) str[0];
                    String[] SText = (String[]) str[1];
                    String[] Ocuurences = (String[]) str[2];
                    String[] NPos = (String[]) str[3];
                    String[] CID = (String[]) str[4];
                    String[] Desc = (String[]) str[5];
                    String[] Ntype = (String[]) str[6];
                    for (int j = 0; j < SText.length; j++) {
                        String query = "insert into tblNote(BName,PageNo,SText,Occurence,SDesc,NPos,Color,ProcessSText,TabNo,Exported,NoteType) values" +
                                "('"+ bookId + "','" + pageNo + "','" + SText[j] + "','" + Ocuurences[j] + "','" + Desc[j] + "','" + NPos[j] + "','" + CID[j] + "','" + SText[j] + "','" + 0 + "','" + "0" + "','" + Ntype[j] + "')";
                        if(context instanceof BookViewReadActivity) {
                            ((BookViewReadActivity) context).db.executeQuery(query);
                        } if(context instanceof MainActivity) {
                            ((MainActivity) context).db.executeQuery(query);
                        }
                    }
                    if(context instanceof BookViewReadActivity) {
                        final String[] sTextArray = ((BookViewReadActivity) context).db.getStext(selectedPage.getStoreId(), selectedPage.getpageNumber(), 0);
                        final int[] occcurenceArray = ((BookViewReadActivity) context).db.getOccurence(selectedPage.getStoreId(), selectedPage.getpageNumber(), 0);
                        for (int j = 0; j < sTextArray.length; j++) {
                            String query = "insert into tblHighlight(BName,PageNo,SText,Occurence,Color,TabNo) values('"+ bookId + "', '" + pageNo + "', '" + sTextArray[j] + "', '" + occcurenceArray[j] + "', '', '" + 0 + "')";
                            ((BookViewReadActivity) context).db.executeQuery(query);
                        }
                        ArrayList<Enrichments> enrichmentLinksList = ((BookViewReadActivity) context).db.getAllEnrichmentLinksForExport(selectedPage.getBookID(), context);
                        for (Enrichments enrichments : enrichmentLinksList) {
                            if (selectedPage.getpageNumber() == enrichments.getEnrichmentPageNo()) {
                                ((BookViewReadActivity) context).db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,categoryID)values('" + bookId + "','" + pageNo + "','" + enrichments.getEnrichmentTitle() + "','Search', '0','" + enrichments.getEnrichmentPath() + "','0')");
                            }
                        }
                    }else if(context instanceof MainActivity) {
                        final String[] sTextArray = ((MainActivity) context).db.getStext(selectedPage.getStoreId(), selectedPage.getpageNumber(), 0);
                        final int[] occcurenceArray = ((MainActivity) context).db.getOccurence(selectedPage.getStoreId(), selectedPage.getpageNumber(), 0);
                        for (int j = 0; j < sTextArray.length; j++) {
                            String query = "insert into tblHighlight(BName,PageNo,SText,Occurence,Color,TabNo) values('" + bookId + "', '" + pageNo + "', '" + sTextArray[j] + "', '" + occcurenceArray[j] + "', '', '" + 0 + "')";
                            ((MainActivity) context).db.executeQuery(query);
                        }
                        ArrayList<Enrichments> enrichmentLinksList = ((MainActivity) context).db.getAllEnrichmentLinksForExport(selectedPage.getBookID(), context);
                        for (Enrichments enrichments : enrichmentLinksList) {
                            if (selectedPage.getpageNumber() == enrichments.getEnrichmentPageNo()) {
                                ((MainActivity) context).db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,categoryID)values('" + bookId + "','" + pageNo + "','" + enrichments.getEnrichmentTitle() + "','Search', '0','" + enrichments.getEnrichmentPath() + "','0')");
                            }
                        }
                    }
                }
               /*
                * for css Books
                */
                if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+selectedPage.getStoreId()+"Book/FreeFiles/css").exists() && !new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId+"/FreeFiles/"+selectedPage.getStoreId()+"/css").exists()){
                    try {
                        UserFunctions.copyDirectory(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+selectedPage.getStoreId()+"Book/FreeFiles/css"),new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId+"/FreeFiles/"+selectedPage.getStoreId()+"/css"));
                        UserFunctions.copyDirectory(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+selectedPage.getStoreId()+"Book/FreeFiles/css"),new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId+"/FreeFiles/"+selectedPage.getStoreId()+"/css"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
            JSONArray bookDetailsJson=getBookDetailsJson();
            JSONObject jsonData=new JSONObject();
            try {
                // String absPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
                //  String path= absPath+"/Download/";

                jsonData.put("malzamah",jsonArray);
                jsonData.put("bookDetails", bookDetailsJson);
                FileWriter file=new FileWriter(new File(filePath));
                file.write(jsonData.toString());
                file.flush();
                file.close();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
           //generateLastPage(totalPages, bookId,selectedBook.getBookID(),selectedBook.get_bStoreID(),selectedBook.getTotalPages());
           // flipPath="fpimages/"+ totalPages +".jpg";
            bookXml=generateBookXML(totalPages, totalPages, bookXml, flipPath);
          //  UserFunctions.createNewBooksDirAndCoptTemplates(Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId + "/FreeFiles/", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/");
            creatingFile(bookXml, Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/Book.xml");
            //UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/FreeFiles/card.png", path+ "card.png");
//            try {
//                UserFunctions.copyDirectory(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId+"/"),new File(path+bookId));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progresdialog.dismiss();

            if(context instanceof BookViewReadActivity) {
                UserFunctions.DisplayAlertDialog(((BookViewReadActivity)context), R.string.malzamah_created, R.string.done);
                //popoverView.dissmissPopover(true);
                ((BookViewReadActivity)context).exportEnrPopover.dismiss();
            }else if(context instanceof MainActivity) {
                UserFunctions.DisplayAlertDialog(((MainActivity)context), R.string.malzamah_created, R.string.done);
                ((MainActivity)context).gridView.invalidateViews();
                ((MainActivity)context).PopoverView.dissmissPopover(true);
            }

        }
    }

    private void copyingImageInToNewBookPath(int pageNumber, int newBookId, int pageNo, int bookID, String storeId) {
        ArrayList<HTMLObjectHolder> originalStoreObjectList = null;
        int maxObjUniqueId=0;
        ArrayList<QuizSequenceObject> enrQuizSequenceObjectArray = null;
        String pNo = String.valueOf(pageNumber);
        if(context instanceof BookViewReadActivity) {
           maxObjUniqueId = ((BookViewReadActivity) context).db.getMaxUniqueRowID("objects");
           originalStoreObjectList = ((BookViewReadActivity) context).db.getAllObjectsFromHomePageToCreateEnrichment(pNo, bookID, 0);

        }else  if(context instanceof MainActivity) {
            maxObjUniqueId = ((MainActivity) context).db.getMaxUniqueRowID("objects");
            originalStoreObjectList = ((MainActivity) context).db.getAllObjectsFromHomePageToCreateEnrichment(pNo, bookID, 0);
        }
         for (HTMLObjectHolder htmlObjectHolder : originalStoreObjectList) {
            maxObjUniqueId = maxObjUniqueId + 1;
            int BID = htmlObjectHolder.getObjBID();
            int objPageNo = Integer.parseInt(htmlObjectHolder.getObjPageNo());
            String objLocation = htmlObjectHolder.getObjXPos() + "|" + htmlObjectHolder.getObjYPos();
            String objSize;
            String objType = htmlObjectHolder.getObjType();
            String objContent = htmlObjectHolder.getObjContentPath();
            int objSequentialId = htmlObjectHolder.getObjSequentialId();
            String objScalePageTofit = htmlObjectHolder.getObjScalePageToFit();
            String objLock = htmlObjectHolder.getObjLocked();
            int objUniqueId = htmlObjectHolder.getObjUniqueRowId();
            int objEnrichId = htmlObjectHolder.getObjEnrichID();
            if (objType.equals(Globals.OBJTYPE_PAGEBG) || objType.equals(Globals.OBJTYPE_MINDMAPBG)||objType.equals("WebTable")) {
                objSize = htmlObjectHolder.getObjSize();
            } else {
                objSize = htmlObjectHolder.getObjWidth() + "|" + htmlObjectHolder.getObjHeight();
            }
            // String objContent = htmlObjectHolder.getObjContentPath();

            String newObjContent = null;
             UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images");
            if (objType.equals("WebText") || objType.equals("TextSquare") || objType.equals("TextRectangle") || objType.equals("TextCircle") || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT) || objType.equals(Globals.objType_pageNoText) || objType.equals("WebTable") || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
                if (objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
                    ArrayList<QuizSequenceObject> homeQuizSequenceObjectArray = ((BookViewReadActivity)context).db.getQuizSequenceArray(objUniqueId);

                    for (int i = 0; i < homeQuizSequenceObjectArray.size(); i++) {
                        if(context instanceof BookViewReadActivity) {
                            ((BookViewReadActivity) context).db.executeQuery("insert into tblMultiQuiz(quizId) values('" + maxObjUniqueId + "')");
                        }else   if(context instanceof MainActivity) {
                            ((MainActivity) context).db.executeQuery("insert into tblMultiQuiz(quizId) values('" + maxObjUniqueId + "')");
                        }
                    }
                    if(context instanceof BookViewReadActivity) {
                       enrQuizSequenceObjectArray = ((BookViewReadActivity) context).db.getQuizSequenceArray(maxObjUniqueId);
                    }else  if(context instanceof MainActivity) {
                        enrQuizSequenceObjectArray = ((MainActivity) context).db.getQuizSequenceArray(maxObjUniqueId);
                    }
                    for (int j = 0; j < enrQuizSequenceObjectArray.size(); j++) {
                        QuizSequenceObject homeQuizSequenceObject = homeQuizSequenceObjectArray.get(j);
                        int homeQuizUniqueId = homeQuizSequenceObject.getUniqueid();
                        int homeQuizSectionId = homeQuizSequenceObject.getSectionId();
                        QuizSequenceObject enrQuizSequenceObject = enrQuizSequenceObjectArray.get(j);
                        int enrQuizUniqueId = enrQuizSequenceObject.getUniqueid();
                        int enrQuizSectionId = enrQuizSequenceObject.getSectionId();

                        if (objContent.contains("q"+homeQuizSectionId+"_img_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_a_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_a_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_a_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_b_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_b_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_b_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_c_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_c_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_c_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_d_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_d_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_d_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_e_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_e_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_e_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }
                        if (objContent.contains("q"+homeQuizSectionId+"_img_f_"+homeQuizUniqueId+"")) {
                            String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_f_"+homeQuizUniqueId+".png";
                            String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/q"+enrQuizSectionId+"_img_f_"+enrQuizUniqueId+".png";
                            UserFunctions.copyFiles(homeImgContent, enrImgContent);
                        }

                        objContent = objContent.replace("previousquestion:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "previousquestion:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
                        objContent = objContent.replace("deletequestion:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "deletequestion:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
                        objContent = objContent.replace("addquestions:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "addquestions:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
                        objContent = objContent.replace("nextquestion:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "nextquestion:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("Title"+homeQuizSectionId+"", "Title"+enrQuizSectionId+"");
                        objContent = objContent.replace("quizTitle_"+homeQuizUniqueId+"", "quizTitle_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("pTitle_"+homeQuizUniqueId+"", "pTitle_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("quiz_question_"+homeQuizUniqueId+"", "quiz_question_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("pQuiz_"+homeQuizUniqueId+"", "pQuiz_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("quiz_options_"+homeQuizUniqueId+"", "quiz_options_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_result_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_result_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_a_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_a_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"a_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"a_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("btnCheckAnswer"+homeQuizSectionId+"_"+homeQuizUniqueId+"", "btnCheckAnswer"+enrQuizSectionId+"_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_b_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_b_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"b_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"b_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_c_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_c_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"c_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"c_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_d_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_d_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"d_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"d_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_e_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_e_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"e_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"e_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_div_f_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_f_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"f_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"f_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("quiz_checkanswer_"+homeQuizUniqueId+"", "quiz_checkanswer_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("btnNext"+homeQuizSectionId+"_"+homeQuizUniqueId+"", "btnNext"+enrQuizSectionId+"_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_a_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_a_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_b_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_b_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_c_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_c_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_d_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_d_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_e_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_e_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("q"+homeQuizSectionId+"_img_f_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_f_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("results_container_"+homeQuizUniqueId+"", "results_container_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("results_"+homeQuizUniqueId+"", "results_"+enrQuizUniqueId+"");
                        objContent = objContent.replace("quiz_result_cont_"+homeQuizUniqueId+"", "quiz_result_cont_"+enrQuizUniqueId+"");
                        objContent=objContent.replace("/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + BID + "/", "/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + newBookId + "/");

                        newObjContent = objContent.replace("'", "''");
                    }
                } else {

                    objContent=objContent.replace("/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + BID + "/", "/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + newBookId + "/");
                    newObjContent = objContent.replace("'", "''");
                }
            }else  if (objType.equals("Image") || objType.equals("DrawnImage")) {
                newObjContent = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBookId + "/images/" + maxObjUniqueId + ".png";
                UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + newBookId + "/images");
                UserFunctions.copyFiles(objContent, newObjContent);
                // bookView.db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + newBookId + "', '" + pageNo + "', '" + objLocation + "', '" + objSize + "', '" + newObjContent + "', '" + objSequentialId + "', '" + objScalePageTofit + "', '" + objLock + "', '0')");

            }
            else if (objType.equals("EmbedCode")) {
                newObjContent = objContent.replace("'", "''");
            } else if (objType.equals("Audio")) {
                newObjContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/"+maxObjUniqueId+".m4a";
                UserFunctions.copyFiles(objContent, newObjContent);
            } else if (objType.equals("IFrame")) {
                newObjContent = objContent;
            } else if (objType.equals("YouTube")) {
                if (objContent.contains("youtube.com")) {
                    newObjContent = objContent;
                } else {
                    newObjContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/"+maxObjUniqueId+".mp4";
                    UserFunctions.copyFiles(objContent, newObjContent);
                    String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/images/"+maxObjUniqueId+".png";
                    String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/"+objUniqueId+".png";
                    UserFunctions.copyFiles(homeImgContent, enrImgContent);
                }
            }
            if(objScalePageTofit.contains("css")){
                 String[] split=objScalePageTofit.split("##");
                 String main="FreeFiles/"+storeId+"/css/main.css";
                 String normalize="FreeFiles/"+storeId+"/css/normalize.css";
                 objScalePageTofit=objScalePageTofit.replace("FreeFiles/css/normalize.css",normalize);
                 objScalePageTofit=objScalePageTofit.replace("FreeFiles/css/main.css",main);
             }
             if(context instanceof BookViewReadActivity){
                 ((BookViewReadActivity)context).db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + newBookId + "', '" + pageNo + "', '" + objLocation + "', '" + objSize + "', '" + newObjContent + "', '" + objSequentialId + "', '" + objScalePageTofit + "', '" + objLock + "', '0')");
             }else  if(context instanceof MainActivity){
                 ((MainActivity)context).db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + newBookId + "', '" + pageNo + "', '" + objLocation + "', '" + objSize + "', '" + newObjContent + "', '" + objSequentialId + "', '" + objScalePageTofit + "', '" + objLock + "', '0')");
             }

        }
    }

    private void changeHtmlFile(File file, int bookId,String storeId,int storeBookId) {
        String sourceString = readFileFromSDCard(file);
        //String sourceString=UserFunctions.decryptFile(file);
        sourceString.replace("file:///data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" +storeId + "/", "file:///data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + bookId + "/");
        copyingImages(sourceString,bookId,storeId,storeBookId);
        /*for css Books
         */
        String normalize="<link rel=\"stylesheet\" href=\"FreeFiles/css/normalize.css\">";
        String main="<link rel=\"stylesheet\" href=\"FreeFiles/css/main.css\">";
        if(sourceString.contains(main)&&sourceString.contains(normalize)){
            String existingMain="FreeFiles/"+storeId+"/css/main.css";
            String existingNormalize="FreeFiles/"+storeId+"/css/normalize.css";
            sourceString=sourceString.replace("FreeFiles/css/normalize.css",existingNormalize);
            sourceString=sourceString.replace("FreeFiles/css/main.css",existingMain);
        }

        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(sourceString);
            fileWriter.close();
           // UserFunctions.encryptFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyingImages(String sourceSting, int bookId,String storeId,int StoreBookId) {
        if (sourceSting.contains("src")) {
            String[] splitStr = sourceSting.split("src");
            if (splitStr.length > 1) {
                for (int i = 0; i < splitStr.length; i++) {
                    if (i > 0) {
                        String[] path1 = splitStr[i].split("\"");
                        String[] folderPath = path1[1].split("/");
                        if(folderPath.length>1) {
                            UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId +"/" + folderPath[0]);
                            if(folderPath.length>2){
                                UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId+"/"+ folderPath[0]+"/"+folderPath[1]);
                                if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+StoreBookId+"/"+ folderPath[0]+"/"+folderPath[1]).exists()) {
                                    UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + folderPath[0] + "/" + folderPath[1]);
                                }
                            }
                            UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH +storeId + "Book/" + path1[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/"+path1[1]);
                            if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+StoreBookId+"/"+ folderPath[0]).exists()) {
                                UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + folderPath[0]);
                            }
                            UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + path1[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId+"/"+path1[1]);
                        }else{
                            UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + path1[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/"+path1[1]);
                            if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+StoreBookId+"/"+ path1[1]).exists()) {
                                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + path1[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId+"/"+path1[1]);
                            }
                        }

                    }
                }
            }
        }
        if (sourceSting.contains("background")) {
            String[] splitStr = sourceSting.split("background");
            if (splitStr.length > 1) {
                for (int i = 0; i < splitStr.length; i++) {
                    if (i > 0) {
                        String[] p = splitStr[i].split("\"");
                        String[] folderPath = p[1].split("/");  //FreeFiles/front.png
                        if(folderPath.length>1) {
                            UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/" + folderPath[0]);
                            UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId+"/" + folderPath[0]);
                            UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + p[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/" + p[1]);
                            if(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+StoreBookId+"/"+ p[1]).exists()) {
                                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + p[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/" + p[1]);
                            }
                        }else{
                            UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + p[1], Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId +"/" + p[1]);
                            if(new  File(Globals.TARGET_BASE_BOOKS_DIR_PATH+StoreBookId+"/"+ p[1]).exists()) {
                                UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "Book/" + p[1], Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + p[1]);
                            }
                        }
                    }
                }
            }
        }
    }


    private void generatecoverPageHtml(int pageNo, String path) {
        String html = "<html>\n" +
                "<head>\n" +
                "<meta http-equiv='Content-Type' content='text/html'; charset='utf-8'>\n" +
                "<title>Cover Page</title>\n" +
                "<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n" +
                "</meta>\n" +
                "</head>\n" +
                " <body topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" bottommargin=\"0\" marginheight=\"0\" marginwidth=\"0\">\n" +
                "<div id='bgImage' class=\"bgImage\" style='background-image:url(fpimages/"+pageNo+".jpg); background-repeat: no-repeat; -webkit-background-size:100%;width:100%; height:100%; left:0px; top:0px; position:absolute;' >\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";
        try {
            FileWriter fileWriter = new FileWriter(path);
            fileWriter.write(html);
            fileWriter.close();
            File file = new File(path);
            //UserFunctions.encryptFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String generateBookXML(int totalPages,int pageNo, String bookXml, String flipPath){
        String xml;
        if (pageNo == 1) {
            xml = bookXml.concat("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Book><Cover1 Flip=\""+flipPath+"\" Path=\"1.htm\" ETitle=\"Cover 1\" ATitle=\"1 الغلاف\"/>");
        }else if (pageNo == totalPages) {
            xml = bookXml.concat("<Cover2 Flip=\""+flipPath+"\" Path=\""+pageNo+".htm\" ETitle=\"Cover 2\"  ATitle=\"2 الغلاف\"/></Book>");
        } else {
            int tempPageNo = pageNo - 1;
            xml = bookXml.concat("<P"+tempPageNo+" Flip=\""+flipPath+"\" Path=\""+pageNo+".htm\"  ATitle=\""+tempPageNo+" صفحة\" ETitle=\"Page"+tempPageNo+"\"/>");

        }
        return xml;
    }

    private void creatingFile(String sourceString,String path){
        try {
            FileWriter fileWriter = new FileWriter(path);
            fileWriter.write(sourceString);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void creatingBitMapImage(Bitmap bitmap, String Path,int width,int height){
        Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(width, context), Globals.getDeviceIndependentPixels(height, context), true);
        UserFunctions.saveBitmapImage(cardBitmap, Path);
    }

    public void creatingCoverImage(Bitmap picturePath) {
        int lastBookId = 0;
        if(context instanceof BookViewReadActivity) {
            lastBookId = ((BookViewReadActivity) context).db.getMaxUniqueRowID("books");
        }else   if(context instanceof MainActivity){
            lastBookId = ((MainActivity) context).db.getMaxUniqueRowID("books");
        }
        int bookId = lastBookId + 1;
        String freeFilesPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId+ "/FreeFiles/card.png";
        UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId);
        UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId);
        UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles");
        creatingBitMapImage(picturePath, freeFilesPath, 200, 300);

        String frontThumbPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/frontThumb.png";
        creatingBitMapImage(picturePath, frontThumbPath, 130, 170);

        String thumb_path = freeFilesPath + "thumb.png";
        creatingBitMapImage(picturePath, thumb_path, 130, 170);

        String front_p = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/front_P.png";
        creatingBitMapImage(picturePath, front_p, 800, 1182);

        String fpImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId + "/fpimages/1.jpg";
        UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/fpimages");
        creatingBitMapImage(picturePath, fpImagePath, 758, 964);
    }

    public void imagePicked(Bitmap bitmap) {
        imgViewCover.setImageBitmap(bitmap);
        isDeleteBookDir = true;
        creatingCoverImage(bitmap);
    }

    private class ListAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return currentBookPageList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
                view = ((BookViewReadActivity)context).getLayoutInflater().inflate(R.layout.malzamah_inflate_bookpageslist, null);
            }
            TextView tv_pageno = (TextView) view.findViewById(R.id.tv_pageNo);
            ImageView image = (ImageView) view.findViewById(R.id.pg_image);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            PageNumbers page = currentBookPageList.get(position);
            if (page.isChecked()) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            PageNumbers pageNumberObj = currentBookPageList.get(position);
            int pageNo = pageNumberObj.getpageNumber();
            tv_pageno.setText("Page" + (pageNo-1));
            String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + ((BookViewReadActivity)context).currentBook.getBookID() + "/thumbnail_" + pageNo + ".png";
            Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
            image.setImageBitmap(bitmap);

            return view;
        }
    }

    private class MalzBookListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return malzamahBookList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            View view = convertView;
            if (view == null) {
                view = ((BookViewReadActivity)context).getLayoutInflater().inflate(R.layout.malzamah_inflate_book_image, null);
                Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(viewGroup,font);
            }
            ImageView image = (ImageView) view.findViewById(R.id.iv_bookimage);
            image.setVisibility(View.GONE);
            TextView tv_bookName=(TextView)view.findViewById(R.id.tv_pageNo);
            String List = malzamahBookList.get(position);
            String[] bookList=List.split("##");

         /*   String  BID = bookList[4];
            String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/fpimages/1.jpg";
            if(!new File(bookCardImagePath).exists()){
                int id=Integer.parseInt(bookList[0]);
                bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+id+"/FreeFiles/card.png";
            }
            Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
            image.setImageBitmap(bitmap);*/
            if(wantedPosition==position){
                if(clicked) {
                    view.setBackgroundColor(Color.parseColor("#604e3634"));
                }
            }else{
                view.setBackgroundColor(Color.TRANSPARENT);
            }
            tv_bookName.setText(bookList[1]);

            return view;
        }
    }

   /* public class PageNumbers {
        private int pageNumber;
        private boolean isChecked;


        /**
         * @return the searchValues
         */
//       public int getpageNumber() {
//            return pageNumber;
//        }

        /**
         * @param pagenumber the searchValues to set
         */
//        public void setpageNumber(int pagenumber) {
//            this.pageNumber = pagenumber;
//        }

        /**
         * @return the isChecked
         */
//        public boolean isChecked() {
//            return isChecked;
//        }

        /**
         * @paramisChecked the isChecked to set
         */
//        public void setChecked(boolean isChecked) {
//            this.isChecked = isChecked;
//        }
  //  }

    public  void deleteCretaedBookDir(){
        if (isDeleteBookDir) {
            int lastBookId;
            if(context instanceof BookViewReadActivity ) {
                 lastBookId = ((BookViewReadActivity) context).db.getMaxUniqueRowID("books");
            }else{
                lastBookId = ((MainActivity) context).db.getMaxUniqueRowID("books");
            }
            int bookId = lastBookId + 1;
            File bookPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId + "Book");
            File bookIdPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId);
            UserFunctions.DeleteDirectory(bookPath);
            UserFunctions.DeleteDirectory(bookIdPath);
        }
    }

    @Override
    public void popoverViewWillShow(PopoverView view) {

    }

    @Override
    public void popoverViewDidShow(PopoverView view) {

    }

    @Override
    public void popoverViewWillDismiss(PopoverView view) {
        deleteCretaedBookDir();
    }

    @Override
    public void popoverViewDidDismiss(PopoverView view) {

    }
   private void generateLastPage(int totalPages, int bookId,int storeId,String storeBookId,int storeBookTotalPages ){
       UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "/FreeFiles/back_P.png", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/back_P.png");
       UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeId + "/FreeFiles/backThumb.png", Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/backThumb.png");
       UserFunctions.copyFiles(Globals.TARGET_BASE_BOOKS_DIR_PATH + storeBookId + "Book/fpimages/" + storeBookTotalPages + ".jpg", Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId + "/fpimages/" + totalPages + ".jpg");
       generatecoverPageHtml(totalPages, Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId +"/" + totalPages + ".htm");
   }

    private void changeEndpageFiles(int bookId,int existingPage,int totalPages){
        File olPath=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId + "/fpimages/"+ existingPage + ".jpg");
        File newPath=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/fpimages/"+ totalPages + ".jpg");
        olPath.renameTo(newPath);

        File olhtm=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/"+ existingPage +".htm");
        File newhtm=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/"+ totalPages +".htm");
        olhtm.renameTo(newhtm);

      //  String sourceString = UserFunctions.decryptFile(newhtm);
        //if(sourceString==null) {
        if(newhtm.exists()) {
            String sourceString = UserFunctions.readFileFromPath(newhtm);
            //   }
            //String sourceString=UserFunctions.decryptFile(newhtm);
            sourceString = sourceString.replace(existingPage + ".jpg", totalPages + ".jpg");
            creatingFile(sourceString, Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/" + totalPages + ".htm");
        }
        //UserFunctions.encryptFile(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId +"/"+ totalPages +".htm"));
     }

    private JSONArray readJsonFile(String filePath){
        File file=new File(filePath);
        String jsonStr=null;
        JSONArray jsonarray = null;
        try {
            FileInputStream stream = new FileInputStream(file);
            FileChannel fc = stream.getChannel();
            try {
                stream = new FileInputStream(file);
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                jsonStr = Charset.defaultCharset().decode(bb).toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }

            JSONObject jsonObject=new JSONObject(jsonStr);
            JSONArray data=jsonObject.getJSONArray("malzamah");
            JSONArray bookDetails=jsonObject.getJSONArray("bookDetails");
            //String  d= data.toString().replace("[","");
            jsonarray = new JSONArray();
            for(int i=0;i<data.length();i++){
                JSONObject jsonSub = data.getJSONObject(i);
                jsonarray.put(jsonSub);
            }
            for(int i=0;i<bookDetails.length();i++){
                JSONObject jsonSub = bookDetails.getJSONObject(i);
                String description=jsonSub.getString("description");
                String purchaseType=jsonSub.getString("purchaseType");
                String author=jsonSub.getString("author");
                String totalPages=jsonSub.getString("totalPages");
                String title=jsonSub.getString("title");
                String price=jsonSub.getString("price");
                String storeID=jsonSub.getString("storeID");
                String domainURL=jsonSub.getString("domainURL");
                String language=jsonSub.getString("language");
                String imageURL=jsonSub.getString("imageURL");
                String isEditable=jsonSub.getString("isEditable");
                String downloadURL=jsonSub.getString("downloadURL");
//                String isMalzamah=jsonSub.getString("isMalzamah");
//                String storebookFromIpad=jsonSub.getString("storebookFromIpad");
//                String lastVisited=jsonSub.getString( "LastViewedPage");
//                String categoryId=jsonSub.getString("CID");
                String bookData=description+"##"+purchaseType+"##"+author+"##"+totalPages+"##"+title+"##"+price+"##"+storeID+"##"+domainURL+"##"+language+"##"+imageURL+"##"+isEditable+"##"+downloadURL;
                malzamahBookDetails.add(bookData);
                // jsonBookDetails.put(jsonSub);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonarray;
    }
    private JSONObject AddingPagestojsonArray(PageNumbers selectedPage, int pageNo, int bookId) {
        JSONObject groups = new JSONObject();
        int pNo=selectedPage.getpageNumber();
        String Bid=String.valueOf(bookId);
        String htmlPath=selectedPage.getStoreId()+"Book/"+pNo+".htm";
        String sourceThumbPath="Book_" + selectedPage.getBookID() + "/thumbnail_" + pNo + ".png";
        String domainUrl=null;
        try {
            groups.put("thumbPath", sourceThumbPath);
            groups.put("BID", Bid);
            groups.put("htmlPath",htmlPath);
            groups.put("page",pageNo);
            if(context instanceof BookViewReadActivity) {
                ((BookViewReadActivity) context).db.executeQuery("insert into malzamah(BID,pageNo, sourceStoreID,sourceHtmlPath,sourceThumbPath)" +
                        "values('" + bookId+ "','"+pageNo+"','" +selectedPage.getStoreId() + "','" +htmlPath+"','" + sourceThumbPath+ "')");
                domainUrl=((BookViewReadActivity) context).db.getStoreBookDomainUrl(selectedPage.getStoreId(),selectedPage.getBookID());
            }else  if(context instanceof MainActivity) {
                ((MainActivity) context).db.executeQuery("insert into malzamah(BID,pageNo, sourceStoreID,sourceHtmlPath,sourceThumbPath)" +
                        "values('" + bookId+ "','"+pageNo+"','"+selectedPage.getStoreId()+"','" +htmlPath+"','" + sourceThumbPath+ "')");

                domainUrl=((MainActivity) context).db.getStoreBookDomainUrl(selectedPage.getStoreId(),selectedPage.getBookID());
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
        boolean bookExist=false;
        String bookDetails=selectedPage.getBook().get_bDescription()+"##"+selectedPage.getBook().getPurchaseType()+"##"+selectedPage.getBook().getBookAuthorName()+"##"+selectedPage.getBook().getTotalPages()+"##"+selectedPage.getBook().getBookTitle()+"##"+selectedPage.getBook().getPrice()+"##"+selectedPage.getBook().get_bStoreID()+"##"+domainUrl+"##"+selectedPage.getBook().getBookLangugae()+"##"+selectedPage.getBook().getImageURL()+"##"+selectedPage.getBook().getIsEditable()+"##"+selectedPage.getBook().getDownloadURL();
        if(malzamahBookDetails!=null&&malzamahBookDetails.size()>0){
            for(int i=0;i<malzamahBookDetails.size();i++){
                String data=malzamahBookDetails.get(0);
                String split[]=data.split("##");
                if(split[6].equals(selectedPage.getBook().get_bStoreID())){
                    bookExist=true;
                    break;
                }
            }
        }
        if(!bookExist){
            malzamahBookDetails.add(bookDetails);
        }

        return groups;
    }
    public void showAddNewBookDialog(final boolean createMalzamah, final String title, final String description, final ArrayList<PageNumbers> selectedPageList, final Book currentBook){
        final Dialog addNewBookDialog = new Dialog(((BookViewReadActivity)context));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        addNewBookDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addNewBookDialog.setContentView(((BookViewReadActivity)context).getLayoutInflater().inflate(R.layout.add_new_book_dialog, null));
        addNewBookDialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.3), (int)(Globals.getDeviceHeight()/1.5));

        final SegmentedRadioGroup btnSegmentGroup = (SegmentedRadioGroup) addNewBookDialog.findViewById(R.id.btn_segment_group);
        Button btn = (Button) addNewBookDialog.findViewById(R.id.book_btn);
        btn.setVisibility(View.GONE);
        final ProgressBar progressBar = (ProgressBar) addNewBookDialog.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        CircleButton back_btn = (CircleButton) addNewBookDialog.findViewById(R.id.btnback);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewBookDialog.dismiss();
            }
        });
        final OnlineTemplatesGridAdapter adapter = new OnlineTemplatesGridAdapter(context,templatesList);
        final RecyclerView templateGrid = (RecyclerView) addNewBookDialog.findViewById(R.id.gridView1);
        final RadioButton online_temp = (RadioButton)addNewBookDialog.findViewById(R.id.online);
        Button create_btn = (Button) addNewBookDialog.findViewById(R.id.btnNext);
        create_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof BookViewReadActivity){
                    if (!onlineTemplates) {
                        online_temp.setChecked(true);
                        onlineTemplates = true;
                        progressBar.setVisibility(View.VISIBLE);
                        templateGrid.setVisibility(View.INVISIBLE);
                        apiCallBackTask(progressBar, adapter,templateGrid);
                    }
                    Intent bookViewIntent = new Intent(((BookViewReadActivity)context), UserTemplates.class);
                    ((BookViewReadActivity)context).startActivity(bookViewIntent);
                }else if (context instanceof MainActivity){
                    if (!onlineTemplates) {
                        online_temp.setChecked(true);
                        onlineTemplates = true;
                        progressBar.setVisibility(View.VISIBLE);
                        templateGrid.setVisibility(View.INVISIBLE);
                        apiCallBackTask(progressBar, adapter,templateGrid);
                    }
                    Intent bookViewIntent = new Intent(((MainActivity)context), UserTemplates.class);
                    ((MainActivity)context).startActivity(bookViewIntent);
                }

            }
        });
        SegmentedRadioButton segmentedRadioGroup = (SegmentedRadioButton)addNewBookDialog.findViewById(R.id.segment_text);
        segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.online) {
                    onlineTemplates = true;
                    progressBar.setVisibility(View.VISIBLE);
                    templateGrid.setVisibility(View.INVISIBLE);
                    apiCallBackTask(progressBar,adapter,templateGrid);
                } else if (checkedId == R.id.create) {
                    onlineTemplates = false;
                    templateGrid.setVisibility(View.VISIBLE);
                    templatesList.clear();
                    progressBar.setVisibility(View.GONE);
                    String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
                    File file = new File(templatePath);
                    File[] path = file.listFiles();
                    for(File tempFile : path){
                        OnlineTemplates templates1 = new OnlineTemplates();
                        templates1.setTemplateName("Template "+tempFile.getName());
                        templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
                        templates1.setThumbUrl("");
                        templates1.setContentUrl("");
                        if (Integer.parseInt(tempFile.getName())<1000){
                            templates1.setDownloadStatus("Downloaded");
                        }else{
                            templates1.setDownloadStatus("Created");
                            templatesList.add(templates1);
                        }
                    }
                    final OnlineTemplatesGridAdapter tempAdapter = new OnlineTemplatesGridAdapter(context,templatesList);
                    templateGrid.setAdapter(tempAdapter);
                }
            }
        });
        GridLayoutManager gridLayout = new GridLayoutManager(context, context.getResources().getInteger(R.integer.grid_column), GridLayoutManager.VERTICAL, false);
        templateGrid.setLayoutManager(gridLayout);
        templateGrid.setItemAnimator(new DefaultItemAnimator());
        templateGrid.addOnItemTouchListener(new RecyclerTouchListener(context, templateGrid, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                OnlineTemplates templates = templatesList.get(position);
                if (!isDownloading(templates.getTemplateId()) && !isDownloaded(templates.getTemplateId())){
                    addTemplateIdToSharedPreferences(Globals.DownloadingTemplates,templates.getTemplateId());
                    updateTemplateList(position);
                    adapter.notifyDataSetChanged();
                }else if (isDownloaded(templates.getTemplateId())){
                    addNewBookDialog.dismiss();
                    int templateId = view.getId();
                    templateImageId=String.valueOf(templateId);
                    coverPageSelected=true;
                    if(createMalzamah) {
                        new CreateMalzamahBook(title, description, selectedPageList, currentBook).execute();
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        apiCallBackTask(progressBar,adapter,templateGrid);
        btnSegmentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

            }
        });
        addNewBookDialog.show();
    }

    private void parseAndSaveTemplatesList(ArrayList<Object> templates){
        templatesList.clear();
        for (int i=0;i<templates.size();i++){
            HashMap<String,Object> hashMap = (HashMap<String, Object>) templates.get(i);
            OnlineTemplates templates1 = new OnlineTemplates();
            templates1.setTemplateName((String) hashMap.get("name"));
            templates1.setTemplateId((Integer) hashMap.get("ID"));
            templates1.setThumbUrl((String) hashMap.get("ThumbURL"));
            templates1.setContentUrl((String) hashMap.get("contentURL"));
            if (isDownloading(templates1.getTemplateId())){
                templates1.setDownloadStatus("Downloading");
            }else {
                if (new File(Globals.TARGET_BASE_TEMPATES_PATH + templates1.getTemplateId()).exists()) {
                    templates1.setDownloadStatus("Downloaded");
                } else {
                    templates1.setDownloadStatus("Not Downloaded");
                }
            }
            templatesList.add(templates1);
        }
    }

    private void addTemplateIdToSharedPreferences(String key,int ID){
        SharedPreferences.Editor editor = sharedPreference.edit();
        ArrayList<Integer> existList = null;
        try {
            existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<Integer>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        existList.add(ID);
        try {
            editor.putString(key, ObjectSerializer.serialize(existList));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        editor.commit();
    }
    private boolean isDownloading(int Id){
        ArrayList<Integer> existList = null;
        try {
            existList = (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.DownloadingTemplates, ObjectSerializer.serialize(new ArrayList<Integer>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i=0;i<existList.size();i++){
            int existId = existList.get(i);
            if (existId == Id){
                return true;
            }
        }
        return false;
    }
    private void updateTemplateList(int position){
        for (int i=0;i<templatesList.size();i++){
            OnlineTemplates templates = templatesList.get(i);
            if (position == i){
                templates.setDownloadStatus("Downloading");
                break;
            }
        }
    }
    private boolean isDownloaded(int Id){
        for (int i=0;i<templatesList.size();i++){
            OnlineTemplates templates = templatesList.get(i);
            if (Id == templates.getTemplateId()&& (templates.getDownloadStatus().equals("Downloaded") || templates.getDownloadStatus().equals("Created"))){
                return true;
            }
        }
        return false;
    }

    public void showCoverImagesForBook(final boolean createMalzamah, final String title, final String description, final ArrayList<PageNumbers> selectedPageList, final Book currentBook){
        final Dialog addNewBookDialog = new Dialog(((MainActivity)context));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        addNewBookDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addNewBookDialog.setContentView(((MainActivity)context).getLayoutInflater().inflate(R.layout.add_new_book_dialog, null));
        addNewBookDialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.3), (int)(Globals.getDeviceHeight()/1.5));

        final SegmentedRadioGroup btnSegmentGroup = (SegmentedRadioGroup) addNewBookDialog.findViewById(R.id.btn_segment_group);
        Button btn = (Button) addNewBookDialog.findViewById(R.id.book_btn);
        btn.setVisibility(View.GONE);
        final ProgressBar progressBar = (ProgressBar) addNewBookDialog.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        CircleButton back_btn = (CircleButton) addNewBookDialog.findViewById(R.id.btnback);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewBookDialog.dismiss();
            }
        });
        final OnlineTemplatesGridAdapter adapter = new OnlineTemplatesGridAdapter(context,templatesList);
        final RecyclerView templateGrid = (RecyclerView) addNewBookDialog.findViewById(R.id.gridView1);
        final RadioButton online_temp = (RadioButton)addNewBookDialog.findViewById(R.id.online);
        Button create_btn = (Button) addNewBookDialog.findViewById(R.id.btnNext);
        create_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof BookViewReadActivity){
                    if (!onlineTemplates) {
                        online_temp.setChecked(true);
                        onlineTemplates = true;
                        progressBar.setVisibility(View.VISIBLE);
                        templateGrid.setVisibility(View.INVISIBLE);
                        apiCallBackTask(progressBar, adapter,templateGrid);
                    }
                    Intent bookViewIntent = new Intent(((BookViewReadActivity)context), UserTemplates.class);
                    ((BookViewReadActivity)context).startActivity(bookViewIntent);
                }else if (context instanceof MainActivity){
                    if (!onlineTemplates) {
                        online_temp.setChecked(true);
                        onlineTemplates = true;
                        progressBar.setVisibility(View.VISIBLE);
                        templateGrid.setVisibility(View.INVISIBLE);
                        apiCallBackTask(progressBar, adapter,templateGrid);
                    }
                    Intent bookViewIntent = new Intent(((MainActivity)context), UserTemplates.class);
                    ((MainActivity)context).startActivity(bookViewIntent);
                }

            }
        });
        SegmentedRadioButton segmentedRadioGroup = (SegmentedRadioButton)addNewBookDialog.findViewById(R.id.segment_text);
        segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.online) {
                    onlineTemplates = true;
                    progressBar.setVisibility(View.VISIBLE);
                    templateGrid.setVisibility(View.INVISIBLE);
                    apiCallBackTask(progressBar,adapter,templateGrid);
                } else if (checkedId == R.id.create) {
                    onlineTemplates = false;
                    templateGrid.setVisibility(View.VISIBLE);
                    templatesList.clear();
                    progressBar.setVisibility(View.GONE);
                    String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
                    File file = new File(templatePath);
                    File[] path = file.listFiles();
                    for(File tempFile : path){
                        OnlineTemplates templates1 = new OnlineTemplates();
                        templates1.setTemplateName("Template "+tempFile.getName());
                        templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
                        templates1.setThumbUrl("");
                        templates1.setContentUrl("");
                        if (Integer.parseInt(tempFile.getName())<1000){
                            templates1.setDownloadStatus("Downloaded");
                        }else{
                            templates1.setDownloadStatus("Created");
                            templatesList.add(templates1);
                        }
                    }
                    final OnlineTemplatesGridAdapter tempAdapter = new OnlineTemplatesGridAdapter(context,templatesList);
                    templateGrid.setAdapter(tempAdapter);
                }
            }
        });
        GridLayoutManager gridLayout = new GridLayoutManager(context, context.getResources().getInteger(R.integer.grid_column), GridLayoutManager.VERTICAL, false);
        templateGrid.setLayoutManager(gridLayout);
        templateGrid.setItemAnimator(new DefaultItemAnimator());
        apiCallBackTask(progressBar,adapter,templateGrid);
        templateGrid.addOnItemTouchListener(new RecyclerTouchListener(context, templateGrid, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                OnlineTemplates templates = templatesList.get(position);
                if (!isDownloading(templates.getTemplateId()) && !isDownloaded(templates.getTemplateId())){
                    addTemplateIdToSharedPreferences(Globals.DownloadingTemplates,templates.getTemplateId());
                    updateTemplateList(position);
                    adapter.notifyDataSetChanged();
                }else if (isDownloaded(templates.getTemplateId())){
                    addNewBookDialog.dismiss();
                    int templateId = view.getId();
                    templateImageId=String.valueOf(templateId);
                    coverPageSelected=true;
                    if(createMalzamah) {
                        new CreateMalzamahBook(title, description, selectedPageList, currentBook).execute();
                    }
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        btnSegmentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

            }
        });

        addNewBookDialog.show();
    }
    private static String readFileFromSDCard(File file)
    {
        if(!file.exists())
        {
            //changes for empty mainpage in book
            String filePath = "file:///android_asset/nomainpage.html";
            String sourceString = "<html> <head> <title>Main Page Not Found</title> </head> <body><!-- inullpageexists --> </body> </html>";
            return sourceString;
            //throw new RuntimeException("File not found");
        }

        BufferedReader reader = null;
        StringBuilder builder = null;
        try
        {
            reader = new BufferedReader(new FileReader(file));
            builder = new StringBuilder();
            String line;
            while((line = reader.readLine())!=null)
            {
                builder.append(line+"\n");//bug fixed
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if(reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (Exception e2)
                {
                    e2.printStackTrace();
                }
            }
        }
        return builder.toString();
    }
    private JSONArray getBookDetailsJson(){
        JSONArray jsonData = new JSONArray();
       for(int i=0;i<malzamahBookDetails.size();i++){
           String data=malzamahBookDetails.get(i);
           String split[]=data.split("##");
           JSONObject jsonSub=new JSONObject();
           try {
               jsonSub.put("description", split[0]);
               jsonSub.put("purchaseType",split[1]);
               jsonSub.put("author",split[2]);
               jsonSub.put("totalPages",split[3]);
               jsonSub.put("title",split[4]);
               jsonSub.put("price",split[5]);
               jsonSub.put("storeID",split[6]);
               jsonSub.put("domainURL",split[7]);
               jsonSub.put("language",split[8]);
               jsonSub.put("imageURL",split[9]);
               jsonSub.put("isEditable",split[10]);
               jsonSub.put("downloadURL",split[11]);
               jsonData.put(jsonSub);
           } catch (JSONException e) {
               e.printStackTrace();
           }
       }
        return jsonData;
    }
    private void apiCallBackTask(final ProgressBar progressBar, final OnlineTemplatesGridAdapter adapter, final RecyclerView templateGrid){
        templateGrid.setVisibility(View.INVISIBLE);
        ApiCallBackTask apiCallBackTask = new ApiCallBackTask(context,Globals.templatesUrl,null, new ApiCallBackListener<Object>() {

            @Override
            public void onSuccess(Object object) {
                progressBar.setVisibility(View.GONE);
                templateGrid.setVisibility(View.VISIBLE);
                if (onlineTemplates) {
                    templatesList.clear();
                    if (object != null) {
                        Map<String, Object> jsonMap = (Map<String, Object>) object;
                        ArrayList<Object> mapObject = (ArrayList<Object>) jsonMap.get("Templates");
                        parseAndSaveTemplatesList(mapObject);
                    } else {
                        String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
                        File file = new File(templatePath);
                        File[] path = file.listFiles();
                        for (File tempFile : path) {
                            OnlineTemplates templates1 = new OnlineTemplates();
                            templates1.setTemplateName("Template " + tempFile.getName());
                            templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
                            templates1.setThumbUrl("");
                            templates1.setContentUrl("");
                            if (Integer.parseInt(tempFile.getName())<1000){
                                templates1.setDownloadStatus("Downloaded");
                                templatesList.add(templates1);
                            }else{
                                templates1.setDownloadStatus("Created");
                            }

                        }
                    }
                    templateGrid.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Exception e) {
                templatesList.clear();
                templateGrid.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                String templatePath = Globals.TARGET_BASE_TEMPATES_PATH;
                File file = new File(templatePath);
                File[] path = file.listFiles();
                for(File tempFile : path){
                    OnlineTemplates templates1 = new OnlineTemplates();
                    templates1.setTemplateName("Template "+tempFile.getName());
                    templates1.setTemplateId(Integer.parseInt(tempFile.getName()));
                    templates1.setThumbUrl("");
                    templates1.setContentUrl("");
                    if (Integer.parseInt(tempFile.getName())<1000){
                        templates1.setDownloadStatus("Downloaded");
                        templatesList.add(templates1);
                    }else{
                        templates1.setDownloadStatus("Created");
                    }
                }
                templateGrid.setAdapter(adapter);
            }
        });
        apiCallBackTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
