package com.semanoor.manahij;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.WebDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.ptg.views.CircleButton;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.manahij.mqtt.CallConnectionStatusService;
import com.semanoor.manahij.mqtt.CallbackInterface;
import com.semanoor.manahij.mqtt.MQTTSubscriptionService;
import com.semanoor.manahij.mqtt.NoorActivity;
import com.semanoor.manahij.mqtt.NoorUserStatus;
import com.semanoor.manahij.mqtt.datamodels.OnlineStatus;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.Note;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.BookExportDialog;
import com.semanoor.source_sboookauthor.Category;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.GenerateHTML;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.MyContacts;
import com.semanoor.source_sboookauthor.MyGroups;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.UserGroups;
import com.semanoor.source_sboookauthor.WebService;
import com.semanoor.source_sboookauthor.Zip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Krishna on 03-10-2016.
 */
public class ExportEnrGroupActivity  extends NoorActivity implements View.OnClickListener,CallbackInterface{
    DatabaseHandler db;
    Globals global;
    Book currentBook;
    GridShelf gridShelf;
    String[] categoryList;
    ArrayList<com.semanoor.source_sboookauthor.Category> Category;
    ExportEnrichmentDetails enrichmentDetails;
   // ProgressDialog progresdialog;

    private InputStream chunkInputStream;
    private ByteArrayOutputStream chunkBaos;
    private byte[] chunkBytes;
    private int chunkBytesRead;
    String filepath;
    File enrichZipFilePath;
    String enrichXmlString;
    private String chunkBookFlag;
    private int chunkNoOfExecution;
    String desc;
    EditText et_resourcetitle;

    public String userName;
    public String scopeId;
    public int scopeType;
    public String email;

    HashMap dicEnrichments;

    boolean isprivate;
    String mailid;
    String resourceTitle;
    private boolean enrichmentTabSelected = true, noteTabSelected = false, enrichmentLinkSelected = false, groupSelected=false,emailSelected=false, mindmapTabSelected = false;
    public GDriveExport gDriveExport;
    WebService webService;
    public ArrayList<UserGroups> userGroups;
    UserContactDetails userContacts;

    public static final int MY_PERMISSIONS_REQUEST_GET_ACCOUNTS = 100001;
    public static int AUTH_CODE_REQUEST_CODE = 1010;
    public boolean grpSelected=false;
    RecyclerView recyclerView;
    MyGroups selectGroup;
    MyBroadcastReceiver receiver;
    ArrayList<OnlineStatus> statusList ;
    ArrayList<MyGroups> groupList;
    ArrayList<MyContacts> myContacts;
    ArrayList<MyContacts> selectedContact=new ArrayList<MyContacts> ();
    ArrayList<MyContacts> gmailContacts=new ArrayList<MyContacts> ();
    String themesPath =  Globals.TARGET_BASE_FILE_PATH+"Themes/current/";
    String isEditable = "no";
    private AdView adView;
    private Groups groups;
    private String[] share = {"Facebook","Twitter","Email"};
    Dialog sharePopUp;
    boolean Share;
    private IabHelper mHelper;
    public boolean fromCloud = false;
    ManahijApp app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        loadLocale();


        //Remove title Bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        db = DatabaseHandler.getInstance(this);

        global = Globals.getInstance();
        new getToken().execute();
         receiver= new MyBroadcastReceiver();
        userContacts=UserContactDetails.getInstance(ExportEnrGroupActivity.this,email);
        setContentView(R.layout.export_enr_list);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/FiraSans-Regular.otf");
        ViewGroup rootView_font = (ViewGroup) findViewById(android.R.id.content).getRootView();

        UserFunctions.changeFont(rootView_font, font);
        groups = Groups.getInstance();
        if (Globals.isLimitedVersion()) {
            initializeAds();
        }
        fromCloud = getIntent().getBooleanExtra("fromCloud",false);
        currentBook = (Book) getIntent().getSerializableExtra("Book");
        gridShelf = (GridShelf) getIntent().getSerializableExtra("Shelf");
        webService = new WebService(ExportEnrGroupActivity.this, Globals.getNewCurriculumWebServiceURL());

        Category = new ArrayList<Category>();
        categoryList = new String[]{getResources().getString(R.string.my_groups), getResources().getString(R.string.my_contacts), getResources().getString(R.string.gmail_contacts)};

        if(currentBook!=null){
            enrichmentDetails = ExportEnrichmentDetails.getInstance();
        }else{
            if(enrichmentDetails!=null &&currentBook==null) {
                enrichmentDetails.enrichmentSelectedList = null;
                enrichmentDetails.enrichmentSelectedLinkList = null;
                enrichmentDetails.mindmapSelectedList = null;
                enrichmentDetails.noteSelectedList = null;
            }
        }
        for (int i = 0; i < categoryList.length; i++) {
            Category category = new Category();
            category.setCategoryId(i);
            category.setCategoryName(categoryList[i]);
            category.setNewCatID(i);
            category.setHidden(false);
            Category.add(category);
        }
        RelativeLayout rl= (RelativeLayout) findViewById(R.id.shelfRootViewLayout);
        TextView txtNooorTitle=(TextView)  findViewById(R.id.txtNooorTitle);
        txtNooorTitle.setText(R.string.groups);
       // rl.setBackgroundColor(Color.parseColor("#DDEEE3"));
        UserFunctions.applyingBackgroundImage(themesPath+"group_shelf.png",rl, ExportEnrGroupActivity.this);
        //rl.setBackgroundResource(R.drawable.group_shelf);
//        RelativeLayout rl_toolbar= (RelativeLayout) findViewById(R.id.relativeLayout1);
//        rl_toolbar.setBackgroundColor(Color.parseColor("#D3E9DD"));
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setBackgroundColor(Color.parseColor("#2DB3DAC2"));
        Button btn_next = (Button) findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);
        CircleButton btn_back= (CircleButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);
        Button btn_share=(Button)findViewById(R.id.btn_share);
       // btn_share.setVisibility(View.VISIBLE);
        btn_share.setOnClickListener(this);
        btn_next.setText(R.string.export_contact);
        btn_next.setVisibility(View.INVISIBLE);
        btn_share.setVisibility(View.INVISIBLE);
        if(currentBook!=null||fromCloud){
            btn_next.setVisibility(View.VISIBLE);
            btn_share.setVisibility(View.VISIBLE);
        }
        app= (ManahijApp)getApplicationContext();
        app.setState(false);
        ManahijApp.getInstance().getPrefManager().addIsinBackground(false);
        if(enrichmentDetails!=null) {
            if (enrichmentDetails.enrichmentSelectedList != null || enrichmentDetails.enrichmentSelectedLinkList != null || enrichmentDetails.mindmapSelectedList != null || enrichmentDetails.noteSelectedList != null) {
                btn_next.setVisibility(View.VISIBLE);
                //    txtNooorTitle.setText(getResources().getString(R.string.app_name));
                btn_share.setVisibility(View.VISIBLE);
            }
        }

        Grp_SelectionAdapter enr = new Grp_SelectionAdapter(ExportEnrGroupActivity.this);



        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.setAdapter(mAdapter);
        selectGroup=null;
        groupList=userContacts.getGroupList();
        myContacts=userContacts.getUserContactList();








      //  groupList=userContacts.getGroupList();
      //  myContacts=userContacts.getUserContactList();

        try {
            if(groupList.size()>0){

            }
            MyContacts updatenoor = null;
            JSONArray emailArr = new JSONArray();
            if(myContacts.size()>0) {
                for (int i = 0; i < myContacts.size(); i++) {
                    updatenoor   = myContacts.get(i);
                    updatenoor.getEmailId();
                    emailArr.put(updatenoor.getEmailId());
                    //Log.e("emailexception", "noor" + updatenoor.getEmailId());

                }
            }
            if(groupList.size()>0) {
                for(MyGroups groups:groupList) {
                    for (int i = 0; i < groups.groupsContactList.size(); i++) {
                        updatenoor = groups.groupsContactList.get(i);
                        updatenoor.getEmailId();
                        emailArr.put(updatenoor.getEmailId());
                        // Log.e("emailexception", "noor" + updatenoor.getEmailId());

                    }
                }
            }
/*
            if(gmailContacts.size()>0){
                for (int i = 0; i < gmailContacts.size(); i++) {
                    MyContacts updatenoor = gmailContacts.get(i);
                    updatenoor.getEmailId();
                    emailArr.put(updatenoor.getEmailId());
                    Log.e("emailexception", "noor" + updatenoor.getEmailId());

                }
            }*/
            if(updatenoor!=null||emailArr.length()>0) {
            if(ManahijApp.getInstance().isInternetAvailable()) {
                NoorUserStatus noorUserStatus = new NoorUserStatus(this, emailArr);
                noorUserStatus.execute();
            }
            }
        }catch (Exception e){
            Log.e("emailexception",e.toString());
        }

       // Log.e("grouplist",""+groupList.size()+gmailContacts.size());
        recyclerView.setAdapter(enr.new GroupShelfAdapter(ExportEnrGroupActivity.this, Category));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                //Movie movie = movieList.get(position);
                //Toast.makeText(getApplicationContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    /**
     * Load Locale language from shared preference and change language in the application
     */
    private void loadLocale() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String language = prefs.getString(Globals.languagePrefsKey, "en");
        scopeId = prefs.getString(Globals.sUserIdKey, "");
        userName = prefs.getString(Globals.sUserNameKey, "");
        scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
        email =prefs.getString(Globals.sUserEmailIdKey, "");
         //email="krishna@semasoft.co.in";
      //  changeLang(language);
    }

    @Override
    public void onHandleSelection(MyContacts res) {
       // Log.e("res",res.toString()+"email"+res.getEmailId());
        Intent secondActivity = new Intent();
        secondActivity.putExtra("response",  res);
        setResult(Activity.RESULT_OK,secondActivity);
        finish();

    }




    public class MyBroadcastReceiver extends BroadcastReceiver {
        public static final String ACTION = "com.example.ACTION_STATUS";
        @Override
        public void onReceive(Context context, Intent intent) {
            statusList = intent.getParcelableArrayListExtra("onlinlist");
           // Log.e("sizelist",""+statusList.size());

            try {
                if(statusList.size()>=0) {
                    if (myContacts.size() >=0) {
                        for (MyContacts t : myContacts) {
                            for (OnlineStatus t2 : statusList) {
                                if (t.getEmailId().equals(t2.getEmail())) {
                                    t.setOnline(t2.getOnlineStatus());

                                }
                            }
                        }

                    }
                }

                if(statusList.size()>=0) {
                    if (groupList.size() >=0) {
                        for (MyGroups groups : groupList) {

                            for (MyContacts t : groups.groupsContactList) {
                                for (OnlineStatus t2 : statusList) {
                                    if (t.getEmailId().equals(t2.getEmail())) {
                                        //if(Integer.parseInt(t2.getOnlineStatus())==0){
                                        t.setOnline(t2.getOnlineStatus());
                                        //}
                                        //else{ t.setOnline(true);}
                                    }
                                }
                            }

                        }
                    }
                }


                if(statusList.size()>=0) {
                    if (gmailContacts.size() >=0) {


                            for (MyContacts t : gmailContacts) {
                                for (OnlineStatus t2 : statusList) {
                                    if (t.getEmailId().equals(t2.getEmail())) {
                                        t.setOnline(t2.getOnlineStatus());

                                    }
                                }
                            }

                    }
                }
                recyclerView.getAdapter().notifyDataSetChanged();
            }catch (Exception e){
                Log.e("exr",e.toString());
            }

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        this.registerReceiver(receiver, new IntentFilter(MyBroadcastReceiver.ACTION));
        try {
          /*  if (!app.getState()*//* ManahijApp.getInstance().getPrefManager().getIsinBackground()*//*) {
                if (ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
                    startService(new Intent(ExportEnrGroupActivity.this, CallConnectionStatusService.class));
                }
            }*/
            app.setState(false);
            ManahijApp.getInstance().getPrefManager().addIsinBackground(false);
        }catch (Exception e){
            Log.e("exrec",e.toString());
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
      /*  if(!app.getState()  *//*ManahijApp.getInstance().getPrefManager().getIsinBackground()*//*){
            stopService(new Intent(ExportEnrGroupActivity.this, CallConnectionStatusService.class));
            MQTTSubscriptionService s = new MQTTSubscriptionService();
            s.disConnectCall();

        }*/
        this.unregisterReceiver(receiver);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next: {
                if(selectedContact.size()>0  ) {
                    if (fromCloud){
                        ArrayList<String> emailList = new ArrayList<>();
                        for (int i=0;i<selectedContact.size();i++){
                            MyContacts contacts = selectedContact.get(i);
                            emailList.add(contacts.getEmailId());
                        }
                        setResult(RESULT_OK, getIntent().putExtra("contactList", emailList));
                        finish();
                    }else {
                        if (currentBook.is_bStoreBook()) {
                            exportEnrichDialog(exportEnrichmentstoGroups());
                        } else {

                           normalBookSharing();
                        }
                    }
                }else{
                    UserFunctions.alert(ExportEnrGroupActivity.this.getResources().getString(R.string.select_contact),ExportEnrGroupActivity.this);

                  }
                break;
            }
            case R.id.btn_back: {
                onBackPressed();
                break;
            }
            case R.id.btn_share:{
                if(currentBook.is_bStoreBook()) {
                    Share = true;
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
                    String formattedDate = df.format(c.getTime());
                    String android_id = Settings.Secure.getString(ExportEnrGroupActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    String uniqueId;
                    if (android_id.length() > 8) {
                        uniqueId = android_id.substring(android_id.length() - 8);
                    } else {
                        uniqueId = android_id;
                    }
                    String id = formattedDate + "-" + uniqueId;
                    String Url = "http://www.nooor.com/share.html?id=" + id;
                    exportEnrichDialog(id);
                }else{
                    normalBookSharing();
                }
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(grpSelected){
            grpSelected=false;
            selectGroup=null;
            Grp_SelectionAdapter enr = new Grp_SelectionAdapter(ExportEnrGroupActivity.this);
            recyclerView.setAdapter(enr.new GroupShelfAdapter(ExportEnrGroupActivity.this, Category));
            //recyclerView.getAdapter().notifyDataSetChanged();
        }else{
            finish();
        }

    }

    @Override
    public void finish() {
        super.finish();
        if (UserFunctions.isInternetExist(ExportEnrGroupActivity.this)) {
            new savingUserGroupDetailstoServer().execute();
        }
    }
    private void normalBookSharing(){
        final Dialog exportEnrPopover = new Dialog(ExportEnrGroupActivity.this);
        exportEnrPopover.getWindow().setBackgroundDrawable(new ColorDrawable(ExportEnrGroupActivity.this.getResources().getColor(R.color.semi_transparent)));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        exportEnrPopover.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exportEnrPopover.setContentView(ExportEnrGroupActivity.this.getLayoutInflater().inflate(R.layout.export_normal_bookdialog, null));
        exportEnrPopover.getWindow().setLayout((int) getResources().getDimension(R.dimen.export_newbook_width), (int) getResources().getDimension(R.dimen.export_newbook_height));
        Button btn_export = (Button) exportEnrPopover.findViewById(R.id.btn_export);
        CircleButton btn_back=(CircleButton) exportEnrPopover.findViewById(R.id.btn_back);
        final EditText et_title = (EditText) exportEnrPopover.findViewById(R.id.et_name);
        mailid = exportEnrichmentstoGroups();
        final EditText et_email = (EditText) exportEnrPopover.findViewById(R.id.et_email);

        et_email.setText(mailid);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportEnrPopover.dismiss();
            }
        });

        btn_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // et_title
                String toEmailId = et_email.getText().toString();
                String title = et_title.getText().toString();
                if (!toEmailId.equals("") && !title.equals("")) {
                    BookExportDialog bookdialog = new BookExportDialog(ExportEnrGroupActivity.this, currentBook);
                    bookdialog.new ExportBook(mailid, title, isEditable).execute();
                    exportEnrPopover.dismiss();
                }
                // String str = ExportEnrGroupActivity.this.getResources().getString(R.string.please_wait);
                // progresdialog = ProgressDialog.show(ExportEnrGroupActivity.this, "", str, true);

            }
        });
        Switch btnswitch = (Switch) exportEnrPopover.findViewById(R.id.switch4);
        btnswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isEditable = "yes";
                } else {
                    isEditable = "no";
                }
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportEnrPopover.dismiss();
            }
        });
        exportEnrPopover.show();
    }
    public class validateLoginAndPublishEnrichments extends AsyncTask<Void, Void, Void> {
        boolean link,enrichment,mindmap,noteTab;
        ProgressDialog progress;
        public validateLoginAndPublishEnrichments(boolean enrichmentLinkSelected, boolean enrichmentTabSelected, boolean mindmapTabSelected, boolean noteTabSelected, ProgressDialog progresdialog) {
         this.link=enrichmentLinkSelected;
            this.enrichment=enrichmentTabSelected;
            this.mindmap=mindmapTabSelected;
            this.noteTab=noteTabSelected;
            this.progress=progresdialog;

        }

        @Override
        protected void onPreExecute() {

            if (link) {
                progress.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.upload_please_wait));
                genratingXmlforLinksSelected();
                desc = "Hello This is My Link";
                filepath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp/resources.xml";
            } else if (noteTab) {
                progress.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.upload_please_wait));
                generatingXmlfornoteSelected();
                desc = "Hello This is My Note";
                filepath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp/resources.xml";
            } else if (mindmap) {
                if (Globals.ISGDRIVEMODE()) {
                    progress.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.upload_gdrive_please_wait));
                } else {
                    progress.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.upload_please_wait));
                }
                genratingXmlforMindmapSelected();
                desc = "Hello This is My Mindmap";
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
                String formattedDate = df.format(c.getTime());
                Zip zip = new Zip();
                String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/"+currentBook.getBookID()+".zip";
                String htmlFileDirPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/";
                zip.zipFilesForEnrichments(new File(htmlFileDirPath), destPath);

                filepath=destPath;
            } else {
                if (Globals.ISGDRIVEMODE()) {
                    progress.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.upload_gdrive_please_wait));
                } else {
                    progress.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.upload_please_wait));
                }
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
                String formattedDate = df.format(c.getTime());
                filepath = Globals.TARGET_BASE_BOOKS_DIR_PATH +currentBook.getBookID() + "/temp/" + formattedDate + ".zip";
                initUpload(filepath);
            }

            enrichZipFilePath = new File(filepath);
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (Globals.ISGDRIVEMODE()) {
                if (enrichment||mindmap) {
                    String zipFileName = enrichZipFilePath.getName();
                    String type;
                    if (enrichment){
                        type = "Enrichment";
                    }else{
                        type = "Resource";
                    }
                    gDriveExport = new GDriveExport(ExportEnrGroupActivity.this, enrichZipFilePath.getPath(), zipFileName, mailid, enrichXmlString, dicEnrichments, resourceTitle, progress,type);
                    if (Build.VERSION.SDK_INT >= 23) {
                        requestAccountsPermissionAndAuthorize();
                    } else {
                        gDriveExport.Authorize();
                    }
                } else {
                    gDriveExport = new GDriveExport(ExportEnrGroupActivity.this, mailid, enrichXmlString, resourceTitle, progress);
                    ExportEnrGroupActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gDriveExport.new createJsonValueForResource().execute();
                        }
                    });
                }
            } else {
                if (link|| noteTab|| enrichment|| mindmap) {
                    PublishEnrichmentsAsChunks(progress,link,enrichment,mindmap,noteTab);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

        public void zippingMindmapPageForExport(int enrichId, String mindMapPath, int pageNumber, String enrichmentTitle){
            File file=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp");
            String htmlFileDirPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/";
            UserFunctions.createNewDirectory(htmlFileDirPath);
            String htmlFilePath = htmlFileDirPath+enrichmentTitle;
            UserFunctions.copyFiles(mindMapPath+".xml",htmlFilePath+"_"+currentBook.getBookID()+".xml");
            UserFunctions.copyFiles(mindMapPath+".png",htmlFilePath+".png");

        }

        /**
         * Generate all the selected enrichment html pages and and make it as zip and create an xml to prepare for publish or update.
         */
        private void initUpload(String destPath) {

            //String storBookId = bookView.currentBook.get_bStoreID().replace("M", "");
            String[] bookName =currentBook.get_bStoreID().split("M");
            String storBookId =currentBook.get_bStoreID();
            enrichXmlString = "<Enrichments>";
            if (Globals.ISGDRIVEMODE()) {
                dicEnrichments = new HashMap();
            }
            int i = 0;
            for (Enrichments enrichments : enrichmentDetails.enrichmentSelectedList) {
                if (enrichments.isEnrichmentSelected()) {
                    new GenerateHTML(ExportEnrGroupActivity.this, db, currentBook, String.valueOf(enrichments.getEnrichmentPageNo()), null, false).generateEnrichedPageForExportAndZipIt(enrichments);
                    String fileName = currentBook.getBookID() + "-" + enrichments.getEnrichmentPageNo() + "-" + enrichments.getEnrichmentId() + ".zip";
                    //String fileName = enrichments.getEnrichmentPageNo()+"-"+enrichments.getEnrichmentId()+".zip";

                    int idPage = enrichments.getEnrichmentPageNo();
                    String bookname = currentBook.get_bStoreID().replace("M", "");
                    //bookname=bookname.replace("M", "");
                    // int categoryId=0;
                    enrichXmlString = enrichXmlString.concat("<Enriched EnrichmentID='" + enrichments.getEnrichmentId() + "' IDUser='" + scopeId + "' IDBook='" + storBookId + "' IDPage='" + idPage + "' Title='" + enrichments.getEnrichmentTitle() + "' Password='' isPublished='3' PublishToEnrichementsSite='true' IsEditable='0' file='" + fileName + "' bookTitle='" +currentBook.getBookTitle()+"' categoryId='0'/>");
                    if (Globals.ISGDRIVEMODE()) {
                        HashMap<String, String> dicEnr = new HashMap<String, String>();
                        String storeID = "001";
                        String currentDateTime = UserFunctions.getCurrentDateTimeInString();
                        String enrIDGlobal = storeID + "-" + currentBook.getBookID() + "-" + idPage + "-" + enrichments.getEnrichmentId() + "-" + scopeId;
                        dicEnr.put("file1", fileName);
                        dicEnr.put("title", enrichments.getEnrichmentTitle());
                        dicEnr.put("pageNo", String.valueOf(idPage));
                        dicEnr.put("dateTime", currentDateTime);
                        dicEnr.put("enrichmentIDGlobal", enrIDGlobal);
                        dicEnr.put("categoryId","0");
                        String enrKey = "enrichment" + i;
                        dicEnrichments.put(enrKey, dicEnr);
                    }
                    i++;
                }
            }
            enrichXmlString = enrichXmlString.concat("</Enrichments>");

            Zip zip = new Zip();
            String sourcePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp/";
            //String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/zipFile222.zip";
            zip.zipFilesForEnrichments(new File(sourcePath), destPath);


        }
        public void requestAccountsPermissionAndAuthorize(){
            if (ContextCompat.checkSelfPermission(ExportEnrGroupActivity.this, android.Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ExportEnrGroupActivity.this, new String[]{android.Manifest.permission.GET_ACCOUNTS},MY_PERMISSIONS_REQUEST_GET_ACCOUNTS);
            } else if (ContextCompat.checkSelfPermission(ExportEnrGroupActivity.this, android.Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED){
                gDriveExport.Authorize();
            }
        }

        /**
         * generating xml for Links
         *
         * @return0
         */
        private void generatingXmlfornoteSelected() {
            int idPage;
            enrichXmlString = "<Notes>";
            for (Note notes : enrichmentDetails.noteSelectedList) {
                if (notes.isNoteSelected()) {
                    enrichXmlString = enrichXmlString.concat("<note>");
                    idPage = notes.getPageNo();
                    enrichXmlString = enrichXmlString.concat("\n" + currentBook.get_bStoreID() + "|" + idPage + "|" + notes.getsText() + "|" + notes.getOccurence() + "|" + notes.getsDesc() + "|" + notes.getnPos() + "|" + notes.getColor() + "|" + notes.getProcessStext() + "|" + notes.getTabNo() + "\n");
                    enrichXmlString = enrichXmlString.concat("</note>");
                }
            }
            enrichXmlString = enrichXmlString.concat("</Notes>");
        }

        /**
         * generating xml for Notes
         *
         * @return
         */
        private void genratingXmlforLinksSelected() {
            int idPage;
            enrichXmlString = "<Links>";
            for (Enrichments enrichments : enrichmentDetails.enrichmentSelectedLinkList) {
                if (enrichments.isEnrichmentSelected()) {
                    enrichXmlString = enrichXmlString.concat("<link>");
                    idPage = enrichments.getEnrichmentPageNo();
                    enrichXmlString = enrichXmlString.concat("\n" + enrichments.getEnrichmentId() + "|" + enrichments.getEnrichmentBid() + "|" + idPage + "|" + enrichments.getEnrichmentTitle() + "|" + enrichments.getEnrichmentType() + "|" + enrichments.getEnrichmentPath() + "|" + enrichments.getEnrichmentExportValue() + "|" +0+"\n");
                    enrichXmlString = enrichXmlString.concat("</link>");
                }
            }
            enrichXmlString = enrichXmlString.concat("</Links>");
        }

        /**
         * generating xml for Mindmap
         *
         * @return
         */
        private void genratingXmlforMindmapSelected() {
            int idPage;
            enrichXmlString = "<MindMaps>";
            if (Globals.ISGDRIVEMODE()) {
                dicEnrichments = new HashMap();
            }
            int i = 0;
            for (Enrichments enrichments : enrichmentDetails.mindmapSelectedList) {
                if (enrichments.isEnrichmentSelected()) {
                    //enrichXmlString = enrichXmlString.concat("<mindmap>");
                    String fileName = currentBook.getBookID() + "-" + enrichments.getEnrichmentPageNo() + "-" + enrichments.getEnrichmentId() + ".zip";
                    idPage = enrichments.getEnrichmentPageNo();
                    String enrPath = enrichments.getEnrichmentPath();
                    String mindMapPath = Globals.TARGET_BASE_MINDMAP_PATH + enrPath;
                    zippingMindmapPageForExport(enrichments.getEnrichmentId(),mindMapPath,enrichments.getEnrichmentPageNo(),enrichments.getEnrichmentTitle());
                    enrichXmlString=enrichXmlString.concat("<mindmap ID=''IDUser='' IDBook='" +currentBook.get_bStoreID()+ "' IDPage='" +enrichments.getEnrichmentPageNo()+"' Title='" +enrichments.getEnrichmentTitle()+"'Password='' isPublished='3' PublishToEnrichementsSite='true' IsEditable='0' file='" +enrichments.getEnrichmentTitle()+"' bookTitle='" +currentBook.getBookTitle()+"' categoryId='0'>");

                    enrichXmlString = enrichXmlString.concat("</mindmap>");
                    i++;
                }
            }
            enrichXmlString = enrichXmlString.concat("</MindMaps>");

        }


        /**
         * Initialize all values to publish book as chunks
         * @param progress
         * @param link
         * @param enrichment
         * @param mindmap
         * @param noteTab
         */
        private void PublishEnrichmentsAsChunks(ProgressDialog progress, boolean link, boolean enrichment, boolean mindmap, boolean noteTab) {
            try {
                chunkInputStream = new FileInputStream(filepath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            chunkBaos = new ByteArrayOutputStream();
            chunkBytes = new byte[1024 * 1024];
            chunkBytesRead = 0;
            chunkNoOfExecution = 1;
            new publishEnrichmentsAsChunks(progress,link,enrichment,mindmap,noteTab).execute();
        }

    }

    /**
     * Async task class to publish book as chunks
     *
     * @author
     */
    public class publishEnrichmentsAsChunks extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressdialog;
        String bookZipBase64Chunks = null;
        byte[] byteArray = null;
        String base64Binary;
        boolean enrichmentLinkSelected,enrichmentTabSelected,mindmapTabSelected,noteTabSelected;

        public publishEnrichmentsAsChunks(ProgressDialog progress, boolean link, boolean enrichment, boolean mindmap, boolean noteTab) {
         this.progressdialog=progress;
            this.enrichmentTabSelected=enrichment;
            this.enrichmentLinkSelected=link;
            this.mindmapTabSelected=mindmap;
            this.noteTabSelected=noteTab;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (enrichmentLinkSelected || noteTabSelected || mindmapTabSelected) {
                base64Binary = Base64.encodeToString(enrichXmlString.getBytes(), Base64.DEFAULT);
            } else {
                convertFileToBase64BinaryChunks(enrichZipFilePath);
            }
        }


        @Override
        protected Void doInBackground(Void... params) {
            //System.out.println(chunkBytesRead+"");
            //String storeBookId=bookView.currentBook.get_bStoreID().replace("M", "");
            String[] bookId = currentBook.get_bStoreID().split("M");
            String storeBookId = bookId[1];
            if (enrichmentLinkSelected || noteTabSelected || mindmapTabSelected) {
                webService.publishResouce(0,scopeId, storeBookId, 0, resourceTitle, desc, 1, 5, base64Binary, mailid, isprivate);
            } else if (enrichmentTabSelected) {
                if (chunkBytesRead > 0) {
                    // String encode=URLEncodedUtils.isEncoded(entity)
                    //bookView.webService.Xmlparser.parseXml(enrichXmlString);
                   webService.newUploadBookAsChunks(scopeId, enrichZipFilePath.getName(), bookZipBase64Chunks, chunkBookFlag);
                } else {
                    webService.PublishNewEnrichedGroup(scopeId, enrichZipFilePath.getName(), enrichXmlString, isprivate, mailid);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (webService.ResourceId != 0 && enrichmentLinkSelected) {
                for (Enrichments enrich : enrichmentDetails.enrichmentSelectedLinkList) {
                    enrich.setEnrichmentExportValue(webService.ResourceId);
                    enrich.setEnrichmentSelected(false);
                    db.executeQuery("update enrichments set Exported='" + webService.ResourceId + "' where enrichmentID='" + enrich.getEnrichmentId() + "'");
                }
                if(enrichmentDetails.getMindmapSelectedList().size()>0){
                    //progresdialog.setMessage();
                    progressdialog.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.upload_please_wait));
                }else {
                    UserFunctions.DisplayAlertDialog(ExportEnrGroupActivity.this, R.string.publish_completed_successfully, R.string.published);
                    File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                    UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                    progressdialog.dismiss();
                }
            } else if (webService.ResourceId != 0 && mindmapTabSelected) {
                for (Enrichments enrich : enrichmentDetails.mindmapSelectedList) {
                    enrich.setEnrichmentExportValue(webService.ResourceId);
                    enrich.setEnrichmentSelected(false);
                    db.executeQuery("update enrichments set Exported='" + webService.ResourceId + "' where enrichmentID='" + enrich.getEnrichmentId() + "'");
                }
                if(enrichmentDetails.getNoteSelectedList().size()>0){
                    //progresdialog.setMessage();
                    progressdialog.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.upload_please_wait));
                }else {
                    UserFunctions.DisplayAlertDialog(ExportEnrGroupActivity.this, R.string.publish_completed_successfully, R.string.published);
                    File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                    UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                    progressdialog.dismiss();
                }

            } else if (webService.ResourceId != 0 && noteTabSelected) {
                for (Note notes : enrichmentDetails.noteSelectedList) {
                    notes.setExportedId(webService.ResourceId);
                    notes.setNoteSelected(false);
                    db.executeQuery("update tblNote set Exported='" + webService.ResourceId + "' where TID='" + notes.getId() + "'");

                }
                if(enrichmentDetails.getEnrichmentSelectedList().size()>0){
                    progressdialog.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.upload_please_wait));
                }else {
                    UserFunctions.DisplayAlertDialog(ExportEnrGroupActivity.this, R.string.publish_completed_successfully, R.string.published);
                    File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                    UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                    progressdialog.dismiss();
                }

            } else if (enrichmentTabSelected) {
                if (chunkBytesRead > 0) {
                    if (webService.uploadJunkFileIpadResult != "") {
                        if (webService.uploadJunkFileIpadResult.equals("true")) {
                            chunkNoOfExecution++;
                            new publishEnrichmentsAsChunks(progressdialog, enrichmentLinkSelected, enrichmentTabSelected, mindmapTabSelected, noteTabSelected).execute();
                        } else {

                            progressdialog.dismiss();
                            String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp";

                            File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH +currentBook.getBookID() + "/temp");
                            UserFunctions.DeleteDirectory(enrichedPagesFilePath);

                            UserFunctions.DisplayAlertDialog(ExportEnrGroupActivity.this, R.string.went_wrong, R.string.oops);
                        }
                    }
                } else {
                    if (webService.enrichedPageResponseServerIdList.size() > 0) {
                        for (int i = 0; i < webService.enrichedPageResponseServerIdList.size(); i++) {
                            Enrichments enrichments = enrichmentDetails.enrichmentSelectedList.get(i);
                            int enrichPageServerId = webService.enrichedPageResponseServerIdList.get(i);
                            enrichments.setEnrichmentExportValue(enrichPageServerId);
                            enrichments.setEnrichmentSelected(false);
                            db.executeQuery("update enrichments set Exported='" + enrichPageServerId + "' where enrichmentID='" + enrichments.getEnrichmentId() + "'");
                        }
                        //enrichmentSelectedList.clear();
                        progressdialog.dismiss();
                        File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH +currentBook.getBookID() + "/temp");
                        UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                        UserFunctions.DisplayAlertDialog(ExportEnrGroupActivity.this, R.string.publish_completed_successfully, R.string.published);

                    } else {
                        progressdialog.dismiss();
                        File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/temp");
                        UserFunctions.DeleteDirectory(enrichedPagesFilePath);
                        UserFunctions.DisplayAlertDialog(ExportEnrGroupActivity.this, R.string.went_wrong, R.string.oops);

                    }
                }
            } else {
                if(enrichmentLinkSelected){
                    progressdialog.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.failed_linkUpload));
                }else if(mindmapTabSelected){
                    progressdialog.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.failed_mindmapUpload));
                }else if(noteTabSelected){
                    progressdialog.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.failed_noteUpload));
                }else {
                    progressdialog.setMessage(ExportEnrGroupActivity.this.getResources().getString(R.string.failed_EnrichUpload));
                    progressdialog.dismiss();
                    UserFunctions.DisplayAlertDialog(ExportEnrGroupActivity.this, R.string.went_wrong, R.string.oops);
                }
            }
        }


        /**
         * convert file to base64 binary Chunks
         *
         * @param file
         * @return
         */
        private void convertFileToBase64BinaryChunks(File file) {
            try {
                if ((chunkBytesRead = chunkInputStream.read(chunkBytes)) != -1) {
                    ////System.out.println("bytesRead: "+chunkBytesRead+"i: "+i);
                    chunkBaos.write(chunkBytes, 0, chunkBytesRead);
                    byteArray = chunkBaos.toByteArray();
                    bookZipBase64Chunks = Base64.encodeToString(byteArray, Base64.DEFAULT).trim();
                    if (chunkNoOfExecution == 1) {
                        chunkBookFlag = "Start";
                    } else if (chunkBytesRead < 1024 * 1024) {
                        chunkBookFlag = "End";
                    } else {
                        chunkBookFlag = "Middle";
                    }
                    chunkBaos.reset();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper != null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        if(requestCode == AUTH_CODE_REQUEST_CODE){
            if (gDriveExport != null) {
                gDriveExport.Authorize();
            }
        }else if (requestCode == 64206) {
            Session session = Session.getActiveSession();
            session.onActivityResult(this, requestCode, resultCode, data);
            if (session != null && session.isOpened()) {
                Request.newMeRequest(session, new Request.GraphUserCallback() {

                    @Override
                    public void onCompleted(GraphUser _user, Response response) {
                        if (_user != null) {
                            //String Url="http://192.168.0.52/SemaLibrary/share.html?id="+mailid;       // just for testing
                            String url=Globals.enrShareUrl+mailid;
                            publishFeedDialog(_user.getName(), url);
                        }
                    }
                }).executeAsync();
            }
		 }

    }

    public String exportEnrichmentstoGroups(){
        String mailid = null;
        boolean first=false;
        for(int i=0;i<selectedContact.size();i++){
            MyContacts getemail=selectedContact.get(i);
            if(i==0){
                if(i==selectedContact.size()-1){
                    mailid=getemail.getEmailId();
                }else {
                    mailid = getemail.getEmailId() + ",";
                }
            }else{
                if(i==selectedContact.size()-1){
                    mailid=mailid.concat(getemail.getEmailId());
                }else {
                    mailid = mailid.concat(getemail.getEmailId() + ",");
                }
            }

        }
        return mailid;
    }
    private void initializeAds(){
        SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
        boolean adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);
        final InterstitialAd interstitial;
        if (!adsPurchased) {
//            interstitial = new InterstitialAd(ExportEnrGroupActivity.this);
//            interstitial.setAdUnitId(Globals.INTERSTITIAL_AD_UNIT_ID);
            adView = (AdView) findViewById(R.id.adView);
            adView.bringToFront();
            AdRequest adRequest = new AdRequest.Builder()
                    //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    //.addTestDevice("505F22CBB28695386696655EED14B98B")
                    .build();
            adView.loadAd(adRequest);
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (!groups.getBannerAdsVisible()) {
                        adView.setVisibility(View.VISIBLE);
                    }
                }
            });

        }
    }
    private void exportEnrichDialog(final String emailId){
        final Dialog exportEnrPopover = new Dialog(ExportEnrGroupActivity.this);
        exportEnrPopover.getWindow().setBackgroundDrawable(new ColorDrawable(ExportEnrGroupActivity.this.getResources().getColor(R.color.semi_transparent)));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        exportEnrPopover.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exportEnrPopover.setContentView(ExportEnrGroupActivity.this.getLayoutInflater().inflate(R.layout.enr_export_dialog, null));
        exportEnrPopover.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.3), (int) (Globals.getDeviceWidth() / 1.3));
        Button btn_export = (Button) exportEnrPopover.findViewById(R.id.btn_export);
        final EditText et_title = (EditText) exportEnrPopover.findViewById(R.id.et_name);
        CircleButton  btn_back =(CircleButton)exportEnrPopover.findViewById(R.id.btn_back);
        final EditText tv_description = (EditText) exportEnrPopover.findViewById(R.id.et_description);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exportEnrPopover.dismiss();
            }
        });
        btn_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resourceTitle = et_title.getText().toString();
                if (!tv_description.getText().toString().equals("") && !resourceTitle.equals("")){
                 String str = ExportEnrGroupActivity.this.getResources().getString(R.string.please_wait);
                 ProgressDialog  progresdialog = ProgressDialog.show(ExportEnrGroupActivity.this, "", str, true);
                mailid = emailId;

                if (enrichmentDetails.getEnrichmentSelectedLinkList().size() > 0) {
                    enrichmentLinkSelected = true;
                    enrichmentTabSelected = false;
                    mindmapTabSelected = false;
                    noteTabSelected = false;
                    new validateLoginAndPublishEnrichments(enrichmentLinkSelected,enrichmentTabSelected,mindmapTabSelected,noteTabSelected,progresdialog).execute();
                }
                if (enrichmentDetails.getMindmapSelectedList().size() > 0) {
                    enrichmentLinkSelected = false;
                    enrichmentTabSelected = false;
                    mindmapTabSelected = true;
                    noteTabSelected = false;
                    new validateLoginAndPublishEnrichments(enrichmentLinkSelected,enrichmentTabSelected,mindmapTabSelected,noteTabSelected,progresdialog).execute();
                }
                if (enrichmentDetails.getNoteSelectedList().size() > 0) {
                    enrichmentLinkSelected = false;
                    enrichmentTabSelected = false;
                    mindmapTabSelected = false;
                    noteTabSelected = true;
                    new validateLoginAndPublishEnrichments(enrichmentLinkSelected,enrichmentTabSelected,mindmapTabSelected,noteTabSelected,progresdialog).execute();
                }
                if (enrichmentDetails.getEnrichmentSelectedList().size() > 0) {
                    enrichmentLinkSelected = false;
                    enrichmentTabSelected = true;
                    mindmapTabSelected = false;
                    noteTabSelected = false;
                    new validateLoginAndPublishEnrichments(enrichmentLinkSelected,enrichmentTabSelected,mindmapTabSelected,noteTabSelected,progresdialog).execute();
                }
                    exportEnrPopover.dismiss();
            }
            }
        });

        exportEnrPopover.show();
    }

    public String getCurrentDateTimeInString(){
        String rtnValue = "";
        Calendar Cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        rtnValue = df.format(Cal.getTime());
        //Toast.makeText(this, rtnValue, Toast.LENGTH_SHORT).show();
        return rtnValue;
    }

    public void share(final String webSelectedText){
        //selectedText = webSelectedText;
        sharePopUp = new Dialog(this);
        sharePopUp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
        sharePopUp.setTitle("Share via");

        sharePopUp.setContentView(getLayoutInflater().inflate(R.layout.share, null));
        ListView advListView = (ListView) sharePopUp.findViewById(R.id.listViewShare);
        advListView.setDividerHeight(2);
        advListView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1f));
        advListView.setAdapter(new shareAdapter(ExportEnrGroupActivity.this, webSelectedText));
        Share=false;
        advListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                       // if (webView.allowPublish(200)) {
                            //mHandler.post(mUpdateResults);
                      //  }
                        break;

                    case 1:
                        //if (webView.allowPublish(117)) {
//                            Intent i=new Intent(BookViewReadActivity.this,Tw.class);
//                            startActivity(i);

                            //new twitterLogin().execute();
                            TwitterDialog twitterDialog = new TwitterDialog(ExportEnrGroupActivity.this);
                            twitterDialog.loginToTwitter(webSelectedText);

                       // }

                        break;

                    case 2:
                      //  if (webView.allowPublish(200)) {
                            Intent i = new Intent(android.content.Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"recipient@example.com"});
                            i.putExtra(android.content.Intent.EXTRA_CC, new String[]{""});
                            i.putExtra(android.content.Intent.EXTRA_BCC, new String[]{""});
                            i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Note From Manahij");
                            i.putExtra(android.content.Intent.EXTRA_TEXT, webSelectedText);

                            try {
                                startActivity(Intent.createChooser(i, "Send mail...."));
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(ExportEnrGroupActivity.this, "There are no email clients installed", Toast.LENGTH_SHORT).show();
                            }
                      //  }
                        break;

                    default:
                        break;
                }
            }



        });
        sharePopUp.show();
    }
    private class shareAdapter extends BaseAdapter {

        private boolean isFbSessionActive;
        private GraphUser user;
        Session session;
        private String selectedText;

        public Integer[] mThumbIds = {
                R.drawable.facebook,R.drawable.twitter,R.drawable.emailiphone
        };

        public shareAdapter(ExportEnrGroupActivity bookView, String selectedText) {
            this.selectedText = selectedText;
            session = Session.getActiveSession();
            if (session != null && session.isOpened()) {
                isFbSessionActive = true;
                Request.newMeRequest(session, new Request.GraphUserCallback() {

                    @Override
                    public void onCompleted(GraphUser _user, Response response) {
                        user = _user;
                    }
                }).executeAsync();
            } else {
                isFbSessionActive = false;
            }
        }

        @Override
        public int getCount() {
            return share.length;
        }

        @Override
        public Object getItem(int position) {

            return null;
        }

        @Override
        public long getItemId(int position) {

            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View vi = convertView;
            if(convertView==null){
                vi = getLayoutInflater().inflate(R.layout.advpopgrid, null);
            }
            TextView txtSearchItem = (TextView) vi.findViewById(R.id.textViewAdv);
            txtSearchItem.setTextColor(Color.BLACK);
            ImageView imgSearchImg = (ImageView) vi.findViewById(R.id.imageViewAdv);
            LinearLayout fbLayout = (LinearLayout) vi.findViewById(R.id.fb_layout);
            final LoginButton authButton = (LoginButton) vi.findViewById(R.id.authButton);
            authButton.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);

            authButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    sharePopUp.dismiss();
                }
            });
            Button btnShare = (Button) vi.findViewById(R.id.button1);
            btnShare.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (user != null) {
                        publishFeedDialog(user.getName(), selectedText);
                    }
                }
            });

            if (position == 0) {
                if (isFbSessionActive) {
                    btnShare.setVisibility(View.VISIBLE);
                } else {
                    btnShare.setVisibility(View.GONE);
                }
                fbLayout.setVisibility(View.VISIBLE);
                txtSearchItem.setVisibility(View.GONE);
                imgSearchImg.setVisibility(View.GONE);
            } else {
                fbLayout.setVisibility(View.GONE);
                txtSearchItem.setVisibility(View.VISIBLE);
                imgSearchImg.setVisibility(View.VISIBLE);
            }

            txtSearchItem.setText(share[position]);
            imgSearchImg.setImageResource(mThumbIds[position]);
            return vi;
        }

    }
    /*
     *  Sharing link to facebook
     */
    private void publishFeedDialog(String userName, String link) {
        Bundle parameters = new Bundle();
        parameters.putString("link", link);
       // parameters.putString("link",  "");
        parameters.putString("picture", "http://dev.sboook.com/manahijLogo.png");
        parameters.putString("name", userName + " has Shared");
        parameters.putString("caption", getResources().getString(R.string.app_name));
        parameters.putString("description", link);
        parameters.putString("message", "Share your message!");

        // Invoke the dialog
        WebDialog feedDialog = (
                new WebDialog.FeedDialogBuilder(this,
                        Session.getActiveSession(),
                        parameters))
                .setOnCompleteListener(new WebDialog.OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values,
                                           FacebookException error) {
                        if (error == null) {
                            // When the story is posted, echo the success
                            // and the post Id.
                            final String postId = values.getString("post_id");
                            if (postId != null) {
                                Toast.makeText(ExportEnrGroupActivity.this,
                                        "Posted successfully",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                // User clicked the Cancel button
                                Toast.makeText(ExportEnrGroupActivity.this.getApplicationContext(),
                                        "Publish cancelled",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else if (error instanceof FacebookOperationCanceledException) {
                            // User clicked the "x" button
                            Toast.makeText(ExportEnrGroupActivity.this.getApplicationContext(),
                                    "Publish cancelled",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Generic, ex: network error
                            Toast.makeText(ExportEnrGroupActivity.this.getApplicationContext(),
                                    "Error Publish text",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .build();
        feedDialog.show();
    }
    public class savingUserGroupDetailstoServer extends AsyncTask<Void, Void, Void> {
        JSONObject jsoncontactsList = new JSONObject();
        JSONObject jsonGroupList = new JSONObject();

        @Override
        protected void onPreExecute() {
            //UpdatingUsergroupsList();
            JSONArray groupsJsonData = new JSONArray();
            JSONArray jsongrpContacts = null;
            for (int i = 0; i < groupList.size(); i++) {
                MyGroups grp = groupList.get(i);
                JSONObject json = new JSONObject();
                try {
                    json.put("id", grp.getId());
                    json.put("name", grp.getGroupName());
                    json.put("userId", grp.getLoggedInUserId());
                    json.put("enabled", grp.getEnabled());
                    if (grp.getGroupsContactList().size() > 0) {
                        jsongrpContacts = getgrpContacts(grp.getGroupsContactList());
                    }
                    json.put("contacts", jsongrpContacts);
                    groupsJsonData.put(json);
                    //json.put("userId",grp.ge)
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            JSONArray Contacts = getgrpContacts(myContacts);
            try {
                jsoncontactsList.put("contact",Contacts);
                jsonGroupList.put("group",groupsJsonData);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        @Override
        protected Void doInBackground(Void... params) {
            JSONObject finalJsonData = getfinalJsonData(jsonGroupList,jsoncontactsList);
            webService.saveUserGroupDetails(String.valueOf(scopeId),finalJsonData.toString());;
            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

    }

    private JSONArray getgrpContacts(ArrayList<MyContacts> groupsContactList){
        JSONArray jsonContacts=new JSONArray();
        for(int i=0;i<groupsContactList.size();i++){
            MyContacts contacts=groupsContactList.get(i);
            JSONObject json=new JSONObject();
            try {
                json.put("id", contacts.getId());
                json.put("userName", contacts.getUserName());
                json.put("emailId", contacts.getEmailId());
                json.put("userId", contacts.getLoggedInUserId());
                json.put("groupsId", contacts.getGroupsID());
                json.put("enabled",contacts.getEnabled());
                jsonContacts.put(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonContacts;
    }

    private JSONObject getfinalJsonData(JSONObject jsonGroupList, JSONObject jsoncontactsList){
        JSONObject json = new JSONObject();
        try {
            json.put("groups", jsonGroupList);
            json.put("contacts", jsoncontactsList);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public void shareIntent(String url){
        Intent i = new Intent(android.content.Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"recipient@example.com"});
        i.putExtra(android.content.Intent.EXTRA_CC, new String[]{""});
        i.putExtra(android.content.Intent.EXTRA_BCC, new String[]{""});
        i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Note From Manahij");
        i.putExtra(android.content.Intent.EXTRA_TEXT, url);

        try {
            startActivity(Intent.createChooser(i, "Send mail...."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(ExportEnrGroupActivity.this, "There are no email clients installed", Toast.LENGTH_SHORT).show();
        }
    }

    private void getContacts(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ExportEnrGroupActivity.this);
        String authToken = prefs.getString(Globals.sAuthToken,"");
        if (!authToken.equals("")){
            String url = "https://www.google.com/m8/feeds/contacts/default/full?alt=json&v=3.0&max-results=500&access_token="+authToken;
            ApiCallBackTask callBackTask = new ApiCallBackTask(ExportEnrGroupActivity.this, url, null, new ApiCallBackListener() {
                @Override
                public void onSuccess(Object object) {
                    if (object!=null) {
                        parseJson((HashMap<String, Object>) object);
                    }
                }

                @Override
                public void onFailure(Exception e) {

                }
            });
            callBackTask.execute();
        }
    }
    private void parseJson(HashMap<String,Object> jsonList){
        if(jsonList!=null){
        gmailContacts.clear();
        HashMap<String,Object> feedList = (HashMap<String, Object>) jsonList.get("feed");
            if(feedList!=null) {
                ArrayList<Object> entryList = (ArrayList<Object>) feedList.get("entry");
                if(entryList!=null) {
                JSONArray emailArr = new JSONArray();
                for (int i = 0; i < entryList.size(); i++) {
                    HashMap<String, Object> entries = (HashMap<String, Object>) entryList.get(i);
                    HashMap<String, Object> titleHashmap = (HashMap<String, Object>) entries.get("title");
                    String title = (String) titleHashmap.get("$t");
                    ArrayList<Object> emailList = (ArrayList<Object>) entries.get("gd$email");
                    if (emailList != null) {
                        for (int i1 = 0; i1 < emailList.size(); i1++) {
                            HashMap<String, Object> emailHashmap = (HashMap<String, Object>) emailList.get(i1);
                            String emailId = (String) emailHashmap.get("address");
                            MyContacts newContact = new MyContacts(ExportEnrGroupActivity.this);
                            newContact.setLoggedInUserId(email);
                            newContact.setEmailId(emailId);
                            newContact.setEnabled(1);
                            newContact.setUserName(title);
                            newContact.setId(0);
                            newContact.setGroupsID(0);
                            newContact.setSelected(false);
                            if (title != null && !title.equals("") && emailId != null) {
                                gmailContacts.add(newContact);
                            }
                            emailArr.put(emailId);
                        }

                }

                }
                    if(ManahijApp.getInstance().isInternetAvailable()){
                        if(gmailContacts.size()>=0||emailArr.length()>0) {
                    NoorUserStatus noorUserStatus = new NoorUserStatus(this, emailArr);
                    noorUserStatus.execute();}}}
            }

        }

        recyclerView.getAdapter().notifyDataSetChanged();
    }

    private class getToken extends AsyncTask<Void,Void,Void>{


        @Override
        protected Void doInBackground(Void... params) {
            String scopes = "oauth2:profile email";
            try {
                String token = GoogleAuthUtil.getToken(ExportEnrGroupActivity.this,email,scopes);
                if (token!=null){
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ExportEnrGroupActivity.this);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(Globals.sAuthToken, token);
                    editor.commit();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (GoogleAuthException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            getContacts();
        }
    }
}