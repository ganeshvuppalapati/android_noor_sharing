package com.semanoor.manahij;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.widget.ImageView;

import com.artifex.mupdfdemo.MuPDFPageView;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.graphics.Bitmap.createBitmap;
import static android.graphics.Bitmap.createScaledBitmap;

/**
 * Created by Krishna on 04-08-2015.
 */
public class CopyOpenPdfReader {


    private Uri Data;
    DatabaseHandler dataBase;
    UserFunctions userfn;
    SlideMenuWithActivityGroup slideMenuActivity;
    Context context;
    pdfActivity pdfActivity;
    public CopyOpenPdfReader(SlideMenuWithActivityGroup mainActivity, Uri data, DatabaseHandler db, UserFunctions userFunction) {
        slideMenuActivity = mainActivity;
        Data = data;
        dataBase = db;
        userfn = userFunction;
        CopyOpenpdfReader(Data);
    }

    public CopyOpenPdfReader(Context contxt) {
         context = contxt;
    }
    public CopyOpenPdfReader(pdfActivity pdf) {
        pdfActivity=pdf;
    }


     private void CopyOpenpdfReader(Uri data) {

        //String path[] = data.toString().split("/");
        String absPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
        String filepath[]=data.toString().split(absPath);
         if (filepath.length>0) {
             String newpath = filepath[1].toString();
             newpath = newpath.replace("%20","");
             String path[] = newpath.split("/");
             int lastId = dataBase.getMaxUniqueRowID("books");
             int pdf_Id = dataBase.getMaxUniqueRowID("tblCategory");
             int id = lastId + 1;
             userfn.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + "P" + id + "Book");
             // String absPath= Environment.getExternalStorageDirectory().getAbsolutePath();
             File file = new File(data.toString());
             String FilePath = file.toString().replace("file:", "");
             if (FilePath.contains("%20")){
                 FilePath = FilePath.replace("%20"," ");
             }
             userfn.copyFiles(FilePath, Globals.TARGET_BASE_BOOKS_DIR_PATH + "P" + id + "Book/" + path[path.length - 1]);
             int totalPages = 6;
             int lastViewedPage = 0;
             String book = "P" + id;
             int CID = 6;
             String bLanguage = "English";
             String[] pdf_title = path[path.length - 1].split("\\.");
             dataBase.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,storeID,ShowPageNo,CID,Search,Bookmark,Copy,Flip,Navigation,Highlight,Note,GotoPage,IndexPage,Zoom,WebSearch,WikiSearch,UpdateCounts,google,youtube,semaBook,semaContent,yahoo,bing,ask,jazeera,blanguage,TranslationSearch,dictionarySearch,booksearch,StoreBookCreatedFromIpad,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,clientID,bookDirection) " +
                     "values('" + pdf_title[0] + "','description','Semanoor','6','" + totalPages + "','0','0','1','false','" + lastViewedPage + "','" + book + "','false','" + CID + "','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','yes','1','yes','yes','yes','yes','yes','yes','yes','yes','" + bLanguage + "','yes','yes','yes','" + "no" + "','downloading','','yes','no','0.00','','created','0','ltr')");
             int lastBook = dataBase.getMaxUniqueRowID("books");
             UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + lastBook + "/FreeFiles");
             UserFunctions.makeFileWorldReadable(Globals.TARGET_BASE_BOOKS_DIR_PATH + lastBook);
             UserFunctions.createNewDirectory(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + lastBook);
             UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + lastBook + "/tempFiles");
             int width = Globals.getDeviceWidth();
             int height = Globals.getDeviceHeight();
             String offsetWidthAndHeight = width + "|" + height;
             dataBase.executeQuery("update books set OffsetWidthAndHeight='" + offsetWidthAndHeight + "' where BID='" + lastBook + "'");
             dataBase.executeQuery("update tblCategory set isHidden='false' where CID='6'and isHidden='true'");
             slideMenuActivity.openPdfReader = true;
         }else{
             UserFunctions.alert(context.getResources().getString(R.string.unsupported_file),context);
         }
    }

   /* @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void takeScreenShot(String FilePath, int bookId, int page, int pageCount) {
        File f = new File(FilePath);
        Bitmap b;
        String screenShot;
        try {

            //String coverandendpage=absPath+"/Download";
            PdfRenderer pr = new PdfRenderer(ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_WRITE));
            if (page == 0) {
                screenShot = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/card.png";
                //screenShot=coverandendpage+"/card.png";
                b = Bitmap.createBitmap(Globals.getDeviceIndependentPixels(200, context), Globals.getDeviceIndependentPixels(300, context), Bitmap.Config.ARGB_4444);
                pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                //int name=pr.openPage(page).getIndex();
                takeScreenShot(b,screenShot);
                //UserFunctions.saveBitmapImage(b, screenShot);

                int pageNo = page + 1;
                screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_" + pageNo + ".png";
                //UserFunctions.saveBitmapImage(b, screenShot);
                takeScreenShot(b,screenShot);

                String front_thumb_path = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId+"/FreeFiles/frontThumb.png";
                //UserFunctions.saveBitmapImage(b, front_thumb_path);
                takeScreenShot(b,front_thumb_path);

            } else if (page < pr.getPageCount() - 1) {
                int pageNo = page + 1;
                screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_" + pageNo + ".png";
                //screenShot=coverandendpage+"/thumbnail_"+pageNo+".png";
                b = Bitmap.createBitmap(Globals.getDeviceIndependentPixels(130, context), Globals.getDeviceIndependentPixels(170, context), Bitmap.Config.ARGB_4444);
                pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                takeScreenShot(b,screenShot);
                //UserFunctions.saveBitmapImage(b, screenShot);
            } else {
                //screenShot=coverandendpage+"/back_P.png";
                screenShot = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/back_P.png";
                b = Bitmap.createBitmap(Globals.getDeviceIndependentPixels(130, context), Globals.getDeviceIndependentPixels(170, context), Bitmap.Config.ARGB_4444);
                pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
               // UserFunctions.saveBitmapImage(b, screenShot);
                takeScreenShot(b,screenShot);

                String screenShot1 = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_end.png";
               // UserFunctions.saveBitmapImage(b, screenShot1);
                takeScreenShot(b,screenShot1);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }  */
/*
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public class takeScreenShot extends AsyncTask<Void, Void, Void> {
        String FilePath;
        Book book; int page,pageCount;
        TextCircularProgressBar processDialog;
        DownloadBackground background;
        public takeScreenShot(String pdfName, Book newBook, TextCircularProgressBar textCircularProgressBar, DownloadBackground instance) {
              FilePath=pdfName;
              book=newBook;
              processDialog= textCircularProgressBar;
              background=instance;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* processDialog = new ProgressDialog(storeActivity);
            processDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            processDialog.setMessage(storeActivity.getResources().getString(R.string.processing));
            processDialog.setCancelable(false);
            processDialog.setCanceledOnTouchOutside(false);
            processDialog.show();*/
     /*   }

        @Override
        protected Void doInBackground(Void... params) {
            File f = new File(FilePath);
            Bitmap b = null;
            String screenShot;
            try {
                //String coverandendpage=absPath+"/Download";
                PdfRenderer pr = new PdfRenderer(ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_WRITE));
               // processPageProgDialog.setProgress((page * 100) / pr.getPageCount());
                for (int i = 0; i < pr.getPageCount(); i++) {
                    //publishProgress((int) ((i * 100) / pr.getPageCount()));
                    savingImages(FilePath,book,i,pr.getPageCount(),processDialog);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(processDialog!=null) {
                processDialog.setVisibility(View.INVISIBLE);
                if (background.getList().size() > 0) {
                    background.getList().remove(0);
                }
                if (background.getList().size() > 0) {
                    final Book book = background.getList().get(0);
                    background.setDownLoadBook(book);
                    background.setDownloadTaskRunning(true);
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            background.new downloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,book.getDownloadURL());
                        }
                    },2000);
                }else{
                    background.setDownloadTaskRunning(false);
                }
             }
        }
    }
    public void savingImages(String FilePath, Book book, final int page, final int pageCount, final TextCircularProgressBar processDialog) {
        File f = new File(FilePath);
        Bitmap b = null;
        String screenShot;
        if(processDialog!=null) {
//            ((MainActivity) context).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
                    processDialog.setProgress((page * 100) / pageCount);
//                }
//            });
        }
        try {

            //String coverandendpage=absPath+"/Download";
            PdfRenderer pr = new PdfRenderer(ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_WRITE));
           // processPageProgDialog.setProgress((page * 100) / pr.getPageCount());
            if (page == 0) {
                screenShot = Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID() + "/FreeFiles/card.png";
                //screenShot=coverandendpage+"/card.png";
               // if(!new File(screenShot).exists()) {
                    if(context instanceof MainActivity) {
                        b = Bitmap.createBitmap(Globals.getDeviceIndependentPixels(200,(MainActivity)context), Globals.getDeviceIndependentPixels(300, (MainActivity)context), Bitmap.Config.ARGB_4444);
                     }else{
                        b = Bitmap.createBitmap(Globals.getDeviceIndependentPixels(200,(StoreTabViewFragment)context), Globals.getDeviceIndependentPixels(300, (StoreTabViewFragment)context), Bitmap.Config.ARGB_4444);
                     }
                        pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

                   // takeScreenShot(b,screenShot);
                   //UserFunctions.saveBitmapImage(b, screenShot);
               // }

                int pageNo = page + 1;
                screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookID() + "/thumbnail_" + pageNo + ".png";
                //UserFunctions.saveBitmapImage(b, screenShot);
                takeScreenShot(b,screenShot);

                String front_thumb_path = Globals.TARGET_BASE_BOOKS_DIR_PATH+book.getBookID()+"/FreeFiles/frontThumb.png";
                //UserFunctions.saveBitmapImage(b, front_thumb_path);
                takeScreenShot(b,front_thumb_path);

            } else if (page < pr.getPageCount() - 1) {
                int pageNo = page + 1;
                screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookID() + "/thumbnail_"+ pageNo + ".png";
                //screenShot=coverandendpage+"/thumbnail_"+pageNo+".png";
                if(context instanceof MainActivity) {
                    b = Bitmap.createBitmap(Globals.getDeviceIndependentPixels(130, (MainActivity)context), Globals.getDeviceIndependentPixels(170, (MainActivity)context), Bitmap.Config.ARGB_4444);
                }else{
                    b = Bitmap.createBitmap(Globals.getDeviceIndependentPixels(130, (StoreTabViewFragment)context), Globals.getDeviceIndependentPixels(170, (StoreTabViewFragment)context), Bitmap.Config.ARGB_4444);

                }
                pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                takeScreenShot(b,screenShot);
                //UserFunctions.saveBitmapImage(b, screenShot);
            } else {
                screenShot = Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID() + "/FreeFiles/back_P.png";
                if(context instanceof MainActivity) {
                    b = Bitmap.createBitmap(Globals.getDeviceIndependentPixels(130, (MainActivity)context), Globals.getDeviceIndependentPixels(170, (MainActivity)context), Bitmap.Config.ARGB_4444);
                }else{
                    b = Bitmap.createBitmap(Globals.getDeviceIndependentPixels(130, (StoreTabViewFragment)context), Globals.getDeviceIndependentPixels(170, (StoreTabViewFragment)context), Bitmap.Config.ARGB_4444);

                }
                    pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                // UserFunctions.saveBitmapImage(b, screenShot);
                takeScreenShot(b,screenShot);

                String screenShot1 = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookID() + "/thumbnail_end.png";
                // UserFunctions.saveBitmapImage(b, screenShot1);
                takeScreenShot(b,screenShot1);
               // processPageProgDialog.dismiss();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
  }*/

    private void takeScreenShot(Bitmap bit,String imagePath){
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imagePath);
            bit.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();

        } catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
   private Bitmap savingPageImages(File f, int page){
        PdfRenderer pr = null;
        Bitmap b = null;
        try {
             pr = new PdfRenderer(ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_WRITE));
             b = createBitmap(Globals.getDeviceWidth(), Globals.getDeviceHeight(), Bitmap.Config.ARGB_4444);
             pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return b;
      }


    public class thumbnailImages extends AsyncTask<Void, Void, Void> {
        String FilePath;
        Book book; int page,pageCount;
        ProgressDialog processPageProgDialog;
        public thumbnailImages(String pdfName, Book newBook) {
            FilePath=pdfName;
            book=newBook;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*processPageProgDialog = new ProgressDialog(pdfActivity);
            processPageProgDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            processPageProgDialog.setMessage(pdfActivity.getResources().getString(R.string.processing));
            processPageProgDialog.setCancelable(false);
            processPageProgDialog.setCanceledOnTouchOutside(false);
            processPageProgDialog.show();*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (FilePath != null) {
                File f = new File(FilePath);
                Bitmap b = null;
                String screenShot;
                try {
                    GenerateThumbnailsPdf thumbnailsPdf = new GenerateThumbnailsPdf(context, FilePath,null);
                    for (int i = 0; i < thumbnailsPdf.countPages(); i++) {
                        String screenShot1 = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookID() + "/thumbnail_" + i+1 + ".png";
                        if (!new File(screenShot1).exists()) {
                            b = thumbnailsPdf.createThumbnail(Globals.getDeviceWidth(), Globals.getDeviceHeight(), i);
                            savingThumbnails(thumbnailsPdf.countPages(), book.getBookID(), i, b);
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            /*if(processPageProgDialog!=null){
                processPageProgDialog.dismiss();
            }*/
        }
    }

    public void savingThumbnails(int pageCount, int bookId, int page, Bitmap b) {
        //File f = new File(FilePath);
       // Bitmap b;
        String screenShot;

          //  PdfRenderer pr = new PdfRenderer(ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_WRITE));
            if (page == 0) {
            screenShot = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/card.png";
            Bitmap  bitmap = createScaledBitmap(b,Globals.getDeviceIndependentPixels(200, pdfActivity), Globals.getDeviceIndependentPixels(300, pdfActivity), true);
            //pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

            if(!new File(screenShot).exists()) {
                takeScreenShot(bitmap, screenShot);
            }
            int pageNo = page + 1;
            screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_" + pageNo + ".png";
            takeScreenShot(bitmap,screenShot);
            String front_thumb_path = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookId+"/FreeFiles/frontThumb.png";

            takeScreenShot(bitmap,front_thumb_path);
            // Bitmap pageScreenShot= savingPageImages(f,page);
            takeScreenShot(b, Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId+ "/FreeFiles/front_P.png");

        } else if (page < pageCount- 1) {
            int pageNo = page + 1;
            screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_" + pageNo + ".png";

            Bitmap  bitmap = createScaledBitmap(b,Globals.getDeviceIndependentPixels(130, pdfActivity), Globals.getDeviceIndependentPixels(170, pdfActivity), true);
            //pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            if(!new File(screenShot).exists()) {
                takeScreenShot(bitmap, screenShot);
                //Bitmap pageScreenShot= savingPageImages(f,page);
                takeScreenShot(b, Globals.TARGET_BASE_BOOKS_DIR_PATH +bookId+ "/FreeFiles/"+pageNo+".png");
            }
        } else {
            screenShot = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/back_P.png";

            Bitmap  bitmap = createScaledBitmap(b,Globals.getDeviceIndependentPixels(130, pdfActivity), Globals.getDeviceIndependentPixels(170, pdfActivity), true);
            // pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            if(!new File(screenShot).exists()) {
                takeScreenShot(bitmap, screenShot);
            }
            String screenShot1 = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookId + "/thumbnail_end.png";
            takeScreenShot(b,screenShot1);
        }

    }

}
