package com.semanoor.manahij;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.ptg.views.CircleButton;
import com.semanoor.fragments.viewpager.PagerAdapter;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.sboookauthor_store.StoreDatabase;
import com.semanoor.source_sboookauthor.DownloadBackground;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.ObjectSerializer;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class StoreTabViewFragment extends Activity implements OnClickListener {
	private int storeViewActivityCode = 10005;
	public static TabHost mTabHost;
	public static int tabPosition;
	private ViewPager mViewPager;
	private PagerAdapter mPagerAdapter;
	public GridShelf gridShelf;
	public ArrayList<String> downloadedBooksArray;
	public ArrayList<String> downloadingBooksArray;
	public boolean isDrawerOpened = false;

	private boolean adsPurchased;
	private InterstitialAd interstitial;
	public static String ITEM_SKU_SUB;
	public IabHelper mHelper;

	public String language;
	public HorizontalScrollView tabsScrollView;

	ArrayList<StoreList> storeListData = new ArrayList<StoreList>();
	ArrayList<StoreList> featuredListData = new ArrayList<StoreList>();
	ArrayList<StoreCategory> storeCategoryList = new ArrayList<>();
	ArrayList<StoreCountry> storeCountryList = new ArrayList<>();
	ArrayList<StoreUniversity> storeUniversityList = new ArrayList<>();
	ArrayList<StoreCategory> storeLanguageList = new ArrayList<>();
	ArrayList<ArrayList<StoreList>> prevStoreListData = new ArrayList<ArrayList<StoreList>>();
	ArrayList<StoreList> recent_storeListData = new ArrayList<StoreList>();
	ArrayList<StoreList> fav_StoreList = new ArrayList<>();
	ArrayList<String> selectedUniversityList = new ArrayList<>();
	ArrayList<String> selectedCategoryList = new ArrayList<>();
	ArrayList<String> selectedLanguageList = new ArrayList<>();
	ArrayList<String> selectedCountryList = new ArrayList<>();
	ListView Store_list;
	RelativeLayout listViewLayout;
	TextView intMsg, intMsg1;
	//SlidingPaneLayout mslidingPanel;
	LinearLayout store_layout;
	StoreList visitedStore;
	String recentStores;
	String favStore;
	String internetOff = "NO";
	ProgressBar progressBar;
	String Store_ImagesPath = Globals.TARGET_BASE_FILE_PATH + "storeImages";
	int recentImages_size;
	public DrawerLayout drawerLayout;
	public Subscription subscription;
	Bundle savedInstance;
	LinearLayout tab_layout;
	RelativeLayout relativeLayout1, rl_parentLayout;
	public ArrayList<String> downloadList = new ArrayList<>();
	public DownloadBackground background;
	String themesPath = Globals.TARGET_BASE_FILE_PATH + "Themes/current/";
	private ArrayList<String> shelfTitleList;
	private Button settings_btn, category_listbtn, search_btn;
	private RecyclerView categoryRecyclerView;
	private shelfAdapter storeShelfhelfAdapter;
	private ArrayList<Integer> expandList = new ArrayList<>();
	private NewCategoryListAdapter categoryListAdapter;
	private boolean isSaveBtnClickable = false;
	private AdView adView;
	private RelativeLayout backBtnlayout;
	private Groups groups;
	CheckBox checkBox;
	private FirebaseAnalytics firebaseAnalytics;

	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		loadLocale();
		// Inflate the layout
		setContentView(R.layout.store_layout);
		// Initialise the TabHost
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/FiraSans-Regular.otf");
		ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

		UserFunctions.changeFont(rootView, font);

		gridShelf = (GridShelf) getIntent().getSerializableExtra("Shelf");
		savedInstance = savedInstanceState;
		groups = Groups.getInstance();
		firebaseAnalytics = FirebaseAnalytics.getInstance(this);
		firebaseAnalytics.setAnalyticsCollectionEnabled(true);
		firebaseAnalytics.setMinimumSessionDuration(20000);
		Bundle bundle1 = new Bundle();
		String name = "Store Opened";
		bundle1.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
		firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle1);
		rl_parentLayout = (RelativeLayout) findViewById(R.id.root_view);
		listViewLayout = (RelativeLayout) findViewById(R.id.PageView);
		checkBox = (CheckBox) findViewById(R.id.checkBox1);
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_store);
		backBtnlayout = (RelativeLayout) findViewById(R.id.frameLayout);
		RelativeLayout selectallLayout = (RelativeLayout) findViewById(R.id.checklist_layout);
		selectallLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (checkBox.isChecked()) {
					checkBox.setChecked(false);
					removeAllCategory();
					if (categoryListAdapter != null) {
						categoryListAdapter.notifyDataSetChanged();
					}
				} else {
					checkBox.setChecked(true);
					selectAllCategory();
					if (categoryListAdapter != null) {
						categoryListAdapter.notifyDataSetChanged();
					}
				}
			}
		});
		categoryRecyclerView = (RecyclerView) findViewById(R.id.PagelistView);
		intMsg = (TextView) findViewById(R.id.intMsg1);
		intMsg1 = (TextView) findViewById(R.id.intMsg2);
		intMsg.setVisibility(View.GONE);
		intMsg1.setVisibility(View.GONE);
		settings_btn = (Button) findViewById(R.id.settings_btn);
		settings_btn.setOnClickListener(this);
		category_listbtn = (Button) findViewById(R.id.list_category_btn);
		category_listbtn.setOnClickListener(this);
		search_btn = (Button) findViewById(R.id.btn_search);
		search_btn.setOnClickListener(this);
		//store_layout = (LinearLayout) findViewById(R.id.store_layout);
		//	drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		tab_layout = (LinearLayout) findViewById(R.id.tab_layout);
		//	drawerLayout.setScrimColor(Color.TRANSPARENT);
		//mslidingPanel=(SlidingPaneLayout)findViewById(R.id.SlidingPanel);
		UserFunctions.createNewDirectory(Store_ImagesPath);
		UserFunctions.applyingBackgroundImage(themesPath + "store_background.png", rl_parentLayout, StoreTabViewFragment.this);
		IntentFilter filter = new IntentFilter();
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(mNetworkStateIntentReceiver, filter);
		//store_layout.setVisibility(View.GONE);
		//mslidingPanel.setParallaxDistance(200);
		//store_layout.setGravity(Gravity.LEFT);
		//  mslidingPanel.openPane();
		//  store_layout.setVisibility(View.GONE);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		if (checkInternetConnection()) {
			progressBar.setVisibility(View.VISIBLE);
		} else {
			progressBar.setVisibility(View.GONE);
		}
		getSelectedUniversityList();
		getSelectedCategoryList();
		getSelectedLanguageList();
		//	drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		//mslidingPanel.setPanelSlideListener(panelListener);
		//mslidingPanel.openPane();
		//mslidingPanel.setSliderFadeColor(getResources().getColor(android.R.color.transparent));
		shelfTitleList = new ArrayList<>();
//		shelfTitleList.add("Explore");
//		shelfTitleList.add("Favorite");
//		shelfTitleList.add("Recent");
//		shelfTitleList.add("Featured");
		shelfTitleList.add(this.getResources().getString(R.string.all));
		shelfTitleList.add(this.getResources().getString(R.string.favourite));
		shelfTitleList.add(this.getResources().getString(R.string.RECENT));
		shelfTitleList.add(this.getResources().getString(R.string.FEATURED));
		Store_list = (ListView) findViewById(R.id.gridView);
		if (checkInternetConnection()) {
			loadStore();
		}
		drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {

			}

			@Override
			public void onDrawerOpened(View drawerView) {
				if (selectedCategoryList.size() == storeCategoryList.size()) {
					checkBox.setChecked(true);
				} else {
					checkBox.setChecked(false);
				}
			}

			@Override
			public void onDrawerClosed(View drawerView) {

			}

			@Override
			public void onDrawerStateChanged(int newState) {

			}
		});
		Button saveBtn = (Button) findViewById(R.id.save_btn);
		saveBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isSaveBtnClickable && checkForNoneSelected()) {
					saveClicked();
					drawerLayout.closeDrawer(Gravity.RIGHT);
				}
			}
		});
		CircleButton btnLibrary = (CircleButton) findViewById(R.id.LibraryBtn);
		btnLibrary.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

	}

	private void addingSelectedStoreList() {
		boolean exist = false;
		if (recent_storeListData.size() > 0) {
			for (int i = 0; i < recent_storeListData.size(); i++) {
				StoreList list = recent_storeListData.get(i);
				if (list.getId() == visitedStore.getId() && list.getNameEn().equals(visitedStore.getNameEn())) {
					exist = false;
					recent_storeListData.remove(i);
				} else {
					//recent_storeListData.add(0,visitedStore);
					exist = false;
				}
			}
			if (!exist) {
				recent_storeListData.add(0, visitedStore);
			}
		} else {
			recent_storeListData.add(visitedStore);
		}
		if (recent_storeListData.size() > 6) {
			//int size=recentImages_size;
			for (int i = 6; i < recent_storeListData.size(); i++) {
				recent_storeListData.remove(i);
			}
		}

	}

	private void loadingRecentStoreList() {
		/*if(storeListData.size()>1) {
			recentImages_size = storeListData.size() / 2;
		}else{
			recentImages_size=1;
		} */
		//recent_storeListData=new ArrayList<StoreList>();
		if (recentStores != null && !recentStores.equals("")) {
			String[] data = recentStores.split("##");
			for (int i = 0; i < data.length; i++) {
				String store = data[i];
				String[] id = store.split("\\|");
				if (storeListData.size() > 0) {
					for (StoreList storeData : storeListData) {
						if (id[0].equals(storeData.getClientID() + "") && id[1].equals(storeData.getNameEn())) {
							recent_storeListData.add(storeData);
						}
						for (StoreList storeList1 : storeData.getSubClientsList()) {
							if (id[0].equals(storeList1.getClientID() + "") && id[1].equals(storeList1.getNameEn())) {
								recent_storeListData.add(storeList1);
							}
							for (StoreList storeList2 : storeList1.getSubClientsList()) {
								if (id[0].equals(storeList2.getClientID() + "") && id[1].equals(storeList2.getNameEn())) {
									recent_storeListData.add(storeList2);
								}
								for (StoreList storeList3 : storeList2.getSubClientsList()) {
									if (id[0].equals(storeList3.getClientID() + "") && id[1].equals(storeList3.getNameEn())) {
										recent_storeListData.add(storeList3);
									}
								}
							}
						}
					}
				}
				if (featuredListData.size() > 0) {
					for (StoreList storeData : featuredListData) {
						if (id[0].equals(storeData.getClientID() + "") && id[1].equals(storeData.getNameEn())) {
							recent_storeListData.add(storeData);
						}
						for (StoreList storeList1 : storeData.getSubClientsList()) {
							if (id[0].equals(storeList1.getClientID() + "") && id[1].equals(storeList1.getNameEn())) {
								recent_storeListData.add(storeList1);
							}
							for (StoreList storeList2 : storeList1.getSubClientsList()) {
								if (id[0].equals(storeList2.getClientID() + "") && id[1].equals(storeList2.getNameEn())) {
									recent_storeListData.add(storeList2);
								}
								for (StoreList storeList3 : storeList2.getSubClientsList()) {
									if (id[0].equals(storeList3.getClientID() + "") && id[1].equals(storeList3.getNameEn())) {
										recent_storeListData.add(storeList3);
									}
								}
							}
						}
					}
				}
			}
		}
		if (recent_storeListData.size() > 6) {
			//int size=recentImages_size;
			for (int i = 6; i < recent_storeListData.size(); i++) {
				recent_storeListData.remove(i);
			}
		}
		if (recent_storeListData.size() > 0) {
			visitedStore = recent_storeListData.get(0);
		} else if (storeListData.size() > 0) {
			visitedStore = storeListData.get(0);
		}
	}

	private void loadingFavStoreList() {
		/*if(storeListData.size()>1) {
			recentImages_size = storeListData.size() / 2;
		}else{
			recentImages_size=1;
		} */
		//recent_storeListData=new ArrayList<StoreList>();
		fav_StoreList.clear();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		favStore = prefs.getString(Globals.fav_StoreList, "");
		if (favStore != null && !favStore.equals("")) {
			String[] data = favStore.split(",");
			for (int i = 0; i < data.length; i++) {
				String store = data[i];
				if (storeListData.size() > 0) {
					for (StoreList storeData : storeListData) {
						if (store.equals(storeData.getClientID() + "")) {
							fav_StoreList.add(storeData);
						}
						for (StoreList storeList1 : storeData.getSubClientsList()) {
							if (store.equals(storeList1.getClientID() + "")) {
								fav_StoreList.add(storeList1);
							}
							for (StoreList storeList2 : storeList1.getSubClientsList()) {
								if (store.equals(storeList2.getClientID() + "")) {
									fav_StoreList.add(storeList2);
								}
								for (StoreList storeList3 : storeList2.getSubClientsList()) {
									if (store.equals(storeList3.getClientID() + "")) {
										fav_StoreList.add(storeList3);
									}
								}
							}
						}
					}
				}
				if (featuredListData.size() > 0) {
					for (StoreList storeData : featuredListData) {
						if (store.equals(storeData.getClientID() + "")) {
							fav_StoreList.add(storeData);
						}
						for (StoreList storeList1 : storeData.getSubClientsList()) {
							if (store.equals(storeList1.getClientID() + "")) {
								fav_StoreList.add(storeList1);
							}
							for (StoreList storeList2 : storeList1.getSubClientsList()) {
								if (store.equals(storeList2.getClientID() + "")) {
									fav_StoreList.add(storeList2);
								}
								for (StoreList storeList3 : storeList2.getSubClientsList()) {
									if (store.equals(storeList3.getClientID() + "")) {
										fav_StoreList.add(storeList3);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.settings_btn:
				settings_btn.setBackgroundResource(R.drawable.ic_globe_pink);
				backBtnlayout.setVisibility(View.GONE);
				settingsDialog();
				break;
			case R.id.btn_search:

				break;
			case R.id.list_category_btn:
				if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
					drawerLayout.closeDrawer(Gravity.RIGHT);
				} else {
					drawerLayout.openDrawer(Gravity.RIGHT);
				}

				break;
		}
	}


	public class lvimage extends AsyncTask<String, Void, Bitmap> {
		//ImageView imagepath;
		ImageView img;
		int id;


		public lvimage(ImageView imgView, int StoreId) {
			img = imgView;
			id = StoreId;
		}

		protected Bitmap doInBackground(String... urls) {
			String URLpath = urls[0];
			Bitmap bmp = null;
			try {
				URL url = new URL(URLpath);
				URLConnection ucon = url.openConnection();
				ucon.connect();
				bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
				//System.out.println(bmp.getWidth());;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

			return bmp;

		}

		protected void onPostExecute(Bitmap result) {
			if (result != null) {
				img.setVisibility(View.VISIBLE);
				Bitmap bit = result.createScaledBitmap(result, (int) getResources().getDimension(R.dimen.store_image_width), (int) getResources().getDimension(R.dimen.store_image_width), false);
				String imagePath = Store_ImagesPath + "/" + id + ".png";
				//UserFunctions.saveBitmapImage(bit,imagePath);
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(imagePath);
					bit.compress(Bitmap.CompressFormat.PNG, 100, fos);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

				img.setImageBitmap(bit);
			}
		}
	}

	public class getImagesTask extends AsyncTask<Void, Void, Void> {
		JSONObject json;
		String jsonData;


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// imageProgress.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {

			//InputStream in=getAssets().open("38_clients.json");
			String returnResponse = "";
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(StoreTabViewFragment.this);
			String scopeId = prefs.getString(Globals.sUserIdKey, "");
			Clients clients = Clients.getInstance();
			if (UserFunctions.checkLoginAndAlert(StoreTabViewFragment.this, false)) {
				returnResponse = clients.readJsonData(scopeId + "_clients");
			} else {
				if (new File(Globals.TARGET_BASE_FILE_PATH + "clients/0_clients.json").exists()) {
					returnResponse = clients.readJsonData("0_clients");
				} else {
					clients.getClientsList(Globals.redeemURL + "0", "0");
				}
			}

			if (returnResponse == null && returnResponse.equals("")) {
				return null;
			}
			jsonData = returnResponse;
			jsonData = jsonData.replaceAll("\\\\r\\\\n", "");
			jsonData = jsonData.replace("\"{", "{");
			jsonData = jsonData.replace("}\",", "},");
			jsonData = jsonData.replace("}\"", "}");
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (jsonData == null || jsonData.equals("")) {
				return;
			}
			if (storeListData.size() > 0) {
				storeListData.clear();
			}
			//	getJsonData(jsonData);
			JSONArray json = null;
			try {
				json = new JSONArray(jsonData);
				ArrayList<Object> jsonMap = (ArrayList<Object>) toList(json);
				//	storeListData = parseJSON(jsonMap);
//				Store_list.setHasFixedSize(true);
//				RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false);
//				Store_list.setLayoutManager(mLayoutManager);
//				Store_list.setItemAnimator(new DefaultItemAnimator());
//				Store_list.setAdapter(new shelfAdapter(shelfTitleList));
				if (progressBar.isShown()) {
					progressBar.setVisibility(View.GONE);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			loadingRecentStoreList();

			SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
			adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);

			if (!adsPurchased) {
				interstitial = new InterstitialAd(StoreTabViewFragment.this);
				interstitial.setAdUnitId(Globals.INTERSTITIAL_AD_UNIT_ID);
				AdRequest adRequest = new AdRequest.Builder()
						//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
						//.addTestDevice("505F22CBB28695386696655EED14B98B")
						.build();
				interstitial.loadAd(adRequest);
			}
			if (visitedStore != null) {
				//	StoreTabViewFragment.this.initialiseTabHost();
				StoreDatabase store_database = new StoreDatabase(StoreTabViewFragment.this);
				downloadedBooksArray = store_database.DownloadBooksList();
				downloadingBooksArray = store_database.DownloadingBooksList();

				//	subscription = Subscription.getInstance();

			/*	if (savedInstance != null) {
					mTabHost.setCurrentTabByTag(savedInstance.getString("tab")); //set the tab as per the saved state
				}

				storeActivity();*/
//				if (tab_layout.getWidth() < relativeLayout1.getWidth()) {
//					tab_layout.setGravity(Gravity.CENTER);
//				}
			}
		}
	}

	private void getJsonData(String jsonData) {
		HashMap<String, String> map = new HashMap<String, String>();
		String key, key1, value, value1 = null, key_sub, value_sub;
		int id = 0, sub_id;

		try {
			//jsonData=jsonData.replace("[","");
			JSONObject json = new JSONObject(jsonData);

			JSONObject store = json.getJSONObject("semanoorStoreList");
			HashMap<String, String> map1 = new HashMap<String, String>();
			Iterator iter = store.keys();
			while (iter.hasNext()) {
				key = (String) iter.next();
				value = store.getString(key);
				if (key.equals("storeDetails")) {
					JSONArray store1 = new JSONArray(value);
					for (int i = 0; i < store1.length(); i++) {
						StoreList list = new StoreList();
						JSONObject json1 = store1.getJSONObject(i);
						Iterator iter1 = json1.keys();
						while (iter1.hasNext()) {
							key1 = (String) iter1.next();
							if (key1.equals("ID")) {
								id = json1.getInt(key1);
							} else {
								value1 = json1.getString(key1);
							}

							if (key1.equals("nameEn")) {
								list.setNameEn(value1);
							} else if (key1.equals("nameAr")) {
								list.setNameAr(value1);
							} else if (key1.equals("Description")) {
								list.setDescription(value1);
							} else if (key1.equals("ThumbURL")) {
								list.setThumbUrl(value1);
							} else if (key1.equals("country")) {
								list.setCountry(value1);
							}
							if (key1.equals("country")) {
								list.setCountry(value1);
							}
							if (key1.equals("ID")) {
								list.setId(id);
							}
							if (key1.equals("subStoreDetails")) {
								//storeListData.add(list);
								JSONArray subStore = new JSONArray(value1);
								for (int sub = 0; sub < subStore.length(); sub++) {
									SubStoreList sub_Store_List = new SubStoreList();
									JSONObject jsonSub = subStore.getJSONObject(sub);
									Iterator iter_sub = jsonSub.keys();
									while (iter_sub.hasNext()) {
										key_sub = (String) iter_sub.next();

										if (key_sub.equals("subStoreEn")) {
											sub_Store_List.setSubStoreEn(jsonSub.getString(key_sub));
										} else if (key_sub.equals("subStoreAr")) {
											sub_Store_List.setSubStoreAr(jsonSub.getString(key_sub));
										} else if (key_sub.equals("subID")) {
											sub_Store_List.setSubID(jsonSub.getInt(key_sub));
										} else if (key_sub.equals("URL")) {
											sub_Store_List.setUrl(jsonSub.getString(key_sub));
										} else if (key_sub.equals("shelfPosition")) {
											sub_Store_List.setShelfPosition(jsonSub.getString(key_sub));
										} else if (key_sub.equals("countryEn")) {
											sub_Store_List.setCountryEn(jsonSub.getString(key_sub));
										} else if (key_sub.equals("countryAr")) {
											sub_Store_List.setCountryAr(jsonSub.getString(key_sub));
										} else if (key_sub.equals("universityEn")) {
											sub_Store_List.setUniversityEn(jsonSub.getString(key_sub));
										} else if (key_sub.equals("universityAr")) {
											sub_Store_List.setUniversityAr(jsonSub.getString(key_sub));
										} else if (key_sub.equals("categoryEn")) {
											sub_Store_List.setCategoryEn(jsonSub.getString(key_sub));
										} else if (key_sub.equals("categoryAr")) {
											sub_Store_List.setCategoryAr(jsonSub.getString(key_sub));
										}
									}
									list.getChildobjlist().add(sub_Store_List);
								}
							}
							//storeListData.add(list);
						}
						storeListData.add(list);
					}
					//HashMap<String, String> map1=new HashMap<String, String>();
//					Iterator iter1=store1.keys();
//					while(iter1.hasNext()) {
//						key = (String) iter1.next();
//						value = store1.getString(key);
						/*if(key.equals("subStoreDetails")){
							JSONObject json2 = new JSONObject(value);
							JSONObject store2=json2.getJSONObject("storeDetails");
							//HashMap<String, String> map1=new HashMap<String, String>();
							Iterator iter2=store2.keys();
							while(iter2.hasNext()) {
								key = (String) iter2.next();
								value = store2.getString(key);
							}
						}*/
					//  }

				}
				System.out.println(key);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Load Locale language from shared preference and change language in the application
	 */
	private void loadLocale() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		language = prefs.getString(Globals.languagePrefsKey, "en");
		recentStores = prefs.getString(Globals.recent_StoreList, "recent");
		favStore = prefs.getString(Globals.fav_StoreList, "");
		changeLang(language);
	}

	/**
	 * change language in the application
	 *
	 * @param language
	 */
	private void changeLang(String language) {
		Locale myLocale = new Locale(language);
		Locale.setDefault(myLocale);
		android.content.res.Configuration config = new android.content.res.Configuration();
		config.locale = myLocale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
	}

	public static void setTabTitle() {
		/*ImageView iv = ((ImageView)mTabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.icon));
		iv.setBackgroundResource(R.drawable.old_curriculum_store);
		iv.setVisibility(View.VISIBLE);*/

		//Disable Enrichment
		//mTabHost.in


		ImageView iv1 = ((ImageView) mTabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.icon));
		iv1.setBackgroundResource(R.drawable.new_curriculum_store);
		iv1.setVisibility(View.VISIBLE);

		ImageView iv2 = ((ImageView) mTabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.icon));
		iv2.setBackgroundResource(R.drawable.e_lessons_store);
		iv2.setVisibility(View.VISIBLE);

		ImageView iv3 = ((ImageView) mTabHost.getTabWidget().getChildAt(2).findViewById(android.R.id.icon));
//		iv3.setBackgroundResource(R.drawable.quiz_store);
		iv3.setVisibility(View.VISIBLE);

		ImageView iv4 = ((ImageView) mTabHost.getTabWidget().getChildAt(3).findViewById(android.R.id.icon));
//		iv4.setBackgroundResource(R.drawable.quiz_store);
		iv4.setVisibility(View.VISIBLE);

		//((TextView)mTabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title)).setPadding(10, 0, 0, 0);
		((TextView) mTabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title)).setPadding(10, 0, 0, 0); //Disable Enrichment
		((TextView) mTabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title)).setPadding(10, 0, 0, 0);
		((TextView) mTabHost.getTabWidget().getChildAt(2).findViewById(android.R.id.title)).setPadding(10, 0, 0, 0);
		((TextView) mTabHost.getTabWidget().getChildAt(3).findViewById(android.R.id.title)).setPadding(10, 0, 0, 0);
		//((TextView)mTabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title)).setText(R.string.old_curriculum);
		((TextView) mTabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title)).setText(R.string.new_curriculum); //Disable Enrichment
		((TextView) mTabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title)).setText(R.string.e_lesson_store);
		((TextView) mTabHost.getTabWidget().getChildAt(2).findViewById(android.R.id.title)).setText(R.string.quiz_store);
		((TextView) mTabHost.getTabWidget().getChildAt(3).findViewById(android.R.id.title)).setText(R.string.rzooom_store);
	}

	protected void onSaveInstanceState(Bundle outState) {
		if (mTabHost != null) {
			outState.putString("tab", mTabHost.getCurrentTabTag()); //save the tab selected
			super.onSaveInstanceState(outState);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == storeViewActivityCode && data != null) {
			GridShelf grShelf = (GridShelf) data.getSerializableExtra("Shelf");
			gridShelf = grShelf;
			loadingFavStoreList();
			storeShelfhelfAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onBackPressed() {
		if (prevStoreListData.size() > 0) {
			storeListData = prevStoreListData.get(prevStoreListData.size() - 1);
			prevStoreListData.remove(prevStoreListData.size() - 1);
			if (storeShelfhelfAdapter != null) {
				if (prevStoreListData.size() == 0) {
					shelfTitleList.clear();
					shelfTitleList.add(this.getResources().getString(R.string.all));
					shelfTitleList.add(this.getResources().getString(R.string.favourite));
					shelfTitleList.add(this.getResources().getString(R.string.RECENT));
					shelfTitleList.add(this.getResources().getString(R.string.FEATURED));
				}
				storeShelfhelfAdapter.notifyDataSetChanged();
			}
		} else {
			ImageLoader.getInstance().clearMemoryCache();
			ImageLoader.getInstance().stop();
			super.onBackPressed();
			storingRecentStorelist();
			if (interstitial != null) {
				if (Globals.isLimitedVersion() && !adsPurchased) {
					if (interstitial.isLoaded() &&!groups.getInterstitialAdsVisible()) {
						interstitial.show();
					}
				}
			}
			SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(this);
			SharedPreferences.Editor editor = sharedPreference.edit();
			editor.putString(Globals.recent_StoreList, recentStores);
			editor.commit();
			finish();
		}
	}

	private void storingRecentStorelist() {
		String data = "";
		if (recent_storeListData != null && recent_storeListData.size() > 0) {
			for (int i = 0; i < recent_storeListData.size(); i++) {
				StoreList list = recent_storeListData.get(i);
				if (i == 0) {
					data = list.getClientID() + "|" + list.getNameEn();
				} else {
					data = data + "##" + list.getClientID() + "|" + list.getNameEn();
				}
			}
			recentStores = data;
		}

	}

	private void storingSelectedCountry() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String selectedCategory = "";
		for (int i = 0; i < selectedCountryList.size(); i++) {
			selectedCategory = selectedCategory + (selectedCountryList.get(i)) + (",");
		}
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Globals.selectedCountry, selectedCategory);
		editor.commit();
	}

	private void storingSelectedLanguage() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String selectedCategory = "";
		for (int i = 0; i < selectedLanguageList.size(); i++) {
			selectedCategory = selectedCategory + (selectedLanguageList.get(i)) + (",");
		}
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Globals.selectedlanguage, selectedCategory);
		editor.commit();

	}

	private void storingSelectedCategory() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String selectedCategory = "";
		for (int i = 0; i < selectedCategoryList.size(); i++) {
			selectedCategory = selectedCategory + (selectedCategoryList.get(i)) + (",");
		}
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Globals.selectedCategory, selectedCategory);
		editor.commit();
	}

	private void storingSelectedUniversity() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String selectedCategory = "";
		for (int i = 0; i < selectedUniversityList.size(); i++) {
			selectedCategory = selectedCategory + (selectedUniversityList.get(i)) + (",");
		}
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Globals.selectedCUniversity, selectedCategory);
		editor.commit();
	}

	@Override
	public void finish() {
		setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
		super.finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mHelper != null) mHelper.dispose();
		mHelper = null;
		unregisterReceiver(mNetworkStateIntentReceiver);
		//garbageCollection.unbindReferences(getParent(), R.id.tabsrootView);
		System.gc();
	}

	/*public void subscribe(String inAppProductId)  {
		//ITEM_SKU = "android.test.purchased";
		ITEM_SKU_SUB = "sc001";
		mHelper = new IabHelper(StoreTabViewFragment.this, Globals.manahijBase64EncodedPublicKey);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

			@Override
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					////System.out.println("In-app Billing setup failed: " + result);
					String str = StoreTabViewFragment.this.getResources().getString(R.string.in_app_setup_failed);
					UserFunctions.DisplayAlertDialog(StoreTabViewFragment.this, str + result, R.string.billing_setup_failed);
				} else {
					////System.out.println("In-app Billing setup isOK");
					mHelper.launchSubscriptionPurchaseFlow(StoreTabViewFragment.this, ITEM_SKU_SUB,10001, mPurchaseFinishedListener, "");
				}
			}
		});
	}

	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			if (result.isFailure()) {
				String message = result.getMessage();
				int response = result.getResponse();
				//System.out.println(result+":"+response);
				if (response == 7) {
					UserFunctions.DisplayAlertDialog(StoreTabViewFragment.this,"You've subscribed already", R.string.subscribed);
					//startDownload();
				} else {
					String str = StoreTabViewFragment.this.getResources().getString(R.string.in_app_purchase_failed_);
					UserFunctions.DisplayAlertDialog(StoreTabViewFragment.this, str + result, R.string.purchase_failed);
				}
				return;
			} else if (purchase.getSku().equals(ITEM_SKU_SUB)) {
				consumeItem();
			}
		}
	};

	public void consumeItem() {
		mHelper.queryInventoryAsync(mReceivedInventoryListener);
	}

	IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {

		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {
			if (result.isFailure()) {
				////System.out.println("Failed");
				UserFunctions.DisplayAlertDialogNotFromStringsXML(StoreTabViewFragment.this, result.getMessage(), "");
			} else {
				mHelper.consumeAsync(inv.getPurchase(ITEM_SKU_SUB), mConsumeFinishedListener);
			}
		}
	};

	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {

		@Override
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if (result.isSuccess()) {
				////System.out.println("Success");
				UserFunctions.DisplayAlertDialog(StoreTabViewFragment.this,"Subscribed successfully", R.string.subscribed);
				//startDownload();
			} else {
				////System.out.println("Failed");
				UserFunctions.DisplayAlertDialog(StoreTabViewFragment.this, result.getMessage(), R.string.purchase_failed);
			}
		}
	};*/
	/*boolean verifyDeveloperPayload(Purchase p){
		String payload = p.getDeveloperPayload();
		return true;
	}*/

	private boolean checkInternetConnection() {

		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		// ARE WE CONNECTED TO THE NET

		if (conMgr.getActiveNetworkInfo() != null
				&& conMgr.getActiveNetworkInfo().isAvailable()
				&& conMgr.getActiveNetworkInfo().isConnected()) {
			return true;
		} else {
			return false;
		}

	}

	private BroadcastReceiver mNetworkStateIntentReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			//int extraWifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);
			/*int extraWifiState;
			ConnectivityManager conman = (ConnectivityManager) fa_enrich.getSystemService(Context.CONNECTIVITY_SERVICE);
    		if(conman.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
    				conman.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
    		{
    			extraWifiState=3;
    		}
    		else
    		{
    			extraWifiState=1;
    		}*/

			int extraWifiState = 0;
			if (intent.getAction().equals(
					ConnectivityManager.CONNECTIVITY_ACTION)) {

				NetworkInfo info = intent.getParcelableExtra(
						ConnectivityManager.EXTRA_NETWORK_INFO);
				String typeName = info.getTypeName();
				String subtypeName = info.getSubtypeName();
				// //System.out.println("Network is up ******** "+typeName+":::"+subtypeName);

				if (checkInternetConnection() == true) {
					extraWifiState = 3;

				} else {
					extraWifiState = 1;
				}
			} else {
				extraWifiState = 3;
			}

			switch (extraWifiState) {
				case WifiManager.WIFI_STATE_DISABLED:
					////System.out.println("InternetState:disconnected");
					internetOff = "YES";
					intMsg.setVisibility(View.VISIBLE);
					intMsg1.setVisibility(View.VISIBLE);
					//	listViewLayout.setVisibility(View.GONE);
					//	lv_recentStores.setVisibility(View.GONE);
					Store_list.setVisibility(View.GONE);
					progressBar.setVisibility(View.GONE);
					settings_btn.setOnClickListener(null);
					if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
						drawerLayout.closeDrawer(Gravity.RIGHT);
					}
					drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
					storingRecentStorelist();
					SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(StoreTabViewFragment.this);
					SharedPreferences.Editor editor = sharedPreference.edit();
					editor.putString(Globals.recent_StoreList, recentStores);
					editor.commit();
					storeListData.clear();
					recent_storeListData.clear();
					featuredListData.clear();
					storeCategoryList.clear();
					storeLanguageList.clear();
					storeUniversityList.clear();
					storeCountryList.clear();
					prevStoreListData.clear();
					shelfTitleList.clear();
					break;

				//case WifiManager.WIFI_STATE_DISABLING:

				case WifiManager.WIFI_STATE_ENABLED:
					////System.out.println("InternetState:connected");
					if (internetOff.equals("YES")) {
						//toast.cancel();
						progressBar.setVisibility(View.VISIBLE);
						intMsg.setVisibility(View.GONE);
						intMsg1.setVisibility(View.GONE);
						//	listViewLayout.setVisibility(View.VISIBLE);
						Store_list.setVisibility(View.VISIBLE);
						settings_btn.setOnClickListener(StoreTabViewFragment.this);
						drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
						shelfTitleList.add(getResources().getString(R.string.all));
						shelfTitleList.add(getResources().getString(R.string.favourite));
						shelfTitleList.add(getResources().getString(R.string.RECENT));
						shelfTitleList.add(getResources().getString(R.string.FEATURED));
						if (internetOff.equals("YES")) {
							loadStore();
							internetOff = "NO";
						} else {
							internetOff = "NO";
						}
					}
					break;
			}
		}
	};

	private Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
		Map<String, Object> retMap = new HashMap<String, Object>();
		if (json != JSONObject.NULL) {
			retMap = toMap(json);
		}
		return retMap;
	}

	private Map<String, Object> toMap(JSONObject object) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<String> keysIter = object.keys();
		while (keysIter.hasNext()) {
			String key = keysIter.next();
			Object value = object.get(key);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	private List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

	private ArrayList<StoreList> parseJSON(ArrayList<Object> jsonList, ArrayList<StoreList> storeList) {
		for (int i = 0; i < jsonList.size(); i++) {
			HashMap<String, Object> values = (HashMap<String, Object>) jsonList.get(i);
			StoreList list = parseJSON1(values);
			ArrayList<Object> subChildList = (ArrayList<Object>) values.get("Children");
			for (int sub = 0; sub < subChildList.size(); sub++) {
				list.getSubClientsList().add(getSubClients((HashMap<String, Object>) subChildList.get(sub)));
			}
			storeList.add(list);
		}
		return storeList;
	}

	private StoreList getSubClients(HashMap<String, Object> values) {
		StoreList list = new StoreList();
		list.setNameAr((String) values.get("titleNameAR"));
		list.setClientID((Integer) values.get("ClientId"));
		list.setNameEn((String) values.get("titleNameEN"));
		list.setDescription((String) values.get("EDescription"));
		list.setArDescription((String) values.get("ADescription"));
		list.setEdit((Boolean) values.get("isEdit"));
		list.setPeriod((Integer) values.get("Period"));
		list.setPrice((String) values.get("Price"));
		list.setPublic((Boolean) values.get("isPublic"));
		list.setShare((Boolean) values.get("isShare"));
		list.setSubcription((Boolean) values.get("isSubscription"));
		list.setThumbUrl((String) values.get("ThumbURL"));
		String inAppId = (String) values.get("InAppId");
		list.setInAppId(inAppId.toLowerCase());
		ArrayList<HashMap<String, Object>> subStoreList = (ArrayList<HashMap<String, Object>>) values.get("storeDetails");
		for (int i1 = 0; i1 < subStoreList.size(); i1++) {
			SubStoreList subList = new SubStoreList();
			if (subStoreList.size() > 0) {
				HashMap<String, Object> subHashMap = subStoreList.get(i1);
				subList.setSubStoreAr((String) subHashMap.get("subStoreAr"));
				subList.setSubStoreEn((String) subHashMap.get("subStoreEn"));
				subList.setApplicationID((Integer) subHashMap.get("ApplicationId"));
				subList.setStoreID((Integer) subHashMap.get("StoreId"));
				subList.setCategoryAr((String) subHashMap.get("categoryAr"));
				subList.setCountryEn((String) subHashMap.get("countryEn"));
				subList.setCategoryEn((String) subHashMap.get("categoryEn"));
				subList.setCountryAr((String) subHashMap.get("countryAr"));
				subList.setUrl((String) subHashMap.get("URL"));
				subList.setUniversityAr((String) subHashMap.get("universityAr"));
				subList.setUniversityEn((String) subHashMap.get("universityEn"));
				subList.setShelfPosition(String.valueOf(subHashMap.get("ApplicationId")));
				subList.setCountry((String) subHashMap.get("country"));
				subList.setUniversity((String) subHashMap.get("university"));
				subList.setCategory((String) subHashMap.get("category"));
				subList.setPrefixName((String) subHashMap.get("prefixName"));
			}
			list.getChildobjlist().add(subList);
		}
		ArrayList<Object> subChildList = (ArrayList<Object>) values.get("Children");
		for (int sub = 0; sub < subChildList.size(); sub++) {
			parseJSON(subChildList, list.getSubClientsList());
		}
		return list;
	}

	private StoreList parseJSON1(HashMap<String, Object> values) {
		StoreList list = new StoreList();
		list.setNameAr((String) values.get("titleNameAR"));
		list.setClientID((Integer) values.get("ClientId"));
		list.setNameEn((String) values.get("titleNameEN"));
		list.setDescription((String) values.get("EDescription"));
		list.setArDescription((String) values.get("ADescription"));
		list.setEdit((Boolean) values.get("isEdit"));
		list.setPeriod((Integer) values.get("Period"));
		list.setPrice((String) values.get("Price"));
		list.setPublic((Boolean) values.get("isPublic"));
		list.setShare((Boolean) values.get("isShare"));
		list.setSubcription((Boolean) values.get("isSubscription"));
		list.setThumbUrl((String) values.get("ThumbURL"));
		String inAppId = (String) values.get("InAppId");
		list.setInAppId(inAppId.toLowerCase());
		ArrayList<HashMap<String, Object>> subStoreList = (ArrayList<HashMap<String, Object>>) values.get("storeDetails");
		for (int i1 = 0; i1 < subStoreList.size(); i1++) {
			SubStoreList subList = new SubStoreList();
			if (subStoreList.size() > 0) {
				HashMap<String, Object> subHashMap = subStoreList.get(i1);
				subList.setSubStoreAr((String) subHashMap.get("subStoreAr"));
				subList.setSubStoreEn((String) subHashMap.get("subStoreEn"));
				subList.setApplicationID((Integer) subHashMap.get("ApplicationId"));
				subList.setStoreID((Integer) subHashMap.get("StoreId"));
				subList.setCategoryAr((String) subHashMap.get("categoryAr"));
				subList.setCountryEn((String) subHashMap.get("countryEn"));
				subList.setCategoryEn((String) subHashMap.get("categoryEn"));
				subList.setCountryAr((String) subHashMap.get("countryAr"));
				subList.setUrl((String) subHashMap.get("URL"));
				subList.setUniversityAr((String) subHashMap.get("universityAr"));
				subList.setUniversityEn((String) subHashMap.get("universityEn"));
				subList.setShelfPosition(String.valueOf(subHashMap.get("ApplicationId")));
				subList.setCountry((String) subHashMap.get("country"));
				subList.setUniversity((String) subHashMap.get("university"));
				subList.setCategory((String) subHashMap.get("category"));
				subList.setPrefixName((String) subHashMap.get("prefixName"));
			}
			list.getChildobjlist().add(subList);
		}
		return list;
	}

	private boolean checkForStoreinPrev() {
		boolean exist = false;
		if (prevStoreListData.size() > 0) {
			ArrayList<StoreList> prevData = prevStoreListData.get(prevStoreListData.size() - 1);
			if (storeExist(prevData)) {
				exist = true;
			} else {
				exist = false;
			}
		}
		return exist;
	}

	private boolean storeExist(ArrayList<StoreList> storeList) {
		boolean exist = false;
		for (int i1 = 0; i1 < storeList.size(); i1++) {
			StoreList list = storeList.get(i1);
			if (list.getId() == visitedStore.getId() && list.getNameEn().equals(visitedStore.getNameEn())) {
				exist = true;
				break;
			} else {
				exist = false;
			}
		}
		return exist;
	}

	public class shelfAdapter extends BaseAdapter {

		private ArrayList<String> stringArrayList;
		private int actionBarHeight;

		public shelfAdapter(ArrayList<String> titleList) {
			this.stringArrayList = titleList;
			TypedValue tv = new TypedValue();
			if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
				actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
			}
		}


		public class ViewHolder {
			public TextView title;
			public RecyclerView recyclerView;
			public Button expand_btn;
			public FrameLayout frameLayout;

		}

		@Override
		public int getCount() {
			return stringArrayList.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			if (view == null) {
				view = getLayoutInflater().inflate(R.layout.store_list_inflate, null);
				holder = new ViewHolder();
				holder.title = (TextView) view.findViewById(R.id.text);
				holder.recyclerView = (RecyclerView) view.findViewById(R.id.scrollView1);
				holder.expand_btn = (Button) view.findViewById(R.id.button1);
				holder.frameLayout = (FrameLayout) view.findViewById(R.id.frameButton1);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}
			holder.title.setText(stringArrayList.get(position));
			if (prevStoreListData.size() > 0) {
				NewStoreListAdapter storeListAdapter = new NewStoreListAdapter(storeListData, false);
				ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
				gridlayoutParams.height = Globals.getDeviceHeight()-(actionBarHeight+actionBarHeight)-(int) getResources().getDimension(R.dimen.store_toolbar_layout);
				holder.recyclerView.setLayoutParams(gridlayoutParams);
				GridLayoutManager gridLayout = new GridLayoutManager(StoreTabViewFragment.this, getResources().getInteger(R.integer.recycler_grid_column), GridLayoutManager.VERTICAL, false);
				//	RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
				holder.recyclerView.setLayoutManager(gridLayout);
				holder.recyclerView.setHasFixedSize(true);
				holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
				holder.recyclerView.setAdapter(storeListAdapter);
			} else {
				if (position == 0) {
					NewStoreListAdapter storeListAdapter = new NewStoreListAdapter(storeListData, false);
					if (isViewExpanded(position)) {
						ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
						gridlayoutParams.height = (int) getResources().getDimension(R.dimen.store_shelf_recycler_height) + (int) getResources().getDimension(R.dimen.store_shelf_recycler_height);
						holder.recyclerView.setLayoutParams(gridlayoutParams);
						GridLayoutManager gridLayout = new GridLayoutManager(StoreTabViewFragment.this, getResources().getInteger(R.integer.recycler_grid_column), GridLayoutManager.VERTICAL, false);
						holder.recyclerView.setLayoutManager(gridLayout);
					} else {
						ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
						gridlayoutParams.height = (int) getResources().getDimension(R.dimen.store_shelf_recycler_height);
						holder.recyclerView.setLayoutParams(gridlayoutParams);
						RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
						holder.recyclerView.setLayoutManager(mLayoutManager);
					}
					holder.recyclerView.setHasFixedSize(true);
					holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
					holder.recyclerView.setAdapter(storeListAdapter);
				} else if (position == 1) {
					NewStoreListAdapter storeListAdapter = new NewStoreListAdapter(fav_StoreList, false);
					holder.recyclerView.setHasFixedSize(true);
					if (isViewExpanded(position)) {
						ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
						gridlayoutParams.height = (int) getResources().getDimension(R.dimen.store_shelf_recycler_height) + (int) getResources().getDimension(R.dimen.store_shelf_recycler_height);
						holder.recyclerView.setLayoutParams(gridlayoutParams);
						GridLayoutManager gridLayout = new GridLayoutManager(StoreTabViewFragment.this, getResources().getInteger(R.integer.recycler_grid_column), GridLayoutManager.VERTICAL, false);
						holder.recyclerView.setLayoutManager(gridLayout);
					} else {
						ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
						gridlayoutParams.height = (int) getResources().getDimension(R.dimen.store_shelf_recycler_height);
						holder.recyclerView.setLayoutParams(gridlayoutParams);
						RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
						holder.recyclerView.setLayoutManager(mLayoutManager);
					}
					holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
					holder.recyclerView.setAdapter(storeListAdapter);
				} else if (position == 2) {
					NewStoreListAdapter storeListAdapter = new NewStoreListAdapter(recent_storeListData, false);
					holder.recyclerView.setHasFixedSize(true);
					if (isViewExpanded(position)) {
						ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
						gridlayoutParams.height = (int) getResources().getDimension(R.dimen.store_shelf_recycler_height) + (int) getResources().getDimension(R.dimen.store_shelf_recycler_height);
						holder.recyclerView.setLayoutParams(gridlayoutParams);
						GridLayoutManager gridLayout = new GridLayoutManager(StoreTabViewFragment.this, getResources().getInteger(R.integer.recycler_grid_column), GridLayoutManager.VERTICAL, false);
						holder.recyclerView.setLayoutManager(gridLayout);
					} else {
						ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
						gridlayoutParams.height = (int) getResources().getDimension(R.dimen.store_shelf_recycler_height);
						holder.recyclerView.setLayoutParams(gridlayoutParams);
						RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
						holder.recyclerView.setLayoutManager(mLayoutManager);
					}
					holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
					holder.recyclerView.setAdapter(storeListAdapter);
				} else if (position == 3) {
					NewStoreListAdapter storeListAdapter = new NewStoreListAdapter(featuredListData, false);
					holder.recyclerView.setHasFixedSize(true);
					if (isViewExpanded(position)) {
						ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
						gridlayoutParams.height = (int) getResources().getDimension(R.dimen.store_shelf_recycler_height) + (int) getResources().getDimension(R.dimen.store_shelf_recycler_height);
						holder.recyclerView.setLayoutParams(gridlayoutParams);
						GridLayoutManager gridLayout = new GridLayoutManager(StoreTabViewFragment.this, getResources().getInteger(R.integer.recycler_grid_column), GridLayoutManager.VERTICAL, false);
						holder.recyclerView.setLayoutManager(gridLayout);
					} else {
						ViewGroup.LayoutParams gridlayoutParams = holder.recyclerView.getLayoutParams();
						gridlayoutParams.height = (int) getResources().getDimension(R.dimen.store_shelf_recycler_height);
						holder.recyclerView.setLayoutParams(gridlayoutParams);
						RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
						holder.recyclerView.setLayoutManager(mLayoutManager);
					}
					holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
					holder.recyclerView.setAdapter(storeListAdapter);
				}
			}
			holder.frameLayout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (canExpand(position)) {
						if (isViewExpanded(position)) {
							for (int i = 0; i < expandList.size(); i++) {
								if (position == expandList.get(i)) {
									expandList.remove(i);
									holder.expand_btn.setBackgroundResource(R.drawable.s_client_down);
									break;
								}
							}
						} else {
							holder.expand_btn.setBackgroundResource(R.drawable.s_client_up);
							expandList.add(position);
						}
						notifyDataSetChanged();
					}
				}
			});
			holder.expand_btn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (canExpand(position)) {
						if (isViewExpanded(position)) {
							for (int i = 0; i < expandList.size(); i++) {
								if (position == expandList.get(i)) {
									expandList.remove(i);
									holder.expand_btn.setBackgroundResource(R.drawable.s_client_down);
									break;
								}
							}
						} else {
							holder.expand_btn.setBackgroundResource(R.drawable.s_client_up);
							expandList.add(position);
						}
						notifyDataSetChanged();
					}
				}
			});
			return view;
		}

	}

	private void settingsDialog() {
		final Dialog settingsDialog = new Dialog(StoreTabViewFragment.this);
		settingsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		settingsDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.settings_dialog, null));
		settingsDialog.getWindow().setBackgroundDrawable(null);
		if (Globals.isTablet()) {
			//Globals.getDeviceIndependentPixels(300, context), RelativeLayout.LayoutParams.WRAP_CONTENT
			settingsDialog.getWindow().setLayout((int) (Globals.getDeviceWidth()), (int) (Globals.getDeviceHeight() / 1.177));
		} else {
			settingsDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		}
		settingsDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation1;
		final CheckBox checkBox = (CheckBox) settingsDialog.findViewById(R.id.checkBox1);
		if (selectedLanguageList.size() == storeLanguageList.size()) {
			checkBox.setChecked(true);
		} else {
			checkBox.setChecked(false);
		}
		LinearLayout checklist_layout = (LinearLayout) settingsDialog.findViewById(R.id.checklist_layout);
		RecyclerView country_recyler = (RecyclerView) settingsDialog.findViewById(R.id.country_list);
		LinearLayoutManager horizontalLayoutManagaer
				= new LinearLayoutManager(StoreTabViewFragment.this, LinearLayoutManager.HORIZONTAL, false);
		//  horizontalLayoutManagaer.setStackFromEnd(true);
		// vertical_recycler_view.setLayoutManager(horizontalLayoutManagaer);
		country_recyler.setNestedScrollingEnabled(false);
		country_recyler.setLayoutManager(horizontalLayoutManagaer);
		country_recyler.setAdapter(new CountriesAdapter(storeCountryList, country_recyler));
		country_recyler.setItemAnimator(new DefaultItemAnimator());
		final RecyclerView language_recycler = (RecyclerView) settingsDialog.findViewById(R.id.language_list);
		GridLayoutManager gridLayout = new GridLayoutManager(StoreTabViewFragment.this, getResources().getInteger(R.integer.grid_column), GridLayoutManager.VERTICAL, false);
		//llm.canScrollHorizontally();
		language_recycler.setLayoutManager(gridLayout);
		language_recycler.setHasFixedSize(true);
		final NewLanguageListAdapter adapter = new NewLanguageListAdapter(storeLanguageList, true, checkBox);
		language_recycler.setAdapter(adapter);
		checklist_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (checkBox.isChecked()) {
					checkBox.setChecked(false);
					removeAllLanguages();
					adapter.notifyDataSetChanged();
				} else {
					checkBox.setChecked(true);
					selectAllLanguages();
					adapter.notifyDataSetChanged();
				}
			}
		});
		Button save_btn = (Button) settingsDialog.findViewById(R.id.save_btn);
		save_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isSaveBtnClickable && checkForNoneSelected()) {
					saveClicked();
					settingsDialog.dismiss();
				}
			}
		});
		settingsDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				settings_btn.setBackgroundResource(R.drawable.ic_globe);
				backBtnlayout.setVisibility(View.VISIBLE);
			}
		});
		settingsDialog.show();
	}

	public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.MyViewHolder> {
		ArrayList<StoreCountry> countryArrayList;
		RecyclerView country_Recycler;

		public class MyViewHolder extends RecyclerView.ViewHolder {
			ImageView flag_image;
			RecyclerView recyclerView;
			RelativeLayout checklist_Layout;
			CheckBox checkBox;

			public MyViewHolder(View view) {
				super(view);
				flag_image = (ImageView) view.findViewById(R.id.flag_image);
				recyclerView = (RecyclerView) view.findViewById(R.id.university_list);
				checklist_Layout = (RelativeLayout) view.findViewById(R.id.checklist_layout);
				checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
			}
		}


		public CountriesAdapter(ArrayList<StoreCountry> countryList, RecyclerView country_recyler) {
			this.countryArrayList = countryList;
			this.country_Recycler = country_recyler;
		}

		@Override
		public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.country_list_item, parent, false);

			return new MyViewHolder(itemView);
		}

		@Override
		public void onBindViewHolder(final MyViewHolder holder, int position) {
			StoreCountry country = countryArrayList.get(position);
			Glide.with(StoreTabViewFragment.this).load(country.getFlagPath())
					.thumbnail(0.5f)
					.crossFade()
					.diskCacheStrategy(DiskCacheStrategy.ALL)
					.override(200, 200)
					.into(holder.flag_image);
			holder.recyclerView.setHasFixedSize(true);
			holder.checklist_Layout.setTag(country.getId());
			if (isSelectedAll(country.getId())) {
				holder.checkBox.setChecked(true);
			} else {
				holder.checkBox.setChecked(false);
			}
			RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
			holder.recyclerView.setLayoutManager(mLayoutManager);
			holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
			ArrayList<StoreUniversity> universityList = new ArrayList<>();
			for (int i = 0; i < storeUniversityList.size(); i++) {
				StoreUniversity university = storeUniversityList.get(i);
				if (university.getCountryId() == country.getId()) {
					universityList.add(university);
				}
			}
			if (universityList.size() > 0) {
				holder.checklist_Layout.setVisibility(View.VISIBLE);
			} else {
				holder.checklist_Layout.setVisibility(View.GONE);
			}
			holder.recyclerView.setAdapter(new NewUniversityListAdapter(universityList, false, country_Recycler));
			holder.checklist_Layout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (holder.checkBox.isChecked()) {
						holder.checkBox.setChecked(false);
						removeAllUniversities(String.valueOf(holder.checklist_Layout.getTag()));
						notifyDataSetChanged();
					} else {
						holder.checkBox.setChecked(true);
						selectAllUniversities(String.valueOf(holder.checklist_Layout.getTag()));
						notifyDataSetChanged();
					}
				}
			});
		}

		@Override
		public int getItemCount() {
			return countryArrayList.size();
		}
	}

	public class callWebserviceForExploreList extends AsyncTask<Void, Void, String> {
		String webServiceUrl;
		JSONObject json;
		String jsonData;

		public callWebserviceForExploreList(String url) {
			this.webServiceUrl = url;
		}

		@Override
		protected String doInBackground(Void... params) {
			jsonData = newWebService(webServiceUrl);
			jsonData = jsonData.replaceAll("\\\\r\\\\n", "");
			jsonData = jsonData.replace("\"{", "{");
			jsonData = jsonData.replace("}\",", "},");
			jsonData = jsonData.replace("}\"", "}");
			return jsonData;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (jsonData == null || jsonData.equals("")) {
				return;
			}
			if (storeListData.size() > 0) {
				storeListData.clear();
			}
			JSONArray json = null;
			try {
				json = new JSONArray(jsonData);
				ArrayList<Object> jsonMap = (ArrayList<Object>) toList(json);
				ArrayList<StoreList> storeListArrayList = new ArrayList<>();
				storeListData = parseJSON(jsonMap, storeListArrayList);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

	}

	public class NewStoreListAdapter extends RecyclerView.Adapter<NewStoreListAdapter.MyViewHolder> {

		ArrayList<StoreList> storeList = new ArrayList<StoreList>();
		boolean addClient;

		public class MyViewHolder extends RecyclerView.ViewHolder {
			Button info;
			ImageView img_storeView;
			TextView tv_storeName;

			public MyViewHolder(View view) {
				super(view);
				img_storeView = (ImageView) view.findViewById(R.id.iv_store);
				tv_storeName = (TextView) view.findViewById(R.id.tv_storename);
				info = (Button)view.findViewById(R.id.btn_cleartext);
			}
		}


		public NewStoreListAdapter(ArrayList<StoreList> storeList1, boolean addSubClient) {
			if (visitedStore != null && visitedStore.getChildobjlist().size() > 0 && visitedStore.getSubClientsList().size() > 0) {
				if (checkForStoreinPrev()) {
					if (!storeExist(storeList1)) {
						storeList1.add(0, visitedStore);
					}
					this.storeList = storeList1;
				} else {
					this.storeList = storeList1;
				}
			} else {
				this.storeList = storeList1;
			}
		}

		@Override
		public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.store_items, parent, false);

			return new MyViewHolder(itemView);
		}

		@Override
		public void onBindViewHolder(final MyViewHolder holder, int position) {
			final StoreList store = storeList.get(position);
			holder.itemView.setTag(store.getClientID());
			Glide.with(StoreTabViewFragment.this).load(store.getThumbUrl())
					.thumbnail(0.5f)
					.crossFade()
					.diskCacheStrategy(DiskCacheStrategy.ALL)
					.override(200, 200)
					.centerCrop()
					.into(holder.img_storeView);
		final	String title;
			if (language.equals("en")) {
				title = store.getNameEn();
			} else {
				title = store.getNameAr();
			}
			holder.tv_storeName.setText(title);
			if (storeList.size() > 0 && position == storeList.size() - 1) {
				if (progressBar.isShown()) {
					progressBar.setVisibility(View.GONE);
					//	lv_recentStores.setVisibility(View.VISIBLE);
				}
			}
			holder.info.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					final Dialog infoPopover = new Dialog(StoreTabViewFragment.this);
					infoPopover.getWindow().setBackgroundDrawable(new ColorDrawable(StoreTabViewFragment.this.getResources().getColor(R.color.semi_transparent)));
					//addNewBookDialog.setTitle(R.string.create_new_book);
					infoPopover.requestWindowFeature(Window.FEATURE_NO_TITLE);
					infoPopover.setContentView(StoreTabViewFragment.this.getLayoutInflater().inflate(R.layout.description, null));
					infoPopover.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.3), (int) (Globals.getDeviceWidth() / 1.3));
					TextView btn_info = (TextView) infoPopover.findViewById(R.id.tv_title);
					CircleButton  btn_back =(CircleButton)infoPopover.findViewById(R.id.btn_back);
					btn_back.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							infoPopover.dismiss();
						}
					});
					btn_info.setText(title);
					infoPopover.show();
				}
			});
			holder.img_storeView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (holder.itemView instanceof RelativeLayout && storeShelfhelfAdapter != null) {
						int id = (int) holder.itemView.getTag();
						StoreList Store = storeList(id);
						updateSharedPreferencesForClient(Store.getInAppId());
						if (Store.getChildobjlist().size() > 0 && Store.getSubClientsList().size() > 0) {
							visitedStore = Store;
							if (checkForStoreinPrev()) {
								addingSelectedStoreList();
								//	lv_recentStores.invalidateViews();
								Intent storeViewIntent = new Intent(StoreTabViewFragment.this, StoreViewActivity.class);
								storeViewIntent.putExtra("visitedStore", visitedStore);
								storeViewIntent.putExtra("gridShelf", gridShelf);
								storeViewIntent.putExtra("recentList", recent_storeListData);
								storeViewIntent.putExtra("isFav", checkIsFav(String.valueOf(visitedStore.getClientID())));
								startActivityForResult(storeViewIntent, storeViewActivityCode);
							} else {
								prevStoreListData.add(storeListData);
								//	Store_list.setAdapter(new storeListAdapter(Store.getSubClientsList(),true));
								storeListData = Store.getSubClientsList();
								shelfTitleList.clear();
								shelfTitleList.add(StoreTabViewFragment.this.getResources().getString(R.string.all));
								storeShelfhelfAdapter.notifyDataSetChanged();
								//	addingSelectedStoreList();
								//	lv_recentStores.invalidateViews();
							}

						} else if (Store.getChildobjlist().size() > 0 && Store.getSubClientsList().size() == 0) {
							visitedStore = Store;
							addingSelectedStoreList();
							//	lv_recentStores.invalidateViews();
							Intent storeViewIntent = new Intent(StoreTabViewFragment.this, StoreViewActivity.class);
							storeViewIntent.putExtra("visitedStore", visitedStore);
							storeViewIntent.putExtra("gridShelf", gridShelf);
							storeViewIntent.putExtra("recentList", recent_storeListData);
							storeViewIntent.putExtra("isFav", checkIsFav(String.valueOf(visitedStore.getClientID())));
							startActivityForResult(storeViewIntent, storeViewActivityCode);
						} else if (Store.getChildobjlist().size() == 0 && Store.getSubClientsList().size() > 0) {
							visitedStore = Store;
							prevStoreListData.add(storeListData);
							//	Store_list.setAdapter(new storeListAdapter(Store.getSubClientsList(),false));
							storeListData = Store.getSubClientsList();
							shelfTitleList.clear();
							shelfTitleList.add(StoreTabViewFragment.this.getResources().getString(R.string.all));
							storeShelfhelfAdapter.notifyDataSetChanged();
							//	addingSelectedStoreList();
							//	lv_recentStores.invalidateViews();
						} else if (Store.getChildobjlist().size() == 0 && Store.getSubClientsList().size() == 0) {
							UserFunctions.alert(getString(R.string.no_stores), StoreTabViewFragment.this);
						}
						if ( Store.getChildobjlist().size() != 0){
							Bundle bundle = new Bundle();
							String itemId = String.valueOf(Store.getClientID());
							String name = Store.getNameEn();
							bundle.putString(FirebaseAnalytics.Param.ITEM_ID, itemId);
							bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
							firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
						}
					}
				}
			});
		}

		@Override
		public int getItemCount() {
			return storeList.size();
		}

	}

	private String newWebService(String url) {
		HttpURLConnection urlConnection = null;
		StringBuilder sb = new StringBuilder();
		String returnResponse = "";
		try {
			URL urlToRequest = new URL(url);

			urlConnection = (HttpURLConnection) urlToRequest.openConnection();
			urlConnection.setConnectTimeout(10000);
			urlConnection.setReadTimeout(25000);
			urlConnection.setRequestProperty("accept", "application/json");
			urlConnection.setRequestProperty("Content-Type", "application/json");
			int statusCode = urlConnection.getResponseCode();
			if (statusCode == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
				String line = null;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}

				br.close();
				System.out.println("" + sb.toString());
			} else {
				System.out.println(urlConnection.getResponseMessage());
			}

		} catch (MalformedURLException e) {

		} catch (SocketTimeoutException e) {

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return sb.toString();
	}

	public class callWebserviceForFeaturedList extends AsyncTask<Void, Void, String> {
		String webServiceUrl;
		JSONObject json;
		String jsonData;

		public callWebserviceForFeaturedList(String url) {
			this.webServiceUrl = url;
		}

		@Override
		protected String doInBackground(Void... params) {
			jsonData = newWebService(webServiceUrl);
			jsonData = jsonData.replaceAll("\\\\r\\\\n", "");
			jsonData = jsonData.replace("\"{", "{");
			jsonData = jsonData.replace("}\",", "},");
			jsonData = jsonData.replace("}\"", "}");
			return jsonData;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (jsonData == null || jsonData.equals("")) {
				return;
			}
			JSONArray json = null;
			try {
				json = new JSONArray(jsonData);
				ArrayList<Object> jsonMap = (ArrayList<Object>) toList(json);
				ArrayList<StoreList> storeLists = new ArrayList<>();
				featuredListData = parseJSON(jsonMap, storeLists);
				loadingRecentStoreList();
				loadingFavStoreList();
				initializeAds();
				storeShelfhelfAdapter = new shelfAdapter(shelfTitleList);
				Store_list.setAdapter(storeShelfhelfAdapter);
				if (progressBar.isShown()) {
					progressBar.setVisibility(View.GONE);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
			adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);

			if (!adsPurchased) {
				interstitial = new InterstitialAd(StoreTabViewFragment.this);
				interstitial.setAdUnitId(Globals.INTERSTITIAL_AD_UNIT_ID);
				AdRequest adRequest = new AdRequest.Builder()
						.build();
				interstitial.loadAd(adRequest);
			}
			if (visitedStore != null) {
				StoreDatabase store_database = new StoreDatabase(StoreTabViewFragment.this);
				downloadedBooksArray = store_database.DownloadBooksList();
				downloadingBooksArray = store_database.DownloadingBooksList();
				//	subscription = Subscription.getInstance();

			}
		}

	}

	public class callWebserviceForCategoryList extends AsyncTask<Void, Void, String> {
		String webServiceUrl;
		JSONObject json;
		String jsonData;

		public callWebserviceForCategoryList(String url) {
			this.webServiceUrl = url;
		}

		@Override
		protected String doInBackground(Void... params) {
			jsonData = newWebService(webServiceUrl);
			jsonData = jsonData.replaceAll("\\\\r\\\\n", "");
			jsonData = jsonData.replace("\"{", "{");
			jsonData = jsonData.replace("}\",", "},");
			jsonData = jsonData.replace("}\"", "}");
			return jsonData;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (jsonData == null || jsonData.equals("")) {
				return;
			}
			JSONArray json = null;
			try {
				json = new JSONArray(jsonData);
				ArrayList<Object> jsonMap = (ArrayList<Object>) toList(json);
				storeCategoryList = parseJsonAndGetStoreCategoryList(jsonMap);
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(StoreTabViewFragment.this);
				String selectedCategory = prefs.getString(Globals.selectedCategory, "");
				if (selectedCategory.equals("")) {
					selectAllCategory();
				}
				categoryRecyclerView.setHasFixedSize(true);
				RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
				categoryRecyclerView.setLayoutManager(mLayoutManager);
				categoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
				categoryListAdapter = new NewCategoryListAdapter(storeCategoryList, true,checkBox);
				categoryRecyclerView.setAdapter(categoryListAdapter);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private ArrayList<StoreCategory> parseJsonAndGetStoreCategoryList(ArrayList<Object> jsonList) {
		ArrayList<StoreCategory> storeList = new ArrayList<StoreCategory>();
		for (int i = 0; i < jsonList.size(); i++) {
			HashMap<String, Object> values = (HashMap<String, Object>) jsonList.get(i);
			StoreCategory list = new StoreCategory();
			list.setId((Integer) values.get("ID"));
			list.setActive((Integer) values.get("Active"));
			list.setArbName((String) values.get("ArbName"));
			list.setEngName((String) values.get("EngName"));
			storeList.add(list);
		}
		return storeList;
	}

	public class NewCategoryListAdapter extends RecyclerView.Adapter<NewCategoryListAdapter.MyViewHolder> {

		ArrayList<StoreCategory> storeList = new ArrayList<StoreCategory>();
		boolean addBorder;
		CheckBox select_all;
		public class MyViewHolder extends RecyclerView.ViewHolder {
			CheckBox checkBox;
			TextView tv_categoryName;

			public MyViewHolder(View view) {
				super(view);
				tv_categoryName = (TextView) view.findViewById(R.id.textView1);
				checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
			}
		}


		public NewCategoryListAdapter(ArrayList<StoreCategory> storeList1, boolean add_border,CheckBox checkbox) {
			this.storeList = storeList1;
			this.addBorder = add_border;
			this.select_all=checkbox;
		}

		@Override
		public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.category_list_store, parent, false);

			return new MyViewHolder(itemView);
		}

		@Override
		public void onBindViewHolder(final MyViewHolder holder, int position) {
			if (addBorder) {
				holder.itemView.setBackgroundResource(R.drawable.recycler_border);
			}
			StoreCategory storeCategory = storeList.get(position);
			if (checkCategoryidExist(String.valueOf(storeCategory.getId()))) {
				holder.checkBox.setChecked(true);
			} else {
				holder.checkBox.setChecked(false);
				select_all.setChecked(false);
			}
			holder.itemView.setTag(storeCategory.getId());
			if (language.equals("ar")) {
				holder.tv_categoryName.setText(storeCategory.getArbName());
			} else{
				holder.tv_categoryName.setText(storeCategory.getEngName());
		    }
			holder.itemView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (v instanceof RelativeLayout) {
						CheckBox checkBox = (CheckBox) ((RelativeLayout) v).getChildAt(1);
						if (checkBox.isChecked()) {
							checkBox.setChecked(false);
							selectedCategoryList.remove(String.valueOf(holder.itemView.getTag()));
						} else {
							checkBox.setChecked(true);
							selectedCategoryList.add(String.valueOf(holder.itemView.getTag()));
						}

					}
					if(selectedCategoryList.size()==storeList.size()){
						select_all.setChecked(true);
					}else{
						select_all.setChecked(false);
					}
				}
			});
		}

		@Override
		public int getItemCount() {
			return storeList.size();
		}

	}

	public class callWebserviceForCountryUniversityLanguage extends AsyncTask<Void, Void, String> {
		String webServiceUrl;
		String serviceType;
		JSONObject json;
		String jsonData;

		public callWebserviceForCountryUniversityLanguage(String url, String webServiceType) {
			this.webServiceUrl = url;
			this.serviceType = webServiceType;
		}

		@Override
		protected String doInBackground(Void... params) {
			jsonData = newWebService(webServiceUrl);
			jsonData = jsonData.replaceAll("\\\\r\\\\n", "");
			jsonData = jsonData.replace("\"{", "{");
			jsonData = jsonData.replace("}\",", "},");
			jsonData = jsonData.replace("}\"", "}");
			return jsonData;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (jsonData == null || jsonData.equals("")) {
				return;
			}
			JSONArray json = null;
			try {
				json = new JSONArray(jsonData);
				ArrayList<Object> jsonMap = (ArrayList<Object>) toList(json);
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(StoreTabViewFragment.this);
				if (serviceType.equals("country")) {
					storeCountryList = parseJsonAndGetStoreCountryList(jsonMap);
				} else if (serviceType.equals("university")) {
					storeUniversityList = parseJsonAndGetStoreUniversityList(jsonMap);
					String selectedUniversity = prefs.getString(Globals.selectedCUniversity, "");
					if (selectedUniversity.equals("")) {
						selectAllUniversity();
					}
				} else if (serviceType.equals("language")) {
					storeLanguageList = parseJsonAndGetStoreCategoryList(jsonMap);
					String selectedLanguage = prefs.getString(Globals.selectedlanguage, "");
					if (selectedLanguage.equals("")) {
						selectAllLanguages();
					}
					isSaveBtnClickable = true;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private ArrayList<StoreCountry> parseJsonAndGetStoreCountryList(ArrayList<Object> jsonList) {
		ArrayList<StoreCountry> storeList = new ArrayList<StoreCountry>();
		for (int i = 0; i < jsonList.size(); i++) {
			HashMap<String, Object> values = (HashMap<String, Object>) jsonList.get(i);
			StoreCountry list = new StoreCountry();
			list.setId((Integer) values.get("ID"));
			list.setActive((Integer) values.get("Active"));
			list.setArbName((String) values.get("ArbName"));
			list.setEngName((String) values.get("EngName"));
			list.setFlagPath((String) values.get("FlagPath"));
			storeList.add(list);
		}
		return storeList;
	}

	private ArrayList<StoreUniversity> parseJsonAndGetStoreUniversityList(ArrayList<Object> jsonList) {
		ArrayList<StoreUniversity> storeList = new ArrayList<StoreUniversity>();
		for (int i = 0; i < jsonList.size(); i++) {
			HashMap<String, Object> values = (HashMap<String, Object>) jsonList.get(i);
			ArrayList<Object> subChildList = (ArrayList<Object>) values.get("UniversityList");
			for (int sub = 0; sub < subChildList.size(); sub++) {
				HashMap<String, Object> hashMap = (HashMap<String, Object>) subChildList.get(sub);
				StoreUniversity list = new StoreUniversity();
				list.setId((Integer) hashMap.get("ID"));
				list.setActive((Integer) hashMap.get("Active"));
				list.setArbName((String) hashMap.get("ArbName"));
				list.setEngName((String) hashMap.get("EngName"));
				list.setCountryId((Integer) hashMap.get("CountryID"));
				storeList.add(list);
			}
		}
		return storeList;
	}

	public class NewUniversityListAdapter extends RecyclerView.Adapter<NewUniversityListAdapter.MyViewHolder> {

		ArrayList<StoreUniversity> storeList = new ArrayList<StoreUniversity>();
		RecyclerView country_Recycler;

		public class MyViewHolder extends RecyclerView.ViewHolder {
			CheckBox checkBox;
			TextView tv_categoryName;

			public MyViewHolder(View view) {
				super(view);
				tv_categoryName = (TextView) view.findViewById(R.id.textView1);
				checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
			}
		}


		public NewUniversityListAdapter(ArrayList<StoreUniversity> storeList1, boolean addSubClient, RecyclerView country_rcycler) {
			this.storeList = storeList1;
			country_Recycler = country_rcycler;
		}

		@Override
		public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.store_check_list, parent, false);

			return new MyViewHolder(itemView);
		}

		@Override
		public void onBindViewHolder(final MyViewHolder holder, int position) {
			StoreUniversity storeCategory = storeList.get(position);
			if (checkUniversityidExist(String.valueOf(storeCategory.getId()))) {
				holder.checkBox.setChecked(true);
			} else {
				holder.checkBox.setChecked(false);
			}
			holder.itemView.setTag(storeCategory.getId());
			if (language.equals("ar")) {
				holder.tv_categoryName.setText(storeCategory.getArbName());
			}else {
				holder.tv_categoryName.setText(storeCategory.getEngName());
			}
			holder.itemView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (holder.checkBox.isChecked()) {
						holder.checkBox.setChecked(false);
						selectedUniversityList.remove(String.valueOf(holder.itemView.getTag()));
					} else {
						holder.checkBox.setChecked(true);
						selectedUniversityList.add(String.valueOf(holder.itemView.getTag()));
					}
					country_Recycler.getAdapter().notifyDataSetChanged();
				}
			});
		}

		@Override
		public int getItemCount() {
			return storeList.size();
		}

	}

	private StoreList storeList(int id) {
		StoreList selectedStore = null;
		for (int i = 0; i < storeListData.size(); i++) {
			selectedStore = storeListData.get(i);
			if (selectedStore.getClientID() == id) {
				return selectedStore;
			}
		}
		for (int i = 0; i < featuredListData.size(); i++) {
			selectedStore = featuredListData.get(i);
			if (selectedStore.getClientID() == id) {
				return selectedStore;
			}
		}
		for (int i = 0; i < recent_storeListData.size(); i++) {
			selectedStore = recent_storeListData.get(i);
			if (selectedStore.getClientID() == id) {
				return selectedStore;
			}
		}
		for (int i = 0; i < fav_StoreList.size(); i++) {
			selectedStore = fav_StoreList.get(i);
			if (selectedStore.getClientID() == id) {
				return selectedStore;
			}
		}
		return selectedStore;
	}

	private boolean isViewExpanded(int position) {
		boolean expand = false;
		for (int i = 0; i < expandList.size(); i++) {
			if (position == expandList.get(i)) {
				expand = true;
				break;
			}
		}
		return expand;
	}

	public class NewLanguageListAdapter extends RecyclerView.Adapter<NewLanguageListAdapter.MyViewHolder> {

		ArrayList<StoreCategory> storeList = new ArrayList<StoreCategory>();
		boolean addBorder;
		CheckBox checkBox_all;

		public class MyViewHolder extends RecyclerView.ViewHolder {
			CheckBox checkBox;
			TextView tv_categoryName;

			public MyViewHolder(View view) {
				super(view);
				tv_categoryName = (TextView) view.findViewById(R.id.textView1);
				checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
			}
		}


		public NewLanguageListAdapter(ArrayList<StoreCategory> storeList1, boolean add_border, CheckBox checkBox) {
			this.storeList = storeList1;
			this.addBorder = add_border;
			this.checkBox_all = checkBox;

		}

		@Override
		public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.store_check_list, parent, false);

			return new MyViewHolder(itemView);
		}

		@Override
		public void onBindViewHolder(final MyViewHolder holder, int position) {
			if (addBorder) {
				//	holder.itemView.setBackgroundResource(R.drawable.recycler_border);
			}
			StoreCategory storeCategory = storeList.get(position);
			if (checkLanguageidExist(String.valueOf(storeCategory.getId()))) {
				holder.checkBox.setChecked(true);
			} else {
				holder.checkBox.setChecked(false);
			}
			holder.itemView.setTag(storeCategory.getId());
			if (language.equals("ar")) {
				holder.tv_categoryName.setText(storeCategory.getArbName());
			}else {
				holder.tv_categoryName.setText(storeCategory.getEngName());
			}
			holder.itemView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (v instanceof RelativeLayout) {
						CheckBox checkBox = (CheckBox) ((RelativeLayout) v).getChildAt(1);
						if (checkBox.isChecked()) {

							checkBox.setChecked(false);
							selectedLanguageList.remove(String.valueOf(holder.itemView.getTag()));
						} else {
							checkBox.setChecked(true);
							selectedLanguageList.add(String.valueOf(holder.itemView.getTag()));
						}
						if (selectedLanguageList.size() == storeList.size()) {
							checkBox_all.setChecked(true);
						} else {
							checkBox_all.setChecked(false);
						}
					}
				}
			});
		}

		@Override
		public int getItemCount() {
			return storeList.size();
		}
	}

	private void getSelectedUniversityList() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String str = prefs.getString(Globals.selectedCUniversity, "");
		String[] array = str.split(",");
		for (int i = 0; i < array.length; i++) {
			if (!array[i].equals("")) {
				selectedUniversityList.add(array[i]);
			}
		}
	}

	private void getSelectedCategoryList() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String str = prefs.getString(Globals.selectedCategory, "");
		String[] array = str.split(",");
		for (int i = 0; i < array.length; i++) {
			if (!array[i].equals("")) {
				selectedCategoryList.add(array[i]);
			}
		}
	}

	private void getSelectedLanguageList() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String str = prefs.getString(Globals.selectedlanguage, "");
		String[] array = str.split(",");
		for (int i = 0; i < array.length; i++) {
			if (!array[i].equals("")) {
				selectedLanguageList.add(array[i]);
			}
		}
	}

	private void addingCountryList() {
		for (int i = 0; i < selectedUniversityList.size(); i++) {
			String id = selectedUniversityList.get(i);
			for (int i1 = 0; i1 < storeUniversityList.size(); i1++) {
				StoreUniversity university = storeUniversityList.get(i1);
				String univId = String.valueOf(university.getId());
				if (id.equals(univId)) {
					if (!checkCountryIdExist(String.valueOf(university.getCountryId()))) {
						selectedCountryList.add(String.valueOf(university.getCountryId()));
						break;
					}
				}
			}
		}
	}

	private boolean checkCountryIdExist(String id) {
		for (int i = 0; i < selectedCountryList.size(); i++) {
			String countryId = selectedCountryList.get(i);
			if (id.equals(countryId)) {
				return true;
			}
		}
		return false;
	}

	private boolean checkUniversityidExist(String id) {
		for (int i = 0; i < selectedUniversityList.size(); i++) {
			String countryId = selectedUniversityList.get(i);
			if (id.equals(countryId)) {
				return true;
			}
		}
		return false;
	}

	private boolean checkLanguageidExist(String id) {
		for (int i = 0; i < selectedLanguageList.size(); i++) {
			String countryId = selectedLanguageList.get(i);
			if (id.equals(countryId)) {
				return true;
			}
		}
		return false;
	}

	private boolean checkCategoryidExist(String id) {
		for (int i = 0; i < selectedCategoryList.size(); i++) {
			String countryId = selectedCategoryList.get(i);
			if (id.equals(countryId)) {
				return true;
			}
		}
		return false;
	}

	private boolean checkIsFav(String id) {
		for (int i = 0; i < fav_StoreList.size(); i++) {
			StoreList storeList = fav_StoreList.get(i);
			if (id.equals(storeList.getClientID() + "")) {
				return true;
			}
		}
		return false;
	}

	private void selectAllUniversities(String countryId) {
		for (int i = 0; i < storeUniversityList.size(); i++) {
			StoreUniversity university = storeUniversityList.get(i);
			if (countryId.equals(university.getCountryId() + "")) {
				if (!IdExist(String.valueOf(university.getId()), selectedUniversityList)) {
					selectedUniversityList.add(String.valueOf(university.getId()));
				}
			}
		}
	}

	private void selectAllUniversity() {
		for (int i = 0; i < storeUniversityList.size(); i++) {
			StoreUniversity university = storeUniversityList.get(i);
			if (!IdExist(String.valueOf(university.getId()), selectedUniversityList)) {
				selectedUniversityList.add(String.valueOf(university.getId()));
			}
		}
	}

	private void removeAllUniversities(String countryId) {
		for (int i = 0; i < storeUniversityList.size(); i++) {
			StoreUniversity university = storeUniversityList.get(i);
			if (countryId.equals(university.getCountryId() + "")) {
				if (IdExist(String.valueOf(university.getId()), selectedUniversityList)) {
					selectedUniversityList.remove(String.valueOf(university.getId()));
				}
			}
		}
	}

	private boolean IdExist(String id, ArrayList<String> selectedList) {
		for (int i = 0; i < selectedList.size(); i++) {
			String selectedId = selectedList.get(i);
			if (id.equals(selectedId)) {
				return true;
			}
		}
		return false;
	}

	private boolean isSelectedAll(int countryId) {
		ArrayList<String> universityList = new ArrayList<>();
		ArrayList<String> selectList = new ArrayList<>();
		for (int i = 0; i < storeUniversityList.size(); i++) {
			StoreUniversity university = storeUniversityList.get(i);
			if (university.getCountryId() == countryId) {
				universityList.add(String.valueOf(university.getId()));
			}
		}
		for (int i1 = 0; i1 < universityList.size(); i1++) {
			String univId = universityList.get(i1);
			for (int i2 = 0; i2 < selectedUniversityList.size(); i2++) {
				String selectedId = selectedUniversityList.get(i2);
				if (univId.equals(selectedId)) {
					selectList.add(selectedId);
				}
			}
		}
		if (universityList.size() == selectList.size()) {
			return true;
		} else {
			return false;
		}
	}

	private void selectAllLanguages() {
		for (int i = 0; i < storeLanguageList.size(); i++) {
			StoreCategory category = storeLanguageList.get(i);
			if (!IdExist(String.valueOf(category.getId()), selectedLanguageList)) {
				selectedLanguageList.add(String.valueOf(category.getId()));
			}
		}
	}

	private void removeAllLanguages() {
		for (int i = 0; i < storeLanguageList.size(); i++) {
			StoreCategory category = storeLanguageList.get(i);
			if (IdExist(String.valueOf(category.getId()), selectedLanguageList)) {
				selectedLanguageList.remove(String.valueOf(category.getId()));
			}
		}
	}

	private void selectAllCategory() {
		for (int i = 0; i < storeCategoryList.size(); i++) {
			StoreCategory category = storeCategoryList.get(i);
			if (!IdExist(String.valueOf(category.getId()), selectedCategoryList)) {
				selectedCategoryList.add(String.valueOf(category.getId()));
			}
		}
	}

	private void removeAllCategory() {
		for (int i = 0; i < storeCategoryList.size(); i++) {
			StoreCategory category = storeCategoryList.get(i);
			if (IdExist(String.valueOf(category.getId()), selectedCategoryList)) {
				selectedCategoryList.remove(String.valueOf(category.getId()));
			}
		}
	}

	private void saveClicked() {
		storingSelectedUniversity();
		storingSelectedLanguage();
		addingCountryList();
		storingSelectedCountry();
		storingSelectedCategory();
		progressBar.setVisibility(View.VISIBLE);
		prevStoreListData.clear();
		shelfTitleList.clear();
		shelfTitleList.add(this.getResources().getString(R.string.all));
		shelfTitleList.add(this.getResources().getString(R.string.favourite));
		shelfTitleList.add(this.getResources().getString(R.string.RECENT));
		shelfTitleList.add(this.getResources().getString(R.string.FEATURED));
		storeListData.clear();
		featuredListData.clear();
		recent_storeListData.clear();
		fav_StoreList.clear();
		storeShelfhelfAdapter.notifyDataSetChanged();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(StoreTabViewFragment.this);
		String selectedUniversity = prefs.getString(Globals.selectedCUniversity, "");
		String selectedCategory = prefs.getString(Globals.selectedCategory, "");
		String selectedLanguage = prefs.getString(Globals.selectedlanguage, "");
		String selectedCountry = prefs.getString(Globals.selectedCountry, "");
		String featuredUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetClientFeaturedList?UserId=1&Country=," + selectedCountry + "&University=," + selectedUniversity + "&Category=," + selectedCategory + "&Language=," + selectedLanguage + "";
		String exploredUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetClientStoreListByUserPreferences?UserId=1&Country=," + selectedCountry + "&University=," + selectedUniversity + "&Category=," + selectedCategory + "&Language=," + selectedLanguage + "";
		new callWebserviceForExploreList(exploredUrl).execute();
		new callWebserviceForFeaturedList(featuredUrl).execute();
	}

	private void updateSharedPreferencesForClient(String productID) {
		SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(StoreTabViewFragment.this);
		SharedPreferences.Editor editor = sharedPreference.edit();
		ArrayList<HashMap<String, String>> existList = null;
		try {
			existList = (ArrayList<HashMap<String, String>>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.SUBSLIST, ObjectSerializer.serialize(new ArrayList<HashMap<String, String>>())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (UserFunctions.checkClientIdExist(existList, productID)) {
			HashMap<String, String> hashMap = getHashMap(existList, productID);
			editor.putString(Globals.SUBSCRIPTENDDATEKEY, hashMap.get("EndDate"));
			editor.putString(Globals.SUBSCRIPTDAYSLEFT, hashMap.get("daysLeft"));
			editor.commit();
		}else if (UserFunctions.checkRedeemIdExist(productID)){
			editor.putString(Globals.SUBSCRIPTENDDATEKEY, UserFunctions.getRedeemInAppIdEndDate(productID));
			editor.putString(Globals.SUBSCRIPTDAYSLEFT, UserFunctions.getDaysleft(productID));
			editor.commit();
		} else {
			editor.putString(Globals.SUBSCRIPTENDDATEKEY, "");
			editor.putString(Globals.SUBSCRIPTDAYSLEFT, "0");
			editor.commit();
		}
	}

	private HashMap<String, String> getHashMap(ArrayList<HashMap<String, String>> existList, String inAppId) {
		HashMap<String, String> hashMap = null;
		for (int i = 0; i < existList.size(); i++) {
			hashMap = existList.get(i);
			if (hashMap.get("AppstoreID").equals(inAppId)) {
				return hashMap;
			}
		}
		return hashMap;
	}

	private void initializeAds() {
		SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
		adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);
		if (!adsPurchased) {
			interstitial = new InterstitialAd(StoreTabViewFragment.this);
			interstitial.setAdUnitId(Globals.INTERSTITIAL_AD_UNIT_ID);
			adView = (AdView) findViewById(R.id.adView);
			AdRequest adRequest = new AdRequest.Builder()
					//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
					//.addTestDevice("505F22CBB28695386696655EED14B98B")
					.build();
			adView.loadAd(adRequest);
			adView.setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					super.onAdLoaded();
					if (!groups.getBannerAdsVisible()) {
						adView.setVisibility(View.VISIBLE);
					}
				}
			});
			if (interstitial.isLoaded() && !groups.getInterstitialAdsVisible()) {
				interstitial.show();
			}
			interstitial.loadAd(adRequest);
		}
	}

	private boolean checkForNoneSelected() {
		if (selectedUniversityList.size() == 0) {
			UserFunctions.DisplayAlertDialog(StoreTabViewFragment.this, R.string.select_university, R.string.warning);
			return false;
		} else if (selectedCategoryList.size() == 0) {
			UserFunctions.DisplayAlertDialog(StoreTabViewFragment.this, R.string.select_one_category, R.string.warning);
			return false;
		} else if (selectedLanguageList.size() == 0) {
			UserFunctions.DisplayAlertDialog(StoreTabViewFragment.this, R.string.select_language, R.string.warning);
			return false;
		} else {
			return true;
		}
	}

	private void loadStore() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(StoreTabViewFragment.this);
		String userId = prefs.getString(Globals.sUserIdKey, "");
		if (userId.equals("")){
			userId = "0";
		}
		String selectedUniversity = prefs.getString(Globals.selectedCUniversity, "");
		String selectedCategory = prefs.getString(Globals.selectedCategory, "");
		String selectedLanguage = prefs.getString(Globals.selectedlanguage, "");
		String selectedCountry = prefs.getString(Globals.selectedCountry, "");
		String featuredUrl;
		String exploredUrl;
		if (selectedUniversity.equals("") || selectedCategory.equals("") || selectedLanguage.equals("")) {
			featuredUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetClientFeaturedList?UserId="+userId+"&Country=&University=&Category=&Language=";
			exploredUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetClientStoreListByUserPreferences?UserId="+userId+"&Country=&University=&Category=&Language=";
		} else {
			featuredUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetClientFeaturedList?UserId="+userId+"&Country=," + selectedCountry + "&University=," + selectedUniversity + "&Category=," + selectedCategory + "&Language=," + selectedLanguage + "";
			exploredUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetClientStoreListByUserPreferences?UserId="+userId+"&Country=," + selectedCountry + "&University=," + selectedUniversity + "&Category=," + selectedCategory + "&Language=," + selectedLanguage + "";
		}
		String categoryListUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetCategoryActiveList";
		String countryUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetCountryActiveList";
		String universityUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetCountryUniversityList";
		String languageUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetLanguageActiveList";
		new callWebserviceForExploreList(exploredUrl).execute();
		new callWebserviceForFeaturedList(featuredUrl).execute();
		new callWebserviceForCategoryList(categoryListUrl).execute();
		new callWebserviceForCountryUniversityLanguage(countryUrl, "country").execute();
		new callWebserviceForCountryUniversityLanguage(universityUrl, "university").execute();
		new callWebserviceForCountryUniversityLanguage(languageUrl, "language").execute();
	}
	private boolean canExpand(int position) {
		boolean expand = false;
		if (position == 0) {
			if (storeListData.size() > 2) {
				expand = true;
			}
		} else if (position == 1) {
			if (fav_StoreList.size() > 2) {
				expand = true;
			}
		} else if (position == 2) {
			if (recent_storeListData.size() > 2) {
				expand = true;
			}
		} else if (position == 3) {
			if (featuredListData.size() > 2) {
				expand = true;
			}
		}
		return expand;
	}
}










