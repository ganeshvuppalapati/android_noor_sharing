package com.semanoor.manahij;

/**
 * Created by karthik on 19-08-2016.
 */
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
final class UT {  private UT() {}

    private static final String L_TAG = "_X_";

    static final String MYROOT = "DEMORoot";
    static final String MIME_TEXT = "text/plain";
    static final String MIME_FLDR = "application/vnd.google-apps.folder";

    static final String TITL = "titl";
    static final String GDID = "gdid";
    static final String MIME = "mime";

    private static final String TITL_FMT = "yyMMdd-HHmmss";

    private static SharedPreferences pfs;
    static Context acx;
    static void init(Context ctx) {
        acx = ctx.getApplicationContext();
        pfs = PreferenceManager.getDefaultSharedPreferences(acx);
    }

    static class AM { private AM(){}
        private static final String ACC_NAME = "account_name";
        private static String mEmail = null;

        static void setEmail(String email) {
            UT.pfs.edit().putString(ACC_NAME, (mEmail = email)).apply();
        }
        static String getEmail() {
            return mEmail != null ? mEmail : (mEmail = UT.pfs.getString(ACC_NAME, null));
        }
    }


    static byte[] is2Bytes(InputStream is) {
        byte[] buf = null;
        BufferedInputStream bufIS = null;
        if (is != null) try {
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
            bufIS = new BufferedInputStream(is);
            buf = new byte[4096];
            int cnt;
            while ((cnt = bufIS.read(buf)) >= 0) {
                byteBuffer.write(buf, 0, cnt);
            }
            buf = byteBuffer.size() > 0 ? byteBuffer.toByteArray() : null;
        } catch (Exception ignore) {}
        finally {
            try {
                if (bufIS != null) bufIS.close();
            } catch (Exception ignore) {}
        }
        return buf;
    }

    static void le(Throwable ex) {
        Log.e(L_TAG, Log.getStackTraceString(ex));
    }
    static void lg(String msg) {  Log.d(L_TAG, msg); }
}