package com.semanoor.manahij.mqtt.datamodels;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by semanoor on 19/04/17.
 */

public class TopicsItem implements Serializable {

    String topicName;
    ArrayList<String> messages = new ArrayList<>();
    int unseenMsgCount;
    String lastUpdatedTime;

    public TopicsItem(String topicName, ArrayList<String> messages, int unseenMsgCount, String lastUpdatedTime) {
        this.topicName = topicName;
        this.messages.addAll(messages);
        this.lastUpdatedTime = lastUpdatedTime;
        this.unseenMsgCount = unseenMsgCount;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public ArrayList<String> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<String> messages) {
        this.messages = messages;
    }

    public int getUnseenMsgCount() {
        return unseenMsgCount;
    }

    public void setUnseenMsgCount(int unseenMsgCount) {
        this.unseenMsgCount = unseenMsgCount;
    }

    public void addMessage(String message) {
        messages.add(message);
    }

    public void updateUnseenMsgCount() {
        unseenMsgCount = unseenMsgCount + 1;
    }

    public void resetUnseenMsgCount() {
        unseenMsgCount = 0;
    }

    public String getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(String lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TopicsItem that = (TopicsItem) o;

        return topicName.equals(that.topicName);

    }

    @Override
    public int hashCode() {
        return topicName.hashCode();
    }
}
