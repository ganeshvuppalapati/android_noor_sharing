package com.semanoor.manahij;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.ptg.views.CircleButton;
import com.semanoor.fragments.viewpager.PagerAdapter;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.sboookauthor_store.RenderHTMLObjects;
import com.semanoor.sboookauthor_store.StoreDatabase;
import com.semanoor.source_sboookauthor.DownloadBackground;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.util.ArrayList;
import java.util.Locale;

public class StoreViewActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener{

    public ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    TabLayout tabLayout;
    public StoreList visitedStore;
    public GridShelf gridShelf;
    public String language;
    public ArrayList<String> downloadedBooksArray;
    public ArrayList<String> downloadingBooksArray;
    public IabHelper mHelper;
    public Subscription subscription;
    public DownloadBackground background;
    ArrayList<StoreList> recent_storeListData;
    ListView lv_recentStores;
    DrawerLayout drawerLayout;
    public RenderHTMLObjects webView;
    public ArrayList<String> updateLists = new ArrayList<String>();
    public boolean isFav;
    public FirebaseAnalytics firebaseAnalytics;

    RelativeLayout rl_parentLayout,appBarLayout;
    String themesPath =  Globals.TARGET_BASE_FILE_PATH+"Themes/current/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        loadLocale();
        setContentView(R.layout.activity_store_view);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/FiraSans-Regular.otf");
        ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();
        UserFunctions.changeFont(rootView, font);
        subscription = Subscription.getInstance();
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        firebaseAnalytics.setMinimumSessionDuration(20000);
        StoreDatabase store_database = new StoreDatabase(StoreViewActivity.this);
        downloadedBooksArray = store_database.DownloadBooksList();
        downloadingBooksArray = store_database.DownloadingBooksList();
        recent_storeListData = (ArrayList<StoreList>) getIntent().getSerializableExtra("recentList");
        visitedStore = (StoreList) getIntent().getSerializableExtra("visitedStore");
        gridShelf = (GridShelf) getIntent().getSerializableExtra("gridShelf");
        isFav = getIntent().getBooleanExtra("isFav",false);
        mViewPager = (ViewPager) super.findViewById(R.id.viewPager);
        mViewPager.setOnPageChangeListener(this);
        intialiseViewPager(mViewPager);
        rl_parentLayout=(RelativeLayout) findViewById(R.id.layout);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        appBarLayout=(RelativeLayout) findViewById(R.id.appBarLayout);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        UserFunctions.applyingBackgroundImage(themesPath+"store_background.png",rl_parentLayout, StoreViewActivity.this);
        UserFunctions.applyingBackgroundImage(themesPath+"store_background.png",appBarLayout, StoreViewActivity.this);
        tabLayout.setupWithViewPager(this.mViewPager);
        setUpIcon();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        lv_recentStores = (ListView) findViewById(R.id.PagelistView);
        if (checkInternetConnection()) {
            RecentstoreAdapter storeAdapter = new RecentstoreAdapter(StoreViewActivity.this,recent_storeListData);
            lv_recentStores.setAdapter(storeAdapter);
        }
        lv_recentStores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StoreList Store = recent_storeListData.get(position);
                if (!Store.getNameEn().equals(visitedStore.getNameEn())) {
                    visitedStore = recent_storeListData.get(position);
                    if (visitedStore != null) {
                        if (updateLists.size()>0){
                            updateLists.clear();
                        }
                        for (int i = 0;i<visitedStore.getChildobjlist().size();i++){
                            updateLists.add(String.valueOf(i));
                        }
                        intialiseViewPager(mViewPager);
                        tabLayout.setupWithViewPager(mViewPager);
                        setUpIcon();
                        drawerLayout.closeDrawer(GravityCompat.START);
                    }
                }
            }
        });
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);

    }

    private View createTabView(Context context, String textId, int resid, final int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        TextView tv = (TextView) view.findViewById(R.id.textView1);
        CircleButton iv = (CircleButton) view.findViewById(R.id.imageView1);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(position);
                //SubStoreList sub_store = visitedStore.getChildobjlist().get(position);

            }
        });
        if (position==0){
            tv.setTextColor(getResources().getColor(R.color.hovercolor));
            iv.setColor(getResources().getColor(R.color.hovercolor));
        }
        tv.setText(textId);
        iv.setImageResource(resid);
        return view;
    }
    public void intialiseViewPager(ViewPager pager) {
        this.mPagerAdapter = new PagerAdapter(super.getSupportFragmentManager());
        for (int i = 0; i < visitedStore.getChildobjlist().size(); i++) {
            this.mPagerAdapter.addFragment(Fragment.instantiate(this,Enrichment.class.getName()));
        }
        pager.setAdapter(this.mPagerAdapter);
    }
    public void setUpIcon(){
        int image;
        View tabView;
        for (int i = 0; i < visitedStore.getChildobjlist().size(); i++) {
            int id = i + 1;
            SubStoreList sub_store = visitedStore.getChildobjlist().get(i);
            if (sub_store.getSubStoreEn().equals("Curriculum")) {
                image = R.drawable.s_curriculum;
            } else if (sub_store.getSubStoreEn().equals("E-Lessons") || sub_store.getSubStoreEn().equals("New E-Lessons")|| sub_store.getSubStoreEn().equals("E-Lesson")) {
                image = R.drawable.s_lessons;
            } else if (sub_store.getSubStoreEn().equals("Quiz")) {
                image = R.drawable.s_quiz;
            } else if (sub_store.getSubStoreEn().equals("Rzooom")) {
                image = R.drawable.s_rzoom;
            } else if (sub_store.getSubStoreEn().equals("MindMap")) {
                image = R.drawable.s_mindmap;
            } else if (sub_store.getSubStoreEn().equals("Media")) {
                image = R.drawable.s_audio;
            } else if (sub_store.getSubStoreEn().equals("Video")) {
                image = R.drawable.s_video;
            } else if (sub_store.getSubStoreEn().equals("Resource")) {
                image = R.drawable.s_resources;
            } else {
                image = R.drawable.s_curriculum;
            }
            if (language.equals("en")) {
                tabView = createTabView(this, sub_store.getSubStoreEn(), image,i);
            } else {
                tabView = createTabView(this, sub_store.getSubStoreAr(), image,i);
            }
            tabLayout.getTabAt(i).setCustomView(tabView);
        }
    }
    private void loadLocale() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        language = prefs.getString(Globals.languagePrefsKey, "en");
        changeLang(language);
    }

    /**
     * change language in the application
     *
     * @param language
     */
    private void changeLang(String language) {
        Locale myLocale = new Locale(language);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper != null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Override
    public void finish() {
        setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
        super.finish();
    }
    private boolean checkInternetConnection() {

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        // ARE WE CONNECTED TO THE NET

        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0 ;i<tabLayout.getTabCount();i++){
            View view = tabLayout.getTabAt(i).getCustomView();
            if (position==i){
                if (view!=null) {
                    TextView tv = (TextView) view.findViewById(R.id.textView1);
                    CircleButton iv = (CircleButton) view.findViewById(R.id.imageView1);
                    iv.setColor(getResources().getColor(R.color.hovercolor));
                    tv.setTextColor(getResources().getColor(R.color.hovercolor));
                }
                if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.LEFT)){
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
            }else{
                if (view!=null) {
                    TextView tv = (TextView) view.findViewById(R.id.textView1);
                    CircleButton iv = (CircleButton) view.findViewById(R.id.imageView1);
                    iv.setColor(Color.WHITE);
                    tv.setTextColor(Color.WHITE);
                }
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

