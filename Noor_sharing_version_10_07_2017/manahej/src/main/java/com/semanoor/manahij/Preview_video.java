package com.semanoor.manahij;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by Krishna on 13-08-2015.
 */
public class Preview_video extends Activity {
    MediaController mc;
    String path;
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // WindowManager.LayoutParams lp=new WindowManager.LayoutParams();
      //  Window.window=dialo
       /* View v=getWindow().getDecorView();
        WindowManager.LayoutParams lps=(WindowManager.LayoutParams)v.getLayoutParams();
        lps.x=50;lps.y=20;lps.height=200;

        lps.gravity= Gravity.TOP|Gravity.LEFT;*/
        //lp.dimAmount=0;

        /*DisplayMetrics metricss=getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metricss);
        int width=1000;

        DisplayMetrics metrics=getResources().getDisplayMetrics();
        int Sheight=  (int) (metrics.heightPixels*0.90);
        int Swidth=(int) (metrics.widthPixels*0.50);*/


        setContentView(R.layout.preview_video);
       /* WindowManager.LayoutParams paramds = getWindow().getAttributes();
        paramds.width=width;
        paramds.height=2000;


        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width =1000;
        params.height=1500;
        params.gravity=Gravity.CENTER;

        getWindow().setLayout(Sheight, Swidth);*/

        //getWindow().setContentView(R.layout.preview_video,lp);
        //getWindow().setLayout(R.layout.preview_video,lp);
        path = getIntent().getStringExtra("Path");

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        // getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //RelativeLayout v= (RelativeLayout) findViewById(R.id.layout);
        VideoView iv_preview = (VideoView) findViewById(R.id.videoView);
        mc = new MediaController(this);
        iv_preview.setVideoPath(path);
        iv_preview.setMediaController(mc);
        iv_preview.setZOrderOnTop(false);

        //mc.setForegroundGravity(1000);
        // mc.setPadding(10, 10, Globals.getDeviceIndependentPixels(200, this), (int) this.getResources().getDimension(R.dimen.Image_preview));
       // mc.setScaleX(200);
       // RelativeLayout layout=new RelativeLayout(this);
       // layout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
       // mc.setLayoutParams(layout);
      //  layout.addView(mc);
      //  layout.setPadding(10, 10, 250, 900);
       // ll_layout.addView(mc);

      //  mc.setPaddingRelative(0,0,300,750);
        //mc.setPivotX(100);


        mc.setAnchorView(iv_preview);
        //mc.setForegroundGravity(BIND_ABOVE_CLIENT);

        iv_preview.start();
    }

}

