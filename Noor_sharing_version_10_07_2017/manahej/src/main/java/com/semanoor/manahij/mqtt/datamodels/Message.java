package com.semanoor.manahij.mqtt.datamodels;

/**
 * Created by semanoor on 18/04/17.
 */

public class Message {
    String channel;
    String message;
    String recivedTime;

    public Message(String channel, String message, String recivedTime) {
        this.channel = channel;
        this.message = message;
        this.recivedTime = recivedTime;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRecivedTime() {
        return recivedTime;
    }

    public void setRecivedTime(String recivedTime) {
        this.recivedTime = recivedTime;
    }

    @Override
    public String toString() {
        return message + "\n" + recivedTime;
    }
}
