package com.semanoor.manahij.FontTables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Pushpa on 14/04/17.
 */

public class FontTablesDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    public FontTablesDB(Context context) {
        super(context, "myDB", null, 1);
        //  super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String TBLNOTES = "CREATE TABLE " +
                "FontTable" + "("
                + "ID" + " INTEGER PRIMARY KEY AUTOINCREMENT," + "Name" + " TEXT, " + "vesrion" + " TEXT," + "url" + " TEXT" + ")";
        db.execSQL(TBLNOTES);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + "FontTable");
        onCreate(db);
    }


    public int getCount() {
        int Count = 0;
        SQLiteDatabase db = getReadableDatabase();
        String query = "select count(*) from FontTable ";
        Cursor c = db.rawQuery(query, null);
        if (c != null && c.moveToFirst()) {
            Count = c.getInt(0);
        }
        c.close();
        db.close();
        return Count;
    }

    public boolean addFontTables(String Name, String vesrion, String url) {
        SQLiteDatabase db = getReadableDatabase();
         String mversion = null;
        boolean exists = false;
        try {


            String query = "select * from FontTable where   Name ='" + Name + "' AND vesrion ='" + vesrion + "'";

            Cursor c = db.rawQuery(query, null);
            String query2 = "select * from FontTable where   Name ='" + Name + "'";

            Cursor c2 = db.rawQuery(query2, null);
            exists = c.moveToFirst();
            boolean exists2 = c2.moveToFirst();
            c.close();
            c2.close();
            if (!exists) {


                if (exists2) {
                    ContentValues values = new ContentValues();
                    values.put("Name", Name);
                    values.put("vesrion", vesrion);
                    values.put("url", url);
                    //SQLiteDatabase db = this.getWritableDatabase();
                    db.update("FontTable", values, "Name" + " = ?", new String[]{Name});
                    db.close();
                } else {
                    ContentValues values = new ContentValues();
                    values.put("Name", Name);
                    values.put("vesrion", vesrion);
                    values.put("url", url);
                    //SQLiteDatabase db = this.getWritableDatabase();
                    db.insert("FontTable", null, values);
                    db.close();
                }
            }
        } catch (Exception e) {

        }
        return exists;
    }


    /*public FontTable getVersion(String name, String vesrion, String url) {
        String mversion = null;
        SQLiteDatabase db = getReadableDatabase();
        String query = "select * from FontTable where   Name ='" + name + "'";
        Cursor c = db.rawQuery(query, null);
        FontTable mydata = new FontTable();
        boolean exists = c.moveToFirst();
        mydata.setExits(exists);
        if (exists) {
            c.moveToFirst();
            if (c.moveToFirst()) {
                do {
                    mversion = c.getString(c.getColumnIndex("vesrion"));
                    mydata.setVesrion(mversion);
                } while (c.moveToNext());
            }
        } else {
            ContentValues values = new ContentValues();
            values.put("Name", name);
            values.put("vesrion", vesrion);
            values.put("url", url);
            db.insert("FontTable", null, values);
            db.close();
        }

        return mydata;
    }*/

    public void adddata(String name, String vesrion, String url) {
        SQLiteDatabase db = getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("Name", name);
        values.put("vesrion", vesrion);
        values.put("url", url);
        db.insert("FontTable", null, values);
        db.close();
    }

    public void upDateVersion(String name, String version, String url) {
        SQLiteDatabase db = getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("Name", name);
        values.put("vesrion", version);
        values.put("url", url);
        //SQLiteDatabase db = this.getWritableDatabase();
        db.update("FontTable", values, "Name" + " = ?", new String[]{name});
        db.close();
    }

    public boolean isFontAvailable(String fileName) {


        return true;
    }
}
