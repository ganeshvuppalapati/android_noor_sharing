package com.semanoor.manahij;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ptg.views.CircleButton;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.FileImageAdapter;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MediaAlbumCreatorActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 101;
    public int CAPTURE_GALLERY = 1001;
    public int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1002;
    private Uri selectedGalleryImageUri;
    private String albumPath, albumImgPath, albumThumbPath, currentFilePath;
    private String[] listImgThumbFiles;
    private GridView gridView;
    private GridAdapter gridAdapter;
    private EditText etAlbumName;
    private GridShelf gridShelf;
    private DatabaseHandler db;
    private Uri imageFileUri;
    public  int MEDIA_TYPE_IMAGE = 1;
    public  int MEDIA_TYPE_VIDEO = 2;
    ArrayList<ClickedFilePath> selectedFiles = new ArrayList<ClickedFilePath>();
    ArrayList<ClickedFilePath> FilesandFolders = new ArrayList<ClickedFilePath>();
    int selectedPage = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_media_album_creator);
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
        ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

        UserFunctions.changeFont(rootView,font);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        db = DatabaseHandler.getInstance(this);
        gridShelf = (GridShelf) getIntent().getSerializableExtra("Shelf");

        albumPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + "album/";
        albumImgPath = albumPath + "org_image/";
        albumThumbPath = albumPath + "thumb_image/";
        if (new File(albumPath).exists()) {
            UserFunctions.deleteAllFilesFromDirectory(new File(albumPath));
        }
        UserFunctions.createNewDirectory(albumPath);
        UserFunctions.createNewDirectory(albumImgPath);
        UserFunctions.createNewDirectory(albumThumbPath);

        listImgThumbFiles = new File(albumThumbPath).list();

        Button btnCamera = (Button) findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(this);
        Button btnPhotoLibrary = (Button) findViewById(R.id.btnPhotoLibrary);
        btnPhotoLibrary.setOnClickListener(this);
//        Button btnUserLibrary = (Button) findViewById(R.id.btnUserLibrary);
//        btnUserLibrary.setOnClickListener(this);
//        Button btnCreateAlbum = (Button) findViewById(R.id.btnCreateAlbum);
//        btnCreateAlbum.setOnClickListener(this);
        etAlbumName = (EditText) findViewById(R.id.editText5);
        CircleButton btnLibrary = (CircleButton) findViewById(R.id.btnLibrary);
        btnLibrary.setOnClickListener(this);
        Button create_btn = (Button) findViewById(R.id.preview_btn);
        create_btn.setOnClickListener(this);
        gridView = (GridView) findViewById(R.id.MyGrid);
        gridAdapter = new GridAdapter();
        gridView.setAdapter(gridAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedPage = position;
                gridAdapter.notifyDataSetChanged();
            }
        });
    }

    private class GridAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return listImgThumbFiles.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
                view = getLayoutInflater().inflate(R.layout.my_media_grid, null);
            }
            String imgPath = albumThumbPath + listImgThumbFiles[position];
            RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.layout);
            ImageView imgView = (ImageView) view.findViewById(R.id.grid_item_image);
            // TextView tv_name= (TextView)view. findViewById(R.id.text);
            Button btn_delete = (Button) view.findViewById(R.id.btn_delete);
            btn_delete.setVisibility(View.GONE);
            Bitmap bitmap = BitmapFactory.decodeFile(imgPath);
            imgView.setImageBitmap(bitmap);
            // tv_name.setText(listImgThumbFiles[position]);
            if (selectedPage != -1 && position == selectedPage) {
//                rl.setBackgroundColor(getResources().getColor(R.color.light_grey));
                btn_delete.setVisibility(View.VISIBLE);
            } else {
                rl.setBackgroundColor(getResources().getColor(R.color.tabTransparent));
                btn_delete.setVisibility(View.GONE);
            }
            btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String path = listImgThumbFiles[position];
                    // String unhiddenPage=albumImgPath[position];
                    AlertDialog.Builder builder = new AlertDialog.Builder(MediaAlbumCreatorActivity.this);
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            renameFileNames(position);
                            dialog.cancel();
                        }
                    });
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.setTitle(R.string.delete);
                    builder.setMessage(R.string.suredelete);
                    builder.show();

                }
            });
            return view;
        }
    }

    private void renameFileNames(int position) {
        String[] files = new File(albumImgPath).list();
        File delete1 = new File(albumImgPath + files[position]);
        delete1.delete();
        File delete = new File(albumThumbPath + listImgThumbFiles[position]);
        delete.delete();
        String[] files1 = new File(albumImgPath).list();
        String[] ThumbFiles = new File(albumThumbPath).list();
        for (int i = position; i < ThumbFiles.length; i++) {
            //int p=i-1;
            int p = i + 1;
            String[] image = files1[i].split("-");
            File page = new File(albumImgPath + files1[i]);
            File newFile1 = new File(albumImgPath + "image-" + p + ".png");
            if (page.exists()) {
                page.renameTo(newFile1);
            }
            String[] imageName = ThumbFiles[i].split("-");
            File f = new File(albumThumbPath + ThumbFiles[i]);
            File newFile = new File(albumThumbPath + imageName[0] + "-" + p + ".png");
            if (f.exists()) {
                f.renameTo(newFile);
            }
        }
        selectedPage = -1;
        listImgThumbFiles = new File(albumThumbPath).list();
        gridView.invalidateViews();
    }

    private void copyImagePickedAndReloadGridView(String pickedSrcImage) {
        File albumFile = new File(albumImgPath);
        String[] listAlbumFiles = albumFile.list();
        int sequenceNo = listAlbumFiles.length + 1;
        //String pickedSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, selectedGalleryImageUri);
        String destImgPath = albumImgPath + "image-" + sequenceNo + ".png";
        UserFunctions.copyFiles(pickedSrcImage, destImgPath);
        Bitmap bitmap = BitmapFactory.decodeFile(pickedSrcImage);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) this.getResources().getDimension(R.dimen.media_grid_image_width), (int) this.getResources().getDimension(R.dimen.media_grid_image_height), false);
        String destThumbImgPath = albumThumbPath + ".t_image-" + sequenceNo + ".png";
        UserFunctions.saveBitmapImage(scaledBitmap, destThumbImgPath);
        listImgThumbFiles = new File(albumThumbPath).list();
        gridAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission Granted
                    String pickedSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, selectedGalleryImageUri);
                    copyImagePickedAndReloadGridView(pickedSrcImage);
                } else {
                    //Permission denied
                }
                return;
            }
        }
    }

    public void requestReadExternalStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            /*if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

			} else {*/
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            //}
        } else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            String pickedSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, selectedGalleryImageUri);
            copyImagePickedAndReloadGridView(pickedSrcImage);
        }
    }

    private void createAlbum() {
        if (listImgThumbFiles.length != 0) {
            if (etAlbumName.getText().toString().trim().length() > 0) {
                gridShelf.createNewAlbum(etAlbumName.getText().toString().trim(), listImgThumbFiles.length, db);
                setResult(RESULT_OK, getIntent().putExtra("IsAlbumCreated", true));
                finish();
            } else {
                UserFunctions.DisplayAlertDialog(this,"Please enter your album name",R.string.enter_album_name);
            }
        } else {
            UserFunctions.DisplayAlertDialog(this,"Please select some images to create an album",R.string.select_image);
        }
    }

    @Override
    public void finish() {
        super.finish();
        UserFunctions.deleteAllFilesFromDirectory(new File(albumPath));
    }

    /**
     *  Checks whether the device supports camera or not.
     */
    private boolean isDeviceSupportCamera(){
        if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return true;
        }else{
            return false;
        }
    }

    /**
     *  Creates path to save camera image
     */
    private File getOutputMediaFile(int type){

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "SboookAuthor");
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("CamCamera", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
        String formattedDate = df.format(c.getTime());

        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +"image_"+ formattedDate + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +"VID_"+ formattedDate + ".mp4");
        }
        else {
            return null;
        }
        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_GALLERY && data != null) {
            if (resultCode == RESULT_OK) {
                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.JELLY_BEAN) {
                    if (data.getClipData() != null) {
                        ClipData mclipData = data.getClipData();
                        for (int i = 0; i < mclipData.getItemCount(); i++) {
                            ClipData.Item item = mclipData.getItemAt(i);
                            Uri uri = item.getUri();
                            selectedGalleryImageUri = uri;
                            String pickedSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, selectedGalleryImageUri);
                            if (Build.VERSION.SDK_INT >= 23) {
                                requestReadExternalStoragePermission();
                            } else {
                                copyImagePickedAndReloadGridView(pickedSrcImage);
                            }
                        }
                    }else if(data.getData()!=null){
                        selectedGalleryImageUri = data.getData();
                        String pickedSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, selectedGalleryImageUri);
                        if (Build.VERSION.SDK_INT >= 23) {
                            requestReadExternalStoragePermission();
                        } else {
                            copyImagePickedAndReloadGridView(pickedSrcImage);
                        }
                    }
                }else {
                    selectedGalleryImageUri = data.getData();
                    String pickedSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, selectedGalleryImageUri);
                    if (Build.VERSION.SDK_INT >= 23) {
                        requestReadExternalStoragePermission();
                    } else {
                        copyImagePickedAndReloadGridView(pickedSrcImage);
                    }
                }

            }
        } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                selectedGalleryImageUri = imageFileUri;
                String pickedSrcImage = UserFunctions.getPathFromGalleryForPickedItem(this, selectedGalleryImageUri);
                if (Build.VERSION.SDK_INT >= 23) {
                    requestReadExternalStoragePermission();
                } else {
                    copyImagePickedAndReloadGridView(pickedSrcImage);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCamera:
                if(!isDeviceSupportCamera()){
                    Toast.makeText(MediaAlbumCreatorActivity.this, "Sorry! Your device doesn't support camera", Toast.LENGTH_LONG).show();
                }else{
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    //System.out.println("The image path: "+this.objPathContent);
                    imageFileUri = Uri.fromFile(getOutputMediaFile(MEDIA_TYPE_IMAGE));
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }
                break;
            case R.id.btnPhotoLibrary:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.JELLY_BEAN){
                    galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
                }
                startActivityForResult(galleryIntent, CAPTURE_GALLERY);
                break;
//            case R.id.btnUserLibrary:
//                addFromFileManager();
//                break;
//            case R.id.btnCreateAlbum:
//                createAlbum();
//                break;
            case R.id.btnLibrary:
                finish();
                break;
            case R.id.preview_btn:
                createAlbum();
            default:

                break;
        }
    }


    private void addFromFileManager() {
        View iv_image = LayoutInflater.from(MediaAlbumCreatorActivity.this).inflate(R.layout.files_manager, null);
        final PopupWindow popup = new PopupWindow();
        popup.setContentView(iv_image);
        popup.setWidth((int) (Globals.getDeviceWidth() / 1.8));
        popup.setHeight((int) (Globals.getDeviceHeight() / 1.8));
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new ColorDrawable(R.color.tabTransparent));
        popup.showAtLocation(iv_image, Gravity.CENTER, 0, 0);
        popup.setOutsideTouchable(false);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                popup.dismiss();
                if (selectedFiles.size() > 0) {
                    selectedFiles.clear();
                }
            }
        });
        final ListView listView = (ListView) iv_image.findViewById(R.id.listView1);
        final Button btn_back = (Button) iv_image.findViewById(R.id.btnBack);
        Button btn_Add = (Button) iv_image.findViewById(R.id.btn_add);
        btn_Add.setVisibility(View.VISIBLE);
        currentFilePath = Globals.TARGET_BASE_FILE_PATH + "UserLibrary";
        btn_back.setTextColor(getResources().getColor(R.color.textColor));
        btn_Add.setTextColor(getResources().getColor(R.color.textColor));
        btn_back.setText("UserLibrary");
        listingFilesAndFolders(currentFilePath);
        RelativeLayout rl_top_layout = (RelativeLayout) iv_image.findViewById(R.id.rl_top_layout);
        rl_top_layout.setBackgroundResource(R.drawable.transparent);
        rl_top_layout.setBackgroundColor(getResources().getColor(R.color.lightColor));
        listView.setAdapter(new FileImageAdapter(MediaAlbumCreatorActivity.this, FilesandFolders, "Image"));
        listView.setDividerHeight(2);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
                ClickedFilePath listedFiles = FilesandFolders.get(position);
                if (listedFiles.isFolderSelected() && !listedFiles.isFile()) {
                    //String[]split=listedFiles.getFolderPath().split("/");
                    currentFilePath = listedFiles.getFolderPath();
                    btn_back.setText(listedFiles.getFolderTitle());
                    listingFilesAndFolders(currentFilePath);
                    listView.setAdapter(new FileImageAdapter(MediaAlbumCreatorActivity.this, FilesandFolders, "Image"));

                } else if (listedFiles.isFile() && !listedFiles.isFolderSelected()) {
                  /* if (listedFiles.isSelected()) {
                        listedFiles.setSelected(false);
                        selectedFiles.remove(listedFiles);
                    } else {
                        listedFiles.setSelected(true);
                        selectedFiles.add(listedFiles);
                    }*/
                    boolean exist = false;
                    for (int i = 0; i < selectedFiles.size(); i++) {
                        ClickedFilePath Files = selectedFiles.get(i);
                        if (Files.getFolderPath().equals(listedFiles.getFolderPath()) && Files.getFolderTitle().equals(listedFiles.getFolderTitle())) {
                            exist = true;
                            listedFiles.setSelected(false);
                            selectedFiles.remove(i);
                        }
                    }
                    if (!exist) {
                        listedFiles.setSelected(true);
                        selectedFiles.add(listedFiles);
                    }
                }
                String text = btn_back.getText().toString();
                if (!btn_back.getText().toString().equals("UserLibrary")) {
                    btn_back.setCompoundDrawablesWithIntrinsicBounds(R.drawable.arrownext, 0, 0, 0);
                } else {
                    btn_back.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
                ((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
            }
        });
        btn_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < selectedFiles.size(); i++) {
                    ClickedFilePath listedFiles = selectedFiles.get(i);
                    copyImagePickedAndReloadGridView(listedFiles.getFolderPath());
                }
                popup.dismiss();
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!currentFilePath.equals(Globals.TARGET_BASE_FILE_PATH + "UserLibrary")) {
                    String filePath = currentFilePath.replace("/" + btn_back.getText(), "");
                    currentFilePath = filePath;
                    String[] split = currentFilePath.split("/");
                    btn_back.setText(split[split.length - 1]);
                    listingFilesAndFolders(currentFilePath);
                    listView.setAdapter(new FileImageAdapter(MediaAlbumCreatorActivity.this, FilesandFolders, "Image"));
                    if (btn_back.getText().toString().equals("UserLibrary")) {
                        btn_back.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }
                }
            }
        });

    }

    private void listingFilesAndFolders(String filePath) {
        File f = new File(filePath);
        File listFiles[] = f.listFiles();
        //selectedFiles = new ArrayList<ClickedFilePath>();
        if (FilesandFolders.size() > 0) {
            FilesandFolders.clear();
        }

        ClickedFilePath file;
        for (int i = 0; i < listFiles.length; i++) {
            String fileName = listFiles[i].getName();
            String Path = listFiles[i].getPath();
            if (listFiles[i].isDirectory()) {
                file = new ClickedFilePath(MediaAlbumCreatorActivity.this);
                file.setFolderSelected(true);
                file.setFolderPath(Path);
                file.setFile(false);
                file.setFolderTitle(fileName);
                FilesandFolders.add(file);
            } else {
                if (!listFiles[i].isHidden()) {
                    //  if (objType.equals("Image")) {
                    //if(listFiles[i].isHidden()) {
                    if (fileName.contains(".jpg") || fileName.contains(".png") || fileName.contains(".gif")) {
                        file = new ClickedFilePath(MediaAlbumCreatorActivity.this);
                        file.setFolderSelected(false);
                        file.setFolderPath(Path);
                        file.setFile(true);
                        file.setFolderTitle(fileName);
                        if (selectedFiles.size() > 0) {
                            for (ClickedFilePath selected : selectedFiles) {
                                if (selected.getFolderPath().equals(Path) && selected.getFolderTitle().equals(fileName)) {
                                    file.setSelected(true);
                                    break;
                                }
                            }
                        } else {
                            file.setSelected(false);
                        }
                        FilesandFolders.add(file);
                    }
                }
            }
        }
    }
}