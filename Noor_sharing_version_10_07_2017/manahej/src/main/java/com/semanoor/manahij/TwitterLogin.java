package com.semanoor.manahij;

import com.semanoor.source_sboookauthor.Globals;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TwitterLogin extends Activity {

    Boolean isLoggedIn;
    private EditText mShareEditText;
    private TextView userName;
    private View loginLayout;
    private View shareLayout;
    private ProgressDialog pDialog;
    private static final String PREF_NAME = "sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String PREF_USER_NAME = "twitter_user_name";
    String selectedText;
    String Url;
    WebView webView;

    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        //Remove title Bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //isLoggedIn=getIntent().getExtras().getBoolean("isLoggedIn");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        isLoggedIn = prefs.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
        selectedText = getIntent().getStringExtra("text");

        setContentView(R.layout.twitter_login);

        final Button btnBack = (Button) findViewById(R.id.btnBack);
        RelativeLayout rl_layout = (RelativeLayout) findViewById(R.id.relativeLayout1);
        webView = (WebView) findViewById(R.id.twitterlogin);

       shareLayout = (LinearLayout) findViewById(R.id.share_layout);
        /* mShareEditText = (EditText) findViewById(R.id.share_text);
        userName = (TextView) findViewById(R.id.user_name);
        final Button btn_shareText = (Button) findViewById(R.id.btn_share);

        btn_shareText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                new updateTwitterStatus().execute(selectedText);

            }
        });

        if (isLoggedIn) {
            //rl_layout.setVisibility(View.VISIBLE);
            btnBack.setText(TwitterLogin.this.getResources().getString(R.string.logout));
            webView.setVisibility(View.GONE);
            shareLayout.setVisibility(View.VISIBLE);
            mShareEditText.setText(selectedText);
            String username = prefs.getString(PREF_USER_NAME, "");
            userName.setText(getResources().getString(R.string.hello)
                    + username);

        } else {*/
            Url = this.getIntent().getStringExtra("extra_url");
            shareLayout.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            //rl_layout.setVisibility(View.VISIBLE);
            btnBack.setText(TwitterLogin.this.getResources().getString(R.string.back));
        //}

        btnBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                     setResult(RESULT_OK, getIntent().putExtra(getString(R.string.twitter_oauth_verifier), "Exit"));
                     setResult(RESULT_OK, getIntent().putExtra("text", selectedText));
                     finish();
            }
        });


        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.clearCache(true);
        webView.clearView();
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {


                if (url.contains(Globals.CALLBACK_URL)) {
                    Uri uri = Uri.parse(url);
                /* Sending results back */

                    String verifier = uri.getQueryParameter(getString(R.string.twitter_oauth_verifier));
                    //setResult(RESULT_OK, getIntent().putExtra("oauth_verifier", verifier));
                    //gettingImage(verifier);
                    Intent resultIntent = getIntent();
                    resultIntent.putExtra(getString(R.string.twitter_oauth_verifier), verifier);
                    //resultIntent.putExtra("oauth_verifier",verifier);
                    //setResult(RESULT_OK, getIntent().putExtra("text", selectedText));
                    resultIntent.putExtra("text", selectedText);
                    setResult(RESULT_OK, resultIntent);
                    //setResult(RESULT_OK, getIntent().putExtra("oauth_verifier", verifier));

				/* closing webview */
                    finish();
                    return true;
                }
                return false;
            }
        });

        //System.out.println("url:"+url);
        webView.loadUrl(Url);
    }

    public void onBackPressed() {
        finish();
    }

    /*class updateTwitterStatus extends AsyncTask<String, String, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(TwitterLogin.this);
            pDialog.setMessage("Posting to twitter...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected Void doInBackground(String... args) {

            String status = args[0];
            try {

                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(Globals.CONSUMER_KEY);
                builder.setOAuthConsumerSecret(Globals.CONSUMER_SECRET);

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(TwitterLogin.this);
                // Access Token
                String access_token = prefs.getString(PREF_KEY_OAUTH_TOKEN, "");
                // Access Token Secret
                String access_token_secret = prefs.getString(PREF_KEY_OAUTH_SECRET, "");

                AccessToken accessToken = new AccessToken(access_token, access_token_secret);
                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

                // Update status
                StatusUpdate statusUpdate = new StatusUpdate(status);
                //InputStream is = getResources().openRawResource(R.drawable.lakeside_view);
                // statusUpdate.setMedia("test.jpg", is);

                twitter4j.Status response = twitter.updateStatus(statusUpdate);

                Log.d("Status", response.getText());

            } catch (TwitterException e) {
                Log.d("Failed to post!", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {


            pDialog.dismiss();

            Toast.makeText(TwitterLogin.this, "Posted to Twitter!", Toast.LENGTH_SHORT).show();

            // Clearing EditText field
            mShareEditText.setText("");
            finish();
        }

    }*/
}
