package com.semanoor.manahij;

import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.ptg.views.CircleButton;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.inappbilling.util.IabResult;
import com.semanoor.inappbilling.util.Inventory;
import com.semanoor.inappbilling.util.Purchase;
import com.semanoor.sboookauthor_store.BookStorage;
import com.semanoor.sboookauthor_store.Category;
import com.semanoor.sboookauthor_store.Country;
import com.semanoor.sboookauthor_store.DownloadCounts;
import com.semanoor.sboookauthor_store.EnrichIAdpater;
import com.semanoor.sboookauthor_store.RenderHTMLObjects;
import com.semanoor.sboookauthor_store.Root;
import com.semanoor.sboookauthor_store.StoreDatabase;
import com.semanoor.sboookauthor_store.University;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DownloadBackground;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Enrichment extends Fragment {
    /** Called when the activity is first created. */

    /**
     * Variables in webServices
     */
    public String soapMessage = "";
    public String ENVELOPE = "";
    public String SOAPAction = "";
    public int serviceType = 0;

    /**
     * Variables in Countries
     */
    //public ArrayList<String> EcountryArray= new ArrayList<String>(); 
    private ArrayList<String> EcountryArrayInSQL = new ArrayList<String>();
    private ArrayList<String> EcountryArrayList = new ArrayList<String>();
    private ArrayList<String> EcountryImagePath = new ArrayList<String>();
    public int ESelectedCountryID = 0;                                                    //To store selected Country ID value
    int EselectedCountryIndex = 0;
    private ArrayList<Country> countryList = new ArrayList<Country>();
    private Country selectedCountry;
    private int defSelectedCountryValue = 1;

    /**
     * Variables in Universities
     */
    //public ArrayList<String> EuniversityArray = new ArrayList<String>();
    private ArrayList<String> EuniversityArrayInSQL = new ArrayList<String>();
    private ArrayList<String> EuniversityArrayList = new ArrayList<String>();
    private ArrayList<String> EuniversityImagePath = new ArrayList<String>();
    public ArrayList<String> filteredEuniversityArray = new ArrayList<String>();
    public int EUniversityIDInt = 0;                                                //used to store the universityID value
    int EselectedPublisherIndex = 0;
    private ArrayList<University> universityList = new ArrayList<University>();
    private ArrayList<University> filteredUniversityList = new ArrayList<University>();
    private University selectedUniversity;
    private int defSelectedUniversityValue = 1;

    /**
     * Variables in Categories
     */
    //public ArrayList<String> EcategoryArray = new ArrayList<String>();
    private ArrayList<String> EcategoryArrayList = new ArrayList<String>();
    //public String ELocalCategoryName="05 - Fifth Grade"; 	//To store the selected Category value 05 - Fifth Grade
    public String ELocalCategoryName = "";
    int EselectedCategoriesIndex = 0;
    private ArrayList<Category> categoryList = new ArrayList<Category>();
    private Category selectedCategory;
    private int defSelectedCategoryValue = 0;

    /**
     * Variables in Price
     */
    private ArrayList<String> EpriceArray = new ArrayList<String>();
    private ArrayList<String> EpriceArrayContent = new ArrayList<String>();
    int EselectedSegmentIndex = 0;                                                    // if = 0 -All, 1-Free , 2-Paid
    int EselectedPriceIndex = 0;

    /**
     * Variables in Language
     */
    private ArrayList<String> ElanguageArray = new ArrayList<String>();
    //int language =1;

    /**
     * Variables in Search
     */
    private ArrayList<String> EsearchFilterArray = new ArrayList<String>();
    private ArrayList<String> EsearchFilterArrayContent = new ArrayList<String>();

    public String ELocalSearchText = "Title";                                        //Contains the search text

    /**
     * Variables in Store Images
     */
    public static ArrayList<String> EstoreBookImageArray = new ArrayList<String>();
    private ArrayList<String> EstoreBookImageInSQL = new ArrayList<String>();

    public ArrayList<String> EbooksOriginal = new ArrayList<String>();                //To store all books details from webservices call
    private ArrayList<String> books = new ArrayList<String>();
    public ArrayList<String> EdownloadedBooksArray = new ArrayList<String>();        //To store all downloaded books
    int EloadBookImagesValue = 0;                                                    //Used to call background thread to call directly processScrollviewImages method
    int EDownloadBookValue = 0;                                                        //Used to call background thread to call directly downloadFile method
    public int EbookClickPosition = 0;
    int EselectCategoryId = 1;  // CID value in BookModal
    View bookmodalView = null;
    int bookModalFlag = 0;

    /**
     * Variables of class instances
     */
    public EnrichIAdpater adapter; //for getting images...
    //public Download_book download_book;
    //public CopyStoreContent copy_store_content;
    //public ManahijActivity shelf;
    public StoreDatabase store_database;

    /**
     * Variables of UI elements
     */
    public ProgressBar progressbar;
    public GridView gridview;
    public Dialog bookModalPopwindow;
    ProgressDialog progDialog, downloadProgDialog, processDialog,consumeProgress;
    Button CountryBtn, UniversityBtn, CategoriesBtn, PriceBtn;
    TextView CountryText, UniversityText, CategoryText, PriceText;
    //TextView  CountryBtnLabel;
    TextView bookmessage, intMesg1, intMesg2, txtHeader;
    //public Toast toast;
    //Search:
    EditText searchEditText;
    ImageView clearSearchBox;
    private String SearchFilterSelected = "Title";
    private Button btnSearch, btnFilter;

    /**
     * Other Variables
     */
    public static int ETagValue = 0;
    String internetOff = "NO";
    public int downloadStatus = 0;
    public String EBookIDCode = "";
    public int BNameCountForSdcard = 0;
    public int EWhenDownloadBook = 0;

    /**
     * PayPal Variables
     */
    ProgressDialog mDialog;
    private static final int request = 2;
    protected static final int INITIALIZE_SUCCESS = 0;
    protected static final int INITIALIZE_FAILURE = 1;
    private static float price = 0;

    int deviceWidth = 0, deviceHeight = 0;
    Point lngPoint, searchPoint, countryPoint, publisherPoint, categoryPoint, pricePoint, gridPoint, storeBGPoint;
    LinearLayout popLayout;
    LinearLayout storeBtmBG, titleTxtLayout;

    private RelativeLayout ll_enrich, topBarLayout;
    public StoreViewActivity fa_enrich;

    public ProgressDialog processPageProgDialog;

    private static boolean rootedDevice;
    private static String ITEM_SKU;

    private oldAndNewCurriculumSoapCall oldCurriculumAsyncTask;
    private oldAndNewCurriculumSoapCall newCurriculumAsyncTask;
    private elessonCurriculumSoapCall elessonCurriculumAsyncTask;
    private quizCurriculumSoapCall quizCurriculumAsynctask;
    private rzooomCurriculumSoapCall rzooomCurriculumAsyncTask;
    private mindmapCurriculumSoapCall mindmapCurriculumAsyncTask;
    private mediaCurriculumSoapCall mediaCurriculumAsyncTask;
    private pdfCurriculumSoapCall  pdfCurriculumAsyncTask;

    private String allArabName;

    private PopupWindow popwindow;
    private Dialog popupDialog;

    public int storeTabPosition;
    private boolean isSearched;

    private DisplayImageOptions options;

    private LinearLayout filterView, searchView;
    private Button subs_btn;
    private boolean isBookModalDownloadClicked = false;
    private int clientId;
    private FrameLayout fav_layout;
    private Button fav_btn;
    private LinearLayout dropDownlayout1,dropDownlayout2,dropDownlayout3,dropDownlayout4;

    //static String ITEM_SKU = "android.test.purchased";
    //static final String ITEM_SKU = "android.test.cancelled";
    //static final String ITEM_SKU = "android.test.refunded";
    //static final String ITEM_SKU = "android.test.item_unavailable";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fa_enrich = (StoreViewActivity) super.getActivity();

        Bundle args = getArguments();
        storeTabPosition = args.getInt(Globals.STORE_TABS_SECTION_NUMBER);

        //View view = fa_enrich.mTabHost.getTabWidget().getChildTabViewAt(storeTabPosition);
        //TextView tv = (TextView) view.findViewById(R.id.textView1);

        //txtTitle.setText(title);

        deviceWidth = Globals.getDeviceWidth();
        deviceHeight = Globals.getDeviceHeight();

      //  new CheckRooted().execute();

        ll_enrich = (RelativeLayout) inflater.inflate(R.layout.store_new, container, false);
        Typeface font = Typeface.createFromAsset(fa_enrich.getAssets(),"fonts/FiraSans-Regular.otf");
        ViewGroup rootView = (ViewGroup)fa_enrich. findViewById(android.R.id.content).getRootView();

        UserFunctions.changeFont(rootView,font);
        //toast = new Toast(fa_enrich);
        store_database = new StoreDatabase(fa_enrich);
        //shelfPopWindow = new Dialog(fa_enrich);
        bookModalPopwindow = new Dialog(fa_enrich);
        //download_book = new Download_book(fa_enrich.getApplicationContext());
        //copy_store_content = new CopyStoreContent();
        //shelf = new ManahijActivity();
        StoreDatabase.UpdateEnrichCountrySelected("0"); //Initialising tbllastviewcountry table to zero. It has to be called again in countrylist selected.
        //EcountryArrayInSQL=StoreDatabase.GetCountryListFromSQL();
        //EuniversityArrayInSQL =StoreDatabase.GetUniversityListFromSQL();

        /** Declarations of PayPal */
        mDialog = new ProgressDialog(fa_enrich);
        fav_layout = (FrameLayout) ll_enrich.findViewById(R.id.favLayout);
        fav_btn = (Button) ll_enrich.findViewById(R.id.fav_btn);
        if (fa_enrich.isFav){
            fav_btn.setBackgroundResource(R.drawable.s_fav_gold);
        }else{
            fav_btn.setBackgroundResource(R.drawable.ic_store_bookmark);
        }
        fav_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(fa_enrich);
                if (fa_enrich.isFav){
                    String favStore = prefs.getString(Globals.fav_StoreList, "");
                    String favStore1 = String.valueOf(fa_enrich.visitedStore.getClientID()) + ",";
                    if (favStore.contains(favStore1)){
                        favStore = favStore.replace(favStore1,"");
                    }
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(Globals.fav_StoreList, favStore);
                    editor.commit();
                    fav_btn.setBackgroundResource(R.drawable.ic_store_bookmark);
                    fa_enrich.isFav = false;
                }else {
                    String favStore = prefs.getString(Globals.fav_StoreList, "");
                    String favStore1 = String.valueOf(fa_enrich.visitedStore.getClientID()) + ",";
                    favStore = favStore + favStore1;
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(Globals.fav_StoreList, favStore);
                    editor.commit();
                    fav_btn.setBackgroundResource(R.drawable.s_fav_gold);
                    fa_enrich.isFav = true;
                }
            }
        });
        dropDownlayout1 = (LinearLayout) ll_enrich.findViewById(R.id.dropDownlayout1);
        dropDownlayout2 = (LinearLayout) ll_enrich.findViewById(R.id.dropDownlayout2);
        dropDownlayout3 = (LinearLayout) ll_enrich.findViewById(R.id.dropDownlayout3);
        dropDownlayout4 = (LinearLayout) ll_enrich.findViewById(R.id.dropDownlayout4);

        subs_btn = (Button) ll_enrich.findViewById(R.id.subs_btn);
        if (Globals.isLimitedVersion()){
            subs_btn.setVisibility(View.VISIBLE);
        }else{
            subs_btn.setVisibility(View.INVISIBLE);
        }
        updateSubsButton();
        subs_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (updateSubsButton()) {
                    if (UserFunctions.checkLoginAndAlert(fa_enrich, true)) {
                        subscribePurchase(fa_enrich.visitedStore.getInAppId());
                     //   fa_enrich.subscription.new SaveSubscriptionGroups(fa_enrich, Enrichment.this).execute();
                    }
                }
            }
        });
         if(!fa_enrich.visitedStore.isSubcription()){
             subs_btn.setVisibility(View.INVISIBLE);
         }
        /** Declarations of UI Elements */
        progressbar = (ProgressBar) ll_enrich.findViewById(R.id.progressBar1);
        progressbar.setVisibility(View.VISIBLE);
        progDialog = new ProgressDialog(fa_enrich);
        //downloadProgDialog = new ProgressDialog(fa_enrich);
        //LanguageBtn = (Button) ll_enrich.findViewById(R.id.LanguageBtn);
        //SearchFilterBtn = (Button) ll_enrich.findViewById(R.id.search);
        fa_enrich.webView = (RenderHTMLObjects) ll_enrich.findViewById(R.id.webView1);
        CountryBtn = (Button) ll_enrich.findViewById(R.id.CountryBtn);
        UniversityBtn = (Button) ll_enrich.findViewById(R.id.UniversityBtn);
        CategoriesBtn = (Button) ll_enrich.findViewById(R.id.CategoriesBtn);
        PriceBtn = (Button) ll_enrich.findViewById(R.id.PriceBtn);
        CountryText = (TextView) ll_enrich.findViewById(R.id.CountryText);
        txtHeader = (TextView) ll_enrich.findViewById(R.id.txtHeader);
        UniversityText = (TextView) ll_enrich.findViewById(R.id.PublishersText);
        CategoryText = (TextView) ll_enrich.findViewById(R.id.CategoriesText);
        PriceText = (TextView) ll_enrich.findViewById(R.id.PriceText);
        //CountryBtnLabel = (TextView) ll_enrich.findViewById(R.id.CountryBtnLabel);
        bookmessage = (TextView) ll_enrich.findViewById(R.id.bookMesg);
        bookmessage.setVisibility(View.GONE);
        intMesg1 = (TextView) ll_enrich.findViewById(R.id.intMesg1);
        intMesg1.setVisibility(View.GONE);
        intMesg2 = (TextView) ll_enrich.findViewById(R.id.intMesg2);
        intMesg2.setVisibility(View.GONE);
        searchView = (LinearLayout) ll_enrich.findViewById(R.id.searchView);
        filterView = (LinearLayout) ll_enrich.findViewById(R.id.filterView);
        searchEditText = (EditText) ll_enrich.findViewById(R.id.search_box);
        topBarLayout = (RelativeLayout) ll_enrich.findViewById(R.id.relativeLayout);
        titleTxtLayout = (LinearLayout) ll_enrich.findViewById(R.id.linearLayout);
        gridview = (GridView) ll_enrich.findViewById(R.id.MyGrid);

        storeBtmBG = (LinearLayout) ll_enrich.findViewById(R.id.store_btm_bar);


        //set buttons disabled during the initial state
        CountryBtn.setEnabled(false);
        UniversityBtn.setEnabled(false);
        CategoriesBtn.setEnabled(false);
        PriceBtn.setEnabled(false);

        //Register Receiver for detecting network changes(previously used in on resume)
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        fa_enrich.registerReceiver(mNetworkStateIntentReceiver, filter);

        EpriceArrayContent.clear();
        EsearchFilterArrayContent.clear();

        EpriceArrayContent.add(Enrichment.this.getResources().getString(R.string.allbooks));
        EpriceArrayContent.add(Enrichment.this.getResources().getString(R.string.free));
        EpriceArrayContent.add(Enrichment.this.getResources().getString(R.string.paid));
        EsearchFilterArrayContent.add(Enrichment.this.getResources().getString(R.string.author));
        EsearchFilterArrayContent.add(Enrichment.this.getResources().getString(R.string.title));
        ElanguageArray.add(Enrichment.this.getResources().getString(R.string.english));
        ElanguageArray.add("√ò¬ß√ô‚Äû√ò¬π√ò¬±√ò¬®√ô≈†√ò¬©");

        allArabName = Enrichment.this.getResources().getString(R.string.allbooks);
        //StoreTab.storetabValue=2;   //Variable for knowning the tab number.

        setLanguage();

        EcountryArrayList.add(Enrichment.this.getResources().getString(R.string.allbooks));
        EuniversityArrayList.add(Enrichment.this.getResources().getString(R.string.allbooks));
        EcategoryArrayList.add(Enrichment.this.getResources().getString(R.string.allbooks));
        CircleButton btnBack = (CircleButton) ll_enrich.findViewById(R.id.btnback);
        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            fa_enrich.finish();
            }
        });

        CountryBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ETagValue = 0;
                dropDownlayout1.setBackgroundColor(getResources().getColor(R.color.hovercolor));
                callPopUpWindow();
            }
        });
        UniversityBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ETagValue = 1;
                dropDownlayout2.setBackgroundColor(getResources().getColor(R.color.hovercolor));
                callPopUpWindow();
            }
        });
        CategoriesBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ETagValue = 2;
                dropDownlayout3.setBackgroundColor(getResources().getColor(R.color.hovercolor));
                callPopUpWindow();
            }
        });
        PriceBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ETagValue = 3;
                dropDownlayout4.setBackgroundColor(getResources().getColor(R.color.hovercolor));
                callPopUpWindow();
            }
        });

        btnSearch = (Button) ll_enrich.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (searchView.getVisibility()==View.VISIBLE){
                    searchView.setVisibility(View.INVISIBLE);
                    btnSearch.setBackgroundResource(R.drawable.ic_search);
                    fav_layout.setVisibility(View.VISIBLE);
                    InputMethodManager inputmanager = (InputMethodManager) fa_enrich.getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                    inputmanager.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
                }else {
                    searchView.setVisibility(View.VISIBLE);
                    fav_layout.setVisibility(View.GONE);
                    btnSearch.setBackgroundResource(R.drawable.ic_search_pink);
                }
            }
        });



        /** Search Edittext */
        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() == 0) {
                    clearSearchBox.setVisibility(View.GONE);
                } else {
                    clearSearchBox.setVisibility(View.VISIBLE);
                }
                if (s.toString().length() == 0 && isSearched) {
                    isSearched = false;
                    searchEditText.setText("");
                    //ELocalSearchText="";
                    serviceType = 4;
                    EbooksOriginal.clear();
                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progDialog.setMessage(Enrichment.this.getResources().getString(R.string.loading));
                    progDialog.setCancelable(false);
                    progDialog.setCanceledOnTouchOutside(false);
                    progDialog.show();
                    searchEditText.setFocusableInTouchMode(false);
                    searchEditText.setFocusable(false);
                    searchEditText.clearFocus();
                    searchEditText.setFocusableInTouchMode(true);
                    loadSoapCallMethods(storeTabPosition);
                    InputMethodManager inputmanager = (InputMethodManager) fa_enrich.getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                    inputmanager.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        clearSearchBox = (ImageView) ll_enrich.findViewById(R.id.clear_search_box);
        clearSearchBox.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                isSearched = false;
                clearSearchBox.setVisibility(View.GONE);
                searchEditText.setText("");
                serviceType = 4;
                EbooksOriginal.clear();
                progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDialog.setMessage(Enrichment.this.getResources().getString(R.string.loading));

                progDialog.setCancelable(false);
                progDialog.setCanceledOnTouchOutside(false);
                progDialog.show();

                loadSoapCallMethods(storeTabPosition);
            }
        });

        fa_enrich.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    isSearched = true;
                    clearSearchBox.setVisibility(View.VISIBLE);
                    ELocalSearchText = searchEditText.getText().toString();
                    ELocalSearchText = ELocalSearchText.trim();
                    serviceType = 5;
                    EbooksOriginal.clear();
                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progDialog.setMessage(Enrichment.this.getResources().getString(R.string.loading));
                    progDialog.setCancelable(false);
                    progDialog.setCanceledOnTouchOutside(false);
                    progDialog.show();
                    loadSoapCallMethods(storeTabPosition);
                    InputMethodManager inputmanager = (InputMethodManager) fa_enrich.getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
                    inputmanager.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);

                    return true;
                }
                return false;
            }
        });

        String[] authTitleList = {Enrichment.this.getResources().getString(R.string.title),Enrichment.this.getResources().getString(R.string.author)};
        Spinner spinAuthorTitle = (Spinner) ll_enrich.findViewById(R.id.spinner_auth_title);
        ArrayAdapter<String> spinnerCountryAdapter = new ArrayAdapter<String>(fa_enrich, android.R.layout.simple_spinner_dropdown_item, authTitleList);
        spinAuthorTitle.setAdapter(spinnerCountryAdapter);
        spinAuthorTitle.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View view,
                                       int position, long id) {
                if (position == 0) {
                    SearchFilterSelected = "Title";
                } else {
                    SearchFilterSelected = "Author";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        initializeCountryCategoryList();
        changeCountryUniversityGradeText(storeTabPosition, null, 0);
        if (checkInternetConnection()) {
            loadSoapCallMethods(storeTabPosition);
        }
        return ll_enrich;
    }

    private void initializeCountryCategoryList() {
        if (countryList.size() == 0) {
            Country allCountry = new Country();
            allCountry.setCountryId(0);
            allCountry.setCountryEngName("All");
            allCountry.setCountryArabName(allArabName);
            allCountry.setCountryFlagName("");
            countryList.add(allCountry);
        }
        if (categoryList.size() == 0) {
            categoryList = new ArrayList<Category>();
            Category allCategory = new Category();
            allCategory.setCategoryId(0);
            allCategory.setCategoryEngName("All");
            allCategory.setCategoryArabName(allArabName);
            categoryList.add(allCategory);
        }
    }

    private void changeCountryUniversityGradeText(int position, Dialog popUpDialog, int popUpDialogPosition) {
        // if (position == Globals.oldCurriculumStore) {
      String Country = "",University = "",Category = "",price;
        if (fa_enrich.language.equals("en")) {
            if (fa_enrich.visitedStore.getChildobjlist() != null && fa_enrich.visitedStore.getChildobjlist().size() > 0) {
                Country= fa_enrich.visitedStore.getChildobjlist().get(position).getCountryEn();
                University=fa_enrich.visitedStore.getChildobjlist().get(position).getUniversityEn();
                Category=fa_enrich.visitedStore.getChildobjlist().get(position).getCategoryEn();
            }
            price=Enrichment.this.getResources().getString(R.string.price);
        } else {
            if (fa_enrich.visitedStore.getChildobjlist() != null && fa_enrich.visitedStore.getChildobjlist().size() > 0) {
                Country = fa_enrich.visitedStore.getChildobjlist().get(position).getCountryAr();
                University = fa_enrich.visitedStore.getChildobjlist().get(position).getUniversityAr();
                Category = fa_enrich.visitedStore.getChildobjlist().get(position).getCategoryAr();
            }
            price=Enrichment.this.getResources().getString(R.string.price);
        }
        if (popUpDialog != null) {

            if (popUpDialogPosition == 0) {
                popUpDialog.setTitle(Country);
            } else if (popUpDialogPosition == 1) {
                popUpDialog.setTitle(University);
            } else if (popUpDialogPosition == 2) {
                popUpDialog.setTitle(Category);
            } else if (popUpDialogPosition == 3) {
                popUpDialog.setTitle(price);
            }
        } else {
                CountryText.setText(Country);
                UniversityText.setText(University);
                CategoryText.setText(Category);
                PriceText.setText(price);
        }
       /* } else if (position == Globals.newCurriculumStore) {

            if (popUpDialog != null) {
                if (popUpDialogPosition == 0) {
                    popUpDialog.setTitle(R.string.academic_year);
                } else if (popUpDialogPosition == 1) {
                    popUpDialog.setTitle(R.string.grade);
                } else if (popUpDialogPosition == 2) {
                    popUpDialog.setTitle(R.string.semester);
                } else if (popUpDialogPosition == 3) {
                    popUpDialog.setTitle(R.string.price);
                }
            } else {
                CountryText.setText(Enrichment.this.getResources().getString(R.string.academic_year));
                UniversityText.setText(Enrichment.this.getResources().getString(R.string.grade));
                CategoryText.setText(Enrichment.this.getResources().getString(R.string.semester));
                PriceText.setText(Enrichment.this.getResources().getString(R.string.price));
            }
        } else if (position == Globals.eLessonStore) {

            if (popUpDialog != null) {
                if (popUpDialogPosition == 0) {
                    popUpDialog.setTitle(R.string.grade);
                } else if (popUpDialogPosition == 1) {
                    popUpDialog.setTitle(R.string.subject);
                } else if (popUpDialogPosition == 2) {
                    popUpDialog.setTitle(R.string.semester);
                } else if (popUpDialogPosition == 3) {
                    popUpDialog.setTitle(R.string.price);
                }
            } else {
                CountryText.setText(Enrichment.this.getResources().getString(R.string.grade));
                UniversityText.setText(Enrichment.this.getResources().getString(R.string.subject));
                CategoryText.setText(Enrichment.this.getResources().getString(R.string.semester));
                PriceText.setText(Enrichment.this.getResources().getString(R.string.price));
            }
        } else if (position == Globals.quizStore) {

            if (popUpDialog != null) {
                if (popUpDialogPosition == 0) {
                    popUpDialog.setTitle(R.string.academic_year);
                } else if (popUpDialogPosition == 1) {
                    popUpDialog.setTitle(R.string.grade);
                } else if (popUpDialogPosition == 2) {
                    popUpDialog.setTitle(R.string.semester);
                } else if (popUpDialogPosition == 3) {
                    popUpDialog.setTitle(R.string.price);
                }
            } else {
                CountryText.setText(Enrichment.this.getResources().getString(R.string.academic_year));
                UniversityText.setText(Enrichment.this.getResources().getString(R.string.grade));
                CategoryText.setText(Enrichment.this.getResources().getString(R.string.semester));
                PriceText.setText(Enrichment.this.getResources().getString(R.string.price));
            }
        } else if (position == Globals.rzooomStore) {

            if (popUpDialog != null) {
                if (popUpDialogPosition == 0) {
                    popUpDialog.setTitle(R.string.country);
                } else if (popUpDialogPosition == 1) {
                    popUpDialog.setTitle(R.string.publisher);
                } else if (popUpDialogPosition == 2) {
                    popUpDialog.setTitle(R.string.subject);
                } else if (popUpDialogPosition == 3) {
                    popUpDialog.setTitle(R.string.price);
                }
            } else {
                CountryText.setText(Enrichment.this.getResources().getString(R.string.country));
                UniversityText.setText(Enrichment.this.getResources().getString(R.string.publisher));
                CategoryText.setText(Enrichment.this.getResources().getString(R.string.subject));
                PriceText.setText(Enrichment.this.getResources().getString(R.string.price));
            }
        }*/
    }

    public boolean updateSubsButton(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(fa_enrich);
        int subscriptDays = Integer.parseInt(prefs.getString(Globals.SUBSCRIPTDAYSLEFT, "0"));
        String subsEndDate = prefs.getString(Globals.SUBSCRIPTENDDATEKEY, "");


        if (subsEndDate.equals(getResources().getString(R.string.exceed_device_limit))) {
            subs_btn.setEnabled(false);
            subs_btn.setText(subsEndDate);
            return false;
        } else if (subscriptDays > 0) {
            subs_btn.setEnabled(false);//<---false
            //subsEndDate = "Expires:"+subsEndDate;
            subs_btn.setText(subsEndDate);
            return false;//<---false
        } else {
            subs_btn.setEnabled(true);
            subs_btn.setText(getResources().getString(R.string.subscription));
            return true;
        }
    }


    private void loadSoapCallMethods(int position) {
        clientId = fa_enrich.visitedStore.getChildobjlist().get(position).getStoreID();
        if(position<fa_enrich.visitedStore.getChildobjlist().size()) {
            if (fa_enrich.language.equals("en")) {
                txtHeader.setText(fa_enrich.visitedStore.getNameEn());
            }else{
                txtHeader.setText(fa_enrich.visitedStore.getNameAr());
            }
            if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.oldCurriculumStore)) {
                oldCurriculumAsyncTask = new oldAndNewCurriculumSoapCall(position);
                oldCurriculumAsyncTask.execute();

            } else if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.newCurriculumStore)) {
                //} else if (position == Globals.newCurriculumStore) {
                newCurriculumAsyncTask = new oldAndNewCurriculumSoapCall(position);
                newCurriculumAsyncTask.execute();
                //txtHeader.setText(R.string.new_curriculum);
            } else if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.eLessonStore)) {
                //  } else if (position == Globals.eLessonStore) {
                elessonCurriculumAsyncTask = new elessonCurriculumSoapCall(position);
                elessonCurriculumAsyncTask.execute();
               // txtHeader.setText(R.string.e_lesson_store);
            } else if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.quizStore)) {
                // } else if (position == Globals.quizStore) {
                quizCurriculumAsynctask = new quizCurriculumSoapCall(position);
                quizCurriculumAsynctask.execute();
                //txtHeader.setText(R.string.quiz_store);
            } else if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.rzooomStore)) {
                // } else if (position == Globals.rzooomStore) {
                rzooomCurriculumAsyncTask = new rzooomCurriculumSoapCall(position);
                rzooomCurriculumAsyncTask.execute();
                //txtHeader.setText(R.string.rzooom_store);
            } else if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.mindmapStore)) {
                // } else if (position == Globals.mindmapStore) {
                mindmapCurriculumAsyncTask = new mindmapCurriculumSoapCall(position);
                mindmapCurriculumAsyncTask.execute();
                //txtHeader.setText(R.string.mindmap);
            }else if(fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.mediaStore)){
                mediaCurriculumAsyncTask = new mediaCurriculumSoapCall(position);
                mediaCurriculumAsyncTask.execute();
               //txtHeader.setText(R.string.my_media);
            }else if(fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.pdfStore)){
                pdfCurriculumAsyncTask = new pdfCurriculumSoapCall(position);
                pdfCurriculumAsyncTask.execute();
                //txtHeader.setText(R.string.my_media);
            }
        }
    }

    private void cancelSoapCallMethods(int position) {
        if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.oldCurriculumStore)) {
            oldCurriculumAsyncTask.cancel(true);
        } else if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.newCurriculumStore)) {
            newCurriculumAsyncTask.cancel(true);
        } else if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.eLessonStore)) {
            elessonCurriculumAsyncTask.cancel(true);
        } else if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.quizStore)) {
            quizCurriculumAsynctask.cancel(true);
        } else if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.rzooomStore)) {
            rzooomCurriculumAsyncTask.cancel(true);
        } else if (fa_enrich.visitedStore.getChildobjlist().get(position).getShelfPosition().equals(""+Globals.mindmapStore)) {
            mindmapCurriculumAsyncTask.cancel(true);
        }
    }

	/*
	 *  Check Internet Exist
	 */

    private boolean checkInternetConnection() {

        ConnectivityManager conMgr = (ConnectivityManager) fa_enrich.getSystemService(Context.CONNECTIVITY_SERVICE);

        // ARE WE CONNECTED TO THE NET 

        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }

    }

    public BroadcastReceiver mNetworkStateIntentReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            //int extraWifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);
			/*int extraWifiState;
			ConnectivityManager conman = (ConnectivityManager) fa_enrich.getSystemService(Context.CONNECTIVITY_SERVICE);
    		if(conman.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED || 
    				conman.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
    		{
    			extraWifiState=3;
    		}
    		else
    		{
    			extraWifiState=1;
    		}*/

            int extraWifiState = 0;
            if (intent.getAction().equals(
                    ConnectivityManager.CONNECTIVITY_ACTION)) {

                NetworkInfo info = intent.getParcelableExtra(
                        ConnectivityManager.EXTRA_NETWORK_INFO);
                String typeName = info.getTypeName();
                String subtypeName = info.getSubtypeName();
                // //System.out.println("Network is up ******** "+typeName+":::"+subtypeName); 

                if (checkInternetConnection() == true) {
                    extraWifiState = 3;

                } else {
                    extraWifiState = 1;
                }
            } else {
                extraWifiState = 3;
            }

            switch (extraWifiState) {
                case WifiManager.WIFI_STATE_DISABLED:
                    ////System.out.println("InternetState:disconnected");
                    internetOff = "YES";
                    if (downloadStatus == 1) {
                        String downloadCancelledText = Enrichment.this.getResources().getString(R.string.downloadCancelled);
                        try {
                            //changes for fixing crash issue (try/catch)
                            Toast.makeText(fa_enrich.getApplicationContext(), downloadCancelledText, Toast.LENGTH_SHORT).show();
                        } catch (RuntimeException e) {
                        }
                    }
                    bookModalPopwindow.dismiss();
                    bookmessage.setVisibility(View.GONE);
                    intMesg1.setVisibility(View.VISIBLE);
                    intMesg2.setVisibility(View.VISIBLE);
                    gridview.setVisibility(View.GONE);
                    progressbar.setVisibility(View.GONE);
                    topBarLayout.setVisibility(View.GONE);
                    if (fa_enrich.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        fa_enrich.drawerLayout.closeDrawer(GravityCompat.START);
                        fa_enrich.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    } else {
                        fa_enrich.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    }
                  //  titleTxtLayout.setVisibility(View.GONE);
                    //searchImage.setVisibility(View.GONE);
                    progDialog.dismiss();
                    if (downloadProgDialog != null) {
                        downloadProgDialog.dismiss();
                    }
                    downloadProgDialog = null;
                    break;

                //case WifiManager.WIFI_STATE_DISABLING:

                case WifiManager.WIFI_STATE_ENABLED:
                    ////System.out.println("InternetState:connected");
                    if (internetOff.equals("YES")) {  //EcountryArray.count, EuniversityArray.count, EcategoryArray.count = 0 need to check this condition
                        //toast.cancel();
                        intMesg1.setVisibility(View.GONE);
                        intMesg2.setVisibility(View.GONE);
                        gridview.setVisibility(View.VISIBLE);
                        topBarLayout.setVisibility(View.VISIBLE);
                        fa_enrich.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                        if (books.size() == 0 && internetOff.equals("YES")) {
                            bookmessage.setVisibility(View.VISIBLE);
                        } else {
                            bookmessage.setVisibility(View.GONE);
                        }
                        if (countryList.size() == 0 || universityList.size() == 0 || categoryList.size() == 0) {
                            if (internetOff.equals("YES")) {
                                if (fa_enrich.updateLists.size()>0){
                                    fa_enrich.updateLists.clear();
                                }
                                for (int i = 0;i<fa_enrich.visitedStore.getChildobjlist().size();i++){
                                    fa_enrich.updateLists.add(String.valueOf(i));
                                }
                                progressbar.setVisibility(View.VISIBLE);
                                loadSoapCallMethods(storeTabPosition);
                                internetOff = "NO";
                            } else {
                                internetOff = "NO";
                            }
                        }
                    }
                    break;

                //case WifiManager.WIFI_STATE_ENABLING:

                //case WifiManager.WIFI_STATE_UNKNOWN:
            }
        }
    };

    public class oldAndNewCurriculumSoapCall extends AsyncTask<Void, Void, Void> {

        private int tabPosition;
        private String webServiceUrl;

        public oldAndNewCurriculumSoapCall(int position) {
            this.tabPosition = position;
            if (fa_enrich.updateLists.contains(String.valueOf(position))){
                serviceType = 0;
                countryList.clear();
                universityList.clear();
                categoryList.clear();
                initializeCountryCategoryList();
            }
          //  this.webServiceUrl = oldCurriculumWebServiceURL;
            if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl()==null){
                this.webServiceUrl ="";
            }else {
                this.webServiceUrl = fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (serviceType == 4 || serviceType == 5) {
                EbooksOriginal.clear();
            }
        }

        protected Void doInBackground(Void... params) {

            if (serviceType == 0) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getCountryList xmlns=\"http://semanoor.com.sa/\" />\n"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/getCountryList";
            } else if (serviceType == 1) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getUniversityList xmlns=\"http://semanoor.com.sa/\" />"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/getUniversityList";
            } else if (serviceType == 2) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getCategoryList xmlns=\"http://semanoor.com.sa/\" />"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/getCategoryList";
            } else if (serviceType == 3) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getBooksByUniversity xmlns=\"http://semanoor.com.sa/\">"
                        + "<UniversityID>%i</UniversityID>"
                        + "</getBooksByUniversity>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/getBooksByUniversity";
            } else if (serviceType == 4) {
                EbooksOriginal.clear();
                String CategoryName = "";
                if (selectedCategory == null || selectedCategory.getCategoryEngName().equals("All")) {
                    CategoryName = "";
                } else {
                    CategoryName = selectedCategory.getCategoryEngName();
                }
                int countryId = 0;
                int universityId = 0;
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }
//                if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.oldCurriculumStore)) {
//                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
//                            + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
//                            + "<soap:Body>"
//                            + "<getBooksByCategory xmlns=\"http://semanoor.com.sa/\">"
//                            + "<sCategoryName>" + CategoryName + "</sCategoryName>"
//                            + "<iCountryID>" + countryId + "</iCountryID>"
//                            + "<iUniversityID>" + universityId + "</iUniversityID>"
//                            + "</getBooksByCategory>"
//                            + "</soap:Body>"
//                            + "</soap:Envelope>";
//                } else {
                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                            + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                            + "  <soap:Body>"
                            + "    <getBooksByCategoryClient xmlns=\"http://semanoor.com.sa/\">"
                            + "      <CategoryName>" + CategoryName + "</CategoryName>"
                            + "      <CountryID>"+ countryId +"</CountryID>"
                            + "      <UniversityID>"+ universityId +"</UniversityID>"
                            + "      <ClientID>"+ clientId +"</ClientID>"
                            + "    </getBooksByCategoryClient>"
                            + "  </soap:Body>"
                            + "</soap:Envelope>";
            //    }
                SOAPAction = "http://semanoor.com.sa/getBooksByCategoryClient";
                //System.out.println("GetBooksByCategory: "+CategoryName+selectedCountry.getCountryId()+selectedUniversity.getUniversityId());
            } else if (serviceType == 5) {
                EbooksOriginal.clear();
                String CategoryName = "";
                int countryId = 0;
                int univId= 0;
                if (selectedCategory == null || selectedCategory.getCategoryEngName().equals("All")) {
                    CategoryName = "";
                    //Category category = categoryList.get(1);
                    //CategoryName = category.getCategoryEngName();
                } else {
                    CategoryName = selectedCategory.getCategoryEngName();
                }
                if (selectedCountry == null || selectedCountry.getCountryId() == 0) {
                    if(countryList.size()==2) {
                        Country country = countryList.get(1);
                        countryId = country.getCountryId();
                    }
                } else {
                    countryId = selectedCountry.getCountryId();
                }
                if ( selectedUniversity == null || selectedUniversity.getUniversityId() == 0) {
                    if(filteredUniversityList.size()==2) {
                        University university = filteredUniversityList.get(1);
                        univId = university.getUniversityId();
                    }
                } else {
                    univId = selectedUniversity.getUniversityId();
                }

                if (SearchFilterSelected.equals("Title")) {
                    if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.oldCurriculumStore)) {
                        soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                                + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                + "<soap:Body>"
                                + "<getBooksByTitle xmlns=\"http://semanoor.com.sa/\">\n"
                                + "<sTitle>" + ELocalSearchText + "</sTitle>"
                                + "<sCategoryName>" + CategoryName + "</sCategoryName>"
                                + "<iCountryID>" + countryId + "</iCountryID>"
                                + "<iUniversityID>" + univId + "</iUniversityID>"
                                + "</getBooksByTitle>"
                                + "</soap:Body>"
                                + "</soap:Envelope>";
                    } else {
                        soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                                "  <soap:Body>\n" +
                                "    <getBooksByTitleClient xmlns=\"http://semanoor.com.sa/\">\n" +
                                "      <Title>"+ ELocalSearchText +"</Title>" +
                                "      <CategoryName>"+ CategoryName +"</CategoryName>" +
                                "      <CountryID>"+ countryId +"</CountryID>" +
                                "      <UniversityID>"+ univId +"</UniversityID>" +
                                "      <ClientID>"+ clientId +"</ClientID>" +
                                "    </getBooksByTitleClient>" +
                                "  </soap:Body>\n" +
                                "</soap:Envelope>";
                    }
                    SOAPAction = "http://semanoor.com.sa/getBooksByTitleClient";
                    ////System.out.println("GetBooksByTitle :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                } else {
                    if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.oldCurriculumStore)) {
                        soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                                + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                + "<soap:Body>"
                                + "<getBooksByAuthors xmlns=\"http://semanoor.com.sa/\">"
                                + "<sAuthorName>" + ELocalSearchText + "</sAuthorName>"
                                + "<sCategoryName>" + CategoryName + "</sCategoryName>"
                                + "<iCountryID>" + countryId + "</iCountryID>"
                                + "<iUniversityID>" + univId + "</iUniversityID>"
                                + "</getBooksByAuthors>"
                                + "</soap:Body>"
                                + "</soap:Envelope>";
                    } else {
                        soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                                "  <soap:Body>\n" +
                                "    <getBooksByAuthorsClient xmlns=\"http://semanoor.com.sa/\">" +
                                "      <AuthorName>"+ ELocalSearchText +"</AuthorName>" +
                                "      <CategoryName>"+ CategoryName +"</CategoryName>" +
                                "      <CountryID>"+ countryId +"</CountryID>" +
                                "      <UniversityID>"+ univId +"</UniversityID>" +
                                "      <ClientID>" +clientId +"</ClientID>" +
                                "    </getBooksByAuthorsClient>" +
                                "  </soap:Body>\n" +
                                "</soap:Envelope>";
                    }

                    SOAPAction = "http://semanoor.com.sa/getBooksByAuthorsClient";
                    ////System.out.println("GetBooksByAuthor :"+ELocalSearchText+CategoryName+selectedCountry.getCountryId() +selectedUniversity.getUniversityId() );
                }
            }
            ENVELOPE = String.format(soapMessage, null);
            if (tabPosition<fa_enrich.visitedStore.getChildobjlist().size()) {

                String returnXml = CallNewWebService(this.webServiceUrl, SOAPAction, ENVELOPE, tabPosition);   //Call to web services
                //System.out.println("returnxml:"+returnXml);
                parseXml(returnXml, tabPosition);
            }//Call to parse the web services content
            return null;
        }

        protected void onPostExecute(Void result) {
            if (fa_enrich.updateLists.contains(String.valueOf(tabPosition))){
                fa_enrich.updateLists.remove(String.valueOf(tabPosition));
            }
            if (serviceType == 0) {
                CountryBtn.setEnabled(false);
                UniversityBtn.setEnabled(false);
                CategoriesBtn.setEnabled(false);
                PriceBtn.setEnabled(false);
                serviceType = 1;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 1) {
                serviceType = 2;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 2) {
                serviceType = -1;
            } else if (serviceType == 3) {   //need to check
            } else if (serviceType == 4) {
                CountryBtn.setEnabled(true);
                UniversityBtn.setEnabled(true);
                CategoriesBtn.setEnabled(true);
                PriceBtn.setEnabled(true);

                processGridView();
            } else if (serviceType == 5) {
                processGridView();
            }

            if (serviceType == -1) {
                setDefaultValue();
                serviceType = 4;
                loadSoapCallMethods(this.tabPosition);
            }
        }
    }

    public class quizCurriculumSoapCall extends AsyncTask<Void, Void, Void> {

        private int tabPosition;

        public quizCurriculumSoapCall(int position) {
            this.tabPosition = position;
            if (fa_enrich.updateLists.contains(String.valueOf(position))){
                serviceType = 0;
                countryList.clear();
                universityList.clear();
                categoryList.clear();
                initializeCountryCategoryList();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (serviceType == 4 || serviceType == 5) {
                EbooksOriginal.clear();
            }
        }

        protected Void doInBackground(Void... params) {

            if (serviceType == 0) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getCountryList xmlns=\"http://SemaSvr.org/\">\n"
                        + "<psw>9EnFF7o4LB8C98cL9SMBS/WKwhv7ap6+</psw>\n"
                        + "</getCountryList>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://SemaSvr.org/getCountryList";
            } else if (serviceType == 1) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getUniversityList xmlns=\"http://SemaSvr.org/\">\n"
                        + "<psw>9EnFF7o4LB8C98cL9SMBS/WKwhv7ap6+</psw>\n"
                        + "</getUniversityList>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://SemaSvr.org/getUniversityList";
            } else if (serviceType == 2) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getCategoryList xmlns=\"http://SemaSvr.org/\">\n"
                        + "<psw>9EnFF7o4LB8C98cL9SMBS/WKwhv7ap6+</psw>\n"
                        + "</getCategoryList>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://SemaSvr.org/getCategoryList";
            } else if (serviceType == 3) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getBooksByUniversity xmlns=\"http://SemaSvr.org/\">\n"
                        + "<psw>9EnFF7o4LB8C98cL9SMBS/WKwhv7ap6+</psw>\n"
                        + "<iUniversity>%i</iUniversity>"
                        + "</getBooksByUniversity>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://SemaSvr.org/getBooksByUniversity";
            } else if (serviceType == 4) {
                int countryId = 0;
                int universityId = 0;
                String CategoryName = "";
                if (selectedCategory == null || selectedCategory.getCategoryEngName().equals("All")) {
                    CategoryName = "";
                } else {
                    CategoryName = selectedCategory.getCategoryEngName();
                }
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                        "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                        "  <soap:Body>" +
                        "    <getBooksByCategoryClient xmlns=\"http://SemaSvr.org/\">" +
                        "      <psw>9EnFF7o4LB8C98cL9SMBS/WKwhv7ap6+</psw>" +
                        "      <CategoryName>"+ CategoryName +"</CategoryName>" +
                        "      <CountryID>"+ countryId +"</CountryID>" +
                        "      <UniversityID>"+ universityId +"</UniversityID>" +
                        "      <ClientID>"+ clientId +"</ClientID>" +
                        "    </getBooksByCategoryClient>" +
                        "  </soap:Body>" +
                        "</soap:Envelope>";
                SOAPAction = "http://SemaSvr.org/getBooksByCategoryClient";
                ////System.out.println("GetBooksByCategory: "+CategoryName+ESelectedCountryID+EUniversityIDInt);
            } else if (serviceType == 5) {

                String CategoryName = "";
                if (selectedCategory == null || selectedCategory.getCategoryEngName().equals("All")) {
                    CategoryName = "";
                } else {
                    CategoryName = selectedCategory.getCategoryEngName();
                }
                int countryId = 0;
                int universityId = 0;
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }

                if (SearchFilterSelected.equals("Title")) {

                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                            "  <soap:Body>" +
                            "    <getBooksByTitleClient xmlns=\"http://SemaSvr.org/\">" +
                            "      <psw>9EnFF7o4LB8C98cL9SMBS/WKwhv7ap6+</psw>" +
                            "      <Title>"+ ELocalSearchText +"</Title>" +
                            "      <CategoryName>"+ CategoryName +"</CategoryName>" +
                            "      <CountryID>"+ countryId +"</CountryID>" +
                            "      <UniversityID>"+ universityId +"</UniversityID>" +
                            "      <ClientID>"+ clientId +"</ClientID>" +
                            "    </getBooksByTitleClient>" +
                            "  </soap:Body>" +
                            "</soap:Envelope>";
                    SOAPAction = "http://SemaSvr.org/getBooksByTitleClient";
                    ////System.out.println("GetBooksByTitle :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                } else {

                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                            "  <soap:Body>" +
                            "    <getBooksByAuthorClient xmlns=\"http://SemaSvr.org/\">" +
                            "      <psw>9EnFF7o4LB8C98cL9SMBS/WKwhv7ap6+</psw>" +
                            "      <AuthorName>"+ ELocalSearchText +"</AuthorName>" +
                            "      <CategoryName>"+ CategoryName +"</CategoryName>" +
                            "      <CountryID>"+ countryId +"</CountryID>" +
                            "      <UniversityID>"+ universityId +"</UniversityID>" +
                            "      <ClientID>"+ clientId +"</ClientID>" +
                            "    </getBooksByAuthorClient>" +
                            "  </soap:Body>" +
                            "</soap:Envelope>";
                    SOAPAction = "http://SemaSvr.org/getBooksByAuthorClient";
                    ////System.out.println("GetBooksByAuthor :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                }
            }
            ENVELOPE = String.format(soapMessage, null);
           // String returnXml = CallNewWebService(Globals.quizWebServiceURL, SOAPAction, ENVELOPE, tabPosition);   //Call to web services
           String quizUrl;
            if (tabPosition<fa_enrich.visitedStore.getChildobjlist().size()) {
                if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl() == null) {
                    quizUrl = "";
                } else {
                    quizUrl = fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl();
                }
                String returnXml = CallNewWebService(quizUrl, SOAPAction, ENVELOPE, tabPosition);
                //System.out.println("returnxml:"+returnXml);
                parseXml(returnXml, tabPosition);
            }//Call to parse the web services content

            return null;
        }

        protected void onPostExecute(Void result) {
            if (fa_enrich.updateLists.contains(String.valueOf(tabPosition))){
                fa_enrich.updateLists.remove(String.valueOf(tabPosition));
            }
            if (serviceType == 0) {
                CountryBtn.setEnabled(false);
                UniversityBtn.setEnabled(false);
                CategoriesBtn.setEnabled(false);
                PriceBtn.setEnabled(false);
                serviceType = 1;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 1) {
                serviceType = 2;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 2) {
                serviceType = -1;
            } else if (serviceType == 3) {   //need to check
            } else if (serviceType == 4) {
                CountryBtn.setEnabled(true);
                UniversityBtn.setEnabled(true);
                CategoriesBtn.setEnabled(true);
                PriceBtn.setEnabled(true);

                processGridView();
            } else if (serviceType == 5) {
                processGridView();
            }

            if (serviceType == -1) {
                setDefaultValue();
                serviceType = 4;
                loadSoapCallMethods(this.tabPosition);
            }
        }
    }

    public class elessonCurriculumSoapCall extends AsyncTask<Void, Void, Void> {

        private int tabPosition;

        public elessonCurriculumSoapCall(int position) {
            this.tabPosition = position;
            if (fa_enrich.updateLists.contains(String.valueOf(position))){
                serviceType = 0;
                countryList.clear();
                universityList.clear();
                categoryList.clear();
                initializeCountryCategoryList();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (serviceType == 4 || serviceType == 5) {
                EbooksOriginal.clear();
            }
        }

        protected Void doInBackground(Void... params) {

            if (serviceType == 0) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + " <GetCountryList xmlns=\"http://semanoor.com.sa/\">"
                        + "</GetCountryList>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/GetCountryList";
            } else if (serviceType == 1) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<GetUniversityList xmlns=\"http://semanoor.com.sa/\">"
                        + "</GetUniversityList>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/GetUniversityList";
            } else if (serviceType == 2) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<GetCategoryList xmlns=\"http://semanoor.com.sa/\">"
                        + "</GetCategoryList>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/GetCategoryList";
            } else if (serviceType == 3) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "GetEBagsByUniversity xmlns=\"http://semanoor.com.sa/\">"
                        + "<iUniversity>1</iUniversity>"
                        + "</GetEBagsByUniversity>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/GetEBagsByUniversity";
            } else if (serviceType == 4) {
                EbooksOriginal.clear();
                int categoryId = 0;
                int countryId = 0;
                int universityId = 0;
                if (selectedCategory != null) {
                    categoryId = selectedCategory.getCategoryId();
                }
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                        "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                        "  <soap:Body>" +
                        "    <GetEBagsByCategoryClient xmlns=\"http://semanoor.com.sa/\">" +
                        "      <iCategoryId>"+ categoryId +"</iCategoryId>" +
                        "      <iCountryID>"+ countryId +"</iCountryID>" +
                        "      <iUniversity>"+ universityId +"</iUniversity>" +
                        "      <ClientID>"+ clientId +"</ClientID>" +
                        "    </GetEBagsByCategoryClient>" +
                        "  </soap:Body>" +
                        "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/GetEBagsByCategoryClient";
                ////System.out.println("GetBooksByCategory: "+CategoryName+ESelectedCountryID+EUniversityIDInt);
            } else if (serviceType == 5) {
                EbooksOriginal.clear();
                int categoryId = 0;
                int countryId = 0;
                int universityId = 0;
                if (selectedCategory != null) {
                    categoryId = selectedCategory.getCategoryId();
                }
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }

                if (SearchFilterSelected.equals("Title")) {
                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                            "  <soap:Body>" +
                            "    <GetEBagsByTitleClient xmlns=\"http://semanoor.com.sa/\">" +
                            "      <sTitle>"+ ELocalSearchText +"</sTitle>" +
                            "      <iCategoryId>"+ categoryId +"</iCategoryId>" +
                            "      <iCountryID>"+ countryId +"</iCountryID>" +
                            "      <iUniversity>"+ universityId +"</iUniversity>" +
                            "      <ClientID>"+ clientId +"</ClientID>" +
                            "    </GetEBagsByTitleClient>" +
                            "  </soap:Body>" +
                            "</soap:Envelope>";
                    SOAPAction = "http://semanoor.com.sa/GetEBagsByTitleClient";

                    ////System.out.println("GetBooksByTitle :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                } else {
                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                            "  <soap:Body>" +
                            "    <GetEBagsByTagsClient xmlns=\"http://semanoor.com.sa/\">" +
                            "      <sTags>"+ ELocalSearchText +"</sTags>" +
                            "      <iCategoryId>"+ categoryId +"</iCategoryId>" +
                            "      <iCountryID>"+ countryId +"</iCountryID>" +
                            "      <iUniversity>"+ universityId +"</iUniversity>" +
                            "      <ClientID>"+ clientId +"</ClientID>" +
                            "    </GetEBagsByTagsClient>" +
                            "  </soap:Body>" +
                            "</soap:Envelope>";
                    SOAPAction = "http://semanoor.com.sa/GetEBagsByTagsClient";
                    ////System.out.println("GetBooksByAuthor :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                }
            }
            ENVELOPE = String.format(soapMessage, null);
           // String returnXml = CallNewWebService(Globals.eLessonWebServiceURL, SOAPAction, ENVELOPE, tabPosition);   //Call to web services
            String elessonUrl;
            if (tabPosition<fa_enrich.visitedStore.getChildobjlist().size()) {
                if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl() != null) {
                    elessonUrl = fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl();
                } else {
                    elessonUrl = "";
                }
                String returnXml = CallNewWebService(elessonUrl, SOAPAction, ENVELOPE, tabPosition);
                //System.out.println("returnxml:"+returnXml);
                parseXml(returnXml, tabPosition);
            }//Call to parse the web services content

            return null;
        }

        protected void onPostExecute(Void result) {
            if (fa_enrich.updateLists.contains(String.valueOf(tabPosition))){
                fa_enrich.updateLists.remove(String.valueOf(tabPosition));
            }
            if (serviceType == 0) {
                CountryBtn.setEnabled(false);
                UniversityBtn.setEnabled(false);
                CategoriesBtn.setEnabled(false);
                PriceBtn.setEnabled(false);
                serviceType = 1;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 1) {
                serviceType = 2;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 2) {
                serviceType = -1;
            } else if (serviceType == 3) {   //need to check
            } else if (serviceType == 4) {
                CountryBtn.setEnabled(true);
                UniversityBtn.setEnabled(true);
                CategoriesBtn.setEnabled(true);
                PriceBtn.setEnabled(true);

                processGridView();
            } else if (serviceType == 5) {
                processGridView();
            }

            if (serviceType == -1) {
                setDefaultValue();
                serviceType = 4;
                loadSoapCallMethods(this.tabPosition);
            }
        }
    }

    public class rzooomCurriculumSoapCall extends AsyncTask<Void, Void, Void> {

        private int tabPosition;

        public rzooomCurriculumSoapCall(int position) {
            this.tabPosition = position;
            if (fa_enrich.updateLists.contains(String.valueOf(position))){
                serviceType = 0;
                countryList.clear();
                universityList.clear();
                categoryList.clear();
                initializeCountryCategoryList();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (serviceType == 4 || serviceType == 5) {
                EbooksOriginal.clear();
            }
        }

        protected Void doInBackground(Void... params) {

            if (serviceType == 0) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + " <getCountryList xmlns=\"http://semanooor.com.sa/\">"
                        + "</getCountryList>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanooor.com.sa/getCountryList";
            } else if (serviceType == 1) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getUniversityList xmlns=\"http://semanooor.com.sa/\">\n"
                        + "</getUniversityList>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanooor.com.sa/getUniversityList";
            } else if (serviceType == 2) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getCategoryList xmlns=\"http://semanooor.com.sa/\">"
                        + "</getCategoryList>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanooor.com.sa/getCategoryList";
            } else if (serviceType == 3) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getCharactersByUniversity xmlns=\"http://semanooor.com.sa/\">\n"
                        + "<iUniversity>%i</iUniversity>"
                        + "</getCharactersByUniversity>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanooor.com.sa/getCharactersByUniversity";
            } else if (serviceType == 4) {
                String CategoryName = "";
                int categoryId = 0;
                int countryId = 0;
                int universityId = 0;
                if (selectedCategory == null || selectedCategory.getCategoryEngName().equals("All")) {
                    CategoryName = "";
                } else {
                    CategoryName = selectedCategory.getCategoryEngName();
                    categoryId = selectedCategory.getCategoryId();
                }
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }
                if (CategoryName.equals("")) {
                    soapMessage ="<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                            "  <soap:Body>" +
                            "    <getCharactersByCategoryClient xmlns=\"http://semanooor.com.sa/\">" +
                            "      <sCategoryName>"+ CategoryName +"</sCategoryName>" +
                            "      <iCountryID>"+ countryId +"</iCountryID>" +
                            "      <iUniversity>"+ universityId +"</iUniversity>" +
                            "      <iClientID>"+ clientId +"</iClientID>" +
                            "    </getCharactersByCategoryClient>" +
                            "  </soap:Body>" +
                            "</soap:Envelope>";
                } else {
                    soapMessage ="<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                            "  <soap:Body>" +
                            "    <getCharactersByCategoryClient xmlns=\"http://semanooor.com.sa/\">" +
                            "      <sCategoryName>"+ categoryId +"</sCategoryName>" +
                            "      <iCountryID>"+ countryId +"</iCountryID>" +
                            "      <iUniversity>"+ universityId +"</iUniversity>" +
                            "      <iClientID>"+ clientId +"</iClientID>" +
                            "    </getCharactersByCategoryClient>" +
                            "  </soap:Body>" +
                            "</soap:Envelope>";
                }


                SOAPAction = "http://semanooor.com.sa/getCharactersByCategoryClient";
                ////System.out.println("GetBooksByCategory: "+CategoryName+ESelectedCountryID+EUniversityIDInt);
            } else if (serviceType == 5) {

                String CategoryName = "";
                int categoryId = 0;
                if (selectedCategory == null || selectedCategory.getCategoryEngName().equals("All")) {
                    CategoryName = "";
                } else {
                    CategoryName = selectedCategory.getCategoryEngName();
                    categoryId = selectedCategory.getCategoryId();
                }
                int countryId = 0;
                int universityId = 0;
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }


                if (SearchFilterSelected.equals("Title")) {
                    if (CategoryName.equals("")) {
                        soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                                "  <soap:Body>" +
                                "    <getCharactersByByTitleClient xmlns=\"http://semanooor.com.sa/\">" +
                                "      <sTitle>"+ ELocalSearchText +"</sTitle>" +
                                "      <sCategoryName>"+ CategoryName +"</sCategoryName>" +
                                "      <iCountryID>"+ countryId +"</iCountryID>" +
                                "      <iUniversity>"+ universityId +"</iUniversity>" +
                                "      <iClientID>"+ clientId +"</iClientID>" +
                                "    </getCharactersByByTitleClient>" +
                                "  </soap:Body>" +
                                "</soap:Envelope>";
                    } else {
                        soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                                "  <soap:Body>" +
                                "    <getCharactersByByTitleClient xmlns=\"http://semanooor.com.sa/\">" +
                                "      <sTitle>"+ ELocalSearchText +"</sTitle>" +
                                "      <sCategoryName>"+ categoryId +"</sCategoryName>" +
                                "      <iCountryID>"+ countryId +"</iCountryID>" +
                                "      <iUniversity>"+ universityId +"</iUniversity>" +
                                "      <iClientID>"+ clientId +"</iClientID>" +
                                "    </getCharactersByByTitleClient>" +
                                "  </soap:Body>" +
                                "</soap:Envelope>";
                    }
                    SOAPAction = "http://semanooor.com.sa/getCharactersByByTitleClient";
                    ////System.out.println("GetBooksByTitle :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                } else {
                    if (CategoryName.equals("")) {
                        soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                                "  <soap:Body>" +
                                "    <getCharactersByAuthorNameClient xmlns=\"http://semanooor.com.sa/\">" +
                                "      <sAuthorName>"+ ELocalSearchText +"</sAuthorName>" +
                                "      <sCategoryName>"+ CategoryName +"</sCategoryName>" +
                                "      <iCountryID>"+ countryId +"</iCountryID>" +
                                "      <iUniversity>"+ universityId +"</iUniversity>" +
                                "      <iClientID>"+ clientId +"</iClientID>" +
                                "    </getCharactersByAuthorNameClient>" +
                                "  </soap:Body>" +
                                "</soap:Envelope>";
                    } else {
                        soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                                "  <soap:Body>" +
                                "    <getCharactersByAuthorNameClient xmlns=\"http://semanooor.com.sa/\">" +
                                "      <sAuthorName>"+ ELocalSearchText +"</sAuthorName>" +
                                "      <sCategoryName>"+ categoryId +"</sCategoryName>" +
                                "      <iCountryID>"+ countryId +"</iCountryID>" +
                                "      <iUniversity>"+ universityId +"</iUniversity>" +
                                "      <iClientID>"+ clientId +"</iClientID>" +
                                "    </getCharactersByAuthorNameClient>" +
                                "  </soap:Body>" +
                                "</soap:Envelope>";
                    }
                    SOAPAction = "http://semanooor.com.sa/getCharactersByAuthorNameClient";
                    ////System.out.println("GetBooksByAuthor :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                }
            }
            ENVELOPE = String.format(soapMessage, null);
           // String returnXml = CallNewWebService(Globals.rzooomWebServiceURL, SOAPAction, ENVELOPE, tabPosition);   //Call to web services
           String rzoomUrl;
            if (tabPosition<fa_enrich.visitedStore.getChildobjlist().size()) {
                if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl() != null) {
                    rzoomUrl = fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl();
                } else {
                    rzoomUrl = "";
                }
                String returnXml = CallNewWebService(rzoomUrl, SOAPAction, ENVELOPE, tabPosition);
                //System.out.println("returnxml:"+returnXml);
                parseXml(returnXml, tabPosition);
            }//Call to parse the web services content

            return null;
        }

        protected void onPostExecute(Void result) {
            if (fa_enrich.updateLists.contains(String.valueOf(tabPosition))){
                fa_enrich.updateLists.remove(String.valueOf(tabPosition));
            }
            if (serviceType == 0) {
                CountryBtn.setEnabled(false);
                UniversityBtn.setEnabled(false);
                CategoriesBtn.setEnabled(false);
                PriceBtn.setEnabled(false);
                serviceType = 1;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 1) {
                serviceType = 2;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 2) {
                serviceType = -1;
            } else if (serviceType == 3) {
                //need to check
                //	loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 4) {
                CountryBtn.setEnabled(true);
                UniversityBtn.setEnabled(true);
                CategoriesBtn.setEnabled(true);
                PriceBtn.setEnabled(true);

                processGridView();
            } else if (serviceType == 5) {
                processGridView();
            }

            if (serviceType == -1) {
                setDefaultValue();
                serviceType = 4;
                loadSoapCallMethods(this.tabPosition);
            }
        }
    }

    private class mindmapCurriculumSoapCall extends AsyncTask<Void, Void, Void> {

        private int tabPosition;

        public mindmapCurriculumSoapCall(int position) {
            this.tabPosition = position;
            if (fa_enrich.updateLists.contains(String.valueOf(position))){
                serviceType = 0;
                countryList.clear();
                universityList.clear();
                categoryList.clear();
                initializeCountryCategoryList();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (serviceType == 4 || serviceType == 5) {
                EbooksOriginal.clear();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (serviceType == 0) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<GetCountryList xmlns=\"http://Semanoor.com/\" />"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://Semanoor.com/GetCountryList";
            } else if (serviceType == 1) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<GetUniversityList xmlns=\"http://Semanoor.com/\" />\n"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://Semanoor.com/GetUniversityList";
            } else if (serviceType == 2) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<GetCategoryList xmlns=\"http://Semanoor.com/\" />"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://Semanoor.com/GetCategoryList";
            } else if (serviceType == 3) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>"
                        + "<getMindMapsByUniversity xmlns=\\\"http://semanooor.com.sa/\\\">\n"
                        + "<iUniversity>%i</iUniversity>"
                        + "</getCharactersByUniversity>"
                        + "</soap:Body>"
                        + "</soap:Envelope>";
                SOAPAction = "http://semanooor.com.sa/getCharactersByUniversity";
            } else if (serviceType == 4) {
                int categoryId = 0;
                int countryId = 0;
                int universityId = 0;
                if (selectedCategory != null) {
                    categoryId = selectedCategory.getCategoryId();
                }
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                        "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                        "  <soap:Body>" +
                        "    <GetMindMapsByCategoryClient xmlns=\"http://Semanoor.com/\">" +
                        "      <iCategoryId>"+ categoryId +"</iCategoryId>" +
                        "      <iCountryID>"+ countryId +"</iCountryID>" +
                        "      <iUniversity>"+ universityId +"</iUniversity>" +
                        "      <ClientID>"+clientId+"</ClientID>" +
                        "    </GetMindMapsByCategoryClient>" +
                        "  </soap:Body>" +
                        "</soap:Envelope>";
                SOAPAction = "http://Semanoor.com/GetMindMapsByCategoryClient";
                ////System.out.println("GetBooksByCategory: "+CategoryName+ESelectedCountryID+EUniversityIDInt);
            } else if (serviceType == 5) {

                int categoryId = 0;
                int countryId = 0;
                int universityId = 0;
                if (selectedCategory != null) {
                    categoryId = selectedCategory.getCategoryId();
                }
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }

                if (SearchFilterSelected.equals("Title")) {

                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                            "  <soap:Body>" +
                            "    <GetMindMapsByTitleClient xmlns=\"http://Semanoor.com/\">" +
                            "      <sTitle>"+ ELocalSearchText +"</sTitle>" +
                            "      <iCategoryId>"+ categoryId +"</iCategoryId>" +
                            "      <iCountryID>"+ countryId +"</iCountryID>" +
                            "      <iUniversity>"+ universityId +"</iUniversity>" +
                            "      <ClientID>"+ clientId +"</ClientID>" +
                            "    </GetMindMapsByTitleClient>" +
                            "  </soap:Body>" +
                            "</soap:Envelope>";
                    SOAPAction = "http://Semanoor.com/GetMindMapsByTitleClient";

                    ////System.out.println("GetBooksByTitle :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                } else {
                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                            "  <soap:Body>" +
                            "    <GetMindMapsByAuthorClient xmlns=\"http://Semanoor.com/\">" +
                            "      <sAuthor>"+ ELocalSearchText +"</sAuthor>" +
                            "      <iCategoryId>"+ categoryId +"</iCategoryId>" +
                            "      <iCountryID>"+ countryId +"</iCountryID>" +
                            "      <iUniversity>"+ universityId +"</iUniversity>" +
                            "      <ClientID>"+ clientId +"</ClientID>" +
                            "    </GetMindMapsByAuthorClient>" +
                            "  </soap:Body>" +
                            "</soap:Envelope>";
                    SOAPAction = "http://Semanoor.com/GetMindMapsByAuthorClient";
                    ////System.out.println("GetBooksByAuthor :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                }
            }
            ENVELOPE = String.format(soapMessage, null);
            //String returnXml = CallNewWebService(Globals.mindmapWebServiceURL, SOAPAction, ENVELOPE, tabPosition);   //Call to web services
            String mindMapUrl;
            if (tabPosition<fa_enrich.visitedStore.getChildobjlist().size()) {
                if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl() != null) {
                    mindMapUrl = fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl();
                } else {
                    mindMapUrl = "";
                }
                String returnXml = CallNewWebService(mindMapUrl, SOAPAction, ENVELOPE, tabPosition);
                //System.out.println("returnxml:"+returnXml);
                parseXml(returnXml, tabPosition);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (fa_enrich.updateLists.contains(String.valueOf(tabPosition))){
                fa_enrich.updateLists.remove(String.valueOf(tabPosition));
            }
            if (serviceType == 0) {
                CountryBtn.setEnabled(false);
                UniversityBtn.setEnabled(false);
                CategoriesBtn.setEnabled(false);
                PriceBtn.setEnabled(false);
                serviceType = 1;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 1) {
                serviceType = 2;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 2) {
                serviceType = -1;
            } else if (serviceType == 3) {   //need to check
            } else if (serviceType == 4) {
                CountryBtn.setEnabled(true);
                UniversityBtn.setEnabled(true);
                CategoriesBtn.setEnabled(true);
                PriceBtn.setEnabled(true);

                processGridView();
            } else if (serviceType == 5) {
                processGridView();
            }

            if (serviceType == -1) {
                setDefaultValue();
                serviceType = 4;
                loadSoapCallMethods(this.tabPosition);
            }
        }
    }



        public class mediaCurriculumSoapCall extends AsyncTask<Void, Void, Void> {

            private int tabPosition;

            public mediaCurriculumSoapCall(int position) {
                this.tabPosition = position;
                if (fa_enrich.updateLists.contains(String.valueOf(position))){
                    serviceType = 0;
                    countryList.clear();
                    universityList.clear();
                    categoryList.clear();
                    initializeCountryCategoryList();
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (serviceType == 4 || serviceType == 5) {
                    EbooksOriginal.clear();
                }
            }

            protected Void doInBackground(Void... params) {

                if (serviceType == 0) {
                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                            + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                            + "<soap:Body>"
                            + " <GetCountryList xmlns=\"http://SemaMediaLibraryApi.org/\">"
                            + "</GetCountryList>"
                            + "</soap:Body>"
                            + "</soap:Envelope>";
                    SOAPAction = "http://semanoor.com.sa/GetCountryList";
                } else if (serviceType == 1) {
                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                            + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                            + "<soap:Body>"
                            + "<GetUniversityList xmlns=\"http://SemaMediaLibraryApi.org/\">"
                            + "</GetUniversityList>"
                            + "</soap:Body>"
                            + "</soap:Envelope>";
                    SOAPAction = "http://semanoor.com.sa/GetUniversityList";
                } else if (serviceType == 2) {
                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                            + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                            + "<soap:Body>"
                            + "<GetCategoryList xmlns=\"http://SemaMediaLibraryApi.org/\">"
                            + "</GetCategoryList>"
                            + "</soap:Body>"
                            + "</soap:Envelope>";
                    SOAPAction = "http://semanoor.com.sa/GetCategoryList";
                } else if (serviceType == 3) {
                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                            + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                            + "<soap:Body>"
                            + "GetEBagsByUniversity xmlns=\"http://SemaMediaLibraryApi.org/\">"
                            + "<iUniversity>1</iUniversity>"
                            + "</GetEBagsByUniversity>"
                            + "</soap:Body>"
                            + "</soap:Envelope>";
                    SOAPAction = "http://semanoor.com.sa/GetEBagsByUniversity";
                } else if (serviceType == 4) {
                    int categoryId = 0;
                    int countryId = 0;
                    int universityId = 0;
                    if (selectedCategory != null) {
                        categoryId = selectedCategory.getCategoryId();
                    }
                    if (selectedCountry != null) {
                        countryId = selectedCountry.getCountryId();
                    }
                    if (selectedUniversity != null) {
                        universityId = selectedUniversity.getUniversityId();
                    }
                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                            + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                            + "<soap:Body>"
                            + "<GetEBagsByCategory xmlns=\"http://SemaMediaLibraryApi.org/\">"
                            + "<iCategoryId>" + categoryId + "</iCategoryId>"
                            + "<iCountryID>" + countryId + "</iCountryID>"
                            + "<iUniversity>" + universityId + "</iUniversity>"
                            + "</GetEBagsByCategory>"
                            + "</soap:Body>"
                            + "</soap:Envelope>";
                    SOAPAction = "http://semanoor.com.sa/GetEBagsByCategory";
                    ////System.out.println("GetBooksByCategory: "+CategoryName+ESelectedCountryID+EUniversityIDInt);
                } else if (serviceType == 5) {
                    String CategoryName = "";
                    int categoryId = 0;
                    if (selectedCategory == null || selectedCategory.getCategoryEngName().equals("All")) {
                        CategoryName = "";
                    } else {
                        CategoryName = selectedCategory.getCategoryEngName();
                        categoryId = selectedCategory.getCategoryId();
                    }
                    int countryId = 0;
                    int universityId = 0;
                    if (selectedCountry != null) {
                        countryId = selectedCountry.getCountryId();
                    }
                    if (selectedUniversity != null) {
                        universityId = selectedUniversity.getUniversityId();
                    }



                   /* int categoryId = 0;
                    int countryId = 0;
                    int universityId = 0;
                    if (selectedCategory != null) {
                        categoryId = selectedCategory.getCategoryId();
                    }
                    if (selectedCountry != null) {
                        countryId = selectedCountry.getCountryId();
                    }
                    if (selectedUniversity != null) {
                        universityId = selectedUniversity.getUniversityId();
                    }*/

                    if (SearchFilterSelected.equals("Title")) {
                        soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                                + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                + "<soap:Body>"
                                + "<getCharactersByTitle xmlns=\"http://semanooor.com.sa/\">\n"
                                + "<sTitle>" + ELocalSearchText + "</sTitle>"
                                + "<sCategoryName>" + categoryId + "</sCategoryName>"
                                + "<iCountryID>" + countryId + "</iCountryID>"
                                + "<iUniversity>" + universityId + "</iUniversity>"
                                + "</getCharactersByTitle>"
                                + "</soap:Body>"
                                + "</soap:Envelope>";

                        SOAPAction = "http://semanooor.com.sa/getCharactersByTitle";
                        ////System.out.println("GetBooksByTitle :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                    } else {
                        soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                                + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                + "<soap:Body>"
                                + "<getCharactersByAuthors xmlns=\"http://semanooor.com.sa/\">\n"
                                + "<sAuthorName>" + ELocalSearchText + "</sAuthorName>"
                                + "<sCategoryName>" + categoryId + "</sCategoryName>"
                                + "<iCountryID>" + countryId + "</iCountryID>"
                                + "<iUniversity>" + universityId + "</iUniversity>"
                                + "</getCharactersByAuthors>"
                                + "</soap:Body>"
                                + "</soap:Envelope>";

                        SOAPAction = "http://semanooor.com.sa/getCharactersByAuthors";
                        ////System.out.println("GetBooksByAuthor :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                    }
                }
                ENVELOPE = String.format(soapMessage, null);
                // String returnXml = CallNewWebService(Globals.eLessonWebServiceURL, SOAPAction, ENVELOPE, tabPosition);   //Call to web services
                String mediaUrl;
                if (tabPosition<fa_enrich.visitedStore.getChildobjlist().size()) {
                    if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl() != null) {
                        mediaUrl = fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl();
                    } else {
                        mediaUrl = "";
                    }
                    String returnXml = CallNewWebService(mediaUrl, SOAPAction, ENVELOPE, tabPosition);
                    //System.out.println("returnxml:"+returnXml);
                    parseXml(returnXml, tabPosition);
                }//Call to parse the web services content

                return null;
            }

            protected void onPostExecute(Void result) {
                if (fa_enrich.updateLists.contains(String.valueOf(tabPosition))){
                    fa_enrich.updateLists.remove(String.valueOf(tabPosition));
                }
                if (serviceType == 0) {
                    CountryBtn.setEnabled(false);
                    UniversityBtn.setEnabled(false);
                    CategoriesBtn.setEnabled(false);
                    PriceBtn.setEnabled(false);
                    serviceType = 1;
                    loadSoapCallMethods(this.tabPosition);
                } else if (serviceType == 1) {
                    serviceType = 2;
                    loadSoapCallMethods(this.tabPosition);
                } else if (serviceType == 2) {
                    serviceType = -1;
                } else if (serviceType == 3) {   //need to check
                } else if (serviceType == 4) {
                    CountryBtn.setEnabled(true);
                    UniversityBtn.setEnabled(true);
                    CategoriesBtn.setEnabled(true);
                    PriceBtn.setEnabled(true);

                    processGridView();
                } else if (serviceType == 5) {
                    processGridView();
                }

                if (serviceType == -1) {
                    setDefaultValue();
                    serviceType = 4;
                    loadSoapCallMethods(this.tabPosition);
                }
            }
        }
    public class pdfCurriculumSoapCall extends AsyncTask<Void, Void, Void> {

        private int tabPosition;

        public pdfCurriculumSoapCall(int position) {
            this.tabPosition = position;
            if (fa_enrich.updateLists.contains(String.valueOf(position))){
                serviceType = 0;
                countryList.clear();
                universityList.clear();
                categoryList.clear();
                initializeCountryCategoryList();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (serviceType == 4 || serviceType == 5) {
                EbooksOriginal.clear();
            }
        }

        protected Void doInBackground(Void... params) {

            if (serviceType == 0) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                        + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                        + "<soap:Body>\n"
                        +" <getCountryList xmlns=\"http://semanoor.com.sa/\" />\n"
                        +"</soap:Body>\n" +
                        "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/getCountryList";
            } else if (serviceType == 1) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                        +"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                        "<soap:Body>\n"
                        +"<getUniversityList xmlns=\"http://semanoor.com.sa/\" />\n"
                        +"</soap:Body>\n"
                        +"</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/getUniversityList";
            } else if (serviceType == 2) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                        "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                        +"<soap:Body>\n"
                        +"<getCategoryList xmlns=\"http://semanoor.com.sa/\" />\n"
                        +"</soap:Body>\n"
                        +"</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/getCategoryList";
            } else if (serviceType == 3) {
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                        "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                        +"<soap:Body>\n"
                        +"<getCharactersByUniversity xmlns=\"http://semanooor.com.sa/\">\n"
                        +"<iUniversity>%i</iUniversity>\n"
                        +"</getCharactersByUniversity>\n"
                        +"</soap:Body>\n"
                        +"</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/getCharactersByUniversity";
            } else if (serviceType == 4) {
                /*int categoryId = 0;
                int countryId = 0;
                int universityId = 0;
                if (selectedCategory != null) {
                    categoryId = selectedCategory.getCategoryId();
                }
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }   */
                EbooksOriginal.clear();
                String CategoryName = "";
                if (selectedCategory == null || selectedCategory.getCategoryEngName().equals("All")) {
                    CategoryName = "";
                } else {
                    CategoryName = selectedCategory.getCategoryEngName();
                }
                int countryId = 0;
                int universityId = 0;
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }
              /*  soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                        "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                        "<soap:Body>\n"
                        +"<getPdfsByCategory xmlns=\"http://semanoor.com.sa/\">\n"
                        +"<CategoryName>"+CategoryName +"</CategoryName>\n"
                        +"<CountryID>"+countryId+"</CountryID>\n"
                        +"<UniversityID>"+universityId+"</UniversityID>\n"
                        +"</getPdfsByCategory>\n"
                        +"</soap:Body>\n" +
                        "</soap:Envelope>";*/
                soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                        "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                        "<soap:Body>\n" +
                        "<getPdfsByCategoryClient xmlns=\"http://semanoor.com.sa/\">\n" +
                        "<CategoryName>"+CategoryName +"</CategoryName>\n" +
                        "<CountryID>"+countryId+"</CountryID>\n" +
                        "<UniversityID>"+universityId+"</UniversityID>\n" +
                        "<ClientID>"+clientId+"</ClientID>\n" +
                        "</getPdfsByCategoryClient>\n" +
                        "</soap:Body>\n" +
                        "</soap:Envelope>";
                SOAPAction = "http://semanoor.com.sa/getPdfsByCategoryClient";
                ////System.out.println("GetBooksByCategory: "+CategoryName+ESelectedCountryID+EUniversityIDInt);
            } else if (serviceType == 5) {
                String CategoryName = "";
                int categoryId = 0;
                if (selectedCategory == null || selectedCategory.getCategoryEngName().equals("All")) {
                    CategoryName = "";
                } else {
                    CategoryName = selectedCategory.getCategoryEngName();
                    categoryId = selectedCategory.getCategoryId();
                }
                int countryId = 0;
                int universityId = 0;
                if (selectedCountry != null) {
                    countryId = selectedCountry.getCountryId();
                }
                if (selectedUniversity != null) {
                    universityId = selectedUniversity.getUniversityId();
                }



                   /* int categoryId = 0;
                    int countryId = 0;
                    int universityId = 0;
                    if (selectedCategory != null) {
                        categoryId = selectedCategory.getCategoryId();
                    }
                    if (selectedCountry != null) {
                        countryId = selectedCountry.getCountryId();
                    }
                    if (selectedUniversity != null) {
                        universityId = selectedUniversity.getUniversityId();
                    }*/

                if (SearchFilterSelected.equals("Title")) {
                 /*   soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                            "<soap:Body>\n" +
                            "<getPdfsByTitle xmlns=\"http://semanoor.com.sa/\">\n"
                            +"<Title>"+ELocalSearchText+"</Title>\n"
                            +"<CategoryName>"+CategoryName+"</CategoryName>\n"
                            +"<CountryID>"+countryId +"</CountryID>\n"
                            +"<UniversityID>"+universityId +"</UniversityID>\n"
                            +"</getPdfsByTitle>\n"
                            +"</soap:Body>\n"
                            +"</soap:Envelope>";*/

                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                            "  <soap:Body>\n" +
                            "    <getPdfsByTitleClient xmlns=\"http://semanoor.com.sa/\">\n" +
                            "      <Title>"+ELocalSearchText+"</Title>\n" +
                            "      <CategoryName>"+CategoryName+"</CategoryName>\n" +
                            "      <CountryID>"+countryId +"</CountryID>\n" +
                            "      <UniversityID>"+universityId +"</UniversityID>\n" +
                            "      <ClientID>"+clientId+"</ClientID>\n" +
                            "    </getPdfsByTitleClient>\n" +
                            "  </soap:Body>\n" +
                            "</soap:Envelope>";

                    SOAPAction = "http://semanoor.com.sa/getPdfsByTitleClient";
                    ////System.out.println("GetBooksByTitle :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                } else {
                  /*  soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                            "<soap:Body>\n"
                            +"<getPdfsByAuthors xmlns=\"http://semanoor.com.sa/\">\n"
                            +"<AuthorName>"+ELocalSearchText+"</AuthorName>\n"
                            +"<CategoryName>"+CategoryName +"</CategoryName>\n"
                            +"<CountryID>"+countryId +"</CountryID>\n"
                            +"<UniversityID>"+universityId +"</UniversityID>\n"
                            +"</getPdfsByAuthors>\n" +
                            "</soap:Body>\n" +
                            "</soap:Envelope>";*/

                    soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                            "  <soap:Body>\n" +
                            "    <getPdfsByAuthorsClient xmlns=\"http://semanoor.com.sa/\">\n" +
                            "      <AuthorName>"+ELocalSearchText+"</AuthorName>\n" +
                            "      <CategoryName>"+CategoryName+"</CategoryName>\n" +
                            "      <CountryID>"+countryId+"</CountryID>\n" +
                            "      <UniversityID>"+universityId+"</UniversityID>\n" +
                            "      <ClientID>"+clientId+"</ClientID>\n" +
                            "    </getPdfsByAuthorsClient>\n" +
                            "  </soap:Body>\n" +
                            "</soap:Envelope>";

                    SOAPAction = "http://semanoor.com.sa/getPdfsByAuthorsClient";
                    ////System.out.println("GetBooksByAuthor :"+ELocalSearchText+CategoryName+selectCID +selectUID );
                }
            }
            ENVELOPE = String.format(soapMessage, null);
            // String returnXml = CallNewWebService(Globals.eLessonWebServiceURL, SOAPAction, ENVELOPE, tabPosition);   //Call to web services
            String pdfUrl;
            if (tabPosition<fa_enrich.visitedStore.getChildobjlist().size()) {
                if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl() != null) {
                    pdfUrl = fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getUrl();
                } else {
                    pdfUrl = "";
                }
                String returnXml = CallNewWebService(pdfUrl, SOAPAction, ENVELOPE, tabPosition);
                //System.out.println("returnxml:"+returnXml);
                parseXml(returnXml, tabPosition);
            }//Call to parse the web services content

            return null;
        }

        protected void onPostExecute(Void result) {
            if (fa_enrich.updateLists.contains(String.valueOf(tabPosition))){
                fa_enrich.updateLists.remove(String.valueOf(tabPosition));
            }
            if (serviceType == 0) {
                CountryBtn.setEnabled(false);
                UniversityBtn.setEnabled(false);
                CategoriesBtn.setEnabled(false);
                PriceBtn.setEnabled(false);
                serviceType = 1;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 1) {
                serviceType = 2;
                loadSoapCallMethods(this.tabPosition);
            } else if (serviceType == 2) {
                serviceType = -1;
            } else if (serviceType == 3) {   //need to check
            } else if (serviceType == 4) {
                CountryBtn.setEnabled(true);
                UniversityBtn.setEnabled(true);
                CategoriesBtn.setEnabled(true);
                PriceBtn.setEnabled(true);

                processGridView();
            } else if (serviceType == 5) {
                processGridView();
            }

            if (serviceType == -1) {
                setDefaultValue();
                serviceType = 4;
                loadSoapCallMethods(this.tabPosition);
            }
        }
    }


        private void countrySelected(int selectCountryValue, int selectUniversityValue) {
        if (countryList != null  && countryList.size()>selectCountryValue) {
            selectedCountry = countryList.get(selectCountryValue);
        }
        filteredUniversityList.clear();
        University allUniversity = new University();
        allUniversity.setUniversityId(0);
        allUniversity.setUniversityCountryId(0);
        allUniversity.setUniversityEngName("All");
        allUniversity.setUniversityArabName(allArabName);
        allUniversity.setUniversityFlag("");
        filteredUniversityList.add(allUniversity);
        for (int i = 0; i < universityList.size(); i++) {
            University university = universityList.get(i);
            if (selectedCountry.getCountryId() != 0) {
                if (selectedCountry.getCountryId() == university.getUniversityCountryId()) {
                    filteredUniversityList.add(university);
                }
            } else {
                filteredUniversityList.add(university);
            }
        }

        if (filteredUniversityList.size() > 1 &&filteredUniversityList.size()>selectUniversityValue) {
            selectedUniversity = filteredUniversityList.get(selectUniversityValue);
        }
    }

    public void setDefaultValue() {
        if (countryList.size() != 0 && countryList.size()>defSelectedCountryValue) {
            countrySelected(defSelectedCountryValue, defSelectedUniversityValue);
        }
        if (categoryList.size() != 0  &&categoryList.size()>defSelectedCategoryValue) {
            selectedCategory = categoryList.get(defSelectedCategoryValue);
        }
        //if (language == 1) {
        if (countryList.size() == 0) {
            CountryBtn.setText("");
        } else {
            String countryLabel=null;
            if(selectedCountry!=null) {
                if (fa_enrich.language.equals("en")) {
                    countryLabel = selectedCountry.getCountryEngName();
                } else {
                    countryLabel = selectedCountry.getCountryArabName();
                }
            }
            CountryBtn.setText(countryLabel);
        }
        if (universityList.size() == 0) {
            UniversityBtn.setText("");
        } else {
            String universityLabel = null;
            if (fa_enrich.language.equals("en")) {
                if (selectedUniversity != null) {
                    universityLabel = selectedUniversity.getUniversityEngName();
                }
            } else {
                if (selectedUniversity != null) {
                    universityLabel = selectedUniversity.getUniversityArabName();
                }
            }
            UniversityBtn.setText(universityLabel);
        }
        if (categoryList.size() == 0) {
            CategoriesBtn.setText("");
        } else {
            String categoriesLabel=null;
            if(selectedCategory!=null) {
                if (fa_enrich.language.equals("en")) {
                    categoriesLabel = selectedCategory.getCategoryEngName();
                } else {
                    categoriesLabel = selectedCategory.getCategoryArabName();
                }
            }
            CategoriesBtn.setText(categoriesLabel);
        }
        //}
    }

    /*public void setDefaultValue(){
		ESelectedCountryID = 0;
		EUniversityIDInt = 1;
		ELocalCategoryName = "all";
		EselectedCountryIndex = 1;
		EselectedPublisherIndex = 1;
		EselectedCategoriesIndex = 1;
		if(language == 1){
			if(EcountryArray.size() == 0){
				CountryBtn.setText("");
			}else{
				String countryLabel = EcountryArray.get(EselectedCountryIndex).split("#")[1];
				if(countryLabel.length()>5)
					countryLabel = countryLabel.substring(0,6)+"..";
				CountryBtn.setText(countryLabel);
			}
			if(EuniversityArray.size() == 0){
				UniversityBtn.setText("");
			}else{
				String universityLabel = filteredEuniversityArray.get(EselectedPublisherIndex).split("#")[1];
				if(universityLabel.length()>5)
					universityLabel = universityLabel.substring(0,6)+"..";
				UniversityBtn.setText(universityLabel);
			}
			if(EcategoryArray.size() == 0){
				CategoriesBtn.setText("");
			}else{
				String categoriesLabel = EcategoryArray.get(EselectedCategoriesIndex).split("#")[2];
				if(categoriesLabel.length()>5)
					categoriesLabel = categoriesLabel.substring(0,6)+"..";
				CategoriesBtn.setText(categoriesLabel);
			}
		}
		else{
			if(EcountryArray.size() == 0){
				CountryBtn.setText("");
			}else{
				String countryLabel = EcountryArray.get(EselectedCountryIndex).split("#")[0];
				if(countryLabel.length()>6)
					countryLabel = countryLabel.substring(0,6)+"..";
				CountryBtn.setText(countryLabel);
			}
			if(EuniversityArray.size() == 0){
				UniversityBtn.setText("");
			}else{
				String universityLabel = filteredEuniversityArray.get(EselectedPublisherIndex).split("#")[0];
				if(universityLabel.length()>6)
					universityLabel = universityLabel.substring(0,6)+"..";
				UniversityBtn.setText(universityLabel);
			}
			if(EcategoryArray.size() == 0){
				CategoriesBtn.setText("");
			}else{
				String categoriesLabel = EcategoryArray.get(EselectedCategoriesIndex).split("#")[0];
				if(categoriesLabel.length()>6)
					categoriesLabel = categoriesLabel.substring(0,6)+"..";
				CategoriesBtn.setText(categoriesLabel);
			}
		}
	}*/
	/*
	 *  Web service call to get details of Countries list, Universities list, Categories list and Books list
	 */
    private String CallWebService(String url, String sOAPAction, String eNVELOPE, final int tabPosition) {
        final DefaultHttpClient httpClient = new DefaultHttpClient();
        // request parameters
        HttpParams params = httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, 60000);
        HttpConnectionParams.setSoTimeout(params, 60000);
        // set parameter
        HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);
        // POST the envelope
        HttpPost httppost = new HttpPost(url);
        // add headers
        httppost.setHeader("SOAPAction", sOAPAction);
        httppost.setHeader("Content-Type", "text/xml; charset=utf-8");
        String responseString = "Nothingggg";
        try {
            // the entity holds the request
            HttpEntity entity = new StringEntity(eNVELOPE, HTTP.UTF_8);  //Changes
            httppost.setEntity(entity);
            // Response handler
            ResponseHandler<String> rh = new ResponseHandler<String>() {
                // invoked when client receives response
                public String handleResponse(HttpResponse response)
                        throws ClientProtocolException, IOException {
                    // get response entity
                    HttpEntity entity = response.getEntity();
                    //read the response as byte array
                    StringBuffer out = new StringBuffer();
                    byte[] b = EntityUtils.toByteArray(entity);
                    // write the response byte array to a string buffer
                    out.append(new String(b, 0, b.length));
                    String responseXml = out.toString(); //&gt;&lt; 
                    String finalStr = responseXml.replace("&lt;", "<");
                    finalStr = finalStr.replace("&gt;", ">");
                    String splitStr[] = {""};
                    String finalSplitStr[] = {""};
                    if (serviceType == 0) {
                         //if (tabPosition == Globals.eLessonStore || tabPosition == Globals.mindmapStore) {
                        if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.eLessonStore)||fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore) ||fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mediaStore)){
                            splitStr = finalStr.split("<GetCountryListResult>");
                            finalSplitStr = splitStr[1].split("</GetCountryListResult>");
                        } else {
                            splitStr = finalStr.split("<getCountryListResult>");
                            finalSplitStr = splitStr[1].split("</getCountryListResult>");
                        }
                    } else if (serviceType == 1) {
                        if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.eLessonStore)||fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore)||fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mediaStore)){
                            // if (tabPosition == Globals.eLessonStore || tabPosition == Globals.mindmapStore) {
                            splitStr = finalStr.split("<GetUniversityListResult>");
                            finalSplitStr = splitStr[1].split("</GetUniversityListResult>");
                        } else {
                            splitStr = finalStr.split("<getUniversityListResult>");
                            finalSplitStr = splitStr[1].split("</getUniversityListResult>");
                        }
                        ////System.out.println("finalSplitStr:"+finalSplitStr);
                    } else if (serviceType == 2) {
                        if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.eLessonStore)||fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore)||fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mediaStore)){
                            //if (tabPosition == Globals.eLessonStore || tabPosition == Globals.mindmapStore) {
                            splitStr = finalStr.split("<GetCategoryListResult>");
                            finalSplitStr = splitStr[1].split("</GetCategoryListResult>");
                        } else {
                            splitStr = finalStr.split("<getCategoryListResult>");
                            finalSplitStr = splitStr[1].split("</getCategoryListResult>");
                        }
                    } else if (serviceType == 3) {
                    } else if (serviceType == 4) {
                        if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.eLessonStore)){
                            //if (tabPosition == ""+Globals.eLessonStore) {
                            splitStr = finalStr.split("<GetEBagsByCategoryResult>");
                            finalSplitStr = splitStr[1].split("</GetEBagsByCategoryResult>");
                        } else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.rzooomStore)){
                            //if (tabPosition == Globals.rzooomStore) {
                            splitStr = finalStr.split("<getCharactersByCategoryResult>");
                            finalSplitStr = splitStr[1].split("</getCharactersByCategoryResult>");
                        } else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore)){
                           //if (tabPosition == Globals.mindmapStore) {
                            splitStr = finalStr.split("<GetMindMapsByCategoryResult>");
                            finalSplitStr = splitStr[1].split("</GetMindMapsByCategoryResult>");
                        }else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mediaStore)){
                            splitStr = finalStr.split("<GetEBagsByCategoryResult>");
                            finalSplitStr = splitStr[1].split("</GetEBagsByCategoryResult>");
                        }
                        else{
                            splitStr = finalStr.split("<getBooksByCategoryResult>");
                            finalSplitStr = splitStr[1].split("</getBooksByCategoryResult>");
                        }
                    } else if (serviceType == 5) {
                        if (SearchFilterSelected.equals("Title")) {
                            if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.eLessonStore) ||fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mediaStore)){
                                 // if (tabPosition == Globals.eLessonStore) {
                                splitStr = finalStr.split("<GetEBagsByTitleResult>");
                                finalSplitStr = splitStr[1].split("</GetEBagsByTitleResult>");
                            } else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.rzooomStore)){
                               //if (tabPosition == Globals.rzooomStore) {
                                splitStr = finalStr.split("<getCharactersByTitleResult>");
                                finalSplitStr = splitStr[1].split("</getCharactersByTitleResult>");
                            } else  if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore)){
                                //if (tabPosition == Globals.mindmapStore){
                                splitStr = finalStr.split("<GetMindMapsByTitleResult>");
                                finalSplitStr = splitStr[1].split("</GetMindMapsByTitleResult>");
                            }else{
                                splitStr = finalStr.split("<getBooksByTitleResult>");
                                finalSplitStr = splitStr[1].split("</getBooksByTitleResult>");
                            }
                        } else {
                            if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.eLessonStore)||fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mediaStore)){
                                //  if (tabPosition == Globals.eLessonStore) {
                                splitStr = finalStr.split("<getBooksByAuthorsResult>");
                                finalSplitStr = splitStr[1].split("</getBooksByAuthorsResult>");
                            } else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.rzooomStore)){
                               //if (tabPosition == Globals.rzooomStore) {
                                splitStr = finalStr.split("<getCharactersByAuthorsResult>");
                                finalSplitStr = splitStr[1].split("</getCharactersByAuthorsResult>");
                            } else  if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore)){
                               //if (tabPosition == Globals.mindmapStore) {
                                splitStr = finalStr.split("<GetMindMapsByAuthorResult>");
                                finalSplitStr = splitStr[1].split("</GetMindMapsByAuthorResult>");
                            } else{
                                splitStr = finalStr.split("<getBooksByAuthorsResult>");
                                finalSplitStr = splitStr[1].split("</getBooksByAuthorsResult>");
                            }
                        }
                    }
                    return finalSplitStr[0];
                }
            };
            responseString = httpClient.execute(httppost, rh);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("me", "Exc : " + e.toString());
        }
        // close the connection
        httpClient.getConnectionManager().shutdown();
        return responseString;
    }

    private String CallNewWebService(String url, String sOAPAction, String eNVELOPE, final int tabPosition){
        HttpURLConnection urlConnection = null;
        String finalSplitStr[] = {""};
        try {
            URL urlToRequest = new URL(url);

            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(25000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            urlConnection.setRequestProperty("SOAPAction", sOAPAction);

            OutputStream out = urlConnection.getOutputStream();
            out.write(eNVELOPE.getBytes());
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

            } else if (statusCode != HttpURLConnection.HTTP_OK) {

            }

            InputStream in = urlConnection.getInputStream();
            String returnResponse = UserFunctions.convertStreamToString(in);

            if (returnResponse == null || returnResponse.equals("")){
                return returnResponse;
            }

            String finalStr = returnResponse.replace("&lt;", "<");
            finalStr = finalStr.replace("&gt;", ">");
            String splitStr[] = {""};

            if (tabPosition<fa_enrich.visitedStore.getChildobjlist().size()) {

                if (serviceType == 0) {
                    //if (tabPosition == Globals.eLessonStore || tabPosition == Globals.mindmapStore) {
                    if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.eLessonStore) || fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mindmapStore) || fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mediaStore)) {
                        splitStr = finalStr.split("<GetCountryListResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</GetCountryListResult>");
                        }
                    } else {
                        splitStr = finalStr.split("<getCountryListResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</getCountryListResult>");
                        }
                    }
                } else if (serviceType == 1) {
                    if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.eLessonStore) || fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mindmapStore) || fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mediaStore)) {
                        // if (tabPosition == Globals.eLessonStore || tabPosition == Globals.mindmapStore) {
                        splitStr = finalStr.split("<GetUniversityListResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</GetUniversityListResult>");
                        }
                    } else {
                        splitStr = finalStr.split("<getUniversityListResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</getUniversityListResult>");
                        }
                    }
                    ////System.out.println("finalSplitStr:"+finalSplitStr);
                } else if (serviceType == 2) {
                    if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.eLessonStore) || fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mindmapStore) || fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mediaStore)) {
                        //if (tabPosition == Globals.eLessonStore || tabPosition == Globals.mindmapStore) {
                        splitStr = finalStr.split("<GetCategoryListResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</GetCategoryListResult>");
                        }
                    } else {
                        splitStr = finalStr.split("<getCategoryListResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</getCategoryListResult>");
                        }
                    }
                } else if (serviceType == 3) {
                } else if (serviceType == 4) {
                    if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.eLessonStore)) {
                        //if (tabPosition == ""+Globals.eLessonStore) {
                        splitStr = finalStr.split("<GetEBagsByCategoryClientResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</GetEBagsByCategoryClientResult>");
                        }
                    } else if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.rzooomStore)) {
                        //if (tabPosition == Globals.rzooomStore) {
                        splitStr = finalStr.split("<getCharactersByCategoryClientResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</getCharactersByCategoryClientResult>");
                        }
                    } else if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mindmapStore)) {
                        //if (tabPosition == Globals.mindmapStore) {
                        splitStr = finalStr.split("<GetMindMapsByCategoryClientResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</GetMindMapsByCategoryClientResult>");
                        }
                    } else if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mediaStore)) {
                        splitStr = finalStr.split("<GetEBagsByCategoryResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</GetEBagsByCategoryResult>");
                        }
                    } else if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.pdfStore)) {
                        splitStr = finalStr.split("<getPdfsByCategoryClientResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</getPdfsByCategoryClientResult>");
                        }
                    } else {
                        splitStr = finalStr.split("<getBooksByCategoryClientResult>");
                        if (splitStr.length >= 2) {
                            finalSplitStr = splitStr[1].split("</getBooksByCategoryClientResult>");
                        }
                    }
                } else if (serviceType == 5) {
                    if (SearchFilterSelected.equals("Title")) {
                        if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.eLessonStore) || fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mediaStore)) {
                            // if (tabPosition == Globals.eLessonStore) {
                            splitStr = finalStr.split("<GetEBagsByTitleClientResult>");
                            if (splitStr.length >= 2) {
                                finalSplitStr = splitStr[1].split("</GetEBagsByTitleClientResult>");
                            }
                        } else if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.rzooomStore)) {
                            //if (tabPosition == Globals.rzooomStore) {
                            splitStr = finalStr.split("<getCharactersByByTitleClientResult>");
                            if (splitStr.length >= 2) {
                                finalSplitStr = splitStr[1].split("</getCharactersByByTitleClientResult>");
                            }
                        } else if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mindmapStore)) {
                            //if (tabPosition == Globals.mindmapStore){
                            splitStr = finalStr.split("<GetMindMapsByTitleClientResult>");
                            if (splitStr.length >= 2) {
                                finalSplitStr = splitStr[1].split("</GetMindMapsByTitleClientResult>");
                            }
                        } else if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.pdfStore)) {
                            splitStr = finalStr.split("<getPdfsByTitleClientResult");
                            if (splitStr.length >= 2) {
                                finalSplitStr = splitStr[1].split("</getPdfsByTitleClientResult>");
                            }
                        } else {
                            splitStr = finalStr.split("<getBooksByTitleClientResult>");
                            if (splitStr.length >= 2) {
                                finalSplitStr = splitStr[1].split("</getBooksByTitleClientResult>");
                            }
                        }
                    } else {
                        if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.eLessonStore) || fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mediaStore)) {
                            //  if (tabPosition == Globals.eLessonStore) {
                            splitStr = finalStr.split("<GetEBagsByTagsClientResult>");
                            if (splitStr.length >= 2) {
                                finalSplitStr = splitStr[1].split("</GetEBagsByTagsClientResult>");
                            }
                        } else if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.rzooomStore)) {
                            //if (tabPosition == Globals.rzooomStore) {
                            splitStr = finalStr.split("<getCharactersByAuthorNameClientResult>");
                            if (splitStr.length >= 2) {
                                finalSplitStr = splitStr[1].split("</getCharactersByAuthorNameClientResult>");
                            }
                        } else if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.mindmapStore)) {
                            //if (tabPosition == Globals.mindmapStore) {
                            splitStr = finalStr.split("<GetMindMapsByAuthorClientResult>");
                            if (splitStr.length >= 2) {
                                finalSplitStr = splitStr[1].split("</GetMindMapsByAuthorClientResult>");
                            }
                        } else if (fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals("" + Globals.pdfStore)) {
                            //if (tabPosition == Globals.mindmapStore) {
                            splitStr = finalStr.split("<getPdfsByAuthorClientResult>");
                            if (splitStr.length >= 2) {
                                finalSplitStr = splitStr[1].split("</getPdfsByAuthorClientResult>");
                            }
                        } else {
                            splitStr = finalStr.split("<getBooksByAuthorsClientResult>");
                            if (splitStr.length >= 2) {
                                finalSplitStr = splitStr[1].split("</getBooksByAuthorsClientResult>");
                            }
                        }
                    }
                }
            }
        } catch (MalformedURLException e) {

        } catch (SocketTimeoutException e) {

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return finalSplitStr[0];
    }


    /*
	 *  Parse Xml method for parsing the return string of Webservice calls
	 */
    private void parseXml(String returnXml, int tabPosition) {
        try {
            SAXParserFactory mySAXParserFactory = SAXParserFactory.newInstance();
            SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
            XMLReader myXMLReader = mySAXParser.getXMLReader();
            MyXmlHandler myRSSHandler = new MyXmlHandler(tabPosition);
            myXMLReader.setContentHandler(myRSSHandler);
            InputSource inputsource = new InputSource();
            inputsource.setEncoding("UTF-8");
            inputsource.setCharacterStream(new StringReader(returnXml));
            myXMLReader.parse(inputsource);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class MyXmlHandler extends DefaultHandler {
        private boolean b_country = false;
        private boolean b_university = false;
        private boolean b_category = false;
        private boolean b_booksByCategory = false;
        private int tabPosition;

        public MyXmlHandler(int tabPosition) {
            this.tabPosition = tabPosition;
        }

        public void startDocument() throws SAXException {
            //super.startDocument();
        }

        public void endDocument() throws SAXException {
            //super.endDocument();
        }

        public void startElement(String uri, String localName, String qName,
                                 Attributes attributes) throws SAXException {
            //super.startElement(uri, localName, qName, attributes);
            ////System.out.println("In parse Start Element Method with localname: "+localName);
            if (serviceType == 0) {
                if (localName.equals("Country")) {
                    Country country = new Country();
                    country.setCountryId(Integer.parseInt(attributes.getValue("ID")));
                    country.setCountryEngName(attributes.getValue("EnglishName"));
                    country.setCountryArabName(attributes.getValue("ArabicName"));
                    country.setCountryFlagName(attributes.getValue("Flag"));
                    if (countryIdExistForClient(country.getCountryId())) {
                        countryList.add(country);
                    }
                    this.b_country = true;
                }
            }
            if (serviceType == 1) {
                if (localName.equals("University")) {
                    University university = new University();
                    university.setUniversityId(Integer.parseInt(attributes.getValue("ID")));
                    //if (tabPosition == Globals.rzooomStore) {
                   if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.rzooomStore)){
                        university.setUniversityCountryId(Integer.parseInt(attributes.getValue("Country_ID")));
                    } else {
                        university.setUniversityCountryId(Integer.parseInt(attributes.getValue("CountryID")));
                    }
                    university.setUniversityEngName(attributes.getValue("EnglishName"));
                    university.setUniversityArabName(attributes.getValue("ArabicName"));
                    university.setUniversityFlag(attributes.getValue("Flag"));
                    if (universityIdExistForClient(university.getUniversityId())) {
                        universityList.add(university);
                    }
                    this.b_university = true;
                }
            }

            if (serviceType == 2) {
                if (localName.equals("Category")) {
                    Category category = new Category();
                    category.setCategoryId(Integer.parseInt(attributes.getValue("ID")));
                    category.setCategoryEngName(attributes.getValue("EnglishName"));
                    category.setCategoryArabName(attributes.getValue("ArabicName"));
                    if (categoryIdExistForClient(category.getCategoryId())) {
                        categoryList.add(category);
                    }
                }
                this.b_category = true;
            }

            if (serviceType == 3) {
                String xmlTagValue;
                String store;
                int coverEndPage = 0;
              //  if (tabPosition == Globals.eLessonStore) {
                if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.eLessonStore)){
                    xmlTagValue = "EBag";
                    store = fa_enrich.visitedStore.getId()+"E";
                } else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.quizStore)){    //if (tabPosition == ""+Globals.quizStore) {
                    store = fa_enrich.visitedStore.getId()+"Q";
                    xmlTagValue = "book";
                } else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.rzooomStore)){// if (tabPosition == Globals.rzooomStore) {
                    store = fa_enrich.visitedStore.getId()+"R";
                    xmlTagValue = "book";
                } else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore)){//if (tabPosition == Globals.mindmapStore) {
                    store = fa_enrich.visitedStore.getId()+"I";
                    xmlTagValue = "MindMap";
                }else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mediaStore)){
                    store = fa_enrich.visitedStore.getId()+"A";
                    xmlTagValue = "Media";
                }else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.pdfStore)){
                    store = fa_enrich.visitedStore.getId()+"P";
                    xmlTagValue = "Pdf";
                }else  {
                    store = fa_enrich.visitedStore.getId()+"M";
                    xmlTagValue = "book";
                    if (Globals.isLimitedVersion()) {
                        coverEndPage = 2;
                    } else {
                        coverEndPage = 0;
                    }
                }
                if (localName.equals(xmlTagValue)) {
                    String bookID = attributes.getValue("ID");

                    String book = store.concat(bookID);   //Changes after merge
                    book = bookID.replaceFirst(bookID, book);
                    String bookURL = attributes.getValue("Url");
                    String title = attributes.getValue("Title");
                    String author = attributes.getValue("Author");

                    String totalPagesStr = attributes.getValue("totalpages");
                    String totalPages = null;
                    if (totalPagesStr != null) {
                        int totalPg = Integer.parseInt(totalPagesStr) + coverEndPage;
                        totalPages = String.valueOf(totalPg);
                    }

                    String searchtext = attributes.getValue("search");
                    String bookmarktext = attributes.getValue("bookmark");
                    String copytext = attributes.getValue("copy");
                    String flip = attributes.getValue("flip");
                    String navigation = attributes.getValue("nextPrevious");
                    String highlight = attributes.getValue("highlight");
                    String note = attributes.getValue("note");
                    String goToPage = attributes.getValue("goToPage");
                    String indexPage = attributes.getValue("Index");
                    String zoom = attributes.getValue("zoom");
                    String webSearch = attributes.getValue("websearch");
                    String wikiSearch = attributes.getValue("wikipediasearch");
                    String updateCounts = attributes.getValue("UpdateCounts");
                    String youtube = attributes.getValue("youtubeSearch");
                    String google = attributes.getValue("googleSearch");
                    String jazira = attributes.getValue("jazira");
                    String bing = attributes.getValue("bing");
                    String ask = attributes.getValue("ask");
                    String yahoo = attributes.getValue("yahoo");
                    String noor = attributes.getValue("nooorSearch");
                    String noorbook = attributes.getValue("nooorBookSearch");
                    String translationSearch = attributes.getValue("TranslationSearch");
                    String dictionarySearch = attributes.getValue("dictionarySearch");
                    String bookSearch = attributes.getValue("booksearch");
                    String bookLanguage = attributes.getValue("Language");
                    String coverPageName, MapID = null, BundleMapID = null, IsBundle = null, price, description;
                    if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getSubStoreEn().equals("MindMap")){  //if (tabPosition == Globals.mindmapStore) {
                        price = attributes.getValue("Price");
                        description = attributes.getValue("Description");
                        coverPageName = attributes.getValue("CoverPage");
                        MapID = attributes.getValue("MapID");
                        BundleMapID = attributes.getValue("BundleMapID");
                        IsBundle = attributes.getValue("IsBundle");
                    } else {
                        price = attributes.getValue("price");
                        description = attributes.getValue("Discription");
                        coverPageName = attributes.getValue("coverpage");
                    }
                    String bookSize = attributes.getValue("Size");

                    String inAppProductID;
                    if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.eLessonStore) || fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore)|| fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.pdfStore)){   //   if (tabPosition == Globals.eLessonStore || tabPosition == Globals.mindmapStore) {
                        inAppProductID = fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getPrefixName()+ attributes.getValue("ID");
                    } else {
                        inAppProductID = attributes.getValue("InAppProductID").toLowerCase();
                    }

                    String[] imageUrlArray = bookURL.split("/");  //http://school.nooor.com/ipadadmin/ipadbooks/2012/ipad-72-328-0-2_2.zip
                    //	    				try {
                    //							URL Url = new URL("http://http://school.nooor.com/ipadadmin/ipadbooks/2012/ipad-72-328-0-2_2.gif ");//changes
                    //						} catch (MalformedURLException e) {
                    //							// TODO Auto-generated catch block
                    //							e.printStackTrace();
                    //						}
                    int imgInt = imageUrlArray.length;
                    //String imageName = imageUrlArray[imgInt-1];  
                    String imgConcat = "";
                    for (int i = 0; i < imgInt - 1; i++) {
                        imgConcat = imgConcat.concat(imageUrlArray[i] + "/");
                    }
                    String imgURL = imgConcat.concat(coverPageName); //Verified http://School.nooor.com/Semalibrary/Sboook/Iphone/a783ac60-096a-4ae2-94ca-5079473b13c2/Card.gif
                    //  //System.out.println("imgURL:"+imgURL);
                    String addToBook = "";
                    addToBook = addToBook.concat(book + "#" + price + "#" + bookURL + "#" + title + "#" + description + "#" + author + "#" + imgURL + "#" + totalPages + "#" + searchtext + "#" + bookmarktext + "#" + copytext + "#" + flip + "#" + navigation + "#" + highlight + "#" + note + "#" + goToPage + "#" + indexPage + "#" + zoom + "#" + webSearch + "#" + wikiSearch + "#" + updateCounts + "#" + youtube + "#" + google + "#" + jazira + "#" + bing + "#" + ask + "#" + yahoo + "#" + noor + "#" + noorbook + "#" + inAppProductID + "#" + translationSearch + "#" + dictionarySearch + "#" + bookSearch + "#" + bookLanguage + "#" + tabPosition + "#" + bookSize + "#" + MapID + "#" + BundleMapID + "#" + IsBundle);
                    EbooksOriginal.add(addToBook);
                }
            }
            if ((serviceType == 4) || (serviceType == 5)) {
                int coverEndPage = 0;
                String xmlTagValue;
                String store = null;
                if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.eLessonStore)){ //if (tabPosition == Globals.eLessonStore) {
                    xmlTagValue = "EBag";
                    store = fa_enrich.visitedStore.getId()+"E";
                } else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.quizStore)){   //if (tabPosition == Globals.quizStore) {
                    store = fa_enrich.visitedStore.getId()+"Q";
                    xmlTagValue = "book";
                } else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.rzooomStore)){   //if (tabPosition == Globals.rzooomStore) {
                    store = fa_enrich.visitedStore.getId()+"R";
                    xmlTagValue = "book";
                } else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore)){  //if (tabPosition == Globals.mindmapStore) {
                    store = fa_enrich.visitedStore.getId()+"I";
                    xmlTagValue = "MindMap";
                }else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mediaStore)){
                    store = fa_enrich.visitedStore.getId()+"A";
                    xmlTagValue = "Media";
                }else if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.pdfStore)){
                    store = fa_enrich.visitedStore.getId()+"P";
                    xmlTagValue = "Pdf";
                }else {
                    store = fa_enrich.visitedStore.getId()+"M";
                    xmlTagValue = "book";
                    if (Globals.isLimitedVersion()) {
                        coverEndPage = 2;
                    } else {
                        coverEndPage = 0;
                    }
                }
                if (localName.equals(xmlTagValue)) {
                    String bookID = attributes.getValue("ID");
                    String book = store.concat(bookID);   //Changes after merge
                    book = bookID.replaceFirst(bookID, book);

                    String bookURL = attributes.getValue("Url");
                    String title = attributes.getValue("Title");

                    String author = attributes.getValue("Author");

                    String totalPagesStr = attributes.getValue("totalpages");
                    String totalPages = null;
                    if (totalPagesStr != null) {
                        int totalPg = Integer.parseInt(totalPagesStr) + coverEndPage;
                        totalPages = String.valueOf(totalPg);
                    }

                    String searchtext = attributes.getValue("search");
                    String bookmarktext = attributes.getValue("bookmark");
                    String copytext = attributes.getValue("copy");
                    String flip = attributes.getValue("flip");
                    String navigation = attributes.getValue("nextPrevious");
                    String highlight = attributes.getValue("highlight");
                    String note = attributes.getValue("note");
                    String goToPage = attributes.getValue("goToPage");
                    String indexPage = attributes.getValue("Index");
                    String zoom = attributes.getValue("zoom");
                    String webSearch = attributes.getValue("websearch");
                    String wikiSearch = attributes.getValue("wikipediasearch");
                    String updateCounts = attributes.getValue("UpdateCounts");
                    String youtube = attributes.getValue("youtubeSearch");
                    String google = attributes.getValue("googleSearch");
                    String jazira = attributes.getValue("jazira");
                    String bing = attributes.getValue("bing");
                    String ask = attributes.getValue("ask");
                    String yahoo = attributes.getValue("yahoo");
                    String noor = attributes.getValue("nooorSearch");
                    String noorbook = attributes.getValue("nooorBookSearch");
                    String translationSearch = attributes.getValue("TranslationSearch");
                    String dictionarySearch = attributes.getValue("dictionarySearch");
                    String bookSearch = attributes.getValue("booksearch");
                    String bookLanguage = attributes.getValue("Language");
                    String isEditable = attributes.getValue("IsEditable");

                    String coverPageName, MapID = null, BundleMapID = null, IsBundle = null, price, description;
                    if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore)){  //if (tabPosition == Globals.mindmapStore) {
                        price = attributes.getValue("Price");
                        description = attributes.getValue("Description");
                        coverPageName = attributes.getValue("CoverPage");
                        if (coverPageName==null){
                            coverPageName = attributes.getValue("coverpage");
                        }
                        MapID = attributes.getValue("MapID");
                        BundleMapID = attributes.getValue("BundleMapID");
                        IsBundle = attributes.getValue("IsBundle");
                    } else {
                        price = attributes.getValue("price");
                        description = attributes.getValue("Discription");
                        coverPageName = attributes.getValue("coverpage");
                    }
                    String bookSize = attributes.getValue("Size");

                    String inAppProductID;
                    if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.mindmapStore) || fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.eLessonStore)|| fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.pdfStore)){  //  if (tabPosition == Globals.eLessonStore || tabPosition == Globals.mindmapStore) {
                        inAppProductID = fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getPrefixName() + attributes.getValue("ID");
                    } else {
                        inAppProductID = attributes.getValue("InAppProductID");
                    }

                    String[] imageUrlArray = bookURL.split("/");  //http://School.nooor.com/Semalibrary/Sboook/Iphone/a783ac60-096a-4ae2-94ca-5079473b13c2/a783ac60-096a-4ae2-94ca-5079473b13c2.zip
                    int imgInt = imageUrlArray.length;
                    //String imageName = imageUrlArray[imgInt-1];  
                    String imgConcat = "";
                    for (int i = 0; i < imgInt - 1; i++) {
                        imgConcat = imgConcat.concat(imageUrlArray[i] + "/");
                    }
                    //String[] imgName = imageUrlArray[6].split("\\.");
                    //String imgURL = imgConcat.concat(imgName[0]+".gif"); //Verified http://School.nooor.com/Semalibrary/Sboook/Iphone/a783ac60-096a-4ae2-94ca-5079473b13c2/Card.gif

                    String imgURL;
                    if(fa_enrich.visitedStore.getChildobjlist().get(tabPosition).getShelfPosition().equals(""+Globals.rzooomStore)){  //if (tabPosition == Globals.rzooomStore) {
                        imgURL = coverPageName;
                        imgURL = imgURL.replace("https://", "http://");
                        bookURL = bookURL.replace("https://", "http://");
                    } else {
                        if (coverPageName==null){
                            coverPageName = "";
                        }
                        imgURL = imgConcat.concat(coverPageName);
                    }
                    //System.out.println("imgURL:"+imgURL);
                    String addToBook = "";
                    addToBook = addToBook.concat(book + "#" + price + "#" + bookURL + "#" + title + "#" + description + "#" + author + "#" + imgURL + "#" + totalPages + "#" + searchtext + "#" + bookmarktext + "#" + copytext + "#" + flip + "#" + navigation + "#" + highlight + "#" + note + "#" + goToPage + "#" + indexPage + "#" + zoom + "#" + webSearch + "#" + wikiSearch + "#" + updateCounts + "#" + youtube + "#" + google + "#" + jazira + "#" + bing + "#" + ask + "#" + yahoo + "#" + noor + "#" + noorbook + "#" + inAppProductID + "#" + translationSearch + "#" + dictionarySearch + "#" + bookSearch + "#" + bookLanguage + "#" + tabPosition + "#" + bookSize + "#" + MapID + "#" + BundleMapID + "#" + IsBundle + "#" + isEditable);
                    EbooksOriginal.add(addToBook);
                    this.b_booksByCategory = true;
                }
            }
        }


        public void characters(char[] ch, int start, int length)
                throws SAXException {
            //super.characters(ch, start, length);
            ////System.out.println("In parse characters method");

            if (this.b_country) {
            } else if (this.b_university) {
            } else if (this.b_category) {
            } else if (this.b_booksByCategory) {
            }
        }

        public void endElement(String uri, String localName, String qName)
                throws SAXException {
            //super.endElement(uri, localName, qName);
            ////System.out.println("In parse End Element Method");
            if (this.b_country) {
                ////System.out.println("TheEcountryArray length:"+EcountryArray.size());
                ////System.out.println("EcountryArray"+":"+EcountryArray);
                this.b_country = false;
            } else if (this.b_university) {
                ////System.out.println("TheEuniversityArray length:"+EuniversityArray.size());
                ////System.out.println("EuniversityArray"+":"+EuniversityArray);
                this.b_university = false;
            } else if (this.b_category) {
                ////System.out.println("TheEcategoryArray length:"+EcategoryArray.size());
                ////System.out.println("EcategoryArray"+":"+EcategoryArray);
                this.b_category = false;
            } else if (this.b_booksByCategory) {
                ////System.out.println("The EbooksOriginal length:"+EbooksOriginal.size());
                ////System.out.println("EbooksOriginal"+":"+EbooksOriginal);
                this.b_booksByCategory = false;
            }
        }
    }

    /*
	 *  PopUp window used for displaying Countries, Universities, Categories, Price, Search and Language list...
	 */
    public void callPopUpWindow() {
        InputMethodManager inputmanager = (InputMethodManager) fa_enrich.getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
        inputmanager.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
        final View popView = fa_enrich.getLayoutInflater().inflate(R.layout.popup, null, false);
        if (Globals.isTablet()) {
            popwindow = new PopupWindow(fa_enrich);
            popwindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            //popLayout = new LinearLayout(fa_enrich);
            //popView.setBackgroundColor(Color.TRANSPARENT);
            //popLayout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            //popLayout.addView(popView);
            popwindow.setContentView(popView);
            popwindow.setFocusable(true);
            popwindow.setTouchable(true);
            popwindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    dropDownlayout1.setBackgroundColor(getResources().getColor(R.color.dropdown_bar_color));
                    dropDownlayout2.setBackgroundColor(getResources().getColor(R.color.dropdown_bar_color));
                    dropDownlayout3.setBackgroundColor(getResources().getColor(R.color.dropdown_bar_color));
                    dropDownlayout4.setBackgroundColor(getResources().getColor(R.color.dropdown_bar_color));
                }
            });
        } else {
            popupDialog = new Dialog(fa_enrich);
            popupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));

            popupDialog.setContentView(popView);
            popupDialog.getWindow().setLayout((int) ((deviceWidth / 4) * 3.1), (int) ((deviceHeight / 4) * 2.1));//resize popup
            popupDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dropDownlayout1.setBackgroundColor(getResources().getColor(R.color.dropdown_bar_color));
                    dropDownlayout2.setBackgroundColor(getResources().getColor(R.color.dropdown_bar_color));
                    dropDownlayout3.setBackgroundColor(getResources().getColor(R.color.dropdown_bar_color));
                    dropDownlayout4.setBackgroundColor(getResources().getColor(R.color.dropdown_bar_color));
                }
            });
        }

        final ListView lv = (ListView) popView.findViewById(R.id.listView1);
        //lv.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1f));
        lv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                if (ETagValue == 0 || ETagValue == 1 || ETagValue == 2 || ETagValue == 3) {
                    progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    //if(language ==1){
                    progDialog.setMessage(Enrichment.this.getResources().getString(R.string.loading));
                    //}
					/*else{
						progDialog.setMessage("√ò¬¨√ò¬ß√ò¬±√ô≈† √ò¬ß√ô‚Äû√ò¬™√ò¬≠√ô‚Ä¶√ô≈†√ô‚Äû..");
					}*/
                    progDialog.setCancelable(false);
                    progDialog.setCanceledOnTouchOutside(false);
                    progDialog.show();
                }
                if (ETagValue == 0) {
                    ////System.out.println("selectedETagValue:"+ETagValue+" With itemclickposition: "+arg2);
                    countrySelected(position, 1);
                    //EselectedCountryIndex=arg2;
                    ////System.out.println("EselectedCountryIndex :"+EselectedCountryIndex);
                    ////System.out.println("EcountryArray:"+EcountryArray);
                    //String[] CountryArrraySplit = EcountryArray.get(arg2).split("#");
                    //String CountryID=CountryArrraySplit[2];
                    //ESelectedCountryID=Integer.parseInt(CountryID);
                    //StoreDatabase.UpdateEnrichCountrySelected(CountryID);
                    //filteredEuniversityArray.clear();
                    //filteredEuniversityArray=StoreDatabase.getEnrichUniversityNameForTheSelectedCountry(EuniversityArray, ESelectedCountryID);
                    //SplitCountryorUniversityorCategoryList(language);
                    serviceType = 4;
                    //Need to cancel thread i.e stop soapcall asynctask;    //Need to check
                    ////System.out.println("ESelectedCountryID:"+ESelectedCountryID+"UniversityID:"+ EUniversityIDInt+"ELocalCategoryName:"+ELocalCategoryName);
                    EbooksOriginal.clear();
                    View parent = (View) arg1.getParent();
                    if (parent != null) {
                        String label = null, univLabel = null;
                        if(selectedCountry!=null &&selectedUniversity!=null) {
                            if (fa_enrich.language.equals("en")) {
                                label = selectedCountry.getCountryEngName();
                                univLabel = selectedUniversity.getUniversityEngName();
                            } else {
                                label = selectedCountry.getCountryArabName();
                                univLabel = selectedUniversity.getUniversityArabName();
                            }
                        }
                        CountryBtn.setText(label);
                        UniversityBtn.setText(univLabel);
                    }
                    loadSoapCallMethods(fa_enrich.mViewPager.getCurrentItem());
                } else if (ETagValue == 1) {
                    ////System.out.println("selectedETagValue:"+ETagValue+" With itemclickposition: "+arg2);
                    selectedUniversity = filteredUniversityList.get(position);
                    //EselectedPublisherIndex=arg2;
                    ////System.out.println("EuniversityArray:"+EuniversityArray);
                    //String[] UniversityArrraySplit = EuniversityArray.get(arg2).split("#");
                    //String[] UniversityArrraySplit = filteredEuniversityArray.get(arg2).split("#");
                    //String UniversityID=UniversityArrraySplit[2];
                    //EUniversityIDInt=Integer.parseInt(UniversityID);
                    serviceType = 4;
                    //Need to cancel thread i.e stop soapcall asynctask;
                    ////System.out.println("ESelectedCountryID:"+ESelectedCountryID+"UniversityID:"+ EUniversityIDInt+"ELocalCategoryName:"+ELocalCategoryName);
                    EbooksOriginal.clear();
                    View parent = (View) arg1.getParent();
                    if (parent != null) {
                        String label=null;
                        if(selectedUniversity!=null) {
                            if (fa_enrich.language.equals("en")) {
                                label = selectedUniversity.getUniversityEngName();
                            } else {
                                label = selectedUniversity.getUniversityArabName();
                            }
                        }
                        UniversityBtn.setText(label);
                    }
                    loadSoapCallMethods(fa_enrich.mViewPager.getCurrentItem());
                } else if (ETagValue == 2) {
                    ////System.out.println("selectedETagValue:"+ETagValue+" With itemclickposition: "+arg2);
                    if(categoryList.size()>position) {
                        selectedCategory = categoryList.get(position);
                    }
                    //EselectedCategoriesIndex=arg2;
                    //String[] EcategoryArraySplit = EcategoryArray.get(arg2).split("#");
                    //String CategoryID = EcategoryArraySplit[2];
                    //ELocalCategoryName=CategoryID;
                    serviceType = 4;
                    ////System.out.println("ESelectedCountryID:"+ESelectedCountryID+"UniversityID:"+ EUniversityIDInt+"ELocalCategoryName:"+ELocalCategoryName);
                    EbooksOriginal.clear();
                    View parent = (View) arg1.getParent();
                    if (parent != null) {
                        String label = null;
                        if(selectedCategory!=null) {
                            if (fa_enrich.language.equals("en")) {
                                label = selectedCategory.getCategoryEngName();
                            } else {
                                label = selectedCategory.getCategoryArabName();
                            }
                        }
                        CategoriesBtn.setText(label);
                    }
                    loadSoapCallMethods(fa_enrich.mViewPager.getCurrentItem());
                } else if (ETagValue == 3) {
                    ////System.out.println("selectedETagValue:"+ETagValue+" With itemclickposition: "+arg2);
                    EselectedPriceIndex = position;
                    EselectedSegmentIndex = position;
                    serviceType = 4;
                    ////System.out.println("ESelectedCountryID:"+ESelectedCountryID+"UniversityID:"+ EUniversityIDInt+"ELocalCategoryName:"+ELocalCategoryName);
                    EbooksOriginal.clear();
                    View parent = (View) arg1.getParent();
                    if (parent != null) {
                        String label=null;
                        if(EpriceArray.size()>position) {
                            label = EpriceArray.get(position);
                        }
                        PriceBtn.setText(label);
                    }
                    loadSoapCallMethods(fa_enrich.mViewPager.getCurrentItem());
                } else if (ETagValue == 4) {
                    if (position == 0) {
                        SearchFilterSelected = "Author";
                    } else {
                        SearchFilterSelected = "Title";
                    }
                } else if (ETagValue == 5) {
                    if (position == 0) {
                        //language = 1;
                        //StoreTabViewFragment.setTabTitle();
                        //StoreTab.setTitleText("Store", 0);
                        //StoreTab.setTitleText("Enrichments", 1);
                    } else {
                        //language = 2;
                        //StoreTabViewFragment.setTabTitle();
                        //StoreTab.setTitleText("√ò¬ß√ò¬ß√ô‚Äû√ô‚Ä¶√ò¬™√ò¬¨√ò¬±", 0);
                        //StoreTab.setTitleText("√ò¬•√ò¬´√ò¬±√ò¬ß√ò¬°√ò¬ß√ò¬™", 1);
                    }
                    //StoreDatabase.updateDatabaseLanguage(language);
                    //SplitCountryorUniversityorCategoryList(language);
                    setLanguage();
                }
                if (Globals.isTablet()) {
                    popwindow.dismiss();
                } else {
                    popupDialog.dismiss();
                }
            }
        });
        //onWindowFocusChanged(true);
        if (ETagValue == 0) {
            ////System.out.println("tag: "+0);	
            lv.setAdapter(new popAdapter(fa_enrich, null, countryList, null, null, ETagValue));
            if (Globals.isTablet()) {
                //popwindow.showAtLocation(popView, Gravity.NO_GRAVITY, storeBGPoint.x, storeBGPoint.y+storeBtmBG.getHeight());
                popwindow.showAsDropDown(CountryBtn, 0, 0);
                popwindow.update((CountryBtn.getWidth()), LayoutParams.WRAP_CONTENT);
            } else {
                changeCountryUniversityGradeText(storeTabPosition, popupDialog, ETagValue);
                popupDialog.show();
            }
        } else if (ETagValue == 1) {
            lv.setAdapter(new popAdapter(fa_enrich, null, null, filteredUniversityList, null, ETagValue));
            if (Globals.isTablet()) {
                //popwindow.showAtLocation(popView, Gravity.NO_GRAVITY, publisherPoint.x-UniversityBtn.getWidth()/2,storeBGPoint.y+storeBtmBG.getHeight());
                popwindow.showAsDropDown(UniversityBtn, 0, 0);
                popwindow.update((UniversityBtn.getWidth()), LayoutParams.WRAP_CONTENT);
            } else {
                changeCountryUniversityGradeText(storeTabPosition, popupDialog, ETagValue);
                popupDialog.show();
            }
            ////System.out.println("tag: "+ETagValue);
        } else if (ETagValue == 2) {
            lv.setAdapter(new popAdapter(fa_enrich, null, null, null, categoryList, ETagValue));
            if (Globals.isTablet()) {
                //popwindow.showAtLocation(popView, Gravity.NO_GRAVITY, categoryPoint.x-CategoriesBtn.getWidth()/2,storeBGPoint.y+storeBtmBG.getHeight());
                popwindow.showAsDropDown(CategoriesBtn, 0, 0);
                popwindow.update((CategoriesBtn.getWidth()), LayoutParams.WRAP_CONTENT);
            } else {
                changeCountryUniversityGradeText(storeTabPosition, popupDialog, ETagValue);
                popupDialog.show();
            }
            ////System.out.println("tag: "+ETagValue);
        } else if (ETagValue == 3) {
            lv.setAdapter(new popAdapter(fa_enrich, EpriceArray, null, null, null, ETagValue));
            if (Globals.isTablet()) {
                //popwindow.showAtLocation(popView, Gravity.NO_GRAVITY, pricePoint.x-PriceBtn.getWidth()/2,storeBGPoint.y+storeBtmBG.getHeight());
                popwindow.showAsDropDown(PriceBtn, 0, 0);
                popwindow.update((PriceBtn.getWidth()), LayoutParams.WRAP_CONTENT);
            } else {
                changeCountryUniversityGradeText(storeTabPosition, popupDialog, ETagValue);
                popupDialog.show();
            }
            ////System.out.println("tag: "+ETagValue);
        } else if (ETagValue == 4) {
			/*lv.setAdapter(new popAdapter(fa_enrich, EsearchFilterArray, null, null, null, ETagValue));
			popwindow.showAtLocation(popView, Gravity.NO_GRAVITY, searchPoint.x-SearchFilterBtn.getWidth()/2-popwindow.getWidth(),searchPoint.y+SearchFilterBtn.getHeight()); 
			popwindow.update(4*(SearchFilterBtn.getWidth()), LayoutParams.WRAP_CONTENT);*/
            ////System.out.println("tag: "+ETagValue);
        } else if (ETagValue == 5) {
            lv.setAdapter(new popAdapter(fa_enrich, ElanguageArray, null, null, null, ETagValue));
            //popwindow.showAtLocation(popView, Gravity.NO_GRAVITY, lngPoint.x,lngPoint.y+LanguageBtn.getHeight()); 
            //popwindow.update(LanguageBtn.getWidth()+50, LayoutParams.WRAP_CONTENT);
            ////System.out.println("tag: "+ETagValue);	
        }
    }


    public void onWindowFocusChanged(boolean hasFocus) {
        int[] location = new int[2];
        //LanguageBtn.getLocationOnScreen(location);
        lngPoint = new Point();
        lngPoint.x = location[0];
        lngPoint.y = location[1];

		/*SearchFilterBtn.getLocationOnScreen(location);
		searchPoint = new Point();
		searchPoint.x = location[0];
		searchPoint.y = location[1];*/

        CountryBtn.getLocationOnScreen(location);
        countryPoint = new Point();
        countryPoint.x = location[0];
        countryPoint.y = location[1];

        UniversityBtn.getLocationOnScreen(location);
        publisherPoint = new Point();
        publisherPoint.x = location[0];
        publisherPoint.y = location[1];

        CategoriesBtn.getLocationOnScreen(location);
        categoryPoint = new Point();
        categoryPoint.x = location[0];
        categoryPoint.y = location[1];

        PriceBtn.getLocationOnScreen(location);
        pricePoint = new Point();
        pricePoint.x = location[0];
        pricePoint.y = location[1];

        storeBtmBG.getLocationOnScreen(location);
        storeBGPoint = new Point();
        storeBGPoint.x = location[0];
        storeBGPoint.y = location[1];

    }

    private class popAdapter extends BaseAdapter {
        private Context mContext;

        private ArrayList<String> ListViewList;
        private ArrayList<Country> ctryList;
        private ArrayList<University> univList;
        private ArrayList<Category> catList;

        int TagValueContent = 0;

        public popAdapter(Context context, ArrayList<String> ArrayListContent, ArrayList<Country> ctryList, ArrayList<University> univList, ArrayList<Category> catList, int Tagvalue) {
            mContext = context;
            ListViewList = ArrayListContent;
            this.ctryList = ctryList;
            this.univList = univList;
            this.catList = catList;
            TagValueContent = Tagvalue;
        }

        public int getCount() {
            if (TagValueContent == 0) {
                return ctryList.size();
            } else if (TagValueContent == 1) {
                return univList.size();
            } else if (TagValueContent == 2) {
                return catList.size();
            } else {
                return ListViewList.size();
            }
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            ImageView image;
            if (convertView == null) {
                Typeface font = Typeface.createFromAsset(fa_enrich.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
                vi = fa_enrich.getLayoutInflater().inflate(R.layout.popgrid, null);
            }
            TextView Titletext = (TextView) vi.findViewById(R.id.textView1);
            image = (ImageView) vi.findViewById(R.id.imageView1);
            image.setVisibility(View.GONE);
            Titletext.setTextColor(Color.WHITE);

            if (TagValueContent == 0) {
                Country country = ctryList.get(position);
                if (fa_enrich.language.equals("en")) {
                    Titletext.setText(country.getCountryEngName());
                } else {
                    Titletext.setText(country.getCountryArabName());
                }
				/*if(position ==0){
					image.setVisibility(View.INVISIBLE);
				}
				else{
					image.setVisibility(View.VISIBLE);
					File file= new File(country.getCountryFlagName());
					if(file.exists()){
						String Path = country.getCountryFlagName();
						Bitmap bm = BitmapFactory.decodeFile(Path);
						image.setImageBitmap(bm);
					}
					else{
						image.setImageResource(R.drawable.ic_launcher);
					}
				}*/
            }
            if (TagValueContent == 1) {
                University university = univList.get(position);
                if (fa_enrich.language.equals("en")) {
                    Titletext.setText(university.getUniversityEngName());
                } else {
                    Titletext.setText(university.getUniversityArabName());
                }
            } else if (TagValueContent == 2) {
                Category category = catList.get(position);
                if (fa_enrich.language.equals("en")) {
                    Titletext.setText(category.getCategoryEngName());
                } else {
                    Titletext.setText(category.getCategoryArabName());
                }
            } else if (TagValueContent == 3) {
                Titletext.setText(ListViewList.get(position));
            } else if (TagValueContent == 4) {
                Titletext.setText(ListViewList.get(position));
                ArrayList<String> sampleSearchEContent = new ArrayList<String>();
                for (int j = 0; j < EsearchFilterArrayContent.size(); j++) {
                    String content = EsearchFilterArrayContent.get(j).split("#")[0];
                    sampleSearchEContent.add(content);
                }
                if (SearchFilterSelected.equals(sampleSearchEContent.get(position))) {
                    Titletext.setBackgroundColor(Enrichment.this.getResources().getColor(R.color.selection_color));
                    Titletext.setTextColor(Color.WHITE);
                } else {
                    Titletext.setBackgroundColor(Color.TRANSPARENT); //DEDFDE rgb value:
                    Titletext.setTextColor(Color.WHITE);
                }
            } else if (TagValueContent == 5) {
                if (position == 0) {
                    Titletext.setBackgroundColor(Enrichment.this.getResources().getColor(R.color.selection_color));
                    Titletext.setTextColor(Color.WHITE);
                } else {
                    Titletext.setBackgroundColor(Color.TRANSPARENT); //DEDFDE rgb value:
                    Titletext.setTextColor(Color.WHITE);
                }
                Titletext.setText(ListViewList.get(position));
            } else if (TagValueContent == 6) {                      //Shelflist:
					/*if(position == 0){
	        		Titletext.setBackgroundColor(Color.rgb(47,128,154));
	        	}
	        	Titletext.setGravity(Gravity.CENTER);
	        	Titletext.setText(ListViewList.get(position));*/

                //changes for default selection new method :
                if (position == bookModalFlag) {
                    vi.setBackgroundColor(Enrichment.this.getResources().getColor(R.color.selection_color));
                } else {
                    vi.setBackgroundColor(Color.TRANSPARENT);
                }
                //Titletext.setGravity(Gravity.CENTER);
                Titletext.setText(ListViewList.get(position));
            }

            return vi;
        }
    }

    /*
	 * SetCountryFlag stores country images and in database.
	 */
    public String SetCountryFlag(String CountryID, String flagurl) {
        String returnValue = "";
        SaveCountryImage(CountryID, flagurl);
        if (EcountryArrayInSQL.size() == 0) {
            //SaveCountryImage(CountryID,flagurl);
            StoreDatabase.InsertCountry(CountryID, flagurl);
            returnValue = CountryID;
        } else {
            int flagExist = 0;
            int flagupdate = 0;
            for (int i = 0; i < EcountryArrayInSQL.size(); i++) {
                String[] splitEcountryArray = EcountryArrayInSQL.get(i).split("#");
                if (splitEcountryArray[0].equals(CountryID)) {
                    flagExist = 1;
                    if (!(splitEcountryArray[1].equals(flagurl))) {
                        flagupdate = 1;
                    }
                    break;
                }
            }
            if (flagExist == 1) {
                if (flagupdate == 0) {
                    returnValue = CountryID;
                } else {
                    //SaveCountryImage(CountryID,flagurl);
                    StoreDatabase.UpdateCountry(CountryID, flagurl);
                    returnValue = CountryID;
                }
            }
        }
        return returnValue;
    }

    /*
	 *  SaveCountryImage stores images in the internal memory
	 */
    public void SaveCountryImage(String CountryID, String flagurl) {
        ////System.out.println("Called saveCountryImage");
        //Modified to store images in Internal storage:
        String CImagePath = "CImageID_" + CountryID + ".png";
        ////System.out.println("EcountryImagePath:"+CImagePath);
        String CImageFinalPath = Environment.getDataDirectory().getPath().concat("/data/com.semanoor.manahij/files/" + CImagePath);
        EcountryImagePath.add(CImageFinalPath);
        File countryImageFile = new File(fa_enrich.getFilesDir() + "/" + CImagePath);
        if (countryImageFile.exists()) {
            ////System.out.println("Country Image file exists at the given path with name"+CountryID+".png");
        } else {
            ////System.out.println("CountryImage file doesn't exists at the given path called download method");
            Downloadfile(flagurl, CImagePath);
            //create NoCover.gif image and copy it..  this can be done in imageloader.java
        }
    }

    /*
	 *  SetUniversityFlag stores university images and in database.
	 */
    public String SetUniversityFlag(String UniversityID, String flagurl) {
        String returnValue = "";
        if (EuniversityArrayInSQL.size() == 0) {
            EuniversityImagePath = StoreDatabase.SaveUniversityImage(UniversityID, flagurl);
            StoreDatabase.InsertUniversity(UniversityID, flagurl);
            returnValue = UniversityID;
        } else {
            int flagExist = 0;
            int flagupdate = 0;
            for (int i = 0; i < EuniversityArrayInSQL.size(); i++) {
                String[] splitEuniversityArray = EuniversityArrayInSQL.get(i).split("#");
                if (splitEuniversityArray[0].equals(UniversityID)) {
                    flagExist = 1;
                    if (!(splitEuniversityArray[1].equals(flagurl))) {
                        flagupdate = 1;
                    }
                    break;
                }
            }
            if (flagExist == 1) {
                if (flagupdate == 0) {
                    returnValue = UniversityID;
                } else {
                    EuniversityImagePath = StoreDatabase.SaveUniversityImage(UniversityID, flagurl);
                    StoreDatabase.UpdateUniversity(UniversityID, flagurl);
                    returnValue = UniversityID;
                }
            }
        }
        return returnValue;
    }

	/*
	 *  SplitCountry...  splits the parsing content 
	 */
	/*public void SplitCountryorUniversityorCategoryList(int lang){
		int language=lang;
		EcountryArrayList.clear();
		EuniversityArrayList.clear();
		EcategoryArrayList.clear();
		if(language == 1){				//For English
			for(int i=0;i<EcountryArray.size();i++){
				String[] splitStr=EcountryArray.get(i).split("#");
				////System.out.println("EcountryArray:"+splitStr[1]);
				EcountryArrayList.add(splitStr[1]);
			}
			for(int i=0;i<filteredEuniversityArray.size();i++){
				String[] splitStr=filteredEuniversityArray.get(i).split("#");
				////System.out.println("FilteredEuniversityArray:"+splitStr[1]);
				EuniversityArrayList.add(splitStr[1]);
			}
			for(int i=0;i<EcategoryArray.size();i++){
				String[] splitStr=EcategoryArray.get(i).split("#");
				////System.out.println("EcategoryArray:"+splitStr[2]);
				EcategoryArrayList.add(splitStr[2]);
			}
		}
		if(language == 2){				//For Arabic
			for(int i=0;i<EcountryArray.size();i++){
				String[] splitStr=EcountryArray.get(i).split("#");
				////System.out.println("EcountryArray"+splitStr[0]);
				EcountryArrayList.add(splitStr[0]);
			}
			for(int i=0;i<filteredEuniversityArray.size();i++){
				String[] splitStr=filteredEuniversityArray.get(i).split("#");
				////System.out.println("FilteredEuniversityArray"+splitStr[0]);
				EuniversityArrayList.add(splitStr[0]);
			}
			for(int i=0;i<EcategoryArray.size();i++){
				String[] splitStr=EcategoryArray.get(i).split("#");
				////System.out.println("EcategoryArray"+splitStr[0]);
				EcategoryArrayList.add(splitStr[0]);
			}
		} 	
	}*/


    /*
	 *  ProcessGridView for processing books  in the GridView
	 */
    public void processGridView() {
        if (Enrichment.this.getActivity() != null) {
            ////System.out.println("In process GridView");
            books.clear(); //Removes all the objects inside the arraylist
            if (EselectedSegmentIndex == 0) { //All
                for (int i = 0; i < EbooksOriginal.size(); i++) {
                    //    			String[] splitStr=EbooksOriginal.get(i).split(",");    //Need to check
                    //    			//System.out.println("Book: "+splitStr[i]);
                    books.add(EbooksOriginal.get(i));

                }
            } else if (EselectedSegmentIndex == 1) { //Free
                for (int i = 0; i < EbooksOriginal.size(); i++) {
                    String[] splitStr = EbooksOriginal.get(i).split("#");
                    String price = splitStr[1];
                    float priceValue = Float.parseFloat(price);
                    if (priceValue == 0.00) {
                        ////System.out.println("Book: "+splitStr[i]);
                        books.add(EbooksOriginal.get(i));
                    }
                }
            } else if (EselectedSegmentIndex == 2) { //Paid
                for (int i = 0; i < EbooksOriginal.size(); i++) {
                    String[] splitStr = EbooksOriginal.get(i).split("#");
                    String price = splitStr[1];
                    float priceValue = Float.parseFloat(price);
                    if (priceValue != 0.00) {                //Price not equal to zero
                        ////System.out.println("Book: "+splitStr[i]);
                        books.add(EbooksOriginal.get(i));
                    }
                }
            }

            if (books.size() == 0) {
                //Need to show the lblbookmsges.
                if (internetOff.equals("NO")) {
                    bookmessage.setVisibility(View.VISIBLE);
                } else {
                    bookmessage.setVisibility(View.GONE);
                }
                if (serviceType == 4) {
                    //if(language == 1){
                    if (Enrichment.this.getActivity() != null) {
                        bookmessage.setText(Enrichment.this.getResources().getString(R.string.no_books_available_category));

                    }
				/*}
				else{
					bookmessage.setText("√ô‚Äû√ò¬ß √ô≈†√ôÀÜ√ò¬¨√ò¬Ø √ô∆í√ò¬™√ò¬® √ô‚Ä¶√ò¬™√ò¬ß√ò¬≠√ò¬© √ôÔøΩ√ô≈† √ô‚Ä°√ò¬∞√ò¬ß √ò¬ß√ô‚Äû√ò¬™√ò¬µ√ô‚Ä†√ô≈†√ôÔøΩ");
				}*/
                } else {
                    //if(language ==1){
                    if (SearchFilterSelected.equals("Title")) {
                        bookmessage.setText(Enrichment.this.getResources().getString(R.string.titlemessage));
                    } else {
                        bookmessage.setText(Enrichment.this.getResources().getString(R.string.authormessage));
                    }

				/*}else{

					if(SearchFilterSelected.equals("Title")){
						bookmessage.setText("√ô‚Äû√ò¬ß √ô≈†√ôÀÜ√ò¬¨√ò¬Ø √ô∆í√ò¬™√ò¬® √ô‚Ä¶√ò¬™√ò¬ß√ò¬≠√ò¬© √ò¬®√ô‚Ä°√ò¬∞√ò¬ß √ò¬ß√ô‚Äû√ò¬π√ô‚Ä†√ôÀÜ√ò¬ß√ô‚Ä†");
					}
					else{
						bookmessage.setText("√ô‚Äû√ò¬ß √ô≈†√ôÀÜ√ò¬¨√ò¬Ø √ô∆í√ò¬™√ò¬® √ô‚Ä¶√ò¬™√ò¬ß√ò¬≠√ò¬© √ô‚Äû√ô‚Ä°√ò¬∞√ò¬ß √ò¬ß√ô‚Äû√ô‚Ä¶√ò¬§√ô‚Äû√ôÔøΩ");
					}
				}*/
                }
                progDialog.dismiss();
                progressbar.setVisibility(View.GONE);
                //bookmessage.setVisibility(View.VISIBLE);
                SetButtonImages();
            } else {
                bookmessage.setVisibility(View.GONE);
                for (int i = 0; i < books.size(); i++) {
                    String CurrentBook = books.get(i);
                    ////System.out.println("currentBook: "+CurrentBook);
                }
                progDialog.dismiss();
                progressbar.setVisibility(View.GONE);
                SetButtonImages();
                //    		if(EloadBookImagesValue == 0){
                //    			EloadBookImagesValue=1;
                //    			new soapCall().execute();
                //    		}
            }
        }
    }
    /*
	 *  Set Button Images in the GridView 
	 */
    private void SetButtonImages() {
        ////System.out.println("In SetButton Images");
        adapter = new EnrichIAdpater(fa_enrich, books, this, fa_enrich.downloadedBooksArray,fa_enrich.downloadingBooksArray);
        gridview.setAdapter(adapter);
        gridview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                ////System.out.println("On itemclick listener");
                EbookClickPosition = arg2;
                callBookmodalPopUpWindow();
            }
        });
    }


    private void callBookmodalPopUpWindow() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(fa_enrich);
        final int subscriptDays = Integer.parseInt(prefs.getString(Globals.SUBSCRIPTDAYSLEFT, "0"));
        //bookModalPopwindow = new Dialog(fa,R.style.PauseDialog);
        bookModalPopwindow = new Dialog(fa_enrich);
        bookModalPopwindow.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        bookModalPopwindow.setContentView(fa_enrich.getLayoutInflater().inflate(R.layout.bookmodal_neww, null));
        if (Globals.isTablet()) {
            bookModalPopwindow.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.5));
        }else{
            bookModalPopwindow.getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
        }
        bookModalPopwindow.getWindow().getAttributes().windowAnimations=R.style.DialogAnimation;
        ViewGroup viewGroup = (RelativeLayout) bookModalPopwindow.findViewById(R.id.bookmodal_layout);
        Typeface font = Typeface.createFromAsset(fa_enrich.getAssets(),"fonts/FiraSans-Regular.otf");
        UserFunctions.changeFont(viewGroup,font);
        String CurrentBook = books.get(EbookClickPosition);
        String[] BookDetails = CurrentBook.split("#");
        String priceContent = BookDetails[1];
        String bookTitleContent = BookDetails[3];
        String descriptionContent = BookDetails[4];
        String authorContent = BookDetails[5];
        if (authorContent.equals("null")) {
            authorContent = "";
        }
        String totalPagesContent = BookDetails[7];
        if (totalPagesContent.equals("null")) {
            totalPagesContent = "-";
        }
        String langContent = BookDetails[33];
        if (langContent.equals("null")) {
            langContent = "-";
        }
        String imageURL = BookDetails[6];
        String size = BookDetails[35];
        if (size.equals("null")) {
            size = "-";
        }
        String BookID = BookDetails[0];

        //final  RelativeLayout ll_txtcontainer = (RelativeLayout) bookModalPopwindow.findViewById(R.id.ll_txtcontainer);
        //ll_txtcontainer.setVisibility(View.VISIBLE);
        //TextView title =  (TextView)bookModalPopwindow.findViewById(R.id.title);
        TextView titleLabel = (TextView) bookModalPopwindow.findViewById(R.id.book_Label);
        TextView languageLabel = (TextView) bookModalPopwindow.findViewById(R.id.langLabel);
        TextView descriptionLabel = (TextView) bookModalPopwindow.findViewById(R.id.desLabel);
        TextView authorLabel = (TextView) bookModalPopwindow.findViewById(R.id.authorLabel);
        TextView totalPagesLabel = (TextView) bookModalPopwindow.findViewById(R.id.totalPagesLabel);
        TextView txtSize = (TextView) bookModalPopwindow.findViewById(R.id.txtSize);
        TextView txtCategory = (TextView) bookModalPopwindow.findViewById(R.id.txtCategory);
        //TextView priceLabel =  (TextView) bookModalPopwindow.findViewById(R.id.priceLabel);
        ImageView bookImage = (ImageView) bookModalPopwindow.findViewById(R.id.imageView1);
        CircleButton back_btn = (CircleButton) bookModalPopwindow.findViewById(R.id.btn_back_bookmodel);
        back_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                bookModalPopwindow.dismiss();
            }
        });
        bookModalPopwindow.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isBookModalDownloadClicked){
                    gridview.invalidateViews();
                    isBookModalDownloadClicked = false;
                }
            }
        });

        ETagValue = 6;
        bookModalFlag = 0;

        Glide.with(fa_enrich).load(imageURL)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(200,200)
                .into(bookImage);
        titleLabel.setText(bookTitleContent);
        languageLabel.setText(langContent);
        descriptionLabel.setText(descriptionContent);
        authorLabel.setText(authorContent);
        totalPagesLabel.setText(totalPagesContent);
        txtSize.setText(size);
        String  category;
        if(fa_enrich.language.equals("en")){
            category=fa_enrich.visitedStore.getChildobjlist().get(storeTabPosition).getSubStoreEn();
        }else{
            category=fa_enrich.visitedStore.getChildobjlist().get(storeTabPosition).getSubStoreAr();
        }
     //   txtCategory.setText(category);
        /*if (fa_enrich.visitedStore.getChildobjlist().get(storeTabPosition).getShelfPosition().equals(""+Globals.oldCurriculumStore)) {
            txtCategory.setText(Enrichment.this.getResources().getString(R.string.old_curriculum));
        } else if (fa_enrich.visitedStore.getChildobjlist().get(storeTabPosition).getShelfPosition().equals(""+Globals.newCurriculumStore)) {
            txtCategory.setText(Enrichment.this.getResources().getString(R.string.new_curriculum));
        } else if(fa_enrich.visitedStore.getChildobjlist().get(storeTabPosition).getShelfPosition().equals(""+Globals.eLessonStore)){
            //if (storeTabPosition == Globals.eLessonStore) {
            txtCategory.setText(Enrichment.this.getResources().getString(R.string.e_lesson_store));
        } else if(fa_enrich.visitedStore.getChildobjlist().get(storeTabPosition).getShelfPosition().equals(""+Globals.quizStore)){  //if (storeTabPosition == Globals.quizStore) {
            txtCategory.setText(Enrichment.this.getResources().getString(R.string.quiz_store));
        } else if(fa_enrich.visitedStore.getChildobjlist().get(storeTabPosition).getShelfPosition().equals(""+Globals.rzooomStore)){
          //if (storeTabPosition == Globals.rzooomStore) {
            txtCategory.setText(Enrichment.this.getResources().getString(R.string.rzooom_store));
        }*/

        final Button download = (Button) bookModalPopwindow.findViewById(R.id.downloadBtn);
        download.setBackgroundColor(Enrichment.this.getResources().getColor(R.color.hovercolor));
        final String bookID = BookDetails[0];
        final float price = (float) Float.parseFloat(BookDetails[1]);
        String inAppId = BookDetails[29];
        if (price > 0.00) {
            if (Globals.isLimitedVersion()) {
                if (subscriptDays > 0 || UserFunctions.checkRedeemInAppIdExist(inAppId)) {
                    download.setText(Enrichment.this.getResources().getString(R.string.get));
                } else {
                    download.setText(String.valueOf(price) + "$");
                }
            } else {
                download.setText(String.valueOf(price) + "$");
            }
        } else {
            download.setText(Enrichment.this.getResources().getString(R.string.download));
            download.setBackgroundColor(Enrichment.this.getResources().getColor(R.color.hovercolor));
        }
        for (int i = 0; i < fa_enrich.downloadedBooksArray.size(); i++) {
            if (bookID.equals(fa_enrich.downloadedBooksArray.get(i))) {
                download.setText(Enrichment.this.getResources().getString(R.string.downloaded));
                download.setBackgroundColor(Enrichment.this.getResources().getColor(R.color.hovercolor));
                break;
            }
        }
        for (int i = 0; i < fa_enrich.downloadingBooksArray.size(); i++) {
            if (bookID.equals(fa_enrich.downloadingBooksArray.get(i))) {
                download.setText(Enrichment.this.getResources().getString(R.string.downloading));
             //   download.setBackgroundColor(Enrichment.this.getResources().getColor(R.color.hovercolor));
                break;
            }
        }
        download.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (!download.getText().toString().equals(Enrichment.this.getResources().getString(R.string.downloaded)) &&!download.getText().toString().equals(Enrichment.this.getResources().getString(R.string.downloading)) ) {
                    downloadClicked();
                    if (subscriptDays>0) {
                        download.setText(Enrichment.this.getResources().getString(R.string.downloading));
                        isBookModalDownloadClicked = true;
                    }else {
                        if (!(price >0.00)){
                            download.setText(Enrichment.this.getResources().getString(R.string.downloading));
                            isBookModalDownloadClicked = true;
                        }
                    }
                    v.invalidate();
                }
            }
        });

        bookModalPopwindow.show();
    }

    public void downloadClicked() {
//    	if(!rootedDevice){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(fa_enrich);
            int subscriptDays = Integer.parseInt(prefs.getString(Globals.SUBSCRIPTDAYSLEFT, "0"));
 			String currentBook = books.get(EbookClickPosition);
			String[] BookDetails = currentBook.split("#");
			//String bnameCheck = StoreDatabase.getPayPalPaidBooks(currentBook);
			price = (float) Float.parseFloat(BookDetails[1]);
			if (price > 0.00) {
                if (Globals.isLimitedVersion()) {
                    if (subscriptDays > 0) {
                        String inAppProductId = BookDetails[29];
                        fa_enrich.subscription.checkForStoreBooksPurchased(inAppProductId);
                        startDownload();
                    } else {
                        String inAppProductId = BookDetails[29];
                        startPurchase(inAppProductId);
                    }
                } else {
                    String inAppProductId = BookDetails[29];
                    startPurchase(inAppProductId);
                }
			} else {
				startDownload();
			}
//		} else {
//            UserFunctions.DisplayAlertDialog(fa_enrich, R.string.books_cannot_download_rooted_device, R.string.rooted_device);
//		}
       // startDownload();
    }


    /**
     * SubScription
     */
    private void subscribePurchase(final String subscribeProductId){
//        if(!rootedDevice){
            fa_enrich.mHelper = new IabHelper(fa_enrich, Globals.manahijBase64EncodedPublicKey);
            fa_enrich.mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

                @Override
                public void onIabSetupFinished(IabResult result) {
                    if (!result.isSuccess()) {
                        // Oh noes, there was a problem.
                        UserFunctions.complain("Problem setting up in-app billing: " + result, fa_enrich);
                        return;
                    }
                    // Have we been disposed of in the meantime? If so, quit.
                    if (fa_enrich.mHelper == null) return;

                    //fa_enrich.mHelper.launchPurchaseFlow(fa_enrich,subscribeProductId, IabHelper.ITEM_TYPE_SUBS,10001, mPurchaseFinishedListener, "");
                    fa_enrich.mHelper.launchPurchaseFlow(fa_enrich, subscribeProductId, 10001, mPurchaseFinishedListener, "");
                }
            });
//        } else {
//            UserFunctions.DisplayAlertDialog(fa_enrich, R.string.books_cannot_download_rooted_device, R.string.rooted_device);
//        }
    }

    /**
     * Initiate the purchase
     *
     * @param inAppProductId
     */
    private void startPurchase(String inAppProductId) {
        //ITEM_SKU = "android.test.purchased";
        ITEM_SKU = inAppProductId;
        fa_enrich.mHelper = new IabHelper(fa_enrich, Globals.manahijBase64EncodedPublicKey);
        fa_enrich.mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    UserFunctions.complain("Problem setting up in-app billing: " + result, fa_enrich);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (fa_enrich.mHelper == null) return;

                fa_enrich.mHelper.launchPurchaseFlow(fa_enrich, ITEM_SKU, 10001, mPurchaseFinishedListener, "");
            }
        });

    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            // if we were disposed of in the meantime, quit.
            if (fa_enrich.mHelper == null) return;

            if (result.isFailure()) {
                int response = result.getResponse();
                if (response == 7) {
                  //  UserFunctions.DisplayAlertDialog(fa_enrich, R.string.item_already_owned, R.string.restoring_purchase);
                    if (ITEM_SKU != null && purchase.getSku().equals(ITEM_SKU)) {
                        startDownload();
                        gridview.invalidateViews();
                    }else if (ITEM_SKU == null && purchase == null){
                        consumeItem();
                    }
                } else {
                    //UserFunctions.complain("Error purchasing: " + result, fa_enrich);
                }
                return;
            }

            //Log.d(TAG, "Purchase successful.");

            if (purchase.getSku().equals(ITEM_SKU)) {
                //fa_enrich.mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                startDownload();
                gridview.invalidateViews();
            } else if (purchase.getSku().equals(fa_enrich.visitedStore.getInAppId())){
                consumeProgress = UserFunctions.displayProcessDialog(fa_enrich, getResources().getString(R.string.consuming_product));
                fa_enrich.mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                //UserFunctions.alert("Thank you for subscribing", fa_enrich);
            }

        }
    };

    public void consumeItem() {
        fa_enrich.mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {

        @Override
        public void onQueryInventoryFinished(IabResult result, Inventory inv) {
            if (result.isFailure()) {
                ////System.out.println("Failed");
                UserFunctions.DisplayAlertDialogNotFromStringsXML(fa_enrich, result.getMessage(), "");
            } else {
                Purchase gasPurchase = inv.getPurchase(fa_enrich.visitedStore.getInAppId());
                if (gasPurchase != null) {
                    consumeProgress = UserFunctions.displayProcessDialog(fa_enrich, getResources().getString(R.string.consuming_product));
                    fa_enrich.mHelper.consumeAsync(inv.getPurchase(fa_enrich.visitedStore.getInAppId()), mConsumeFinishedListener);
                }
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            // if we were disposed of in the meantime, quit.
            if (fa_enrich.mHelper == null) return;
            if (consumeProgress!=null){
                consumeProgress.dismiss();
            }

            if (result.isSuccess()) {
                ////System.out.println("Success");
                //UserFunctions.DisplayAlertDialog(fa_enrich, R.string.purchase_completed_successfully, R.string.purchased);
                //startDownload();
                if (purchase.getSku().equals(fa_enrich.visitedStore.getInAppId())) {
                    //UserFunctions.alert(purchase.getOriginalJson(), fa_enrich);
                    fa_enrich.subscription.new SaveSubscriptionGroups(purchase,fa_enrich, Enrichment.this).execute();
                }
            } else {
                ////System.out.println("Failed");
                //UserFunctions.DisplayAlertDialog(fa_enrich, result.getMessage(), R.string.purchase_failed);
                UserFunctions.complain("Error while consuming: " + result, fa_enrich);
            }
        }
    };

    //PayPal
    public void startDownload() {
        //shelfPopWindow.dismiss();
        //if(!rootedDevice){
        downloadProgDialog = new ProgressDialog(fa_enrich);
        downloadProgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        //if(language == 1){
        downloadProgDialog.setTitle(Enrichment.this.getResources().getString(R.string.downloading));
			/*}
			else{
				downloadProgDialog.setTitle("√ò¬¨√ò¬ß√ò¬±√ô≈† √ò¬ß√ô‚Äû√ò¬™√ò¬≠√ô‚Ä¶√ô≈†√ô‚Äû");
			}*/
        downloadProgDialog.setMessage("");
        downloadProgDialog.getWindow().setTitleColor(Color.rgb(222, 223, 222));
        downloadProgDialog.setIndeterminate(false);
        downloadProgDialog.setMax(100);
        downloadProgDialog.setCancelable(false);
        downloadProgDialog.setCanceledOnTouchOutside(false);
        //downloadProgDialog.show();
			/*SettingsDialog = new Dialog(fa_enrich);
			SettingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
			SettingsDialog.setTitle("Settings");
			SettingsDialog.setContentView(fa_enrich.getLayoutInflater().inflate(R.layout.preview_inflate, null));
			SettingsDialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.5), (int)(Globals.getDeviceHeight()/1.5));*/
        download();
			/*RenderHTMLObjects webView = (RenderHTMLObjects) ll_enrich.findViewById(R.id.webView1);
			//StoreDatabase.SaveBooksToDatabase(books,EbookClickPosition, EselectCategoryId);
			String CurrentBook= books.get(EbookClickPosition);
			String[] bookDetails = CurrentBook.split("#");
			int newBookId = fa_enrich.gridShelf.addNewStoreBook(bookDetails[3], bookDetails[4], bookDetails[5], 0, Integer.parseInt(bookDetails[7]), 0, Globals.portrait, true, 1, bookDetails[0]);
			//RenderHTMLObjects renderHtmlObjects = new RenderHTMLObjects(fa_enrich, null, bookDetails[0]+"Book", Integer.parseInt(bookDetails[7]), newBookId);
			webView.setInitialValues(bookDetails[0]+"Book", Integer.parseInt(bookDetails[7]), newBookId, Enrichment.this);
			setUpProgressDialogForProcessingPages();
			webView.processHtmlPages();*/
			/*String CurrentBook= books.get(EbookClickPosition);
			String[] bookDetails = CurrentBook.split("#");
			int newBookId = fa_enrich.gridShelf.addNewStoreBook(bookDetails[3], bookDetails[4], bookDetails[5], 0, Integer.parseInt(bookDetails[7]), 0, Globals.portrait, true, 1, bookDetails[0]);
			RenderHTMLObjects renderHtmlObjects = new RenderHTMLObjects(fa_enrich, bookDetails[0]+"Book", Integer.parseInt(bookDetails[7]), newBookId, SettingsDialog);
			renderHtmlObjects.processHtmlPages();*/
		/*}
		else{
			AlertDialog.Builder builder = new AlertDialog.Builder(fa_enrich);
			builder.setMessage("Books cannot be downloaded in Rooted Device");
			builder.setCancelable(false);
			builder.setTitle("Rooted Device");
			builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {

				//@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}*/


    }

	/*public void setUpPayment(){
		PayPalPayment payment = payPal.SimplePayment(Float.toString(price));
		// Use checkout to create our Intent.
		Intent checkoutIntent = PayPal.getInstance().checkout(payment, fa_enrich, new ResultDelegate());
		// Use the android's startActivityForResult() and pass in our Intent. This will start the library.
		startActivityForResult(checkoutIntent, request);
	}*/

	/*@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		/*if (requestCode == 2)
			if (resultCode == Activity.RESULT_OK){
				String currentBook = books.get(EbookClickPosition);
				//EDownloadBookValue=1;
				String[] BookDetails = currentBook.split("#");
				////System.out.println("EBookID:"+BookDetails[0]);
				StoreDatabase.insertBnameToPayPaltbl(BookDetails[0]);
				startDownload();
				AlertDialog.Builder builder = new AlertDialog.Builder(fa_enrich);
				builder.setMessage(R.string.sucessfully_completed);
				builder.setCancelable(false);
				builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

					//@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			}else if(resultCode == Activity.RESULT_CANCELED){
				AlertDialog.Builder builder = new AlertDialog.Builder(fa_enrich);
				builder.setMessage(R.string.trans_cancelled);
				builder.setCancelable(false);
				builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

					//@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			}
	}*/

    //PayPal

    public void download() {
        String CurrentBook = books.get(EbookClickPosition);
        ////System.out.println("currentBook: "+CurrentBook);
        String[] BookDetails = CurrentBook.split("#");
        String BookID = BookDetails[0];
        String BookURL = BookDetails[2];
        //String BookDownloadPath ="/sdcard/Manahij/"+BookID;
        ////System.out.println("BookDownloadPath:"+BookDownloadPath);
        //com.semanoor.manahij.ManahijActivity.shelfDestroy=0;			//Called this to delete the contents of country, university, categories array's etc..
        ////System.out.println("shelfdestroy in download: "+ com.semanoor.manahij.ManahijActivity.shelfDestroy);
        //int updateDValue = 0;			 //Changes after merge
        //StoreDatabase.updateEnrichStoreDestroy(updateDValue);
        EWhenDownloadBook = 1;
        /***************************************************************/
        Boolean internalStorageAvailability = BookStorage.checkInternalStorageAvailability();
		/*Boolean internal;
			if(sampleTest==0){
			 internal = true;
			 sampleTest++;
			}else{
				internal = false;
			}
			//Boolean internal = false;*/
        if (internalStorageAvailability) {
            //String internalPath = Environment.getDataDirectory().getPath()+"/"+BookID+"Book";
            //Store to internal memory
            downloadStatus = 1;
            EBookIDCode = BookID;
         //   new progressDownload().execute(BookURL);
            String CurrentBook1 = books.get(EbookClickPosition);
            String[] bookDetails = CurrentBook1.split("#");
            int tabno = Integer.parseInt(bookDetails[34]);
            //StoreDatabase.SaveBooksToDatabase(books,EbookClickPosition, EselectCategoryId);
            int templateId = 6;
            int categoryId;
            if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.eLessonStore)){//if (tabno == Globals.eLessonStore) {
                categoryId = 4;
            } else if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.quizStore)){//if (tabno == Globals.quizStore) {
                categoryId = 3;
            } else if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.rzooomStore)){//if (tabno == Globals.rzooomStore) {
                categoryId = 5;
            } else if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.mindmapStore)){//if (tabno == Globals.mindmapStore) {
                categoryId = 8;
            } else if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.mediaStore)){
                categoryId = 7;
            }else if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.pdfStore)){
                categoryId = 6;
            }
            else {
                categoryId = 1;
            }

            String LSearch, LBookmark, LCopy, LFlip, LNavigation;
            String LHighlight, LNote, LGoToPage, LIndexPage, LZoom, LWebSearch, LWikiSearch, LUpdateCounts, LYoutube, LGoogle, LJazira, LBing, LAsk;
            String LYahoo, LNoor, LNoorBook, LInAppProductID, LTranslationSearch, LDictionarySearch, LBookSearch, LBookLanguage;

            LSearch = bookDetails[8];
            LBookmark = bookDetails[9];
            LCopy = bookDetails[10];
            LFlip = bookDetails[11];
            LNavigation = bookDetails[12];
            LHighlight = bookDetails[13];
            LNote = bookDetails[14];
            LGoToPage = bookDetails[15];
            LIndexPage = bookDetails[16];
            LZoom = bookDetails[17];
            LWebSearch = bookDetails[18];
            LWikiSearch = bookDetails[19];
            LUpdateCounts = bookDetails[20];
            LYoutube = bookDetails[21];
            LGoogle = bookDetails[22];
            LJazira = bookDetails[23];
            LBing = bookDetails[24];
            LAsk = bookDetails[25];
            LYahoo = bookDetails[26];
            LNoor = bookDetails[27];
            LNoorBook = bookDetails[28];
            LTranslationSearch = bookDetails[30];
            LDictionarySearch = bookDetails[31];
            LBookSearch = bookDetails[32];
            LBookLanguage = bookDetails[33];
            String pages = bookDetails[7];
            int pagecount;
            if (pages.equals("null")) {
                pagecount = 0;
            } else {
                pagecount = Integer.parseInt(bookDetails[7]);
            }
            String isDownloadCompleted = "downloading";
            String isEditable = bookDetails[39];
            String downloadURL = bookDetails[2];
            String imgURL = bookDetails[6];
            String price = bookDetails[1];
            String purchaseType = "";
            //String bStoreID=bookDetails[0];
            // bookDetails[0]=fa_enrich.visitedStore.getId()+bookDetails[0];

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(fa_enrich);
            int subscriptDays = Integer.parseInt(prefs.getString(Globals.SUBSCRIPTDAYSLEFT, "0"));
            float flPrice = (float) Float.parseFloat(bookDetails[1]);
            if (flPrice > 0.00) {
                if (Globals.isLimitedVersion()) {
                    if (subscriptDays > 0) {
                        if (fa_enrich.subscription.isBookPurchased) {
                            purchaseType = "purchased";
                        } else {
                            purchaseType = "subscribed";
                        }
                    } else {
                        purchaseType = "purchased";
                    }
                } else {
                    purchaseType = "purchased";
                }
            } else {
                purchaseType = "free";
            }

            final Book newBook = fa_enrich.gridShelf.addNewStoreBook(bookDetails[3], bookDetails[4], bookDetails[5], templateId, pagecount, 0, Globals.portrait, true, 0, bookDetails[0], categoryId, LSearch, LBookmark, LCopy, LFlip, LNavigation, LHighlight, LNote, LGoToPage, LIndexPage, LZoom, LWebSearch, LWikiSearch, LUpdateCounts, LGoogle, LYoutube, LNoor, LNoorBook, LYahoo, LBing, LAsk, LJazira, LBookLanguage, LTranslationSearch, LDictionarySearch, LBookSearch, isDownloadCompleted, downloadURL, isEditable, price, imgURL, purchaseType,fa_enrich.visitedStore.getChildobjlist().get(tabno).getUrl(),fa_enrich.visitedStore.getInAppId());
            String storename = newBook.get_bStoreID();
            String sourceDir;
            String freeFilesPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.getBookID() + "/FreeFiles/";
            sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/Card.gif";
            fa_enrich.downloadingBooksArray.add(newBook.get_bStoreID());
            Bundle bundle = new Bundle();
            String itemId = String.valueOf(newBook.get_bStoreID());
            String name = newBook.getBookTitle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, itemId);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
            fa_enrich.firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
            new saveImage(imgURL, sourceDir, freeFilesPath).execute();
            fa_enrich.background=DownloadBackground.getInstance();
            ArrayList<Book> downloadArray;
            if (fa_enrich.background.getList().size()>0){
                downloadArray = fa_enrich.background.getList();
                downloadArray.add(newBook);
                fa_enrich.background.setList(downloadArray);
            }else{
                downloadArray = new ArrayList<>();
                downloadArray.add(newBook);
                fa_enrich.background.setDownLoadBook(newBook);
                fa_enrich.background.setList(downloadArray);
                fa_enrich.background.setTextCircularProgressBar(null);
                fa_enrich.background.setContext(fa_enrich);
                fa_enrich.background.setDownloadTaskRunning(true);
                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fa_enrich.background.new downloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,newBook.getDownloadURL());
                    }
                },1000);
            }

        }else {
            try {
                //changes for fixing crash issue (try/catch)
                Toast.makeText(fa_enrich.getApplicationContext(), R.string.storage_full, Toast.LENGTH_SHORT).show();
            } catch (RuntimeException e) {
            }
        }
        ////System.out.println("dismiss popupwindow");  
    }


	/*  private void processScrollViewImages(){
       	//System.out.println("ProcessScrollViewImages");
       	//EstoreBookImageInSQL=StoreDatabase.GetStoreBookImageListFromSQL();                    //Method to get the storeBookImageListFrom sql......
       	for(int i=0;i<books.size();i++){
       		String CurrentBook = books.get(i);
       		String[] bookDetails= CurrentBook.split("#");
       		String imgURL =bookDetails[6];
       		String BookID = bookDetails[0];
       		SetBookImageFlag(BookID,imgURL);              //Method to save book image and store and update in database......
       		//String BookImagePath =BookID.concat(".png");
       		String BookImagePath ="/sdcard/StoreImages20/"+BookID+".png";
       		//System.out.println("BookImagePath:"+BookImagePath);
       		/*File file = getBaseContext().getFileStreamPath(BookImagePath);
       		if(file.exists()){
       			//System.out.println("Image file exists at the given path");
       		}
       		else{
       			//System.out.println("Image file doesn't exists at the given path");
       			  //create NoCover.gif image and copy it..
       		}*/
    //SetButtonImages();      					 //Perform  buttonImages in Background.....
	/*	}
       	//System.out.println("Finished downloading and copying book images");
       }

       public String SetBookImageFlag(String BookID, String imgurl){
       	//System.out.println("SetBookImageFlag");
       	String returnValue="";
       	if(EstoreBookImageInSQL.size() == 0){ //Crash due to size.....
       		StoreDatabase.SaveStoreBookImage(BookID,imgurl);
       		StoreDatabase.InsertStoreBookImage(BookID,imgurl);
       		returnValue = BookID;
       	}
       	else{
       			int flagExist = 0;
       			int flagupdate = 0;
       			for(int i=0;i<EstoreBookImageInSQL.size();i++){
       				String[] splitEstoreBookImageArray = EstoreBookImageInSQL.get(i).split("#");
       				if(splitEstoreBookImageArray[0].equals(BookID)){
       					flagExist = 1;
       					if(!(splitEstoreBookImageArray[1].equals(imgurl))){
       						flagupdate = 1;
       					}
       				break;	
       				}
       			}
       			if(flagExist == 1){
       				if(flagupdate == 0){
       					returnValue = BookID;
       				}
       				else{
       					StoreDatabase.SaveStoreBookImage(BookID,imgurl);
       		    		StoreDatabase.UpdateStoreBookImage(BookID,imgurl);
       		    		returnValue = BookID;
       				}
       			}
       			else{
   	    				StoreDatabase.SaveStoreBookImage(BookID,imgurl);
   			    		StoreDatabase.InsertStoreBookImage(BookID,imgurl);
   			    		returnValue = BookID;
       				}
       	}
   		return returnValue;	
       }*/

    /*
	 *  Download files with the argument url's
	 */
    public Boolean Downloadfile(String DownloadURL, String file) {
        final int TIMEOUT_CONNECTION = 5000;//5sec
        final int TIMEOUT_SOCKET = 30000;//30sec
        try {
            URL url = new URL(DownloadURL);
            long startTime = System.currentTimeMillis();
            ////System.out.println( "image download beginning: "+DownloadURL);
            //Open a connection to that URL.
            URLConnection ucon = url.openConnection();
            //this timeout affects how long it takes for the app to realize there's a connection problem
            ucon.setReadTimeout(TIMEOUT_CONNECTION);
            ucon.setConnectTimeout(TIMEOUT_SOCKET);
            //Define InputStreams to read from the URLConnection.
            // uses 3KB download buffer
            InputStream is = ucon.getInputStream();
            BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);

            // new File(file).delete();
            // FileOutputStream outStream = new FileOutputStream(file);
            FileOutputStream outStream = fa_enrich.openFileOutput(file, fa_enrich.MODE_PRIVATE);
            byte[] buff = new byte[5 * 1024];
            //Read bytes (and store them) until there is nothing more to read(-1)
            int len;
            while ((len = inStream.read(buff)) != -1) {
                outStream.write(buff, 0, len);
            }
            //clean up
            outStream.flush();
            outStream.close();
            inStream.close();
            ////System.out.println( "download completed in "+ ((System.currentTimeMillis() - startTime) / 1000)+ " sec");
            return true;
        } catch (IOException e) {
            ////System.out.println("error while storing:"+e);
            return false;
        }
    }

    //Language methods:
    public void setLanguage() {
        //language = StoreDatabase.checkLanguage();
        //language = 1;
        //StoreTabViewFragment.setTabTitle();
        //if(language ==1){
			/*searchEditText.setHint("Search");
			LanguageBtn.setText("Language");
			LibraryBtn.setText("Library");
			CountryText.setText("Countries");
			UniversityText.setText("Academic Year");
			CategoryText.setText("Grade");
			PriceText.setText("Price");*/
        if (countryList != null && countryList.size() == 0) {
            CountryBtn.setText("");
        } else {
            if (selectedCountry != null) {
                String countryLabel;
                if (fa_enrich.language.equals("en")) {
                    countryLabel = selectedCountry.getCountryEngName();
                } else {
                    countryLabel = selectedCountry.getCountryArabName();
                }
                CountryBtn.setText(countryLabel);
            }
        }
        if (filteredUniversityList != null && filteredUniversityList.size() == 0) {
            UniversityBtn.setText("");
        } else {
            if (selectedUniversity != null) {
                String universityLabel;
                if (fa_enrich.language.equals("en")) {
                    universityLabel = selectedUniversity.getUniversityEngName();
                } else {
                    universityLabel = selectedUniversity.getUniversityArabName();
                }
                UniversityBtn.setText(universityLabel);
            }
        }
        if (categoryList != null && categoryList.size() == 0) {
            CategoriesBtn.setText("");
        } else {
            if (selectedCategory != null) {
                String categoriesLabel;
                if (fa_enrich.language.equals("en")) {
                    categoriesLabel = selectedCategory.getCategoryEngName();
                } else {
                    categoriesLabel = selectedCategory.getCategoryArabName();
                }
                CategoriesBtn.setText(categoriesLabel);
            }
        }
        if (EpriceArrayContent != null) {
            String priceLabel = EpriceArrayContent.get(EselectedPriceIndex).split("#")[0];
            PriceBtn.setText(priceLabel);
            EpriceArray.clear();
            for (int i = 0; i < EpriceArrayContent.size(); i++) {
                String content = EpriceArrayContent.get(i).split("#")[0];
                EpriceArray.add(content);
            }
            EsearchFilterArray.clear();
            for (int j = 0; j < EsearchFilterArrayContent.size(); j++) {
                String content = EsearchFilterArrayContent.get(j).split("#")[0];
                EsearchFilterArray.add(content);
            }
				/*bookmessage.setText("No books are available in this category");
			intMesg1.setText("Cannot Connect to the Manahij Store");
			intMesg2.setText("Please check your Internet connection to access the Manahij store.");*/
        }
        //}else{

			/*searchEditText.setHint("√ò¬®√ò¬≠√ò¬´");
			LanguageBtn.setText("√ò¬ß√ô‚Äû√ô‚Äû√ò¬∫√ò¬©");
			LibraryBtn.setText("√ò¬ß√ô‚Äû√ô‚Ä¶√ô∆í√ò¬™√ò¬®√ò¬©");
			CountryText.setText("√ò¬ß√ô‚Äû√ò¬®√ô‚Äû√ò¬Ø√ò¬ß√ô‚Ä†");
			UniversityText.setText("√ò¬ß√ô‚Äû√ò¬≥√ô‚Ä†√ò¬© √ò¬ß√ô‚Äû√ò¬Ø√ò¬±√ò¬ß√ò¬≥√ô≈†√ò¬©");
			CategoryText.setText("√ò¬ß√ô‚Äû√ô‚Ä¶√ò¬±√ò¬≠√ô‚Äû√ò¬©");
			PriceText.setText("√ò¬ß√ô‚Äû√ò¬≥√ò¬π√ò¬±");*/
			/*if(countryList.size() == 0){
				CountryBtn.setText("");
			}else{
				String countryLabel = selectedCountry.getCountryArabName();
				if(countryLabel.length()>6)
					countryLabel = countryLabel.substring(0,6)+"..";
				CountryBtn.setText(countryLabel);
			}
			if(filteredUniversityList.size() == 0){
				UniversityBtn.setText("");
			}else{
				String universityLabel = selectedUniversity.getUniversityArabName();
				if(universityLabel.length()>6)
					universityLabel = universityLabel.substring(0,6)+"..";
				UniversityBtn.setText(universityLabel);
			}
			if(categoryList.size() == 0){
				CategoriesBtn.setText("");
			}else{
				String categoriesLabel = selectedCategory.getCategoryArabName();
				if(categoriesLabel.length()>6)
					categoriesLabel = categoriesLabel.substring(0,6)+"..";
				CategoriesBtn.setText(categoriesLabel);
			}
			String priceLabel = EpriceArrayContent.get(EselectedPriceIndex).split("#")[1];
			PriceBtn.setText(priceLabel);
			EpriceArray.clear();
			for(int i=0;i<EpriceArrayContent.size();i++){
				String content = EpriceArrayContent.get(i).split("#")[1];
				EpriceArray.add(content);
			}
			EsearchFilterArray.clear();
			for(int j=0;j<EsearchFilterArrayContent.size();j++){
				String content = EsearchFilterArrayContent.get(j).split("#")[1];
				EsearchFilterArray.add(content);
			}
			/*bookmessage.setText("√ô‚Äû√ò¬ß √ô≈†√ôÀÜ√ò¬¨√ò¬Ø √ô∆í√ò¬™√ò¬® √ô‚Ä¶√ò¬™√ò¬ß√ò¬≠√ò¬© √ôÔøΩ√ô≈† √ô‚Ä°√ò¬∞√ò¬ß √ò¬ß√ô‚Äû√ò¬™√ò¬µ√ô‚Ä†√ô≈†√ôÔøΩ");
			intMesg1.setText("√ô‚Äû√ò¬ß √ô≈†√ô‚Ä¶√ô∆í√ô‚Ä† √ò¬ß√ô‚Äû√ôÀÜ√ò¬µ√ôÀÜ√ô‚Äû √ò¬•√ô‚Äû√ô‚Ä∞ √ô‚Ä¶√ò¬™√ò¬¨√ò¬± √ò¬ß√ô‚Äû√ô‚Ä¶√ô‚Ä†√ò¬ß√ô‚Ä°√ò¬¨");
			intMesg2.setText("√ôÔøΩ√ò¬∂√ô‚Äû√ò¬ß √ò¬™√ò¬≠√ô‚Äö√ô‚Äö √ô‚Ä¶√ô‚Ä† √ò¬ß√ô‚Äû√ò¬•√ò¬™√ò¬µ√ò¬ß√ô‚Äû √ò¬®√ò¬¥√ò¬®√ô∆í√ò¬© √ò¬ß√ô‚Äû√ò¬•√ô‚Ä†√ò¬™√ò¬±√ô‚Ä†√ò¬™ √ô‚Äû√ô‚Äû√ôÀÜ√ò¬µ√ôÀÜ√ô‚Äû √ò¬•√ô‚Äû√ô‚Ä∞ √ô‚Ä¶√ò¬™√ò¬¨√ò¬± √ò¬ß√ô‚Äû√ô‚Ä¶√ô‚Ä†√ò¬ß√ô‚Ä°√ò¬¨");*/
        //}*/
    }


    public Boolean CheckMediaAvailability() {
        Boolean Available_Write = false;
        ////System.out.println("checking Media availability");
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            Available_Write = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            Available_Write = false;
        } else {
            Available_Write = false;
        }
        return Available_Write;
    }

    public void StartEncrypting(String inputFile, String outputFile) throws InvalidAlgorithmParameterException {
        ////System.out.println("started encrypting");
        FileInputStream fis;
        FileOutputStream fos;
        CipherOutputStream cos;
        byte[] iv = {1, 5, 0xB, 4, 7, 0xF, 9, 3, 0x17, 1, 6, 0xC, 8, 0xD, 91, 0xA};
        IvParameterSpec IV = new IvParameterSpec(iv);
        final String CipherModePadding = "AES/CBC/PKCS7Padding";
        try {
            fis = new FileInputStream(inputFile);
            fos = new FileOutputStream(outputFile);
            SecretKeySpec skeySpec = new SecretKeySpec("NoorSemaTecIndia".getBytes(), "AES");
            Cipher cipher1 = Cipher.getInstance(CipherModePadding);
            cipher1.init(Cipher.ENCRYPT_MODE, skeySpec, IV);

            cos = new CipherOutputStream(fos, cipher1);
            // Here you read from the file in fis and write to cos.
            byte[] b = new byte[4 * 1024];   //Multiplying by 4
            int i = fis.read(b);
            while (i != -1) {
                cos.write(b, 0, i);
                i = fis.read(b);
            }
            cos.flush();
            cos.close();
            fis.close();
            ////System.out.println("finished Encrypting");
        } catch (IOException e) {
            ////System.out.println("errorrrrrrr:"+e);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            ////System.out.println("errorrrrrrr:"+e);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            ////System.out.println("errorrrrrrr:"+e);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            ////System.out.println("errorrrrrrr:"+e);
        }
    }

    @Override
    public void onStop() {
        ////System.out.println("called onStop");
        if (EWhenDownloadBook == 1) {
            int updateDValue = 0;
            StoreDatabase.updateEnrichStoreDestroy(updateDValue);
			/*CopyStoreContent.copyEnrichContent(EcountryArray,EuniversityArray,EcategoryArray,filteredEuniversityArray,
					EbooksOriginal,ESelectedCountryID,EUniversityIDInt,ELocalCategoryName,serviceType,EcountryImagePath, EuniversityImagePath,EselectedCountryIndex,EselectedPublisherIndex,
					EselectedCategoriesIndex,EselectedPriceIndex);*/
        } else {
            EWhenDownloadBook = 0;
            int updateDValue = 1;
            StoreDatabase.updateEnrichStoreDestroy(updateDValue);
			/*CopyStoreContent.copyEnrichContent(EcountryArray,EuniversityArray,EcategoryArray,filteredEuniversityArray,
					EbooksOriginal,ESelectedCountryID,EUniversityIDInt,ELocalCategoryName,serviceType,EcountryImagePath, EuniversityImagePath,EselectedCountryIndex,EselectedPublisherIndex,
					EselectedCategoriesIndex,EselectedPriceIndex);*/
        }
        super.onStop();
    }


    /*
	 * AsyncTask for downloading book  
	 */
    private class progressDownload extends AsyncTask<String, Integer, String> {
        public String DprogressString = "";
        public Boolean downloadSuccess;

        protected void onPreExecute() {
            super.onPreExecute();
            ////System.out.println("OnPreExecute downloading In internal Storage");
            downloadProgDialog.show();
        }

        @Override
        protected String doInBackground(String... downloadurl) {

            final int TIMEOUT_CONNECTION = 5000;//5sec
            final int TIMEOUT_SOCKET = 30000;//30sec
            long bytes = 1024 * 1024;
            try {
                URL url = new URL(downloadurl[0]);
                //Open a connection to that URL.
                URLConnection ucon = url.openConnection();
                ucon.connect();
                int fileLength = ucon.getContentLength();
                ////System.out.println("filelength:"+fileLength);
                long startTime = System.currentTimeMillis();

                //this timeout affects how long it takes for the app to realize there's a connection problem
                ucon.setReadTimeout(TIMEOUT_CONNECTION);
                ucon.setConnectTimeout(TIMEOUT_SOCKET);
                //Define InputStreams to read from the URLConnection.
                // uses 3KB download buffer
                InputStream is = ucon.getInputStream();
                BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);

                // new File(file).delete();
                //Internal storage
                //FileOutputStream outStream = new FileOutputStream(DownloadPath);
                String bookPath = EBookIDCode;
                FileOutputStream outStream = fa_enrich.openFileOutput(bookPath, fa_enrich.MODE_PRIVATE);
                byte[] buff = new byte[5 * 1024];
                //Read bytes (and store them) until there is nothing more to read(-1)
                int len;
                String totalMB, fileLengthMB;
                long total = 0;
                DecimalFormat twoDformat = new DecimalFormat("#.##");
                while ((len = inStream.read(buff)) != -1) {
                    total += len;
                    //////System.out.println("total:"+total);
                    totalMB = twoDformat.format((float) total / bytes);
                    fileLengthMB = twoDformat.format((float) fileLength / bytes);
                    DprogressString = totalMB + " MB" + " of " + fileLengthMB + " MB";
                    publishProgress((int) ((total * 100) / fileLength));
                    outStream.write(buff, 0, len);
                }
                //clean up
                outStream.flush();
                outStream.close();
                inStream.close();
                downloadSuccess = true;
                downloadStatus = 0;
                ////System.out.println( "download completed in "+ ((System.currentTimeMillis() - startTime) / 1000)+ " sec");

            } catch (IOException e) {
                ////System.out.println("error while storing:"+e);
                downloadSuccess = false;
                downloadStatus = 0;
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            //////System.out.println("OnprogressUpdate:"+progress[0]);
            downloadProgDialog.setMessage(DprogressString);
            downloadProgDialog.setProgress(progress[0]);

        }

        protected void onPostExecute(String result) {
            ////System.out.println("OnpostExecute");
            if (downloadSuccess) { //unzipping will be done when downloadSuccess is true.
                //setUpProgressDialogForProcessingPages();
                String bookpath = EBookIDCode;   //Changes after merge

                // String book="M".concat(EBookIDCode);
                //book=EBookIDCode.replaceFirst(EBookIDCode, book);
                ////System.out.println("book"+book);

                String zipfile = Globals.TARGET_BASE_FILE_PATH.concat(bookpath);
                String unzippathWithSlash = EBookIDCode.concat("Book/");
                //String unzippathWithOutSlash=bookpath.concat("Book");
                //File file1 = fa_enrich.getDir(unzippathWithOutSlash, Context.MODE_PRIVATE);
                String unziplocation = Globals.TARGET_BASE_BOOKS_DIR_PATH.concat(unzippathWithSlash); //"/" is a must here..
                boolean unzipSuccess = unzipDownloadfileNew(zipfile, unziplocation); //if unzipping success then need to store in database:
                if (unzipSuccess) {
                    if (price > 0.00) {
                        StoreDatabase.deleteBnamefromPayPaltbl(EBookIDCode);
                    }
                    String CurrentBook = books.get(EbookClickPosition);
                    String[] bookDetails = CurrentBook.split("#");
                    int tabno = Integer.parseInt(bookDetails[34]);
                    if (fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+ Globals.eLessonStore) || fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.quizStore) || fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+ Globals.rzooomStore) || fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.mindmapStore)|| fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.pdfStore)) {
                        setUpProcessDialogForSavingImage();
                    } else {
                        setUpProgressDialogForProcessingPages();
                    }
                    //StoreDatabase.SaveBooksToDatabase(books,EbookClickPosition, EselectCategoryId);
                    int templateId = 6;
                    int categoryId;
                    if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.eLessonStore)){//if (tabno == Globals.eLessonStore) {
                        categoryId = 4;
                    } else if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.quizStore)){//if (tabno == Globals.quizStore) {
                        categoryId = 3;
                    } else if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.rzooomStore)){//if (tabno == Globals.rzooomStore) {
                        categoryId = 5;
                    } else if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.mindmapStore)){//if (tabno == Globals.mindmapStore) {
                        categoryId = 8;
                    } else if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.mediaStore)){
                        categoryId = 7;
                    }else if(fa_enrich.visitedStore.getChildobjlist().get(tabno).getShelfPosition().equals(""+Globals.pdfStore)){
                        categoryId = 6;
                    }
                    else {
                        categoryId = 1;
                    }

                    String LSearch, LBookmark, LCopy, LFlip, LNavigation;
                    String LHighlight, LNote, LGoToPage, LIndexPage, LZoom, LWebSearch, LWikiSearch, LUpdateCounts, LYoutube, LGoogle, LJazira, LBing, LAsk;
                    String LYahoo, LNoor, LNoorBook, LInAppProductID, LTranslationSearch, LDictionarySearch, LBookSearch, LBookLanguage;

                    LSearch = bookDetails[8];
                    LBookmark = bookDetails[9];
                    LCopy = bookDetails[10];
                    LFlip = bookDetails[11];
                    LNavigation = bookDetails[12];
                    LHighlight = bookDetails[13];
                    LNote = bookDetails[14];
                    LGoToPage = bookDetails[15];
                    LIndexPage = bookDetails[16];
                    LZoom = bookDetails[17];
                    LWebSearch = bookDetails[18];
                    LWikiSearch = bookDetails[19];
                    LUpdateCounts = bookDetails[20];
                    LYoutube = bookDetails[21];
                    LGoogle = bookDetails[22];
                    LJazira = bookDetails[23];
                    LBing = bookDetails[24];
                    LAsk = bookDetails[25];
                    LYahoo = bookDetails[26];
                    LNoor = bookDetails[27];
                    LNoorBook = bookDetails[28];
                    LTranslationSearch = bookDetails[30];
                    LDictionarySearch = bookDetails[31];
                    LBookSearch = bookDetails[32];
                    LBookLanguage = bookDetails[33];
                    String pages = bookDetails[7];
                    int pagecount;
                    if (pages.equals("null")) {
                        pagecount = 0;
                    } else {
                        pagecount = Integer.parseInt(bookDetails[7]);
                    }
                    String isDownloadCompleted = "downloaded";
                    String isEditable = bookDetails[39];
                    String downloadURL = bookDetails[2];
                    String imgURL = bookDetails[6];
                    String price = bookDetails[1];
                    String purchaseType = "";
                    //String bStoreID=bookDetails[0];
                   // bookDetails[0]=fa_enrich.visitedStore.getId()+bookDetails[0];

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(fa_enrich);
                    int subscriptDays = Integer.parseInt(prefs.getString(Globals.SUBSCRIPTDAYSLEFT, "0"));
                    float flPrice = (float) Float.parseFloat(bookDetails[1]);
                    if (flPrice > 0.00) {
                        if (Globals.isLimitedVersion()) {
                            if (subscriptDays > 0) {
                                if (fa_enrich.subscription.isBookPurchased) {
                                    purchaseType = "purchased";
                                } else {
                                    purchaseType = "subscribed";
                                }
                            } else {
                                purchaseType = "purchased";
                            }
                        } else {
                            purchaseType = "purchased";
                        }
                    } else {
                        purchaseType = "free";
                    }

                    Book newBook = fa_enrich.gridShelf.addNewStoreBook(bookDetails[3], bookDetails[4], bookDetails[5], templateId, pagecount, 0, Globals.portrait, true, 0, bookDetails[0], categoryId, LSearch, LBookmark, LCopy, LFlip, LNavigation, LHighlight, LNote, LGoToPage, LIndexPage, LZoom, LWebSearch, LWikiSearch, LUpdateCounts, LGoogle, LYoutube, LNoor, LNoorBook, LYahoo, LBing, LAsk, LJazira, LBookLanguage, LTranslationSearch, LDictionarySearch, LBookSearch, isDownloadCompleted, downloadURL, isEditable, price, imgURL, purchaseType, fa_enrich.visitedStore.getChildobjlist().get(tabno).getUrl(),fa_enrich.visitedStore.getInAppId());
                    fa_enrich.downloadedBooksArray.add(bookDetails[0]);
                    gridview.invalidateViews();
                    String storename = newBook.get_bStoreID();
                    String sourceDir;
                    String freeFilesPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.getBookID() + "/FreeFiles/";

                    if (storename.contains("E")){
						/*sourceDir= Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.get_bStoreID()+"Book/Resources/";
						//String card_path = freeFilesPath+"card.png";
						String split[]=newBook.get_bStoreID().split("E");
					  if (new File(sourceDir+"ScreenShot_"+split[1]+".png").exists()) {
						  coverImageForStoreBook(sourceDir+"ScreenShot_"+split[1]+".png",freeFilesPath);
					  }else if(new File(sourceDir+"ScreenShot_"+split[1]+".gif").exists()){
						  coverImageForStoreBook(sourceDir+"ScreenShot_"+split[1]+".gif",freeFilesPath);
					  }else if(new File(sourceDir+"ScreenShot_"+split[1]+".jpg").exists()){
						  coverImageForStoreBook(sourceDir+"ScreenShot_"+split[1]+".jpg",freeFilesPath);
					  }*/

                        sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/Card.gif";
                        new saveImage(imgURL, sourceDir, freeFilesPath).execute();

                        String filePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/Data/";
                        changingfilespathname(filePath);
                        if (processDialog!=null){
                            processDialog.dismiss();
                            processDialog=null;
                        }
                        bookModalPopwindow.dismiss();
                    } else if (storename.contains("Q")) {
                        sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/Card.gif";
                        if (new File(sourceDir).exists()) {
                            coverImageForStoreBook(sourceDir, freeFilesPath);
                        } else {
                            new saveImage(imgURL, sourceDir, freeFilesPath).execute();
                        }
                        bookModalPopwindow.dismiss();
                    } else if (storename.contains("R")) {
                        sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/Card.gif";
                        if (new File(sourceDir).exists()) {
                            coverImageForStoreBook(sourceDir, freeFilesPath);
                        } else {
                            new saveImage(imgURL, sourceDir, freeFilesPath).execute();
                        }
                        bookModalPopwindow.dismiss();

                    } else if (storename.contains("I")) {
                        sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/Card.gif";
                        if (new File(sourceDir).exists()) {
                            coverImageForStoreBook(sourceDir, freeFilesPath);
                        } else {
                            new saveImage(imgURL, sourceDir, freeFilesPath).execute();
                        }
                        String srcPath=Globals.TARGET_BASE_FILE_PATH+"books/"+newBook.get_bStoreID()+"Book";
                        File mindMapDir = new File(srcPath);
                        File[] mindMapFiles = mindMapDir.listFiles();
                        for (File file:mindMapFiles){
                            String filaName=file.getName();
                            String desPath = Globals.TARGET_BASE_FILE_PATH+"books/"+ newBook.getBookID()+"/mindMaps/";
                            if (file.isFile()&&filaName.contains(".xml")){
                                UserFunctions.copyFiles(file.getPath(),desPath+filaName);
                            }else if(file.isDirectory()){
                                try {
                                    UserFunctions.copyDirectory(file,new File(desPath));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        bookModalPopwindow.dismiss();
                    } else if (storename.contains("P")) {
                        sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/Card.gif";
                        if (new File(sourceDir).exists()) {
                            coverImageForStoreBook(sourceDir, freeFilesPath);
                        } else {
                            new saveImage(imgURL, sourceDir, freeFilesPath).execute();
                        }
                        bookModalPopwindow.dismiss();

                        String pdfName = null;
                        File srcDir = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.get_bStoreID()+"Book");
                        String[] path = srcDir.list();
                        for(String file : path) {
                            String fromPath = srcDir + "/" + file;
                            String[] str = file.split("\\.");
                            //title_text.setText(str[0]);
                            if(file.contains(".pdf")){
                                pdfName=fromPath;
                                break;
                            }
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            //setUpProcessDialogForSavingImage();
                            //PdfRenderer pr = new PdfRenderer(ParcelFileDescriptor.open(new File(pdfName), ParcelFileDescriptor.MODE_READ_WRITE));
                           // CopyOpenPdfReader pdfReader = new CopyOpenPdfReader(fa_enrich);
                            //pdfReader.new takeScreenShot(pdfName, newBook, textCircularProgressBar).execute();
                        }

                    } else {
                        sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/Card.gif";
                        UserFunctions.makeFileWorldReadable(Globals.TARGET_BASE_BOOKS_DIR_PATH + newBook.get_bStoreID() + "Book/");
                        if (new File(sourceDir).exists()) {
                            coverImageForStoreBook(sourceDir, freeFilesPath);
                        } else {
                            new saveImage(imgURL, sourceDir, freeFilesPath).execute();
                        }
                        //bookModalPopwindow.dismiss();
//                        webView.setInitialValues(bookDetails[0] + "Book", Integer.parseInt(bookDetails[7]), newBook, Enrichment.this);
//                        webView.processHtmlPages();
                    }
                }
                DownloadCounts.downloadCountsWebservice(EBookIDCode); //To call webservice download counts webservice.
                //Need to delete the zip file which is download
                File fileDir = fa_enrich.getFilesDir();   //Deleting zip file in the internal storage
                File zipfilelocation = new File(fileDir, EBookIDCode);
                // File file =  new File(zipfile);
                try {
                    zipfilelocation.delete();
                } catch (Exception e) {
                    ////System.out.println("Failed to deletefile with Error "+e);
                }
            }
        }

    }

    public class saveImage extends AsyncTask<Void, Void, Void> {
        String sourcePath, destPath;
        String freeFilesPath;

        public saveImage(String sourcePath, String destPath, String freeFilesPath) {
            this.sourcePath = sourcePath;
            this.destPath = destPath;
            this.freeFilesPath = freeFilesPath;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                URL url = new URL(sourcePath);
                Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                UserFunctions.saveBitmapImage(bitmap, destPath);
                String card_path = freeFilesPath + "card.png";
                if (!new File(card_path).exists()) {
                    Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(200, fa_enrich), Globals.getDeviceIndependentPixels(300, fa_enrich), true);
                    UserFunctions.saveBitmapImage(cardBitmap, card_path);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    public void changingfilespathname(String filePath) {
        //String filePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+ currentBook.get_bStoreID()+"Book/Data/";
        //String sourceString = UserFunctions.readFileFromPath(new File(filePath));
        File srcDir = new File(filePath);
        String[] path = srcDir.list();
        File[] files = srcDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                String split[] = file.getName().split("_");
                if (split[0].equals("PageName")) {
                    File[] fileList = file.listFiles();
                    for (File fileslist : fileList) {
                        String fileName = String.valueOf(fileslist);
                        int i = fileName.lastIndexOf('/');
                        String fileEx = fileName.substring(i + 1);
//						String split1[] = fileEx.split(".");
//						if (split1.length == 2){
                        if (fileEx.contains(".html") || fileEx.contains(".htm")) {
                            String sourceString1 = UserFunctions.readFileFromPath(fileslist);
                            sourceString1 = sourceString1.replaceAll("file://///200.1.1.66/IJST/", "file:../../../IJST/");
                            //System.out.println(sourceString1);
                            //sourceString1.contentEquals("")
                            //FileWriter fileWriter;
                            try {
                                FileWriter fileWriter = new FileWriter(fileslist);
                                fileWriter.write(sourceString1);
                                fileWriter.close();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                        }
//					}

                    }
                }
            }

        }

    }

    public void coverImageForStoreBook(String bookImagePath, String freeFilesPath) {
        if(new File(bookImagePath).exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(bookImagePath);
            String card_path = freeFilesPath + "card.png";
            if (!new File(card_path).exists()) {
                Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(200, fa_enrich), Globals.getDeviceIndependentPixels(300, fa_enrich), true);
                UserFunctions.saveBitmapImage(cardBitmap, card_path);
            }
        }
//        if (processDialog != null) {
//            processDialog.dismiss();
//        }
    }

    private void setUpProgressDialogForProcessingPages() {
        processPageProgDialog = new ProgressDialog(fa_enrich);
        processPageProgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        processPageProgDialog.setTitle("Processing Files ...");
        processPageProgDialog.setMessage("");
        processPageProgDialog.getWindow().setTitleColor(Color.rgb(222, 223, 222));
        processPageProgDialog.setIndeterminate(false);
        processPageProgDialog.setMax(100);
        processPageProgDialog.setCancelable(false);
        processPageProgDialog.setCanceledOnTouchOutside(false);
        processPageProgDialog.show();
    }

    private void setUpProcessDialogForSavingImage() {
        processDialog = new ProgressDialog(fa_enrich);
        processDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        processDialog.setMessage(Enrichment.this.getResources().getString(R.string.processing));
        processDialog.setCancelable(false);
        processDialog.setCanceledOnTouchOutside(false);
        processDialog.show();
    }

    private boolean unzipDownloadfileNew(String zipfilepath, String unziplocation) {
        //String eleessonPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/SboookAuthor/normalbooks1/";
        ZipFile zipFile = null;
        List<FileHeader> headers = null;
        try {
            zipFile = new ZipFile(new File(zipfilepath));
            headers = zipFile.getFileHeaders();
        } catch (ZipException e) {
            e.printStackTrace();
            if (downloadProgDialog != null &&downloadProgDialog.isShowing()) {
                 downloadProgDialog.dismiss();
            }
            if(bookModalPopwindow!=null &&downloadProgDialog.isShowing()) {
                bookModalPopwindow.dismiss();
             }
            downloadProgDialog = null;
        }

        if (headers != null) {
            for (int i = 0; i < headers.size(); i++) {
                FileHeader header = headers.get(i);
                header.setFileName(header.getFileName().replace("\\", "//"));
                //File  file = new File(eleessonPath+header.getFileName());
                try {
                    zipFile.extractFile(header, unziplocation);
                } catch (ZipException e) {
                    e.printStackTrace();

                      if(downloadProgDialog != null &&downloadProgDialog.isShowing()) {
                            downloadProgDialog.dismiss();
                        }
                    if(bookModalPopwindow!=null &&bookModalPopwindow.isShowing()) {
                        bookModalPopwindow.dismiss();
                    }
                    downloadProgDialog = null;
                }

            }
        }
        if (downloadProgDialog != null &&downloadProgDialog.isShowing()) {
            downloadProgDialog.dismiss();
        }
        downloadProgDialog = null;
        return true;
    }

	/*private Boolean unzipDownloadfileNew(String zipfilepath, String unziplocation)
	{ 
		InputStream is;
		ZipInputStream zis;
		try
		{
			is = new FileInputStream(zipfilepath);
			zis = new ZipInputStream(new BufferedInputStream(is));
			ZipEntry ze = zis.getNextEntry();
			while (ze != null) 
			{	
				String zipEntryName = ze.getName();
				zipEntryName = zipEntryName.replace("\\", "/");
				//System.out.println("zipEntryName: "+zipEntryName);
				File  file = new File(unziplocation+zipEntryName);
				if(file.exists()){
					//System.out.println("file exists at the given path:" +file);
				}else{

					if(ze.isDirectory()){
						////System.out.println("Ze is an directory. It will create directory and exit");
						file.mkdirs();
						continue;
					}

					file.getParentFile().mkdirs();   //Horror line..............
					byte[] buffer = new byte[1024];
					FileOutputStream fout = new FileOutputStream(file);
					BufferedOutputStream baos  = new BufferedOutputStream(fout, 1024);
					int count;
					while((count = zis.read(buffer, 0, 1024))!= -1){
						baos.write(buffer, 0, count);
					}
					baos.flush();
					baos.close();
				}
				try {
					ze = zis.getNextEntry();
				} catch (Exception e) {
					ze = zis.getNextEntry();
				}
			}
			zis.close();
			if(downloadProgDialog!= null){
				downloadProgDialog.dismiss();
			}
			//bookModalPopwindow.dismiss();
			downloadProgDialog = null;
			//SettingsDialog.show();
			////System.out.println("finished unzipping");
			return true;
		}catch(Exception e){
			////System.out.println("unzipping error"+e);
			if(downloadProgDialog!= null){
				downloadProgDialog.dismiss();
			}
			bookModalPopwindow.dismiss();
			downloadProgDialog = null;
			return false;
		}
	}*/

    @Override
    public void onPause() {
        super.onPause();
        //fa_enrich.unregisterReceiver(mNetworkStateIntentReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /*
	 * AsyncTask for checking rooted or not
	 */
    private class CheckRooted extends AsyncTask<String, Integer, String> {
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... downloadurl) {
            rootedDevice = Root.isDeviceRooted();
            return null;
        }

        protected void onPostExecute(String result) {

        }
    }

    public void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            try {
                ((ViewGroup) view).removeAllViews();
            } catch (Exception e) {
                // TODO: handle exception

            }

        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            cancelSoapCallMethods(storeTabPosition);
            //new progressDownload().cancel(true);
            fa_enrich.unregisterReceiver(mNetworkStateIntentReceiver);
            unbindDrawables(ll_enrich.findViewById(R.id.storeView));
            System.gc();
        } catch (OutOfMemoryError e) {
        } catch (NullPointerException e) {
        } catch (RuntimeException e) {
        } catch (Exception e) {
        }
    }
    private boolean countryIdExistForClient(int clientId){
       String countryStr = fa_enrich.visitedStore.getChildobjlist().get(storeTabPosition).getCountry();
        String[] countrySplit = countryStr.split("#");
        if (countrySplit.length>0){
            for (String str:countrySplit){
                String id = String.valueOf(clientId);
                if (str.equals(id)){
                    return true;
                }
            }
        }
        return false;
    }
    private boolean categoryIdExistForClient(int clientId){
        String countryStr = fa_enrich.visitedStore.getChildobjlist().get(storeTabPosition).getCategory();
        String[] countrySplit = countryStr.split("#");
        if (countrySplit.length>0){
            for (String str:countrySplit){
                String id = String.valueOf(clientId);
                if (str.equals(id)){
                    return true;
                }
            }
        }
        return false;
    }
    private boolean universityIdExistForClient(int clientId){
        String countryStr = fa_enrich.visitedStore.getChildobjlist().get(storeTabPosition).getUniversity();
        String[] countrySplit = countryStr.split("#");
        if (countrySplit.length>0){
            for (String str:countrySplit){
                String id = String.valueOf(clientId);
                if (str.equals(id)){
                    return true;
                }
            }
        }
        return false;
    }
}






