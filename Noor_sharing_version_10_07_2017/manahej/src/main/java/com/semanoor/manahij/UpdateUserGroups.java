package com.semanoor.manahij;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

/**
 * Created by karthik on 18-05-2017.
 */

public class UpdateUserGroups extends AsyncTask<Void,Void,Void> {
    private Context context;

    public UpdateUserGroups(Context ctx){
        this.context = ctx;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String scopeId = prefs.getString(Globals.sUserIdKey, "");
        com.semanoor.manahij.getGroupsList groupsList = new com.semanoor.manahij.getGroupsList(context);
      //  Clients clients = Clients.getInstance();
        if (scopeId.equals("")){
            scopeId = "0";
        }
        String deviceId = UserFunctions.getMacAddress(context);
        String groupUrl = Globals.groupUrl+scopeId+"&MacId="+deviceId;
     //   clients.getClientsList(Globals.redeemURL + scopeId, scopeId);
        groupsList.getGroupsList(groupUrl, scopeId);
        groupsList.getInAppIdList(Globals.redeemInAppIdUrl+scopeId,scopeId);
        return null;
    }

    @Override
    protected void onPostExecute(Void newVersion) {
        super.onPostExecute(newVersion);
    }
}
