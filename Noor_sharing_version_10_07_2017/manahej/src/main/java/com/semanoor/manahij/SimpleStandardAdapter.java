package com.semanoor.manahij;

import java.util.ArrayList;

import pl.polidea.treeview.AbstractTreeViewAdapter;
import pl.polidea.treeview.TreeNodeInfo;
import pl.polidea.treeview.TreeStateManager;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.semanoor.source_sboookauthor.UserFunctions;

/**
 * This is a very simple adapter that provides very basic tree view with a
 * checkboxes and simple item description.
 * 
 */
class SimpleStandardAdapter extends AbstractTreeViewAdapter<Long> {

    private Long selected;
    private ArrayList<Object> listItemObject;
    private Elesson elessonBookView;

    private final OnCheckedChangeListener onCheckedChange = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(final CompoundButton buttonView,
                                     final boolean isChecked) {
            final Long id = (Long) buttonView.getTag();
            changeSelected(isChecked, id);
            if (selected == id) {
                buttonView.setChecked(true);
            }
        }
    };

    private void changeSelected(final boolean isChecked, final Long id) {
        if (isChecked) {
            selected = id;
            this.refresh();
        }/* else {
            selected.remove(id);
        }
    	selected = id;*/
    }

    public SimpleStandardAdapter(final Elesson elessonBookView,
                                 final Long selectedId,
                                 final TreeStateManager<Long> treeStateManager,
                                 final int numberOfLevels, ArrayList<Object> listItemObject) {
        super(elessonBookView, treeStateManager, numberOfLevels);
        this.selected = selectedId;
        this.listItemObject = listItemObject;
        this.elessonBookView = elessonBookView;

    }

    private String getDescription(final long id) {
        RAdataobject raobjdata = (RAdataobject)listItemObject.get((int) id);
        return raobjdata.getName();
    }
    private RAdataobject getUrl(final long id){
        RAdataobject rAdataobject=(RAdataobject)listItemObject.get((int) id);
        return rAdataobject;
    }


    @Override
    public View getNewChildView(final TreeNodeInfo<Long> treeNodeInfo) {
        final ViewGroup viewLayout = (LinearLayout) getActivity()
                .getLayoutInflater().inflate(R.layout.demo_list_item, null);
        Typeface font = Typeface.createFromAsset(elessonBookView.getAssets(),"fonts/FiraSans-Regular.otf");
        UserFunctions.changeFont(viewLayout,font);
        return updateView(viewLayout, treeNodeInfo);
    }

    @Override
    public LinearLayout updateView(final View view,
                                   final TreeNodeInfo<Long> treeNodeInfo) {
        final LinearLayout viewLayout = (LinearLayout) view;
        final TextView descriptionView = (TextView) viewLayout
                .findViewById(R.id.demo_list_item_description);
        final TextView levelView = (TextView) viewLayout
                .findViewById(R.id.demo_list_item_level);
        descriptionView.setText(getDescription(treeNodeInfo.getId()));
        levelView.setText(Integer.toString(treeNodeInfo.getLevel()));
        setPaddingForDescView(treeNodeInfo, descriptionView);
        final CheckBox box = (CheckBox) viewLayout
                .findViewById(R.id.demo_list_checkbox);
        box.setTag(treeNodeInfo.getId());
        if (treeNodeInfo.isWithChildren()) {
            box.setVisibility(View.GONE);
        } else {
            //box.setVisibility(View.VISIBLE);
            //System.out.println("treeNode: "+treeNodeInfo.getId());
            if (treeNodeInfo.getId() == selected) {
				box.setChecked(true);
			} else {
				box.setChecked(false);
			}
            //box.setChecked(selected.contains(treeNodeInfo.getId()));
        }
        box.setOnCheckedChangeListener(onCheckedChange);
        return viewLayout;
    }

    public void setPaddingForDescView(TreeNodeInfo<Long> treeNodeInfo, TextView descriptionView){
        int paddingTB = (int) elessonBookView.getResources().getDimension(R.dimen.elesson_subTab_padding_tb);
        switch (treeNodeInfo.getLevel()) {
            case 0:
                int paddingLR = (int) elessonBookView.getResources().getDimension(R.dimen.elesson_subTab1_padding_lr);
                descriptionView.setPadding(paddingLR, 0, paddingLR, 0);
                break;
            case 1:
                int paddingLR1 = (int) elessonBookView.getResources().getDimension(R.dimen.elesson_subTab2_padding_lr);
                descriptionView.setPadding(paddingLR1, 0, paddingLR1, 0);
                break;
            case 2:
                int paddingLR2 = (int) elessonBookView.getResources().getDimension(R.dimen.elesson_subTab3_padding_lr);
                descriptionView.setPadding(paddingLR2, 0, paddingLR2, 0);
                break;
            case 3:
                int paddingLR3 = (int) elessonBookView.getResources().getDimension(R.dimen.elesson_subTab3_padding_lr);
                descriptionView.setPadding(paddingLR3, 0, paddingLR3, 0);
                break;
            default:
                int paddingLR4 = (int) elessonBookView.getResources().getDimension(R.dimen.elesson_subTab1_padding_lr);
                descriptionView.setPadding(paddingLR4, 0, paddingLR4, 0);
        }
    }

    @Override
    public Drawable getBackgroundDrawable(TreeNodeInfo<Long> treeNodeInfo) {
        switch (treeNodeInfo.getLevel()) {
            case 0:
                if (treeNodeInfo.getId() == selected) {
                    return elessonBookView.getResources().getDrawable(R.drawable.elesson_tab_clicked);
                } else {
                    return elessonBookView.getResources().getDrawable(R.drawable.elesson_subtab1);
                }
            case 1:
                if (treeNodeInfo.getId() == selected) {
                    return elessonBookView.getResources().getDrawable(R.drawable.elesson_tab_clicked);
                } else {
                    return elessonBookView.getResources().getDrawable(R.drawable.elesson_subtab2);
                }
            case 2:
                if (treeNodeInfo.getId() == selected) {
                    return elessonBookView.getResources().getDrawable(R.drawable.elesson_tab_clicked);
                } else {
                    return elessonBookView.getResources().getDrawable(R.drawable.elesson_subtab3);
                }
            case 3:
                if (treeNodeInfo.getId() == selected) {
                    return elessonBookView.getResources().getDrawable(R.drawable.elesson_tab_clicked);
                } else {
                    return elessonBookView.getResources().getDrawable(R.drawable.elesson_subtab4);
                }
            default:
                return null;
        }
    }

    @Override
    public void handleItemClick(final View view, final Object id) {
        final Long longId = (Long) id;
        final TreeNodeInfo<Long> info = getManager().getNodeInfo(longId);
        if (info.isWithChildren()) {
            super.handleItemClick(view, id);
        } else {
            final ViewGroup vg = (ViewGroup) view;
            final CheckBox cb = (CheckBox) vg
                    .findViewById(R.id.demo_list_checkbox);
            cb.performClick();

            elessonBookView.stopMeasuringAndUpdateXml();
            RAdataobject dataObj = getUrl((Long) id);
            elessonBookView.loadWebView(dataObj);
            //elessonBookView.new loadingWebview(dataObj).execute();
        }
    }

    @Override
    public long getItemId(final int position) {
        return getTreeId(position);
    }
}