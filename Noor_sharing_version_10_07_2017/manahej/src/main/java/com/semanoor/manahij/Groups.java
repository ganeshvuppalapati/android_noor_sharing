package com.semanoor.manahij;

/**
 * Created by karthik on 08-07-2016.
 */
public class Groups {

    private static Groups instance;
    //Banner
    private boolean isBannerAdsVisible;
    private boolean isInterstitialAdsVisible;
    //Elesson
    private boolean elessonSearchEnable;
    private boolean elessonStatisticsEnable;
    private boolean elessonfavouriteEnable;
    private boolean elessonPrintEnable;
    private boolean elessonNoteEnable;
    private boolean elessonFlashcardEnable;
    private boolean elessonDrawingEnable;
    private boolean elessonSearchInWebEnable;
    private boolean elessonFeedbackEnable;
    private boolean elessonPowerPointEnable;
    //Bookreader
    private boolean bookReaderBookmarkEnable;
    private boolean bookReaderFlashCardEnable;
    private boolean bookReaderDrawingEnable;
    private boolean bookReaderMindmapEnable;
    private boolean bookReaderMalzamahEnable;
    private boolean bookReaderAdvanceSearchEnable;
    private boolean bookReaderBooksearchEnable;
    private boolean bookReaderNoteEnable;
    private boolean bookReaderHighlightEnable;
    private boolean bookReaderEnrichment;
    //Shelf
    private boolean shelfCloudSharingEnable;
    private boolean shelfCreateNewBookEnable;
    private boolean shelfMindmapEnable;
    private boolean shelfAlbumEnable;
    private boolean shelfSearchForBookEnable;
    private boolean shelfSearchInsideBookEnable;
    private boolean shelfCreateMalzamahEnable;
    private boolean shelfinboxEnable;
    //editBook
    private boolean textController;
    private boolean imageController;
    private boolean videoController;
    private boolean drawingEditor;
    private boolean soundRecorder;
    private boolean mindmapEditor;
    private boolean quiz;
    private boolean embedCode;
    private boolean iframe;
    private boolean tables;
    private boolean searchInWeb;



    public static synchronized Groups getInstance(){
        if(instance == null){
            instance = new Groups();
        }
        return instance;
    }

    public Boolean getBannerAdsVisible() {
        return isBannerAdsVisible;
    }

    public void setBannerAdsVisible(Boolean bannerAdsVisible) {
        isBannerAdsVisible = bannerAdsVisible;
    }

    public Boolean getInterstitialAdsVisible() {
        return isInterstitialAdsVisible;
    }

    public void setInterstitialAdsVisible(Boolean interstitialAdsVisible) {
        isInterstitialAdsVisible = interstitialAdsVisible;
    }

    public Boolean getElessonSearchEnable() {
        return elessonSearchEnable;
    }

    public void setElessonSearchEnable(Boolean elessonSearchEnable) {
        this.elessonSearchEnable = elessonSearchEnable;
    }

    public Boolean getElessonStatisticsEnable() {
        return elessonStatisticsEnable;
    }

    public void setElessonStatisticsEnable(Boolean elessonStatisticsEnable) {
        this.elessonStatisticsEnable = elessonStatisticsEnable;
    }

    public Boolean getElessonfavouriteEnable() {
        return elessonfavouriteEnable;
    }

    public void setElessonfavouriteEnable(Boolean elessonfavouriteEnable) {
        this.elessonfavouriteEnable = elessonfavouriteEnable;
    }

    public Boolean getElessonPrintEnable() {
        return elessonPrintEnable;
    }

    public void setElessonPrintEnable(Boolean elessonPrintEnable) {
        this.elessonPrintEnable = elessonPrintEnable;
    }

    public Boolean getElessonNoteEnable() {
        return elessonNoteEnable;
    }

    public void setElessonNoteEnable(Boolean elessonNoteEnable) {
        this.elessonNoteEnable = elessonNoteEnable;
    }

    public Boolean getElessonFlashcardEnable() {
        return elessonFlashcardEnable;
    }

    public void setElessonFlashcardEnable(Boolean elessonFlashcardEnable) {
        this.elessonFlashcardEnable = elessonFlashcardEnable;
    }

    public Boolean getElessonDrawingEnable() {
        return elessonDrawingEnable;
    }

    public void setElessonDrawingEnable(Boolean elessonDrawingEnable) {
        this.elessonDrawingEnable = elessonDrawingEnable;
    }

    public Boolean getElessonSearchInWebEnable() {
        return elessonSearchInWebEnable;
    }

    public void setElessonSearchInWebEnable(Boolean elessonSearchInWebEnable) {
        this.elessonSearchInWebEnable = elessonSearchInWebEnable;
    }

    public Boolean getElessonFeedbackEnable() {
        return elessonFeedbackEnable;
    }

    public void setElessonFeedbackEnable(Boolean elessonFeedbackEnable) {
        this.elessonFeedbackEnable = elessonFeedbackEnable;
    }

    public Boolean getElessonPowerPointEnable() {
        return elessonPowerPointEnable;
    }

    public void setElessonPowerPointEnable(Boolean elessonPowerPointEnable) {
        this.elessonPowerPointEnable = elessonPowerPointEnable;
    }

    public Boolean getBookReaderBookmarkEnable() {
        return bookReaderBookmarkEnable;
    }

    public void setBookReaderBookmarkEnable(Boolean bookReaderBookmarkEnable) {
        this.bookReaderBookmarkEnable = bookReaderBookmarkEnable;
    }

    public Boolean getBookReaderFlashCardEnable() {
        return bookReaderFlashCardEnable;
    }

    public void setBookReaderFlashCardEnable(Boolean bookReaderFlashCardEnable) {
        this.bookReaderFlashCardEnable = bookReaderFlashCardEnable;
    }

    public Boolean getBookReaderDrawingEnable() {
        return bookReaderDrawingEnable;
    }

    public void setBookReaderDrawingEnable(Boolean bookReaderDrawingEnable) {
        this.bookReaderDrawingEnable = bookReaderDrawingEnable;
    }

    public Boolean getBookReaderMindmapEnable() {
        return bookReaderMindmapEnable;
    }

    public void setBookReaderMindmapEnable(Boolean bookReaderMindmapEnable) {
        this.bookReaderMindmapEnable = bookReaderMindmapEnable;
    }

    public Boolean getBookReaderMalzamahEnable() {
        return bookReaderMalzamahEnable;
    }

    public void setBookReaderMalzamahEnable(Boolean bookReaderMalzamahEnable) {
        this.bookReaderMalzamahEnable = bookReaderMalzamahEnable;
    }

    public Boolean getBookReaderAdvanceSearchEnable() {
        return bookReaderAdvanceSearchEnable;
    }

    public void setBookReaderAdvanceSearchEnable(Boolean bookReaderAdvanceSearchEnable) {
        this.bookReaderAdvanceSearchEnable = bookReaderAdvanceSearchEnable;
    }

    public Boolean getBookReaderBooksearchEnable() {
        return bookReaderBooksearchEnable;
    }

    public void setBookReaderBooksearchEnable(Boolean bookReaderBooksearchEnable) {
        this.bookReaderBooksearchEnable = bookReaderBooksearchEnable;
    }

    public Boolean getBookReaderNoteEnable() {
        return bookReaderNoteEnable;
    }

    public void setBookReaderNoteEnable(Boolean bookReaderNoteEnable) {
        this.bookReaderNoteEnable = bookReaderNoteEnable;
    }

    public Boolean getBookReaderHighlightEnable() {
        return bookReaderHighlightEnable;
    }

    public void setBookReaderHighlightEnable(Boolean bookReaderHighlightEnable) {
        this.bookReaderHighlightEnable = bookReaderHighlightEnable;
    }

    public Boolean getShelfCloudSharingEnable() {
        return shelfCloudSharingEnable;
    }

    public void setShelfCloudSharingEnable(Boolean shelfCloudSharingEnable) {
        this.shelfCloudSharingEnable = shelfCloudSharingEnable;
    }

    public Boolean getShelfCreateNewBookEnable() {
        return shelfCreateNewBookEnable;
    }

    public void setShelfCreateNewBookEnable(Boolean shelfCreateNewBookEnable) {
        this.shelfCreateNewBookEnable = shelfCreateNewBookEnable;
    }

    public Boolean getShelfMindmapEnable() {
        return shelfMindmapEnable;
    }

    public void setShelfMindmapEnable(Boolean shelfMindmapEnable) {
        this.shelfMindmapEnable = shelfMindmapEnable;
    }

    public Boolean getShelfAlbumEnable() {
        return shelfAlbumEnable;
    }

    public void setShelfAlbumEnable(Boolean shelfAlbumEnable) {
        this.shelfAlbumEnable = shelfAlbumEnable;
    }

    public Boolean getShelfSearchForBookEnable() {
        return shelfSearchForBookEnable;
    }

    public void setShelfSearchForBookEnable(Boolean shelfSearchForBookEnable) {
        this.shelfSearchForBookEnable = shelfSearchForBookEnable;
    }

    public Boolean getShelfSearchInsideBookEnable() {
        return shelfSearchInsideBookEnable;
    }

    public void setShelfSearchInsideBookEnable(Boolean shelfSearchInsideBookEnable) {
        this.shelfSearchInsideBookEnable = shelfSearchInsideBookEnable;
    }

    public Boolean getShelfCreateMalzamahEnable() {
        return shelfCreateMalzamahEnable;
    }

    public void setShelfCreateMalzamahEnable(Boolean shelfCreateMalzamahEnable) {
        this.shelfCreateMalzamahEnable = shelfCreateMalzamahEnable;
    }

    public void setAdsTypeList(String adsTypeName){
        if (adsTypeName.equals("Banner")) {
            setBannerAdsVisible(true);
        } else if (adsTypeName.equals("FullPage")) {
            setInterstitialAdsVisible(true);
        }
    }

    public void setShelveFeatureList(String shelveFeatureName) {

        if (shelveFeatureName.equals("cloud sharing")) {
            setShelfCloudSharingEnable(true);
        } else if (shelveFeatureName.equals("Create Book")) {
            setShelfCreateNewBookEnable(true);
        } else if (shelveFeatureName.equals("Create Mindmap")) {
           setShelfMindmapEnable(true);
        } else if (shelveFeatureName.equals("Create Album")) {
            setShelfAlbumEnable(true);
        } else if (shelveFeatureName.equals("Search For Book")) {
            setShelfSearchForBookEnable(true);
        } else if (shelveFeatureName.equals("Search Inside Book")) {
            setShelfSearchInsideBookEnable(true);
        } else if (shelveFeatureName.equals("Create Malzamah")) {
            setShelfCreateMalzamahEnable(true);
        }else if (shelveFeatureName.equals("Inbox")){
            setShelfinboxEnable(true);
        }
    }

    public void setBookReaderFeatureList(String bookReaderFeatureName) {

        if (bookReaderFeatureName.equals("Bookmark")) {
            setBookReaderBookmarkEnable(true);
        } else if (bookReaderFeatureName.equals("Flash Card")) {
            setBookReaderFlashCardEnable(true);
        } else if (bookReaderFeatureName.equals("Drawing")) {
            setBookReaderDrawingEnable(true);
        } else if (bookReaderFeatureName.equals("Mindmap")) {
            setBookReaderMindmapEnable(true);
        } else if (bookReaderFeatureName.equals("Malzamah")) {
            setBookReaderMalzamahEnable(true);
        } else if (bookReaderFeatureName.equals("Advanced Search")) {
            setBookReaderAdvanceSearchEnable(true);
        } else if (bookReaderFeatureName.equals("Book Search")) {
            setBookReaderBooksearchEnable(true);
        } else if (bookReaderFeatureName.equals("Note")) {
            setBookReaderNoteEnable(true);
        } else if (bookReaderFeatureName.equals("Highlight")) {
            setBookReaderHighlightEnable(true);
        }else if (bookReaderFeatureName.equals("Enrichment")){
            setBookReaderEnrichment(true);
        }
    }

    public void setElessonFeatureList(String elessonfeatureName) {

        if (elessonfeatureName.equals("Search In Elesson")) {
            setElessonSearchEnable(true);
        } else if (elessonfeatureName.equals("Statistics")) {
            setElessonStatisticsEnable(true);
        } else if (elessonfeatureName.equals("Favourite")) {
            setElessonfavouriteEnable(true);
        } else if (elessonfeatureName.equals("Print")) {
            setElessonPrintEnable(true);
        } else if (elessonfeatureName.equals("Note")) {
            setElessonNoteEnable(true);
        } else if (elessonfeatureName.equals("Flash Card")) {
            setElessonFlashcardEnable(true);
        } else if (elessonfeatureName.equals("Drawing")) {
            setElessonDrawingEnable(true);
        } else if (elessonfeatureName.equals("Search Internet")) {
            setElessonSearchInWebEnable(true);
        } else if (elessonfeatureName.equals("Feedback")) {
            setElessonFeedbackEnable(true);
        } else if (elessonfeatureName.equals("Presentation Mode")) {
            setElessonPowerPointEnable(true);
        }
    }

    public void setBookEditorFeature(String editorFeatureName){
        if (editorFeatureName.equals("Text_Controller")){
            setTextController(true);
        } else if (editorFeatureName.equals("Image_Controller")) {
            setImageController(true);
        } else if (editorFeatureName.equals("Video_Controller")) {
            setVideoController(true);
        } else if (editorFeatureName.equals("Drawing_Editor")) {
            setDrawingEditor(true);
        } else if (editorFeatureName.equals("Sound_Recorder")) {
            setSoundRecorder(true);
        } else if (editorFeatureName.equals("Mindmap_editor")) {
            setMindmapEditor(true);
        } else if (editorFeatureName.equals("Quiz")) {
            setQuiz(true);
        } else if (editorFeatureName.equals("Embed")) {
           setEmbedCode(true);
        } else if (editorFeatureName.equals("Iframe")) {
            setIframe(true);
        } else if (editorFeatureName.equals("Tables")) {
            setTables(true);
        } else if (editorFeatureName.equals("Search_In_Web")) {
            setSearchInWeb(true);
        }
    }

    public void setAllToFalse(){
        setBannerAdsVisible(false);
        setInterstitialAdsVisible(false);
        setShelfCloudSharingEnable(false);
        setShelfCreateNewBookEnable(false);
        setShelfMindmapEnable(false);
        setShelfAlbumEnable(false);
        setShelfSearchForBookEnable(false);
        setShelfSearchInsideBookEnable(false);
        setShelfCreateMalzamahEnable(false);
        setBookReaderBookmarkEnable(false);
        setBookReaderFlashCardEnable(false);
        setBookReaderDrawingEnable(false);
        setBookReaderMindmapEnable(false);
        setBookReaderMalzamahEnable(false);
        setBookReaderAdvanceSearchEnable(false);
        setBookReaderBooksearchEnable(false);
        setBookReaderNoteEnable(false);
        setBookReaderHighlightEnable(false);
        setBookReaderEnrichment(false);
        setElessonSearchEnable(false);
        setElessonStatisticsEnable(false);
        setElessonfavouriteEnable(false);
        setElessonPrintEnable(false);
        setElessonNoteEnable(false);
        setElessonFlashcardEnable(false);
        setElessonDrawingEnable(false);
        setElessonSearchInWebEnable(false);
        setElessonFeedbackEnable(false);
        setElessonPowerPointEnable(false);
        setTextController(false);
        setImageController(false);
        setVideoController(false);
        setDrawingEditor(false);
        setSoundRecorder(false);
        setMindmapEditor(false);
        setQuiz(false);
        setEmbedCode(false);
        setIframe(false);
        setTables(false);
        setSearchInWeb(false);
    }

    public boolean isBookReaderEnrichment() {
        return bookReaderEnrichment;
    }

    public void setBookReaderEnrichment(boolean bookReaderEnrichment) {
        this.bookReaderEnrichment = bookReaderEnrichment;
    }

    public boolean isTextController() {
        return textController;
    }

    public void setTextController(boolean textController) {
        this.textController = textController;
    }

    public boolean isImageController() {
        return imageController;
    }

    public void setImageController(boolean imageController) {
        this.imageController = imageController;
    }

    public boolean isVideoController() {
        return videoController;
    }

    public void setVideoController(boolean videoController) {
        this.videoController = videoController;
    }

    public boolean isDrawingEditor() {
        return drawingEditor;
    }

    public void setDrawingEditor(boolean drawingEditor) {
        this.drawingEditor = drawingEditor;
    }

    public boolean isSoundRecorder() {
        return soundRecorder;
    }

    public void setSoundRecorder(boolean soundRecorder) {
        this.soundRecorder = soundRecorder;
    }

    public boolean isMindmapEditor() {
        return mindmapEditor;
    }

    public void setMindmapEditor(boolean mindmapEditor) {
        this.mindmapEditor = mindmapEditor;
    }

    public boolean isQuiz() {
        return quiz;
    }

    public void setQuiz(boolean quiz) {
        this.quiz = quiz;
    }

    public boolean isEmbedCode() {
        return embedCode;
    }

    public void setEmbedCode(boolean embedCode) {
        this.embedCode = embedCode;
    }

    public boolean isIframe() {
        return iframe;
    }

    public void setIframe(boolean iframe) {
        this.iframe = iframe;
    }

    public boolean isTables() {
        return tables;
    }

    public void setTables(boolean tables) {
        this.tables = tables;
    }

    public boolean isSearchInWeb() {
        return searchInWeb;
    }

    public void setSearchInWeb(boolean searchInWeb) {
        this.searchInWeb = searchInWeb;
    }

    public boolean isShelfinboxEnable() {
        return shelfinboxEnable;
    }

    public void setShelfinboxEnable(boolean shelfinboxEnable) {
        this.shelfinboxEnable = shelfinboxEnable;
    }
}
