package com.semanoor.manahij;

import android.view.View;

/**
 * Created by karthik on 10-04-2017.
 */

public interface OnclickCallBackInterface<T> {
    public void onClick(T object);
}
