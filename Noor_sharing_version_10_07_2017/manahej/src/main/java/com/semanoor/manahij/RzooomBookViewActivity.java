package com.semanoor.manahij;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.ptg.views.CircleButton;
import com.semanoor.source_manahij.RzooomPage;
import com.semanoor.source_manahij.RzooonScene;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Locale;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class RzooomBookViewActivity extends Activity{
	private Book currentBook;
	private String currentBookPath;
	public static String bookName; 
	private Button btn_arrow_previous,btn_arrow_next;
	private CircleButton libraryButton;
	private TextView rzooom_title,rzooom_pagecount;
	static String filePath = new String();
	public VideoView videoView;
	RzooomPage rzooomPage;
	int sceneCount = 0;
	String rScenePath;
	MediaController mc;
	int totalCount;
	String currentPageNo;
	public RelativeLayout rzoom_layout;
	ArrayList<RzooonScene> rzooomSceneList =new ArrayList<RzooonScene>();
	
	private boolean adsPurchased;
	private InterstitialAd interstitial;
	RelativeLayout rzooom_layout_back;
	ImageView img_bar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		loadLocale();
		setContentView(R.layout.activity_rzooom_view);

		Typeface font = Typeface.createFromAsset(getAssets(),"fonts/FiraSans-Regular.otf");
		ViewGroup rootView = (ViewGroup) findViewById(android.R.id.content).getRootView();

		UserFunctions.changeFont(rootView,font);
		videoView = (VideoView) findViewById(R.id.videoView1);

		rzoom_layout = (RelativeLayout) findViewById(R.id.rzooom_toolbar);
		rzooom_layout_back = (RelativeLayout) findViewById(R.id.rzooom_top_layout);
		img_bar = (ImageView) findViewById(R.id.img_bar);
		//   rzoom_layout.setVisibility(View.INVISIBLE);

		libraryButton = (CircleButton) findViewById(R.id.btnLibrary);
		libraryButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		rzooom_title = (TextView) findViewById(R.id.rzooom_title);
		rzooom_pagecount = (TextView) findViewById(R.id.rzooom_pagecount);
		rzooom_pagecount.setVisibility(View.GONE);
		currentBook = (Book) getIntent().getSerializableExtra("Book");
		bookName = currentBook.get_bStoreID() + "Book";
		currentBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookName + "/";
		mc = new MediaController(this);
		/*mc.setAnchorView(videoView);
		
		mc.setMediaPlayer(videoView);
		videoView.setMediaController(mc);
		videoView.requestFocus();
		videoView.start();*/

		videoView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				final int action = event.getAction();
				switch (action & MotionEvent.ACTION_MASK) {
					case MotionEvent.ACTION_DOWN: {
						if (rzoom_layout.getVisibility() == View.VISIBLE) {
							rzoom_layout.setVisibility(View.INVISIBLE);
							rzooom_layout_back.setVisibility(View.INVISIBLE);
							img_bar.setVisibility(View.INVISIBLE);
						} else {
							rzoom_layout.setVisibility(View.VISIBLE);
							rzooom_layout_back.setVisibility(View.VISIBLE);
							img_bar.setVisibility(View.VISIBLE);
						}
						break;
					}
					default:
						break;
				}

				return false;
			}
		});

		//mc.setAnchorView(videoView);
		parseRzooomBookXml();
		if (rzooomPage.getrwidth() != null && rzooomPage.getrheight() != null){
			int width = Integer.parseInt(rzooomPage.getrwidth());
		int height = Integer.parseInt(rzooomPage.getrheight());

		if (width > height) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
	 }else{
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		}
		RzooonScene rscene = rzooomSceneList.get(sceneCount);
		rScenePath = rscene.getrScenePath();

		String rSceneTitle = rscene.getrFolderName();
		rzooom_title.setText(rSceneTitle);
		totalCount=rzooomSceneList.size();
		VideoView(rscene);

		btn_arrow_next = (Button)findViewById(R.id.btn_arrow_next);
		btn_arrow_next.setOnClickListener(new OnClickListener() {


			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(rzooomSceneList.size()>Integer.parseInt(currentPageNo)){
				    int	nextSceneCount = Integer.parseInt(currentPageNo);
					//videoView.pause();
					videoView.stopPlayback();
					RzooonScene rscene = rzooomSceneList.get(nextSceneCount);
					VideoView(rscene);
					btn_arrow_previous.setEnabled(true);
				}
			}
		});


		btn_arrow_previous = (Button)findViewById(R.id.btn_arrow_previous);
		btn_arrow_previous.setOnClickListener(new OnClickListener() {


			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//System.out.println(rzooomSceneList.size());
				//if(rzooomSceneList.size()>Integer.parseInt(currentPageNo) ||rzooomSceneList.size()==Integer.parseInt(currentPageNo)){
				if(rzooomSceneList.size()>1){
					int	previousSceneCount = Integer.parseInt(currentPageNo)-2;
					//videoView.pause();
					videoView.stopPlayback();
					RzooonScene rscene = rzooomSceneList.get(previousSceneCount);
					VideoView(rscene);
					if(previousSceneCount==0){
						btn_arrow_previous.setEnabled(false);
					}
				}
			}
		});
		if(rzooomSceneList.size()==1){
			btn_arrow_previous.setVisibility(View.GONE);
			btn_arrow_next.setVisibility(View.GONE);
		}
		/*if(rzooomSceneList.size()==1){
			btn_arrow_next.setEnabled(false);
			btn_arrow_previous.setEnabled(false);
		}*/
		
		SharedPreferences preference = getSharedPreferences(Globals.PREF_AD_PURCHASED, MODE_PRIVATE);
		adsPurchased = preference.getBoolean(Globals.ADS_DISPLAY_KEY, false);

		if (!adsPurchased) {
			interstitial = new InterstitialAd(RzooomBookViewActivity.this);
			interstitial.setAdUnitId(Globals.INTERSTITIAL_AD_UNIT_ID);
			AdRequest adRequest = new AdRequest.Builder()
			//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
			//.addTestDevice("505F22CBB28695386696655EED14B98B") 
			.build();
			interstitial.loadAd(adRequest);
		}
	}
	
	/**
	 * Load Locale language from shared preference and change language in the application
	 */
	private void loadLocale() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String language = prefs.getString(Globals.languagePrefsKey, "en");
		changeLang(language);
	}
	
	/**
	 * change language in the application
	 * @param language
	 */
	private void changeLang(String language) {
		Locale myLocale = new Locale(language);
		Locale.setDefault(myLocale);
		android.content.res.Configuration config = new android.content.res.Configuration();
		config.locale = myLocale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
	}
	
	public void VideoView(RzooonScene rscene){
		videoView.setVideoPath(rscene.getrScenePath());
		videoView.setMediaController(mc);
		videoView.start();
		currentPageNo=rscene.getrPageNo();
		if(!(rzooomSceneList.size()==1)){
			rzooom_pagecount.setVisibility(View.VISIBLE);
			rzooom_pagecount.setText(""+currentPageNo+"/"+totalCount+"");
		}
		
		rzooom_title.setText(rscene.getrFolderName());
	}

	public void parseRzooomBookXml() {

		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			DefaultHandler handler = new DefaultHandler(){
				String currentNode = "";


				public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException 
				{

					if(qName.contains("Scenes"))
					{
						String RName = attributes.getValue("NAME");
						String RAuthorName = attributes.getValue("AUTHORNAME");
						String RType = attributes.getValue("TYPE");
						String RWidth = attributes.getValue("WIDTH");
						String RHeight = attributes.getValue("HEIGHT");
						String BgColor = attributes.getValue("bgColor");

						rzooomPage  = new RzooomPage();
						rzooomPage.setRzooomName(RName);
						rzooomPage.setRzooomAuthorName(RAuthorName);
						rzooomPage.setrsooomType(RType);
						rzooomPage.setrheight(RHeight);
						rzooomPage.setrwidth(RWidth);
						rzooomPage.setrzooombgColor(BgColor);

						//rzooomSceneList = new ArrayList<RzooonScene>();
					}else if(qName.contains("Scene")){
						String RUrl = attributes.getValue("URL");
						String pno = attributes.getValue("PgNo");
						String folderName = attributes.getValue("FOLDERNAME");
						String bgAudio = attributes.getValue("BGAUDIO");
						String fileName = attributes.getValue("FILENAME");
						String ScenePath = currentBookPath+"Data/"+folderName+"/"+"Resource/"+fileName;

						RzooonScene rscene = new RzooonScene();

						rscene.setrUrl(RUrl);
						rscene.setrPageNo(pno);
						rscene.setrFolderName(folderName);
						rscene.setrBgAudio(bgAudio);
						rscene.setrFilename(fileName);
						rscene.setrScenePath(ScenePath);

						rzooomSceneList.add(rscene);
					}

				}

				@Override
				public void endElement(String uri, String localName, String qName) throws SAXException {

					if(qName.contains("Scenes")){
						rzooomPage.setRzoomSceneList(rzooomSceneList);
					}
				}
			};

			File sdPath = getFilesDir();
			//revert_book_hide :
			filePath = currentBookPath+"Data/"+"Book.xml";
			InputStream inputStream = new FileInputStream(filePath);
			Reader reader = new InputStreamReader(inputStream, "UTF-8");

			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");

			saxParser.parse(is, handler);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (Globals.isLimitedVersion() && !adsPurchased) {
			if (interstitial.isLoaded()) {
				interstitial.show();
			}
		}
		finish();
	}
}
