package com.semanoor.manahij;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.semanoor.manahij.mqtt.CallbackInterface;
import com.semanoor.manahij.mqtt.MQTTSubscriptionService;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.source_sboookauthor.MyContacts;

import org.json.JSONObject;

import java.util.ArrayList;

public final class SimpleCardStackAdapter extends CardStackAdapter  {

	public static boolean animation=false;
	Button btn_turn;
	static int studycard_y;
	public RelativeLayout layout;
	public BookViewReadActivity bookview;
	public Elesson elesson;
	public FlashCardActivity flashCardActivity;
	private ArrayList<NoteData> noteAdapterObject;
	public OnClickListener clickListener;
	Context Context;
    int position;
	public SimpleCardStackAdapter(Context mContext,BookViewReadActivity bookView, ArrayList<NoteData> noteAdapter) {
		super(mContext);
		bookview = bookView;
		Context=mContext;
		noteAdapterObject = noteAdapter;
	}

	public SimpleCardStackAdapter(Context mContext,Elesson elessonActivity, ArrayList<NoteData> noteAdapter) {
		super(mContext);
		Context=mContext;
		elesson = elessonActivity;
		noteAdapterObject = noteAdapter;
	}
	public SimpleCardStackAdapter(Context mContext,FlashCardActivity cardActivity, ArrayList<NoteData> noteAdapter) {
		super(mContext);
		Context=mContext;
		flashCardActivity = cardActivity;
		noteAdapterObject = noteAdapter;
	}

	@Override
	public View getCardView(final int position, CardModel model, View convertView, ViewGroup parent) {

		if(convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.std_card_inner, parent, false);


			assert convertView != null;
		}

		layout=(RelativeLayout)convertView.findViewById(R.id.global_container);

		final NoteData noteData2 = noteAdapterObject.get(position);
		if (bookview!=null) {
			layout.setBackgroundResource(bookview.cardImgId[noteData2.getNoteColor() - 1]);
		}else if (elesson!=null){
			layout.setBackgroundResource(elesson.cardImgId[noteData2.getNoteColor() - 1]);
		}else if (flashCardActivity!=null){
			layout.setBackgroundResource(flashCardActivity.cardImgId[noteData2.getNoteColor() - 1]);
		}
		final NoteData noteData=noteData2;
	   TextView	tv_selected2 = (TextView)convertView.findViewById(R.id.image);

		tv_selected2.setText(noteData.getNoteTitle());
		Button btn_pageno=(Button)convertView.findViewById(R.id.button1);
		btn_pageno.setText(model.getTitle());
		btn_pageno.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				int currentPageNo = noteData.getNotePageNo();
				int currentTabNo = noteData.getNoteTabNo();

			}

		});

		final TextView	tv_selected=tv_selected2;
		btn_turn=(Button)convertView.findViewById(R.id.btn_turn);

		//	btn_turn.setBackground(model.getCardImageDrawable());
            clickListener=	new OnClickListener() {
			@Override
			public void onClick(View v) {
				tv_selected .setText(noteData.getNoteDesc());
				if(animation==false){
					if(CardContainer.mTopCard!=null){
						studycard_y= (int) CardContainer.mTopCard.getY();
						Flip3dAnimation rotation=new Flip3dAnimation(180,90,CardContainer.mTopCard.getWidth()/2.0f,CardContainer.mTopCard.getHeight()/2.0f);
						rotation.setDuration(150);
						rotation.setFillAfter(true);
						rotation.setInterpolator(new AccelerateInterpolator());
						rotation.setAnimationListener(new DisplayNextView());
						CardContainer.mTopCard.startAnimation(rotation);
						animation=true;
						publishTurn(animation);
					}
				}else{
					//holder.helloTxt.setText(noteData.getNoteTitle());
					tv_selected .setText(noteData.getNoteTitle());
					//System.out.println("entered");
					if(CardContainer.mTopCard!=null){
						Flip3dAnimation rotation=new Flip3dAnimation(180,90,CardContainer.mTopCard.getWidth()/2.0f,CardContainer.mTopCard.getHeight()/2.0f);
						rotation.setDuration(150);
						rotation.setFillAfter(true);
						rotation.setInterpolator(new AccelerateInterpolator());
						rotation.setAnimationListener(new DisplayNextView());
						CardContainer.mTopCard.startAnimation(rotation);
						animation=false;
						publishTurn(animation);
					}
				}
			}
		};
		btn_turn.setOnClickListener(clickListener);
		noteAdapterObject.get(position).setNoTitle(convertView);

		convertView.setTag(R.id._linearLayout,noteData.getNoteTitle());
		return convertView;
	}

	private void publishTurn(boolean b) {
try{
	if(ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
		MQTTSubscriptionService service = new MQTTSubscriptionService();
		JSONObject object = new JSONObject();
		object.put("type", Constants.FLASHCARD);
		object.put("action", Constants.KEYFLASHTURN);
		object.put("flip", b);
		object.put("device_id",ManahijApp.getInstance().getPrefManager().getDeviceID());
		service.publishMsg(ManahijApp.getInstance().getPrefManager().getMainChannel(), object.toString(), Context);
	}

}catch (Exception e){}


	}

	private int Color(Integer integer) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static  void changeanimation() {
		Flip3dAnimation rotation=new Flip3dAnimation(90,0,CardContainer.mTopCard.getWidth()/2.0f,CardContainer.mTopCard.getHeight()/2.0f);
		rotation.setDuration(500);
		rotation.setFillAfter(true);
		rotation.setInterpolator(new DecelerateInterpolator());
		CardContainer.mTopCard.startAnimation(rotation);

	}

	public void onUpturn(boolean flip) {
try {

	NoteData noteData = getItemNoteData(getStudycard_y());
	View convertView = noteData.getNoTitle();
	String tt = (String) convertView.getTag(R.id._linearLayout);
	TextView tv_selected = (TextView) convertView.findViewById(R.id.image);
	if (tv_selected != null) {
		((TextView) CardContainer.mTopCard.findViewById(R.id.image)).setText(noteData.getNoteDesc());
		if (animation == false) {
				if(CardContainer.mTopCard!=null){
					studycard_y= (int) CardContainer.mTopCard.getY();
					Flip3dAnimation rotation=new Flip3dAnimation(180,90,CardContainer.mTopCard.getWidth()/2.0f,CardContainer.mTopCard.getHeight()/2.0f);
					rotation.setDuration(150);
					rotation.setFillAfter(true);
					rotation.setInterpolator(new AccelerateInterpolator());
					rotation.setAnimationListener(new DisplayNextView());
					CardContainer.mTopCard.startAnimation(rotation);
					animation=true;

				}
			animation = true;
		} else {
			tv_selected.setText(noteData.getNoteTitle());
			convertView.invalidate();
			((TextView) CardContainer.mTopCard.findViewById(R.id.image)).setText(noteData.getNoteTitle());
				if(CardContainer.mTopCard!=null){
					Flip3dAnimation rotation=new Flip3dAnimation(180,90,CardContainer.mTopCard.getWidth()/2.0f,CardContainer.mTopCard.getHeight()/2.0f);
					rotation.setDuration(150);
					rotation.setFillAfter(true);
					rotation.setInterpolator(new AccelerateInterpolator());
					rotation.setAnimationListener(new DisplayNextView());
					CardContainer.mTopCard.startAnimation(rotation);
					animation=false;
				}
			animation = false;
		}
		convertView.invalidate();
		tv_selected.invalidate();
		CardContainer.mTopCard.invalidate();
		notifyDataSetChanged();
	}

}catch (Exception e){
	Log.e("flip",e.toString());
}
	}


	public NoteData getItemNoteData(int position) {
		return noteAdapterObject.get(position);
	}
	public int getStudycard_y() {
		if(CardContainer.mTopCard!=null)
			position = (int) CardContainer.cardPosition;

		return position;
	}


}
