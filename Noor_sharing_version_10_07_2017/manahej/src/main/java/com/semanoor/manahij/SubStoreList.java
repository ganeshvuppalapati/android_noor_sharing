package com.semanoor.manahij;

import java.io.Serializable;

/**
 * Created by Krishna on 12-03-2016.
 */
public class SubStoreList implements Serializable {
    private String subStoreEn;
    private String subStoreAr;
    private int subID;
    private String Url;
    private String shelfPosition;
    private String countryEn;
    private String countryAr;
    private String  universityEn;
    private String  universityAr;
    private String categoryEn;
    private String categoryAr;
    private int storeID;
    private int ApplicationID;
    private String country;
    private String university;
    private String category;
    private String prefixName;


    public String getSubStoreEn() {
        return subStoreEn;
    }

    public void setSubStoreEn(String subStoreEn) {
        this.subStoreEn = subStoreEn;
    }

    public String getSubStoreAr() {
        return subStoreAr;
    }

    public void setSubStoreAr(String subStoreAr) {
        this.subStoreAr = subStoreAr;
    }

    public int getSubID() {
        return subID;
    }

    public void setSubID(int subID) {
        this.subID = subID;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getShelfPosition() {
        return shelfPosition;
    }

    public void setShelfPosition(String shelfPosition) {
        this.shelfPosition = shelfPosition;
    }

    public String getCountryEn() {
        return countryEn;
    }

    public void setCountryEn(String countryEn) {
        this.countryEn = countryEn;
    }

    public String getCountryAr() {
        return countryAr;
    }

    public void setCountryAr(String countryAr) {
        this.countryAr = countryAr;
    }

    public String getUniversityEn() {
        return universityEn;
    }

    public void setUniversityEn(String universityEn) {
        this.universityEn = universityEn;
    }

    public String getUniversityAr() {
        return universityAr;
    }

    public void setUniversityAr(String universityAr) {
        this.universityAr = universityAr;
    }

    public String getCategoryEn() {
        return categoryEn;
    }

    public void setCategoryEn(String categoryEn) {
        this.categoryEn = categoryEn;
    }

    public String getCategoryAr() {
        return categoryAr;
    }

    public void setCategoryAr(String categoryAr) {
        this.categoryAr = categoryAr;
    }

    public int getStoreID() {
        return storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public int getApplicationID() {
        return ApplicationID;
    }

    public void setApplicationID(int applicationID) {
        ApplicationID = applicationID;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrefixName() {
        return prefixName;
    }

    public void setPrefixName(String prefixName) {
        this.prefixName = prefixName;
    }
}
