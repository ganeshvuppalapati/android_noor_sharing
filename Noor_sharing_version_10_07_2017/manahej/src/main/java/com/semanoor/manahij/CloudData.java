package com.semanoor.manahij;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Krishna on 11-08-2016.
 */
public class CloudData implements Serializable {
    private String bookDetails;
    private String type;
    private String storeId;
    private int categoryId;
    private HashMap<String, String> map;
    private Bitmap img_bitmap;
    private ArrayList<CloudData> enrichments=new ArrayList<CloudData>();
    private ArrayList<CloudData> resources=new ArrayList<CloudData>();
    private  boolean selected;

    public String getBookDetails() {
        return bookDetails;
    }

    public void setBookDetails(String bookDetails) {
        this.bookDetails = bookDetails;
    }

    public ArrayList<CloudData> getEnrichments() {
        return enrichments;
    }

    public void setEnrichments(ArrayList<CloudData> enrichments) {
        this.enrichments = enrichments;
    }

    public ArrayList<CloudData> getResources() {
        return resources;
    }

    public void setResources(ArrayList<CloudData> resources) {
        this.resources = resources;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public HashMap<String, String> getMap() {
        return map;
    }

    public void setMap(HashMap<String, String> map) {
        this.map = map;
    }

    public Bitmap getImg_bitmap() {
        return img_bitmap;
    }

    public void setImg_bitmap(Bitmap img_bitmap) {
        this.img_bitmap = img_bitmap;
    }
}
