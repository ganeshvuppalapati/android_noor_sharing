package com.semanoor.manahij;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ptg.mindmap.widget.AppDict;
import com.ptg.mindmap.widget.LoadMindMapContent;
import com.ptg.views.CircleButton;
import com.ptg.views.TwoDScrollView;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.GenerateHTML;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.GridShelf;
import com.semanoor.source_sboookauthor.HyperLinkDialog;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.unnamed.b.atv.model.TreeNode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class PreviewActivity extends Activity implements OnClickListener,OnTouchListener {

	private Button btnFirst,btnLast,btn_reference;
	private CircleButton btnBack;
	private Book currentBook;
	private String currentPageNumber;
	private ViewPager viewPager;
	private DatabaseHandler db;
	public WebView currentWebView;
	public ImageView currentImage;
	public LinearLayout ref_linearLayout;
	private int currentDisplayedPageNo;
	private ArrayList<View> deletedObjectListArray;
	private int loadPageNumber;
	public static float mScale = 1.0f;
	private boolean mScrolling = false;
    private float mScrollDiffY = 0;
    private float mLastTouchY = 0;
    private float mScrollDiffX = 0;
    private float mLastTouchX = 0;
    private static final int SCROLLING_THRESHOLD = 10;
    RelativeLayout back_btn_layout;
	private RelativeLayout toolbar,preview_rootvew,bottom_layout;
	boolean isEnrichmentExist = false;
	private boolean shouldOverride = false;
	ArrayList<PageNumbers>storeBookPages;
	public GridShelf gridShelf;
	public boolean mindMapEdited = false;
	WebView webView;
	ImageView bg_ImageView;
	private int bookViewActivityrequestCode = 1;
	ViewAdapter viewAdapter;
	public ArrayList<String> malzamahBookList;
	SeekBar sb_pageslider;
	String bookTitle,pageNo,imageUrl;
	ArrayList<String>malzamahBookDetails;
	private RecyclerView CurrentRecyclerView,subRecyclerView,subRecylerView1;
	private EditText et_search;
	private RelativeLayout rootView;
	public GestureDetector gestureDetector;
	private boolean isLoadEnrInpopview;
	private View gestureView;
	private PopoverView popoverView;
	private RelativeLayout currentParentEnrRLayout,subTabsLayout,subTabLayout1,enr_layout;
	public RelativeLayout add_btn_layout;
	private RecyclerView clickableRecyclerView;
	private Button add_btn,search_img,arrow_btn,search_btn;
	private TreeStructureView treeStructureView;
	private int categoryId;
	private InternetSearch internetSearch;
	public Enrichments currentBtnEnrichment,selectedEnr;
	private LinearLayout search_layout,icon_layout;
	private int currentEnrichmentTabId;
	private ProgressBar progress;
	private CircleButton dropdown_btn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Remove title Bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.preview);
		
		db = DatabaseHandler.getInstance(this);

		currentBook = (Book) getIntent().getSerializableExtra("Book");
		currentPageNumber = getIntent().getExtras().getString("currentPageNo");
		gridShelf = (GridShelf) getIntent().getSerializableExtra("Shelf");
		loadPageNumber = Integer.parseInt(currentPageNumber);
		
		if (currentBook.getBookOrientation() == Globals.portrait) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		}

		gestureDetector = new GestureDetector(PreviewActivity.this,new GestureListener());
		toolbar = (RelativeLayout) findViewById(R.id.relativeLayout1);
		bottom_layout = (RelativeLayout) findViewById(R.id.bottom_layout);
		preview_rootvew = (RelativeLayout) findViewById(R.id.preview_rootvew);
		back_btn_layout = (RelativeLayout) findViewById(R.id.linearLayout);
		sb_pageslider=(SeekBar)findViewById(R.id.sb_pageslider);
		viewPager = (ViewPager) findViewById(R.id.viewPager);
		btnBack= (CircleButton) findViewById(R.id.btn_back);
		CircleButton btn_scroll_first=(CircleButton) findViewById(R.id.btn_scroll_first);
		CircleButton btn_scroll_last=(CircleButton) findViewById(R.id.btn_scroll_last);
		btnBack.setOnClickListener(this);
		btn_scroll_first.setOnClickListener(this);
		btn_scroll_last.setOnClickListener(this);

		btnFirst= (Button) findViewById(R.id.btn_first);
		btnFirst.setOnClickListener(this);

		btnLast= (Button) findViewById(R.id.btn_last);
		btnLast.setOnClickListener(this);

		CircleButton btnEnrichEdit = (CircleButton) findViewById(R.id.btnEnrichEdit);
		ref_linearLayout  =(LinearLayout)findViewById(R.id.enr_linearLayout);
		btn_reference = (Button) findViewById(R.id.btn_reference);
		btnEnrichEdit.setOnClickListener(this);
		btn_reference.setOnClickListener(this);
		if(Globals.isTablet()){
		   if(checkBookEditable()) {
			   btnEnrichEdit.setVisibility(View.INVISIBLE);
		   }else{
			   btnEnrichEdit.setVisibility(View.INVISIBLE);
		   }
	   }else{
		   btnEnrichEdit.setVisibility(View.GONE);
		  // System.out.println("mobile");
	   }
		//malzamahBookList=db.getMalzamahBooks();
		readJsonFile();
		if(Globals.getDesignPageHeight()==0){
			Display display = getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			Globals.setDeviceWidth(size.x);
			Globals.setDeviceHeight(size.y);
			Globals.setDesignPageWidth(size.x);
			Globals.setDesignPageHeight(size.y);
		}

		generateHtmlPages();
		viewAdapter=new ViewAdapter();
		viewPager.setAdapter(viewAdapter);
		if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
			viewPager.setCurrentItem(Integer.parseInt(currentPageNumber) - 1);
		}else{
			viewPager.setCurrentItem(currentBook.getTotalPages() - Integer.parseInt(currentPageNumber));
		}
		sb_pageslider.setMax(currentBook.getTotalPages() - 1);
		setSeekBarValues();
		sb_pageslider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
//				if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
//					viewPager.setCurrentItem(i-1);
//					//setSeekBarTextValues(i - 1);
//				} else {
//					viewPager.setCurrentItem(currentBook.getTotalPages() - i);
//					//setSeekBarTextValues(currentBook.getTotalPages() - i);
//				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				viewPager.setCurrentItem(seekBar.getProgress());
			}
		});
		//loadViewPager();
		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {
				int pageNumber = 0;
				if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
					pageNumber = viewPager.getCurrentItem() + 1;
				} else {
					pageNumber = currentBook.getTotalPages() - viewPager.getCurrentItem();
				}
				currentPageNumber=String.valueOf(pageNumber);
				
				setSeekBarValues();
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});
		/*viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {
                int pageNo=viewPager.getCurrentItem()+1;
				if(!currentBook.is_bStoreBook() &&currentBook.getIsMalzamah().equals("yes")) {
					String pageDetails = db.getPageDetailsinMalzamah(currentBook.getBookID(), pageNo);
					//exist = checkingBookStatus(currentDisplayedPageNo);
					if (pageDetails != null) {
						AlertDialog.Builder builder = new AlertDialog.Builder(PreviewActivity.this);
						builder.setTitle(R.string.alert);
						builder.setMessage("Download Original Book from store to Display this page");
						builder.setCancelable(false);
						builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
						AlertDialog alert = builder.create();
						alert.show();
					}
				}
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});    */


	}
	private void setSeekBarValues(){
		int pageNumber=Integer.parseInt(currentPageNumber);
		if (currentBook.getBookLangugae().equalsIgnoreCase("English")) {
			sb_pageslider.setProgress(pageNumber - 1);
		} else {
			sb_pageslider.setProgress(currentBook.getTotalPages() - pageNumber);
		}
		//setSeekBarTextValues(currentPageNumber);
	}
	private class ViewAdapter extends PagerAdapter{

		private Button btnEnrHome;
		private RelativeLayout rlTabsLayout;
		ArrayList<Enrichments> enrichmentTabListArray;
		RecyclerView recyclerview;


		@Override
		public int getCount() {
			return currentBook.getTotalPages();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return (view == object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position){
			position = position+1;
//			currentPageNumber = String.valueOf(position);
			View view = getLayoutInflater().inflate(R.layout.preview_inflate, null);
			recyclerview = (RecyclerView) view.findViewById(R.id.gridView1 );
			btnEnrHome = (Button) view.findViewById(R.id.btnEnrHome);
			bg_ImageView = (ImageView) view.findViewById(R.id.bgImgView);
			webView = (WebView) view.findViewById(R.id.preview_webView);
			btnEnrHome.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					currentWebView.getSettings().setLoadWithOverviewMode(true);
					currentWebView.getSettings().setUseWideViewPort(true);

					currentWebView.loadUrl("file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+currentDisplayedPageNo+".htm");
                  if(currentBook.get_bStoreID()!=null &&currentBook.get_bStoreID().contains("P")){
						  String path=Globals.TARGET_BASE_BOOKS_DIR_PATH +currentBook.getBookID();
						  String filesPath;
						  if(currentDisplayedPageNo==1){
							  filesPath= path+"/FreeFiles/front_P.png";
						  }else if(currentDisplayedPageNo==currentBook.getTotalPages()){
							  filesPath= path+"/FreeFiles/back_P.png";
							  if(!new File(filesPath).exists()){
								  filesPath =Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" +currentBook.getBookID()+"/thumbnail_end.png";
							  }
						  }else{
							  filesPath= path+"/FreeFiles/"+currentDisplayedPageNo+".png";
						  }
						  Bitmap bitmap= BitmapFactory.decodeFile(filesPath);
						  if(bitmap!=null) {
							  Bitmap b = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceWidth(), Globals.getDeviceHeight(), true);
							  bg_ImageView.setImageBitmap(b);
						  }
						  bg_ImageView.setVisibility(View.VISIBLE);
						  webView.setVisibility(View.GONE);
					  }

				}
			});
//			if (toolbar.isShown()){
//				rlTabsLayout.setVisibility(View.VISIBLE);
//			} else {
//				rlTabsLayout.setVisibility(View.GONE);
//			}

			webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			webView.getSettings().setJavaScriptEnabled(true);
			webView.setWebChromeClient(new WebChromeClient());
			webView.getSettings().setAllowContentAccess(true);
			webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
			webView.getSettings().setAllowFileAccess(true);
			webView.getSettings().setPluginState(PluginState.ON);
			webView.getSettings().setDomStorageEnabled(true);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
				webView.getSettings().setAllowFileAccessFromFileURLs(true);
				webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
			}
			if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab()) {
				webView.getSettings().setLoadWithOverviewMode(true);
				webView.getSettings().setUseWideViewPort(true);
			}
			webView.setWebViewClient(new WebViewClient(){
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					String hyperlink_url[]=url.split("\\?");
					if (hyperlink_url[0].contains("hyperurl://")) {

						String hyperLinkUrl;
						String urlLowerCase=hyperlink_url[0].toLowerCase();
						if(!urlLowerCase.contains("http://")) {
							hyperLinkUrl = urlLowerCase.replace("hyperurl://", "http://");
						}else{
							hyperLinkUrl = urlLowerCase.replace("hyperurl://", "");
						}

						//String hyperLinkUrl = hyperlink_url[0].replace("hyperurl://", "http://");
						HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(PreviewActivity.this, currentWebView);
						hyperLinkDialog.imageDialogWindow(hyperLinkUrl,false,true);

					} else if (hyperlink_url[0].contains("hyperpage://")) {
						String hyperLink_pageNo = hyperlink_url[0].replace("file:///android_asset/hyperpage://", "file:///");
						String hyperLink = hyperlink_url[0].replace("hyperpage://", "file:///");
						HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(PreviewActivity.this, currentWebView);
						hyperLinkDialog.imageDialogWindow(hyperLink,false,true);

					  }else if(hyperlink_url[0].contains("hyperfile://")) {
						String [] split=hyperlink_url[0].split("/");
						String hyperLink = hyperlink_url[0].replace("hyperfile://", "");
						String imgPath=split[split.length-1];
						if (imgPath.contains(".png")||imgPath.contains(".jpg")||imgPath.contains(".gif")) {
							shouldOverride = true;
							HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(PreviewActivity.this, currentWebView);
							hyperLinkDialog.imageDialogWindow(hyperLink,true,false);
						} else if (imgPath.contains(".mp4")||imgPath.contains(".mkv")||imgPath.contains(".3gp")||imgPath.contains(".avi")||imgPath.contains(".flv")) {
							shouldOverride = true;
							HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(PreviewActivity.this, currentWebView);
							hyperLinkDialog.videoHyperLink(hyperLink);
						} else if (imgPath.contains(".mp3")||imgPath.contains(".m4a")) {
							shouldOverride = true;
							HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(PreviewActivity.this, currentWebView);
							hyperLinkDialog.audioHyperLink(hyperLink);
						}
					}
					 		return true;
							//return super.shouldOverrideUrlLoading(view, url);
					}
			});
			//checkForEnrichments(Integer.parseInt(currentPageNumber));
			new loadEnrichmentTabs(position,recyclerview,currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

			File filePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + position + ".htm");
			String htmlFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/";
			if (filePath.exists()) {
				webView.clearCache(true);
				if (!currentBook.is_bStoreBook() && currentBook.getIsMalzamah().equals("yes")) {
					webView.getSettings().setLoadWithOverviewMode(true);
					webView.getSettings().setUseWideViewPort(true);

				}
				String hyperPage= Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + position + ".htm";
				String Path = "file:///" + Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() +"/";

				File f=new File(hyperPage);
				 /* for(int i=0;i<storeBookPages.size();i++){
					  exist=false;
					  PageNumbers page=storeBookPages.get(i);
					  if(page.getpageNumber()==position){
						  exist=true;
						  break;
					  }
				  }*/
				//if(exist==true){

				//String srcString = UserFunctions.decryptFile(f);
//				if(!srcString.equals("")){
//					webView.loadDataWithBaseURL(Path, srcString, "text/html", "utf-8", null);
//				}else{
				boolean  exist=false;
               if(!currentBook.is_bStoreBook() &&currentBook.getIsMalzamah().equals("yes")) {
				    String query="select * from malzamah where BID='"+currentBook.getBookID()+"' and pageNo='"+position+"'";
				    ArrayList<String> malzamahBookList=db.getMalzamahBooks(query);
				    if(malzamahBookList.size()==0) {
						webView.loadUrl("file:///" + hyperPage);
					}else{
					for(int i=0;i<malzamahBookList.size();i++) {
						String pageDetails=malzamahBookList.get(i);
						String split[] = pageDetails.split("##");
						String StoreBID = split[2];
						int bookCount = db.getStoreBookCountFromBookList(StoreBID);
						if(bookCount!=0){
							webView.loadUrl("file:///" + hyperPage);
						}
					}
					}
				}else{
					webView.loadUrl("file:///" + hyperPage);
				}

				//}


			} else if (currentBook.get_bStoreID() != null && currentBook.get_bStoreID().contains("P")) {
				String path = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID();
				String filesPath;
				if (position == 1) {
					filesPath = path + "/FreeFiles/front_P.png";
				} else if (position == currentBook.getTotalPages()) {
					filesPath = path + "/FreeFiles/back_P.png";
					if (!new File(filesPath).exists()) {
						filesPath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + currentBook.getBookID() + "/thumbnail_end.png";
					}
				} else {
					filesPath = path + "/FreeFiles/" + position + ".png";
				}
				Bitmap bitmap= BitmapFactory.decodeFile(filesPath);
				if(bitmap!=null) {
					Bitmap b = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceWidth(), Globals.getDeviceHeight(), true);
					bg_ImageView.setImageBitmap(b);
				}
				bg_ImageView.setVisibility(View.VISIBLE);
				webView.setVisibility(View.GONE);
			}
			//webView.getSettings().setLoadWithOverviewMode(true);
			//webView.getSettings().setUseWideViewPort(true);
			((ViewPager)container).addView(view, 0);
			return view;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object){
			((ViewPager)container).removeView((View) object);
		}

		@Override
		public void setPrimaryItem(ViewGroup container, int position,
				Object object) {
			super.setPrimaryItem(container, position, object);
			RelativeLayout view = (RelativeLayout) object;
			currentWebView = (WebView) view.getChildAt(0);
			currentImage= (ImageView) view.getChildAt(1);
			enr_layout = (RelativeLayout) view.getChildAt(3);

			currentParentEnrRLayout = (RelativeLayout)enr_layout.getChildAt(0);
			subTabsLayout = (RelativeLayout)enr_layout.getChildAt(1);
			subTabLayout1 = (RelativeLayout)enr_layout.getChildAt(2);
			CurrentRecyclerView= (RecyclerView) currentParentEnrRLayout.getChildAt(0);
			subRecyclerView = (RecyclerView) subTabsLayout.getChildAt(0);
			subRecylerView1 = (RecyclerView) subTabLayout1.getChildAt(0);
			RelativeLayout dropdown_layout = (RelativeLayout) subTabLayout1.getChildAt(1);
			dropdown_btn = (CircleButton) dropdown_layout.getChildAt(0);
			dropdown_btn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (SubtabsAdapter.level3!=0) {
						if (currentBtnEnrichment.getEnrichmentId() == SubtabsAdapter.level3) {
							isLoadEnrInpopview = true;
							viewTreeList(v, false, currentBtnEnrichment.getEnrichmentId());
						}
					}
				}
			});
			add_btn_layout = (RelativeLayout) currentParentEnrRLayout.getChildAt(1);
			add_btn = (Button) add_btn_layout.getChildAt(0);
			if (!checkBookEditable()){
				add_btn.setVisibility(View.GONE);
			}
			add_btn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					viewTreeList(v,true,0);
				}
			});
			progress=(ProgressBar)view.getChildAt(2);

			search_layout = (LinearLayout) enr_layout.getChildAt(4);
			LinearLayout layout1 = (LinearLayout) search_layout.getChildAt(0);
			LinearLayout layout2 = (LinearLayout) layout1.getChildAt(0);
			icon_layout = (LinearLayout) layout2.getChildAt(0);
			search_img = (Button) icon_layout.getChildAt(1);
			search_img.setOnClickListener(PreviewActivity.this);
			arrow_btn = (Button) icon_layout.getChildAt(0);
			arrow_btn.setOnClickListener(PreviewActivity.this);
			et_search = (EditText) layout2.getChildAt(1);
			RelativeLayout relativeLayout1 = (RelativeLayout) search_layout.getChildAt(1);
			FrameLayout frameLayout = (FrameLayout) relativeLayout1.getChildAt(0);
			search_btn = (Button) frameLayout.getChildAt(0);
			search_btn.setOnClickListener(PreviewActivity.this);
			internetSearch = new InternetSearch(search_layout, et_search, PreviewActivity.this, search_img);
			currentDisplayedPageNo = position + 1;
			//currentWebView.setOnTouchListener(PreviewActivity.this);
			currentImage.setOnTouchListener(PreviewActivity.this);
			if (Integer.parseInt(currentPageNumber) != 1 && Integer.parseInt(currentPageNumber) != currentBook.getTotalPages()){
				enr_layout.setVisibility(View.VISIBLE);
			}else{
				enr_layout.setVisibility(View.GONE);
			}
			WebView adv_search = (WebView) view.getChildAt(0);
			adv_search.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:{
							break;
						}
						case MotionEvent.ACTION_UP:{
							hideToolbarAndEnrichmentTabs();
							break;
						}
						default:
							break;
					}
					return false;
				}
			});
			boolean exist=false;
			String storeId = null;
			if (!currentBook.is_bStoreBook() && currentBook.getIsMalzamah().equals("yes")) {
				for (int j = 0; j < storeBookPages.size(); j++) {
					PageNumbers page = storeBookPages.get(j);
					exist=false;
					if (currentDisplayedPageNo==page.getpageNumber()) {
						//storePage = true;
						exist=true;
						storeId=page.getHtmlPath();
						pageNo=String.valueOf(page.getpageNumber());
						//bookTitle=page.get
						break;
					}
				}
				if(exist){
					ref_linearLayout.setVisibility(View.VISIBLE);

				}else{
					ref_linearLayout.setVisibility(View.INVISIBLE);
				}
			}else{
				ref_linearLayout.setVisibility(View.INVISIBLE);
			}
			if(storeId!=null) {
				if (malzamahBookDetails != null && malzamahBookDetails.size() > 0) {
					for (int i = 0; i < malzamahBookDetails.size(); i++) {
						String data = malzamahBookDetails.get(0);
						String split[] = data.split("##");
						if (storeId.contains(split[6])) {
							bookTitle = split[4];
							imageUrl = split[9];

							break;
						}
					}
				}
			}

//			if (toolbar.isShown()){
//				rl.setVisibility(View.VISIBLE);
//			} else {
//				rl.setVisibility(View.GONE);
//			}
		}

     public void loadWebView(int position){
//		   checkForEnrichments(Integer.parseInt(currentPageNumber));
		   File filePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+position+".htm");
		   String htmlFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/";
		   if (!filePath.exists()) {
			   if (loadPageNumber == Integer.parseInt(currentPageNumber)) {
				   deletedObjectListArray = Globals.deletedObjectList;
			   } else {
				   deletedObjectListArray = null;
			   }
			   boolean storePage = false;
			   if(!currentBook.is_bStoreBook()&&currentBook.getIsMalzamah().equals("yes")){
				   for(int i=0;i<storeBookPages.size();i++){
					   PageNumbers page=storeBookPages.get(i);
					   if(position==page.getpageNumber()){
						   storePage=true;
						   webView.getSettings().setLoadWithOverviewMode(true);
						   webView.getSettings().setUseWideViewPort(true);
					   }
				   }
			   }
			   new GenerateHTML(PreviewActivity.this, db, currentBook, currentPageNumber, enrichmentTabListArray,storePage).generatePage(htmlFilePath, deletedObjectListArray);
		   }
		   webView.clearCache(true);
		   if(!currentBook.is_bStoreBook()&&currentBook.getIsMalzamah().equals("yes")) {
			   webView.getSettings().setLoadWithOverviewMode(true);
			   webView.getSettings().setUseWideViewPort(true);
		   }
		   webView.loadUrl("file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+position+".htm");
		   if(currentBook.get_bStoreID()!=null &&currentBook.get_bStoreID().contains("P")){
			   String path=Globals.TARGET_BASE_BOOKS_DIR_PATH +currentBook.getBookID();
			   String filesPath;
			   if(position==1){
				   filesPath= path+"/FreeFiles/front_P.png";
			   }else if(position==currentBook.getTotalPages()){
				   filesPath= path+"/FreeFiles/back_P.png";
				   if(!new File(filesPath).exists()){
					   filesPath =Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" +currentBook.getBookID()+"/thumbnail_end.png";
				   }
			   }else{
				   filesPath= path+"/FreeFiles/"+position+".png";
			   }
			   Bitmap bitmap= BitmapFactory.decodeFile(filesPath);
			   if(bitmap!=null) {
				   Bitmap b = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceWidth(), Globals.getDeviceHeight(), true);
				   bg_ImageView.setImageBitmap(b);
			   }
			   bg_ImageView.setVisibility(View.VISIBLE);
			   webView.setVisibility(View.GONE);
		   }
	   }


		/**
		 * check for enrichment tabs and load it in enrichment list array
		 * @param pageNo
		 */

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:{
				break;
			}
			case MotionEvent.ACTION_UP:{
				hideToolbarAndEnrichmentTabs();
				break;
			}
			default:
				break;
		}
		if(v.equals(currentImage)){
			return true;
		}else{
			return false;
		}
	}

		public void readJsonFile() {
			storeBookPages = new ArrayList<PageNumbers>();
			String filePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/FreeFiles/";
			File file = new File(filePath + "malzamah.json");
			if (file.exists()) {
				String jsonStr = null;
				try {
					FileInputStream stream = new FileInputStream(file);
					FileChannel fc = stream.getChannel();
					try {
						stream = new FileInputStream(file);
						MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
						jsonStr = Charset.defaultCharset().decode(bb).toString();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						stream.close();
					}

					JSONObject jsonObject = new JSONObject(jsonStr);
					JSONArray data = jsonObject.getJSONArray("malzamah");
					JSONArray bookDetails = jsonObject.getJSONArray("bookDetails");
					getSelectedStoreBookDetails(bookDetails);
					for (int i = 0; i < data.length(); i++) {
						JSONObject json = data.getJSONObject(i);
						int pageNo = json.getInt("page");
						String Bid = json.getString("BID");
						String htmlPath = json.getString("htmlPath");
						PageNumbers page = new PageNumbers(PreviewActivity.this);
						page.setpageNumber(pageNo) ;
						page.setStoreId(Bid);
						page.setHtmlPath(htmlPath);
						storeBookPages.add(page);

					}
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}

		/**
		 * Make Enrich Button to be selected
		 *
		 * @param v
		 */
        private void getSelectedStoreBookDetails(JSONArray bookDetails){
			malzamahBookDetails=new ArrayList<String>();
			for (int i = 0; i < bookDetails.length(); i++) {
				try {
					//JSONObject json = bookDetails.getJSONObject(i);
					JSONObject jsonSub = bookDetails.getJSONObject(i);
					String description=jsonSub.getString("description");
					String purchaseType=jsonSub.getString("purchaseType");
					String author=jsonSub.getString("author");
					String totalPages=jsonSub.getString("totalPages");
					String title=jsonSub.getString("title");
					String price=jsonSub.getString("price");
					String storeID=jsonSub.getString("storeID");
					String domainURL=jsonSub.getString("domainURL");
					String language=jsonSub.getString("language");
					String imageURL=jsonSub.getString("imageURL");
					String isEditable=jsonSub.getString("isEditable");
					String downloadURL=jsonSub.getString("downloadURL");
					String bookData=description+"##"+purchaseType+"##"+author+"##"+totalPages+"##"+title+"##"+price+"##"+storeID+"##"+domainURL+"##"+language+"##"+imageURL+"##"+isEditable+"##"+downloadURL;
					malzamahBookDetails.add(bookData);
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

		}
		/**
		 * load the enrichment page with sequence id
		 *
		 * @param enrichSequenceId
		 * @param enrichmentPath
		 */
		public void loadEnricmentPage(int enrichSequenceId, String enrType, String enrichmentPath) {
			if (enrType.equals("duplicate") || enrType.equals("")) {
				currentWebView.getSettings().setLoadWithOverviewMode(true);
				currentWebView.getSettings().setUseWideViewPort(true);
			} else {
				currentWebView.getSettings().setLoadWithOverviewMode(false);
				currentWebView.getSettings().setUseWideViewPort(false);
			}
			currentWebView.loadUrl("file:///" + Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + currentDisplayedPageNo + "-" + enrichSequenceId + ".htm");
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
				case R.id.btn_back: {
					currentWebView.reload();
					onBackPressed();
					break;
				}
				case R.id.btn_first: {
					viewPager.setCurrentItem(0);
					break;
				}
				case R.id.btn_last: {
					viewPager.setCurrentItem(currentBook.getTotalPages() - 1);
					break;
				}
				case R.id.btnEnrichEdit: {
					String htmlFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID();
					UserFunctions.deleteGeneratedHtmlPage(htmlFilePath);
                    Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
					//Intent bookViewIntent = new Intent(getApplicationContext(), PreviewActivity.class);
					bookViewIntent.putExtra("Book", currentBook);
					//bookViewIntent.putExtra("CurrentPageNumber", String.valueOf(currentBook.get_lastViewedPage()));
					bookViewIntent.putExtra("currentPageNo", viewPager.getCurrentItem()+1);
					bookViewIntent.putExtra("Shelf", gridShelf);
					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
					break;
				}
				case R.id.btn_scroll_first:{
					viewPager.setCurrentItem(0);
					break;
				}
				case R.id.btn_scroll_last: {
					viewPager.setCurrentItem(currentBook.getTotalPages() - 1);
					break;
				}
				case R.id.btn_reference:{
					PopoverView page_info = new PopoverView(PreviewActivity.this, R.layout.malzamah_info);
					int width =  (int) getResources().getDimension(R.dimen.page_info_width);
					int height =  (int) getResources().getDimension(R.dimen.page_info_height);
					page_info.setContentSizeForViewInPopover(new Point(width, height));
					//exportEnrPopover.setDelegate(PreviewActivity.this);
					page_info.showPopoverFromRectInViewGroup(this.preview_rootvew, PopoverView.getFrameForView(ref_linearLayout), PopoverView.PopoverArrowDirectionUp, true);
					ImageView book_image= (ImageView)page_info.findViewById(R.id.imageView2);
					TextView tv_bookTitle= (TextView)page_info. findViewById(R.id.tv_bookTitle);
					TextView tv_pageNo= (TextView)page_info. findViewById(R.id.page_no);
					tv_bookTitle.setText(bookTitle);
					tv_pageNo.setText("Page:"+pageNo);
					Glide.with(PreviewActivity.this).load(imageUrl)
							.thumbnail(0.5f)
							.crossFade()
							.diskCacheStrategy(DiskCacheStrategy.ALL)
							.override(200,200)
							.into(book_image);
					break;
				} case R.id.add_btn_tab:{
					viewTreeList(v,true,0);
					break;
				}case R.id.btn_search:{
					internetSearch.searchClicked(new InternetSearchCallBack() {
						@Override
						public void onClick(String url, String title) {
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(preview_rootvew.getWindowToken(), 0);
							new createAdvanceSearchTab(url, title).execute();
						}
					});
					break;
				}case R.id.search_image:{
					internetSearch.showSearchPopUp(v);
					break;
				}case R.id.arrow_btn:{
					internetSearch.showSearchPopUp(v);
					break;
				}
				default:
					break;
			}
		}

		@Override
		protected void onPause() {
			super.onPause();
		/*try {
			Class.forName("android.webkit.WebView").getMethod("onPause", (Class[]) null).invoke(webView, (Object[])null);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}*/
		}

		public void onBackPressed() {
		/*viewPager.removeAllViews();
		webView.clearHistory();
		webView.clearCache(true);
		webView.loadUrl("about:blank");
		webView.freeMemory();
		webView.pauseTimers();
		webView = null;*/

			UserFunctions.copyHTMLPageToMalzamah(currentBook);
		/*String absPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
		try {
			UserFunctions.copyDirectory(new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID()),new File(absPath+currentBook.getBookID()));
		} catch (IOException e) {
			e.printStackTrace();
		} */
			db.executeQuery("update books set LastViewedPage='" + currentPageNumber + "' where BID='" + currentBook.getBookID() + "'");
			currentBook.set_lastViewedPage(Integer.parseInt(currentPageNumber));
			String htmlFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID();
			//UserFunctions.deleteGeneratedHtmlPage(htmlFilePath);
			setResult(RESULT_OK, getIntent().putExtra("Book", currentBook));
			setResult(RESULT_OK, getIntent().putExtra("Shelf", gridShelf));
			setResult(RESULT_OK, getIntent().putExtra("mindMap", mindMapEdited));
			finish();
		}

		@Override
		protected void onDestroy() {
			super.onDestroy();
		}

	
	/*public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		    float xPoint = getDensityIndependentValue(event.getX(), PreviewActivity.this) / getDensityIndependentValue(mScale, PreviewActivity.this);
	        float yPoint = getDensityIndependentValue(event.getY(), PreviewActivity.this) / getDensityIndependentValue(mScale, PreviewActivity.this);
	        switch (event.getAction()) {
	        case MotionEvent.ACTION_DOWN:
	        
	        	  break;
	        case MotionEvent.ACTION_UP:
	            if (!mScrolling) {
	               // endSelectionMode();
	                hideSeekbarAndEnrichmentTabs();
	                //
	                // Fixes 4.4 double selection
	                // See: http://stackoverflow.com/questions/20391783/how-to-avoid-default-selection-on-long-press-in-android-kitkat-4-4
	                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
	                    return false;
	                }
	            }
	            mScrollDiffX = 0;
	            mScrollDiffY = 0;
	            mScrolling = false;
	            //
	            // Fixes 4.4 double selection
	            // See: http://stackoverflow.com/questions/20391783/how-to-avoid-default-selection-on-long-press-in-android-kitkat-4-4
	            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
	            	return true;
	            }
	            
	            

	            break;
	        case MotionEvent.ACTION_MOVE:
	            mScrollDiffX += (xPoint - mLastTouchX);
	            mScrollDiffY += (yPoint - mLastTouchY);
	            mLastTouchX = xPoint;
	            mLastTouchY = yPoint;
	            if (Math.abs(mScrollDiffX) > SCROLLING_THRESHOLD || Math.abs(mScrollDiffY) > SCROLLING_THRESHOLD) {
	                mScrolling = true;
	                //isDoubleTapped = false;
	            }
	            break;
	        }

		return false;
	}*/

	public void hideToolbarAndEnrichmentTabs(){

		/*Page slider - Begin*/
			ArrayList<Enrichments> enrichmentTabListArray;
			if (toolbar.isShown()) {
				if (currentBook.is_bStoreBook() || currentBook.get_bStoreID() != null && currentBook.get_bStoreID().contains("P")) {
					enrichmentTabListArray = db.getEnrichmentTabList(currentBook.getBookID(), viewPager.getCurrentItem() + 1, PreviewActivity.this);
					//if (enrichmentTabListArray.size()== 1) {

					//}
				}
				enr_layout.setVisibility(View.GONE);
				toolbar.setVisibility(View.GONE);
				back_btn_layout.setVisibility(View.GONE);
				bottom_layout.setVisibility(View.GONE);
			} else {
				if (currentBook.is_bStoreBook() || currentBook.get_bStoreID() != null && currentBook.get_bStoreID().contains("P")) {
					enrichmentTabListArray = db.getEnrichmentTabList(currentBook.getBookID(), viewPager.getCurrentItem() + 1, PreviewActivity.this);
				}
				toolbar.setVisibility(View.VISIBLE);
				enr_layout.setVisibility(View.VISIBLE);
				back_btn_layout.setVisibility(View.VISIBLE);
				bottom_layout.setVisibility(View.VISIBLE);
			}
		}

		private float getDensityIndependentValue(float val, Context ctx) {
			Display display = ((WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
			DisplayMetrics metrics = new DisplayMetrics();
			display.getMetrics(metrics);
			return val / (metrics.densityDpi / 160f);
		}

		@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {

			if (requestCode == bookViewActivityrequestCode && data != null) {
			/*if (Globals.isLimitedVersion()) {
				return;
			}*/
				currentBook = (Book) data.getSerializableExtra("Book");
				gridShelf = (GridShelf) data.getSerializableExtra("Shelf");
				boolean mindMapedited = data.getBooleanExtra("mindMap", false);
				currentPageNumber = data.getExtras().getString("currentPageNo");
				readJsonFile();
				if (viewAdapter != null) {
					//viewAdapter.loadWebView(Integer.parseInt(currentPageNumber));
					//viewAdapter.new generateHtmlandload(Integer.parseInt(currentPageNumber)).execute();
					loadPageNumber = Integer.parseInt(currentPageNumber);
					//malzamahBookList=db.getMalzamahBooks();
					int pageNo=Integer.parseInt(currentPageNumber);
					generateHtmlPages();
					viewAdapter=new ViewAdapter();
					viewPager.setAdapter(viewAdapter);
					viewPager.setCurrentItem(pageNo-1);
					//viewAdapter.notifyDataSetChanged();
				}
			}
		}

    public void generateHtmlPages() {
        for (int i = 0; i < currentBook.getTotalPages(); i++) {
			int pageNo = i + 1;
			String PageNumber = String.valueOf(pageNo);
			ArrayList<Enrichments> enrichmentTabListArray = db.getEnrichmentTabList(currentBook.getBookID(), pageNo, PreviewActivity.this);
			File filePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + pageNo + ".htm");
			String htmlFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/";
			//if (!filePath.exists()) {
				if (loadPageNumber == Integer.parseInt(PageNumber)) {
					deletedObjectListArray = Globals.deletedObjectList;
				} else {
					deletedObjectListArray = null;
				}
				boolean storePage = false;
				if (!currentBook.is_bStoreBook() && currentBook.getIsMalzamah().equals("yes")) {
					for (int j = 0; j < storeBookPages.size(); j++) {
						PageNumbers page = storeBookPages.get(j);
						if (pageNo == page.getpageNumber()) {
							storePage = true;
							//webView.getSettings().setLoadWithOverviewMode(true);
							//webView.getSettings().setUseWideViewPort(true);
						}
					}
				}
				new GenerateHTML(PreviewActivity.this, db, currentBook, PageNumber, enrichmentTabListArray, storePage).generatePage(htmlFilePath, deletedObjectListArray);
			//}
		}
	}
	 private boolean checkingBookStatus(int page){
		 boolean exist=false;
		 boolean storeBookExist=false;
		 String sourceStoreId=null;
		 //if(currentBook.is_bStoreBook()) {
			 for (int i = 0; i < malzamahBookList.size(); i++) {
				 exist = false;
				 String malzamahBook = malzamahBookList.get(i);
				 String[] split = malzamahBook.split("##");
				 int bookId = Integer.parseInt(split[0]);
				 int PageNo = Integer.parseInt(split[1]);
				 sourceStoreId = split[2];
				 String htmlPath = split[3];
				 //int totalPages = Integer.parseInt(c.getString(5));
				 //boolean storeBook = Boolean.parseBoolean(c.getString(9));
				 String thumbPath = split[4];
				 if (bookId ==currentBook.getBookID() &&PageNo==page) {
					 exist = true;
					 break;
				 }
			 }
		// }
         if(exist) {
			 for (int i = 0; i < gridShelf.bookListArray.size(); i++) {
				 Book book = (Book) gridShelf.bookListArray.get(i);
				 if (book.is_bStoreBook() && book.get_bStoreID().equals(sourceStoreId)) {
					 storeBookExist=true;
					 break;
				 }
			 }
		 }else{
			 storeBookExist=true;
		 }

		 return storeBookExist;

	 }

	public static boolean isTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK)
				>= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	private boolean checkBookEditable(){
		String query = "select * from objects where BID='"+currentBook.getBookID()+"'order by SequentialID";
		ArrayList<Object> objectList = db.getAllObjectsFromPage(query);
		if(currentBook.getIsEditable().equals("yes") && currentBook.getDownloadURL()!=null &&objectList.size()>2){
			return true;
		}else{
			return false;
		}
	}

	public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

	   private ArrayList<Enrichments>horizontalList;
		RecyclerView recyclerView;
		int pageNo;
		public class MyViewHolder extends RecyclerView.ViewHolder {
			public Button btn_home;
			public Button del_btn;
			public Button add_btn;

			public MyViewHolder(View view) {
				super(view);
				btn_home = (Button) view.findViewById(R.id.btnEnrHome);
				del_btn = (Button) view.findViewById(R.id.btn_enr_del);
				add_btn = (Button) view.findViewById(R.id.add_btn);
				//enrichmentTabList = db.getEnrichmentTabListForBookReadAct1(currentBook.getBookID(), pageNo, BookViewReadActivity.this, enrichmentLayout);

			}
		}


		public HorizontalAdapter(int pageNumber, RecyclerView recycler, ArrayList<Enrichments> enrichmentTabListArray) {
			// this.horizontalList = horizontalList;
			this.pageNo=pageNumber;
			this.recyclerView=recycler;
			this.horizontalList=enrichmentTabListArray;
		}

		@Override
		public HorizontalAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.enrichment_buttons, parent, false);

			return new HorizontalAdapter.MyViewHolder(itemView);
		}

		@Override
		public void onBindViewHolder(final HorizontalAdapter.MyViewHolder holder, final int position) {

			final Enrichments enrich= horizontalList.get(position);
			holder.btn_home.setText(enrich.getEnrichmentTitle());

			if(enrich.isEnrichmentSelected()){
				holder.btn_home.setSelected(true);
				//enrichmentButtonOnClickOperation(enrich);
				if(position==0) {
					loadingHomePage();
				}

				LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
				llm.scrollToPositionWithOffset(position , horizontalList.size());
				llm.scrollToPosition(position);
			}else{
				holder.btn_home.setSelected(false);
			}

			if(enrich.getEnrichmentType().equals(Globals.advancedSearchType)){
				holder.del_btn.setVisibility(View.VISIBLE);
			}else{
				holder.del_btn.setVisibility(View.GONE);
			}

			holder.add_btn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// addTab();
				}
			});
			holder.btn_home.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					if(enrich.equals("Search")){
						enrich.showAdvsearchEnrichDialog(enrich);
					}else{
						enrich.showEnrichDialog(enrich);
					}
					return false;
				}
			});
			holder.btn_home.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

//                    selectedPosition = String.valueOf(position);
//                    loadWebView();
					for(Enrichments enrich:horizontalList){
						if(enrich.isEnrichmentSelected()){
							enrich.setEnrichmentSelected(false);
							break;
						}
					}
					/*if (enrich.getEnrichmentType().equals(Globals.advancedSearchType)) {
						//enrichmentButtonOnClickOperation(enrich);
						// clickedAdvanceSearchtab(enrich);
					} else if (enrich.getEnrichmentType().equals(Globals.onlineEnrichmentsType) || (enrich.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB))) {
						//  clickedAdvanceSearchtab(enrich);
						//enrichmentButtonOnClickOperation(enrich);
					} else {
						// clickedEnrichments(enrich);
						//enrichmentButtonOnClickOperation(enrich);

					} */
					if(position==0){
						enrich.setEnrichmentSelected(true);
						loadingHomePage();
					}else {
						enrich.setEnrichmentSelected(true);
						currentImage.setVisibility(View.GONE);
						currentWebView.setVisibility(View.VISIBLE);
						//makeEnrichedBtnSlected(btnEnrichmentTab);
						loadEnricmentPage(enrich.getEnrichmentId(), enrich.getEnrichmentType(), enrich.getEnrichmentPath());
						holder.btn_home.setSelected(true);
						enrich.setEnrichmentSelected(true);
						//currentEnrichmentTabId=enrich.getEnrichmentId();
						//currentBtnEnrichment=enrich;
					}
					notifyDataSetChanged();
//                    recyclerView

					//   adapter.notifyDataSetChanged();
				}
			});
			holder.del_btn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					//deletingSearchEnrichments(enrich);
					String path = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.get_bStoreID()+"Book/xmlForUpdateRecords.xml";
					// deleteXmlEntryInUpdateRecordsXml(path,enrich);
				}




			});
		}

		@Override
		public int getItemCount() {
			return horizontalList.size();
		}

	}

	public class loadEnrichmentTabs extends AsyncTask<Void, Void, Void>{

		ArrayList<Enrichments> enrichmentTabList;
		int pageNumber;
		RecyclerView recyclerview;
		int Id;

		public loadEnrichmentTabs(int pageNumber, RecyclerView recyclerView, int SelectecEnrId){
			this.pageNumber = pageNumber;
			this.recyclerview=recyclerView;
			this.Id=SelectecEnrId;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... voids) {
			enrichmentTabList = db.getEnrichmentTabListForBookReadAct1(currentBook.getBookID(), pageNumber, PreviewActivity.this);
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			boolean selected = false;
			for(int i=0;i<enrichmentTabList.size();i++){
				Enrichments enrich=enrichmentTabList.get(i);
				selected=false;
				if(enrich.getEnrichmentId()==Id){
					enrichmentTabList.remove(i);
					enrich.setEnrichmentSelected(true);
					enrichmentTabList.add(i,enrich);
					selected=true;
					break;
				}
			}
			if(selected&&Id!=0){
				Enrichments enrich=enrichmentTabList.get(0);
				enrich.setEnrichmentSelected(false);
			}
			ArrayList<Enrichments> topEnrList = new ArrayList<>();
			for (Enrichments enrichments : enrichmentTabList){
				if (enrichments.getCategoryID()==0){
					topEnrList.add(enrichments);
				}
			}
			loadView(pageNumber,topEnrList,recyclerview);
		}
	}

	public class GestureListener extends GestureDetector.SimpleOnGestureListener{

		@Override
		public boolean onDown(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onDoubleTap(MotionEvent e) {
			if (gestureView!=null) {
				if (checkBookEditable()) {
					viewTreeList(gestureView, false, selectedEnr.getEnrichmentId());
				}
			}
			return true;
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			return false;
		}

		@Override
		public void onLongPress(MotionEvent e) {
			super.onLongPress(e);
		}
	}

	public class loadSubEnrichmentTabs extends AsyncTask<Void, Void, Void>{

		ArrayList<Enrichments> enrichmentTabList;
		int pageNumber;
		RecyclerView recyclerview;
		int Id;
		int categoryId;
		RecyclerView clickableRecycler;

		public loadSubEnrichmentTabs(int pageNumber, RecyclerView recyclerView, int SelectecEnrId,int catId,RecyclerView newRecycler){
			this.pageNumber = pageNumber;
			this.recyclerview=recyclerView;
			this.Id=SelectecEnrId;
			this.categoryId = catId;
			this.clickableRecycler = newRecycler;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... voids) {
			enrichmentTabList = db.getEnrichmentTabListForCatId(currentBook.getBookID(), pageNumber, PreviewActivity.this,categoryId);
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			if (enrichmentTabList.size()>0) {
				subTabsLayout.setVisibility(View.VISIBLE);
				if (recyclerview.getId() == R.id.gridView3) {
					subTabLayout1.setVisibility(View.VISIBLE);
				}else{
					subTabLayout1.setVisibility(View.GONE);
				}

				loadView(pageNumber,enrichmentTabList,recyclerview);
			}else{
				if (clickableRecycler.getId()==R.id.gridView1) {
					subTabsLayout.setVisibility(View.GONE);
				}
				subTabLayout1.setVisibility(View.GONE);
			}
		}
	}

	public void loadView(int pageNumber,ArrayList<Enrichments> enrichmentTabList,RecyclerView recyclerview){
		SubtabsAdapter  adapter = new SubtabsAdapter(subTabsLayout,subTabLayout1,subRecyclerView,subRecylerView1,db,PreviewActivity.this, pageNumber, enrichmentTabList, recyclerview, new OnclickCallBackInterface<Object>() {
			@Override
			public void onClick(Object object) {
				progress.setVisibility(View.GONE);
				CallBackObjects objects = (CallBackObjects) object;
				if (objects.doubleTap){
					gestureView = objects.view;
					isLoadEnrInpopview = objects.loadEnrPoview;
					//  treeViewStructureList(gestureView, false,selectedEnr.getEnrichmentId());
					viewPopUp(gestureView,currentBtnEnrichment.getEnrichmentId());
					//viewTreeList(gestureView, false,selectedEnr.getEnrichmentId());
				}else {
					RecyclerView recyclerView = objects.recyclerView;
					clickableRecyclerView = recyclerView;
					if (recyclerView != null) {
						int position = objects.adapterPosition;
						Enrichments enrichment = objects.horizontalList.get(position);
						if (recyclerView.getId() == R.id.gridView2){
							SubtabsAdapter.level2 = enrichment.getEnrichmentId();
						}else if (recyclerView.getId() == R.id.gridView3){
							SubtabsAdapter.level3 = enrichment.getEnrichmentId();
						}else{
							SubtabsAdapter.level1 = enrichment.getEnrichmentId();
						}
						for (Enrichments enrich : objects.horizontalList) {
							if (enrich.isEnrichmentSelected()) {
								enrich.setEnrichmentSelected(false);
								break;
							}
						}
						if (recyclerView.getId() == R.id.gridView1 && position == 0 && enrichment.getCategoryID() == 0) {
							loadingHomePage();
						}
						if (enrichment.getEnrichmentType().equals(Globals.advancedSearchType)) {
							clickedAdvanceSearchtab(enrichment);
						} else if (enrichment.getEnrichmentType().equals(Globals.onlineEnrichmentsType) || (enrichment.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB))) {
							clickedAdvanceSearchtab(enrichment);
						} else {
							if (enrichment.getEnrichmentId() != 0) {
								clickedEnrichments(enrichment);
							}
						}
						enrichment.setEnrichmentSelected(true);
						currentEnrichmentTabId = enrichment.getEnrichmentId();
						currentBtnEnrichment = enrichment;
						selectedEnr = currentBtnEnrichment;
						if (recyclerView.getId() == R.id.gridView1) {
							if (enrichment.getEnrichmentId() != 0) {
								new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, enrichment.getEnrichmentId(), recyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
							} else {
								subTabsLayout.setVisibility(View.GONE);
								subTabLayout1.setVisibility(View.GONE);
							}
						} else if (recyclerView.getId() == R.id.gridView2) {
							new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, enrichment.getEnrichmentId(), recyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
						}
					}
				}
			}
		});
		LinearLayoutManager horizontalLayoutManagaer
				= new LinearLayoutManager(PreviewActivity.this, LinearLayoutManager.HORIZONTAL, false);
		recyclerview.setNestedScrollingEnabled(false);
		recyclerview.setLayoutManager(horizontalLayoutManagaer);
		recyclerview.setAdapter(adapter);
	}
	private void renamePopup(){
		final Dialog groupDialog = new Dialog(PreviewActivity.this);
		groupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
		groupDialog.setTitle(R.string.title);
		groupDialog.setContentView(getLayoutInflater().inflate(R.layout.group_edit, null));
		groupDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.5), RelativeLayout.LayoutParams.WRAP_CONTENT);
		final EditText editText = (EditText) groupDialog.findViewById(R.id.enr_editText);
		Button btnSave = (Button) groupDialog.findViewById(R.id.enr_save);
		btnSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (editText.getText().toString().length()>0){
					db.executeQuery("update enrichments set Title='" + editText.getText().toString() + "' where enrichmentID='" + currentBtnEnrichment.getEnrichmentId() + "' and BID='"+currentBook.getBookID()+"'");
					if (clickableRecyclerView.getId() == R.id.gridView1) {
						new loadEnrichmentTabs(Integer.parseInt(currentPageNumber),CurrentRecyclerView,currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
						new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
					} else if (clickableRecyclerView.getId() == R.id.gridView2) {
						new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
						new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
					}else{
						new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
					}
					groupDialog.dismiss();
				}
			}
		});
		Button btnCancel = (Button) groupDialog.findViewById(R.id.enr_cancel);
		btnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				groupDialog.dismiss();
			}
		});
		groupDialog.show();
	}

	public void deletePopup(){
		AlertDialog.Builder builder = new AlertDialog.Builder(PreviewActivity.this);
		builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				db.executeQuery("delete from enrichments where enrichmentID = '" + currentBtnEnrichment.getEnrichmentId() + "'");
				if (clickableRecyclerView.getId() == R.id.gridView1 &&currentBtnEnrichment.getCategoryID() == 0) {
					subTabsLayout.setVisibility(View.GONE);
					subTabLayout1.setVisibility(View.GONE);
				}else{
					RecyclerView curRecycler;
					int catID;
					if (clickableRecyclerView.getId() == R.id.gridView2){
						curRecycler = CurrentRecyclerView;
						catID = SubtabsAdapter.level1;
					}else{
						curRecycler = clickableRecyclerView;
						catID = SubtabsAdapter.level2;
					}
					new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber),clickableRecyclerView,0,catID,curRecycler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				}
				dialog.cancel();
			}
		});
		builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.setTitle(R.string.delete);
		builder.setMessage(R.string.suredelete);
		builder.show();
	}

	public void viewTreeList(View view, boolean isAddEnrich, final int catId){
		categoryId = catId;
		treeStructureView = new TreeStructureView(currentBook, db, Integer.parseInt(currentPageNumber), popoverView, PreviewActivity.this, preview_rootvew, new TreeStructureCallBack() {
			@Override
			public void onClick(TreeNode node, Object object, PopoverView popoverView) {
				MyTreeHolder.IconTreeItem treeItem = (MyTreeHolder.IconTreeItem) object;
				Enrichments enrichments3 = treeItem.enrich;
				if (enrichments3.getEnrichmentId()!=0){
					currentBtnEnrichment = enrichments3;
					if (enrichments3.getEnrichmentType().equals(Globals.advancedSearchType)) {

						clickedAdvanceSearchtab(enrichments3);
					} else if (enrichments3.getEnrichmentType().equals(Globals.onlineEnrichmentsType) || (enrichments3.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB))) {
						clickedAdvanceSearchtab(enrichments3);
					} else {
						if (enrichments3.getEnrichmentId() != 0) {
							clickedEnrichments(enrichments3);
						}
					}
				}
				node.setSelected(true);
				if (enrichments3.getEnrichmentTitle().equals("")) {
					if (popoverView != null) {
						popoverView.dissmissPopover(true);
					}
					//  treeViewStructureList(gestureView, true, enrichments1.getCategoryID());
					viewTreeList(gestureView, true, enrichments3.getCategoryID());
				} else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.internet_search))) {
					if (popoverView != null) {
						popoverView.dissmissPopover(true);
					}
					search_layout.setVisibility(View.VISIBLE);
				}else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.rename))){
					if (popoverView != null) {
						popoverView.dissmissPopover(true);
					}
					renamePopup();
				}else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.delete))){
					if (popoverView != null) {
						popoverView.dissmissPopover(true);
					}
					deletePopup();
				}else if (enrichments3.getEnrichmentId()==0 && (enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.blank_page)) || enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.duplicate_page)))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
					bookViewIntent.putExtra("Book", currentBook);
					bookViewIntent.putExtra("currentPageNo", viewPager.getCurrentItem()+1);
					bookViewIntent.putExtra("enrichmentTabId", 0);
					bookViewIntent.putExtra("Shelf", gridShelf);
					if (enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.blank_page))) {
						bookViewIntent.putExtra("createBlankEnr", "blank");
					}else{
						bookViewIntent.putExtra("createBlankEnr", "duplicate");
					}
					bookViewIntent.putExtra("categoryId",catId);
					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
				}else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.edit))){
                    if (popoverView != null) {
                        popoverView.dissmissPopover(true);
                    }
                    Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
					bookViewIntent.putExtra("Book", currentBook);
					bookViewIntent.putExtra("currentPageNo", viewPager.getCurrentItem()+1);
					bookViewIntent.putExtra("enrichmentTabId", currentBtnEnrichment.getEnrichmentId());
					bookViewIntent.putExtra("Shelf", gridShelf);
					bookViewIntent.putExtra("createBlankEnr", "edit");
					bookViewIntent.putExtra("categoryId",catId);
					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
				}
			}
		});
		treeStructureView.treeViewStructureList(isLoadEnrInpopview,currentBtnEnrichment,view,isAddEnrich,catId);
	}

	private void loadingHomePage(){
		currentWebView.getSettings().setLoadWithOverviewMode(true);
		currentWebView.getSettings().setUseWideViewPort(true);

		currentWebView.loadUrl("file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+currentDisplayedPageNo+".htm");
		if(currentBook.get_bStoreID()!=null &&currentBook.get_bStoreID().contains("P")){
			String path=Globals.TARGET_BASE_BOOKS_DIR_PATH +currentBook.getBookID();
			String filesPath;
			if(currentDisplayedPageNo==1){
				filesPath= path+"/FreeFiles/front_P.png";
			}else if(currentDisplayedPageNo==currentBook.getTotalPages()){
				filesPath= path+"/FreeFiles/back_P.png";
				if(!new File(filesPath).exists()){
					filesPath =Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" +currentBook.getBookID()+"/thumbnail_end.png";
				}
			}else{
				filesPath= path+"/FreeFiles/"+currentDisplayedPageNo+".png";
			}
			Bitmap bitmap= BitmapFactory.decodeFile(filesPath);
			if(bitmap!=null) {
				Bitmap b = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceWidth(), Globals.getDeviceHeight(), true);
				bg_ImageView.setImageBitmap(b);
			}
			bg_ImageView.setVisibility(View.VISIBLE);
			webView.setVisibility(View.GONE);
		}
	}

	public void clickedAdvanceSearchtab(final Enrichments enrButton) {
		currentWebView.clearView();
		currentParentEnrRLayout.setVisibility(View.VISIBLE);
		currentWebView.setVisibility(View.VISIBLE);
		currentImage.setVisibility(View.GONE);
		currentEnrichmentTabId = enrButton.getEnrichmentId();
		currentBtnEnrichment = enrButton;
		// checkForPaintAndApply(currentCanvasImgView, currentPageNumber, enrButton.getEnrichmentId());
		if (enrButton.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
			currentWebView.getSettings().setLoadWithOverviewMode(false);
			currentWebView.getSettings().setUseWideViewPort(false);
		} else {
			currentWebView.getSettings().setLoadWithOverviewMode(true);
			currentWebView.getSettings().setUseWideViewPort(true);
		}
		currentWebView.getSettings().setJavaScriptEnabled(true);
		currentWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		currentWebView.clearCache(true);
		currentWebView.getSettings().setAllowContentAccess(true);
		currentWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		currentWebView.getSettings().setAllowFileAccess(true);
		currentWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
		currentWebView.getSettings().setDomStorageEnabled(true);
		if (enrButton.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
			String mindMapPath = Globals.TARGET_BASE_MINDMAP_PATH + enrButton.getEnrichmentPath() + ".xml";
			if (!new File(mindMapPath).exists()) {
				UserFunctions.DisplayAlertDialog(PreviewActivity.this, R.string.mindmap_deleted_msg, R.string.mindmap_deleted);
			} else {
				currentWebView.setVisibility(View.GONE);
				progress.setVisibility(View.GONE);
				//loadMindMap(enrButton.getEnrichmentPath(),designPageLayout,pdfActivity.this);
			}
		} else {
			currentWebView.loadUrl(enrButton.getEnrichmentPath());
		}
		progress.setVisibility(View.VISIBLE);

		if (enrButton.getEnrichmentType().equals("Search")) {
			currentWebView.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:{
							break;
						}
						case MotionEvent.ACTION_UP:{
							currentWebView.setWebViewClient(new CustomWebViewClient());
							if (enr_layout.getVisibility() == View.VISIBLE) {
								enr_layout.setVisibility(View.GONE);
							} else {
								enr_layout.setVisibility(View.VISIBLE);
							}
							break;
						}
						default:
							break;
					}
					return false;
				}
			});
		}

		currentWebView.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageFinished(WebView view, String url) {
				progress.setVisibility(View.INVISIBLE);
			}


		});
	}

	public void loadMindMap(final String title,final RelativeLayout layout,final Context context) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final RelativeLayout canvasView = new RelativeLayout(context);
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
				params.addRule(RelativeLayout.BELOW,enr_layout.getId());
				canvasView.setLayoutParams(params);
				final TwoDScrollView twoDScroll = new TwoDScrollView(context, canvasView);
				twoDScroll.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
				twoDScroll.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
				twoDScroll.setEnabled(false);
				canvasView.addView(twoDScroll);
				ViewGroup drawingView = new RelativeLayout(context);
				drawingView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				twoDScroll.addView(drawingView);
				//setSelectedView(viewEdit);
				drawingView.getLayoutParams().width = AppDict.DEFAULT_OFFSET;
				drawingView.getLayoutParams().height = AppDict.DEFAULT_OFFSET;
				String url = Globals.TARGET_BASE_MINDMAP_PATH + title + ".xml";
				LoadMindMapContent mindMapContent = new LoadMindMapContent(twoDScroll, drawingView, context);
				mindMapContent.loadXmlData(url, false);
				Rect rectf = new Rect();
				drawingView.getLocalVisibleRect(rectf);
				twoDScroll.calculateBounds();
				twoDScroll.scroll((int) (AppDict.DEFAULT_OFFSET - canvasView.getWidth() + 120) / 2,
						(int) (AppDict.DEFAULT_OFFSET - canvasView.getHeight()) / 2);
				layout.addView(canvasView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
			}
		});
	}

	public class createAdvanceSearchTab extends AsyncTask<Void, Void, Void> {
		String Url;
		String title;

		public createAdvanceSearchTab(String url, String s) {
			Url = url;
			title = s;
		}

		@Override
		protected void onPreExecute() {
			title = title.replace("'", "''");
			int  objSequentialId = db.getMaximumSequentialId(Integer.parseInt(currentPageNumber), currentBook.getBookID()) + 1;
			db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,SequentialID,categoryID)values('" + currentBook.getBookID() + "','" + currentPageNumber + "','" + title + "','Search', '0','" + Url + "','"+objSequentialId+"','"+categoryId+"')");
			int objUniqueId = db.getMaxUniqueRowID("enrichments");
			title = title.replace("''", "'");
			currentEnrichmentTabId = objUniqueId;
			Enrichments enrichments = db.getEnrichment(objUniqueId,objSequentialId,PreviewActivity.this);
			currentBtnEnrichment = enrichments;
			if (clickableRecyclerView == null){
				SubtabsAdapter.level1 = objUniqueId;
				new loadEnrichmentTabs(Integer.parseInt(currentPageNumber), CurrentRecyclerView, currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			}else {
				if (clickableRecyclerView.getId() == R.id.gridView1) {
					new loadEnrichmentTabs(Integer.parseInt(currentPageNumber), CurrentRecyclerView, currentEnrichmentTabId).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
					if (currentBtnEnrichment.getCategoryID()!=0) {
						new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, currentBtnEnrichment.getCategoryID(), clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
					}
				} else if (clickableRecyclerView.getId() == R.id.gridView2) {
					new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecyclerView, 0, SubtabsAdapter.level1, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
					new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, currentBtnEnrichment.getCategoryID(), clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
				} else {
					SubtabsAdapter.level3 = objUniqueId;
					new loadSubEnrichmentTabs(Integer.parseInt(currentPageNumber), subRecylerView1, 0, SubtabsAdapter.level2, clickableRecyclerView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
				}
			}
			if (enrichments!=null){
				clickedAdvanceSearchtab(enrichments);
			}
		}

		@Override
		protected Void doInBackground(Void... voids) {

			return null;
		}


		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
		}
	}

	public class CustomWebViewClient extends WebViewClient {
		@Override
		public void onReceivedError(WebView view, int errorCode,
									String description, String failingUrl) {
			String url = "";
			//createAdvancedSearchTab(advtitle, url).execute();
		}

		@Override
		// Method where you(get) load the 'URL' which you have clicked
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (currentBtnEnrichment.getEnrichmentType().equals(Globals.advancedSearchType)) {
				try {
					if (url != null) {
						//currentProgressbar.setVisibility(View.VISIBLE);
						String path = url;
						String[] adv_bkmrktype = path.split("/");
						String url1 = adv_bkmrktype[2].replace(".", "/");
						String[] type = url1.split("/");
						for (int i = 0; i < type.length; i++) {
							if (i == type.length - 2) {
								// createAdvancedSearchTab(url,type[i]);
								new createAdvanceSearchTab(url, type[i]).execute();
								break;
							}
						}
						return true;
					} else {
						return false;
					}

				} catch (Exception e) {
					e.printStackTrace();

				}
			}
			return true;
		}
	}

	public void clickedEnrichments(Enrichments enrButton) {
		currentImage.setVisibility(View.GONE);
		currentWebView.setVisibility(View.VISIBLE);
		currentEnrichmentTabId = enrButton.getEnrichmentId();
		currentBtnEnrichment = enrButton;
		String filepath = null;
		currentWebView.getSettings().setLoadWithOverviewMode(true);
		currentWebView.getSettings().setUseWideViewPort(true);

		currentWebView.getSettings().setJavaScriptEnabled(true);
		currentWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		currentWebView.clearCache(true);
		currentWebView.setWebViewClient(new WebViewClient());
		currentWebView.getSettings().setAllowContentAccess(true);
		currentWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		currentWebView.getSettings().setAllowFileAccess(true);
		currentWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
		currentWebView.getSettings().setDomStorageEnabled(true);
		// File  file = new File(currentBookPath+currentBtnEnrichment.getEnrichmentPath());
		if (currentBtnEnrichment != null && currentBtnEnrichment.getEnrichmentType().equals(Globals.downloadedEnrichmentType)) {
			if (Globals.ISGDRIVEMODE()) {
				//if (pageNumber == currentBtnEnrichment.getEnrichmentPageNo()) {
				File file = new File(currentBtnEnrichment.getEnrichmentPath());
				filepath = "file:///" + currentBtnEnrichment.getEnrichmentPath();
				if (!file.exists()) {
					String modFilePath = currentBtnEnrichment.getEnrichmentPath().replace("/index.htm", "");
					modFilePath = modFilePath.substring(0, modFilePath.lastIndexOf("/"));
					modFilePath = modFilePath + "/index.htm";
					file = new File(modFilePath);
					filepath = "file:///" + modFilePath;
				}
			}
		} else {
			String currentBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/";
			filepath = "file:///" + currentBookPath + currentBtnEnrichment.getEnrichmentPageNo() + "-" + currentBtnEnrichment.getEnrichmentId() + ".htm";
		}
		currentWebView.loadUrl(filepath);
	}

	private void viewPopUp(View view,final int catId){
		categoryId = catId;
		treeStructureView = new TreeStructureView(currentBook, db, Integer.parseInt(currentPageNumber), popoverView, PreviewActivity.this, preview_rootvew, new TreeStructureCallBack() {
			@Override
			public void onClick(TreeNode node, Object object, PopoverView popoverView) {
				Enrichments enrichments3 = (Enrichments) object;
				if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.rename))){
					if (popoverView != null) {
						popoverView.dissmissPopover(true);
					}
					renamePopup();
				}else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.delete))){
					if (popoverView != null) {
						popoverView.dissmissPopover(true);
					}
					deletePopup();
				}else if (enrichments3.getEnrichmentId()==0 && enrichments3.getEnrichmentTitle().equals(getResources().getString(R.string.edit))){
					if (popoverView != null) {
						popoverView.dissmissPopover(true);
					}
					Intent bookViewIntent = new Intent(getApplicationContext(), BookViewActivity.class);
					bookViewIntent.putExtra("Book", currentBook);
					bookViewIntent.putExtra("currentPageNo", viewPager.getCurrentItem()+1);
					bookViewIntent.putExtra("enrichmentTabId", currentBtnEnrichment.getEnrichmentId());
					bookViewIntent.putExtra("Shelf", gridShelf);
					bookViewIntent.putExtra("createBlankEnr", "edit");
					bookViewIntent.putExtra("categoryId",catId);
					startActivityForResult(bookViewIntent, bookViewActivityrequestCode);
				}
				if (enrichments3.getEnrichmentTitle().equals("")) {
					if (popoverView != null) {
						popoverView.dissmissPopover(true);
					}
					//  treeViewStructureList(gestureView, true, enrichments1.getCategoryID());
					viewTreeList(gestureView, true, enrichments3.getCategoryID());
				}
			}
		});
		treeStructureView.treeViewStructureListNew(view,currentBtnEnrichment);
	}

}

