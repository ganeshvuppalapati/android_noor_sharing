/**
 * 
 */
package com.semanoor.fragments.viewpager;

import java.util.ArrayList;
import java.util.List;

import com.semanoor.source_sboookauthor.Globals;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * The <code>PagerAdapter</code> serves the fragments when paging.
 * @author mwho
 */
public class PagerAdapter extends FragmentPagerAdapter {

	private List<Fragment> fragments = new ArrayList<>();

	/**
	 * @param fm
	 */
	public PagerAdapter(FragmentManager fm) {
		super(fm);
	}
	/* (non-Javadoc)
     * @see android.support.v4.app.FragmentPagerAdapter#getItem(int)
     */
	@Override
	public Fragment getItem(int position) {
		Fragment fragment = this.fragments.get(position);
		Bundle args = new Bundle();
		args.putInt(Globals.STORE_TABS_SECTION_NUMBER, position);
		fragment.setArguments(args);
		return fragment;
	}

	/* (non-Javadoc)
     * @see android.support.v4.view.PagerAdapter#getCount()
     */
	@Override
	public int getCount() {
		return this.fragments.size();
	}

	public void addFragment(Fragment fragment){
		fragments.add(fragment);
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}
}
