package com.semanoor.sboookauthor_store;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.semanoor.manahij.BookView;
import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.PreviewActivity;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.HTMLObjectHolder;
import com.semanoor.source_sboookauthor.QuizSequenceObject;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.Zip;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Enrichments {
	public int enrichmentId;
	private int enrichmentBid;
	private int enrichmentPageNo;
	private int enrichmentSequenceId;
	private String enrichmentTitle;
	private String enrichmentType;
	private int enrichmentExportValue;
	private boolean enrichmentSelected;
	private Context context;
	private String enrichmentPath;
	public static String objContent;
	public static String enrichPath = "";
	private int enrichmentUserId;
	private String enrichmentUserName;
	private String enrichmentDateUp;
	private String enrichmentSize;
	private String enrichmentBookTitle;
	private String enrichmentEmailId;
	private String enrichmentDate;
	private String enrichmentPassword;
	private String enrichmentStoreBookId;
	private String enrichmentStoreId;
	private int dbBookId;
	private boolean isReadStatus;
	private boolean isDownloaded;
	public View mDrapView;
	private int categoryID;
	private ArrayList<Enrichments> subItemArray = new ArrayList<>();
	//private GestureDetector mGestureDetector;
	public Enrichments(Context _context) {
		this.context = _context;
	}
	/**
	 * @return the enrichmentId
	 */
	public int getEnrichmentId() {
		return enrichmentId;
	}
	/**
	 * @param enrichmentId the enrichmentId to set
	 */
	public void setEnrichmentId(int enrichmentId) {
		this.enrichmentId = enrichmentId;
	}
	/**
	 * @return the enrichmentBid
	 */
	public int getEnrichmentBid() {
		return enrichmentBid;
	}
	/**
	 * @param enrichmentBid the enrichmentBid to set
	 */
	public void setEnrichmentBid(int enrichmentBid) {
		this.enrichmentBid = enrichmentBid;
	}
	/**
	 * @return the enrichmentPageNo
	 */
	public int getEnrichmentPageNo() {
		return enrichmentPageNo;
	}
	/**
	 * @param enrichmentPageNo the enrichmentPageNo to set
	 */
	public void setEnrichmentPageNo(int enrichmentPageNo) {
		this.enrichmentPageNo = enrichmentPageNo;
	}
	/**
	 * @return the enrichmentTitle
	 */
	public String getEnrichmentTitle() {
		return enrichmentTitle;
	}
	/**
	 * @param enrichmentTitle the enrichmentTitle to set
	 */
	public void setEnrichmentTitle(String enrichmentTitle) {
		this.enrichmentTitle = enrichmentTitle;
	}
	
	/**
	 * @return the enrichmentType
	 */
	public String getEnrichmentType() {
		return enrichmentType;
	}
	/**
	 * @param enrichmentType the enrichmentType to set
	 */
	public void setEnrichmentType(String enrichmentType) {
		this.enrichmentType = enrichmentType;
	}
	
	/**
	 * Create New Duplicate Enrichments
	 */
	public void createNewDuplicateEnrichments() {
		//int enrichTotalNoOfTabs = context.enrichmentTabListArray.size()+1;
		//String enrichTitle = "Enrichment"+enrichTotalNoOfTabs;
		((BookViewActivity)context).db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,SequentialID,categoryID)values('"+this.enrichmentBid+"', '"+this.enrichmentPageNo+"', '"+this.enrichmentTitle+"', '"+this.enrichmentType+"', '"+this.enrichmentExportValue+"','null','"+this.enrichmentSequenceId+"','0')");
		//int maxEnrichId = context.db.getMaxUniqueRowID("enrichments");
		int maxObjUniqueId = ((BookViewActivity)context).db.getMaxUniqueRowID("objects");
		ArrayList<HTMLObjectHolder> originalStoreObjectList = ((BookViewActivity)context).db.getAllObjectsFromHomePageToCreateEnrichment(((BookViewActivity)context).currentPageNumber, ((BookViewActivity)context).currentBook.getBookID(), 0);
		for (HTMLObjectHolder htmlObjectHolder : originalStoreObjectList) {
			maxObjUniqueId = maxObjUniqueId + 1;
			String objType = htmlObjectHolder.getObjType();
			int BID = htmlObjectHolder.getObjBID();
			int objPageNo = Integer.parseInt(htmlObjectHolder.getObjPageNo());
			String objLocation = htmlObjectHolder.getObjXPos()+"|"+htmlObjectHolder.getObjYPos();
			String objSize;
			if (objType.equals(Globals.OBJTYPE_PAGEBG) || objType.equals(Globals.OBJTYPE_MINDMAPBG)) {
				objSize = htmlObjectHolder.getObjSize();
			} else {
				objSize = htmlObjectHolder.getObjWidth()+"|"+htmlObjectHolder.getObjHeight();
			}
			String objContent = htmlObjectHolder.getObjContentPath();
			int objSequentialId = htmlObjectHolder.getObjSequentialId();
			String objScalePageTofit = htmlObjectHolder.getObjScalePageToFit();
			String objLock = htmlObjectHolder.getObjLocked();
			int objUniqueId = htmlObjectHolder.getObjUniqueRowId();
			int objEnrichId = htmlObjectHolder.getObjEnrichID();
			String newObjContent = null;
			if (objType.equals("WebText") || objType.equals("TextSquare") || objType.equals("TextRectangle") || objType.equals("TextCircle") || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT) || objType.equals(Globals.objType_pageNoText) || objType.equals("WebTable") || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
				if (objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
					ArrayList<QuizSequenceObject> homeQuizSequenceObjectArray = ((BookViewActivity)context).db.getQuizSequenceArray(objUniqueId);
					for (int i = 0; i < homeQuizSequenceObjectArray.size(); i++) {
						((BookViewActivity)context).db.executeQuery("insert into tblMultiQuiz(quizId) values('"+maxObjUniqueId+"')");
					}
					ArrayList<QuizSequenceObject> enrQuizSequenceObjectArray = ((BookViewActivity)context).db.getQuizSequenceArray(maxObjUniqueId);
					for (int j = 0; j < enrQuizSequenceObjectArray.size(); j++) {
						QuizSequenceObject homeQuizSequenceObject = homeQuizSequenceObjectArray.get(j);
						int homeQuizUniqueId = homeQuizSequenceObject.getUniqueid();
						int homeQuizSectionId = homeQuizSequenceObject.getSectionId();
						QuizSequenceObject enrQuizSequenceObject = enrQuizSequenceObjectArray.get(j);
						int enrQuizUniqueId = enrQuizSequenceObject.getUniqueid();
						int enrQuizSectionId = enrQuizSequenceObject.getSectionId();
						
						if (objContent.contains("q"+homeQuizSectionId+"_img_"+homeQuizUniqueId+"")) {
							String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_"+homeQuizUniqueId+".png";
							String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+enrQuizSectionId+"_img_"+enrQuizUniqueId+".png";
							UserFunctions.copyFiles(homeImgContent, enrImgContent);
						}
						if (objContent.contains("q"+homeQuizSectionId+"_img_a_"+homeQuizUniqueId+"")) {
							String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_a_"+homeQuizUniqueId+".png";
							String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+enrQuizSectionId+"_img_a_"+enrQuizUniqueId+".png";
							UserFunctions.copyFiles(homeImgContent, enrImgContent);
						}
						if (objContent.contains("q"+homeQuizSectionId+"_img_b_"+homeQuizUniqueId+"")) {
							String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_b_"+homeQuizUniqueId+".png";
							String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+enrQuizSectionId+"_img_b_"+enrQuizUniqueId+".png";
							UserFunctions.copyFiles(homeImgContent, enrImgContent);
						}
						if (objContent.contains("q"+homeQuizSectionId+"_img_c_"+homeQuizUniqueId+"")) {
							String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_c_"+homeQuizUniqueId+".png";
							String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+enrQuizSectionId+"_img_c_"+enrQuizUniqueId+".png";
							UserFunctions.copyFiles(homeImgContent, enrImgContent);
						}
						if (objContent.contains("q"+homeQuizSectionId+"_img_d_"+homeQuizUniqueId+"")) {
							String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_d_"+homeQuizUniqueId+".png";
							String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+enrQuizSectionId+"_img_d_"+enrQuizUniqueId+".png";
							UserFunctions.copyFiles(homeImgContent, enrImgContent);
						}
						if (objContent.contains("q"+homeQuizSectionId+"_img_e_"+homeQuizUniqueId+"")) {
							String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_e_"+homeQuizUniqueId+".png";
							String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+enrQuizSectionId+"_img_e_"+enrQuizUniqueId+".png";
							UserFunctions.copyFiles(homeImgContent, enrImgContent);
						}
						if (objContent.contains("q"+homeQuizSectionId+"_img_f_"+homeQuizUniqueId+"")) {
							String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+homeQuizSectionId+"_img_f_"+homeQuizUniqueId+".png";
							String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/q"+enrQuizSectionId+"_img_f_"+enrQuizUniqueId+".png";
							UserFunctions.copyFiles(homeImgContent, enrImgContent);
						}
						
						objContent = objContent.replace("previousquestion:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "previousquestion:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
						objContent = objContent.replace("deletequestion:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "deletequestion:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
						objContent = objContent.replace("addquestions:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "addquestions:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
						objContent = objContent.replace("nextquestion:q"+homeQuizSectionId+"_"+homeQuizUniqueId+":"+homeQuizSectionId+"", "nextquestion:q"+enrQuizSectionId+"_"+enrQuizUniqueId+":"+enrQuizSectionId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_"+enrQuizUniqueId+"");
						objContent = objContent.replace("Title"+homeQuizSectionId+"", "Title"+enrQuizSectionId+"");
						objContent = objContent.replace("quizTitle_"+homeQuizUniqueId+"", "quizTitle_"+enrQuizUniqueId+"");
						objContent = objContent.replace("pTitle_"+homeQuizUniqueId+"", "pTitle_"+enrQuizUniqueId+"");
						objContent = objContent.replace("quiz_question_"+homeQuizUniqueId+"", "quiz_question_"+enrQuizUniqueId+"");
						objContent = objContent.replace("pQuiz_"+homeQuizUniqueId+"", "pQuiz_"+enrQuizUniqueId+"");
						objContent = objContent.replace("quiz_options_"+homeQuizUniqueId+"", "quiz_options_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_result_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_result_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_div_a_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_a_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"a_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"a_"+enrQuizUniqueId+"");
						objContent = objContent.replace("btnCheckAnswer"+homeQuizSectionId+"_"+homeQuizUniqueId+"", "btnCheckAnswer"+enrQuizSectionId+"_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_div_b_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_b_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"b_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"b_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_div_c_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_c_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"c_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"c_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_div_d_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_d_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"d_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"d_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_div_e_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_e_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"e_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"e_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_div_f_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_div_f_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"f_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"f_"+enrQuizUniqueId+"");
						objContent = objContent.replace("quiz_checkanswer_"+homeQuizUniqueId+"", "quiz_checkanswer_"+enrQuizUniqueId+"");
						objContent = objContent.replace("btnNext"+homeQuizSectionId+"_"+homeQuizUniqueId+"", "btnNext"+enrQuizSectionId+"_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_img_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_img_a_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_a_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_img_b_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_b_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_img_c_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_c_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_img_d_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_d_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_img_e_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_e_"+enrQuizUniqueId+"");
						objContent = objContent.replace("q"+homeQuizSectionId+"_img_f_"+homeQuizUniqueId+"", "q"+enrQuizSectionId+"_img_f_"+enrQuizUniqueId+"");
						objContent = objContent.replace("results_container_"+homeQuizUniqueId+"", "results_container_"+enrQuizUniqueId+"");
						objContent = objContent.replace("results_"+homeQuizUniqueId+"", "results_"+enrQuizUniqueId+"");
						objContent = objContent.replace("quiz_result_cont_"+homeQuizUniqueId+"", "quiz_result_cont_"+enrQuizUniqueId+"");
						
						newObjContent = objContent.replace("'", "''");
					}
				} else {
					newObjContent = objContent.replace("'", "''");
				}
			} else if (objType.equals("Image") || objType.equals("DrawnImage")) {
				newObjContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/"+maxObjUniqueId+".png";
				UserFunctions.copyFiles(objContent, newObjContent);
			} else if (objType.equals("EmbedCode")) {
				newObjContent = objContent.replace("'", "''");
			} else if (objType.equals("Audio")) {
				newObjContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/"+maxObjUniqueId+".m4a";
				UserFunctions.copyFiles(objContent, newObjContent);
			} else if (objType.equals("IFrame")) {
				newObjContent = objContent;
			} else if (objType.equals("YouTube")) {
				if (objContent.contains("youtube.com")) {
					newObjContent = objContent;
				} else {
					newObjContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/"+maxObjUniqueId+".mp4";
					UserFunctions.copyFiles(objContent, newObjContent);
					String enrImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/"+maxObjUniqueId+".png";
					String homeImgContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/images/"+objUniqueId+".png";
					UserFunctions.copyFiles(homeImgContent, enrImgContent);
				}
			} else if (objType.equals(Globals.OBJTYPE_MINDMAPBG) || objType.equals(Globals.OBJTYPE_MINDMAPBG)) {
				newObjContent = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/FreeFiles/pageBG_"+objPageNo+"-"+this.enrichmentId+".png";
				UserFunctions.copyFiles(objContent, newObjContent);
			}
			((BookViewActivity)context).db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+objType+"', '"+BID+"', '"+objPageNo+"', '"+objLocation+"', '"+objSize+"', '"+newObjContent+"', '"+objSequentialId+"', '"+objScalePageTofit+"', '"+objLock+"', '"+this.enrichmentId+"')");
		}
		//createEnrichmentEntryInBookXml();
	}
	
	/**
	 * Create Blank Enrichment Page
	 */
	public void createBlankEnrichments(int catId) {
		((BookViewActivity)context).db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,categoryID)values('"+this.enrichmentBid+"', '"+this.enrichmentPageNo+"', '"+this.enrichmentTitle+"', '"+this.enrichmentType+"', '"+this.enrichmentExportValue+"','null','"+catId+"')");
		//createEnrichmentEntryInBookXml();
	}

	
	/**
	 * creating an enrichment entry in book.xml file
	 */
	public void createEnrichmentEntryInBookXml(){
		String bookXmlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+((BookViewActivity)context).currentBook.get_bStoreID()+"Book/"+"Book.xml";
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

			Document document = documentBuilder.parse(new File(bookXmlPath));
			String tagName;
			if (((BookViewActivity)context).currentBook.isStoreBookCreatedFromTab()) {
				if (this.enrichmentPageNo == 1) {
					tagName = "Cover1";
				} else if (this.enrichmentPageNo == ((BookViewActivity)context).currentBook.getTotalPages()) {
					tagName = "Cover2";
				} else {
					tagName = "P"+(this.enrichmentPageNo - 1);
				}
			} else {
				tagName = "P"+(this.enrichmentPageNo - 1);
			}
			Node pageNode = document.getElementsByTagName(tagName).item(0);
			//NodeList nodes = pageNode.getChildNodes();
            
		    //create new and update elements :
			String eleName = "Enr"+"1";
			String elePath = "../"+this.enrichmentBid+"/"+this.enrichmentPageNo+"-"+this.enrichmentSequenceId+".htm";
			String eleTitle = this.enrichmentTitle;
			String eleID = String.valueOf(this.enrichmentId);
			String eleType = this.enrichmentType;
			Element enrElement = document.createElement(eleName);
			enrElement.setAttribute("Path", elePath);
			enrElement.setAttribute("Title", eleTitle);
			enrElement.setAttribute("ID", eleID);
			enrElement.setAttribute("Type", eleType);
			pageNode.appendChild(enrElement);

			// write the DOM object to the file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);

			StreamResult streamResult = new StreamResult(new File(bookXmlPath));
			transformer.transform(domSource, streamResult);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();	        
		} catch (TransformerException tfe) {
			tfe.printStackTrace();	        
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	/**
	 * Create advsearchEnrichment tabs for the current page in readmode
	 */
	public Button createSearchEnrichmentTabs() {
		RelativeLayout layout=new RelativeLayout(context);
		layout.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));

		final Button btnEnrichmentTab = new Button(context);
		btnEnrichmentTab.setId(R.id.btnEnrichmentTab);
		btnEnrichmentTab.setTag(this.enrichmentId);
		btnEnrichmentTab.setTextSize(TypedValue.COMPLEX_UNIT_PX,(int)context. getResources().getDimension(R.dimen.enrichments_tex_size));
		btnEnrichmentTab.setTextColor(Color.WHITE);
		String text=this.enrichmentTitle;
		/*if(text.length()>9){
			String subString=text.substring(0,8);
			String t=subString+"....";
			//System.out.println(t+"text");
			btnEnrichmentTab.setText(t);
		}else{
			btnEnrichmentTab.setText(this.enrichmentTitle);
		}*/
		btnEnrichmentTab.setText(this.enrichmentTitle);
		btnEnrichmentTab.setEllipsize(TextUtils.TruncateAt.END);
		btnEnrichmentTab.setSingleLine(true);
		btnEnrichmentTab.setBackgroundResource(R.drawable.btn_enrich_tab);
		String  Path = Globals.TARGET_BASE_BOOKS_DIR_PATH+BookView.bookName+"/";
		((BookView)context).new lvimage(btnEnrichmentTab,0).execute("http://www.google.com/s2/favicons?domain="+enrichmentPath);
		btnEnrichmentTab.setGravity(Gravity.CENTER);  
		layout.addView(btnEnrichmentTab);

		RelativeLayout.LayoutParams   lp = new RelativeLayout.LayoutParams((int)context.getResources().getDimension(R.dimen.bookview_searchTab_remove),(int)context.getResources().getDimension(R.dimen.bookview_searchTab_height));
		Button btnEnrichDelete = new Button(context);
		btnEnrichDelete.setId(R.id.btnEnrichmentTab);
		btnEnrichDelete.setTag(this.enrichmentId);
		btnEnrichDelete.setBackgroundResource(R.drawable.btn_enr_del);
		lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		lp.addRule(RelativeLayout.ALIGN_BASELINE,btnEnrichmentTab.getId());
		lp.addRule(RelativeLayout.CENTER_VERTICAL,btnEnrichmentTab.getId());
		lp.setMargins(0, 0, (int)context.getResources().getDimension(R.dimen.btn_remove),0); //30
		btnEnrichDelete.setLayoutParams(lp);
		layout.addView(btnEnrichDelete);
		
		btnEnrichmentTab.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				BookView.adv_search.setVisibility(View.INVISIBLE);
				((BookView)context).btnSearchEnrichmentClicked(v);
				if(enrichmentType.equals("Search")){
					((BookView)context).progressbar.setVisibility(View.VISIBLE);
					BookView.adv_search.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
					BookView.adv_search.clearCache(true);
					BookView.adv_search.clearHistory();
					BookView.type=enrichmentType;
					BookView.tabNo=enrichmentId;
					BookView.displayBookMark(enrichmentPageNo,enrichmentId);
					BookView.webView.setVisibility(View.INVISIBLE);
					BookView.adv_search.setVisibility(View.VISIBLE);
					((BookView)context).loadAdvancedSearchWebView(enrichmentPath,enrichmentType);
				}else{
					BookView.type=enrichmentType;
					BookView.adv_search.setVisibility(View.INVISIBLE);
					BookView.webView.setVisibility(View.VISIBLE);
				}
			}
		});
		btnEnrichmentTab.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				int id=(Integer)v.getTag();

				for(int i=0;i<((BookView)context).currentEnrichmentList.size();i++){
					((BookView)context).currentEnrichment = ((BookView)context).currentEnrichmentList.get(i);
					if(id==((BookView)context).currentEnrichment.getEnrichmentId() && enrichmentType.equals("Search")){
						((BookView)context).showAdvsearchEnrichDialog((Button) v);
						break;
					}
				}

				return false;
			}
		});
		
		 btnEnrichDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int id=(Integer)v.getTag();
				BookView.tabNo=id;
				BookView.removeBookMark();
				((BookView)context).deletingAdvsearchEnrichments(v,id);
				 
			 }
		});
		 
		((BookView)context).currentEnrTabsLayout.addView(layout, new RelativeLayout.LayoutParams((int)context.getResources().getDimension(R.dimen.enrichment_btn_width), (int)context.getResources().getDimension(R.dimen.enrichment_btn_height)));
		//((BookView)context).currentEnrTabsLayout.addView(layout, new LayoutParams((int) context.getResources().getDimension(R.dimen.btn_enrichment_width), LayoutParams.MATCH_PARENT));
		return btnEnrichmentTab;
	}
	
   /**
	 * Create Enrichment tabs in editmode(SBA)
	 */
	public Button createEnrichmentTabs() {
		final RelativeLayout layout=new RelativeLayout(context);
		layout.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));

		final Button btnEnrichmentTab = new Button(context);
		btnEnrichmentTab.setId(R.id.btnEnrichmentTab);
		btnEnrichmentTab.setTag(this);
		/*String text=this.enrichmentTitle;
		if(enrichmentType.equals("Search")){
			if(text.length()>9){
				String subString=text.substring(0,8);
				String t=subString+"....";
				//System.out.println(t+"text");
				btnEnrichmentTab.setText(t);
			}else{
				btnEnrichmentTab.setText(this.enrichmentTitle);
			}
		}else{
			btnEnrichmentTab.setText(this.enrichmentTitle);
		}*/
		btnEnrichmentTab.setText(this.enrichmentTitle);
		btnEnrichmentTab.setSingleLine(true);
		btnEnrichmentTab.setEllipsize(TextUtils.TruncateAt.END);
		btnEnrichmentTab.setTextSize(TypedValue.COMPLEX_UNIT_PX,(int)context. getResources().getDimension(R.dimen.enrichments_tex_size));
		btnEnrichmentTab.setTextColor(Color.WHITE);
		btnEnrichmentTab.setBackgroundResource(R.drawable.btn_enrich_tab);

		if(enrichmentType.equals("Search")){
			((BookViewActivity)context).new image(btnEnrichmentTab,0).execute("http://www.google.com/s2/favicons?domain="+this.enrichmentPath);
		}
		btnEnrichmentTab.setGravity(Gravity.CENTER);  
		layout.addView(btnEnrichmentTab);

		if(enrichmentType.equals("Search")){
			RelativeLayout.LayoutParams   lp = new RelativeLayout.LayoutParams((int)context.getResources().getDimension(R.dimen.bookview_searchTab_remove),(int)context.getResources().getDimension(R.dimen.bookview_searchTab_height));
			Button btnEnrichDelete = new Button(context);
			btnEnrichDelete.setId(R.id.btnEnrichmentTab);
			btnEnrichDelete.setTag(this);
			btnEnrichDelete.setBackgroundResource(R.drawable.btn_enr_del);
			lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			lp.addRule(RelativeLayout.ALIGN_BASELINE, btnEnrichmentTab.getId());
			lp.addRule(RelativeLayout.CENTER_VERTICAL, btnEnrichmentTab.getId());
			lp.setMargins(0, 0, 10, 0); //30
			// 
			btnEnrichDelete.setLayoutParams(lp);
			layout.addView(btnEnrichDelete);
			btnEnrichDelete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Enrichments id = (Enrichments) btnEnrichmentTab.getTag();

					for (int j = 0; j < ((BookViewActivity) context).enrTabsLayout.getChildCount(); j++) {
						if (j == 0) {
							Button btnEnr = (Button) ((BookViewActivity) context).enrTabsLayout.getChildAt(j);
						} else {
							RelativeLayout rlview = (RelativeLayout) ((BookViewActivity) context).enrTabsLayout.getChildAt(j);
							Button btnEnrTab = (Button) rlview.getChildAt(0);
							final Enrichments enrich = (Enrichments) btnEnrTab.getTag();

							if (enrich.getEnrichmentId() == id.getEnrichmentId()) {
								((BookViewActivity) context).enrTabsLayout.removeView(rlview);
							}
						}
					}
					// ((BookViewActivity)context).enrTabsLayout.removeView(id.getEnrichmentSequenceId());
					String deleteEnrQuery = "delete from enrichments where enrichmentID = '" + id.getEnrichmentId() + "'";
					((BookViewActivity) context).db.executeQuery(deleteEnrQuery);
					int enrichid = id.getEnrichmentId();
					for (Enrichments enrichments : ((BookViewActivity) context).enrichmentTabListArray) {
						if (enrichments.getEnrichmentId() == id.getEnrichmentId()) {
							((BookViewActivity) context).enrichmentTabListArray.remove(enrichments);
							break;
						}
					}
					int tagValue = 0;
					if (((BookViewActivity) context).enrTabsLayout.getChildCount() == 1) {
						((BookViewActivity) context).rlTabsLayout.setVisibility(View.GONE);
						((BookViewActivity) context).wv_advSearch.setVisibility(View.INVISIBLE);
						((BookViewActivity) context).bgImgView.setVisibility(View.VISIBLE);
						((BookViewActivity) context).designScrollPageView.setVisibility(View.VISIBLE);
						tagValue = 0;
					} else {
						for (int j = 0; j < ((BookViewActivity) context).enrTabsLayout.getChildCount(); j++) {
							if (j == 0) {
								Button btnEnr = (Button) ((BookViewActivity) context).enrTabsLayout.getChildAt(j);
							} else {
								if (j == ((BookViewActivity) context).enrTabsLayout.getChildCount() - 1) {
									RelativeLayout rlview = (RelativeLayout) ((BookViewActivity) context).enrTabsLayout.getChildAt(((BookViewActivity) context).enrTabsLayout.getChildCount() - 1);
									Button btnEnrTab = (Button) rlview.getChildAt(0);
									final Enrichments enrich = (Enrichments) btnEnrTab.getTag();
									((BookViewActivity) context).makeEnrichedBtnSlected(btnEnrTab);
									tagValue = enrich.enrichmentId;

									if (enrich.getEnrichmentType().equals("Search")) {
										((BookViewActivity) context).wv_advSearch.setVisibility(View.VISIBLE);
										((BookViewActivity) context).wv_advSearch.setWebViewClient(new WebViewClient());
										if (enrich.getEnrichmentTitle().equals("BookPage")){
											String[] split = enrich.getEnrichmentPath().split("/");
											String filePath = "file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+((BookViewActivity) context).currentBook.get_bStoreID()+"Book/";
											File hyperPage = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+((BookViewActivity) context).currentBook.get_bStoreID()+"Book/"+split[split.length-1]);
											String srcString = UserFunctions.decryptFile(hyperPage);
											((BookViewActivity) context).wv_advSearch.loadDataWithBaseURL(filePath, srcString, "text/html", "utf-8", null);
										}else{
											((BookViewActivity) context).wv_advSearch.loadUrl(enrich.getEnrichmentPath());
										}
										((BookViewActivity) context).bgImgView.setVisibility(View.INVISIBLE);
										((BookViewActivity) context).designScrollPageView.setVisibility(View.INVISIBLE);

									} else if (enrich.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
										((BookViewActivity) context).designScrollPageView.setVisibility(View.INVISIBLE);
										((BookViewActivity) context).wv_advSearch.setVisibility(View.INVISIBLE);
										((BookViewActivity) context).bgImgView.setVisibility(View.INVISIBLE);

									} else {
										((BookViewActivity) context).designScrollPageView.setVisibility(View.VISIBLE);
										((BookViewActivity) context).wv_advSearch.setVisibility(View.INVISIBLE);
										((BookViewActivity) context).bgImgView.setVisibility(View.VISIBLE);
									}
								}
							}
						}
					}
					((BookViewActivity) context).checkForEnrichments(Integer.parseInt(((BookViewActivity) context).currentPageNumber), tagValue);
					((BookViewActivity) context).instantiateNewPage(Integer.parseInt(((BookViewActivity) context).currentPageNumber), tagValue);
					int enrTabCount = ((BookViewActivity) context).enrichTabCountListAllPages.get(Integer.parseInt(((BookViewActivity) context).currentPageNumber) - 1);
					enrTabCount = enrTabCount - 1;
					if (((BookViewActivity) context).currentBook.is_bStoreBook()) {
						deleteEnrichmentEntryInBookXml(enrichid);
					}
					((BookViewActivity) context).enrichTabCountListAllPages.set(Integer.parseInt(((BookViewActivity) context).currentPageNumber) - 1, enrTabCount);
					((BookViewActivity) context).pageListView.invalidateViews();
				}
			});
	     }
	     btnEnrichmentTab.setOnClickListener(((BookViewActivity)context));
		 btnEnrichmentTab.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				if(enrichmentType.equals("Search")){
					//showAdvsearchEnrichDialog((Button)v);
				}else{
					//showEnrichDialog((Button) v);
				}
				return false;
			}
		});

		((BookViewActivity)context).enrTabsLayout.addView(layout, new RelativeLayout.LayoutParams((int)context.getResources().getDimension(R.dimen.enrichment_btn_width), (int)context.getResources().getDimension(R.dimen.enrichment_btn_height)));
		return btnEnrichmentTab;
	}



	/**
	 * Deleting and saving Duplicate and blank enrichments (editmode)
	 */
	public void showEnrichDialog(final Enrichments btnEnr){
		final Dialog enrichDialog = new Dialog(context);
		enrichDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.semi_transparent)));
		enrichDialog.setTitle(R.string.edit_enrichment);
		enrichDialog.setContentView(((BookViewActivity)context).getLayoutInflater().inflate(R.layout.enrichment_edit, null));
		enrichDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, context), LayoutParams.WRAP_CONTENT);
		final EditText editText = (EditText) enrichDialog.findViewById(R.id.enr_editText);
		editText.setText(enrichmentTitle);
		Button btnSave = (Button) enrichDialog.findViewById(R.id.enr_save);
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String enrTitle = editText.getText().toString();
				btnEnr.setEnrichmentTitle(enrTitle);
				setEnrichmentTitle(enrTitle);
				((BookViewActivity)context).db.executeQuery("update enrichments set Title='"+enrichmentTitle+"'where enrichmentID='"+enrichmentId+"'and Type='"+enrichmentType+"' ");
				updateEnrichmentEntryInBookXml(enrichmentId);
				((BookViewActivity)context).recyclerView.getAdapter().notifyDataSetChanged();
				enrichDialog.dismiss();
				
			}
		});
		
		Button btnDelete = (Button) enrichDialog.findViewById(R.id.enr_delete);
		btnDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Enrichments id=(Enrichments) btnEnr.getTag();
				Enrichments id=btnEnr;
				/*for (int j = 0; j <((BookViewActivity)context).enrTabsLayout.getChildCount(); j++) {
					if(j==0){
						Button btnEnr = (Button) ((BookViewActivity)context).enrTabsLayout.getChildAt(j);
					}else{               
						RelativeLayout rlview = (RelativeLayout) ((BookViewActivity)context).enrTabsLayout.getChildAt(j);
						Button btnEnrTab = (Button) rlview.getChildAt(0);
						final Enrichments enrich=(Enrichments) btnEnrTab.getTag();

						if(enrich.getEnrichmentId()==id.getEnrichmentId()){
							((BookViewActivity)context).enrTabsLayout.removeView(rlview);
						}
					}
				} */

				String deleteEnrQuery = "delete from enrichments where enrichmentID = '"+id.getEnrichmentId()+"'";
				((BookViewActivity)context).db.executeQuery(deleteEnrQuery);
				((BookViewActivity)context).db.executeQuery("delete from objects where BID='"+((BookViewActivity)context).currentBook.getBookID()+"' and pageNO='"+((BookViewActivity)context).currentPageNumber+"' and EnrichedID='"+enrichmentId+"'");
				ArrayList<Object> objectList = ((BookViewActivity)context).db.getAllObjectsForGeneratingHTML(((BookViewActivity)context).currentPageNumber, ((BookViewActivity)context).currentBook.getBookID(), enrichmentId);
				for (int i = 0; i < objectList.size(); i++) {
					HTMLObjectHolder htmlObject = (HTMLObjectHolder) objectList.get(i);
					if (htmlObject.getObjContentPath().contains("data/data")) {
						String path = htmlObject.getObjContentPath();

						if (path != null) {
							File file = new File(path);
							if(file.exists()){
								file.delete();
							}
						}
					}
				}
				int enrichid=enrichmentId;
				/*for (Enrichments enrichments : ((BookViewActivity)context).enrichmentTabListArray) {
					if (enrichments.getEnrichmentId() == enrichid) {
						((BookViewActivity)context).enrichmentTabListArray.remove(enrichments);
						break;
					}

				} */
				((BookViewActivity)context).enrichmentTabListArray.remove(id);
				int tagValue=0;
				/*if (((BookViewActivity)context).enrTabsLayout.getChildCount() == 1) {
					((BookViewActivity)context).rlTabsLayout.setVisibility(View.GONE);
					tagValue = 0;
				} else {*/
					for (int j = 0; j <((BookViewActivity)context).enrichmentTabListArray.size(); j++) {
					      	Enrichments enrich=((BookViewActivity)context).enrichmentTabListArray.get(j);
						//if(enrich.isEnrichmentSelected()) {
							enrich.setEnrichmentSelected(false);
					//	}
						/*if(j==0){
							Button btnEnr = (Button) ((BookViewActivity)context).enrTabsLayout.getChildAt(j);
						}else{  */
							if(j==((BookViewActivity)context).enrichmentTabListArray.size()-1){
								//RelativeLayout rlview = (RelativeLayout) ((BookViewActivity)context).enrTabsLayout.getChildAt(((BookViewActivity)context).enrTabsLayout.getChildCount()-1);
								//Button btnEnrTab = (Button) rlview.getChildAt(0);
								// Enrichments btnEnrTab=(Enrichments) btnEnrTab.getTag();
								//((BookViewActivity)context).makeEnrichedBtnSlected(enrich);
								tagValue=enrich.enrichmentId;
								enrich.setEnrichmentSelected(true);
								if(enrich.getEnrichmentType().equals("Search")){
									((BookViewActivity)context).wv_advSearch.setVisibility(View.VISIBLE);
									((BookViewActivity)context).wv_advSearch.setWebViewClient(new WebViewClient());
									if (enrich.getEnrichmentTitle().equals("BookPage")){
										String[] split = enrich.getEnrichmentPath().split("/");
										String filePath = "file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+((BookViewActivity)context).currentBook.get_bStoreID()+"Book/";
										File hyperPage = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+((BookViewActivity)context).currentBook.get_bStoreID()+"Book/"+split[split.length-1]);
										String srcString = UserFunctions.decryptFile(hyperPage);
										((BookViewActivity)context).wv_advSearch.loadDataWithBaseURL(filePath, srcString, "text/html", "utf-8", null);
									}else{
										((BookViewActivity)context).wv_advSearch.loadUrl(enrich.getEnrichmentPath());
									}
									((BookViewActivity)context).bgImgView.setVisibility(View.INVISIBLE);

								} else if (enrich.getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
									((BookViewActivity) context).designScrollPageView.setVisibility(View.INVISIBLE);
									((BookViewActivity) context).wv_advSearch.setVisibility(View.INVISIBLE);
									((BookViewActivity) context).bgImgView.setVisibility(View.INVISIBLE);
								}
								else{
									((BookViewActivity)context).wv_advSearch.setVisibility(View.INVISIBLE);
									((BookViewActivity)context).bgImgView.setVisibility(View.VISIBLE);
								}
								break;
							}

						//}
					}

				//}
				//((BookViewActivity)context).checkForEnrichments(Integer.parseInt(((BookViewActivity)context).currentPageNumber), tagValue);
				((BookViewActivity)context).instantiateNewPage(Integer.parseInt(((BookViewActivity)context).currentPageNumber), tagValue);
				enrichDialog.dismiss();
				int enrTabCount = ((BookViewActivity)context).enrichTabCountListAllPages.get(Integer.parseInt(((BookViewActivity)context).currentPageNumber) - 1);
				enrTabCount = enrTabCount - 1;
				deleteEnrichmentEntryInBookXml(enrichid);

				((BookViewActivity)context).enrichTabCountListAllPages.set(Integer.parseInt(((BookViewActivity)context).currentPageNumber) - 1, enrTabCount);
				((BookViewActivity)context).pageListView.invalidateViews();
				((BookViewActivity)context).recyclerView.getAdapter().notifyDataSetChanged();
				if(((BookViewActivity)context).enrichmentTabListArray.size()>1) {
					((BookViewActivity) context).rlTabsLayout.setVisibility(View.VISIBLE);
				}else{
					((BookViewActivity) context).rlTabsLayout.setVisibility(View.INVISIBLE);
				}
			}
		});
		enrichDialog.show();
	}
	
	
	
	/**
	 * deleting duplicate and blankEnrichment tabs
	 * @param enrichid
	 */
	public void deleteEnrichmentEntryInBookXml(int enrichid) {
		String bookXmlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+((BookViewActivity)context).currentBook.get_bStoreID()+"Book/"+"Book.xml";
		String enrichvalue=String.valueOf(enrichid);
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

			Document document = documentBuilder.parse(new File(bookXmlPath));
			String tagName = "P"+(this.enrichmentPageNo - 1);
			Node pageNode = document.getElementsByTagName(tagName).item(0);
			NodeList nodes = pageNode.getChildNodes();
			boolean deleted = false;
		    for (int i = 0; i < nodes.getLength(); i++) {
				Node element = nodes.item(i);
				NamedNodeMap attribute = element.getAttributes();
				Node nodeAttr = attribute.getNamedItem("ID");
				Node nodepath = attribute.getNamedItem("Path");
				Node nodetype = attribute.getNamedItem("Type");
				String path = nodepath.getNodeValue();
				String[] split = path.split("/");
				//System.out.println(split[2]+"split");
				if (split.length > 1) {
					String htmlpath = split[2];

					if (deleted == true) {
						if (nodetype.getNodeValue().equals("duplicate") || nodetype.getNodeValue().equals("blank")) {
							String elePath;
							String[] splits = htmlpath.split("-");
							String p = splits[1].replace(".htm", "");
							int pageno = Integer.parseInt(p) - 1;
							elePath = splits[0] + "-" + String.valueOf(pageno) + ".htm";
							String html = split[0] + "/" + split[1] + "/" + elePath;
							nodepath.setNodeValue(html);
						}
					}

					if (nodeAttr.getNodeValue().equals(enrichvalue)) {
						//System.out.println("nodevalue entered");
						deleted = true;
						pageNode.removeChild(element);

					}
					String htmlFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ((BookViewActivity) context).currentBook.getBookID() + "/" + htmlpath;
					//System.out.println(htmlFilePath+"path");
					File file = new File(htmlFilePath);
					if (file.exists()) {
						file.delete();
					}
				}
			}
		        // write the DOM object to the file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource domSource = new DOMSource(document);
		
				StreamResult streamResult = new StreamResult(new File(bookXmlPath));
				transformer.transform(domSource, streamResult);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();	        
		} catch (TransformerException tfe) {
			tfe.printStackTrace();	        
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	
	}
	
	
	public void showAdvsearchEnrichDialog(final Enrichments  btnEnr){
		final Dialog enrichDialog = new Dialog(context);
		enrichDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.semi_transparent)));
		enrichDialog.setTitle(R.string.advsearch_bookmark);
		enrichDialog.setContentView(((BookViewActivity)context).getLayoutInflater().inflate(R.layout.advsearch_bkmark_dialog, null));
		enrichDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, context), LayoutParams.WRAP_CONTENT);
		final EditText editText = (EditText) enrichDialog.findViewById(R.id.enr_editText);
		editText.setText(enrichmentTitle);
        Button btnSave = (Button) enrichDialog.findViewById(R.id.enr_save);
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String enrTitle = editText.getText().toString();
				enrTitle = enrTitle.replace("'", "''");
				((BookViewActivity)context).bkmarkLayout.setVisibility(View.VISIBLE);
				((BookViewActivity)context).creatingBookmarkButton(enrTitle,btnEnr);
				 enrichDialog.dismiss();
			}
		});
		enrichDialog.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				// TODO Auto-generated method stub
				if (((BookViewActivity)context).bookmark_container.getWidth()>((BookViewActivity)context).bkmarkLayout.getWidth()) {
					   int  enrTabsWidth1 = ((BookViewActivity)context).bookmark_container.getWidth();
					   ((BookViewActivity)context).bookmrk_sv.smoothScrollTo(enrTabsWidth1, 0);
				   }
			}
		});
		enrichDialog.show();
		
	}
	/**
	 * Updating SBAEnrichmentTitle in book.xml
	 * @param enrichid
	 */
	private void updateEnrichmentEntryInBookXml(int enrichid) {
		// TODO Auto-generated method stub
		String bookXmlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+((BookViewActivity)context).currentBook.get_bStoreID()+"Book/"+"Book.xml";
		String enrichvalue=String.valueOf(enrichid);
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

			Document document = documentBuilder.parse(new File(bookXmlPath));
			String tagName = "P"+(this.enrichmentPageNo - 1);
			Node pageNode = document.getElementsByTagName(tagName).item(0);
			NodeList nodes = pageNode.getChildNodes();


			for (int i = 0; i < nodes.getLength(); i++) {       
                Node element = nodes.item(i);
                NamedNodeMap attribute = element.getAttributes();
	            Node nodeAttr = attribute.getNamedItem("ID");
	            Node enrAttr = attribute.getNamedItem("Title");
	            Node nodetype = attribute.getNamedItem("Type");
	            
	            if(nodeAttr.getNodeValue().equals(enrichvalue)){
	                    //System.out.println("nodevalue entered");
	                    enrAttr.setNodeValue(this.enrichmentTitle+"");
	            	
	              }  
	          }
			// write the DOM object to the file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);
		
			StreamResult streamResult = new StreamResult(new File(bookXmlPath));
			transformer.transform(domSource, streamResult);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();	        
		} catch (TransformerException tfe) {
			tfe.printStackTrace();	        
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
	
	/**
	 * Deleting advsearchEnrichments in book.xml
	 */
	public void deleteAdvSearchEnrichmentInBookXml(int pageno,String enrichid,String  type,String bookXmlPath) {
		// TODO Auto-generated method stub
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

			Document document = documentBuilder.parse(new File(bookXmlPath));
			String tagName = "P"+(pageno - 1);
			Node pageNode = document.getElementsByTagName(tagName).item(0);
			NodeList nodes = pageNode.getChildNodes();
			//boolean deleted = false;
		   for (int i = 0; i < nodes.getLength(); i++) {       
                    Node element = nodes.item(i);
	                NamedNodeMap attribute = element.getAttributes();
		            Node nodeAttr = attribute.getNamedItem("ID");
		            Node nodepath = attribute.getNamedItem("Path");
		            Node nodetype = attribute.getNamedItem("Type");
		            String path=nodepath.getNodeValue();
		            String[] split = path.split("/");
	            	////System.out.println(split[2]+"split");
	            	//String htmlpath=split[2];
                   
                    if(nodeAttr.getNodeValue().equals(enrichid) && nodetype.getNodeValue().equals(type)){
    	                    //System.out.println("nodevalue entered");
		            	    pageNode.removeChild(element);
    	              }
		          }
		        // write the DOM object to the file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource domSource = new DOMSource(document);
		
				StreamResult streamResult = new StreamResult(new File(bookXmlPath));
				transformer.transform(domSource, streamResult);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();	        
		} catch (TransformerException tfe) {
			tfe.printStackTrace();	        
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	
	}
/*	public Button createEnrichmentTabsForPreview(final PreviewActivity ctxPreview) {
		final Button btnEnrichmentTab = new Button(ctxPreview);
		btnEnrichmentTab.setId(R.id.btnEnrichmentTab);
		btnEnrichmentTab.setTag(this.enrichmentId);
		btnEnrichmentTab.setText(this.enrichmentTitle);
		btnEnrichmentTab.setEllipsize(TextUtils.TruncateAt.END);
		btnEnrichmentTab.setSingleLine(true);
		btnEnrichmentTab.setTextColor(Color.WHITE);
		btnEnrichmentTab.setBackgroundResource(R.drawable.btn_enrich_tab);
		btnEnrichmentTab.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ctxPreview.currentImage.setVisibility(View.GONE);
				ctxPreview.currentWebView.setVisibility(View.VISIBLE);
				ctxPreview.makeEnrichedBtnSlected(btnEnrichmentTab);
				ctxPreview.loadEnricmentPage(getEnrichmentId(), enrichmentType,enrichmentPath);
			}
		});
		ctxPreview.enrTabsLayout.addView(btnEnrichmentTab, new RelativeLayout.LayoutParams((int)context.getResources().getDimension(R.dimen.enrichment_btn_width), (int)context.getResources().getDimension(R.dimen.enrichment_btn_height)));
		return btnEnrichmentTab;
	}*/
	
	/**
	 * Adding AdvsearchEnrichments into book.xml
	 * @param bookXmlPath
	 */
	public void UpdateAdvSearchintoBookXml(String bookXmlPath){ 
		
			// TODO Auto-generated method stub
				try {
					DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

					Document document = documentBuilder.parse(new File(bookXmlPath));
					String tagName = "P"+(this.enrichmentPageNo - 1);
					Node pageNode = document.getElementsByTagName(tagName).item(0);
					NodeList nodes = pageNode.getChildNodes();
					int id;
		            if(nodes.getLength()!=0){
		            	 id=nodes.getLength()+1;
		            }else{
		            	 id=1;
		            }
				    //create new and update elements :
					String eleName = "Enr"+id+"";
					String elePath = this.getEnrichmentPath();
					String eleTitle = this.getEnrichmentTitle();
					String eleID = String.valueOf(this.enrichmentId);
					String type = String.valueOf(this.enrichmentType);
					Element enrElement = document.createElement(eleName);
					enrElement.setAttribute("Path", elePath);
					enrElement.setAttribute("Type",type );
					enrElement.setAttribute("Title", eleTitle);
 					enrElement.setAttribute("ID", eleID);
					pageNode.appendChild(enrElement);

					// write the DOM object to the file
					TransformerFactory transformerFactory = TransformerFactory.newInstance();
					Transformer transformer = transformerFactory.newTransformer();
					DOMSource domSource = new DOMSource(document);

					StreamResult streamResult = new StreamResult(new File(bookXmlPath));
					transformer.transform(domSource, streamResult);

				} catch (ParserConfigurationException pce) {
					pce.printStackTrace();	        
				} catch (TransformerException tfe) {
					tfe.printStackTrace();	        
				} catch (IOException ioe) {
					ioe.printStackTrace();
				} catch (SAXException sae) {
					sae.printStackTrace();
				}
	  }

	public String getEnrichmentStoreBookId() {
		return enrichmentStoreBookId;
	}

	public void setEnrichmentStoreBookId(String enrichmentStoreBookId) {
		this.enrichmentStoreBookId = enrichmentStoreBookId;
	}

	public String getEnrichmentStoreId() {
		return enrichmentStoreId;
	}

	public void setEnrichmentStoreId(String enrichmentBookStoreId) {
		this.enrichmentStoreId = enrichmentBookStoreId;
	}

	public int getDbBookId() {
		return dbBookId;
	}

	public void setDbBookId(int dbBookId) {
		this.dbBookId = dbBookId;
	}

	public boolean isReadStatus() {
		return isReadStatus;
	}

	public void setReadStatus(boolean readStatus) {
		isReadStatus = readStatus;
	}

	public boolean isDownloaded() {
		return isDownloaded;
	}

	public void setDownloaded(boolean downloaded) {
		isDownloaded = downloaded;
	}

	public int getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public ArrayList<Enrichments> getSubItemArray() {
		return subItemArray;
	}

	public void setSubItemArray(ArrayList<Enrichments> subItemArray) {
		this.subItemArray = subItemArray;
	}

	public class EnrichmentDownloadAndUpdate extends AsyncTask<Void, Integer, Void>{

		ProgressDialog downloadProgDialog;
		String enrichFileName;
		public String dProgressString="";
		public boolean downloadSuccess;
		String XMLfilePath;
		private ArrayList<Enrichments> enrichList, selectedEnrichments;
		private Enrichments enrichment;
		String bookDir;
		
		public EnrichmentDownloadAndUpdate(ProgressDialog downloadProgDialog, ArrayList<Enrichments> enrichList, ArrayList<Enrichments> selectedEnrichments, Enrichments enrichment){
			this.enrichList = enrichList;
			this.selectedEnrichments = selectedEnrichments;
			this.enrichment = enrichment;
			this.downloadProgDialog = downloadProgDialog;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			downloadProgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);   
			downloadProgDialog.setMessage("");
			downloadProgDialog.getWindow().setTitleColor(Color.rgb(222, 223, 222));
			downloadProgDialog.setIndeterminate(false);
			downloadProgDialog.setCancelable(false);
			downloadProgDialog.setCanceledOnTouchOutside(false);
			downloadProgDialog.setTitle(enrichmentTitle);
	    	downloadProgDialog.setProgress(0);
	    	downloadProgDialog.setMax(100);
	    	downloadProgDialog.show();
	    	
	    	bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+enrichmentStoreId+"M"+enrichmentBid+"Book/";
	    	enrichFileName = bookDir+"Enrichments/Page"+enrichmentPageNo+"/Enriched"+enrichmentId;
	    	if (!new File(enrichFileName).exists()) {
				new File(enrichFileName).mkdirs();
			}
	    	
	    	XMLfilePath = bookDir+"xmlForUpdateRecords.xml";
	    	File xmlFile = new File(XMLfilePath);
	    	if(!xmlFile.exists()){
	    		//Copy xml file from assets to book folder :
	    		copyFiles("xmlForUpdateRecords.xml", XMLfilePath);
	    	}
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			final int TIMEOUT_CONNECTION = 5000;//5sec
			 final int TIMEOUT_SOCKET = 30000;//30sec
			long bytes = 1024 * 1024;
			 try{
			 URL url = new URL(enrichmentPath);
			//Open a connection to that URL.
			 URLConnection ucon = url.openConnection();
			ucon.connect();
			int   fileLength = ucon.getContentLength();
			//////System.out.println("filelength:"+fileLength);
			 long startTime = System.currentTimeMillis();
			 
			 //this timeout affects how long it takes for the app to realize there's a connection problem
			 ucon.setReadTimeout(TIMEOUT_CONNECTION);
			 ucon.setConnectTimeout(TIMEOUT_SOCKET);
			 //Define InputStreams to read from the URLConnection.
			 // uses 3KB download buffer
			 InputStream is = ucon.getInputStream();
			 BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
			  //Internal storage
			 String filePath = enrichFileName+".zip";
			 FileOutputStream outStream = new FileOutputStream(filePath);
			 byte[] buff = new byte[5 * 1024];
			 //Read bytes (and store them) until there is nothing more to read(-1)
			 int len;
			String totalMB,fileLengthMB;
			long total = 0;
			DecimalFormat twoDformat = new DecimalFormat("#.##");
			 while ((len = inStream.read(buff)) != -1)
			 {
				total += len;
				////////System.out.println("total:"+total);
				totalMB=twoDformat.format((float)total/bytes);
				fileLengthMB= twoDformat.format((float)fileLength/bytes);
				dProgressString=totalMB+" MB"+" of "+fileLengthMB+" MB";
				publishProgress((int)((total*100)/fileLength));
				outStream.write(buff,0,len);
			 }
			 //clean up
			 outStream.flush();
			 outStream.close();
			 inStream.close();
			 downloadSuccess = true;
			 ////System.out.println( "download completed in "+ ((System.currentTimeMillis() - startTime) / 1000)+ " sec");
			 
			 }
			 catch (IOException e){
				 ////System.out.println("error while storing:"+e);
				 downloadSuccess = false;
			 }
			return null;
		}
		
		protected void onProgressUpdate(Integer... progress){
			super.onProgressUpdate(progress);
			if(downloadProgDialog != null){
				////System.out.println("OnprogressUpdate:"+progress[0]);
				downloadProgDialog.setMessage(dProgressString);
				downloadProgDialog.setProgress(progress[0]);
			}
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (downloadSuccess) {
				String zipfile = enrichFileName + ".zip";
				String unziplocation = enrichFileName + "/"; //"/" is a must here..
				Boolean unzipSuccess= Zip.unzipDownloadfileTemp(zipfile,unziplocation); //if unzipping success then need to store in database:
				if(unzipSuccess){
					UserFunctions.traverseAndMakeFileWorldReadable(new java.io.File(bookDir+"Enrichments"));
					//DB entry :
				}
				//Need to delete the zip file which is download
				File zipfilelocation = new File(enrichFileName+".zip");  //Deleting zip file in the internal storage
				try{
					zipfilelocation.delete();
				}catch(Exception e){
					//////System.out.println("Failed to deletefile with Error "+e);
				}
				createDownloadEnrichmentEntryInBookXml();
				checkAndUpdateXmlForUpdateRecords(XMLfilePath);
				enrichList.remove(enrichment);
				selectedEnrichments.remove(enrichment);
				
				if (selectedEnrichments.size() == 0) {
					downloadProgDialog.dismiss();
				}
			}
		}
		
	    /**
	     * Method to copy file xmlForUpdateRecords.xml to the book 
	     * @param fromPath
	     * @param toPath
	     * @return
	     */
	    public boolean copyFiles(String fromPath, String toPath){
			InputStream in =null;           
			OutputStream out =null;
			try{
				in = context.getAssets().open(fromPath);
				new File(toPath).createNewFile();
				out = new FileOutputStream(toPath);
				byte[] buffer = new byte[1024];
				int read;
				while((read = in.read(buffer)) != -1){
					out.write(buffer,0,read);
				}
				in.close();
				in=null;
				out.flush();
				out.close();
				out = null;
				return true;
			}catch(Exception e){
				e.printStackTrace();
				return false;
			}
		}
	}
	
	/**
	 * creating an enrichment entry in book.xml file
	 */
	public void createDownloadEnrichmentEntryInBookXml(){
		DatabaseHandler db = DatabaseHandler.getInstance(context);
		String enrPath = "Enrichments/Page"+getEnrichmentPageNo()+"/Enriched"+getEnrichmentId()+"/index.htm";
		db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,categoryID)values('" + dbBookId + "','" + getEnrichmentPageNo() + "','" + getEnrichmentTitle() + "','"+Globals.downloadedEnrichmentType+"', '0','" + enrPath + "','0')");
		/*String bookXmlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+"M"+enrichmentBid+"Book/"+"Book.xml";
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

			Document document = documentBuilder.parse(new File(bookXmlPath));
			String tagName = "P"+(this.enrichmentPageNo - 1);
			Node pageNode = document.getElementsByTagName(tagName).item(0);
			//NodeList nodes = pageNode.getChildNodes();
            
		    //create new and update elements :
			String eleName = "Enr"+"1";
			String elePath = "Enrichments/Page"+enrichmentPageNo+"/Enriched"+enrichmentId+"/index.htm";
			String eleTitle = this.enrichmentTitle;
			String eleID = String.valueOf(this.enrichmentId);
			String eleType = "Downloaded";
			Element enrElement = document.createElement(eleName);
			enrElement.setAttribute("Path", elePath);
			enrElement.setAttribute("Title", eleTitle);
			enrElement.setAttribute("ID", eleID);
			enrElement.setAttribute("Type", eleType);
			pageNode.appendChild(enrElement);

			// write the DOM object to the file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);

			StreamResult streamResult = new StreamResult(new File(bookXmlPath));
			transformer.transform(domSource, streamResult);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();	        
		} catch (TransformerException tfe) {
			tfe.printStackTrace();	        
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}*/
	}

	public void createDownloadEnrichmentEntryInBookXmlForGdrive(String elePath){
		//String bookXmlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+"M"+enrichmentBid+"Book/"+"Book.xml";
		String bookXmlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+enrichmentStoreBookId+"Book/"+"Book.xml";
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

			Document document = documentBuilder.parse(new File(bookXmlPath));
			String tagName = "P"+(this.enrichmentPageNo - 1);
			Node pageNode = document.getElementsByTagName(tagName).item(0);
			//NodeList nodes = pageNode.getChildNodes();

			//create new and update elements :
			String eleName = "Enr"+"1";
			//String elePath = "Enrichments/Page"+enrichmentPageNo+"/Enriched"+enrichmentId+"/index.htm";
			String eleTitle = this.enrichmentTitle;
			String eleID = String.valueOf(this.enrichmentId);
			String eleType = "Downloaded";
			Element enrElement = document.createElement(eleName);
			enrElement.setAttribute("Path", elePath);
			enrElement.setAttribute("Title", eleTitle);
			enrElement.setAttribute("ID", eleID);
			enrElement.setAttribute("Type", eleType);
			pageNode.appendChild(enrElement);

			// write the DOM object to the file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);

			StreamResult streamResult = new StreamResult(new File(bookXmlPath));
			transformer.transform(domSource, streamResult);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}

	public void checkAndUpdateDbForUpdateRecords(String enrPath){
		DatabaseHandler db = DatabaseHandler.getInstance(context);
		//String enrPath = "Enrichments/Page"+getEnrichmentPageNo()+"/Enriched"+getEnrichmentId()+"/index.htm";
		db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,categoryID)values('" + enrichmentBid + "','" + getEnrichmentPageNo() + "','" + getEnrichmentTitle() + "','"+Globals.downloadedEnrichmentType+"', '10','" + enrPath + "','0')");
	}
	
	/**
	 * Check and update xml in XmlUpdateRecordsinXml
	 * @param XmlPath
	 */
	public void checkAndUpdateXmlForUpdateRecords(String XmlPath){

		//revert_book_hide :
		//String bookXmlPath =  bookView.getFilesDir().getPath()+"/M"+BookID+"Book"+"/xmlForUpdateRecords.xml"; //data/data/com.semanoor.manahij/files/
		//String bookXmlPath =  bookView.getFilesDir().getPath()+"/.M"+BookID+"Book"+"/xmlForUpdateRecords.xml";
		String bookXmlPath = XmlPath;
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

			Document document = documentBuilder.parse(new File(bookXmlPath));
			String tagName = "pages";
			String toModifyTagName = "page"+enrichmentPageNo;
			Boolean isTagExists = false;
			Node pagesNode = document.getElementsByTagName(tagName).item(0);

			NodeList pagesnodesList = pagesNode.getChildNodes();
			for(int i = 0;i< pagesnodesList.getLength(); i++){

				Node element = pagesnodesList.item(i);
				if(element.getNodeName().equals(toModifyTagName)){
					//tag exists
					isTagExists = true;
					break;
				}else{
					//Tag doesn't exist. create a new tag.
					isTagExists = false;
				}
			}

			if(!isTagExists){
				Element pageElement = document.createElement(toModifyTagName);
				pagesNode.appendChild(pageElement);
			}

			//create new and update elements :
			Node currentPageNode = document.getElementsByTagName(toModifyTagName).item(0);
			String eleName = "enriched";

			Element enrElement = document.createElement(eleName);
			enrElement.setAttribute("IDPage", String.valueOf(enrichmentPageNo));
			enrElement.setAttribute("ID", String.valueOf(enrichmentId));
			enrElement.setAttribute("Title", enrichmentTitle);
			enrElement.setAttribute("DateUp", enrichmentDateUp);
			enrElement.setAttribute("UserName", enrichmentUserName);
			enrElement.setAttribute("Size", enrichmentSize);
			enrElement.setAttribute("Path", enrichmentPath);
			enrElement.setAttribute("IDEnr", "0");
			enrElement.setAttribute("IDUser", String.valueOf(enrichmentUserId));
			enrElement.setAttribute("Date", enrichmentDate);
			enrElement.setAttribute("Password", getEnrichmentPassword());
			enrElement.setAttribute("BookTitle", enrichmentBookTitle);
			enrElement.setAttribute("BookID", String.valueOf(enrichmentBid));
			enrElement.setAttribute("Email", enrichmentEmailId);

			currentPageNode.appendChild(enrElement);

			// write the DOM object to the file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);

			StreamResult streamResult = new StreamResult(new File(bookXmlPath));
			transformer.transform(domSource, streamResult);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();	        
		} catch (TransformerException tfe) {
			tfe.printStackTrace();	        
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}

	/**
	 * @return the enrichmentSequenceId
	 */
	public int getEnrichmentSequenceId() {
		return enrichmentSequenceId;
	}
	/**
	 * @param enrichmentSequenceId the enrichmentSequenceId to set
	 */
	public void setEnrichmentSequenceId(int enrichmentSequenceId) {
		this.enrichmentSequenceId = enrichmentSequenceId;
	}
	/**
	 * @return the enrichmentExportValue
	 */
	public int getEnrichmentExportValue() {
		return enrichmentExportValue;
	}
	/**
	 * @param enrichmentExportValue the enrichmentExportValue to set
	 */
	public void setEnrichmentExportValue(int enrichmentExportValue) {
		this.enrichmentExportValue = enrichmentExportValue;
	}
	/**
	 * @return the enrichmentSelected
	 */
	public boolean isEnrichmentSelected() {
		return enrichmentSelected;
	}
	/**
	 * @param enrichmentSelected the enrichmentSelected to set
	 */
	public void setEnrichmentSelected(boolean enrichmentSelected) {
		this.enrichmentSelected = enrichmentSelected;
	}
	/**
	 * @return the enrichmentPath
	 */
	public String getEnrichmentPath() {
		return enrichmentPath;
	}
	/**
	 * @param enrichmentPath the enrichmentPath to set
	 */
	public void setEnrichmentPath(String enrichmentPath) {
		this.enrichmentPath = enrichmentPath;
	}
	/**
	 * @return the enrichmentUserId
	 */
	public int getEnrichmentUserId() {
		return enrichmentUserId;
	}
	/**
	 * @param enrichmentUserId the enrichmentUserId to set
	 */
	public void setEnrichmentUserId(int enrichmentUserId) {
		this.enrichmentUserId = enrichmentUserId;
	}
	/**
	 * @return the enrichmentUserName
	 */
	public String getEnrichmentUserName() {
		return enrichmentUserName;
	}
	/**
	 * @param enrichmentUserName the enrichmentUserName to set
	 */
	public void setEnrichmentUserName(String enrichmentUserName) {
		this.enrichmentUserName = enrichmentUserName;
	}
	/**
	 * @return the enrichmentDateUp
	 */
	public String getEnrichmentDateUp() {
		return enrichmentDateUp;
	}
	/**
	 * @param enrichmentDateUp the enrichmentDateUp to set
	 */
	public void setEnrichmentDateUp(String enrichmentDateUp) {
		this.enrichmentDateUp = enrichmentDateUp;
	}
	/**
	 * @return the enrichmentSize
	 */
	public String getEnrichmentSize() {
		return enrichmentSize;
	}
	/**
	 * @param enrichmentSize the enrichmentSize to set
	 */
	public void setEnrichmentSize(String enrichmentSize) {
		this.enrichmentSize = enrichmentSize;
	}
	/**
	 * @return the enrichmentBookTitle
	 */
	public String getEnrichmentBookTitle() {
		return enrichmentBookTitle;
	}
	/**
	 * @param enrichmentBookTitle the enrichmentBookTitle to set
	 */
	public void setEnrichmentBookTitle(String enrichmentBookTitle) {
		this.enrichmentBookTitle = enrichmentBookTitle;
	}
	/**
	 * @return the enrichmentEmailId
	 */
	public String getEnrichmentEmailId() {
		return enrichmentEmailId;
	}
	/**
	 * @param enrichmentEmailId the enrichmentEmailId to set
	 */
	public void setEnrichmentEmailId(String enrichmentEmailId) {
		this.enrichmentEmailId = enrichmentEmailId;
	}
	/**
	 * @return the enrichmentDate
	 */
	public String getEnrichmentDate() {
		return enrichmentDate;
	}
	/**
	 * @param enrichmentDate the enrichmentDate to set
	 */
	public void setEnrichmentDate(String enrichmentDate) {
		this.enrichmentDate = enrichmentDate;
	}
	/**
	 * @return the enrichmentPassword
	 */
	public String getEnrichmentPassword() {
		return enrichmentPassword;
	}
	/**
	 * @param enrichmentPassword the enrichmentPassword to set
	 */
	public void setEnrichmentPassword(String enrichmentPassword) {
		this.enrichmentPassword = enrichmentPassword;
	}

public  void setlayoutvisibilityInvisible(View v,boolean status){
	for (int i = 0, j =  ((BookViewActivity)context).enrTabsLayout.getChildCount(); i < j; i++) {
		Button btnEnrTab;
		RelativeLayout rlview = null;
		if (i == 0) {
			btnEnrTab = (Button) ((BookViewActivity) context).enrTabsLayout.getChildAt(i);
		} else {
			rlview = (RelativeLayout) ((BookViewActivity) context).enrTabsLayout.getChildAt(i);
			btnEnrTab = (Button) rlview.getChildAt(0);
		}
		if (btnEnrTab ==v) {
			if(status){
				rlview.setVisibility(View.VISIBLE);
			}else {
				rlview.setVisibility(View.INVISIBLE);
			}
		}
	}
}
public View getselectedItemPositionInlayout(LinearLayout enrTabsLayout, View vieww){
	for (int i = 0, j = enrTabsLayout.getChildCount(); i < j; i++) {
		Button btnEnrTab;
		RelativeLayout rlview = null;
		if (i == 0) {
			btnEnrTab = (Button) ((BookViewActivity) context).enrTabsLayout.getChildAt(i);
		} else {
			rlview = (RelativeLayout) ((BookViewActivity) context).enrTabsLayout.getChildAt(i);
			btnEnrTab = (Button) rlview.getChildAt(0);
		}
		if (btnEnrTab ==vieww) {
			return rlview;
		}
	 }
	return null;
}
}
