package com.semanoor.sboookauthor_store;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.TypedValue;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.semanoor.manahij.CloudEnrichDetails;
import com.semanoor.manahij.Enrichment;
import com.semanoor.manahij.MainActivity;
import com.semanoor.manahij.SlideMenuWithActivityGroup;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.DownloadBackground;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.QuizSequenceObject;
import com.semanoor.source_sboookauthor.TextCircularProgressBar;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RenderHTMLObjects extends WebView implements StoreObjectsJavascriptInterfaceListener {

	private String bName;
	private int currentPage;
	private int totalPages;
	private Book newBook;
	private int newBookId;
	private DatabaseHandler db;
	private Context context;
	private int sequentialId;
	private String htmlDir;
	private Enrichment enrich;
	private CloudEnrichDetails cloudData;
	private boolean isStoreBookIpadQueryUpdated;
	//private final AtomicInteger evalJsIndex = new AtomicInteger(0);
	private Handler handler = new Handler();
	private final Map<Integer, String> jsReturnValues = new HashMap<Integer, String>();
	private final Object jsReturnValueLock = new Object();
	private String jScriptReturnValues;
	private String isBookFromManahijTab;
	private String version;
	private int kj = 0;
	private ArrayList<String> classNamesList;
	private int elemLength = 0;
	private boolean webTextProcessing = false, webTableProcessing = false, textSquareProcessing = false, textRectangleProcessing = false, textCircleProcessing = false, titleTextProcessing = false, authorTextProcessing = false, pageNoTextProcessing = false, quizWebTextProcessing = false, imageProcessing = false, drawnImageProcessing = false, embedBoxProcessing = false, audioProcessing = false, iframeProcessing = false, youtubeProcessing = false;
	SlideMenuWithActivityGroup MainActivity;
	MainActivity activity;
	TextCircularProgressBar textCircularProgressBar;
	DownloadBackground background;

	public RenderHTMLObjects(Context _context, AttributeSet attrs) {
		super(_context, attrs);
		this.context = _context;
		this.setup(_context);
	}
	public void setInitialValues1(String bookName, int _totalPages, Book newBook, SlideMenuWithActivityGroup enrichment){
		this.bName = bookName;
		this.totalPages = _totalPages;
		this.newBook = newBook;
		this.newBookId = newBook.getBookID();
		db = DatabaseHandler.getInstance(context);
		this.MainActivity = enrichment;
		currentPage = 1;
		copyFreeFiles();
		isStoreBookIpadQueryUpdated = false;
	}
	public void setInitialValues2(String bookName, int _totalPages, Book newBook, MainActivity enrichment, TextCircularProgressBar progressBar, DownloadBackground background){
		this.bName = bookName;
		this.totalPages = _totalPages;
		this.newBook = newBook;
		this.newBookId = newBook.getBookID();
		db = DatabaseHandler.getInstance(context);
		this.activity = enrichment;
		this.background = background;
		this.textCircularProgressBar = progressBar;
		currentPage = 1;
		copyFreeFiles();
		isStoreBookIpadQueryUpdated = false;
	}
	public void setInitialValues3(String bookName, int _totalPages, Book newBook, CloudEnrichDetails Cloudenrich, TextCircularProgressBar progressBar, DownloadBackground background){
		this.bName = bookName;
		this.totalPages = _totalPages;
		this.newBook = newBook;
		this.newBookId = newBook.getBookID();
		db = DatabaseHandler.getInstance(context);
		this.cloudData = Cloudenrich;
		this.background = background;
		this.textCircularProgressBar = progressBar;
		currentPage = 1;
		copyFreeFiles();
		isStoreBookIpadQueryUpdated = false;
	}
	/**
	 * setUp the WebView
	 * @param context
	 */
	private void setup(Context context) {
		this.setVisibility(View.INVISIBLE);
		
		this.getSettings().setJavaScriptEnabled(true);
		this.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		this.getSettings().setAllowContentAccess(true);

		// Zoom out fully
		this.getSettings().setLoadWithOverviewMode(true);
		this.getSettings().setUseWideViewPort(true);
		//this.getSettings().setSupportZoom(true);
		//this.getSettings().setBuiltInZoomControls(true);

		this.getSettings().setRenderPriority(RenderPriority.HIGH);
		this.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		this.clearCache(true);
		this.setFocusable(true);
		
		StoreObjectsJavascriptInterace javascriptInterface = new StoreObjectsJavascriptInterace(this);
		this.addJavascriptInterface(javascriptInterface, javascriptInterface.getInterfaceName());
	}

	/**
	 * Capture the ScreenShot of the page only for the last page
	 */
	/*@Override
	public void invalidate() {
		super.invalidate();
		if (currentPage > totalPages) {
			saveScreenshotforPage(this, currentPage-1);
			this.setVisibility(View.GONE);
		}
	}*/
	
	/**
	 * Process and render all the objects from Html page loading it in the webview
	 */
	public void processHtmlPages(){
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT){
			version = "KITKAT";
		} else {
			version = "ICS";
		}
		String basUrlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/";
		String filePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/"+currentPage+".htm";
		String sourceString = UserFunctions.decryptFile(new File(filePath));
		if (sourceString == null){
			sourceString = "";
		} else {
			sourceString = sourceString.replaceFirst("<head>", "<head><script src=" + Globals.TARGET_BASE_STOREOBJETCS_JS_PATH + "></script>");
			sourceString = sourceString.replaceAll("../FreeFiles", "FreeFiles");
			if (sourceString.contains("function isBookFromManahijIpad()")) {
				isBookFromManahijTab = "true";
			} else {
				isBookFromManahijTab = "false";
			}
		}
		//System.out.println(sourceString);
		this.loadDataWithBaseURL("file:///"+basUrlPath, sourceString, "text/html", "utf-8", null);
		
		this.setWebViewClient(new WebViewClient(){

			public void onPageFinished(WebView view, String url) {
				sequentialId = 1;
				if (Globals.isTablet()){
					if (isBookFromManahijTab.equals("true")) {
						if (!isStoreBookIpadQueryUpdated) {
							String query = "update books set StoreBookCreatedFromIpad=\"yes\" where BID='"+newBookId+"'";
							db.executeQuery(query);
							isStoreBookIpadQueryUpdated = true;
							newBook.setStoreBookCreatedFromTab(true);
						}
						if (version == "KITKAT") {
							scanAndGetAllObjectsFromHtmlForKitKat(view, currentPage);
							checkForPageBgExistAndUpdatetoDb(currentPage);
						} else {
							scanAndGetAllObjectsFromHtml(view, currentPage);
							checkForPageBgExistAndUpdatetoDb(currentPage);
						}
					} else {
						if (currentPage != 1 && currentPage != totalPages) {
							if (currentPage == 2) {
								loadJSScript("javascript:getDocumentOffsetWidthAndHeight();", view);
							}
							int paddingSpace;
							if (newBook.isCssType) {
								paddingSpace = 80;
							} else {
								paddingSpace = 60;
							}
							int actionBarHeight = getActionBarHeight() + paddingSpace;
							//int actionBarHeight = getActionBarHeight();
							int width = Globals.getDeviceWidth();  ///800
							int height = Globals.getDeviceHeight()-actionBarHeight;  //1280
							loadJSScript("javascript:getHTMLDir();", view);
							loadJSScript("javascript:getWebContent('"+currentPage+"');", view);
							loadJSScript("javascript:tagCoordinates('"+width+"', '"+height+"', '"+currentPage+"');", view);
							loadJSScript("javascript:getTableBackground('"+currentPage+"');", view);
//							loadJSScript("javascript:getExternalCSSPath();",view);
//							loadJSScript("javascript:getPath();",view);
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
								view.evaluateJavascript("javascript:getExternalCSSPath('"+version+"','"+currentPage+"')", new ValueCallback<String>() {
									@Override
									public void onReceiveValue(String value) {
										String cssFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/FreeFiles/css";
										int page = 0;
										String scale = null;
										if (new File(cssFilePath).exists() &&value.contains("css")) {
											if (!value.equals("")) {
												String[] scalePage = value.split("\\|");
												String p = scalePage[1].replace("\"", "");
												scale = scalePage[0].replace("\"", "");
												String scalePageToFit="yes##0px##0px##0px##0px##undefined##undefined##".concat(scale);
												page = Integer.parseInt(p);
												db.executeQuery("update objects set ScalePageToFit='" + scalePageToFit + "' where BID='" + newBook.getBookID() + "' and pageNo='" + page + "' and ScalePageToFit='yes'");
                                            }
										}
									}
								});
							}else{
								view.loadUrl("javascript:getExternalCSSPath('"+version+"');");
							}

						}
					}
				}

				if (currentPage != 1) {
					saveScreenshotforPage(view, currentPage-1);
				}

				if (isBookFromManahijTab.equals("false")) {
					processPages(view);
				}
			};
		});
		
		this.setWebChromeClient(new WebChromeClient(){
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
			}
		});
	}

	private int getActionBarHeight(){
		int actionBarHeight = 0;
		TypedValue tv = new TypedValue();
		if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
			actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
		}
		return actionBarHeight;
	}

	private void processPages(WebView view){
		currentPage = currentPage+1;
		if (currentPage <= totalPages+1) {
			if (background.getTextCircularProgressBar()!=null){
				  background.getTextCircularProgressBar().setProgress((currentPage * 100) / totalPages);
			} else if (MainActivity!=null){
				MainActivity.processPageProgDialog.setProgress((currentPage * 100) / totalPages);
			}
			processHtmlPages();
		} else {
			copyNewBookCssFiles();
			db.executeQuery("update books set isDownloadedCompleted='downloaded'where BID ='" + background.getDownLoadBook().getBookID() + "'");
 			  if (background.getTextCircularProgressBar()!=null) {
				  background.getTextCircularProgressBar().setVisibility(INVISIBLE);
				  if(activity!=null) {
					  activity.loadGridView();
				  }
			  }
			background.getList().remove(0);
			if (background.getList().size() > 0) {
				final Book book = background.getList().get(0);
				background.setDownLoadBook(book);
				background.setDownloadTaskRunning(true);
				new android.os.Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						background.new downloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, book.getDownloadURL());
					}
				}, 2000);
			} else {
				background.setDownloadTaskRunning(false);
			}
		}
	}

	private void copyNewBookCssFiles(){
		String cssFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/FreeFiles/css";
		if (new File(cssFilePath).exists()) {
			String srcStoreBookFreefilesDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + bName + "/FreeFiles";
			String destCreatedBookFreefilesDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBookId + "/" + "FreeFiles/";
			try {
				UserFunctions.copyDirectory(new File(srcStoreBookFreefilesDir), new File(destCreatedBookFreefilesDir));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void checkForPageBgExistAndUpdatetoDb(int pageNo){
		String pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/"+"FreeFiles/pageBG_"+pageNo+".png";
		if (new File(pageBgPath).exists()) {
			String contentPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/"+"FreeFiles/pageBG_"+pageNo+".png";
			boolean objLocked = false;
			int enrichedId = 0;
			db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + "PageBG" + "', '" + newBookId + "', '" + pageNo + "', '" + "0|0" + "', '" + "ScaleToFit" + "', '" + contentPath + "', '" + 0 + "', '" + "no" + "', '" + objLocked + "', '" + enrichedId + "')");
		}
	}
	
	private void scanAndGetAllObjectsFromHtmlForKitKat(WebView view, int pageNo){
		classNamesList = new ArrayList<String>();
		getElementsTagNameLength(view, pageNo);
	}
	
	private void getElementsTagNameLength(final WebView view, final int pageNo){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getElementsTagNameLength('" + version + "');", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    value = getValuFromJsonreader(value);
                    if (value != "") {
                        elemLength = Integer.parseInt(value);
                    }
                    for (int i = 0; i < elemLength; i++) {
                        if (i == elemLength - 1) {
                            getElementsClassName(i, view, pageNo, true);
                        } else {
                            getElementsClassName(i, view, pageNo, false);
                        }
                    }
                }
            });
		}
	}
	
	private void getElementsClassName(final int i, final WebView view, final int pageNo, final boolean isFinalClass){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getElementsClassName('"+i+"', '"+version+"');", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String className = getValuFromJsonreader(value);
                    classNamesList.add(className);
                    if (className.equals("WebText") || className.equals("TextSquare") || className.equals("TextRectangle") || className.equals("TextCircle") || className.equals(Globals.OBJTYPE_TITLETEXT) || className.equals(Globals.OBJTYPE_AUTHORTEXT) || className.equals(Globals.objType_pageNoText) || className.equals("webTable") || className.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
                        setElementProcessing(className, true);
                        getWebDivContentHTML(i, view, className, pageNo);
                    } else if (className.equals("Image") || className.equals("DrawImage")) {
                        setElementProcessing(className, true);
                        getImageElementSize(i, view, className, pageNo);
                    }  else if (className.equals("EmbedCodeBox")) {
                        setElementProcessing(className, true);
                        getEmbedBoxSize(i, view, className, pageNo);
                    } else if (className.equals("Audio")) {
                        setElementProcessing(className, true);
                        getAudioElemContent(i, view, className, pageNo);
                    } else if (className.equals("Iframe")) {
                        setElementProcessing(className, true);
                        getIframeSize(i, view, className, pageNo);
                    } else if (className.equals("YouTube")) {
                        setElementProcessing(className, true);
                        getYoutubeSize(i, view, className, pageNo);
                    } else if (isFinalClass) {
                        if (className.equals("null") || className.equals("")) {
                            setScanningFinished(i);
                        }
                    }
                }
            });
		}
	}
	
	private void setElementProcessing(String className, boolean processing){
		if (className.equals("WebText")) {
			webTextProcessing = processing;
		} else if (className.equals("TextSquare")) {
			textSquareProcessing = processing;
		} else if (className.equals("TextRectangle")) {
			textRectangleProcessing = processing;
		} else if (className.equals("TextCircle")) {
			textCircleProcessing = processing;
		} else if (className.equals(Globals.OBJTYPE_TITLETEXT)) {
			titleTextProcessing = processing;
		} else if (className.equals(Globals.OBJTYPE_AUTHORTEXT)) {
			authorTextProcessing = processing;
		} else if (className.equals(Globals.objType_pageNoText)) {
			pageNoTextProcessing = processing;
		} else if (className.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
			quizWebTextProcessing = processing;
		} else if (className.equals("webTable")) {
			webTableProcessing = processing;
		} else if (className.equals("Image")) {
			imageProcessing = processing;
		} else if (className.equals("DrawImage")) {
			drawnImageProcessing = processing;
		} else if (className.equals("EmbedCodeBox")) {
			embedBoxProcessing = processing;
		} else if (className.equals("Audio")) {
			audioProcessing = processing;
		} else if (className.equals("Iframe")) {
			iframeProcessing = processing;
		} else if (className.equals("YouTube")) {
			youtubeProcessing = processing;
		}
	}
	
	private void getImageElementSize(final int i, WebView view, final String className, final int pageNo){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getImageElementSize('"+i+"', '"+version+"');", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String imageElemAttr = getValuFromJsonreader(value);
                    updateImageElementToDb(imageElemAttr, className, pageNo);
                    setElementProcessing(className, false);
                    setScanningFinished(i);
                }
            });
		}
	}
	
	private void getEmbedBoxSize(final int i, final WebView view, final String className, final int pageNo){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getEmbedBoxSize('"+i+"', '"+version+"');", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String embedElemAttr = getValuFromJsonreader(value);
                    getEmbedDivContentHTML(i, view, className, pageNo, embedElemAttr);
                }
            });
		}
	}
	
	private void getEmbedDivContentHTML(final int i, WebView view, final String className, final int pageNo, final String embedElemAttr){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getDivContentHTML('"+i+"', '"+version+"')", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    //value = value.replace("\\u003E", ">");
                    //value = value.replace("\\u003C", "<").replace("\\\"", "\"");
                    String embedCodeBoxContent = getValuFromJsonreader(value);
                    updateEmbedElementToDb(embedElemAttr, embedCodeBoxContent, className, pageNo);
                    setElementProcessing(className, false);
                    setScanningFinished(i);
                }

            });
		}
	}
	
	private void getAudioElemContent(final int i, final WebView view, final String className, final int pageNo){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getAudioElemContent('"+i+"', '"+version+"')", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String audioElemAttr = getValuFromJsonreader(value);
                    updateAudioElementToDb(audioElemAttr, className, pageNo);
                    setElementProcessing(className, false);
                    setScanningFinished(i);
                }
            });
		}
	}
	
	private void getIframeSize(final int i, final WebView view, final String className, final int pageNo){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getIframeSize('"+i+"', '"+version+"')", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String iframeElemAttr = getValuFromJsonreader(value);
                    updateIframeElementToDb(iframeElemAttr, className, pageNo);
                    setElementProcessing(className, false);
                    setScanningFinished(i);
                }
            });
		}
	}
	
	private void getYoutubeSize(final int i, final WebView view, final String className, final int pageNo){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getYoutubeSize('"+i+"', '"+version+"')", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String youtubeElemAttr = getValuFromJsonreader(value);
                    getYoutubeHtmlId(i, view, className, pageNo, youtubeElemAttr);
                }
            });
		}
	}
	
	private void getYoutubeHtmlId(final int i, final WebView view, final String className, final int pageNo, final String youtubeElemAttr){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getYoutubeHtmlId('"+i+"', '"+version+"')", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String youtubeHtmlId = getValuFromJsonreader(value);
                    updateYoutubeElementToDb(youtubeElemAttr, youtubeHtmlId, className, pageNo);
                    setElementProcessing(className, false);
                    setScanningFinished(i);
                }
            });
		}
	}
	
	private void getWebDivContentHTML(final int i, final WebView view, final String className, final int pageNo){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getDivContentHTML('"+i+"', '"+version+"')", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    String divContent = getValuFromJsonreader(value);
                    if (className.equals("WebText") || className.equals("TextSquare") || className.equals("TextRectangle") || className.equals("TextCircle") || className.equals(Globals.OBJTYPE_TITLETEXT) || className.equals(Globals.OBJTYPE_AUTHORTEXT) || className.equals(Globals.objType_pageNoText)) {
                        getElementXandYPos(i, view, className, pageNo, divContent);
                    } else if (className.equals("webTable")) {
                        getElementXandYPosForWebTable(i, view, className, pageNo, divContent);
                    } else if (className.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
                        //divContent = divContent.replace("\\n", "");
                        getQuizElementLength(i, view, className, pageNo, divContent);
                    }
                }
            });
		}
	}
	
	private String getValuFromJsonreader(String value){
		String msg = "null";
		JsonReader reader = new JsonReader(new StringReader(value));
		reader.setLenient(true);
		try {
			if (reader.peek() != JsonToken.NULL) {
				if (reader.peek() == JsonToken.STRING) {
					msg = reader.nextString();
					if (msg != null) {
						return msg;
					}
				}
			}
		} catch (Exception e) {
			
		} finally {
			try {
				reader.close();
			} catch (Exception e2) {
			}
		}
		return msg;
	}
	
	private void getElementXandYPos(final int i, final WebView view, final String className, final int pageNo, final String divContent){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getElementXandYPos('"+i+"', '"+version+"')", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String locationXandY = getValuFromJsonreader(value);
                    getWebTextElementSize(i, view, className, pageNo, divContent, locationXandY);
                }
            });
		}
	}
	
	private void getWebTextElementSize(final int i, final WebView view, final String className, final int pageNo, final String divContent, final String locationXandY){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getWebTextElementSize('"+i+"', '"+version+"')", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String webTextSize = getValuFromJsonreader(value);;
                    updateWebTextToDb(locationXandY, webTextSize, divContent, className, pageNo);
                    setElementProcessing(className, false);
                    setScanningFinished(i);
                }
            });
		}
	}
	
	private void getElementXandYPosForWebTable(final int i, final WebView view, final String className, final int pageNo, final String divContent){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getElementXandYPos('"+i+"', '"+version+"')", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String locationXandY = getValuFromJsonreader(value);;
                    getWebTableElementSize(i, view, className, pageNo, divContent, locationXandY);
                }
            });
		}
	}
	
	private void getWebTableElementSize(final int i, final WebView view, final String className, final int pageNo, final String divContent, final String locationXandY){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getWebTableElementSize('"+i+"', '"+version+"')", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String webTableSize = getValuFromJsonreader(value);;
                    updateWebTableElementToDb(locationXandY, webTableSize, divContent, className, pageNo);
                    setElementProcessing(className, false);
                    setScanningFinished(i);
                }
            });
		}
	}
	
	private void getQuizElementLength(final int i, final WebView view, final String className, final int pageNo, final String divContent){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getQuizElementLength('"+i+"', '"+version+"');", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String quizElementLengthStr = getValuFromJsonreader(value);
                    int quizElementLength = Integer.parseInt(quizElementLengthStr) - 1;
                    int objUniqueId = db.getMaxUniqueRowID("objects")+1;
                    for (int j = 0; j < quizElementLength; j++) {
                        db.executeQuery("insert into tblMultiQuiz(quizId) values('"+objUniqueId+"')");
                    }
                    final ArrayList<QuizSequenceObject> quizSequenceObjectArray = db.getQuizSequenceArray(objUniqueId);
                    final HashMap<Integer, Integer> quizSectionMap = new HashMap<Integer, Integer>();
                    int j = 0;
                    getQuizElementId(i, j, view, className, pageNo, divContent, objUniqueId, quizSequenceObjectArray, quizSectionMap);
                }
            });
		}
	}
	
	private void getQuizElementId(final int i, final int j, final WebView view, final String className, final int pageNo, final String divContent, final int objUniqueId, final ArrayList<QuizSequenceObject> quizSequenceObjectArray, final HashMap<Integer, Integer> quizSectionMap){
		//for (int j = 0; j < quizSequenceObjectArray.size(); j++) {
			//kj = j2;
			final QuizSequenceObject quizSequence = quizSequenceObjectArray.get(j);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getQuizElementId('"+i+"', '"+j+"', '"+version+"')", new ValueCallback<String>() {

				@Override
				public void onReceiveValue(String value) {
					//value = trimDoubleQuotes(value);
					String quizElementId = getValuFromJsonreader(value);
					updateQuizElementDivContent(i, j, view, className, pageNo, objUniqueId, quizSequenceObjectArray, quizSectionMap, quizElementId, quizSequence, divContent);
				}

			});
		}
		//}
	}
	
	private void getImgElemLength(final int i, final WebView view, final String className, final int pageNo, final String divContent, final int objUniqueId, final HashMap<Integer, Integer> quizSectionMap){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getImgElemLength('"+i+"', '"+version+"');", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    value = getValuFromJsonreader(value);
                    boolean isLoopFinished = false;
                    int imgElemLength = Integer.parseInt(value);
                    for (int j = 0; j < imgElemLength; j++) {
                        if (j == imgElemLength-1) {
                            isLoopFinished = true;
                        }
                        getImgSrcElementForQuiz(i, j, view, className, pageNo, divContent, objUniqueId, quizSectionMap, isLoopFinished);
                    }
                }
            });
		}
	}
	
	private void getImgSrcElementForQuiz(final int i, int j, final WebView view, final String className, final int pageNo, final String divContent, final int objUniqueId, final HashMap<Integer, Integer> quizSectionMap, final boolean isLoopFinished){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			view.evaluateJavascript("javascript:getImgSrcElement('"+i+"', '"+j+"', '"+version+"');", new ValueCallback<String>() {

                @Override
                public void onReceiveValue(String value) {
                    //value = trimDoubleQuotes(value);
                    String imgSrc = getValuFromJsonreader(value);
                    updateQuizImageToFilesDir(imgSrc, objUniqueId, quizSectionMap);
                    if (isLoopFinished) {
                        getElementXandYPos(i, view, className, pageNo, divContent);
                    }
                }
            });
		}
	}
	
	private void setScanningFinished(int i){
		/*boolean classNameExist = false;
		for (int j = i+1; j < classNamesList.size(); j++) {
			String className = classNamesList.get(j);
			if (className.equals("WebText") || className.equals("TextSquare") || className.equals("TextRectangle") || className.equals("TextCircle") || className.equals(Globals.OBJTYPE_TITLETEXT) || className.equals(Globals.OBJTYPE_AUTHORTEXT) || className.equals(Globals.objType_pageNoText) || className.equals(Globals.OBJTYPE_QUIZWEBTEXT) || className.equals("Image") || className.equals("DrawImage") || className.equals("EmbedCodeBox") || className.equals("Audio") || className.equals("Iframe") || className.equals("YouTube")) {
				classNameExist = true;
			}
		}
		if (!classNameExist) {
			processPages();
		}*/
		if (!webTextProcessing && !webTableProcessing && !textSquareProcessing && !textRectangleProcessing && !textCircleProcessing && !titleTextProcessing && !authorTextProcessing && !pageNoTextProcessing && !quizWebTextProcessing && !imageProcessing && !drawnImageProcessing && !embedBoxProcessing && !audioProcessing && !iframeProcessing && !youtubeProcessing) {
			processPages(null);
		}
	}
	
	private void scanAndGetAllObjectsFromHtml(WebView view, int pageNo){
		String strLength = javascriptReturnValue("javascript:getElementsTagNameLength('"+version+"');", view);
		int elemLength = 0;
		if (strLength != "") {
			elemLength = Integer.parseInt(strLength);
		}
		for (int i = 0; i < elemLength; i++) {
			String className = javascriptReturnValue("javascript:getElementsClassName('"+i+"', '"+version+"');", view);
			if (className.equals("WebText") || className.equals("TextSquare") || className.equals("TextRectangle") || className.equals("TextCircle") || className.equals(Globals.OBJTYPE_TITLETEXT) || className.equals(Globals.OBJTYPE_AUTHORTEXT) || className.equals(Globals.objType_pageNoText) || className.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
				String divContent = javascriptReturnValue("javascript:getDivContentHTML('"+i+"', '"+version+"')", view);
				if (className.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
					String quizElementLengthStr = javascriptReturnValue("javascript:getQuizElementLength('"+i+"', '"+version+"');", view);
					int quizElementLength = Integer.parseInt(quizElementLengthStr) - 1;
					int objUniqueId = db.getMaxUniqueRowID("objects")+1;
					for (int j = 0; j < quizElementLength; j++) {
						db.executeQuery("insert into tblMultiQuiz(quizId) values('"+objUniqueId+"')");
					}
					ArrayList<QuizSequenceObject> quizSequenceObjectArray = db.getQuizSequenceArray(objUniqueId);
					HashMap<Integer, Integer> quizSectionMap = new HashMap<Integer, Integer>();
					for (int j = 0; j < quizSequenceObjectArray.size(); j++) {
						QuizSequenceObject quizSequence = quizSequenceObjectArray.get(j);
						String quizElementId = javascriptReturnValue("javascript:getQuizElementId('"+i+"', '"+j+"', '"+version+"')", view);
						divContent = updateQuizElementDivContent(quizElementId, quizSequence, quizSectionMap, divContent);
					}
					
					divContent = divContent.replace("value=\"CheckAnswer\"", "value=\"CheckAnswer\" style=\"visibility:hidden\"");
					divContent = divContent.replace("value=\"+\" style=\"visibility:hidden\"", "value=\"+\"");
					divContent = divContent.replace("value=\"-\" style=\"visibility:hidden\"", "value=\"-\"");
					String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/"+"images/";
					divContent = divContent.replace("src=\"Img_Thumb/", "src=\"/"+imgDir+"");
					//divContent = divContent.replace("src=\"Img_Thumb/", "src=\"images/");
					
					int imgElemLength = Integer.parseInt(javascriptReturnValue("javascript:getImgElemLength('"+i+"', '"+version+"');", view));
					for (int j = 0; j < imgElemLength; j++) {
						String imgSrc = javascriptReturnValue("javascript:getImgSrcElement('"+i+"', '"+j+"', '"+version+"');", view);
						updateQuizImageToFilesDir(imgSrc, objUniqueId, quizSectionMap);
					}
				}
				String locationXandY = javascriptReturnValue("javascript:getElementXandYPos('"+i+"', '"+version+"')", view);
				if (className.equals("webTable")) {
					String webTableSize = javascriptReturnValue("javascript:getWebTableElementSize('"+i+"', '"+version+"')", view);
					updateWebTableElementToDb(locationXandY, webTableSize, divContent, className, pageNo);
				} else {
					String webTextSize = javascriptReturnValue("javascript:getWebTextElementSize('"+i+"', '"+version+"')", view);
					updateWebTextToDb(locationXandY, webTextSize, divContent, className, pageNo);
				}
			} else if (className.equals("Image") || className.equals("DrawImage")) {
				String imageElemAttr = javascriptReturnValue("javascript:getImageElementSize('"+i+"', '"+version+"');", view);
				updateImageElementToDb(imageElemAttr, className, pageNo);
			} else if (className.equals("EmbedCodeBox")) {
				String embedElemAttr = javascriptReturnValue("javascript:getEmbedBoxSize('"+i+"', '"+version+"')", view);
				String embedCodeBoxContent = javascriptReturnValue("javascript:getDivContentHTML('"+i+"', '"+version+"')", view);
				updateEmbedElementToDb(embedElemAttr, embedCodeBoxContent, className, pageNo);
			} else if (className.equals("Audio")) {
				String audioElemAttr = javascriptReturnValue("javascript:getAudioElemContent('"+i+"', '"+version+"')", view);
				updateAudioElementToDb(audioElemAttr, className, pageNo);
			} else if (className.equals("Iframe")) {
				String iframeElemAttr = javascriptReturnValue("javascript:getIframeSize('"+i+"', '"+version+"')", view);
				updateIframeElementToDb(iframeElemAttr, className, pageNo);
			} else if (className.equals("YouTube")) {
				String youtubeElemAttr = javascriptReturnValue("javascript:getYoutubeSize('"+i+"', '"+version+"')", view);
				String youtubeHtmlId = javascriptReturnValue("javascript:getYoutubeHtmlId('"+i+"', '"+version+"')", view);
				updateYoutubeElementToDb(youtubeElemAttr, youtubeHtmlId, className, pageNo);
			}
		}
	}
	
	private void updateQuizImageToFilesDir(String imgSrc, int objUniqueId, HashMap<Integer, Integer> quizSectionMap){
		String[] imgSrcSplit = imgSrc.split("/");
		String[] imgSrcSplit1 = imgSrcSplit[(imgSrcSplit.length)-1].split("\\?");
		String quizImgName = imgSrcSplit1[0];
		String[] quizImgNameSplit = quizImgName.split("_");
		int oldImgQuizSecId = Integer.parseInt(quizImgNameSplit[0].split("q")[1]);
		int oldImgQuizUniId = Integer.parseInt(quizImgNameSplit[(quizImgNameSplit.length)-1].replace(".png", ""));
		int newUniqueId = objUniqueId;
		int newSecId = quizSectionMap.get(oldImgQuizSecId);
		
		quizImgName = quizImgName.replace("q"+oldImgQuizSecId+"_img_"+oldImgQuizUniId+"", "q"+newSecId+"_img_"+newUniqueId+"");
		quizImgName = quizImgName.replace("q"+oldImgQuizSecId+"_img_a_"+oldImgQuizUniId+"", "q"+newSecId+"_img_a_"+newUniqueId+"");
		quizImgName = quizImgName.replace("q"+oldImgQuizSecId+"_img_b_"+oldImgQuizUniId+"", "q"+newSecId+"_img_b_"+newUniqueId+"");
		quizImgName = quizImgName.replace("q"+oldImgQuizSecId+"_img_c_"+oldImgQuizUniId+"", "q"+newSecId+"_img_c_"+newUniqueId+"");
		quizImgName = quizImgName.replace("q"+oldImgQuizSecId+"_img_d_"+oldImgQuizUniId+"", "q"+newSecId+"_img_d_"+newUniqueId+"");
		quizImgName = quizImgName.replace("q"+oldImgQuizSecId+"_img_e_"+oldImgQuizUniId+"", "q"+newSecId+"_img_e_"+newUniqueId+"");
		quizImgName = quizImgName.replace("q"+oldImgQuizSecId+"_img_f_"+oldImgQuizUniId+"", "q"+newSecId+"_img_f_"+newUniqueId+"");
		
		String storeBookImgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/"+imgSrc;
		storeBookImgPath = storeBookImgPath.split("\\?")[0];
		String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/"+"images/";
		File fileImgDir = new File(imgDir);
		if (!fileImgDir.exists()) {
			fileImgDir.mkdir();
		}
		String bookImagePath = imgDir+quizImgName;
		UserFunctions.copyFiles(storeBookImgPath, bookImagePath);
	}
	
	private String updateQuizElementDivContent(String quizElementId,
			QuizSequenceObject quizSequence,
			HashMap<Integer, Integer> quizSectionMap, String divContent) {
		String[] quizElemIdSplit = quizElementId.split("_");
		int oldQuizUniqId = Integer.parseInt(quizElemIdSplit[1]);
		int oldQuizSecId = Integer.parseInt(quizElemIdSplit[0].split("q")[1]);
		int newQuizUniqId = quizSequence.getUniqueid();
		int newQuizSecId = quizSequence.getSectionId();
		quizSectionMap.put(oldQuizSecId, newQuizSecId);
		
		divContent = divContent.replace("previousquestion:q"+oldQuizSecId+"_"+oldQuizUniqId+":"+oldQuizSecId+"", "previousquestion:q"+newQuizSecId+"_"+newQuizUniqId+":"+newQuizSecId+"");
		divContent = divContent.replace("deletequestion:q"+oldQuizSecId+"_"+oldQuizUniqId+":"+oldQuizSecId+"", "deletequestion:q"+newQuizSecId+"_"+newQuizUniqId+":"+newQuizSecId+"");
		divContent = divContent.replace("addquestions:q"+oldQuizSecId+"_"+oldQuizUniqId+":"+oldQuizSecId+"", "addquestions:q"+newQuizSecId+"_"+newQuizUniqId+":"+newQuizSecId+"");
		divContent = divContent.replace("nextquestion:q"+oldQuizSecId+"_"+oldQuizUniqId+":"+oldQuizSecId+"", "nextquestion:q"+newQuizSecId+"_"+newQuizUniqId+":"+newQuizSecId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_"+oldQuizUniqId+"", "q"+newQuizSecId+"_"+newQuizUniqId+"");
		divContent = divContent.replace("Title"+oldQuizSecId+"", "Title"+newQuizSecId+"");
		divContent = divContent.replace("quizTitle_"+oldQuizUniqId+"_"+oldQuizSecId+"", "quizTitle_"+newQuizUniqId+"_"+newQuizSecId+"");
		divContent = divContent.replace("pTitle_"+oldQuizUniqId+"", "pTitle_"+newQuizUniqId+"");
		divContent = divContent.replace("quiz_question_"+oldQuizUniqId+"_"+oldQuizSecId+"", "quiz_question_"+newQuizUniqId+"_"+newQuizSecId+"");
		divContent = divContent.replace("pQuiz_"+oldQuizUniqId+"", "pQuiz_"+newQuizUniqId+"");
		divContent = divContent.replace("quiz_options_"+oldQuizUniqId+"_"+oldQuizSecId+"", "quiz_options_"+newQuizUniqId+"_"+newQuizSecId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_result_"+oldQuizUniqId+"", "q"+newQuizSecId+"_result_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_div_a_"+oldQuizUniqId+"", "q"+newQuizSecId+"_div_a_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"a_"+oldQuizUniqId+"", "q"+newQuizSecId+"a_"+newQuizUniqId+"");
		divContent = divContent.replace("btnCheckAnswer"+oldQuizSecId+"_"+oldQuizUniqId+"", "btnCheckAnswer"+newQuizSecId+"_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_div_b_"+oldQuizUniqId+"", "q"+newQuizSecId+"_div_b_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"b_"+oldQuizUniqId+"", "q"+newQuizSecId+"b_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_div_c_"+oldQuizUniqId+"", "q"+newQuizSecId+"_div_c_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"c_"+oldQuizUniqId+"", "q"+newQuizSecId+"c_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_div_d_"+oldQuizUniqId+"", "q"+newQuizSecId+"_div_d_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"d_"+oldQuizUniqId+"", "q"+newQuizSecId+"d_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_div_e_"+oldQuizUniqId+"", "q"+newQuizSecId+"_div_e_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"e_"+oldQuizUniqId+"", "q"+newQuizSecId+"e_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_div_f_"+oldQuizUniqId+"", "q"+newQuizSecId+"_div_f_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"f_"+oldQuizUniqId+"", "q"+newQuizSecId+"f_"+newQuizUniqId+"");
		divContent = divContent.replace("quiz_checkanswer_"+oldQuizUniqId+"", "quiz_checkanswer_"+newQuizUniqId+"");
		divContent = divContent.replace("btnNext"+oldQuizSecId+"_"+oldQuizUniqId+"", "btnNext"+newQuizSecId+"_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_img_"+oldQuizUniqId+"", "q"+newQuizSecId+"_img_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_img_a_"+oldQuizUniqId+"", "q"+newQuizSecId+"_img_a_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_img_b_"+oldQuizUniqId+"", "q"+newQuizSecId+"_img_b_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_img_c_"+oldQuizUniqId+"", "q"+newQuizSecId+"_img_c_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_img_d_"+oldQuizUniqId+"", "q"+newQuizSecId+"_img_d_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_img_e_"+oldQuizUniqId+"", "q"+newQuizSecId+"_img_e_"+newQuizUniqId+"");
		divContent = divContent.replace("q"+oldQuizSecId+"_img_f_"+oldQuizUniqId+"", "q"+newQuizSecId+"_img_f_"+newQuizUniqId+"");
		divContent = divContent.replace("results_container_"+oldQuizUniqId+"", "results_container_"+newQuizUniqId+"");
		divContent = divContent.replace("results_"+oldQuizUniqId+"", "results_"+newQuizUniqId+"");
		divContent = divContent.replace("quiz_result_cont_"+oldQuizUniqId+"", "quiz_result_cont_"+newQuizUniqId+"");
		
		return divContent;
	}
	
	private String updateQuizElementDivContent(int i, int j, WebView view, String className, int pageNo, int objUniqueId, ArrayList<QuizSequenceObject> quizSequenceObjectArray, HashMap<Integer, Integer> quizSectionMap, String quizElementId, QuizSequenceObject quizSequence, String divContent){
		divContent = updateQuizElementDivContent(quizElementId, quizSequence, quizSectionMap, divContent);
		j = j+1;
		if (version == "KITKAT") {
			if (j < quizSequenceObjectArray.size()) {
				getQuizElementId(i, j, view, className, pageNo, divContent, objUniqueId, quizSequenceObjectArray, quizSectionMap);
			} else {
				divContent = divContent.replace("value=\"CheckAnswer\"", "value=\"CheckAnswer\" style=\"visibility:hidden\"");
				divContent = divContent.replace("value=\"+\" style=\"visibility:hidden\"", "value=\"+\"");
				divContent = divContent.replace("value=\"-\" style=\"visibility:hidden\"", "value=\"-\"");
				String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/"+"images/";
				divContent = divContent.replace("src=\"Img_Thumb/", "src=\"/"+imgDir+"");
				
				getImgElemLength(i, view, className, pageNo, divContent, objUniqueId, quizSectionMap);
			}
		}
		return divContent;
	}
	
	private void updateWebTableElementToDb(String locationXandY, String webTableSize, String divContent, String className, int pageNo){
		String[] splitLocationXandY = locationXandY.split("\\|");
		int Xpos = Math.round(Float.parseFloat(splitLocationXandY[0].replace("px", "")));
		int Ypos = Math.round(Float.parseFloat(splitLocationXandY[1].replace("px", "")));
		Xpos = Globals.getDeviceIndependentPixels(Xpos, context);
		Ypos = Globals.getDeviceIndependentPixels(Ypos, context);
		String location = Xpos+"|"+Ypos;
		String[] splitWebTableSize = webTableSize.split("\\|");
		int objWidth = Math.round(Float.parseFloat(splitWebTableSize[0].replace("px", "")));
		int objHeight = Math.round(Float.parseFloat(splitWebTableSize[1].replace("px", "")));
		objWidth = Globals.getDeviceIndependentPixels(objWidth, context);
		objHeight = Globals.getDeviceIndependentPixels(objHeight, context);
		int objRowCount = Math.round(Float.parseFloat(splitWebTableSize[2].replace("px", "")));
		int objColumnCount = Math.round(Float.parseFloat(splitWebTableSize[3].replace("px", "")));
		String size = objWidth+"|"+objHeight+"|"+objRowCount+"|"+objColumnCount;
		divContent = divContent.replace("contenteditable=\"false\"", "contenteditable=\"true\"");
		divContent = divContent.replace("'", "''");
		className = "WebTable";
		boolean objLocked = false;
		int enrichedId = 0;
		db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+className+"', '"+newBookId+"', '"+pageNo+"', '"+location+"', '"+size+"', '"+divContent+"', '"+sequentialId+++"', '"+"no"+"', '"+objLocked+"', '"+enrichedId+"')");
	}
	
	private void updateWebTextToDb(String locationXandY, String webTextSize, String divContent, String className, int pageNo){
		String[] splitLocationXandY = locationXandY.split("\\|");
		int Xpos = Math.round(Float.parseFloat(splitLocationXandY[0].replace("px", "")));
		int Ypos = Math.round(Float.parseFloat(splitLocationXandY[1].replace("px", "")));
		Xpos = Globals.getDeviceIndependentPixels(Xpos, context);
		Ypos = Globals.getDeviceIndependentPixels(Ypos, context);
		String location = Xpos+"|"+Ypos;
		String[] splitWebTextSize = webTextSize.split("\\|");
		int objWidth = Math.round(Float.parseFloat(splitWebTextSize[0].replace("px", "")));
		int objHeight = Math.round(Float.parseFloat(splitWebTextSize[1].replace("px", "")));
		objWidth = Globals.getDeviceIndependentPixels(objWidth, context);
		objHeight = Globals.getDeviceIndependentPixels(objHeight, context);
		String size = objWidth+"|"+objHeight;
		divContent = divContent.replace("'", "''");
		boolean objLocked = false;
		int enrichedId = 0;
		db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+className+"', '"+newBookId+"', '"+pageNo+"', '"+location+"', '"+size+"', '"+divContent+"', '"+sequentialId+++"', '"+"no"+"', '"+objLocked+"', '"+enrichedId+"')");
	}
	
	private void updateYoutubeElementToDb(String youtubeElemAttr, String youtubeHtmlId, String className, int pageNo){
		String[] splitYoutubeElemAttr = youtubeElemAttr.split("#\\|#");
		int objXpos = Math.round(Float.parseFloat(splitYoutubeElemAttr[0].replace("px", "")));
		int objYpos = Math.round(Float.parseFloat(splitYoutubeElemAttr[1].replace("px", "")));
		objXpos = Globals.getDeviceIndependentPixels(objXpos, context);
		objYpos = Globals.getDeviceIndependentPixels(objYpos, context);
		String location = objXpos+"|"+objYpos;
		int objWidth = Math.round(Float.parseFloat(splitYoutubeElemAttr[2].replace("px", "")));
		int objHeight = Math.round(Float.parseFloat(splitYoutubeElemAttr[3].replace("px", "")));
		objWidth = Globals.getDeviceIndependentPixels(objWidth, context);
		objHeight = Globals.getDeviceIndependentPixels(objHeight, context);
		String size = objWidth+"|"+objHeight;
		String youtubeSrc = splitYoutubeElemAttr[4];
		if (youtubeSrc.contains("youtube.com")) {
			String[] idSplit = youtubeHtmlId.split("_");
			
			String storeImgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/"+idSplit[1]+".png";
			int objUniqueId = db.getMaxUniqueRowID("objects")+1;
			String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/"+"images/";
			File fileImgDir = new File(imgDir);
			if (!fileImgDir.exists()) {
				fileImgDir.mkdir();
			}
			String bookImgPath = imgDir+objUniqueId+".png";
			UserFunctions.copyFiles(storeImgPath, bookImgPath);
		} else {
			String imgSrc = youtubeSrc.replace(".mp4", ".png");
			String storeVideoPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/"+youtubeSrc;
			storeVideoPath = storeVideoPath.replace("../", "");
			storeVideoPath = storeVideoPath.split("\\?")[0];
			
			String storeImgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/"+imgSrc;
			storeImgPath = storeImgPath.replace("../", "");
			storeImgPath = storeImgPath.split("\\?")[0];
			
			int objUniqueId = db.getMaxUniqueRowID("objects")+1;
			String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/"+"images/";
			File fileImgDir = new File(imgDir);
			if (!fileImgDir.exists()) {
				fileImgDir.mkdir();
			}
			String bookVideoPath = imgDir+objUniqueId+".mp4";
			String bookImgPath = imgDir+objUniqueId+".png";
			UserFunctions.copyFiles(storeVideoPath, bookVideoPath);
			UserFunctions.copyFiles(storeImgPath, bookImgPath);
			youtubeSrc = bookVideoPath;
		}
		boolean objLocked = false;
		int enrichedId = 0;
		db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+className+"', '"+newBookId+"', '"+pageNo+"', '"+location+"', '"+size+"', '"+youtubeSrc+"', '"+sequentialId+++"', '"+"no"+"', '"+objLocked+"', '"+enrichedId+"')");
	}
	
	private void updateIframeElementToDb(String iframeElemAttr, String className, int pageNo){
		String[] splitIframeElemAttr = iframeElemAttr.split("#\\|#");
		int objXpos = Math.round(Float.parseFloat(splitIframeElemAttr[0].replace("px", "")));
		int objYpos = Math.round(Float.parseFloat(splitIframeElemAttr[1].replace("px", "")));
		objXpos = Globals.getDeviceIndependentPixels(objXpos, context);
		objYpos = Globals.getDeviceIndependentPixels(objYpos, context);
		String location = objXpos+"|"+objYpos;
		int objWidth = Math.round(Float.parseFloat(splitIframeElemAttr[2].replace("px", "")));
		int objHeight = Math.round(Float.parseFloat(splitIframeElemAttr[3].replace("px", "")));
		objWidth = Globals.getDeviceIndependentPixels(objWidth, context);
		objHeight = Globals.getDeviceIndependentPixels(objHeight, context);
		String size = objWidth+"|"+objHeight;
		
		String iframeSrc = splitIframeElemAttr[4];
		boolean objLocked = false;
		int enrichedId = 0;
		db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+"IFrame"+"', '"+newBookId+"', '"+pageNo+"', '"+location+"', '"+size+"', '"+iframeSrc+"', '"+sequentialId+++"', '"+"no"+"', '"+objLocked+"', '"+enrichedId+"')");
	}
	
	private void updateAudioElementToDb(String audioElemAttr, String className, int pageNo){
		String[] splitAudioElemAttr = audioElemAttr.split("#\\|#");
		int objXpos = Math.round(Float.parseFloat(splitAudioElemAttr[0].replace("px", "")));
		int objYpos = Math.round(Float.parseFloat(splitAudioElemAttr[1].replace("px", "")));
		objXpos = Globals.getDeviceIndependentPixels(objXpos, context);
		objYpos = Globals.getDeviceIndependentPixels(objYpos, context);
		String location = objXpos+"|"+objYpos;
		int objWidth = Math.round(Float.parseFloat(splitAudioElemAttr[2].replace("px", "")));
		int objHeight = Math.round(Float.parseFloat(splitAudioElemAttr[3].replace("px", "")));
		objWidth = Globals.getDeviceIndependentPixels(objWidth, context);
		objHeight = Globals.getDeviceIndependentPixels(objHeight, context);
		String size = objWidth+"|"+objHeight;
		String audioOnClickAttr = splitAudioElemAttr[4];
		//System.out.println(audioOnClickAttr);
		String[] splitAudioOnClickAttr = audioOnClickAttr.split("'");
		String[] splitAudioOnClickAttr1 = splitAudioOnClickAttr[1].split("/");
		
		String storeAudioFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/"+splitAudioOnClickAttr[1];
		int objUniqueId = db.getMaxUniqueRowID("objects")+1;
		String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/"+"images/";
		File fileImgDir = new File(imgDir);
		if (!fileImgDir.exists()) {
			fileImgDir.mkdir();
		}
		File audio_play_default_path = new File(imgDir+"Default_Audio_Play.png");
		File audio_stop_default_path = new File(imgDir+"Default_Audio_Stop.png");
		if (!(audio_play_default_path.exists() || audio_stop_default_path.exists())) {
			UserFunctions.copyAsset(context.getAssets(), "Default_Audio_Play.png", audio_play_default_path.toString());
			UserFunctions.copyAsset(context.getAssets(), "Default_Audio_Stop.png", audio_stop_default_path.toString());
		}
		String audioFilePath = imgDir+objUniqueId+".m4a";
		UserFunctions.copyFiles(storeAudioFilePath, audioFilePath);
		boolean objLocked = false;
		int enrichedId = 0;
		db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+className+"', '"+newBookId+"', '"+pageNo+"', '"+location+"', '"+size+"', '"+audioFilePath+"', '"+sequentialId+++"', '"+"no"+"', '"+objLocked+"', '"+enrichedId+"')");
	}
	
	private void updateEmbedElementToDb(String embedElemAttr, String embedCodeBoxContent, String className, int pageNo){
		String[] splitEmbedElemAttr = embedElemAttr.split("\\|");
		int objXpos = Math.round(Float.parseFloat(splitEmbedElemAttr[0].replace("px", "")));
		int objYpos = Math.round(Float.parseFloat(splitEmbedElemAttr[1].replace("px", "")));
		objXpos = Globals.getDeviceIndependentPixels(objXpos, context);
		objYpos = Globals.getDeviceIndependentPixels(objYpos, context);
		String location = objXpos+"|"+objYpos;
		int objWidth = Math.round(Float.parseFloat(splitEmbedElemAttr[2].replace("px", "")));
		int objHeight = Math.round(Float.parseFloat(splitEmbedElemAttr[3].replace("px", "")));
		objWidth = Globals.getDeviceIndependentPixels(objWidth, context);
		objHeight = Globals.getDeviceIndependentPixels(objHeight, context);
		String size = objWidth+"|"+objHeight;
		embedCodeBoxContent = embedCodeBoxContent.replace("'", "''");
		boolean objLocked = false;
		int enrichedId = 0;
		db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+"EmbedCode"+"', '"+newBookId+"', '"+pageNo+"', '"+location+"', '"+size+"', '"+embedCodeBoxContent+"', '"+sequentialId+++"', '"+"no"+"', '"+objLocked+"', '"+enrichedId+"')");
	}
	
	private void updateImageElementToDb(String imageElemAttr, String className, int pageNo){
		String[] splitImageElemAttr = imageElemAttr.split("\\|");
		int objXpos = Math.round(Float.parseFloat(splitImageElemAttr[0].replace("px", "")));
		int objYpos = Math.round(Float.parseFloat(splitImageElemAttr[1].replace("px", "")));
		objXpos = Globals.getDeviceIndependentPixels(objXpos, context);
		objYpos = Globals.getDeviceIndependentPixels(objYpos, context);
		String location = objXpos+"|"+objYpos;
		int objWidth = Math.round(Float.parseFloat(splitImageElemAttr[2]));
		int objHeight = Math.round(Float.parseFloat(splitImageElemAttr[3]));
		objWidth = Globals.getDeviceIndependentPixels(objWidth, context);
		objHeight = Globals.getDeviceIndependentPixels(objHeight, context);
		String size = objWidth+"|"+objHeight;
		
		String storeBookImgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/"+splitImageElemAttr[5];
		int objUniqueId = db.getMaxUniqueRowID("objects")+1;
		String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/"+"images/";
		File fileImgDir = new File(imgDir);
		if (!fileImgDir.exists()) {
			fileImgDir.mkdir();
		}
		String bookImagePath = imgDir+objUniqueId+".png";
		UserFunctions.copyFiles(storeBookImgPath, bookImagePath);
		boolean objLocked = false;
		int enrichedId = 0;
		if (className.equals("DrawImage")) {
			className = "DrawnImage";
		}
		db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+className+"', '"+newBookId+"', '"+pageNo+"', '"+location+"', '"+size+"', '"+bookImagePath+"', '"+sequentialId+++"', '"+"no"+"', '"+objLocked+"', '"+enrichedId+"')");
	}
	
	@Override
	public void sojiSetDocumentOffsetWidthAndHeight(String offsetWidthAndHeight) {
		/*String[] splitOffsetWidthAndHeight = offsetWidthAndHeight.split("\\|");
		int offsetWidth = Integer.parseInt(splitOffsetWidthAndHeight[0]);
		int offsetHeight = Integer.parseInt(splitOffsetWidthAndHeight[1]);
		//System.out.println("offsetWidth:"+offsetWidth+"offsetheight:"+offsetHeight);*/
		if (enrich != null){
			((Book) this.enrich.fa_enrich.gridShelf.bookListArray.get(0)).set_bOffsetWidthAndHeight(offsetWidthAndHeight);
	    }else if (textCircularProgressBar!=null){
			((Book) this.activity.gridShelf.bookListArray.get(0)).set_bOffsetWidthAndHeight(offsetWidthAndHeight);
		}else if(cloudData!=null){
			((Book) this.cloudData.gridShelf.bookListArray.get(0)).set_bOffsetWidthAndHeight(offsetWidthAndHeight);
		}
		else{
			((Book) this.MainActivity.gridShelf.bookListArray.get(0)).set_bOffsetWidthAndHeight(offsetWidthAndHeight);
	     }
		db.executeQuery("update books set OffsetWidthAndHeight='"+offsetWidthAndHeight+"' where BID='"+newBookId+"'");
	}

	
	/**
	 * Capture ScreenShot of webView and save it to the files directory
	 * @param view
	 * @param pageNo
	 */
	public void saveScreenshotforPage(WebView view, int pageNo) {
		Bitmap bitmap;
		if (new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/fpimages/"+pageNo+".jpg").exists()){
			bitmap = BitmapFactory.decodeFile(Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/fpimages/"+pageNo+".jpg");
		}else {
			bitmap = takeScreenShot(view);
		}
		if (bitmap!=null) {
			String freeFilesPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + newBookId + "/FreeFiles/";
			String pageScreenShotPath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + newBookId + "/";
			if (pageNo == 1) {
				String front_port_path = freeFilesPath + "front_P.png";
				if (!new File(front_port_path).exists()) {
					UserFunctions.saveBitmapImage(bitmap, front_port_path);
				}

				String front_thumb_path = freeFilesPath + "frontThumb.png";
				if (!new File(front_thumb_path).exists()) {
					Bitmap frontThumbBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(130, context), Globals.getDeviceIndependentPixels(170, context), true);
					UserFunctions.saveBitmapImage(frontThumbBitmap, front_thumb_path);
					String thumb_path = freeFilesPath + "thumb.png";
					UserFunctions.saveBitmapImage(frontThumbBitmap, thumb_path);
				}

				String card_path = freeFilesPath + "card.png";
				if (!new File(card_path).exists()) {
					Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(200, context), Globals.getDeviceIndependentPixels(300, context), true);
					UserFunctions.saveBitmapImage(cardBitmap, card_path);
				}
			} else if (pageNo == totalPages) {
				String back_port_path = freeFilesPath + "back_P.png";
				if (!new File(back_port_path).exists()) {
					UserFunctions.saveBitmapImage(bitmap, back_port_path);
				}

				String back_thumb_path = freeFilesPath + "backThumb.png";
				if (!new File(back_thumb_path).exists()) {
					Bitmap backThumbBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(130, context), Globals.getDeviceIndependentPixels(170, context), true);
					UserFunctions.saveBitmapImage(backThumbBitmap, back_thumb_path);
				}
			} else {
				String pageThumbPath = pageScreenShotPath + "thumbnail_" + pageNo + ".png";
				if (!new File(pageThumbPath).exists()) {
					Bitmap pageThumbBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(130, context), Globals.getDeviceIndependentPixels(170, context), true);
					UserFunctions.saveBitmapImage(pageThumbBitmap, pageThumbPath);
				}
			}
		}
	}
	
	/**
	 * Capture bitmap image of the view
	 * @param view
	 * @return
	 */
	public Bitmap takeScreenShot(WebView view) {
		view.destroyDrawingCache();
		view.setDrawingCacheEnabled(true);
		view.setDrawingCacheQuality(RelativeLayout.DRAWING_CACHE_QUALITY_AUTO);
		Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
//		view.setDrawingCacheEnabled(false);
		view.buildDrawingCache();
//		Picture picture = view.capturePicture();
//		Bitmap b = Bitmap.createBitmap(picture.getWidth(), picture.getHeight(), Bitmap.Config.ARGB_8888);
//		Canvas c = new Canvas(b);
//		picture.draw(c);
		return bitmap;
	}

	/**
	 * Copy all the freefiles images from store book to newly created book
	 */
	public void copyFreeFiles() {
		String sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/FreeFiles";
		String destDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/FreeFiles";
		UserFunctions.createNewBooksDirAndCoptTemplates(sourceDir, destDir);
	}

	@Override
	public void sojiSetTagCoordinates(String content, String currentPageNo) {
		//String tagCoordinates = content;
		////System.out.println("TagCoordinates:"+tagCoordinates);
		updateImageTagCoordinatesIndb(content, currentPageNo);
	}
	
	/**
	 * Update all the Image tag coordinates to the database
	 * @param content
	 * @param currentPageNo
	 */
	public void updateImageTagCoordinatesIndb(String content,  String currentPageNo){
		String[] imgContentArray = content.split("\\$");
		for (int i = 0; i < imgContentArray.length; i++) {
			String[] imgContent = imgContentArray[i].split("#");
			if (imgContent.length > 3) {
				String storeBookImgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/"+imgContent[4];
				storeBookImgPath = storeBookImgPath.replace("../", "");
				int objUniqueId = db.getMaxUniqueRowID("objects")+1;
				String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/"+"images/";
				File fileImgDir = new File(imgDir);
				if (!fileImgDir.exists()) {
					fileImgDir.mkdir();
				}
				String bookImagePath = imgDir+objUniqueId+".png";
				UserFunctions.copyFiles(storeBookImgPath, bookImagePath);
				
				int objPageNo = Integer.parseInt(currentPageNo);
				int objXpos = Math.round(Float.parseFloat(imgContent[0]));
				int objYpos = Math.round(Float.parseFloat(imgContent[1]));
				String location = objXpos+"|"+objYpos;
				int objWidth = Math.round(Float.parseFloat(imgContent[2]));
				int objHeight = Math.round(Float.parseFloat(imgContent[3]));
				String size = objWidth+"|"+objHeight;
				boolean objLocked = false;
				int enrichedId = 0;
				
				db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+"Image"+"', '"+newBookId+"', '"+objPageNo+"', '"+location+"', '"+size+"', '"+bookImagePath+"', '"+sequentialId+++"', '"+"no"+"', '"+objLocked+"', '"+enrichedId+"')");
			}
		}
	}

	@Override
	public void sojiSetWebContent(String _webContent, String currentPageNo) {
		String webContent = _webContent;
		////System.out.println("WebContent:"+webContent);
		processWebPage(webContent, currentPageNo);
	}
	
	/**
	 * Process whole content to and place it in the db as main web text
	 * @param webPageContent
	 * @param currentPageNo
	 */
	public void processWebPage(String webPageContent, String currentPageNo) {
		String mainDiv;
		if (htmlDir != null && htmlDir.equals("rtl")) {
			mainDiv = "<div id=\"content\" dir=\"rtl\" contenteditable=\"true\" style=\"background-color:transparent;\">";
		} else if (htmlDir != null && htmlDir.equals("ltr")) {
			mainDiv = "<div id=\"content\" dir=\"ltr\" contenteditable=\"true\" style=\"background-color:transparent;\">";
		} else {
			mainDiv = "<div id=\"content\" contenteditable=\"true\" style=\"background-color:transparent;\">";
		}
		
		mainDiv = mainDiv.concat(webPageContent).concat("</div>");
		mainDiv = filterImageTags(mainDiv);
		
		mainDiv = mainDiv.replace("background=\"", "background=\"file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/");
		mainDiv = mainDiv.replace("'", "''");
		
		String objType = "WebText";
		int objPageNo = Integer.parseInt(currentPageNo);
		String location = "0|0";
		//String size = Globals.getDesignPageWidth()+"|"+Globals.getDesignPageHeight();
		int width = this.getWidth();
		int height = this.getHeight();
		String size = width+"|"+height;
		boolean objLocked = false;
		int enrichedId = 0;
		
		db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+objType+"', '"+newBookId+"', '"+objPageNo+"', '"+location+"', '"+size+"', '"+mainDiv+"', '"+sequentialId+++"', '"+"yes"+"', '"+objLocked+"', '"+enrichedId+"')");
	}
	
	/**
	 * Filer all the Image tags in HML page and set its visibility to hidden
	 * @param content
	 * @return
	 */
	private String filterImageTags(String content) {
		String replaceImageTag = "<img style=\"visibility:hidden;\"";
		content = content.replace("<img", replaceImageTag);
		content = content.replace("src=\"../", "src=");
		content = content.replace("background=\"../", "background=\"");
		return content;
	}

	@Override
	public void sojiSetTableBackgroundContent(String _tblBgContent, String currentPageNo) {
		String tblBgContent = _tblBgContent;
		////System.out.println("tblBgContent:"+tblBgContent);
		copyTableBackgroundImage(tblBgContent, currentPageNo);
	}
	
	/**
	 * copy all the table background images to files book directory 
	 * @param imgContent
	 * @param currentPageNo
	 */
	private void copyTableBackgroundImage(String imgContent, String currentPageNo){
		String[] bgSplit = imgContent.split("\\$");
		String storeBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bName+"/";
		String sbaBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBookId+"/";
		for (int i = 0; i < bgSplit.length; i++) {
			if (!bgSplit[i].isEmpty()) {
				String imgStorePath = storeBookPath+bgSplit[i];
				String[] pathArray = bgSplit[i].split("/");
				if (pathArray.length > 1) {
					File finalImgDir = new File(sbaBookPath+pathArray[0]);
					if (!finalImgDir.exists()) {
						finalImgDir.mkdir();
						//UserFunctions.makeFileWorldReadable(finalImgDir.toString());
					}
					String finalImage = finalImgDir.toString()+"/"+pathArray[1];
					UserFunctions.copyFiles(imgStorePath, finalImage);
					//UserFunctions.makeFileWorldReadable(finalImage);
				} else {
					String finalImage = sbaBookPath+"FreeFiles/"+bgSplit[i];
					UserFunctions.copyFiles(imgStorePath, finalImage);
					//UserFunctions.makeFileWorldReadable(finalImage);
				}
			}
		}
	}
	
	/**
	 * trims double Quotes
	 * @param text
	 * @return
	 */
	public static String trimDoubleQuotes(String text){
		int textLength = text.length();
		if (textLength >= 2 && text.charAt(0) == '"' && text.charAt(textLength - 1) == '"') {
			return text.substring(1, textLength - 1);
		}
		return text;
	}
	
	

	/**
	 * set the html direction of the page
	 */
	@Override
	public void sojiSetHtmlDir(String _htmlDir) {
		this.htmlDir = _htmlDir;
	}
	
	private String javascriptReturnValue(final String script, final WebView view){
		//int index = evalJsIndex.incrementAndGet();
		int index = 0;
		loadJSScript(script, view);
		return waitForJsReturnValue(index, 1000);
	}
	
	 private String waitForJsReturnValue(int index, int waitMs) {

		    long start = System.currentTimeMillis();

		    while (true) {
		    	try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
		        long elapsed = System.currentTimeMillis() - start;
		        if (elapsed > waitMs)
		            break;
		        synchronized (jsReturnValueLock) {
		            String value = jsReturnValues.remove(index);
		            if (value != null)
		                return value;

		            long toWait = waitMs - (System.currentTimeMillis() - start);
		            if (toWait > 0)
		                try {
		                    jsReturnValueLock.wait(toWait);
		                } catch (InterruptedException e) {
		                    break;
		                }
		            else
		                break;
		        }
		    }
		    //Log.e("MyActivity", "Giving up; waited " + (waitMs/1000) + "sec for return value " + index);
		    return "";
		}

	
	/**
	 * this loads the javascript by comparing the version
	 * @param script
	 * @param view
	 */
	 private void loadJSScript(final String script, final WebView view){
		 if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT){
			view.evaluateJavascript(script, null);
		 } else {
			 view.loadUrl(script);
		 }
	 }

	@Override
	public void sojiSetIsBookFromManahijAndroid(int index, String isBookFromManahijAndroid) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, isBookFromManahijAndroid);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetElementsTagNameLength(String length) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, length);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetElementsClassName(String className) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, className);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetDivContentHTML(String content) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, content);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetElementXandYPos(String location) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, location);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetWebTableElementSize(String size) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, size);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetImageElementSize(String imgElemAttr) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, imgElemAttr);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetEmbedBoxSize(String embedBoxAttr) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, embedBoxAttr);
			jsReturnValueLock.notifyAll();
		}		
	}

	@Override
	public void sojiSetIframeSize(String iframeElemAttr) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, iframeElemAttr);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetYoutubeSize(String youtubeElemAttr) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, youtubeElemAttr);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetQuizElementLength(String quizElementLength) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, quizElementLength);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetQuizElementId(String quizElemId) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, quizElemId);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetImgElemLength(String imgElemLength) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, imgElemLength);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojisetWebTextElementSize(String size) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, size);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojisetImgSrcElement(String imgSrc) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, imgSrc);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojisetAudioElemContent(String audioElemAttr) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, audioElemAttr);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetYoutubeHtmlId(String youtubeHtmlId) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, youtubeHtmlId);
			jsReturnValueLock.notifyAll();
		}
	}

	@Override
	public void sojiSetExternalCSSPath(String path) {
		String[] scalePage=path.split("\\|");
		db.executeQuery("update objects set ScalePageToFit='"+scalePage[0]+"' where BID='"+newBook.getBookID()+"' and pageNo='"+scalePage[1]+"'");
	}

}
