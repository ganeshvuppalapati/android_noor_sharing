package com.semanoor.sboookauthor_store;

import java.util.ArrayList;

import android.content.Context;


public class ListViewItem {

	private String tabTitle;
	private String tabType;
	private Context context;
	private String tabPath;
	private ArrayList<ListViewItem> listViewItem = new ArrayList<ListViewItem>();
	private int nodeType;

	public ListViewItem(Context _context) {
		this.context = _context;
	}

	/*
	 * @return the enrichmentTitle
	 */
	public String getTabTitle() {
		return tabTitle;
	}
	/**
	 * @param tabTitle the tabTitle to set
	 */
	public void setTabTitle(String tabTitle) {
		this.tabTitle = tabTitle;
	}

	/**
	 * @return the tabType
	 */
	public String getTabType() {
		return tabType;
	}
	/**
	 * @param tabType the tabType to set
	 */
	public void settabType(String tabType) {
		this.tabType = tabType;
	}
	/**
	 * @return the tabPath
	 */
	public String getTabPath() {
		return tabPath;
	}
	/**
	 * @param tabPath the tabPath to set
	 */
	public void setTabPath(String tabPath) {
		this.tabPath =tabPath;
	}

	/**
	 * @return the listViewItem
	 */
	public ArrayList<ListViewItem> getListViewItem() {
		return listViewItem;
	}

	/**
	 * @param listViewItem the listViewItem to set
	 */
	public void setListViewItem(ArrayList<ListViewItem> listViewItem) {
		this.listViewItem = listViewItem;
	}

	/**
	 * @return the nodeType
	 */
	public int getNodeType() {
		return nodeType;
	}

	/**
	 * @param nodeType the nodeType to set
	 */
	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

}
