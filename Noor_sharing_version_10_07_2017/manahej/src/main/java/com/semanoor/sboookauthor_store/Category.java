package com.semanoor.sboookauthor_store;

public class Category {

	private int categoryId;
	private String categoryEngName;
	private String categoryArabName;
	/**
	 * @return the categoryId
	 */
	public int getCategoryId() {
		return categoryId;
	}
	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * @return the categoryEngName
	 */
	public String getCategoryEngName() {
		return categoryEngName;
	}
	/**
	 * @param categoryEngName the categoryEngName to set
	 */
	public void setCategoryEngName(String categoryEngName) {
		this.categoryEngName = categoryEngName;
	}
	/**
	 * @return the categoryArabName
	 */
	public String getCategoryArabName() {
		return categoryArabName;
	}
	/**
	 * @param categoryArabName the categoryArabName to set
	 */
	public void setCategoryArabName(String categoryArabName) {
		this.categoryArabName = categoryArabName;
	}
}
