package com.semanoor.sboookauthor_store;

public class Country {

	private int countryId;
	private String countryEngName;
	private String countryArabName;
	private String countryFlagName;
	/**
	 * @return the countryId
	 */
	public int getCountryId() {
		return countryId;
	}
	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	/**
	 * @return the countryEngName
	 */
	public String getCountryEngName() {
		return countryEngName;
	}
	/**
	 * @param countryEngName the countryEngName to set
	 */
	public void setCountryEngName(String countryEngName) {
		this.countryEngName = countryEngName;
	}
	/**
	 * @return the countryArabName
	 */
	public String getCountryArabName() {
		return countryArabName;
	}
	/**
	 * @param countryArabName the countryArabName to set
	 */
	public void setCountryArabName(String countryArabName) {
		this.countryArabName = countryArabName;
	}
	/**
	 * @return the countryFlagName
	 */
	public String getCountryFlagName() {
		return countryFlagName;
	}
	/**
	 * @param countryFlagName the countryFlagName to set
	 */
	public void setCountryFlagName(String countryFlagName) {
		this.countryFlagName = countryFlagName;
	}
}
