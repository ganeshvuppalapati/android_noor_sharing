package com.semanoor.sboookauthor_store;

import java.util.ArrayList;


public class CopyStoreContent {
	
	public static  ArrayList<String> countryArray= new ArrayList<String>(); 
    public static ArrayList<String> universityArray = new ArrayList<String>();
    public static ArrayList<String> categoryArray = new ArrayList<String>();
    public static ArrayList<String> filteredUniversityArray = new ArrayList<String>();
    public static ArrayList<String> booksOriginal = new ArrayList<String>();
    public static ArrayList<String> countryImagePath = new ArrayList<String>();
    public static ArrayList<String> universityImagePath = new ArrayList<String>();
    public static int UniversityIDInt=0;   	
    public static int SelectedCountryID=0;		
    public static String LocalCategoryName="";
    public static int serviceType=0;
    public static int selectedCountryIndex = 0;
    public static int selectedPublisherIndex = 0;
    public static int selectedCategoryIndex = 0;
    public static int selectedPriceIndex = 0;
    
    //Enrich changes:
    public static  ArrayList<String> EcountryArray= new ArrayList<String>(); 
    public static ArrayList<String> EuniversityArray = new ArrayList<String>();
    public static ArrayList<String> EcategoryArray = new ArrayList<String>();
    public static ArrayList<String> filteredEUniversityArray = new ArrayList<String>();
    public static ArrayList<String> EbooksOriginal = new ArrayList<String>();
    public static ArrayList<String> EcountryImagePath = new ArrayList<String>();
    public static ArrayList<String> EuniversityImagePath = new ArrayList<String>();
    public static int EUniversityIDInt=0;   	
    public static int ESelectedCountryID=0;		
    public static String ELocalCategoryName="";
    public static int EserviceType=0;
    public static int EselectedCountryIndex = 0;
    public static int EselectedPublisherIndex = 0;
    public static int EselectedCategoryIndex = 0;
    public static int EselectedPriceIndex = 0;
    
    public static void copyContent(ArrayList<String>country,ArrayList<String>university,ArrayList<String>category, ArrayList<String>filterUniv, ArrayList<String> booksOrig, int SelectedCID,int univID,String Lcategory, int Stype, ArrayList<String> cImagePath, ArrayList<String> uImagePath, int ScountryIndex, int SpublisherIndex, int ScategoriesIndex, int SpriceIndex ){
    	//System.out.println("called copyContent");
    	countryArray.clear();
    	universityArray.clear();
    	categoryArray.clear();
    	filteredUniversityArray.clear();
    	booksOriginal.clear();
    	countryImagePath.clear();
    	universityImagePath.clear();
    	countryArray = country;
    	universityArray= university;
    	categoryArray = category;
    	filteredUniversityArray= filterUniv;
    	booksOriginal= booksOrig;
    	SelectedCountryID= SelectedCID;
    	UniversityIDInt = univID;
    	LocalCategoryName= Lcategory;
    	serviceType= Stype;
    	countryImagePath=cImagePath;
    	universityImagePath=uImagePath;
    	selectedCountryIndex = ScountryIndex;
    	selectedPublisherIndex = SpublisherIndex;
    	selectedCategoryIndex = ScategoriesIndex;
    	selectedPriceIndex = SpriceIndex;
    }
    
    public static void copyEnrichContent(ArrayList<String>country,ArrayList<String>university,ArrayList<String>category, ArrayList<String>filterUniv, ArrayList<String> booksOrig, int SelectedCID,int univID,String Lcategory, int Stype, ArrayList<String> cImagePath, ArrayList<String> uImagePath, int ScountryIndex, int SpublisherIndex, int ScategoriesIndex, int SpriceIndex ){
    	//System.out.println("called copyContent for Enrich");
    	EcountryArray.clear();
    	EuniversityArray.clear();
    	EcategoryArray.clear();
    	filteredEUniversityArray.clear();
    	EbooksOriginal.clear();
    	EcountryImagePath.clear();
    	EuniversityImagePath.clear();
    	EcountryArray = country;
    	EuniversityArray= university;
    	EcategoryArray = category;
    	filteredEUniversityArray= filterUniv;
    	EbooksOriginal= booksOrig;
    	ESelectedCountryID= SelectedCID;
    	EUniversityIDInt = univID;
    	ELocalCategoryName= Lcategory;
    	EserviceType= Stype;
    	EcountryImagePath=cImagePath;
    	EuniversityImagePath=uImagePath;
    	EselectedCountryIndex = ScountryIndex;
    	EselectedPublisherIndex = SpublisherIndex;
    	EselectedCategoryIndex = ScategoriesIndex;
    	EselectedPriceIndex = SpriceIndex;
    }
}
