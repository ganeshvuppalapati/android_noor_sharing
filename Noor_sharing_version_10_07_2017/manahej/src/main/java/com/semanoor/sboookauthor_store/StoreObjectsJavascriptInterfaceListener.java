package com.semanoor.sboookauthor_store;

public interface StoreObjectsJavascriptInterfaceListener {

	public abstract void sojiSetTagCoordinates(String content, String currentPageNo);

	public abstract void sojiSetWebContent(String webContent, String currentPageNo);

	public abstract void sojiSetTableBackgroundContent(String tblBgContent, String currentPageNo);

	public abstract void sojiSetHtmlDir(String htmlDir);

	public abstract void sojiSetDocumentOffsetWidthAndHeight(
			String offsetWidthAndHeight);

	public abstract void sojiSetIsBookFromManahijAndroid(
			int index, String isBookFromManahijAndroid);

	public abstract void sojiSetElementsTagNameLength(String length);

	public abstract void sojiSetElementsClassName(String className);

	public abstract void sojiSetDivContentHTML(String content);

	public abstract void sojiSetElementXandYPos(String location);

	public abstract void sojiSetWebTableElementSize(String size);

	public abstract void sojiSetImageElementSize(String imgElemAttr);

	public abstract void sojiSetEmbedBoxSize(String embedBoxAttr);

	public abstract void sojiSetIframeSize(String iframeElemAttr);

	public abstract void sojiSetYoutubeSize(String youtubeElemAttr);

	public abstract void sojiSetQuizElementLength(String quizElementLength);

	public abstract void sojiSetQuizElementId(String quizElemId);

	public abstract void sojiSetImgElemLength(String imgElemLength);

	public abstract void sojisetWebTextElementSize(String size);

	public abstract void sojisetImgSrcElement(String imgSrc);

	public abstract void sojisetAudioElemContent(String audioElemAttr);

	public abstract void sojiSetYoutubeHtmlId(String youtubeHtmlId);

	public abstract void sojiSetExternalCSSPath(String path);

}
