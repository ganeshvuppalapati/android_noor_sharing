package com.semanoor.sboookauthor_store;

import java.util.ArrayList;

import com.semanoor.manahij.ElessonBookViewActivity;
import com.semanoor.manahij.R;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout.LayoutParams;

public class MainTabs {

	private String tabTitle;
	private Context context;
	private String tabPath;
	private ArrayList<SubTabs> subTabsList = new ArrayList<SubTabs>();
	private Button btnMainTab;

	public MainTabs(Context _context) {
		this.context = _context;
	}

	/*
	 * @return the tabTitle
	 */
	public String getTabTitle() {
		return tabTitle;
	}
	/**
	 * @param tabTitle the tabTitle to set
	 */
	public void setTabTitle(String tabTitle) {
		this.tabTitle = tabTitle;
	}



	public ArrayList<SubTabs> getSubTabsList() {
		return subTabsList;
	}

	public void setSubTabsList(ArrayList<SubTabs> subTabsList) {
		this.subTabsList = subTabsList;
	}

	/**
	 * @return the tabPath
	 */
	public String getTabPath() {
		return tabPath;
	}
	/**
	 * @param tabPath the tabPath to set
	 */
	public void setTabPath(String tabPath) {
		this.tabPath =tabPath;
	}
	

	public void createMainTabs(int i) {
		// TODO Auto-generated method stub
		btnMainTab = new Button(context);
		btnMainTab.setId(i);
		btnMainTab.setText(this.tabTitle);
		btnMainTab.setTag(this);
		btnMainTab.setSingleLine(true);
		btnMainTab.setTextSize(TypedValue.COMPLEX_UNIT_PX,(int)context. getResources().getDimension(R.dimen.store_bookmoda_text_medium_size));
		btnMainTab.setTextColor(Color.parseColor("#000000"));
		btnMainTab.setGravity(Gravity.CENTER);
		btnMainTab.setBackgroundResource(R.drawable.elesson_main_tab);
		btnMainTab.setRotation(180);
		if (i == 0) {
			((ElessonBookViewActivity)context).currentSelectedMainTab = this;
			btnMainTab.setSelected(true);
		} else {
			btnMainTab.setSelected(false);
		}
		/*if(((ElessonBookView)context).Language.contentEquals("Arabic")){
			btnMainTab.setRotation(180);
			String text=this.tabTitle;
		}*/
		btnMainTab.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((ElessonBookViewActivity)context).mDrawer.close();
				((ElessonBookViewActivity)context).mDrawer.setVisibility(View.GONE);
				((ElessonBookViewActivity)context).currentSelectedMainTab.getBtnMainTab().setSelected(false);
				MainTabs mTab=(MainTabs) btnMainTab.getTag();
				btnMainTab.setSelected(true);
				((ElessonBookViewActivity)context).currentSelectedMainTab = mTab;
				((ElessonBookViewActivity)context).loadListItemView(mTab.getSubTabsList());
			}
		});

		((ElessonBookViewActivity)context).linearTabsLayout.addView(btnMainTab,new LayoutParams((int) context.getResources().getDimension(R.dimen.btn_enrichment_width), LayoutParams.WRAP_CONTENT));
	}

	/**
	 * @return the btnMainTab
	 */
	public Button getBtnMainTab() {
		return btnMainTab;
	}

	/**
	 * @param btnMainTab the btnMainTab to set
	 */
	public void setBtnMainTab(Button btnMainTab) {
		this.btnMainTab = btnMainTab;
	}


}
