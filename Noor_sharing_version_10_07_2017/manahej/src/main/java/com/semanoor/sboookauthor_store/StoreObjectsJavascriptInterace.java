package com.semanoor.sboookauthor_store;

import android.webkit.JavascriptInterface;

public class StoreObjectsJavascriptInterace {

	/** The javascript interface name for adding to web view. */
	private final String interfaceName = "StoreObjects";
	
	/** The webview to work with. */
	private StoreObjectsJavascriptInterfaceListener listener;
	
	public StoreObjectsJavascriptInterace(StoreObjectsJavascriptInterfaceListener _listener) {
		this.listener = _listener;
	}
	
	/**
	 * Gets the interface name
	 * @return
	 */
	@JavascriptInterface
	public String getInterfaceName(){
		return this.interfaceName;
	}
	
	@JavascriptInterface
	public void setTagCoordinates(String content, String currentPageNo) {
		if (this.listener != null) {
			this.listener.sojiSetTagCoordinates(content, currentPageNo);
		}
	}
	
	@JavascriptInterface
	public void setWebContent(String webContent, String currentPageNo) {
		if (this.listener != null) {
			this.listener.sojiSetWebContent(webContent, currentPageNo);
		}
	}

	@JavascriptInterface
	public void setTableBackgroundContent(String tblBgContent, String currentPageNo) {
		if (this.listener != null) {
			this.listener.sojiSetTableBackgroundContent(tblBgContent, currentPageNo);
		}
	}
	
	@JavascriptInterface
	public void setHtmlDir(String htmlDir) {
		if (this.listener != null) {
			this.listener.sojiSetHtmlDir(htmlDir);
		}
	}
	
	@JavascriptInterface
	public void setDocumentOffsetWidthAndHeight(String offsetWidthAndHeight) {
		if (this.listener != null) {
			this.listener.sojiSetDocumentOffsetWidthAndHeight(offsetWidthAndHeight);
		}
	}
	
	@JavascriptInterface
	public void setIsBookFromManahijAndroid(int index, String isBookFromManahijAndroid) {
		if (this.listener != null) {
			this.listener.sojiSetIsBookFromManahijAndroid(index, isBookFromManahijAndroid);
		}
	}
	
	@JavascriptInterface
	public void setElementsTagNameLength(String length){
		if (this.listener != null) {
			this.listener.sojiSetElementsTagNameLength(length);
		}
	}
	
	@JavascriptInterface
	public void setElementsClassName(String className) {
		if (this.listener != null) {
			this.listener.sojiSetElementsClassName(className);
		}
	}
	
	@JavascriptInterface
	public void setDivContentHTML(String content) {
		if (this.listener != null) {
			this.listener.sojiSetDivContentHTML(content);
		}
	}
	
	@JavascriptInterface
	public void setElementXandYPos(String location) {
		if (this.listener != null) {
			this.listener.sojiSetElementXandYPos(location);
		}
	}
	
	@JavascriptInterface
	public void setWebTextElementSize(String size) {
		if (this.listener != null) {
			this.listener.sojisetWebTextElementSize(size);
		}
	}
	
	@JavascriptInterface
	public void setWebTableElementSize(String size) {
		if (this.listener != null) {
			this.listener.sojiSetWebTableElementSize(size);
		}
	}
	
	@JavascriptInterface
	public void setImageElementSize(String imgElemAttr) {
		if (this.listener != null) {
			this.listener.sojiSetImageElementSize(imgElemAttr);
		}
	}
	
	@JavascriptInterface
	public void setEmbedBoxSize(String embedBoxAttr){
		if (this.listener != null) {
			this.listener.sojiSetEmbedBoxSize(embedBoxAttr);
		}
	}
	
	@JavascriptInterface
	public void setIframeSize(String iframeElemAttr){
		if (this.listener != null) {
			this.listener.sojiSetIframeSize(iframeElemAttr);
		}
	}
	
	@JavascriptInterface
	public void setYoutubeSize(String youtubeElemAttr){
		if (this.listener != null) {
			this.listener.sojiSetYoutubeSize(youtubeElemAttr);
		}
	}
	
	@JavascriptInterface
	public void setQuizElementLength(String quizElementLength) {
		if (this.listener != null) {
			this.listener.sojiSetQuizElementLength(quizElementLength);
		}
	}
	
	@JavascriptInterface
	public void setQuizElementId(String quizElemId){
		if (this.listener != null) {
			this.listener.sojiSetQuizElementId(quizElemId);
		}
	}
	
	@JavascriptInterface
	public void setImgElemLength(String imgElemLength) {
		if (this.listener != null) {
			this.listener.sojiSetImgElemLength(imgElemLength);
		}
	}
	
	@JavascriptInterface
	public void setImgSrcElement(String imgSrc){
		if (this.listener != null) {
			this.listener.sojisetImgSrcElement(imgSrc);
		}
	}
	
	@JavascriptInterface
	public void setAudioElemContent(String audioElemAttr) {
		if (this.listener != null) {
			this.listener.sojisetAudioElemContent(audioElemAttr);
		}
	}
	
	@JavascriptInterface
	public void setYoutubeHtmlId(String youtubeHtmlId) {
		if (this.listener != null) {
			this.listener.sojiSetYoutubeHtmlId(youtubeHtmlId);
		}
	}
	public void SetExternalCSSPath(String cssPath) {
		if (this.listener != null) {
			this.listener.sojiSetExternalCSSPath(cssPath);
		}
	}

}
