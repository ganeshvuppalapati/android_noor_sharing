package com.semanoor.sboookauthor_store;

public class University {

	private int universityId;
	private String universityEngName;
	private String universityArabName;
	private String universityFlag;
	private int universityCountryId;
	/**
	 * @return the universityId
	 */
	public int getUniversityId() {
		return universityId;
	}
	/**
	 * @param universityId the universityId to set
	 */
	public void setUniversityId(int universityId) {
		this.universityId = universityId;
	}
	/**
	 * @return the universityEngName
	 */
	public String getUniversityEngName() {
		return universityEngName;
	}
	/**
	 * @param universityEngName the universityEngName to set
	 */
	public void setUniversityEngName(String universityEngName) {
		this.universityEngName = universityEngName;
	}
	/**
	 * @return the universityArabName
	 */
	public String getUniversityArabName() {
		return universityArabName;
	}
	/**
	 * @param universityArabName the universityArabName to set
	 */
	public void setUniversityArabName(String universityArabName) {
		this.universityArabName = universityArabName;
	}
	/**
	 * @return the universityFlag
	 */
	public String getUniversityFlag() {
		return universityFlag;
	}
	/**
	 * @param universityFlag the universityFlag to set
	 */
	public void setUniversityFlag(String universityFlag) {
		this.universityFlag = universityFlag;
	}
	/**
	 * @return the universityCountryId
	 */
	public int getUniversityCountryId() {
		return universityCountryId;
	}
	/**
	 * @param universityCountryId the universityCountryId to set
	 */
	public void setUniversityCountryId(int universityCountryId) {
		this.universityCountryId = universityCountryId;
	}
	
}
