package com.semanoor.sboookauthor_store;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import android.os.Environment;
import android.os.StatFs;

public class BookStorage {
	
	public static Boolean checkInternalStorageAvailability(){
		
		float availableSpace = InternalStorageAvailableMB();
		if(availableSpace>30.00){  
			return true;
		}
		else{
			return false;
		}
		
	}
	
	
	public static float InternalStorageAvailableMB() {
			
			StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
			long bytesAvailable = (long)stat.getBlockSize() * (long)stat.getAvailableBlocks();
			//System.out.println("BInterytesAvailable: "+bytesAvailable/(1024.f * 1024.f)+"MB");
			return bytesAvailable / (1024.f * 1024.f);
		}
	
	public static Boolean checkExternalStorageAvailability(){
		float availableSpace = ExternalStorageAvailableMB();
		if(availableSpace>30.00){  
			return true;
		}
		else{
			return false;
		}
	}
	
	public static float ExternalStorageAvailableMB() {
		
		StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
		long bytesAvailable = (long)stat.getBlockSize() * (long)stat.getAvailableBlocks();
		//System.out.println("BytesAvailable: "+bytesAvailable/(1024.f * 1024.f)+"MB");
		return bytesAvailable / (1024.f * 1024.f);
	}
 
public static void moveToSdCard(File src, File dst) throws IOException{
		
		FileChannel inChannel = new FileInputStream(src).getChannel();
		FileChannel outChannel = new FileOutputStream(dst).getChannel();
		try{
			inChannel.transferTo(0, inChannel.size(), outChannel);
		}
		finally{
			if (inChannel != null)
				inChannel.close();
			if (outChannel != null)
				outChannel.close();
			}
		//System.out.println("Successfully moved to Sdcard");
		}
}
