package com.semanoor.sboookauthor_store;

import android.content.ClipData;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.MyDragShadowBuilder;
import com.semanoor.manahij.R;
import com.semanoor.manahij.pdfActivity;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Suriya on 11/05/15.
 */
public class EnrichmentButton extends Button {

    private int enrichmentId;
    private int enrichmentBid;
    private int enrichmentPageNo;
    private int enrichmentSequenceId;
    private String enrichmentTitle;
    private String enrichmentType;
    private int enrichmentExportValue;
    private String enrichmentPath;
    private BookViewReadActivity bookViewReadActivity;
    private pdfActivity pdf_readActivity;
    private GestureDetector mGestureDetector;
//    public Context ctxt;
//    LinearLayout enrLayout;
    public View mDrapView;
    public EnrichmentButton(final Context context, String enrType, final int enrID, int enrBid, int enrPageNo, String enrTitle, int enrSeqId, int enrExportedId, String path, final LinearLayout enrTabsLayout, int sequenceId) {
        super(context);
//        ctxt=context;
//        enrLayout=enrTabsLayout;
        if(context instanceof BookViewReadActivity){
            this.bookViewReadActivity = (BookViewReadActivity) context;
        }else{
            this.pdf_readActivity= (pdfActivity) context;
        }
        this.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) context.getResources().getDimension(R.dimen.enrichments_tex_size));
      //  this.setTextColor(Color.parseColor("#000000"));
        this.setSingleLine(true);
        this.setTextColor(Color.WHITE);
        this.setBackgroundResource(R.drawable.btn_enrich_tab);
        this.setGravity(Gravity.CENTER);
        this.setEllipsize(TextUtils.TruncateAt.END);
        this.setEnrichmentType(enrType);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.enrichment_btn_width), (int) context.getResources().getDimension(R.dimen.enrichment_btn_height));
        this.setLayoutParams(layoutParams);

        this.setEnrichmentId(enrID);
        this.setEnrichmentBid(enrBid);
        this.setEnrichmentPageNo(enrPageNo);
        this.setEnrichmentTitle(enrTitle);
        this.setEnrichmentSequenceId(sequenceId);
        this.setEnrichmentExportValue(enrExportedId);
        this.setEnrichmentPath(path);
        this.setText(enrTitle);

        if (this.enrichmentType.equals(Globals.advancedSearchType)) {   // || this.enrichmentType.equals(Globals.downloadedEnrichmentType)
            final RelativeLayout layout = new RelativeLayout(context);
            layout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            layout.addView(this);
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) getResources().getDimension(R.dimen.bookview_searchTab_remove), (int) getResources().getDimension(R.dimen.bookview_searchTab_height));
                Button btnEnrichDelete = new Button(context);
                btnEnrichDelete.setId(R.id.btnEnrichmentTab);
                btnEnrichDelete.setTag(enrID);
                btnEnrichDelete.setBackgroundResource(R.drawable.btn_enr_del);
                lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                lp.addRule(RelativeLayout.ALIGN_BASELINE, this.getId());
                lp.addRule(RelativeLayout.CENTER_VERTICAL, this.getId());
                lp.setMargins(0, 0, (int) getResources().getDimension(R.dimen.btn_remove), 0); //30
                btnEnrichDelete.setLayoutParams(lp);
                btnEnrichDelete.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (context instanceof BookViewReadActivity) {
                            if (enrichmentType.equals(Globals.advancedSearchType)) {
                             //   bookViewReadActivity.deleteAdvancedEnrichTab(EnrichmentButton.this);
                            }else{
                               // bookViewReadActivity.deleteDownloadedEnrichTab(EnrichmentButton.this);
                                String path = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookViewReadActivity.currentBook.get_bStoreID()+"Book/xmlForUpdateRecords.xml";
                               // bookViewReadActivity .deleteXmlEntryInUpdateRecordsXml(path,EnrichmentButton.this);
                            }
                        } else {
                          //  pdf_readActivity.deleteAdvancedEnrichTab(EnrichmentButton.this);
                        }
                    }
                });
                new lvimage(this, 0).execute("http://www.google.com/s2/favicons?domain=" + this.enrichmentPath);
                layout.addView(btnEnrichDelete);
            if(context instanceof BookViewReadActivity) {
                ((BookViewReadActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        enrTabsLayout.addView(layout, new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.enrichment_btn_width), (int) context.getResources().getDimension(R.dimen.enrichment_btn_height)));
                    }
                });
            }else{
                ((pdfActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        enrTabsLayout.addView(layout, new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.enrichment_btn_width), (int) context.getResources().getDimension(R.dimen.enrichment_btn_height)));
                    }
                });
            }
        } else {
            if(context instanceof BookViewReadActivity) {
                ((BookViewReadActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        enrTabsLayout.addView(EnrichmentButton.this);
                    }
                });
            }else{
                ((pdfActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        enrTabsLayout.addView(EnrichmentButton.this);
                    }
                });
            }
        }

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(context instanceof BookViewReadActivity) {
                    if (((EnrichmentButton) view).getEnrichmentType().equals(Globals.advancedSearchType)) {
                       // bookViewReadActivity.advSearchTab(EnrichmentButton.this);
                    } else if (((EnrichmentButton) view).getEnrichmentType().equals(Globals.onlineEnrichmentsType) || ((EnrichmentButton) view).getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
                       // bookViewReadActivity.clickedAdvanceSearchtab((EnrichmentButton) view);
                    } else {
                      //  bookViewReadActivity.clickedEnrichments((EnrichmentButton) view);
                    }
                }else {
                    if (((EnrichmentButton) view).getEnrichmentType().equals(Globals.advancedSearchType)) {
                     //   pdf_readActivity.advSearchTab(EnrichmentButton.this);
                    } else if (((EnrichmentButton) view).getEnrichmentType().equals(Globals.onlineEnrichmentsType) || ((EnrichmentButton) view).getEnrichmentType().equals(Globals.OBJTYPE_MINDMAPTAB)) {
                     //   pdf_readActivity.clickedAdvanceSearchtab((EnrichmentButton) view);
                    }else{
                      //  pdf_readActivity.clickedEnrichments((EnrichmentButton) view);
                    }
                }
            }

        });

        this.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(context instanceof BookViewReadActivity) {
                    if (((EnrichmentButton) view).getEnrichmentType().equals(Globals.advancedSearchType)) {
                        ((BookViewReadActivity) context).showAdvsearchEnrichDialog((EnrichmentButton) view);
                    }
                    ((BookViewReadActivity) context).mDrapView = view;
                    ClipData data = ClipData.newPlainText("", "");
                   MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(
                            view, ((BookViewReadActivity) context));
                    view.startDrag(data, shadowBuilder, view, 0);
                }else{
                    if (((EnrichmentButton) view).getEnrichmentType().equals(Globals.advancedSearchType)) {
                      //  ((pdfActivity) context).showAdvsearchEnrichDialog((EnrichmentButton) view);
                    }
                }
                return false;
            }
        });
    }

    public int getEnrichmentId() {
        return enrichmentId;
    }

    public void setEnrichmentId(int enrichmentId) {
        this.enrichmentId = enrichmentId;
    }

    public int getEnrichmentBid() {
        return enrichmentBid;
    }

    public void setEnrichmentBid(int enrichmentBid) {
        this.enrichmentBid = enrichmentBid;
    }

    public int getEnrichmentPageNo() {
        return enrichmentPageNo;
    }

    public void setEnrichmentPageNo(int enrichmentPageNo) {
        this.enrichmentPageNo = enrichmentPageNo;
    }

    public int getEnrichmentSequenceId() {
        return enrichmentSequenceId;
    }

    public void setEnrichmentSequenceId(int enrichmentSequenceId) {
        this.enrichmentSequenceId = enrichmentSequenceId;
    }

    public String getEnrichmentTitle() {
        return enrichmentTitle;
    }

    public void setEnrichmentTitle(String enrichmentTitle) {
        this.enrichmentTitle = enrichmentTitle;
    }

    public String getEnrichmentType() {
        return enrichmentType;
    }

    public void setEnrichmentType(String enrichmentType) {
        this.enrichmentType = enrichmentType;
    }

    public int getEnrichmentExportValue() {
        return enrichmentExportValue;
    }

    public void setEnrichmentExportValue(int enrichmentExportValue) {
        this.enrichmentExportValue = enrichmentExportValue;
    }

    public String getEnrichmentPath() {
        return enrichmentPath;
    }

    public void setEnrichmentPath(String enrichmentPath) {
        this.enrichmentPath = enrichmentPath;
    }

    private class lvimage extends AsyncTask<String, Void, Bitmap> {
        ImageView imagepath;
        ImageView img;
        EnrichmentButton btn;
        int id;

        public lvimage(EnrichmentButton btnEnrichmentTab, int i) {
            // TODO Auto-generated constructor stub
            this.btn = btnEnrichmentTab;
            id = i;
        }

        protected Bitmap doInBackground(String... urls) {
            String URLpath = urls[0];
            Bitmap bmp = null;
            try {
                URL url = new URL(URLpath);
                URLConnection ucon = url.openConnection();
                ucon.connect();
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                //System.out.println(bmp.getWidth());;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return bmp;

        }

        protected void onPostExecute(Bitmap result) {
            if (result == null) {

            } else {
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, result.getWidth(), getResources().getDisplayMetrics());
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, result.getHeight(), getResources().getDisplayMetrics());

                Bitmap b = Bitmap.createScaledBitmap(result, width, height, false);
                Drawable f = new BitmapDrawable(getResources(), b);
                String Path = Globals.TARGET_BASE_BOOKS_DIR_PATH + btn.getEnrichmentBid() + "/" + "bkmarkimages/";
                UserFunctions.createNewDirectory(Path);
                String correct = Path + id + ".png";
                btn.setCompoundDrawablesWithIntrinsicBounds(f, null, null, null);

                if (id != 0) {
                    UserFunctions.saveBitmapImage(b, correct);
                }
            }
        }
    }

}
