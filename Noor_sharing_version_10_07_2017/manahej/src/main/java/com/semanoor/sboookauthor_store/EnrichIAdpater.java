package com.semanoor.sboookauthor_store;

import java.util.ArrayList;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.semanoor.manahij.Enrichment;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class EnrichIAdpater extends BaseAdapter {

    private Activity activity;
    private ArrayList<String> data = new ArrayList<String>();
    private LayoutInflater inflater;
    public Enrichment enrichment;
    public ArrayList<String> downloadedBooksArray;
    public ArrayList<String> downloadingBooksArray;
    private DisplayImageOptions options;
    private int subscriptDays;

    public EnrichIAdpater(Activity a, ArrayList<String> books, Enrichment enrichment, ArrayList<String> downloadedBooksArray,ArrayList<String> downloadingBooksList) {
        activity = a;
        data = books;
        inflater = LayoutInflater.from(activity.getApplicationContext());
        this.enrichment = enrichment;
        this.downloadedBooksArray = downloadedBooksArray;
        this.downloadingBooksArray = downloadingBooksList;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(a);
        this.subscriptDays = Integer.parseInt(prefs.getString(Globals.SUBSCRIPTDAYSLEFT, "0"));
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.neww_store_grid, parent, false);
            Typeface font = Typeface.createFromAsset(activity.getAssets(),"fonts/FiraSans-Regular.otf");
            UserFunctions.changeFont(parent,font);
            holder = new ViewHolder();
            assert view != null;
            holder.imageView = (ImageView) view.findViewById(R.id.grid_item_image);
            holder.titletext = (TextView) view.findViewById(R.id.grid_item_title);
            holder.authorText = (TextView) view.findViewById(R.id.grid_item_author);
            //holder.descriptionText = (TextView) view.findViewById(R.id.grid_item_description);
            holder.btnGet = (Button) view.findViewById(R.id.button1);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        String CurrentBook = data.get(position);
        //System.out.println("currentBook: "+CurrentBook);
        String[] BookDetails = CurrentBook.split("#");
        String Title = BookDetails[3];
        String Author = BookDetails[5];
        String Description = BookDetails[4];
        String URL = BookDetails[6];
        final String bookID = BookDetails[0];
        //System.out.println("URL path:"+URL);
        holder.btnGet.setTag(position);
        final float price = (float) Float.parseFloat(BookDetails[1]);
        String inAppId = BookDetails[29];
        if (price > 0.00) {
            if (Globals.isLimitedVersion()) {
                if (subscriptDays > 0 || UserFunctions.checkRedeemInAppIdExist(inAppId)) {
                    holder.btnGet.setText(activity.getResources().getString(R.string.get));
                } else {
                    holder.btnGet.setText(String.valueOf(price) + "$");
                }
            } else {
                holder.btnGet.setText(String.valueOf(price) + "$");
            }
        } else {
            holder.btnGet.setText(activity.getResources().getString(R.string.free));
            holder.btnGet.setTextColor(Color.WHITE);
        }
        for (int i = 0; i < downloadedBooksArray.size(); i++) {
            if (bookID.equals(downloadedBooksArray.get(i))) {
                holder.btnGet.setText(activity.getResources().getString(R.string.downloaded));
                holder.btnGet.setTextColor(Color.WHITE);
                break;
            }
        }
        for (int i = 0; i < downloadingBooksArray.size(); i++) {
            if (bookID.equals(downloadingBooksArray.get(i))) {
                holder.btnGet.setText(activity.getResources().getString(R.string.downloading));
                //    holder.btnGet.setTextColor(Color.WHITE);
                break;
            }
        }
        holder.btnGet.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (((Button) v).getText() != activity.getResources().getString(R.string.downloaded) && ((Button) v).getText() != activity.getResources().getString(R.string.downloading)) {
                    enrichment.EbookClickPosition = (Integer) v.getTag();
                    enrichment.downloadClicked();
                    if (subscriptDays>0) {
                        ((Button) v).setText(activity.getResources().getString(R.string.downloading));
                    }else{
                        if (!(price >0.00)){
                            ((Button) v).setText(activity.getResources().getString(R.string.downloading));
                        }
                    }
                    ((Button) v).invalidate();
                }
            }
        });
        holder.titletext.setText(Title);
//        if (Author.equals("null")) {
//            holder.authorText.setVisibility(View.INVISIBLE);
//        } else {
//            holder.authorText.setVisibility(View.VISIBLE);
//            holder.authorText.setText(Author);
//        }
        //holder.descriptionText.setText(Description);
        Glide.with(activity).load(URL)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(200,200)
                .into(holder.imageView);
        return view;
    }

    static class ViewHolder {
        ImageView imageView;
        TextView titletext;
        TextView authorText;
        //TextView descriptionText;
        Button btnGet;
    }
}