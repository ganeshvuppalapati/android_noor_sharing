package com.semanoor.sboookauthor_store;

import com.semanoor.manahij.BookView;
import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.R;
import com.semanoor.manahij.pdfActivity;
import com.semanoor.source_sboookauthor.Globals;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;

public class BookMarkEnrichments {
	private int enrichmentId;
	private int enrichmentBid;
	private int enrichmentPageNo;
	private int enrichmentSequenceId;
	private String enrichmentTitle;
    private boolean IsForAllPages;
	private Context context;
	private String enrichmentPath;
	private int searchTabId;

	
	public BookMarkEnrichments(Context _context) {
		this.context = _context;
	}
	/**
	 * @return the enrichmentId
	 */
	public int getEnrichmentId() {
		return enrichmentId;
	}
	/**
	 * @param enrichmentId the enrichmentId to set
	 */
	public void setEnrichmentId(int enrichmentId) {
		this.enrichmentId = enrichmentId;
	}
	/**
	 * @return the enrichmentBid
	 */
	public int getEnrichmentBid() {
		return enrichmentBid;
	}
	/**
	 * @param enrichmentBid the enrichmentBid to set
	 */
	public void setEnrichmentBid(int enrichmentBid) {
		this.enrichmentBid = enrichmentBid;
	}
	/**
	 * @return the enrichmentPageNo
	 */
	public int getEnrichmentPageNo() {
		return enrichmentPageNo;
	}
	/**
	 * @param enrichmentPageNo the enrichmentPageNo to set
	 */
	public void setEnrichmentPageNo(int enrichmentPageNo) {
		this.enrichmentPageNo = enrichmentPageNo;
	}
	/**
	 * @return the enrichmentTitle
	 */
	public String getEnrichmentTitle() {
		return enrichmentTitle;
	}
	/**
	 * @param enrichmentTitle the enrichmentTitle to set
	 */
	public void setEnrichmentTitle(String enrichmentTitle) {
		this.enrichmentTitle = enrichmentTitle;
	}
	
	
	
	/**
	 * Create Enrichment tabs
	 */
	public Button createEnrichmentTabs(LinearLayout ll_bkContainer) {
		Button btnBookmarkTab = new Button(context);
		btnBookmarkTab.setId(R.id.btnEnrichmentTab);
		btnBookmarkTab.setTag(this);
		btnBookmarkTab.setText(this.enrichmentTitle);
		btnBookmarkTab.setTextColor(Color.parseColor("#000000"));
		String currenturl=enrichmentPath;
		if(context instanceof BookViewReadActivity) {
			((BookViewReadActivity) context).new lvimage(btnBookmarkTab, enrichmentId).execute("http://www.google.com/s2/favicons?domain=" + currenturl);
		}else{
			((pdfActivity) context).new lvimage(btnBookmarkTab, enrichmentId).execute("http://www.google.com/s2/favicons?domain=" + currenturl);
		}
		btnBookmarkTab.setTextSize(TypedValue.COMPLEX_UNIT_PX,context. getResources().getDimension(R.dimen.enrichments_tex_size));
		btnBookmarkTab.setSingleLine(true);
		btnBookmarkTab.setBackgroundResource(R.drawable.book_mark);
		btnBookmarkTab.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(context instanceof BookViewReadActivity) {
					//((BookViewReadActivity) context).createAdvancedSearchTab(enrichmentPath, enrichmentTitle);
					((BookViewReadActivity) context).new createAdvanceSearchTab(enrichmentPath, enrichmentTitle).execute();
				}else {
				//	((pdfActivity) context).new createAdvanceSearchTab(enrichmentPath, enrichmentTitle).execute();
				}
			}
		});

		ll_bkContainer.addView(btnBookmarkTab, new LayoutParams(Globals.getDeviceIndependentPixels(150, context), LayoutParams.WRAP_CONTENT));
		return btnBookmarkTab;
	}



	/**
	 * Create bookMarkTabsForAllPages
	 */
	public Button BkMarkTabsForAllPagesinReadMode() {
		final Button btnBookmarkTab = new Button(context);
		btnBookmarkTab.setId(R.id.btnEnrichmentTab);
		btnBookmarkTab.setTag(this.enrichmentId);
		btnBookmarkTab.setText(this.enrichmentTitle);
		btnBookmarkTab.setMaxWidth(Globals.getDeviceIndependentPixels(100, context));
		btnBookmarkTab.setSingleLine(true);
		btnBookmarkTab.setTextColor(Color.parseColor("#000000"));
		btnBookmarkTab.setTextSize(TypedValue.COMPLEX_UNIT_PX,context. getResources().getDimension(R.dimen.enrichments_tex_size));
		btnBookmarkTab.setSingleLine(true);
		btnBookmarkTab.setBackgroundResource(R.drawable.book_mark);

		String currenturl=this.enrichmentPath;
		((BookView)context).new lvimage(btnBookmarkTab,enrichmentId).execute("http://www.google.com/s2/favicons?domain="+currenturl);
		btnBookmarkTab.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((BookView)context).makeBookmarkEnrichedBtnSlected(v);
				int id= (Integer)v.getTag();
				((BookView)context).new clickedAdvancedSearchTabs(btnBookmarkTab.getText().toString(),enrichmentPath).execute();

			}
		});
		((BookView)context).bookmark_container.addView(btnBookmarkTab, new LayoutParams(Globals.getDeviceIndependentPixels(150, context), LayoutParams.WRAP_CONTENT));
		return btnBookmarkTab;
	}
	
	/**
	 * Create BookMark tabs  in edit mode
	 */
	public Button createBookMarkTabs() {
		final Button btnBookmarkTab = new Button(context);
		btnBookmarkTab.setId(R.id.btnEnrichmentTab);
		btnBookmarkTab.setTag(this);
		btnBookmarkTab.setText(this.enrichmentTitle);
		btnBookmarkTab.setMaxWidth(Globals.getDeviceIndependentPixels(80, context));
		btnBookmarkTab.setSingleLine(true);
		btnBookmarkTab.setTextColor(Color.parseColor("#000000"));
		btnBookmarkTab.setTextSize(TypedValue.COMPLEX_UNIT_PX,context. getResources().getDimension(R.dimen.enrichments_tex_size));
		btnBookmarkTab.setSingleLine(true);
		btnBookmarkTab.setBackgroundResource(R.drawable.book_mark);

		String currenturl=enrichmentPath;
		((BookViewActivity)context).new image(btnBookmarkTab,enrichmentId).execute("http://www.google.com/s2/favicons?domain="+currenturl);
		btnBookmarkTab.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((BookViewActivity)context).new CreatingAdvsearchEnrichments(enrichmentTitle,enrichmentPath).execute();   
			}
		});
		((BookViewActivity)context).bookmark_container.addView(btnBookmarkTab, new LayoutParams(Globals.getDeviceIndependentPixels(150, context), LayoutParams.WRAP_CONTENT));
		return btnBookmarkTab;
	}
	/**
	 * @return the enrichmentSequenceId
	 */
	public int getEnrichmentSequenceId() {
		return enrichmentSequenceId;
	}
	/**
	 * @param enrichmentSequenceId the enrichmentSequenceId to set
	 */
	public void setEnrichmentSequenceId(int enrichmentSequenceId) {
		this.enrichmentSequenceId = enrichmentSequenceId;
	}
	/**
	 * @return the enrichmentPath
	 */
	public String getEnrichmentPath() {
		return enrichmentPath;
	}
	/**
	 * @param enrichmentPath the enrichmentPath to set
	 */
	public void setEnrichmentPath(String enrichmentPath) {
		this.enrichmentPath = enrichmentPath;
	}
	/**
	 * @return the searchTabId
	 */
	public int getSearchTabId() {
		return searchTabId;
	}
	/**
	 * @param searchTabId the searchTabId to set
	 */
	public void setSearchTabId(int searchTabId) {
		this.searchTabId = searchTabId;
	}
	/**
	 * @return the isForAllPages
	 */
	public boolean isIsForAllPages() {
		return IsForAllPages;
	}
	/**
	 * @param isForAllPages the isForAllPages to set
	 */
	public void setIsForAllPages(boolean isForAllPages) {
		IsForAllPages = isForAllPages;
	}

}
