package com.semanoor.sboookauthor_store;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;

import com.semanoor.source_sboookauthor.UserFunctions;

public class DownloadCounts {

	public static String soapMessage="";
    public static String ENVELOPE="";
    public static String url="";
    public static String SOAPAction="";
    public static String bookID="";
    
    public static void downloadCountsWebservice(String bookid){
    	if(!bookid.contains("M")){
    	bookID = bookid;
    	}
    	else{
    		bookID = bookid.replaceAll("M", "");
    	}
    	new soapDownloadCountsCall().execute();
    }
    
    public static class  soapDownloadCountsCall extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params){
			
			soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
					   + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
					   + "<soap:Body>\n"
					   + "<addDownloadsCount xmlns=\"http://semanoor.com.sa/\">\n"
                       + "<iBookID>"+bookID+"</iBookID>\n"
					   + "</addDownloadsCount>\n"
					   + "</soap:Body>\n"
					   + "</soap:Envelope>";
			SOAPAction = "http://semanoor.com.sa/addDownloadsCount";
			ENVELOPE =String.format(soapMessage, null);
			url = "http://school.nooor.com/sboook/sboookservice.asmx";	
			String returnXml = CallWebService(url, SOAPAction, ENVELOPE);
			//System.out.println("returnxml:"+returnXml);
			//parseXml(returnXml);
			return null;
		}

		protected void onPostExecute(Void result) {
			//System.out.println("finished webservice downloadCounts call");
		}
    	
    }

    private static String CallOldWebService(String url, String sOAPAction, String eNVELOPE) {
		
		final DefaultHttpClient httpClient = new DefaultHttpClient();
	    // request parameters
	    HttpParams params = httpClient.getParams();
	    HttpConnectionParams.setConnectionTimeout(params, 10000);
	    HttpConnectionParams.setSoTimeout(params, 15000);
	    // set parameter
	    HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

	    // POST the envelope
	    HttpPost httppost = new HttpPost(url);
	    // add headers
	    httppost.setHeader("SOAPAction", sOAPAction);
	    httppost.setHeader("Content-Type", "text/xml; charset=utf-8");

	    String responseString = "Nothingggg";
	    try {
	    	// the entity holds the request
	        HttpEntity entity = new StringEntity(eNVELOPE);
	        httppost.setEntity(entity);
	        // Response handler
	        ResponseHandler<String> rh = new ResponseHandler<String>() {
	            // invoked when client receives response
				public String handleResponse(HttpResponse response)
						throws ClientProtocolException, IOException {	
					// get response entity
	                HttpEntity entity = response.getEntity();
	                 //read the response as byte array
	                StringBuffer out = new StringBuffer();
	                byte[] b = EntityUtils.toByteArray(entity);
	                // write the response byte array to a string buffer
	                out.append(new String(b, 0, b.length)); 
	                String responseXml = out.toString(); 
	                String finalStr = responseXml;
	                String splitStr[]={""};
	                String finalSplitStr[]={""};
	                splitStr = finalStr.split("<addDownloadsCountResult>");
                	finalSplitStr= splitStr[1].split("</addDownloadsCountResult>");
	                return finalSplitStr[0];
				}
	        };

	         responseString = httpClient.execute(httppost, rh);

	    } catch (Exception e) {
	        e.printStackTrace();
	        //Log.d("me","Exc : "+ e.toString());

	    }

	    // close the connection
	    httpClient.getConnectionManager().shutdown();
	    //System.out.println("ResponseString:"+responseString);
	    return responseString;
	}

	private static String CallWebService(String url, String sOAPAction, String eNVELOPE){
		HttpURLConnection urlConnection = null;
		String returnResponse = "";
		String finalSplitStr[]={""};
		try {
			URL urlToRequest = new URL(url);

			urlConnection = (HttpURLConnection) urlToRequest.openConnection();
			urlConnection.setConnectTimeout(10000);
			urlConnection.setReadTimeout(25000);
			urlConnection.setRequestMethod("POST");
			urlConnection.setUseCaches(false);
			urlConnection.setDoInput(true);
			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-type", "text/xml; charset=utf-8");
			urlConnection.setRequestProperty("SOAPAction", sOAPAction);

			OutputStream out = urlConnection.getOutputStream();
			out.write(eNVELOPE.getBytes());
			int statusCode = urlConnection.getResponseCode();
			if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

			} else if (statusCode != HttpURLConnection.HTTP_OK) {

			}

			InputStream in = urlConnection.getInputStream();
			returnResponse = UserFunctions.convertStreamToString(in);
			String finalStr = returnResponse;
			String splitStr[]={""};
			splitStr = finalStr.split("<addDownloadsCountResult>");
			if (splitStr.length >= 2) {
				finalSplitStr = splitStr[1].split("</addDownloadsCountResult>");
			}

		} catch (MalformedURLException e) {

		} catch (SocketTimeoutException e) {

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return finalSplitStr[0];
	}
}
