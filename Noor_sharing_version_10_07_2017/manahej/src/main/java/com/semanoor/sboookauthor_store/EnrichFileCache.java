package com.semanoor.sboookauthor_store;

import java.io.File;
import java.net.URLEncoder;

import com.semanoor.manahij.Enrichment;
import com.semanoor.source_sboookauthor.Globals;

import android.content.Context;

public class EnrichFileCache {

	private File cacheDir;
	   
    public EnrichFileCache(Context context){
        //Find the dir to save cached images
        /*if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            //cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"StoreImages");
        	//cacheDir=new File(Environment.getDataDirectory().getPath().concat("/data/com.semanoor.manahij/files"));
        	cacheDir = context.getFilesDir();
        else
            cacheDir=context.getCacheDir();*/
    	//String book_storeImagePath = Environment.getDataDirectory().getPath().concat("/data/com.semanoor.manahij/files/store_bookImage");
    	cacheDir = new File(Globals.TARGET_BASE_STORE_BOOKIMAGEPATH);
        if(!cacheDir.exists())
            cacheDir.mkdirs();
    }
    
    public File getFile(String url){
    	 //String filename = "bookImage:"+imageNumber;
        //I identify images by hashcode. Not a perfect solution, good for the demo.
       // String filename=String.valueOf(url.hashCode());
        //Another possible solution (thanks to grantland)
    	//String filename= url;
       String filename = URLEncoder.encode(url);
       File f = new File(cacheDir, filename);
       //System.out.println("imagefilename in FileCache:"+filename);
      // ManahijStore.storeBookImageArray.add(filename);
       //if(StoreTab.storetabValue == 1){
     
    	   	Enrichment.EstoreBookImageArray.add(filename);

        return f;
        
    }
  
}
