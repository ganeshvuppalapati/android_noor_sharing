package com.semanoor.sboookauthor_store;

import java.io.File;
import java.util.ArrayList;

import com.semanoor.source_sboookauthor.DatabaseHandler;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class StoreDatabase 
{	
	private static DatabaseHandler mydatabase;
	private static ArrayList<String> countryImagePath = new ArrayList<String>();
	private static ArrayList<String> countryArrayInSQL = new ArrayList<String>();
	private static ArrayList<String> filteredUniversityArray = new ArrayList<String>();
	private static ArrayList<String> universityImagePath = new ArrayList<String>();
	private static ArrayList<String> universityArrayInSQL = new ArrayList<String>();
	private static ArrayList<String> storeBookImageInSQL = new ArrayList<String>();
	private static ArrayList<String> downloadedBooksArray;
	private static ArrayList<String> downloadingBooksArray;

	//Enrich variables:
	private static ArrayList<String> EfilteredUniversityArray = new ArrayList<String>();

	public  StoreDatabase(Context context)
	{
		mydatabase = DatabaseHandler.getInstance(context);
	}

	/** Country Methods Started */
	/** InsertCountry with CountryID and countryFlag in the database */

	public static void InsertCountry(String CountryID, String flagurl)
	{
		try
		{
			SQLiteDatabase sql = mydatabase.getWritableDatabase(); //Need to check data base exists or not
			//System.out.println("CountryID:"+CountryID+"flagurl:"+flagurl);
			String insertCountryQuery = "insert into tblCountryFlag(CID,FlagURL) values('"+CountryID+"','"+flagurl+"')";

			sql.execSQL(insertCountryQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch(SQLException e){}
		catch (RuntimeException e){}
	}

	/**  UpdateCountry with CountryID and countryFlag in the database */

	public static void UpdateCountry(String CountryID, String flagurl)
	{
		try
		{
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			//System.out.println("In UpdateCountry CountryID:"+CountryID+"flagurl:"+flagurl);
			String updateCountryQuery = "update tblCountryFlag set FlagURL = '"+flagurl+"'"+"where CID ='"+CountryID+"'"; 
			sql.execSQL(updateCountryQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch(SQLException e){}
		catch (RuntimeException e){}
	}

	/** SaveCountryImage in the CountryImages folder inside Manahij */

	public static ArrayList<String> SaveCountryImage(String CountryID,String flagurl)
	{
		File file = new File(Environment.getExternalStorageDirectory().toString(),"/CountryImages"); 
		try
		{
			if(file.exists())
			{
				//System.out.println("Country image folder file exists already");
			}
			else
			{
				if(file.mkdirs())
				{
					//System.out.println("Country image folder File created");
				}
			}
		}
		catch(Exception e)
		{
			//System.out.println("Failed to create folder country image with Error"+e);
		}

		String CImagePath ="/sdcard/CountryImages/"+CountryID+".png";
		//System.out.println("CountryImagePath:"+CImagePath);
		countryImagePath.add(CImagePath);
		File countryImageFile = new File(CImagePath);
		if(countryImageFile.exists())
		{
			//System.out.println("Country Image file exists at the given path with name"+CountryID+".png");
		}
		else
		{
			//System.out.println("CountryImage file doesn't exists at the given path called download method");
			//ManahijStore.Downloadfile(flagurl,CImagePath);
			//create NoCover.gif image and copy it..  this can be done in imageloader.java
		}
		return countryImagePath;
	}

	/**  Get the selected CountryID from the Database */

	public static String GetCountrySelected()
	{  
		//Need to call these functions
		//System.out.println("GetCountrySelected");
		String returnValue = "";
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String getCountryListQuery = "select CountryID from tblLastViewedCountry";
		Cursor c = sql.rawQuery(getCountryListQuery, null);
		//System.out.println("Cursor c"+c.getString(0));
		c.moveToFirst();
		returnValue =c.getString(0);
		c.close();
		sql.close();
		return returnValue;
	}

	/**  Update country selected in the Database */

	public static void UpdateCountrySelected(String Value)
	{   
		try
		{
			//Need to call these functions
			//System.out.println("UpdateCountrySelectedWith:"+Value);
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			String updateCountryQuery = "update tblLastViewedCountry set CountryID = '"+Value+"'"; //Need to check the update tblcountryflag 
			sql.execSQL(updateCountryQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch(SQLException e){}
		catch (RuntimeException e){}
	}

	/** GetCountryList from the Database */

	public static ArrayList<String>  GetCountryListFromSQL()
	{
		//System.out.println("In getCountryListFromSQL");
		countryArrayInSQL.clear();
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String getCountryListQuery = "select * from tblCountryFlag";
		Cursor c = sql.rawQuery(getCountryListQuery, null);
		//System.out.println("Cursor c"+c.getCount());
		c.moveToFirst();
		while(c.isAfterLast() ==false)
		{
			String CID = c.getString(0);
			String flagurl = c.getString(1);
			String CID_flagurl = CID.concat("#").concat(flagurl);
			countryArrayInSQL.add(CID_flagurl);  					
			c.moveToNext();
		}
		//for(int i=0;i<countryArrayInSQL.size();i++)
		//System.out.println("CountryArrayinSql content at i:"+i+countryArrayInSQL.get(i));
		c.close();
		sql.close();

		return countryArrayInSQL;
	}

	/** Country Methods Ended */

	/**  University Methods Started */

	public static ArrayList<String> getUniversityNameForTheSelectedCountry(ArrayList<String> universityArray,int SelectedCountryID)
	{
		filteredUniversityArray.clear();
		if(SelectedCountryID>0)
		{
			for( int i=0; i<universityArray.size();i++)
			{
				String UnivArrayContent =universityArray.get(i);
				//System.out.println("UnivArrayContent: "+UnivArrayContent);
				String[] splitUnivContent = UnivArrayContent.split("#");
				int CountryID = Integer.parseInt(splitUnivContent[4]);
				//System.out.println("CountryID"+CountryID);
				if(CountryID == SelectedCountryID)
				{
					if(filteredUniversityArray.size() == 0)
					{ 
						//الجميع
						String ConcatStr ="الجميع#All#0#0";//جميع
						filteredUniversityArray.add(ConcatStr);
					}
					filteredUniversityArray.add(universityArray.get(i));
					//System.out.println("FilteredUniversityArray:"+filteredUniversityArray);
				}
			}
		}
		else
		{
			String  CValue="";
			int CFlag = 0;
			for(int i=0;i<universityArray.size();i++)
			{
				if(filteredUniversityArray.size() == 0)
				{
					String ConcatStr ="الجميع#All#0#0";
					filteredUniversityArray.add(ConcatStr);
				}
				String[] splitUnivContent = universityArray.get(i).split("#");
				CValue = splitUnivContent[1].toLowerCase();
				CFlag = 0;
				for (int j=0; j<filteredUniversityArray.size();j++)
				{
					String[] splitFilterUnivContent = filteredUniversityArray.get(j).split("#");
					String splitValue = splitFilterUnivContent[1].toLowerCase();
					if(CValue.equals(splitValue))
					{
						CFlag = 1;
						break;
					}
				}
				if(CFlag == 0)
				{
					filteredUniversityArray.add(universityArray.get(i));
				}
			}
			//System.out.println("FilteredUniversityArray:"+filteredUniversityArray);
		}
		return filteredUniversityArray;
	}

	/** save universityimage in universityimages folder in Manahij */

	public static ArrayList<String> SaveUniversityImage(String UniversityID,String flagurl)
	{
		//load the flagurl and save it as Data. Resize the image and save it in files directory.
		//System.out.println("Called saveUniversityImage");
		File file = new File(Environment.getExternalStorageDirectory().toString(),"/UniversityImages"); //Gets the external sdCard/StoreImages/
		try
		{
			if(file.mkdirs())
			{
				//System.out.println("Universityimages folder created");
			}
		}
		catch(Exception e)
		{
			//System.out.println("Failed to create universityimages folder with Error"+e);
		}
		String UImagePath ="/sdcard/UniversityImages/"+UniversityID+".png";
		//System.out.println("CountryImagePath:"+UImagePath);
		//file = getBaseContext().getFileStreamPath(UniversityImagePath);
		universityImagePath.add(UImagePath);
		File universityimagefile = new File(UImagePath);
		if(universityimagefile.exists())
		{
			//System.out.println("UniversityImage file exists at the given path with name"+UniversityID+".png");
		}
		else
		{
			//System.out.println("UniversityImage file doesn't exists at the given path called download method");
			//ManahijStore.Downloadfile(flagurl,UImagePath);
			//create NoCover.gif image and copy it..  this can be done in imageloader.java
		} 	
		return universityImagePath;
	}

	/* InsertUniversity ID and flag url in database tblUniversityFlag */

	public static void InsertUniversity(String UniversityID, String flagurl)
	{
		try
		{
			//System.out.println("Called insertUniversity");
			SQLiteDatabase sql = mydatabase.getWritableDatabase(); //Need to check data base exists or not
			//System.out.println("UniversityID:"+UniversityID+"flagurl:"+flagurl);
			String insertUniversityQuery = "insert into tblUniversityFlag(UID,FlagURL) values('"+UniversityID+"','"+flagurl+"')";
			sql.execSQL(insertUniversityQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch(SQLException e){}
		catch (RuntimeException e){}
	}

	/* UpdateUniversity ID and flag url in database tblUniversityFlag */

	public static void UpdateUniversity(String UniversityID, String flagurl)
	{
		try
		{
			//System.out.println("UpdateUniversity");
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			//System.out.println("In UpdateUniversity UniversityID:"+UniversityID+"flagurl:"+flagurl);
			String updateUniversityQuery = "update tblUniversityFlag set FlagURL = '"+flagurl+"'"+"where UID ='"+UniversityID+"'"; 
			sql.execSQL(updateUniversityQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch(SQLException e){}
		catch (RuntimeException e){}
	}

	/* Get the universitylist from the database tblUniversityflag */

	public static ArrayList<String> GetUniversityListFromSQL()
	{
		//System.out.println("In getUniversityListFromSQL");
		//universityArrayInSQL.removeAll(universityArrayInSQL); //Removes all content.
		universityArrayInSQL.clear();
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String getCountryListQuery = "select * from tblUniversityFlag";
		Cursor c = sql.rawQuery(getCountryListQuery, null);
		//System.out.println("Cursor c"+c.getCount());
		c.moveToFirst();
		while(c.isAfterLast() ==false)
		{
			String CID = c.getString(0);
			String flagurl = c.getString(1);
			String CID_flagurl = CID.concat("#").concat(flagurl);
			universityArrayInSQL.add(CID_flagurl);
			c.moveToNext();
		}
		//System.out.println("UniversityArrayinSql count:"+universityArrayInSQL.size());	
		c.close();
		sql.close();
		return universityArrayInSQL;
	}

	/** University methods ended */

	/* Store Book Images */

	/* Insert StoreBookImage in the database tblStoreBookImage */  

	public static void InsertStoreBookImage(String BookID, String imgurl)
	{
		try
		{
			//System.out.println("Called InsertStoreBookImage");
			SQLiteDatabase sql = mydatabase.getWritableDatabase(); //Need to check data base exists or not
			//System.out.println("BookID:"+BookID+"flagurl:"+imgurl);
			String insertStoreImageQuery = "insert into tblStoreBookImage(storeBookID,storeBookImageURL) values('"+BookID+"','"+imgurl+"')";
			sql.execSQL(insertStoreImageQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
	}

	/* Updatestorebookimage in the database tblStoreBookImage */

	public static void UpdateStoreBookImage(String BookID, String imgurl)
	{
		try
		{
			//System.out.println("UpdateStoreBookImage");
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			//System.out.println("In UpdateStoreBookImage BookID:"+BookID+"flagurl:"+imgurl);
			String updateStoreImageQuery = "update tblStoreBookImage set storeBookImageURL = '"+imgurl+"'"+"where storeBookImageURL ='"+BookID+"'"; //Need to check the update tblcountryflag 
			sql.execSQL(updateStoreImageQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
	}

	/*
	 *  SaveStoreBookImage in StoreImages folder in Manahij
	 */

	public static void SaveStoreBookImage(String BookID,String imgurl)
	{
		//load the flagurl and save it as Data. Resize the image and save it in files directory.
		//System.out.println("Called saveStoreBookImage");
		File file = new File(Environment.getExternalStorageDirectory().toString(),"/StoreImages"); //Gets the external sdCard/StoreImages/
		try
		{
			if(file.exists())
			{
				//System.out.println("Storeimages folder exists already");
			}
			else
			{
				if(file.mkdirs())
				{
					//System.out.println("Storeimages folder created");
				}
			}
		}
		catch(Exception e)
		{
			//System.out.println("Failed to create Storeimages folder with Error "+e);
		}
		String StoreBImagePath ="/sdcard/StoreImages/"+BookID+".png";
		//System.out.println("StoreBookImagePath:"+StoreBImagePath);
		//file = getBaseContext().getFileStreamPath(StoreBImagePath);
		File storeImageFile = new File(StoreBImagePath);
		if(storeImageFile.exists())
		{
			//System.out.println("StoreBImage file exists at the given path with name"+BookID+".png");
		}
		else
		{
			//System.out.println("StoreBImage file doesn't exists at the given path called download method");
			//Downloadfile(imgurl,StoreBImagePath); 
		}

	}

	/*
	 *  Get the storebookimage list from databse tblStoreBookImage
	 */

	public static ArrayList<String> GetStoreBookImageListFromSQL()
	{
		//System.out.println("In getStoreBookImageListFromSQL");
		//storeBookImageInSQL.removeAll(storeBookImageInSQL); //Removes all content.
		storeBookImageInSQL.clear();
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String getCountryListQuery = "select * from tblStoreBookImage";
		Cursor c = sql.rawQuery(getCountryListQuery, null);
		//System.out.println("Cursor c"+c.getCount());
		c.moveToFirst();
		while(c.isAfterLast() ==false)
		{
			String BookID = c.getString(0);
			String flagurl = c.getString(1);
			String BookID_flagurl = BookID.concat("#").concat(flagurl);
			storeBookImageInSQL.add(BookID_flagurl);
			c.moveToNext();
		}
		//System.out.println("StoreBookImageInSQL count:"+storeBookImageInSQL.size());
		c.close();
		sql.close();
		return storeBookImageInSQL;
	}

	/*
	 *  checkLanguge:
	 */

	public static Integer checkLanguage()
	{
		int lang=1;
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String LangQuery ="select * from tblLanguage";
		Cursor c = sql.rawQuery(LangQuery, null);
		c.moveToFirst();
		lang = c.getInt(0);
		c.close();
		sql.close();
		return lang; 	
	}

	/*
	 * update language in database tblLanguage
	 */

	public static void updateDatabaseLanguage(int lang)
	{
		try
		{
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			//System.out.println("In UpdateDatabaseLang:"+lang);
			String updateUniversityQuery = "update tblLanguage set LanguageID = '"+lang+"'"; 
			sql.execSQL(updateUniversityQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
	}
	/*
	 *  get the downloadBooks list from database tblBook used in BookModal
	 */
	public ArrayList<String> DownloadBooksList()
	{
		downloadedBooksArray = new ArrayList<String>();
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String downloadedBooksQuery = "select storeID from books where StoreBook='true'";
		Cursor c = sql.rawQuery(downloadedBooksQuery, null);
		c.moveToFirst();
		while(c.isAfterLast() == false)
		{
			String Bname = c.getString(0);
			//String CID = c.getString(1);
			//String Bname_CID = Bname.concat("#").concat(CID);
			downloadedBooksArray.add(Bname);
			c.moveToNext();
		}
		c.close();
		sql.close();
		return downloadedBooksArray; 
	}
	public ArrayList<String> DownloadingBooksList()
	{
		downloadingBooksArray = new ArrayList<String>();
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String downloadedBooksQuery = "select storeID from books where isDownloadedCompleted='downloading'";
		Cursor c = sql.rawQuery(downloadedBooksQuery, null);
		c.moveToFirst();
		while(c.isAfterLast() == false)
		{
			String Bname = c.getString(0);
			//String CID = c.getString(1);
			//String Bname_CID = Bname.concat("#").concat(CID);
			downloadingBooksArray.add(Bname);
			c.moveToNext();
		}
		c.close();
		sql.close();
		return downloadingBooksArray;
	}

	/*
	 *  save complete details about a book in database tblBook
	 */
	public static void SaveBooksToDatabase(ArrayList<String>books,int bookClickPosition, int selectCategoryId)
	{
		String ID, price, bookURL, bookTitle, description, author, imageURL, totalPages, LSearch, LBookmark, LCopy, LFlip, LNavigation;
		String LHighlight, LNote, LGoToPage, LIndexPage, LZoom, LWebSearch, LWikiSearch, LUpdateCounts, LYoutube, LGoogle, LJazira, LBing, LAsk;
		String LYahoo, LNoor, LNoorBook, LInAppProductID, LTranslationSearch, LDictionarySearch, LBookSearch, LBookLanguage;
		String CurrentBook= books.get(bookClickPosition);
		//String CurrentBook="261#0.00#http://School.nooor.com/Semalibrary/Sboook/Iphone/88ffe611-0ac5-44f8-b819-5f37db29c30e/88ffe611-0ac5-44f8-b819-5f37db29c30e.zip#√ø√ü≈∏√ë√ø‚Ñ¢√ø¬±√ø¬Æ≈∏√§√ø¬© √ø√ü≈∏√ë√ø¬£√ø‚â•√ø¬±≈∏√§√ø¬© - ≈∏√â√ø‚Ñ¢√ø√ü√ø¬Æ √ø√ü≈∏√ë√ø‚àë√ø√ü≈∏√ë√ø¬Æ√ø¬© #√ø√ü≈∏√ë√ø¬µ≈∏√Ö √ø√ü≈∏√ë√ø¬¥√ø√ü≈∏√ú≈∏√§ √ø√ü≈∏√ë√ø√ü√ø¬Æ√ø‚Ñ¢√ø√ò√ø√ü√ø¬∂≈∏√§ - √ø√ü≈∏√ë≈∏√Ö√ø¬µ≈∏√ë √ø√ü≈∏√ë√ø¬¥√ø√ü≈∏√ú≈∏√§#Semanoor#http://School.nooor.com/Semalibrary/Sboook/Iphone/88ffe611-0ac5-44f8-b819-5f37db29c30e/Card.gif#118#yes#yes#yes#yes#yes#yes#yes#yes#yes#yes#yes#yes#0#yes#yes#yes#yes#yes#yes#yes#yes#S261#yes#yes#yes#Arabic";
		String[] BookDetails = CurrentBook.split("#");
		ID=BookDetails[0];price=BookDetails[1]; bookURL=BookDetails[2]; bookTitle=BookDetails[3]; description=BookDetails[4]; 
		author=BookDetails[5]; imageURL=BookDetails[6]; totalPages=BookDetails[7]; LSearch=BookDetails[8]; LBookmark=BookDetails[9];
		LCopy=BookDetails[10]; LFlip=BookDetails[11]; LNavigation=BookDetails[12];LHighlight=BookDetails[13]; LNote=BookDetails[14];
		LGoToPage=BookDetails[15]; LIndexPage=BookDetails[16]; LZoom=BookDetails[17]; LWebSearch=BookDetails[18]; LWikiSearch=BookDetails[19];
		LUpdateCounts=BookDetails[20]; LYoutube=BookDetails[21]; LGoogle=BookDetails[22]; LJazira=BookDetails[23]; LBing=BookDetails[24];
		LAsk=BookDetails[25];LYahoo=BookDetails[26]; LNoor=BookDetails[27]; LNoorBook=BookDetails[28]; LInAppProductID=BookDetails[29];
		LTranslationSearch=BookDetails[30]; LDictionarySearch=BookDetails[31]; LBookSearch=BookDetails[32]; LBookLanguage=BookDetails[33];

		String purchased=CheckPurchase(); //changes for Purchase
		if(purchased.equals("unpaid"))
		{
			LSearch = "yes"; LBookmark ="yes"; LCopy="no"; LFlip="yes"; LNavigation="yes"; LHighlight="no"; LNote="no";
			LGoToPage="yes"; LIndexPage="yes"; LZoom="yes"; LWebSearch="no"; LWikiSearch="no";LYoutube="no";LGoogle="no";
			LJazira="no"; LBing="no"; LAsk="no"; LYahoo="no"; LNoor="no";LNoorBook="no"; LTranslationSearch="no"; LDictionarySearch="no"; LBookSearch="no";
		}
		else if(purchased.equals("paid"))
		{
			LSearch = "yes"; LBookmark ="yes"; LCopy="yes"; LFlip="yes"; LNavigation="yes"; LHighlight="yes"; LNote="yes";
			LGoToPage="yes"; LIndexPage="yes"; LZoom="yes"; LWebSearch="yes"; LWikiSearch="yes";LYoutube="yes";LGoogle="yes";
			LJazira="yes"; LBing="yes"; LAsk="yes"; LYahoo="yes"; LNoor="yes";LNoorBook="yes"; LTranslationSearch="yes"; LDictionarySearch="yes"; LBookSearch="yes"; 
		}
		/*else{
					 LSearch = "yes"; LBookmark ="yes"; LCopy="no"; LFlip="yes"; LNavigation="yes"; LHighlight="no"; LNote="no";
					 LGoToPage="yes"; LIndexPage="yes"; LZoom="yes"; LWebSearch="no"; LWikiSearch="no";LYoutube="no";LGoogle="no";
					 LJazira="no"; LBing="no"; LAsk="no"; LYahoo="no"; LNoor="no";LNoorBook="no"; LTranslationSearch="no"; LDictionarySearch="no"; LBookSearch="no";*/

		// } //changes for purchase
		//String purchase = "unpaid";
		try
		{
			SQLiteDatabase sql = mydatabase.getWritableDatabase(); //Need to check data base exists or not
			String BookModalQuery = "insert into tblBook(BName,NoOfPages,CID,Search,Bookmark,Copy,Flip,Navigation,Highlight,Note,GotoPage,indexPage,Zoom,WebSearch,WikiSearch,UpdateCounts,MLastViewedPage,youtube,google,jazeera,bing,ask,yahoo,semaContent,semaBook,TranslationSearch,dictionarySearch,booksearch,blanguage)"+
					"values('"+ID+"','"+Integer.parseInt(totalPages)+"','"+selectCategoryId+"','"+LSearch+"','"+LBookmark+"','"+LCopy+"','"+LFlip+"','"+LNavigation+"','"+LHighlight+"','"+LNote+"','"+LGoToPage+"','"+LIndexPage+"','"+LZoom+"','"+LWebSearch+"','"+LWikiSearch+"','"+Integer.parseInt(LUpdateCounts)+"','"+0+"','"+LYoutube+"','"+LGoogle+"','"+LJazira+"','"+LBing+"','"+LAsk+"','"+LYahoo+"','"+LNoor+"','"+LNoorBook+"','"+LTranslationSearch+"','"+LDictionarySearch+"','"+LBookSearch+"','"+LBookLanguage+"')";
			sql.execSQL(BookModalQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
		//System.out.println("successfully updated bookdetails in database");

	}

	public static String getBookNameToStoreInExternalSDCard( int Count)
	{
		int i=0;
		String Bname="";
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String BnameQuery = "select BName from tblBook";
		Cursor c = sql.rawQuery(BnameQuery, null);
		c.moveToFirst();
		while(c.isAfterLast() == false)
		{
			if(i==Count)
			{
				Bname = c.getString(0);
				break;
			}
			i++;
			c.moveToNext();
		}
		c.close();
		sql.close();
		return Bname;
	}

	/*
	 *  checkStoreDestroy:
	 */

	public static Integer checkStoreDestroy()
	{
		int Dvalue = 0;
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String LangQuery ="select * from tblStoreDestroy";
		Cursor c = sql.rawQuery(LangQuery, null);
		c.moveToFirst();
		Dvalue = c.getInt(0);
		c.close();
		sql.close();
		return Dvalue; 	
	}

	/*
	 * update Dvalue in database tblStoreDestroy
	 */

	public static void updateStoreDestroy(int Dvalue)
	{
		try
		{
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			//System.out.println("In UpdateStoreDestroy:"+Dvalue);
			String updateUniversityQuery = "update tblStoreDestroy set DestroyValue = '"+Dvalue+"'"; 
			sql.execSQL(updateUniversityQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
	}

	/*
	 *  Database to update purchase state:
	 */
	public static void updatePurchase(String PValue)
	{
		try
		{
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			//System.out.println("In updatePurchase:"+PValue);
			String updatePurchaseQuery = "update tblPurchase set Purchase = '"+PValue+"'"; 
			sql.execSQL(updatePurchaseQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
	}

	/*
	 *  Get purchase value :
	 */
	public static String CheckPurchase()
	{
		String PValue = "";
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String PurchaseQuery ="select * from tblPurchase";
		Cursor c = sql.rawQuery(PurchaseQuery, null);
		c.moveToFirst();
		PValue = c.getString(0);
		c.close();
		sql.close();
		return PValue; 	
	}

	/*
	 *  Enrichment store Methods:
	 *  
	 */

	/*
	 *  Get the selected CountryID from the Database
	 */

	public static String GetEnrichCountrySelected()
	{  
		//Need to call these functions
		//System.out.println("GetCountrySelected");
		String returnValue = "";
		SQLiteDatabase sql =mydatabase.getReadableDatabase();
		String getCountryListQuery = "select CountryID from tblEnrichLastViewedCountry";
		Cursor c = sql.rawQuery(getCountryListQuery, null);
		//System.out.println("Cursor c"+c.getString(0));
		c.moveToFirst();
		returnValue =c.getString(0);
		c.close();
		sql.close();
		return returnValue;
	}

	/*
	 *  Update country selected in the Database
	 */


	public static void UpdateEnrichCountrySelected(String Value)
	{   
		try
		{
			//Need to call these functions
			//System.out.println("UpdateCountrySelectedWith:"+Value);
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			String updateCountryQuery = "update tblEnrichLastViewedCountry set CountryID = '"+Value+"'"; //Need to check the update tblcountryflag 
			sql.execSQL(updateCountryQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
	}

	public static ArrayList<String> getEnrichUniversityNameForTheSelectedCountry(ArrayList<String> universityArray,int SelectedCountryID)
	{
		EfilteredUniversityArray.clear();
		if(SelectedCountryID>0)
		{
			for( int i=0; i<universityArray.size();i++)
			{
				String UnivArrayContent =universityArray.get(i);
				//System.out.println("UnivArrayContent: "+UnivArrayContent);
				String[] splitUnivContent = UnivArrayContent.split("#");
				int CountryID = Integer.parseInt(splitUnivContent[4]);
				//System.out.println("CountryID"+CountryID);
				if(CountryID == SelectedCountryID)
				{
					if(EfilteredUniversityArray.size() == 0)
					{ 
						//الجميع
						String ConcatStr ="الجميع#All#0#0";      //لجميع
						EfilteredUniversityArray.add(ConcatStr);
					}
					EfilteredUniversityArray.add(universityArray.get(i));
					//System.out.println("EFilteredUniversityArray:"+EfilteredUniversityArray);
				}
			}
		}
		else
		{
			String  CValue="";
			int CFlag = 0;
			for(int i=0;i<universityArray.size();i++)
			{
				if(EfilteredUniversityArray.size() == 0)
				{
					String ConcatStr ="الجميع#All#0#0";
					EfilteredUniversityArray.add(ConcatStr);
				}
				String[] splitUnivContent = universityArray.get(i).split("#");
				CValue = splitUnivContent[1].toLowerCase();
				CFlag = 0;
				for (int j=0; j<EfilteredUniversityArray.size();j++)
				{
					String[] splitFilterUnivContent = EfilteredUniversityArray.get(j).split("#");
					String splitValue = splitFilterUnivContent[1].toLowerCase();
					if(CValue.equals(splitValue))
					{
						CFlag = 1;
						break;
					}
				}
				if(CFlag == 0)
				{
					EfilteredUniversityArray.add(universityArray.get(i));
				}
			}
			//System.out.println("EFilteredUniversityArray:"+EfilteredUniversityArray);
		}

		return EfilteredUniversityArray;
	}

	public static Integer checkEnrichStoreDestroy()
	{
		int Dvalue = 0;
		SQLiteDatabase sql = mydatabase.getReadableDatabase();
		String LangQuery ="select * from tblEnrichStoreDestroy";
		Cursor c = sql.rawQuery(LangQuery, null);
		c.moveToFirst();
		Dvalue = c.getInt(0);
		c.close();
		sql.close();
		return Dvalue; 	
	}

	/*
	 * update Dvalue in database tblStoreDestroy
	 */

	public static void updateEnrichStoreDestroy(int Dvalue)
	{
		try
		{
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			//System.out.println("In UpdateStoreDestroy:"+Dvalue);
			String updateUniversityQuery = "update tblEnrichStoreDestroy set DestroyValue = '"+Dvalue+"'"; 
			sql.execSQL(updateUniversityQuery);
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
	}

	/*
	 * Slect BName from tblPayPalCheck
	 */

	public static String getPayPalPaidBooks(String bname)
	{
		String BName = "";
		try
		{
			SQLiteDatabase db = mydatabase.getReadableDatabase();
			String query = "select BName from tblPayPalCheck where BName='"+bname+"'";
			Cursor c = db.rawQuery(query, null);
			c.moveToFirst();
			if(c != null && c.moveToFirst())
			{
				BName = c.getString(0);
			}
			c.close();
			db.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
		return BName;
	}

	/*
	 * Insert BName to tblPayPalCheck
	 */

	public static void insertBnameToPayPaltbl(String BookIDCode)
	{
		try
		{
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			sql.execSQL("insert into tblPayPalCheck(BName, Status) values('"+BookIDCode+"', '"+"paid"+"')");
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
	}

	/*
	 * Delete BName after tblPayPalCheck
	 */
	public static void deleteBnamefromPayPaltbl(String BookIDCode)
	{
		try
		{
			SQLiteDatabase sql = mydatabase.getWritableDatabase();
			sql.execSQL("delete from tblPayPalCheck where BName='"+BookIDCode+"'");
			sql.close();
		}
		catch (IndexOutOfBoundsException e){}
		catch (NullPointerException e){}
		catch (SQLException e){}
		catch (RuntimeException e){}
	}
}
