package com.semanoor.sboookauthor_store;

import java.util.ArrayList;
import android.content.Context;

public class SubTabs {

	private String tabTitle;

	private Context context;
	private String tabPath;
	private ArrayList<ListViewItem> listItemTabs = new ArrayList<ListViewItem>();
	private int nodeType;

	public SubTabs(Context _context) {
		this.context = _context;
	}

	/*
	 * @return the enrichmentTitle
	 */
	public String getTabTitle() {
		return tabTitle;
	}
	/**
	 * @param tabTitle the tabTitle to set
	 */
	public void setTabTitle(String tabTitle) {
		this.tabTitle = tabTitle;
	}


	/**
	 * @return the tabPath
	 */
	public String getTabPath() {
		return tabPath;
	}
	/**
	 * @param tabPath the tabPath to set
	 */
	public void setTabPath(String tabPath) {
		this.tabPath =tabPath;
	}

	public ArrayList<ListViewItem> getListItemTabs() {
		return listItemTabs;
	}

	public void setListItemTabs(ArrayList<ListViewItem> listItemTabs) {
		this.listItemTabs = listItemTabs;
	}

	/**
	 * @return the nodeType
	 */
	public int getNodeType() {
		return nodeType;
	}

	/**
	 * @param nodeType the nodeType to set
	 */
	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

}
