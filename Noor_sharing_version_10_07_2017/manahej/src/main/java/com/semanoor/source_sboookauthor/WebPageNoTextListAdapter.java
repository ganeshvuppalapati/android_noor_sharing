package com.semanoor.source_sboookauthor;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.TextView;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;

public class WebPageNoTextListAdapter extends BaseAdapter {
	
	private BookViewActivity context;
	private Integer[] webPageNoTextInfoListArray;
	private CustomWebRelativeLayout customWRL;
	private PopoverView popoverView;
	
	public WebPageNoTextListAdapter(BookViewActivity _context, Integer[] _webPageNoTextInfoListArray, CustomWebRelativeLayout customWebRelativeLayout, PopoverView popoverView) {
		this.context = _context;
		this.webPageNoTextInfoListArray = _webPageNoTextInfoListArray;
		this.customWRL = customWebRelativeLayout;
		this.popoverView = popoverView;
	}

	@Override
	public int getCount() {
		return webPageNoTextInfoListArray.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (position == 0) {
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_size, null);
			NumberPicker np = (NumberPicker) view.findViewById(R.id.numberPicker1);
			np.setMinValue(8);
			np.setMaxValue(72);
			np.setValue(customWRL.fontSize);
			np.setOnValueChangedListener(new OnValueChangeListener() {
				
				@Override
				public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
					customWRL.getAndSetSelectionIsActive();
					customWRL.changeFontSize(newVal, "content");
				}
			});
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webPageNoTextInfoListArray[position]);
		}
		else if (position == 1) {
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_font, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webPageNoTextInfoListArray[position]);
			TextView tv_fontFamily = (TextView) view.findViewById(R.id.textView2);
			tv_fontFamily.setText(customWRL.fontFamily);
		}
		else if (position == 2) {
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webPageNoTextInfoListArray[position]);
			ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
			if (customWRL.fontColor != null && !customWRL.fontColor.equals("transparent")) {
				iv.setBackgroundColor(Color.parseColor(customWRL.fontColor));
			}
		}
		else if (position == 4) {
			final int initPageNoSize = (int) context.getResources().getDimension(R.dimen.bookview_initial_text_page_no_size);
			view = context.getLayoutInflater().inflate(R.layout.popover_page_no, null);
			final int initPageNoPos = (int) context.getResources().getDimension(R.dimen.bookview_initial_text_page_no_pos);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setTypeface(null, Typeface.NORMAL);
			Button btn_lu = (Button) view.findViewById(R.id.btn_lu);
			Button btn_cu = (Button) view.findViewById(R.id.btn_cu);
			Button btn_ru = (Button) view.findViewById(R.id.btn_ru);
			Button btn_ld = (Button) view.findViewById(R.id.btn_ld);
			Button btn_cd = (Button) view.findViewById(R.id.btn_cd);
			Button btn_rd = (Button) view.findViewById(R.id.btn_rd);
			btn_lu.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					int initialWebTextXpos = initPageNoPos;
					int initialWebTextYpos = initPageNoPos;
					context.showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
					popoverView.dissmissPopover(true);
				}
			});
			
			btn_cu.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					int initialWebTextXpos = context.designPageLayout.getWidth()/2-initPageNoSize/2;
					int initialWebTextYpos = initPageNoPos;
					context.showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
					popoverView.dissmissPopover(true);
				}
			});
			
			btn_ru.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					int initialWebTextXpos = (context.designPageLayout.getWidth()-initPageNoPos)-initPageNoSize;
					int initialWebTextYpos = initPageNoPos;
					context.showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
					popoverView.dissmissPopover(true);
				}
			});

			btn_ld.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					int initialWebTextXpos = initPageNoPos;
					int initialWebTextYpos = (context.designPageLayout.getHeight()-initPageNoPos)-initPageNoSize;
					context.showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
					popoverView.dissmissPopover(true);
				}
			});

			btn_cd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					int initialWebTextXpos = context.designPageLayout.getWidth()/2-initPageNoSize/2;
					int initialWebTextYpos = (context.designPageLayout.getHeight()-initPageNoPos)-initPageNoSize;
					context.showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
					popoverView.dissmissPopover(true);
				}
			});

			btn_rd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					int initialWebTextXpos = (context.designPageLayout.getWidth()-initPageNoPos)-initPageNoSize;
					int initialWebTextYpos = (context.designPageLayout.getHeight()-initPageNoPos)-initPageNoSize;
					context.showAddPageNoDialogView(initialWebTextXpos, initialWebTextYpos);
					popoverView.dissmissPopover(true);
				}
			});
		}
		else if (position == 6) {
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
			ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
			if (customWRL.backgroundColor != null && !customWRL.backgroundColor.equals("transparent")) {
				iv.setBackgroundColor(Color.parseColor(customWRL.backgroundColor));
			}
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webPageNoTextInfoListArray[position]);
		}
		else {
			view = context.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setTextColor(Color.parseColor("#ff000000"));
			tv.setText(webPageNoTextInfoListArray[position]);
		}
		return view;
	}

}
