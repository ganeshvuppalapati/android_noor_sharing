package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;

import com.ptg.mindmap.widget.MindmapPlayer;
import com.semanoor.sboookauthor_store.Enrichments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

public class GenerateHTML {

	private Context context;
	private ArrayList<Object> objectList;
	private int pageNumber;
	private DatabaseHandler db;
	private Book currentBook;
	private double scaleX, scaleY;
	private ArrayList<Enrichments> enrichmentTabListArray;
	private String metaViewPort;
	private int htmlPageWidth, htmlPageHeight;
	boolean storeBookPage;

	public GenerateHTML(Context ctx, DatabaseHandler _db, Book _currentBook, String pageNum, ArrayList<Enrichments> _enrichmentTabListArray,boolean StorePage) {
		this.context = ctx;
		this.db = _db;
		this.currentBook = _currentBook;
		this.pageNumber = Integer.parseInt(pageNum);
		storeBookPage=StorePage;
		if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() && !currentBook.get_bStoreID().contains("P")||storeBookPage) {
			int pageWidth = Globals.getDeviceWidth();
			//int pageHeight = (int) (Globals.getDeviceHeight() - context.getResources().getDimension(R.dimen.shelf_toolbar_height));
			int paddingSpace;
			if (currentBook.isCssType) {
				paddingSpace = 80;
			} else {
				paddingSpace = 60;
			}
			int actionBarHeight = getActionBarHeight() + paddingSpace;
			int pageHeight = Globals.getDeviceHeight() - actionBarHeight;
			//String[] offsetWidthAndHeight = currentBook.get_bOffsetWidthAndHeight().split("\\|");
			int offsetWidth = 980, offsetHeight = 1334;
			this.scaleX = (double) pageWidth/offsetWidth;
			this.scaleY = (double) pageHeight/offsetHeight;
			//this.scaleX = 0.784;
			//this.scaleY = 0.783;
			metaViewPort = "";
		} else {
			metaViewPort = "<meta name='viewport' content='width=device-width, height = device-height, initial-scale=1.0'></meta>";
		}
		this.enrichmentTabListArray = _enrichmentTabListArray;
	}

	private int getActionBarHeight(){
		int actionBarHeight = 0;
		TypedValue tv = new TypedValue();
		if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
			actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
		}
		return actionBarHeight;
	}
	
	public void generatePage(String fileBasePath, ArrayList<View> deletedObjectList) {
		this.objectList = db.getAllObjectsForGeneratingHTML(String.valueOf(pageNumber), currentBook.getBookID(), 0);
		setCurrentPageWidthAndHeight("");
		if (deletedObjectList != null && !deletedObjectList.isEmpty()) {
			filterObjectList(deletedObjectList);
		}
		//File htmlFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+pageNumber+".htm");
		File htmlFilePath = new File(fileBasePath+pageNumber+".htm");
		generateHTMLFromObjectList(htmlFilePath, false, "", 0);
		generateOnlyEnrichedPages(deletedObjectList);
	}
	
	public void generateOnlyEnrichedPages(ArrayList<View> deletedObjectList){
		
		if (enrichmentTabListArray != null) {
			for (int i=1; i<enrichmentTabListArray.size(); i++) {
				Enrichments enrichments = enrichmentTabListArray.get(i);
				this.objectList = db.getAllObjectsForGeneratingHTML(String.valueOf(pageNumber), currentBook.getBookID(), enrichments.getEnrichmentId());
				setCurrentPageWidthAndHeight(enrichments.getEnrichmentType());
				if (deletedObjectList != null && !deletedObjectList.isEmpty()) {
					filterObjectList(deletedObjectList);
				}
				int enrichNo = i+1;
				File htmlFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+pageNumber+"-"+enrichments.getEnrichmentId()+".htm");
				generateHTMLFromObjectList(htmlFilePath, false, enrichments.getEnrichmentType(), enrichments.getEnrichmentId());
			}
		}
	}
	
	/**
	 * this filters the object from deleted object list array
	 * @param deletedObjectList
	 */
	private void filterObjectList(ArrayList<View> deletedObjectList){
		ArrayList<Object> tempObjectList = new ArrayList<Object>(this.objectList);
		for (int i = 0; i < objectList.size(); i++) {
			for (int j = 0; j < deletedObjectList.size(); j++) {
				HTMLObjectHolder htmlObject = (HTMLObjectHolder) objectList.get(i);
				View view = deletedObjectList.get(j);
				if (view instanceof CustomImageRelativeLayout) {
					int delObjUniqueId = ((CustomImageRelativeLayout) view).getObjectUniqueId();
					int objUniqueId = htmlObject.getObjUniqueRowId();
					if (delObjUniqueId == objUniqueId) {
						//this.objectList.remove(i);
						//removeIndexValue.add(i);
						tempObjectList.remove(htmlObject);
					}
				} else if (view instanceof CustomWebRelativeLayout) {
					int delObjUniqueId = ((CustomWebRelativeLayout) view).getObjectUniqueId();
					int objUniqueId = htmlObject.getObjUniqueRowId();
					if (delObjUniqueId == objUniqueId) {
						//this.objectList.remove(i);
						//removeIndexValue.add(i);
						tempObjectList.remove(htmlObject);
					}
				} else if (view instanceof MindmapPlayer) {
					int delObjUniqueId = ((MindmapPlayer) view).getObjectUniqueId();
					int objUniqueId = htmlObject.getObjUniqueRowId();
					if (delObjUniqueId == objUniqueId) {
						//this.objectList.remove(i);
						//removeIndexValue.add(i);
						tempObjectList.remove(htmlObject);
					}
				}
			}
		}
		this.objectList = tempObjectList;
	}
	
	public void generateEnrichedPageForExportAndZipIt(Enrichments enrich){
		this.objectList = db.getAllObjectsForGeneratingHTML(String.valueOf(pageNumber), currentBook.getBookID(), enrich.getEnrichmentId());
		setCurrentPageWidthAndHeight("");

		String htmlFileDirPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/"+currentBook.getBookID()+"-"+pageNumber+"-"+enrich.getEnrichmentId();
		UserFunctions.createNewDirectory(htmlFileDirPath);
		File htmlFilePath = new File(htmlFileDirPath+"/index.htm");
		generateHTMLFromObjectList(htmlFilePath, true, enrich.getEnrichmentType(), enrich.getEnrichmentId());
		Zip zip = new Zip();
		String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/"+currentBook.getBookID()+"-"+pageNumber+"-"+enrich.getEnrichmentId()+".zip";
		zip.zipFileAtPath(htmlFileDirPath, destPath);
		UserFunctions.DeleteDirectory(new File(htmlFileDirPath));
	}
	
	private void setCurrentPageWidthAndHeight(String enrichmentType) {
		if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() && (enrichmentType.equals("duplicate") || enrichmentType.equals("")) ||storeBookPage) {
			if (objectList.isEmpty()) {
				htmlPageWidth = Globals.getPixelValue(Globals.getDesignPageWidth(), context);
				htmlPageHeight = Globals.getPixelValue(Globals.getDesignPageHeight(), context);
				return;
			}
			HTMLObjectHolder htmlObject = (HTMLObjectHolder) objectList.get(0);
			int width_px, height_px;
			String objType = htmlObject.getObjType();
			if (objType.equals("WebText")) {
				width_px = (int) (htmlObject.getObjWidth()/scaleX);
				height_px = (int) (htmlObject.getObjHeight()/scaleY);
				htmlPageWidth = width_px;
				htmlPageHeight = height_px;
			} else {
				htmlPageWidth = Globals.getPixelValue(Globals.getDesignPageWidth(), context);
				htmlPageHeight = Globals.getPixelValue(Globals.getDesignPageHeight(), context);
			}
		} else {
			htmlPageWidth = Globals.getPixelValue(Globals.getDesignPageWidth(), context);
			htmlPageHeight = Globals.getPixelValue(Globals.getDesignPageHeight(), context);
		}
	}

	private void setMetaViewPort(String enrichmentType){
		if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() ||storeBookPage) {
			if (enrichmentType.equals("duplicate") || enrichmentType.equals("")) {
				metaViewPort = "";
			} else {
				metaViewPort = "<meta name='viewport' content='width=device-width, height = device-height, initial-scale=1.0'></meta>";
			}
		} else {
			metaViewPort = "<meta name='viewport' content='width=device-width, height = device-height, initial-scale=1.0'></meta>";
		}
	}

	private void generateHTMLFromObjectList(File htmlFilePath, boolean isExportEnrichments, String enrichmentType, int enrichId){
		String HTMLString = null;
		String pageBgPath;
		String pageBgColorValue;
		String imageRotationAngleandScaleMode;
		int imageRotatedAngle = 0;
		String scaleMode = null;
		String BGScript;
		String audioScript;
		setMetaViewPort(enrichmentType);
		//Audio Script
		if (isExportEnrichments) {
			audioScript = "\nfunction isPlaying(playerId){"+
					"\nvar player = document.getElementById(playerId);"+
					"\nreturn !player.paused && !player.ended && 0 < player.currentTime;}"+
					"\nfunction FinishedPlaying(imageId){"+
					"\nvar imgId = document.getElementById(imageId);"+
					"\nimgId.src = \"Default_Audio_Play.png\";}"+
					"\nfunction StartOrStop(audioFile, imageId, audioId){"+
					"\nvar playing = false;"+
					"\nvar audie = document.getElementById(audioId);"+
					"\nvar imgId = document.getElementById(imageId);"+
					"\nplaying = isPlaying(audioId);"+
					"\naudie.src = audioFile;"+
					"\nif(playing){"+
					"\nimgId.src = \"Default_Audio_Play.png\";"+
					"\naudie.pause();"+
					"\n}else{"+
					"\nimgId.src = \"Default_Audio_Stop.png\";"+
					"\naudie.play();}}";
		} else {
			audioScript = "\nfunction isPlaying(playerId){"+
					"\nvar player = document.getElementById(playerId);"+
					"\nreturn !player.paused && !player.ended && 0 < player.currentTime;}"+
					"\nfunction FinishedPlaying(imageId){"+
					"\nvar imgId = document.getElementById(imageId);"+
					"\nimgId.src = \"images/Default_Audio_Play.png\";}"+
					"\nfunction StartOrStop(audioFile, imageId, audioId){"+
					"\nvar playing = false;"+
					"\nvar audie = document.getElementById(audioId);"+
					"\nvar imgId = document.getElementById(imageId);"+
					"\nplaying = isPlaying(audioId);"+
					"\naudie.src = audioFile;"+
					"\nif(playing){"+
					"\nimgId.src = \"images/Default_Audio_Play.png\";"+
					"\naudie.pause();"+
					"\n}else{"+
					"\nimgId.src = \"images/Default_Audio_Stop.png\";"+
					"\naudie.play();}}";
		}
		
		
		/* String quizScript ="function checkAnswer(answerId, buttonId, radioBtnGroupName, resultDivId){\n"+
                        "var btnCheckAnswer = document.getElementById(buttonId);\n"+
                        "if(btnCheckAnswer.value == 'CheckAnswer') {\n"+
                            "var btnInput = document.getElementById(answerId);\n"+
                            "var resultElement = document.getElementById(resultDivId);\n"+
                            "resultElement.style.visibility = \"visible\";"+
                            "if(btnInput.checked == true) {\n"+
                                "//alert(\"Correct Answer\");\n"+
                                "//var audio = new Audio('images/Correct.mp3');\n"+
                                "//audio.play();\n"+
                                "resultElement.innerHTML = '<p style=\"color:#3f8f3f\"> Excellent </p>';\n"+
                                "localStorage.setItem(radioBtnGroupName, 'true');\n"+
                                "//btnCheckAnswer.value = 'ClearAnswer';\n"+
                                "//btnInput.setAttribute('checked', true);\n"+
                                "//disableAllRadioButtons(radioBtnGroupName);\n"+
                            "} else {\n"+
                                "//alert(\"Wrong Answer\");\n"+
                                "//var audio = new Audio('images/Wrong.mp3');\n"+
                                "//audio.play();\n"+
                                "resultElement.innerHTML = '<p style=\"color:#dc2f19\"> Wrong Answer </p>';\n"+
                                "localStorage.setItem(radioBtnGroupName, 'false');\n"+
                            "}\n"+
                        "} else if(btnCheckAnswer.value == 'ClearAnswer') {\n"+
                            "btnCheckAnswer.value = 'CheckAnswer';\n"+
                            "enableAllRadioButtons(radioBtnGroupName);\n"+
                            "removeAllCheckedAttribute(radioBtnGroupName);\n"+
                        "}\n"+
                        "}\n"+
                        "function disableAllRadioButtons(radioBtnGroupName){\n"+
                            "var radioBtnGroups = document.getElementsByName(radioBtnGroupName);\n"+
                            "for(var i=0; i<radioBtnGroups.length; i++) {\n"+
                                "radioBtnGroups[i].disabled = true;\n"+
                            "}\n"+
                        "}\n"+
                        "function enableAllRadioButtons(radioBtnGroupName){\n"+
                            "var radioBtnGroups = document.getElementsByName(radioBtnGroupName);\n"+
                            "for(var i=0; i<radioBtnGroups.length; i++) {\n"+
                                "radioBtnGroups[i].disabled = false;\n"+
                            "}\n"+
                        "}\n"+
                        "function removeAllCheckedAttribute(radioBtnGroupName){\n"+
                            "var radioBtnGroups = document.getElementsByName(radioBtnGroupName);\n"+
                            "for(var i=0; i<radioBtnGroups.length; i++) {\n"+
                                "radioBtnGroups[i].setAttribute('checked', false);\n"+
                                "radioBtnGroups[i].removeAttribute('checked');\n"+
                            "}\n"+
                        "}\n"+
                        "function quizNextView(parentDiv){\n"+
                            "var element = document.getElementById(parentDiv);\n"+
                            "element.parentNode.scrollLeft += element.offsetWidth;\n"+
                            "if((element.parentNode.childElementCount - 1) * element.offsetWidth == element.parentNode.scrollLeft) {\n"+
                                "var correctlyAnswered = 0;\n"+
                                "for(var i=0; i<element.parentNode.childElementCount; i++){\n"+
                                    "var id = element.parentNode.children[i].id;\n"+
                                    "var value = localStorage.getItem(id);\n"+
                                    //alert(value);+
                                    "if(value == 'true'){\n"+
                                        "correctlyAnswered++;\n"+
                                    "}\n"+
                                    //alert(id);+
                                "}\n"+
                            "var totalNoOfQuestions = (element.parentNode.childElementCount-1);\n"+
                            "var wronglyAnswered = (element.parentNode.childElementCount-1) - correctlyAnswered;\n"+
                            "var totalMarks = Math.round((correctlyAnswered/totalNoOfQuestions) * 100);\n"+
                            "var splitDiv = parentDiv.split(\"_\");\n"+
                            "//alert(splitDiv[1]);\n"+
                            "var secId = splitDiv[0].split(\"q\");\n"+
                            "//alert(secId[1]);\n"+
                            "var resultContainerEle = document.getElementById('results_'+splitDiv[1]);\n"+
                        "var innerHtml = '<p style=\"text-align: left; font-weight:bold;\">Results:</p><p style=\"font-weight:bold; color:#3f8f3f;\">Questions Answered Correctly:'+correctlyAnswered+'</p><p style=\"font-weight:bold; color:#dc2f19;\">Questions Answered Wrongly:'+wronglyAnswered+'</p><p style=\"font-weight:bold; color:#000000;\">Total Marks:'+totalMarks+'';\n"+
                            "resultContainerEle.innerHTML = innerHtml;\n"+
                            "//alert(correctlyAnswered+'/'+(element.parentNode.childElementCount-1));\n"+
                        "}\n"+
                        "}\n"+
                        "function quizPreviousView(parentDiv){\n"+
                            "var element = document.getElementById(parentDiv);\n"+
                            "element.parentNode.scrollLeft -= element.offsetWidth;\n"+
                        "}\n"+
                        "function restartQuiz(divId) {\n"+
                            "var element = document.getElementById(divId);\n"+
                            "for(var i=0; i<(element.parentNode.childElementCount-1); i++){\n"+
                                "var radioBtnGroupName = element.parentNode.children[i].id;\n"+
                                "var splitDiv = radioBtnGroupName.split(\"_\");\n"+
                                "var secIdSplit = splitDiv[0].split(\"q\");\n"+
                                "var secId = secIdSplit[1];\n"+
                                "var uniqueId = splitDiv[1];\n"+
                                "var resultDivId = 'q'+secId+'_result_'+uniqueId+'';\n"+
                                "var resultDivElement = document.getElementById(resultDivId);\n"+
                                "resultDivElement.style.visibility = 'hidden';\n"+
                                "removeAllCheckedAttribute(radioBtnGroupName);\n"+
                            "}\n"+
                            "element.parentNode.scrollLeft = 0;\n"+
                        "}\n"+
                        "function isBookFromManahijIpad(){\n"+
                            "return true;\n"+
                        "}"+
                        "function isBookFromManahijAndroid(){\n"+
                        	"window.StoreObjects.setIsBookFromManahijAndroid(0, \"true\");"+
                        "}";

		*/
		String quizScript ="function checkAnswer(answerId, buttonId, radioBtnGroupName, resultDivId){\n" +
				                         "var Qdirection = document.getElementById(radioBtnGroupName).style.direction;" +
				                          //"alert(Qdirection);\"\n" +
				                          "var btnCheckAnswer = document.getElementById(buttonId);\n" +
				                          "if(btnCheckAnswer.value == 'CheckAnswer' || btnCheckAnswer.value == \"تحقق من الأجوبة  \") {\n" +
				                           "var btnInput = document.getElementById(answerId);\n" +
				                           "var resultElement = document.getElementById(resultDivId);\n" +
				                            "resultElement.style.visibility = \"visible\";\n" +
				                             "if(btnInput.checked == true) {\n" +
				                              "//alert(\"Correct Answer\");\n" +
				                               "//var audio = new Audio('images/Correct.mp3');\n" +
				                                "//audio.play();\n" +
				                               "if(Qdirection == \"ltr\"){" +
				                               "resultElement.innerHTML = '<p style=\"color:#3f8f3f\"> Excellent </p>';}\n"+//Excellent
				                                 "else{resultElement.innerHTML = '<p style=\"color:#3f8f3f\"> ممتاز </p>';}\n"+
				                               "localStorage.setItem(radioBtnGroupName, 'true');\n" +
				                                "//btnCheckAnswer.value = 'ClearAnswer';\n" +
				                                "//btnInput.setAttribute('checked', true);\n" +
				                                 "//disableAllRadioButtons(radioBtnGroupName);\n" +
				                                 "} else {\n" +
				                                 " //alert(\"Wrong Answer\");\n" +
				                                 "//var audio = new Audio('images/Wrong.mp3');\n" +
				                                  "//audio.play();\n" +
				                                  "if(Qdirection == \"ltr\"){" +
				                                  "resultElement.innerHTML = '<p style=\"color:#dc2f19\"> Wrong Answer </p>';} //Wrong Answer\n"+
				                                  "else{resultElement.innerHTML = '<p style=\"color:#dc2f19\"> الجواب خاطئ </p>';}\n" +
				                                  "localStorage.setItem(radioBtnGroupName, 'false');\n" +
				                              "}\n" +
				                            "} else if(btnCheckAnswer.value == 'ClearAnswer') {\n" +
				                             "if(Qdirection == \"ltr\"){" +
				                             "btnCheckAnswer.value = 'CheckAnswer';\n}" +//CheckAnswer\n" +
				                             "else{btnCheckAnswer.value ='تحقق من الأجوبة  ';\n}" +
				                             "enableAllRadioButtons(radioBtnGroupName);\n" +
				                            "removeAllCheckedAttribute(radioBtnGroupName);\n" +
				                        "}\n" +
				                       "}\n" +
				                        "function disableAllRadioButtons(radioBtnGroupName){\n" +
				                            "var radioBtnGroups = document.getElementsByName(radioBtnGroupName);\n" +
				                            "for(var i=0; i<radioBtnGroups.length; i++) {\n" +
				                                "radioBtnGroups[i].disabled = true;\n" +
				                             "}\n" +
				                           "}\n" +
				                           "function enableAllRadioButtons(radioBtnGroupName){\n"+
				                           "var radioBtnGroups = document.getElementsByName(radioBtnGroupName);\n" +
				                            "for(var i=0; i<radioBtnGroups.length; i++) {\n" +
				                                "radioBtnGroups[i].disabled = false;\n" +
				                           "}\n" +
				                         "}\n" +
				                         "function removeAllCheckedAttribute(radioBtnGroupName){\n" +
				                            "var radioBtnGroups = document.getElementsByName(radioBtnGroupName);\n" +
				                            "for(var i=0; i<radioBtnGroups.length; i++) {\n" +
				                                "radioBtnGroups[i].setAttribute('checked', false);\n" +
				                                "radioBtnGroups[i].removeAttribute('checked');\n" +
				                            "}\n" +
				                        "}\n" +
				                        "function quizNextView(parentDiv){\n" +
				                            "var element = document.getElementById(parentDiv);\n" +
				                           "var Qdirection = element.style.direction;\n" +
				                           "element.parentNode.scrollLeft += element.offsetWidth;\n" +
				                           "if((element.parentNode.childElementCount - 1) * element.offsetWidth == element.parentNode.scrollLeft) {\n" +
				                                "var correctlyAnswered = 0;\n" +
				                                "for(var i=0; i<element.parentNode.childElementCount; i++){\n" +
				                                    "var id = element.parentNode.children[i].id;\n" +
				                                    "var value = localStorage.getItem(id);\n" +
				                                    //alert(value);\n" +
				                                    "if(value == 'true'){\n" +
				                                        "correctlyAnswered++;\n" +
				                                    "}\n" +
				                                    //alert(id);\n" +
				                               "}\n" +
				                            "var totalNoOfQuestions = (element.parentNode.childElementCount-1);\n" +
				                           "var wronglyAnswered = (element.parentNode.childElementCount-1) - correctlyAnswered;\n" +
				                            "var totalMarks = Math.round((correctlyAnswered/totalNoOfQuestions) * 100);\n" +
				                           "var splitDiv = parentDiv.split(\"_\");\n" +
				                            "//alert(splitDiv[1]);\n" +
				                           "var secId = splitDiv[0].split(\"q\");\n" +
				                           "//alert(secId[1]);\n" +
				                           "var resultContainerEle = document.getElementById('results_'+splitDiv[1]);\n" +

				                            //\"var BtnRestart = document.getElementById('btnRestart').value = \\\"%@\\\";\"\n" +
				                            //\"var BtnBack = document.getElementById('btnBack').value = \\\"%@\\\";\"\n" +
				                            //\"alert(BtnBack.value+BtnRestart.value);\"\n" +
				                            //\"alert(Qdirection);\"\n" +
				                            "if(Qdirection == \"ltr\"){" +
				                           "var innerHtml = '<p style=\"text-align: left; font-weight:bold;\">Results</p><p style=\"font-weight:bold; color:#3f8f3f;\">Questions Answered Correctly:'+correctlyAnswered+'</p><p style=\"font-weight:bold; color:#dc2f19;\">Questions Answered Wrongly:'+wronglyAnswered+'</p><p style=\"font-weight:bold; color:#000000;\">Total Marks:'+totalMarks+'';\n" +
				                                "resultContainerEle.innerHTML = innerHtml;\n" +
				                                "resultContainerEle.style.direction = \"ltr\";\n" +
				                            "}else{\n" +
				                                "var innerHtml = '<p style=\"text-align: right; font-weight:bold;\">النتيجة</p><p style=\"font-weight:bold; color:#3f8f3f;\">عدد الاجوبة الصحيحة :'+correctlyAnswered+'</p><p style=\"font-weight:bold; color:#dc2f19;\">عدد الاجوبة الخاطئة :'+wronglyAnswered+'</p><p style=\"font-weight:bold; color:#000000;\">مجموع العلامات :'+totalMarks+'';\n" +
				                                "resultContainerEle.innerHTML = innerHtml;\n" +
				                               "resultContainerEle.style.direction = \"rtl\";\n" +
				                              "}" +
				                            "//alert(correctlyAnswered+'/'+(element.parentNode.childElementCount-1));\n" +//text-align, Results: ,Questions Answered Correctly:, Questions Answered Wrongly:, Total Marks:\n" +
				                               "}\n" +
				                        "}\n" +
				                        "function quizPreviousView(parentDiv){\n" +
				                            "var element = document.getElementById(parentDiv);\n" +
				                            "element.parentNode.scrollLeft -= element.offsetWidth;\n" +
				                        "}\n" +
				                        "function restartQuiz(divId) {\n" +
				                            "var element = document.getElementById(divId);\n" +
				                           "for(var i=0; i<(element.parentNode.childElementCount-1); i++){\n" +
				                               "var radioBtnGroupName = element.parentNode.children[i].id;\n" +
				                                "var splitDiv = radioBtnGroupName.split(\"_\");\n" +
				                               "var secIdSplit = splitDiv[0].split(\"q\");\n" +
				                                "var secId = secIdSplit[1];\n" +
				                                "var uniqueId = splitDiv[1];\n" +
				                                "var resultDivId = 'q'+secId+'_result_'+uniqueId+'';\n" +
				                                "var resultDivElement = document.getElementById(resultDivId);\n" +
				                                "resultDivElement.style.visibility = 'hidden';\n" +
				                              "removeAllCheckedAttribute(radioBtnGroupName);\n" +
				                            "}\n"+
				                            "element.parentNode.scrollLeft = 0;\n" +
				                        "}\n" +
				                        "function isBookFromManahijIpad(){\n" +
				                            "return true;\n" +
				                        "}"+
				                        "function isBookFromManahijAndroid(){\n"+
				                        "window.StoreObjects.setIsBookFromManahijAndroid(0, \"true\");"+
				                        "}";

		
		
		
		String quizStyles = ".btnChkAnswer, .btnQuizBack, .btnQuizRestart {display: inline-block;margin: 0 5px 0 0;padding: 3px 5px;" +
				"font-size: 18px;font-family: \"Bitter\",serif;line-height: 1.8;appearance: none;-webkit-appearance: none;color: #fff;" +
				"background-color: #1d2233;border: none;border-radius: 5px;}.btnChkAnswer:active {background-color: #25486f;" +
				"text-shadow: -1px 1px #193047;}.btnPrev {display: inline-block;margin: 0 5px 0 0;padding: 3px 17px;font-size: 18px;" +
				"font-family: \"Bitter\",serif;line-height: 1.8;appearance: none;-webkit-appearance: none;color: #fff;" +
				"background-color: #1d2233;border: none;border-radius: 5px;}.btnPrev:active {background-color: #25486f;" +
				"text-shadow: -1px 1px #193047;}.btnDel {display: inline-block;margin: 0 5px 0 0;padding: 3px 17px;font-size: 18px;" +
				"font-family: \"Bitter\",serif;line-height: 1.8;appearance: none;-webkit-appearance: none;color: #fff;" +
				"background-color: #1d2233;border: none;border-radius: 5px;}.btnDel:active {background-color: #25486f;" +
				"text-shadow: -1px 1px #193047;}.btnAdd {display: inline-block;margin: 0 5px 0 0;padding: 3px 17px;font-size: 18px;" +
				"font-family: \"Bitter\",serif;line-height: 1.8;appearance: none;-webkit-appearance: none;color: #fff;" +
				"background-color: #1d2233;border: none;border-radius: 5px;}.btnAdd:active {background-color: #25486f;" +
				"text-shadow: -1px 1px #193047;}.btnNext {display: inline-block;margin: 0 5px 0 0;padding: 3px 17px;font-size: 18px;" +
				"font-family: \"Bitter\",serif;line-height: 1.8;appearance: none;-webkit-appearance: none;color: #fff;" +
				"background-color: #1d2233;border: none;border-radius: 5px;}.btnNext:active {background-color: #25486f;" +
				"text-shadow: -1px 1px #193047;}";
		// Assigning CSS stylesheet for tables
		String tableStyles = "\ntable#SBATable{border-collapse:collapse;table-layout:fixed; overflow-x:scroll; word-break:break-all; width:100%;"+
				"\nheight:100%;}table#SBATable, td#SBATd{ border:1px solid black;background-color:transparent;text-align:center;}";
		
		String mainDivContent = "<div id='mainContent' style='margin:0 auto;position:relative;width:"+htmlPageWidth+"; height:"+htmlPageHeight+";'>";
		String mindmapJsScriptPath = "<script src=\"../../mindMaps/semamindmap.min.js\"></script>";
		String mindmapCssStylePath = "<link rel=\"stylesheet\" href=\"../../mindMaps/style.min.css\">";
		String cssFile = "<link rel=\"stylesheet\" href=\"../../mindMaps/style.min.css\">\n<link rel=\"stylesheet\" href=\"FreeFiles/css/normalize.css\">\n<link rel=\"stylesheet\" href=\"FreeFiles/css/main.css\">";

		//Get Page BackGround Path
		if (pageNumber == 1) {
			if (enrichId == 0) {
				pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/FreeFiles/pageBG_"+ pageNumber + ".png";
			}else{
				pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/FreeFiles/pageBG_" + pageNumber+"-"+enrichId + ".png";
			}
			if (!new File(pageBgPath).exists()) {
				BGScript = "\n<script type='text/javascript'>function SetDimensions(){} "+audioScript+"\n"+quizScript+"\n</script>";
				// Check for plain color page background
				pageBgColorValue = isPlainColorBackgroundExists(String.valueOf(pageNumber));
				if (pageBgColorValue.equals("")) {
					//Default Page Bg
					if (currentBook.getBookOrientation() == 0) {
						if (isExportEnrichments) {
							pageBgPath = "front_P.png";
							File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
							String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
							UserFunctions.copyFiles(sourcePath.toString(), destPath);
						} else {
							pageBgPath = "FreeFiles/front_P.png";
						}
					} else {
						if (isExportEnrichments) {
							pageBgPath = "front.png";
							File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
							String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
							UserFunctions.copyFiles(sourcePath.toString(), destPath);
						} else {
							pageBgPath = "FreeFiles/front.png";
						}
					}
					HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n"+mindmapCssStylePath+"\n"+mindmapJsScriptPath+"\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+quizStyles+"\n"+UserFunctions.fontFace+"\n" +
							"\n</style>\n"+BGScript+"\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n"+metaViewPort+"\n</head>" +
							"\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'> \n<div id='bgImage' style='background-image:url("+pageBgPath+"); background-repeat: no-repeat; " +
							"-webkit-background-size:100%; min-width:"+htmlPageWidth+"; min-height:"+htmlPageHeight+"; width:100%; height:100%; left:0px; top:0px; position:absolute;' >\n</div>\n"+mainDivContent+"";
				} else {
					//Page Bg plain color
					String hexPageBgColor = String.format("#%06X", (0xFFFFFF & Integer.parseInt(pageBgColorValue)));
					HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n"+mindmapCssStylePath+"\n"+mindmapJsScriptPath+"\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+quizStyles+"\n"+UserFunctions.fontFace+"\n" +
							"\n</style>\n"+BGScript+"\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n"+metaViewPort+"\n</head>" +
							"\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'> \n<div id='bgImage' style='background-color:"+hexPageBgColor+"; background-repeat: no-repeat; " +
							"-webkit-background-size:100%; min-width:"+htmlPageWidth+"; min-height:"+htmlPageHeight+"; width:100%; height:100%; left:0px; top:0px; position:absolute;' >\n</div>\n"+mainDivContent+"";
				}
			} else {
				imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), "PageBG", String.valueOf(pageNumber));
				if(imageRotationAngleandScaleMode==null){
					imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), Globals.OBJTYPE_MINDMAPBG, String.valueOf(pageNumber));
				}
				//PageBg path exist
				//imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), "PageBG", String.valueOf(pageNumber));
				imageRotatedAngle = Integer.parseInt(imageRotationAngleandScaleMode.split("\\|")[0]);
				scaleMode = imageRotationAngleandScaleMode.split("\\|")[1];
				BGScript = getPageBackgroundScript(imageRotatedAngle, audioScript,quizScript);
				String bgImageScaleOrTiledMode = getBgImageScaleorTiledString(pageBgPath, scaleMode, isExportEnrichments, htmlFilePath.getParent().toString(), mainDivContent);
				HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n"+mindmapCssStylePath+"\n"+mindmapJsScriptPath+"\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+quizStyles+"\n"+UserFunctions.fontFace+"\n" +
						"\n</style>\n"+BGScript+"\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n"+metaViewPort+"\n</head>\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'>"+bgImageScaleOrTiledMode+"";
			}
		} else if (pageNumber == currentBook.getTotalPages()) {
			pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/pageBG_End.png";
			if (!new File(pageBgPath).exists()) {
				BGScript = "\n<script type='text/javascript'>function SetDimensions(){} "+audioScript+"\n"+quizScript+"\n</script>";
				// Check for plain color page background
				pageBgColorValue = isPlainColorBackgroundExists("End");
				if (pageBgColorValue.equals("")) {
					if (currentBook.getBookOrientation() == 0) {
						if (isExportEnrichments) {
							pageBgPath = "back_P.png";
							File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
							String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
							UserFunctions.copyFiles(sourcePath.toString(), destPath);
						} else {
							pageBgPath = "FreeFiles/back_P.png";
						}
					} else {
						if (isExportEnrichments) {
							pageBgPath = "back.png";
							File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
							String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
							UserFunctions.copyFiles(sourcePath.toString(), destPath);
						} else {
							pageBgPath = "FreeFiles/back.png";
						}
					}
					HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n"+mindmapCssStylePath+"\n"+mindmapJsScriptPath+"\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+quizStyles+"\n"+UserFunctions.fontFace+"\n" +
							"\n</style>\n"+BGScript+"\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n"+metaViewPort+"\n</head>" +
							"\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'>\n<div id='bgImage' style='background-image:url("+pageBgPath+"); background-repeat: no-repeat;" +
							"-webkit-background-size:100%; min-width:"+htmlPageWidth+"; min-height:"+htmlPageHeight+"; width:100%; height:100%;left:0px; top:0px;position:absolute;'>\n</div>\n"+mainDivContent+"";
				} else {
					//Page bg plain color
					String hexPageBgColor = String.format("#%06X", (0xFFFFFF & Integer.parseInt(pageBgColorValue)));
					HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n"+mindmapCssStylePath+"\n"+mindmapJsScriptPath+"\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+quizStyles+"\n"+UserFunctions.fontFace+"\n" +
							"\n</style>\n"+BGScript+"\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n"+metaViewPort+"\n</head>" +
							"\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'>\n<div id='bgImage' style='background-color:"+hexPageBgColor+"; background-repeat: no-repeat;" +
							"-webkit-background-size:100%; min-width:"+htmlPageWidth+"; min-height:"+htmlPageHeight+"; width:100%; height:100%;left:0px; top:0px;position:absolute;'>\n</div>\n"+mainDivContent+"";
				}
			} else {
				//PageBg path exist
				imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), "PageBGEnd", "End");
				if(imageRotationAngleandScaleMode==null){
					imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), Globals.OBJTYPE_MINDMAPBG, "End");
				}
				imageRotatedAngle = Integer.parseInt(imageRotationAngleandScaleMode.split("\\|")[0]);
				scaleMode = imageRotationAngleandScaleMode.split("\\|")[1];
				BGScript = getPageBackgroundScript(imageRotatedAngle, audioScript,quizScript);
				String bgImageScaleOrTiledMode = getBgImageScaleorTiledString(pageBgPath, scaleMode, isExportEnrichments, htmlFilePath.getParent().toString(), mainDivContent);
				HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n"+mindmapCssStylePath+"\n"+mindmapJsScriptPath+"\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+quizStyles+"\n"+UserFunctions.fontFace+"\n" +
						"\n</style>\n"+BGScript+"\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n"+metaViewPort+"\n</head>\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'>"+bgImageScaleOrTiledMode+"";
			}
		} else {
			pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/pageBG_ALL.png";
			if (!new File(pageBgPath).exists()) {
				// Check for plain color page background
				BGScript = "\n<script type='text/javascript'>function SetDimensions(){} "+audioScript+"\n"+quizScript+"\n</script>";
				pageBgColorValue = isPlainColorBackgroundExists("0");
				if (pageBgColorValue.equals("")) {
					if (enrichId == 0) {
						pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/pageBG_"+pageNumber+".png";
					}else{
						pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/FreeFiles/pageBG_" +pageNumber+"-"+enrichId + ".png";
					}
					if (!new File(pageBgPath).exists()) {
						pageBgColorValue = isPlainColorBackgroundExists(String.valueOf(pageNumber));
						if (pageBgColorValue.equals("")) {
							if (currentBook.getBookOrientation() == 0) {
								if (isExportEnrichments) {
									if (currentBook.get_bStoreID() != null &&currentBook.get_bStoreID().contains("P")) {
										if(enrichmentType.equals("blank")){
											pageBgPath = "page_P.png";
										}else {
											pageBgPath = pageNumber + ".png";
										}
									}else {
										pageBgPath = "page_P.png";
									}
									File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
									String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
									UserFunctions.copyFiles(sourcePath.toString(), destPath);
								} else {
									//pageBgPath = "FreeFiles/page_P.png";
									if (currentBook.get_bStoreID() != null &&currentBook.get_bStoreID().contains("P")) {
										if(enrichmentType.equals("blank")){
											pageBgPath = "FreeFiles/page_P.png";
										}else {
											pageBgPath = "FreeFiles/" + pageNumber + ".png";
										}
									}else {
										pageBgPath = "FreeFiles/page_P.png";
									}
								}
							} else {
								if (isExportEnrichments) {
									pageBgPath = "page.png";
									File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
									String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
									UserFunctions.copyFiles(sourcePath.toString(), destPath);
								} else {
									pageBgPath = "FreeFiles/page.png";
								}
							}
							HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n"+mindmapCssStylePath+"\n"+mindmapJsScriptPath+"\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+quizStyles+"\n"+UserFunctions.fontFace+"\n" +
									"\n</style>\n"+BGScript+"\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n"+metaViewPort+"\n</head>" +
									"\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'>\n<div id='bgImage' style='background-repeat: no-repeat; background-position:center;" +
									"-webkit-background-size:100%; min-width:"+htmlPageWidth+"; min-height:"+htmlPageHeight+"; width:100%; height:100%;left:0px; top:0px;position:absolute;background-image:url("+pageBgPath+");'>\n</div>\n"+mainDivContent+"";
						} else {
							//Page bg plain color
							String hexPageBgColor = String.format("#%06X", (0xFFFFFF & Integer.parseInt(pageBgColorValue)));
							HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n"+mindmapCssStylePath+"\n"+mindmapJsScriptPath+"\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+quizStyles+"\n"+UserFunctions.fontFace+"\n" +
									"\n</style>\n"+BGScript+"\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n"+metaViewPort+"\n</head>" +
									"\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'>\n<div id='bgImage' style='background-repeat: no-repeat; background-position:center; " +
									"-webkit-background-size:100%; min-width:"+htmlPageWidth+"; min-height:"+htmlPageHeight+"; width:100%; height:100%;left:0px; top:0px;position:absolute;background-color:"+hexPageBgColor+";'>\n</div>\n"+mainDivContent+"";
						}
					}
					else {
						//Page bg path exist
						imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), "PageBG", String.valueOf(pageNumber));
						if(imageRotationAngleandScaleMode==null){
							imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), Globals.OBJTYPE_MINDMAPBG, String.valueOf(pageNumber));
						}
						imageRotatedAngle = Integer.parseInt(imageRotationAngleandScaleMode.split("\\|")[0]);
						scaleMode = imageRotationAngleandScaleMode.split("\\|")[1];
						BGScript = getPageBackgroundScript(imageRotatedAngle, audioScript, quizScript);
						String bgImageScaleOrTiledMode = getBgImageScaleorTiledString(pageBgPath, scaleMode, isExportEnrichments, htmlFilePath.getParent().toString(), mainDivContent);
						HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n" + mindmapCssStylePath + "\n" + mindmapJsScriptPath + "\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} " + tableStyles + "\n" + quizStyles + "\n" + UserFunctions.fontFace + "\n" +
								"\n</style>\n" + BGScript + "\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n" + metaViewPort + "\n</head>\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'>" + bgImageScaleOrTiledMode + "";
					}
				} else {
					//Page bg All plain color
					String hexPageBgColor = String.format("#%06X", (0xFFFFFF & Integer.parseInt(pageBgColorValue)));
					HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n"+mindmapCssStylePath+"\n"+mindmapJsScriptPath+"\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+quizStyles+"\n"+UserFunctions.fontFace+"\n" +
							"\n</style>\n"+BGScript+"\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n"+metaViewPort+"\n</head>" +
							"\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'>\n<div id='bgImage' style='background-repeat: no-repeat; background-position:center; " +
							"-webkit-background-size:100%; min-width:"+htmlPageWidth+"; min-height:"+htmlPageHeight+"; width:100%; height:100%;left:0px; top:0px;position:absolute;background-color:"+hexPageBgColor+";'>\n</div>\n"+mainDivContent+"";
				}
			}else {
				//Page bg All path exist
				imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), "PageBG", "0");
				imageRotatedAngle = Integer.parseInt(imageRotationAngleandScaleMode.split("\\|")[0]);
				scaleMode = imageRotationAngleandScaleMode.split("\\|")[1];
				BGScript = getPageBackgroundScript(imageRotatedAngle, audioScript,quizScript);
				String bgImageScaleOrTiledMode = getBgImageScaleorTiledString(pageBgPath, scaleMode, isExportEnrichments, htmlFilePath.getParent().toString(), mainDivContent);
				HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>"+cssFile+"\n"+mindmapCssStylePath+"\n"+mindmapJsScriptPath+"\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+quizStyles+"\n"+UserFunctions.fontFace+"\n" +
						"\n</style>\n"+BGScript+"\n<meta http-equiv='Content-Type' content='text/html' charset='utf-8'/>\n<title>New Page</title>\n"+metaViewPort+"\n</head>\n<body onload='SetDimensions();' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0' marginwidth='0' marginheight='0'>"+bgImageScaleOrTiledMode+"";
			}
		}

		for (int i = 0; i < objectList.size(); i++) {
			//System.out.println("objectList:"+objectList.get(i));
			HTMLObjectHolder htmlObject = (HTMLObjectHolder) objectList.get(i);
			String objType = htmlObject.getObjType();
			if(htmlObject.getObjScalePageToFit().contains("css")){
				String[] split=htmlObject.getObjScalePageToFit().split("##");
				String main="\""+split[8]+"\"";
				String normalize="\""+split[7]+"\"";
				String cssPath = "<link rel=\"stylesheet\" href=\"FreeFiles/css/normalize.css\">\n<link rel=\"stylesheet\" href=\"FreeFiles/css/main.css\">";
				String css = "<link rel=\"stylesheet\" href="+normalize+">\n<link rel=\"stylesheet\" href="+main+">";
                HTMLString=HTMLString.replace(cssPath,css);
			}
			if (objType.equals("DrawnImage")) {
				String imgPath = getHtmlObjectPath(htmlObject.getObjContentPath(), isExportEnrichments, htmlFilePath.getParent().toString());
				int seqID = htmlObject.getObjSequentialId();
				HTMLString = HTMLString.concat("\n<img class="+"\"DrawImage\""+" style='position:absolute; top:0px; left:0px;pointer-events:none;width:100%;z-index:"+seqID+";height:100%' src='"+imgPath+"'/>");
			} else if (objType.equals("Image")) {
				int x, y, width, height, x_px, y_px, width_px, height_px;
				if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() && (enrichmentType.equals("duplicate") || enrichmentType.equals("")) ||storeBookPage) {
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				String imgPath = getHtmlObjectPath(htmlObject.getObjContentPath(), isExportEnrichments, htmlFilePath.getParent().toString());
				int seqID = htmlObject.getObjSequentialId();
				HTMLString = HTMLString.concat("\n<img class="+"\"Image\""+" style='position:absolute;z-index:"+seqID+"; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; height:"+height_px+"px;-webkit-transform:rotate(0deg);' src='"+imgPath+"'>\n</img>");
			} else if (objType.equals("YouTube")) {
				int x, y, width, height, x_px, y_px, width_px, height_px;
				if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() && (enrichmentType.equals("duplicate") || enrichmentType.equals("")) ||storeBookPage) {
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				String urlPath = getHtmlObjectPath(htmlObject.getObjContentPath(), isExportEnrichments, htmlFilePath.getParent().toString());
				int seqID = htmlObject.getObjSequentialId();
				int uniqueID = htmlObject.getObjUniqueRowId();
				if (urlPath.contains("http://www.youtube.com/embed/")) {
					HTMLString = HTMLString.concat("\n<iframe class="+"\"YouTube\""+" id='player_"+uniqueID+"' style='position:absolute;z-index:"+seqID+"; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; height:"+height_px+"px' src='"+urlPath+"' frameborder='0' allowfullscreen>\n</iframe>");
				} else {
					HTMLString = HTMLString.concat("\n<video class="+"\"YouTube\""+" id=\"player_"+uniqueID+"\" style='position:absolute;z-index:"+seqID+"; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; height:"+height_px+"px' src='"+urlPath+"' controls=\"false\" autoplay=\"true\"> <source src='"+urlPath+"' type=\"video/mp4\"/></video>");
				}
			} else if (objType.equals("IFrame")) {
				int x, y, width, height, x_px, y_px, width_px, height_px;
				if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() && (enrichmentType.equals("duplicate") || enrichmentType.equals("")) ||storeBookPage) {
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				String urlPath = htmlObject.getObjContentPath();
				//urlPath = "http://"+urlPath;
				int seqID = htmlObject.getObjSequentialId();
				int uniqueID = htmlObject.getObjUniqueRowId();
				String loadingDivId = "LoadingIframe"+uniqueID;
//				if (width_px>530&&height_px>730){
//					HTMLString = HTMLString.concat("\n" +
//							"<html>\n" +
//							"<head>\n" +
//							"<script>document.location = \"https://www.google.co.in/?gfe_rd=cr&ei=2vEiWMO5E-nI8AeJrZGAAw&gws_rd=ssl\";</script>\n" +
//							"</head>\n" +
//							"<body>test</body>\n" +
//							"</html>");
////					HTMLString = HTMLString.concat("\n<div class=" + "\"Iframe\"" + " style='width:100%; z-index:" + seqID + "; width:100%; height:100%; " +
////							"overflow:auto; -webkit-overflow-scrolling:touch;background-color:#F5F5F5;'>\n<div id='" + loadingDivId + "' style=\"top:40%;left:40%;position:absolute;\">Loading..\n</div>\n<script>window.open('"+ urlPath +"', '_self ', 'location=yes');document.getElementById('" + loadingDivId + "').style.display='none';</script></div>");
//				}else {
					HTMLString = HTMLString.concat("\n<div class=" + "\"Iframe\"" + " style='position:absolute; z-index:" + seqID + "; top:" + y_px + "px; left:" + x_px + "px; width:" + width_px + "px; height:" + height_px + "px; " +
							"overflow:auto; -webkit-overflow-scrolling:touch;background-color:#F5F5F5;'>\n<iframe onLoad=\"document.getElementById('" + loadingDivId + "').style.display='none';\"" +
							" frameborder='0' style='width:100%;height:100%' src='" + urlPath + "'></iframe>\n<div id='" + loadingDivId + "' style=\"top:40%;left:40%;position:absolute;\">Loading..\n</div>\n</div>");
			//	}
			} else if (objType.equals("EmbedCode")) {
				int x, y, width, height, x_px, y_px, width_px, height_px;
				if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() && (enrichmentType.equals("duplicate") || enrichmentType.equals("")) ||storeBookPage) {
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				String embedTextContent = htmlObject.getObjContentPath();
				int seqID = htmlObject.getObjSequentialId();
				HTMLString = HTMLString.concat("<div class="+"\"EmbedCodeBox\""+" style='position:absolute; z-index:"+seqID+";border:1px solid;border-color:black; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; height:"+height_px+"px; overflow:auto;'>"+embedTextContent+"</div>");
			} else if (objType.equals("Audio")) {//Need to check for audio src bg img path
				int x, y, width, height, x_px, y_px, width_px, height_px;
				if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() && (enrichmentType.equals("duplicate") || enrichmentType.equals("")) ||storeBookPage) {
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				//makeFileWorldReadable(htmlObject.getObjContentPath());
				String urlPath = getHtmlObjectPath(htmlObject.getObjContentPath(), isExportEnrichments, htmlFilePath.getParent().toString());
				//String urlPath = "file:///mnt/sdcard/Music/SBA_AUDIO.m4a";
				//String urlPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/SBA_AUDIO.m4a";
				int seqID = htmlObject.getObjSequentialId();
				int uniqueID = htmlObject.getObjUniqueRowId();
				String imageID = "Audio_Image_ID_"+uniqueID;
				String audioID = "MyAudio_ID_"+uniqueID;
				String audImgSrc;
				if (isExportEnrichments) {
					audImgSrc = "Default_Audio_Play.png";
					File srcAudPlayPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "images/Default_Audio_Play.png");
					File srcAudStopPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "images/Default_Audio_Stop.png");
					File destAudPlayPath = new File(htmlFilePath.getParent().toString()+"/"+srcAudPlayPath.getName());
					File destAudStopPath = new File(htmlFilePath.getParent().toString()+"/"+srcAudStopPath.getName());
					if (!destAudPlayPath.exists()) {
						UserFunctions.copyFiles(srcAudPlayPath.toString(), destAudPlayPath.toString());
					} if (!destAudStopPath.exists()) {
						UserFunctions.copyFiles(srcAudStopPath.toString(), destAudStopPath.toString());
					}
				} else {
					audImgSrc = "images/Default_Audio_Play.png";
				}
				HTMLString = HTMLString.concat("\n<img class="+"\"Audio\""+" id='"+imageID+"' style='position:absolute; z-index:"+seqID+"; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; " +
						"height:"+height_px+"px' src='"+audImgSrc+"' onClick=\"StartOrStop('"+urlPath+"', '"+imageID+"', '"+audioID+"');\" alt=\"Play Button\">" +
						"\n</img>\n<audio style='visibility:hidden' onEnded=\"FinishedPlaying('"+imageID+"');\" controls id='"+audioID+"'>\n</audio>");
			} else if (objType.equals("WebText") || objType.equals("TextSquare") || objType.equals("TextRectangle") || objType.equals("TextCircle") || objType.equals(Globals.objType_pageNoText) || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT) || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
				int x, y, width, height, dupWidth, dupHeight, dupWidth_px, dupHeight_px, x_px, y_px, width_px, height_px;
				String htmlTextContent = htmlObject.getObjContentPath();
				if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() && (enrichmentType.equals("duplicate") || enrichmentType.equals(""))||storeBookPage) {
					dupWidth = htmlObject.getObjWidth();
					dupHeight = htmlObject.getObjHeight();
					dupWidth_px = Globals.getPixelValue(dupWidth, context);
					dupHeight_px = Globals.getPixelValue(dupHeight, context);
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
					htmlTextContent = htmlTextContent.replace("width: "+dupWidth_px+"px; height: "+dupHeight_px+"px;", "width: "+width_px+"px; height: "+height_px+"px;");
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				int seqID = htmlObject.getObjSequentialId();
				htmlTextContent = htmlTextContent.replace("contenteditable=\"true\"", "contenteditable=\"false\"");
				if (objType.equals(Globals.objType_pageNoText)) {
					int pno = pageNumber - 1;
					htmlTextContent = htmlTextContent.concat(pno+"</div>");
				} else if (objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
					htmlTextContent = htmlTextContent.replace("checked=\"true\"", "");
					htmlTextContent = htmlTextContent.replace("src=\"/"+Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/", "src=\"");
					//theHtmlText = [theHtmlText stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"src=\"file://localhost%@/",toDirTillBooks] withString:@"src=\""];
					if (htmlTextContent.contains("ltr")) {
						htmlTextContent = htmlTextContent.replace("value=\"CheckAnswer\" style=\"visibility:hidden\"","value=\"CheckAnswer\"");
						htmlTextContent = htmlTextContent.replace("value=\"تحقق من الأجوبة  \" style=\"visibility:hidden\"","value=\"CheckAnswer\"");
					}else{
						htmlTextContent = htmlTextContent.replace("value=\"CheckAnswer\" style=\"visibility:hidden\"" , "value=\"تحقق من الأجوبة  \"");
						htmlTextContent = htmlTextContent.replace("value=\"تحقق من الأجوبة  \" style=\"visibility:hidden\"", "value=\"تحقق من الأجوبة  \"");
					}
					//htmlTextContent = htmlTextContent.replace("value=\"CheckAnswer\" style=\"visibility:hidden\"", "value=\"CheckAnswer\"");
					htmlTextContent = htmlTextContent.replace("value=\"+\"", "value=\"+\" style=\"visibility:hidden\"");
					htmlTextContent = htmlTextContent.replace("value=\"-\"", "value=\"-\" style=\"visibility:hidden\"");
					if (isExportEnrichments) {
						String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+"images/";
						htmlTextContent= images(htmlTextContent,imgDir,htmlFilePath.getParent().toString());
					}
				}
				HTMLString = HTMLString.concat("\n<div class="+objType+" width='"+width_px+"' height='"+height_px+"' style='top:"+y_px+"px;left:"+x_px+"px;position:absolute;z-index:"+seqID+";visibility:show;overflow:hidden;word-wrap: break-word;'>"+htmlTextContent+"\n</div>");
			} else if (objType.equals("WebTable")) {
				int x, y, width, height, dupWidth, dupHeight, dupWidth_px, dupHeight_px, x_px, y_px, width_px, height_px;
				String htmlTextContent = htmlObject.getObjContentPath();
				if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() && (enrichmentType.equals("duplicate") || enrichmentType.equals("")) ||storeBookPage) {
					dupWidth = htmlObject.getObjWidth();
					dupHeight = htmlObject.getObjHeight();
					dupWidth_px = Globals.getPixelValue(dupWidth, context);
					dupHeight_px = Globals.getPixelValue(dupHeight, context);
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
					htmlTextContent = htmlTextContent.replace("width: "+dupWidth_px+"px; height: "+dupHeight_px+"px;", "width: "+width_px+"px; height: "+height_px+"px;");
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				
				int seqID = htmlObject.getObjSequentialId();
				htmlTextContent = htmlTextContent.replace("contenteditable=\"true\"", "contenteditable=\"false\"");
				HTMLString = HTMLString.concat("\n<div class="+"\"webTable\""+" style='width:"+width_px+"px; height:"+height_px+"px; top:"+y_px+"px;left:"+x_px+"px;position: absolute;z-index:"+seqID+";visibility: show;margin:0px;overflow:hidden;word-wrap: break-word;'>"+htmlTextContent+"\n</div>");
			} else if (objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
				int x, y, width, height, dupWidth, dupHeight, dupWidth_px, dupHeight_px, x_px, y_px, width_px, height_px;
				String mindmapFileName = htmlObject.getObjContentPath();
				if (currentBook.is_bStoreBook() && !currentBook.isStoreBookCreatedFromTab() && (enrichmentType.equals("duplicate") || enrichmentType.equals(""))||storeBookPage) {
					dupWidth = htmlObject.getObjWidth();
					dupHeight = htmlObject.getObjHeight();
					dupWidth_px = Globals.getPixelValue(dupWidth, context);
					dupHeight_px = Globals.getPixelValue(dupHeight, context);
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				String mindMapSplit=mindmapFileName.replace(".xml",".png");
				String srcPath=Globals.TARGET_BASE_MINDMAP_PATH+mindMapSplit;
				String desPath=Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/" + "images/";
				File fileImgDir = new File(desPath);
				if (!fileImgDir.exists()) {
					fileImgDir.mkdir();
					UserFunctions.makeFileWorldReadable(desPath);
				}
				String mindMapDesPath = desPath+mindMapSplit;
				UserFunctions.copyFiles(srcPath,mindMapDesPath);
				String mindMapPath = "images/"+mindMapSplit;
				int seqID = htmlObject.getObjSequentialId();
				HTMLString = HTMLString.concat("\n<img class="+"\"Image\""+" style='position:absolute;z-index:"+seqID+"; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; height:"+height_px+"px;-webkit-transform:rotate(0deg);' src='"+mindMapPath+"'>\n</img>");
//				String mindmapScript = generateMindMapHTMLContent(mindmapFileName, width_px, height_px, x_px, y_px);
//				HTMLString = HTMLString.concat(mindmapScript);
			}
		}
		HTMLString = HTMLString.concat("</div>\n</body>\n</html>");
		
		if (isExportEnrichments) {
			HTMLString = extractBgImagesFromHTML(HTMLString, htmlFilePath.getParent().toString());
		}
		
		try {
			FileWriter fileWriter = new FileWriter(htmlFilePath);
			fileWriter.write(HTMLString);
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String generateMindMapHTMLContent(String mindMapName, int width, int height, int xPos, int yPos){
		String mindMapPath = Globals.TARGET_BASE_MINDMAP_PATH+mindMapName+".xml";
		String mindMapContent = UserFunctions.readFileFromPath(new File(mindMapPath));
		mindMapContent = mindMapContent.replace("\n", "");
		mindMapContent = mindMapContent.replace(" ", "");
		String[] splitMindMapContent = mindMapContent.split("\\$");
		String scriptString = String.format(Locale.US, "\n<script>\n" +
				"var mapPropertiesXML ='%s';" +
				"var mapNodePropertiesXML = '%s';" +
				"mapNodePropertiesXML = mapNodePropertiesXML.replace(/\\(|null\\)/g, \"\");" +
				"var dataProvider = [];" +
				"dataProvider[0] = mapPropertiesXML;" +
				"dataProvider[1] = mapNodePropertiesXML;" +
				"var birdsView = false;" +
				"var sMap = new SemaMindmap({id:'newDiv',width:%d,height:%d,top:%d,left:%d,displayBirdsView:birdsView,data:dataProvider});" +
				"sMap.createMap();" +
				"function updateMindMap(w,h,l,t){" +
				"SMM.repositionMindMap(l,t);" +
				"SMM.resizeMindMap(w,h);" +
				"}" +
				"function repositionMindMap(l,t){" +
				"SMM.repositionMindMap(l,t);" +
				"}" +
				"function resizeMindMap(l,t){" +
				"SMM.resizeMindMap(l,t);" +
				"}" +
				"</script>\n", splitMindMapContent[0], splitMindMapContent[1], width, height, yPos, xPos);
		return scriptString;
	}
	
	/**
	 * this extracts the background image while exporting enriched pages
	 * @param htmlString
	 * @param htmlFileDirPath
	 * @return
	 */
	private String extractBgImagesFromHTML(String htmlString, String htmlFileDirPath){
		if (htmlString.contains("file:///data/data/"+Globals.getCurrentProjPackageName()+"/files/books/")) {
			String[] splitStr = htmlString.split("file:///data/data/"+Globals.getCurrentProjPackageName()+"/files/books/");
			String[] splitstr1;
			for (int i = 1; i < splitStr.length; i++) {
				splitstr1 = splitStr[i].split("\"");
				String finalStr = "file:///data/data/"+Globals.getCurrentProjPackageName()+"/files/books/"+splitstr1[0];
				File srcBgImgPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+splitstr1[0]);
				File destPath = new File(htmlFileDirPath+"/"+srcBgImgPath.getName());
				UserFunctions.copyFiles(srcBgImgPath.toString(), destPath.toString());
				htmlString = htmlString.replace(finalStr, srcBgImgPath.getName());
			}
		}
		return htmlString;
	}
	
	private void makeFileWorldReadable(String path){
		File f = new File(path);
		if (f.exists()) {
			try {
				Runtime.getRuntime().exec("chmod o+r"+path);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Filter the path to get html object path
	 * @param path
	 * @param isExportEnrichments 
	 * @param htmlFileDirPath 
	 * @return
	 */
	private String getHtmlObjectPath(String path, boolean isExportEnrichments, String htmlFileDirPath) {
		if (path.contains("data/data") && !isExportEnrichments) {
			String[] pathArr = path.split("/");
			String imgPath = pathArr[pathArr.length - 2]+"/"+pathArr[pathArr.length - 1];
			return imgPath;
		} else if (path.contains("data/data") && isExportEnrichments) {
			File sourcePath = new File(path);
			File destPath = new File(htmlFileDirPath+"/"+sourcePath.getName());
			UserFunctions.copyFiles(sourcePath.toString(), destPath.toString());
			File filePath = new File(path);
			return filePath.getName();
		}
		return path;
	}
	
	/**
	 * Check isPlainColorBackground exist
	 * @param pNumber
	 * @return
	 */
	public String isPlainColorBackgroundExists(String pNumber){
		String objType;
		int pageNo = 0;
		if (!pNumber.equals("End")) {
			pageNo = Integer.parseInt(pNumber);
		}
		if (pageNo == 1) {
			objType = "PageColor";
		} else if (pNumber.equals("End")) {
			objType = "PageColorEnd";
		} else {
			objType = "PageColor";
		}
		String bgColorValue = db.getPlainBackgroundColorFromDB(currentBook.getBookID(), objType, pNumber);
		return bgColorValue;
	}

	/**
	 * Get the page background script
	 * @param rotationAngle
	 * @param audioJScript
	 * @return
	 */
	public String getPageBackgroundScript(int rotationAngle, String audioJScript, String quizScript){
		String BGScript = null;
		if (rotationAngle == 0 || rotationAngle == 180) {
			BGScript = "\n<script type='text/javascript'>\nfunction SetDimensions(){"+
					"\nvar BGDiv = document.getElementById('bgImage');"+
					"\nBGDiv.style.webkitTransform ='rotate("+rotationAngle+"deg)';"+
					"\n}"+audioJScript+"\n"+quizScript+"\n</script>";
		} else if (rotationAngle == 90 || rotationAngle == -90) {
			if (currentBook.getBookOrientation() == 0) {
				BGScript = "\n<script type='text/javascript'> \nfunction SetDimensions(){"+
						"\nvar height = document.body.scrollHeight; var BGDiv = document.getElementById('bgImage');BGDiv.style.height = height+'px'; BGDiv.style.width = height + 'px'; var width = document.body.clientWidth;"+
						"\nvar scaleX=0; var translateX=0;"+
						"\nif(height<width){"+
						"\nscaleX=height/width;"+
						"\ntranslateX=(width - height)/2;}"+
						"\nelse{"+
						"\nscaleX=width/height;"+
						"\ntranslateX=(height-width)/2;}"+
						"\nBGDiv.style.webkitTransform ='translate('+-translateX+'px,'+0+'px ) scale('+scaleX+',1.0) rotate("+rotationAngle+"deg)';}"+
						"\n"+audioJScript+"\n"+quizScript+"\n</script>";
			} else {
				BGScript = "\n<script type='text/javascript'> \nfunction SetDimensions(){"+
						"\nvar width = document.body.scrollWidth; var BGDiv = document.getElementById('bgImage');BGDiv.style.width = width+'px'; BGDiv.style.height = width + 'px'; var height = document.body.clientHeight;"+
						"\nvar scaleY=0; var translateY=0;"+
						"\nif(width<height){"+
						"\nscaleY=width/height;"+
						"\ntranslateY=(height - width)/2;}"+
						"\nelse{"+
						"\nscaleY=height/width;"+
						"\ntranslateY=(width-height)/2;}"+
						"\nBGDiv.style.webkitTransform ='translate('+0+'px ,'+-translateY+'px) scale(1.0,'+scaleY+') rotate("+rotationAngle+"deg)';}"+
						"\n"+audioJScript+"\n"+quizScript+"\n</script>";
			}
		}
		return BGScript;
	}

	/**
	 * get the bgimage scaled or tiled string
	 * @param pageBgPath
	 * @param scaleMode
	 * @param isExportEnrichments 
	 * @param htmlFileDirPath 
	 * @return
	 */
	public String getBgImageScaleorTiledString(String pageBgPath, String scaleMode, boolean isExportEnrichments, String htmlFileDirPath, String mainDivContent){
		String BgImage;
		String path = getHtmlObjectPath(pageBgPath, isExportEnrichments, htmlFileDirPath);
		if (scaleMode.equals("Tiled")) {
			BgImage = "\n<div id='bgImage' style ='background-image:url("+path+"); min-width:"+htmlPageWidth+"; min-height:"+htmlPageHeight+"; width:100%; height:100%; left:0px; top:0px;position:absolute;'>\n</div>\n"+mainDivContent+"";
		} else {
			BgImage = "\n<div id='bgImage' style='background-image:url("+path+");  background-repeat: no-repeat; -webkit-background-size:100%; min-width:"+htmlPageWidth+"; min-height:"+htmlPageHeight+"; width:100%; height:100%;left:0px; top:0px; position:absolute;'>\n</div>\n"+mainDivContent+"";
		}
		return BgImage;
	}


	public String images(String quiz,  String srcPath, String filePath){
		String content=quiz;
		if (content.contains("src")) {
			String[] splitStr = content.split("src=");
			if (splitStr.length > 0) {
				for(int i=0;i<splitStr.length;i++) {
					if(i!=0) {
//						if(splitStr[i].contains("onclick"))
						String[] path1 = splitStr[i].split("onclick");
						if(path1[0].contains(".png")) {
							String value = path1[0].replace("\"", "");
							String[] imagePath= path1[0].split("/");
							String[] Path=imagePath[imagePath.length-1].split("\\?");
							System.out.println(imagePath.length);

							File sourcePath = new File(srcPath+Path[0]);
							String destPath = filePath+"/"+sourcePath.getName();
							UserFunctions.copyFiles(sourcePath.toString(), destPath);
							content=content.replace("images/"+Path[0],Path[0]);

						}
					}
				}
			}
		}
		return content;

	}
}
