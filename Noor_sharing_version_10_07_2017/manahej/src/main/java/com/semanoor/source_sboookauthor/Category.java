package com.semanoor.source_sboookauthor;

import java.io.Serializable;

public class Category implements Serializable {
	private int categoryId;
	private String categoryName;
	private String categoryImagePath;
	private boolean isAccordExpanded;
	private int newCatID;
	private boolean isHidden;
	/**
	 * @return the categoryId
	 */
	public int getCategoryId() {
		return categoryId;
	}
	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}
	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	/**
	 * @return the isAccordExpanded
	 */
	public boolean isAccordExpanded() {
		return isAccordExpanded;
	}
	/**
	 * @param isAccordExpanded the isAccordExpanded to set
	 */
	public void setAccordExpanded(boolean isAccordExpanded) {
		this.isAccordExpanded = isAccordExpanded;
	}
	/**
	 * @return the categoryImagePath
	 */
	public String getCategoryImagePath() {
		return categoryImagePath;
	}
	/**
	 * @param categoryImagePath the categoryImagePath to set
	 */
	public void setCategoryImagePath(String categoryImagePath) {
		this.categoryImagePath = categoryImagePath;
	}

	public int getNewCatID() {
		return newCatID;
	}

	public void setNewCatID(int newCatID) {
		this.newCatID = newCatID;
	}

	public boolean isHidden() {
		return isHidden;
	}

	public void setHidden(boolean hidden) {
		isHidden = hidden;
	}
}
