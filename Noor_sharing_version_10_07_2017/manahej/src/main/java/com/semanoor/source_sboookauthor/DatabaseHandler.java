package com.semanoor.source_sboookauthor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ptg.mindmap.widget.MindmapPlayer;
import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.GroupBooks;
import com.semanoor.manahij.NoteData;
import com.semanoor.manahij.SharedUsers;
import com.semanoor.manahij.SlideMenuWithActivityGroup;
import com.semanoor.manahij.Theme;
import com.semanoor.sboookauthor_store.BookMarkEnrichments;
import com.semanoor.sboookauthor_store.EnrichmentButton;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.Note;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;


public class DatabaseHandler extends SQLiteOpenHelper implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//singleton/ single instance reference of database instance
	private static DatabaseHandler _dbHandler;

	//The Android's default system path of your application database.
	//private static String DB_PATH = "/data/data/YOUR_PACKAGE/databases/";
	private static String DB_PATH;

	//database name
	private static String DB_NAME = "sboookAuthor.sqlite";
	private static int DB_VERSION = 2;
	//reference of database
	public SQLiteDatabase _sqliteDb;

	private final Context _context;


	public DatabaseHandler(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		this._context = context;
		DB_PATH = _context.getFilesDir().getParentFile().getPath()+"/databases/";
	}
	
	public static synchronized DatabaseHandler getInstance(Context context){
		if(_dbHandler == null){
			_dbHandler = new DatabaseHandler(context);
			try {
				_dbHandler.createDataBase();
				_dbHandler.executeQuery("CREATE TABLE if not exists \"tblElessonBmark\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, \"lessonID\" TEXT, \"pageID\" TEXT, \"pageTitle\" TEXT, \"reserveText\" TEXT)");
				_dbHandler.executeQuery("CREATE TABLE IF NOT EXISTS \"tblNoteElesson\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, \"lessonID\" TEXT, \"pageID\" TEXT, \"noteText\" TEXT, \"date\" TEXT, \"time\" TEXT, \"pageName\" TEXT)");
                _dbHandler.executeQuery("CREATE TABLE IF NOT EXISTS \"malzamah\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, \"BID\" integer, \"pageNO\" integer, \"sourceStoreID\"  text,\"sourceHtmlPath\" text, \"sourceThumbPath\"  text)");
				_dbHandler.executeQuery("CREATE TABLE if not exists \"groupBooks\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT, \"groupName\" TEXT, \"categoryID\" INTEGER)");
				_dbHandler.executeQuery("CREATE TABLE if not exists \"tblUserGroups\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,\"name\" TEXT, \"userId\" TEXT,\"enabled\" INTEGER)");
				_dbHandler.executeQuery("CREATE TABLE if not exists \"tblUserContacts\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,\"userName\" TEXT, \"emailID\" TEXT,\"userID\" TEXT,\"groupsID\" INTEGER,\"enabled\" INTEGER)");
			    _dbHandler.executeQuery("CREATE TABLE if not exists \"Themes\" (\"themeID\" INTEGER PRIMARY KEY AUTOINCREMENT, \"themeName\" TEXT, \"createDate\" TEXT, \"foregroundColor\" TEXT, \"category\" INTEGER)");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return _dbHandler;
	}

	/**
	 * @author Creating Database
	 *
	 */
	public void createDataBase() throws IOException
	{
		boolean dbExist = checkDataBase();
		if(dbExist)
		{
			//do nothing - database already exist
		}else{
			this.getReadableDatabase();
			try {
				copyDataBase();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}


	public void alterTableAddColumnDomainURL() {
		String defaultDomainURL = "http://www.nooor.com/sboook/IPadSSboookService.asmx";

		boolean success =checkingDomainUrl();

		if (success) {
			executeQuery("ALTER TABLE books add domainURL text");
		//int booksCount = [BookContentDatabase getBooksCount];
		//for (int i = 0; i < booksCount; i++) {
		//update books set domainURL='http://www.nooor.com/sboook/IPadSSboookService.asmx' where storeID LIKE '%M%'
		//NSString *insertQuery = [NSString stringWithFormat:@"update books set domainURL='%@' where storeID LIKE '%%M%%'", defaultDomainURL];
		 String insertQuery = "update books set domainURL='" + defaultDomainURL + "'";
		 executeQuery(insertQuery);
		//[BookContentDatabase executeQuery:insertQuery];
		}
		//}
	}
	/**
	 * @author Check Database Exist in the Pathstor
	 *
	 */
	private boolean checkDataBase()
	{
		SQLiteDatabase checkDB = null;
		try
		{
			String myPath = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

		}
		catch(SQLiteException e)
		{
			//database does't exist yet.
		}
		if(checkDB != null)
		{
			checkDB.close();
		}
		return checkDB != null ? true : false;
	}

	/**
	 * @author Copy Database
	 *
	 */
	private void copyDataBase() throws IOException
	{
		//Open your local db as the input stream
		InputStream myInput = _context.getAssets().open(DB_NAME);

		// Path to the just created empty db
		String outFileName = DB_PATH + DB_NAME;

		//Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		//transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer))>0)
		{
			myOutput.write(buffer, 0, length);
		}
		//Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		if (ver==1){
//			db.execSQL("ALTER TABLE tblCategory ADD COLUMN newCID integer");
//			db.execSQL("INSERT INTO tblCategory ( CName,CBgImagePath,newCID ) VALUES ('Pdf','6.png','6' )");
//			updateNewCID(db);
		//}
	}
	
	/**
	 * @author Get No. of Bookss
	 */
	public int getBookCount(int groupId){
		int bookCount = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select count(*) from books where groupID='"+groupId+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			bookCount = c.getInt(0);
		}
		c.close();
		//db.close();
		return bookCount;
	}
	
	/**
	 * @author Get All Book Content and store it in Array list
	 * @param shelf 
	 */
	public ArrayList<Object> getAllBookContent(Shelf shelf, String searchText) {
		// TODO Auto-generated method stub
		ArrayList<Object> bookListArray = new ArrayList<Object>();
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from books";
		String query="select * from books where Title like \"%"+searchText+"%\"order by BID DESC";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			int BID = Integer.parseInt(c.getString(0));
			String Title = c.getString(1);
			String Description = c.getString(2);
			String Author = c.getString(3);
			int templateId = Integer.parseInt(c.getString(4));
			int totalPages = Integer.parseInt(c.getString(5));
			int pageBgValue = Integer.parseInt(c.getString(6));
			int orientation = Integer.parseInt(c.getString(7));
			int bIndex = Integer.parseInt(c.getString(8));
			boolean storeBook = Boolean.parseBoolean(c.getString(9));
			int LastViewdPage = Integer.parseInt(c.getString(10));
			String storeID = c.getString(11);
			String offsetWidthAndHeight = c.getString(12);
			boolean showPageNo = Boolean.parseBoolean(c.getString(13));
			String pageNopos = c.getString(14);
			String pageNoContent = c.getString(15);
			Templates template = shelf.getTemplates(templateId, orientation,_context);
			String storeBookCreatedFromTab = c.getString(43);
			String isDownloadCompleted = c.getString(44);
			String downloadURL = c.getString(45);
			String editable = c.getString(46);
			String isMalzamah = c.getString(47);
			String price = c.getString(48);
			String imageURL = c.getString(49);
			String purchaseType = c.getString(50);
			int bCategoryId = c.getInt(16);
			int groupID=c.getInt(51);
			String clientId = c.getString(53);
			String bookDirection = c.getString(54);
			boolean isStoreBookCreatedFromTab = false;
			if (storeBookCreatedFromTab.equals("yes")) {
				isStoreBookCreatedFromTab = true;
			}
			String bookLanguage = c.getString(39);
			Book book = new Book(BID, Title, Description, Author, templateId, totalPages, pageBgValue, orientation, bIndex, storeBook,LastViewdPage,storeID,offsetWidthAndHeight,showPageNo,pageNopos,pageNoContent,template,isStoreBookCreatedFromTab,bCategoryId,bookLanguage,isDownloadCompleted,downloadURL,editable,isMalzamah,price,imageURL,purchaseType,groupID,clientId,bookDirection);
			bookListArray.add(book);
			c.moveToNext();
		}
		c.close();
		//db.close();
		return bookListArray;
	}



	public Book getAllBooks(String bookId) {
		// TODO Auto-generated method stub

		Book book=null;
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from books";
		String query = "select * from books where storeID='" + bookId + "'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int BID = Integer.parseInt(c.getString(0));
			String Title = c.getString(1);
			String Description = c.getString(2);
			String Author = c.getString(3);
			int templateId = Integer.parseInt(c.getString(4));
			int totalPages = Integer.parseInt(c.getString(5));
			int pageBgValue = Integer.parseInt(c.getString(6));
			int orientation = Integer.parseInt(c.getString(7));
			int bIndex = Integer.parseInt(c.getString(8));
			boolean storeBook = Boolean.parseBoolean(c.getString(9));
			int LastViewdPage = Integer.parseInt(c.getString(10));
			String storeID = c.getString(11);
			String offsetWidthAndHeight = c.getString(12);
			boolean showPageNo = Boolean.parseBoolean(c.getString(13));
			String pageNopos = c.getString(14);
			String pageNoContent = c.getString(15);
			String storeBookCreatedFromTab = c.getString(43);
			String isDownloadCompleted = c.getString(44);
			String downloadURL = c.getString(45);
			String editable = c.getString(46);
			String isMalzamah = c.getString(47);
			String price = c.getString(48);
			String imageURL = c.getString(49);
			String purchaseType = c.getString(50);
			int bCategoryId = c.getInt(16);
			int groupID = c.getInt(51);


			String clientId = c.getString(53);
			boolean isStoreBookCreatedFromTab = false;
			if (storeBookCreatedFromTab.equals("yes")) {
				isStoreBookCreatedFromTab = true;
			}
			String bookLanguage = c.getString(39);
			book = new Book(BID, Title, Description, Author, templateId, totalPages, pageBgValue, orientation, bIndex, storeBook, LastViewdPage, storeID, offsetWidthAndHeight, showPageNo, pageNopos, pageNoContent, isStoreBookCreatedFromTab, bCategoryId, bookLanguage, isDownloadCompleted, downloadURL, editable, isMalzamah, price, imageURL, purchaseType, groupID, clientId);

			c.moveToNext();
		}
		c.close();
		//db.close();
		return book;
	}
	public ArrayList<GroupBooks> getAllGroupList() {
		ArrayList<GroupBooks> grpList = new ArrayList<GroupBooks>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from groupBooks";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int Id = c.getInt(0);
			String groupName = c.getString(1);
			int Category = c.getInt(2);

			GroupBooks grp = new GroupBooks();
			grp.setGroupName(groupName);
			grp.setCategoryId(Category);
			grp.setGroupId(Id);
			//GroupBooks enrichments = new EnrichmentButton(context, enrType, enrID, enrBid, enrPageNo, enrTitle, enrichmentTabList.size()+1, enrExportedId, path, enrTabsLayout);
			grpList.add(grp);
			c.moveToNext();
		}
		c.close();
		//db.close();

		return grpList;
	}

	/**
	 * @author ExecuteQuery
	 * @param query
	 */
	public void executeQuery(String query){
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL(query);
		//db.close();

	}
	public boolean checkingDomainUrl(){
		SQLiteDatabase db = getWritableDatabase();
		Cursor c =db.rawQuery("select *from books",null);
		int columnIndex= c.getColumnCount(); //c.getColumnIndex("52");
		if(columnIndex<52){
			return true;

		}
		return  false;
		//db.close();

	}
	/**
	 * @author Get All Objects for the current page
	 * @param enrichedPageId 
	 * @return
	 */
	public void getAllObjectsFortheCurrentPage(String pageNumber, final BookViewActivity ctx, int enrichedPageId){
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from objects where pageNO='"+pageNumber+"' and EnrichedID='"+enrichedPageId+"' and BID='"+ctx.currentBook.getBookID()+"' and objType<>'"+"PageTableBG"+"' order by SequentialID";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			String objType = c.getString(1);
			int objBID = Integer.parseInt(c.getString(2));
			String objPageNo = c.getString(3);
			final String objLocation = c.getString(4);
			final String objSize = c.getString(5);
			String objContent = c.getString(6);
			int objUniqueRowId = Integer.parseInt(c.getString(7));
			int objSequentialId = Integer.parseInt(c.getString(8));
			String objSclaPageToFit = c.getString(9);
			String strObjLock = c.getString(10);
			boolean objLock = false;
			if (strObjLock != null) {
				if (strObjLock.equals("true")) {
					objLock = true;
				} else {
					objLock = false;
				}
			}
			int objEnrichId = c.getInt(11);
			if(objSclaPageToFit.contains("##")){
				String[] contenttoFit=objSclaPageToFit.split("##");
				objSclaPageToFit=contenttoFit[0];
			}
			if (objType.equals("Image") || objType.equals("YouTube") || objType.equals("DrawnImage") || objType.equals("IFrame") || objType.equals("Audio") || objType.equals("EmbedCode")) {
				final CustomImageRelativeLayout imgRl = new CustomImageRelativeLayout(ctx, objType, objBID, objPageNo, objLocation, objSize, objContent, objUniqueRowId, objSequentialId, objSclaPageToFit, objLock, objEnrichId, null);
				ctx.page.pagePublishProgressUpdate(imgRl);
			} else if (objType.equals("WebText") || objType.equals("WebTable") || objType.equals("TextSquare") || objType.equals("TextRectangle") || objType.equals("TextCircle") || objType.equals(Globals.objType_pageNoText) || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT) || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT) ) {
				objContent.replace("'", "");
				final CustomWebRelativeLayout webRl = new CustomWebRelativeLayout(ctx, objType, objBID, objPageNo, objLocation, objSize, objContent, objUniqueRowId, objSequentialId, objSclaPageToFit, objLock, objEnrichId, null);
				ctx.page.pagePublishProgressUpdate(webRl);
			}else if (objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)){
				final MindmapPlayer player = new MindmapPlayer(ctx, objType, objBID, objPageNo, objLocation, objSize, objContent, objUniqueRowId, objSequentialId, objSclaPageToFit, objLock, objEnrichId, null,false);
				ctx.page.pagePublishProgressUpdate(player);
			}
			c.moveToNext();
		}
		c.close();
		//db.close();
	}

	/**
	 * getAllEnrichmentTabList
	 * @param BID
	 * @param pageNo
	 * @param context
	 * @return
	 */
	public ArrayList<EnrichmentButton> getEnrichmentTabListForBookReadAct(int BID, int pageNo, Context context, LinearLayout enrTabsLayout) {
		ArrayList<EnrichmentButton> enrichmentTabList = new ArrayList<EnrichmentButton>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from enrichments where BID='"+BID+"' and pageNO='"+pageNo+"'order by SequentialID";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			String path=c.getString(6);
			int sequenceId=c.getInt(7);

			EnrichmentButton enrichments = new EnrichmentButton(context, enrType, enrID, enrBid, enrPageNo, enrTitle, enrichmentTabList.size()+1, enrExportedId, path, enrTabsLayout,sequenceId);
			enrichmentTabList.add(enrichments);
			c.moveToNext();
		}
		c.close();
		//db.close();

		return enrichmentTabList;
	}

	/**
	 * getAllEnrichmentTabList
	 * @param BID
	 * @param pageNo
	 * @param context
	 * @return
	 */
	public ArrayList<Enrichments> getEnrichmentTabListForBookReadAct1(int BID, int pageNo, Context context) {
		ArrayList<Enrichments> enrichmentTabList = new ArrayList<Enrichments>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from enrichments where BID='"+BID+"' and pageNO='"+pageNo+"'order by SequentialID";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			String path=c.getString(6);
			int sequenceId=c.getInt(7);
			int catID = c.getInt(8);

			if(enrichmentTabList.size()==0){
				Enrichments enrichments = new Enrichments(context);
				enrichments.setEnrichmentId(0);
				enrichments.setEnrichmentBid(enrBid);
				enrichments.setEnrichmentPageNo(pageNo);
				enrichments.setEnrichmentTitle("Home");
				enrichments.setEnrichmentType("Home");
				enrichments.setEnrichmentSequenceId(0);
				enrichments.setEnrichmentExportValue(0);
				enrichments.setEnrichmentPath("null");
				enrichments.setCategoryID(0);
			//	if(((BookViewReadActivity)context).currentEnrichmentTabId==0) {
					enrichments.setEnrichmentSelected(true);
//				}else{
//					enrichments.setEnrichmentSelected(false);
//				}
                enrichmentTabList.add(enrichments);
			}

			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(enrID);
			enrichments.setEnrichmentBid(enrBid);
			enrichments.setEnrichmentPageNo(enrPageNo);
			enrichments.setEnrichmentTitle(enrTitle);
			enrichments.setEnrichmentType(enrType);
			enrichments.setEnrichmentSequenceId(sequenceId);
			enrichments.setEnrichmentExportValue(enrExportedId);
			enrichments.setEnrichmentPath(path);
			enrichments.setCategoryID(catID);
//			if(((BookViewReadActivity)context).currentEnrichmentTabId==enrID) {
//                enrichments.setEnrichmentSelected(true);
//			}else{
//				enrichments.setEnrichmentSelected(false);
//			}
			enrichmentTabList.add(enrichments);
			c.moveToNext();

		}
		//db.close();
		if(enrichmentTabList.size()==0){
			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(0);
			enrichments.setEnrichmentBid(BID);
			enrichments.setEnrichmentPageNo(pageNo);
			enrichments.setEnrichmentTitle("Home");
			enrichments.setEnrichmentType("Home");
			enrichments.setEnrichmentSequenceId(0);
			enrichments.setEnrichmentExportValue(0);
			enrichments.setEnrichmentPath("null");
			enrichments.setEnrichmentSelected(true);
			enrichments.setCategoryID(0);
			enrichmentTabList.add(enrichments);
		}
		c.close();
		return enrichmentTabList;
	}

	public ArrayList<Enrichments> getEnrichmentTabListForCatId(int BID, int pageNo, Context context,int catId) {
		ArrayList<Enrichments> enrichmentTabList = new ArrayList<Enrichments>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from enrichments where BID='"+BID+"' and pageNO='"+pageNo+"' and categoryId='"+catId+"'order by SequentialID";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			String path=c.getString(6);
			int sequenceId=c.getInt(7);
			int catID = c.getInt(8);


			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(enrID);
			enrichments.setEnrichmentBid(enrBid);
			enrichments.setEnrichmentPageNo(enrPageNo);
			enrichments.setEnrichmentTitle(enrTitle);
			enrichments.setEnrichmentType(enrType);
			enrichments.setEnrichmentSequenceId(sequenceId);
			enrichments.setEnrichmentExportValue(enrExportedId);
			enrichments.setEnrichmentPath(path);
			enrichments.setCategoryID(catID);
			enrichmentTabList.add(enrichments);
			c.moveToNext();

		}
		c.close();
		//db.close();
		return enrichmentTabList;
	}

	public ArrayList<Enrichments> getAllEnrichmentList(int BID, int pageNo, Context context) {
		ArrayList<Enrichments> enrichmentTabList = new ArrayList<Enrichments>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from enrichments where BID='"+BID+"' and pageNO='"+pageNo+"'order by SequentialID";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			String path=c.getString(6);
			int sequenceId=c.getInt(7);
			int catID = c.getInt(8);


			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(enrID);
			enrichments.setEnrichmentBid(enrBid);
			enrichments.setEnrichmentPageNo(enrPageNo);
			enrichments.setEnrichmentTitle(enrTitle);
			enrichments.setEnrichmentType(enrType);
			enrichments.setEnrichmentSequenceId(sequenceId);
			enrichments.setEnrichmentExportValue(enrExportedId);
			enrichments.setEnrichmentPath(path);
			enrichments.setCategoryID(catID);
			enrichmentTabList.add(enrichments);
			c.moveToNext();

		}
		c.close();
		//db.close();
		return enrichmentTabList;
	}
	/**
	 * getAllEnrichmentTabList
	 * @param BID
	 * @param pageNo
	 * @param context
	 * @return
	 */
	public ArrayList<Enrichments> getAllEnrichmentTabList(int BID, int pageNo, Context context) {
		ArrayList<Enrichments> enrichmentTabList = new ArrayList<Enrichments>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from enrichments where BID='" + BID + "' and pageNO='" + pageNo + "' and type<>'" + Globals.downloadedEnrichmentType + "' order by SequentialID";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			String path = c.getString(6);
			int catID = c.getInt(8);

			if (enrichmentTabList.size() == 0) {
				Enrichments enrichments = new Enrichments(context);
				enrichments.setEnrichmentId(0);
				enrichments.setEnrichmentBid(BID);
				enrichments.setEnrichmentPageNo(pageNo);
				enrichments.setEnrichmentTitle("Home");
				enrichments.setEnrichmentType("Home");
				enrichments.setEnrichmentSequenceId(0);
				enrichments.setEnrichmentExportValue(0);
				enrichments.setEnrichmentPath("null");
				enrichments.setEnrichmentSelected(true);
				enrichments.setCategoryID(0);
				enrichmentTabList.add(enrichments);

			}
			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(enrID);
			enrichments.setEnrichmentBid(enrBid);
			enrichments.setEnrichmentPageNo(enrPageNo);
			enrichments.setEnrichmentTitle(enrTitle);
			enrichments.setEnrichmentType(enrType);
			enrichments.setEnrichmentSequenceId(enrichmentTabList.size() + 1);
			enrichments.setEnrichmentExportValue(enrExportedId);
			enrichments.setEnrichmentPath(path);
			enrichments.setCategoryID(catID);

			enrichmentTabList.add(enrichments);
			c.moveToNext();
		}
		c.close();
		//db.close();
		if (enrichmentTabList.size() == 0) {
			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(0);
			enrichments.setEnrichmentBid(BID);
			enrichments.setEnrichmentPageNo(pageNo);
			enrichments.setEnrichmentTitle("Home");
			enrichments.setEnrichmentType("Home");
			enrichments.setEnrichmentSequenceId(0);
			enrichments.setEnrichmentExportValue(0);
			enrichments.setEnrichmentPath("null");
			enrichments.setEnrichmentSelected(true);
			enrichments.setCategoryID(0);
			enrichmentTabList.add(enrichments);
		}
		return enrichmentTabList;
	}
	
	
	/**
	 * getAllEnrichmentTabList in preview
	 * @param BID
	 * @param pageNo
	 * @param context
	 * @return
	 */
	public ArrayList<Enrichments> getEnrichmentTabList(int BID, int pageNo, Context context) {
		ArrayList<Enrichments> enrichmentTabList = new ArrayList<Enrichments>();
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from enrichments where BID='"+BID+"' and pageNO='"+pageNo+"'";
		String query="select * from enrichments where BID='"+BID+"' and pageNO='"+pageNo+"'and Type<>'Search' and Type<>'"+Globals.onlineEnrichmentsType+"' and Type<>'"+Globals.OBJTYPE_MINDMAPTAB+"' and Type<>'"+Globals.downloadedEnrichmentType+"'order by SequentialID";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			String path=c.getString(6);
			int catID = c.getInt(8);

			if (enrichmentTabList.size() == 0) {
				Enrichments enrichments = new Enrichments(context);
				enrichments.setEnrichmentId(0);
				enrichments.setEnrichmentBid(BID);
				enrichments.setEnrichmentPageNo(pageNo);
				enrichments.setEnrichmentTitle("Home");
				enrichments.setEnrichmentType("Home");
				enrichments.setEnrichmentSequenceId(0);
				enrichments.setEnrichmentExportValue(0);
				enrichments.setEnrichmentPath("null");
				enrichments.setEnrichmentSelected(true);
				enrichments.setCategoryID(0);
				enrichmentTabList.add(enrichments);

			}
			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(enrID);
			enrichments.setEnrichmentBid(enrBid);
			enrichments.setEnrichmentPageNo(enrPageNo);
			enrichments.setEnrichmentTitle(enrTitle);
			enrichments.setEnrichmentType(enrType);
			enrichments.setEnrichmentSequenceId(enrichmentTabList.size()+1);
			enrichments.setEnrichmentExportValue(enrExportedId);
			enrichments.setEnrichmentPath(path);
			enrichments.setCategoryID(catID);
			
			enrichmentTabList.add(enrichments);
			c.moveToNext();
		}
		c.close();
		//db.close();
		if (enrichmentTabList.size() == 0) {
			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(0);
			enrichments.setEnrichmentBid(BID);
			enrichments.setEnrichmentPageNo(pageNo);
			enrichments.setEnrichmentTitle("Home");
			enrichments.setEnrichmentType("Home");
			enrichments.setEnrichmentSequenceId(0);
			enrichments.setEnrichmentExportValue(0);
			enrichments.setEnrichmentPath("null");
			enrichments.setEnrichmentSelected(true);
			enrichments.setCategoryID(0);
			enrichmentTabList.add(enrichments);

		}
		
		return enrichmentTabList;
	}
	
	/**
	 * this returns the enrichment tab count for the current page
	 * @param BID
	 * @param pageNo
	 * @return
	 */
	public int getCountForEnrichmentTabList(int BID, int pageNo) {
		int enrTabCount = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select count(*) from enrichments where BID='"+BID+"' and pageNO='"+pageNo+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			enrTabCount = c.getInt(0);
		}
		c.close();
		//db.close();
		return enrTabCount;
	}

	public int getOnlineEnrCount(int BID) {
		int enrTabCount = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select count(*) from enrichments where BID='"+BID+"' and Type='" + Globals.onlineEnrichmentsType + "'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			enrTabCount = c.getInt(0);
		}
		c.close();
		//db.close();
		return enrTabCount;
	}

	public ArrayList<SharedUsers> getCountForSharedList(String LoginID,Context context) {
		ArrayList<SharedUsers> sharedList = new ArrayList<SharedUsers>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from sharedUsers where LoginID='"+LoginID+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			
			int id = c.getInt(0);
			String userLoginID = c.getString(1);
			String UserName = c.getString(2);
			String EmailId = c.getString(3);
			String Blacklist = c.getString(4);
			String SharedType = c.getString(5);
			
			SharedUsers sharedUsers = new SharedUsers(context);
			sharedUsers.setUsersId(id);
			sharedUsers.setSharedUsersId(userLoginID);
			sharedUsers.setSharedUsersName(UserName);
			sharedUsers.setSharedUsersEmailId(EmailId);
			sharedUsers.setSharedUsersBlacklist(Blacklist);
			sharedUsers.setSharedUsersType(SharedType);
			
			sharedList.add(sharedUsers);
			c.moveToNext();
			
		
		}
		
		c.close();
		//db.close();
		
		return sharedList;
	}
	public ArrayList<SharedUsers> getCountForSharedList1(String LoginID,String UserName) {
		ArrayList<SharedUsers> sharedList1 = new ArrayList<SharedUsers>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from sharedUsers where LoginID='"+LoginID+"' and UserName = '"+UserName+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			
			int id = c.getInt(0);
			String userLoginID = c.getString(1);
			String Username = c.getString(2);
			String EmailId = c.getString(3);
			String Blacklist = c.getString(4);
			String SharedType = c.getString(5);	
			
			SharedUsers sharedUsers1 = new SharedUsers(_context);
			sharedUsers1.setUsersId(id);
			sharedUsers1.setSharedUsersId(userLoginID);
			sharedUsers1.setSharedUsersName(Username);
			sharedUsers1.setSharedUsersEmailId(EmailId);
			sharedUsers1.setSharedUsersBlacklist(Blacklist);
			sharedUsers1.setSharedUsersType(SharedType);
			
			sharedList1.add(sharedUsers1);
			c.moveToNext();
		}
//		c.close();
//		//db.close();
		return sharedList1;
	}
	
	public ArrayList<SharedUsers> getCountForSharedBlackList1(String Blacklist) {
		ArrayList<SharedUsers> sharedList1 = new ArrayList<SharedUsers>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from sharedUsers where Blacklist = '"+Blacklist+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			
			int id = c.getInt(0);
			String userLoginID = c.getString(1);
			String Username = c.getString(2);
			String EmailId = c.getString(3);
			String BlackList = c.getString(4);
			String SharedType = c.getString(5);	
			
			SharedUsers sharedUsers1 = new SharedUsers(_context);
			sharedUsers1.setUsersId(id);
			sharedUsers1.setSharedUsersId(userLoginID);
			sharedUsers1.setSharedUsersName(Username);
			sharedUsers1.setSharedUsersEmailId(EmailId);
			sharedUsers1.setSharedUsersBlacklist(BlackList);
			sharedUsers1.setSharedUsersType(SharedType);
			
			sharedList1.add(sharedUsers1);
			c.moveToNext();
		}
//		c.close();
//		//db.close();
		return sharedList1;
	}
	public Boolean sharedUsersList(String LoginID, String UserName){
		Boolean returnValue = false;
		ArrayList<SharedUsers> sharedList = new ArrayList<SharedUsers>();
				SQLiteDatabase db = getReadableDatabase();
				String query = "select * from sharedUsers where LoginID='"+LoginID+"' and UserName = '"+UserName+"'";
				sharedList = this.getCountForSharedList1(LoginID, UserName);
				Cursor c = db.rawQuery(query, null);
				c.moveToFirst();
				 if (sharedList.size() > 0) {
				        return true;
				    }
				    c.close();
					//db.close();
				    return returnValue;
			}
	
	public Boolean sharedUsersBlackList(String Blacklist){
		
		SQLiteDatabase db = getReadableDatabase();
		Boolean returnValue = false;
		String query = "select * from sharedUsers where Blacklist = 'yes'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		
		//if(c != null && c.moveToFirst())
		if(c.getCount()>0)
		{
			returnValue = true;
		}
		    c.close();
			//db.close();
		    return returnValue;
	}
	
	/**
	 * Get all bookMarkTabs for the book
	 * @param BID
	 * @parami
	 * @param context 
	 * @return
	 */
	public ArrayList<BookMarkEnrichments> getAllbookMarkTabListForTheBook(int pageno, int BID, Context context) {
		ArrayList<BookMarkEnrichments>enrichmentTabList = new ArrayList<BookMarkEnrichments>();
 		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from BookmarkedSearchTabs  where Bid='"+BID+"'and PageNo='"+pageno+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			String enrTitle = c.getString(1);
			int searchtabid = c.getInt(2);
			int enrPageNo = c.getInt(3);
			boolean isforallpages =Boolean.parseBoolean(c.getString(4));
			String path = c.getString(5);
			int enrBid = c.getInt(6);
			
			BookMarkEnrichments bokmarkTabs = new BookMarkEnrichments(context);
			bokmarkTabs.setEnrichmentId(enrID);
			bokmarkTabs.setEnrichmentTitle(enrTitle);
			bokmarkTabs.setSearchTabId(searchtabid);
			bokmarkTabs.setEnrichmentPageNo(enrPageNo);
			bokmarkTabs.setIsForAllPages(isforallpages);
			bokmarkTabs.setEnrichmentPath(path);
			bokmarkTabs.setEnrichmentBid(enrBid);
			
			enrichmentTabList.add(bokmarkTabs);
			c.moveToNext();
		}
		c.close();
		//db.close();
		
		return enrichmentTabList;
	}
	public ArrayList<HashMap<String,String>> getAllElessonBookmark(String lessonID){
		ArrayList<HashMap<String,String>> ElessonBookmarkList = new ArrayList<HashMap<String,String>>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select pageTitle, pageID from tblElessonBmark where lessonID = '"+lessonID+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			String pageTitle = c.getString(0);
			String pageId = c.getString(1);

			HashMap<String,String> map = new HashMap<>();
			map.put("PageTitle", pageTitle);
			map.put("pageID", pageId);
			ElessonBookmarkList.add(map);
			c.moveToNext();
		}
		c.close();
		//db.close();

		return ElessonBookmarkList;
	}
	public ArrayList<HashMap<String,String>> getAllElessonNote(String lessonID){
		ArrayList<HashMap<String,String>> ElessonNoteList = new ArrayList<HashMap<String,String>>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select id,pageID, noteText,date,pageName from tblNoteElesson where lessonID = '"+lessonID+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			String id = c.getString(0);
			String pageId = c.getString(1);
			String noteText = c.getString(2);
			String date = c.getString(3);
			String pageTitle = c.getString(4);


			HashMap<String,String> map = new HashMap<>();
			map.put("id", id);
			map.put("PageTitle", pageTitle);
			map.put("pageID", pageId);
			map.put("noteText", noteText);
			map.put("date", date);

			ElessonNoteList.add(map);
			c.moveToNext();
		}
		c.close();
		//db.close();

		return ElessonNoteList;
	}
	/**
	 * Get all Enrichments for the book
	 * @param BID
	 * @param context
	 * @return
	 */
	public ArrayList<Enrichments> getAllEnrichmentTabListForTheBook(int BID, Context context) {
		ArrayList<Enrichments> enrichmentTabList = new ArrayList<Enrichments>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from enrichments where BID='"+BID+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			
			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(enrID);
			enrichments.setEnrichmentBid(enrBid);
			enrichments.setEnrichmentPageNo(enrPageNo);
			enrichments.setEnrichmentTitle(enrTitle);
			enrichments.setEnrichmentType(enrType);
			enrichments.setEnrichmentSequenceId(enrichmentTabList.size() + 1);
			enrichments.setEnrichmentExportValue(enrExportedId);
			
			enrichmentTabList.add(enrichments);
			c.moveToNext();
		}
		c.close();
		//db.close();
		
		return enrichmentTabList;
	}
	
	/**
	 * Get all Enrichments for Exporting
	 * @param BID
	 * @param context
	 * @return
	 */
	public ArrayList<Enrichments> getAllEnrichmentTabListForExport(int BID, Context context) {
		ArrayList<Enrichments> enrichmentTabList = new ArrayList<Enrichments>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from enrichments where BID='"+BID+"' and type<>'Search' and type<>'"+Globals.OBJTYPE_MINDMAPTAB+"' and type<>'"+Globals.downloadedEnrichmentType+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			int catID = c.getInt(8);
			
			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(enrID);
			enrichments.setEnrichmentBid(enrBid);
			enrichments.setEnrichmentPageNo(enrPageNo);
			enrichments.setEnrichmentTitle(enrTitle);
			enrichments.setEnrichmentType(enrType);
			enrichments.setEnrichmentSequenceId(enrichmentTabList.size() + 1);
			enrichments.setEnrichmentExportValue(enrExportedId);
			enrichments.setCategoryID(catID);
			
			enrichmentTabList.add(enrichments);
			c.moveToNext();
		}
		c.close();
		//db.close();
		
		return enrichmentTabList;
	}
	
	/**
	 * Get all Enrichments links for Exporting
	 * @param BID
	 * @param context
	 * @return
	 */
	public ArrayList<Enrichments> getAllEnrichmentLinksForExport(int BID, Context context) {
		ArrayList<Enrichments> enrichmentTabList = new ArrayList<Enrichments>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from enrichments where BID='"+BID+"' and type='Search'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			int catID = c.getInt(8);
			
			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(enrID);
			enrichments.setEnrichmentBid(enrBid);
			enrichments.setEnrichmentPageNo(enrPageNo);
			enrichments.setEnrichmentTitle(enrTitle);
			enrichments.setEnrichmentType(enrType);
			enrichments.setEnrichmentSequenceId(enrichmentTabList.size() + 1);
			enrichments.setEnrichmentExportValue(enrExportedId);
			enrichments.setEnrichmentPath(c.getString(6));
			enrichments.setCategoryID(catID);
			enrichmentTabList.add(enrichments);
			c.moveToNext();
		}
		c.close();
		//db.close();
		
		return enrichmentTabList;
	}

	/**
	 * Get all Enrichments links for Exporting
	 * @param BID
	 * @param context
	 * @return
	 */
	public ArrayList<Enrichments> getAllEnrichmentMindmapForExport(int BID, Context context) {
		ArrayList<Enrichments> enrichmentTabList = new ArrayList<Enrichments>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from enrichments where BID='"+BID+"' and type='"+Globals.OBJTYPE_MINDMAPTAB+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			int catID = c.getInt(8);

			Enrichments enrichments = new Enrichments(context);
			enrichments.setEnrichmentId(enrID);
			enrichments.setEnrichmentBid(enrBid);
			enrichments.setEnrichmentPageNo(enrPageNo);
			enrichments.setEnrichmentTitle(enrTitle);
			enrichments.setEnrichmentType(enrType);
			enrichments.setEnrichmentSequenceId(enrichmentTabList.size() + 1);
			enrichments.setEnrichmentExportValue(enrExportedId);
			enrichments.setEnrichmentPath(c.getString(6));
			enrichments.setCategoryID(catID);
			enrichmentTabList.add(enrichments);
			c.moveToNext();
		}
		c.close();
		//db.close();

		return enrichmentTabList;
	}
	
	/**
	 * getAll Note for Export
	 * @param storeBookId
	 * @param context
	 * @return
	 */
	public ArrayList<Note> getAllNotesForExport(String storeBookId, Context context) {
		ArrayList<Note> noteList = new ArrayList<Note>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from tblNote where BName='"+storeBookId+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			Note note = new Note(context);
			note.setId(c.getInt(0));
			note.setbName(c.getString(1));
			note.setPageNo(c.getInt(2));
			note.setsText(c.getString(3));
			note.setOccurence(c.getInt(4));
			note.setsDesc(c.getString(5));
			note.setnPos(c.getInt(6));
			note.setColor(c.getInt(7));
			note.setProcessStext(c.getString(8));
			note.setTabNo(c.getInt(9));
			note.setExportedId(c.getInt(10));
			noteList.add(note);
			c.moveToNext();
		}
		c.close();
		//db.close();
		
		return noteList;
	}
	
	public ArrayList<UserGroups> getMyGroups(String userId, Context context) {
		ArrayList<UserGroups> groupList = new ArrayList<UserGroups>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from userGroups where userID='"+userId+"' group by groupName";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			UserGroups groups = new UserGroups(context);
			groups.setId(c.getInt(0));
			groups.setUserScopeId(c.getInt(1));
			groups.setGroupName(c.getString(2));
			groups.setEmailId(c.getString(4));
			groups.setUserName(c.getString(3));
			groups.setEnabled(c.getInt(5));
			groups.setGroupSelected(false);
			groupList.add(groups);
			c.moveToNext();
		}
		c.close();
		//db.close();
		
		return groupList;
	}
	
	public ArrayList<UserGroups> getselectedgroupdetails(String groupName,int scopeId, Context context) {
		ArrayList<UserGroups> groupList = new ArrayList<UserGroups>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from userGroups where groupName='"+groupName+"'and userID='"+scopeId+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			UserGroups groups = new UserGroups(context);
			groups.setId(c.getInt(0));
			groups.setUserScopeId(c.getInt(1));
			groups.setGroupName(c.getString(2));
			groups.setEmailId(c.getString(4));
			groups.setUserName(c.getString(3));
			groups.setEnabled(c.getInt(5));
			groups.setGroupSelected(false);
			groupList.add(groups);
			c.moveToNext();
		}
		c.close();
		//db.close();

		return groupList;
	}

	public ArrayList<MyGroups> getMyGroupsList(String userId, Context context) {
		ArrayList<MyGroups> groupList = new ArrayList<MyGroups>();
		ArrayList<MyContacts> groupContacts;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from tblUserGroups where userID='"+userId+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			MyGroups groups = new MyGroups(context);
			groups.setId(c.getInt(0));
			groups.setGroupName(c.getString(1));
			groups.setLoggedInUserId(c.getString(2));
			//groups.setEmailId(c.getString(4));
			//groups.setUserName(c.getString(3));
			groups.setEnabled(c.getInt(3));
			groups.setGroupSelected(false);
			String query2 = "select * from tblUserContacts where groupsID='"+c.getString(0)+"'and userID='"+userId+"'";
			//userContacts=exportEnrActivity.db.getContactDetails(query,exportEnrActivity);
			groupContacts=getContactDetails(query2,context);
			groups.setGroupsContactList(groupContacts);
			groupList.add(groups);
			c.moveToNext();
		}
		c.close();
		//db.close();

		return groupList;
	}
	public ArrayList<MyContacts> getContactDetails(String query, Context context) {
		ArrayList<MyContacts> groupList = new ArrayList<MyContacts>();
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from userGroups where groupName='"+groupName+"'and userID='"+scopeId+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
            MyContacts contact = new MyContacts(context);
			contact.setId(c.getInt(0));
			contact.setUserName(c.getString(1));
			contact.setEmailId(c.getString(2));
			contact.setLoggedInUserId(c.getString(3));
			//groups.setGroupName(c.getString(2));
			contact.setGroupsID(c.getInt(4));
			contact.setOnline("0");
			contact.setEnabled(c.getInt(5));
			contact.setSelected(false);
			groupList.add(contact);
			c.moveToNext();
		}
		c.close();
		//db.close();

		return groupList;
	}
   /**
	 * @author Get max Unique row id
	 * @return 
	 */
	public int getMaxUniqueID(String groupName){
		int maxID = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = " select MAX(ID) from userGroups where groupName='"+groupName+"'and userName=''";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		if(c != null && c.moveToFirst()){
			maxID = c.getInt(0);
		}
		c.close();
		//db.close();
		return maxID;
	}
	 
	/**
	 * get all objects and store it in arraylist
	 * @param pageNumber
	 * @param bookId
	 * @return
	 */
	public ArrayList<Object> getAllObjectsForGeneratingHTML(String pageNumber, int bookId, int enrichedId) {
		ArrayList<Object> arrayList = new ArrayList<Object>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from objects where pageNO='"+pageNumber+"' and EnrichedID='"+enrichedId+"' and BID='"+bookId+"' and objType<>'"+"PageTableBG"+"' order by SequentialID";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			String objType = c.getString(1);
			int objBID = Integer.parseInt(c.getString(2));
			String objPageNo = c.getString(3);
			String objLocation = c.getString(4);
			String objSize = c.getString(5);
			String objContent = c.getString(6);
			int objUniqueRowId = Integer.parseInt(c.getString(7));
			int objSequentialId = Integer.parseInt(c.getString(8));
			String objSclaPageToFit = c.getString(9);
			String objLocked = c.getString(10);
			int objEnrichID = Integer.parseInt(c.getString(11));
			if (objType.equals("Image") || objType.equals("YouTube") || objType.equals("DrawnImage") || objType.equals("IFrame") || objType.equals("WebText")  || objType.equals("WebTable") || objType.equals("Audio") || objType.equals("TextSquare") || objType.equals("TextRectangle") || objType.equals("TextCircle") || objType.equals("EmbedCode") || objType.equals(Globals.objType_pageNoText) || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT) || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT) || objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
				HTMLObjectHolder object = new HTMLObjectHolder(objType, objBID, objPageNo, objLocation, objSize, objContent, objUniqueRowId, objSequentialId, objSclaPageToFit, objLocked, objEnrichID);
				arrayList.add(object);
			}
			c.moveToNext();
		}
		c.close();
		//db.close();
		return arrayList;
	}
	public ArrayList<Object> getAllObjectsFromPage(String query) {
		ArrayList<Object> arrayList = new ArrayList<Object>();
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from objects where pageNO='"+pageNumber+"' and EnrichedID='"+enrichedId+"' and BID='"+bookId+"'order by SequentialID";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			String objType = c.getString(1);
			int objBID = Integer.parseInt(c.getString(2));
			String objPageNo = c.getString(3);
			String objLocation = c.getString(4);
			String objSize = c.getString(5);
			String objContent = c.getString(6);
			int objUniqueRowId = Integer.parseInt(c.getString(7));
			int objSequentialId = Integer.parseInt(c.getString(8));
			String objSclaPageToFit = c.getString(9);
			String objLocked = c.getString(10);
			int objEnrichID = Integer.parseInt(c.getString(11));
			//if (objType.equals("Image") || objType.equals("YouTube") || objType.equals("DrawnImage") || objType.equals("IFrame") || objType.equals("WebText")  || objType.equals("WebTable") || objType.equals("Audio") || objType.equals("TextSquare") || objType.equals("TextRectangle") || objType.equals("TextCircle") || objType.equals("EmbedCode") || objType.equals(Globals.objType_pageNoText) || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT) || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT) || objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
				HTMLObjectHolder object = new HTMLObjectHolder(objType, objBID, objPageNo, objLocation, objSize, objContent, objUniqueRowId, objSequentialId, objSclaPageToFit, objLocked, objEnrichID);
				arrayList.add(object);
			//}
			c.moveToNext();
		}
		c.close();
		//db.close();
		return arrayList;
	}
	/**
	 * getAllObjects from original store page to create enrichments
	 * @param pageNumber
	 * @param bookId
	 * @param enrichedId
	 * @return
	 */
	public ArrayList<HTMLObjectHolder> getAllObjectsFromHomePageToCreateEnrichment(String pageNumber, int bookId, int enrichedId){
		ArrayList<HTMLObjectHolder> arrayList = new ArrayList<HTMLObjectHolder>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from objects where pageNO='"+pageNumber+"' and EnrichedID='"+enrichedId+"' and BID='"+bookId+"' and objType<>'"+"PageTableBG"+"' order by SequentialID";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			String objType = c.getString(1);
			int objBID = Integer.parseInt(c.getString(2));
			String objPageNo = c.getString(3);
			String objLocation = c.getString(4);
			String objSize = c.getString(5);
			String objContent = c.getString(6);
			int objUniqueRowId = Integer.parseInt(c.getString(7));
			int objSequentialId = Integer.parseInt(c.getString(8));
			String objSclaPageToFit = c.getString(9);
			String objLocked = c.getString(10);
			int objEnrichID = Integer.parseInt(c.getString(11));
			HTMLObjectHolder object = new HTMLObjectHolder(objType, objBID, objPageNo, objLocation, objSize, objContent, objUniqueRowId, objSequentialId, objSclaPageToFit, objLocked, objEnrichID);
			arrayList.add(object);
			c.moveToNext();
		}
		c.close();
		//db.close();
		return arrayList;
	}

	/**
	 * @author Get max Unique row id
	 * @return 
	 */
	public int getMaxUniqueRowID(String tableName){
		int maxID = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from sqlite_sequence where name='"+tableName+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		if(c != null && c.moveToFirst()){
			maxID = c.getInt(1);
		}
		c.close();
		//db.close();
		return maxID;
	}

	public String getMaxId(String tableName){
		SQLiteDatabase db = getReadableDatabase();
		StringBuffer buffer = new StringBuffer();
		Cursor c = db.query(tableName, new String[]{"MAX(templateId)"}, null, null, null, null, null);
		if(c.getCount()>0){
			c.moveToFirst();
			String max_id=c.getString(0);
			buffer.append(max_id);
		}
		c.close();
	//	db.close();
		return buffer.toString();
	}
	
	/**
	 * Check is it in tiled mode
	 */
	public Boolean checkTiledModePageBackground(int bookID, String objType, String pageNumber){
		
		Boolean returnValue = false;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select size from objects where BID ="+bookID+" and objType='"+objType+"'"+"and pageNO='"+pageNumber+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		if(c.getCount()>0){
			String mode =c.getString(0);
			if(mode.equals("Tiled")){
				returnValue = true;
			}else{
				returnValue = false;
			}
		}
		c.close();
		//db.close();
		return returnValue;
	}

	public String[] getPgeBgObjType(int BID, String pageNumber, int enrId) {
		String objType = "";
		String content = "";
		String[] objContent = new String[2];
		SQLiteDatabase db = getReadableDatabase();
		String query = "select objType, content from objects where (BID ='"+BID+"' and pageNO='"+pageNumber+"' and EnrichedID='"+enrId+"') and (objType='"+Globals.OBJTYPE_PAGEBG+"' OR objType='"+Globals.OBJTYPE_MINDMAPBG+"' OR objType='"+Globals.OBJTYPE_PAGEBGEND+"')";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			objType = c.getString(0);
			content = c.getString(1);
			objContent[0] = objType;
			objContent[1] = content;
		}
		c.close();
		//db.close();
		return objContent;
	}
	
	/**
	 *  Update tiled/scaletofit mode
	 */
	public void updateTiledOrScaleToFitPageBackground(int bookID, String objType, String pageNumber, String mode){
		try
		{
			SQLiteDatabase db = getWritableDatabase();
			String location;
			String updateModeQuery;
			if(mode.equals("Tiled")){
				location = "0|0";
				updateModeQuery = "update objects set size ='"+mode+"', location='"+location+"' where BID='"+bookID+"'"+" and objType = '"+objType+"'"+" and pageNO = '"+pageNumber+"'";
			}else{
				updateModeQuery = "update objects set size ='"+mode+"'"+" where BID='"+bookID+"'"+" and objType = '"+objType+"'"+" and pageNO = '"+pageNumber+"'";
			}
			
			db.execSQL(updateModeQuery);
			//db.close();
		}
		catch (SQLException e){}
		catch (RuntimeException e){}
	}
	
	/**
	 *  Page background rotation angle
	 */
	
	public int getPageBackgroundRotationAngle(int bookID, String objType, String pageNumber, int enrichId){
		
		int returnValue = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query;
		if (Integer.parseInt(pageNumber) == 0){
			query = "select location from objects where BID =" + bookID + " and objType='" + objType + "'" + "and pageNO='" + pageNumber + "'";
		} else {
			query = "select location from objects where BID =" + bookID + " and objType='" + objType + "'" + "and pageNO='" + pageNumber + "' and EnrichedID = '" + enrichId + "'";
		}
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		if(c.getCount()>0){
		String rotation =c.getString(0);
		String splitarray[] = rotation.split("\\|");
		returnValue = Integer.parseInt(splitarray[0]);
		}
		c.close();
		//db.close();
		return returnValue;
		
	}
	  
	/**
	 *  Get page background color value from DB
	 */
	
	public String getPageBackgroundColor(int bookID, String objType, String pageNumber, int enrichId){
		
		String returnValue = "NoEntry";
		SQLiteDatabase db = getReadableDatabase();
		String query;
		if(pageNumber.equals("End")){
			query = "select content from objects where BID =" + bookID + " and objType='" + objType + "'" + "and pageNO='" + pageNumber + "'";
		}else {
			if (Integer.parseInt(pageNumber) == 0) {
				query = "select content from objects where BID =" + bookID + " and objType='" + objType + "'" + "and pageNO='" + pageNumber + "'";
			} else {
				query = "select content from objects where BID =" + bookID + " and objType='" + objType + "'" + "and pageNO='" + pageNumber + "' and EnrichedID = '" + enrichId + "'";
			}
		}

		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		if(c.getCount()>0){
			
			String rotation =c.getString(0);
			returnValue = rotation;
		}
			c.close();
			//db.close();
			return returnValue;
	}
	
	/**
	 * @author Get Sequential Id
	 * @param uniqueId
	 * @return
	 */
	public int getSequentialIdFromDb(int uniqueId) {
		int seqID = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select SequentialID from objects where RowID='"+uniqueId+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			seqID = c.getInt(0);
		}
		c.close();
		//db.close();
		return seqID;
	}

	public int getupdateCountsFromDb(int BID) {
		int updateCount = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select UpdateCounts from books where BID='"+BID+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			updateCount = c.getInt(0);
		}
		c.close();
		//db.close();
		return updateCount;
	}
	/**
	 * Get Plain Background Color from database
	 * @param BID
	 * @param objType
	 * @param pageNumber
	 * @return
	 */
	public String getPlainBackgroundColorFromDB(int BID, String objType, String pageNumber) {
		String content = "";
		SQLiteDatabase db = getReadableDatabase();
		String query = "select content from objects where objType = '"+objType+"' and BID = '"+BID+"' and pageNO = '"+pageNumber+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			content = c.getString(0);
		}
		c.close();
		//db.close();
		return content;
	}
	
	/**
	 * Get Background Image Rotation Angle and scale mode from DB
	 * @param BID
	 * @param objType
	 * @param pageNumber
	 * @return
	 */
	public String getBackgroundImageRotationAngleAndScaleModeinDB(int BID, String objType, String pageNumber){
		String imageRotatedAngle = null, location = null, size = null;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select location,size from objects where objType = '"+objType+"' and BID = '"+BID+"' and pageNO = '"+pageNumber+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			location = c.getString(0);
			size = c.getString(1);
		}
		c.close();
		//db.close();
		if (location != null) {
			String[] locaArr = location.split("\\|");
			imageRotatedAngle = locaArr[0];
		}
		if(imageRotatedAngle!=null) {
			return Integer.parseInt(imageRotatedAngle) + "|" + size;
		}else{
			return null;
		}
	}
	
	/**
	 * getAll templates
	 * @return
	 */
	public ArrayList<Templates> getAllTemplatesListBasedOnOrientation(String orientation) {
		ArrayList<Templates> templateList = new ArrayList<Templates>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from templates where type = '"+orientation+"' or type = 'both'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int templateId = c.getInt(0);
			String templateName = c.getString(1);
			String templateType = c.getString(2);
			String templateTitleFont = c.getString(3);
			int templateTitleSize = c.getInt(4);
			String templateTitleColor = c.getString(5);
			String templateAuthorFont = c.getString(6);
			int templateAuthorSize = c.getInt(7);
			String templateAuthorColor = c.getString(8);
			String templateParaFont = c.getString(9);
			String templateParaColor = c.getString(10);
			int templateParaSize = c.getInt(11);
			
			Templates template = new Templates();
			template.setTemplateId(templateId);
			template.setTemplateName(templateName);
			template.setTemplateType(templateType);
			template.setTemplateTitleFont(templateTitleFont);
			template.setTemplateTitleSize(templateTitleSize);
			template.setTemplateTitleColor(templateTitleColor);
			template.setTemplateAuthorFont(templateAuthorFont);
			template.setTemplateAuthorSize(templateAuthorSize);
			template.setTemplateAuthorColor(templateAuthorColor);
			template.setTemplateParaFont(templateParaFont);
			template.setTemplateParaColor(templateParaColor);
			template.setTemplateParaSize(templateParaSize);
			
			templateList.add(template);
			c.moveToNext();
		}
		c.close();
		//db.close();
		return templateList;
	}
	
	/**
	 * getAll category list
	 * @return
	 */
	public ArrayList<Category> getAllCategoryList() {
		ArrayList<Category> categoryList = new ArrayList<Category>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from tblCategory order by newCID ASC";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int categoryId = c.getInt(0);
			String categoryName = c.getString(1);
			String categoryImagePath = c.getString(2);
			int newCatId=c.getInt(3);
			boolean isCategoryHidden=Boolean.parseBoolean(c.getString(4));
			if (categoryImagePath != null && categoryImagePath.contains("#")){
			} else {
				categoryImagePath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+categoryImagePath;
			}

			Category category = new Category();
			category.setCategoryId(categoryId);
			category.setCategoryName(categoryName);
			category.setCategoryImagePath(categoryImagePath);
			category.setNewCatID(newCatId);
			category.setHidden(isCategoryHidden);
			if (!category.isHidden()) {
				categoryList.add(category);
			}
			c.moveToNext();
		}
		Collections.sort(categoryList, new Comparator<Category>() {
			@Override
			public int compare(Category lhs, Category rhs) {
				int val1 = lhs.getNewCatID();
				int val2 = rhs.getNewCatID();
				return Integer.valueOf(val1).compareTo(Integer.valueOf(val2));
			}
		});


	/*	Category category = new Category();
		category.setCategoryId(0);
		category.setCategoryName("Add category");
		String imagePath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH+"-1.png";
		category.setCategoryImagePath(imagePath);
		categoryList.add(category); */
		
		c.close();
		//db.close();
		return categoryList;
	}
	
	/**
	 * get page number object count
	 * @param pageNumber
	 * @return
	 */
	public int getPageNumberObjectUniqueRowId(int pageNumber, int bookId, String objType) {
		int objectCount = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select count(*) from objects where pageNO='"+pageNumber+"' and BID='"+bookId+"' and objType='"+objType+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			objectCount = c.getInt(0);
		}
		c.close();
		//db.close();
		return objectCount;
	}
	
	/**
	 * get Maximum SequentialId value
	 * @param pageNumber
	 * @return
	 */
	public int getMaximumSequentialIdValue(int pageNumber, int bookId){
		int maxID = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select MAX(SequentialID) from objects where pageNO='"+pageNumber+"' and BID='"+bookId+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			maxID = c.getInt(0);
		}
		c.close();
		//db.close();
		return maxID;
	}
	/**
	 * get Maximum SequentialId value
	 * @param Enrichments
	 * @return
	 */
	public int getMaximumSequentialId(int pageNumber, int bookId){
		int maxID = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select MAX(SequentialID) from enrichments where pageNO='"+pageNumber+"' and BID='"+bookId+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			maxID = c.getInt(0);
		}
		c.close();
		//db.close();
		return maxID;
	}

	/**
	 * getSequential id based on page number and object type
	 * @param pageNumber
	 * @param objType
	 * @return
	 */
	public int getSequentialId(int pageNumber, String objType, int bookId) {
		int seqID = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select SequentialID from objects where pageNO='"+pageNumber+"' and objType='"+objType+"' and BID='"+bookId+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			seqID = c.getInt(0);
		}
		c.close();
		//db.close();
		return seqID;
	}
	
	public String checkPaidVersion()
	{
		String pv="";
		SQLiteDatabase db = getReadableDatabase();
		String query = "select purchase from tblPurchase";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst())
		{
			pv = c.getString(0);
		}
		c.close();
		//db.close();
		return pv;
	}
	
	public String getRecord(String TID) {
		String Record1 = "", Record2= "";
		SQLiteDatabase db = getReadableDatabase();
		String query = "select SDesc,Color from tblNote where TID ='"+TID+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			Record1 = c.getString(0);
			Record2 = c.getString(1);
		}
		c.close();
		//db.close();
		return Record1+"|"+Record2;
	}
	
	public String[] getStext(String bName, int pageNo, int tabNo){
		SQLiteDatabase db = getReadableDatabase();
		String query = "select SText from tblHighlight where BName='"+bName+"' and PageNo='"+pageNo+"' and TabNo='"+tabNo+"'";
		Cursor c = db.rawQuery(query, null);
		String[] sTextArray = new String[c.getCount()];
		int i = 0;
		c.moveToFirst();
		while (c.isAfterLast() == false){
			sTextArray[i] = c.getString(0);
			i++;
			c.moveToNext();
		}
		c.close();
		//db.close();
		return sTextArray;
	}
	
	public int[] getOccurence(String bName, int pageNo, int tabNo){
		SQLiteDatabase db = getReadableDatabase();
		String query = "select Occurence from tblHighlight where BName='"+bName+"' and PageNo='"+pageNo+"' and TabNo='"+tabNo+"'";
		Cursor c = db.rawQuery(query, null);
		int[] occurenceArray = new int[c.getCount()];
		int i = 0;
		c.moveToFirst();
		while (c.isAfterLast() == false){
			occurenceArray[i] = c.getInt(0);
			i++;
			c.moveToNext();
		}
		c.close();
		//db.close();
		return occurenceArray;
	}
	
	public String getPageFilter(String bName) {
		// TODO Auto-generated method stub
		String LSearch = "", LBookmark = "", LCopy = "", LFlip = "", LNavigation = "", LHighlight = "", LNote = "", LGotoPage = "", LindexPage = "", LZoom = "", LWebSearch = "", LTranslationSearch = "", LBookLanguage = "";
		SQLiteDatabase db = getReadableDatabase();
		String query = "select Search,Bookmark,Copy,Flip,Navigation,Highlight,Note,GotoPage,indexPage,Zoom,WebSearch,WikiSearch,TranslationSearch,dictionarySearch,booksearch,blanguage from books where storeID ='"+bName+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			LSearch = c.getString(0);
			LBookmark = c.getString(1);
			LCopy = c.getString(2);
			LFlip = c.getString(3);
			LNavigation = c.getString(4);
			LHighlight = c.getString(5);
			LNote = c.getString(6);
			LGotoPage = c.getString(7);
			LindexPage = c.getString(8);
			LZoom = c.getString(9);
			LWebSearch = c.getString(10);
			LTranslationSearch = c.getString(12);
			LBookLanguage = c.getString(15);
		}
		c.close();
		//db.close();
		return LSearch+"|"+LBookmark+"|"+LCopy+"|"+LFlip+"|"+LNavigation+"|"+LHighlight+"|"+LNote+"|"+LGotoPage+"|"+LindexPage+"|"+LZoom+"|"+LWebSearch+"|"+LTranslationSearch+"|"+LBookLanguage;
	}
	
	public int getHighlightCount(String bName, int pageNumber, String IVal){
		int retval = 0;
		String[] searchResults = IVal.split("\\|");
		String STextTemp = searchResults[3].trim();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select count(*) from tblHighlight where BName='"+bName+"' and PageNo='"+pageNumber+"' and SText='"+STextTemp+"' and Occurence='"+searchResults[1]+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			retval = c.getInt(0);
		}
		c.close();
		//db.close();
		return retval;
	}

	public String getMultipleRecord(String TID) {
		String BName = "", PNo = "", NPos = "", tabNo = "";
		SQLiteDatabase db = getReadableDatabase();
		String query = "select BName,PageNo,NPos,TabNo from tblNote where TID ='"+TID+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			BName = c.getString(0);
			PNo = c.getString(1);
			NPos = c.getString(2);
			tabNo = c.getString(3);
		}
		c.close();
		//db.close();
		return BName+"|"+PNo+"|"+NPos+"|"+tabNo;
	}
	
	public ArrayList<String> selectPageRecords(String multRecs) {
		SQLiteDatabase db = getReadableDatabase();
		String[] searchresults = multRecs.split("\\|");
		String query = "select SText from tblNote where BName='"+searchresults[0]+"' and PageNo='"+searchresults[1]+"' and NPos='"+searchresults[2]+"' and TabNo='"+searchresults[3]+"'";
		Cursor c = db.rawQuery(query, null);
		ArrayList<String> pages = new ArrayList<String>();
		c.moveToFirst();
		while (c.isAfterLast() == false){
			pages.add(c.getString(0));
			c.moveToNext();
		}
		c.close();
		//db.close();
		return pages;
	}
	
	public ArrayList<String> selectPageValuesRecord(String multRecs) {
		SQLiteDatabase db = getReadableDatabase();
		String[] searchresults = multRecs.split("\\|");
		String query = "select TID from tblNote where BName='"+searchresults[0]+"' and PageNo='"+searchresults[1]+"' and NPos='"+searchresults[2]+"' and TabNo='"+searchresults[3]+"'";
		Cursor c = db.rawQuery(query, null);
		ArrayList<String> pagesValue = new ArrayList<String>();
		c.moveToFirst();
		while (c.isAfterLast() == false){
			pagesValue.add("E|"+c.getString(0));
			c.moveToNext();
		}
		c.close();
		//db.close();
		return pagesValue;
	}
	
	public int getSameLinePosition(String bName, int pageNo, int iVal, int tabNo) {
		int retVal = 0;
		SQLiteDatabase db = getReadableDatabase();
		int iValM = iVal-5;
		int iValP = iVal+5;
		String query = "select NPos from tblNote where BName='"+bName+"' and PageNo='"+pageNo+"' and TabNo='"+tabNo+"' and NPos>='"+iValM+"' and NPos<='"+iValP+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			retVal = c.getInt(0);
		}
		c.close();
		//db.close();
		return retVal;
	}
	
	public Object[] loadNoteDivToDatabase(String bookName, int pageNumber, int tabNo) {
		SQLiteDatabase db = getReadableDatabase();
		String query = "select TID,SText,Occurence,NPos,Color from tblNote where BName='"+bookName+"' and PageNo='"+pageNumber+"' and TabNo='"+tabNo+"'";
		Cursor c=null;
		String[] TID,SText,Occurences,NPos,CID;
		try {
			c = db.rawQuery(query, null);
			TID = new String[c.getCount()];
			SText = new String[c.getCount()];
			Occurences = new String[c.getCount()];
			NPos = new String[c.getCount()];
			CID = new String[c.getCount()];
			int i = 0;
			c.moveToFirst();
			while (c.isAfterLast() == false) {
				TID[i] = c.getString(0);
				SText[i] = c.getString(1);
				Occurences[i] = c.getString(2);
				NPos[i] = c.getString(3);
				CID[i] = c.getString(4);
				i++;
				c.moveToNext();
			}
		}finally {
			if(c!=null) {
				c.close();
			}
		}
		//db.close();
		return new Object[]{TID, SText, Occurences, NPos, CID};
	}
	
	public ArrayList<NoteData> loadNoteDivToArray(String bookName) {
		ArrayList<NoteData> noteDataArrayList = new ArrayList<>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select PageNo,SText,SDesc,Color,TabNo,NoteType from tblNote where  CallStatus='"+0+"' and BName='"+bookName+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			NoteData noteData = new NoteData();
			noteData.setNotePageNo(Integer.parseInt(c.getString(0)));
			noteData.setNoteTitle(c.getString(1));
			noteData.setNoteDesc(c.getString(2));
			noteData.setNoteColor(Integer.parseInt(c.getString(3)));
			noteData.setNoteTabNo(Integer.parseInt(c.getString(4)));
			noteData.setNoteType(c.getInt(5));
			noteDataArrayList.add(noteData);
			c.moveToNext();
		}
		c.close();
		//db.close();
		return noteDataArrayList;
	}

	public ArrayList<NoteData> loadNoteInCallDivToArray(String bookName) {
		ArrayList<NoteData> noteDataArrayList = new ArrayList<>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select PageNo,SText,SDesc,Color,TabNo,NoteType from tblNote where  CallStatus='"+1+"' and BName='"+bookName+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			NoteData noteData = new NoteData();
			noteData.setNotePageNo(Integer.parseInt(c.getString(0)));
			noteData.setNoteTitle(c.getString(1));
			noteData.setNoteDesc(c.getString(2));
			noteData.setNoteColor(Integer.parseInt(c.getString(3)));
			noteData.setNoteTabNo(Integer.parseInt(c.getString(4)));
			noteData.setNoteType(c.getInt(5));
			noteDataArrayList.add(noteData);
			c.moveToNext();
		}
		c.close();
		//db.close();
		return noteDataArrayList;
	}
	public ArrayList<NoteData> loadNotesInCurrentPage(String bookName,int pageNo) {
		ArrayList<NoteData> noteDataArrayList = new ArrayList<>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select PageNo,SText,SDesc,Color,TabNo,NoteType from tblNote where BName='"+bookName+"' and  CallStatus='"+0+"' and PageNo='"+pageNo+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			NoteData noteData = new NoteData();
			noteData.setNotePageNo(Integer.parseInt(c.getString(0)));
			noteData.setNoteTitle(c.getString(1));
			noteData.setNoteDesc(c.getString(2));
			noteData.setNoteColor(Integer.parseInt(c.getString(3)));
			noteData.setNoteTabNo(Integer.parseInt(c.getString(4)));
			noteData.setNoteType(c.getInt(5));
			noteDataArrayList.add(noteData);
			c.moveToNext();
		}
		c.close();
		//db.close();
		return noteDataArrayList;
	}
	public Object[] loadElessonNoteDivToArray(String lessonID) {
		SQLiteDatabase db = getReadableDatabase();
		String query = "select noteText from tblNoteElesson where lessonID='"+lessonID+"'";
		Cursor c = db.rawQuery(query, null);
		String[] noteText = new String[c.getCount()];

		int i = 0;
		c.moveToFirst();
		while (c.isAfterLast() == false){
			noteText[i] = c.getString(0);

			i++;
			c.moveToNext();
		}
		c.close();
		//db.close();
		return new Object[]{noteText};
	}
	public int getNoteCountFromDB(String bName, int pageNumber, String iVal) {
		int retval = 0;
		String[] searchResults = iVal.split("\\|");
		String STextTemp = searchResults[3].trim();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select count(*) from tblNote where BName='"+bName+"' and PageNo='"+pageNumber+"' and SText='"+STextTemp+"' and Occurence='"+searchResults[1]+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			retval = c.getInt(0);
		}
		c.close();
		//db.close();
		return retval;
	}
	
	public int getBookMarkCount(String bName, int pageNo, int tabNo){
		int bookMarkCount = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select count(*) from tblBookMark where BName='"+bName+"' and PageNo='"+pageNo+"' and TabNo='"+tabNo+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			bookMarkCount = c.getInt(0);
		}
		c.close();
		//db.close();
		return bookMarkCount;
	}

	public int getBookMarkTagID(String bName, int pageNo, int tabNo){
		int bookMarkTagID = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select TagID from tblBookMark where BName='"+bName+"' and PageNo='"+pageNo+"' and TabNo='"+tabNo+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			bookMarkTagID = c.getInt(0);
		}
		c.close();
		//db.close();
		return bookMarkTagID;
	}

	public Object[] getAllNotesFromDb(String bookName, int pageNumber, int tabNo) {
		SQLiteDatabase db = getReadableDatabase();
		String query = "select TID,SText,Occurence,NPos,Color,SDesc,NoteType from tblNote where BName='"+bookName+"' and PageNo='"+pageNumber+"' and TabNo='"+tabNo+"'";
		Cursor c = db.rawQuery(query, null);
		String[] TID = new String[c.getCount()];
		String[] SText = new String[c.getCount()];
		String[] Occurences = new String[c.getCount()];
		String[] NPos = new String[c.getCount()];
		String[] CID = new String[c.getCount()];
		String[] SDesc = new String[c.getCount()];
		String[] NTpye = new String[c.getCount()];
		int i = 0;
		c.moveToFirst();
		while (c.isAfterLast() == false){
			TID[i] = c.getString(0);
			SText[i] = c.getString(1);
			Occurences[i] = c.getString(2);
			NPos[i] = c.getString(3);
			CID[i] = c.getString(4);
			SDesc[i]=c.getString(5);
			NTpye[i] = c.getString(6);
			i++;
			c.moveToNext();
		}
		c.close();
		//db.close();
		return new Object[]{TID, SText, Occurences, NPos, CID,SDesc,NTpye};
	}

	/*public ArrayList<String> getMalzamahBooksList(String searchText){
		ArrayList<String> bookMarkArray = new ArrayList<String>();
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from books"; where Title like "%"+searchText+"%\"";
		String query="select * from books where storeID  like \"%"+searchText+"%\"";

		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			int  BID = Integer.parseInt(c.getString(0));
			String Title = c.getString(1);
			String Description = c.getString(2);
			String Author = c.getString(3);
			int totalPages = Integer.parseInt(c.getString(5));
			boolean storeBook = Boolean.parseBoolean(c.getString(9));
			String storeID = c.getString(11);
			String bookLanguage = c.getString(39);
			String bookStr=BID+"##"+Title+"##"+Description+"##"+Author+"##"+storeID+"##"+bookLanguage+"##"+totalPages;
			bookMarkArray.add(bookStr);
			c.moveToNext();
		}
		c.close();
		db.close();
		return bookMarkArray;
	} */
	public ArrayList<String> getMalzamahBooksList(){
		ArrayList<String> bookMarkArray = new ArrayList<String>();
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from books"; where Title like "%"+searchText+"%\"";
		String query="select * from books where CID='2'";

		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			int  BID = Integer.parseInt(c.getString(0));
			String Title = c.getString(1);
			String Description = c.getString(2);
			String Author = c.getString(3);
			int totalPages = Integer.parseInt(c.getString(5));
			boolean storeBook = Boolean.parseBoolean(c.getString(9));
			String storeID = c.getString(11);
			String bookLanguage = c.getString(39);
			String bookStr=BID+"##"+Title+"##"+Description+"##"+Author+"##"+storeID+"##"+bookLanguage+"##"+totalPages;
			bookMarkArray.add(bookStr);
			c.moveToNext();
		}
		c.close();
		//db.close();
		return bookMarkArray;
	}

	public ArrayList<String> getMalzamahBooks(String query){
		ArrayList<String> bookMarkArray = new ArrayList<String>();
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from books"; where Title like "%"+searchText+"%\"";
		//String query="select * from malzamah";

		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			int  BID = Integer.parseInt(c.getString(1));
			int PageNo = Integer.parseInt(c.getString(2));
			String sourceStoreId = c.getString(3);
			String htmlPath = c.getString(4);
			//int totalPages = Integer.parseInt(c.getString(5));
			//boolean storeBook = Boolean.parseBoolean(c.getString(9));
			String thumbPath = c.getString(5);
			//String bookLanguage = c.getString(39);
			String bookStr=BID+"##"+PageNo+"##"+sourceStoreId+"##"+htmlPath+"##"+thumbPath;
			bookMarkArray.add(bookStr);
			c.moveToNext();
		}
		c.close();
		//db.close();
		return bookMarkArray;
	}


	public int getStoreBookCountFromBookList(String storeBID) {int enrTabCount = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select count(*) FROM books where storeID='"+storeBID+"' and isDownloadedCompleted='downloaded';";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			enrTabCount = c.getInt(0);
		}
		c.close();
		//db.close();
		return enrTabCount;
	}

	/**
 * getQuizSequenceArray
 * @return
 */
public ArrayList<QuizSequenceObject> getQuizSequenceArray(int quizId) {
	ArrayList<QuizSequenceObject> quizSequenceObjectArray = new ArrayList<QuizSequenceObject>();
	SQLiteDatabase db = getReadableDatabase();
	String query = "select * from tblMultiQuiz where quizId ='"+quizId+"'";
	Cursor c = db.rawQuery(query, null);
	c.moveToFirst();
	while (c.isAfterLast() == false) {
		int sectionId = c.getInt(0);
		int quizUniqueId = c.getInt(1);
		
		QuizSequenceObject quizSequenceObject = new QuizSequenceObject();
		quizSequenceObject.setSectionId(sectionId);
		quizSequenceObject.setUniqueid(quizUniqueId);
		
		
		quizSequenceObjectArray.add(quizSequenceObject);
		c.moveToNext();
	}
	c.close();
	//db.close();
	return quizSequenceObjectArray;
}

	public ArrayList<String> selectPgTableBgFromObjects(String BID,int PageNo) {
		SQLiteDatabase db = getReadableDatabase();
		String query = "select location, content from objects where objType ='"+"PageTableBG"+"' and PageNo='"+PageNo+"'and  BID='"+BID+"'";
		Cursor c = db.rawQuery(query, null);
		ArrayList<String> contentValue = new ArrayList<String>();
		c.moveToFirst();
		while (c.isAfterLast() == false){
			contentValue.add(c.getString(0)+"##"+c.getString(1));
			c.moveToNext();
		}
		c.close();
		return contentValue;
	}

	public ArrayList<String> selectMindMapBgFromObjects(String mindMapTitle) {
		SQLiteDatabase db = getReadableDatabase();
		String query = "select BID, pageNO, EnrichedID from objects where content ='"+mindMapTitle+"' and objType='"+Globals.OBJTYPE_MINDMAPBG+"'";
		Cursor c = db.rawQuery(query, null);
		ArrayList<String> contentValue = new ArrayList<String>();
		c.moveToFirst();
		while (c.isAfterLast() == false){
			contentValue.add(c.getString(0)+"##"+c.getString(1)+"##"+c.getString(2));
			c.moveToNext();
		}
		c.close();
		return contentValue;
	}
	
	public void unbindDrawables(View view){
		if(view.getBackground() != null){
			view.getBackground().setCallback(null);
		}
		if(view instanceof ViewGroup){
			for(int i = 0; i < ((ViewGroup) view).getChildCount(); i++){
				unbindDrawables(((ViewGroup) view).getChildAt(i));
			}
			try {
				((ViewGroup) view).removeAllViews();
			} catch (Exception e) {

			}
		}
	}

	public ArrayList<Integer> getDownloadedCloudEnrList() {
		ArrayList<Integer> downloadedCloudEnrList = new ArrayList<Integer>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from CloudObjects";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int objId = Integer.parseInt(c.getString(0));
			downloadedCloudEnrList.add(objId);
			c.moveToNext();
		}
		c.close();
		return downloadedCloudEnrList;
	}

	private void updateNewCID(SQLiteDatabase db){
		db.execSQL("update  tblCategory set newCID='1' where CID='1'");
		db.execSQL("update  tblCategory set newCID='2' where CID='2'");
		db.execSQL("update  tblCategory set newCID='3' where CID='3'");
		db.execSQL("update  tblCategory set newCID='4' where CID='4'");
		db.execSQL("update  tblCategory set newCID='5' where CID='5'");
//		db.execSQL("update  tblCategory set newCID='6' where CName='Pdf'");
	}

	public ArrayList<String> getselectedgroup(String groupName, int scopeId, SlideMenuWithActivityGroup context) {
		ArrayList<String> contentValue = new ArrayList<String>();
		SQLiteDatabase db = getReadableDatabase();
		String query = "select * from userGroups where groupName='"+groupName+"'and userID='"+scopeId+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {

			if(!c.getString(3).equals("")) {
				contentValue.add(c.getString(3) + "-" + c.getString(4));
			}
			c.moveToNext();
		}
		c.close();
		//db.close();

		return contentValue;


	}

	public String getStoreBookDomainUrl(String bName,int Bid){
		String domainUrl= null;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select domainURL from books where storeID='"+bName+"' and BID='"+Bid+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			domainUrl =c.getString(0);
		}
		c.close();
		//db.close();
		return domainUrl;
	}

	/**
	 * getAllThemeDetails
	 * @paramBID
	 * @parampageNo
	 * @param context
	 * @return
	 */
	public ArrayList<Theme> getThemeDetails( String query,Context context) {
		ArrayList<Theme> themesList = new ArrayList<Theme>();
		SQLiteDatabase db = getReadableDatabase();

		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int themeId = c.getInt(0);
			String themeName = c.getString(1);
			String createDate = c.getString(2);
			String foregroundColor = c.getString(3);
			int category = c.getInt(4);

			Theme themeData=new Theme(context);
			themeData.setThemeID(themeId);
			themeData.setThemeName(themeName);
			themeData.setCreateDate(createDate);
			themeData.setCategory(category);
			themeData.setForeGroundColor(foregroundColor);
			String filePath= Globals.TARGET_BASE_FILE_PATH+"Themes/"+themeId;
			File[] adapterFiles=themeData.getFilesList(filePath);
			themeData.setAdapterFiles(adapterFiles);
			themesList.add(themeData);

			c.moveToNext();
		}
		c.close();
		//db.close();

		return themesList;
	}

	/**
	 * @author Get No. of Bookss
	 */
	public int getThemeCount(int themeId){
		int themeCount = 0;
		SQLiteDatabase db = getReadableDatabase();
		String query = "select count(*) from Themes where themeID='"+themeId+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			themeCount = c.getInt(0);
		}
		c.close();
		//db.close();
		return themeCount;
	}
	public boolean isTemplateExist(String templateId){
		boolean exist;
		Cursor cursor = null;
		SQLiteDatabase db = getReadableDatabase();
		String sql ="SELECT templateID FROM templates WHERE templateID="+templateId;
		cursor= db.rawQuery(sql,null);
		if(cursor.getCount()>0){
			exist = true;
		}else{
			exist = false;
		}
		cursor.close();
		return exist;
	}

	/**
	 * @author Get  Category details
	 */
	public int checkCategoryExist(String query){
		int categoryCount = 0;
		SQLiteDatabase db = getReadableDatabase();
//	String query = "select count(*) from Themes where themeID='"+themeId+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			categoryCount = c.getInt(0);
		}
		c.close();
		//db.close();
		return categoryCount;
	}
	/**
	 * this returns the enrichment tab count for the current page
	 * @param BID
	 * @param pageNo
	 * @return
	 */
	public int getCountForEnrichments(int BID,String query) {
		int enrTabCount = 0;
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select count(*) from enrichments where BID='"+BID+"'";
		Cursor c = db.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			enrTabCount = c.getInt(0);
		}
		c.close();
		//db.close();
		return enrTabCount;
	}
	public void addPdfNotes(String BookName, int currentPage, String setectedText, String WritenNote) {
		if (setectedText != null && setectedText.length() > 0) {
			ContentValues values = new ContentValues();
			values.put("BName", BookName);
			values.put("PageNo", currentPage);
			values.put("sText", WritenNote);
			values.put("Occurence", 0);
			values.put("SDesc", "");
			values.put("NPos", 0);
			values.put("Color", 0);
			values.put("ProcessSText", setectedText);
			values.put("TabNo", 0);
			values.put("Exported", 0);
			values.put("Flag", 0);
			values.put("Rect", setectedText);
			values.put("NoteText", WritenNote);
			SQLiteDatabase db = this.getWritableDatabase();
			db.insert("tblNote", null, values);
			db.close();
		}

	}

	public void savePdfNotes() {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("Flag", 1);
		db.update("tblNote", values, "Flag" + " = ?", new String[]{String.valueOf(0)});
		db.close();

	}
	public void updateInCall() {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("CallStatus", 0);
		db.update("tblNote", values, "CallStatus" + " = ?", new String[]{String.valueOf(1)});
		db.close();

	}
	public void deletePdfNotes() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete("tblNote", "Flag" + " = ?", new String[]{String.valueOf(0)});
		db.close();
	}

	public Enrichments getEnrichment(int enrId,int sequenceId,Context context) {
		Enrichments enrichment = null;
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from enrichments where BID='"+BID+"' and pageNO='"+pageNo+"'";
		String query="select * from enrichments where enrichmentID='"+enrId+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			int enrID = c.getInt(0);
			int enrBid = c.getInt(1);
			int enrPageNo = c.getInt(2);
			String enrTitle = c.getString(3);
			String enrType = c.getString(4);
			int enrExportedId = c.getInt(5);
			String path=c.getString(6);
			int catID = c.getInt(8);

			enrichment = new Enrichments(context);
			enrichment.setEnrichmentId(enrID);
			enrichment.setEnrichmentBid(enrBid);
			enrichment.setEnrichmentPageNo(enrPageNo);
			enrichment.setEnrichmentTitle(enrTitle);
			enrichment.setEnrichmentType(enrType);
			enrichment.setEnrichmentSequenceId(sequenceId);
			enrichment.setEnrichmentExportValue(enrExportedId);
			enrichment.setEnrichmentPath(path);
			enrichment.setCategoryID(catID);

			c.moveToNext();
		}
		c.close();
		//db.close();
		return enrichment;
	}
}
