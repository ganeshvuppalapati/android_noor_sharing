package com.semanoor.source_sboookauthor;

import java.util.ArrayList;

import com.semanoor.sboookauthor_store.BookMarkEnrichments;

public class QuizSequenceObject {
private int sectionId;
private int uniqueid;
	/**
	 * @return the uniqueid
	 */
	public int getUniqueid() {
		return uniqueid;
	}

	/**
	 * @param uniqueid the uniqueid to set
	 */
	public void setUniqueid(int uniqueid) {
		this.uniqueid = uniqueid;
	}

	/**
	 * @return the sectionId
	 */
	public int getSectionId() {
		return sectionId;
	}

	/**
	 * @param sectionId the sectionId to set
	 */
	public void setSectionId(int sectionId) {
		this.sectionId = sectionId;
	}

}
