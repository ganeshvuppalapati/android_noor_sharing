package com.semanoor.source_sboookauthor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;

import com.semanoor.manahij.R;
import com.semanoor.sboookauthor_store.Enrichments;

public class EpubExportGenerateHTML {


	private Context context;
	private ArrayList<Object> objectList;
	private int pageNumber;
	private DatabaseHandler db;
	private Book currentBook;
	private float scaleX, scaleY; 
	private ArrayList<Enrichments> enrichmentTabListArray;
	private int pageWidth;
	private int pageHeight;

	public EpubExportGenerateHTML(Context ctx, DatabaseHandler _db, Book _currentBook, String pageNum, ArrayList<Enrichments> _enrichmentTabListArray) {
		this.context = ctx;
		this.db = _db;
		this.currentBook = _currentBook;
		this.pageNumber = Integer.parseInt(pageNum);
		if (currentBook.is_bStoreBook()) {
			String[] offsetWidthAndHeight = currentBook.get_bOffsetWidthAndHeight().split("\\|");
			this.scaleX = Globals.getMainDesignPageWidth()/Float.parseFloat(offsetWidthAndHeight[0]);
			this.scaleY = Globals.getMainDesignPageHeight()/Float.parseFloat(offsetWidthAndHeight[1]);
		}
		this.enrichmentTabListArray = _enrichmentTabListArray;
		pageWidth = Globals.getDeviceWidth();
		pageHeight = (int) (Globals.getDeviceHeight() - context.getResources().getDimension(R.dimen.shelf_toolbar_height));
	}

	public void generatePage(String fileBasePath) {
		this.objectList = db.getAllObjectsForGeneratingHTML(String.valueOf(pageNumber), currentBook.getBookID(), 0);
		//File htmlFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+pageNumber+".htm");
		File htmlFilePath = new File(fileBasePath+pageNumber+".htm");
		generateHTMLFromObjectList(htmlFilePath, false, "");
		if (enrichmentTabListArray != null) {
			for (int i=0; i<enrichmentTabListArray.size(); i++) {
				Enrichments enrichments = enrichmentTabListArray.get(i);
				this.objectList = db.getAllObjectsForGeneratingHTML(String.valueOf(pageNumber), currentBook.getBookID(), enrichments.getEnrichmentId());
				int enrichNo = i+1;
				htmlFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+pageNumber+"-"+enrichNo+".htm");
				generateHTMLFromObjectList(htmlFilePath, false, enrichments.getEnrichmentType());
			}
		}
	}

	public void generateEnrichedPageForExportAndZipIt(int enrichId){
		this.objectList = db.getAllObjectsForGeneratingHTML(String.valueOf(pageNumber), currentBook.getBookID(), enrichId);
		String htmlFileDirPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/"+currentBook.getBookID()+"-"+pageNumber+"-"+enrichId;
		UserFunctions.createNewDirectory(htmlFileDirPath.toString());
		File htmlFilePath = new File(htmlFileDirPath+"/index.htm");
		generateHTMLFromObjectList(htmlFilePath, true, "");
		Zip zip = new Zip();
		String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/temp/"+currentBook.getBookID()+"-"+pageNumber+"-"+enrichId+".zip";
		zip.zipFileAtPath(htmlFileDirPath, destPath);
		UserFunctions.DeleteDirectory(new File(htmlFileDirPath));
	}

	private void generateHTMLFromObjectList(File htmlFilePath, boolean isExportEnrichments, String enrichmentType){
		String HTMLString = null;
		String pageBgPath;
		String pageBgColorValue;
		String imageRotationAngleandScaleMode;
		int imageRotatedAngle = 0;
		String scaleMode = null;
		String BGScript;
		String audioScript="";
		//Audio Script
		/*if (isExportEnrichments) {
				audioScript = "\nfunction isPlaying(playerId){"+
						"\nvar player = document.getElementById(playerId);"+
						"\n<![CDATA[return !player.paused && !player.ended && 0 < player.currentTime;]]>}"+
						"\nfunction FinishedPlaying(imageId){"+
						"\nvar imgId = document.getElementById(imageId);"+
						"\nimgId.src = \"Default_Audio_Play.png\";}"+
						"\nfunction StartOrStop(audioFile, imageId, audioId){"+
						"\nvar playing = false;"+
						"\nvar audie = document.getElementById(audioId);"+
						"\nvar imgId = document.getElementById(imageId);"+
						"\nplaying = isPlaying(audioId);"+
						"\naudie.src = audioFile;"+
						"\nif(playing){"+
						"\nimgId.src = \"Default_Audio_Play.png\";"+
						"\naudie.pause();"+
						"\n}else{"+
						"\nimgId.src = \"Default_Audio_Stop.png\";"+
						"\naudie.play();}}";
			} else {
				audioScript = "\nfunction isPlaying(playerId){"+
						"\nvar player = document.getElementById(playerId);"+
						"\n<![CDATA[return !player.paused && !player.ended && 0 < player.currentTime;]]>}"+
						"\nfunction FinishedPlaying(imageId){"+
						"\nvar imgId = document.getElementById(imageId);"+
						"\nimgId.src = \"images/Default_Audio_Play.png\";}"+
						"\nfunction StartOrStop(audioFile, imageId, audioId){"+
						"\nvar playing = false;"+
						"\nvar audie = document.getElementById(audioId);"+
						"\nvar imgId = document.getElementById(imageId);"+
						"\nplaying = isPlaying(audioId);"+
						"\naudie.src = audioFile;"+
						"\nif(playing){"+
						"\nimgId.src = \"images/Default_Audio_Play.png\";"+
						"\naudie.pause();"+
						"\n}else{"+
						"\nimgId.src = \"images/Default_Audio_Stop.png\";"+
						"\naudie.play();}}";
			}*/

		// Assigning CSS stylesheet for tables
		String tableStyles = "\ntable#SBATable{border-collapse:collapse;table-layout:fixed; overflow-x:scroll; word-break:break-all; width:100%;"+
				"\nheight:100%;}table#SBATable, td#SBATd{ border:2px solid black;background-color:transparent;text-align:center;}";

		//Get Page BackGround Path
		if (pageNumber == 1) {
			pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/pageBG_"+pageNumber+".png";
			if (!new File(pageBgPath).exists()) {
				//BGScript = "\n<script type='text/javascript'>function SetDimensions(){} "+audioScript+"\n</script>";
				BGScript = "\n<script type='text/javascript'>"+audioScript+"\n</script>";
				// Check for plain color page background
				pageBgColorValue = isPlainColorBackgroundExists(String.valueOf(pageNumber));
				if (pageBgColorValue.equals("")) {
					//Default Page Bg
					if (currentBook.getBookOrientation() == 0) {
						if (isExportEnrichments) {
							pageBgPath = "front_P.png";
							File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
							String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
							UserFunctions.copyFiles(sourcePath.toString(), destPath);
						} else {
							pageBgPath = "FreeFiles/front_P.png";
						}
					} else {
						if (isExportEnrichments) {
							pageBgPath = "front.png";
							File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
							String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
							UserFunctions.copyFiles(sourcePath.toString(), destPath);
						} else {
							pageBgPath = "FreeFiles/front.png";
						}
					}
					HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
							"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>" +
							"\n<body onload='SetDimensions();'>"+BGScript+" \n<div id='bgImage' style='background-image:url("+pageBgPath+"); background-repeat: no-repeat; " +
							"-webkit-background-size:100%; width:100%; height:100%; left:0px; top:0px; position:absolute;' >\n</div>";
				} else {
					//Page Bg plain color
					String hexPageBgColor = String.format("#%06X", (0xFFFFFF & Integer.parseInt(pageBgColorValue)));
					HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
							"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>" +
							"\n<body onload='SetDimensions();'>"+BGScript+" \n<div id='bgImage' style='background-color:"+hexPageBgColor+"; background-repeat: no-repeat; " +
							"-webkit-background-size:100%; width:100%; height:100%; left:0px; top:0px; position:absolute;' >\n</div>";
				}
			} else {
				//PageBg path exist
				imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), "PageBG", String.valueOf(pageNumber));
				imageRotatedAngle = Integer.parseInt(imageRotationAngleandScaleMode.split("\\|")[0]);
				scaleMode = imageRotationAngleandScaleMode.split("\\|")[1];
				BGScript = getPageBackgroundScript(imageRotatedAngle, audioScript);
				String bgImageScaleOrTiledMode = getBgImageScaleorTiledString(pageBgPath, scaleMode, isExportEnrichments, htmlFilePath.getParent().toString());
				HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
						"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>\n<body onload='SetDimensions();'>"+BGScript+""+bgImageScaleOrTiledMode+"";
			}
		} else if (pageNumber == currentBook.getTotalPages()) {
			pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/pageBG_End.png";
			if (!new File(pageBgPath).exists()) {
				//BGScript = "\n<script type='text/javascript'>function SetDimensions(){} "+audioScript+"\n</script>";
				BGScript = "\n<script type='text/javascript'>"+audioScript+"\n</script>";
				// Check for plain color page background
				pageBgColorValue = isPlainColorBackgroundExists("End");
				if (pageBgColorValue.equals("")) {
					if (currentBook.getBookOrientation() == 0) {
						if (isExportEnrichments) {
							pageBgPath = "back_P.png";
							File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
							String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
							UserFunctions.copyFiles(sourcePath.toString(), destPath);
						} else {
							pageBgPath = "FreeFiles/back_P.png";
						}
					} else {
						if (isExportEnrichments) {
							pageBgPath = "back.png";
							File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
							String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
							UserFunctions.copyFiles(sourcePath.toString(), destPath);
						} else {
							pageBgPath = "FreeFiles/back.png";
						}
					}
					HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
							"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>" +
							"\n<body onload='SetDimensions();'>"+BGScript+"\n<div id='bgImage' style='background-image:url("+pageBgPath+"); background-repeat: no-repeat;" +
							"-webkit-background-size:100%; width:100%; height:100%;left:0px; top:0px;position:absolute;'>\n</div>";
				} else {
					//Page bg plain color
					String hexPageBgColor = String.format("#%06X", (0xFFFFFF & Integer.parseInt(pageBgColorValue)));
					HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
							"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>" +
							"\n<body onload='SetDimensions();'>"+BGScript+"\n<div id='bgImage' style='background-color:"+hexPageBgColor+"; background-repeat: no-repeat;" +
							"-webkit-background-size:100%; width:100%; height:100%;left:0px; top:0px;position:absolute;'>\n</div>";
				}
			} else {
				//PageBg path exist
				imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), "PageBGEnd", "End");
				imageRotatedAngle = Integer.parseInt(imageRotationAngleandScaleMode.split("\\|")[0]);
				scaleMode = imageRotationAngleandScaleMode.split("\\|")[1];
				BGScript = getPageBackgroundScript(imageRotatedAngle, audioScript);
				String bgImageScaleOrTiledMode = getBgImageScaleorTiledString(pageBgPath, scaleMode, isExportEnrichments, htmlFilePath.getParent().toString());
				HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
						"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>\n<body onload='SetDimensions();'>"+BGScript+""+bgImageScaleOrTiledMode+"";
			}
		} else {
			pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/pageBG_ALL.png";
			if (!new File(pageBgPath).exists()) {
				// Check for plain color page background
				//BGScript = "\n<script type='text/javascript'>function SetDimensions(){} "+audioScript+"\n</script>";
				BGScript = "\n<script type='text/javascript'>"+audioScript+"\n</script>";
				pageBgColorValue = isPlainColorBackgroundExists("0");
				if (pageBgColorValue.equals("")) {
					pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/pageBG_"+pageNumber+".png";
					if (!new File(pageBgPath).exists()) {
						pageBgColorValue = isPlainColorBackgroundExists(String.valueOf(pageNumber));
						if (pageBgColorValue.equals("")) {
							if (currentBook.getBookOrientation() == 0) {
								if (isExportEnrichments) {
									pageBgPath = "page_P.png";
									File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
									String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
									UserFunctions.copyFiles(sourcePath.toString(), destPath);
								} else {
									pageBgPath = "FreeFiles/page_P.png";
								}
							} else {
								if (isExportEnrichments) {
									pageBgPath = "page.png";
									File sourcePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/FreeFiles/"+pageBgPath);
									String destPath = htmlFilePath.getParent().toString()+"/"+sourcePath.getName();
									UserFunctions.copyFiles(sourcePath.toString(), destPath);
								} else {
									pageBgPath = "FreeFiles/page.png";
								}
							}
							HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
									"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>" +
									"\n<body onload='SetDimensions();'>"+BGScript+"\n<div id='bgImage' style='background-repeat: no-repeat; background-position:center;" +
									"-webkit-background-size:100%; width:100%; height:100%;left:0px; top:0px;position:absolute;background-image:url("+pageBgPath+");'>\n</div>";
						} else {
							//Page bg plain color
							String hexPageBgColor = String.format("#%06X", (0xFFFFFF & Integer.parseInt(pageBgColorValue)));
							HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
									"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>" +
									"\n<body onload='SetDimensions();'>"+BGScript+"\n<div id='bgImage' style='background-repeat: no-repeat; background-position:center; " +
									"-webkit-background-size:100%; width:100%; height:100%;left:0px; top:0px;position:absolute;background-color:"+hexPageBgColor+";'>\n</div>";
						}
					} else {
						//Page bg path exist
						imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), "PageBG", String.valueOf(pageNumber));
						imageRotatedAngle = Integer.parseInt(imageRotationAngleandScaleMode.split("\\|")[0]);
						scaleMode = imageRotationAngleandScaleMode.split("\\|")[1];
						BGScript = getPageBackgroundScript(imageRotatedAngle, audioScript);
						String bgImageScaleOrTiledMode = getBgImageScaleorTiledString(pageBgPath, scaleMode, isExportEnrichments, htmlFilePath.getParent().toString());
						HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
								"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>\n<body onload='SetDimensions();'>"+BGScript+""+bgImageScaleOrTiledMode+"";
					}
				} else {
					//Page bg All plain color
					String hexPageBgColor = String.format("#%06X", (0xFFFFFF & Integer.parseInt(pageBgColorValue)));
					HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
							"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>" +
							"\n<body onload='SetDimensions();'>"+BGScript+"\n<div id='bgImage' style='background-repeat: no-repeat; background-position:center; " +
							"-webkit-background-size:100%; width:100%; height:100%;left:0px; top:0px;position:absolute;background-color:"+hexPageBgColor+";'>\n</div>";
				}
			} else {
				//Page bg All path exist
				imageRotationAngleandScaleMode = db.getBackgroundImageRotationAngleAndScaleModeinDB(currentBook.getBookID(), "PageBG", "0");
				imageRotatedAngle = Integer.parseInt(imageRotationAngleandScaleMode.split("\\|")[0]);
				scaleMode = imageRotationAngleandScaleMode.split("\\|")[1];
				BGScript = getPageBackgroundScript(imageRotatedAngle, audioScript);
				String bgImageScaleOrTiledMode = getBgImageScaleorTiledString(pageBgPath, scaleMode, isExportEnrichments, htmlFilePath.getParent().toString());
				HTMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<style type='text/css'>span.exclusion-newline:before {content: '\\A';white-space: pre;} "+tableStyles+"\n"+UserFunctions.fontFace+"\n" +
						"\n</style>\n<meta name=\"viewport\" content=\"width="+pageWidth+", height="+pageHeight+"\"/>\n<title>New Page</title>\n</head>\n<body onload='SetDimensions();'>"+BGScript+""+bgImageScaleOrTiledMode+"";
			}
		}

		for (int i = 0; i < objectList.size(); i++) {
			//System.out.println("objectList:"+objectList.get(i));
			HTMLObjectHolder htmlObject = (HTMLObjectHolder) objectList.get(i);
			String objType = htmlObject.getObjType();
			if (objType.equals("DrawnImage")) {
				String imgPath = getHtmlObjectPath(htmlObject.getObjContentPath(), isExportEnrichments, htmlFilePath.getParent().toString());
				int seqID = htmlObject.getObjSequentialId();
				HTMLString = HTMLString.concat("\n<img style='position:absolute; top:0px; left:0px;pointer-events:none;width:100%;z-index:"+seqID+";height:100%' src='"+imgPath+"'/>");
			} else if (objType.equals("Image")) {
				int x, y, width, height, x_px, y_px, width_px, height_px;
				if (currentBook.is_bStoreBook() && (enrichmentType.equals("duplicate") || enrichmentType.equals(""))) {
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				String imgPath = getHtmlObjectPath(htmlObject.getObjContentPath(), isExportEnrichments, htmlFilePath.getParent().toString());
				int seqID = htmlObject.getObjSequentialId();
				HTMLString = HTMLString.concat("\n<img style='position:absolute;z-index:"+seqID+"; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; height:"+height_px+"px;-webkit-transform:rotate(0deg);' src='"+imgPath+"'>\n</img>");
			} else if (objType.equals("YouTube")) {
				int x, y, width, height, x_px, y_px, width_px, height_px;
				if (currentBook.is_bStoreBook() && (enrichmentType.equals("duplicate") || enrichmentType.equals(""))) {
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				String urlPath = getHtmlObjectPath(htmlObject.getObjContentPath(), isExportEnrichments, htmlFilePath.getParent().toString());
				int seqID = htmlObject.getObjSequentialId();
				//HTMLString = HTMLString.concat("\n<iframe id='player' style='position:absolute;z-index:"+seqID+"; top:"+y+"px; left:"+x+"px; width:"+width+"px; height:"+height+"px' src='"+urlPath+"' frameborder='0' allowfullscreen>\n</iframe>");
				HTMLString = HTMLString.concat("\n<video id=\"player\" style='position:absolute;z-index:"+seqID+"; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; height:"+height_px+"px' controls=\"true\" autoplay=\"false\"> <source src='"+urlPath+"' type=\"video/mp4\"/></video>");
			} else if (objType.equals("IFrame")) {
				int x, y, width, height, x_px, y_px, width_px, height_px;
				if (currentBook.is_bStoreBook() && (enrichmentType.equals("duplicate") || enrichmentType.equals(""))) {
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				String urlPath = htmlObject.getObjContentPath();
				int seqID = htmlObject.getObjSequentialId();
				int uniqueID = htmlObject.getObjUniqueRowId();
				String loadingDivId = "LoadingIframe"+uniqueID;
				HTMLString = HTMLString.concat("\n<div style='position:absolute; z-index:"+seqID+"; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; height:"+height_px+"px; " +
						"overflow:auto; -webkit-overflow-scrolling:touch;background-color:#F5F5F5;'>\n<iframe onLoad=\"document.getElementById('"+loadingDivId+"').style.display='none';\"" +
						" frameborder='0' src='"+urlPath+"'></iframe>\n<div id='"+loadingDivId+"' style=\"top:40%;left:40%;position:absolute;\">Loading..\n</div>\n</div>");
			} else if (objType.equals("EmbedCode")) {
				int x, y, width, height, x_px, y_px, width_px, height_px;
				if (currentBook.is_bStoreBook() && (enrichmentType.equals("duplicate") || enrichmentType.equals(""))) {
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				String embedTextContent = htmlObject.getObjContentPath();
				int seqID = htmlObject.getObjSequentialId();
				HTMLString = HTMLString.concat("<div style='position:absolute; z-index:"+seqID+";border:1px solid;border-color:black; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; height:"+height_px+"px; overflow:auto;'>"+embedTextContent+"</div>");
			} else if (objType.equals("Audio")) {//Need to check for audio src bg img path
				int x, y, width, height, x_px, y_px, width_px, height_px;
				if (currentBook.is_bStoreBook() && (enrichmentType.equals("duplicate") || enrichmentType.equals(""))) {
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				//makeFileWorldReadable(htmlObject.getObjContentPath());
				String urlPath = getHtmlObjectPath(htmlObject.getObjContentPath(), isExportEnrichments, htmlFilePath.getParent().toString());
				//String urlPath = "file:///mnt/sdcard/Music/SBA_AUDIO.m4a";
				//String urlPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/SBA_AUDIO.m4a";
				int seqID = htmlObject.getObjSequentialId();
				int uniqueID = htmlObject.getObjUniqueRowId();
				String imageID = "Audio_Image_ID_"+uniqueID;
				String audioID = "MyAudio_ID_"+uniqueID;
				HTMLString = HTMLString.concat("\n<img id='"+imageID+"' style='position:absolute; z-index:"+seqID+"; top:"+y_px+"px; left:"+x_px+"px; width:"+width_px+"px; " +
						"height:"+height_px+"px' src='images/Default_Audio_Play.png' onClick=\"StartOrStop('"+urlPath+"', '"+imageID+"', '"+audioID+"');\" alt=\"Play Button\">" +
						"\n</img>\n<audio style='visibility:hidden' onEnded=\"FinishedPlaying('"+imageID+"');\" controls=\"true\" id='"+audioID+"'>\n</audio>");
			} else if (objType.equals("WebText") || objType.equals("TextSquare") || objType.equals("TextRectangle") || objType.equals("TextCircle") || objType.equals(Globals.objType_pageNoText) || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT)) {
				int x, y, width, height, dupWidth, dupHeight, dupWidth_px, dupHeight_px, x_px, y_px, width_px, height_px;
				String htmlTextContent = htmlObject.getObjContentPath();
				if (currentBook.is_bStoreBook() && (enrichmentType.equals("duplicate") || enrichmentType.equals(""))) {
					dupWidth = htmlObject.getObjWidth();
					dupHeight = htmlObject.getObjHeight();
					dupWidth_px = Globals.getPixelValue(dupWidth, context);
					dupHeight_px = Globals.getPixelValue(dupHeight, context);
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
					htmlTextContent = htmlTextContent.replace("width: "+dupWidth_px+"px; height: "+dupHeight_px+"px;", "width: "+width_px+"px; height: "+height_px+"px;");
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}
				int seqID = htmlObject.getObjSequentialId();
				htmlTextContent = htmlTextContent.replace("contenteditable=\"true\"", "contenteditable=\"false\"");
				//Replace div with br and &nsbp; with nil
				htmlTextContent = htmlTextContent.replace("&nbsp;", "");
				htmlTextContent = htmlTextContent.replace("<div><br></div>", "");
				if (objType.equals(Globals.objType_pageNoText)) {
					int pno = pageNumber - 1;
					htmlTextContent = htmlTextContent.concat(pno+"</div>");
				}
				HTMLString = HTMLString.concat("\n<div width='"+width_px+"' height='"+height_px+"' style='top:"+y_px+"px;left:"+x_px+"px;position:absolute;z-index:"+seqID+";visibility:show;'>"+htmlTextContent+"\n</div>");
			} else if (objType.equals("WebTable")) {
				int x, y, width, height, dupWidth, dupHeight, dupWidth_px, dupHeight_px, x_px, y_px, width_px, height_px;
				String htmlTextContent = htmlObject.getObjContentPath();
				if (currentBook.is_bStoreBook() && (enrichmentType.equals("duplicate") || enrichmentType.equals(""))) {
					dupWidth = htmlObject.getObjWidth();
					dupHeight = htmlObject.getObjHeight();
					dupWidth_px = Globals.getPixelValue(dupWidth, context);
					dupHeight_px = Globals.getPixelValue(dupHeight, context);
					x_px = (int) (htmlObject.getObjXPos()/scaleX);
					y_px = (int) (htmlObject.getObjYPos()/scaleY);
					width_px = (int) (htmlObject.getObjWidth()/scaleX);
					height_px = (int) (htmlObject.getObjHeight()/scaleY);
					//x_px = Globals.getPixelValue(x, context);
					//y_px = Globals.getPixelValue(y, context);
					//width_px = Globals.getPixelValue(width, context);
					//height_px = Globals.getPixelValue(height, context);
					htmlTextContent = htmlTextContent.replace("width: "+dupWidth_px+"px; height: "+dupHeight_px+"px;", "width: "+width_px+"px; height: "+height_px+"px;");
				} else {
					x = htmlObject.getObjXPos();
					y = htmlObject.getObjYPos();
					width = htmlObject.getObjWidth();
					height = htmlObject.getObjHeight();
					x_px = Globals.getPixelValue(x, context);
					y_px = Globals.getPixelValue(y, context);
					width_px = Globals.getPixelValue(width, context);
					height_px = Globals.getPixelValue(height, context);
				}

				int seqID = htmlObject.getObjSequentialId();
				htmlTextContent = htmlTextContent.replace("contenteditable=\"true\"", "contenteditable=\"false\"");
				HTMLString = HTMLString.concat("\n<div style='width:"+width_px+"px; height:"+height_px+"px; top:"+y_px+"px;left:"+x_px+"px;position: absolute;z-index:"+seqID+";visibility: show;margin:0px;'>"+htmlTextContent+"\n</div>");
			}
		}
		HTMLString = HTMLString.concat("\n</body>\n</html>");

		try {
			FileWriter fileWriter = new FileWriter(htmlFilePath);
			fileWriter.write(HTMLString);
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void makeFileWorldReadable(String path){
		File f = new File(path);
		if (f.exists()) {
			try {
				Runtime.getRuntime().exec("chmod o+r"+path);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Filter the path to get html object path
	 * @param path
	 * @param isExportEnrichments 
	 * @param htmlFileDirPath 
	 * @return
	 */
	private String getHtmlObjectPath(String path, boolean isExportEnrichments, String htmlFileDirPath) {
		if (path.contains("data/data") && !isExportEnrichments) {
			String[] pathArr = path.split("/");
			String imgPath = pathArr[pathArr.length - 2]+"/"+pathArr[pathArr.length - 1];
			return imgPath;
		} else if (path.contains("data/data") && isExportEnrichments) {
			File sourcePath = new File(path);
			File destPath = new File(htmlFileDirPath+"/"+sourcePath.getName());
			UserFunctions.copyFiles(sourcePath.toString(), destPath.toString());
			File filePath = new File(path);
			return filePath.getName();
		}
		return path;
	}

	/**
	 * Check isPlainColorBackground exist
	 * @param pNumber
	 * @return
	 */
	public String isPlainColorBackgroundExists(String pNumber){
		String objType;
		int pageNo = 0;
		if (!pNumber.equals("End")) {
			pageNo = Integer.parseInt(pNumber);
		}
		if (pageNo == 1) {
			objType = "PageColor";
		} else if (pNumber.equals("End")) {
			objType = "PageColorEnd";
		} else {
			objType = "PageColor";
		}
		String bgColorValue = db.getPlainBackgroundColorFromDB(currentBook.getBookID(), objType, pNumber);
		return bgColorValue;
	}

	/**
	 * Get the page background script
	 * @param rotationAngle
	 * @param audioJScript
	 * @return
	 */
	public String getPageBackgroundScript(int rotationAngle, String audioJScript){
		String BGScript = null;
		if (rotationAngle == 0 || rotationAngle == 180) {
			BGScript = "\n<script type='text/javascript'>\nfunction SetDimensions(){"+
					"\nvar BGDiv = document.getElementById('bgImage');"+
					"\nBGDiv.style.webkitTransform ='rotate("+rotationAngle+"deg)';"+
					"\n}"+audioJScript+"\n</script>";
		} else if (rotationAngle == 90 || rotationAngle == -90) {
			if (currentBook.getBookOrientation() == 0) {
				BGScript = "\n<script type='text/javascript'> \nfunction SetDimensions(){"+
						"\nvar height = document.body.scrollHeight; var BGDiv = document.getElementById('bgImage');BGDiv.style.height = height+'px'; BGDiv.style.width = height + 'px'; var width = document.body.clientWidth;"+
						"\nvar scaleX=0; var translateX=0;"+
						"\nif(height<width){"+
						"\nscaleX=height/width;"+
						"\ntranslateX=(width - height)/2;}"+
						"\nelse{"+
						"\nscaleX=width/height;"+
						"\ntranslateX=(height-width)/2;}"+
						"\nBGDiv.style.webkitTransform ='translate('+-translateX+'px,'+0+'px ) scale('+scaleX+',1.0) rotate("+rotationAngle+"deg)';}"+
						"\n"+audioJScript+"\n</script>";
			} else {
				BGScript = "\n<script type='text/javascript'> \nfunction SetDimensions(){"+
						"\nvar width = document.body.scrollWidth; var BGDiv = document.getElementById('bgImage');BGDiv.style.width = width+'px'; BGDiv.style.height = width + 'px'; var height = document.body.clientHeight;"+
						"\nvar scaleY=0; var translateY=0;"+
						"\nif(width<height){"+
						"\nscaleY=width/height;"+
						"\ntranslateY=(height - width)/2;}"+
						"\nelse{"+
						"\nscaleY=height/width;"+
						"\ntranslateY=(width-height)/2;}"+
						"\nBGDiv.style.webkitTransform ='translate('+0+'px ,'+-translateY+'px) scale(1.0,'+scaleY+') rotate("+rotationAngle+"deg)';}"+
						"\n"+audioJScript+"\n</script>";
			}
		}
		return BGScript;
	}

	/**
	 * get the bgimage scaled or tiled string
	 * @param pageBgPath
	 * @param scaleMode
	 * @param isExportEnrichments 
	 * @param htmlFileDirPath 
	 * @return
	 */
	public String getBgImageScaleorTiledString(String pageBgPath, String scaleMode, boolean isExportEnrichments, String htmlFileDirPath){
		String BgImage;
		String path = getHtmlObjectPath(pageBgPath, isExportEnrichments, htmlFileDirPath);
		if (scaleMode.equals("Tiled")) {
			BgImage = "\n<div id='bgImage' style ='background-image:url("+path+"); width:100%; height:100%; left:0px; top:0px;position:absolute;'>\n</div>";
		} else {
			BgImage = "\n<div id='bgImage' style='background-image:url("+path+");  background-repeat: no-repeat; -webkit-background-size:100%; width:100%; height:100%;left:0px; top:0px; position:absolute;'>\n</div>";
		}
		return BgImage;
	}
}
