package com.semanoor.source_sboookauthor;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.VideoView;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.ClickedFilePath;
import com.semanoor.manahij.R;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Krishna on 23-09-2015.
 */
public class HyperLinkDialog {
    private Context Context;
    private BookViewReadActivity bookViewReadActivity;
    //EditText hyperlink_et;
    WebView helpWebview;
    private EditText hyperlink_et;
    private boolean isHyperlinkUrl = true;
    public boolean hyperlink_image = true;
    String currentFilePath;
    ArrayList<ClickedFilePath> selectedFiles = new ArrayList<ClickedFilePath>();

    public HyperLinkDialog(Context context, WebView webView) {
        Context=context;
        helpWebview=webView;
    }




    public void showHyperLinkDialog() {
        {
            // HyperLinkDialog mindMapDialog = new HyperLinkDialog(((BookViewActivity) context));
            //  getAndSetSelectionIsActive();
            hyperlink_image = false;
            final Dialog hyperlinkPopup = new Dialog(Context);
            View iv_image = LayoutInflater.from(Context).inflate(R.layout.hyperlink_dialog, null);
            hyperlinkPopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
            hyperlinkPopup.setContentView(iv_image);
            final RelativeLayout layoutt = (RelativeLayout) hyperlinkPopup.findViewById(R.id.first_layout);
            final RelativeLayout layout = (RelativeLayout) hyperlinkPopup.findViewById(R.id.layout);
            final RelativeLayout layout1 = (RelativeLayout) hyperlinkPopup.findViewById(R.id.layout1);
            final SegmentedRadioButton segtext = (SegmentedRadioButton) hyperlinkPopup.findViewById(R.id.segment_text);
            final Button save = (Button) hyperlinkPopup.findViewById(R.id.btn_save);
            hyperlink_et = (EditText) hyperlinkPopup.findViewById(R.id.et);

            isHyperlinkUrl = true;
            segtext.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    if (group == segtext) {

                        if (checkedId == R.id.segUrl) {
                            isHyperlinkUrl = true;
                            hyperlink_image = false;

                            layout.setVisibility(View.VISIBLE);
                            layout1.setVisibility(View.GONE);
                            hyperlink_et.setHint(Context.getResources().getString(R.string.enter_url));
                            hyperlink_et.setInputType(InputType.TYPE_CLASS_TEXT);

                        } else if (checkedId == R.id.segPageNo) {
                            isHyperlinkUrl = false;
                            hyperlink_image = false;

                            layout.setVisibility(View.VISIBLE);
                            layout1.setVisibility(View.GONE);
                            hyperlink_et.setHint(Context.getResources().getString(R.string.enter_pageNo));
                            hyperlink_et.setInputType(InputType.TYPE_CLASS_NUMBER);

                        } else if (checkedId == R.id.segGallery) {
                            layout.setVisibility(View.GONE);
                            layout1.setVisibility(View.VISIBLE);

                            final Button bt = (Button) hyperlinkPopup.findViewById(R.id.gallery);
                            bt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    hyperlinkPopup.dismiss();
                                    String fileType = "Image";
                                    // String objFormat = ".png";

                                    final Dialog hyperlinkPopup1 = new Dialog(Context);
                                    hyperlinkPopup1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
                                    View iv_image = LayoutInflater.from(Context).inflate(R.layout.files_manager, null);
                                    hyperlinkPopup1.setContentView(iv_image);
                                    hyperlinkPopup1.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.5), (int)(Globals.getDeviceHeight()/1.5));
                                    final ListView listView = (ListView) hyperlinkPopup1.findViewById(R.id.listView1);
                                    final Button btn_back = (Button) hyperlinkPopup1.findViewById(R.id.btnBack);
                                    currentFilePath = Globals.TARGET_BASE_FILE_PATH + "UserLibrary";
                                    FilesAndFoldersinHyperLink(currentFilePath, fileType);
                                    listView.setAdapter(new FileImageAdapter(((BookViewActivity) Context), selectedFiles, fileType));
                                    hyperlinkPopup1.show();

                                    //listView.setAdapter(new FileManagerAdapter(context, currentFilePath, objType));
                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                        @Override
                                        public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                                            String fileType = "Image";
                                            ClickedFilePath listedFiles = selectedFiles.get(position);
                                            if (listedFiles.isFolderSelected() && !listedFiles.isFile()) {
                                                //String[]split=listedFiles.getFolderPath().split("/");
                                                currentFilePath = listedFiles.getFolderPath();
                                                btn_back.setText(listedFiles.getFolderTitle());
                                                FilesAndFoldersinHyperLink(currentFilePath, fileType);
                                                listView.setAdapter(new FileImageAdapter(((BookViewActivity) Context), selectedFiles, fileType));
                                                //changeHyperlink(currentFilePath);
                                            } else if (listedFiles.isFile() && !listedFiles.isFolderSelected()) {
                                                isHyperlinkUrl = false;
                                                hyperlink_image = true;
                                                int randomNoToRefresh = new Random().nextInt();
                                                //final String quiz_imgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/images/" + "" + quizImageFileName + ".png";
                                                // hyperLinkForImage("image_path://" + listedFiles.getFolderPath());
                                                hyperLinkForImage("hyperfile://" + listedFiles.getFolderPath() + "?+" + randomNoToRefresh);
                                                hyperlinkPopup1.dismiss();
                                            }

                                            if (!btn_back.getText().toString().equals("UserLibrary")) {
                                                btn_back.setCompoundDrawablesWithIntrinsicBounds(R.drawable.arrownext, 0, 0, 0);
                                            } else {
                                                btn_back.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                            }

                                        }
                                    });
                                    btn_back.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String fileType = "Image";
                                            if (!currentFilePath.equals(Globals.TARGET_BASE_FILE_PATH + "UserLibrary")) {
                                                String filePath = currentFilePath.replace("/" + btn_back.getText(), "");
                                                currentFilePath = filePath;
                                                String[] split = currentFilePath.split("/");
                                                btn_back.setText(split[split.length - 1]);
                                                FilesAndFoldersinHyperLink(currentFilePath, fileType);
                                                listView.setAdapter(new FileImageAdapter(((BookViewActivity) Context), selectedFiles, "Image"));
                                                if (btn_back.getText().toString().equals("UserLibrary")) {
                                                    btn_back.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                                }
                                            }
                                        }
                                    });
                                }

                            });

                            Button btn_video = (Button) hyperlinkPopup.findViewById(R.id.video);
                            btn_video.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String fileType = "YouTube";

                                    final Dialog hyperlink_video = new Dialog(Context);
                                    hyperlink_video.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
                                    View iv_image = LayoutInflater.from(Context).inflate(R.layout.files_manager, null);
                                    hyperlink_video.setContentView(iv_image);
                                    hyperlink_video.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.5), (int)(Globals.getDeviceHeight()/1.5));
                                    final ListView listView = (ListView) hyperlink_video.findViewById(R.id.listView1);
                                    final Button btn_back = (Button) hyperlink_video.findViewById(R.id.btnBack);
                                    currentFilePath = Globals.TARGET_BASE_FILE_PATH + "UserLibrary";
                                    FilesAndFoldersinHyperLink(currentFilePath, fileType);
                                    listView.setAdapter(new FileImageAdapter(((BookViewActivity) Context), selectedFiles, "YouTube"));
                                    hyperlink_video.show();

                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            ClickedFilePath listedFiles = selectedFiles.get(position);
                                            String fileType = "YouTube";

                                            if (listedFiles.isFolderSelected() && !listedFiles.isFile()) {
                                                //String[]split=listedFiles.getFolderPath().split("/");
                                                currentFilePath = listedFiles.getFolderPath();
                                                btn_back.setText(listedFiles.getFolderTitle());
                                                FilesAndFoldersinHyperLink(currentFilePath, fileType);
                                                listView.setAdapter(new FileImageAdapter(((BookViewActivity) Context), selectedFiles, fileType));

                                            } else if (listedFiles.isFile() && !listedFiles.isFolderSelected()) {
                                                int randomNoToRefresh = new Random().nextInt();
                                                // hyperlinkForVideo("video_path://" + listedFiles.getFolderPath());
                                                hyperlinkForVideo("hyperfile://" + listedFiles.getFolderPath() + "?+" + randomNoToRefresh);
                                                hyperlinkPopup.dismiss();
                                                hyperlink_video.dismiss();
                                            }
                                        }
                                    });
                                    btn_back.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String fileType = "YouTube";
                                            if (!currentFilePath.equals(Globals.TARGET_BASE_FILE_PATH + "UserLibrary")) {
                                                String filePath = currentFilePath.replace("/" + btn_back.getText(), "");
                                                currentFilePath = filePath;
                                                String[] split = currentFilePath.split("/");
                                                btn_back.setText(split[split.length - 1]);
                                                FilesAndFoldersinHyperLink(currentFilePath, fileType);
                                                listView.setAdapter(new FileImageAdapter(((BookViewActivity) Context), selectedFiles, "YouTube"));
                                                if (btn_back.getText().toString().equals("UserLibrary")) {
                                                    btn_back.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                            Button btn_audio = (Button) hyperlinkPopup.findViewById(R.id.audio);
                            btn_audio.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String fileType = "Audio";

                                    final Dialog hyperlink_audio = new Dialog(Context);
                                    hyperlink_audio.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
                                    View iv_image = LayoutInflater.from(Context).inflate(R.layout.files_manager, null);
                                    hyperlink_audio.setContentView(iv_image);
                                    hyperlink_audio.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.5), (int)(Globals.getDeviceHeight()/1.5));
                                    final ListView listView = (ListView) hyperlink_audio.findViewById(R.id.listView1);
                                    final Button btn_back = (Button) hyperlink_audio.findViewById(R.id.btnBack);
                                    currentFilePath = Globals.TARGET_BASE_FILE_PATH + "UserLibrary";
                                    FilesAndFoldersinHyperLink(currentFilePath, fileType);
                                    listView.setAdapter(new FileImageAdapter(((BookViewActivity) Context), selectedFiles, fileType));
                                    hyperlink_audio.show();

                                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            ClickedFilePath listedFiles = selectedFiles.get(position);
                                            String fileType = "Audio";

                                            if (listedFiles.isFolderSelected() && !listedFiles.isFile()) {
                                                //String[]split=listedFiles.getFolderPath().split("/");
                                                currentFilePath = listedFiles.getFolderPath();
                                                btn_back.setText(listedFiles.getFolderTitle());
                                                FilesAndFoldersinHyperLink(currentFilePath, fileType);
                                                listView.setAdapter(new FileImageAdapter(((BookViewActivity) Context), selectedFiles, fileType));

                                            } else if (listedFiles.isFile() && !listedFiles.isFolderSelected()) {
                                                int randomNoToRefresh = new Random().nextInt();
                                                // hyperlinkForAudio("audio_path://" + listedFiles.getFolderPath());
                                                hyperlinkForAudio("hyperfile://" + listedFiles.getFolderPath() + "?+" + randomNoToRefresh);
                                                hyperlinkPopup.dismiss();
                                                hyperlink_audio.dismiss();
                                            }
                                        }
                                    });
                                    btn_back.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String fileType = "Audio";
                                            if (!currentFilePath.equals(Globals.TARGET_BASE_FILE_PATH + "UserLibrary")) {
                                                String filePath = currentFilePath.replace("/" + btn_back.getText(), "");
                                                currentFilePath = filePath;
                                                String[] split = currentFilePath.split("/");
                                                btn_back.setText(split[split.length - 1]);
                                                FilesAndFoldersinHyperLink(currentFilePath, fileType);
                                                listView.setAdapter(new FileImageAdapter(((BookViewActivity) Context), selectedFiles, "Audio"));
                                                if (btn_back.getText().toString().equals("UserLibrary")) {
                                                    btn_back.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                                }
                                            }
                                        }
                                    });

                                }
                            });
                        }
                    }
                }
            });

            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!hyperlink_et.getText().toString().equals("")) {
                        boolean pageExists = false;
                        int randomNoToRefresh = new Random().nextInt();
                        if (isHyperlinkUrl) {
                            changeHyperlink("hyperurl://" + hyperlink_et.getText().toString() + "?+" + randomNoToRefresh);
                            hyperlinkPopup.dismiss();
                        } else {
                            int pgno = Integer.parseInt(hyperlink_et.getText().toString()) + 1;
                            if (pgno<((BookViewActivity) Context).currentBook.getTotalPages()) {
                                pageExists = true;
                            }
                            if (pageExists) {
                                String enrBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ((BookViewActivity) Context).currentBook.get_bStoreID() + "Book" + "/" + pgno + ".htm";
                                //hyperLinkforPageNo("hyperlink_page://" + enrBookPath);
                                hyperLinkforPageNo("hyperpage://" + enrBookPath + "?+" + randomNoToRefresh);
                                hyperlinkPopup.dismiss();
                            }else{
                                UserFunctions.DisplayAlertDialog(Context, "Please enter valid page number.", R.string.page_number);
                            }
                        }
                    }
                }
            });
            hyperlinkPopup.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {

                }
            });

            hyperlinkPopup.show();
        }
    }



    public void changeHyperlink(String _url) {
        String script = "javascript:addHyperLink('" + _url + "');";             //javascript:addHyperLink('hyperlink://www.google.com');
        loadJSScript(script, helpWebview);
    }

    public void hyperLinkforPageNo(String _pageno) {
        String script = "javascript:addHyperLink('" + _pageno + "');";
        loadJSScript(script, helpWebview);
    }

    public void hyperLinkForImage(String _image) {
        String script = "javascript:addHyperLink('" + _image + "');";
        loadJSScript(script, helpWebview);
    }

    public void hyperlinkForVideo(String _video) {
        String script = "javascript:addHyperLink('" + _video + "');";
        loadJSScript(script, helpWebview);
    }

    public void hyperlinkForAudio(String _audio) {
        String script = "javascript:addHyperLink('" + _audio + "');";
        loadJSScript(script, helpWebview);
    }

    public static void loadJSScript(final String script, final WebView view) {
        view.post(new Runnable() {

            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    view.evaluateJavascript(script, null);
                } else {
                    view.loadUrl(script);
                }

            }
        });
    }

    private void FilesAndFoldersinHyperLink(String filePath, String fileType) {
        File f = new File(filePath);
        File listFiles[] = f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {

                return !file.isHidden();
            }
        });
        //selectedFiles = new ArrayList<ClickedFilePath>();
        if (selectedFiles.size() > 0) {
            selectedFiles.clear();
        }
        ClickedFilePath file;
        for (int i = 0; i < listFiles.length; i++) {
            String fileName = listFiles[i].getName();
            String Path = listFiles[i].getPath();
            if (listFiles[i].isDirectory()) {
                file = new ClickedFilePath(Context);
                file.setFolderSelected(true);
                file.setFolderPath(Path);
                file.setFile(false);
                file.setFolderTitle(fileName);
                selectedFiles.add(file);
            } else {
                if (!listFiles[i].isHidden()) {
                    if (fileType.equals("Image")) {
                        if (fileName.contains(".jpg") || fileName.contains(".png") || fileName.contains(".gif")) {
                            file = new ClickedFilePath(Context);
                            file.setFolderSelected(false);
                            file.setFolderPath(Path);
                            file.setFile(true);
                            file.setFolderTitle(fileName);
                            selectedFiles.add(file);
                        }
                    } else if (fileType.equals("YouTube")) {
                        //if(listFiles[i].isHidden()) {
                        if (fileName.contains(".mp4") || fileName.contains(".3gp") || fileName.contains(".mkv") || fileName.contains(".avi") || fileName.contains(".flv")) {
                            file = new ClickedFilePath(Context);
                            file.setFolderSelected(false);
                            file.setFolderPath(Path);
                            file.setFile(true);
                            file.setFolderTitle(fileName);
                            selectedFiles.add(file);
                        }
                        //}
                    } else if (fileType.equals("Audio")) {
                        if (fileName.contains(".mp3") || fileName.contains(".m4a")) {
                            file = new ClickedFilePath(Context);
                            file.setFolderSelected(false);
                            file.setFolderPath(Path);
                            file.setFile(true);
                            file.setFolderTitle(fileName);
                            selectedFiles.add(file);
                        }
                    }
                }
            }

        }

    }

    public void imageDialogWindow(String url,boolean image,boolean webView_Type) {
        View iv_image = LayoutInflater.from(Context).inflate(R.layout.preview_image, null);
        ImageView iv = (ImageView) iv_image.findViewById(R.id.imageView2);
        WebView webView=(WebView)iv_image.findViewById(R.id.webView);
        Button btn_back=(Button)iv_image.findViewById(R.id.btn_back);

        iv.setBackgroundResource(0);
        String	image_paths;
        final ProgressBar progress=(ProgressBar)iv_image.findViewById(R.id.progressBar);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                progress.setVisibility(View.INVISIBLE);
            }
        });

        if(Context instanceof BookViewReadActivity) {
            image_paths = url.replace("file:///data/data/com.semanoor.SboookAuthor/files/books/1/hyperfile://", "");
        }else {
            image_paths = url.replace("file:///android_asset/hyperfile://", "");
        }

        if(webView_Type) {
            webView.setVisibility(View.VISIBLE);
            progress.setVisibility(View.VISIBLE);
            iv.setVisibility(View.INVISIBLE);
            if (url.startsWith("http:")){
                webView.loadUrl(url);
            }else{
                String[] split = url.split("///");
                if (split.length>1){
                    File file = new File(split[1]);
                    String[] split1 = url.split("/");
                    String filePath = "";
                    for (int i = 0;i<split1.length-1;i++){
                        String str = split1[i];
                        filePath = filePath+str+"/";
                    }
                    String srcString = UserFunctions.decryptFile(file);
                    webView.loadDataWithBaseURL(filePath, srcString, "text/html", "utf-8", null);
                }
            }
        }else if(image){
            webView.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.INVISIBLE);
            iv.setVisibility(View.VISIBLE);
            final Bitmap bitmap = BitmapFactory.decodeFile(image_paths);
            Bitmap b=bitmap.createScaledBitmap(bitmap, (int)(Globals.getDeviceWidth()/1.5), (int)(Globals.getDeviceHeight()/1.5), false);
            iv.setImageBitmap(b);
        }


        final Dialog popup=new Dialog(Context);
        popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popup.getWindow().setBackgroundDrawable(new ColorDrawable(Context.getResources().getColor(R.color.semi_transparent)));
        // hyperlinkPopupp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
       // popup.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.setContentView(iv_image);
        popup.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.5), (int)(Globals.getDeviceHeight()/1.5));
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
        popup.show();

    }

    public void videoHyperLink(String url){
        final Dialog hyperlinkPopupp = new Dialog(Context);
        View iv_image = LayoutInflater.from(Context).inflate(R.layout.preview_video, null);
        hyperlinkPopupp.requestWindowFeature(Window.FEATURE_NO_TITLE);
       // hyperlinkPopupp.getWindow().setBackgroundDrawable(new ColorDrawable(Context.getResources().getColor(R.color.semi_transparent)));
       // hyperlinkPopupp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
        hyperlinkPopupp.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.5), (int)(Globals.getDeviceHeight()/1.5));
        hyperlinkPopupp.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        hyperlinkPopupp.setContentView(iv_image);

        VideoView iv_preview = (VideoView)hyperlinkPopupp. findViewById(R.id.videoView);
        Button btn_back=(Button)hyperlinkPopupp.findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hyperlinkPopupp.dismiss();
            }
        });
        String video_paths;

        if(Context instanceof BookViewReadActivity){
            video_paths = url.replace("file:///data/data/com.semanoor.SboookAuthor/files/books/1/video_path://", "file:///");
        }else {
            video_paths = url.replace("file:///android_asset/video_path://", "file:///");
        }


        iv_preview.setVideoPath(video_paths);
        iv_preview.start();
        hyperlinkPopupp.show();
    }

    public void audioHyperLink(String url){
        {
            View iv_image = LayoutInflater.from(Context).inflate(R.layout.preview_audio, null);
            final Button play = (Button) iv_image.findViewById(R.id.btn_play);
            final SeekBar audio_progress=(SeekBar)iv_image.findViewById(R.id.seekBar);
            ImageView iv= (ImageView)iv_image. findViewById(R.id.imageView);

            final PopupWindow popup=new PopupWindow();
            popup.setContentView(iv_image);
           // popup.setWidth(Globals.getDeviceIndependentPixels(500, Context));
           // popup.setHeight(Globals.getDeviceIndependentPixels(500, Context));
            popup.setWidth((int)(Globals.getDeviceWidth()/1.5));
            popup.setHeight((int)(Globals.getDeviceHeight()/1.5));
            popup.setFocusable(true);
            popup.setBackgroundDrawable(new ColorDrawable(R.color.white));
            popup.showAtLocation(iv_image, Gravity.CENTER, 0, 0);
            popup.setOutsideTouchable(false);
            final MediaPlayer mp = new MediaPlayer();
            String audio_path;
            if(Context instanceof BookViewReadActivity){
                 audio_path =	url.replace("file:///data/data/com.semanoor.SboookAuthor/files/books/1/audio_path://","file:///");
            }else {
                 audio_path = url.replace("file:///android_asset/audio_path://", "file:///");
            }

            popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    popup.dismiss();
                    mp.stop();
                }
            });

            try {
                mp.setDataSource(audio_path);
                mp.prepare();
                audio_progress.setMax(mp.getDuration());
                //audio_progress.setProgress(mp.getDuration());
                //audio_progress.setMax(mp.get);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    audio_progress.setProgress(mediaPlayer.getCurrentPosition());
                    // String p=context.getResources().getString(R.string.play).toString();
                    if (play.getText().toString().equals(Context.getResources().getString(R.string.stop).toString())) {
                        play.setText(Context.getResources().getString(R.string.play));
                    }

                }
            });

            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!mp.isPlaying()) {
                        //mp.getSelectedTrack(mp.getCurrentPosition());
                        mp.start();
                        audio_progress.setProgress(mp.getCurrentPosition());
                        //audio_progress.setMax(mp.getDuration());
                        play.setSelected(true);
                        play.setText(Context.getResources().getString(R.string.stop));
                        audio_progress.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                audio_progress.setProgress(mp.getCurrentPosition());
                            }
                        }, 1000);
                    } else {
                        play.setText(Context.getResources().getString(R.string.play));
                        mp.pause();
                        play.setSelected(false);
                        audio_progress.setProgress(mp.getCurrentPosition());
                        //audio_progress.setMax(mp.getDuration());

                    }
                    // mp.start();
                }
            });
            audio_progress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, final int i, boolean fromUser) {
                    //mp.seekTo(i);
                    audio_progress.setProgress(i);
                    audio_progress.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            audio_progress.setProgress(mp.getCurrentPosition());
                        }
                    }, 1000);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int s = seekBar.getProgress();
                    mp.seekTo(s);

                }
            });

        }
    }
}
