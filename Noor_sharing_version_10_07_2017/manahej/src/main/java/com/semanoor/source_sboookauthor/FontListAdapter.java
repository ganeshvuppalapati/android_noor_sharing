package com.semanoor.source_sboookauthor;

import java.io.File;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class FontListAdapter extends BaseAdapter {

	private BookViewActivity context;
	private CustomWebRelativeLayout customWebView;
	
	public FontListAdapter(BookViewActivity _context, CustomWebRelativeLayout _customWebView){
		this.context = _context;
		this.customWebView = _customWebView;
	}
	
	@Override
	public int getCount() {
		//return Globals.fontnames.length;
		return UserFunctions.m_fontNames.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public static class ViewHolder{
		public CheckBox checkBox;
		public TextView txtView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		if (view == null) {
			view = context.getLayoutInflater().inflate(R.layout.inflate_checklist, null);
			holder=new ViewHolder();
			holder.checkBox=(CheckBox) view.findViewById(R.id.checkBox1);
			holder.txtView=(TextView) view.findViewById(R.id.textView1);
			view.setTag(holder);
		} else {
			holder=(ViewHolder)view.getTag();
		}
		String fontname = UserFunctions.m_fontNames.get(position);
		String fontPath = UserFunctions.m_fontPaths.get(position);
		holder.txtView.setText(fontname);
		holder.txtView.setTextColor(Color.BLACK);
		File fontFilePath = new File(fontPath);
		Typeface tface = Typeface.createFromFile(fontFilePath);
		holder.txtView.setTypeface(tface);
		if (customWebView.fontFamily.equals(fontname) || customWebView.fontFamily.equals("'"+fontname+"'")) {
			holder.checkBox.setChecked(true);
		} else {
			holder.checkBox.setChecked(false);
		}
		return view;
	}

}
