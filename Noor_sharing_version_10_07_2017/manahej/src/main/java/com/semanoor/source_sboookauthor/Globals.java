package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.View;

import com.semanoor.manahij.R;

import java.util.ArrayList;

public class Globals {
    private static Globals instance;

    public static String sbaPackageName = "com.semanoor.SboookAuthor";
    public static String manahijPackageName = "com.semanoor.manahij";

    private static String currentProjPackageName;

    public static String googleImgSearchId = "010167618665075241078:npbixwzfsii";
    public static String googleYoutubeSearchKey = "AIzaSyCqo5QHbQG1Z2JzgelmPUZ9S7PrR7OGeN4";

    private static int deviceHeight;
    private static int deviceWidth;

    public static String STORE_TABS_SECTION_NUMBER = "store_tabs";
    public static String recent_StoreList = "store_list";
    public static String fav_StoreList = "fav";

    public static int oldCurriculumStore = 1;
    public static int newCurriculumStore = 3;
    public static int eLessonStore = 2;
    public static int quizStore = 4;
    public static int rzooomStore = 6;
    public static int mindmapStore = 5;
    public static int mediaStore = 7;
    public static int pdfStore = 8;

    public static int myMediaCatId = 9;
    public static int mindMapCatId = 8;

    private static boolean ISGDRIVEMODE;
    public static String oldCurriculumWebServiceURL = "http://school.nooor.com/sboook/IPadSSboookService.asmx";
    public static String manahijNewCurriculumWebServiceURL = "http://www.nooor.com/sboook/IPadSSboookService.asmx";
    public static String sbaNewCurriculumWebServiceURL = "http://demo.semanoor.com/sboook/IPadSSboookService.asmx";
    public static String templatesUrl = "http://www.nooor.com/IJST/NooorTemplates/templates.json";
    //public static String sbaNewCurriculumWebServiceURL = "http://sem7.nooor.com/semaenrichments/IPadSSboookService.asmx";// Bahrain Store

    public static String manahijSubscriptionURL = "http://www.nooor.com/SBoook/SBookAndroidService.asmx";
    //public static String manahijSubscriptionURL = "http://kaladi.semanoorsoft.com/SBOOK2014/SBookIOSService.asmx";
    public static String sbaSubscriptionURL = "http://demo.semanoor.com/SBoook/SBookAndroidService.asmx";
    //public static String sbaSubscriptionURL = "http://sem7.nooor.com/semaenrichments/IPadSSboookService.asmx";// Bahrain Store

    public static String storeJsonUrl = "http://www.nooor.com/stores/storeList-live.json";
    public static String demoStoreJsonUrl = "http://www.nooor.com/stores/storeList-demo.json";

    //public static String sbaNewCurriculumWebServiceURL ="http://kaladik.semanoortech.com/SBOOK2014/IPadSSboookService.asmx";
    public static String newCurriculumWebServiceURL; //newCurriculumWebServiceURL //Live http://www.nooor.com/semaenrichments/IPadSSboookService.asmx
    public static String subscriptionWebserviceURL;
    public static String storeUrl;
    public static String encryptFilePassWord = "SeMaNoOrFiLeS";

    //public static String eLessonWebServiceURL = "http://www.nooor.com/eLessons/webservices/SemaiosService.asmx";
    public static String eLessonWebServiceURL = "http://demo.semanoor.com/SemaEbagsWithNewUI/webservices/SemaiosService.asmx";
    public static String quizWebServiceURL = "http://school.nooor.com/ipadadmin/SboookService.asmx";
    //public static String rzooomWebServiceURL = "https://rzoom.com/iphoneservice.asmx";
    public static String rzooomWebServiceURL = "https://www.rzoom.com/iphoneservice.asmx";
    public static String mindmapWebServiceURL = "http://demo.semanoor.com/SemaMindMapNew/service.asmx";
    public static String mediaLibraryWebserviceURL = "http://dev1.semanoor.com/M.Sbook/M.SbookApi/SemaMediaLibraryApi.asmx";
    public static String redeemURL = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetClientStoreListByUserID?Id=";
    public static String groupUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetGroupDetailByUserID?Id=";
    public static String redeemInAppIdUrl = "http://school.nooor.com/NooorReaderPermissionService/PermissionService/GetRedeemInAppIDsByUserID?Id=";
    public static String IJSTupdateUrl = "http://www.nooor.com/ijst/ijst.txt";

    public static String NORMALWEBSERVICEURL = "http://ss.semanoor.com/sboook/sboookservice.asmx";
    public static String ENRICHWEBSERVICEURL = "http://sem20.nooor.com/IPadAdmin/SboookService.asmx";
    //public static String NORMALWEBSERVICEURL = "http://192.168.0.49/SBook2014/sboookservice.asmx";
    //public static String NORMALWEBSERVICEURL = "http://kaladik.semanoorindia.com/SBook2014/sboookservice.asmx";
    //public static String NORMALWEBSERVICEURL = "http://kaladik.semanoorindia.com/SBOOOK/sboookservice.asmx";
    //public static String WEBSERVICEURL = "http://kaladik.semanoorindia.com/Enrichments2014/IPadSSboookService.asmx";

    public static String TARGET_BASE_FONT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath().concat("/fonts");
    public static String TARGET_BASE_FILE_PATH;
    public static String TARGET_BASE_MINDMAP_PATH;
    public static String TARGET_BASE_TEMPATES_PATH;
    public static String TARGET_BASE_BOOKS_DIR_PATH;
    public static String TARGET_BASE_SCREENSHOTS_DIR_PATH;
    public static String TARGET_BASE_SHELF_RACK_IMG_DIR_PATH;
    public static String TARGET_BASE_FONT_TABLES_PTH;
    public static String TARGET_BASE_STORE_BOOKIMAGEPATH;
    public static String TARGET_BASE_INDEX_ASSET_PATH = "file:///android_asset/index.html";
    public static String TARGET_BASE_ENRICH_INDEX_ASSET_PATH = "file:///android_asset/enrichIndex.html";
    public static String TARGET_BASE_TABLE_INDEX_ASSET_PATH = "file:///android_asset/tableIndex.html";
    public static String TARGET_BASE_EXTERNAL_STORAGE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/SboookAuthor/";
    public static String TARGET_BASE_STOREOBJETCS_JS_PATH = "file:///android_asset/storeObjects.js";
    public static int portrait = 0;
    public static int landscape = 1;
    public static String[] fontnames = {"Arial", "Arial Black", "Comic Sans MS", "Trebuchet MS", "Verdana", "Georgia", "Times New Roman", "Andale Mono", "Courier New", "Impact", "Webdings"};
    public static Integer[] textAlign = {R.string.left, R.string.right, R.string.center, R.string.justify};
    public static Integer[] options = {R.string.option_two, R.string.option_three, R.string.option_four, R.string.option_five, R.string.option_six};
    public static String[] advSearch = {"Google", "Wikipedia", "Youtube", "Yahoo", "Bing", "Ask"};
    public static String fontColorKey = "arraylist_fontcolor";
    public static String backgroundColorKey = "arraylist_backgroundColor";
    public static String paintColorKey = "arraylist_paintColor";
    public static String pageBackgroundColorKey = "arraylist_pageBackgroundColor";
    public static String webTablebackgroundColorKey = "arraylist_webTableBackgroundColor";
    public static String webTablefontColorKey = "arraylist_webTablefontcolor";
    public static String inAppKey = "inAppPurchase";
    private static View clipboardView;
    public static String languagePrefsKey = "Language";
    public static String wipeAllData = "wipe_data";
    public static String IJSTversion = "";
    public static String downloadID = "ID";
    public static String oldDatabase = "oldDb";
    public static String Exist_Folder_Id = "id";
    public static String selectedCategory = "sc";
    public static String selectedCUniversity = "su";
    public static String selectedlanguage = "sl";
    public static String selectedCountry = "scoun";
    public static String nooorPlusEndDate = "npenddate";
    public static int shelfPosition = -1;
    public static boolean olddb;
    public static String exportEnrichUserNameKey = "export_enrichment_username";
    public static String exportEnrichPasswordKey = "export_enrichment_password";
    public static String rackBackgroundColorKey = "arraylist_rackBgColor";
//	public static String encryptFilePassWord = "SeMaNoOrFiLeS";

    public static String OBJECT_PREVIOUS_STATE = "objectPreviousState";
    public static String OBJECT_PRESENT_STATE = "objectPresentState";
    public static String OBJECT_CREATED = "objectCreated";
    public static String OBJECT_DELETED = "objectDeleted";
    public static String OBJECT_MOVED = "objectMoved";
    public static String OBJECT_CONTENT_EDITED = "objectContentEdited";
    public static String OBJECT_SCALED = "objectScaled";
    public static String OBJECT_LOCKED = "objectLocked";
    public static String OBJECT_SEQUENTIAL_ID_CHANGED = "objSequentialIdChanged";
    public static String OBJECT_CLEAR_ALL_CREATED = "objClearAllCreated";
    public static String OBJECT_CLEAR_ALL_DELETED = "objClearAllDeleted";
    public static String OBJECT_CONTENT_URL_EDITED = "objContentUrlEdited";
    public static String OBJECT_PAINT = "objectPaint";
    public static String OBJECT_BG_IMAGE_CHANGED = "objectBgImageChanged";

    //public static int initialPageNoWebTextWidth = 65, initialPageNoWebTextHeight = 65;

    public static String objType_pageNoText = "PageNoText";
    public static String OBJTYPE_TITLETEXT = "TitleText";
    public static String OBJTYPE_AUTHORTEXT = "AuthorText";
    public static String OBJTYPE_QUIZWEBTEXT = "QuizWebText";
    public static String OBJTYPE_MINDMAPWEBTEXT = "mindmapWebView";
    public static String OBJTYPE_MINDMAPBG = "mindmapBG";
    public static String OBJTYPE_PAGEBG = "PageBG";
    public static String OBJTYPE_PAGEBGEND = "PageBGEnd";
    public static String OBJTYPE_MINDMAPTAB = "mindmapTab";

    private static int designPageWidth, designPageHeight, mainDesignPageWidth, mainDesignPageHeight;

    public static Integer[] pageTemplatesArray = {R.string.title_text, R.string.title_image, R.string.title_bullet_text, R.string.bullet_text_image, R.string.images, R.string.image_text, R.string.image, R.string.blank};
    public static Integer[] listArray = {R.string.open_file, R.string.save_file};

    public static int totalUndoRedoSteps = 20;

    public static ArrayList<View> deletedObjectList;

    public static final String CONSUMER_KEY = "2utDdEHk1Qa68kpWbub5iA";
    public static final String CONSUMER_SECRET = "NcZRUVZ9MNwkDE2wWPMybcqe8SDCrEk53QzMlHVdYXA";
    public static final String CALLBACK_URL = "myapp://oauth";

    public static final String IEXTRA_AUTH_URL = "auth_url";
    public static final String IEXTRA_OAUTH_VERIFIER = "oauth_verifier";
    public static final String IEXTRA_OAUTH_TOKEN = "oauth_token";

    public static final String PREF_NAME = "com.example.android-twitter-oauth-demo";
    public static final String PREF_KEY_ACCESS_TOKEN = "access_token";
    public static final String PREF_KEY_ACCESS_TOKEN_SECRET = "access_token_secret";

    public static int scopeTypeFacebook = 3;
    public static int scopeTypeGoogle = 2;
    public static int scopeTypeSemanoor = 1;

    public static String sUserIdKey = "sUserId";
    public static String sUserNameKey = "sUserName";
    public static String sScopeTypeKey = "sScopeType";
    public static String sUserEmailIdKey = "sUserEmailId";
    public static String sUserEmailPhotoUrl = "sUserEmailPhotoUrl";
    public static String sAuthToken = "authtoken";

    //public static String SBABase64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfAWdB6FXFoyVXufHBg+JfScBwQg33M3E87MgGSTCCM4uUXN8JX/dxEZ2d5EtcOfNSIcH2t9xXAvBRM8MfxwWaRdnPAgPdUhixDOeSVfeKpE14L9kSwNV3bfg7FB7c/29L54rNRrhgf3ddqr0s+BlX7rOp0KIf1n/KtCo58przpA0elgJ5bBtXYcxYlbbK+2p0Y4+f73Bln1FIOEahFQXXUFVXMN0fDmdiD/9t1ksgOsdJWiM/0Yb8CKDhZEUQAyfKLj0gpYLFyd37fndObyyx4qdDXEP4SDIjCkEggrrSHLXVePZlb8nSpn8UDFx/MIYNUZTSgAeBIpWtBxLdNLOQIDAQAB";
    public static String manahijBase64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmrdMwiC+F6jqAh74G7NUC6/S3MTiHcWtMQxeIbpo5FzTbMj9DGSlw3vvKGP4CWQCHi1tITTXLqpVRqAJAw0G2DPm3j8ksYI4kwG1ZjWM0M2L3nhtnqKQjYeaZn9kOtAn9/Mz4g+YmHCcB+4rmddf37v6h18YXEP7gD7gMR3QKmsShYQ6bhs+8KPioOhm3FkfvRabLH3CS9O5kDFU3dTZqsB+Uin1AN3UrYzoCEGFHWFAKa8yyF3vS5LNFhERqF1SF+5rncQ8Sj+CG/6nsBpaoPntOynW+EuyEx+0p9Ib8qrMk5Z+uSrD6wmOPljkDqKSdM8v04seZlvnAgJRZZr4lwIDAQAB";

    private static boolean isLimitedVersion;

    public static final String PREF_AD_PURCHASED = "ads_purchased";
    public static final String ADS_DISPLAY_KEY = "ads_display_key";
    public static String AD_UNIT_ID = "ca-app-pub-6725157589656528/1577659898";
    public static String INTERSTITIAL_AD_UNIT_ID = "ca-app-pub-6725157589656528/7484135492";

    public static String ADS_INAPP_PURCHASE_PRODUCTID = "f006";
    public static String SUBSCRIPTION_INAPP_PURCHASE_PRODUCTID = "scoo1";
    //public static final String ADS_INAPP_PURCHASE_PRODUCTID = "android.test.purchased";

    public static String advancedSearchType = "Search";
    public static String onlineEnrichmentsType = "enrOnline";
    public static String downloadedEnrichmentType = "Downloaded";

    public static String SUBSCRIPTSTARTDATEKEY = "SsubscriptStartDate";
    public static String SUBSCRIPTENDDATEKEY = "SsubscriptEndDate";
    public static String SUBSCRIPTDAYSLEFT = "SsubscriptDaysLeft";
    public static String SUBSLIST = "subsList";
    public static String WEBLIST = "webList";
    public static String DownloadingTemplates = "template";

    public static String mindMapFileNameLastOpened;
    public static Book mindMapLastBook;

    private static boolean isTablet;
    public ArrayList<Object> bookListArray = new ArrayList<Object>();

    public static String enrShareUrl="http://www.nooor.com/share.html?id=";
    public static String gDriveClientId = "417673904045-a4g56jb52ph8474v0p2iv94a10ge9bqc.apps.googleusercontent.com";

    private Globals() {
        TARGET_BASE_FILE_PATH = "data/data/" + getCurrentProjPackageName() + "/files/";
        TARGET_BASE_MINDMAP_PATH = "data/data/" + getCurrentProjPackageName() + "/files/mindMaps/";
        TARGET_BASE_TEMPATES_PATH = "data/data/" + getCurrentProjPackageName() + "/files/templates/";
        TARGET_BASE_BOOKS_DIR_PATH = "data/data/" + getCurrentProjPackageName() + "/files/books/";
        TARGET_BASE_FONT_TABLES_PTH= "data/data/" + getCurrentProjPackageName() + "/files/fonttables";
        TARGET_BASE_SCREENSHOTS_DIR_PATH = "data/data/" + getCurrentProjPackageName() + "/files/PageScreenshots/";
        TARGET_BASE_SHELF_RACK_IMG_DIR_PATH = "data/data/" + getCurrentProjPackageName() + "/files/ShelfRackImg/";
        TARGET_BASE_STORE_BOOKIMAGEPATH = Environment.getDataDirectory().getPath().concat("/data/" + getCurrentProjPackageName() + "/files/store_bookImage/");
    }

    public static void setDeviceHeight(int height) {
        deviceHeight = height;
    }

    public static int getDeviceHeight() {
        return deviceHeight;
    }

    public static void setDeviceWidth(int weight) {
        deviceWidth = weight;
    }

    public static int getDeviceWidth() {
        return deviceWidth;
    }

    /**
     * @return the designPageWidth
     */
    public static int getDesignPageWidth() {
        return designPageWidth;
    }

    /**
     * @param designPageWidth the designPageWidth to set
     */
    public static void setDesignPageWidth(int designPageWidth) {
        Globals.designPageWidth = designPageWidth;
    }

    /**
     * @return the designPageHeight
     */
    public static int getDesignPageHeight() {
        return designPageHeight;
    }

    /**
     * @param designPageHeight the designPageHeight to set
     */
    public static void setDesignPageHeight(int designPageHeight) {
        Globals.designPageHeight = designPageHeight;
    }

    public static synchronized Globals getInstance() {
        if (instance == null) {
            instance = new Globals();
        }
        return instance;
    }

    /**
     * @return the clipboardView
     */
    public static View getClipboardView() {
        return clipboardView;
    }

    /**
     * @param clipboardView the clipboardView to set
     */
    public static void setClipboardView(View clipboardView) {
        Globals.clipboardView = clipboardView;
    }

    /**
     * show Keyboard
     * @param context
     */
    /*public static void showKeyboard(Context context) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
	}*/

    /**
     * hide Keyboard
     * @param context
     */
	/*public static void hideKeyboard(Context context){
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
	}*/

    /**
     * @return the mainDesignPageWidth
     */
    public static int getMainDesignPageWidth() {
        return mainDesignPageWidth;
    }

    /**
     * @param mainDesignPageWidth the mainDesignPageWidth to set
     */
    public static void setMainDesignPageWidth(int mainDesignPageWidth) {
        Globals.mainDesignPageWidth = mainDesignPageWidth;
    }

    /**
     * @return the mainDesignPageHeight
     */
    public static int getMainDesignPageHeight() {
        return mainDesignPageHeight;
    }

    /**
     * @param mainDesignPageHeight the mainDesignPageHeight to set
     */
    public static void setMainDesignPageHeight(int mainDesignPageHeight) {
        Globals.mainDesignPageHeight = mainDesignPageHeight;
    }

    /**
     * get device independent value by passing the pixel value
     *
     * @param pxValue
     * @param context
     * @return
     */
    public static int getDeviceIndependentPixels(int pxValue, Context context) {
        //int dipValue = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pxValue, context.getResources().getDisplayMetrics());
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        //int dipValue = Math.round(pxValue * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        int dipValue = (int) ((pxValue * displayMetrics.density) + 0.5);
        return dipValue;
    }

    /**
     * get Pixel value from device indepent pixel value
     *
     * @param dipValue
     * @param context
     * @return
     */
    public static int getPixelValue(int dipValue, Context context) {
        //int pxValue = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, dipValue, context.getResources().getDisplayMetrics());
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        //int pxValue = Math.round(dipValue / (displayMetrics.density / DisplayMetrics.DENSITY_DEFAULT));
        int pxValue = (int) ((dipValue / displayMetrics.density) + 0.5);
        return pxValue;
    }

    /**
     * @return the isLimitedVersion
     */
    public static boolean isLimitedVersion() {
        return isLimitedVersion;
    }

    /**
     * @param isLimmitedVersion
     */
    public static void setLimitedVersion(boolean isLimmitedVersion) {
        isLimitedVersion = isLimmitedVersion;
    }

    /**
     * @return the newCurriculumWebServiceURL
     */
    public static String getNewCurriculumWebServiceURL() {
        return newCurriculumWebServiceURL;
    }

    /**
     * @param newCurriculumWebServiceURL the newCurriculumWebServiceURL to set
     */
    public static void setNewCurriculumWebServiceURL(
            String newCurriculumWebServiceURL) {
        Globals.newCurriculumWebServiceURL = newCurriculumWebServiceURL;
    }

    /**
     * @return the isTablet
     */
    public static boolean isTablet() {
        return isTablet;
    }

    /**
     * @param isTablet the isTablet to set
     */
    public static void setTablet(boolean isTablet) {
        Globals.isTablet = isTablet;
    }

    public static String getCurrentProjPackageName() {
        return currentProjPackageName;
    }

    public static void setCurrentProjPackageName(String currentProjPackageName) {
        Globals.currentProjPackageName = currentProjPackageName;
    }

    public static String getSubscriptionWebserviceURL() {
        return subscriptionWebserviceURL;
    }

    public static void setSubscriptionWebserviceURL(String subscriptionWebserviceURL) {
        Globals.subscriptionWebserviceURL = subscriptionWebserviceURL;
    }

    public static boolean ISGDRIVEMODE() {
        return ISGDRIVEMODE;
    }

    public static void setISGDRIVEMODE(boolean ISGDRIVEMODE) {
        Globals.ISGDRIVEMODE = ISGDRIVEMODE;
    }

    public static String getStoreUrl() {
        return storeUrl;
    }

    public static void setStoreUrl(String storeUrl) {
        Globals.storeUrl = storeUrl;
    }
}
