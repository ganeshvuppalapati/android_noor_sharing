package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;
import com.semanoor.manahij.ThemeActivity;

import java.util.ArrayList;

public class GoogleImageSearchAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<Object> listImages;
	public ImageLoader imageLoader; 

	public GoogleImageSearchAdapter(Context ctx,
			ArrayList<Object> images, ImageLoader imageLoader) {
		this.context = ctx;
		this.listImages = images;
		this.imageLoader=imageLoader;
	}

	@Override
	public int getCount() {
		return listImages.size();
	}

	@Override
	public Object getItem(int position) {
		return listImages.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public static class ViewHolder{
		public ImageView imgViewImage;
		public TextView txtViewTitle;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		if (convertView == null) {
			if(context instanceof BookViewActivity) {
				view = ((BookViewActivity)context).getLayoutInflater().inflate(R.layout.google_image_search_list, null);
			}else{
				view = ((ThemeActivity)context).getLayoutInflater().inflate(R.layout.google_image_search_list, null);
			}
			holder=new ViewHolder();
			holder.imgViewImage=(ImageView)view.findViewById(R.id.imageView1);
			holder.txtViewTitle=(TextView)view.findViewById(R.id.textView1);

			view.setTag(holder);
		} else {
			holder=(ViewHolder)view.getTag();
		}
		GoogleImageSearchData googleImageSearchData = (GoogleImageSearchData) listImages.get(position);
		holder.imgViewImage.setTag(googleImageSearchData.getThumbUrl());
		if(context instanceof BookViewActivity) {
			imageLoader.DisplayImage(googleImageSearchData.getThumbUrl(), ((BookViewActivity)context), holder.imgViewImage);
		}else{
			imageLoader.DisplayImage(googleImageSearchData.getThumbUrl(), ((ThemeActivity)context), holder.imgViewImage);
		}

		holder.txtViewTitle.setText(Html.fromHtml(googleImageSearchData.getTitle()));
		return view;
	}

}
