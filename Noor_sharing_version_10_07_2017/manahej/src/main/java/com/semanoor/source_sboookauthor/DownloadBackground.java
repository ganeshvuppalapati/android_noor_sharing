package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.view.View;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.CloudBooksActivity;
import com.semanoor.manahij.CloudEnrichDetails;
import com.semanoor.manahij.GenerateThumbnailsPdf;
import com.semanoor.manahij.MainActivity;
import com.semanoor.manahij.StoreViewActivity;
import com.semanoor.sboookauthor_store.DownloadCounts;
import com.semanoor.sboookauthor_store.StoreDatabase;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static android.graphics.Bitmap.createBitmap;

/**
 * Created by karthik on 06-05-2016.
 */
public class DownloadBackground {
    public Book downLoadBook;
    private Context Context;
    Boolean downloadSuccess = false;
    public TextCircularProgressBar textCircularProgressBar;
    private ArrayList<Book> list = new ArrayList<>();
    private boolean downloadTaskRunning;
    private static DownloadBackground instance;
    DatabaseHandler db;
    private ArrayList<HashMap<String, String>> cloudEnrList=new ArrayList<HashMap<String, String>>();
    private ArrayList<HashMap<String, String>> selectedCloudEnrList = new ArrayList<HashMap<String, String>>();
    GenerateThumbnailsPdf thumbnailsPdf;

    private ArrayList<String> jsonContent = new ArrayList<>();

  public static synchronized DownloadBackground getInstance(){
      if(instance == null){
          instance = new DownloadBackground();
      }
      return instance;
  }

    public boolean isDownloadTaskRunning() {
        return downloadTaskRunning;
    }

    public void setDownloadTaskRunning(boolean downloadTaskRunning) {
        this.downloadTaskRunning = downloadTaskRunning;
    }

    public ArrayList<HashMap<String, String>> getCloudEnrList() {
        return cloudEnrList;
    }

    public void setCloudEnrList(ArrayList<HashMap<String, String>> cloudEnrList) {
        this.cloudEnrList = cloudEnrList;
    }

    public ArrayList<HashMap<String, String>> getSelectedCloudEnrList() {
        return selectedCloudEnrList;
    }

    public void setSelectedCloudEnrList(ArrayList<HashMap<String, String>> selectedCloudEnrList) {
        this.selectedCloudEnrList = selectedCloudEnrList;
    }

    public ArrayList<String> getString() {
        return jsonContent;
    }

    public void setString(ArrayList<String> list) {
        this.jsonContent = list;
    }


    public class downloadAsyncTask extends  AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!downLoadBook.is_bStoreBook()&&getSelectedCloudEnrList().size()>0) {
                SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(Context);
                SharedPreferences.Editor editor = sharedPreference.edit();
                try {
                    editor.putString("selectedCloudList", ObjectSerializer.serialize(getSelectedCloudEnrList()));
                    editor.putString("jsonString", ObjectSerializer.serialize(getString()));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                editor.commit();
            }
        }
        @Override
        protected String doInBackground(String... downloadurl) {
            final int TIMEOUT_CONNECTION = 5000;//5sec
            final int TIMEOUT_SOCKET = 30000;//30sec
            long bytes = 1024 * 1024;
            long total = 0;
            int fileLength;
            try {
                if (UserFunctions.isInternetExist(Context)) {
                    //   Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND+Process.THREAD_PRIORITY_LESS_FAVORABLE);
                    URL url = new URL(downloadurl[0]);
                    URLConnection ucon = url.openConnection();
                    File newFile;
                    if( downLoadBook.is_bStoreBook()) {
                       newFile = new File((Globals.TARGET_BASE_FILE_PATH) + downLoadBook.get_bStoreID());
                    }else{
                        newFile = new File((Globals.TARGET_BASE_FILE_PATH) + downLoadBook.getBookID());
                    }
                    if (newFile.exists()) {
                        total = newFile.length();
                        ucon.setRequestProperty("Range", "bytes=" + total + "-");
                        ucon.connect();
                    } else {
                        ucon.setRequestProperty("Range", "bytes=" + total + "-");
                        String length=ucon.getHeaderField("content-length");
                        ucon.connect();
                    }
                    fileLength = ucon.getContentLength();
                    if(!downLoadBook.is_bStoreBook()) {
                        String Stringlist=getString().get(0);
                        try {
                            JSONObject enrJson = new JSONObject(Stringlist);
                            fileLength=Integer.parseInt(enrJson.getString("fileSize"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    long startTime = System.currentTimeMillis();
                    ucon.setReadTimeout(TIMEOUT_CONNECTION);
                    ucon.setConnectTimeout(TIMEOUT_SOCKET);
                    InputStream is = ucon.getInputStream();
                    BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
                    String bookPath;
                    if(downLoadBook.is_bStoreBook()) {
                        bookPath = downLoadBook.get_bStoreID();
                    }else{
                        bookPath = String.valueOf(downLoadBook.getBookID());
                    }
                    FileOutputStream outStream = (total == 0) ? Context.openFileOutput(bookPath, Context.MODE_PRIVATE) : Context.openFileOutput(bookPath, Context.MODE_APPEND);
                    byte[] buff = new byte[5 * 1024];
                    int len;
                    String totalMB, fileLengthMB;
                    DecimalFormat twoDformat = new DecimalFormat("#.##");
                    while (!isCancelled() && (len = inStream.read(buff)) != -1) {
                        total += len;
                        publishProgress((int) ((total * 100) / fileLength));
                        outStream.write(buff, 0, len);
                    }
                    //clean up
                    outStream.flush();
                    outStream.close();
                    inStream.close();
                    downloadSuccess = true;
                }
                    //   downloadStatus = 0;
                    ////System.out.println( "download completed in "+ ((System.currentTimeMillis() - startTime) / 1000)+ " sec");
            }catch (IOException e) {
                ////System.out.println("error while storing:"+e);
                downloadSuccess = false;
                //   downloadStatus = 0;
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            if (textCircularProgressBar!=null){
                textCircularProgressBar.setProgress(progress[0]);
                textCircularProgressBar.setVisibility(View.VISIBLE);
            }
            //////System.out.println("OnprogressUpdate:"+progress[0]);
        }
        protected void onPostExecute(String result) {
            ////System.out.println("OnpostExecute");
            if (downloadSuccess) {
                db = DatabaseHandler.getInstance(Context);
                String bookpath,zipfile,unzippathWithSlash;
                if( downLoadBook.is_bStoreBook()) {
                    bookpath = downLoadBook.get_bStoreID();   //Changes after merge
                    zipfile = Globals.TARGET_BASE_FILE_PATH.concat(bookpath);
                    unzippathWithSlash = downLoadBook.get_bStoreID().concat("Book/");
                }else{
                    bookpath =String.valueOf(downLoadBook.getBookID());   //Changes after merge
                    zipfile = Globals.TARGET_BASE_FILE_PATH.concat(bookpath);
                    unzippathWithSlash = bookpath.concat("/");
                }
                String unziplocation = Globals.TARGET_BASE_BOOKS_DIR_PATH.concat(unzippathWithSlash); //"/" is a must here..
                boolean unzipSuccess = unzipDownloadfileNew(zipfile, unziplocation); //if unzipping success then need to store in database:
               // if(downLoadBook.is_bStoreBook())
                if (unzipSuccess) {
                    float price = Float.valueOf(downLoadBook.getPrice());
                    if (price>0.00){
                        if(downLoadBook.is_bStoreBook()) {
                            StoreDatabase.deleteBnamefromPayPaltbl(downLoadBook.get_bStoreID());
                        }
                    }
                    if (downLoadBook.is_bStoreBook() &&downLoadBook.get_bStoreID().contains("E")) {
                        new downloadIJST().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Globals.IJSTupdateUrl);
                     //   changingfilespathname(filePath);
                    } else if (downLoadBook.is_bStoreBook() &&downLoadBook.get_bStoreID().contains("I")) {
                        String srcPath = Globals.TARGET_BASE_FILE_PATH + "books/" + downLoadBook.get_bStoreID() + "Book";
                        File mindMapDir = new File(srcPath);
                        File[] mindMapFiles = mindMapDir.listFiles();
                        for (File file : mindMapFiles) {
                            String filaName = file.getName();
                            String desPath = Globals.TARGET_BASE_FILE_PATH + "books/" + downLoadBook.getBookID() + "/mindMaps/";
                            if (file.isFile() && filaName.contains(".xml")) {
                                UserFunctions.copyFiles(file.getPath(), desPath + filaName);
                            } else if (file.isDirectory()) {
                                try {
                                    UserFunctions.copyDirectory(file, new File(desPath));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }  else if (downLoadBook.is_bStoreBook() &&downLoadBook.get_bStoreID().contains("P")) {
                        new savingScrennShotForPDF().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    }else if (downLoadBook.is_bStoreBook() &&downLoadBook.get_bStoreID().contains("M")) {
                        if (Context instanceof StoreViewActivity) {
                            ((StoreViewActivity) Context).webView.setInitialValues2(downLoadBook.get_bStoreID() + "Book", downLoadBook.getTotalPages(), downLoadBook, null, null, instance);
                            ((StoreViewActivity) Context).webView.processHtmlPages();
                        } if (Context instanceof CloudEnrichDetails) {
                            ((CloudEnrichDetails) Context).webView.setInitialValues3(downLoadBook.get_bStoreID() + "Book", downLoadBook.getTotalPages(), downLoadBook, ((CloudEnrichDetails) Context), null, instance);
                            ((CloudEnrichDetails) Context).webView.processHtmlPages();
                        }if (Context instanceof MainActivity){
                            ((MainActivity) Context).webView.setInitialValues2(downLoadBook.get_bStoreID() + "Book", downLoadBook.getTotalPages(), downLoadBook, (MainActivity)Context, textCircularProgressBar,instance);
                            ((MainActivity) Context).webView.processHtmlPages();
                        }if (Context instanceof BookViewActivity) {
                            ((BookViewActivity) Context).webView.setInitialValues3(downLoadBook.get_bStoreID() + "Book", downLoadBook.getTotalPages(), downLoadBook, ((CloudEnrichDetails) Context), null, instance);
                            ((BookViewActivity) Context).webView.processHtmlPages();
                        }
                    }

                }
               if (!downLoadBook.is_bStoreBook() &&downLoadBook.get_bStoreID()!=null &&downLoadBook.get_bStoreID().contains("I")) {
                    if(textCircularProgressBar!=null) {
                        textCircularProgressBar.setVisibility(View.INVISIBLE);
                    }
                   db.executeQuery("update books set isDownloadedCompleted='downloaded'where BID ='" + downLoadBook.getBookID() + "'");
                   if(Context instanceof StoreViewActivity) {
                       ((StoreViewActivity) Context).downloadedBooksArray.add(downLoadBook.get_bStoreID());
                       ((StoreViewActivity) Context).downloadingBooksArray.remove(downLoadBook.get_bStoreID());
                   }else if(Context instanceof MainActivity){
                       ((MainActivity) Context).loadGridView();
                   }
                   String mindMapDir = Globals.TARGET_BASE_MINDMAP_PATH;
                   UserFunctions.createNewDirectory(mindMapDir);
                   File Path = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+downLoadBook.getBookID()+"/mindMaps");
                   File[] mindMapFiles = Path.listFiles();
                   for (File file : mindMapFiles) {
                       String filaName = file.getName();
                      // String desPath = Globals.TARGET_BASE_MINDMAP_PATH;
                       UserFunctions.createNewDirectory(mindMapDir);
                       String desPath =mindMapDir;
                       if (file.isFile() && filaName.contains(".xml")) {
                           UserFunctions.copyFiles(file.getPath(), desPath + downLoadBook.getBookTitle()+"_"+downLoadBook.getBookID()+".xml");
                           //UserFunctions.copyFiles(file.getPath(), Path + downLoadBook.getBookTitle()+".xml");
//                           file.delete();
                           File f=new File(Path +"/"+ downLoadBook.getBookTitle()+"_"+downLoadBook.getBookID()+".xml");
                           file.renameTo(f);
                       } else if (file.isDirectory()) {
                           try {
                               UserFunctions.copyDirectory(file, new File(desPath));
                           } catch (IOException e) {
                               e.printStackTrace();
                           }
                       }
                   }
                  // UserFunctions.copyFiles(Globals.TARGET_BASE_FILE_PATH+"FreeFiles/")

                   //String filePath = mindMapDir + mindMapTitle + ".xml";
                }
                if(!downLoadBook.is_bStoreBook()&&downLoadBook.get_bStoreID()==null){
                    new savingImagesforNormalBook().execute();
                }
                if (!downLoadBook.is_bStoreBook() &&downLoadBook.get_bStoreID()!=null &&downLoadBook.get_bStoreID().contains("A")) {
                    if(textCircularProgressBar!=null) {
                        textCircularProgressBar.setVisibility(View.INVISIBLE);
                    }
                    db.executeQuery("update books set isDownloadedCompleted='downloaded'where BID ='" + downLoadBook.getBookID() + "'");
                    if(Context instanceof StoreViewActivity) {
                        ((StoreViewActivity) Context).downloadedBooksArray.add(downLoadBook.get_bStoreID());
                        ((StoreViewActivity) Context).downloadingBooksArray.remove(downLoadBook.get_bStoreID());
                    }else if(Context instanceof MainActivity){
                        ((MainActivity) Context).loadGridView();
                    }
                }
                java.io.File zipfilelocation;
                if(downLoadBook.is_bStoreBook()) {
                   DownloadCounts.downloadCountsWebservice(downLoadBook.get_bStoreID()); //To call webservice download counts webservice.
                   java.io.File fileDir = Context.getFilesDir();   //Deleting zip file in the internal storage
                   zipfilelocation = new java.io.File(fileDir, downLoadBook.get_bStoreID());
                }else {
                    DownloadCounts.downloadCountsWebservice(String.valueOf(downLoadBook.getBookID())); //To call webservice download counts webservice.
                    java.io.File fileDir = Context.getFilesDir();   //Deleting zip file in the internal storage
                    zipfilelocation = new java.io.File(fileDir, String.valueOf(downLoadBook.getBookID()));

                }
                try {
                    zipfilelocation.delete();
                } catch (Exception e) {
                }
                if (Context instanceof MainActivity) {
                    if(downLoadBook.is_bStoreBook()) {
                        if (!downLoadBook.get_bStoreID().contains("M") && !downLoadBook.get_bStoreID().contains("P") && !downLoadBook.get_bStoreID().contains("E")) {
                            textCircularProgressBar.setVisibility(View.INVISIBLE);
                            db.executeQuery("update books set isDownloadedCompleted='downloaded'where BID ='" + downLoadBook.getBookID() + "'");
                            ((MainActivity) Context).loadGridView();
                        }
                    }
                }else{
                    if(Context instanceof StoreViewActivity) {
                       if (downLoadBook.is_bStoreBook() && !downLoadBook.get_bStoreID().contains("M") && !downLoadBook.get_bStoreID().contains("P") && !downLoadBook.get_bStoreID().contains("E")) {
                           db.executeQuery("update books set isDownloadedCompleted='downloaded'where BID ='" + downLoadBook.getBookID() + "'");
                       }
                       ((StoreViewActivity) Context).downloadedBooksArray.add(downLoadBook.get_bStoreID());
                       ((StoreViewActivity) Context).downloadingBooksArray.remove(downLoadBook.get_bStoreID());
                   }
                }
                if (downLoadBook.is_bStoreBook()&&!downLoadBook.get_bStoreID().contains("M") && !downLoadBook.get_bStoreID().contains("P")&& !downLoadBook.get_bStoreID().contains("E")) {
                    list.remove(0);
                    if (list.size() > 0) {
                        final Book book = list.get(0);
                        setDownLoadBook(book);
                        setDownloadTaskRunning(true);
                        new android.os.Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                new downloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, book.getDownloadURL());
                            }
                        }, 2000);
                    } else {
                        setDownloadTaskRunning(false);
                    }
                }
            }
        }
    }
   public void savingImages(Bitmap b, Book book, final int page, final int pageCount, final TextCircularProgressBar processDialog) {
        //File f = new File(FilePath);
        Bitmap bitmap = null;
        String screenShot;
        if(processDialog!=null) {
        //    processDialog.setProgress((page * 100) / pageCount);
        }
      //  try {

            //String coverandendpage=absPath+"/Download";
            //PdfRenderer pr = new PdfRenderer(ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_WRITE));
            // processPageProgDialog.setProgress((page * 100) / pr.getPageCount());
            if (page == 0) {
                screenShot = Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID() + "/FreeFiles/card.png";
                //screenShot=coverandendpage+"/card.png";
                // if(!new File(screenShot).exists()) {
                if(Context instanceof MainActivity) {
                    bitmap = Bitmap.createScaledBitmap(b,Globals.getDeviceIndependentPixels(200,(MainActivity)Context), Globals.getDeviceIndependentPixels(300, (MainActivity)Context), true);
                }else{
                    bitmap = Bitmap.createScaledBitmap(b,Globals.getDeviceIndependentPixels(200,(StoreViewActivity)Context), Globals.getDeviceIndependentPixels(300, (StoreViewActivity)Context), true);
                }
                //pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

                if(!new File(screenShot).exists()) {
                    takeScreenShot(bitmap, screenShot);
                }
                //UserFunctions.saveBitmapImage(b, screenShot);
                // }

                int pageNo = page + 1;
                screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookID() + "/thumbnail_" + pageNo + ".png";
                //UserFunctions.saveBitmapImage(b, screenShot);
                takeScreenShot(bitmap,screenShot);

                String front_thumb_path = Globals.TARGET_BASE_BOOKS_DIR_PATH+book.getBookID()+"/FreeFiles/frontThumb.png";
                //UserFunctions.saveBitmapImage(b, front_thumb_path);
                takeScreenShot(b,front_thumb_path);
                //Bitmap pageScreenShot= savingPageImages(f,page);
                takeScreenShot( b, Globals.TARGET_BASE_BOOKS_DIR_PATH +book.getBookID()+ "/FreeFiles/front_P.png");

            } else if (page < pageCount - 1) {
                int pageNo = page + 1;//2
                screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookID() + "/thumbnail_"+ pageNo + ".png";
                //screenShot=coverandendpage+"/thumbnail_"+pageNo+".png";
                if(Context instanceof MainActivity) {
                    bitmap = Bitmap.createScaledBitmap(b,Globals.getDeviceIndependentPixels(130, (MainActivity)Context), Globals.getDeviceIndependentPixels(170, (MainActivity)Context), true);
                }else{
                    bitmap = Bitmap.createScaledBitmap(b,Globals.getDeviceIndependentPixels(130, (StoreViewActivity)Context), Globals.getDeviceIndependentPixels(170, (StoreViewActivity)Context), true);

                }
               // pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                takeScreenShot(bitmap,screenShot);
                //Bitmap pageScreenShot= savingPageImages(f,page);
                takeScreenShot(b, Globals.TARGET_BASE_BOOKS_DIR_PATH +book.getBookID()+ "/FreeFiles/"+pageNo+".png");
                //UserFunctions.saveBitmapImage(b, screenShot);
            } else {
                screenShot = Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID() + "/FreeFiles/back_P.png";
                if(Context instanceof MainActivity) {
                    bitmap = Bitmap.createScaledBitmap(b,Globals.getDeviceIndependentPixels(130, (MainActivity)Context), Globals.getDeviceIndependentPixels(170, (MainActivity)Context), true);
                }else{
                    bitmap = Bitmap.createScaledBitmap(b,Globals.getDeviceIndependentPixels(130, (StoreViewActivity)Context), Globals.getDeviceIndependentPixels(170, (StoreViewActivity)Context), true);

                }
                //pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                // UserFunctions.saveBitmapImage(b, screenShot);
                takeScreenShot(bitmap,screenShot);

                String screenShot1 = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookID() + "/thumbnail_end.png";
                // UserFunctions.saveBitmapImage(b, screenShot1);
                takeScreenShot(bitmap,screenShot1);
                // processPageProgDialog.dismiss();
            }

//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
    private void takeScreenShot(Bitmap bit,String imagePath){
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imagePath);
            bit.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();

        } catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    private Bitmap savingPageImages(File f, int page){
        PdfRenderer pr = null;
        Bitmap b = null;
        try {
            pr = new PdfRenderer(ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_WRITE));
            b = createBitmap(Globals.getDeviceWidth(), Globals.getDeviceHeight(), Bitmap.Config.ARGB_4444);
            pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return b;
    }

    private boolean unzipDownloadfileNew(String zipfilepath, String unziplocation) {
        ZipFile zipFile = null;
        List<FileHeader> headers = null;
        try {
            zipFile = new ZipFile(new File(zipfilepath));
            headers = zipFile.getFileHeaders();
        } catch (ZipException e) {
            e.printStackTrace();
        }

        if (headers != null) {
            for (int i = 0; i < headers.size(); i++) {
                FileHeader header = headers.get(i);
                header.setFileName(header.getFileName().replace("\\", "//"));
                //File  file = new File(eleessonPath+header.getFileName());
                try {
                    zipFile.extractFile(header, unziplocation);
                    if (downLoadBook.get_bStoreID()!=null){
                        if (downLoadBook.get_bStoreID().contains("M")||downLoadBook.get_bStoreID().contains("E")||downLoadBook.get_bStoreID().contains("Q")){
                            File file = new File(unziplocation + header.getFileName());
                            if (file.exists()) {
                                String str = unziplocation + header.getFileName();
                                String[] split = str.split("\\.");
                                if (split.length > 1) {
                                    String fileEx = split[split.length - 1];
                                    if (fileEx.equals("htm")||fileEx.equals("html")){
                                        UserFunctions.encryptFile(file);
                                    }
                                }
                            }
                        }
                    }
                } catch (ZipException e) {
                    e.printStackTrace();

                }

            }
        }
        return true;
    }

    public ArrayList<Book> getList() {
        return list;
    }

    public void setList(ArrayList<Book> list) {
        this.list = list;
    }

    public TextCircularProgressBar getTextCircularProgressBar() {
        return textCircularProgressBar;
    }

    public void setTextCircularProgressBar(TextCircularProgressBar textCircularProgressBar) {
        this.textCircularProgressBar = textCircularProgressBar;
    }

    public Book getDownLoadBook() {
        return downLoadBook;
    }

    public void setDownLoadBook(Book downLoadBook) {
        this.downLoadBook = downLoadBook;
    }


    public android.content.Context getContext() {
        return Context;
    }

    public void setContext(android.content.Context context) {
        Context = context;
    }

    private class savingScrennShotForPDF extends AsyncTask<Void,Integer,Void>{

        @Override
        protected Void doInBackground(Void... params) {

            String sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + downLoadBook.get_bStoreID() + "Book/Card.gif";
            String pdfName = null;
            File srcDir = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+downLoadBook.get_bStoreID()+"Book");
            String[] path = srcDir.list();
            for(String file : path) {
                String fromPath = srcDir + "/" + file;
                String[] str = file.split("\\.");
                if(file.contains(".pdf")){
                    pdfName=fromPath;
                    break;
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
              thumbnailsPdf = new GenerateThumbnailsPdf(Context, pdfName,null);
                thumbnailsPdf.setComplited(false);
                for (int i = 0; i < thumbnailsPdf.countPages(); i++) {
                    publishProgress((int) ((i * 100) / thumbnailsPdf.countPages()));
                    Bitmap b = thumbnailsPdf.createThumbnail(Globals.getDeviceWidth(), Globals.getDeviceHeight(), i);
                    if (Context instanceof MainActivity) {
                        savingImages(b, downLoadBook, i, thumbnailsPdf.countPages(), textCircularProgressBar);
                    } else {
                        savingImages(b, downLoadBook, i, thumbnailsPdf.countPages(), null);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (textCircularProgressBar!=null){
                textCircularProgressBar.setProgress(values[0]);
                textCircularProgressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            db.executeQuery("update books set isDownloadedCompleted='downloaded'where BID ='" + downLoadBook.getBookID() + "'");
            if (textCircularProgressBar!=null){
                textCircularProgressBar.setVisibility(View.INVISIBLE);
                ((MainActivity) Context).loadGridView();
            }
            if (list.size() > 0) {
                list.remove(0);
                if (list.size() > 0) {
                    final Book bok = list.get(0);
                    setDownLoadBook(bok);
                    setDownloadTaskRunning(true);
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new downloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bok.getDownloadURL());
                        }
                    }, 2000);
                }
            }else{
                setDownloadTaskRunning(false);
            }
        }
    }

    private class savingImagesforNormalBook extends AsyncTask<Void,Integer,Void> {
        int lastBookId;

        @Override
        protected Void doInBackground(Void... params) {
            String bookDetails = null;
            String pageDetails;
            String key, value;
            lastBookId=downLoadBook.getBookID();
            try {
                String data=getString().get(0);
                JSONObject jsonObj=new JSONObject(data);
                pageDetails = jsonObj.getString("book");
                JSONArray store1 = new JSONArray(pageDetails);
                String size = null, content = null,Changedcontent = null, SeqID = null, objType = null, pageNo = null, location = null, ScalePageToFit = null, objID = null, rowID;
                for (int i = 0; i < store1.length(); i++) {
                    publishProgress((int) ((i * 100) /  store1.length()));
                    JSONObject jsonSub = store1.getJSONObject(i);
                    Iterator iter1 = jsonSub.keys();
                    while (iter1.hasNext()) {
                        key = (String) iter1.next();
                        if (key.equals("size")) {
                            size = jsonSub.getString(key);
                        } else if (key.equals("rowID")) {
                            rowID = jsonSub.getString(key);
                        } else if (key.equals("content")) {
                            content = jsonSub.getString(key);
                        } else if (key.equals("SeqID")) {
                            SeqID = jsonSub.getString(key);
                        } else if (key.equals("objType")) {
                            objType = jsonSub.getString(key);
                        } else if (key.equals("pageNo")) {
                            pageNo = jsonSub.getString(key);
                        } else if (key.equals("location")) {
                            location = jsonSub.getString(key);
                        } else if (key.equals("ScalePageToFit")) {
                            ScalePageToFit = jsonSub.getString(key);
                        } else if (key.equals("objID")) {
                            objID = jsonSub.getString(key);
                        }
                    }
                    String path, objContent = null;
                    // if(objType.equals("Image")||objType.equals("YouTube")||objType.equals("Audio")){
                    int objUniqueId = 0;
                    if(Context instanceof MainActivity) {
                        objUniqueId = ((MainActivity) Context).db.getMaxUniqueRowID("objects") + 1;
                    }else if(Context instanceof CloudBooksActivity){
                        objUniqueId = ((CloudBooksActivity) Context).db.getMaxUniqueRowID("objects") + 1;
                    }
                    String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + lastBookId + "/" + "images/";
                    // String split[]=content.split("/");
                    if (objType.equals("Image") || objType.equals("YouTube") || objType.equals("Audio")) {
                        //if(content.contains(Globals.TARGET_BASE_BOOKS_DIR_PATH))
                            String[] split = content.split("/");
                            String imgName=split[split.length-1];
                            if (objType.equals("Image")) {
                                if (content.contains("defaultbg")) {
                                    objContent = imgDir + objUniqueId + "_defaultbg.png";
                                } else {
                                    objContent = imgDir + objUniqueId + ".png";
                                }
                                String imgPath = imgDir + imgName;
                                java.io.File prevPath = new java.io.File(imgPath);
                                java.io.File latest = new java.io.File(objContent);
                                // UserFunctions.copyFiles(content, objContent);
                                prevPath.renameTo(latest);
                                content = objContent;
                            }
                            if (objType.equals("YouTube")) {
                                objContent = imgDir + objUniqueId + ".mp4";
                                String imgPath = imgDir + imgName;
                                java.io.File prevPath = new java.io.File(imgPath);
                                java.io.File latest = new java.io.File(objContent);
                                // UserFunctions.copyFiles(content, objContent);
                                prevPath.renameTo(latest);
                                content = objContent;
                            }else if (objType.equals("Audio")) {
                                objContent = imgDir + objUniqueId + ".m4a";
                                String imgPath = imgDir + imgName;
                                java.io.File prevPath = new java.io.File(imgPath);
                                java.io.File latest = new java.io.File(objContent);
                                // UserFunctions.copyFiles(content, objContent);
                                prevPath.renameTo(latest);
                                content = objContent;
                            }
                        }
                    else if (objType.equals("QuizWebText")) {

//                        if(Context instanceof MainActivity) {
//                            Changedcontent = imagePath(imgDir, objUniqueId, ((MainActivity)Context).db, content,lastBookId);
//                        }else if(Context instanceof CloudBooksActivity) {
//                            Changedcontent = imagePath(imgDir, objUniqueId, ((CloudBooksActivity)Context).db, content, lastBookId);
//
//                        }
                        if(Context instanceof MainActivity) {
                            content = replaceExistingImages(imgDir, objUniqueId, ((MainActivity)Context).db, content, Integer.parseInt(objID),lastBookId);
                        }else if(Context instanceof CloudBooksActivity) {
                            content = replaceExistingImages(imgDir, objUniqueId, ((CloudBooksActivity)Context).db, content, Integer.parseInt(objID),lastBookId);
                        }
                       // content=Changedcontent;
                        content = content.replace("'", "''");
                       // copyImagesForQuizWebtext(content);
                       // objContent = objContent.replace("'", "''");
                    }else  if (objType.equals("WebText")) {
                        content = content.replace("'", "''");
                    }else if (objType.equals("WebTable")) {
                        content = content.replace("'", "''");
                    }
                    // }
                    int objBID = lastBookId,objSequentialId = 0;
                    DatabaseHandler db = null;
                    if(Context instanceof MainActivity) {
                        db=((MainActivity)Context).db;
                        objSequentialId = ((MainActivity)Context).db.getMaximumSequentialIdValue(i, objBID) + 1;
                    }else if(Context instanceof CloudBooksActivity) {
                        db=((CloudBooksActivity)Context).db;
                        objSequentialId = ((CloudBooksActivity)Context).db.getMaximumSequentialIdValue(i, objBID) + 1;
                    }

                    String objSclaPageToFit = "no";
                    boolean objLocked = false;

                    //int objSequentialId = page.objectListArray.size() + 1;
                    //String objSclaPageToFit = "no";
                    //boolean objLocked = webRL.isObjLocked();
                    db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + objType + "', '" + objBID + "', '" + Integer.parseInt(pageNo) + "', '" + location + "', '" + size + "', '" + content + "', '" + objSequentialId + "', '" + objSclaPageToFit + "', '" + objLocked + "', '" + 0 + "')");


                    //  JSONObject jsonObjEnr = new JSONObject(enrJsonContent);
                }
//                        String absPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
//                        try {
//
//                            UserFunctions.copyDirectory(new java.io.File(Globals.TARGET_BASE_BOOKS_DIR_PATH + lastBookId), new java.io.File(absPath + lastBookId));
//                            UserFunctions.copyFiles(ctx.getFilesDir().getParentFile().getPath() + "/databases/sboookAuthor.sqlite" + lastBookId, absPath + "sboookAuthor.sqlite");
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (textCircularProgressBar!=null){
                textCircularProgressBar.setProgress(values[0]);
                textCircularProgressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            db.executeQuery("update books set isDownloadedCompleted='downloaded'where BID ='" + downLoadBook.getBookID() + "'");
            if (textCircularProgressBar!=null){
                textCircularProgressBar.setVisibility(View.INVISIBLE);
                if(Context instanceof MainActivity) {
                    ((MainActivity) Context).loadGridView();
                }else  if(Context instanceof CloudBooksActivity) {
                    ((CloudBooksActivity) Context).gridShelf.bookListArray.add(0,downLoadBook);
                    //((MainActivity) ((CloudBooksActivity) Context).SlideMenuActivity.getCurrentActivity()).loadGridView();
                }
            }
            if (list.size() > 0) {
                list.remove(0);
//                for(int i=0;i<cloudEnrList.size();i++){
//                    HashMap<String, String> cloud=cloudEnrList.get(i);
//                    HashMap<String, String> map= selectedCloudEnrList.get(0);
//                    String jsonContent = cloud.get("ECONTENT");
//                    String selected = map.get("ECONTENT");
//                    if(selected.equals(jsonContent)){
                // cloudEnrList.remove(0);
                getSelectedCloudEnrList().remove(0);
                // getCloudEnrList().remove(0);
                DatabaseHandler db = null;
                if (Context instanceof MainActivity) {
                    db = DatabaseHandler.getInstance(((MainActivity) Context));
                }else if (Context instanceof CloudBooksActivity) {
                    db = DatabaseHandler.getInstance(((CloudBooksActivity) Context));
                }
                db.executeQuery("insert into CloudObjects(objID)values('" + lastBookId + "')");

//                    }
//                }
                //cloudEnrList.remove(0);
                //selectedCloudEnrList.remove(0);
                getString().remove(0);
                SharedPreferences sharedPreference = null;
                if (Context instanceof MainActivity) {
                    sharedPreference= PreferenceManager.getDefaultSharedPreferences(((MainActivity) Context));
                }else if (Context instanceof CloudBooksActivity) {
                    sharedPreference= PreferenceManager.getDefaultSharedPreferences(((CloudBooksActivity) Context));
                }

                SharedPreferences.Editor editor = sharedPreference.edit();
                editor.remove("jsonString");
                editor.remove("selectedCloudList");
                editor.commit();

                if (getString().size() > 0) {
                    SharedPreferences sharedPref;
                    if (Context instanceof MainActivity) {
                        sharedPref = PreferenceManager.getDefaultSharedPreferences(((MainActivity) Context));
                    }else if (Context instanceof CloudBooksActivity) {
                        sharedPref = PreferenceManager.getDefaultSharedPreferences(((CloudBooksActivity) Context));
                    }

                    SharedPreferences.Editor edit = sharedPreference.edit();
                    try {
                        editor.putString("selectedCloudList", ObjectSerializer.serialize(getSelectedCloudEnrList()));
                        editor.putString("jsonString", ObjectSerializer.serialize(getString()));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    editor.commit();
                }
                if (list.size() > 0) {
                    final Book bok = list.get(0);
                    setDownLoadBook(bok);
                    setDownloadTaskRunning(true);
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new downloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bok.getDownloadURL());
                        }
                    }, 2000);
                }
            }else{
                setDownloadTaskRunning(false);
            }
        }

    }

    public String  imagePath(String srcPath, int objUniqueId, DatabaseHandler db, String cont, int fromId) {
        String content = cont;
        String changedContent;
        // String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + fromId + "/" + "images/";

        java.io.File srcDir = new java.io.File(srcPath);
        java.io.File destDir = new java.io.File(srcPath);
        //String imgPath ="q"+sectionId+"_img_"+uniqueId+""+".png";
        String filePath = null;
        int sectionId = 0, prevSectionId = 0, uniqueId = 0;
        //String[] data=content.split()

        //String[] splitStr = content.split("src");
        if (content.contains("src")) {
            String[] splitStr = content.split("src");
            if (splitStr.length > 1) {
                for (int i = 0; i < splitStr.length; i++) {
                    //String s=splitStr[i];
                    if (i > 0) {
                        String[] path1 = splitStr[i].split("\"");
                        String[] folderPath = path1[1].split("/");
                        String imgName = folderPath[folderPath.length - 1];
                        String Bid = folderPath[6];
                        System.out.println(imgName);
                        if (imgName.contains("_img_a_")) {
                            db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                            sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
                            String[] split = imgName.split("_img_a_");
                            String s = split[0].replace("q", "");
                            String[] p = split[1].split("\\.");
                            uniqueId = Integer.parseInt(p[0]);
                            prevSectionId = Integer.parseInt(s);

                            filePath = "q" + sectionId + "_img_a_" + objUniqueId + "" + ".png";
                            String fromPath = srcDir + "/" + imgName;
                            String toCopyPath = destDir + "/" + filePath;
                            java.io.File ExistingPath = new java.io.File(fromPath);
                            java.io.File newPath = new java.io.File(toCopyPath);
                            ExistingPath.renameTo(newPath);
                            content = content.replace(Globals.TARGET_BASE_BOOKS_DIR_PATH + Bid + "/" + imgName, toCopyPath);

                        } else if (imgName.contains("_img_b_")) {
//                            String[] split = imgName.split("_img_b_");
//                            String s = split[0].replace("q", "");
//                            String[] p = split[1].split("\\.");
//                            uniqueId = Integer.parseInt(p[0]);
//                            prevSectionId = Integer.parseInt(s);
//
//                            filePath = "q" + sectionId + "_img_b_" + objUniqueId + "" + ".png";
//                            String fromPath = srcDir + "/" + imgName;
//                            String toCopyPath = destDir + "/" + filePath;
//                            java.io.File ExistingPath = new java.io.File(fromPath);
//                            java.io.File newPath = new java.io.File(toCopyPath);
//                            ExistingPath.renameTo(newPath);
//                            content = content.replace(Globals.TARGET_BASE_BOOKS_DIR_PATH + Bid + "/" + imgName, toCopyPath);
                            content = ChangeFileNames(content, imgName, "_img_b_", sectionId, srcPath, Bid, objUniqueId);
                        } else if (imgName.contains("_img_c_")) {
//                            String[] split = imgName.split("_img_c_");
//                            String s = split[0].replace("q", "");
//                            String[] p = split[1].split("\\.");
//                            uniqueId = Integer.parseInt(p[0]);
//                            prevSectionId = Integer.parseInt(s);
//
//                            filePath = "q" + sectionId + "_img_c_" + objUniqueId + "" + ".png";
//                            String fromPath = srcDir + "/" + imgName;
//                            String toCopyPath = destDir + "/" + filePath;
//                            java.io.File ExistingPath = new java.io.File(fromPath);
//                            java.io.File newPath = new java.io.File(toCopyPath);
//                            ExistingPath.renameTo(newPath);
//                            content = content.replace(Globals.TARGET_BASE_BOOKS_DIR_PATH + Bid + "/" + imgName, toCopyPath);
                            content = ChangeFileNames(content, imgName, "_img_c_", sectionId, srcPath, Bid, objUniqueId);
                        } else if (imgName.contains("_img_d_")) {
//                            String[] split = imgName.split("_img_d_");
//                            String s = split[0].replace("q", "");
//                            String[] p = split[1].split("\\.");
//                            uniqueId = Integer.parseInt(p[0]);
//                            prevSectionId = Integer.parseInt(s);
//
//                            filePath = "q" + sectionId + "_img_c_" + objUniqueId + "" + ".png";
//                            String fromPath = srcDir + "/" + imgName;
//                            String toCopyPath = destDir + "/" + filePath;
//                            java.io.File ExistingPath = new java.io.File(fromPath);
//                            java.io.File newPath = new java.io.File(toCopyPath);
//                            ExistingPath.renameTo(newPath);
//                            content = content.replace(Globals.TARGET_BASE_BOOKS_DIR_PATH + Bid + "/" + imgName, toCopyPath);
                            content = ChangeFileNames(content, imgName, "_img_d_", sectionId, srcPath, Bid, objUniqueId);
                        } else if (imgName.contains("_img_e_")) {
                            content = ChangeFileNames(content, imgName, "_img_e_", sectionId, srcPath, Bid, objUniqueId);

                        } else if (imgName.contains("_img_f_")) {
                            content = ChangeFileNames(content, imgName, "_img_f_", sectionId, srcPath, Bid, objUniqueId);
                        } else if (imgName.contains("_img_")) {
                            db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                            sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
                            content = ChangeFileNames(content, imgName, "_img_", sectionId, srcPath, Bid, objUniqueId);
                        }


                    }

                }
            }
        } else {
            if (content.contains("quiz_1")) {
                String[] splitStr = content.split("<div class=\"quiz_1\"");

                if (splitStr.length > 1) {
                    for (int i = 0; i < splitStr.length; i++) {
                        String s=splitStr[0];
                        if(i>0) {
                            String[] path1 = splitStr[i].split("style");
                            if(path1[0].contains("q")) {
                                String p=path1[0].replace("id=","");
                                String[] folderPath = p.split("_");
                                //line1.replace("\"", "");
                                folderPath[0]=folderPath[0].replace("\"", "");
                                String prevsectionId = folderPath[0].replace("q","");
                                folderPath[1]=folderPath[1].replace("\"", "");
                                String prevUniqueid = folderPath[1];

                               // prevSectionId=Integer.parseInt(prevsectionId);
                               // uniqueId=Integer.parseInt(prevUniqueid);
                                db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                                sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
//                                content=content.replace(prevsectionId,String.valueOf(sectionId));
//                                content=content.replace(prevUniqueid,String.valueOf(objUniqueId));

                                content = content.replace(prevsectionId+"_"+prevUniqueid,sectionId+"_"+objUniqueId);
                                content = content.replace("_"+uniqueId+"_"+prevsectionId,"_"+objUniqueId+"_"+sectionId);
                                content = content.replace("q"+prevsectionId+"_result_"+prevUniqueid,"q"+sectionId+"_result_"+objUniqueId);
                                content = content.replace("q"+prevsectionId,"q"+sectionId);
                                content = content.replace("_"+prevUniqueid,"_"+objUniqueId);
                                content = content.replace("'btnCheckAnswer"+prevsectionId,"'btnCheckAnswer"+sectionId);
                                content = content.replace("Title"+prevsectionId,"Title"+sectionId);
                                content = content.replace("_"+prevUniqueid+":"+prevsectionId,"_"+objUniqueId+":"+sectionId);

                            }
                        }

                    }
                }

//                db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
//                sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
//                String[] splitStr = content.split("id");
//                String data[] = splitStr[1].split("style");

            }

        }
        return content;
    }

     private String ChangeFileNames(String data,String imgName,String SplitContent,int sectionId,String srcDir,String Bid,int objUniqueId){
         String content=data;
         String[] split = imgName.split(SplitContent);
         String s = split[0].replace("q", "");
         String[] p = split[1].split("\\.");
         int uniqueId = Integer.parseInt(p[0]);
         int prevSectionId = Integer.parseInt(s);

         String filePath = "q" + sectionId + SplitContent + objUniqueId + "" + ".png";
         String fromPath = srcDir + "/" + imgName;
         String toCopyPath = srcDir + "/" + filePath;
         java.io.File ExistingPath = new java.io.File(fromPath);
         java.io.File newPath = new java.io.File(toCopyPath);
         ExistingPath.renameTo(newPath);
         content=content.replace( Globals.TARGET_BASE_BOOKS_DIR_PATH +Bid+ "/" + imgName,toCopyPath);
         if (imgName.contains("_img_f_")||imgName.contains("_img_")) {
             content = content.replace(prevSectionId+"_"+uniqueId,sectionId+"_"+objUniqueId);
             content = content.replace("_"+uniqueId+"_"+prevSectionId,"_"+objUniqueId+"_"+sectionId);
             content = content.replace("q"+prevSectionId+"_result_"+uniqueId,"q"+sectionId+"_result_"+objUniqueId);
             content = content.replace("q"+prevSectionId,"q"+sectionId);
             content = content.replace("_"+uniqueId,"_"+objUniqueId);
             content = content.replace("'btnCheckAnswer"+prevSectionId,"'btnCheckAnswer"+sectionId);
             content = content.replace("Title"+prevSectionId,"Title"+sectionId);
             content = content.replace("_"+uniqueId+":"+prevSectionId,"_"+objUniqueId+":"+sectionId);

//             content=content.replace(String.valueOf(uniqueId),String.valueOf(objUniqueId));
//             content=content.replace(String.valueOf(prevSectionId),String.valueOf(sectionId));
         }
         return content;
     }


    /*public String replaceExistingImages(String srcPath, int objUniqueId, DatabaseHandler db, String cont, int fromId,int newBookId) {
        String content = cont;
        String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + fromId + "/" + "images/";

        File srcDir = new File(srcPath);
        File destDir = new File(srcPath);
        //String imgPath ="q"+sectionId+"_img_"+uniqueId+""+".png";
        String filePath = null;
        int sectionId = 0, prevSectionId = 0, uniqueId = 0;
        String[] path = srcDir.list();
        for (String file : path) {
            if (file.contains("q")) {

                if (file.contains("_img_a_")) {
                    db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                    sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
                    String[] split = file.split("_img_a_");
                    String s = split[0].replace("q","");
                    String[] p = split[1].split("\\.");
                    uniqueId = Integer.parseInt(p[0]);
                    prevSectionId = Integer.parseInt(s);

                    filePath = "q" + sectionId + "_img_a_" + objUniqueId + "" + ".png";

                } else if (file.contains("_img_b_")) {
                    if(sectionId==0){
                        db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                        sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
                    }
                    filePath = "q" + sectionId + "_img_b_" + objUniqueId + "" + ".png";

                } else if (file.contains("_img_c_")) {
                    if(sectionId==0){
                        db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                        sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
                    }
                    filePath = "q" + sectionId + "_img_c_" + objUniqueId + "" + ".png";
                } else if (file.contains("_img_d_")) {
                    if(sectionId==0){
                        db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                        sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
                    }
                    filePath = "q" + sectionId + "_img_d_" + objUniqueId + "" + ".png";
                } else if (file.contains("_img_e_")) {
                    if(sectionId==0){
                        db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                        sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
                    }
                    filePath = "q" + sectionId + "_img_e_" + objUniqueId + "" + ".png";
                } else if (file.contains("_img_f_")) {
                    if(sectionId==0){
                        db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                        sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
                    }
                    filePath = "q" + sectionId + "_img_f_" + objUniqueId + "" + ".png";
                }else if (file.contains("_img_")) {
                    String[] split = file.split("_img_");
                    String s = split[0].replace("q","");
                    String[] p = split[1].split("\\.");
                    uniqueId = Integer.parseInt(p[0]);
                    prevSectionId = Integer.parseInt(s);

                    db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                    sectionId = db.getMaxUniqueRowID("tblMultiQuiz");

                    filePath = "q" + sectionId + "_img_" + objUniqueId + "" + ".png";
                }

                String fromPath = srcDir + "/" + file;
                String toCopyPath = destDir + "/" + filePath;

                File ExistingPath = new File(fromPath);
                File newPath = new File(toCopyPath);
                ExistingPath.renameTo(newPath);
                if (content.contains(imgDir)) {
                    content = content.replace(imgDir, srcPath);
                }
               if(content.contains(prevSectionId+"")||content.contains(uniqueId+"")) {

                   content = content.replace("previousquestion:q" + prevSectionId + "_" + uniqueId + ":" + prevSectionId + "", "previousquestion:q" + sectionId + "_" + objUniqueId + ":" + sectionId + "");
                   content = content.replace("deletequestion:q" + prevSectionId + "_" + uniqueId + ":" + prevSectionId + "", "deletequestion:q" + sectionId + "_" + objUniqueId + ":" + sectionId + "");
                   content = content.replace("addquestions:q" + prevSectionId + "_" + uniqueId + ":" + prevSectionId + "", "addquestions:q" + sectionId + "_" + objUniqueId + ":" + sectionId + "");
                   content = content.replace("nextquestion:q" + prevSectionId + "_" + uniqueId + ":" + prevSectionId + "", "nextquestion:q" + sectionId + "_" + objUniqueId + ":" + sectionId + "");
                   content = content.replace("q" + prevSectionId + "_" + uniqueId + "", "q" + sectionId + "_" + objUniqueId + "");
                   content = content.replace("Title" + prevSectionId + "", "Title" + sectionId + "");
                   content = content.replace("quizTitle_" + uniqueId + "", "quizTitle_" + objUniqueId + "");
                   content = content.replace("pTitle_" + uniqueId + "", "pTitle_" + objUniqueId + "");
                   content = content.replace("quiz_question_" + uniqueId + "", "quiz_question_" + objUniqueId + "");
                   content = content.replace("pQuiz_" + uniqueId + "", "pQuiz_" + objUniqueId + "");
                   content = content.replace("quiz_options_" + uniqueId + "", "quiz_options_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_result_" + uniqueId + "", "q" + sectionId + "_result_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_div_a_" + uniqueId + "", "q" + sectionId + "_div_a_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "a_" + uniqueId + "", "q" + sectionId + "a_" + objUniqueId + "");
                   content = content.replace("btnCheckAnswer" + prevSectionId + "_" + uniqueId + "", "btnCheckAnswer" + sectionId + "_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_div_b_" + uniqueId + "", "q" + sectionId + "_div_b_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "b_" + uniqueId + "", "q" + sectionId + "b_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_div_c_" + uniqueId + "", "q" + sectionId + "_div_c_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "c_" + uniqueId + "", "q" + sectionId + "c_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_div_d_" + uniqueId + "", "q" + sectionId + "_div_d_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "d_" + uniqueId + "", "q" + sectionId + "d_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_div_e_" + uniqueId + "", "q" + sectionId + "_div_e_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "e_" + uniqueId + "", "q" + sectionId + "e_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_div_f_" + uniqueId + "", "q" + sectionId + "_div_f_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "f_" + uniqueId + "", "q" + sectionId + "f_" + objUniqueId + "");
                   content = content.replace("quiz_checkanswer_" + uniqueId + "", "quiz_checkanswer_" + objUniqueId + "");
                   content = content.replace("btnNext" + prevSectionId + "_" + uniqueId + "", "btnNext" + sectionId + "_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_img_" + uniqueId + "", "q" + sectionId + "_img_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_img_a_" + uniqueId + "", "q" + sectionId + "_img_a_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_img_b_" + uniqueId + "", "q" + sectionId + "_img_b_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_img_c_" + uniqueId + "", "q" + sectionId + "_img_c_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_img_d_" + uniqueId + "", "q" + sectionId + "_img_d_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_img_e_" + uniqueId + "", "q" + sectionId + "_img_e_" + objUniqueId + "");
                   content = content.replace("q" + prevSectionId + "_img_f_" + uniqueId + "", "q" + sectionId + "_img_f_" + objUniqueId + "");
                   content = content.replace("results_container_" + uniqueId + "", "results_container_" + objUniqueId + "");
                   content = content.replace("results_" + uniqueId + "", "results_" + objUniqueId + "");
                   content = content.replace("quiz_result_cont_" + uniqueId + "", "quiz_result_cont_" + objUniqueId + "");
                   content = content.replace("/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + fromId + "/", "/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + newBookId + "/");
               }
            }

          }
       if(cont.contains("quiz_1")){
            db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
            sectionId = db.getMaxUniqueRowID("tblMultiQuiz");

            if(content.contains(prevSectionId+"")||content.contains(uniqueId+"")) {

                content = content.replace("previousquestion:q"+prevSectionId+"_"+uniqueId+":"+prevSectionId+"", "previousquestion:q"+sectionId+"_"+objUniqueId+":"+sectionId+"");
                content = content.replace("deletequestion:q"+prevSectionId+"_"+uniqueId+":"+prevSectionId+"", "deletequestion:q"+sectionId+"_"+objUniqueId+":"+sectionId+"");
                content = content.replace("addquestions:q"+prevSectionId+"_"+uniqueId+":"+prevSectionId+"", "addquestions:q"+sectionId+"_"+objUniqueId+":"+sectionId+"");
                content = content.replace("nextquestion:q"+prevSectionId+"_"+uniqueId+":"+prevSectionId+"", "nextquestion:q"+sectionId+"_"+objUniqueId+":"+sectionId+"");
                content = content.replace("q"+prevSectionId+"_"+uniqueId+"", "q"+sectionId+"_"+objUniqueId+"");
                content = content.replace("Title"+prevSectionId+"", "Title"+sectionId+"");
                content = content.replace("quizTitle_"+uniqueId+"", "quizTitle_"+objUniqueId+"");
                content = content.replace("pTitle_"+uniqueId+"", "pTitle_"+objUniqueId+"");
                content = content.replace("quiz_question_"+uniqueId+"", "quiz_question_"+objUniqueId+"");
                content = content.replace("pQuiz_"+uniqueId+"", "pQuiz_"+objUniqueId+"");
                content = content.replace("quiz_options_"+uniqueId+"", "quiz_options_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_result_"+uniqueId+"", "q"+sectionId+"_result_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_a_"+uniqueId+"", "q"+sectionId+"_div_a_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"a_"+uniqueId+"", "q"+sectionId+"a_"+objUniqueId+"");
                content = content.replace("btnCheckAnswer"+prevSectionId+"_"+uniqueId+"", "btnCheckAnswer"+sectionId+"_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_b_"+uniqueId+"", "q"+sectionId+"_div_b_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"b_"+uniqueId+"", "q"+sectionId+"b_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_c_"+uniqueId+"", "q"+sectionId+"_div_c_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"c_"+uniqueId+"", "q"+sectionId+"c_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_d_"+uniqueId+"", "q"+sectionId+"_div_d_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"d_"+uniqueId+"", "q"+sectionId+"d_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_e_"+uniqueId+"", "q"+sectionId+"_div_e_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"e_"+uniqueId+"", "q"+sectionId+"e_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_f_"+uniqueId+"", "q"+sectionId+"_div_f_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"f_"+uniqueId+"", "q"+sectionId+"f_"+objUniqueId+"");
                content = content.replace("quiz_checkanswer_"+uniqueId+"", "quiz_checkanswer_"+objUniqueId+"");
                content = content.replace("btnNext"+prevSectionId+"_"+uniqueId+"", "btnNext"+sectionId+"_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_img_"+uniqueId+"", "q"+sectionId+"_img_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_img_a_"+uniqueId+"", "q"+sectionId+"_img_a_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_img_b_"+uniqueId+"", "q"+sectionId+"_img_b_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_img_c_"+uniqueId+"", "q"+sectionId+"_img_c_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_img_d_"+uniqueId+"", "q"+sectionId+"_img_d_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_img_e_"+uniqueId+"", "q"+sectionId+"_img_e_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_img_f_"+uniqueId+"", "q"+sectionId+"_img_f_"+objUniqueId+"");
                content = content.replace("results_container_"+uniqueId+"", "results_container_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_2_a_"+uniqueId+"", "q"+sectionId+"_div_2_a_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_1_a_"+uniqueId+"", "q"+sectionId+"_div_1_a_"+objUniqueId+"");

                content = content.replace("q"+prevSectionId+"_div_2_b_"+uniqueId+"", "q"+sectionId+"_div_2_b_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_1_b_"+uniqueId+"", "q"+sectionId+"_div_1_b_"+objUniqueId+"");

                content = content.replace("q"+prevSectionId+"_div_2_c_"+uniqueId+"", "q"+sectionId+"_div_2_c_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_1_c_"+uniqueId+"", "q"+sectionId+"_div_1_c_"+objUniqueId+"");

                content = content.replace("q"+prevSectionId+"_div_2_d_"+uniqueId+"", "q"+sectionId+"_div_2_d_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_1_d_"+uniqueId+"", "q"+sectionId+"_div_1_d_"+objUniqueId+"");

                content = content.replace("q"+prevSectionId+"_div_2_e_"+uniqueId+"", "q"+sectionId+"_div_2_e_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_1_e_"+uniqueId+"", "q"+sectionId+"_div_1_e_"+objUniqueId+"");

                content = content.replace("q"+prevSectionId+"_div_2_f_"+uniqueId+"", "q"+sectionId+"_div_2_f_"+objUniqueId+"");
                content = content.replace("q"+prevSectionId+"_div_1_f_"+uniqueId+"", "q"+sectionId+"_div_1_f_"+objUniqueId+"");

                content = content.replace("results_"+uniqueId+"", "results_"+objUniqueId+"");
                content = content.replace("quiz_result_cont_"+uniqueId+"", "quiz_result_cont_"+objUniqueId+"");
                content=content.replace("/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + fromId + "/", "/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + newBookId + "/");


            }
        }

        content = content.replace("'", "''");
        return content;

    }  */

    public String replaceExistingImages(String srcPath, int objUniqueId, DatabaseHandler db, String cont, int fromId,int newBookId) {
        ArrayList<String> quizContent=new ArrayList<String>();
        String con= null;
        String Content=cont;
        if (Content.contains("id")) {

            //String id=value.split();
//				}
            String d="quizNextView";
            String [] Split=Content.split(d);
            for(int i=0;i<Split.length-1;i++) {
                String s=Split[i];
                if(i!=0){
                    String[] dt=Split[i].split("<div class=");
                    // String dd=dt[0].replace("\"", "").trim();;
                    //String endPart=d.concat(dt[0]);
                    s=s.replace(dt[0],"");
                }
                System.out.println(Split[i]);
                String quiz=null;
                if(i==Split.length-2){
                    String[] data = Split[i + 1].split("<div class=");
                   // String dd = data[0].replace("\"", "");
                   // String endPart = d.concat(dd);
                    quiz = s.concat(Split[i + 1]);
                }else {
                    String[] data = Split[i + 1].split("<div class=");
                   // String dd = data[0].replace("\"", "");
                    String dd = data[0];
                    String endPart = d.concat(dd);
                    quiz = s.concat(endPart);
                }
              //  System.out.println(data.length);
                String[] splitStr = quiz.split("id=");

                if (splitStr.length > 1) {
//				for (int i = 0; i < splitStr.length; i++) {
                    //String[] path1 = splitStr[3].split("style");
                    String[] path1;
                    if(i==0){
                        path1 = splitStr[3].split("style");
                    }else{
                        path1 = splitStr[2].split("style");
                    }

                    String id = path1[0].replace("\"", "");
                    ;
                    String[] value = id.split("_");
                    String uniqueId = value[1].replace("\"", "").trim();
                    String sectionId = value[2].replace("\"", "").trim();
                    uniqueId=uniqueId.replace("\\s","");
                    sectionId=sectionId.replace("\\s","");
                    int h=Integer.parseInt(sectionId);
                    System.out.println(value);


                   // int latestsectionId=2;
                   // int latestUniqueId=3;
                    db.executeQuery("insert into tblMultiQuiz(quizId) values('" + objUniqueId + "')");
                    int latestsectionId = db.getMaxUniqueRowID("tblMultiQuiz");
                    if (quiz.contains("src")) {
                        quiz= images(quiz,newBookId,srcPath,objUniqueId,latestsectionId,fromId);
                    }
                    //quiz = quiz.replace("\"", "");
                    quiz = quiz.replace("previousquestion:q"+sectionId+"_"+uniqueId+":"+sectionId+"", "previousquestion:q"+latestsectionId+"_"+objUniqueId+":"+latestsectionId+"");
                    quiz = quiz.replace("deletequestion:q"+sectionId+"_"+uniqueId+":"+sectionId+"", "deletequestion:q"+latestsectionId+"_"+objUniqueId+":"+latestsectionId+"");
                    quiz = quiz.replace("addquestions:q"+sectionId+"_"+uniqueId+":"+sectionId+"", "addquestions:q"+latestsectionId+"_"+objUniqueId+":"+latestsectionId+"");
                    quiz = quiz.replace("nextquestion:q"+sectionId+"_"+uniqueId+":"+sectionId+"", "nextquestion:q"+latestsectionId+"_"+objUniqueId+":"+latestsectionId+"");
                    quiz = quiz.replace("q"+sectionId+"_"+uniqueId+"", "q"+latestsectionId+"_"+objUniqueId+"");
                    quiz = quiz.replace("Title"+sectionId+"", "Title"+latestsectionId+"");

                    quiz = quiz.replace("quizTitle_"+uniqueId+"_"+sectionId+"", "quizTitle_"+objUniqueId+"_"+latestsectionId+"");
                    quiz = quiz.replace("quizTitle_"+uniqueId+"", "quizTitle_"+objUniqueId+"");
                    quiz = quiz.replace("pTitle_"+uniqueId+"", "pTitle_"+objUniqueId+"");

                    quiz = quiz.replace("quiz_question_"+uniqueId+"_"+sectionId+"", "quiz_question_"+objUniqueId+"_"+latestsectionId+"");
                    quiz = quiz.replace("quiz_question_"+uniqueId+"", "quiz_question_"+objUniqueId+"");
                    quiz = quiz.replace("pQuiz_"+uniqueId+"", "pQuiz_"+objUniqueId+"");
                    quiz = quiz.replace("quiz_options_"+uniqueId+"_"+sectionId+"", "quiz_options_"+objUniqueId+"_"+latestsectionId+"");

                    quiz = quiz.replace("quiz_options_"+uniqueId+"", "quiz_options_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_result_"+uniqueId+"", "q"+latestsectionId+"_result_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_a_"+uniqueId+"", "q"+latestsectionId+"_div_a_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"a_"+uniqueId+"", "q"+latestsectionId+"a_"+objUniqueId+"");
                    quiz = quiz.replace("btnCheckAnswer"+sectionId+"_"+uniqueId+"", "btnCheckAnswer"+latestsectionId+"_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_b_"+uniqueId+"", "q"+latestsectionId+"_div_b_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"b_"+uniqueId+"", "q"+latestsectionId+"b_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_c_"+uniqueId+"", "q"+latestsectionId+"_div_c_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"c_"+uniqueId+"", "q"+latestsectionId+"c_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_d_"+uniqueId+"", "q"+latestsectionId+"_div_d_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"d_"+uniqueId+"", "q"+latestsectionId+"d_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_e_"+uniqueId+"", "q"+latestsectionId+"_div_e_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"e_"+uniqueId+"", "q"+latestsectionId+"e_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_f_"+uniqueId+"", "q"+latestsectionId+"_div_f_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"f_"+uniqueId+"", "q"+latestsectionId+"f_"+objUniqueId+"");
                    quiz = quiz.replace("quiz_checkanswer_"+uniqueId+"", "quiz_checkanswer_"+objUniqueId+"");
                    quiz = quiz.replace("btnNext"+sectionId+"_"+uniqueId+"", "btnNext"+latestsectionId+"_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_img_"+uniqueId+"", "q"+latestsectionId+"_img_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_img_a_"+uniqueId+"", "q"+latestsectionId+"_img_a_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_img_b_"+uniqueId+"", "q"+latestsectionId+"_img_b_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_img_c_"+uniqueId+"", "q"+latestsectionId+"_img_c_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_img_d_"+uniqueId+"", "q"+latestsectionId+"_img_d_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_img_e_"+uniqueId+"", "q"+latestsectionId+"_img_e_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_img_f_"+uniqueId+"", "q"+latestsectionId+"_img_f_"+objUniqueId+"");

                    quiz = quiz.replace("q"+sectionId+"_div_2_a_"+uniqueId+"", "q"+latestsectionId+"_div_2_a_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_1_a_"+uniqueId+"", "q"+latestsectionId+"_div_1_a_"+objUniqueId+"");

                    quiz = quiz.replace("q"+sectionId+"_div_2_b_"+uniqueId+"", "q"+latestsectionId+"_div_2_b_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_1_b_"+uniqueId+"", "q"+latestsectionId+"_div_1_b_"+objUniqueId+"");

                    quiz = quiz.replace("q"+sectionId+"_div_2_c_"+uniqueId+"", "q"+latestsectionId+"_div_2_c_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_1_c_"+uniqueId+"", "q"+latestsectionId+"_div_1_c_"+objUniqueId+"");

                    quiz = quiz.replace("q"+sectionId+"_div_2_d_"+uniqueId+"", "q"+latestsectionId+"_div_2_d_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_1_d_"+uniqueId+"", "q"+latestsectionId+"_div_1_d_"+objUniqueId+"");

                    quiz = quiz.replace("q"+sectionId+"_div_2_e_"+uniqueId+"", "q"+latestsectionId+"_div_2_e_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_1_e_"+uniqueId+"", "q"+latestsectionId+"_div_1_e_"+objUniqueId+"");

                    quiz = quiz.replace("q"+sectionId+"_div_2_f_"+uniqueId+"", "q"+latestsectionId+"_div_2_f_"+objUniqueId+"");
                    quiz = quiz.replace("q"+sectionId+"_div_1_f_"+uniqueId+"", "q"+latestsectionId+"_div_1_f_"+objUniqueId+"");


                    quiz = quiz.replace("results_container_"+uniqueId+"", "results_container_"+objUniqueId+"");
                    quiz = quiz.replace("results_"+uniqueId+"", "results_"+objUniqueId+"");
                    quiz = quiz.replace("quiz_result_cont_"+uniqueId+"", "quiz_result_cont_"+objUniqueId+"");
                    System.out.println(quiz);
                    //quiz=quiz.replace("/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + BID + "/", "/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + newBookId + "/");


                }
                quizContent.add(quiz);

            }
            System.out.println(quizContent.size());

            for(int i=0;i<quizContent.size();i++){
                if(i==0){
                    con=quizContent.get(i);
                }else{
                    String data=quizContent.get(i);
                    con=con.concat(data);
                }
            }
            //con = con.replace("'", "''");
            System.out.println(con);


        }
        return  con;
    }


    public String images(String quiz, int newBookId, String srcPath, int objUniqueId, int latestsectionId,int fromId){
        String content=quiz;
        if (content.contains("src")) {
            String[] splitStr = content.split("src=");
            if (splitStr.length > 0) {
                for(int i=0;i<splitStr.length;i++) {
                    if(i!=0) {
//						if(splitStr[i].contains("onclick"))
                        String[] path1 = splitStr[i].split("onclick");
                        if(path1[0].contains(".png")) {
                            String value = path1[0].replace("\"", "");
                            String[] imagePath= path1[0].split("/");
                            String[] Path=imagePath[imagePath.length-1].split("\\?");
                            System.out.println(imagePath.length);
                            String filePath = null;
                            if (Path[0].contains("_img_a_")) {
                                filePath = "q" + latestsectionId + "_img_a_" + objUniqueId + "" + ".png";

                            } else if (Path[0].contains("_img_b_")) {

                                filePath = "q" + latestsectionId + "_img_b_" + objUniqueId + "" + ".png";

                            } else if (Path[0].contains("_img_c_")) {

                                filePath = "q" + latestsectionId + "_img_c_" + objUniqueId + "" + ".png";
                            } else if (Path[0].contains("_img_d_")) {

                                filePath = "q" + latestsectionId + "_img_d_" + objUniqueId + "" + ".png";
                            } else if (Path[0].contains("_img_e_")) {

                                filePath = "q" + latestsectionId + "_img_e_" + objUniqueId + "" + ".png";
                            } else if (Path[0].contains("_img_f_")) {

                                filePath = "q" + latestsectionId + "_img_f_" + objUniqueId + "" + ".png";
                            }else if (Path[0].contains("_img_")) {
                                 filePath = "q" + latestsectionId + "_img_" + objUniqueId + "" + ".png";
                            }

                            String fromPath = srcPath+ Path[0];
                            String toCopyPath = srcPath+ filePath;

                            File ExistingPath = new File(fromPath);
                            File newPath = new File(toCopyPath);
                            ExistingPath.renameTo(newPath);
                            content=content.replace("/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + fromId + "/", "/data/data/" + Globals.getCurrentProjPackageName() + "/files/books/" + newBookId + "/");
                            content=content.replace(Path[0],filePath);

                        }
                    }
                }
            }
        }
        return content;

    }
    public class downloadIJST extends AsyncTask<String,Integer,String>{

        String IJSTfileName = "";
        String existIJST = "";

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            URL urlToRequest = null;
            long bytes = 1024 * 1024;
            long total = 0;
            try {
                if (UserFunctions.isInternetExist(Context)) {
                    urlToRequest = new URL(params[0]);
                    urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                    urlConnection.setConnectTimeout(10000);
                    urlConnection.setReadTimeout(25000);

                    int statusCode = urlConnection.getResponseCode();
                    if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

                    } else if (statusCode != HttpURLConnection.HTTP_OK) {

                    }
                    InputStream in = urlConnection.getInputStream();

                    String returnResponse = UserFunctions.convertStreamToString(in);
                    if (returnResponse == null && returnResponse.equals("")) {
                        return null;
                    }
                    if (returnResponse.contains("\n")) {
                        returnResponse = returnResponse.replace("\n", "");
                    }
                    IJSTfileName = returnResponse;
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(Context);
                    existIJST = pref.getString(Globals.IJSTversion,"");
                    if (!IJSTfileName.equals(existIJST)) {
                        URL url = new URL("http://www.nooor.com/ijst/" + returnResponse);
                        //Open a connection to that URL.
                        URLConnection ucon = url.openConnection();
                        File newFile = new File((Globals.TARGET_BASE_FILE_PATH) + "IJST");
                        if (newFile.exists()) {
                            total = newFile.length();
                            ucon.setRequestProperty("Range", "bytes=" + total + "-");
                            ucon.connect();
                        } else {
                            ucon.setRequestProperty("Range", "bytes=" + total + "-");
                            ucon.connect();
                        }
                        int fileLength = ucon.getContentLength();
                        ////System.out.println("filelength:"+fileLength);
                        long startTime = System.currentTimeMillis();

                        //this timeout affects how long it takes for the app to realize there's a connection problem
                        ucon.setReadTimeout(5000);
                        ucon.setConnectTimeout(30000);
                        InputStream is = ucon.getInputStream();
                        BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
                        String bookPath = "IJST";
                        FileOutputStream outStream = (total == 0) ? Context.openFileOutput(bookPath, Context.MODE_PRIVATE) : Context.openFileOutput(bookPath, Context.MODE_APPEND);
                        byte[] buff = new byte[5 * 1024];
                        //Read bytes (and store them) until there is nothing more to read(-1)
                        int len;
                        while ((len = inStream.read(buff)) != -1) {
                            total += len;
                            publishProgress((int) ((total * 100) / fileLength));
                            outStream.write(buff, 0, len);
                        }
                        //clean up
                        outStream.flush();
                        outStream.close();
                        inStream.close();
                        downloadSuccess = true;
                    }
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                downloadSuccess = false;
            }
            return null;
        }
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            if (textCircularProgressBar!=null){
                textCircularProgressBar.setProgress(progress[0]);
                textCircularProgressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPostExecute(String s) {
           if (downloadSuccess){
               if (db==null) {
                   db = DatabaseHandler.getInstance(Context);
               }
               db.executeQuery("update books set isDownloadedCompleted='downloaded'where BID ='" + downLoadBook.getBookID() + "'");
               if (!IJSTfileName.equals(existIJST)) {
                   UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + "IJST");
                   Boolean unzipSuccess = unZipDownloadedFile(Globals.TARGET_BASE_FILE_PATH + "IJST", Globals.TARGET_BASE_BOOKS_DIR_PATH + "IJST");
                   File zipfile = new File(Globals.TARGET_BASE_FILE_PATH + "IJST");
                   zipfile.delete();
                   SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(Context);
                   SharedPreferences.Editor editor = pref.edit();
                   editor.putString(Globals.IJSTversion, IJSTfileName);
                   editor.commit();
               }
               if (textCircularProgressBar!=null){
                   textCircularProgressBar.setVisibility(View.INVISIBLE);
                   ((MainActivity) Context).loadGridView();
               }
               if (list.size() > 0) {
                   list.remove(0);
                   if (list.size() > 0) {
                       final Book bok = list.get(0);
                       setDownLoadBook(bok);
                       setDownloadTaskRunning(true);
                       new android.os.Handler().postDelayed(new Runnable() {
                           @Override
                           public void run() {
                               new downloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bok.getDownloadURL());
                           }
                       }, 2000);
                   }
               }else{
                   setDownloadTaskRunning(false);
               }
           }
        }
    }
    public Boolean unZipDownloadedFile(String zipfilepath, String unziplocation) {
        InputStream is;
        ZipInputStream zis;
        try {
            is = new FileInputStream(zipfilepath);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                String zipEntryName = ze.getName();
                //zipEntryName = zipEntryName.replace("\\", "/");
                ////System.out.println("zipEntryName: "+zipEntryName);
                File file = new File(unziplocation + "/" + zipEntryName);
                if (file.exists()) {
                    ////System.out.println("file exists at the given path:" +file);
                    byte[] buffer = new byte[1024];
                    FileOutputStream fout = new FileOutputStream(file);
                    BufferedOutputStream baos = new BufferedOutputStream(fout, 1024);
                    int count;
                    while ((count = zis.read(buffer, 0, 1024)) != -1) {
                        baos.write(buffer, 0, count);
                    }
                    baos.flush();
                    baos.close();
                } else {

                    if (ze.isDirectory()) {
                        ////System.out.println("Ze is an directory. It will create directory and exit");
                        file.mkdirs();
                        continue;
                    }

                    file.getParentFile().mkdirs();   //Horror line..............
                    byte[] buffer = new byte[1024];
                    FileOutputStream fout = new FileOutputStream(file);
                    BufferedOutputStream baos = new BufferedOutputStream(fout, 1024);
                    int count;
                    while ((count = zis.read(buffer, 0, 1024)) != -1) {
                        baos.write(buffer, 0, count);
                    }
                    baos.flush();
                    baos.close();
                }
            }
            zis.close();
            return true;
        } catch (Exception e) {
            ////System.out.println("unzipping error"+e);
            e.printStackTrace();
            return false;
        }

    }
}
