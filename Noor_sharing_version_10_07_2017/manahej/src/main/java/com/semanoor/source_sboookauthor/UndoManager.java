package com.semanoor.source_sboookauthor;

import java.util.ArrayList;

import com.semanoor.paint.PainterCanvas;

public class UndoManager {
	
	private ArrayList<UndoRedoObjectAction> currentUndoRedoActionCollection;
	private int currentScenePointer = 0;
	private Page page;
	private PainterCanvas paintCanvas;
	
	public UndoManager(Page page, PainterCanvas painterCanvas){
		currentUndoRedoActionCollection = new ArrayList<UndoRedoObjectAction>();
		if (page != null) {
			this.page = page;
			page.setUndoRedoBtnEnableOrDisable(false, false);
		} else if (painterCanvas != null) {
			this.paintCanvas = painterCanvas;
			paintCanvas.setUndoRedoBtnEnableOrDisable(false, false);
		}
	}
	
	/**
	 * This will add an action into current scene undo redo collection by indexing pointer value, and this will as well 
	 * check for the pointer position and manuplates the collection
	 * @param undoRedoObject
	 * 
	 */
	public void setUndoRedoAction(UndoRedoObjectAction undoRedoObject) {
		if (currentScenePointer != currentUndoRedoActionCollection.size()) {
			if (currentScenePointer%2 != 0) {
				currentScenePointer --;
			}
			splice(currentScenePointer);
		}
		currentUndoRedoActionCollection.add(currentScenePointer, undoRedoObject);
		currentScenePointer ++;
		enableDisableUndoRedo();
		filterUndoRedoCollection();
	}
	
	/**
	 * This will shift the pointer by decrementing each pointer value and performs each undone action 
	 * 
	 */
	public void shiftPointerOnUndo() {
		if (currentScenePointer > 0) {
			currentScenePointer --;
			if (currentScenePointer%2 == 0) {
				currentScenePointer --;
			}
		}
		performUndoRedoAction();
		enableDisableUndoRedo();
	}
	
	/**
	 * This will shift the pointer by incrementing each pointer value and performs each redone action  
	 * 
	 */
	public void shiftPointerOnRedo() {
		if (currentScenePointer <= currentUndoRedoActionCollection.size()) {
			currentScenePointer ++;
			if (currentScenePointer%2 != 0) {
				currentScenePointer ++;
			}
		}
		performUndoRedoAction();
		enableDisableUndoRedo();
	}
	
	/**
	 * this will splice the values from the specified position of array list
	 * @param pointerPosition
	 */
	private void splice(int pointerPosition){
		int size = currentUndoRedoActionCollection.size();
		for (int i = pointerPosition; i < size; i++) {
			currentUndoRedoActionCollection.remove(pointerPosition);
		}
	}
	
	/**
	 * this will filter undo and redo collection if goes beyond total undo and redo steps
	 */
	private void filterUndoRedoCollection() {
		if (currentUndoRedoActionCollection.size() > Globals.totalUndoRedoSteps) {
			currentUndoRedoActionCollection.remove(0);
			currentScenePointer = Globals.totalUndoRedoSteps;
		}
	}
	
	/**
	 * Performs an action on each undo/redo click
	 * 
	 */
	private void performUndoRedoAction() {
		int indexValue = (currentScenePointer > 0)?currentScenePointer-1:currentScenePointer;
		UndoRedoObjectAction undoRedoObject = currentUndoRedoActionCollection.get(indexValue);
		if (page != null) {
			page.performUndoRedoActioForObjects(undoRedoObject);
		} else if (paintCanvas != null) {
			paintCanvas.performUndoRedoActioForObjects(undoRedoObject);
		}
	}
	
	/**
	 * Disables/Enables the undo/redo icons 
	 * 
	 */
	private void enableDisableUndoRedo() {
		if (currentUndoRedoActionCollection.size() == 0) {
			if (page != null) {
				page.setUndoRedoBtnEnableOrDisable(false, false);
			} else if (paintCanvas != null) {
				paintCanvas.setUndoRedoBtnEnableOrDisable(false, false);
			}
		} else if (currentScenePointer >= currentUndoRedoActionCollection.size()) {
			if (page != null) {
				page.setUndoRedoBtnEnableOrDisable(true, false);
			} else if (paintCanvas != null) {
				paintCanvas.setUndoRedoBtnEnableOrDisable(true, false);
			}
		} else if (currentScenePointer <= 1) {
			if (page != null) {
				page.setUndoRedoBtnEnableOrDisable(false, true);
			} else if (paintCanvas != null) {
				paintCanvas.setUndoRedoBtnEnableOrDisable(false, true);
			}
		} else {
			if (page != null) {
				page.setUndoRedoBtnEnableOrDisable(true, true);
			} else if (paintCanvas != null) {
				paintCanvas.setUndoRedoBtnEnableOrDisable(true, true);
			}
		}
	}
	
}
