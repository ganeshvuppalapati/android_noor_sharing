package com.semanoor.source_sboookauthor;

import com.semanoor.manahij.R;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DirectoryTextViewListAdapter extends BaseAdapter {
	
	private Context context;
	private String[] names;

	public DirectoryTextViewListAdapter(Context context, String[] names) {
		this.context = context;
		this.names = names;
	}

	@Override
	public int getCount() {
		return names.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = ((Activity) context).getLayoutInflater().inflate(R.layout.image_text_list_item, null);
		}
		
		TextView textView = (TextView) view.findViewById(R.id.textView1);
		textView.setText(names[position]);
		return view;
	}

}
