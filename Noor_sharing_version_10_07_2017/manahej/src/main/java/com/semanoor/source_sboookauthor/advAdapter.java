package com.semanoor.source_sboookauthor;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.Elesson;
import com.semanoor.manahij.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

public class advAdapter extends BaseAdapter implements ListAdapter {
	private String[]  advsearch;
	//private BookViewActivity context;
	private Context context;
	public advAdapter(Context context, String[] advSearch) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.advsearch=advSearch;
		
	}

	public Integer[] mThumbIds = {
		     R.drawable.google1, R.drawable.wikipedia1, R.drawable.youtube1, R.drawable.yahoo_icon,
			R.drawable.bing_icon, R.drawable.ask
	};
	public Integer[] mSearchThumbIds = {
			R.drawable.google1, R.drawable.wikipedia1, R.drawable.youtube1, R.drawable.yahoo_icon,
			R.drawable.bing_icon, R.drawable.ask
	};
	//@Override
	public int getCount() {

		return advsearch.length;
	}

	//@Override
	public Object getItem(int position) {

		return advsearch[position];
	}

	//@Override
	public long getItemId(int position) {

		return position;
	}

	//@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View vi = convertView;
		if(convertView==null){
			Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
			UserFunctions.changeFont(parent,font);
			if(context instanceof BookViewActivity) {
				vi = ((BookViewActivity)context).getLayoutInflater().inflate(R.layout.advpopgrid, null);
			}else{
				vi = ((Elesson)context).getLayoutInflater().inflate(R.layout.advpopgrid, null);
			}
		}
		LinearLayout fblayout = (LinearLayout) vi.findViewById(R.id.fb_layout);
		fblayout.setVisibility(View.GONE);
		TextView txtSearchItem = (TextView) vi.findViewById(R.id.textViewAdv);
		txtSearchItem.setVisibility(View.VISIBLE);
		ImageView imgSearchImg = (ImageView) vi.findViewById(R.id.imageViewAdv);
		imgSearchImg.setVisibility(View.VISIBLE);
		if(context instanceof Elesson){
			txtSearchItem.setTextColor(context.getResources().getColor(R.color.white));
			txtSearchItem.setText(advsearch[position]);
			imgSearchImg.setImageResource(mSearchThumbIds[position]);
		}else{
			txtSearchItem.setText(advsearch[position]);
			imgSearchImg.setImageResource(mThumbIds[position]);
		}
		return vi;
	}

}
