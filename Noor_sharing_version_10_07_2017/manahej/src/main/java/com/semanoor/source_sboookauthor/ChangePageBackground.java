package com.semanoor.source_sboookauthor;

import java.io.File;
import java.util.ArrayList;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.PopupColorPicker.ColorPickerListener;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ChangePageBackground implements OnClickListener {
	
	private BookViewActivity context;
	private PopoverView popView;
	private int PAGE_BACKGROUND_GALLERY = 3;
	private String currentPage;
	private int bookID;
	private String pageBGPath;
	private Button removeBackgroundBtn, applyBackgroundBtn, tiledBtn, scaleToFitBtn, rotateBtn, colorBtn, btnMindmap;
	private int backgroundColor = 0xffffffff;
	boolean isHtmlBgRemoved = false;
	
	public ChangePageBackground(BookViewActivity bookViewActivity) {
		// TODO Auto-generated constructor stub
		context = bookViewActivity;
		bookID = bookViewActivity.currentBook.getBookID();
	}
	
	public void callPageBgPopwidnow(Button btnbackground){
		
	    popView = new PopoverView(context, R.layout.apply_background);
		popView.setContentSizeForViewInPopover(new Point(Globals.getDeviceIndependentPixels(60, context),  Globals.getDeviceIndependentPixels(400, context)));
		popView.showPopoverFromRectInViewGroup(context.rootView, PopoverView.getFrameForView(btnbackground), PopoverView.PopoverArrowDirectionDown, true);

		
		removeBackgroundBtn = (Button) popView.findViewById(R.id.remove_background);
		removeBackgroundBtn.setOnClickListener(this);

		applyBackgroundBtn = (Button) popView.findViewById(R.id.apply_background);
		applyBackgroundBtn.setOnClickListener(this);

		tiledBtn = (Button) popView.findViewById(R.id.tiled);
		tiledBtn.setOnClickListener(this);

		scaleToFitBtn = (Button) popView.findViewById(R.id.scale_fit);
		scaleToFitBtn.setOnClickListener(this);

		rotateBtn = (Button) popView.findViewById(R.id.rotate);
		rotateBtn.setOnClickListener(this);

		btnMindmap = (Button) popView.findViewById(R.id.btn_mindmap);
		btnMindmap.setOnClickListener(this);
		
		getBackgroundColor();
		colorBtn = (Button) popView.findViewById(R.id.color);
		colorBtn.setOnClickListener(this);
		//colorBtn.setBackgroundColor(backgroundColor);
		checkBackgroundImage();
		
	}

	@Override
	public void onClick(View v) {

		switch(v.getId()){
			case R.id.remove_background:{
				context.removeBackgroundDialog();
				popView.dissmissPopover(true);
			break;
			}
			case R.id.apply_background:{
				
				takePageBackgroundFromGallery();
				popView.dissmissPopover(true);
			break;
			}
			case R.id.tiled:{
				
				checkApplyTiledMode(pageBGPath, currentPage);
				popView.dissmissPopover(true);
			break;

			}
			case R.id.scale_fit:{
				
				checkApplyScaleToFitMode(pageBGPath, currentPage);
				popView.dissmissPopover(true);
			break;

			}
			case R.id.rotate:{
				
				context.showRotateDialog();
				popView.dissmissPopover(true);
			break;

			}
			case R.id.color:{
				
				callColorPicker();
			break;

			}
			case R.id.btn_mindmap:
				boolean isForBackgroundImage = true;
				MindMapDialog mindMapDialog = new MindMapDialog(context, isForBackgroundImage);
				mindMapDialog.showMindMapDialog("");
				popView.dissmissPopover(true);
				break;
		}
	}


	/**
	 * Check background image exists and get objectType pageBG orPageBGAll
	 */
	private void checkBackgroundImage(){
		String PagePath = context.page.pageBG_PATH;
		String PageAllPath = context.page.pageBG_ALL_PATH;
		File pagefile = new File(PagePath);
		File pageAllfile = new File(PageAllPath);
		int pageNo = Integer.parseInt(context.currentPageNumber);
		if(pagefile.exists()){
			enableButtons();
			currentPage =context.page.currentPageStr;
			pageBGPath = PagePath;
		}else if(context.CheckForApplyallRemoveAll() && pageNo>1 && pageNo<context.pageCount){
			enableButtons();
			currentPage = String.valueOf(0);	 //Since for pageBG_all page value will be 0
			pageBGPath = PageAllPath;
		}else if(checkPageColorexists()){
			disableButtons();
			removeBackgroundBtn.setEnabled(true);
			currentPage =context.page.currentPageStr;
			pageBGPath = PagePath;
		}else if(checkPageColorAllExists() && pageNo>1 && pageNo<context.pageCount){
			disableButtons();
			removeBackgroundBtn.setEnabled(true);
			currentPage = String.valueOf(0);	
			pageBGPath = PageAllPath;
		}
		else{
			disableButtons();
		}
	}
	
	/**
	 *  Method to disable changePageBackground popwindow buttons
	 */
	private void disableButtons(){
		
		removeBackgroundBtn.setEnabled(false);
		tiledBtn.setEnabled(false);
		scaleToFitBtn.setEnabled(false);
		rotateBtn.setEnabled(false);
	}
	
	/**
	 *  Method to enable changePageBackground popwindow buttons
	 */
	private void enableButtons(){
		
		removeBackgroundBtn.setEnabled(true);
		tiledBtn.setEnabled(true);
		scaleToFitBtn.setEnabled(true);
		rotateBtn.setEnabled(true);
	}
	/**
	 * @author Take page background photo from the Gallery
	 * 
	 */
	private void takePageBackgroundFromGallery(){
		Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		galleryIntent.setType("image/*");
		context.startActivityForResult(galleryIntent, PAGE_BACKGROUND_GALLERY);
	}
	
	/**
	 *  Check and apply tiled mode and also updating the mode in DB.
	 */
	private void checkApplyTiledMode(String pageBgPath, String pageNumber){
		
		String objType = context.page.pageBG_objType;
		if(!context.db.checkTiledModePageBackground(context.currentBook.getBookID(), objType, pageNumber)){
			UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
			uRPrevObjData.setuRPageNoObjDataStatus("ScaleToFit");
			uRPrevObjData.setuRObjDataType(objType);
			uRPrevObjData.setuRObjDataContent(pageBGPath);
			context.page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PREVIOUS_STATE);
			
			//Apply tiled mode
			context.page.applyTiledToPageBackground(pageBGPath, currentPage);
			//Update in db with tile mode
			context.db.updateTiledOrScaleToFitPageBackground(context.currentBook.getBookID(), objType, currentPage, "Tiled");
			
			UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
			uRCurrObjData.setuRPageNoObjDataStatus("Tiled");
			uRCurrObjData.setuRObjDataType(objType);
			uRCurrObjData.setuRObjDataContent(pageBGPath);
			context.page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);
		}
	}
	
	/**
	 *  Check and apply Scale to fit mode and also updating the mode in DB.
	 */
	private void checkApplyScaleToFitMode(String pageBgPath, String pageNumber){
		
		String objType = context.page.pageBG_objType;
		if(context.db.checkTiledModePageBackground(context.currentBook.getBookID(), objType, currentPage)){
			UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
			uRPrevObjData.setuRPageNoObjDataStatus("Tiled");
			uRPrevObjData.setuRObjDataType(objType);
			uRPrevObjData.setuRObjDataContent(pageBGPath);
			context.page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PREVIOUS_STATE);
			
			//Apply scaleToFit mode
			context.page.applyScaleTofitPageBackground(pageBGPath, currentPage);
			//Upade in db with scaleToFit mode
			context.db.updateTiledOrScaleToFitPageBackground(context.currentBook.getBookID(), objType, currentPage, "ScaleToFit");
			
			UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
			uRCurrObjData.setuRPageNoObjDataStatus("ScaleToFit");
			uRCurrObjData.setuRObjDataType(objType);
			uRCurrObjData.setuRObjDataContent(pageBGPath);
			context.page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PRESENT_STATE);
		}
	}
	
	/**
	 * Get current page background color while loading the background popwindow
	 */
	private void getBackgroundColor(){
	
		if(context.page.pageBackgroundColor.equals("NoEntry")){
		backgroundColor = Integer.parseInt(context.page.pageBackgroundColor);
		}
	}
	
	private Boolean checkPageColorexists(){

		String pageColorValue = context.db.getPageBackgroundColor(context.currentBook.getBookID(), context.page.pageColor_objType, context.page.currentPageStr, context.page.enrichedPageId);
		if(!pageColorValue.equals("NoEntry")){
		
			return true;
		}else{
			return false;
			}
	}
	
	private Boolean checkPageColorAllExists(){
		
		//check for apply all color background
		String pageAllColorValue = context.db.getPageBackgroundColor(context.currentBook.getBookID(), "PageColor", "0", context.page.enrichedPageId);
		if(!pageAllColorValue.equals("NoEntry")){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Call color picker to select and catch the listner events while dismissing the color picker pop window.
	 */
	private void callColorPicker(){
		int color = backgroundColor;
		final ArrayList<String> recentPageBackgroundColorList = context.page.getRecentColorsFromSharedPreference(Globals.pageBackgroundColorKey);
		new PopupColorPicker(context, color, null, recentPageBackgroundColorList, false, new ColorPickerListener() {
			String hexColor = null;
			@Override
			public void pickedColor(int color) {
				if (context.currentBook.is_bStoreBook() && !isHtmlBgRemoved){
					context.page.storePageWebRl.getParentTableBgForStoreWebRl();
					isHtmlBgRemoved = true;
				}
				hexColor = String.format("#%06X", (0xFFFFFF & color));
				backgroundColor = Color.parseColor(hexColor);
				context.page.setPageBackgroundWithColor(backgroundColor);
			}

			@Override
			public void dismissPopWindow() {
				context.showColorDialog(backgroundColor);
				popView.dissmissPopover(true);
				if (hexColor != null) {
					context.page.setandUpdateRecentColorsToSharedPreferences(Globals.pageBackgroundColorKey, recentPageBackgroundColorList, hexColor);
				}
			}

			@Override
			public void pickedTransparentColor(String transparentColor) {
				
			}

			@Override
			public void pickColorFromBg() {
				
			}
		});
	}
}


