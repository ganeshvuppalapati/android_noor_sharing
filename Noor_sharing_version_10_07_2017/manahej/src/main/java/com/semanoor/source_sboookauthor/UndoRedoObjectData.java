package com.semanoor.source_sboookauthor;

import java.util.ArrayList;

import android.view.View;

public class UndoRedoObjectData {
	
	private int uRObjDataUniqueId, uRObjDataXPos, uRObjDataYPos, uRObjDataWidth, uRObjDataHeight, uRObjDataSequentialId;
	private int uRObjDataWebTableRow, uRObjDataWebTableColumn, uRObjectDataRotatedAngle;
	private String uRPageNoObjDataStatus;
	private String uRObjDataType;
	private String uRObjDataContent, uRObjDataSize, uRObjDataLocation;
	private String uRObjDataVideoThumbPath;
	private View uRObjDataCustomView;
	private boolean uRObjDataLocked, uRObjPickedColorValue;
	private ArrayList<View> uRObjDataViewArrayList;
	private byte[] paintBuffer;
	
	/**
	 * @return the uRObjDataWebTableRow
	 */
	public int getuRObjDataWebTableRow() {
		return uRObjDataWebTableRow;
	}

	/**
	 * @param uRObjDataWebTableRow the uRObjDataWebTableRow to set
	 */
	public void setuRObjDataWebTableRow(int uRObjDataWebTableRow) {
		this.uRObjDataWebTableRow = uRObjDataWebTableRow;
	}

	/**
	 * @return the uRObjDataWebTableColumn
	 */
	public int getuRObjDataWebTableColumn() {
		return uRObjDataWebTableColumn;
	}

	/**
	 * @param uRObjDataWebTableColumn the uRObjDataWebTableColumn to set
	 */
	public void setuRObjDataWebTableColumn(int uRObjDataWebTableColumn) {
		this.uRObjDataWebTableColumn = uRObjDataWebTableColumn;
	}

	/**
	 * @return the uRObjDataUniqueId
	 */
	public int getuRObjDataUniqueId() {
		return uRObjDataUniqueId;
	}

	/**
	 * @param uRObjDataUniqueId the uRObjDataUniqueId to set
	 */
	public void setuRObjDataUniqueId(int uRObjDataUniqueId) {
		this.uRObjDataUniqueId = uRObjDataUniqueId;
	}

	/**
	 * @return the uRObjDataXPos
	 */
	public int getuRObjDataXPos() {
		return uRObjDataXPos;
	}

	/**
	 * @param uRObjDataXPos the uRObjDataXPos to set
	 */
	public void setuRObjDataXPos(int uRObjDataXPos) {
		this.uRObjDataXPos = uRObjDataXPos;
	}

	/**
	 * @return the uRObjDataYPos
	 */
	public int getuRObjDataYPos() {
		return uRObjDataYPos;
	}

	/**
	 * @param uRObjDataYPos the uRObjDataYPos to set
	 */
	public void setuRObjDataYPos(int uRObjDataYPos) {
		this.uRObjDataYPos = uRObjDataYPos;
	}

	/**
	 * @return the uRObjDataWidth
	 */
	public int getuRObjDataWidth() {
		return uRObjDataWidth;
	}

	/**
	 * @param uRObjDataWidth the uRObjDataWidth to set
	 */
	public void setuRObjDataWidth(int uRObjDataWidth) {
		this.uRObjDataWidth = uRObjDataWidth;
	}

	/**
	 * @return the uRObjDataHeight
	 */
	public int getuRObjDataHeight() {
		return uRObjDataHeight;
	}

	/**
	 * @param uRObjDataHeight the uRObjDataHeight to set
	 */
	public void setuRObjDataHeight(int uRObjDataHeight) {
		this.uRObjDataHeight = uRObjDataHeight;
	}

	/**
	 * @return the uRObjDataContent
	 */
	public String getuRObjDataContent() {
		return uRObjDataContent;
	}

	/**
	 * @param uRObjDataContent the uRObjDataContent to set
	 */
	public void setuRObjDataContent(String uRObjDataContent) {
		this.uRObjDataContent = uRObjDataContent;
	}

	/**
	 * @return the uRObjDataCustomView
	 */
	public View getuRObjDataCustomView() {
		return uRObjDataCustomView;
	}

	/**
	 * @param uRObjDataCustomView the uRObjDataCustomView to set
	 */
	public void setuRObjDataCustomView(View uRObjDataCustomView) {
		this.uRObjDataCustomView = uRObjDataCustomView;
	}

	/**
	 * @return the uRObjDataLocked
	 */
	public boolean isuRObjDataLocked() {
		return uRObjDataLocked;
	}

	/**
	 * @param uRObjDataLocked the uRObjDataLocked to set
	 */
	public void setuRObjDataLocked(boolean uRObjDataLocked) {
		this.uRObjDataLocked = uRObjDataLocked;
	}

	/**
	 * @return the uRObjDataSequentialId
	 */
	public int getuRObjDataSequentialId() {
		return uRObjDataSequentialId;
	}

	/**
	 * @param uRObjDataSequentialId the uRObjDataSequentialId to set
	 */
	public void setuRObjDataSequentialId(int uRObjDataSequentialId) {
		this.uRObjDataSequentialId = uRObjDataSequentialId;
	}

	/**
	 * @return the uRObjDataViewArrayList
	 */
	public ArrayList<View> getuRObjDataViewArrayList() {
		return uRObjDataViewArrayList;
	}

	/**
	 * @param uRObjDataViewArrayList the uRObjDataViewArrayList to set
	 */
	public void setuRObjDataViewArrayList(ArrayList<View> uRObjDataViewArrayList) {
		this.uRObjDataViewArrayList = uRObjDataViewArrayList;
	}

	/**
	 * @return the uRObjDataType
	 */
	public String getuRObjDataType() {
		return uRObjDataType;
	}

	/**
	 * @param uRObjDataType the uRObjDataType to set
	 */
	public void setuRObjDataType(String uRObjDataType) {
		this.uRObjDataType = uRObjDataType;
	}

	/**
	 * @return the uRObjDataVideoThumbPath
	 */
	public String getuRObjDataVideoThumbPath() {
		return uRObjDataVideoThumbPath;
	}

	/**
	 * @param uRObjDataVideoThumbPath the uRObjDataVideoThumbPath to set
	 */
	public void setuRObjDataVideoThumbPath(String uRObjDataVideoThumbPath) {
		this.uRObjDataVideoThumbPath = uRObjDataVideoThumbPath;
	}

	/**
	 * @return the uRPageNoObjDataStatus
	 */
	public String getuRPageNoObjDataStatus() {
		return uRPageNoObjDataStatus;
	}

	/**
	 * @param uRPageNoObjDataStatus the uRPageNoObjDataStatus to set
	 */
	public void setuRPageNoObjDataStatus(String uRPageNoObjDataStatus) {
		this.uRPageNoObjDataStatus = uRPageNoObjDataStatus;
	}

	/**
	 * @return the uRObjDataSize
	 */
	public String getuRObjDataSize() {
		return uRObjDataSize;
	}

	/**
	 * @param uRObjDataSize the uRObjDataSize to set
	 */
	public void setuRObjDataSize(String uRObjDataSize) {
		this.uRObjDataSize = uRObjDataSize;
	}

	/**
	 * @return the uRObjDataLocation
	 */
	public String getuRObjDataLocation() {
		return uRObjDataLocation;
	}

	/**
	 * @param uRObjDataLocation the uRObjDataLocation to set
	 */
	public void setuRObjDataLocation(String uRObjDataLocation) {
		this.uRObjDataLocation = uRObjDataLocation;
	}

	/**
	 * @return the paintBuffer
	 */
	public byte[] getPaintBuffer() {
		return paintBuffer;
	}

	/**
	 * @param paintBuffer the paintBuffer to set
	 */
	public void setPaintBuffer(byte[] paintBuffer) {
		this.paintBuffer = paintBuffer;
	}

	/**
	 * @return the uRObjectDataRotatedAngle
	 */
	public int getuRObjectDataRotatedAngle() {
		return uRObjectDataRotatedAngle;
	}

	/**
	 * @param uRObjectDataRotatedAngle the uRObjectDataRotatedAngle to set
	 */
	public void setuRObjectDataRotatedAngle(int uRObjectDataRotatedAngle) {
		this.uRObjectDataRotatedAngle = uRObjectDataRotatedAngle;
	}

	/**
	 * @return the uRObjPickedColorValue
	 */
	public boolean isuRObjPickedColorValue() {
		return uRObjPickedColorValue;
	}

	/**
	 * @param uRObjPickedColorValue the uRObjPickedColorValue to set
	 */
	public void setuRObjPickedColorValue(boolean uRObjPickedColorValue) {
		this.uRObjPickedColorValue = uRObjPickedColorValue;
	}
	

}
