package com.semanoor.source_sboookauthor;

import java.util.ArrayList;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.PopoverView.PopoverViewDelegate;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;


public class PopOverColorPicker implements OnClickListener, PopoverViewDelegate {
	
	public interface ClrPickerListener {
		void pickedColor(int color);
		void dismissPopWindow();
		void pickedTransparentColor(String transparentColor);
		void pickColorFromBg();
	}

	final PopoverView colorPopUpView;
	final ClrPickerListener listener;
	final View viewHue;
	final ColorView viewSatVal;
	final ImageView viewCursor;
	final Button btnRecentFirstColor,btnRecentSecondColor,btnRecentThirdColor,btnRecentFourthColor,btnRecentFifthColor,btnTransparent,btnPickColorFromBackground;
	final View viewNewColor;
	final ImageView viewTarget;
	final ViewGroup viewContainer;
	final float[] currentColorHsv = new float[3];

	

	
	public PopOverColorPicker(BookViewActivity context, int color, View parentView, ArrayList<String> recentColorList, ClrPickerListener _listener) {
		this.listener = _listener;
		Color.colorToHSV(color, currentColorHsv);
		
		final View view = LayoutInflater.from(context).inflate(R.layout.custom_color_picker, null);
		viewHue = view.findViewById(R.id.viewHue);
		viewSatVal = (ColorView) view.findViewById(R.id.viewSatBri);
		viewCursor = (ImageView) view.findViewById(R.id.cursor);
		btnRecentFirstColor = (Button) view.findViewById(R.id.recentColor1);
		btnRecentFirstColor.setOnClickListener(this);
		btnRecentSecondColor = (Button) view.findViewById(R.id.recentColor2);
		btnRecentSecondColor.setOnClickListener(this);
		btnRecentThirdColor = (Button) view.findViewById(R.id.recentColor3);
		btnRecentThirdColor.setOnClickListener(this);
		btnRecentFourthColor = (Button) view.findViewById(R.id.recentColor4);
		btnRecentFourthColor.setOnClickListener(this);
		btnRecentFifthColor = (Button) view.findViewById(R.id.recentColor5);
		btnRecentFifthColor.setOnClickListener(this);
		btnTransparent = (Button) view.findViewById(R.id.btnTransparent);
		btnTransparent.setOnClickListener(this);
		btnPickColorFromBackground = (Button) view.findViewById(R.id.btnPickClrBackground);
		btnPickColorFromBackground.setOnClickListener(this);
		
		viewNewColor = view.findViewById(R.id.newColor);
		
		viewTarget = (ImageView) view.findViewById(R.id.target);
		viewContainer = (ViewGroup) view.findViewById(R.id.viewContainer);

		viewSatVal.setHue(getHue());
		viewNewColor.setBackgroundColor(color);
		
		if (recentColorList.get(0) != null) {
			btnRecentFirstColor.setBackgroundColor(Color.parseColor(recentColorList.get(0)));
		}
		if (recentColorList.get(1) != null) {
			btnRecentSecondColor.setBackgroundColor(Color.parseColor(recentColorList.get(1)));
		}
		if (recentColorList.get(2) != null) {
			btnRecentThirdColor.setBackgroundColor(Color.parseColor(recentColorList.get(2)));
		}
		if (recentColorList.get(3) != null) {
			btnRecentFourthColor.setBackgroundColor(Color.parseColor(recentColorList.get(3)));
		}
		if (recentColorList.get(4) != null) {
			btnRecentFifthColor.setBackgroundColor(Color.parseColor(recentColorList.get(4)));
		}
		
		viewHue.setOnTouchListener(new View.OnTouchListener() {
			@Override 
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_MOVE
						|| event.getAction() == MotionEvent.ACTION_DOWN
						|| event.getAction() == MotionEvent.ACTION_UP) {

					float y = event.getY();
					if (y < 0.f) y = 0.f;
					if (y > viewHue.getMeasuredHeight()) y = viewHue.getMeasuredHeight() - 0.001f; // to avoid looping from end to start.
					float hue = 360.f - 360.f / viewHue.getMeasuredHeight() * y;
					if (hue == 360.f) hue = 0.f;
					setHue(hue);

					// update view
					viewSatVal.setHue(getHue());
					moveCursor();
					viewNewColor.setBackgroundColor(getColor());
					if (PopOverColorPicker.this.listener != null) {
						PopOverColorPicker.this.listener.pickedColor(getColor());
					}
					
					return true;
				}
				return false;
			}
		});
		viewSatVal.setOnTouchListener(new View.OnTouchListener() {
			@Override 
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_MOVE
						|| event.getAction() == MotionEvent.ACTION_DOWN
						|| event.getAction() == MotionEvent.ACTION_UP) {

					float x = event.getX(); // touch event are in dp units.
					float y = event.getY();

					if (x < 0.f) x = 0.f;
					if (x > viewSatVal.getMeasuredWidth()) x = viewSatVal.getMeasuredWidth();
					if (y < 0.f) y = 0.f;
					if (y > viewSatVal.getMeasuredHeight()) y = viewSatVal.getMeasuredHeight();

					setSat(1.f / viewSatVal.getMeasuredWidth() * x);
					setVal(1.f - (1.f / viewSatVal.getMeasuredHeight() * y));

					// update view
					moveTarget();
					viewNewColor.setBackgroundColor(getColor());
					if (PopOverColorPicker.this.listener != null) {
						PopOverColorPicker.this.listener.pickedColor(getColor());
					}
					return true;
				} 
				return false;
			}
		});
		
		colorPopUpView = new PopoverView(context, view);
		int popupWidth = (int) context.getResources().getDimension(R.dimen.bookview_color_picker_popup_window_width);
		int popupHeight = (int) context.getResources().getDimension(R.dimen.bookview_color_picker_popup_window_height);
		colorPopUpView.setContentSizeForViewInPopover(new Point(popupWidth,popupHeight)); 
		colorPopUpView.setDelegate(this);
		colorPopUpView.showPopoverFromRectInViewGroup(context.designPageLayout, PopoverView.getFrameForView(parentView), PopoverView.PopoverArrowDirectionDown, true);
		
		// move cursor & target on first draw
		ViewTreeObserver vto = view.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override public void onGlobalLayout() {
				moveCursor();
				moveTarget();
				view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
			}
		});
	}


	protected void moveCursor() {
		float y = viewHue.getMeasuredHeight() - (getHue() * viewHue.getMeasuredHeight() / 360.f);
		if (y == viewHue.getMeasuredHeight()) y = 0.f;
		RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewCursor.getLayoutParams();
		layoutParams.leftMargin = (int) (viewHue.getLeft() - Math.floor(viewCursor.getMeasuredWidth() / 2) - viewContainer.getPaddingLeft());
		;
		layoutParams.topMargin = (int) (viewHue.getTop() + y - Math.floor(viewCursor.getMeasuredHeight() / 2) - viewContainer.getPaddingTop());
		;
		viewCursor.setLayoutParams(layoutParams);
	}

	protected void moveTarget() {
		float x = getSat() * viewSatVal.getMeasuredWidth();
		float y = (1.f - getVal()) * viewSatVal.getMeasuredHeight();
		RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) viewTarget.getLayoutParams();
		layoutParams.leftMargin = (int) (viewSatVal.getLeft() + x - Math.floor(viewTarget.getMeasuredWidth() / 2) - viewContainer.getPaddingLeft());
		layoutParams.topMargin = (int) (viewSatVal.getTop() + y - Math.floor(viewTarget.getMeasuredHeight() / 2) - viewContainer.getPaddingTop());
		viewTarget.setLayoutParams(layoutParams);
	}

	private int getColor() {
		return Color.HSVToColor(currentColorHsv);
	}

	private float getHue() {
		return currentColorHsv[0];
	}

	private float getSat() {
		return currentColorHsv[1];
	}

	private float getVal() {
		return currentColorHsv[2];
	}

	private void setHue(float hue) {
		currentColorHsv[0] = hue;
	}

	private void setSat(float sat) {
		currentColorHsv[1] = sat;
	}

	private void setVal(float val) {
		currentColorHsv[2] = val;
	}

	@Override
	public void onClick(View v) {
		ColorDrawable btnColor;
		switch (v.getId()) {
		case R.id.recentColor1:
			btnColor = (ColorDrawable) btnRecentFirstColor.getBackground();
			updateColorPickerView(btnColor);
			break;
			
		case R.id.recentColor2:
			btnColor = (ColorDrawable) btnRecentSecondColor.getBackground();
			updateColorPickerView(btnColor);
			break;
			
		case R.id.recentColor3:
			btnColor = (ColorDrawable) btnRecentThirdColor.getBackground();
			updateColorPickerView(btnColor);
			break;
			
		case R.id.recentColor4:
			btnColor = (ColorDrawable) btnRecentFourthColor.getBackground();
			updateColorPickerView(btnColor);
			break;
			
		case R.id.recentColor5:
			btnColor = (ColorDrawable) btnRecentFifthColor.getBackground();
			updateColorPickerView(btnColor);
			break;
			
		case R.id.btnTransparent:
			if (PopOverColorPicker.this.listener != null) {
				PopOverColorPicker.this.listener.pickedTransparentColor("transparent");
			}
			break;

		case R.id.btnPickClrBackground:
			if (PopOverColorPicker.this.listener != null) {
				PopOverColorPicker.this.listener.pickColorFromBg();
			}
			colorPopUpView.dissmissPopover(true);
			break;
			
		default:
			break;
		}
	}
	
	/**
	 * UpdateColorPickerView
	 * @param
	 */
	private void updateColorPickerView(ColorDrawable color){
		Color.colorToHSV(color.getColor(), currentColorHsv);
		if (PopOverColorPicker.this.listener != null) {
			PopOverColorPicker.this.listener.pickedColor(getColor());
		}
		viewSatVal.setHue(getHue());
		viewNewColor.setBackgroundColor(getColor());
		moveTarget();
		moveCursor();
	}

	@Override
	public void popoverViewWillShow(PopoverView view) {
		
	}

	@Override
	public void popoverViewDidShow(PopoverView view) {
		
	}

	@Override
	public void popoverViewWillDismiss(PopoverView view) {
		if (PopOverColorPicker.this.listener != null) {
			PopOverColorPicker.this.listener.dismissPopWindow();
		}
	}

	@Override
	public void popoverViewDidDismiss(PopoverView view) {
		
	}
}
