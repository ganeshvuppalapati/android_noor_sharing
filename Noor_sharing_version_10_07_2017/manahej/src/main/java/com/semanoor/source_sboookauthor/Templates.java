package com.semanoor.source_sboookauthor;

import java.io.Serializable;

public class Templates implements Serializable{
	private int templateId;
	private String templateName;
	private String templateType;
	private String templateTitleFont;
	private int templateTitleSize;
	private String templateTitleColor;
	private String templateAuthorFont;
	private int templateAuthorSize;
	private String templateAuthorColor;
	private String templateParaFont;
	private int templateParaSize;
	private String templateParaColor;
	/**
	 * @return the templateId
	 */
	public int getTemplateId() {
		return templateId;
	}
	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}
	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}
	/**
	 * @param templateName the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	/**
	 * @return the templateType
	 */
	public String getTemplateType() {
		return templateType;
	}
	/**
	 * @param templateType the templateType to set
	 */
	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}
	/**
	 * @return the templateTitleFont
	 */
	public String getTemplateTitleFont() {
		return templateTitleFont;
	}
	/**
	 * @param templateTitleFont the templateTitleFont to set
	 */
	public void setTemplateTitleFont(String templateTitleFont) {
		this.templateTitleFont = templateTitleFont;
	}
	/**
	 * @param templateTitleSize the templateTitleSize to set
	 */
	public void setTemplateTitleSize(int templateTitleSize) {
		this.templateTitleSize = templateTitleSize;
	}
	/**
	 * @return the templateTitleColor
	 */
	public String getTemplateTitleColor() {
		return templateTitleColor;
	}
	/**
	 * @param templateTitleColor the templateTitleColor to set
	 */
	public void setTemplateTitleColor(String templateTitleColor) {
		this.templateTitleColor = templateTitleColor;
	}
	/**
	 * @return the templateAuthorFont
	 */
	public String getTemplateAuthorFont() {
		return templateAuthorFont;
	}
	/**
	 * @param templateAuthorFont the templateAuthorFont to set
	 */
	public void setTemplateAuthorFont(String templateAuthorFont) {
		this.templateAuthorFont = templateAuthorFont;
	}
	/**
	 * @param templateAuthorSize the templateAuthorSize to set
	 */
	public void setTemplateAuthorSize(int templateAuthorSize) {
		this.templateAuthorSize = templateAuthorSize;
	}
	/**
	 * @return the templateAuthorColor
	 */
	public String getTemplateAuthorColor() {
		return templateAuthorColor;
	}
	/**
	 * @param templateAuthorColor the templateAuthorColor to set
	 */
	public void setTemplateAuthorColor(String templateAuthorColor) {
		this.templateAuthorColor = templateAuthorColor;
	}
	/**
	 * @return the templateParaFont
	 */
	public String getTemplateParaFont() {
		return templateParaFont;
	}
	/**
	 * @param templateParaFont the templateParaFont to set
	 */
	public void setTemplateParaFont(String templateParaFont) {
		this.templateParaFont = templateParaFont;
	}
	/**
	 * @param templateParaSize the templateParaSize to set
	 */
	public void setTemplateParaSize(int templateParaSize) {
		this.templateParaSize = templateParaSize;
	}
	/**
	 * @return the templateParaColor
	 */
	public String getTemplateParaColor() {
		return templateParaColor;
	}
	/**
	 * @param templateParaColor the templateParaColor to set
	 */
	public void setTemplateParaColor(String templateParaColor) {
		this.templateParaColor = templateParaColor;
	}
}
