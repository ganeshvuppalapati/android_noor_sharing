package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class CustomDesignScrollView extends ScrollView {
	
	private boolean enableScrolling = true;
	
	public boolean isEnableScrolling() {
		return enableScrolling;
	}
	
	public void setEnableScrolling(boolean _enaableScrolling) {
		this.enableScrolling = _enaableScrolling;
	}

	public CustomDesignScrollView(Context context) {
		super(context);
	}
	
	public CustomDesignScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public CustomDesignScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (isEnableScrolling()) {
			return super.onInterceptTouchEvent(ev);
		} else {
			return false;
		}
		
	}

}
