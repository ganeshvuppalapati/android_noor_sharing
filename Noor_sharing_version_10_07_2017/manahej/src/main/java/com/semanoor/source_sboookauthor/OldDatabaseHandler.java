package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;
import java.util.ArrayList;

public class OldDatabaseHandler extends SQLiteOpenHelper {

	//singleton/ single instance reference of database instance
		private static OldDatabaseHandler _dbHandler;

		//The Android's default system path of your application database.
		//private static String DB_PATH = "/data/data/YOUR_PACKAGE/databases/";
		private static String DB_PATH;
		
		//database name
		private static String DB_NAME = "sbook.sqlite";
		//reference of database
		public SQLiteDatabase _sqliteDb;
		//
		private String _searchToken;
		
		private final Context _context;


		public OldDatabaseHandler(Context context) {
			super(context, DB_NAME, null, 1);
			this._context = context;
			DB_PATH = _context.getFilesDir().getParentFile().getPath()+"/databases/";
		}
		
		public static synchronized OldDatabaseHandler getInstance(Context context){
			if(_dbHandler == null){
				_dbHandler = new OldDatabaseHandler(context);
				try {
					_dbHandler.createDataBase();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return _dbHandler;
		}
		
		/**
		 * @author Creating Database
		 *
		 */
		public void createDataBase() throws IOException
		{
			boolean dbExist = checkDataBase();
			if(dbExist)
			{
				//do nothing - database already exist
			}else{
				this.getReadableDatabase();
				/*try {
					//copyDataBase();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}*/
			}
		}
		
		/**
		 * @author Check Database Exist in the Path
		 *
		 */
		private boolean checkDataBase()
		{
			SQLiteDatabase checkDB = null;
			try
			{
				String myPath = DB_PATH + DB_NAME;
				checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
			}
			catch(SQLiteException e)
			{
				//database does't exist yet.
			}
			if(checkDB != null)
			{
				checkDB.close();
			}
			return checkDB != null ? true : false;
		}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	
	public ArrayList<String> getAllBookContent(){
		ArrayList<String> bookListArray = new ArrayList<String>();
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from books";
		String query="select * from tblBook";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			//int BID = Integer.parseInt(c.getString(0));
			String storeID = c.getString(1);
			int totalPages = c.getInt(2);
			int CID = 1;
			int lastViewedPage = c.getInt(17);
			String title = c.getString(27);
			String bLanguage = c.getString(28);
			String bookStr = storeID+"|"+totalPages+"|"+CID+"|"+lastViewedPage+"|"+title+"|"+bLanguage;
			bookListArray.add(bookStr);
			c.moveToNext();
		}
		c.close();
		db.close();
		return bookListArray;
	}
	public ArrayList<String> getAllBookMarkCount(String bName){
		ArrayList<String> bookMarkArray = new ArrayList<String>();
		SQLiteDatabase db = getReadableDatabase();
		//String query = "select * from books";
		String query="select * from tblBookmark where BName='"+bName+"'";
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();
		while (c.isAfterLast() == false){
			//int BID = Integer.parseInt(c.getString(0));
			//int  BMID = c.getInt(0);
			String BName = c.getString(1);
			int pageNo = c.getInt(2);
			int TagId = c.getInt(3);
			int TabNo = c.getInt(4);
		
			//String bLanguage = c.getString(28);
			String bookStr =BName+"|"+pageNo+"|"+TagId+"|"+TabNo;
			bookMarkArray.add(bookStr);
			c.moveToNext();
		}
		c.close();
		db.close();
		return bookMarkArray;
	}
	
	public Object[] loadNoteDivToArray(String bookName) {
		SQLiteDatabase db = getReadableDatabase();
		String query = "select PageNo,SText,SDesc,Color,TabNo,Occurence,NPos,ProcessSText from tblNote where BName='"+bookName+"'";
		Cursor c = db.rawQuery(query, null);
		String[] PageNo = new String[c.getCount()];
		String[] SText = new String[c.getCount()];
		String[] SDesc = new String[c.getCount()];
		String[] Color = new String[c.getCount()];
		String[] TabNo = new String[c.getCount()];
		Integer[] Occurence = new Integer[c.getCount()];
		Integer[] NPos = new Integer[c.getCount()];
		String[] ProcessText = new String[c.getCount()];
		
		int i = 0;
		c.moveToFirst();
		while (c.isAfterLast() == false){
			PageNo[i] = c.getString(0);
			SText[i] = c.getString(1);
			SDesc[i] = c.getString(2);
			Color[i] = c.getString(3);
			TabNo[i] = c.getString(4);
			Occurence[i]=c.getInt(5);
			NPos[i]=c.getInt(6);
			ProcessText[i] = c.getString(7);
			i++;
			c.moveToNext();
		}
		c.close();
		db.close();
		return new Object[]{PageNo,SText,SDesc,Color,TabNo,Occurence,NPos,ProcessText};
	}
	public Object[] getallhighlights(String bookName) {
		SQLiteDatabase db = getReadableDatabase();
		String query = "select PageNo,SText,Occurence,TabNo from tblHighlight where BName='"+bookName+"'";
		Cursor c = db.rawQuery(query, null);
		String[] PageNo = new String[c.getCount()];
		String[] SText = new String[c.getCount()];
		Integer[] Occurence = new Integer[c.getCount()];
		String[] TabNo = new String[c.getCount()];
		
		int i = 0;
		c.moveToFirst();
		while (c.isAfterLast() == false){
			PageNo[i] = c.getString(0);
			SText[i] = c.getString(1);
			Occurence[i]=c.getInt(2);
			TabNo[i] = c.getString(3);
			i++;
			c.moveToNext();
		}
		c.close();
		db.close();
		return new Object[]{PageNo,SText,Occurence,TabNo};
	}
	
	

}
