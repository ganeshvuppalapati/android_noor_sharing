package com.semanoor.source_sboookauthor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;

public class ClearObjects implements OnClickListener {
	
	private BookViewActivity context;
	private PopoverView popView;
	private Button ClearAllObjects, ClearAllObjectsExceptText;
	
	public ClearObjects(BookViewActivity bookViewActivity) {
		context = bookViewActivity;
	}
	
	public void clearObjectsPopwidnow(Button btnclearpage){
		
		popView = new PopoverView(context, R.layout.clear_objects);
		popView.setContentSizeForViewInPopover(new Point(Globals.getDeviceIndependentPixels(60, context),  Globals.getDeviceIndependentPixels(150, context)));
		popView.showPopoverFromRectInViewGroup(context.rootView, PopoverView.getFrameForView(btnclearpage), PopoverView.PopoverArrowDirectionDown, true);

		ClearAllObjects = (Button) popView.findViewById(R.id.clear_allobjects);
		ClearAllObjects.setOnClickListener(this);
        
		ClearAllObjectsExceptText = (Button) popView.findViewById(R.id.clear_allobjects_excepttext);
		ClearAllObjectsExceptText.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.clear_allobjects:{
			//	context.page.clearAllObjects(true);
			AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);
			alertdialog.setTitle(R.string.clear_page);
			alertdialog.setMessage(R.string.clear_whole_page);
			alertdialog.setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					context.page.clearAllObjects(true);
					popView.dissmissPopover(true);
				}
			});
			alertdialog.setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
					popView.dissmissPopover(true);
				}
			});
			alertdialog.show();
			break;
		}
		case R.id.clear_allobjects_excepttext:{
			//context.page.clearAllObjects(false);
			AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);
			alertdialog.setTitle(R.string.clear_page_except_text);
			alertdialog.setMessage(R.string.clear_whole_page_except_text);
			alertdialog.setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					context.page.clearAllObjects(false);
					popView.dissmissPopover(true);
				}
			});
			alertdialog.setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
					popView.dissmissPopover(true);
				}
			});
			alertdialog.show();

			break;
		}
		}
}
}