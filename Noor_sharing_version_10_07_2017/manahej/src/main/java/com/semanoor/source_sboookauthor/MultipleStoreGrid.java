package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.GridView;

import com.semanoor.manahij.R;

/**
 * Created by karthik on 21-03-2016.
 */
public class MultipleStoreGrid extends GridView {

    private Bitmap mShelfBackground = null;

    private int mShelfWidth;
    private int mShelfHeight;
    private Context context;

    public MultipleStoreGrid(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        mShelfBackground = BitmapFactory.decodeResource(getResources(), R.drawable.store_rack_img);
        setBackground(mShelfBackground);
    }

    private void setBackground(Bitmap background) {
        this.mShelfBackground = background;
        mShelfWidth = background.getWidth();
        mShelfHeight = (int) getResources().getDimension(R.dimen.multiple_store_grid_height);
    }

    @Override
    protected void dispatchDraw(Canvas canvas){
        final int count = getChildCount();
        final int top = count > 0 ? getChildAt(0).getTop() : 0;
        final int shelfWidth = mShelfWidth;
        final int shelfHeight = mShelfHeight + Globals.getDeviceIndependentPixels(5, context);
        final int shelfRackHeight = (int) getResources().getDimension(R.dimen.multiple_store_gridrack_height);
        final int width = getWidth();
        final int height = getHeight();
        final Bitmap background = this.mShelfBackground;

        for (int x = 0; x < width; x+=shelfWidth) {
            for (int y = top - shelfRackHeight; y < height; y+=shelfHeight) {
                canvas.drawBitmap(background, x, y, null);
            }

            //This draws the top pixels of the shelf above the current one

            //Rect source = new Rect(0, mShelfHeight-top, mShelfWidth, mShelfHeight);
            //Rect dest = new Rect(x, 0, x+mShelfWidth, top);

            //canvas.drawBitmap(background, source, dest, null);
        }

        super.dispatchDraw(canvas);
    }
}
