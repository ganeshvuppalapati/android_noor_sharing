package com.semanoor.source_sboookauthor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptg.mindmap.widget.MindMapActivity;
import com.ptg.mindmap.widget.MindmapPlayer;
import com.ptg.views.CircleButton;
import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.MainActivity;
import com.semanoor.manahij.R;
import com.semanoor.sboookauthor_store.EnrichmentButton;
import com.semanoor.sboookauthor_store.Enrichments;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Suriya on 22/05/15.
 */
public class MindMapDialog {

    private Context context;
    private ArrayList<Object> mindMapFileNames;
    private ArrayList<Object> mindMapFiles;
    EditText editText;
    MindMapGridAdapter mindMapGridAdapter;
    private Dialog settingsDialog;
    public static int MindMapActivityRequestCode = 1004;
    private boolean isForBackgroundImage;
    private boolean isMindMapDeleted;
    private int bookId;
    private boolean mindMapeditedfrmShelf=false;
    private boolean mindMapNameEdited=false;
    private boolean mindMapEditedForStrBok=false;

    public MindMapDialog(BookViewActivity context, boolean isForBackgroundImage){
        this.context = context;
        this.isForBackgroundImage = isForBackgroundImage;
        this.mindMapFiles=context.gridShelf.bookListArray;
    }

    public MindMapDialog(BookViewReadActivity context, boolean isForBackgroundImage){
        this.context = context;
        this.isForBackgroundImage = isForBackgroundImage;
        this.mindMapFiles=context.gridShelf.bookListArray;
    }

    public MindMapDialog(MainActivity context, boolean isForBackgroundImage){
        this.context = context;
        this.isForBackgroundImage = isForBackgroundImage;
        this.mindMapFiles=context.gridShelf.bookListArray;
    }

    public void showMindMapDialog(String orphanNode){
        settingsDialog = new Dialog(context);
        settingsDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(((Activity) context).getLayoutInflater().inflate(R.layout.mindmap_dialog, null));
        settingsDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ViewGroup linearLayout = (LinearLayout) settingsDialog.findViewById(R.id.mindMap_layout);
        Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
        UserFunctions.changeFont(linearLayout,font);
        settingsDialog.getWindow().getAttributes().windowAnimations=R.style.DialogAnimation;


        File semaMindmapJSfile = new File(Globals.TARGET_BASE_MINDMAP_PATH+"semamindmap.min.js");
        File semaMindmapStyleFile = new File(Globals.TARGET_BASE_MINDMAP_PATH+"style.min.css");
        UserFunctions.copyAsset(context.getAssets(), "semamindmap.min.js", Globals.TARGET_BASE_MINDMAP_PATH+"semamindmap.min.js");
        UserFunctions.copyAsset(context.getAssets(), "style.min.css", Globals.TARGET_BASE_MINDMAP_PATH+"style.min.css");
        mindMapFileNames=new ArrayList<Object>();

        for (int i=0;i<mindMapFiles.size();i++){
            Book book= (Book) mindMapFiles.get(i);
            if (!book.is_bStoreBook()&&book.getbCategoryId()==Globals.mindMapCatId){
                mindMapFileNames.add(book);
            }
        }
        CircleButton back_btn = (CircleButton) settingsDialog.findViewById(R.id.btn_back);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();
            }
        });
        editText = (EditText) settingsDialog.findViewById(R.id.editText);
        editText.setText(orphanNode);
        CircleButton btnCreateMindMap = (CircleButton) settingsDialog.findViewById(R.id.button5);
        btnCreateMindMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean fileExist = false;
                String title = editText.getText().toString();
                if (title.isEmpty()) {

                } else {
                    if (mindMapFileNames != null) {
                        for (int i = 0; i < mindMapFileNames.size(); i++) {
                            Book book =(Book)mindMapFileNames.get(i);
                            String fileName = book.getBookTitle();
                            if (title.equals(fileName)) {
                                UserFunctions.DisplayAlertDialog(context, R.string.mindmap_already_exist_msg, R.string.mindmap_exist);
                                fileExist = true;
                                break;
                            }
                        }
                    }
                    if (!fileExist) {
                        addMindMapBook(title);
                        settingsDialog.dismiss();
                    }
                }
            }
        });

        if (isForBackgroundImage){
            editText.setVisibility(View.GONE);
            btnCreateMindMap.setVisibility(View.GONE);
        }

        GridView gridView = (GridView) settingsDialog.findViewById(R.id.gridView);
        if (mindMapFileNames != null) {
            mindMapGridAdapter = new MindMapGridAdapter();
            gridView.setAdapter(mindMapGridAdapter);
            if (isForBackgroundImage){
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        addMindmapAsbackground(position, Globals.OBJTYPE_MINDMAPBG);
                    }
                });
            } else {
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                          final Book book =(Book)mindMapFileNames.get(position);
                        final String mindMapFileName = book.getBookTitle();
                        if (context instanceof BookViewActivity) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(R.string.add_mindmap_asa);
                            builder.setItems(new CharSequence[]{context.getResources().getString(R.string.add_inside_page), context.getResources().getString(R.string.add_as_tab), context.getResources().getString(R.string.open_for_edit)},
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case 0:
                                                    //Add inside page
                                                    addInsidePage(mindMapFileName);
                                                    settingsDialog.dismiss();
                                                    dialog.cancel();
                                                    break;
                                                case 1:
                                                    //Add as tab
                                                  //  new CreatingMindmapEnrichments(mindMapFileName, mindMapFileName).execute();
                                                    settingsDialog.dismiss();
                                                    dialog.cancel();
                                                    break;
                                               /* case 2:
                                                    //Add as Background
                                                    addMindmapAsbackground(position, Globals.OBJTYPE_MINDMAPBG);
                                                    settingsDialog.dismiss();
                                                    dialog.cancel();
                                                    break;*/
                                                case 2:
                                                    //Open for edit
                                                    openMindmapActivity("", editText.getText().toString(), mindMapFileName,book);
                                                    settingsDialog.dismiss();
                                                    dialog.cancel();
                                                    break;
                                            }
                                        }
                                    });
                            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.create().show();
                        } else if (context instanceof BookViewReadActivity){
                            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(R.string.add_mindmap_asa);
                            builder.setItems(new CharSequence[]{ context.getResources().getString(R.string.add_as_tab), context.getResources().getString(R.string.open_for_edit)},
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case 0:
                                                    //Add as tab
                                                 //   new createMindMapTab(mindMapFileName).execute();
                                                    settingsDialog.dismiss();
                                                    dialog.cancel();
                                                    break;
                                                case 1:
                                                    //Open for edit
                                                    openMindmapActivity("", editText.getText().toString(), mindMapFileName,book);
                                                    settingsDialog.dismiss();
                                                    dialog.cancel();
                                                    break;
                                            }
                                        }
                                    });
                            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.create().show();
                        }else if (context instanceof MainActivity){
                            openMindmapActivity("", editText.getText().toString(), mindMapFileName,book);
                            settingsDialog.dismiss();
                        }
                    }
                });
            }
        }
        settingsDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!isMindMapDeleted) {
                    return;
                }
                if (context instanceof BookViewActivity){
                    ((BookViewActivity) context).checkForEnrichments(Integer.parseInt(((BookViewActivity) context).currentPageNumber), ((BookViewActivity) context).page.enrichedPageId);
                    ((BookViewActivity) context).instantiateNewPage(Integer.parseInt(((BookViewActivity) context).currentPageNumber), ((BookViewActivity) context).page.enrichedPageId);
                }
            }
        });
        settingsDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (context instanceof MainActivity && mindMapeditedfrmShelf){
                    ((MainActivity) context).loadGridView();
                }else if (context instanceof BookViewActivity && mindMapNameEdited ){
                    ((BookViewActivity) context).checkForEnrichments(Integer.parseInt(((BookViewActivity) context).currentPageNumber), ((BookViewActivity) context).page.enrichedPageId);
                    ((BookViewActivity) context).instantiateNewPage(Integer.parseInt(((BookViewActivity) context).currentPageNumber), ((BookViewActivity) context).page.enrichedPageId);
                }
            }
        });
        settingsDialog.show();
    }

    public class CreatingMindmapEnrichments extends AsyncTask<Void, Void, Void> {
        String title;
        String enrichmentPath;

        public CreatingMindmapEnrichments(String Title, String url) {
            this.title=Title;
            this.enrichmentPath=url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String bookXmlPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+((BookViewActivity)context).currentBook.get_bStoreID()+"Book/"+"Book.xml";
            //int enrichSequenceId = enrichmentTabListArray.size()+1;
            //String enrichTitle = "Enrichment"+enrichSequenceId;
            int  objSequentialId =  ((BookViewActivity)context).db.getMaximumSequentialId(Integer.parseInt(((BookViewActivity)context).currentPageNumber),  ((BookViewActivity)context).currentBook.getBookID()) + 1;
            title=title.replace("'", "''");
            for(Enrichments enrich:((BookViewActivity)context).enrichmentTabListArray){
                if(enrich.isEnrichmentSelected()){
                    enrich.setEnrichmentSelected(false);
                    break;
                }
            }
            ((BookViewActivity)context).db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,SequentialID,categoryID)values('"+((BookViewActivity)context).currentBook.getBookID()+"', '"+((BookViewActivity)context).currentPageNumber+"', '"+title+"', '"+Globals.OBJTYPE_MINDMAPTAB+"', '0','"+enrichmentPath+"','"+objSequentialId+"','0')");
            int maxEnrichId = ((BookViewActivity)context).db.getMaxUniqueRowID("enrichments");
            Enrichments enrichments = new Enrichments(context);
            enrichments.setEnrichmentId(maxEnrichId);
            enrichments.setEnrichmentBid(((BookViewActivity)context).currentBook.getBookID());
            enrichments.setEnrichmentSequenceId(objSequentialId);
            enrichments.setEnrichmentPageNo(Integer.parseInt(((BookViewActivity)context).currentPageNumber));
            enrichments.setEnrichmentTitle(title.replace( "''", "'"));
            enrichments.setEnrichmentPath(enrichmentPath);
            enrichments.setEnrichmentSelected(true);
            enrichments.setEnrichmentType(Globals.OBJTYPE_MINDMAPTAB);
            enrichments.setEnrichmentExportValue(0);
            ((BookViewActivity)context).enrichmentTabListArray.add(enrichments);
			/*if(currentBook.is_bStoreBook()){
				enrichments.UpdateAdvSearchintoBookXml(bookXmlPath);
			}*/
            //Button enrichBtnCreated = enrichments.createEnrichmentTabs();
            //((BookViewActivity)context).makeEnrichedBtnSlected(enrichBtnCreated);
            //System.out.println(enrichmentTabListArray.size());

            if (((BookViewActivity)context).enrichmentTabListArray.size()>1) {
//                if (((BookViewActivity)context).rlTabsLayout.getVisibility() != 0) {
                    ((BookViewActivity)context).rlTabsLayout.setVisibility(View.VISIBLE);
//                }
            }else{
                ((BookViewActivity)context).rlTabsLayout.setVisibility(View.INVISIBLE);
            }
            ((BookViewActivity)context).wv_advSearch.setVisibility(View.INVISIBLE);
            ((BookViewActivity)context).loadMindMap(title,((BookViewActivity)context).mainDesignView,context);
            ((BookViewActivity)context).bgImgView.setVisibility(View.INVISIBLE);
            ((BookViewActivity)context).designScrollPageView.setVisibility(View.INVISIBLE);
            ((BookViewActivity)context).instantiateNewPage(Integer.parseInt(((BookViewActivity)context).currentPageNumber), maxEnrichId);
            ((BookViewActivity)context).isEnrichmentCreated = true;
            if(((BookViewActivity)context).recyclerView!=null) {
                ((BookViewActivity)context).recyclerView.getAdapter().notifyDataSetChanged();
            }
            ((BookViewActivity)context).disableToolbarButtons();
            //System.out.println(enrichTabCountListAllPages);
            int enrTabCount = ((BookViewActivity)context).enrichTabCountListAllPages.get(Integer.parseInt(((BookViewActivity)context).currentPageNumber) - 1);
            enrTabCount = enrTabCount + 1;
            ((BookViewActivity)context).enrichTabCountListAllPages.set(Integer.parseInt(((BookViewActivity)context).currentPageNumber) - 1, enrTabCount);
            ((BookViewActivity)context).pageListView.invalidateViews();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ((BookViewActivity)context).recyclerView.getAdapter().notifyDataSetChanged();
            if (((BookViewActivity)context).enrTabsLayout.getWidth() > ((BookViewActivity)context).designPageLayout.getWidth()) {
                int enrTabsWidth = ((BookViewActivity)context).enrTabsLayout.getWidth();
                ((BookViewActivity)context).enrScrollView.smoothScrollTo(enrTabsWidth, 0);
            }

        }
    }

    public void openMindmapActivity(String rootNodeText, String orphanNodeText, String mindMapFname,Book book){
        if (!(context instanceof MainActivity)) {
            Globals.mindMapFileNameLastOpened = mindMapFname;
            Globals.mindMapLastBook=book;
        }
        Intent intent = new Intent(context.getApplicationContext(), MindMapActivity.class);
        intent.putExtra("MIRootNodeText", rootNodeText);
        intent.putExtra("MIOrphanNodeText", orphanNodeText);
        intent.putExtra("MindMapTitle", mindMapFname);
//        intent.putExtra("bookArray",bookListArray);
        intent.putExtra("book",book);
        ((Activity) context).startActivityForResult(intent, MindMapActivityRequestCode);
    }


    private void addMindmapAsbackground(int position, String objType){
        Book book =(Book)mindMapFileNames.get(position);
        final String mindMapFileName = book.getBookTitle();
        String imgFilePath = Globals.TARGET_BASE_MINDMAP_PATH+mindMapFileName+".png";
        ((BookViewActivity) context).showBackgroundDialog(imgFilePath, objType);
        settingsDialog.dismiss();
    }

    public class MindMapGridAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return mindMapFileNames.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
                view = ((Activity) context).getLayoutInflater().inflate(R.layout.mindmap_grid_inflate, null);
            }
            Book book =(Book)mindMapFileNames.get(position);
            final String mindMapFileName = book.getBookTitle();
            int BID = book.getBookID();
            String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/card.png";
            ImageView imgView = (ImageView) view.findViewById(R.id.imageView);
            Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
            imgView.setImageBitmap(bitmap);

            Button btnEdit = (Button) view.findViewById(R.id.btnEdit);
            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changingTheFile(position);
                }
            });

            Button btnDelete = (Button) view.findViewById(R.id.btnDelete);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(R.string.delete);
                    builder.setMessage(R.string.suredelete);
                    builder.setCancelable(false);
                    builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            Book book =(Book)mindMapFileNames.get(position);
                            final String mindMapFileName = book.getBookTitle();
                            File bookPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+book.getBookID());
                            UserFunctions.DeleteDirectory(bookPath);
                            File mindMapImgPath = new File(Globals.TARGET_BASE_MINDMAP_PATH+mindMapFileName+".png");
                            File mindMapXmlPath = new File(Globals.TARGET_BASE_MINDMAP_PATH+mindMapFileName+".xml");
                            if (mindMapImgPath.exists()){
                                mindMapImgPath.delete();
                                mindMapXmlPath.delete();
                                mindMapFileNames.remove(book);
                                mindMapGridAdapter.notifyDataSetChanged();
                                isMindMapDeleted = true;
                                // Delete Enrichment tabs and objects
                                String deleteEnrQuery = "delete from enrichments where Path='"+mindMapFileName+"' and Type='"+Globals.OBJTYPE_MINDMAPTAB+"'";
                                String deleteObjQuery = "delete from objects where content='"+mindMapFileName+".xml" +"' and objType='"+Globals.OBJTYPE_MINDMAPWEBTEXT+"'";
                                String deleteBookQuery = "delete from books where BID = '"+book.getBookID()+"'";
                                if (context instanceof BookViewActivity) {
                                    if (mindMapFileNames.size()==0){
                                        ((BookViewActivity)context).db.executeQuery("update tblCategory set isHidden='true' where CID='" + book.getbCategoryId() + "'and isHidden='false'");
                                    }
                                    ((BookViewActivity)context).db.executeQuery(deleteEnrQuery);
                                    ((BookViewActivity)context).db.executeQuery(deleteObjQuery);
                                    ((BookViewActivity)context).db.executeQuery(deleteBookQuery);
                                    ((BookViewActivity)context).gridShelf.bookListArray.remove(book);
                                    ((BookViewActivity)context).mindMapEdited=true;
                                    mindMapNameEdited=true;
                                } else if (context instanceof BookViewReadActivity){
                                    if (mindMapFileNames.size()==0){
                                        ((BookViewReadActivity)context).db.executeQuery("update tblCategory set isHidden='true' where CID='" + book.getbCategoryId() + "'and isHidden='false'");
                                    }
                                    ((BookViewReadActivity)context).db.executeQuery(deleteEnrQuery);
                                    ((BookViewReadActivity)context).db.executeQuery(deleteObjQuery);
                                    ((BookViewReadActivity)context).db.executeQuery(deleteBookQuery);
                                    ((BookViewReadActivity)context).gridShelf.bookListArray.remove(book);
                                    mindMapEditedForStrBok=true;
                                    ((BookViewReadActivity)context).mindMapEdited=true;
                                }else {
                                    if (mindMapFileNames.size()==0){
                                        ((MainActivity)context).db.executeQuery("update tblCategory set isHidden='true' where CID='" + book.getbCategoryId() + "'and isHidden='false'");
                                    }
                                    ((MainActivity)context).db.executeQuery(deleteEnrQuery);
                                    ((MainActivity)context).db.executeQuery(deleteObjQuery);
                                    ((MainActivity)context).db.executeQuery(deleteBookQuery);
                                    ((MainActivity)context).gridShelf.bookListArray.remove(book);
                                    mindMapeditedfrmShelf=true;
                                }
                            }
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });

            TextView txtTitle = (TextView) view.findViewById(R.id.textView5);
            txtTitle.setText(mindMapFileName);

            if (isForBackgroundImage){
                btnEdit.setVisibility(View.GONE);
                btnDelete.setVisibility(View.GONE);
            }

            return view;
        }
    }

    private void changingTheFile(final int position){
        Book book =(Book)mindMapFileNames.get(position);
        final String mindMapFileName = book.getBookTitle();
        final Dialog textDialog = new Dialog(context);
        textDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.semi_transparent)));
        textDialog.setTitle(R.string.edit_mindmap);
                textDialog.setContentView(((Activity) context).getLayoutInflater().inflate(R.layout.group_edit, null));
        textDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(280, context), RelativeLayout.LayoutParams.WRAP_CONTENT);
        final EditText editText = (EditText) textDialog.findViewById(R.id.enr_editText);
        editText.setText(mindMapFileName);
        //editText.setText(enrichmentTitle);
        Button btnSave = (Button) textDialog.findViewById(R.id.enr_save);
        btnSave.setOnClickListener(new View.OnClickListener() {

                                       @Override
                                       public void onClick(View v) {
                                           boolean exist = false;
                                           String enrTitle = editText.getText().toString();
                                           if (enrTitle.length() > 0) {
                                               for (int i = 0; i < mindMapFileNames.size(); i++) {
                                                   Book book = (Book) mindMapFileNames.get(i);
                                                   if (enrTitle.equals(book.getBookTitle())) {
                                                       exist = true;
                                                       break;
                                                   }
                                               }
                                               if (exist) {
                                                   UserFunctions.DisplayAlertDialog(context, R.string.mindmap_already_exist_msg, R.string.mindmap_exist);
                                               } else {
                                                   Book book =(Book)mindMapFileNames.get(position);
                                                   book.setBookTitle(enrTitle);
//                                                   mindMapFileNames.remove(position);
//                                                   mindMapFileNames.add(position, book);
                                                   File mindMapImgPath = new File(Globals.TARGET_BASE_MINDMAP_PATH + mindMapFileName + ".png");
                                                   File mindMapXmlPath = new File(Globals.TARGET_BASE_MINDMAP_PATH + mindMapFileName + ".xml");
                                                   File newImgPath = new File(Globals.TARGET_BASE_MINDMAP_PATH + enrTitle + ".png");
                                                   File newXmlPath = new File(Globals.TARGET_BASE_MINDMAP_PATH + enrTitle + ".xml");
                                                   File bookXmlPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID()+"/mindMaps/"+mindMapFileName + ".xml");
                                                   File newBookXmlPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID()+"/mindMaps/" +enrTitle + ".xml");
                                                   bookXmlPath.renameTo(newBookXmlPath);
                                                   mindMapImgPath.renameTo(newImgPath);
                                                   mindMapXmlPath.renameTo(newXmlPath);
                                                   mindMapGridAdapter.notifyDataSetChanged();
                                                   // Update Enrichment tabs and objects
                                                   String updateEnrQuery = "update enrichments set Path='" + enrTitle + "',Title='" + enrTitle + "' where Path='" + mindMapFileName + "' and Type='"+Globals.OBJTYPE_MINDMAPTAB+"'";
                                                   String updateObjQuery = "update objects set content='"+enrTitle+".xml"+"' where content='"+mindMapFileName+".xml"+"' and objType='"+Globals.OBJTYPE_MINDMAPWEBTEXT+"'";
                                                   String updateBookQuery = "update books set Title='"+enrTitle+"' where Title='"+mindMapFileName+"' and CID='"+Globals.mindMapCatId+"'";
                                                   if (context instanceof BookViewActivity) {
                                                       ((BookViewActivity)context).db.executeQuery(updateEnrQuery);
                                                       ((BookViewActivity)context).db.executeQuery(updateObjQuery);
                                                       ((BookViewActivity)context).db.executeQuery(updateBookQuery);
                                                       for (int i=0;i<((BookViewActivity)context).page.objectListArray.size();i++){
                                                           Object object = ((BookViewActivity)context).page.objectListArray.get(i);
                                                           if (object instanceof MindmapPlayer){
                                                               MindmapPlayer player = (MindmapPlayer)object;
                                                               player.setObjContent(enrTitle+".xml");
                                                           }
                                                       }
                                                       mindMapNameEdited=true;
                                                       ((BookViewActivity)context).mindMapEdited=true;
                                                   } else if(context instanceof BookViewReadActivity) {
                                                       ((BookViewReadActivity)context).db.executeQuery(updateEnrQuery);
                                                       ((BookViewReadActivity)context).db.executeQuery(updateObjQuery);
                                                       ((BookViewReadActivity)context).db.executeQuery(updateBookQuery);
                                                       ((BookViewReadActivity)context).mindMapEdited=true;
                                                       mindMapEditedForStrBok=true;
                                                   }else {
                                                       ((MainActivity)context).db.executeQuery(updateEnrQuery);
                                                       ((MainActivity)context).db.executeQuery(updateObjQuery);
                                                       ((MainActivity)context).db.executeQuery(updateBookQuery);
                                                       mindMapeditedfrmShelf=true;
                                                   }
                                                   textDialog.dismiss();
                                               }
                                           }
                                       }
                                   }

        );

        Button btnCancel = (Button) textDialog.findViewById(R.id.enr_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick (View v){
                // TODO Auto-generated method stub
                textDialog.dismiss();
            }
        });
        textDialog.show();

    }
    public void addInsidePage(String mindMapFileName){
        ((BookViewActivity) context).hidePageListView();
        int initialWebTextWidth = (int) context.getResources().getDimension(R.dimen.bookview_initial_text_width);
        int initialWebTextHeight = (int) context.getResources().getDimension(R.dimen.bookview_initial_text_height);
        int initialWebTextXpos = ((BookViewActivity) context).designPageLayout.getWidth() / 2 - initialWebTextWidth / 2;
        int initialWebTextYpos = ((BookViewActivity) context).designPageLayout.getHeight() / 2 - initialWebTextHeight / 2;
        String location = initialWebTextXpos + "|" + initialWebTextYpos;
        String size = initialWebTextWidth + "|" + initialWebTextHeight;
        String objContent=mindMapFileName+".xml";
        String type=Globals.OBJTYPE_MINDMAPWEBTEXT;
        String objScalePageToFit = "no";
        int objSequentialId = ((BookViewActivity) context).page.objectListArray.size() + 1;
        String objPageNo = ((BookViewActivity) context).currentPageNumber;
        int objBID = ((BookViewActivity) context).currentBook.getBookID();
        int enrichId=((BookViewActivity) context).page.enrichedPageId;
        int objUniqueId = ((BookViewActivity) context).db.getMaxUniqueRowID("objects") + 1;
        ((BookViewActivity) context).db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '"+type+"', '" + objBID + "', '" + objPageNo + "', '" + location + "', '" + size + "', '"+objContent+"', '" + objSequentialId + "', 'no', '" + false + "', '" + enrichId + "')");
        MindmapPlayer player=new MindmapPlayer((BookViewActivity)context,type,objBID,objPageNo,location,size,objContent,objUniqueId,objSequentialId,objScalePageToFit,false,enrichId,null,false);
        ((BookViewActivity) context).page.addMindMapLayout(player);
        UndoRedoObjectData uRObjData = new UndoRedoObjectData();
        uRObjData.setuRObjDataUniqueId(player.getObjectUniqueId());
        uRObjData.setuRObjDataCustomView(player);
        ((BookViewActivity) context).page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_DELETED, Globals.OBJECT_PREVIOUS_STATE);
        ((BookViewActivity) context).page.createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CREATED, Globals.OBJECT_PRESENT_STATE);

        //Remove the current handler area from the view and set the handler area to the newly added view
        ((BookViewActivity) context).page.removeHandlerArea();
        ((BookViewActivity) context).page.setCurrentView(player);

        ((BookViewActivity) context).page.makeSelectedArea(player);
    }

    public void addMindMapBook(String title){
        if (context instanceof MainActivity) {
            int lastBookId = ((MainActivity) context).db.getMaxUniqueRowID("books");
            bookId = lastBookId + 1;
            int categoryId = Globals.mindMapCatId;
            ((MainActivity) context).db.executeQuery("update tblCategory set isHidden='false' where CID='"+categoryId+"'and isHidden='true'");
            ((MainActivity) context).db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,storeID,ShowPageNo,CID,blanguage,StoreBookCreatedFromIpad,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,clientID,bookDirection) " +
                    "values('" + title + "','" + "MindMap" + "','Android','6','" + 1 + "','0','" + Globals.portrait + "','" + 1 + "','no','1','I" + bookId + "','no','" + categoryId + "','English','no','downloaded','','no','no','0.00','','created','0','ltr')");
            Templates template = ((MainActivity) context).gridShelf.getTemplates(1, 0,context);
            Book newBook = new Book(bookId, title, "MindMap", "Android", 6, 1, 0, Globals.portrait, 1, false, 1, "I" + bookId, null, false, null, null, template, false, categoryId, "English", "downloaded", "", "no", "no", "0.00", "", "created",0,"0","ltr");
            ((MainActivity) context).gridShelf.bookListArray.add(0, newBook);
            ((MainActivity) context).gridShelf.setNoOfBooks(((MainActivity) context).gridShelf.bookListArray.size());
            openMindmapActivity(title, "", title,newBook);
        }else if (context instanceof BookViewActivity){
            int lastBookId = ((BookViewActivity) context).db.getMaxUniqueRowID("books");
            bookId = lastBookId + 1;
            int categoryId = Globals.mindMapCatId;
            ((BookViewActivity) context).db.executeQuery("update tblCategory set isHidden='false' where CID='"+categoryId+"'and isHidden='true'");
            ((BookViewActivity) context).db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,storeID,ShowPageNo,CID,blanguage,StoreBookCreatedFromIpad,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,clientID,bookDirection) " +
                    "values('" + title + "','" + "MindMap" + "','Android','6','" + 1 + "','0','" + Globals.portrait + "','" + 1 + "','no','1','I" + bookId + "','no','" + categoryId + "','English','no','downloaded','','no','no','0.00','','created','0','ltr')");
            Templates template = ((BookViewActivity) context).gridShelf.getTemplates(1, 0,context);
            Book newBook = new Book(bookId, title, "MindMap", "Android", 6, 1, 0, Globals.portrait, 1, false, 1, "I" + bookId, null, false, null, null, template, false, categoryId, "English", "downloaded", "", "no", "no", "0.00", "", "created",0,"0","ltr");
            ((BookViewActivity) context).gridShelf.bookListArray.add(0, newBook);
            ((BookViewActivity) context).gridShelf.setNoOfBooks(((BookViewActivity) context).gridShelf.bookListArray.size());
            openMindmapActivity(title, "", title,newBook);
        }else if (context instanceof BookViewReadActivity){
            int lastBookId = ((BookViewReadActivity) context).db.getMaxUniqueRowID("books");
            bookId = lastBookId + 1;
            int categoryId = Globals.mindMapCatId;
            ((BookViewReadActivity) context).db.executeQuery("update tblCategory set isHidden='false' where CID='"+categoryId+"'and isHidden='true'");
            ((BookViewReadActivity) context).db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,storeID,ShowPageNo,CID,blanguage,StoreBookCreatedFromIpad,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,clientID,bookDirection) " +
                    "values('" + title + "','" + "MindMap" + "','Android','6','" + 1 + "','0','" + Globals.portrait + "','" + 1 + "','no','1','I" + bookId + "','no','" + categoryId + "','English','no','downloaded','','no','no','0.00','','created','0','ltr')");
            Templates template = ((BookViewReadActivity) context).gridShelf.getTemplates(6, 0,context);
            Book newBook = new Book(bookId, title, "MindMap", "Android", 6, 1, 0, Globals.portrait, 1, false, 1, "I" + bookId, null, false, null, null, template, false, categoryId, "English", "downloaded", "", "no", "no", "0.00", "", "created",0,"0","ltr");
            ((BookViewReadActivity) context).gridShelf.bookListArray.add(0, newBook);
            ((BookViewReadActivity) context).gridShelf.setNoOfBooks(((BookViewReadActivity) context).gridShelf.bookListArray.size());
            openMindmapActivity(title, "", title,newBook);
        }

        UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId);
        UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles");
        UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/mindMaps");
    }
    public class createMindMapTab extends AsyncTask<Void, Void, Void>{
        String title;
        public createMindMapTab( String s) {
            title=s;
        }

        @Override
        protected void onPreExecute() {
            title = title.replace("'","''");
            int objUniqueId = ((BookViewReadActivity) context).db.getMaxUniqueRowID("enrichments");
            ((BookViewReadActivity)context).db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,categoryID)values('"+((BookViewReadActivity)context).currentBook.getBookID()+"', '"+((BookViewReadActivity)context).currentPageNumber+"', '"+title+"', '"+Globals.OBJTYPE_MINDMAPTAB+"', '0','"+title+"','0')");
            int  sequentialId = ((BookViewReadActivity)context).db.getMaximumSequentialId(((BookViewReadActivity)context).currentPageNumber, ((BookViewReadActivity)context).currentBook.getBookID()) + 1;
            RelativeLayout layout=new RelativeLayout(context);
            layout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            EnrichmentButton enrichmentButton = new EnrichmentButton(context, Globals.OBJTYPE_MINDMAPTAB, objUniqueId, ((BookViewReadActivity) context).currentBook.getBookID(), ((BookViewReadActivity) context).currentPageNumber, title, ((BookViewReadActivity) context).currentEnrTabsLayout.getChildCount()+1, 0, title, ((BookViewReadActivity) context).currentEnrTabsLayout,sequentialId);
            //((BookViewReadActivity)context).clickedAdvanceSearchtab(enrichmentButton);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }
}
