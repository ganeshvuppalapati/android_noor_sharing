package com.semanoor.source_sboookauthor;

import android.content.Context;

import java.io.Serializable;

public class UserGroups implements Serializable{
	 private Context context;
	 private int userScopeId;
	 private String userName;
	 private int Enabled;
	 private String groupName;
	 private String emailId;
	 private boolean groupSelected;
	 private String  prevGrpName;
	 private int id;
	 private boolean delete;

	 public UserGroups(Context _context) {
			this.context = _context;
	 }

	 /**
	 * @return the userId
	 */
	 public int getUserScopeId() {
		return userScopeId;
	}

	/**
	 * @param userScopeId the userId to set
	 */
	public void setUserScopeId(int userScopeId) {
		this.userScopeId = userScopeId;
	}
	
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the userName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the emaild
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the userName to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	
	/**
	 * @return the scopeType
	 */
	public int getEnabled() {
		return Enabled;
	}

	/**
	 * @param enabled the scopeType to set
	 */
	public void setEnabled(int enabled) {
		this.Enabled = enabled;
	}
	/**
	 * @return the noteSelected
	 */
	public boolean isGroupSelected() {
		return groupSelected;
	}
	/**
	 * @param groupSelected the noteSelected to set
	 */
	public void setGroupSelected(boolean groupSelected) {
		this.groupSelected = groupSelected;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

}
