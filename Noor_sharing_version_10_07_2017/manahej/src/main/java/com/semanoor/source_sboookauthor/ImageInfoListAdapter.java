package com.semanoor.source_sboookauthor;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class ImageInfoListAdapter extends BaseAdapter implements ListAdapter {

	private BookViewActivity context;
	private Integer[] imgInfoListArray;
	private CustomImageRelativeLayout customImgRl;
	
	public ImageInfoListAdapter(BookViewActivity _context, Integer[] _imgInfoListArray, CustomImageRelativeLayout customImageRelativeLayout) {
		this.context = _context;
		this.imgInfoListArray = _imgInfoListArray;
		this.customImgRl = customImageRelativeLayout;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return imgInfoListArray.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = context.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
		}
		
		if (imgInfoListArray[position].equals(R.string.lock) || imgInfoListArray[position].equals(R.string.unlock)) {
			if (customImgRl.isObjLocked()) {
				imgInfoListArray[position] = R.string.unlock;
			} else {
				imgInfoListArray[position] = R.string.lock;
			}
		}
		
		TextView textView = (TextView) view.findViewById(R.id.textView1);
		textView.setTextColor(Color.BLACK);
		textView.setText(imgInfoListArray[position]);
		return view;
	}

}
