package com.semanoor.source_sboookauthor;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;
import com.semanoor.manahij.ThemeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Random;

public class GoogleImageSearchDialogWindow implements OnClickListener {

	private BookViewActivity ctx;
	private EditText txtSearchText;
	private ListView googleImageListView;
	public TextView totalResultsText;
	private Button prevBtn;
	private Button nextBtn;
	//Old Api
	//private int MAXIMUM_NO_OF_IMAGES = 8;
	//New Api
	private int MAXIMUM_NO_OF_IMAGES = 10;
	private long totalItems;
	public long currentJSONData;
	private int CURRENT_IMAGE_RESULTS = 0;
	String strSearch = null;
	private ArrayList<Object> listImages;
	private ProgressBar imageProgress;

	//Old Api
	/*private String TITLE = "title";
	private String THUMB_URL = "tbUrl";
	private String URL = "url";*/

	//New Api
	private String TITLE = "title";
	private String THUMB_URL = "thumbnailLink";
	private String URL = "link";

	private GoogleImageSearchAdapter adapter;
	private ImageLoader imageLoader;
	private RelativeLayout customRLayout;
	private Dialog googleImageSearchDialog;
    String text;
	String googleResult="GoogleSearch";
	Context ctxt;
	public GoogleImageSearchDialogWindow(Context context, RelativeLayout customRelativeLayout) {
		this.ctxt = context;
		this.customRLayout = customRelativeLayout;
	}


	/**
	 * @author callPopUpWindow for Youtube
	 *
	 */
	public void callDialogWindow(){

		googleImageSearchDialog = new Dialog(ctxt);
		googleImageSearchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(ctxt.getResources().getColor(R.color.semi_transparent)));
		googleImageSearchDialog.setTitle(R.string.search_google_images);
		if(ctxt instanceof  BookViewActivity){
			googleImageSearchDialog.setContentView(((BookViewActivity)ctxt).getLayoutInflater().inflate(R.layout.image_results, null));
		}else{
			googleImageSearchDialog.setContentView(((ThemeActivity)ctxt).getLayoutInflater().inflate(R.layout.image_results, null));
		}

		googleImageSearchDialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.4), (int) (Globals.getDeviceHeight() / 1.5));
		googleImageSearchDialog.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				if (imageLoader != null) {
					imageLoader.clearCache();
				}
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString(googleResult, txtSearchText.getText().toString());
				editor.commit();
			}
		});

		txtSearchText = (EditText) googleImageSearchDialog.findViewById(R.id.editText1);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
		text = prefs.getString(googleResult, "");
		txtSearchText.setText("");
		txtSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					if (!txtSearchText.getText().equals("")) {
						searchClicked();
					}
				}
				return false;
			}
		});

		googleImageListView = (ListView) googleImageSearchDialog.findViewById(R.id.listView1);
		prevBtn =(Button)googleImageSearchDialog.findViewById(R.id.button2);
		prevBtn.setOnClickListener(this);
		nextBtn =(Button)googleImageSearchDialog.findViewById(R.id.button3);
		nextBtn.setOnClickListener(this);
		prevBtn.setEnabled(false);
		nextBtn.setEnabled(false);
		totalResultsText = (TextView)googleImageSearchDialog.findViewById(R.id.textView1);
		imageProgress = (ProgressBar) googleImageSearchDialog.findViewById(R.id.progressBar1);
		imageProgress.setVisibility(View.GONE);
		googleImageListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position,
									long arg3) {
				if (UserFunctions.isInternetExist(ctxt)) {
					new saveAndSetGoogleImages(position).execute();
				} else {
					UserFunctions.DisplayAlertDialogNotFromStringsXML(ctxt, ctx.getResources().getString(R.string.check_internet_connectivity), ctxt.getResources().getString(R.string.connection_error));
				}
			}
		});
		if (!text.equals("")&& text!=null){
			txtSearchText.setText(text);
			searchClicked();
		}
		googleImageSearchDialog.show();

	}

	class saveAndSetGoogleImages extends AsyncTask<Void, Void, Void>
	{
		private int position;
		String imageUrl;
		ProgressDialog dialog;
		String tempGoogleImagePath;

		public saveAndSetGoogleImages(int position) {
			this.position = position;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = ProgressDialog.show(ctxt, "", "Please wait...");
			GoogleImageSearchData googleImageSearchData = (GoogleImageSearchData) listImages.get(position);
			imageUrl = googleImageSearchData.getUrl();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				URL url = new URL(imageUrl);
				Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
				String imgDir;
				if(ctxt instanceof  BookViewActivity) {
					imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + ((BookViewActivity)ctxt).currentBook.getBookID() + "/" + "images/";
				}else{
					String imageUrl=((ThemeActivity)ctxt).filePath;
					imgDir =imageUrl+"/";
				}
				File fileImgDir = new File(imgDir);
				if (!fileImgDir.exists()) {
					fileImgDir.mkdir();
				}
				tempGoogleImagePath = imgDir+"googleImage_temp.png";
				UserFunctions.saveBitmapImage(bitmap, tempGoogleImagePath);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(ctxt instanceof BookViewActivity) {
				if (customRLayout instanceof CustomImageRelativeLayout) {
					CustomImageRelativeLayout imageRLayout = (CustomImageRelativeLayout) customRLayout;
					imageRLayout.setImageBackground(tempGoogleImagePath);
					imageRLayout.setUndoRedoForImageContentChanged(tempGoogleImagePath, null);
					imageRLayout.SaveUriFile(tempGoogleImagePath);
				} else if (customRLayout instanceof CustomWebRelativeLayout) {
					CustomWebRelativeLayout webRLayout = (CustomWebRelativeLayout) customRLayout;
					String quiz_imgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ((BookViewActivity) ctxt).currentBook.getBookID() + "/images/" + "" + webRLayout.quizImageFileName + ".png";
					webRLayout.SaveUriFile(tempGoogleImagePath, quiz_imgPath);
					String quiz_CamImageName = webRLayout.quizImageFileName;
					String bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + ((BookViewActivity) ctxt).currentBook.getBookID();
					int randomNoToRefresh = new Random().nextInt();

					CustomWebRelativeLayout.loadJSScript("javascript:document.getElementById('" + quiz_CamImageName + "').src=\"/" + bookDir + "/images/" + quiz_CamImageName + ".png?" + randomNoToRefresh + "\"", webRLayout.webView);

				} else if (customRLayout instanceof CustomWebRelativeLayout) {


				}
			}else{
				((ThemeActivity) ctxt).SaveUriFile(tempGoogleImagePath);
			}

			File file = new File(tempGoogleImagePath);
			if (file.exists()) {
				file.delete();
			}
			if(dialog.isShowing())
			{
				dialog.dismiss();
			}
			googleImageSearchDialog.dismiss();
		}
	}

	public class getImagesTask extends AsyncTask<Void, Void, Void>
	{
		JSONObject json;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			imageProgress.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			URL url;
			try {
				//Old Api
				//url = new URL("https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q="+strSearch+"&rsz="+MAXIMUM_NO_OF_IMAGES+"&start="+(currentJSONData+1)+"");
				//New Api
				url = new URL("https://www.googleapis.com/customsearch/v1?cx="+Globals.googleImgSearchId+"&q="+strSearch+"&searchType=image&start="+(currentJSONData+1)+"&key="+Globals.googleYoutubeSearchKey+"");

				URLConnection connection = url.openConnection();

				String line;
				StringBuilder builder = new StringBuilder();
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				while((line = reader.readLine()) != null) {
					builder.append(line);
				}

				json = new JSONObject(builder.toString());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			imageProgress.setVisibility(View.GONE);

			try {
				if (json != null) {
					//Old Api
					/*JSONObject responseObject = json.getJSONObject("responseData");
					totalItems = responseObject.getJSONObject("cursor").getInt("estimatedResultCount");
					JSONArray resultArray = responseObject.getJSONArray("results");*/

					//New Api
					totalItems = json.getJSONObject("searchInformation").getInt("totalResults");
					JSONArray resultArray = json.getJSONArray("items");
					currentJSONData = currentJSONData + resultArray.length();
					CURRENT_IMAGE_RESULTS = resultArray.length();

					listImages = getImageList(resultArray);
					if (currentJSONData == 0) {
						totalResultsText.setText("No results");
						prevBtn.setEnabled(false);
						nextBtn.setEnabled(false);
					} else {
						if (currentJSONData / MAXIMUM_NO_OF_IMAGES <= 1) {
							String resultText = "About " + totalItems + " results";
							prevBtn.setEnabled(false);
							nextBtn.setEnabled(true);
							totalResultsText.setText(resultText);
						} else {
							//System.out.println("current jsondata:"+currentJSONData);
							int currentPage = (int) currentJSONData / MAXIMUM_NO_OF_IMAGES;
							if (currentJSONData % MAXIMUM_NO_OF_IMAGES != 0) {
								currentPage = currentPage + 1;
								totalResultsText.setText("Page " + currentPage + " of about " + totalItems + " results");
							} else {
								totalResultsText.setText("Page " + currentPage + " of about " + totalItems + " results");
							}
						}
						SetListViewAdapter(listImages);
					}
				}
			} catch (JSONException e) {
				nextBtn.setEnabled(false);
				e.printStackTrace();
			}

		}
	}

	public ArrayList<Object> getImageList(JSONArray resultArray)
	{
		ArrayList<Object> listImages = new ArrayList<Object>();
		GoogleImageSearchData googleImgSearchData;

		try
		{
			for(int i=0; i<resultArray.length(); i++)
			{
				JSONObject obj;
				obj = resultArray.getJSONObject(i);
				googleImgSearchData = new GoogleImageSearchData();
				googleImgSearchData.setTitle(obj.getString(TITLE));
				//Old Api
				//googleImgSearchData.setThumbUrl(obj.getString(THUMB_URL));
				//New Api
				googleImgSearchData.setThumbUrl(obj.getJSONObject("image").getString(THUMB_URL));
				googleImgSearchData.setUrl(obj.getString(URL));

				listImages.add(googleImgSearchData);
			}
			return listImages;
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public void SetListViewAdapter(ArrayList<Object> images)
	{
		imageLoader=new ImageLoader(ctxt);
		adapter = new GoogleImageSearchAdapter(ctxt, images, imageLoader);
		googleImageListView.setAdapter(adapter);
	}

	/**
	 * this performs the google image search
	 */
	private void searchClicked() {
		if (UserFunctions.isInternetExist(ctxt)) {
			strSearch = txtSearchText.getText().toString();
			strSearch = Uri.encode(strSearch);
			currentJSONData = 0;
			new getImagesTask().execute();
		} else {
			UserFunctions.DisplayAlertDialogNotFromStringsXML(ctxt, ctxt.getResources().getString(R.string.check_internet_connectivity), ctxt.getResources().getString(R.string.connection_error));
		}
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.button2){	 //Prev clicked
			if(currentJSONData >=MAXIMUM_NO_OF_IMAGES*2){
				prevBtn.setEnabled(true);
				nextBtn.setEnabled(true);
				currentJSONData = currentJSONData - (CURRENT_IMAGE_RESULTS + MAXIMUM_NO_OF_IMAGES);
				new getImagesTask().execute();
			}else{
				prevBtn.setEnabled(false);
			}
		}else if(v.getId() == R.id.button3){	//Next clicked
			if(currentJSONData < MAXIMUM_NO_OF_IMAGES*MAXIMUM_NO_OF_IMAGES){
				prevBtn.setEnabled(true);
				if (currentJSONData == MAXIMUM_NO_OF_IMAGES*MAXIMUM_NO_OF_IMAGES) {
					nextBtn.setEnabled(false);
				} else {
					nextBtn.setEnabled(true);
				}
				new getImagesTask().execute();
			}else{
				nextBtn.setEnabled(false);
			}
		}
	}
}
