package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.util.Base64;

import com.semanoor.manahij.SharedUsers;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.Resources;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class XMLParser extends DefaultHandler {

	private WebService wbservice;
	private StringBuilder builder;
	private HashMap<String, String> hashMap;
	private boolean isEnrichmentStatus;
	private Context context;
	//public DatabaseHandler db;
	private SharedUsers shared;
	
	public XMLParser(Context context, WebService webService) {
		this.wbservice = webService;
		this.context = context;
	}

	public void parseXml(String returnXml) {
		try{
			SAXParserFactory mySAXParserFactory = SAXParserFactory.newInstance();
			SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
			XMLReader myXMLReader = mySAXParser.getXMLReader();
			myXMLReader.setContentHandler(this);
			InputSource inputsource = new InputSource();
			inputsource.setEncoding("UTF-8");
			inputsource.setCharacterStream(new StringReader(returnXml));
			myXMLReader.parse(inputsource);
		}catch(MalformedURLException e){
			e.printStackTrace();
		}catch(ParserConfigurationException e){
			e.printStackTrace();
		}catch(SAXException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
	} 
	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
 		super.startElement(uri, localName, qName, attributes);
		//System.out.println("qname:"+qName);
		builder = new StringBuilder();
		if (localName.equals("PasswordQues") || localName.equals("Groups")) {
			hashMap = new HashMap<String, String>();
		}
		else if (localName.equals("Country")) {
			String countryArabName = attributes.getValue("ArabicName");
			String countryEnName = attributes.getValue("EnglishName");
			String countryID = attributes.getValue("ID");
			String countryFlag = attributes.getValue("Flag");
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(wbservice.KEY_COUNTRY_ID, countryID);
			map.put(wbservice.KEY_COUNTRY_EN_NAME, countryEnName);
			map.put(wbservice.KEY_COUNTRY_AR_NAME, countryArabName);
			map.put(wbservice.KEY_COUNTRY_FLAG, countryFlag);
			wbservice.countryHashMapList.add(map);
		} else if(localName.equals("University")) {
			String universityArabName = attributes.getValue("ArabicName");
			String universityEnName = attributes.getValue("EnglishName");
			String universityID = attributes.getValue("ID");
			String universityFlag = attributes.getValue("Flag");
			String universityCountryId = attributes.getValue("CountryID");
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(wbservice.KEY_UNIVERSITY_ID, universityID);
			map.put(wbservice.KEY_UNIVERSITY_EN_NAME, universityEnName);
			map.put(wbservice.KEY_UNIVERSITY_AR_NAME, universityArabName);
			map.put(wbservice.KEY_UNIVERSITY_FLAG, universityFlag);
			map.put(wbservice.KEY_UNIVERSITY_COUNTRY_ID, universityCountryId);
			wbservice.universityHashMapList.add(map);
		} else if (localName.equals("Category")) {
			String categoryArabName = attributes.getValue("ArabicName");
			String categoryEnName = attributes.getValue("EnglishName");
			String categoryID = attributes.getValue("ID");
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(wbservice.KEY_CATEGORY_ID, categoryID);
			map.put(wbservice.KEY_CATEGORY_EN_NAME, categoryEnName);
			map.put(wbservice.KEY_CATEGORY_AR_NAME, categoryArabName);
			wbservice.categoryHashMapList.add(map);
		} else if (localName.equals("Price")) {
			String priceValue = attributes.getValue("Value");
			wbservice.priceList.add(priceValue);
		} else if (localName.equals("book")) {
			String bookTitle = attributes.getValue("Title");
			String bookStatus = attributes.getValue("Status");
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(wbservice.KEY_BOOK_STATUS_TITLE, bookTitle);
			map.put(wbservice.KEY_BOOK_STATUS, bookStatus);
			wbservice.bookStatusList.add(map);
		} else if (localName.equals("user")) {
			String userId = attributes.getValue("ID");
			String userName = attributes.getValue("Name");
			String userEmail = attributes.getValue("EMail");
			String userCanPublish = attributes.getValue("isPublish");
			String userAuthorizeDirectPublishingId = attributes.getValue("AuthorizedDirectlyPublishing");
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(wbservice.KEY_ENRICH_USER_ID, userId);
			map.put(wbservice.KEY_ENRICH_USER_NAME, userName);
			map.put(wbservice.KEY_ENRICH_USER_EMAIL, userEmail);
			map.put(wbservice.KEY_ENRICH_USER_CANPUBLISH, userCanPublish);
			map.put(wbservice.KEY_ENRICH_USER_AUTHORIZE_DIRECT_PUBLISH_ID, userAuthorizeDirectPublishingId);
			wbservice.enrichUserLoginHashMapDatails = map;
		} else if (localName.equals("enriched") && wbservice.isForParsingEnrichments) {
			String enrId = attributes.getValue("ID");
			String enrPageNo = attributes.getValue("IDPage");
			String userId = attributes.getValue("IDUser");
			String userName = attributes.getValue("UserName");
			String enrTitle = attributes.getValue("Title");
			String enrPath = attributes.getValue("Path");
			String enrDateUp = attributes.getValue("DateUp");
			String enrDate = attributes.getValue("Date");
			String enrPassword = attributes.getValue("Password");
			String enrSize = attributes.getValue("Size");
			String bookTitle = attributes.getValue("BookTitle");
			String bookId = attributes.getValue("BookID");
			String emailId = attributes.getValue("Email");
			
			Enrichments enrichment = new Enrichments(context);
			enrichment.setEnrichmentId(Integer.parseInt(enrId));
			enrichment.setEnrichmentPageNo(Integer.parseInt(enrPageNo));
			enrichment.setEnrichmentUserId(Integer.parseInt(userId));
			enrichment.setEnrichmentUserName(userName);
			enrichment.setEnrichmentTitle(enrTitle);
			enrichment.setEnrichmentPath(enrPath);
			enrichment.setEnrichmentDateUp(enrDateUp);
			enrichment.setEnrichmentDate(enrDate);
			enrichment.setEnrichmentPassword(enrPassword);
			enrichment.setEnrichmentSize(enrSize);
			enrichment.setEnrichmentBookTitle(bookTitle);
			enrichment.setEnrichmentBid(Integer.parseInt(bookId));
			enrichment.setEnrichmentEmailId(emailId);
			if (wbservice.isLocalFileParsing) {
				wbservice.downloadedEnrichmentList.add(enrichment);
			} else {
				boolean enrichmentDownloaded = false;
				boolean enrichmentBlocked = false;
				if (wbservice.downloadedEnrichmentList == null || wbservice.downloadedEnrichmentList.size() == 0) {
					//if(!db.sharedUsersBlackList(userName)){
					if(wbservice.readEnrichments!=null) {
						for (int i = 0; i < wbservice.readEnrichments.size(); i++) {
							String id = wbservice.readEnrichments.get(i);
							if (id.equals("" + enrichment.getEnrichmentId())) {
								enrichment.setReadStatus(true);
								//enrichmentDownloaded = true;
								break;
							}
						}
					}
					  wbservice.enrichmentList.add(enrichment);
					//}else{
					//	wbservice.enrichmentList.add(enrichment);
					//}	
				} else {
					for (int i = 0; i < wbservice.downloadedEnrichmentList.size(); i++) {
						Enrichments enrich = wbservice.downloadedEnrichmentList.get(i);
						if (enrich.getEnrichmentId() == enrichment.getEnrichmentId()) {
							enrichment.setDownloaded(true);
							//enrichmentDownloaded = true;
							break;
						}
					}
					if(wbservice.readEnrichments!=null) {
						for (int i = 0; i < wbservice.readEnrichments.size(); i++) {
							String id = wbservice.readEnrichments.get(i);
							if (id.equals("" + enrichment.getEnrichmentId())) {
								enrichment.setReadStatus(true);
								//enrichmentDownloaded = true;
								break;
							}
						}
					}
					for(int j=0;j<wbservice.blockmaild.size();j++){
						SharedUsers block_mailid=wbservice.blockmaild.get(j);
						if(block_mailid.getSharedUsersEmailId().equals(enrichment.getEnrichmentEmailId()) && block_mailid.getSharedUsersBlacklist().equals("yes")){
							enrichmentBlocked = true;
							break;
						}
					}
					//if (!enrichmentDownloaded && !enrichmentBlocked) {
					if (!enrichmentBlocked) {
					    //if(!db.sharedUsersBlackList(userName)){
							wbservice.enrichmentList.add(enrichment);
						//}else{
							//wbservice.enrichmentList.add(enrichment);
						//}
					}
				  }
				}
			
		} else if (localName.equals("enriched")) {
			if (isEnrichmentStatus) {
				String id = attributes.getValue("ID");
				String isPublished = attributes.getValue("isPublished");
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(wbservice.KEY_ENRICH_STATUS_ID, id);
				map.put(wbservice.KEY_ENRICH_ISPUBLISHED, isPublished);
				wbservice.enrichStatusHashMapList.add(map);
			} else {
				String id = attributes.getValue("ID");
				wbservice.enrichedPageResponseServerIdList.add(Integer.parseInt(id));
			}
		} else if (localName.equals("Enriched")) {
			String responseResult = attributes.getValue("Result");
			wbservice.enrichedPageUpdateResponseResult.add(responseResult);
		} else if (localName.equals("check")) {
			isEnrichmentStatus = true;
		} else if (localName.equals("Resource")) {
			String resId = attributes.getValue("ResourceID");
			String userId = attributes.getValue("IDUser");
			String userName = attributes.getValue("UserName");
			String resTitle = attributes.getValue("Title");
			String resPath = attributes.getValue("Path");
			String resDateUp = attributes.getValue("DateUp");
			String resDate = attributes.getValue("Date");
			String resSize = attributes.getValue("Size");
			String bookTitle = attributes.getValue("BookTitle");
			String bookId = attributes.getValue("BookID");
			String emailId = attributes.getValue("Email");
			String resPageNo = attributes.getValue("IDPage");

			Resources resource = new Resources(context);
			resource.setResourceId(Integer.parseInt(resId));
			resource.setResourceUserId(Integer.parseInt(userId));
			resource.setResourceUserName(userName);
			resource.setResourceTitle(resTitle);
			resource.setResourcePath(resPath);
			resource.setResourceDateUp(resDateUp);
			resource.setResourceDate(resDate);
			resource.setResourceSize(resSize);
			resource.setResourceBookTitle(bookTitle);
			resource.setResourceBid(Integer.parseInt(bookId));
			resource.setResourceEmailId(emailId);
			resource.setResourcePageNo(Integer.parseInt(resPageNo));
			if (wbservice.isLocalFileParsing) {
				wbservice.downloadedResourceList.add(resource);
			} else {
				boolean resourceDownloaded = false;
				boolean resourceBlocked= false;
				if (wbservice.downloadedResourceList == null || wbservice.downloadedResourceList.size() == 0) {
					if(wbservice.readResources!=null) {
						for (int i = 0; i < wbservice.readResources.size(); i++) {
							String id = wbservice.readResources.get(i);
							if (id.equals("" + resource.getResourceId())) {
								resource.setReadStatus(true);
								//enrichmentDownloaded = true;
								break;
							}
						}
					}
					     wbservice.resourceList.add(resource);
				} else {
					for (int i = 0; i < wbservice.downloadedResourceList.size(); i++) {
						Resources res = wbservice.downloadedResourceList.get(i);
						if (res.getResourceId() == resource.getResourceId()) {
							resource.setResourceDownloaded(true);
							break;
						}
					}
					if(wbservice.readResources!=null) {
						for (int i = 0; i < wbservice.readResources.size(); i++) {
							String id = wbservice.readResources.get(i);
							if (id.equals("" + resource.getResourceId())) {
								resource.setReadStatus(true);
								//enrichmentDownloaded = true;
								break;
							}
						}
					}
					for(int j=0;j<wbservice.blockmaild.size();j++){
						SharedUsers block_mailid=wbservice.blockmaild.get(j);
						if(block_mailid.getSharedUsersEmailId().equals(resource.getResourceEmailId())&& block_mailid.getSharedUsersBlacklist().equals("yes")){
							resourceBlocked = true;
							break;
						}
					}
					if (!resourceBlocked) {
							wbservice.resourceList.add(resource);
					}
				}
			}
		} else if (localName.equals("ENRICHMENT")) {
			String objId = attributes.getValue("CENRICHMENT");
			String enrMsg = attributes.getValue("EMESSAGE");
			String enrContent = attributes.getValue("ECONTENT");
			if (!wbservice.downloadedCloudEnrList.contains(Integer.parseInt(objId))) {
				try {
					byte[] data = Base64.decode(enrContent, Base64.DEFAULT);
					enrContent = new String(data, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}

				HashMap<String, String> hashMap = new HashMap<String, String>();    //filter cloudEnrList here
				hashMap.put(wbservice.KEY_CENRICHMENTS, objId);
				hashMap.put(wbservice.KEY_CEMESSAGE, enrMsg);
				hashMap.put(wbservice.KEY_CECONTENT, enrContent);
				hashMap.put(wbservice.KEY_CLOUDENRSELECTED, "false");
				wbservice.cloudEnrList.add(hashMap);
			}
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String tempString = new String(ch, start, length);
		builder.append(tempString);
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("QuestionEN")) {
			hashMap.put(wbservice.KEY_QUESTION_EN_NAME, builder.toString());
		} else if (localName.equals("QuestionAR")) {
			hashMap.put(wbservice.KEY_QUESTION_AR_NAME, builder.toString());
		} else if (localName.equals("QuestionId")) {
			hashMap.put(wbservice.KEY_QUESTION_ID, builder.toString());
		} else if (localName.equals("PasswordQues")) {
			wbservice.questionHashMapList.add(hashMap);
		}
		else if (localName.equals("GroupEnName")) {
			hashMap.put(wbservice.KEY_GROUP_EN_NAME, builder.toString());
		} else if (localName.equals("GroupArName")) {
			hashMap.put(wbservice.KEY_GROUP_AR_NAME, builder.toString());
		} else if (localName.equals("GroupId")) {
			hashMap.put(wbservice.KEY_GROUP_ID, builder.toString());
		} else if (localName.equals("Groups")) {
			wbservice.groupHashMapList.add(hashMap);
		}
		else if (localName.equals("RegistratonUserResult")) {
			wbservice.SignUpResponse=builder.toString();
		}else if (localName.equals("CheckUserNamePasswordResult")) {
			if (builder.toString().equals("true")) {
				wbservice.LogInResponse=true;
			}else {
				wbservice.LogInResponse=false;
			}
		} else if (localName.equals("LoginResult")) {
			wbservice.newSignInResponse = builder.toString();
		} else if (localName.equals("GadgetRegistrationResult")) {
			wbservice.newSignUpResponse = builder.toString();
		} else if (localName.equals("AddNewEbooKIpadJunkResult")) {
			wbservice.uploadBookResponse = builder.toString();
		} else if (localName.equals("UploadJunkFileIpadResult")) {
			wbservice.uploadJunkFileIpadResult = builder.toString();
		} else if (localName.equals("AddNewBookResult")) {
			wbservice.uploadBookResponse = builder.toString();
		} else if (localName.equals("PublishEnriched_Step1Result") || localName.equals("UpdateEnriched_Step1Result")) {
			wbservice.enrichUploadBookResponse = builder.toString();
		} else if (localName.equals("InsertAddNewEbookIpadJunkResult")) {
			wbservice.insertAddNewEbookIpadJunkResult = builder.toString();
		} else if (localName.equals("check")) {
			isEnrichmentStatus = false;
		} else if (localName.equals("note")) {
			String noteStr = builder.toString();
			String[] noteStrSplit = noteStr.split("\\|");
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(wbservice.KEY_NOTE_BOOK_ID, noteStrSplit[0]);
			map.put(wbservice.KEY_NOTE_PAGE_NO, noteStrSplit[1]);
			map.put(wbservice.KEY_NOTE_STEXT, noteStrSplit[2]);
			map.put(wbservice.KEY_NOTE_OCCURENCE, noteStrSplit[3]);
			map.put(wbservice.KEY_NOTE_SDESC, noteStrSplit[4]);
			map.put(wbservice.KEY_NOTE_NPOS, noteStrSplit[5]);
			map.put(wbservice.KEY_NOTE_COLOR, noteStrSplit[6]);
			map.put(wbservice.KEY_NOTE_PROCESS_STEXT, noteStrSplit[7]);
			map.put(wbservice.KEY_NOTE_ENRICHID, noteStrSplit[8]);
			wbservice.noteList.add(map);
			
		} else if (localName.equals("link")) {
			String linkStr = builder.toString();
			String[] linkStrSplit = linkStr.split("\\|");
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(wbservice.KEY_LINK_PAGE_NO, linkStrSplit[2]);
			map.put(wbservice.KEY_LINK_TITLE, linkStrSplit[3]);
			map.put(wbservice.KEY_LINK_URL, linkStrSplit[5]);
			wbservice.linkList.add(map);
		} else if (localName.equals("mindmap")) {
			String mindMapStr = builder.toString();
			String[] splitMindMpaStr = mindMapStr.split("\\|");
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(wbservice.KEY_MI_PAGE_NO, splitMindMpaStr[2]);
			map.put(wbservice.KEY_MI_TITLE, splitMindMpaStr[3]);
			map.put(wbservice.KEY_MI_PATH_NAME, splitMindMpaStr[5].replace(".xml", ""));
			map.put(wbservice.KEY_MI_CONTENT, splitMindMpaStr[7]);
			wbservice.mindmapList.add(map);
		} else if(localName.equals("SaveResourceDetailsResult")){
			wbservice.ResourceId=Integer.parseInt(builder.toString());
		}else if(localName.equals("UploadJunkFileIpadResult")){
			wbservice.uploadJunkFileIpadResult=builder.toString();
		} else if(localName.equals("GetMediaLibraryCategoryUploadResult")){
			wbservice.mediaCategoryResponseResult = builder.toString();
		} else if(localName.equals("GetMediaLibraryMediaByUserIDResult")){
			wbservice.importMediaResponseResult=builder.toString();
		} else if(localName.equals("getUserGroupsResult")){
			wbservice.userGrpData=builder.toString();
		} /*else if(localName.equals("saveUserGroupsResult")){
			wbservice.saveuserGrpData=builder.toString();
		}*/
		else if (localName.equals("DeleteEnrichmentByUserIDResult")) {
			//wbservice.enrichmentsList=builder.toString();
			try {
				//JSONObject jsonData = new JSONObject(builder.toString());
				JSONArray store1 = new JSONArray(builder.toString());
				for (int i = 0; i < store1.length(); i++) {
					JSONObject json1 = store1.getJSONObject(i);
					String Id=json1.getString("ID");
					String result=json1.getString("Result");
                    if(result.equals("true")){
						wbservice.enrichmentsList.put("ID",Id);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

	}
}
