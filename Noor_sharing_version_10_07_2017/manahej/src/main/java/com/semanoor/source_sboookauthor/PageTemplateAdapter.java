package com.semanoor.source_sboookauthor;

import java.io.File;
import java.io.InputStream;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PageTemplateAdapter extends BaseAdapter {

	private BookViewActivity context;
	private Integer[] pageTemplatesArray;
	private String untitledText, textContent, bulletTextContent, leftBulletTextContent, rightTextContent, pageNotextContent;
	private int untitledTextWidth, untitledTextHeight, textContentWidth, textContentHeight, bulletTextContentWidth, bulletTextContentHeight, leftBulletTextContentWidth, leftBulletTextContentHeight, rightTextContentWidth, rightTextContentHeight;
	private int untitledTextWidthSpacing, untitledTextHeightSpacing, textContentWidthSpacing, textContentHeightSpacing, bulletTextContentWidthSpacing, bulletTextContentHeightSpacing, leftBulletTextContentWidthSpacing, leftBulletTextContentHeightSpacing, rightTextContentSizeSpacing;
	private Templates templates;
	private int pageWidth, pageHeight;
	
	public PageTemplateAdapter(BookViewActivity _context,
			Integer[] _pageTemplatesArray) {
		this.context = _context;
		this.pageTemplatesArray = _pageTemplatesArray;
		pageWidth = Globals.getDeviceWidth();
		pageHeight = (int) (Globals.getDeviceHeight() - context.getResources().getDimension(R.dimen.shelf_toolbar_height));
		initiateValues();
	}
	
	/**
	 * Initialise all values for the default page template
	 */
	private void initiateValues() {
		templates = context.currentBook.getTemplate();
		int fontSize = (int) (context.getResources().getDimension(R.dimen.bookview_new_para_text_size) - 5);
		String fontFamily = templates.getTemplateParaFont();
		String[] rgbFontColor = templates.getTemplateParaColor().split("-");
		untitledTextWidthSpacing = (int) context.getResources().getDimension(R.dimen.template_untitled_text_width_spacing);
		untitledTextHeightSpacing = (int) context.getResources().getDimension(R.dimen.template_untitled_text_height_spacing);
		untitledTextWidth = pageWidth - untitledTextWidthSpacing;
		untitledTextHeight = untitledTextHeightSpacing;
		int untitledTextWidth_px = Globals.getPixelValue(untitledTextWidth, context);
		int untitledTextHeight_px = Globals.getPixelValue(untitledTextHeight, context);
		untitledText = "<div id=\"content\" contenteditable=\"true\" style=\"background-color: transparent; font-size: 36px; color: rgb("+rgbFontColor[0]+", "+rgbFontColor[1]+", "+rgbFontColor[2]+"); " +
				"direction: ltr; text-align: center; width: "+untitledTextWidth_px+"px; height: "+untitledTextHeight_px+"px;box-sizing:border-box;font-family: "+fontFamily+";\">UNTITLED</div>";
		
		textContentWidthSpacing = (int) context.getResources().getDimension(R.dimen.template_text_content_width_spacing);
		textContentHeightSpacing = (int) context.getResources().getDimension(R.dimen.template_text_content_height_spacing);
		textContentWidth = pageWidth - textContentWidthSpacing;
		textContentHeight = pageHeight - textContentHeightSpacing;
		int textContentWidth_px = Globals.getPixelValue(textContentWidth, context);
		int textContentHeight_px = Globals.getPixelValue(textContentHeight, context);
		textContent = "<div id=\"content\" contenteditable=\"true\" style=\"background-color: transparent; visibility: visible; font-size: "+fontSize+"px; color: rgb("+rgbFontColor[0]+", "+rgbFontColor[1]+", "+rgbFontColor[2]+"); " +
						"direction: ltr; width: "+textContentWidth_px+"px; height: "+textContentHeight_px+"px; text-align: left;box-sizing:border-box;font-family: "+fontFamily+";\"><div><br></div><div><br></div>" +
						"&nbsp; &nbsp; &nbsp; Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore " +
						"magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in " +
						"reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia " +
						"deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.</div>";
		
		bulletTextContentWidthSpacing = (int) context.getResources().getDimension(R.dimen.template_bullet_text_content_width_spacing);
		bulletTextContentHeightSpacing = (int) context.getResources().getDimension(R.dimen.template_bullet_text_content_height_spacing);
		bulletTextContentWidth = pageWidth - bulletTextContentWidthSpacing;
		bulletTextContentHeight = pageHeight - bulletTextContentHeightSpacing;
		int bulletTextContentWidth_px = Globals.getPixelValue(bulletTextContentWidth, context);
		int bulletTextContentHeight_px = Globals.getPixelValue(bulletTextContentHeight, context);
		bulletTextContent = "<div id=\"content\" contenteditable=\"true\" style=\"background-color: transparent; visibility: visible; font-size: "+fontSize+"px; color: rgb("+rgbFontColor[0]+", "+rgbFontColor[1]+", "+rgbFontColor[2]+"); " +
						"direction: ltr; width: "+bulletTextContentWidth_px+"px; height: "+bulletTextContentHeight_px+"px; text-align: left;box-sizing:border-box;font-family: "+fontFamily+";\"><div><br></div>" +
						"<ul><li>Lorem ipsum dolor sit amet, ligula suspendisse nulla pretium, rhoncus tempor placerat fermentum, enim integer ad vestibulum volutpat.</li>" +
						"<li>Nisl rhoncus turpis est, vel elit, congue wisi enim nunc ultricies sit, magna tincidunt. Maecenas aliquam maecenas ligula nostra, accumsan taciti.</li>" +
						"<li>Sociis mauris in integer, a dolor netus non dui aligquet, sagittis felis sodales, dolor sociis mauris, vel eu libero cras.</li>" +
						"<li>Interdum at. Edget habitasse elementum est, ipsum purus pede porttitor class, ut adipiscing, aliquet sed auctor, imperdiet arcu per diam dapibus " +
						"libero duis.</li><li>Enim eros in vel. Volutpat nec pellentesque leo, temporibus scelerisque nec.</li>" +
						"<li>Ac dolor ac adipiscing amet bibendum nullam, massa lacus molestie ut libero nec, diam et, pharetra sodales eget, feugiat ullamcorper id " +
						"tempor eget id vitae.</li></ul></div>";
		
		leftBulletTextContentWidthSpacing = (int) context.getResources().getDimension(R.dimen.template_left_bullet_text_content_width_spacing);
		leftBulletTextContentHeightSpacing = (int) context.getResources().getDimension(R.dimen.template_left_bullet_text_content_height_spacing);
		leftBulletTextContentWidth = (pageWidth - leftBulletTextContentWidthSpacing) / 2;
		leftBulletTextContentHeight = pageHeight - leftBulletTextContentHeightSpacing;
		int leftBulletTextContentWidth_px = Globals.getPixelValue(leftBulletTextContentWidth, context);
		int leftBulletTextContentHeight_px = Globals.getPixelValue(leftBulletTextContentHeight, context);
		leftBulletTextContent = "<div id=\"content\" contenteditable=\"true\" style=\"background-color: transparent; visibility: visible; font-size: "+fontSize+"px; color: rgb("+rgbFontColor[0]+", "+rgbFontColor[1]+", "+rgbFontColor[2]+"); " +
				"direction: ltr; width: "+leftBulletTextContentWidth_px+"px; height: "+leftBulletTextContentHeight_px+"px; text-align: left;box-sizing:border-box;font-family: "+fontFamily+";\"><div><br></div>" +
						"<ul><li>Lorem ipsum dolor sit amet, ligula suspendisse nulla pretium, rhoncus tempor placerat fermentum, enim integer ad vestibulum volutpat.</li>" +
						"<li>Nisl rhoncus turpis est, vel elit, congue wisi enim nunc ultricies sit, magna tincidunt. Maecenas aliquam maecenas ligula nostra, accumsan taciti.</li>" +
						"<li>Sociis mauris in integer, a dolor netus non dui aligquet, sagittis felis sodales, dolor sociis mauris, vel eu libero cras.</li>" +
						"<li>Interdum at. Edget habitasse elementum est, ipsum purus pede porttitor class, ut adipiscing, aliquet sed auctor, imperdiet arcu per diam dapibus" +
						" libero duis.</li><li>Enim eros in vel. Volutpat nec pellentesque leo, temporibus scelerisque nec.</li>" +
						"<li>Ac dolor ac adipiscing amet bibendum nullam, massa lacus molestie ut libero nec, diam et, pharetra sodales eget, feugiat ullamcorper id tempor " +
						"eget id vitae.</li></ul></div>";
		
		rightTextContentSizeSpacing = (int) context.getResources().getDimension(R.dimen.template_right_text_content_size);
		int rImgWidth = (pageWidth - rightTextContentSizeSpacing)/3;
		rightTextContentWidth = pageWidth - (rImgWidth + rightTextContentSizeSpacing);
		rightTextContentHeight = (pageHeight - rightTextContentSizeSpacing)/2;
		int rightTextContentWidth_px = Globals.getPixelValue(rightTextContentWidth, context);
		int rightTextContentHeight_px = Globals.getPixelValue(rightTextContentHeight, context);
		rightTextContent = "<div id=\"content\" contenteditable=\"true\" style=\"background-color: transparent; visibility: visible; font-size: "+fontSize+"px; color: rgb("+rgbFontColor[0]+", "+rgbFontColor[1]+", "+rgbFontColor[2]+"); " +
				"direction: ltr; width: "+rightTextContentWidth_px+"px; height: "+rightTextContentHeight_px+"px; text-align: left;box-sizing:border-box; font-family: "+fontFamily+"; padding:5px;\"><div><br></div><div><div>&nbsp; &nbsp; &nbsp; &nbsp; " +
				"Lorem ipsum dolor sit amet, ligula suspendisse nulla pretium, rhoncus tempor placerat fermentum, enim integer ad vestibulum volutpat.Nisl " +
				"rhoncus turpis est, vel elit, congue wisi enim nunc ultricies sit, magna tincidunt. Maecenas aliquam maecenas ligula nostra, accumsan taciti.Sociis mauris " +
				"in integer, a dolor netus non dui aligquet, sagittis felis sodales, dolor sociis mauris, vel eu libero cras.</div><div>Interdum at. Edget habitasse elementum " +
				"est, ipsum purus pede porttitor class, ut adipiscing, aliquet sed auctor, imperdiet arcu per diam dapibus libero duis.</div><div><br></div><div>Enim eros in " +
				"vel. Volutpat nec pellentesque leo, temporibus scelerisque nec. Ac dolor ac adipiscing amet bibendum nullam, massa lacus molestie ut libero nec, diam et, " +
				"pharetra sodales eget, feugiat ullamcorper id tempor eget id vitae.</div></div></div>";
	}
	
	/**
	 * Add WebText To database
	 * @param location
	 * @param size
	 * @param objContent
	 * @param objSequentialId
	 * @param pageNumber
	 * @param enrichPageId
	 */
	private void addWebTextToDB(String location, String size, String objContent, int objSequentialId, int pageNumber, int enrichPageId, String objType){
		int objBID = context.currentBook.getBookID();
		String objSclaPageToFit = "no";
		boolean objLocked = false;
		context.db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+objType+"', '"+objBID+"', '"+pageNumber+"', '"+location+"', '"+size+"', '"+objContent+"', '"+objSequentialId+"', '"+objSclaPageToFit+"', '"+objLocked+"', '"+enrichPageId+"')");
	}
	
	/**
	 * Add Image To database
	 * @param location
	 * @param size
	 * @param objPathContent
	 * @param objSequentialId
	 * @param pageNumber
	 * @param enrichPageId
	 */
	private void addImageObjectToDB(String location, String size, String objPathContent, int objSequentialId, int pageNumber, int enrichPageId){
		String imageType = "Image";
		int objBID = context.currentBook.getBookID();
		String objSclaPageToFit = "no";
		boolean objLocked = false;
		context.db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+imageType+"', '"+objBID+"', '"+pageNumber+"', '"+location+"', '"+size+"', '"+objPathContent+"', '"+objSequentialId+"', '"+objSclaPageToFit+"', '"+objLocked+"', '"+enrichPageId+"')");
	}
	
	/**
	 * call this method to add default template
	 * @param templateSelectionVal
	 * @param pageNumber
	 * @param enrichPageId
	 */
	public void addDefaultTemplate(int templateSelectionVal, int pageNumber, int enrichPageId) {
		switch (templateSelectionVal) {
		case 1:{
			//Add Title and large textbox
			String objType = "WebText";
			int xPos = untitledTextWidthSpacing/2;
			int yPos = untitledTextHeightSpacing;
			String location = xPos+"|"+yPos;
			String size = untitledTextWidth+"|"+untitledTextHeight;
			String objContent = untitledText;
			int objSequentialId = 1;
			addWebTextToDB(location, size, objContent, objSequentialId, pageNumber, enrichPageId, objType);
			
			//Add large textbox :
			int xPos1 = textContentWidthSpacing/2;
			int yPos1 = textContentHeightSpacing - untitledTextHeight;
			String location1 = xPos1+"|"+yPos1;
			String size1 = textContentWidth+"|"+textContentHeight;
			String objContent1 = textContent;
			int objSequentialId1 = 2;
			addWebTextToDB(location1, size1, objContent1, objSequentialId1, pageNumber, enrichPageId, objType);
			
			addPageNumberObject(pageNumber, enrichPageId);
			
			break;
		}
			
		case 2:{
			//Add Title and an image
			String objType = "WebText";
			int xPos = untitledTextWidthSpacing/2;
			int yPos = untitledTextHeightSpacing;
			String location = xPos+"|"+yPos;
			String size = untitledTextWidth+"|"+untitledTextHeight;
			String objContent = untitledText;
			int objSequentialId = 1;
			addWebTextToDB(location, size, objContent, objSequentialId, pageNumber, enrichPageId, objType);
			
			//Add Image
			int xPos1 = textContentWidthSpacing/2;
			int yPos1 = textContentHeightSpacing - untitledTextHeight;
			String location1 = xPos1+"|"+yPos1;
			String size1 = textContentWidth+"|"+textContentHeight;
			int objSequentialId1 = 2;
			String objPathContent = saveImageToPath(textContentWidth, textContentHeight);
			addImageObjectToDB(location1, size1, objPathContent, objSequentialId1, pageNumber, enrichPageId);
			
			addPageNumberObject(pageNumber, enrichPageId);
			
			break;
		}
		
		case 3: {
			//Add Title and Bullet Text
			String objType = "WebText";
			int xPos = untitledTextWidthSpacing/2;
			int yPos = untitledTextHeightSpacing;
			String location = xPos+"|"+yPos;
			String size = untitledTextWidth+"|"+untitledTextHeight;
			String objContent = untitledText;
			int objSequentialId = 1;
			addWebTextToDB(location, size, objContent, objSequentialId, pageNumber, enrichPageId, objType);
			
			//Add Bullettext
			int xPos1 = textContentWidthSpacing/2;
			int yPos1 = textContentHeightSpacing - untitledTextHeight;
			String location1 = xPos1+"|"+yPos1;
			String size1 = bulletTextContentWidth+"|"+bulletTextContentHeight;
			String objContent1 = bulletTextContent;
			int objSequentialId1 = 2;
			addWebTextToDB(location1, size1, objContent1, objSequentialId1, pageNumber, enrichPageId, objType);
			
			addPageNumberObject(pageNumber, enrichPageId);
			
			break;
		}
		
		case 4: {
			//Add Bullet Text and Image
			String objType = "WebText";
			int xyPos = leftBulletTextContentWidthSpacing/3;
			String location = xyPos+"|"+xyPos;
			String size = leftBulletTextContentWidth+"|"+leftBulletTextContentHeight;
			String objContent = leftBulletTextContent;
			int objSequentialId = 1;
			addWebTextToDB(location, size, objContent, objSequentialId, pageNumber, enrichPageId, objType);
			
			//Add Image
			int xPos = leftBulletTextContentWidth + (xyPos * 2);
			String location1 = xPos+"|"+xyPos;
			String size1 = leftBulletTextContentWidth+"|"+leftBulletTextContentHeight;
			int objSequentialId1 = 2;
			String objPathContent = saveImageToPath(leftBulletTextContentWidth, leftBulletTextContentHeight);
			addImageObjectToDB(location1, size1, objPathContent, objSequentialId1, pageNumber, enrichPageId);
			
			addPageNumberObject(pageNumber, enrichPageId);
			
			break;
		}
		
		case 5: {
			//Add Three Images.
			//Add First Image
			int xyPos = untitledTextHeightSpacing;
			String location = xyPos+"|"+xyPos;
			int imgWidth = (pageWidth - (xyPos * 3))/2;
			int imgHeight = (pageHeight - (xyPos * 3))/2;
			String size = imgWidth+"|"+imgHeight;
			int objSequentialId = 1;
			String objPathContent = saveImageToPath(imgWidth, imgHeight);
			addImageObjectToDB(location, size, objPathContent, objSequentialId, pageNumber, enrichPageId);
			
			//Add Second Image
			int yPos = imgHeight + (xyPos * 2);
			String location1 = xyPos+"|"+yPos;
			int objSequentialId1 = 2;
			String objPathContent1 = saveImageToPath(imgWidth, imgHeight);
			addImageObjectToDB(location1, size, objPathContent1, objSequentialId1, pageNumber, enrichPageId);
			
			//Add Third Image
			int xPos = imgWidth + (xyPos * 2);
			String location2 = xPos+"|"+xyPos;
			int objSequentialId2 = 3;
			int imgHeight2 = (imgHeight * 2) + xyPos;
			String size2 = imgWidth+"|"+imgHeight2;
			String objPathContent2 = saveImageToPath(imgWidth, imgHeight2);
			addImageObjectToDB(location2, size2, objPathContent2, objSequentialId2, pageNumber, enrichPageId);
			
			addPageNumberObject(pageNumber, enrichPageId);
			
			break;
		}
		
		case 6: {
			//Add Image and text to right
			int xyPos = untitledTextHeightSpacing;
			String location = xyPos+"|"+xyPos;
			int imgWidth = (pageWidth - (xyPos * 3))/3;
			int imgHeight = (pageHeight - (xyPos * 3))/2;
			String size = imgWidth+"|"+imgHeight;
			int objSequentialId = 1;
			String objPathContent = saveImageToPath(imgWidth, imgHeight);
			addImageObjectToDB(location, size, objPathContent, objSequentialId, pageNumber, enrichPageId);
			
			//Add Text to right
			String objType = "WebText";
			int xPos = imgWidth + (xyPos * 2);
			String location1 = xPos+"|"+xyPos;
			String size1 = rightTextContentWidth+"|"+rightTextContentHeight;
			String objContent = rightTextContent;
			int objSequentialId1 = 2;
			addWebTextToDB(location1, size1, objContent, objSequentialId1, pageNumber, enrichPageId, objType);
			
			addPageNumberObject(pageNumber, enrichPageId);
			
			break;
		}
		
		case 7: {
			int xyPos = untitledTextHeightSpacing;
			String location = xyPos+"|"+xyPos;
			int imgWidth = pageWidth - (xyPos * 2);
			int imgHeight = pageHeight - (xyPos * 2);
			String size = imgWidth+"|"+imgHeight;
			int objSequentialId = 1;
			String objPathContent = saveImageToPath(imgWidth, imgHeight);
			addImageObjectToDB(location, size, objPathContent, objSequentialId, pageNumber, enrichPageId);
			
			addPageNumberObject(pageNumber, enrichPageId);
			
			break;
		}
		
		case 8: {
			addPageNumberObject(pageNumber, enrichPageId);
			break;
		}

		default:
			break;
		}
	}
	
	/**
	 * Add Page Number Object
	 * @param pageNumber
	 * @param enrichPageId
	 */
	private void addPageNumberObject(int pageNumber, int enrichPageId){
		if (!context.currentBook.is_bStoreBook() && context.currentBook.is_bShowPageNo()) {
			//Add Page Number Object
			int initPageNoSize = (int) context.getResources().getDimension(R.dimen.bookview_initial_text_page_no_size);
			String objType = Globals.objType_pageNoText;
			String location2 = context.currentBook.getPageNoXPos()+"|"+context.currentBook.getPageNoYPos();
			String size2 = initPageNoSize+"|"+initPageNoSize;
			String objContent2 = context.currentBook.get_bPageNoContent();
			int objSequentialId2 = 3;
			addWebTextToDB(location2, size2, objContent2, objSequentialId2, pageNumber, enrichPageId, objType);
		}
	}
	
	/**
	 * Save Image To path and return that path
	 * @param width
	 * @param height
	 * @return
	 */
	private String saveImageToPath(int width, int height){
		int objUniqueId = context.db.getMaxUniqueRowID("objects")+1;
		String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/"+"images/";
		File fileImgDir = new File(imgDir);
		if (!fileImgDir.exists()) {
			fileImgDir.mkdir();
		}
		String objPathContent = imgDir+objUniqueId+".png";
		InputStream inputStream = null;
		/*try {
			 //inputStream=Globals.TARGET_BASE_TEMPATES_PATH+templates.getTemplateId()+"/front_P.png";
			inputStream = context.getAssets().open("templates/"+templates.getTemplateId()+"/front_P.png");
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		Bitmap frontBitmap = BitmapFactory.decodeFile(Globals.TARGET_BASE_TEMPATES_PATH+templates.getTemplateId()+"/front_P.png");
		Bitmap frontScaleBitmap = Bitmap.createScaledBitmap(frontBitmap, width, height, true);
		UserFunctions.saveBitmapImage(frontScaleBitmap, objPathContent);
		return objPathContent;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return pageTemplatesArray.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return pageTemplatesArray[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (view == null) {
			view = context.getLayoutInflater().inflate(R.layout.templates_gridlayout, null);
		}
		TextView tv = (TextView) view.findViewById(R.id.text);
		tv.setText(pageTemplatesArray[position]);
		ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
		if (position == 0) {
			iv.setBackgroundResource(R.drawable.page_template_1);
		} else if (position == 1) {
			iv.setBackgroundResource(R.drawable.page_template_2);
		} else if (position == 2) {
			iv.setBackgroundResource(R.drawable.page_template_3);
		} else if (position == 3) {
			iv.setBackgroundResource(R.drawable.page_template_4);
		} else if (position == 4) {
			iv.setBackgroundResource(R.drawable.page_template_5);
		} else if (position == 5) {
			iv.setBackgroundResource(R.drawable.page_template_6);
		} else if (position == 6) {
			iv.setBackgroundResource(R.drawable.page_template_7);
		} else if (position == 7) {
			iv.setBackgroundResource(R.drawable.page_template_8);
		}
		
		return view;
	}

}
