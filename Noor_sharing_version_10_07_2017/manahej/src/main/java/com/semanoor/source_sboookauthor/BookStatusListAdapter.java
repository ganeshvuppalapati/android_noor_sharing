package com.semanoor.source_sboookauthor;

import java.util.ArrayList;
import java.util.HashMap;

import com.semanoor.manahij.ExportActivity;
import com.semanoor.manahij.R;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class BookStatusListAdapter extends BaseAdapter {

	private ArrayList<HashMap<String, String>> bookStatusList;
	private ExportActivity context;
	
	public BookStatusListAdapter(
			ExportActivity exportActivity, ArrayList<HashMap<String, String>> _bookStatusList) {
		this.bookStatusList = _bookStatusList;
		this.context = exportActivity;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return bookStatusList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = context.getLayoutInflater().inflate(R.layout.bookstatus_popup, null);
		}
		
		TextView tv_bookTitle = (TextView) view.findViewById(R.id.tv_bookTitle);
		TextView tv_bookStatus = (TextView) view.findViewById(R.id.tv_approveStatus);
		HashMap<String, String> map = bookStatusList.get(position);
		tv_bookTitle.setText("Book : "+map.get(context.webService.KEY_BOOK_STATUS_TITLE));
		tv_bookStatus.setText("Status : "+map.get(context.webService.KEY_BOOK_STATUS));
		return view;
	}

}
