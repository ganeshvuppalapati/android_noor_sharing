package com.semanoor.source_sboookauthor;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.pm.PackageManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.ClickedFilePath;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.PopOverColorPicker.ClrPickerListener;
import com.semanoor.source_sboookauthor.PopoverView.PopoverViewDelegate;
import com.semanoor.source_sboookauthor.PopupColorPicker.ColorPickerListener;

public class CustomWebRelativeLayout extends RelativeLayout implements WebTextJavascriptInterfaceListener, OnTouchListener, OnClickListener, PopoverViewDelegate, OnFocusChangeListener {

	public static BookViewActivity context;
	private int prevX;
	public int prevY;

	public String objType;
	private int objBid, objEnrichId;
	private String objPageNo;
	public int mWebTextXpos, mWebTextYpos, mWebTextWidth, mWebTextHeight, mWebTableRow, mWebTableColumn;
	public int mWebTextPrevXpos, mWebTextPrevYpos, mWebTextPrevWidth, mWebTextPrevHeight, mWebTablePrevRow, mWebTablePrevColumn;
	public int mImageXpos, mImageYpos, mImageWidth, mImageHeight;
	private String objLocation, objSize;
	public String objContent;
	private int objUniqueId;
	private int objSequentialId, prevObjSequentialId;
	private String objScalePageToFit;
	private boolean objLocked, prevObjLocked;
	public HelpWebView webView;
	private WebTextJavascriptInterface webTextJSInterface;
	public int fontSize;
	public String fontFamily;
	public String fontColor;
	public Button btnInfo;
	public String backgroundColor;
	public boolean objContentUpdated = false;
	public String prevObjContent;
	public boolean isURObjContent = false;

	public static boolean SearchClicked, mindMapMenuClicked, hyperLinkClicked;
	private static Integer[] webTextInfoListArray = {R.string.size, R.string.font, R.string.colour, R.string.nil, R.string.background_color, R.string.nil, R.string.lock, R.string.nil, R.string.bring_to_front, R.string.send_to_back, R.string.nil, R.string.text_align};
	private static Integer[] webTableInfoListArray = {R.string.cell_color, R.string.row_color, R.string.column_color, R.string.background_color, R.string.font_color, R.string.size, R.string.font, R.string.add_row, R.string.delete_row, R.string.add_column, R.string.delete_column, R.string.lock, R.string.nil, R.string.bring_to_front, R.string.send_to_back};
	//private static Integer[] quizTextInfoListArray = {R.string.langage,R.string.size, R.string.font, R.string.nil, R.string.colour,R.string.nil, R.string.background_color, R.string.nil, R.string.lock,  R.string.bring_to_front,R.string.nil, R.string.send_to_back,  R.string.answers,R.string.nil,R.string.text_align};
	private static Integer[] quizTextInfoListArray = {R.string.langage, R.string.size, R.string.font, R.string.colour, R.string.nil, R.string.background_color, R.string.nil, R.string.lock, R.string.nil, R.string.bring_to_front, R.string.send_to_back, R.string.nil, R.string.answers, R.string.nil, R.string.text_align};

	private static Integer[] imgInfoListArray = {R.string.search_google_images, R.string.pick_photo, R.string.take_photo_camera, R.string.nil, R.string.send_to_library, R.string.add_from_Filemanager};
	private static Integer[] pageTextInfoListArray = {R.string.size, R.string.font, R.string.colour, R.string.nil, R.string.page_number_position, R.string.nil, R.string.background_color, R.string.nil, R.string.bring_to_front, R.string.send_to_back};

	public ArrayList<QuizSequenceObject> quizSequenceObjectArray;
	public int quizSequenceId;

	private View currentSelectedListView;
	public Boolean firstTimeStartUp = true;
	public int CAPTURE_WEB_IMAGE_ACTIVITY_REQUEST_CODE = 500;
	public int MEDIA_TYPE_IMAGE = 1;
	public int MEDIA_TYPE_VIDEO = 2;

	private GestureDetector gestureDetector;
	public int CAPTURE_FROM_GALLERY = 4;
	public boolean textSelected;
	//Table info buttons :
	public String tableCellColor;
	public String tableRowColor;
	public String tableColumnColor;
	public String tableBackgroundColor;
	public String tableFontColor;
	public static String selecttext;
	public String quizImageFileName;
	public Uri imageFileUri;
	private ArrayList<String> mindMapFileNames;

	private final Object jsReturnValueLock = new Object();
	private final Map<Integer, String> jsReturnValues = new HashMap<Integer, String>();
	private String version;

	public String Language;


	private boolean shouldOverride = false;
	String currentFilePath;
	ArrayList<ClickedFilePath> selectedFiles = new ArrayList<ClickedFilePath>();
	PopoverView popoverView;
	int rowPosition,columnPosition;

	public CustomWebRelativeLayout(BookViewActivity _context, String _type, int _BID, String _objPageNo, String _location, String _size, String _objContent, int _objUniqueId, int _objSequentialId, String _objScalePageToFit, boolean _objLocked, int _objEnrichId, AttributeSet _attrs) {
		super(_context, _attrs);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			version = "KITKAT";
		} else {
			version = "ICS";
		}
		this.context = _context;
		this.objType = _type;
		this.setObjBid(_BID);
		this.setObjPageNo(_objPageNo);
		this.setObjLocation(_location);
		this.setObjSize(_size);
		mWebTextXpos = Integer.parseInt(getObjLocation().split("\\|")[0]);
		mWebTextYpos = Integer.parseInt(getObjLocation().split("\\|")[1]);
		if (_type.equals("WebTable")) {
			mWebTextWidth = Integer.parseInt(getObjSize().split("\\|")[0]);
			mWebTextHeight = Integer.parseInt(getObjSize().split("\\|")[1]);
			setmWebTableRow(Integer.parseInt(getObjSize().split("\\|")[2]));
			setmWebTableColumn(Integer.parseInt(getObjSize().split("\\|")[3]));
		} else {
			mWebTextWidth = Integer.parseInt(getObjSize().split("\\|")[0]);
			mWebTextHeight = Integer.parseInt(getObjSize().split("\\|")[1]);
		}

		this.objContent = _objContent;
		this.prevObjContent = objContent;
		this.objUniqueId = _objUniqueId;
		this.objSequentialId = _objSequentialId;
		this.setObjScalePageToFit(_objScalePageToFit);
		this.setObjLocked(_objLocked);
		this.setObjEnrichId(_objEnrichId);

		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mWebTextWidth, mWebTextHeight);

		if (_type.equals("WebTable") || _type.equals(Globals.OBJTYPE_TITLETEXT) || _type.equals(Globals.OBJTYPE_AUTHORTEXT) || _type.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
			this.setBackgroundResource(R.drawable.border);
		}
		if (_type.equals(Globals.objType_pageNoText)) {
			int pno = Integer.parseInt(getObjPageNo()) - 1;
			this.objContent = this.objContent.concat(pno + "</div>");
		}
		if (_type.equals(Globals.OBJTYPE_TITLETEXT)) {
			String[] split = objContent.split(">");
			String titleText = split[0] + ">" + context.currentBook.getBookTitle() + "</div>";
			this.objContent = titleText;
		} else if (_type.equals(Globals.OBJTYPE_AUTHORTEXT)) {
			String[] split = objContent.split(">");
			String authorText = split[0] + ">" + context.currentBook.getBookAuthorName() + "</div>";
			this.objContent = authorText;
		} else if (_type.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
			this.quizSequenceObjectArray = context.db.getQuizSequenceArray(objUniqueId);
			this.quizSequenceId = 0;
		}
		context.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				setUpandLoadWebView();

				//if ((context.currentBook.is_bStoreBook() && objEnrichId != 0) || !(context.currentBook.is_bStoreBook())) {
				if (objScalePageToFit.equals("no")) {
					RelativeLayout toolBarlayout = new RelativeLayout(context);
					 RelativeLayout.LayoutParams tbLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
					tbLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
					tbLayoutParams.setMargins(5, 5, 5, 0);
					toolBarlayout.setLayoutParams(tbLayoutParams);
					//toolBarlayout.setBackgroundColor(context.getResources().getColor(R.color.toolbar_bluecolor));
					CustomWebRelativeLayout.this.addView(toolBarlayout);

					int dpDimen = (int) context.getResources().getDimension(R.dimen.objects_btn_size);
					btnInfo = new Button(context);
					btnInfo.setBackgroundResource(R.drawable.button_info_black);
					btnInfo.setId(R.id.btnInfo);
					btnInfo.setVisibility(GONE);
					btnInfo.setOnClickListener(CustomWebRelativeLayout.this);

					Button btnObjDelete = new Button(context);
					btnObjDelete.setBackgroundResource(R.drawable.delete_normal_new);
					btnObjDelete.setId(R.id.btn_delete_object);
					btnObjDelete.setOnClickListener(CustomWebRelativeLayout.this);
					//btnObjDelete.setX(btnInfo.getWidth() + dpDimen);

					if (objType.equals(Globals.objType_pageNoText)) {
						int pageNoDpDimen = (int) context.getResources().getDimension(R.dimen.objects_page_no_btn_size);
						int pageNoDelPos = (int) context.getResources().getDimension(R.dimen.objects_page_no_del_pos);
						RelativeLayout.LayoutParams btnDelLayoutParams = new RelativeLayout.LayoutParams(pageNoDpDimen, pageNoDpDimen);
						btnDelLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						btnObjDelete.setLayoutParams(btnDelLayoutParams);
						toolBarlayout.addView(btnInfo, pageNoDpDimen, pageNoDpDimen);
						toolBarlayout.addView(btnObjDelete);
						//btnObjDelete.setX(btnInfo.getWidth() + pageNoDelPos);
					} else {
						RelativeLayout.LayoutParams btnDelLayoutParams = new RelativeLayout.LayoutParams(dpDimen, dpDimen);
						btnDelLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						btnObjDelete.setLayoutParams(btnDelLayoutParams);
						toolBarlayout.addView(btnInfo, dpDimen, dpDimen);
						toolBarlayout.addView(btnObjDelete);
						//btnObjDelete.setX(btnInfo.getWidth() + dpDimen);
					}
				}
			}
		});

		this.setX(mWebTextXpos);
		this.setY(mWebTextYpos);
		this.setLayoutParams(layoutParams);

		context.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				gestureDetector = new GestureDetector(context, new GestureListener());
			}
		});

	}

	private boolean checkForStoreBookEnrichmentMalzamah() {
		if ((context.currentBook.is_bStoreBook() && context.currentBook.get_bStoreID().contains("C")) || (context.currentBook.is_bStoreBook() && getObjEnrichId() != 0) || !(context.currentBook.is_bStoreBook())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @author setUpandLoadWebView
	 */
	private void setUpandLoadWebView() {
		webView = new HelpWebView(context);
		//if (this.getObjScalePageToFit().equals("no")) {
		webView.setBackgroundColor(0 * 00000000);
		webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
		//}
		webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebChromeClient(new WebChromeClient());
		webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		webView.getSettings().setAllowContentAccess(true);

		// Zoom out fully
		if (this.getObjScalePageToFit().equals("yes")) {
			webView.getSettings().setLoadWithOverviewMode(true);
			webView.getSettings().setUseWideViewPort(true);
			webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
			//webView.setBackgroundColor(Color.TRANSPARENT);
		}
		webView.getSettings().setSupportZoom(true);
		webView.getSettings().setBuiltInZoomControls(false);
		webView.getSettings().setDisplayZoomControls(false);
		webView.getSettings().setDomStorageEnabled(true);

		webView.getSettings().setRenderPriority(RenderPriority.HIGH);
		webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		webView.setHorizontalScrollBarEnabled(false);
		webView.setVerticalScrollBarEnabled(false);
		webView.clearCache(true);
		webView.setFocusable(true);
		webView.setOnFocusChangeListener(this);
		//	webView.setWebViewClient(new CustomWebViewClient());
		webView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				//System.out.println("OnLongClick");
				//webView.sendView(webView);
				//loadJSScript("javascript:window.getSelection();",webView);
				// webView.getSplit(selText1)

				//loadJSScript("console.log(window.getSelection().baseNode.nodeValue);",webView);
				return false;
			}
		});
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				if (objType.equals("WebText") || objType.equals(Globals.objType_pageNoText) || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT) || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT) || objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
					if (objContent.equals("") || objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
						int pxWidth = Globals.getPixelValue(mWebTextWidth, context);
						int pxHeight = Globals.getPixelValue(mWebTextHeight, context);
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
							loadJSScript("javascript:document.getElementById('content').style.width = '" + pxWidth + "px'", webView);
							loadJSScript("javascript:document.getElementById('content').style.height = '" + pxHeight + "px'", webView);
						} else {
							loadJSScript("javascript:document.getElementById('content').style.width = '" + pxWidth + "px'", webView);
							loadJSScript("javascript:document.getElementById('content').style.height = '" + pxHeight + "px'", webView);
						}
						if (objType.equals("WebText") || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT) || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
							updateDefaultTextProperties("content");
						} else if (objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
							resizeMindmapDivWidthAndHeight(mWebTextWidth, mWebTextHeight);
						}
						//Update MarginTop for webtext
						if (CustomWebRelativeLayout.this.objScalePageToFit.equals("no") && (objType.equals("WebText") || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT))) {
							loadJSScript("javascript:document.getElementById('content').style.marginTop = '30px'", webView);
						}
					}
					if (!objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
						updateTextProperties(view, "content");
					}
					updateTextProperties(view, "content");
					if (objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
						QuizSequenceObject quizSeqObject = quizSequenceObjectArray.get(quizSequenceId);
						getQuizLanguage(quizSeqObject.getUniqueid(), quizSeqObject.getSectionId(), view);
					}
				} else if (objType.equals("WebTable")) {
					if (objContent.equals("")) {
						//int pxWidth = Globals.getPixelValue(mWebTextWidth, context);
						//int pxHeight = Globals.getPixelValue(mWebTextHeight, context);
						//webView.loadUrl("javascript:document.getElementById('SBATable').style.width = '"+pxWidth+"px'");
						//webView.loadUrl("javascript:document.getElementById('SBATable').style.height = '"+pxHeight+"px'");
						updateDefaultTextProperties("SBATable");
					}
					updateTableTextProperties(view, "SBATable");
				} else if (objType.equals("TextSquare")) {
					if (objContent.equals("")) {
						int pxWidth = Globals.getPixelValue(mWebTextWidth, context);
						int pxHeight = Globals.getPixelValue(mWebTextHeight, context);
						loadJSScript("javascript:document.getElementById('content').style.width = '" + pxWidth + "px'", webView);
						loadJSScript("javascript:document.getElementById('content').style.height = '" + pxHeight + "px'", webView);
						loadJSScript("javascript:document.getElementById('content').style.backgroundColor = '#56bec2'", webView);
						loadJSScript("javascript:document.getElementById('content').style.marginTop = '30px'", webView);
						updateDefaultTextProperties("content");
					}
					updateTextProperties(view, "content");
				} else if (objType.equals("TextRectangle")) {
					if (objContent.equals("")) {
						int pxWidth = Globals.getPixelValue(mWebTextWidth, context);
						int pxHeight = Globals.getPixelValue(mWebTextHeight, context);
						loadJSScript("javascript:document.getElementById('content').style.width = '" + pxWidth + "px'", webView);
						loadJSScript("javascript:document.getElementById('content').style.height = '" + pxHeight + "px'", webView);
						loadJSScript("javascript:document.getElementById('content').style.backgroundColor = '#56bec2'", webView);
						loadJSScript("javascript:document.getElementById('content').style.marginTop = '30px'", webView);
						updateDefaultTextProperties("content");
					}
					updateTextProperties(view, "content");
				} else if (objType.equals("TextCircle")) {
					if (objContent.equals("")) {
						int pxWidth = Globals.getPixelValue(mWebTextWidth, context);
						int pxHeight = Globals.getPixelValue(mWebTextHeight, context);
						loadJSScript("javascript:document.getElementById('content').style.width = '" + pxWidth + "px'", webView);
						loadJSScript("javascript:document.getElementById('content').style.height = '" + pxHeight + "px'", webView);
						loadJSScript("javascript:document.getElementById('content').style.backgroundColor = '#56bec2'", webView);
						loadJSScript("javascript:document.getElementById('content').style.MozBorderRadius = '50%'", webView);
						loadJSScript("javascript:document.getElementById('content').style.WebkitBorderRadius = '50%'", webView);
						loadJSScript("javascript:document.getElementById('content').style.borderRadius = '50%'", webView);
						loadJSScript("javascript:document.getElementById('content').style.behavior = 'url(PIE.htc)'", webView);
						loadJSScript("javascript:document.getElementById('content').style.textAlign = 'center'", webView);
						updateDefaultTextProperties("content");
					}
					updateTextProperties(view, "content");
				}
				if ((context.currentBook.is_bStoreBook() && getObjEnrichId() == 0 && !context.currentBook.get_bStoreID().contains("C")) || objType.equals(Globals.objType_pageNoText) || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT) || objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
					loadJSScript("javascript:document.getElementById('content').contentEditable = false;", webView);
				} else {
					loadJSScript("javascript:document.getElementById('content').contentEditable = true;", webView);
				}
				
				/*if (context.currentBook.is_bStoreBook() && objScalePageToFit.equals("yes")) {
					//webView.loadUrl("javascript:document.getElementById('viewport').setAttribute(content='width=device-width,target-densitydpi=device-dpi,initial-scale=.8,maximum-scale=.8,minimum-scale=.8,user-scalable=yes'");
					//webView.loadUrl("javascript:document.getElementById('viewport').setAttribute('content', 'width="+mWebTextWidth+"px, maximum-scale=1.0, minimum-scale=1.0')");
				}*/
			}

			@Override
			public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
				// TODO Auto-generated method stub
				if(url.contains("hyperurl://")||url.contains("hyperpage://")||url.contains("hyperfile://")) {
					final Dialog hyperlink_dialog = new Dialog(context);
					hyperlink_dialog.setTitle(context.getResources().getString(R.string.pick_image));
					hyperlink_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					hyperlink_dialog.setContentView(context.getLayoutInflater().inflate(R.layout.hyperlink_alert, null));
					hyperlink_dialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(240, context),Globals.getDeviceIndependentPixels(280, context));
					Button btn_open_Url = (Button) hyperlink_dialog.findViewById(R.id.open_url);
					btn_open_Url.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							String hyperlink_url[]=url.split("\\?");
                            if (hyperlink_url[0].contains("hyperurl://")) {

								String hyperLinkUrl;
								String url=hyperlink_url[0].toLowerCase();
								if(!url.contains("http://")) {
									hyperLinkUrl = url.replace("hyperurl://", "http://");
								}else{
									hyperLinkUrl = url.replace("hyperurl://", "");
								}
								context.new CreatingAdvsearchEnrichments(hyperLinkUrl, hyperLinkUrl).execute();
								//hyperlink_dialog.dismiss();

							} else if (hyperlink_url[0].contains("hyperpage://")) {
								String hyperLink_pageNo = hyperlink_url[0].replace("file:///android_asset/hyperpage://", "file:///");
								String hyperLink = hyperlink_url[0].replace("hyperpage://", "file:///");
								context.new CreatingAdvsearchEnrichments("BookPage", hyperLink).execute();
								//hyperlink_dialog.dismiss();
							}else if(hyperlink_url[0].contains("hyperfile://")) {
								String [] split=hyperlink_url[0].split("/");
								String hyperLink = hyperlink_url[0].replace("hyperfile://", "");
								String imgPath=split[split.length-1];
								if (imgPath.contains(".png")||imgPath.contains(".jpg")||imgPath.contains(".gif")) {
									shouldOverride = true;
									HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(((BookViewActivity) context), webView);
									hyperLinkDialog.imageDialogWindow(hyperLink,true,false);
									//return shouldOverride;
									//	hyperlink_dialog.dismiss();
								} else if (imgPath.contains(".mp4")||imgPath.contains(".mkv")||imgPath.contains(".3gp")||imgPath.contains(".avi")||imgPath.contains(".flv")) {
									shouldOverride = true;
									HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(((BookViewActivity) context), webView);
									hyperLinkDialog.videoHyperLink(hyperLink);
									//return shouldOverride;
									//hyperlink_dialog.dismiss();
								} else if (imgPath.contains(".mp3")||imgPath.contains(".m4a")) {
									shouldOverride = true;
									HyperLinkDialog hyperLinkDialog = new HyperLinkDialog(((BookViewActivity) context), webView);
									hyperLinkDialog.audioHyperLink(hyperLink);
									//return shouldOverride;

								}
							}
							hyperlink_dialog.dismiss();
						}
					});
					Button btn_remove_link = (Button) hyperlink_dialog.findViewById(R.id.btn_remove_link);
					btn_remove_link.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							String script = "javascript:removeHyperLink('" + url + "');";
							getAndUpdateWebViewContentHeight((HelpWebView) view);
							getAndSetWebViewContent();
							loadJSScript(script, (HelpWebView) view);
							hyperlink_dialog.dismiss();
							System.out.println(objContent);
						}
					});
					Button btn_cancel = (Button) hyperlink_dialog.findViewById(R.id.btn_cancel);
					btn_cancel.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							hyperlink_dialog.dismiss();
						}
					});
					hyperlink_dialog.show();
				}else if(objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)){
				 if (url != null && url.startsWith("ios:")) {
					String[] split = url.split(":");
					final String quiz_ImageName = split[1];

					final PopoverView quizPopoverView = new PopoverView(context, R.layout.list_info_view);
					//popoverView.setContentSizeForViewInPopover(new Point(350,350));
					quizPopoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.web_info_popover_width), (int) context.getResources().getDimension(R.dimen.web_info_popover_height)));
					quizPopoverView.showPopoverFromRectInViewGroup(context.mainDesignView, PopoverView.getFrameForView(btnInfo), PopoverView.PopoverArrowDirectionAny, true);

					TextView txtTitle = (TextView) quizPopoverView.findViewById(R.id.textView1);
					txtTitle.setText(R.string.image_properties);

					final ListView listView = (ListView) quizPopoverView.findViewById(R.id.info_list_view);
					listView.setAdapter(new ImageInfoListAdapter(context, imgInfoListArray, null));
						listView.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> parent,
													View view, int position, long id) {
								switch (position) {
									case 0://Search Google Images
										quizImageFileName = quiz_ImageName;
										GoogleImageSearchDialogWindow googleImgSearchView = new GoogleImageSearchDialogWindow(context, CustomWebRelativeLayout.this);
										googleImgSearchView.callDialogWindow();
										break;

									case 1://PickPhoto
										quizImageFileName = quiz_ImageName;
										takePhotoFromGallery();
										quizPopoverView.dissmissPopover(true);
										break;

									case 2://Take Photo from Camera
										quizImageFileName = quiz_ImageName;
										callCameraToTakePhoto();
										break;

									case 4://Send to Library
										quizImageFileName = quiz_ImageName;
										//String quiz_ImageName = quizImageFileName;
										Calendar cal = Calendar.getInstance();
										int second = cal.get(Calendar.SECOND);
										int minute = cal.get(Calendar.MINUTE);
										int hour = cal.get(Calendar.HOUR);
										int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
										//String quiz_imgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/images/" + "" + quiz_ImageName + ".png";

										String quiz_imgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/images/" + quizImageFileName + ".png";
										//UserFunctions.copyFiles(quiz_imgPath,);
										//String[] split=objPathContent.split("/");
										//SaveUriFile(quiz_imgPath, Globals.TARGET_BASE_FILE_PATH+"UserLibrary/"+quizImageFileName+ "_"+hourOfDay+ "_"+minute+ "_"+second+ ".png");
										//SaveUriFile(quiz_imgPath, Globals.TARGET_BASE_FILE_PATH+"UserLibrary/."+quizImageFileName+ "_"+hourOfDay+ "_"+minute+ "_"+second+ ".png");
										UserFunctions.copyFiles(quiz_imgPath, Globals.TARGET_BASE_FILE_PATH + "UserLibrary/" + quizImageFileName + "_" + hourOfDay + "_" + minute + "_" + second + ".png");
										UserFunctions.copyFiles(quiz_imgPath, Globals.TARGET_BASE_FILE_PATH + "UserLibrary/." + quizImageFileName + "_" + hourOfDay + "_" + minute + "_" + second + ".png");
										//callCameraToTakePhoto();
										break;
									case 5://Add file from File manager
										quizImageFileName = quiz_ImageName;
										addFromFileManager();
										break;
									default:
										break;
							}
						}
					});

					return true;
				} else if (url != null && url.startsWith("addquestions:")) {
					String[] arrResults = url.split(":");
					String quesId = arrResults[1];
					String[] split = quesId.split("_");

					int quesuniqueId = Integer.parseInt(split[1]);
					int quesSectionNo = Integer.parseInt(arrResults[2]);
					context.showQuizPopover(true, quesId, quesuniqueId, quesSectionNo, view, btnInfo, Language);
					getAndSetWebViewContent();
					return true;
				} else if (url != null && url.startsWith("nextquestion:")) {
					if (quizSequenceId < quizSequenceObjectArray.size() - 1) {
						quizSequenceId = quizSequenceId + 1;
					}

					return true;
				} else if (url != null && url.startsWith("previousquestion:")) {
					if (quizSequenceId > 0) {
						quizSequenceId = quizSequenceId - 1;
					}

					return true;
				} else if (url != null && url.startsWith("deletequestion:")) {
					 //UserFunctions.DisplayAlertDialog(context, R.string.delete, R.string.warning);
					 if (quizSequenceObjectArray.size() > 1) {

						 AlertDialog.Builder builder = new AlertDialog.Builder(context);
						 builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

							 @Override
							 public void onClick(DialogInterface dialog, int which) {

								 //if (quizSequenceId > 0) {
								 String[] arrResults = url.split(":");
								 String quesId = arrResults[1];
								 String[] split = quesId.split("_");
								 int quesuniqueId = Integer.parseInt(split[1]);
								 int quesSectionNo = Integer.parseInt(arrResults[2]);
								 int quesTypeNo = Integer.parseInt(arrResults[3]);

								 if (quesTypeNo == 2 || quesTypeNo == 3) {
									 String imgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/" + "images/q" + quesSectionNo + "_img_" + quesuniqueId + ".png";
									 new File(imgPath).delete();

								 } else if (quesTypeNo == 4) {
									 String imgPath1 = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/" + "images/q" + quesSectionNo + "_img_a_" + quesuniqueId + ".png";
									 String imgPath2 = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/" + "images/q" + quesSectionNo + "_img_b_" + quesuniqueId + ".png";
									 String imgPath3 = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/" + "images/q" + quesSectionNo + "_img_c_" + quesuniqueId + ".png";
									 String imgPath4 = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/" + "images/q" + quesSectionNo + "_img_d_" + quesuniqueId + ".png";
									 String imgPath5 = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/" + "images/q" + quesSectionNo + "_img_e_" + quesuniqueId + ".png";
									 String imgPath6 = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/" + "images/q" + quesSectionNo + "_img_f_" + quesuniqueId + ".png";

									 new File(imgPath1).delete();
									 new File(imgPath2).delete();
									 new File(imgPath3).delete();
									 new File(imgPath4).delete();
									 new File(imgPath5).delete();
									 new File(imgPath6).delete();
								 }

								 String sqlQuery = "delete from tblMultiQuiz where id='" + quesSectionNo + "'";
								 context.db.executeQuery(sqlQuery);
								 String javascriptString = "javascript:quizDeleteView('" + quesId + "');";
								 loadJSScript(javascriptString, view);

								 if (quizSequenceObjectArray.size() > 1) {
									 if (quizSequenceId == 0) {
										 quizSequenceObjectArray.remove(quizSequenceId);
										 quizSequenceId = 0;
									 } else {
										 quizSequenceObjectArray.remove(quizSequenceId);
										 quizSequenceId = quizSequenceId - 1;
									 }
								 }
								 getAndSetWebViewContent();
								 //}

							 }
						 });
						 builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

							 @Override
							 public void onClick(DialogInterface dialog, int which) {
								 dialog.cancel();
							 }
						 });

						 builder.setTitle(R.string.delete);
						 builder.setMessage(R.string.suredelete);
						 builder.show();
					 }
				 }


				}
				return true;
				//return super.shouldOverrideUrlLoading(view, url);
			}
		});



		webView.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				if (newProgress == 100) {
					getAndUpdateWebViewContentHeight((HelpWebView) view);
				}
			}
		});

		if (checkForStoreBookEnrichmentMalzamah()) {
			webView.setOnTouchListener(this);
		}

		this.addView(webView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		this.webTextJSInterface = new WebTextJavascriptInterface(this.context, this);
		webView.addJavascriptInterface(this.webTextJSInterface, this.webTextJSInterface.getInterfaceName());

		if (this.objType.equals("WebTable")) {
			if (this.objContent.equals("")) {
				//Load from tableIndex.html
				String htmlTableStr = createTablesWithSpecifiedWidthAndHeight(this.mWebTextWidth, this.mWebTextHeight, this.getmWebTableRow(), this.getmWebTableColumn());
				loadObjContentToWebView(htmlTableStr);
			} else {
				loadObjContentToWebView(objContent);
			}
		} else if (this.objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
//			String mindMapSource = generateMindMapHTMLContent(context, mWebTextWidth, mWebTextHeight, objContent);
//			webView.loadDataWithBaseURL(Globals.TARGET_BASE_INDEX_ASSET_PATH, mindMapSource, "text/html", "utf-8", null);
		} else {
			if (this.objContent.equals("")) {
				String fileName;
				String baseURL;
				if (this.getObjScalePageToFit().equals("yes")) {
					fileName = "enrichIndex.html";
					baseURL = "file:///"+Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID()+"/";
				} else {
					fileName = "index.html";
					baseURL = Globals.TARGET_BASE_INDEX_ASSET_PATH;
				}
				String indexPageHtmlContent = UserFunctions.readFileFromAssetPath(this.context.getAssets(), fileName);
				indexPageHtmlContent = indexPageHtmlContent.replace("</style>", UserFunctions.fontFace + "</style>");
				webView.loadDataWithBaseURL(baseURL, indexPageHtmlContent, "text/html", "utf-8", null);
			} else {
				loadObjContentToWebView(objContent);
			}
		}
	}







	public static String generateMindMapHTMLContent(Context ctx, int width, int height, String mindMapName) {
		String mindMapPath = Globals.TARGET_BASE_MINDMAP_PATH + mindMapName + ".xml";
		String fileName = "mindmapIndex.html";
		String mindMapIndexPageHtmlContent = UserFunctions.readFileFromAssetPath(ctx.getAssets(), fileName);
		String mindMapContent = UserFunctions.readFileFromPath(new File(mindMapPath));
		mindMapContent = mindMapContent.replace("\n", "");
		mindMapContent = mindMapContent.replace(" ", "");
		String[] splitMindMapContent = mindMapContent.split("\\$");
		int pxWidth = Globals.getPixelValue(width, ctx);
		int pxHeight = Globals.getPixelValue(height, ctx);
		//mindMapIndexPageHtmlContent = mindMapIndexPageHtmlContent.replace("semamindmap.min.js", "file:///"+Globals.TARGET_BASE_MINDMAP_PATH+"semamindmap.min.js");
		//mindMapIndexPageHtmlContent = mindMapIndexPageHtmlContent.replace("style.min.css", "file:///"+Globals.TARGET_BASE_MINDMAP_PATH+"style.min.css");
		String[] spiltMindMapHtmlContent = mindMapIndexPageHtmlContent.split("<div id=");
		String scriptString = String.format(Locale.US, "\n<script>\n" +
				"var mapPropertiesXML ='%s';" +
				"var mapNodePropertiesXML = '%s';" +
				"mapNodePropertiesXML = mapNodePropertiesXML.replace(/\\(|null\\)/g, \"\");" +
				"var dataProvider = [];" +
				"dataProvider[0] = mapPropertiesXML;" +
				"dataProvider[1] = mapNodePropertiesXML;" +
				"var birdsView = false;" +
				"var sMap = new SemaMindmap({id:'newDiv',width:%d,height:%d,top:0,left:0,displayBirdsView:birdsView,data:dataProvider});" +
				"sMap.createMap();" +
				"function updateMindMap(w,h,l,t){" +
				"SMM.repositionMindMap(l,t);" +
				"SMM.resizeMindMap(w,h);" +
				"}" +
				"function repositionMindMap(l,t){" +
				"SMM.repositionMindMap(l,t);" +
				"}" +
				"function resizeMindMap(l,t){" +
				"SMM.resizeMindMap(l,t);" +
				"}" +
				"</script>\n", splitMindMapContent[0], splitMindMapContent[1], pxWidth, pxHeight);
		String finalMindMapSource = spiltMindMapHtmlContent[0] + scriptString + "<div id=" + spiltMindMapHtmlContent[1];
		return finalMindMapSource;
	}

	/**
	 * This loads Object Content to Webview
	 */
	public void loadObjContentToWebView(String _objContent) {
		if (objType.equals("WebTable")) {
			String fileName = "tableIndex.html";
			String indexPageHtmlContent = UserFunctions.readFileFromAssetPath(this.context.getAssets(), fileName);
			indexPageHtmlContent = indexPageHtmlContent.replace("</style>", UserFunctions.fontFace + "</style>");
			String[] splitContent = indexPageHtmlContent.split("<body>");
			String replaceIndexPageHtmlContent = splitContent[0] + "<body>" + _objContent.trim() + splitContent[1];
			////System.out.println("HtmlTableContent:"+replaceIndexPageHtmlContent);
			webView.loadDataWithBaseURL(Globals.TARGET_BASE_TABLE_INDEX_ASSET_PATH, replaceIndexPageHtmlContent, "text/html", "utf-8", null);
		} else {
			String fileName;
			String baseURL;
			String main = null, normalize = null;

			if (this.getObjScalePageToFit().equals("yes")) {
				fileName = "enrichIndex.html";
				baseURL = "file:///" + Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/";
			} else {
				if (this.getObjScalePageToFit().contains(".css")) {
					String[] scalePage = this.getObjScalePageToFit().split("##");
					main = scalePage[8];
					normalize = scalePage[7];
					fileName = "enrichIndex.html";
					baseURL = "file:///" + Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/";
				} else {
					fileName = "index.html";
					baseURL = Globals.TARGET_BASE_INDEX_ASSET_PATH;
				}
			}
			String indexPageHtmlContent = UserFunctions.readFileFromAssetPath(this.context.getAssets(), fileName);
			if (main != null && normalize != null) {
				indexPageHtmlContent = indexPageHtmlContent.replace("FreeFiles/css/normalize.css", normalize);
				indexPageHtmlContent = indexPageHtmlContent.replace("FreeFiles/css/main.css", main);
			}
			indexPageHtmlContent = indexPageHtmlContent.replace("</style>", UserFunctions.fontFace + "</style>");
			String[] splitContent = indexPageHtmlContent.split("<body>");
			String replaceIndexPageHtmlContent = splitContent[0] + "<body>" + _objContent.trim() + "</body></html>";
			////System.out.println("HtmlContent:"+replaceIndexPageHtmlContent);
			webView.loadDataWithBaseURL(baseURL, replaceIndexPageHtmlContent, "text/html", "utf-8", null);
		}
	}

	/*public void updateScrollHeight(){
		if (this.objScalePageToFit.equals("yes")) {
			this.mWebTextHeight = webView.getContentHeight();
			RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mWebTextWidth, mWebTextHeight);
			this.setLayoutParams(layoutParams);z
		}
	}*/

	/**
	 * Loading the table from the table index HTML page:
	 * @param tableContent
	 */
	/*private void loadTableFromTableIndexHTMLWithContent(String tableContent){
		String fileName = "tableIndex.html";
		String indexPageHtmlContent = UserFunctions.readFileFromAssetPath(this.context.getAssets(), fileName);
		indexPageHtmlContent = indexPageHtmlContent.replace("</style>", UserFunctions.fontFace+"</style>");
		String[] splitContent = indexPageHtmlContent.split("<body>");
		String replaceIndexPageHtmlContent = splitContent[0]+"<body>"+tableContent+splitContent[1];
		////System.out.println("HtmlTableContent:"+replaceIndexPageHtmlContent);
		webView.loadDataWithBaseURL(Globals.TARGET_BASE_INDEX_ASSET_PATH, replaceIndexPageHtmlContent, "text/html", "utf-8", null);
	}*/

	/**
	 * create table with specified width and height
	 *
	 * @param width
	 * @param height
	 * @param rows
	 * @param columns
	 * @return
	 */
	private String createTablesWithSpecifiedWidthAndHeight(int width, int height, int rows, int columns) {

		int pxWidth = Globals.getPixelValue(width, context);
		int pxHeight = Globals.getPixelValue(height, context) - 25;
		String tableContent = "<div style=\"position:absolute;\">\n<table id=\"SBATable\" style=\"width: " + pxWidth + "px; height: " + pxHeight + "px;\">";
		for (int i = 1; i <= rows; i++) {
			String trStr = "\n<tr>";
			for (int j = 1; j <= columns; j++) {
				String rowContent = "\n<td id=\"SBATd\" contenteditable=\"true\"></td>";
				trStr = trStr.concat(rowContent);
			}
			trStr = trStr.concat("\n</tr>");
			tableContent = tableContent.concat(trStr);
		}
		String appendText = "\n</table>\n</div>";
		tableContent = tableContent.concat(appendText);
		return tableContent;
	}

	/**
	 * Update default text properties for the specific selected template
	 *
	 * @param idValue
	 */
	private void updateDefaultTextProperties(String idValue) {
		Templates template = context.currentBook.getTemplate();
		if (template != null) {
			int paraFontSize = (int) context.getResources().getDimension(R.dimen.bookview_new_para_text_size);
			changeFontFamily(template.getTemplateParaFont(), idValue);
			changeFontSize(paraFontSize, idValue);
			String[] rgbColor = template.getTemplateParaColor().split("-");
			int color = Color.rgb(Integer.parseInt(rgbColor[0]), Integer.parseInt(rgbColor[1]), Integer.parseInt(rgbColor[2]));
			String hexColor = String.format("#%06X", (0xFFFFFF & color));
			changeFontColor(hexColor, idValue);
		}
		getAndSetWebViewContent();
	}

	/**
	 * @param view
	 * @param idValue
	 * @author updateTextProperties
	 */
	private void updateTextProperties(WebView view, String idValue) {
		getAndSetWebViewFontSize(view, idValue);
		getAndSetWebViewFontFamily(view, idValue);
		getAndSetWebViewFontColor(view, idValue);
		getAndSetWebViewBackgroundColor(view, idValue);
	}

	/**
	 * Method to update the Table Text properties
	 *
	 * @param view
	 * @param idValue
	 */
	private void updateTableTextProperties(WebView view, String idValue) {
		getAndSetWebTableCellColor(view, idValue);
		getAndSetWebTableRowColor(view, idValue);
		getAndSetWebTableColumnColor(view, idValue);
		getAndSetWebTableBackgroundColor(view, idValue);
		getAndSetWebTableFontColor(view, idValue);
		getAndSetWebViewFontSize(view, idValue);
		getAndSetWebViewFontFamily(view, idValue);
	}

	/**
	 * @return
	 * @author getObjectUniqueId
	 */
	public int getObjectUniqueId() {
		return this.objUniqueId;
	}

	/**
	 * @return
	 * @author getObjectSequentialId
	 */
	public int getObjectSequentialId() {
		this.objSequentialId = context.db.getSequentialIdFromDb(this.objUniqueId);
		return this.objSequentialId;
	}

	/**
	 * this sets the object Sequentialid
	 *
	 * @param objSequentialId
	 */
	public void setObjectSequentialId(int objSequentialId) {
		this.objSequentialId = objSequentialId;
	}

	/**
	 * @return the objType
	 */
	public String getObjType() {
		return objType;
	}

	/**
	 * @return the mWebTableRow
	 */
	public int getmWebTableRow() {
		return mWebTableRow;
	}

	/**
	 * @param mWebTableRow the mWebTableRow to set
	 */
	public void setmWebTableRow(int mWebTableRow) {
		this.mWebTableRow = mWebTableRow;
	}

	/**
	 * @return the mWebTableColumn
	 */
	public int getmWebTableColumn() {
		return mWebTableColumn;
	}

	/**
	 * @param mWebTableColumn the mWebTableColumn to set
	 */
	public void setmWebTableColumn(int mWebTableColumn) {
		this.mWebTableColumn = mWebTableColumn;
	}

	/**
	 * @return the objLocked
	 */
	public boolean isObjLocked() {
		return objLocked;
	}

	/**
	 * @param objLocked the objLocked to set
	 */
	public void setObjLocked(boolean objLocked) {
		this.objLocked = objLocked;
	}

	/**
	 * @return the objScalePageToFit
	 */
	public String getObjScalePageToFit() {
		return objScalePageToFit;
	}

	/**
	 * @param objScalePageToFit the objScalePageToFit to set
	 */
	public void setObjScalePageToFit(String objScalePageToFit) {
		this.objScalePageToFit = objScalePageToFit;
	}

	/**
	 * @return the objLocation
	 */
	public String getObjLocation() {
		return objLocation;
	}

	/**
	 * @param objLocation the objLocation to set
	 */
	public void setObjLocation(String objLocation) {
		this.objLocation = objLocation;
	}

	/**
	 * @return the objSize
	 */
	public String getObjSize() {
		return objSize;
	}

	/**
	 * @param objSize the objSize to set
	 */
	public void setObjSize(String objSize) {
		this.objSize = objSize;
	}

	/**
	 * @return the objBid
	 */
	public int getObjBid() {
		return objBid;
	}

	/**
	 * @param objBid the objBid to set
	 */
	public void setObjBid(int objBid) {
		this.objBid = objBid;
	}

	/**
	 * @return the objPageNo
	 */
	public String getObjPageNo() {
		return objPageNo;
	}

	/**
	 * @param objPageNo the objPageNo to set
	 */
	public void setObjPageNo(String objPageNo) {
		this.objPageNo = objPageNo;
	}

	/**
	 * @return the objEnrichId
	 */
	public int getObjEnrichId() {
		return objEnrichId;
	}

	/**
	 * @param objEnrichId the objEnrichId to set
	 */
	public void setObjEnrichId(int objEnrichId) {
		this.objEnrichId = objEnrichId;
	}

	/**
	 * Gesture listener class for the double tap event
	 *
	 * @author Suriya
	 */
	private class GestureListener extends GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onDown(MotionEvent e) {
			return false;
		}

		// event when double tap occurs
		@Override
		public boolean onDoubleTap(MotionEvent e) {
			if (objType.equals(Globals.objType_pageNoText) || CustomWebRelativeLayout.this.objScalePageToFit.equals("yes") || objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
				if (CustomWebRelativeLayout.this.objScalePageToFit.equals("yes") && !objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
					context.pasteViews();
				}
				return true;
			}
			int toastMsg;
			Globals.setClipboardView(CustomWebRelativeLayout.this);
			if (objType.equals("TextCircle")) {
				toastMsg = R.string.circle_copied;
			} else if (objType.equals("WebTable")) {
				toastMsg = R.string.table_copied;
			} else {
				toastMsg = R.string.text_copied;

			}
			Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT).show();

			return true;
		}

		@Override
		public boolean onDoubleTapEvent(MotionEvent e) {
			return true;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
							   float velocityY) {
			return false;
		}

		@Override
		public void onLongPress(MotionEvent e) {

		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
								float distanceY) {
			return true;
		}

		@Override
		public void onShowPress(MotionEvent e) {

		}
		/*@Override
		public boolean onSingleTapUp(MotionEvent e) {
			if (CustomWebRelativeLayout.this.objScalePageToFit.equals("yes")) {
				if(context.rlTabsLayout.getVisibility()==View.VISIBLE) {
					if (context.enrichmentTabListArray.size() > 0 ) {
						context.rlTabsLayout.setVisibility(View.GONE);
					} else {
						context.rlTabsLayout.setVisibility(View.GONE);
					}
				}else{
					if (context.enrichmentTabListArray.size() > 0) {
						context.rlTabsLayout.setVisibility(View.VISIBLE);
					} else {
						context.rlTabsLayout.setVisibility(View.GONE);
					}

				}
				if(context.bkmarkLayout.getVisibility()==View.VISIBLE) {
					if (context.enrichmentbkmrkList.size() > 0) {
						context.bkmarkLayout.setVisibility(View.GONE);
					}else{
						context.bkmarkLayout.setVisibility(View.GONE);
					}
				}else{
					if (context.enrichmentbkmrkList.size() > 0) {
						context.bkmarkLayout.setVisibility(View.VISIBLE);
					} else {
						context.bkmarkLayout.setVisibility(View.GONE);
					}

				}

			}
			//System.out.println("Hello");
			return true;
		}*/

	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		final int action = event.getAction();
		int positionX = (int) event.getRawX();
		int positionY = (int) event.getRawY();

		switch (action & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN: {
				if (getObjScalePageToFit().equals("no")) {
					context.designScrollPageView.setEnableScrolling(false);
				}
				prevX = positionX;
				prevY = positionY;

				mWebTextPrevXpos = mWebTextXpos;
				mWebTextPrevYpos = mWebTextYpos;

				context.page.setCurrentView(this);
				context.page.makeSelectedArea(this);
				break;
			}

			case MotionEvent.ACTION_MOVE: {
				if (!isObjLocked() && getObjScalePageToFit().equals("no")) {
					int deltaX = positionX - prevX;
					int deltaY = positionY - prevY;

					float viewCenterX = (this.getX() + deltaX) + (this.getWidth() / 2);
					float viewCenterY = (this.getY() + deltaY) + (this.getHeight() / 2);
					PointF viewCenterPoint = new PointF(viewCenterX, viewCenterY);

					//Snap To grid
					Object[] gridArr = context.page.checkForSnapPointsForGrid(viewCenterPoint);
					viewCenterPoint = (PointF) gridArr[0];
					boolean gridPointsSnapped = (Boolean) gridArr[1];
					Object[] objArr = context.page.checkForSnapBetweenObjects(this, viewCenterPoint);
					viewCenterPoint = (PointF) objArr[0];
					boolean objectPointsSnapped = (Boolean) objArr[2];

					float viewXpos = viewCenterPoint.x - (this.getWidth() / 2);
					float viewYpos = viewCenterPoint.y - (this.getHeight() / 2);

					//Restrict the objects within the boundry
					if (viewXpos <= 0) {
						viewXpos = 0;
					}
					if (viewYpos <= 0) {
						viewYpos = 0;
					}
					if (viewXpos + getWidth() >= context.designPageLayout.getWidth()) {
						viewXpos = context.designPageLayout.getWidth() - getWidth();
					}
					if (viewYpos + getHeight() >= context.designPageLayout.getHeight()) {
						//viewYpos = context.designPageLayout.getHeight() - getHeight();
					}

					setXAndYPos(viewXpos, viewYpos);

					prevX = positionX;
					prevY = positionY;

					if (objectPointsSnapped) {
						context.page.snapToObjects(this, (View) objArr[1]);
					} else {
						context.page.removeObjectGridLines();
					}

					if (gridPointsSnapped) {
						context.page.snapToGrid(this);
					} else {
						context.page.removeGridLines();
					}

					context.page.extendScrollViewHeight(this, viewCenterPoint);

					context.page.reassignHandlerArea(this);
				}
				break;
			}

			case MotionEvent.ACTION_UP: {
				if (getObjScalePageToFit().equals("no")) {
					context.designScrollPageView.setEnableScrolling(true);
				}
				context.page.removeObjectGridLines();
				context.page.removeGridLines();

				if (mWebTextPrevXpos != mWebTextXpos || mWebTextPrevYpos != mWebTextYpos) {
					UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
					uRPrevObjData.setuRObjDataUniqueId(objUniqueId);
					uRPrevObjData.setuRObjDataXPos(mWebTextPrevXpos);
					uRPrevObjData.setuRObjDataYPos(mWebTextPrevYpos);
					context.page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_MOVED, Globals.OBJECT_PREVIOUS_STATE);

					UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
					uRCurrObjData.setuRObjDataUniqueId(objUniqueId);
					uRCurrObjData.setuRObjDataXPos(mWebTextXpos);
					uRCurrObjData.setuRObjDataYPos(mWebTextYpos);
					context.page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_MOVED, Globals.OBJECT_PRESENT_STATE);
				}

				break;
			}

			case MotionEvent.ACTION_CANCEL: {
				break;
			}

			case MotionEvent.ACTION_POINTER_UP: {
				break;
			}
		}

		//return false;
		return gestureDetector.onTouchEvent(event);
	}

	/**
	 * Sets X and Y positon for the views
	 *
	 * @param viewXpos
	 * @param viewYpos
	 */
	public void setXAndYPos(float viewXpos, float viewYpos) {
		this.setX(viewXpos);
		this.setY(viewYpos);
		mWebTextXpos = (int) viewXpos;
		mWebTextYpos = (int) viewYpos;
	}

	/**
	 * This updates the transformation values for the view
	 */
	public void setTransformationValues(float xPos, float yPos, LayoutParams layoutParams) {
		mWebTextXpos = (int) xPos;
		mWebTextYpos = (int) yPos;
		mWebTextWidth = layoutParams.width;
		mWebTextHeight = layoutParams.height;
	}

	/**
	 * This sets the transformation for the views
	 */
	public void setTransformation(float xPos, float yPos, LayoutParams layoutParams) {
		this.setX(xPos);
		this.setY(yPos);
		this.setLayoutParams(layoutParams);
		resizeDivWidthAndHeight(layoutParams.width, layoutParams.height);
		mWebTextXpos = (int) xPos;
		mWebTextYpos = (int) yPos;
		getAndSetWebViewContent();
		//mWebTextWidth = layoutParams.width;
		//mWebTextHeight = layoutParams.height;
	}

	/**
	 * This sets the transformation for the webTable views
	 */
	public void setWebTableTransformation(int noOfRows, int noOfColumns, LayoutParams layoutParams) {
		mWebTablePrevRow = noOfRows;
		mWebTableColumn = noOfColumns;
		mWebTextWidth = layoutParams.width;
		mWebTextHeight = layoutParams.height;
		this.setLayoutParams(layoutParams);
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btnInfo:
				context.page.setCurrentView(this);
				context.page.makeSelectedArea(this);
				if (context.currentBook.is_bStoreBook() && !context.currentBook.isStoreBookCreatedFromTab()) {
					context.designScrollPageView.setEnableScrolling(false);
				}
				context.hideToolBarLayout();
				showinfoPopOverView(v);
				break;

			case R.id.btn_delete_object:
				context.page.setCurrentView(this);
				context.page.makeSelectedArea(this);
				context.deleteSelectedObject();
				break;

			default:
				break;
		}
	}

	//TextSelectionSupport text=TextSelectionSupport.support(context, webView);

	/**
	 * @param v
	 * @author showinfoPopOverView
	 */
	public void showinfoPopOverView(View v) {
		prevObjSequentialId = getObjectSequentialId();
		popoverView = new PopoverView(context, R.layout.list_info_view);
		popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.info_popover_width), (int) context.getResources().getDimension(R.dimen.web_info_height)));
		popoverView.setDelegate(this);
		popoverView.showPopoverFromRectInViewGroup(context.mainDesignView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionAny, true);

		if (this.objType.equals("WebTable")) {
			prevObjContent = objContent;
			prevObjLocked = objLocked;
			mWebTablePrevRow = mWebTableRow;
			mWebTablePrevColumn = mWebTableColumn;
			mWebTextPrevWidth = mWebTextWidth;
			mWebTextPrevHeight = mWebTextHeight;
			loadJSScript("javascript:rowPosition();", webView);
			final ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
			listView.setAdapter(new WebTableListAdapter(context, webTableInfoListArray, this));
			TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
			txtTitle.setText(R.string.table_properties);
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
										int position, long arg3) {
					if (position == 0) { //Cell color
						int color = 0;
						if (tableCellColor != null && !tableCellColor.equals("transparent")) {
							color = Color.parseColor(tableCellColor);
						}
						final ArrayList<String> recentWebTableBackgroundColorList = context.page.getRecentColorsFromSharedPreference(Globals.webTablebackgroundColorKey);
						new PopupColorPicker(context, color, null, recentWebTableBackgroundColorList, true, new ColorPickerListener() {
							String hexColor = null;

							@Override
							public void pickedColor(int color) {
								hexColor = String.format("#%06X", (0xFFFFFF & color));
								changeCellBackgroundColor(hexColor);
								listView.invalidateViews();
							}

							@Override
							public void dismissPopWindow() {
								if (hexColor != null) {
									context.page.setandUpdateRecentColorsToSharedPreferences(Globals.webTablebackgroundColorKey, recentWebTableBackgroundColorList, hexColor);
								}
							}

							@Override
							public void pickedTransparentColor(
									String transparentColor) {
								changeCellBackgroundColor(transparentColor);
							}

							@Override
							public void pickColorFromBg() {
								context.pickColorFromBgView = true;
								context.pickedColorFor = "BackgroundColor";
							}
						});
					} else if (position == 1) { //Row color
						int color = 0;
						if (tableRowColor != null && !tableRowColor.equals("transparent")) {
							color = Color.parseColor(tableRowColor);
						}
						final ArrayList<String> recentWebTableBackgroundColorList = context.page.getRecentColorsFromSharedPreference(Globals.webTablebackgroundColorKey);
						new PopupColorPicker(context, color, null, recentWebTableBackgroundColorList, true, new ColorPickerListener() {
							String hexColor = null;

							@Override
							public void pickedColor(int color) {
								hexColor = String.format("#%06X", (0xFFFFFF & color));
								changeRowBackgroundColor(hexColor);
								listView.invalidateViews();
							}

							@Override
							public void dismissPopWindow() {
								if (hexColor != null) {
									context.page.setandUpdateRecentColorsToSharedPreferences(Globals.webTablebackgroundColorKey, recentWebTableBackgroundColorList, hexColor);
								}
							}

							@Override
							public void pickedTransparentColor(
									String transparentColor) {
								changeRowBackgroundColor(transparentColor);
							}

							@Override
							public void pickColorFromBg() {
								context.pickColorFromBgView = true;
								context.pickedColorFor = "BackgroundColor";
							}
						});
					} else if (position == 2) { //Column color
						int color = 0;
						if (tableColumnColor != null && !tableColumnColor.equals("transparent")) {
							color = Color.parseColor(tableColumnColor);
						}
						final ArrayList<String> recentWebTableBackgroundColorList = context.page.getRecentColorsFromSharedPreference(Globals.webTablebackgroundColorKey);
						new PopupColorPicker(context, color, null, recentWebTableBackgroundColorList, true, new ColorPickerListener() {
							String hexColor = null;

							@Override
							public void pickedColor(int color) {
								hexColor = String.format("#%06X", (0xFFFFFF & color));
								changeColumnBackgroundColor(hexColor);
								listView.invalidateViews();
							}

							@Override
							public void dismissPopWindow() {
								if (hexColor != null) {
									context.page.setandUpdateRecentColorsToSharedPreferences(Globals.webTablebackgroundColorKey, recentWebTableBackgroundColorList, hexColor);
								}
							}

							@Override
							public void pickedTransparentColor(
									String transparentColor) {
								changeColumnBackgroundColor(transparentColor);
							}

							@Override
							public void pickColorFromBg() {
								context.pickColorFromBgView = true;
								context.pickedColorFor = "BackgroundColor";
							}
						});
					} else if (position == 3) { //Background color
						int color = 0;
						if (tableBackgroundColor != null && !tableBackgroundColor.equals("transparent")) {
							color = Color.parseColor(tableBackgroundColor);
						}
						final ArrayList<String> recentWebTableBackgroundColorList = context.page.getRecentColorsFromSharedPreference(Globals.webTablebackgroundColorKey);
						new PopupColorPicker(context, color, null, recentWebTableBackgroundColorList, true, new ColorPickerListener() {
							String hexColor = null;

							@Override
							public void pickedColor(int color) {
								hexColor = String.format("#%06X", (0xFFFFFF & color));
								changeTableBackgroundColor(hexColor);
								listView.invalidateViews();
							}

							@Override
							public void dismissPopWindow() {
								if (hexColor != null) {
									context.page.setandUpdateRecentColorsToSharedPreferences(Globals.webTablebackgroundColorKey, recentWebTableBackgroundColorList, hexColor);
								}
							}

							@Override
							public void pickedTransparentColor(
									String transparentColor) {
								changeTableBackgroundColor(transparentColor);
							}

							@Override
							public void pickColorFromBg() {
								context.pickColorFromBgView = true;
								context.pickedColorFor = "BackgroundColor";
							}
						});

					} else if (position == 4) { //Font color
						int color = 0;
						if (fontColor != null && !fontColor.equals("transparent")) {
							color = Color.parseColor(fontColor);
						}
						final ArrayList<String> recentFontColorList = context.page.getRecentColorsFromSharedPreference(Globals.webTablefontColorKey);
						new PopupColorPicker(context, color, null, recentFontColorList, true, new ColorPickerListener() {
							String hexColor = null;
							;

							@Override
							public void pickedColor(int color) {
								hexColor = String.format("#%06X", (0xFFFFFF & color));
								changeFontColor(hexColor, "SBATable");
								listView.invalidateViews();
							}

							@Override
							public void dismissPopWindow() {
								if (hexColor != null) {
									context.page.setandUpdateRecentColorsToSharedPreferences(Globals.webTablefontColorKey, recentFontColorList, hexColor);
								}
							}

							@Override
							public void pickedTransparentColor(
									String transparentColor) {
								changeFontColor(transparentColor, "SBATable");
							}

							@Override
							public void pickColorFromBg() {
								context.pickColorFromBgView = true;
								context.pickedColorFor = "FontColor";
							}
						});
						//popoverView.dissmissPopover(true);

					} else if (position == 5) { //Font size


					} else if (position == 6) { //Font
						final Dialog fontListDialog = new Dialog(context);
						fontListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
						fontListDialog.setTitle("Choose Font");
						fontListDialog.setContentView(context.getLayoutInflater().inflate(R.layout.font_list_view, null));
						fontListDialog.getWindow().setLayout((int) (context.global.getDeviceWidth() / 1.8), (int) (context.global.getDeviceHeight() / 1.8));
						ListView lv = (ListView) fontListDialog.findViewById(R.id.listView1);
						lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
						lv.setAdapter(new FontListAdapter(context, CustomWebRelativeLayout.this));
						lv.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> adapterView, View view,
													int position, long arg3) {
								if (firstTimeStartUp) {
									currentSelectedListView = adapterView.getChildAt(2);
								}
								firstTimeStartUp = false;
								if (currentSelectedListView != null && currentSelectedListView != view) {
									UnHighlightCurrentSlectedView(currentSelectedListView);
								}
								currentSelectedListView = view;
								highlightCurrentSlectedView(currentSelectedListView);

								//changeFontFamily(UserFunctions.m_fontNames.get(position), "SBATable");
								changeTableFont(UserFunctions.m_fontNames.get(position));
							}

						});
						fontListDialog.show();
						//popoverView.dissmissPopover(true);
					} else if (position == 11) { //Lock
						if (isObjLocked()) {
							setObjLocked(false);
							webTableInfoListArray[9] = R.string.lock;
							listView.invalidateViews();
						} else {
							setObjLocked(true);
							webTableInfoListArray[9] = R.string.unlock;
							listView.invalidateViews();
						}
					} else if (position == 13) { //Bring to Front
						context.page.bringViewToFront(CustomWebRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
					} else if (position == 14) { //Send to Back
						context.page.sendViewToBack(CustomWebRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
					}
				}
			});
		} else if (this.objType.equals("WebText") || objType.equals("TextSquare") || objType.equals("TextRectangle") || objType.equals("TextCircle") || objType.equals(Globals.OBJTYPE_TITLETEXT) || objType.equals(Globals.OBJTYPE_AUTHORTEXT)) {
			prevObjContent = objContent;
			prevObjLocked = objLocked;
			final ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
			listView.setAdapter(new WebTextListAdapter(context, webTextInfoListArray, this));
			TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
			txtTitle.setText(R.string.text_properties);
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view, int position,
										long arg3) {
					if (position == 1) {
						getAndSetSelectionIsActive();
						showFontListDialog();
						//popoverView.dissmissPopover(true);
					} else if (position == 2) {
						getAndSetSelectionIsActive();
						showPopUpColorPickerDialog(listView);
						listView.invalidateViews();
						//popoverView.dissmissPopover(true);
					} else if (position == 4) {
						int color = 0;
						if (backgroundColor != null && !backgroundColor.equals("transparent")) {
							color = Color.parseColor(backgroundColor);
						}
						final ArrayList<String> recentBackgroundColorList = context.page.getRecentColorsFromSharedPreference(Globals.backgroundColorKey);
						new PopupColorPicker(context, color, null, recentBackgroundColorList, true, new ColorPickerListener() {
							String hexColor = null;

							@Override
							public void pickedColor(int color) {
								hexColor = String.format("#%06X", (0xFFFFFF & color));
								changeBackgroundColor(hexColor, "content");
								listView.invalidateViews();
							}

							@Override
							public void dismissPopWindow() {
								if (hexColor != null) {
									context.page.setandUpdateRecentColorsToSharedPreferences(Globals.backgroundColorKey, recentBackgroundColorList, hexColor);
								}
							}

							@Override
							public void pickedTransparentColor(
									String transparentColor) {
								changeBackgroundColor(transparentColor, "content");
							}

							@Override
							public void pickColorFromBg() {
								context.pickColorFromBgView = true;
								context.pickedColorFor = "BackgroundColor";
							}
						});
						//popoverView.dissmissPopover(true);
					} else if (position == 6) { //Lock
						if (isObjLocked()) {
							setObjLocked(false);
							webTextInfoListArray[6] = R.string.lock;
							listView.invalidateViews();
						} else {
							setObjLocked(true);
							webTextInfoListArray[6] = R.string.unlock;
							listView.invalidateViews();
						}
					} else if (position == 8) { //Bring to Front
						context.page.bringViewToFront(CustomWebRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
					} else if (position == 9) { //Send to Back
						context.page.sendViewToBack(CustomWebRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
					} else if (position == 11) {
						final Dialog textAlignListDialog = new Dialog(context);
						textAlignListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
						textAlignListDialog.setTitle(R.string.text_align);
						textAlignListDialog.setContentView(context.getLayoutInflater().inflate(R.layout.font_list_view, null));
						textAlignListDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(350, context), Globals.getDeviceIndependentPixels(300, context));
						ListView lv = (ListView) textAlignListDialog.findViewById(R.id.listView1);
						lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
						lv.setAdapter(new TextAlignListAdapter(context, Globals.textAlign));
						lv.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> adapterView, View view,
													int position, long arg3) {
								if (firstTimeStartUp) {
									currentSelectedListView = adapterView.getChildAt(2);
								}
								firstTimeStartUp = false;
								if (currentSelectedListView != null && currentSelectedListView != view) {
									UnHighlightCurrentSlectedView(currentSelectedListView);
								}
								currentSelectedListView = view;
								highlightCurrentSlectedView(currentSelectedListView);

								String txtAlignString = context.getResources().getString(Globals.textAlign[position]);
								textAlign(txtAlignString, "content");
							}
						});
						textAlignListDialog.show();
						//popoverView.dissmissPopover(true);
					}
				}
			});
		} else if (this.objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
			prevObjContent = objContent;
			prevObjLocked = objLocked;

			final ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
			listView.setAdapter(new WebTextListAdapter(context, quizTextInfoListArray, this));
			TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
			txtTitle.setText(R.string.text_properties);
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view, int position,
										long arg3) {
					if (position == 2) {     //2.font //3.nil
						getAndSetSelectionIsActive();
						showFontListDialog();
						//popoverView.dissmissPopover(true);
					} else if (position == 3) { //4.colour //5.nil
						getAndSetSelectionIsActive();
						showPopUpColorPickerDialog(listView);
						listView.invalidateViews();
						//popoverView.dissmissPopover(true);
					} else if (position == 5) {    // 6.background_color  //7.nil
						int color = 0;
						if (backgroundColor != null && !backgroundColor.equals("transparent")) {
							color = Color.parseColor(backgroundColor);
						}
						final ArrayList<String> recentBackgroundColorList = context.page.getRecentColorsFromSharedPreference(Globals.backgroundColorKey);
						new PopupColorPicker(context, color, null, recentBackgroundColorList, true, new ColorPickerListener() {
							String hexColor = null;

							@Override
							public void pickedColor(int color) {
								hexColor = String.format("#%06X", (0xFFFFFF & color));
								changeBackgroundColor(hexColor, "content");
								listView.invalidateViews();
							}

							@Override
							public void dismissPopWindow() {
								if (hexColor != null) {
									context.page.setandUpdateRecentColorsToSharedPreferences(Globals.backgroundColorKey, recentBackgroundColorList, hexColor);
								}
							}

							@Override
							public void pickedTransparentColor(
									String transparentColor) {
								changeBackgroundColor(transparentColor, "content");
							}

							@Override
							public void pickColorFromBg() {
								context.pickColorFromBgView = true;
								context.pickedColorFor = "BackgroundColor";
							}
						});
						//popoverView.dissmissPopover(true);
					} else if (position == 7) { //8.Lock
						if (isObjLocked()) {
							setObjLocked(false);
							quizTextInfoListArray[7] = R.string.lock;
							listView.invalidateViews();
						} else {
							setObjLocked(true);
							quizTextInfoListArray[7] = R.string.unlock;
							listView.invalidateViews();
						}
					} else if (position == 9) { //9.Bring to Front //10.nil
						context.page.bringViewToFront(CustomWebRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
					} else if (position == 10) { //11.Send to Back //12.nill
						context.page.sendViewToBack(CustomWebRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
					} else if (position == 12) {   //13.select options //14.nill

						final Dialog textAlignListDialog = new Dialog(context);
						textAlignListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
						textAlignListDialog.setContentView(context.getLayoutInflater().inflate(R.layout.options_listview, null));
						textAlignListDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(350, context), Globals.getDeviceIndependentPixels(350, context));

						ListView lv = (ListView) textAlignListDialog.findViewById(R.id.listView1);
						textAlignListDialog.setTitle(R.string.select_options);
						lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
						lv.setAdapter(new OptionsListAdapter(context, Globals.options));
						lv.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> adapterView, View view,
													int position, long arg3) {
								int noOfOptions = position + 2;

								QuizSequenceObject quizSeqObject = quizSequenceObjectArray.get(quizSequenceId);
								String radioBtnGroupName = "q" + quizSeqObject.getSectionId() + "_" + quizSeqObject.getUniqueid() + "";
								String stringByEvaluatingJavaScriptFromString = "javascript:showOrHideOptions(\"" + noOfOptions + "\", \"" + quizSeqObject.getUniqueid() + "\", \"" + radioBtnGroupName + "\", \"" + quizSeqObject.getSectionId() + "\")";
								loadJSScript(stringByEvaluatingJavaScriptFromString, webView);

								textAlignListDialog.dismiss();
								popoverView.dissmissPopover(true);
							}
						});
						textAlignListDialog.show();
					} else if (position == 14) {       //15.textAlign
						final Dialog textAlignListDialog = new Dialog(context);
						textAlignListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
						textAlignListDialog.setTitle(R.string.text_align);
						textAlignListDialog.setContentView(context.getLayoutInflater().inflate(R.layout.font_list_view, null));
						textAlignListDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(350, context), Globals.getDeviceIndependentPixels(300, context));
						ListView lv = (ListView) textAlignListDialog.findViewById(R.id.listView1);
						lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
						lv.setAdapter(new TextAlignListAdapter(context, Globals.textAlign));
						lv.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> adapterView, View view,
													int position, long arg3) {
								if (firstTimeStartUp) {
									currentSelectedListView = adapterView.getChildAt(2);
								}
								firstTimeStartUp = false;
								if (currentSelectedListView != null && currentSelectedListView != view) {
									UnHighlightCurrentSlectedView(currentSelectedListView);
								}
								currentSelectedListView = view;
								highlightCurrentSlectedView(currentSelectedListView);

								String txtAlignString = context.getResources().getString(Globals.textAlign[position]);
								textAlign(txtAlignString, "content");
							}
						});
						textAlignListDialog.show();
						//popoverView.dissmissPopover(true);
					}
				}
			});


		} else if (this.objType.equals(Globals.objType_pageNoText)) {
			int pno = Integer.parseInt(getObjPageNo()) - 1;
			String[] split = objContent.split(pno + "</div>");
			objContent = split[0];
			prevObjContent = objContent;
			final ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
			listView.setAdapter(new WebPageNoTextListAdapter(context, pageTextInfoListArray, this, popoverView));
			TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
			txtTitle.setText(R.string.text_properties);
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
										int position, long arg3) {
					if (position == 1) {
						getAndSetSelectionIsActive();
						showFontListDialog();
						//popoverView.dissmissPopover(true);
					} else if (position == 2) {
						getAndSetSelectionIsActive();
						showPopUpColorPickerDialog(listView);
						listView.invalidateViews();
						//popoverView.dissmissPopover(true);
					} else if (position == 6) {
						int color = 0;
						if (backgroundColor != null && !backgroundColor.equals("transparent")) {
							color = Color.parseColor(backgroundColor);
						}
						final ArrayList<String> recentBackgroundColorList = context.page.getRecentColorsFromSharedPreference(Globals.backgroundColorKey);
						new PopupColorPicker(context, color, null, recentBackgroundColorList, true, new ColorPickerListener() {
							String hexColor = null;

							@Override
							public void pickedColor(int color) {
								hexColor = String.format("#%06X", (0xFFFFFF & color));
								changeBackgroundColor(hexColor, "content");
								listView.invalidateViews();
							}

							@Override
							public void dismissPopWindow() {
								if (hexColor != null) {
									context.page.setandUpdateRecentColorsToSharedPreferences(Globals.backgroundColorKey, recentBackgroundColorList, hexColor);
								}
							}

							@Override
							public void pickedTransparentColor(
									String transparentColor) {
								changeBackgroundColor(transparentColor, "content");
							}

							@Override
							public void pickColorFromBg() {
								context.pickColorFromBgView = true;
								context.pickedColorFor = "BackgroundColor";
							}
						});
						//popoverView.dissmissPopover(true);
					} else if (position == 8) { //Bring to Front
						context.page.bringViewToFront(CustomWebRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
					} else if (position == 9) { //Send to Back
						context.page.sendViewToBack(CustomWebRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
					}
				}
			});
		} else if (this.objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
//			ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
//			listView.setVisibility(View.GONE);
//			TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
//			txtTitle.setVisibility(View.GONE);
//
//			GridView gridView = (GridView) popoverView.findViewById(R.id.gridView);
//			gridView.setVisibility(View.VISIBLE);
//			gridView.setAdapter(new MindmapGridadapter());
//			gridView.setOnItemClickListener(new OnItemClickListener() {
//				@Override
//				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//					String mindMapFileName = mindMapFileNames.get(position);
//					objContent = mindMapFileName;
//					String mindMapSource = generateMindMapHTMLContent(context, mWebTextWidth, mWebTextHeight, objContent);
//					webView.loadDataWithBaseURL(Globals.TARGET_BASE_INDEX_ASSET_PATH, mindMapSource, "text/html", "utf-8", null);
//					popoverView.dissmissPopover(false);
//				}
//			});
		}
	}


	public class MindmapGridadapter extends BaseAdapter {

		public MindmapGridadapter() {
			File mindMapDir = new File(Globals.TARGET_BASE_MINDMAP_PATH);
			final String[] mindMapFiles = mindMapDir.list();
			if (mindMapFiles != null) {
				getMindMapFiles(mindMapFiles);
			}
		}

		private void getMindMapFiles(String[] mindMapFiles) {
			mindMapFileNames = new ArrayList<String>();
			for (int i = 0; i < mindMapFiles.length; i++) {
				if (mindMapFiles[i].contains(".xml")) {
					mindMapFileNames.add(mindMapFiles[i].replace(".xml", ""));
				}
			}
		}

		@Override
		public int getCount() {
			return mindMapFileNames.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				view = ((Activity) context).getLayoutInflater().inflate(R.layout.mindmap_grid_inflate, null);
			}
			String mindMapFileName = mindMapFileNames.get(position);
			String imgFilePath = Globals.TARGET_BASE_MINDMAP_PATH + mindMapFileName + ".png";
			ImageView imgView = (ImageView) view.findViewById(R.id.imageView);
			Bitmap bitmap = BitmapFactory.decodeFile(imgFilePath);
			imgView.setImageBitmap(bitmap);

			Button btnEdit = (Button) view.findViewById(R.id.btnEdit);
			btnEdit.setVisibility(View.GONE);
			Button btnDelete = (Button) view.findViewById(R.id.btnDelete);
			btnDelete.setVisibility(View.GONE);

			TextView txtTitle = (TextView) view.findViewById(R.id.textView5);
			txtTitle.setText(mindMapFileName);

			return view;
		}
	}

	/**
	 * @param view
	 * @author HighlightView
	 */
	public void highlightCurrentSlectedView(View view) {
		//view.setBackgroundColor(Color.DKGRAY);
		CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
		checkBox.setChecked(true);
	}

	/**
	 * @param view
	 * @author UnhighlightView
	 */
	public void UnHighlightCurrentSlectedView(View view) {
		//view.setBackgroundColor(Color.TRANSPARENT);
		CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
		checkBox.setChecked(false);
	}

	/**
	 * @param height
	 * @param width
	 * @author ResizeDivWidthAndHeight
	 */
	public void resizeDivWidthAndHeight(int width, int height) {
		this.mWebTextWidth = width;
		this.mWebTextHeight = height;
		int pxWidth = Globals.getPixelValue(mWebTextWidth, context);
		int pxHeight = Globals.getPixelValue(mWebTextHeight, context);
		if (this.objType.equals("WebTable")) {
			pxHeight = pxHeight - 25;
			loadJSScript("javascript:document.getElementById('SBATable').style.width = '" + pxWidth + "px'", webView);
			loadJSScript("javascript:document.getElementById('SBATable').style.height = '" + pxHeight + "px'", webView);
		} else {
			loadJSScript("javascript:document.getElementById('content').style.width = '" + pxWidth + "px'", webView);
			loadJSScript("javascript:document.getElementById('content').style.height = '" + pxHeight + "px'", webView);
			if (this.objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
				int scrollLeft = pxWidth * quizSequenceId;
				String script = "javascript:document.getElementById('content').scrollLeft = '" + scrollLeft + "'";
				loadJSScript(script, webView);
			}
		}
	}

	/**
	 * Resize Mindmap div width and height
	 *
	 * @param width
	 * @param height
	 */
	public void resizeMindmapDivWidthAndHeight(int width, int height) {
		this.mWebTextWidth = width;
		this.mWebTextHeight = height;
		int pxWidth = Globals.getPixelValue(mWebTextWidth, context);
		int pxHeight = Globals.getPixelValue(mWebTextHeight, context);
		if (this.objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
			String scriptString = String.format(Locale.US, "javascript:resizeMindMap(%d,%d);", pxWidth, pxHeight);
			loadJSScript(scriptString, webView);
		}
	}

	/**
	 * @param size
	 * @author ChangeFontSize
	 */
	public void changeFontSize(int size, String idValue) {
		if (textSelected) {
			loadJSScript("javascript:surroundSelectionFontSize('" + size + "px');", webView);
		} else {
			loadJSScript("javascript:document.getElementById('" + idValue + "').style.fontSize = '" + size + "px'", webView);
			this.fontSize = size;
		}
	}

	public void getAndUpdateWebViewContentHeight(HelpWebView view) {
		if (getObjScalePageToFit().equals("yes")) {

			//loadJSScript("javascript:getContentDocumentHeight();", webView);
			int height = view.getContentHeightt();
			if (height == 0) {
				loadJSScript("javascript:getContentDocumentHeight();", webView);
			} else {
                if(mWebTextHeight==0) {
					mWebTextHeight = height;
				}
				final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mWebTextWidth, mWebTextHeight);
				setLayoutParams(layoutParams);
			}
		}
	}

	public void wtjisetDocumentHeight(final String documentContentHeight) {
		int height = Integer.parseInt(documentContentHeight);
		height = Globals.getDeviceIndependentPixels(height, context);
		if(mWebTextHeight==0) {
			mWebTextHeight = height;
		}
		context.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mWebTextWidth, mWebTextHeight);
				setLayoutParams(layoutParams);
			}
		});
	}

	private String javascriptReturnValue(final String script, final WebView view) {
		//int index = evalJsIndex.incrementAndGet();
		int index = 0;
		view.post(new Runnable() {

			@Override
			public void run() {
				view.loadUrl(script);
			}
		});
		return waitForJsReturnValue(index, 1000);
	}

	private String waitForJsReturnValue(int index, int waitMs) {

		long start = System.currentTimeMillis();

		while (true) {
			long elapsed = System.currentTimeMillis() - start;
			if (elapsed > waitMs)
				break;
			synchronized (jsReturnValueLock) {
				String value = jsReturnValues.remove(index);
				if (value != null)
					return value;

				long toWait = waitMs - (System.currentTimeMillis() - start);
				if (toWait > 0)
					try {
						jsReturnValueLock.wait(toWait);
					} catch (InterruptedException e) {
						break;
					}
				else
					break;
			}
		}
		//Log.e("MyActivity", "Giving up; waited " + (waitMs/1000) + "sec for return value " + index);
		return "";
	}

	private void getAndReturnWebViewContent() {
		String script = "javascript:getAndReturnWebContent('" + version + "');";
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			webView.evaluateJavascript(script, new ValueCallback<String>() {
				@Override
				public void onReceiveValue(String value) {
					value = UserFunctions.getValuFromJsonreader(value);
					updateObjContent(value);
				}
			});
		} else {
			String text = javascriptReturnValue(script, webView);
			updateObjContent(text);
		}
	}

	private void updateObjContent(String text) {
		if (context.currentBook.is_bStoreBook() && !context.currentBook.isStoreBookCreatedFromTab()) {
			context.designScrollPageView.setEnableScrolling(true);
		}
		if (text == "") {
			return;
		}
		if (objType.equals(Globals.objType_pageNoText)) {
			int pno = Integer.parseInt(getObjPageNo()) - 1;
			String[] split = text.split(pno + "</div>");
			this.objContent = split[0];
		} else if (objType.equals(Globals.OBJTYPE_TITLETEXT)) {
			String[] split = text.split(">");
			String title = split[1].replace("</div", "").replace("&nbsp;", "");
			context.currentBook.setBookTitle(title);
			this.objContent = text;
		} else if (objType.equals(Globals.OBJTYPE_AUTHORTEXT)) {
			String[] split = text.split(">");
			String author = split[1].replace("</div", "").replace("&nbsp;", "");
			context.currentBook.setBookAuthorName(author);
			this.objContent = text;
		} else {
			this.objContent = text;
		}
		if (objType.equals(Globals.objType_pageNoText)) {
			//while (!objContentUpdated) {
			//System.out.println("NotUpdated");
			//}
			final String currObjContent = getObjectContent().trim();
			final String preObjContent = prevObjContent.trim();
			if (!currObjContent.contentEquals(preObjContent)) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setMessage(R.string.page_number_position);
				builder.setPositiveButton(R.string.apply, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						UndoRedoObjectData prevURObjData = new UndoRedoObjectData();
						prevURObjData.setuRObjDataUniqueId(getObjectUniqueId());
						prevURObjData.setuRObjDataType(objType);
						prevURObjData.setuRPageNoObjDataStatus("apply");
						prevURObjData.setuRObjDataContent(preObjContent);

						UndoRedoObjectData currURObjData = new UndoRedoObjectData();
						currURObjData.setuRObjDataUniqueId(getObjectUniqueId());
						currURObjData.setuRObjDataType(objType);
						currURObjData.setuRPageNoObjDataStatus("apply");
						currURObjData.setuRObjDataContent(currObjContent);

						context.page.createAndRegisterUndoRedoObjectData(prevURObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PREVIOUS_STATE);
						context.page.createAndRegisterUndoRedoObjectData(currURObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PRESENT_STATE);
						prevObjContent = objContent;

						pageNoContentUpdateForCurrentPage(objContent);
						dialog.cancel();
					}
				});

				builder.setNeutralButton(R.string.apply_all, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						UndoRedoObjectData prevURObjData = new UndoRedoObjectData();
						prevURObjData.setuRObjDataUniqueId(getObjectUniqueId());
						prevURObjData.setuRObjDataType(objType);
						prevURObjData.setuRPageNoObjDataStatus("applyAll");
						prevURObjData.setuRObjDataContent(preObjContent);

						UndoRedoObjectData currURObjData = new UndoRedoObjectData();
						currURObjData.setuRObjDataUniqueId(getObjectUniqueId());
						currURObjData.setuRObjDataType(objType);
						currURObjData.setuRPageNoObjDataStatus("applyAll");
						currURObjData.setuRObjDataContent(currObjContent);

						context.page.createAndRegisterUndoRedoObjectData(prevURObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PREVIOUS_STATE);
						context.page.createAndRegisterUndoRedoObjectData(currURObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PRESENT_STATE);
						prevObjContent = objContent;

						pageNoContentUpdateForAllPages(objContent);
						dialog.cancel();
					}
				});

				builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						prevObjContent = objContent;
						dialog.cancel();
					}
				});
				builder.show();
			}
		}
	}

	@Override
	public void wtjiSetAndReturnWebContent(String webContent) {
		synchronized (jsReturnValueLock) {
			jsReturnValues.put(0, webContent);
			jsReturnValueLock.notifyAll();
		}
	}

	/**
	 * @author getAndSetWebViewContent
	 */
	public void getAndSetWebViewContent() {
		if (!objType.equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
			this.objContentUpdated = false;
			loadJSScript("javascript:getWebContent();", webView);
		}
	}

	/**
	 * @author setWebViewContentFromJavascriptListener
	 */
	@Override
	public void wtjiSetWebContent(String text) {
		if (objType.equals(Globals.objType_pageNoText)) {
			int pno = Integer.parseInt(getObjPageNo()) - 1;
			String[] split = text.split(pno + "</div>");
			this.objContent = split[0];
		} else if (objType.equals(Globals.OBJTYPE_TITLETEXT)) {
			String[] split = text.split(">");
			String title = split[1].replace("</div", "").replace("&nbsp;", "");
			context.currentBook.setBookTitle(title);
			this.objContent = text;
		} else if (objType.equals(Globals.OBJTYPE_AUTHORTEXT)) {
			String[] split = text.split(">");
			String author = split[1].replace("</div", "").replace("&nbsp;", "");
			context.currentBook.setBookAuthorName(author);
			this.objContent = text;
		} else {
			this.objContent = text;
			String location = Math.round(this.getX())+"|"+Math.round(this.getY());
			String updateQuery = "update objects set location='"+location+"',content='"+objContent+"', locked='"+objLocked+"' where RowID='"+objUniqueId+"'";
			//String updateQuery = "update objects set content='"+objContent+"', locked='"+objLocked+"' where RowID='"+objUniqueId+"'";
			context.db.executeQuery(updateQuery);
		}
		if(!context.currentBook.is_bStoreBook()&& context.backClicked &&context.titleTextClicked) {
			context.titleTextClicked=false;
			context.finish();
		}
		//context.gridView.invalidateViews();
		//context.db.executeQuery("update objects set content='"+objContent.replace("'", "''")+"' where RowID='"+objUniqueId+"'");
		this.objContentUpdated = true;

		if (prevObjContent != "" && isURObjContent && !objType.equals(Globals.objType_pageNoText)) {
			String currObjContent = getObjectContent().trim();
			String preObjContent = prevObjContent.trim();
			if (!currObjContent.contentEquals(preObjContent)) {
				UndoRedoObjectData prevURObjData = new UndoRedoObjectData();
				prevURObjData.setuRObjDataUniqueId(getObjectUniqueId());
				prevURObjData.setuRObjDataType(objType);
				prevURObjData.setuRObjDataContent(preObjContent);

				UndoRedoObjectData currURObjData = new UndoRedoObjectData();
				currURObjData.setuRObjDataUniqueId(getObjectUniqueId());
				currURObjData.setuRObjDataType(objType);
				currURObjData.setuRObjDataContent(currObjContent);
				if (objType.equals("WebTable")) {
					if (mWebTablePrevRow != mWebTableRow || mWebTablePrevColumn != mWebTableColumn) {
						prevURObjData.setuRObjDataWidth(mWebTextPrevWidth);
						prevURObjData.setuRObjDataHeight(mWebTextPrevHeight);
						prevURObjData.setuRObjDataWebTableColumn(mWebTablePrevColumn);
						prevURObjData.setuRObjDataWebTableRow(mWebTablePrevRow);
						prevURObjData.setuRObjDataType("WebTable");
						currURObjData.setuRObjDataWidth(mWebTextWidth);
						currURObjData.setuRObjDataHeight(mWebTextHeight);
						currURObjData.setuRObjDataWebTableRow(mWebTableRow);
						currURObjData.setuRObjDataWebTableColumn(mWebTableColumn);
						currURObjData.setuRObjDataType("WebTable");
						mWebTablePrevRow = mWebTableRow;
						mWebTablePrevColumn = mWebTableColumn;
					}
				}
				context.page.createAndRegisterUndoRedoObjectData(prevURObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PREVIOUS_STATE);
				context.page.createAndRegisterUndoRedoObjectData(currURObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PRESENT_STATE);
			}
			isURObjContent = false;
			prevObjContent = objContent;
		}
	}

	/**
	 * @author getObjectContent
	 */
	public String getObjectContent() {
		return this.objContent;
	}


	/**
	 * @param view
	 * @param idValue
	 * @author getAndSetWebViewFontSize
	 */
	public void getAndSetWebViewFontSize(WebView view, String idValue) {
		String script = "javascript:getFontSize('" + idValue + "');";
		loadJSScript(script, view);
	}

	/**
	 * @author setWebViewFontSizeFromJavascriptListener
	 */
	@Override
	public void wtjisetWebFontSize(int _fontSize) {
		this.fontSize = _fontSize;
	}

	@Override
	public void popoverViewWillShow(PopoverView view) {

	}

	@Override
	public void popoverViewDidShow(PopoverView view) {

	}

	@Override
	public void popoverViewWillDismiss(PopoverView view) {
		isURObjContent = true;
		getAndReturnWebViewContent();
	}

	@Override
	public void popoverViewDidDismiss(PopoverView view) {
		if (prevObjLocked != objLocked && !objType.equals(Globals.objType_pageNoText)) {
			UndoRedoObjectData prevURObjData = new UndoRedoObjectData();
			prevURObjData.setuRObjDataUniqueId(getObjectUniqueId());
			prevURObjData.setuRObjDataLocked(prevObjLocked);
			context.page.createAndRegisterUndoRedoObjectData(prevURObjData, Globals.OBJECT_LOCKED, Globals.OBJECT_PREVIOUS_STATE);

			UndoRedoObjectData currURObjData = new UndoRedoObjectData();
			currURObjData.setuRObjDataUniqueId(getObjectUniqueId());
			currURObjData.setuRObjDataLocked(objLocked);
			context.page.createAndRegisterUndoRedoObjectData(currURObjData, Globals.OBJECT_LOCKED, Globals.OBJECT_PRESENT_STATE);
		}
		if (prevObjSequentialId != objSequentialId) {
			UndoRedoObjectData prevURObjData = new UndoRedoObjectData();
			prevURObjData.setuRObjDataUniqueId(getObjectUniqueId());
			prevURObjData.setuRObjDataType(objType);
			prevURObjData.setuRObjDataSequentialId(prevObjSequentialId);
			context.page.createAndRegisterUndoRedoObjectData(prevURObjData, Globals.OBJECT_SEQUENTIAL_ID_CHANGED, Globals.OBJECT_PREVIOUS_STATE);

			UndoRedoObjectData currURObjData = new UndoRedoObjectData();
			currURObjData.setuRObjDataUniqueId(getObjectUniqueId());
			currURObjData.setuRObjDataType(objType);
			currURObjData.setuRObjDataSequentialId(objSequentialId);
			context.page.createAndRegisterUndoRedoObjectData(currURObjData, Globals.OBJECT_SEQUENTIAL_ID_CHANGED, Globals.OBJECT_PRESENT_STATE);
		}
	}

	/**
	 * update page number content for current page
	 */
	public void pageNoContentUpdateForCurrentPage(String _objContent) {
		int pno = Integer.parseInt(getObjPageNo()) - 1;
		String[] split = _objContent.split(pno + "</div>");
		this.objContent = split[0];

		objContent = objContent.replace("'", "''");
		context.db.executeQuery("update objects set content='" + objContent + "' where RowID='" + objUniqueId + "'");
		objContent = objContent.replace("''", "'");
	}

	/**
	 * update page number content for all pages
	 */
	public void pageNoContentUpdateForAllPages(String _objContent) {
		int pno = Integer.parseInt(getObjPageNo()) - 1;
		String[] split = _objContent.split(pno + "</div>");
		this.objContent = split[0];

		objContent = objContent.replace("'", "''");
		context.db.executeQuery("update objects set content='" + objContent + "' where objType='" + Globals.objType_pageNoText + "' and BID='" + getObjBid() + "'");
		context.db.executeQuery("update books set PageNoContent='" + objContent + "' where BID='" + getObjBid() + "'");
		context.currentBook.set_bPageNoContent(objContent);
		objContent = objContent.replace("''", "'");
	}

	@Override
	public void tsjiJSError(String error) {

	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (!hasFocus) {
			if (v instanceof WebView) {
				getAndSetWebViewContent();
			}
		}
	}

	/**
	 * @param view
	 * @param idValue
	 * @author getAndSetWebViewFontFamily
	 */
	public void getAndSetWebViewFontFamily(WebView view, String idValue) {
		String script = "javascript:getFontFamily('" + idValue + "');";
		loadJSScript(script, view);
	}

	/**
	 * @author setWebFontFamilyFromJavascriptListener
	 */
	@Override
	public void wtjisetWebFontFamily(String _fontFamily) {
		_fontFamily = _fontFamily.replace("'", "");
		this.fontFamily = _fontFamily;
	}

	/**
	 * @author ChangeFontFamily
	 */
	public void changeFontFamily(String _fontFamily, String idValue) {
		//Typeface tface = Typeface.createFromFile(_fontPath);
		if (textSelected) {
			String script = "javascript:surroundSelectionFontFamily('" + _fontFamily + "');";
			loadJSScript(script, webView);
		} else {
			String script = "javascript:document.getElementById('" + idValue + "').style.fontFamily ='" + _fontFamily + "'";
			loadJSScript(script, webView);
			_fontFamily = _fontFamily.replace("'", "");
			this.fontFamily = _fontFamily;
		}
	}

	public void createSpanForSelectedText() {
		String script = "javascript:createSpan();";
		loadJSScript(script, webView);
	}

	/**
	 * @param _fontColor
	 * @param idValue
	 * @author ChangeFontColor
	 */
	public void changeFontColor(String _fontColor, String idValue) {
		//Typeface tface = Typeface.createFromFile(_fontPath);
		if (textSelected) {
			String script = "javascript:surroundSelection('" + _fontColor + "');";
			loadJSScript(script, webView);
		} else {
			String script = "javascript:document.getElementById('" + idValue + "').style.color = '" + _fontColor + "'";
			loadJSScript(script, webView);
			this.fontColor = _fontColor;
		}
	}

	/**
	 * @param view
	 * @param idValue
	 * @author getAndSetWebViewFontColor
	 */
	private void getAndSetWebViewFontColor(WebView view, String idValue) {
		loadJSScript("javascript:getFontColor('" + idValue + "');", view);
	}

	/**
	 * @author setWebFontColorFromJavascriptListener
	 */
	@Override
	public void wtjisetWebFontColor(String _fontColor) {
		this.fontColor = _fontColor;
	}

	/**
	 * @param _backgroundColor
	 * @param idValue
	 * @author ChangeBackgroundColor
	 */
	public void changeBackgroundColor(String _backgroundColor, String idValue) {
		loadJSScript("javascript:document.getElementById('" + idValue + "').style.backgroundColor = '" + _backgroundColor + "'", webView);
		this.backgroundColor = _backgroundColor;
	}

	/**
	 * Method to change the cell backgroundColor in table
	 *
	 * @param _cellColor
	 */
	public void changeCellBackgroundColor(String _cellColor) {
		loadJSScript("javascript:CellBackground('" + _cellColor + "');", webView);
		this.tableCellColor = _cellColor;
	}

	/**
	 * Method to change the row background color in table
	 *
	 * @param _cellColor
	 */
	public void changeRowBackgroundColor(String _cellColor) {
		loadJSScript("javascript:RowBackground('" + _cellColor + "');", webView);
		this.tableRowColor = _cellColor;
	}

	/**
	 * Method to change the column background color in table
	 *
	 * @param _cellColor
	 */
	public void changeColumnBackgroundColor(String _cellColor) {
		loadJSScript("javascript:ColumnBackground('" + _cellColor + "');", webView);
		this.tableColumnColor = _cellColor;
	}

	/**
	 * Method to change the table background color in table
	 *
	 * @param _backgroundColor
	 */
	public void changeTableBackgroundColor(String _backgroundColor) {
		loadJSScript("javascript:TableBackground('" + _backgroundColor + "');", webView);
		this.tableBackgroundColor = _backgroundColor;
	}

	public void changeTableFont(String _fontFamily) {
		//webView.loadUrl("javascript:document.getElementById('SBATable').style.fontFamily ='"+_fontFamily+"'");
		loadJSScript("javascript:ChangeFont('" + _fontFamily + "');", webView);
		_fontFamily = _fontFamily.replace("'", "");
		this.fontFamily = _fontFamily;
	}

	public void insertRowsInTable(int rowAt, int newRows) {
		this.setmWebTableRow(this.getmWebTableRow() + newRows);
		loadJSScript("javascript:InsertRow('" + rowAt + "', '" + newRows + "', '" + this.getmWebTableColumn() + "');", webView);
		//getAndSetWebTableHeight(webView);
		//resizeDivWidthAndHeight(mWebTextWidth,mWebTextHeight+150);
		if(popoverView!=null){
			popoverView.dissmissPopover(false);
		}
	}

	public void insertColumnsInTable(int columnAt, int newColumns) {
		this.setmWebTableColumn(this.getmWebTableColumn() + newColumns);
		loadJSScript("javascript:InsertColumn('" + columnAt + "', '" + newColumns + "', '" + this.getmWebTableRow() + "');", webView);
		//getAndSetWebTableHeight(webView);
		if(popoverView!=null){
			popoverView.dissmissPopover(false);
		}
	}

	public void deleteRowInTable(int rowAt, int deleteRows) {
		this.setmWebTableRow(this.getmWebTableRow() - deleteRows);
		loadJSScript("javascript:DeleteRow('" + rowAt + "', '" + deleteRows + "');", webView);
		//getAndSetWebTableHeight(webView);
		if(popoverView!=null){
			popoverView.dissmissPopover(false);
		}
	}

	public void deleteColumnInTable(int columnAt, int deleteColumns) {
		this.setmWebTableColumn(this.getmWebTableColumn() - deleteColumns);
		loadJSScript("javascript:DeleteColumn('" + columnAt + "', '" + deleteColumns + "', '" + this.getmWebTableRow() + "');", webView);
		//getAndSetWebTableHeight(webView);
		if(popoverView!=null){
			popoverView.dissmissPopover(false);
		}
	}

	/**
	 * @param view
	 * @param idValue
	 * @author getAndSetWebViewBackgroundColor
	 */
	private void getAndSetWebViewBackgroundColor(WebView view, String idValue) {
		loadJSScript("javascript:getBackgroundColor('" + idValue + "');", view);
	}

	/**
	 * @author setWebBackgroundColorFromJavascriptListener
	 */
	@Override
	public void wtjisetWebBackgroundColor(String _backgroundColor) {
		this.backgroundColor = _backgroundColor;
	}

	/**
	 * Get the current table cell color and set it
	 *
	 * @param view
	 * @param idValue
	 */
	public void getAndSetWebTableCellColor(WebView view, String idValue) {
		loadJSScript("javascript:getTableCellColor();", webView);
	}

	/**
	 * Get the current table row color and set it.
	 *
	 * @param view
	 * @param idValue
	 */
	public void getAndSetWebTableRowColor(WebView view, String idValue) {
		loadJSScript("javascript:getTableRowColor();", view);
	}

	/**
	 * Get and set the current table Column color :
	 *
	 * @param view
	 * @param idValue
	 */
	public void getAndSetWebTableColumnColor(WebView view, String idValue) {
		loadJSScript("javascript:getTableColumnColor();", view);
	}

	/**
	 * Get and set the current table background color:
	 *
	 * @param view
	 * @param idValue
	 */
	public void getAndSetWebTableBackgroundColor(WebView view, String idValue) {
		loadJSScript("javascript:getTableBackgroundColor();", view);
	}

	/**
	 * Get and set the current table font color :
	 *
	 * @param view
	 * @param idValue
	 */
	public void getAndSetWebTableFontColor(WebView view, String idValue) {
		loadJSScript("javascript:getTableFontColor();", view);
	}

	public void getAndSetWebTableHeight(WebView view) {
		loadJSScript("javascript:getTableHeight();", view);
	}

	@Override
	public void wtjisetTableCellColor(String cellColor) {
		this.tableCellColor = cellColor;
	}

	@Override
	public void wtjisetTableRowColor(String rowColor) {
		this.tableRowColor = rowColor;
	}

	@Override
	public void wtjisetTableColumnColor(String columnColor) {
		this.tableColumnColor = columnColor;
	}

	@Override
	public void wtjisetTableBackgroundColor(String backgroundColor) {
		this.tableBackgroundColor = backgroundColor;
	}

	@Override
	public void wtjisetTableFontColor(String fontColor) {
		this.tableFontColor = fontColor;
		this.fontColor = fontColor;
	}

	@Override
	public void wtjisetTableHeight(int height) {

		this.mWebTextHeight = height + 25;
		//Update the frame :
		context.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mWebTextWidth, mWebTextHeight);
				setLayoutParams(layoutParams);
				context.page.reassignHandlerArea(CustomWebRelativeLayout.this);
			}
		});
	}

	/**
	 * Add Row to Web Table
	 *
	 * @param totalColumns
	 * @param totalRows
	 */
	public void addRows(int totalRows, int totalColumns) {
		this.setmWebTableRow(this.getmWebTableRow() + totalRows);
		loadJSScript("javascript:addRowsToTable(" + totalRows + ", " + totalColumns + ");", webView);
	}

	/**
	 * DeleteRowsFromTable
	 *
	 * @param totalRows
	 */
	public void delRows(int totalRows) {
		this.setmWebTableRow(this.getmWebTableRow() - totalRows);
		loadJSScript("javascript:deleteRowsFromTable(" + totalRows + ")", webView);
	}

	/**
	 * Add Columns to Web Table
	 *
	 * @param totalColumns
	 * @param totalRows
	 */
	public void addColumns(int totalRows, int totalColumns) {
		this.setmWebTableColumn(this.getmWebTableColumn() + totalColumns);
		loadJSScript("javascript:addColumnsToTable(" + totalRows + ", " + totalColumns + ");", webView);
	}

	/**
	 * DeleteColumnsFromTable
	 *
	 * @param totalColumns
	 * @param totalColumns
	 */
	public void delColumns(int totalRows, int totalColumns) {
		this.setmWebTableColumn(this.getmWebTableColumn() - totalColumns);
		loadJSScript("javascript:deleteColumnsFromTable(" + totalRows + ", " + totalColumns + ")", webView);
	}

	/**
	 * Makes the webview align the text when any image moves above it
	 *
	 * @param imageUniqueId
	 * @param imageXPos
	 * @param imageYPos
	 */
	public void alignTextAsImageMoves(String imageObjType, int imageXPos, int imageYPos, int imageWidth, int imageHeight, int sequenceId) {
		Rect imageRect = new Rect(imageXPos, imageYPos, imageXPos + imageWidth, imageYPos + imageHeight);
		Rect webRect = new Rect((int) this.getX(), (int) this.getY(), (int) this.getX() + this.getWidth(), (int) this.getY() + this.getHeight());
		int pxWidth = Globals.getPixelValue(imageWidth, context);
		int pxHeight = Globals.getPixelValue(imageHeight, context);
		if (Rect.intersects(webRect, imageRect) && this.objType.equals("WebText") && sequenceId > this.objSequentialId) {
			loadJSScript("javascript:createDiv(" + imageObjType + ")", webView);
			loadJSScript("javascript:document.getElementById('exclude'+" + imageObjType + ").style.width='" + pxWidth + "px'", webView);
			loadJSScript("javascript:document.getElementById('exclude'+" + imageObjType + ").style.height='" + pxHeight + "px'", webView);
			int xDistance = Globals.getPixelValue(imageXPos - (int) this.getX(), context);
			int yDistance = Globals.getPixelValue(imageYPos - (int) this.getY(), context);
			loadJSScript("javascript:mahuMove(" + xDistance + ", " + yDistance + ", " + pxWidth + ", " + pxHeight + ")", webView);
		}
	}

	/**
	 * this shows the color picker dialog
	 * @param listView
	 */
	public void showPopUpColorPickerDialog(final ListView list) {
		int color = 0;
		if (fontColor != null && !fontColor.equals("transparent")) {
			color = Color.parseColor(fontColor);
		}
		final ArrayList<String> recentFontColorList = context.page.getRecentColorsFromSharedPreference(Globals.fontColorKey);
		new PopupColorPicker(context, color, null, recentFontColorList, true, new ColorPickerListener() {
			String hexColor = null;
			;

			@Override
			public void pickedColor(int color) {
				hexColor = String.format("#%06X", (0xFFFFFF & color));
				changeFontColor(hexColor, "content");
				if(list!=null) {
					list.invalidateViews();
				}
			}

			@Override
			public void dismissPopWindow() {
				if (hexColor != null) {
					context.page.setandUpdateRecentColorsToSharedPreferences(Globals.fontColorKey, recentFontColorList, hexColor);
				}
			}

			@Override
			public void pickedTransparentColor(
					String transparentColor) {
				changeFontColor(transparentColor, "content");
			}

			@Override
			public void pickColorFromBg() {
				context.pickColorFromBgView = true;
				context.pickedColorFor = "FontColor";
			}
		});
	}

	/**
	 * Shows Color Picker Dialog in a PopOver view to change the Font Color of webtext
	 *
	 * @param view
	 */
	public void showPopOverColorPickerDialogToChangeFontColor(View view) {
		int color = 0;
		if (fontColor != null && !fontColor.equals("transparent")) {
			color = Color.parseColor(fontColor);
		}
		final ArrayList<String> recentFontColorList = context.page.getRecentColorsFromSharedPreference(Globals.fontColorKey);
		new PopOverColorPicker(context, color, view, recentFontColorList, new ClrPickerListener() {
			String hexColor = null;

			@Override
			public void pickedColor(int color) {
				hexColor = String.format("#%06X", (0xFFFFFF & color));
				changeFontColor(hexColor, "content");
				context.btnKeyToolbar_pickColor.setBackgroundColor(color);
			}

			@Override
			public void dismissPopWindow() {
				if (hexColor != null) {
					context.page.setandUpdateRecentColorsToSharedPreferences(Globals.fontColorKey, recentFontColorList, hexColor);
				}
			}

			@Override
			public void pickedTransparentColor(String transparentColor) {
				changeFontColor(transparentColor, "content");
			}

			@Override
			public void pickColorFromBg() {
				context.pickColorFromBgView = true;
				context.pickedColorFor = "FontColor";
				//Globals.hideKeyboard(context);
			}
		});
	}

	/**
	 * this shows font list dialog to choose fonts
	 */
	public void showFontListDialog() {
		final Dialog fontListDialog = new Dialog(context);
		fontListDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
		fontListDialog.setTitle("Choose Font");
		fontListDialog.setContentView(context.getLayoutInflater().inflate(R.layout.font_list_view, null));
		fontListDialog.getWindow().setLayout((int) (context.global.getDeviceWidth() / 1.8), (int) (context.global.getDeviceHeight() / 1.8));
		ListView lv = (ListView) fontListDialog.findViewById(R.id.listView1);
		lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		lv.setAdapter(new FontListAdapter(context, CustomWebRelativeLayout.this));
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
									int position, long arg3) {
				if (firstTimeStartUp) {
					currentSelectedListView = adapterView.getChildAt(2);
				}
				firstTimeStartUp = false;
				if (currentSelectedListView != null && currentSelectedListView != view) {
					UnHighlightCurrentSlectedView(currentSelectedListView);
				}
				currentSelectedListView = view;
				highlightCurrentSlectedView(currentSelectedListView);

				changeFontFamily(UserFunctions.m_fontNames.get(position), "content");
			}
		});
		fontListDialog.show();
	}

	/**
	 * Shows Font Picker Dialog in a PopOver view to change the Font of webtext
	 *
	 * @param view
	 */
	public void showPopOverFontPickerDialog(View view) {
		final PopoverView popoverView = new PopoverView(context, R.layout.list_view);
		int popOverWidth = (int) context.getResources().getDimension(R.dimen.bookview_font_picker_popover_width);
		int popOverHeight = (int) context.getResources().getDimension(R.dimen.bookview_font_picker_popover_height);
		popoverView.setContentSizeForViewInPopover(new Point(popOverWidth, popOverHeight));
		popoverView.setDelegate(this);
		popoverView.showPopoverFromRectInViewGroup(context.designPageLayout, PopoverView.getFrameForView(view), PopoverView.PopoverArrowDirectionDown, true);
		ListView lv = (ListView) popoverView.findViewById(R.id.listView1);
		lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		lv.setAdapter(new FontListAdapter(context, CustomWebRelativeLayout.this));
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
									int position, long arg3) {
				if (firstTimeStartUp) {
					currentSelectedListView = adapterView.getChildAt(2);
				}
				firstTimeStartUp = false;
				if (currentSelectedListView != null && currentSelectedListView != view) {
					UnHighlightCurrentSlectedView(currentSelectedListView);
				}
				currentSelectedListView = view;
				highlightCurrentSlectedView(currentSelectedListView);

				context.btnKeyToolbar_pickFont.setText(UserFunctions.m_fontNames.get(position));

				changeFontFamily(UserFunctions.m_fontNames.get(position), "content");
			}
		});
	}

	/**
	 * Change the WebText of text alignment
	 *
	 * @param _textAlign
	 * @param idValue
	 */
	public void textAlign(String _textAlign, String idValue) {
		String script = "javascript:document.getElementById('" + idValue + "').style.textAlign = '" + _textAlign + "'";
		loadJSScript(script, webView);
	}

	/**
	 * insert ordered list for webtext
	 *
	 * @param idValue
	 */
	public void insertOrderedList() {
		String script = "javascript:document.execCommand('insertOrderedList', 'false', 'orderedList')";
		loadJSScript(script, webView);
	}

	/**
	 * insert unordered list for webtext
	 *
	 * @param idValue
	 */
	public void insertUnOrderedList() {
		String script = "javascript:document.execCommand('insertUnOrderedList', 'false', 'unOrderedList')";
		loadJSScript(script, webView);
	}

	/**
	 * Set On touch Listener active
	 */
	public void setOnTouchListenerFroWebView() {
		webView.setOnTouchListener(this);
	}

	/**
	 * Set On touch Listener to null
	 */
	public void setOnTouchListenerToNullForWebView() {
		webView.requestFocus(View.FOCUS_DOWN);
		webView.setOnTouchListener(null);
	}

	/**
	 * check and set the selection is active in webview
	 */
	public void getAndSetSelectionIsActive() {
		String script = "javascript:isSelectionActive();";
		loadJSScript(script, webView);
	}

	public void changeLanguageQuiz(String direction, String textAlign, String backValue, String restartValue) {
		for (int i = 0; i < quizSequenceObjectArray.size(); i++) {
			QuizSequenceObject quizSeqObject = quizSequenceObjectArray.get(i);
			String script1 = "javascript:changeLanguageQuiz('" + quizSeqObject.getUniqueid() + "','" + quizSeqObject.getSectionId() + "','" + direction + "','" + textAlign + "','" + backValue + "','" + restartValue + "');";
			loadJSScript(script1, webView);
		}
	}


	private void getQuizLanguage(int UniqueId, int sectionId, WebView view) {
		String version;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			version = "KITKAT";
			webView.evaluateJavascript("javascript:getLanguage('" + UniqueId + "','" + sectionId + "','" + version + "');", new ValueCallback<String>() {
				@Override
				public void onReceiveValue(String value) {
					String language = UserFunctions.getValuFromJsonreader(value);
					Language = language;

				}
			});
		} else {
			version = "ICS";
			webView.loadUrl("javascript:getLanguage('" + UniqueId + "','" + sectionId + "','" + version + "');");
		}
		// String script="javascript:getLanguage('"+UniqueId+"','"+sectionId+"');";
		//loadJSScript(script, webView);
	}

	@Override
	public void wtjisetSelectionActive(String selectionActive) {
		if (selectionActive.equals("true") || selectionActive.equals("")) {
			textSelected = false;
			String script = "javascript:resetSelectionRangeValue();";
			loadJSScript(script, webView);
		} else {
			textSelected = true;
			String script = "javascript:createSpan();";
			loadJSScript(script, webView);
		}
	}

	/**
	 * this loads the javascript by comparing the version
	 *
	 * @param script
	 * @param view
	 */
	public static void loadJSScript(final String script, final WebView view) {
		view.post(new Runnable() {

			@Override
			public void run() {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
					view.evaluateJavascript(script, null);
				} else {
					view.loadUrl(script);
				}

			}
		});
	}

    public void  wtjisetSelectedRowColumn(String row,String Column){
		rowPosition=Integer.parseInt(row);
		columnPosition=Integer.parseInt(Column);
		rowPosition=rowPosition+1;
		columnPosition=columnPosition+1;
	}
	@Override
	public void wtjisetSelectedText(String selectedText) {
		// TODO Auto-generated method stub
		//System.out.println(selectedText+"text");
		selecttext = selectedText;
		if (SearchClicked) {
			final Dialog advSearchPopUp = new Dialog(context);
			advSearchPopUp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
			advSearchPopUp.setTitle("Search via");
			advSearchPopUp.setContentView(context.getLayoutInflater().inflate(R.layout.advsearch, null));
			ListView advListView = (ListView) advSearchPopUp.findViewById(R.id.listViewAdv);
			final EditText etAdv = (EditText) advSearchPopUp.findViewById(R.id.editTextAdv);
			etAdv.setText(selectedText);
			advSearchPopUp.getWindow().setLayout((int) (context.global.getDeviceWidth() / 1.8), (int) (context.global.getDeviceHeight() / 1.8));
			advListView.setDividerHeight(2);
			advListView.setAdapter(new advAdapter(context, Globals.advSearch));
			advSearchPopUp.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					SearchClicked = false;
					if (context.enrTabsLayout.getWidth() > context.designPageLayout.getWidth()) {
						int enrTabsWidth = context.enrTabsLayout.getWidth();
						context.enrScrollView.smoothScrollTo(enrTabsWidth, 0);

					}
				}
			});
			advListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, final int position,
										long arg3) {
					// TODO Auto-generated method stub
					final String url;
					String advSearchtext;
					switch (position) {

//						case 0:
//							url = "http://projects.nooor.com/SemaBooksSearch/BkSearch.aspx?Srch=" + etAdv.getText().toString() + "";
//							advSearchtext = etAdv.getText().toString();
//							context.runOnUiThread(new Runnable() {
//
//								@Override
//								public void run() {
//									context.new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
//									advSearchPopUp.dismiss();
//								}
//							});//clickedAdvancedSearch(Globals.advSearch[position], url);
//							//advSearchPopUp.setOnDismissListener(BookView.this);
//							advSearchPopUp.dismiss();
//							break;
//
//						case 1:
//							url = "http://www.nooor.com/search/" + etAdv.getText().toString() + "/";
//							advSearchtext = etAdv.getText().toString();
//							//clickedAdvancedSearch(advSearch[position], url);
//							context.runOnUiThread(new Runnable() {
//
//								@Override
//								public void run() {
//									context.new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
//									advSearchPopUp.dismiss();
//								}
//							});
//							break;

						case 0:
							url = "http://www.google.com/search?q=" + etAdv.getText().toString() + "";
							advSearchtext = etAdv.getText().toString();
							context.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									context.new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
									advSearchPopUp.dismiss();
								}
							});
							//advSearchPopUp.setOnDismissListener(BookView.this);
							advSearchPopUp.dismiss();
							break;

						case 1:
							String text = etAdv.getText().toString();
							String language = detectLanguage(text);

							String percentEscpe = Uri.encode(text);
							if (language.equals("ar")) {
								url = "http://ar.wikipedia.org/wiki/" + percentEscpe + "";
							} else {
								url = "http://en.wikipedia.org/wiki/" + percentEscpe + "";
							}
							advSearchtext = etAdv.getText().toString();
							context.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									context.new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
									advSearchPopUp.dismiss();
								}
							});
							//advSearchPopUp.setOnDismissListener(BookView.this);
							advSearchPopUp.dismiss();
							break;

						case 2:
							url = "http://m.youtube.com/#/results?q=" + etAdv.getText().toString() + "";
							advSearchtext = etAdv.getText().toString();
							context.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									context.new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
									advSearchPopUp.dismiss();
								}
							});//advSearchPopUp.setOnDismissListener(BookView.this);
							advSearchPopUp.dismiss();
							break;

						case 3:
							url = "http://search.yahoo.com/search?p=" + etAdv.getText().toString() + "";
							advSearchtext = etAdv.getText().toString();
							context.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									context.new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
									advSearchPopUp.dismiss();
								}
							});
							//advSearchPopUp.setOnDismissListener(BookView.this);
							advSearchPopUp.dismiss();
							break;

						case 4:
							url = "http://www.bing.com/search?q=" + etAdv.getText().toString() + "";
							advSearchtext = etAdv.getText().toString();
							context.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									context.new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
									advSearchPopUp.dismiss();
								}
							});
							//advSearchPopUp.setOnDismissListener(BookView.this);
							advSearchPopUp.dismiss();
							break;

						case 5:
							url = "http://www.ask.com/web?q=" + etAdv.getText().toString() + "";
							advSearchtext = etAdv.getText().toString();
							context.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									context.new CreatingAdvsearchEnrichments(Globals.advSearch[position], url).execute();
									//context.CreatingAdvsearchEnrichments(Globals.advSearch[position], url);
									advSearchPopUp.dismiss();
								}
							});
							advSearchPopUp.dismiss();
							break;

						default:
							break;
					}
				}

			});
			advSearchPopUp.show();

		} else if (mindMapMenuClicked) {
			MindMapDialog mindMapDialog = new MindMapDialog(((BookViewActivity) context), false);
			if (Globals.mindMapFileNameLastOpened == null) {
				mindMapDialog.showMindMapDialog(selecttext);
				mindMapMenuClicked=false;
			} else {
				mindMapDialog.openMindmapActivity("", selecttext, Globals.mindMapFileNameLastOpened,Globals.mindMapLastBook);
			}
		} else if (hyperLinkClicked) {
                HyperLinkDialog mindMapDialog = new HyperLinkDialog(((BookViewActivity) context),webView);
                getAndSetSelectionIsActive();
		    	mindMapDialog.showHyperLinkDialog();
			    hyperLinkClicked=false;
			//	hyperlink_image = false;
     } else {
			ClipboardManager mClipboard = (ClipboardManager) context.getSystemService(context.CLIPBOARD_SERVICE);
			ClipData data = ClipData.newPlainText("label", selectedText);
			mClipboard.setPrimaryClip(data);
			//mClipboard.setText(this.selectedText);
			Toast toast = Toast.makeText(context, "Text Copied to Clipboard", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.BOTTOM, 0, 0);
			toast.show();
		}
	}

	public static void loadurl(WebView web) {
		// TODO Auto-generated method stub
		loadJSScript("javascript:getSelectedText();", web);
	}



	public static String detectLanguage(String selectedText) {

		String alpha = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
		String[] alphaArray = alpha.split(",");
		for (int i = 0; i < alphaArray.length; i++) {
			String current = alphaArray[i];
			if (selectedText.contains(current)) {
				return "en";
			}
		}
		return "ar";
	}

	/**
	 * Replaces the image in the files folder.
	 *
	 * @param quiz_imgPath
	 */
	/*public void SaveUriFile(String sourceUri, String quiz_imgPath){
		String imgDir = quiz_imgPath;
		String SourceFilePath = sourceUri;
		String DestFilePath = imgDir;
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		try{
			bis = new BufferedInputStream(new FileInputStream(SourceFilePath));
			bos = new BufferedOutputStream(new FileOutputStream(DestFilePath,false));
			byte[] buffer = new byte[1024];
			bis.read(buffer);
			do{
				bos.write(buffer);
			}while(bis.read(buffer)!=-1);
		}catch(IOException e){

		}finally{
			try{
				if(bis!=null) bis.close();
				if(bos!=null) bos.close();
			}catch(IOException e){

			}
		}
		UserFunctions.makeFileWorldReadable(imgDir);
	}*/
	public void SaveUriFile(String sourceUri, String quiz_imgPath) {
		String imgDir = quiz_imgPath;
		String SourceFilePath = sourceUri;
		String DestFilePath = imgDir;
		Bitmap b = BitmapFactory.decodeFile(SourceFilePath);
		Bitmap out = Bitmap.createScaledBitmap(b, mWebTextWidth / 2, mWebTextHeight / 2, false);
		File f = new File(DestFilePath);
		FileOutputStream fos = null;
		//String videoImagePath = toPath;
		try {
			fos = new FileOutputStream(f);
			out.compress(Bitmap.CompressFormat.PNG, 100, fos);
			fos.flush();
			fos.close();
			//setVideoThumbnail(videoImagePath);
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		UserFunctions.makeFileWorldReadable(imgDir);
	}

	/**
	 * @param quiz ImageName
	 * @author Take photo from gallery
	 */
	public void takePhotoFromGallery() {
		Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		galleryIntent.setType("image/*");
		context.startActivityForResult(galleryIntent, CAPTURE_FROM_GALLERY);
	}

	/**
	 * Checks whether the device supports camera or not.
	 */
	private boolean isDeviceSupportCamera() {
		if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @author Take photo from camera
	 */
	public void callCameraToTakePhoto() {

		//Checking camera availability
		if (!isDeviceSupportCamera()) {
			Toast.makeText(context, "Sorry! Your device doesn't support camera", Toast.LENGTH_LONG).show();
		} else {

			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			//	//System.out.println("The image path: "+this.objPathContent);
			imageFileUri = Uri.fromFile(getOutputMediaFile(MEDIA_TYPE_IMAGE));
			intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
			context.startActivityForResult(intent, CAPTURE_WEB_IMAGE_ACTIVITY_REQUEST_CODE);
		}
	}

	/**
	 * Creates path to save camera image
	 */
	private File getOutputMediaFile(int type) {

		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "SboookAuthor");
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("CamCamera", "failed to create directory");
				return null;
			}
		}
		// Create a media file name
		String ImageName = "" + objUniqueId;
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator + "image_" + ImageName + ".jpg");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + ImageName + ".mp4");
		} else {
			return null;
		}
		return mediaFile;
	}

	public void getParentTableBgForStoreWebRl() {
		String version;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			version = "KITKAT";
			webView.evaluateJavascript("javascript:getParentTableBackground('" + version + "');", new ValueCallback<String>() {
				@Override
				public void onReceiveValue(String value) {
					String splitValue = UserFunctions.getValuFromJsonreader(value);
					insertTableBgForStoreWebRl(splitValue);
				}
			});
		} else {
			version = "ICS";
			webView.loadUrl("javascript:getParentTableBackground('" + version + "');");
		}
	}

	@Override
	public void wtjisetParentTableBg(String content) {
		insertTableBgForStoreWebRl(content);
	}

	@Override
	public void wtjisetLanguage(String language) {
		Language = language;
	}

	private void insertTableBgForStoreWebRl(String splitValue) {
		if (splitValue != "") {
			String[] backgroundSplit = splitValue.split("\\$\\$");
			for (int i = 0; i < backgroundSplit.length; i++) {
				String[] splitTableContent = backgroundSplit[i].split("\\#\\#");

				if (splitTableContent.length > 1) {
					String filePath = splitTableContent[1];
					String location = splitTableContent[0];
					String size = "ScaleToFit";
					//	int sequentialId = ((CustomImageRelativeLayout) view).getObjectSequentialId();
					context.db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + "PageTableBG" + "', '" + context.currentBook.getBookID() + "', '" + context.currentPageNumber + "', '" + location + "', '" + size + "', '" + filePath + "', '" + "0" + "', '" + "yes" + "', '" + "0" + "',  '" + context.page.enrichedPageId + "')");
				}
			}
			getAndSetWebViewContent();
		}
	}

	public void resetParentTableBgForStoreWebRl() {
		String version;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			version = "KITKAT";
		} else {
			version = "ICS";
		}
		ArrayList<String> TableBGContentArray = context.db.selectPgTableBgFromObjects(String.valueOf(context.currentBook.getBookID()), Integer.parseInt(context.currentPageNumber));
		for (int j = 0; j < TableBGContentArray.size(); j++) {
			String[] splitContent = TableBGContentArray.get(j).split("\\#\\#");

			String location = splitContent[0];
			String filePath = splitContent[1];
			if (filePath.contains("$$")) {
				filePath = filePath.replace("$$", "");
			}
			if (version == "KITKAT") {
				webView.evaluateJavascript("javascript:setParentTableBackground('" + location + "', '" + filePath + "')", null);
			} else {
				webView.loadUrl("javascript:setParentTableBackground('" + location + "', '" + filePath + "')");
			}
		}
		context.db.executeQuery("delete from objects where objType='" + "PageTableBG" + "' and PageNo='" + context.currentPageNumber + "' and BID='" + context.currentBook.getBookID() + "' and EnrichedID = '" + context.page.enrichedPageId + "'");
		getAndSetWebViewContent();
	}


	private void addFromFileManager() {
		final PopoverView popoverView = new PopoverView(context, R.layout.files_manager);
		popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.info_popover_width), (int) context.getResources().getDimension(R.dimen.info_popover_height)));
		popoverView.setDelegate(this);
		popoverView.showPopoverFromRectInViewGroup(context.mainDesignView, PopoverView.getFrameForView(btnInfo), PopoverView.PopoverArrowDirectionAny, true);
		final ListView listView = (ListView) popoverView.findViewById(R.id.listView1);
		final Button btn_back = (Button) popoverView.findViewById(R.id.btnBack);
		currentFilePath = Globals.TARGET_BASE_FILE_PATH + "UserLibrary";
		listingFilesAndFolders(currentFilePath);
		listView.setAdapter(new FileImageAdapter(context, selectedFiles, "Image"));
		//listView.setAdapter(new FileManagerAdapter(context, currentFilePath, objType));
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
									int position, long arg3) {
				ClickedFilePath listedFiles = selectedFiles.get(position);
				if (listedFiles.isFolderSelected() && !listedFiles.isFile()) {
					//String[]split=listedFiles.getFolderPath().split("/");
					currentFilePath = listedFiles.getFolderPath();
					btn_back.setText(listedFiles.getFolderTitle());
					listingFilesAndFolders(currentFilePath);
					listView.setAdapter(new FileImageAdapter(context, selectedFiles, "Image"));

				} else if (listedFiles.isFile() && !listedFiles.isFolderSelected()) {
					//if(objType.equals("Image")){
					//setImageBackground(listedFiles.getFolderPath());
					//SaveUriFile(listedFiles.getFolderPath());
					String quiz_imgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID() + "/images/" + "" + quizImageFileName + ".png";
					SaveUriFile(listedFiles.getFolderPath(), quiz_imgPath);
					String bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + context.currentBook.getBookID();
					int randomNoToRefresh = new Random().nextInt();

					CustomWebRelativeLayout.loadJSScript("javascript:document.getElementById('" + quizImageFileName + "').src=\"/" + bookDir + "/images/" + quizImageFileName + ".png?" + randomNoToRefresh + "\"", webView);

					//}

					popoverView.dissmissPopover(true);
				}
				if (!btn_back.getText().toString().equals("UserLibrary")) {
					btn_back.setCompoundDrawablesWithIntrinsicBounds(R.drawable.arrownext, 0, 0, 0);
				} else {
					btn_back.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
				}

			}
		});
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!currentFilePath.equals(Globals.TARGET_BASE_FILE_PATH + "UserLibrary")) {
					String filePath = currentFilePath.replace("/" + btn_back.getText(), "");
					currentFilePath = filePath;
					String[] split = currentFilePath.split("/");
					btn_back.setText(split[split.length - 1]);
					listingFilesAndFolders(currentFilePath);
					listView.setAdapter(new FileImageAdapter(context, selectedFiles, "Image"));
					if (btn_back.getText().toString().equals("UserLibrary")) {
						btn_back.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
					}
				}
			}
		});

	}

	private void listingFilesAndFolders(String filePath) {
		File f = new File(filePath);
		File listFiles[] = f.listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {

				return !file.isHidden();
			}
		});
		//selectedFiles = new ArrayList<ClickedFilePath>();
		if (selectedFiles.size() > 0) {
			selectedFiles.clear();
		}
		ClickedFilePath file;
		for (int i = 0; i < listFiles.length; i++) {
			String fileName = listFiles[i].getName();
			String Path = listFiles[i].getPath();
			if (listFiles[i].isDirectory()) {
				file = new ClickedFilePath(context);
				file.setFolderSelected(true);
				file.setFolderPath(Path);
				file.setFile(false);
				file.setFolderTitle(fileName);
				selectedFiles.add(file);
			} else {
				if (!listFiles[i].isHidden()) {
					if (fileName.contains(".jpg") || fileName.contains(".png") || fileName.contains(".gif")) {
						file = new ClickedFilePath(context);
						file.setFolderSelected(false);
						file.setFolderPath(Path);
						file.setFile(true);
						file.setFolderTitle(fileName);
						selectedFiles.add(file);
					}
				}
			}
		}

	}



}


