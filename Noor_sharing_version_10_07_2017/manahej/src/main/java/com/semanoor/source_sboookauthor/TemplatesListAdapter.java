package com.semanoor.source_sboookauthor;

import java.util.ArrayList;

import com.semanoor.manahij.MainActivity;
import com.semanoor.manahij.R;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TemplatesListAdapter extends BaseAdapter {

	private MainActivity context;
	private ArrayList<Templates> templateList;
	
	public TemplatesListAdapter(MainActivity _context, ArrayList<Templates> _templateList) {
		this.context = _context;
		this.templateList = _templateList;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return templateList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	public static class ViewHolder{
		public ImageView imgView;
		public TextView txtView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		if (convertView == null) {
			view = context.getLayoutInflater().inflate(R.layout.image_text_list, null);
			holder=new ViewHolder();
			holder.imgView=(ImageView)view.findViewById(R.id.imageView1);
			holder.txtView=(TextView)view.findViewById(R.id.textView1);
			view.setTag(holder);
		} else {
			holder=(ViewHolder)view.getTag();
		}
		Templates templates = templateList.get(position);
		view.setId(templates.getTemplateId());
		holder.txtView.setText(templates.getTemplateName());
		String thumbImagePath = Globals.TARGET_BASE_TEMPATES_PATH + templates.getTemplateId() + "/frontThumb.png";
		Bitmap bitmap = BitmapFactory.decodeFile(thumbImagePath);
		holder.imgView.setImageBitmap(bitmap);

		return view;
	}

}
