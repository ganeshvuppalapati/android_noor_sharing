package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.ClickedFilePath;
import com.semanoor.manahij.MediaAlbumCreatorActivity;
import com.semanoor.manahij.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Krishna on 26-08-2015.
 */
public class FileImageAdapter extends BaseAdapter implements ListAdapter  {
    BookViewActivity activity;
    String filePath;
    File files[];
    String object;
    File listedFiles[];
    ArrayList<ClickedFilePath> selectedFiles;
    Context context;
    public FileImageAdapter(Context context, ArrayList<ClickedFilePath> path,String objType) {
       this.context=context;
       this.selectedFiles=path;
      // File f=new File(path);
      // files=f.listFiles();
       object=objType;
       //listingFilesAndFolders(filePath);


    }


    @Override
    public int getCount() {
        return selectedFiles.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            if(context instanceof BookViewActivity) {
                view = ((BookViewActivity)context).getLayoutInflater().inflate(R.layout.image_text_list_item, null);
            }else if(context instanceof MediaAlbumCreatorActivity){
                view = ((MediaAlbumCreatorActivity)context).getLayoutInflater().inflate(R.layout.image_text_list_item, null);
            }
        }
        RelativeLayout rl_imageLayout = (RelativeLayout) view.findViewById(R.id.layout);
        final ImageView imgView = (ImageView) view.findViewById(R.id.imageView1);
        TextView tv= (TextView) view.findViewById(R.id.textView1);
        CheckBox cb=(CheckBox)view.findViewById(R.id.checkBox);
        imgView.setBackgroundResource(0);
        ClickedFilePath listedFiles=selectedFiles.get(position);
        if(context instanceof BookViewActivity) {
            cb.setVisibility(View.GONE);
            if (object.equals("Image")) {
                if (listedFiles.isFolderSelected() && !listedFiles.isFile()) {
                    imgView.setBackgroundResource(0);
                    imgView.setImageBitmap(null);
                    imgView.setBackgroundResource(R.drawable.collectioncell_normal);
                    tv.setText(listedFiles.getFolderTitle());
                } else if (!listedFiles.isFolderSelected() && listedFiles.isFile()) {
                    String split[] = listedFiles.getFolderPath().split("/");
                    String fullName = listedFiles.getFolderPath().replace(split[split.length - 1], "." + split[split.length - 1]);
                    File f = new File(fullName);
                    Bitmap bitmap = null;
                    if (f.exists()) {
                        bitmap = BitmapFactory.decodeFile(fullName);
                    } else {
                        bitmap = BitmapFactory.decodeFile(listedFiles.getFolderPath());
                    }
                    imgView.setImageBitmap(bitmap);
                    tv.setText(listedFiles.getFolderTitle());
                }
            } else if (object.equals("YouTube")) {
                if (listedFiles.isFolderSelected() && !listedFiles.isFile()) {
                    imgView.setBackgroundResource(0);
                    imgView.setImageBitmap(null);
                    imgView.setBackgroundResource(R.drawable.collectioncell_normal);
                    tv.setText(listedFiles.getFolderTitle());
                } else if (!listedFiles.isFolderSelected() && listedFiles.isFile()) {
                    // Bitmap bitmap = BitmapFactory.decodeFile(listedFiles.getFolderPath());
                    String split[] = listedFiles.getFolderPath().split("/");
                    String fullName = listedFiles.getFolderPath().replace(split[split.length - 1], "." + split[split.length - 1]);
                    File f = new File(fullName);
                    Bitmap bitmap = null;
                    if (f.exists()) {
                        bitmap = BitmapFactory.decodeFile(fullName);
                    } else {
                        bitmap = BitmapFactory.decodeFile(listedFiles.getFolderPath());
                    }
                    imgView.setImageBitmap(bitmap);
                    tv.setText(listedFiles.getFolderTitle());
                }
            } else if (object.equals("Audio")) {
                if (listedFiles.isFolderSelected() && !listedFiles.isFile()) {
                    imgView.setBackgroundResource(0);
                    imgView.setImageBitmap(null);
                    imgView.setBackgroundResource(R.drawable.collectioncell_normal);
                    tv.setText(listedFiles.getFolderTitle());
                } else if (!listedFiles.isFolderSelected() && listedFiles.isFile()) {
                    //Bitmap bitmap = BitmapFactory.decodeFile(R.drawable.default_audio_stop);
                    imgView.setBackgroundResource(R.drawable.default_audio_stop);
                    tv.setText(listedFiles.getFolderTitle());
                }
            }
        }else if(context instanceof MediaAlbumCreatorActivity) {

            if (object.equals("Image")) {
                if (listedFiles.isFolderSelected() && !listedFiles.isFile()) {
                    imgView.setBackgroundResource(0);
                    imgView.setImageBitmap(null);
                    imgView.setBackgroundResource(R.drawable.collectioncell_normal);
                    tv.setText(listedFiles.getFolderTitle());
                    cb.setVisibility(View.GONE);
                } else if (!listedFiles.isFolderSelected() && listedFiles.isFile()) {
                    cb.setVisibility(View.VISIBLE);
                    String split[] = listedFiles.getFolderPath().split("/");
                    String fullName = listedFiles.getFolderPath().replace(split[split.length - 1], "." + split[split.length - 1]);
                    File f = new File(fullName);
                    Bitmap bitmap = null;
                    if (f.exists()) {
                        bitmap = BitmapFactory.decodeFile(fullName);
                    } else {
                        bitmap = BitmapFactory.decodeFile(listedFiles.getFolderPath());
                    }
                    imgView.setImageBitmap(bitmap);
                    tv.setText(listedFiles.getFolderTitle());
                    if(listedFiles.isSelected()){
                       cb.setChecked(true);
                    }else{
                        cb.setChecked(false);
                    }
                }
            }


        }

        return view;
    }


}
