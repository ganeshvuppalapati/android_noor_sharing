package com.semanoor.source_sboookauthor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import android.os.Environment;
import android.util.Log;

public class Zip {
	
	private static final int BUFFER = 2048;
	
	 public boolean zipFileAtPath(String sourcePath, String toLocation) {

		    // ArrayList<String> contentList = new ArrayList<String>();
		    File sourceFile = new File(sourcePath);
		    try {
		        BufferedInputStream origin = null;
		        FileOutputStream dest = new FileOutputStream(toLocation);
		        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
		                dest));
		        if (sourceFile.isDirectory()) {
		            zipSubFolder(out, sourceFile, sourceFile.getParent().length());
		        } else {
		            byte data[] = new byte[BUFFER];
		            FileInputStream fi = new FileInputStream(sourcePath);
		            origin = new BufferedInputStream(fi, BUFFER);
		            ZipEntry entry = new ZipEntry(getLastPathComponent(sourcePath));
		            out.putNextEntry(entry);
		            int count;
		            while ((count = origin.read(data, 0, BUFFER)) != -1) {
		                out.write(data, 0, count);
		            }
		        }
		        out.close();
		    } catch (Exception e) {
		        e.printStackTrace();
		        return false;
		    }
		    return true;
		}

		private void zipSubFolder(ZipOutputStream out, File folder,
		        int basePathLength) throws IOException {
		    File[] fileList = folder.listFiles();
		    BufferedInputStream origin = null;
		    for (File file : fileList) {
		        if (file.isDirectory()) {
		            zipSubFolder(out, file, basePathLength);
		        } else {
		            byte data[] = new byte[BUFFER];
		            String unmodifiedFilePath = file.getPath();
		            String relativePath = unmodifiedFilePath
		                    .substring(basePathLength);
		            Log.i("ZIP SUBFOLDER", "Relative Path : " + relativePath);
		            FileInputStream fi = new FileInputStream(unmodifiedFilePath);
		            origin = new BufferedInputStream(fi, BUFFER);
		            ZipEntry entry = new ZipEntry(relativePath);
		            out.putNextEntry(entry);
		            int count;
		            while ((count = origin.read(data, 0, BUFFER)) != -1) {
		                out.write(data, 0, count);
		            }
		            origin.close();
		        }
		    }
		}
		
		/**
		 * generate Zip Files for Enrichments
		 */
		public void zipFilesForEnrichments(File folder, String zipFilePath){
			String zipFileName = new File(zipFilePath).getName();
			try {
				BufferedInputStream origin = null;
				FileOutputStream dest = new FileOutputStream(zipFilePath);
				ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
				File[] files = folder.listFiles();
				byte data[] = new byte[BUFFER];
				for (File file : files) {
					if (!file.getName().contains(zipFileName)) {
						FileInputStream fi = new FileInputStream(file);
						origin = new BufferedInputStream(fi, BUFFER);
						ZipEntry entry = new ZipEntry(file.toString().substring(file.toString().lastIndexOf("/") + 1));
						out.putNextEntry(entry);
						int count;
						while ((count = origin.read(data, 0, BUFFER)) != -1) {
							out.write(data, 0, count);
						}
						origin.close();
					}
				}
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		/**
	 	* Generate Zip for Media Album
	 	*/
		public void zipFilesForMediaAlbum(File folder, String zipFilePath){
			String zipFileName = new File(zipFilePath).getName();
			try {
				BufferedInputStream origin = null;
				FileOutputStream dest = new FileOutputStream(zipFilePath);
				ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
				File[] files = folder.listFiles();
				byte data[] = new byte[BUFFER];
				for (File file : files) {
					if (!file.getName().contains(zipFileName) && !file.getName().contains("FreeFiles") && !file.isHidden()) {
						FileInputStream fi = new FileInputStream(file);
						origin = new BufferedInputStream(fi, BUFFER);
						ZipEntry entry = new ZipEntry(file.toString().substring(file.toString().lastIndexOf("/") + 1));
						out.putNextEntry(entry);
						int count;
						while ((count = origin.read(data, 0, BUFFER)) != -1) {
							out.write(data, 0, count);
						}
						origin.close();
					}
				}
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * generate Zip Files for Epub
		 */
		public void zipFilesForEpub(File folder, String zipFilePath){
			try {
				BufferedInputStream origin = null;
				FileOutputStream dest = new FileOutputStream(zipFilePath);
				ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
				File[] files = folder.listFiles();
				byte data[] = new byte[BUFFER];
				for (File file : files) {
					if (file.isDirectory()) {
						zipSubFolder(out, file, file.getParent().length());
					} else {
						FileInputStream fi = new FileInputStream(file);
						origin = new BufferedInputStream(fi, BUFFER);
						ZipEntry entry = new ZipEntry(file.toString().substring(file.toString().lastIndexOf("/") + 1));
						out.putNextEntry(entry);
						int count;
						while ((count = origin.read(data, 0, BUFFER)) != -1) {
							out.write(data, 0, count);
						}
						origin.close();
					}
				}
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public String getLastPathComponent(String filePath) {
		    String[] segments = filePath.split("/");
		    String lastPathComponent = segments[segments.length - 1];
		    return lastPathComponent;
		}
		
		public static Boolean unzipDownloadfileNew(String zipfilepath, String unziplocation)
	    { 
	    	InputStream is;
	    	ZipInputStream zis;
	    	try
	    	{
	    		is = new FileInputStream(zipfilepath);
	    		zis = new ZipInputStream(new BufferedInputStream(is));
	    		ZipEntry ze;
	    		while ((ze = zis.getNextEntry()) != null) 
	    		{	
	    			String zipEntryName = ze.getName();
	    			zipEntryName = zipEntryName.replace("\\", "/");
	    			////System.out.println("zipEntryName: "+zipEntryName);
	    			File  file = new File(unziplocation+zipEntryName);
	    			if(file.exists()){
	    				////System.out.println("file exists at the given path:" +file);
	    			}else{
	    				
	    				if(ze.isDirectory()){
	    					////System.out.println("Ze is an directory. It will create directory and exit");
	    					file.mkdirs();
	    					continue;
	    				}
	    				
	    					file.getParentFile().mkdirs();   //Horror line..............
	    					byte[] buffer = new byte[1024];
	    					FileOutputStream fout = new FileOutputStream(file);
	    					BufferedOutputStream baos  = new BufferedOutputStream(fout, 1024);
	    					int count;
	    					while((count = zis.read(buffer, 0, 1024))!= -1){
	    						baos.write(buffer, 0, count);
	    					}
	    					baos.flush();
	    					baos.close();
	    			}
	    		}
	    			zis.close();
	    			////System.out.println("finished unzipping");
	    			return true;
	    	}catch(Exception e){
	    		////System.out.println("unzipping error"+e);
	    		return false;
	    	}
	    }
	public static Boolean unzipDownloadfileTemp(String zipfilepath, String unziplocation)
	{
		InputStream is;
		ZipInputStream zis;
		try
		{
			is = new FileInputStream(zipfilepath);
			zis = new ZipInputStream(new BufferedInputStream(is));
			ZipEntry ze;
			while ((ze = zis.getNextEntry()) != null)
			{
				String zipEntryName = ze.getName();
				zipEntryName = zipEntryName.replace("\\", "/");
				if (zipEntryName.contains("/")){
					String[] split = zipEntryName.split("/");
					zipEntryName = split[split.length-1];
				}
				////System.out.println("zipEntryName: "+zipEntryName);
				File  file = new File(unziplocation+zipEntryName);
				if(file.exists()){
					////System.out.println("file exists at the given path:" +file);
				}else{

					if(ze.isDirectory()){
						////System.out.println("Ze is an directory. It will create directory and exit");
						file.mkdirs();
						continue;
					}

					file.getParentFile().mkdirs();   //Horror line..............
					byte[] buffer = new byte[1024];
					FileOutputStream fout = new FileOutputStream(file);
					BufferedOutputStream baos  = new BufferedOutputStream(fout, 1024);
					int count;
					while((count = zis.read(buffer, 0, 1024))!= -1){
						baos.write(buffer, 0, count);
					}
					baos.flush();
					baos.close();
				}
			}
			zis.close();
			////System.out.println("finished unzipping");
			return true;
		}catch(Exception e){
			////System.out.println("unzipping error"+e);
			return false;
		}
	}

}