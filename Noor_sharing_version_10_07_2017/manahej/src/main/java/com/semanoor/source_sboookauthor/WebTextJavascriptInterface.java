package com.semanoor.source_sboookauthor;

import android.webkit.JavascriptInterface;

import com.semanoor.manahij.BookViewActivity;

public class WebTextJavascriptInterface {
	
	/** The javascript interface name for adding to web view. */
	private final String interfaceName = "WebText";
	
	/** The webview to work with. */
	private WebTextJavascriptInterfaceListener listener;
	
	/** The context. */
	BookViewActivity mContext;

	public WebTextJavascriptInterface(BookViewActivity _context,
			WebTextJavascriptInterfaceListener _listener) {
		// TODO Auto-generated constructor stub
		this.mContext = _context;
		this.listener = _listener;
	}
	
	/**
	 * Gets the interface name
	 * @return
	 */
	@JavascriptInterface
	public String getInterfaceName(){
		return this.interfaceName;
	}
	
	/**
	 * Handles javascript errors.
	 * @param error
	 */
	@JavascriptInterface
	public void jsError(String error){
		if(this.listener != null){
			this.listener.tsjiJSError(error);
		}
	}
	
	/**
	 * Sets the Web Content
	 *
	 */
	@JavascriptInterface
	public void setWebContent(String text){
		if (this.listener != null) {
			//System.out.println("WebText:"+text);
			this.listener.wtjiSetWebContent(text);
		}
	}

	@JavascriptInterface
	public void setAndReturnWebContent(String webContent) {
		if (this.listener != null) {
			this.listener.wtjiSetAndReturnWebContent(webContent);
		}
	}
	
	
	/**
	 * Sets the Web Font Size
	 *
	 */
	@JavascriptInterface
	public void setFontSize(String fontSize){
		if (this.listener != null) {
			//System.out.println("WebText:"+text);
			String split[] = fontSize.split("px");
			this.listener.wtjisetWebFontSize(Integer.parseInt(split[0]));
		}
	}
	
	/**
	 * Sets the Web Font Family
	 *
	 */
	@JavascriptInterface
	public void setFontFamily(String fontFamily){
		if (this.listener != null) {
			this.listener.wtjisetWebFontFamily(fontFamily);
		}
	}
	
	/**
	 * Sets the Web Font Color
	 *
	 */
	@JavascriptInterface
	public void setFontColor(String fontColor){
		if (this.listener != null) {
			this.listener.wtjisetWebFontColor(fontColor);
		}
	}
	
	/**
	 * Sets the Web Background Color
	 *
	 */
	@JavascriptInterface
	public void setBackgroundColor(String backgroundColor){
		if (this.listener != null) {
			this.listener.wtjisetWebBackgroundColor(backgroundColor);
		}
	}
	
	/**
	 * setSelectionActive
	 * @param selectionActive
	 */
	@JavascriptInterface
	public void setSelectionActive(String selectionActive) {
		if (this.listener != null) {
			this.listener.wtjisetSelectionActive(selectionActive);
		}
	}
	
	/**
	 * setDocumentHeight
	 * @param documentHeight
	 */
	@JavascriptInterface
	public void setDocumentHeight(String documentHeight) {
		if (this.listener != null) {
			this.listener.wtjisetDocumentHeight(documentHeight);
		}
	}
	
	/**
	 * set table cellColor
	 * @param cellColor
	 */
	@JavascriptInterface
	public void setTableCellColor(String cellColor){
		if (this.listener != null) {
			this.listener.wtjisetTableCellColor(cellColor);
		}
	}
	
	/**
	 * set table row color
	 * @param rowColor
	 */
	@JavascriptInterface
	public void setTableRowColor(String rowColor){
		if (this.listener != null) {
			this.listener.wtjisetTableRowColor(rowColor);
		}
	}
	
	/**
	 * set table column color
	 * @param columnColor
	 */
	@JavascriptInterface
	public void setTableColumnColor(String columnColor){
		if (this.listener != null) {
			this.listener.wtjisetTableColumnColor(columnColor);
		}
	}
	
	/**
	 * set table background color
	 * @param backgroundColor
	 */
	@JavascriptInterface
	public void setTableBackgroundColor(String backgroundColor){
		if (this.listener != null) {
			this.listener.wtjisetTableBackgroundColor(backgroundColor);
		}
	}
	
	/**
	 * set table font color
	 * @param FontColor
	 */
	@JavascriptInterface
	public void setTableFontColor(String FontColor){
		if (this.listener != null) {
			this.listener.wtjisetTableFontColor(FontColor);
		}
	}
	
	@JavascriptInterface
	public void setTableHeight(int height){
		if(this.listener != null){
			this.listener.wtjisetTableHeight(height);
		}
	}
	
	@JavascriptInterface
	public void setSelectedText(String selectedText){
		if(this.listener != null){
			this.listener.wtjisetSelectedText(selectedText.toString());
		}
	}

	@JavascriptInterface
	public void setParentTableBg(String content){
		if(this.listener != null){
			this.listener.wtjisetParentTableBg(content.toString());
		}
	}

	@JavascriptInterface
	public void setLanguage(String language){
		if(this.listener!=null){
			this.listener.wtjisetLanguage(language.toString());
		}
	}
	@JavascriptInterface
	public void setColoumnandRow(String row,String column ){
		if(this.listener != null){
			this.listener.wtjisetSelectedRowColumn(row.toString(),column.toString());
		}
	}
}
