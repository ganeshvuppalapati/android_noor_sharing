package com.semanoor.source_sboookauthor;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class WebTextListAdapter extends BaseAdapter {

	private BookViewActivity context;
	private Integer[] webTextInfoListArray;
	private CustomWebRelativeLayout customWRL;

	public WebTextListAdapter(BookViewActivity _context, Integer[] _webTextInfoListArray, CustomWebRelativeLayout customWebRelativeLayout) {
		this.context = _context;
		this.webTextInfoListArray = _webTextInfoListArray;
		this.customWRL = customWebRelativeLayout;
	}

	@Override
	public int getCount() {
		return webTextInfoListArray.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		//String Language=context.getResources().getString(R.string.langage);
		if(customWRL.objType.equals(Globals.OBJTYPE_QUIZWEBTEXT)) {
			if (position == 0) {
				view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_language, null);
				SegmentedRadioGroup segmentGroup = (SegmentedRadioGroup) view.findViewById(R.id.segment_text);
				RadioButton Radio_english = (RadioButton) segmentGroup.findViewById(R.id.segEng);
				RadioButton Radio_arabic = (RadioButton) segmentGroup.findViewById(R.id.segArabic);
				//objUniqueId = context.db.getMaxUniqueRowID("objects");
				//sectionId = context.db.getMaxUniqueRowID("tblMultiQuiz");
				//customWRL.getQuizLanguage(objUniqueId,sectionId);
				//customWRL.Language="en";
				if (customWRL.Language.contains("en")) {
					Radio_english.setChecked(true);
					Radio_arabic.setChecked(false);
				} else {
					Radio_arabic.setChecked(true);
					Radio_english.setChecked(false);
				}

				segmentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						String direction = null;
						String txtAlign = null;
						String backValue = null;
						String restartValue = null;
						if (checkedId == R.id.segEng) {
							customWRL.Language = "en";
							direction = "ltr";
							txtAlign = "left";
							backValue = "Back";
							restartValue = "Restart";
							// customWRL.changeLanguageQuiz(direction,txtAlign,backValue,restartValue);

						} else if (checkedId == R.id.segArabic) {
							customWRL.Language = "ar";
							direction = "rtl";
							txtAlign = "right";
							backValue = "رجوع";
							restartValue = "إعادة الاختبار";

						}
						customWRL.changeLanguageQuiz(direction, txtAlign, backValue, restartValue);
					}
				});
			} else if (position == 1) {      //1.size
				view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_size, null);
				NumberPicker np = (NumberPicker) view.findViewById(R.id.numberPicker1);
				np.setMinValue(8);
				np.setMaxValue(72);
				np.setValue(customWRL.fontSize);
				np.setOnValueChangedListener(new OnValueChangeListener() {

					@Override
					public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
						customWRL.getAndSetSelectionIsActive();
						customWRL.changeFontSize(newVal, "content");
					}
				});
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setText(webTextInfoListArray[position]);
			} else if (position == 2) {  //2.font //3.nil
				view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_font, null);
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setText(webTextInfoListArray[position]);
				TextView tv_fontFamily = (TextView) view.findViewById(R.id.textView2);
				tv_fontFamily.setText(customWRL.fontFamily);
			} else if (position == 3) {  //4.colour   //5.nil
				view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setText(webTextInfoListArray[position]);
				ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
				if (customWRL.fontColor != null && !customWRL.fontColor.equals("transparent")) {
					iv.setBackgroundColor(Color.parseColor(customWRL.fontColor));
				}
			} else if (position == 5) {  // // 6.background_color  //7.nill
				view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setText(webTextInfoListArray[position]);
				ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
				if (customWRL.backgroundColor != null && !customWRL.backgroundColor.equals("transparent")) {
					iv.setBackgroundColor(Color.parseColor(customWRL.backgroundColor));
				}
			} else if (position == 7) {   //8.lock or unlock
				if (customWRL.isObjLocked()) {
					webTextInfoListArray[7] = R.string.unlock;
				} else {
					webTextInfoListArray[7] = R.string.lock;
				}
				view = context.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setTextColor(Color.BLACK);
				tv.setText(webTextInfoListArray[position]);
			} else {
				view = context.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setTextColor(Color.BLACK);
				tv.setText(webTextInfoListArray[position]);
			}
		}
		else{
			if (position == 0) {      //1.size
				view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_size, null);
				NumberPicker np = (NumberPicker) view.findViewById(R.id.numberPicker1);
				np.setMinValue(8);
				np.setMaxValue(72);
				np.setValue(customWRL.fontSize);
				np.setOnValueChangedListener(new OnValueChangeListener() {

					@Override
					public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
						customWRL.getAndSetSelectionIsActive();
						customWRL.changeFontSize(newVal, "content");
					}
				});
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setText(webTextInfoListArray[position]);

			} else if (position == 1) {  //2.font //3.nil
				view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_font, null);
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setText(webTextInfoListArray[position]);
				TextView tv_fontFamily = (TextView) view.findViewById(R.id.textView2);
				tv_fontFamily.setText(customWRL.fontFamily);
			} else if (position == 2) {  //4.colour   //5.nil
				view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setText(webTextInfoListArray[position]);
				ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
				if (customWRL.fontColor != null && !customWRL.fontColor.equals("transparent")) {
					iv.setBackgroundColor(Color.parseColor(customWRL.fontColor));
				}
			} else if (position == 4) {  // // 6.background_color  //7.nill
				view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setText(webTextInfoListArray[position]);
				ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
				if (customWRL.backgroundColor != null && !customWRL.backgroundColor.equals("transparent")) {
					iv.setBackgroundColor(Color.parseColor(customWRL.backgroundColor));
				}
			} else if (position == 6) {   //8.lock or unlock
				if (customWRL.isObjLocked()) {
					webTextInfoListArray[6] = R.string.unlock;
				} else {
					webTextInfoListArray[6] = R.string.lock;
				}
				view = context.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setTextColor(Color.BLACK);
				tv.setText(webTextInfoListArray[position]);
			} else {
				view = context.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
				TextView tv = (TextView) view.findViewById(R.id.textView1);
				tv.setTextColor(Color.BLACK);
				tv.setText(webTextInfoListArray[position]);
			}
		}
		return view;
	}

}
