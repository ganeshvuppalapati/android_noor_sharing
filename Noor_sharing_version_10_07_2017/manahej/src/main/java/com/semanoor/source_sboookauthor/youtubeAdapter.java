package com.semanoor.source_sboookauthor;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListAdapter;
import android.widget.TextView;

public class youtubeAdapter implements ListAdapter {

	private BookViewActivity context;
	private YoutubePopUpWindow youtubePopUpWindow;
	
	public youtubeAdapter(BookViewActivity _context, YoutubePopUpWindow _youtubePopUpWindow) {
		this.context = _context;
		this.youtubePopUpWindow = _youtubePopUpWindow;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		//System.out.println("SearchTitleArray count:"+youtubePopUpWindow.SearchTitleArray.size());
		//System.out.println("SearchThumbArray count:"+youtubePopUpWindow.SearchThumbArray.size());
		//System.out.println("SearchViewCountArray count:"+youtubePopUpWindow.SearchViewCountArray.size());
		//System.out.println("SearchUploaderArray count:"+youtubePopUpWindow.SearchUploaderArray.size());
		//System.out.println("SearchDurationArray count:"+youtubePopUpWindow.SearchDurationArray.size());
		return youtubePopUpWindow.SearchTitleArray.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View view = convertView;
		if (view == null) {
			view = context.getLayoutInflater().inflate(R.layout.google_image_search_list, null);
		}
		ImageView imageView = (ImageView) view.findViewById(R.id.imageView1);
		imageView.setScaleType(ScaleType.FIT_XY);
		TextView titleText = (TextView) view.findViewById(R.id.textView1);
		/*TextView uploaderText = (TextView) view.findViewById(R.id.textView2);
		TextView viewsText = (TextView) view.findViewById(R.id.textView3);
		TextView durationText = (TextView) view.findViewById(R.id.textView4);*/
		
		titleText.setText(youtubePopUpWindow.SearchTitleArray.get(position));
		imageView.setImageBitmap(youtubePopUpWindow.SearchThumbArray.get(position));
		/*viewsText.setText(youtubePopUpWindow.SearchViewCountArray.get(position));
		uploaderText.setText(youtubePopUpWindow.SearchUploaderArray.get(position));
		durationText.setText(youtubePopUpWindow.SearchDurationArray.get(position));*/
		return view;
	}

	
	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		//return youtubeActivity.SearchTitleArray.size();
		return 1;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return true;
	}

}
