package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioGroup;

import com.semanoor.manahij.R;

/**
 * Created by Krishna on 28-10-2015.
 */
public class SegmentedRadioButton extends RadioGroup {

    public SegmentedRadioButton(Context context) {
            super(context);
        }

        public SegmentedRadioButton(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        protected void onFinishInflate() {
            super.onFinishInflate();
            changeButtonsImages();
        }

        private void changeButtonsImages(){
            int count = super.getChildCount();

            if(count > 1){
                super.getChildAt(0).setBackgroundResource(R.drawable.segment_radio_left1);
                for(int i=1; i < count-1; i++){
                    super.getChildAt(i).setBackgroundResource(R.drawable.segment_radio_middle1);
                }
                super.getChildAt(count-1).setBackgroundResource(R.drawable.segment_radio_right1);
            }else if (count == 1){
                super.getChildAt(0).setBackgroundResource(R.drawable.segment_button1);
            }
        }

}
