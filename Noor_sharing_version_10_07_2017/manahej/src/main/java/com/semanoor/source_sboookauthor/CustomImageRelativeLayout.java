package com.semanoor.source_sboookauthor;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.ClickedFilePath;
import com.semanoor.manahij.CloudActivity;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.PopoverView.PopoverViewDelegate;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


public class CustomImageRelativeLayout extends RelativeLayout implements OnTouchListener, OnClickListener, PopoverViewDelegate {

	private BookViewActivity context;
	public ImageView imgView;
	private int prevX;
	private int prevY;

	private String objType;
	private int objBid, objEnrichId;
	private String objPageNo;
	public int mImageXpos, mImageYpos, mImageWidth, mImageHeight;
	public int mImagePrevXpos, mImagePrevYpos, mImagePrevWidth, mImagePrevHeight;
	private String objLocation, objSize;
	private String objPathContent, prevObjPathContent;
	private int objUniqueId;
	private int objSequentialId, prevObjSequentialId;
	private String objScalePageToFit;
	private boolean objLocked, prevObjLocked;
	public  int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	public  int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
	public  int CAPTURE_GALLERY = 1;
	public  int VIDEO_GALLERY = 2;
	public  int MEDIA_TYPE_IMAGE = 1;
	public  int MEDIA_TYPE_VIDEO = 2;
	public  Uri imageFileUri;

	//private static Integer[] imgInfoListArray = {R.string.search_google_images, R.string.pick_photo, R.string.take_photo_camera, R.string.nil, R.string.lock, R.string.nil,R.string.send_to_library,R.string.add_from_Filemanager,R.string.nil, R.string.bring_to_front, R.string.send_to_back};
	private static Integer[] imgInfoListArray = {R.string.search_google_images, R.string.pick_photo, R.string.take_photo_camera, R.string.nil, R.string.lock, R.string.nil, R.string.bring_to_front, R.string.send_to_back};
    //private static Integer[] vidInfoListArray = {R.string.pick_video_youtube, R.string.pick_video_library, R.string.nil, R.string.lock, R.string.nil,R.string.send_to_library,R.string.add_from_Filemanager,R.string.nil, R.string.bring_to_front, R.string.send_to_back};
	private static Integer[] vidInfoListArray = {R.string.pick_video_youtube, R.string.pick_video_library, R.string.nil, R.string.lock, R.string.nil, R.string.bring_to_front, R.string.send_to_back};
	private static Integer[] iframeInfoListArray = {R.string.browse_url, R.string.nil, R.string.lock, R.string.nil, R.string.bring_to_front, R.string.send_to_back};
	private static Integer[] embedInfoListArray = {R.string.embed_code, R.string.nil, R.string.lock, R.string.nil, R.string.bring_to_front, R.string.send_to_back};
	//private static Integer[] audioInfoListArray = {R.string.play, R.string.stop, R.string.nil, R.string.lock, R.string.nil,R.string.send_to_library,R.string.add_from_Filemanager,R.string.nil, R.string.bring_to_front, R.string.send_to_back};
	private static Integer[] audioInfoListArray = {R.string.play, R.string.stop, R.string.nil, R.string.lock, R.string.nil, R.string.bring_to_front, R.string.send_to_back};

	private MediaPlayer mPlayer;
	
	private GestureDetector gestureDetector;
	private int imageContentChangeCount;
	Button btnInfo;
	String currentFilePath;
	ArrayList<ClickedFilePath> selectedFiles= new ArrayList<ClickedFilePath>();
	public CustomImageRelativeLayout(BookViewActivity _context, String _type, int _BID, String _objPageNo, String _location, String _size, String _objPathContent, int _objUniqueId, int _objSequentialId, String _objScalePageToFit, boolean _objLocked,  int _objEnrichId, AttributeSet _attrs) {
		super(_context, _attrs);

		this.context = _context;
		this.objType = _type;
		this.objBid = _BID;
		this.setObjPageNo(_objPageNo);
		this.setObjLocation(_location);
		this.setObjSize(_size);
		mImageXpos = Integer.parseInt(getObjLocation().split("\\|")[0]);
		mImageYpos = Integer.parseInt(getObjLocation().split("\\|")[1]);
		mImageWidth = Integer.parseInt(getObjSize().split("\\|")[0]);
		mImageHeight = Integer.parseInt(getObjSize().split("\\|")[1]);
		this.objPathContent = _objPathContent;
		this.objUniqueId = _objUniqueId;
		this.objSequentialId = _objSequentialId;
		this.setObjScalePageToFit(_objScalePageToFit);
		this.setObjLocked(_objLocked);
		this.setObjEnrichId(_objEnrichId);

		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mImageWidth, mImageHeight);

		imgView = new ImageView(context);
		if (!objType.equals("DrawnImage") && checkForStoreBookEnrichmentMalzamah()) {
				this.setBackgroundResource(R.drawable.border);
				imgView.setOnTouchListener(this);
		}
		this.addView(imgView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		if (!objType.equals("DrawnImage") && mImageWidth >= Globals.getDeviceIndependentPixels(30, context) && mImageHeight >= Globals.getDeviceIndependentPixels(30, context) && checkForStoreBookEnrichmentMalzamah()) {
			RelativeLayout toolBarlayout=new RelativeLayout(context);
			RelativeLayout.LayoutParams tbLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			tbLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			tbLayoutParams.setMargins(5, 5, 5, 0);
			toolBarlayout.setLayoutParams(tbLayoutParams);
			//toolBarlayout.setBackgroundColor(context.getResources().getColor(R.color.toolbar_bluecolor));
			this.addView(toolBarlayout);

			int dpDimen = (int) context.getResources().getDimension(R.dimen.objects_btn_size);
			btnInfo = new Button(context);
			btnInfo.setBackgroundResource(R.drawable.button_info_black);
			btnInfo.setOnClickListener(this);
			btnInfo.setVisibility(GONE);
			btnInfo.setId(R.id.btnInfo);

			toolBarlayout.addView(btnInfo, dpDimen, dpDimen);
			
			Button btnObjDelete = new Button(context);
			btnObjDelete.setBackgroundResource(R.drawable.delete_normal_new);
			btnObjDelete.setId(R.id.btn_delete_object);
			if(objType.equals("Image") || objType.equals("YouTube")){
				btnObjDelete.setVisibility(GONE);
			}
			btnObjDelete.setOnClickListener(this);
			RelativeLayout.LayoutParams btnDelLayoutParams = new RelativeLayout.LayoutParams(dpDimen, dpDimen);
			//btnDelLayoutParams.setMargins(5,0,5,0);
			btnDelLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			btnObjDelete.setLayoutParams(btnDelLayoutParams);
			toolBarlayout.addView(btnObjDelete);
		}

		this.setLayoutParams(layoutParams);
		this.setX(mImageXpos);
		this.setY(mImageYpos);
		if(objType.equals("Image")){
			imgView.setScaleType(ScaleType.FIT_XY);
			setImageBackground(objPathContent);
		}else if (objType.equals("YouTube")) {
			String videoPath = getPathByUniqueId();
			setVideoThumbnail(videoPath);
		}else if (objType.equals("DrawnImage")) {
			setBackgroundImageForDrawing();
		}else if (objType.equals("IFrame")) {
			imgView.setScaleType(ScaleType.FIT_XY);
			imgView.setImageDrawable(getResources().getDrawable(R.drawable.default_iframe_bg));
		}else if (objType.equals("EmbedCode")) {
			imgView.setScaleType(ScaleType.FIT_XY);
			imgView.setImageDrawable(getResources().getDrawable(R.drawable.iframe_embedcode));
		}else if (objType.equals("Audio")) {
			imgView.setScaleType(ScaleType.FIT_XY);
			String audioPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+objBid+"/"+"images/Default_Audio_Play.png";
			Bitmap audioBitmap = BitmapFactory.decodeFile(audioPath);
			imgView.setImageBitmap(audioBitmap);
		}
		context.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				gestureDetector = new GestureDetector(context, new GestureListener());
			}
		});
	}

	private boolean checkForStoreBookEnrichmentMalzamah(){
		if ((context.currentBook.is_bStoreBook() && context.currentBook.get_bStoreID().contains("C")) || (context.currentBook.is_bStoreBook() && getObjEnrichId() != 0) || !(context.currentBook.is_bStoreBook())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @author getObjectUniqueId
	 * @return 
	 */
	public int getObjectUniqueId(){
		return this.objUniqueId;
	}
	
	/**
	 * @author getObjectType
	 * @return 
	 */
	public String getObjectType(){
		return this.objType;
	}
	
	/**
	 * @author getObjectPathContent
	 * @return 
	 */
	public String getObjectPathContent(){
		return this.objPathContent;
	}
	
	/**
	 * this sets the obj path content
	 * @param objPathContent
	 */
	public void setObjectPathContent(String objPathContent) {
		this.objPathContent = objPathContent;
	}
	
	/**
	 * @author getObjectSequentialId
	 * @return 
	 */
	public int getObjectSequentialId(){
		this.objSequentialId = context.db.getSequentialIdFromDb(this.objUniqueId);
		return this.objSequentialId;
	}
	
	/**
	 * this sets the object Sequentialid
	 * @param objSequentialId
	 */
	public void setObjectSequentialId(int objSequentialId) {
		this.objSequentialId = objSequentialId;
	}
	
	/**
	 * get object Book ID
	 * @return
	 */
	public int getObjectBID() {
		return objBid;
	}
	
	/**
	 * @return the objLocked
	 */
	public boolean isObjLocked() {
		return objLocked;
	}

	/**
	 * @param objLocked the objLocked to set
	 */
	public void setObjLocked(boolean objLocked) {
		this.objLocked = objLocked;
	}
	
	/**
	 * @return the objPageNo
	 */
	public String getObjPageNo() {
		return objPageNo;
	}

	/**
	 * @param objPageNo the objPageNo to set
	 */
	public void setObjPageNo(String objPageNo) {
		this.objPageNo = objPageNo;
	}
	
	/**
	 * @return the objScalePageToFit
	 */
	public String getObjScalePageToFit() {
		return objScalePageToFit;
	}

	/**
	 * @param objScalePageToFit the objScalePageToFit to set
	 */
	public void setObjScalePageToFit(String objScalePageToFit) {
		this.objScalePageToFit = objScalePageToFit;
	}
	
	/**
	 * @return the objEnrichId
	 */
	public int getObjEnrichId() {
		return objEnrichId;
	}

	/**
	 * @param objEnrichId the objEnrichId to set
	 */
	public void setObjEnrichId(int objEnrichId) {
		this.objEnrichId = objEnrichId;
	}
	
	/**
	 * @return the objLocation
	 */
	public String getObjLocation() {
		return objLocation;
	}

	/**
	 * @param objLocation the objLocation to set
	 */
	public void setObjLocation(String objLocation) {
		this.objLocation = objLocation;
	}

	/**
	 * @return the objSize
	 */
	public String getObjSize() {
		return objSize;
	}

	/**
	 * @param objSize the objSize to set
	 */
	public void setObjSize(String objSize) {
		this.objSize = objSize;
	}

	/**
	 * @author setBackgroundImageForDrawing
	 */
	public void setBackgroundImageForDrawing(){
		if (new File(this.objPathContent).exists()) {
			Bitmap bitmap = BitmapFactory.decodeFile(this.objPathContent);
			imgView.setImageBitmap(bitmap);
		} else {
			imgView.setImageBitmap(null);
		}
	}
	
	public String getPathByUniqueId() {
		String path = Globals.TARGET_BASE_BOOKS_DIR_PATH+objBid+"/images/"+objUniqueId+".png";
		return path;
	}
	
	/**
	 * Gesture listener class for the double tap event
	 * @author Suriya
	 *
	 */
	private class GestureListener extends GestureDetector.SimpleOnGestureListener {
		@Override
	    public boolean onDown(MotionEvent event) {
	        return true;
	    }
		
	    // event when double tap occurs
	    @Override
	    public boolean onDoubleTap(MotionEvent e) {
	    	int toastMsg = 0;
	    	Globals.setClipboardView(CustomImageRelativeLayout.this);
	    	if (objType.equals("Image")) {
				toastMsg = R.string.image_copied;
			} else if (objType.equals("YouTube")) {
				toastMsg = R.string.video_copied;
			} else if (objType.equals("IFrame")) {
				toastMsg = R.string.iframe_copied;
			} else if (objType.equals("Audio")) {
				toastMsg = R.string.audio_copied;
			} else if (objType.equals("EmbedCode")) {
				toastMsg = R.string.embed_view_copied;
			}
	    	Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT).show();
	        return true;
	    }
	    
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		final int action = event.getAction();
		int positionX = (int) event.getRawX();
		int positionY = (int) event.getRawY();
		switch (action & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN: {
			context.designScrollPageView.setEnableScrolling(false);
			prevX = positionX;
			prevY = positionY;

			mImagePrevXpos = mImageXpos;
			mImagePrevYpos = mImageYpos;
			
			context.page.setCurrentView(this);
			context.page.makeSelectedArea(this);
			break;
		}

		case MotionEvent.ACTION_MOVE: {
			if (!isObjLocked()) {
				int deltaX = positionX-prevX;
				int deltaY = positionY-prevY;

				float viewCenterX = (this.getX()+deltaX)+(this.getWidth()/2);
				float viewCenterY = (this.getY()+deltaY)+(this.getHeight()/2);
				PointF viewCenterPoint = new PointF(viewCenterX, viewCenterY);

				if (context.page.webRelativeLayout != null && this.objType.equals("Image") && context.page.webRelativeLayout.getObjScalePageToFit().equals("no")) {
					context.page.webRelativeLayout.alignTextAsImageMoves(this.objType, (int)this.getX(), (int)this.getY(), this.getWidth(), this.getHeight(), this.objSequentialId);
				}

				//Snap To grid
				Object[] gridArr = context.page.checkForSnapPointsForGrid(viewCenterPoint);
				viewCenterPoint = (PointF) gridArr[0];
				boolean gridPointsSnapped = (Boolean) gridArr[1];
				Object[] objArr = context.page.checkForSnapBetweenObjects(this, viewCenterPoint);
				viewCenterPoint = (PointF) objArr[0];
				boolean objectPointsSnapped = (Boolean) objArr[2];

				float viewXpos = viewCenterPoint.x - (this.getWidth()/2);
				float viewYpos = viewCenterPoint.y - (this.getHeight()/2);

				//Restrict the objects within the boundry
				if (viewXpos <= 0) {
					viewXpos = 0;
				} if (viewYpos <= 0) {
					viewYpos = 0;
				} if (viewXpos+getWidth() >= context.designPageLayout.getWidth()) {
					viewXpos = context.designPageLayout.getWidth() - getWidth();
				} if (viewYpos+getHeight() >= context.designPageLayout.getHeight()) {
					//viewYpos = context.designPageLayout.getHeight() - getHeight();
				}

				setXAndYPos(viewXpos, viewYpos);

				prevX = positionX;  
				prevY = positionY;
				
				if (objectPointsSnapped) {
					context.page.snapToObjects(this, (View) objArr[1]);
				} else {
					context.page.removeObjectGridLines();
				}

				if (gridPointsSnapped) {
					context.page.snapToGrid(this);
				} else {
					context.page.removeGridLines();
				}

				context.page.extendScrollViewHeight(this, viewCenterPoint);
				
				context.page.reassignHandlerArea(this);
			}

			break;
		}

		case MotionEvent.ACTION_UP: {
			if (context.page.webRelativeLayout != null && this.objType.equals("Image") && context.page.webRelativeLayout.getObjScalePageToFit().equals("no")) {
				context.page.webRelativeLayout.getAndSetWebViewContent();
			}
			context.designScrollPageView.setEnableScrolling(true);
			context.page.removeObjectGridLines();
			context.page.removeGridLines();
			
			if (mImagePrevXpos != mImageXpos || mImagePrevYpos != mImageYpos) {
				UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
				uRPrevObjData.setuRObjDataUniqueId(objUniqueId);
				uRPrevObjData.setuRObjDataXPos(mImagePrevXpos);
				uRPrevObjData.setuRObjDataYPos(mImagePrevYpos);
				context.page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_MOVED, Globals.OBJECT_PREVIOUS_STATE);
				
				UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
				uRCurrObjData.setuRObjDataUniqueId(objUniqueId);
				uRCurrObjData.setuRObjDataXPos(mImageXpos);
				uRCurrObjData.setuRObjDataYPos(mImageYpos);
				context.page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_MOVED, Globals.OBJECT_PRESENT_STATE);
			}
				
			break;
		}

		case MotionEvent.ACTION_CANCEL: {

			break;
		}

		case MotionEvent.ACTION_POINTER_UP: {

			break;
		}
		}
		//return true;
		return gestureDetector.onTouchEvent(event);
	}
	
	/**
	 * Sets X and Y positon for the views
	 * @param viewXpos
	 * @param viewYpos
	 */
	public void setXAndYPos(float viewXpos, float viewYpos) {
		this.setX(viewXpos);
		this.setY(viewYpos);
		mImageXpos = (int) viewXpos;
		mImageYpos = (int) viewYpos;
	}
	
	/** 
	 * This updates the transformation values for the view
	 */
	public void setTransformationValues(float xPos, float yPos, LayoutParams layoutParams) {
		mImageXpos = (int) xPos;
		mImageYpos = (int) yPos;
		mImageWidth = layoutParams.width;
		mImageHeight = layoutParams.height;
	}
	
	/**
	 * This sets the transformation for the views
	 */
	public void setTransformation(float xPos, float yPos, LayoutParams layoutParams) {
		this.setX(xPos);
		this.setY(yPos);
		this.setLayoutParams(layoutParams);
		mImageXpos = (int) xPos;
		mImageYpos = (int) yPos;
		mImageWidth = layoutParams.width;
		mImageHeight = layoutParams.height;
	}
	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnInfo:
			//Show the image info in a PopOverView
			context.page.setCurrentView(this);
			context.page.makeSelectedArea(this);
			if (context.currentBook.is_bStoreBook() && !context.currentBook.isStoreBookCreatedFromTab()) {
				context.designScrollPageView.setEnableScrolling(false);
			}
			context.hideToolBarLayout();
			showinfoPopOverView(v);
			break;
			
		case R.id.btn_delete_object:
			context.page.setCurrentView(this);
			context.page.makeSelectedArea(this);
			context.deleteSelectedObject();
			break;

		default:
			break;
		}
	}

	/**
	 * @author ShowInfoinPopOverView
	 * @param v
	 */
	public void showinfoPopOverView(View v) {
		prevObjLocked = objLocked;
		prevObjSequentialId = getObjectSequentialId();
		if (objType.equals("Image")) {
			final PopoverView popoverView = new PopoverView(context, R.layout.list_info_view);
			popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.info_popover_width), (int) context.getResources().getDimension(R.dimen.web_info_height)));
			popoverView.setDelegate(this);
			popoverView.showPopoverFromRectInViewGroup(context.mainDesignView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionAny, true);

			TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
			txtTitle.setText(R.string.image_properties);
			
			final ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
			listView.setAdapter(new ImageInfoListAdapter(context, imgInfoListArray, this));
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long arg3) {
					switch (position) {
						case 0://Search Google Images
							GoogleImageSearchDialogWindow googleImgSearchView = new GoogleImageSearchDialogWindow(context, CustomImageRelativeLayout.this);
							googleImgSearchView.callDialogWindow();
							break;

						case 1://PickPhoto
							takePhotoFromGallery();
							popoverView.dissmissPopover(true);
							break;

						case 2://Take Photo from Camera
							callCameraToTakePhoto();
							popoverView.dissmissPopover(true);
							break;

					case 4://Lock
						if (isObjLocked()) {
							setObjLocked(false);
							imgInfoListArray[4] = R.string.lock;
							listView.invalidateViews();
						} else {
							setObjLocked(true);
							imgInfoListArray[4] = R.string.unlock;
							listView.invalidateViews();
						}
						break;
					/*case 6:     //send to  FileManager
						 if(!objPathContent.contains("defaultbg")) {
								Calendar cal = Calendar.getInstance();
								SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
								String formattedDate = df.format(cal.getTime());
								//String[] split = objPathContent.split("/");
								//String fileName[] = split[split.length - 1].split("\\.");
								//Bitmap bitmap = BitmapFactory.decodeFile(objPathContent);
								UserFunctions.copyFiles(objPathContent, Globals.TARGET_BASE_FILE_PATH + "UserLibrary/" + formattedDate + ".png");
								UserFunctions.copyFiles(objPathContent, Globals.TARGET_BASE_FILE_PATH + "UserLibrary/." + formattedDate + ".png");
								if(new File(Globals.TARGET_BASE_FILE_PATH + "UserLibrary/" + formattedDate + ".png").exists()){
									Toast.makeText(context, R.string.file_sent, Toast.LENGTH_SHORT).show();
								}
							}else{
								UserFunctions.DisplayAlertDialog(context, R.string.image_alert, R.string.warning);
							}

							popoverView.dissmissPopover(true);
						break;
					case 7:         //Add from FileManager
						//popoverView.dissmissPopover(true);
						addFromFileManager();
						break;  */
					case 6://Bring View to front
						context.page.bringViewToFront(CustomImageRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
						break;

					case 7://Send View to back
						context.page.sendViewToBack(CustomImageRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
						break;

					default:
						break;
					}
				}
			});
		} else if (objType.equals("YouTube")) {
			final PopoverView popoverView = new PopoverView(context, R.layout.list_info_view);
			popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.info_popover_width), (int) context.getResources().getDimension(R.dimen.web_info_height)));
			popoverView.setDelegate(this);
			popoverView.showPopoverFromRectInViewGroup(context.mainDesignView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionAny, true);

			TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
			txtTitle.setText(R.string.video_properties);
			
			final ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
			listView.setAdapter(new ImageInfoListAdapter(context, vidInfoListArray, this));
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long arg3) {
					switch (position) {
					case 0://PickVideofromYoutube
						YoutubePopUpWindow youTubeWindow = new YoutubePopUpWindow(context);
						youTubeWindow.callPopupWindow();
						popoverView.dissmissPopover(true);
						break;

					case 1://Pick Video from Video Library
						takeVideoFromGallery();
						popoverView.dissmissPopover(true);
						break;
						
					case 3://Lock
						if (isObjLocked()) {
							setObjLocked(false);
							vidInfoListArray[3] = R.string.lock;
							listView.invalidateViews();
						} else {
							setObjLocked(true);
							vidInfoListArray[3] = R.string.unlock;
							listView.invalidateViews();
						}
						break;
					/*case 5:
						Bitmap videoThumbnail = ThumbnailUtils.createVideoThumbnail(objPathContent, MediaStore.Video.Thumbnails.MINI_KIND);
						if (!objPathContent.contains("http") && new File(objPathContent).exists() &&videoThumbnail!=null) {
                            Calendar cal = Calendar.getInstance();
							SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
							String formattedDate = df.format(cal.getTime());
                            //String fileName[] = split[split.length - 1].split("\\.");
							//String[] split=objPathContent.split("/");
							UserFunctions.copyFiles(objPathContent, Globals.TARGET_BASE_FILE_PATH + "UserLibrary/"+formattedDate+ ".mp4");
							// UserFunctions.copyFiles(objPathContent, Environment.getExternalStorageDirectory().getAbsolutePath()+"/UserLibrary/."+split[split.length-1]);
							UserFunctions.saveBitmapImage(videoThumbnail, Globals.TARGET_BASE_FILE_PATH + "UserLibrary/."+formattedDate+ ".mp4");
							popoverView.dissmissPopover(true);
						}else{
							UserFunctions.DisplayAlertDialog(context, R.string.video_alert, R.string.warning);
						}
							break;
					case 6:
							//popoverView.dissmissPopover(true);
							addFromFileManager();
							break;  */
					case 5://Bring View to front
						context.page.bringViewToFront(CustomImageRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
						break;

					case 6://Send View to back
						context.page.sendViewToBack(CustomImageRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
						break;

					default:
						break;
					}
				}
			});
		} else if (objType.equals("IFrame")) {
			prevObjPathContent = objPathContent;
			final PopoverView popoverView = new PopoverView(context, R.layout.list_info_view);
			popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.info_popover_width), (int) context.getResources().getDimension(R.dimen.web_info_height)));
			popoverView.setDelegate(this);
			popoverView.showPopoverFromRectInViewGroup(context.mainDesignView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionAny, true);

			TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
			txtTitle.setText(R.string.iframe_properties);
			
			final ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
			listView.setAdapter(new ImageInfoListAdapter(context, iframeInfoListArray, this));
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long arg3) {
					switch (position) {
					case 0: //Browse URL
						if (!UserFunctions.isInternetExist(context)) {
							UserFunctions.DisplayAlertDialogNotFromStringsXML(context, context.getResources().getString(R.string.check_internet_connectivity), context.getResources().getString(R.string.connection_error));
						}
						showBrowseUrlPopUpWindow();
						break;
						
					case 2://Lock
						if (isObjLocked()) {
							setObjLocked(false);
							iframeInfoListArray[2] = R.string.lock;
							listView.invalidateViews();
						} else {
							setObjLocked(true);
							iframeInfoListArray[2] = R.string.unlock;
							listView.invalidateViews();
						}
						break;

					case 4: //Bring View to front
						context.page.bringViewToFront(CustomImageRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
						break;
						
					case 5: //Send View to back
						context.page.sendViewToBack(CustomImageRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
						break;

					default:
						break;
					}
				}
			});
		} else if (objType.equals("EmbedCode")) {
			prevObjPathContent = objPathContent;
			final PopoverView popoverView = new PopoverView(context, R.layout.list_info_view);
			popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.info_popover_width), (int) context.getResources().getDimension(R.dimen.web_info_height)));
			popoverView.setDelegate(this);
			popoverView.showPopoverFromRectInViewGroup(context.mainDesignView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionAny, true);

			TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
			txtTitle.setText(R.string.embed_properties);
			
			final ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
			listView.setAdapter(new ImageInfoListAdapter(context, embedInfoListArray, this));
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long arg3) {
					switch (position) {
					case 0://Embed Code
						showEmbedCodePopUpWindow();
						break;
						
					case 2://Lock
						if (isObjLocked()) {
							setObjLocked(false);
							embedInfoListArray[2] = R.string.lock;
							listView.invalidateViews();
						} else {
							setObjLocked(true);
							embedInfoListArray[2] = R.string.unlock;
							listView.invalidateViews();
						}
						break;
						
					case 4://Bring View to front
						context.page.bringViewToFront(CustomImageRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
						break;
							
					case 5://Send View to back
						context.page.sendViewToBack(CustomImageRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
						break;

					default:
						break;
					}
				}
			});
		} else if (objType.equals("Audio")) {
			final PopoverView popoverView = new PopoverView(context, R.layout.list_info_view);
			popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.info_popover_width), (int) context.getResources().getDimension(R.dimen.web_info_height)));
			popoverView.setDelegate(CustomImageRelativeLayout.this);
			popoverView.showPopoverFromRectInViewGroup(context.mainDesignView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionAny, true);
			
			TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
			txtTitle.setText(R.string.audio_properties);
			
			final ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
			listView.setAdapter(new ImageInfoListAdapter(context, audioInfoListArray, this));
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					switch (position) {
					case 0:
						if (mPlayer == null) { //Initialize Media Player
							mPlayer = new MediaPlayer();
							mPlayer.setOnCompletionListener(new OnCompletionListener() {
								
								@Override
								public void onCompletion(MediaPlayer mp) {
									audioInfoListArray[0] = R.string.play;
									listView.invalidateViews();
								}
							});
							try {
								FileInputStream fileInputStream = new FileInputStream(objPathContent);
								mPlayer.setDataSource(fileInputStream.getFD());
								mPlayer.prepare();
							} catch (IllegalArgumentException e) {
								e.printStackTrace();
							} catch (SecurityException e) {
								e.printStackTrace();
							} catch (IllegalStateException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
							
							mPlayer.setOnErrorListener(new OnErrorListener() {
								
								@Override
								public boolean onError(MediaPlayer mp, int what, int extra) {
									//System.out.println("Error report:"+what);
									return false;
								}
							});
						}
						if (!mPlayer.isPlaying()) {
							mPlayer.start();
							audioInfoListArray[0] = R.string.pause;
							listView.invalidateViews();
						} else {
							mPlayer.pause();
							audioInfoListArray[0] = R.string.play;
							listView.invalidateViews();
						}
						break;
						
					case 1:
						if (mPlayer != null) {
							mPlayer.release();
							mPlayer = null;
							audioInfoListArray[0] = R.string.play;
							listView.invalidateViews();
						}
						break;
						
					case 3://Lock
						if (isObjLocked()) {
							setObjLocked(false);
							audioInfoListArray[3] = R.string.lock;
							listView.invalidateViews();
						} else {
							setObjLocked(true);
							audioInfoListArray[3] = R.string.unlock;
							listView.invalidateViews();
						}
						break;

					/*case 5:
						Calendar cal=Calendar.getInstance();
						SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
						String formattedDate = df.format(cal.getTime());
					    //String fileName[]=split[split.length-1].split("\\.");
						//String[] split=objPathContent.split("/");
						UserFunctions.copyFiles(objPathContent, Globals.TARGET_BASE_FILE_PATH+"UserLibrary/"+formattedDate+ ".mp3");
						popoverView.dissmissPopover(true);
							break;
					case 6:
							//popoverView.dissmissPopover(true);
							addFromFileManager();
							break;  */
					case 5: //Bring View to front
						context.page.bringViewToFront(CustomImageRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
						break;
						
					case 6: //Send View to back
						context.page.sendViewToBack(CustomImageRelativeLayout.this, 0);
						popoverView.dissmissPopover(false);
						break;

					default:
						break;
					}
				}
				
			});
		}
	}
	
	@Override
	public void popoverViewWillShow(PopoverView view) {
		
	}

	@Override
	public void popoverViewDidShow(PopoverView view) {
		
	}

	@Override
	public void popoverViewWillDismiss(PopoverView view) {
		if (context.currentBook.is_bStoreBook() && !context.currentBook.isStoreBookCreatedFromTab()) {
			context.designScrollPageView.setEnableScrolling(true);
		}
		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
			audioInfoListArray[0] = R.string.play;
		}
	}

	@Override
	public void popoverViewDidDismiss(PopoverView view) {
		if (prevObjLocked != objLocked) {
			UndoRedoObjectData prevURObjData = new UndoRedoObjectData();
			prevURObjData.setuRObjDataUniqueId(getObjectUniqueId());
			prevURObjData.setuRObjDataLocked(prevObjLocked);
			context.page.createAndRegisterUndoRedoObjectData(prevURObjData, Globals.OBJECT_LOCKED, Globals.OBJECT_PREVIOUS_STATE);
			
			UndoRedoObjectData currURObjData = new UndoRedoObjectData();
			currURObjData.setuRObjDataUniqueId(getObjectUniqueId());
			currURObjData.setuRObjDataLocked(objLocked);
			context.page.createAndRegisterUndoRedoObjectData(currURObjData, Globals.OBJECT_LOCKED, Globals.OBJECT_PRESENT_STATE);
		}
		if (prevObjSequentialId != objSequentialId) {
			UndoRedoObjectData prevURObjData = new UndoRedoObjectData();
			prevURObjData.setuRObjDataUniqueId(getObjectUniqueId());
			prevURObjData.setuRObjDataSequentialId(prevObjSequentialId);
			context.page.createAndRegisterUndoRedoObjectData(prevURObjData, Globals.OBJECT_SEQUENTIAL_ID_CHANGED, Globals.OBJECT_PREVIOUS_STATE);
			
			UndoRedoObjectData currURObjData = new UndoRedoObjectData();
			currURObjData.setuRObjDataUniqueId(getObjectUniqueId());
			currURObjData.setuRObjDataSequentialId(objSequentialId);
			context.page.createAndRegisterUndoRedoObjectData(currURObjData, Globals.OBJECT_SEQUENTIAL_ID_CHANGED, Globals.OBJECT_PRESENT_STATE);
		}
		if (objType.equals("IFrame") || objType.equals("EmbedCode")) {
			if (prevObjPathContent != null && !prevObjPathContent.equals(objPathContent)) {
				UndoRedoObjectData prevURObjData = new UndoRedoObjectData();
				prevURObjData.setuRObjDataUniqueId(getObjectUniqueId());
				prevURObjData.setuRObjDataType(objType);
				prevURObjData.setuRObjDataContent(prevObjPathContent);
				context.page.createAndRegisterUndoRedoObjectData(prevURObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PREVIOUS_STATE);
				
				UndoRedoObjectData currURObjData = new UndoRedoObjectData();
				currURObjData.setuRObjDataUniqueId(getObjectUniqueId());
				currURObjData.setuRObjDataType(objType);
				currURObjData.setuRObjDataContent(objPathContent);
				context.page.createAndRegisterUndoRedoObjectData(currURObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PRESENT_STATE);
			}
		}
	}
	
	/**
	 * Show Embed Dialog to insert HTML text
	 */
	private void showEmbedCodePopUpWindow(){
		prevObjPathContent = objPathContent;
		Dialog embedCodeDialog = new Dialog(context);
		embedCodeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
		embedCodeDialog.setTitle("Embed Code");
		embedCodeDialog.setContentView(context.getLayoutInflater().inflate(R.layout.embed_code, null));
		embedCodeDialog.getWindow().setLayout((int) (context.global.getDeviceWidth() / 2), (int) (context.global.getDeviceHeight() / 2));

		final WebView webView = (WebView) embedCodeDialog.findViewById(R.id.webView1);
		webView.getSettings().setJavaScriptEnabled(true);
		final EditText editText = (EditText) embedCodeDialog.findViewById(R.id.editText1);
		editText.setText(this.objPathContent);
		editText.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					objPathContent = editText.getText().toString();
					String embedObjContent = "<!DOCTYPE html><body>" + editText.getText().toString() + "</body></html>";
					webView.loadData(embedObjContent, "text/html", "utf-8");
					return true;
				}
				return false;
			}
		});
		
		String embedObjContent = "<!DOCTYPE html><body>"+editText.getText().toString()+"</body></html>";
		webView.loadData(embedObjContent, "text/html", "utf-8");
		embedCodeDialog.show();
	}
	
	/**
	 * Browser View Popup window of IFrame
	 */
	private void showBrowseUrlPopUpWindow(){
		prevObjPathContent = objPathContent;
		Dialog iframeBrowseUrlDialog = new Dialog(context);
		iframeBrowseUrlDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.semi_transparent)));
		iframeBrowseUrlDialog.setTitle("Browse URL");
		iframeBrowseUrlDialog.setContentView(context.getLayoutInflater().inflate(R.layout.iframe_browseurl, null));
		iframeBrowseUrlDialog.getWindow().setLayout((int) (context.global.getDeviceWidth() / 1.5), (int) (context.global.getDeviceHeight() / 1.5));

		final ProgressBar progressBar = (ProgressBar) iframeBrowseUrlDialog.findViewById(R.id.progressBar1);
		progressBar.setVisibility(View.VISIBLE);
		final WebView webView = (WebView) iframeBrowseUrlDialog.findViewById(R.id.webView1);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		final EditText editText = (EditText) iframeBrowseUrlDialog.findViewById(R.id.editText1);
		editText.setText(this.objPathContent);
		editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					progressBar.setVisibility(View.VISIBLE);
					objPathContent = editText.getText().toString();
					webView.loadUrl(editText.getText().toString());
				}
				return false;
			}
		});

		webView.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageFinished(WebView view, String url) {
				progressBar.setVisibility(View.GONE);
			}
		});

		Button btnSave = (Button) iframeBrowseUrlDialog.findViewById(R.id.btnSave);
		btnSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				progressBar.setVisibility(View.VISIBLE);
				objPathContent = editText.getText().toString();
				webView.loadUrl(editText.getText().toString());
			}
		});
		webView.loadUrl(editText.getText().toString());
		iframeBrowseUrlDialog.show();
	}

	/**
	 * @author Take photo from gallery
	 * 
	 */
	public void takePhotoFromGallery(){
		Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		galleryIntent.setType("image/*");
		context.startActivityForResult(galleryIntent, CAPTURE_GALLERY);
	}

	/**
	 * @author Take photo from camera
	 * 
	 */
	public void callCameraToTakePhoto(){

		//Checking camera availability
		if(!isDeviceSupportCamera()){
			Toast.makeText(context, "Sorry! Your device doesn't support camera", Toast.LENGTH_LONG).show();
		}else{

			Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			//System.out.println("The image path: "+this.objPathContent);
			imageFileUri = Uri.fromFile(getOutputMediaFile(MEDIA_TYPE_IMAGE));
			intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);
			context.startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
		}
	}

	public void callCameraToTakeVideo() {
		if (!isDeviceSupportCamera()) {
			Toast.makeText(context, "Sorry! Your device doesn't support camera", Toast.LENGTH_LONG).show();
		} else {
			Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
			context.startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
		}
	}

	/**
	 * @author Take video from gallery
	 * 
	 */
	public void takeVideoFromGallery(){

		Intent videoGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
		videoGalleryIntent.setType("video/*");
		context.startActivityForResult(videoGalleryIntent, VIDEO_GALLERY);
	}

	/**
	 *  Checks whether the device supports camera or not.
	 */
	private boolean isDeviceSupportCamera(){
		if(context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
			return true;
		}else{
			return false;
		}
	}

	/**
	 *  Creates path to save camera image
	 */
	private File getOutputMediaFile(int type){

		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "SboookAuthor");
		if (! mediaStorageDir.exists()){
			if (! mediaStorageDir.mkdirs()){
				Log.d("CamCamera", "failed to create directory");
				return null; 
			}
		}
		// Create a media file name
		String ImageName =""+objUniqueId;
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE){
			mediaFile = new File(mediaStorageDir.getPath() + File.separator +"image_"+ ImageName + ".jpg");
		} else if(type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator +"VID_"+ ImageName + ".mp4");
		}
		else {
			return null;
		}
		return mediaFile;
	}

	/**
	 * @author Set image Background
	 * 
	 */
	public void setImageBackground(String imagePath){
		if (imagePath == null) {
			return;
		}
		int targetW = mImageWidth;
		int targetH = mImageHeight;

		//Get the dimensions of the bitmap :
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(imagePath, options);
		int photoW = options.outWidth;
		int photoH = options.outHeight;

		//Determine the scale down of the image :
		if(targetW>0 && targetH>0){		 //Downsizing the image as it throws outofmemoryexception
			int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
			options.inJustDecodeBounds = false;
			options.inSampleSize = scaleFactor;
			options.inPurgeable = true;
		}
		final Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
		File file = new File(imagePath);
		if(file.exists()){
			try {
				ExifInterface exi = new ExifInterface(imagePath);
				int orientation = exi.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

				if(orientation ==3){
					imgView.setImageBitmap(rotateImage(bitmap, 180));
				}else if(orientation == 6){
					imgView.setImageBitmap(rotateImage(bitmap, 90));
				}else if(orientation == 8){
					imgView.setImageBitmap(rotateImage(bitmap, 270));
				}else{
					imgView.setImageBitmap(bitmap);
				}
			} catch (IOException e) {
				//System.out.println("Error in set image background "+e);
				e.printStackTrace();
			}
			//SaveUriFile(imagePath);
		}
	}
	
	/**
	 * setUndo and redo for image content changed
	 * @param currentPicPath
	 * @param bitmapImage
	 */
	public void setUndoRedoForImageContentChanged(String currentPicPath, Bitmap bitmapImage) {
		if (objType.equals("Image")) {
			imageContentChangeCount ++;
			String fromPath = objPathContent;
			String toPrevPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/tempFiles/"+objUniqueId+"_"+imageContentChangeCount+".png";
			UserFunctions.copyFiles(fromPath, toPrevPath);
			UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
			uRPrevObjData.setuRObjDataUniqueId(objUniqueId);
			uRPrevObjData.setuRObjDataType(objType);
			uRPrevObjData.setuRObjDataContent(toPrevPath);
			context.page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PREVIOUS_STATE);
			
			imageContentChangeCount ++;
			String toCurrPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/tempFiles/"+objUniqueId+"_"+imageContentChangeCount+".png";
			UserFunctions.copyFiles(currentPicPath, toCurrPath);
			UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
			uRCurrObjData.setuRObjDataUniqueId(objUniqueId);
			uRCurrObjData.setuRObjDataType(objType);
			uRCurrObjData.setuRObjDataContent(toCurrPath);
			context.page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PRESENT_STATE);
		} else if (objType.equals("YouTube")) {
			imageContentChangeCount ++;
			String fromPath = objPathContent;
			String toPrevPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/tempFiles/"+objUniqueId+"_"+imageContentChangeCount+".mp4";
			UserFunctions.copyFiles(fromPath, toPrevPath);
			String fromPrevImgThumbPath = getPathByUniqueId();
			String toPrevImgThumbPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/tempFiles/"+objUniqueId+"_"+imageContentChangeCount+".png";
			UserFunctions.copyFiles(fromPrevImgThumbPath, toPrevImgThumbPath);
			UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
			uRPrevObjData.setuRObjDataUniqueId(objUniqueId);
			uRPrevObjData.setuRObjDataType(objType);
			uRPrevObjData.setuRObjDataContent(toPrevPath);
			uRPrevObjData.setuRObjDataVideoThumbPath(toPrevImgThumbPath);
			context.page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PREVIOUS_STATE);
			
			imageContentChangeCount ++;
			String toCurrPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/tempFiles/"+objUniqueId+"_"+imageContentChangeCount+".mp4";
			UserFunctions.copyFiles(currentPicPath, toCurrPath);
			Bitmap currThumbImage = bitmapImage;
			String toCurrImgThumbPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/tempFiles/"+objUniqueId+"_"+imageContentChangeCount+".png";
			copyThumbnnailImageForVideo(currThumbImage, toCurrImgThumbPath);
			UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
			uRCurrObjData.setuRObjDataUniqueId(objUniqueId);
			uRCurrObjData.setuRObjDataType(objType);
			uRCurrObjData.setuRObjDataContent(toCurrPath);
			uRCurrObjData.setuRObjDataVideoThumbPath(toCurrImgThumbPath);
			context.page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PRESENT_STATE);
		}
	}
	
	/**
	 * Sets undo and redo for drawn image
	 * @param undoRedoState
	 */
	public void setUndoPreviousDataForDrawnImage(String undoRedoState){
		imageContentChangeCount ++;
		String fromPath = objPathContent;
		String toPrevPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/tempFiles/"+objUniqueId+"_"+imageContentChangeCount+".png";
		UserFunctions.copyFiles(fromPath, toPrevPath);
		UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
		uRPrevObjData.setuRObjDataUniqueId(objUniqueId);
		uRPrevObjData.setuRObjDataType(objType);
		uRPrevObjData.setuRObjDataContent(toPrevPath);
		if (undoRedoState.equals("previous")) {
			context.page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PREVIOUS_STATE);
		} else {
			context.page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PRESENT_STATE);
		}
	}
	
	/**
	 * this sets undo and redo when youtube url picked
	 * @param currentThumbImage
	 * @param currentContentPath
	 */
	public void setUndoRedoForYoutubeVideo(Bitmap currentThumbImage, String currentContentPath) {
		imageContentChangeCount ++;
		String imgThumbPath = getPathByUniqueId();
		String youtubePath = objPathContent;
		String tempFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/tempFiles/"+objUniqueId+"_"+imageContentChangeCount+".png";
		UserFunctions.copyFiles(imgThumbPath, tempFilePath);
		UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
		uRPrevObjData.setuRObjDataUniqueId(objUniqueId);
		uRPrevObjData.setuRObjDataType(objType);
		uRPrevObjData.setuRObjDataContent(youtubePath);
		uRPrevObjData.setuRObjDataVideoThumbPath(tempFilePath);
		context.page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_CONTENT_URL_EDITED, Globals.OBJECT_PREVIOUS_STATE);
		
		imageContentChangeCount ++;
		String currTempFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/tempFiles/"+objUniqueId+"_"+imageContentChangeCount+".png";
		copyThumbnnailImageForVideo(currentThumbImage, currTempFilePath);
		UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
		uRCurrObjData.setuRObjDataUniqueId(objUniqueId);
		uRCurrObjData.setuRObjDataType(objType);
		uRCurrObjData.setuRObjDataContent(currentContentPath);
		uRCurrObjData.setuRObjDataVideoThumbPath(currTempFilePath);
		context.page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_CONTENT_URL_EDITED, Globals.OBJECT_PRESENT_STATE);
	}
	
	/**
	 *  Set video ThumbNail taken from gallery
	 *
	 */
	public void setVideoThumbnail(String videoPath){

		if (new File(videoPath).exists()) {
			//Bitmap videoThumbnail  = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);
			Bitmap videoBitmap = BitmapFactory.decodeFile(videoPath);
			imgView.setImageBitmap(videoBitmap);
		} else {
			imgView.setImageDrawable(getResources().getDrawable(R.drawable.default_video_bg));
		}
	}

	/**
	 * Method to rotate image
	 */
	private Bitmap rotateImage(Bitmap bitmap, int rotateAngle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(rotateAngle);
		return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
	}

	/**
	 * Replaces the image in the files folder.
	 */
	public void SaveUriFile(String sourceUri){
		String SourceFilePath = sourceUri;
		String defaultImagePath=objPathContent;
		//String DestFilePath = objPathContent;
		String DestFilePath = getPathByUniqueId();
	    BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		try{
			bis = new BufferedInputStream(new FileInputStream(SourceFilePath));
			bos = new BufferedOutputStream(new FileOutputStream(DestFilePath,false));
			byte[] buffer = new byte[1024];
			bis.read(buffer);
			do{
				bos.write(buffer);
			}while(bis.read(buffer)!=-1);
		}catch(IOException e){

		}finally{
			try{
				if(bis!=null) bis.close();
				if(bos!=null) bos.close();
			}catch(IOException e){

			}
		}
		objPathContent=DestFilePath;
		UserFunctions.makeFileWorldReadable(objPathContent);
		if(defaultImagePath.contains("defaultbg")){
			File f=new File(defaultImagePath);
			f.delete();
			String updateQuery = "update objects set content='"+objPathContent+"', locked='"+objLocked+"' where RowID='"+objUniqueId+"'";
			context.db.executeQuery(updateQuery);

		}
	}

	/**
	 * Replaces the image in the files folder.
	 */
	public void SaveUriForVideoFile(String sourceUri){
		String SourceFilePath = sourceUri;
		String DestFilePath = objPathContent;
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		try{
			bis = new BufferedInputStream(new FileInputStream(SourceFilePath));
			bos = new BufferedOutputStream(new FileOutputStream(DestFilePath,false));
			byte[] buffer = new byte[1024];
			bis.read(buffer);
			do{
				bos.write(buffer);
			}while(bis.read(buffer)!=-1);
		}catch(IOException e){

		}finally{
			try{
				if(bis!=null) bis.close();
				if(bos!=null) bos.close();
			}catch(IOException e){

			}
		}
		UserFunctions.makeFileWorldReadable(objPathContent);
	}
	
	/**
	 *  copying Thumbnail images for videos from video gallery and youtube.
	 */
	public void copyThumbnnailImageForVideo(Bitmap youtubeBitmap){
		
		FileOutputStream fos = null;
		String videoImagePath = getPathByUniqueId();
		try {
			fos = new FileOutputStream(videoImagePath);
			youtubeBitmap.compress(CompressFormat.PNG, 90, fos);
			fos.flush();
			fos.close();
		setVideoThumbnail(videoImagePath);
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void copyThumbnnailImageForVideo(Bitmap youtubeBitmap, String toPath){
		FileOutputStream fos = null;
		String videoImagePath = toPath;
		try {
			fos = new FileOutputStream(videoImagePath);
			youtubeBitmap.compress(CompressFormat.PNG, 90, fos);
			fos.flush();
			fos.close();
			setVideoThumbnail(videoImagePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void addFromFileManager(){
		final PopoverView popoverView = new PopoverView(context, R.layout.files_manager);
		popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.info_popover_width), (int) context.getResources().getDimension(R.dimen.web_info_popover_height)));
		popoverView.setDelegate(this);
		popoverView.showPopoverFromRectInViewGroup(context.mainDesignView, PopoverView.getFrameForView(btnInfo), PopoverView.PopoverArrowDirectionAny, true);
		final ListView listView = (ListView) popoverView.findViewById(R.id.listView1);
        final Button btn_back=(Button)popoverView.findViewById(R.id.btnBack);
		currentFilePath = Globals.TARGET_BASE_FILE_PATH+"UserLibrary";
		btn_back.setText("UserLibrary");
		listingFilesAndFolders(currentFilePath);
		listView.setAdapter(new FileImageAdapter(context, selectedFiles, objType));
		//listView.setAdapter(new FileManagerAdapter(context, currentFilePath, objType));
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
									int position, long arg3) {
				ClickedFilePath listedFiles=selectedFiles.get(position);
				if(listedFiles.isFolderSelected() &&!listedFiles.isFile()){
					//String[]split=listedFiles.getFolderPath().split("/");
					currentFilePath=listedFiles.getFolderPath();
					btn_back.setText(listedFiles.getFolderTitle());
					listingFilesAndFolders(currentFilePath);
					listView.setAdapter(new FileImageAdapter(context, selectedFiles, objType));

				}else if(listedFiles.isFile() &&!listedFiles.isFolderSelected()){
					if(objType.equals("Image")){
						setImageBackground(listedFiles.getFolderPath());
						SaveUriFile(listedFiles.getFolderPath());
					}else if(objType.equals("YouTube")){
						//Bitmap b=MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(),listedFiles.getFolderPath(), MediaStore.Images.Thumbnails.MINI_KIND);
						Bitmap videoThumbnail  = ThumbnailUtils.createVideoThumbnail(listedFiles.getFolderPath(), MediaStore.Video.Thumbnails.MINI_KIND);
						//Bitmap bitmap = BitmapFactory.decodeFile(listedFiles.getFolderPath());
						if(videoThumbnail!=null) {
							copyThumbnnailImageForVideo(videoThumbnail);
							SaveUriForVideoFile(listedFiles.getFolderPath());
						}else{
							String split[]=listedFiles.getFolderPath().split("/");
							String FileName=listedFiles.getFolderPath().replace(split[split.length - 1], "." + split[split.length - 1]);
							Bitmap bitmap = BitmapFactory.decodeFile(FileName);
							copyThumbnnailImageForVideo(bitmap);
							SaveUriForVideoFile(listedFiles.getFolderPath());
						}
					}else if(objType.equals("Audio")){
						SaveUriFile(listedFiles.getFolderPath());
					}

					popoverView.dissmissPopover(true);
				}
				if(!btn_back.getText().toString().equals("UserLibrary")) {
					btn_back.setCompoundDrawablesWithIntrinsicBounds(R.drawable.arrownext,0,0, 0);
				}else{
					btn_back.setCompoundDrawablesWithIntrinsicBounds(0,0,0, 0);
				}

			}
		});
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!currentFilePath.equals( Globals.TARGET_BASE_FILE_PATH+"UserLibrary")) {
					String filePath = currentFilePath.replace("/" + btn_back.getText(), "");
					currentFilePath = filePath;
					String[] split = currentFilePath.split("/");
					btn_back.setText(split[split.length - 1]);
					listingFilesAndFolders(currentFilePath);
					listView.setAdapter(new FileImageAdapter(context, selectedFiles, objType));
					if(btn_back.getText().toString().equals("UserLibrary")) {
						btn_back.setCompoundDrawablesWithIntrinsicBounds(0,0,0, 0);
					}
				}
			}
		});

	}

	private void  listingFilesAndFolders(String filePath){
		File f=new File(filePath);
		File  listFiles[]=f.listFiles();
		//selectedFiles = new ArrayList<ClickedFilePath>();
		if(selectedFiles.size()>0){
			selectedFiles.clear();
		}
		ClickedFilePath file;
		for(int i=0;i<listFiles.length;i++) {
			String fileName=listFiles[i].getName();
			String Path=listFiles[i].getPath();
			if (listFiles[i].isDirectory()){
				file=new ClickedFilePath(context);
				file.setFolderSelected(true);
				file.setFolderPath(Path);
				file.setFile(false);
				file.setFolderTitle(fileName);
				selectedFiles.add(file);
			}else{
               if(!listFiles[i].isHidden()) {
					if (objType.equals("Image")) {
						//if(listFiles[i].isHidden()) {
							if (fileName.contains(".jpg") || fileName.contains(".png") || fileName.contains(".gif")) {
								file = new ClickedFilePath(context);
								file.setFolderSelected(false);
								file.setFolderPath(Path);
								file.setFile(true);
								file.setFolderTitle(fileName);
								selectedFiles.add(file);
							}
						//}
					} else if (objType.equals("YouTube")) {
						//if(listFiles[i].isHidden()) {
							if (fileName.contains(".mp4") || fileName.contains(".3gp")|| fileName.contains(".mkv")|| fileName.contains(".avi")|| fileName.contains(".flv")) {
								file = new ClickedFilePath(context);
								file.setFolderSelected(false);
								file.setFolderPath(Path);
								file.setFile(true);
								file.setFolderTitle(fileName);
								selectedFiles.add(file);
							}
						//}
					} else if (objType.equals("Audio")) {
						if (fileName.contains(".mp3")||fileName.contains(".m4a")) {
							file = new ClickedFilePath(context);
							file.setFolderSelected(false);
							file.setFolderPath(Path);
							file.setFile(true);
							file.setFolderTitle(fileName);
							selectedFiles.add(file);
						}
					}
				}
			}
		}

	}
	/**
	 *  Update youtube selected link in the DB.
	 */
	
	/*public void updateYoutubeLinkDB(String videoLink){
		this.objPathContent = videoLink;
		//String updateQuery = "update objects set content = '"+videoLink+"' where RowID ='"+objUniqueId+"'";
		//context.db.executeQuery(updateQuery);
	}*/

}
