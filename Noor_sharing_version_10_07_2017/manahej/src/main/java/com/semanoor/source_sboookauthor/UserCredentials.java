package com.semanoor.source_sboookauthor;

import java.io.Serializable;

public class UserCredentials implements Serializable{
	 private String userScopeId;
	 private String userName;
	 private int scopeType;
	 private String userEmailId;
	
	 public UserCredentials(String userScopeId, String userName, int scopeType, String userEmailId){
		 this.setUserScopeId(userScopeId);
		 this.setUserName(userName);
		 this.setScopeType(scopeType);
		 this.setUserEmailId(userEmailId);
	 }

	/**
	 * @return the userId
	 */
	public String getUserScopeId() {
		return userScopeId;
	}

	/**
	 * @param userScopeId the userId to set
	 */
	public void setUserScopeId(String userScopeId) {
		this.userScopeId = userScopeId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the scopeType
	 */
	public int getScopeType() {
		return scopeType;
	}

	/**
	 * @param scopeType the scopeType to set
	 */
	public void setScopeType(int scopeType) {
		this.scopeType = scopeType;
	}

	public String getUserEmailId() {
		return userEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}
}
