package com.semanoor.source_sboookauthor;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Krishna on 06-10-2016.
 */
public class MyGroups {
    private Context context;
    private int id;
    private String groupName;
    private String loggedInUserId;
    private int Enabled;
    private boolean groupSelected;
    public ArrayList<MyContacts> groupsContactList= new ArrayList<MyContacts>();

    public MyGroups(Context _context) {
        this.context = _context;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public int getEnabled() {
        return Enabled;
    }

    public void setEnabled(int enabled) {
        Enabled = enabled;
    }

    public boolean isGroupSelected() {
        return groupSelected;
    }

    public void setGroupSelected(boolean groupSelected) {
        this.groupSelected = groupSelected;
    }

    public ArrayList<MyContacts> getGroupsContactList() {
        return groupsContactList;
    }

    public void setGroupsContactList(ArrayList<MyContacts> groupsContactList) {
        this.groupsContactList = groupsContactList;
    }
}
