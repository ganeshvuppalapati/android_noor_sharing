package com.semanoor.source_sboookauthor;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.semanoor.manahij.R;

import java.util.ArrayList;

public class TemplateGridListAdapter extends BaseAdapter {
	
	private Activity context;
	private ArrayList<Templates> templatesListArray;
	boolean displayAddButton;

	public TemplateGridListAdapter(Activity mainActivity,
			ArrayList<Templates> _templatesListArray,boolean AddButton) {
		this.context = mainActivity;
		this.templatesListArray = _templatesListArray;
		this.displayAddButton=AddButton;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(displayAddButton) {
			return templatesListArray.size() + 1;
		}else{
			return templatesListArray.size();
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return templatesListArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (view == null) {
			view = context.getLayoutInflater().inflate(R.layout.inflate_book_template, null);
		}
		String freeFilesPath = Globals.TARGET_BASE_TEMPATES_PATH;
		//ImageView bookImage = (ImageView) view.findViewById(R.id.imageView1);
		ImageView bookImg = (ImageView) view.findViewById(R.id.imageView2);
	
		if(position==templatesListArray.size() &&displayAddButton){
			//Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.btn_add_page);//decodeFile(context.getResources().getDrawable(R.drawable.btn_add_page));
			bookImg.setImageDrawable(context.getResources().getDrawable(R.drawable.add_template));
			//bookImg.setImageBitmap(bitmap);
			//bookImage.setVisibility(View.INVISIBLE);
		}else{
			Templates template = templatesListArray.get(position);
			view.setId(template.getTemplateId());
			//bookImage.setVisibility(View.VISIBLE);
			//Bitmap bitmap = UserFunctions.getBitmapFromAsset(context, "templates/"+template.getTemplateId()+"/card.png");
			Bitmap bitmap = BitmapFactory.decodeFile(freeFilesPath+template.getTemplateId()+"/card.png");
			bookImg.setImageBitmap(bitmap);
		}
		return view;
	}

}
