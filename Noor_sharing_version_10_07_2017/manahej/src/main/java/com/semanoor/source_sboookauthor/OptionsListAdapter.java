package com.semanoor.source_sboookauthor;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;

public class OptionsListAdapter extends BaseAdapter {

	private BookViewActivity context;
	private Integer[] arrayValues;

	public OptionsListAdapter(BookViewActivity _context, Integer[] _arrayValues) {
		this.context = _context;
		this.arrayValues = _arrayValues;
	}

	@Override
	public int getCount() {
		return arrayValues.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = context.getLayoutInflater().inflate(R.layout.inflate_options, null);
		}
		TextView tv = (TextView) view.findViewById(R.id.textView1);
		tv.setText(arrayValues[position]);
		return view;
	}

}
