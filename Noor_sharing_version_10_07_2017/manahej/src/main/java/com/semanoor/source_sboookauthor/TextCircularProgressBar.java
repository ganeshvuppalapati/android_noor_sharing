package com.semanoor.source_sboookauthor;

import com.semanoor.source_sboookauthor.CircularProgressBar.OnProgressChangeListener;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by karthik on 04-05-2016.
 */

public class TextCircularProgressBar extends FrameLayout implements OnProgressChangeListener {

    private CircularProgressBar mCircularProgressBar;
    private TextView mRateText;
    public TextCircularProgressBar(Context context) {
        super(context);
        init();
    }
    public TextCircularProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mCircularProgressBar = new CircularProgressBar(getContext());
        this.addView(mCircularProgressBar);
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        lp.gravity = Gravity.CENTER;
        mCircularProgressBar.setLayoutParams(lp);
        mRateText = new TextView(getContext());
        this.addView(mRateText);
        mRateText.setLayoutParams(lp);
        mRateText.setText("0%");
        mRateText.setTypeface(Typeface.DEFAULT_BOLD);
        mRateText.setGravity(Gravity.CENTER);
        mRateText.setTextColor(Color.BLACK);
        mRateText.setTextSize(20);
        mCircularProgressBar.setOnProgressChangeListener(this);
    }

    public void setMax( int max ) {
        mCircularProgressBar.setMax(max);
    }

    public void setProgress(int progress) {
        mCircularProgressBar.setProgress(progress);
    }

    public CircularProgressBar getCircularProgressBar() {
        return mCircularProgressBar;
    }

    public void setTextSize(float size) {
        mRateText.setTextSize(size);
    }

    public void setTextColor( int color) {
        mRateText.setTextColor(color);
    }

    @Override
    public void onChange(int duration, int progress, float rate) {
        mRateText.setText(String.valueOf( (int)(rate * 100 ) + "%"));
    }
}