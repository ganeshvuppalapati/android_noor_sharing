package com.semanoor.source_sboookauthor;

public class UndoRedoObjectAction {
	
	private String ruObjActionName;
	private UndoRedoObjectData ruObject;
	private String ruObjState;
	
	/**
	 * @return the ruObjActionName
	 */
	public String getRuObjActionName() {
		return ruObjActionName;
	}
	/**
	 * @param ruObjActionName the ruObjActionName to set
	 */
	public void setRuObjActionName(String ruObjActionName) {
		this.ruObjActionName = ruObjActionName;
	}
	/**
	 * @return the ruObject
	 */
	public UndoRedoObjectData getRuObject() {
		return ruObject;
	}
	/**
	 * @param ruObject the ruObject to set
	 */
	public void setRuObject(UndoRedoObjectData uRObjData) {
		this.ruObject = uRObjData;
	}
	/**
	 * @return the state
	 */
	public String getRuObjState() {
		return ruObjState;
	}
	/**
	 * @param state the state to set
	 */
	public void setRuObjState(String ruObjState) {
		this.ruObjState = ruObjState;
	}
	
}
