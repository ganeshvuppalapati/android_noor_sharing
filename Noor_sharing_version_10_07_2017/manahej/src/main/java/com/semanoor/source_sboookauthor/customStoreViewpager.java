package com.semanoor.source_sboookauthor;


import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by karthik on 05-04-2016.
 */
public class customStoreViewpager extends ViewPager {

    public customStoreViewpager(Context context) {
        super(context);

    }
    public customStoreViewpager(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }
}
