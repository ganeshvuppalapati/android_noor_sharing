package com.semanoor.source_sboookauthor;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfDocument.Page;
import android.graphics.pdf.PdfDocument.PageInfo;
import android.os.AsyncTask;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.semanoor.manahij.MainActivity;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExportToPdf{
	int pageNumber;
	int totalPages;
	WebView Webview;
	private PdfDocument pdfDocument;
	private Book currentBook;
	private MainActivity cxt;
	//private ExportActivity cxt;
	private DatabaseHandler db;
	private int pageCount;
	private String pdfPath;
	//private ExportActivity.createPdfFile createpdfFile;
	public BookExportDialog.createPdfFile createpdfFile;
	RelativeLayout rootView;

	/*public ExportToPdf(ExportActivity _context, Book _currentBook, DatabaseHandler _db, String pdfPath, ExportActivity.createPdfFile createpdfFile) {
		this.cxt = _context;
		this.currentBook = _currentBook;
		this.db = _db;
		this.pageCount = currentBook.getTotalPages();
		this.pdfPath = pdfPath;
		this.createpdfFile = (com.semanoor.manahij.ExportActivity.createPdfFile) createpdfFile;
	}*/
	public ExportToPdf(MainActivity _context, Book _currentBook, DatabaseHandler _db, String pdfPath, RelativeLayout layout) {
		this.cxt = _context;
		this.currentBook = _currentBook;
		this.db = _db;
		this.pageCount = this.currentBook.getTotalPages();
		this.pdfPath = pdfPath;
		this.rootView=layout;
		//this.createpdfFile = (com.semanoor.manahij.MainActivity.createPdfFile) createpdfFile;
	}

	/**
	 * Method initializes here :
	 */
	public void callToGeneratePDF() {

		String htmlFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/";
		for (int i = 0; i < currentBook.getTotalPages(); i++) {
			new GenerateHTML(cxt, db, currentBook, String.valueOf(i+1), null,false).generatePage(htmlFilePath, null);
		}

		initializeVariables();
		createWebview();
	}


	/**
	 * Initializing values
	 */
	private void initializeVariables(){
		pageNumber = 1; 
		pdfDocument = new PdfDocument();
	}

	/**
	 * Creating  webview
	 * 
	 */
	private void createWebview(){
		this.cxt.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Webview = new WebView(cxt);
				RelativeLayout.LayoutParams webLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
				Webview.setLayoutParams(webLayoutParams);
				Webview.setVisibility(View.INVISIBLE);
				rootView.addView(Webview);
				loadWebview(pageNumber);
			}
		});
	}

	/**
	 * Loading Html pages
	 * 
	 */
	private void loadWebview(int loadPage){
		if(Webview != null){
			String fileName="file:///data/data/"+Globals.getCurrentProjPackageName()+"/files/books/"+currentBook.getBookID()+"/"+loadPage+".htm";
			//System.out.println(fileName+"fileName");
			//System.out.println(pageNumber+" inloadwebview");
			//Webview.getSettings().setLoadWithOverviewMode(true);
			//Webview.getSettings().setUseWideViewPort(true);
			Webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			Webview.getSettings().setJavaScriptEnabled(true);
			Webview.setWebViewClient(new WebViewClient());
			Webview.setWebChromeClient(new WebChromeClient());
			Webview.getSettings().setAllowContentAccess(true);
			Webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
			Webview.getSettings().setAllowContentAccess(true);
			Webview.getSettings().setAllowFileAccess(true);
			Webview.clearCache(true);
			Webview.loadUrl(fileName);
			Webview.setWebChromeClient(new WebChromeClient(){
				@Override
				public void onProgressChanged(WebView view, int newProgress) {
					if (newProgress == 100) {
						new Background().execute();
					}
				}
			});
		}
	}
	public class Background extends AsyncTask<Void, Void, Bitmap>
	{
		@Override
		protected Bitmap doInBackground(Void... params)
		{
			try
			{
				Thread.sleep(2000);
				loadPDFContent();
			}
			catch (InterruptedException e){}
			catch (Exception e){}
			return null;
		}
		@Override
		protected void onPostExecute(Bitmap result)
		{
			pageNumber++;
			removeWebview();
			if (pageNumber <= currentBook.getTotalPages()) {
				createWebview();
			} else {
				generatePDF();
				BookExportDialog bookdialog=new BookExportDialog(cxt,currentBook);
				bookdialog.onFinishCreatePdf();
				//createpdfFile.new createPdfFile("", "").onFinishCreatePdf();
			}
		}
	}
	/**
	 * Loading  content into the page
	 * 
	 */

	private void loadPDFContent(){
		//System.out.println(pageNumber+" inloadPDFContent");
		PageInfo pageInfo = new PageInfo.Builder(Webview.getWidth(), Webview.getHeight(), pageNumber).create();
		Page page = pdfDocument.startPage(pageInfo);

		Canvas canvas = page.getCanvas();

		Bitmap bitmap = Bitmap.createBitmap(Webview.getWidth(), Webview.getHeight(), Config.ARGB_8888); //Config.ARGB_8888
		Canvas canvas2 = new Canvas(bitmap);
		Webview.draw(canvas2);

		canvas.drawBitmap(bitmap, 0, 0, null);
		//System.out.println(canvas+"canvas2");
		pdfDocument.finishPage(page);
	}


	/**
	 * Removing  webview
	 * 
	 */
	private void removeWebview(){

		Webview  = null;
		rootView.removeView(Webview);
	}

	/**
	 * Generating Pdf
	 * 
	 */
	private void generatePDF(){
		//String PDFpath= Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/"+currentBook.getBookID()+".pdf";
		try {
			pdfDocument.writeTo(new FileOutputStream(pdfPath));
			pdfDocument.getPages();
			if(pdfDocument.equals("1")){

			}
			String msg = "File saved successfully to "+pdfPath;
        	Toast.makeText(cxt, msg, Toast.LENGTH_LONG).show();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			////System.out.println("Failed generating PDF"+e.getLocalizedMessage());
			String msg = "Failed generating PDF"+e.getLocalizedMessage();
        	Toast.makeText(cxt, msg, Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			e.printStackTrace();
			////System.out.println("Failed IO exception in PDF"+e.getLocalizedMessage());
			String msg = "Failed IO exception in PDF"+e.getLocalizedMessage();
        	Toast.makeText(cxt, msg, Toast.LENGTH_LONG).show();
		}
	}

}

