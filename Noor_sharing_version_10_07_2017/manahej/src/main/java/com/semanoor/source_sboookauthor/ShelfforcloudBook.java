package com.semanoor.source_sboookauthor;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ptg.views.CircleButton;
import com.semanoor.manahij.CloudBooksActivity;
import com.semanoor.manahij.CloudData;
import com.semanoor.manahij.CloudEnrichDetails;
import com.semanoor.manahij.ExportEnrichmentActivity;
import com.semanoor.manahij.ExportMediaAlbumDialog;
import com.semanoor.manahij.GDriveExport;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.PopoverView.PopoverViewDelegate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShelfforcloudBook implements ListAdapter, OnClickListener, PopoverViewDelegate, Serializable {

    private boolean isInEditMode,editmode;
    public Book currentBook;
    private transient EditText etBookTitle;
    private transient EditText etBookAuthor;
    private transient EditText et_cat;
    public Category categoryRack;
    public Category editCategory;
    CloudBooksActivity context;
    public ArrayList<Object> bookListArray;
    ArrayList<CloudData> miniBook,myBooks,newBooks,mindMapBooks,media;
//    public transient UserFunctions userFunction;
//    public ArrayList<Templates> templatesPortraitListArray;
//    public ArrayList<Templates> templatesLandscapeListArray;
    public ArrayList<Category> categoryListArray;
//    public ShelfforcloudBook(MainActivity context, DatabaseHandler databaseHandler, UserFunctions userfn) {
//
//    }
String KEY_CENRICHMENTS = "CENRICHMENT", KEY_CEMESSAGE = "EMESSAGE", KEY_CECONTENT = "ECONTENT", KEY_CLOUDENRSELECTED = "ESELECTED";

    public ShelfforcloudBook(CloudBooksActivity cloudBooksActivity, ArrayList<Category> category, ArrayList<CloudData> minibook, ArrayList<CloudData> mybooks, ArrayList<CloudData> newbooks, ArrayList<CloudData> mindmap, ArrayList<CloudData> albumBooks) {
        context=cloudBooksActivity;
        categoryListArray=category;
        miniBook=minibook;
        myBooks=mybooks;
        newBooks=newbooks;
        mindMapBooks=mindmap;
        media=albumBooks;
    }

    @Override
    public int getCount() {
        //int bookCount = getNoOfBooks();
        return categoryListArray.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


   public static class ViewHolder {
        public TextView txtTitle;
        public Button btnPropertieCategory;
        public Button btnPropertyAccord;
        public ImageView btnArrangeCategory;
        //public HorizontalScrollView scrollView;
        //public LinearLayout linearScrollLayout;
        public RecyclerView categoryGridView;
        public ImageView bgRackImageView;
        public int position;
        public FrameLayout btnFramePropCategory;
        public FrameLayout btnFramePropAccord;
        public RelativeLayout drag_layout;
        public RelativeLayout shelf_layout;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        final Category category = categoryListArray.get(position);

        if (view == null) {
            view = context.getLayoutInflater().inflate(R.layout.cloud_shelf, null);
            Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
            UserFunctions.changeFont(parent,font);
            holder = new ViewHolder();
            holder.btnFramePropAccord = (FrameLayout) view.findViewById(R.id.frameButton1);
            holder.btnFramePropCategory = (FrameLayout) view.findViewById(R.id.frameButton2);
            holder.shelf_layout = (RelativeLayout) view.findViewById(R.id.shelf_layout);
            holder.drag_layout = (RelativeLayout) view.findViewById(R.id.drag_handle);
            holder.txtTitle = (TextView) view.findViewById(R.id.text);
            holder.btnPropertieCategory = (Button) view.findViewById(R.id.button2);
            holder.btnPropertyAccord = (Button) view.findViewById(R.id.button1);
            holder.btnArrangeCategory = (ImageView) view.findViewById(R.id.drag);
            holder.categoryGridView = (RecyclerView) view.findViewById(R.id.gridView1);
            HorizontalAdapter horizontalAdapter=new HorizontalAdapter(category,holder.categoryGridView);
            holder.categoryGridView.setAdapter(horizontalAdapter);
            holder.bgRackImageView = (ImageView) view.findViewById(R.id.bg_rack_image);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        setTexttoCategoryName(category, holder.txtTitle);
        holder.btnPropertyAccord.setId(position);
        holder.btnPropertieCategory.setId(position);
        holder.btnFramePropCategory.setId(position);
        holder.btnFramePropAccord.setId(position);
        holder.position = position;

       if (position==categoryListArray.size()-1){
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.shelf_layout.getLayoutParams();
            layoutParams.setMargins(0,0,0,0);
        }

        holder.btnPropertieCategory.setVisibility(View.VISIBLE);
        holder.btnPropertyAccord.setVisibility(View.VISIBLE);
        holder.btnFramePropAccord.setVisibility(View.VISIBLE);
        holder.bgRackImageView.setVisibility(View.VISIBLE);
        holder.bgRackImageView.setImageBitmap(null);
        holder.btnFramePropCategory.setVisibility(View.GONE);
        context.gridView.setDragEnabled(false);
        holder.btnPropertyAccord.setEnabled(true);
        holder.btnFramePropAccord.setEnabled(true);
        holder.txtTitle.setFocusable(true);

        if (category.isAccordExpanded()) {
            holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_collapsed_new);
            // holder.scrollView.setVisibility(View.GONE);
            holder.categoryGridView.setVisibility(View.VISIBLE);
            LayoutParams gridlayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.categoryGridView.getLayoutParams();
            LayoutParams imglayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.bgRackImageView.getLayoutParams();
            gridlayoutParams.height=(int) context.getResources().getDimension(R.dimen.shelf_height)+(int) context.getResources().getDimension(R.dimen.shelf_height);
            imglayoutParams.height = holder.categoryGridView.getHeight();
            holder.categoryGridView.setLayoutParams(gridlayoutParams);
            holder.categoryGridView.addOnLayoutChangeListener(new OnLayoutChangeListener() {

                @Override
                public void onLayoutChange(View v, int left, int top, int right,
                                           int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    LayoutParams imglayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.bgRackImageView.getLayoutParams();
                    //System.out.println("GridView Height: "+v.getHeight());
                    imglayoutParams.height = v.getHeight();
                    holder.bgRackImageView.setLayoutParams(imglayoutParams);
                }
            });

            GridLayoutManager gridLayout=new GridLayoutManager(context,3,GridLayoutManager.VERTICAL,false);
            //llm.canScrollHorizontally();
            holder.categoryGridView.setLayoutManager(gridLayout);
            holder.categoryGridView.setHasFixedSize(true);
            // holder.categoryGridView.setAdapter(horizontalAdapter);
            holder.categoryGridView.invalidate();
        } else {
            holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_expanded_new);
            //holder.scrollView.setVisibility(View.VISIBLE);
            holder.categoryGridView.setVisibility(View.VISIBLE);

            LayoutParams imglayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.bgRackImageView.getLayoutParams();
            LayoutParams scrollLayoutParams = holder.categoryGridView.getLayoutParams();
            imglayoutParams.height = scrollLayoutParams.height;


            LinearLayoutManager horizontalLayoutManagaer= new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            // vertical_recycler_view.setLayoutManager(horizontalLayoutManagaer);
            holder.categoryGridView.setLayoutManager(horizontalLayoutManagaer);
            holder.categoryGridView.setHasFixedSize(true);
            holder.categoryGridView.invalidate();


            new addBooksToScrollView(position, category, holder).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

        }

          holder.txtTitle.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Category category;
                for (int i=0 ; i<categoryListArray.size();i++){
                    category = categoryListArray.get(i);
                    if (category.getCategoryId()==holder.txtTitle.getId()){
                        editCategory=category;
                    }
                }
               return  false;
            }
        });

        holder.btnPropertieCategory.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                propertyCategoryClicked(v, holder);
            }
        });

        holder.btnPropertyAccord.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                propertyAccordClicked(v, holder);
            }
        });

        holder.btnFramePropCategory.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                propertyCategoryClicked(v, holder);
            }
        });

        holder.btnFramePropAccord.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                propertyAccordClicked(v, holder);
            }
        });

        return view;
    }



    private void propertyAccordClicked(View v, ViewHolder holder) {
        int pos = v.getId();
        Category category = categoryListArray.get(pos);
        String storeBookId = null;
        int j = 0;
        ArrayList<CloudData> Books = new ArrayList<CloudData>();
        if(category.getCategoryId()==0){
            Books=myBooks;
        }else if(category.getCategoryId()==1){
            Books=newBooks;
        }else if(category.getCategoryId()==2){
            Books=miniBook;
        }


        for (int i = 0; i < Books.size(); i++) {
            //Book book = (Book) bookListArray.get(i);
            CloudData data=Books.get(i);
            HashMap<String, String> map = data.getMap();
            String jsonContent = map.get(KEY_CECONTENT);
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(jsonContent);
                storeBookId = jsonObject.getString("IDBook");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!checkStorBookstatus(storeBookId)) {
               // if (data.getCategoryId() == category.getCategoryId()) {
                    j = j + 1;
                    if (j == 2) {
                        if (category.isAccordExpanded()) {
                            holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_expanded_new);
                            category.setAccordExpanded(false);
                            context.gridView.invalidateViews();
                        } else {
                            holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_collapsed_new);
                            category.setAccordExpanded(true);
                            context.gridView.invalidateViews();
                        }
                        break;
                    }
                //}
            }
        }
    }
    public boolean checkStorBookstatus(String storeBookID){
        ArrayList<Object> bookList = context.gridShelf.bookListArray;
        for (int i = 0; i<bookList.size(); i++) {
            Book book = (Book) bookList.get(i);
            if (book.is_bStoreBook()) {
                if (book.get_bStoreID().equals(storeBookID)&&!book.getIsDownloadCompleted().equals("downloading")) {
                    return true;
                }
            }
        }
        return false;
    }
    private void propertyCategoryClicked(View v, final ViewHolder holder) {
        int pos = v.getId();
        final Category category = categoryListArray.get(pos);

        if (Globals.isTablet()) {
            if ( category.getCategoryId() == 1 || category.getCategoryId() == 2 || category.getCategoryId() == 3 || category.getCategoryId() == 4 || category.getCategoryId() == 5 || category.getCategoryId() == 6 || category.getCategoryId() == 7|| category.getCategoryId() == 8 ) {
                UserFunctions.DisplayAlertDialog(context, R.string.categories_delete_msg, R.string.delete);
            } else {
                Builder builder = new AlertDialog.Builder(context);
                builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean isCategoryEmpty = true;
                        for (int i = 0; i < bookListArray.size(); i++) {
                            Book book = (Book) bookListArray.get(i);
                            if (category.getCategoryId() == book.getbCategoryId()) {
                                isCategoryEmpty = false;
                                break;
                            }
                        }
                        if (isCategoryEmpty) {
                            categoryListArray.remove(category);
                            context.gridView.invalidateViews();

                            String destRackImgPath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + category.getCategoryId() + ".png";
                            File fileImgDir1 = new File(destRackImgPath);
                            if (fileImgDir1.exists()) {
                                fileImgDir1.delete();
                            }

                        } else {
                            UserFunctions.DisplayAlertDialog(context, R.string.categories_delete_msg1, R.string.delete);
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setTitle(R.string.delete_category);
                builder.setMessage(R.string.delete_sure);
                builder.show();
            }
        }
        else if (!Globals.isTablet()) {
            if (category.getCategoryId() == 1 || category.getCategoryId() == 2 || category.getCategoryId() == 3 || category.getCategoryId() == 4 || category.getCategoryId() == 5 || category.getCategoryId() == 7) {
                UserFunctions.DisplayAlertDialog(context, R.string.categories_delete_msg, R.string.delete);
            } else {
                Builder builder = new AlertDialog.Builder(context);
                builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean isCategoryEmpty = true;
                        for (int i = 0; i < bookListArray.size(); i++) {
                            Book book = (Book) bookListArray.get(i);
                            if (category.getCategoryId() == book.getbCategoryId()) {
                                isCategoryEmpty = false;
                                break;
                            }
                        }
                        if (isCategoryEmpty) {
                            categoryListArray.remove(category);
                            context.gridView.invalidateViews();

                            String destRackImgPath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + category.getCategoryId() + ".png";
                            File fileImgDir1 = new File(destRackImgPath);
                            if (fileImgDir1.exists()) {
                                fileImgDir1.delete();
                            }

                        } else {
                            UserFunctions.DisplayAlertDialog(context, R.string.categories_delete_msg1, R.string.delete);
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setTitle(R.string.delete_category);
                builder.setMessage(R.string.delete_sure);
                builder.show();
            }

        }

    }


    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

        private List<String> horizontalList;
       // private ArrayList<Book> categoryBooksList = new ArrayList<Book>();
        ArrayList<CloudData> Books = new ArrayList<CloudData>();
        private Category category;
        RecyclerView recyclerView;

        View itemView;
        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView txtView;
            ImageView bookImg;
            TextView tv_Title;
            RelativeLayout rl;
           // CircleButton btn_download;
            // CardView rl;
            TextCircularProgressBar progressBar;
            Button imgInfoBtn,imgExportBtn,imgDeleteBtn,btn_text;
            public MyViewHolder(View view) {
                super(view);
                //rl = (CardView)view.findViewById(R.id.relativeLayout2);
                rl = (RelativeLayout) view.findViewById(R.id.shelfbookLayout);
                ViewGroup.MarginLayoutParams margins = (MarginLayoutParams) rl.getLayoutParams();
                margins.setMargins(0, 0, 0, 0);
                bookImg = (ImageView) view.findViewById(R.id.imageView2);
                progressBar = (TextCircularProgressBar) view.findViewById(R.id.circular_progress);
                progressBar.setVisibility(View.INVISIBLE);
                tv_Title = (TextView) view.findViewById(R.id.textView1);
                imgInfoBtn = (Button) view.findViewById(R.id.imageButton1);
                imgExportBtn = (Button) view.findViewById(R.id.imageButton2);
                imgDeleteBtn = (Button) view.findViewById(R.id.imageButton3);
                btn_text=(Button) view.findViewById(R.id.btn_text);
               // btn_download= (CircleButton) view.findViewById(R.id.btn_download);

            }
        }


        public HorizontalAdapter(Category category, RecyclerView categoryGridView) {
            this.category = category;
            Book book = null;
            this.recyclerView=categoryGridView;

            if(category.getCategoryId()==0){
                Books=myBooks;
            }else if(category.getCategoryId()==1){
                Books=newBooks;
            }else if(category.getCategoryId()==2){
                Books=miniBook;
            }else if(category.getCategoryId()==3){
                Books=mindMapBooks;
            }else if(category.getCategoryId()==4){
                Books=media;
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shelf_cloud_book, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            final CloudData book = Books.get(position);

            holder.bookImg.setImageBitmap(null);
            holder.btn_text.setText("0");
            holder.btn_text.setVisibility(View.GONE);
           // String data = book.getBookDetails();
            String jsonContent=null;
            if(category.getCategoryId()==0 ||category.getCategoryId()==1){
                HashMap<String, String> map=null;
                if(book.getEnrichments().size()>0) {
                     map = book.getEnrichments().get(0).getMap();
                }else if(book.getResources().size()>0){
                    map = book.getResources().get(0).getMap();
                }

                jsonContent = map.get(KEY_CECONTENT);

            }
            if(category.getCategoryId()==2||category.getCategoryId()==3||category.getCategoryId()==4){
                HashMap<String, String> map =book.getMap();
                String cloudEnrselected = map.get(KEY_CLOUDENRSELECTED);
                String enrichmentNo=map.get(KEY_CENRICHMENTS);
                jsonContent = map.get(KEY_CECONTENT);
            }
            if(jsonContent!=null) {
                //String[] split = data.split("###");
                int enrSize = 0,resSize = 0;
                if(book.getEnrichments().size()>0){
                    enrSize=book.getEnrichments().size();
                }
                if(book.getResources().size()>0){
                    resSize=book.getResources().size();
                }

                int count =enrSize+resSize;

                try {
                    JSONObject jsonObject = new JSONObject(jsonContent);
                    String type = jsonObject.getString("type");
                    if (type.equals("Resource") ||type.equals("Enrichment")) {

                        if(jsonObject.has("BOOK")) {
                            String idBook = jsonObject.getString("BOOK");
                            JSONObject jsonObject1 = new JSONObject(idBook);
                            String imageUrl = jsonObject1.getString("imageURL");

                            Glide.with(context).load(imageUrl)
                                    .thumbnail(0.5f)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .override(200,200)
                                    .into(holder.bookImg);
                            //new saveImage(holder.bookImg,null).execute(imageUrl);
                            String bookTitle = jsonObject1.getString("title");
                            holder.tv_Title.setText(bookTitle);
                        }
                        holder.btn_text.setText(""+count);
                        holder.btn_text.setVisibility(View.VISIBLE);


                    }else if(type.equals("Mindmap") ||type.equals("Album")){
                        String bookTitle=jsonObject.getString("bookTitle");
                        String imageUrl = jsonObject.getString("coverImagePath");
                        if(imageUrl!=null) {
                            Glide.with(context).load(imageUrl)
                                    .thumbnail(0.5f)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .override(200,200)
                                    .into(holder.bookImg);

                        }
                        holder.tv_Title.setText(bookTitle);
                    }
                    else{
                        String bookDetails=jsonObject.getString("bookDetails");
                        JSONObject jsonObject1 = new JSONObject(bookDetails);
                        String bookTitle=jsonObject1.getString("title");
                        String imageUrl = jsonObject.getString("coverImagePath");
                        if(imageUrl!=null) {
                            Glide.with(context).load(imageUrl)
                                    .thumbnail(0.5f)
                                    .crossFade()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .override(200,200)
                                    .into(holder.bookImg);

                        }
                        holder.tv_Title.setText(bookTitle);


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            holder.bookImg.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    return false;
                }
            });
            holder.bookImg.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if(book.getCategoryId()==2||book.getCategoryId()==3||book.getCategoryId()==4){
                        normalBookDialogWindow(Books,position,recyclerView);
                    }else if(book.getResources()!=null||book.getEnrichments()!=null) {
                        HashMap<String, String> map=null;
                        if(book.getEnrichments()!=null&&book.getEnrichments().size()>0) {
                             map = book.getEnrichments().get(0).getMap();
                        }else  if(book.getResources()!=null&&book.getResources().size()>0) {
                            map = book.getResources().get(0).getMap();
                        }
                        String jsonContent = map.get(KEY_CECONTENT);

                        try {
                            JSONObject jsonObject = new JSONObject(jsonContent);
                            String type = jsonObject.getString("type");
                            if (type.equals("Resource") ||type.equals("Enrichment")) {
                            //if (!type.equals("sharedBook")) {
                                Intent intent = new Intent(context, CloudEnrichDetails.class);
                                intent.putExtra("selectedBook", book);
                                intent.putExtra("Shelf", context.gridShelf);
                                context.startActivityForResult(intent, context.storeViewActivityrequestCode);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });



            holder.imgInfoBtn.setOnClickListener(ShelfforcloudBook.this);
            holder.imgInfoBtn.setTag(position);

            holder.imgExportBtn.setOnClickListener(ShelfforcloudBook.this);
            holder.imgExportBtn.setTag(position);

            holder.imgDeleteBtn.setOnClickListener(ShelfforcloudBook.this);
            holder.imgDeleteBtn.setTag(position);


                holder.imgInfoBtn.setVisibility(View.INVISIBLE);
                holder.imgExportBtn.setVisibility(View.INVISIBLE);
                holder.imgDeleteBtn.setVisibility(View.INVISIBLE);

        }

        @Override
        public int getItemCount() {
            return Books.size();
        }

    }

    private void normalBookDialogWindow(final ArrayList<CloudData> Books, final int position, final RecyclerView recyclerView){
        final CloudData book = Books.get(position);
        HashMap<String, String> map =book.getMap();

        String enrichmentNo=map.get(KEY_CENRICHMENTS);
        String jsonContent = map.get(KEY_CECONTENT);
        JSONObject jsonObject = null;
        String imageUrl=null;
        String bookTitle = null,fileSize = null,sharedBy = null,bookDetails,fileName = null,totalPages = null,isEditable = null,desc=null,type=null;
        try {
            jsonObject = new JSONObject(jsonContent);
           // fileSize=jsonObject.getString("fileSize");
            sharedBy=jsonObject.getString("sharedBy");
            type =jsonObject.getString("type");
            imageUrl = jsonObject.getString("coverImagePath");
            fileName=jsonObject.getString("dateTime");
            if(jsonObject.has("bookDetails")) {
                bookDetails = jsonObject.getString("bookDetails");
                JSONObject jsonObject1 = new JSONObject(bookDetails);
                bookTitle = jsonObject1.getString("title");
                totalPages = jsonObject1.getString("totalPages");
                isEditable = jsonObject1.getString("isEditable");
            }else{
                isEditable="no";
                totalPages="1";
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        final Dialog addNewBookDialog = new Dialog(context);
        addNewBookDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.semi_transparent)));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        addNewBookDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addNewBookDialog.setContentView(context.getLayoutInflater().inflate(R.layout.sharedbk_alert, null));
        addNewBookDialog.getWindow().setLayout((int) context.getResources().getDimension(R.dimen.sharedbk_alert_width),  (int)context.getResources().getDimension(R.dimen.sharedbk_alert_heigth));
        addNewBookDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        //ImageView imageView1=(ImageView) addNewBookDialog.findViewById(R.id.imageView1);
        Button btn_download = (Button)addNewBookDialog. findViewById(R.id.btn_download);
        Button btn_delete = (Button)addNewBookDialog. findViewById(R.id.btn_del);
        TextView book_Label=(TextView) addNewBookDialog.findViewById(R.id.book_Label);
        TextView shared_by=(TextView) addNewBookDialog.findViewById(R.id.shared_by);
        TextView desLabel=(TextView) addNewBookDialog.findViewById(R.id.desLabel);
        TextView tv_book_type=(TextView) addNewBookDialog.findViewById(R.id.tv_book_type);
        TextView tv_filepath=(TextView) addNewBookDialog.findViewById(R.id.tv_filepath);
        TextView tv_file_size=(TextView) addNewBookDialog.findViewById(R.id.file_size);
        ImageView bk_image=(ImageView)addNewBookDialog.findViewById(R.id.imageView1);
        CircleButton btn_back=(CircleButton)addNewBookDialog.findViewById(R.id.btn_back);
        // new saveImage(bk_image).execute(imageUrl);
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewBookDialog.dismiss();
            }
        });
        if(book.getImg_bitmap()!=null) {
            Glide.with(context).load(imageUrl)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(200,200)
                    .into(bk_image);
            bk_image.setImageBitmap(book.getImg_bitmap());
        }else{
            Glide.with(context).load(imageUrl)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(200,200)
                    .into(bk_image);
            //new saveImage(bk_image,null).execute(imageUrl);
        }
        book_Label.setText(bookTitle);
        shared_by.setText(sharedBy);
        if(isEditable.equals("yes")){
            desc="Editable";
        }else{
            desc="NotEditable";
        }
        String data=desc+","+context.getResources().getString(R.string.total_pages_colun)+""+totalPages;
        desLabel.setText(data);
        tv_filepath.setText(fileName);
        tv_book_type.setText(type);
        tv_file_size.setText(fileSize);
        btn_download.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewBookDialog.dismiss();
                downloadNormalBook(Books,book,position,recyclerView);
            }
        });
        btn_delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               new  deletingEnrichandResources(Books,book,position,recyclerView).execute();
                addNewBookDialog.dismiss();
            }
        });
        addNewBookDialog.show();
    }
    private void downloadNormalBook(ArrayList<CloudData> Books, CloudData book, int position, RecyclerView recyclerView){
        HashMap<String, String> map =book.getMap();
        String enrichmentNo=map.get(KEY_CENRICHMENTS);
        String jsonContent = map.get(KEY_CECONTENT);
        String message = map.get(KEY_CEMESSAGE);
        Books.remove(book);
        recyclerView.getAdapter().notifyDataSetChanged();
//                    if(book.getEnrichments().size()>0){
//                        data = book.getEnrichments().get(0).getBookDetails();
//                    }
        if(jsonContent!=null) {
            final GDriveExport gdrive = new GDriveExport(context, null);
            JSONObject jsonObj = null;
            String bookDetails=null;
            try {
                jsonObj = new JSONObject(jsonContent);
                String type=jsonObj.getString("type");
                final String downloadFromPath = jsonObj.getString("downloadPath");
               if(jsonObj.has("bookDetails")) {
                   bookDetails = jsonObj.getString("bookDetails");
               }
                ArrayList<HashMap<String, String>> selectedCloudEnrList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> hashmap = null;
                for(int i=0;i<context.webService.cloudEnrList.size();i++){
                    hashmap = context.webService.cloudEnrList.get(i);
                    String enrichment=hashmap.get(context.webService.KEY_CENRICHMENTS);
                    String jsoncontent = hashmap.get(context.webService.KEY_CECONTENT);
                    String messag = hashmap.get(context.webService.KEY_CEMESSAGE);
                    if(jsonContent.equals(jsoncontent) &&enrichment.equals(enrichmentNo)&&message.equals(messag) ){
                        map.put(context.webService.KEY_CLOUDENRSELECTED, "true");
                        selectedCloudEnrList.add(hashmap);
                        context.webService.cloudEnrList.remove(i);
                        break;
                    }

                }
                gdrive.saveBookDetails(bookDetails, downloadFromPath, jsonContent, selectedCloudEnrList,type);
//                      ((MainActivity) SlideMenuWithActivityGroup.this.getCurrentActivity()).loadGridView();
                String enrID = hashmap.get(WebService.KEY_CENRICHMENTS);
                //DatabaseHandler db = DatabaseHandler.getInstance(ctx);
                context.db.executeQuery("insert into CloudObjects(objID)values('" + enrID + "')");
                //HashMap<String, String> selectedMap = selectedCloudEnrList.remove(i);
                context.webService.cloudEnrList.remove(map);
                context.onBackPressed();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private class addBooksToScrollView extends AsyncTask<Void, Object[], Void> {

        private Category category;
        //private LinearLayout linearScrollLayout;
        private ViewHolder holder;
        private int position;

        private addBooksToScrollView(int position, Category category, ViewHolder holder) {
            this.category = category;
            //this.linearScrollLayout = holder.linearScrollLayout;
            this.holder = holder;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            if (holder.position == position) {
                //holder.categoryGridView.removeAllViews();
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
             return null;
        }

        @Override
        protected void onProgressUpdate(Object[]... obj) {
            super.onProgressUpdate(obj);
            if (holder.position == position) {

                Object[] obj1 = obj[0];
                ImageView bookImg = (ImageView) obj1[0];
                Bitmap bitmap = (Bitmap) obj1[1];
                View gridImg = (View) obj1[2];
                holder.categoryGridView.invalidate();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (editmode){
                holder.txtTitle.setOnTouchListener(null);
            }
        }

        private void addNewBookButton() {
            View gridImg = context.getLayoutInflater().inflate(R.layout.shelf_book, null);
            ImageView bookImg = (ImageView) gridImg.findViewById(R.id.imageView2);
            bookImg.setBackgroundResource(R.drawable.add_shelf);
            if (category.getCategoryId() == 2) {
                bookImg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            } else if(category.getCategoryId() == Globals.myMediaCatId) {
                bookImg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }else if(category.getCategoryId()==Globals.mindMapCatId){
                bookImg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }

            TextView tv_Title = (TextView) gridImg.findViewById(R.id.textView1);
            tv_Title.setVisibility(View.GONE);
            Button imgInfoBtn = (Button) gridImg.findViewById(R.id.imageButton1);
            imgInfoBtn.setVisibility(View.GONE);
            Button imgExportBtn = (Button) gridImg.findViewById(R.id.imageButton2);
            imgExportBtn.setVisibility(View.GONE);
            Button imgDeleteBtn = (Button) gridImg.findViewById(R.id.imageButton3);
            imgDeleteBtn.setVisibility(View.GONE);
            Object[] obj = {bookImg, null, gridImg};
            publishProgress(obj);
        }

    }



    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {

            case R.id.imageButton1: {
                //Get the current book from the clicked position
                int position = (Integer) view.getTag();
                currentBook = (Book) bookListArray.get(position);

                //Show the info view in a PopOver
                RelativeLayout rootView = (RelativeLayout) context.findViewById(R.id.shelfRootViewLayout);
                final PopoverView popoverView = new PopoverView(context, R.layout.categories_list_view);
                popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.shelf_info_view_width), (int) context.getResources().getDimension(R.dimen.shelf_info_view_height)));
                popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(view), PopoverView.PopoverArrowDirectionAny, true);

                ListView listView = (ListView) popoverView.findViewById(R.id.listView1);
                TextView txtNoCategories = (TextView) popoverView.findViewById(R.id.textView1);
                ArrayList<Category> catList = new ArrayList<Category>();
                for (int i = 0; i < categoryListArray.size() ; i++) {
                    Category category = categoryListArray.get(i);
                    if (currentBook.is_bStoreBook() && (currentBook.get_bStoreID().charAt(1) == 'M' || currentBook.get_bStoreID().charAt(0) == 'C')) {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'E') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 5 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'Q') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'R') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'P') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.get_bStoreID()!= null && (currentBook.get_bStoreID().charAt(0) == 'I' || currentBook.get_bStoreID().charAt(1) == 'I' )) {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 ) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.get_bStoreID()!= null && !currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(0) == 'A') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6  && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5) {
                            catList.add(category);
                        }
                    }
                    else {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5) {
                            catList.add(category);
                        }
                    }
                }
                if (catList.isEmpty()) {
                    txtNoCategories.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    txtNoCategories.setText(R.string.add_categories);
                } else {
                    txtNoCategories.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                    listView.setAdapter(new CategoryListAdapter(catList));
                    listView.setOnItemClickListener(new OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> adapterView, final View view,
                                                int position, long id) {
                            final Category cat = (Category) view.getTag();
                            String moveDialog = context.getResources().getString(R.string.movedialog) + " " + cat.getCategoryName();
                            Builder builder = new AlertDialog.Builder(context);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    currentBook.setbCategoryId(cat.getCategoryId());
                                    context.gridView.invalidateViews();

                                    popoverView.dissmissPopover(true);
                                    for (int i = 0; i < bookListArray.size(); i++) {
                                        Book  boook = (Book) bookListArray.get(i);
                                        if(currentBook.is_bStoreBook()) {
                                            if (currentBook.get_bStoreID().equals(boook.get_bStoreID()) &&currentBook.getBookID()==boook.getBookID()) {
                                                bookListArray.remove(i);
                                                bookListArray.add(0, currentBook);
                                                break;
                                            }
                                        }else {
                                            if(currentBook.getBookID()==boook.getBookID()){
                                                bookListArray.remove(i);
                                                bookListArray.add(0, currentBook);
                                                break;
                                            }
                                        }
                                    }
                                }
                            });

                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.setTitle(R.string.movebook);
                            builder.setMessage(moveDialog);
                            builder.show();
                        }
                    });
                }
                break;
            }
            case R.id.imageButton2: {
                if (UserFunctions.checkLoginAndAlert(context,true)) {
                    int position = (Integer) view.getTag();
                    currentBook = (Book) bookListArray.get(position);
                    Intent ExportIntent = null;
                    if (currentBook.get_bStoreID() != null && currentBook.get_bStoreID().contains("A")) {
                        ExportMediaAlbumDialog exportMediaAlbumDialog = new ExportMediaAlbumDialog(context, currentBook);
                        exportMediaAlbumDialog.showExportMediaAlbumDialog();
                    } else if (currentBook.is_bStoreBook()) {
                        ExportIntent = new Intent(context, ExportEnrichmentActivity.class);
                        ExportIntent.putExtra("Book", currentBook);
                        //ExportIntent.putExtra("UserCredentials", context.SlideMenuActivity.userCredentials);
                        context.startActivity(ExportIntent);
                    }
                }

                break;
            }
            case R.id.imageButton3: {
                Builder builder = new AlertDialog.Builder(context);
                builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int position = (Integer) view.getTag();
                        Book delBook = (Book)bookListArray.get(position);
                        //deleteBook(position);
                        boolean emptyCategory = true;
                        for (int i = 0; i < bookListArray.size(); i++) {
                            Book book = (Book) bookListArray.get(i);
                            if (delBook.getbCategoryId() == book.getbCategoryId()) {
                                emptyCategory = false;
                                break;
                            }
                        }
                        if (emptyCategory){
                            if ( Globals.isTablet() ? delBook.getbCategoryId()!=2 : delBook.getbCategoryId()!=1) {
                               // db.executeQuery("update tblCategory set isHidden='true' where CID='" + delBook.getbCategoryId() + "'and isHidden='false'");
                               // reloadCategoryListArray();
                            }
                        }
                        context.gridView.invalidateViews();
                    }
                });

                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setTitle(R.string.deletebook);
                builder.setMessage(R.string.suredelete);
                builder.show();

                break;
            }

            default:
                break;
        }
    }

    private class CategoryListAdapter extends BaseAdapter {

        private ArrayList<Category> catList;

        private CategoryListAdapter(ArrayList<Category> catList) {
            this.catList = catList;
        }

        @Override
        public int getCount() {
            return catList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
                view = context.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
            }
            Category category = catList.get(position);
            view.setTag(category);

            TextView txtView = (TextView) view.findViewById(R.id.textView1);
            txtView.setText(category.getCategoryName());
            // setTexttoCategoryName(category,txtView);
            return view;
        }

    }

    /**
     * @author Change if books in editmode to true else to false
     */
    public boolean editBooks() {
        if (isInEditMode) {
            isInEditMode = false;

        } else {
            isInEditMode = true;

        }
        return isInEditMode;
    }



    @Override
    public void popoverViewWillShow(PopoverView view) {

    }

    @Override
    public void popoverViewDidShow(PopoverView view) {

    }

    @Override
    public void popoverViewWillDismiss(PopoverView view) {
        Category category = (Category) et_cat.getTag();
        String catText = et_cat.getText().toString();
        if (!category.getCategoryName().equals(catText)) {
            category.setCategoryName(catText);
            context.gridView.invalidateViews();
            String query = "update tblCategory set CName='" + catText + "' where CID='" + category.getCategoryId() + "'";
            //context.db.executeQuery(query);
        }
    }

    @Override
    public void popoverViewDidDismiss(PopoverView view) {

    }


    public void setTexttoCategoryName(Category category, TextView txtView){
       /* if (category.getCategoryId()==1){
            txtView.setId(category.getCategoryId());
            txtView.setText(R.string.not_downloaded_books);

        }else if (category.getCategoryId()==0){
            txtView.setId(category.getCategoryId());
            txtView.setText(R.string.downloaded_books);

        }else if (category.getCategoryId()==2){
            txtView.setId(category.getCategoryId());
            txtView.setText(R.string.created_books);

        }else if (category.getCategoryId()==3){
            txtView.setId(category.getCategoryId());
            txtView.setText(R.string.mindmap);

        }
        else { */
            txtView.setId(category.getCategoryId());
            txtView.setText(category.getCategoryName());
       // }
    }


    private class deletingEnrichandResources extends AsyncTask<Void, Void, Void> {
        JSONArray enrich_resource;
        String jsonString;
        ArrayList<CloudData> enrich_ResourceList;
        CloudData selected_Book;
        int position;
        RecyclerView recyclerView;

        public deletingEnrichandResources(ArrayList<CloudData> archieve_enrich, CloudData book, int i, RecyclerView recyclView) {
            enrich_ResourceList = archieve_enrich;
            recyclerView=recyclView;
            selected_Book= book;
            position=i;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            enrich_resource = new JSONArray();
            JSONArray enrich_resource = new JSONArray();
           // for (int i = 0; i < enrich_ResourceList.size(); i++) {
                CloudData data = selected_Book;
                HashMap<String, String> map = data.getMap();
                String cloudEnrselected = map.get(KEY_CLOUDENRSELECTED);
                // String enrichmentNo = map.get(webService.KEY_CENRICHMENTS);
                String enrichmentNo = map.get(KEY_CENRICHMENTS);

                    JSONObject id = new JSONObject();
                    try {
                        id.put("ID", enrichmentNo);
                        enrich_resource.put(id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            JSONObject jsonData = new JSONObject();
            try {
                jsonData.put("deleteEnrichId", enrich_resource);
                jsonData.put("deleteType", "");
                jsonString = jsonData.toString();


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
           context.webService.deleteEnrichments(jsonString, context.emaild);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
               CloudData data = selected_Book;
                HashMap<String, String> map = data.getMap();
               // String cloudEnrselected = map.get(context.webService.KEY_CLOUDENRSELECTED);
                String enrichmentNo = map.get(KEY_CENRICHMENTS);

                    for(int j=0;j<context.webService.enrichmentsList.size();j++) {
                        HashMap<String, String> resultData=context.webService.enrichmentsList;
                        if(resultData.get("ID").equals(enrichmentNo)){
                            enrich_ResourceList.remove(position);
                        }
                    }
                recyclerView.getAdapter().notifyDataSetChanged();

        }
      }
    }
