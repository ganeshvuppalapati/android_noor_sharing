package com.semanoor.source_sboookauthor;

import com.semanoor.manahij.R;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ExportEpubPdfListAdapter extends BaseAdapter {

	private Context context;
	private Integer[] listArray;
	
	public ExportEpubPdfListAdapter(Context context, Integer[] listArray){
		this.context = context;
		this.listArray = listArray;
	}
	
	@Override
	public int getCount() {
		return listArray.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = ((Activity) context).getLayoutInflater().inflate(R.layout.inflate_export_epub_pdf_text_view, null);
		}
		
		TextView textView = (TextView) view.findViewById(R.id.textView1);
		textView.setText(listArray[position]);
		return view;
	}

}
