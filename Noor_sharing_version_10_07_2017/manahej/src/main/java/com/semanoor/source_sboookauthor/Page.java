package com.semanoor.source_sboookauthor;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ptg.mindmap.widget.AppDict;
import com.ptg.mindmap.widget.LoadMindMapContent;
import com.ptg.mindmap.widget.MindmapPlayer;
import com.ptg.views.CircleButton;
import com.ptg.views.TwoDScrollView;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.inappbilling.util.IabResult;
import com.semanoor.inappbilling.util.Purchase;
import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.GDriveExport;
import com.semanoor.manahij.R;
import com.semanoor.paint.PainterCanvas;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class Page extends AsyncTask<Void, RelativeLayout, Void> implements OnTouchListener {

    public BookViewActivity ctxBookView;
    private ImageView handlerArea1, handlerArea2, handlerArea3, handlerArea4, handlerArea5, handlerArea6, handlerArea7, handlerArea8;
    public ImageView handlerArea9;
    float previousPositionX = 0;
    float previousPositionY = 0;
    public RelativeLayout currentView;
    public CustomWebRelativeLayout webRelativeLayout;
    public CustomWebRelativeLayout storePageWebRl;
    public ArrayList<Object> objectListArray;
    public ArrayList<View> deletedObjectsListArray;
    private DatabaseHandler db;
    private Globals global;
    public String pageBackgroundPath;
    public String pageBackgroundColor;
    public String BGPage;
    public String pageBG_ALL_PATH;
    public String pageBG_PATH;
    public String pageBG_objType;
    public String pageColor_objType;
    public String pageBgMindMapContent;
    public String currentPageStr;
    public boolean canvasViewAdded = false;
    public CustomImageRelativeLayout canvasImageView;
    public PainterCanvas canvasView;
    private ImageView horizontalLineView = null, verticalLineView = null;
    private ImageView horizontalY1GridLineView = null, horizontalY2GridLineView = null, horizontalY3GridLineView = null;
    private ImageView horizontalX1GridLineView = null, horizontalX2GridLineView = null, horizontalX3GridLineView = null;
    private int snapPoints, gridLineWidth;
    String pNo;
    public int enrichedPageId;
    public CustomWebRelativeLayout pageNumberObject = null;
    private String pageType;
    private UndoManager undoManager;
    public String defaultBgImagePath;
    public int bgImageContentChangeCount;
    public String scaleToFitOrTiled = "";
    private float previousScrollHeight;
    ArrayList<String> malzamahBookList;
    int bookCount=0;
    ArrayList<String>malzamahBookDetails;
    Dialog bookModalPopwindow;
    String StoreBID=null;
    String bookDetails = null;
    //String pageDetails=null;
    public RelativeLayout newLyout;
    public ImageView imageView;


    /**
     * @author Constructor Method
     */
    public Page(BookViewActivity _context, DatabaseHandler _db, Globals _global, String _pageNumber, int enrichedId) {
        this.ctxBookView = _context;
        this.db = _db;
        this.global = _global;
        this.objectListArray = new ArrayList<Object>();
        this.deletedObjectsListArray = new ArrayList<View>();
        this.pNo = _pageNumber;
        this.enrichedPageId = enrichedId;
        snapPoints = Globals.getDeviceIndependentPixels(2, ctxBookView);
        gridLineWidth = Globals.getDeviceIndependentPixels(1, ctxBookView);
        undoManager = new UndoManager(this, null);
    }

    protected void onPreExecute() {
        ctxBookView.progressBar.setVisibility(View.VISIBLE);
        bookCount=0;
    }

    ;

    @Override
    protected Void doInBackground(Void... params) {
        readJsonFile();
        getPageBackgroundPathOrColor(pNo, enrichedPageId);
        setBackgroundImage(currentPageStr, false);
        boolean exist=false;
        if(!ctxBookView.currentBook.is_bStoreBook() &&ctxBookView.currentBook.getIsMalzamah().equals("yes")) {
           // pageDetails = db.getPageDetailsinMalzamah(ctxBookView.currentBook.getBookID(), Integer.parseInt(pNo));
           // String query="select * from malzamah where BID='"+ctxBookView.currentBook.getBookID()+"' and pageNo='"+Integer.parseInt(pNo)+"'";
           // malzamahBookList=db.getMalzamahBooks(query);
            if(malzamahBookList!=null&&malzamahBookList.size()>0) {
                exist=true;
            }

        }
        if(!exist) {
            if (ctxBookView.isCreateBlankEnr != null && ctxBookView.isCreateBlankEnr.equals("blank")) {
                extendDesignPageLayout(Globals.getDeviceHeight());
            }else{
                db.getAllObjectsFortheCurrentPage(pNo, ctxBookView, enrichedPageId);
            }
        }else{
            for(int i=0;i<malzamahBookList.size();i++) {
                String pageDetails=malzamahBookList.get(i);
                String split[] = pageDetails.split("##");
                StoreBID = split[2];
                if(StoreBID!=null) {
                    bookCount = db.getStoreBookCountFromBookList(StoreBID);
                    if (bookCount != 0) {
                        db.getAllObjectsFortheCurrentPage(pNo, ctxBookView, enrichedPageId);
                    }
                }
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(RelativeLayout... layout) {
        super.onProgressUpdate(layout);
        View view = layout[0];
        int designPageLayoutHeight = Globals.getDeviceHeight();
        if (ctxBookView.designPageLayout.getHeight() != 0) {
            designPageLayoutHeight = ctxBookView.designPageLayout.getHeight();
        }
        if (view instanceof CustomImageRelativeLayout) {
            CustomImageRelativeLayout imgLayout = (CustomImageRelativeLayout) view;
            if (imgLayout.mImageYpos + imgLayout.mImageHeight > designPageLayoutHeight) {
                int height = imgLayout.mImageYpos + imgLayout.mImageHeight;
                extendDesignPageLayout(height);
            }
            addImageViewLayout(imgLayout);
            if (ctxBookView.bgImgView.getHeight() < Globals.getDeviceHeight()) {
                LayoutParams imgLayoutParams = ctxBookView.bgImgView.getLayoutParams();
                imgLayoutParams.height = Globals.getDeviceHeight();
                ctxBookView.bgImgView.setLayoutParams(imgLayoutParams);
            }
        } else if (view instanceof CustomWebRelativeLayout) {
            CustomWebRelativeLayout webLayout = (CustomWebRelativeLayout) view;
            if (webLayout.mWebTextYpos + webLayout.mWebTextHeight > designPageLayoutHeight) {
                int height = webLayout.mWebTextYpos + webLayout.mWebTextHeight;
                extendDesignPageLayout(height);
            }
            addWebViewLayout(webLayout);
            if (ctxBookView.bgImgView.getHeight() < Globals.getDeviceHeight()) {
                LayoutParams imgLayoutParams = ctxBookView.bgImgView.getLayoutParams();
                imgLayoutParams.height = Globals.getDeviceHeight();
                ctxBookView.bgImgView.setLayoutParams(imgLayoutParams);
            }
        } else if (view instanceof MindmapPlayer) {
            MindmapPlayer player = (MindmapPlayer) view;
            if (player.mImageYpos + player.mImageHeight > designPageLayoutHeight) {
                int height = player.mImageYpos + player.mImageHeight;
                extendDesignPageLayout(height);
            }
            ctxBookView.designPageLayout.addView(player);
            objectListArray.add(player);
            if (ctxBookView.bgImgView.getHeight() < Globals.getDeviceHeight()) {
                LayoutParams imgLayoutParams = ctxBookView.bgImgView.getLayoutParams();
                imgLayoutParams.height = Globals.getDeviceHeight();
                ctxBookView.bgImgView.setLayoutParams(imgLayoutParams);
            }
        }
    }


    public void pagePublishProgressUpdate(RelativeLayout rlLayout) {
        publishProgress(rlLayout);
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        ctxBookView.progressBar.setVisibility(View.GONE);
        if (malzamahBookList!=null &&malzamahBookList.size()>0 &&bookCount ==0) {
            ctxBookView.disableToolbarButtons();
            AlertDialog.Builder builder = new AlertDialog.Builder(ctxBookView);
            builder.setTitle(R.string.alert);
            builder.setMessage(R.string.malzamah_dependency);
            builder.setCancelable(false);
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                   dialog.cancel();

                }
            });
            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    callBookmodalPopUpWindow(StoreBID);
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    /**
     * @param imgRl
     * @author AddImageViewLayout
     */
    public void addImageViewLayout(final CustomImageRelativeLayout imgRl) {
        ctxBookView.designPageLayout.addView(imgRl);
        objectListArray.add(imgRl);
        if (imgRl.getObjectType().equals("DrawnImage")) {
            canvasImageView = imgRl;
        }
    }

    /**
     * @param webRl
     * @author AddWebViewLayout
     */
    public void addWebViewLayout(final CustomWebRelativeLayout webRl) {
        ctxBookView.designPageLayout.addView(webRl);
        objectListArray.add(webRl);
        if (webRl.getObjType().equals(Globals.objType_pageNoText)) {
            pageNumberObject = webRl;
        }
        if (webRl.getObjScalePageToFit().equals("yes")) {
            storePageWebRl = webRl;
        }
    }

    public void addMindMapLayout(final MindmapPlayer mindmapPlayer){
        ctxBookView.designPageLayout.addView(mindmapPlayer);
        objectListArray.add(mindmapPlayer);
    }

    public void extendDesignPageLayout(int height) {
        LayoutParams designPageLayoutParams = ctxBookView.designPageLayout.getLayoutParams();
        designPageLayoutParams.height = height;
        ctxBookView.designPageLayout.setLayoutParams(designPageLayoutParams);
        LayoutParams imgLayoutParams = ctxBookView.bgImgView.getLayoutParams();
        imgLayoutParams.height = height;
        ctxBookView.bgImgView.setLayoutParams(imgLayoutParams);
    }

    /**
     * delete by selecting objects
     */
    public void deleteCurrentSelectedView(RelativeLayout view) {
        //int rowId = 0,sequentialId = 0;
        ctxBookView.designPageLayout.removeView(view);
        objectListArray.remove(view);
            /*if (currentView instanceof CustomImageRelativeLayout) {
				rowId = ((CustomImageRelativeLayout) currentView).getObjectUniqueId();
				sequentialId = ((CustomImageRelativeLayout) currentView).getObjectSequentialId();
				String path = ((CustomImageRelativeLayout) currentView).getObjectPathContent();

				if (path != null) {
					File file = new File(path);
					if(file.exists()){
						file.delete();
					}

				}
			} else if(currentView instanceof CustomWebRelativeLayout){
				rowId = ((CustomWebRelativeLayout) currentView).getObjectUniqueId();
				sequentialId = ((CustomWebRelativeLayout) currentView).getObjectSequentialId();
			}
			db.executeQuery("delete from objects where RowID ='"+rowId+"'");
			db.executeQuery("update objects set SequentialID=SequentialID-1 where BID ='"+ctxBookView.currentBook.getBookID()+"' and pageNO='"+ctxBookView.currentPageNumber+"' and EnrichedID='"+enrichedPageId+"' and SequentialID>'"+sequentialId+"'");*/
        if (view instanceof CustomWebRelativeLayout && ((CustomWebRelativeLayout) view).getObjType().equals(Globals.objType_pageNoText)) {
            int rowId = ((CustomWebRelativeLayout) view).getObjectUniqueId();
            int sequentialId = ((CustomWebRelativeLayout) view).getObjectSequentialId();
            db.executeQuery("delete from objects where RowID ='" + rowId + "'");
            db.executeQuery("update objects set SequentialID=SequentialID-1 where BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + ctxBookView.currentPageNumber + "' and EnrichedID='" + enrichedPageId + "' and SequentialID>'" + sequentialId + "'");
        } else {
            deletedObjectsListArray.add(view);
            UndoRedoObjectData uRObjData = new UndoRedoObjectData();
            if (currentView instanceof CustomWebRelativeLayout) {
                uRObjData.setuRObjDataUniqueId(((CustomWebRelativeLayout) view).getObjectUniqueId());
                uRObjData.setuRObjDataCustomView(view);
            } else if (currentView instanceof CustomImageRelativeLayout) {
                uRObjData.setuRObjDataUniqueId(((CustomImageRelativeLayout) view).getObjectUniqueId());
                uRObjData.setuRObjDataCustomView(view);
            } else if (currentView instanceof MindmapPlayer) {
                uRObjData.setuRObjDataUniqueId(((MindmapPlayer) view).getObjectUniqueId());
                uRObjData.setuRObjDataCustomView(view);
            }
            createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CREATED, Globals.OBJECT_PREVIOUS_STATE);
            createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_DELETED, Globals.OBJECT_PRESENT_STATE);
        }
        removeHandlerArea();
    }

    /**
     * this deletes all the objects from the db while shifting between pages and from views
     *
     * @param pageNumber
     */
    public void deleteAllObjectsFromDb(String pageNumber) {
        int rowId = 0, sequentialId = 0;
        for (int i = 0; i < deletedObjectsListArray.size(); i++) {
            View deletedView = deletedObjectsListArray.get(i);
            if (deletedView instanceof CustomImageRelativeLayout) {
                rowId = ((CustomImageRelativeLayout) deletedView).getObjectUniqueId();
                sequentialId = ((CustomImageRelativeLayout) deletedView).getObjectSequentialId();
                String path = ((CustomImageRelativeLayout) deletedView).getObjectPathContent();

                if (path != null) {
                    File file = new File(path);
                    if (file.exists()) {
                        file.delete();
                    }
                }
            } else if (deletedView instanceof CustomWebRelativeLayout) {
                rowId = ((CustomWebRelativeLayout) deletedView).getObjectUniqueId();
                sequentialId = ((CustomWebRelativeLayout) deletedView).getObjectSequentialId();
            }else if (deletedView instanceof MindmapPlayer){
                rowId = ((MindmapPlayer) deletedView).getObjectUniqueId();
                sequentialId = ((MindmapPlayer) deletedView).getObjectSequentialId();
            }
            db.executeQuery("delete from objects where RowID ='" + rowId + "'");
            db.executeQuery("update objects set SequentialID=SequentialID-1 where BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + pageNumber + "' and EnrichedID='" + enrichedPageId + "' and SequentialID>'" + sequentialId + "'");
        }
        deletedObjectsListArray.clear();
        String tempFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/tempFiles";
        UserFunctions.deleteAllFilesFromDirectory(new File(tempFilePath));
    }

    /**
     * delete page number object from all the page in database
     */
    public void deletePageNumberObjectFromAllPages() {
        for (int i = 2; i < ctxBookView.pageCount; i++) {
            int sequentialId = db.getSequentialId(i, Globals.objType_pageNoText, ctxBookView.currentBook.getBookID());
            if (sequentialId > 0) {
                db.executeQuery("delete from objects where pageNO='" + i + "' and objType='" + Globals.objType_pageNoText + "' and BID='" + ctxBookView.currentBook.getBookID() + "'");
                db.executeQuery("update objects set SequentialID=SequentialID-1 where BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + i + "' and SequentialID>'" + sequentialId + "'");
            }
        }
        boolean showPageNo = false;
        ctxBookView.currentBook.set_bShowPageNo(showPageNo);
        db.executeQuery("update books set ShowPageNo='" + showPageNo + "' where BID='" + ctxBookView.currentBook.getBookID() + "'");
    }

    /**
     * Clear all the objects in the page based on the parameter
     *
     * @param exceptText
     */
    public void clearAllObjects(boolean exceptText) {
        ArrayList<Object> tempObjectListArray = new ArrayList<Object>();
        tempObjectListArray.addAll(objectListArray);
        ArrayList<View> clearedObjectList = new ArrayList<View>();
        int j = 0;
        for (int i = 0; i < tempObjectListArray.size(); i++) {
            View view = (View) tempObjectListArray.get(i);
            if (view instanceof CustomImageRelativeLayout) {
                if (((CustomImageRelativeLayout) view).getObjectType().equals("DrawnImage")) {
                    canvasImageView = null;
                }
                ctxBookView.designPageLayout.removeView(view);
                deletedObjectsListArray.add(view);
                clearedObjectList.add(view);
                objectListArray.remove(0 + j);
            } else if (view instanceof CustomWebRelativeLayout && exceptText) {
                if (((CustomWebRelativeLayout) view).getObjScalePageToFit().equals("no")) {
                    ctxBookView.designPageLayout.removeView(view);
                    deletedObjectsListArray.add(view);
                    clearedObjectList.add(view);
                    objectListArray.remove(0 + j);
                }
            } else if (view instanceof MindmapPlayer){
                ctxBookView.designPageLayout.removeView(view);
                deletedObjectsListArray.add(view);
                clearedObjectList.add(view);
                objectListArray.remove(0 + j);
            }
            else {
                j++;
            }

            if (view == currentView) {
                removeHandlerArea();
            }
        }

        if (!clearedObjectList.isEmpty()) {
            UndoRedoObjectData uRObjData = new UndoRedoObjectData();
            uRObjData.setuRObjDataViewArrayList(clearedObjectList);
            createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CLEAR_ALL_CREATED, Globals.OBJECT_PREVIOUS_STATE);
            createAndRegisterUndoRedoObjectData(uRObjData, Globals.OBJECT_CLEAR_ALL_DELETED, Globals.OBJECT_PRESENT_STATE);
        }
        tempObjectListArray.clear();
    }

    /**
     * Delete Page
     *
     * @param bookId
     * @param pageNo
     */
    public void deletePage(int bookId, int pageNo) {
        int enrichmentId = 0;
        ArrayList<Object> objectList = ctxBookView.db.getAllObjectsForGeneratingHTML(String.valueOf(pageNo), bookId, enrichmentId);
        for (int i = 0; i < objectList.size(); i++) {
            //View view = (View) objectListArray.get(i);
            HTMLObjectHolder htmlObject = (HTMLObjectHolder) objectList.get(i);
            if (htmlObject.getObjContentPath().contains("data/data")) {
                String path = htmlObject.getObjContentPath();
                if (path != null) {
                    File file = new File(path);
                    if (file.exists()) {
                        file.delete();
                    }
                }
            }
        }
        db.executeQuery("delete from objects where BID='" + bookId + "' and pageNO='" + pageNo + "'");
        db.executeQuery("update books set totalPages='" + ctxBookView.pageCount + "' where BID='" + bookId + "'");
        //RearrangePageNumbers
        db.executeQuery("update objects set pageNO=pageNO-1 where BID='" + bookId + "' and pageNO>'" + pageNo + "' and pageNO!='" + "End" + "'");
        db.executeQuery("delete from enrichments where pageNo='" + pageNo + "' and BID='" + bookId + "' ");
        //ctxBookView.designPageLayout.removeAllViews();
        //objectListArray.clear();
        //DeletePageBg if file exist
        String pageBgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookId + "/FreeFiles/pageBG_" + pageNo + ".png";
        File file = new File(pageBgPath);
        if (file.exists()) {
            file.delete();
        }
        //Delete ScreenShots path if exist
        File pageScreenShotPath = new File(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + ctxBookView.currentBook.getBookID() + "/thumbnail_" + pageNo + ".png");
        if (pageScreenShotPath.exists()) {
            pageScreenShotPath.delete();
        }
        rearrangePageScreenShotAndBackground(pageNo);
    }

    /**
     * Rearrange Page Screenshot and Background image after deletion of page
     *
     * @param pageNo
     */
    public void rearrangePageScreenShotAndBackground(int pageNo) {
        for (int i = pageNo; i < ctxBookView.pageCount; i++) {
            int j = i + 1;
            File pageScreenShotPathFrom = new File(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + ctxBookView.currentBook.getBookID() + "/thumbnail_" + j + ".png");
            File pageScreenShotPathTo = new File(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + ctxBookView.currentBook.getBookID() + "/thumbnail_" + i + ".png");
            if (pageScreenShotPathFrom.exists())
                pageScreenShotPathFrom.renameTo(pageScreenShotPathTo);

            File pageBackgroundPathFrom = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/pageBG_" + j + ".png");
            File pageBackgroundPathTo = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/pageBG_" + i + ".png");
            if (pageBackgroundPathFrom.exists())
                pageBackgroundPathFrom.renameTo(pageBackgroundPathTo);
        }
    }

    /**
     * Swap Page ScreenShots and Page Background Image
     *
     * @param fromPageNumber
     * @param toPageNumber
     */
    public void swapePageScreenshotsAndBackground(int fromPageNumber, int toPageNumber) {
        File screenShotFromPath = new File(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + ctxBookView.currentBook.getBookID() + "/thumbnail_" + fromPageNumber + ".png");
        File tempScreenShotFromPath = new File(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + ctxBookView.currentBook.getBookID() + "/thumbnail_" + fromPageNumber + "_temp.png");
        File screenShotToPath = new File(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + ctxBookView.currentBook.getBookID() + "/thumbnail_" + toPageNumber + ".png");
        if (screenShotFromPath.exists()) screenShotFromPath.renameTo(tempScreenShotFromPath);
        if (screenShotToPath.exists()) screenShotToPath.renameTo(screenShotFromPath);
        if (tempScreenShotFromPath.exists()) tempScreenShotFromPath.renameTo(screenShotToPath);

        File pageBgFromPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/pageBG_" + fromPageNumber + ".png");
        File tempPageBgFromPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/pageBG_" + fromPageNumber + "_temp.png");
        File pageBgToPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/pageBG_" + toPageNumber + ".png");
        if (pageBgFromPath.exists()) pageBgFromPath.renameTo(tempPageBgFromPath);
        if (pageBgToPath.exists()) pageBgToPath.renameTo(pageBgFromPath);
        if (tempPageBgFromPath.exists()) tempPageBgFromPath.renameTo(pageBgToPath);
    }

    /**
     * @param canvas
     * @author AddCanvasview
     */
    public void addCanvasViewLayout(PainterCanvas canvas) {
        canvasView = canvas;
        ctxBookView.designPageLayout.addView(canvas, 0, new LayoutParams(ctxBookView.designPageLayout.getWidth(), ctxBookView.designPageLayout.getHeight()));
        ctxBookView.designPageLayout.bringChildToFront(canvas);
        canvasViewAdded = true;
        if (ctxBookView.currentBook.is_bStoreBook()) {
            if (!ctxBookView.currentBook.isStoreBookCreatedFromTab()) {
                ctxBookView.designScrollPageView.setEnableScrolling(false);
            }
            if (ctxBookView.enrichmentTabListArray.size() > 0) {
                ctxBookView.rlTabsLayout.setVisibility(View.GONE);
            }
        }
    }

    /**
     * @param view
     * @author BringViewToFront
     */
    public void bringViewToFront(final View view, int newSequentialID) {
        if (newSequentialID == 0) {
            newSequentialID = objectListArray.size();
        }
        ctxBookView.designPageLayout.bringChildToFront(view);
        ctxBookView.designPageLayout.invalidate();
        if (view instanceof CustomImageRelativeLayout) {
            int sequentialId = ((CustomImageRelativeLayout) view).getObjectSequentialId();
            int uniqueId = ((CustomImageRelativeLayout) view).getObjectUniqueId();
            db.executeQuery("update objects set SequentialID=SequentialID-1 where BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + ctxBookView.currentPageNumber + "' and EnrichedID='" + enrichedPageId + "' and SequentialID>'" + sequentialId + "'");
            db.executeQuery("update objects set SequentialID='" + newSequentialID + "' where SequentialID='" + sequentialId + "' and BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + ctxBookView.currentPageNumber + "' and RowID='" + uniqueId + "'");
            ((CustomImageRelativeLayout) view).setObjectSequentialId(newSequentialID);
        } else if (view instanceof CustomWebRelativeLayout) {
            int sequentialId = ((CustomWebRelativeLayout) view).getObjectSequentialId();
            int uniqueId = ((CustomWebRelativeLayout) view).getObjectUniqueId();
            db.executeQuery("update objects set SequentialID=SequentialID-1 where BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + ctxBookView.currentPageNumber + "' and EnrichedID='" + enrichedPageId + "' and SequentialID>'" + sequentialId + "'");
            db.executeQuery("update objects set SequentialID='" + newSequentialID + "' where SequentialID='" + sequentialId + "' and BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + ctxBookView.currentPageNumber + "' and RowID='" + uniqueId + "'");
            ((CustomWebRelativeLayout) view).setObjectSequentialId(newSequentialID);
        }
    }

    /**
     * @param view
     * @author SendViewToBack
     */
    public void sendViewToBack(View view, int newSequentialID) {
        int sequentialId = 0, uniqueId = 0;
        ctxBookView.designPageLayout.removeView(view);
        if (view instanceof CustomImageRelativeLayout) {
            sequentialId = ((CustomImageRelativeLayout) view).getObjectSequentialId();
            uniqueId = ((CustomImageRelativeLayout) view).getObjectUniqueId();
        } else if (view instanceof CustomWebRelativeLayout) {
            sequentialId = ((CustomWebRelativeLayout) view).getObjectSequentialId();
            uniqueId = ((CustomWebRelativeLayout) view).getObjectUniqueId();
        }
        if (ctxBookView.currentBook.is_bStoreBook() && !ctxBookView.currentBook.isStoreBookCreatedFromTab() && pageType.equals("duplicate")) {
            if (newSequentialID == 0) {
                newSequentialID = 2;
            }
            ctxBookView.designPageLayout.addView(view, newSequentialID);
            db.executeQuery("update objects set SequentialID=SequentialID+1 where BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + ctxBookView.currentPageNumber + "' and EnrichedID='" + enrichedPageId + "' and SequentialID<'" + sequentialId + "' and SequentialID!=1");
            db.executeQuery("update objects set SequentialID='" + newSequentialID + "' where SequentialID='" + sequentialId + "' and BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + ctxBookView.currentPageNumber + "' and RowID='" + uniqueId + "'");
        } else {
            if (newSequentialID == 0) {
                newSequentialID = 1;
            }
            ctxBookView.designPageLayout.addView(view, 1);
            db.executeQuery("update objects set SequentialID=SequentialID+1 where BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + ctxBookView.currentPageNumber + "' and EnrichedID='" + enrichedPageId + "' and SequentialID<'" + sequentialId + "'");
            db.executeQuery("update objects set SequentialID='" + newSequentialID + "' where SequentialID='" + sequentialId + "' and BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO='" + ctxBookView.currentPageNumber + "' and RowID='" + uniqueId + "'");
        }
        if (view instanceof CustomImageRelativeLayout) {
            ((CustomImageRelativeLayout) view).setObjectSequentialId(newSequentialID);
        } else if (view instanceof CustomWebRelativeLayout) {
            ((CustomWebRelativeLayout) view).setObjectSequentialId(newSequentialID);
        }
    }

    /**
     * Method to get current page background path, background color, page BG type, color type, currentPageString.
     *
     * @param pageNumber
     */
    private void getPageBackgroundPathOrColor(String pageNumber, int enrichId) {
        int pageNo = Integer.parseInt(pageNumber);
        String bgALLImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/pageBG_ALL.png";
        pageBG_ALL_PATH = bgALLImagePath;
        String bgImagePath;
        String[] contentObj = db.getPgeBgObjType(ctxBookView.currentBook.getBookID(), pageNumber, enrichId);
        String pgBgobjType = null;
        if (contentObj != null && contentObj.length == 2) {
            pgBgobjType = contentObj[0];
            pageBgMindMapContent = contentObj[1];
        }
        if (pageNo != ctxBookView.pageCount) {
            if (enrichId == 0) {
                bgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/pageBG_" + pageNo + ".png";
            } else {
                bgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/pageBG_" + pageNo + "-" + enrichId + ".png";

            }
            if (pgBgobjType != null && pgBgobjType.equals(Globals.OBJTYPE_MINDMAPBG)) {
                pageBG_objType = Globals.OBJTYPE_MINDMAPBG;
            } else {
                pageBG_objType = Globals.OBJTYPE_PAGEBG;
            }
            pageColor_objType = "PageColor";
            currentPageStr = pageNumber;
        } else {
            bgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/pageBG_End.png";
            pageBG_objType = "PageBGEnd";
            pageColor_objType = "PageColorEnd";
            currentPageStr = "End";
        }
        pageBG_PATH = bgImagePath;
        if (pageNo == 1 || pageNo == ctxBookView.pageCount) {

            if (checkPageBGexists(bgImagePath)) {
                pageBackgroundPath = bgImagePath;
                pageBackgroundColor = "";
            } else if (checkPageColorexists(currentPageStr, enrichedPageId)) {
                pageBackgroundPath = "";

            } else {
                pageBackgroundPath = "";
                pageBackgroundColor = "";
            }
        } else {

            if (checkPageBGexists(bgImagePath)) {
                pageBackgroundPath = bgImagePath;
                pageBackgroundColor = "";


            } else if (checkPageColorexists(currentPageStr, enrichedPageId)) {
                pageBackgroundPath = "";

            } else if (checkPageBG_ALLexists(pageNo, bgALLImagePath)) {
                pageBackgroundPath = bgALLImagePath;
                pageBackgroundColor = "";

            } else {
                pageBackgroundPath = "";
                pageBackgroundColor = "";
            }
        }
    }

    /**
     * @param pageNumber
     * @author SetBackgroundImageForthePage first it checks for page BG_ALL exists else checks for pageBG exists else checks for page color exists else set default background image.
     */
    public void setBackgroundImage(String pageNumber, final boolean canceledColorPickerBg) {

        int pageNo;
        if (!pageNumber.equals("End")) {
            pageNo = Integer.parseInt(pageNumber);
        } else {
            pageNo = ctxBookView.pageCount;
        }

        if (pageNo == 1 || pageNo == ctxBookView.pageCount) { //Checks for cover and end page
            if (checkPageBGexists(pageBG_PATH)) {
                checkTiledorScaledmode(pageBG_PATH, pageNumber);
                String pno = String.valueOf(pageNo);
                int currentRotation = ctxBookView.db.getPageBackgroundRotationAngle(ctxBookView.currentBook.getBookID(), pageBG_objType, pno, enrichedPageId);
                if (currentRotation != 0) {
                    rotateBitmap(currentRotation, pageBG_PATH);
                }
            } else if (checkPageColorexists(pageNumber, enrichedPageId)) {
                //Set bitmap image with color background
                int color = Integer.parseInt(pageBackgroundColor);
                //System.out.println("Print the color:"+color);
                setPageBackgroundWithColor(color);
            } else {
                setDefaultTemplateBackgroundImage(pageNo);
            }
        } else {
            if (checkPageBGexists(pageBG_PATH)) {
                checkTiledorScaledmode(pageBG_PATH, pageNumber);
                String pno = String.valueOf(pageNo);
                int currentRotation = ctxBookView.db.getPageBackgroundRotationAngle(ctxBookView.currentBook.getBookID(), pageBG_objType, pno, enrichedPageId);
                if (currentRotation != 0) {
                    rotateBitmap(currentRotation, pageBG_PATH);
                }
            } else if (checkPageColorexists(pageNumber, enrichedPageId)) {
                //Set bitmap image with color background
                int color = Integer.parseInt(pageBackgroundColor);
                //System.out.println("Print the color:"+color);
                setPageBackgroundWithColor(color);
            } else if (checkPageBG_ALLexists(pageNo, pageBG_ALL_PATH)) {

                checkTiledorScaledmode(pageBG_ALL_PATH, "0");
                int currentRotation = ctxBookView.db.getPageBackgroundRotationAngle(ctxBookView.currentBook.getBookID(), "PageBG", "0", enrichedPageId);
                if (currentRotation != 0) {
                    rotateBitmap(currentRotation, pageBG_ALL_PATH);
                }
            } else {
                if (ctxBookView.currentBook.is_bStoreBook() && !ctxBookView.currentBook.isStoreBookCreatedFromTab() ||ctxBookView.currentBook.get_bStoreID()!=null&&ctxBookView.currentBook.get_bStoreID().contains("P")) {
                    if(ctxBookView.currentBook.get_bStoreID().contains("P") && enrichedPageId==0){
                         setDefaultTemplateBackgroundImage(pageNo);
                    }else {
                        ctxBookView.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ctxBookView.bgImgView.setImageBitmap(null);
                                ctxBookView.bgImgView.setBackgroundColor(Color.WHITE);
                                if (canceledColorPickerBg) {
                                    if (storePageWebRl != null) {
                                        storePageWebRl.resetParentTableBgForStoreWebRl();
                                    }
                                }
                            }
                        });
                    }
                } else {
                     setDefaultTemplateBackgroundImage(pageNo);

                }
            }
        }
        ctxBookView.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                displayAlphaSliderView();
            }
        });
    }

    public void displayAlphaSliderView() {
        if (ctxBookView.undoRedoLayout.getVisibility() == View.VISIBLE) {
            if (pageBG_objType != null && pageBG_objType.equals(Globals.OBJTYPE_MINDMAPBG)) {
                ctxBookView.mindMapSliderlayout.setVisibility(View.VISIBLE);
            } else {
                ctxBookView.mindMapSliderlayout.setVisibility(View.GONE);
            }
        } else {
            ctxBookView.mindMapSliderlayout.setVisibility(View.GONE);
        }
    }

    /**
     * checks whether pageBG_All background apply all exists
     */
    public Boolean checkPageBG_ALLexists(int pageNo, String bgALLImagePath) {

        if (pageNo != 1 && pageNo != ctxBookView.pageCount) {
            File file = new File(bgALLImagePath);
            if (file.exists()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * checks whether pageBG exists
     */
    public Boolean checkPageBGexists(String bgImagePath) {

        File file = new File(bgImagePath);
        if (file.exists()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * checks whether pageColor exists
     */
    public Boolean checkPageColorexists(String pageNo, int enrichId) {

        String pageColorValue = ctxBookView.db.getPageBackgroundColor(ctxBookView.currentBook.getBookID(), pageColor_objType, pageNo, enrichId);
        if (!pageColorValue.equals("NoEntry")) {
            pageBackgroundColor = pageColorValue;
            return true;
        } else {

            int pageValue;
            if (!pageNo.equals("End")) {
                pageValue = Integer.parseInt(pageNo);
            } else {
                pageValue = ctxBookView.pageCount;
            }
            if (pageValue > 1 && pageValue < ctxBookView.pageCount) {
                //check for apply all color background
                String pageAllColorValue = ctxBookView.db.getPageBackgroundColor(ctxBookView.currentBook.getBookID(), "PageColor", "0", enrichId);
                if (!pageAllColorValue.equals("NoEntry")) {
                    pageBackgroundColor = pageAllColorValue;
                    return true;
                } else {
                    pageBackgroundColor = "";
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * @param pageNo
     * @author Set page background with default template backgrounds
     */
    private void setDefaultTemplateBackgroundImage(int pageNo) {
        boolean exist=false;
        if(!ctxBookView.currentBook.is_bStoreBook() &&ctxBookView.currentBook.getIsMalzamah().equals("yes")) {
           // String pageDetails = db.getPageDetailsinMalzamah(ctxBookView.currentBook.getBookID(), pageNo);
            String query="select * from malzamah where BID='"+ctxBookView.currentBook.getBookID()+"' and pageNo='"+Integer.parseInt(pNo)+"'";
            malzamahBookList=db.getMalzamahBooks(query);
//            if(malzamahBookList.size()>0) {
//                exist=true;
//            }
            if (malzamahBookList.size()>0) {
                exist=true;
            }
            for(int i=0;i<malzamahBookList.size();i++) {
                String pageDetails = malzamahBookList.get(i);
                String split[] = pageDetails.split("##");
                String StoreBID = split[2];
                bookCount = db.getStoreBookCountFromBookList(StoreBID);
                if (bookCount == 0) {
                    ctxBookView.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            ctxBookView.bgImgView.setImageBitmap(null);
                            ctxBookView.bgImgView.setBackgroundColor(Color.WHITE);
                        }
                    });
                    //ctxBookView.bgImgView.setImageBitmap(null);
                }
            }
        }

        if(!exist ||bookCount!=0) {
            if (pageNo == 1) {
                if (ctxBookView.currentBook.getBookOrientation() == Globals.portrait) {
                    defaultBgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/front_P.png";
                } else if (ctxBookView.currentBook.getBookOrientation() == Globals.landscape) {
                    defaultBgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/front.png";
                }
                if (new File(defaultBgImagePath).exists()) {
                    setBitmapImage(defaultBgImagePath);
                }
            } else if (pageNo == ctxBookView.pageCount) {
                if (ctxBookView.currentBook.getBookOrientation() == Globals.portrait) {
                    defaultBgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/back_P.png";
                } else if (ctxBookView.currentBook.getBookOrientation() == Globals.landscape) {
                    defaultBgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/back.png";
                }
                if (new File(defaultBgImagePath).exists()) {
                    setBitmapImage(defaultBgImagePath);
                }
            } else {
                if (ctxBookView.currentBook.getBookOrientation() == Globals.portrait) {
                    defaultBgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/page_P.png";
                } else if (ctxBookView.currentBook.getBookOrientation() == Globals.landscape) {
                    defaultBgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/page.png";
                }
                if (ctxBookView.isCreateBlankEnr!=null && ctxBookView.isCreateBlankEnr.equals("duplicate")){
                    defaultBgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/" + pageNo + ".png";
                }
                //  if(ctxBookView.currentBook.is_bStoreBook()) {
                if (ctxBookView.currentBook.get_bStoreID() != null && ctxBookView.currentBook.get_bStoreID().contains("P")) {
                    if (ctxBookView.isCreateBlankEnr!=null && ctxBookView.isCreateBlankEnr.equals("duplicate")) {
                        defaultBgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/" + pageNo + ".png";
                    }else if (ctxBookView.isCreateBlankEnr!=null && ctxBookView.isCreateBlankEnr.equals("blank")){
                        extendDesignPageLayout(Globals.getDeviceHeight());
                    }
//                    if(enrichedPageId!=0){
//                        ctxBookView.bgImgView.setImageBitmap(null);
//                        ctxBookView.bgImgView.setBackgroundColor(Color.WHITE);
//                    }
                }
                //}
                if (new File(defaultBgImagePath).exists()) {
                    setBitmapImage(defaultBgImagePath);
                }
            }
        }
    }

    /**
     * Resize bitmap, decode file and set background image for book view page
     */
    private void setBitmapImage(String bgImagePath) {
        final int width = Globals.getDeviceWidth();
        final int height = Globals.getDeviceHeight();
        final Bitmap decodeBitmap = resizeDecodeBitmap(bgImagePath);
        ctxBookView.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (decodeBitmap!=null) {
                    if (width > 0 && height > 0) {
                        Bitmap resizedBm = Bitmap.createScaledBitmap(decodeBitmap, width, height, true);
                        ctxBookView.bgImgView.setImageBitmap(resizedBm);
                    } else {
                        ctxBookView.bgImgView.setImageBitmap(decodeBitmap);
                    }
                }
            }
        });
        scaleToFitOrTiled = "ScaleToFit";
    }

    /**
     * Check for tiled mode or scalemode and apply it
     */
    public void checkTiledorScaledmode(String bgImagePath, String pageNumber) {

        if (ctxBookView.db.checkTiledModePageBackground(ctxBookView.currentBook.getBookID(), pageBG_objType, pageNumber)) {
            //Apply tiled mode
            applyTiledToPageBackground(bgImagePath, pageNumber);
        } else {
            setBitmapImage(bgImagePath);
        }
    }

    /**
     * Apply tile mode
     *
     * @param pageBGPath
     * @param currentPage
     */
    public void applyTiledToPageBackground(String pageBGPath, String currentPage) {

        //Bitmap backBitmap = BitmapFactory.decodeFile(pageBGPath);
        Bitmap backBitmap = resizeDecodeBitmap(pageBGPath);
        final BitmapDrawable backDrawble = new BitmapDrawable(ctxBookView.getResources(), createNewBitmap(backBitmap));
        backDrawble.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        ctxBookView.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                ctxBookView.bgImgView.setImageBitmap(null);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    ctxBookView.bgImgView.setBackgroundDrawable(backDrawble);
                } else {
                    ctxBookView.bgImgView.setBackground(backDrawble);
                }
            }
        });
        scaleToFitOrTiled = "Tiled";
    }

    /**
     * Check and apply scaleToFit mode
     *
     * @param currentPage
     */
    public void applyScaleTofitPageBackground(String pageBGPath, String currentPage) {

        //if(ctxBookView.db.checkTiledModePageBackground(ctxBookView.currentBook.getBookID(), pageBG_objType, currentPage)){
        //apply scaleToFit mode
        ctxBookView.bgImgView.setImageBitmap(null);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            ctxBookView.bgImgView.setBackgroundDrawable(null);
        } else {
            ctxBookView.bgImgView.setBackground(null);
        }
        ctxBookView.bgImgView.setScaleType(ScaleType.FIT_XY);
        //Bitmap backBitmap = BitmapFactory.decodeFile(pageBGPath);
        Bitmap backBitmap = resizeDecodeBitmap(pageBGPath);
        ctxBookView.bgImgView.setImageBitmap(createNewBitmap(backBitmap));
        scaleToFitOrTiled = "ScaleToFit";
        //}
    }

    /**
     * CreateNewBitmap method checks and create new bitmap based on width and height to avoid "Out of memory error".
     *
     * @param oldBitmap
     * @return
     */
    private Bitmap createNewBitmap(Bitmap oldBitmap) {

        int width = oldBitmap.getWidth();
        int height = oldBitmap.getHeight();
        if (width > 0 && height > 0) {
            Bitmap createBitmap = Bitmap.createBitmap(oldBitmap, 0, 0, width, height);
            return createBitmap;
        } else {
            return oldBitmap;
        }
    }

    public Bitmap resizeDecodeBitmap(String imagePath) {
        int targetWidth = global.getDeviceWidth();
        int targetHeight = global.getDeviceHeight();

        //Get the dimensions of the bitmap :
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bm = BitmapFactory.decodeFile(imagePath, options);
        int photoW = options.outWidth;
        int photoH = options.outHeight;

        //Determine the scale down of the image as it throws out of memory exception:
        if (targetWidth > 0 && targetHeight > 0) {
            int scaleFactor = Math.min(photoW / targetWidth, photoH / targetHeight);
            options.inJustDecodeBounds = false;
            options.inSampleSize = scaleFactor;
            options.inPurgeable = true;
        }
        Bitmap Decodebitmap = BitmapFactory.decodeFile(imagePath, options);
        return Decodebitmap;
    }

    /**
     * Check and rotate background
     */
    public void rotateBackground(String pageBGPath, String currentPage, int rotateAngle, Boolean updateDB) {

        String objectType = pageBG_objType;
        int currentRotation = ctxBookView.db.getPageBackgroundRotationAngle(ctxBookView.currentBook.getBookID(), objectType, currentPage, enrichedPageId);
        currentRotation = currentRotation + rotateAngle;
        //Update in DB.  Updating both location and size to scaletofit as rotate and tile mode can't be applied.
        if (updateDB) {

            String location = currentRotation + "|0";
            String size = "ScaleToFit";
            String updateQuery = "update objects set location='" + location + "', size='" + size + "' where objType='" + objectType + "' and BID ='" + ctxBookView.currentBook.getBookID() + "' and pageNO ='" + currentPage + "'";
            ctxBookView.db.executeQuery(updateQuery);
        }

        rotateBitmap(currentRotation, pageBGPath);
    }

    /**
     * Method to rotate page background.
     *
     * @param currentRotation
     * @param pageBGPath
     */
    private void rotateBitmap(int currentRotation, String pageBGPath) {
        final Matrix matrix = new Matrix();
        matrix.postRotate(currentRotation);
        //Bitmap bitmap = BitmapFactory.decodeFile(pageBGPath);
        final Bitmap bitmap = resizeDecodeBitmap(pageBGPath);
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        ctxBookView.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (width > 0 && height > 0) {
                    Bitmap rotateBm = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    ctxBookView.bgImgView.setImageBitmap(rotateBm);
                } else {
                    ctxBookView.bgImgView.setImageBitmap(bitmap);
                }
            }
        });
    }

    /**
     * Method to set page background with color
     *
     * @param color
     */

    public void setPageBackgroundWithColor(final int color) {
        ctxBookView.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    ctxBookView.bgImgView.setBackgroundDrawable(null);
                } else {
                    ctxBookView.bgImgView.setBackground(null);
                }
                ctxBookView.bgImgView.setImageBitmap(null);
                ctxBookView.bgImgView.setBackgroundColor(color);
            }
        });
    }

    /**
     * @author SetCurrentView
     */
    public void setCurrentView(RelativeLayout view) {
        if (currentView != null) {
            currentView.clearFocus();
        }
        currentView = view;

        if (currentView instanceof CustomWebRelativeLayout) {
            webRelativeLayout = (CustomWebRelativeLayout) view;
        } else {
            hideKeyboard();
        }
    }

    /**
     * Hide Keyboard
     */
    public void hideKeyboard() {
        if (currentView != null) {
            InputMethodManager imm = (InputMethodManager) ctxBookView.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(currentView.getWindowToken(), 0);
            ctxBookView.designPageLayout.setY(0);
            ctxBookView.keyboardToolbar.setVisibility(View.GONE);
        }
    }

    /**
     * @author GetCurrentView
     */
    public View getCurrentView() {
        return currentView;
    }

    /**
     * @param pageNumber
     * @author SaveObjectsinCurrentPage
     */
    public void saveAllObjectsInCurrentPage(String pageNumber) {
        if (this.getStatus() == AsyncTask.Status.FINISHED) {
            for (int i = 0; i < ctxBookView.designPageLayout.getChildCount(); i++) {
                View childView = ctxBookView.designPageLayout.getChildAt(i);
                if (childView instanceof CustomImageRelativeLayout) {
                    String location = Math.round(childView.getX()) + "|" + Math.round(childView.getY());
                    String size = childView.getWidth() + "|" + childView.getHeight();
                    int objectUniqueId = ((CustomImageRelativeLayout) childView).getObjectUniqueId();
                    String objType = ((CustomImageRelativeLayout) childView).getObjectType();
                    String objContent = ((CustomImageRelativeLayout) childView).getObjectPathContent().replace("'", "''");
                    boolean objLocked = ((CustomImageRelativeLayout) childView).isObjLocked();
                    // Only for iframe update the urlpathcontent in database
                    if (!objType.equals("IFrame") && !objType.equals("EmbedCode") && !objType.equals("YouTube")) {
                        db.executeQuery("update objects set location='" + location + "', size='" + size + "', locked='" + objLocked + "' where RowID='" + objectUniqueId + "'");
                    } else {
                        db.executeQuery("update objects set location='" + location + "', size='" + size + "', content='" + objContent + "', locked='" + objLocked + "' where RowID='" + objectUniqueId + "'");
                    }
                } else if (childView instanceof CustomWebRelativeLayout) {
                    String objectType = ((CustomWebRelativeLayout) childView).getObjType();
                    if (!objectType.equals(Globals.objType_pageNoText)) {
                        String objContent = null;
                        //((CustomWebRelativeLayout) childView).getAndSetWebViewContent();
                        String location = Math.round(childView.getX()) + "|" + Math.round(childView.getY());
                        String size = null;
                        if (objectType.equals("WebTable")) {
                            int rows = ((CustomWebRelativeLayout) childView).getmWebTableRow();
                            int columns = ((CustomWebRelativeLayout) childView).getmWebTableColumn();
                            size = childView.getWidth() + "|" + childView.getHeight() + "|" + rows + "|" + columns;
                        } else {
                            size = childView.getWidth() + "|" + childView.getHeight();
                        }
                      //  if(objectType.equals(Globals.OBJTYPE_TITLETEXT) ||objectType.equals(Globals.OBJTYPE_AUTHORTEXT)){
                        if(ctxBookView.backClicked && objectType.equals(Globals.OBJTYPE_TITLETEXT)) {
                            ctxBookView.titleTextClicked = true;
                        }
                        //}
                        int objectUniqueId = ((CustomWebRelativeLayout) childView).getObjectUniqueId();
						/*while (!((CustomWebRelativeLayout) childView).objContentUpdated) {
							////System.out.println("NotUpdated");
						}*/
                        ((CustomWebRelativeLayout) childView).getAndSetWebViewContent();
                        objContent = ((CustomWebRelativeLayout) childView).getObjectContent();
                        objContent = objContent.replace("'", "''");
                        boolean objLocked = ((CustomWebRelativeLayout) childView).isObjLocked();
                        String updateQuery = "update objects set location='" + location + "', size='" + size + "', content='" + objContent + "', locked='" + objLocked + "' where RowID='" + objectUniqueId + "'";
                        db.executeQuery(updateQuery);
                        updateBookTitleAndAuthorObjects(objContent, objectType);
                    }
                } else if (childView instanceof MindmapPlayer) {
                    String location = Math.round(childView.getX()) + "|" + Math.round(childView.getY());
                    String size = childView.getWidth() + "|" + childView.getHeight();
                    int objectUniqueId = ((MindmapPlayer) childView).getObjectUniqueId();
                    String objContent = ((MindmapPlayer) childView).getObjContent().replace("'", "''");
                    boolean objLocked = ((MindmapPlayer) childView).isObjLocked();
                    // Only for iframe update the urlpathcontent in database
                    db.executeQuery("update objects set location='" + location + "', size='" + size + "', content='" + objContent + "', locked='" + objLocked + "' where RowID='" + objectUniqueId + "'");
                }
            }
        }
    }

    private void updateBookTitleAndAuthorObjects(String text, String objType) {
        if (objType.equals(Globals.OBJTYPE_TITLETEXT)) {
            String updateTitleQuery = "update books set Title='" + ctxBookView.currentBook.getBookTitle() + "' where BID='" + ctxBookView.currentBook.getBookID() + "'";
            db.executeQuery(updateTitleQuery);
        } else if (objType.equals(Globals.OBJTYPE_AUTHORTEXT)) {
            String updateAuthorQuery = "update books set Author='" + ctxBookView.currentBook.getBookAuthorName() + "' where BID='" + ctxBookView.currentBook.getBookID() + "'";
            db.executeQuery(updateAuthorQuery);
        }
    }

    /**
     * @param view
     * @author HandlerMethods MakeObjects to be selected with Handlers
     */
    public void makeSelectedArea(RelativeLayout view) {
        if (handlerArea1 == null || handlerArea2 == null || handlerArea3 == null || handlerArea4 == null || handlerArea5 == null || handlerArea6 == null || handlerArea7 == null || handlerArea8 == null) {
            int handlerSize = (int) ctxBookView.getResources().getDimension(R.dimen.object_handler_size);
            handlerArea1 = new ImageView(ctxBookView);
            handlerArea1.setBackgroundResource(R.drawable.newbluedotimage1);
            handlerArea1.setId(R.id.imghandler1);
            handlerArea1.setTag("Handler");
            handlerArea1.setOnTouchListener(this);
            ctxBookView.designPageLayout.addView(handlerArea1, handlerSize, handlerSize);

            handlerArea2 = new ImageView(ctxBookView);
            handlerArea2.setBackgroundResource(R.drawable.newbluedotimage2);
            handlerArea2.setId(R.id.imghandler2);
            handlerArea2.setTag("Handler");
            handlerArea2.setOnTouchListener(this);
            ctxBookView.designPageLayout.addView(handlerArea2, handlerSize, handlerSize);

            handlerArea3 = new ImageView(ctxBookView);
            handlerArea3.setBackgroundResource(R.drawable.newbluedotimage3);
            handlerArea3.setId(R.id.imghandler3);
            handlerArea3.setTag("Handler");
            handlerArea3.setOnTouchListener(this);
            ctxBookView.designPageLayout.addView(handlerArea3, handlerSize, handlerSize);

            handlerArea4 = new ImageView(ctxBookView);
            handlerArea4.setBackgroundResource(R.drawable.newbluedotimage4);
            handlerArea4.setId(R.id.imghandler4);
            handlerArea4.setTag("Handler");
            handlerArea4.setOnTouchListener(this);
            ctxBookView.designPageLayout.addView(handlerArea4, handlerSize, handlerSize);

            handlerArea5 = new ImageView(ctxBookView);
            handlerArea5.setBackgroundResource(R.drawable.newbluedotimage5);
            handlerArea5.setId(R.id.imghandler5);
            handlerArea5.setTag("Handler");
            handlerArea5.setOnTouchListener(this);
            ctxBookView.designPageLayout.addView(handlerArea5, handlerSize, handlerSize);

            handlerArea6 = new ImageView(ctxBookView);
            handlerArea6.setBackgroundResource(R.drawable.newbluedotimage6);
            handlerArea6.setId(R.id.imghandler6);
            handlerArea6.setTag("Handler");
            handlerArea6.setOnTouchListener(this);
            ctxBookView.designPageLayout.addView(handlerArea6, handlerSize, handlerSize);

            handlerArea7 = new ImageView(ctxBookView);
            handlerArea7.setBackgroundResource(R.drawable.newbluedotimage7);
            handlerArea7.setId(R.id.imghandler7);
            handlerArea7.setTag("Handler");
            handlerArea7.setOnTouchListener(this);
            ctxBookView.designPageLayout.addView(handlerArea7, handlerSize, handlerSize);

            handlerArea8 = new ImageView(ctxBookView);
            handlerArea8.setBackgroundResource(R.drawable.newbluedotimage8);
            handlerArea8.setId(R.id.imghandler8);
            handlerArea8.setTag("Handler");
            handlerArea8.setOnTouchListener(this);
            ctxBookView.designPageLayout.addView(handlerArea8, handlerSize, handlerSize);

            handlerArea9 = new ImageView(ctxBookView);
            handlerArea9.setBackgroundResource(R.drawable.settings_icon);
            handlerArea9.setId(R.id.imghandler2);
            handlerArea9.setTag("Handler");
            handlerArea9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setCurrentView(currentView);
                    makeSelectedArea(currentView);
                    if (ctxBookView.currentBook.is_bStoreBook() && !ctxBookView.currentBook.isStoreBookCreatedFromTab()) {
                        ctxBookView.designScrollPageView.setEnableScrolling(false);
                    }
                    ctxBookView.hideToolBarLayout();
                    if (currentView instanceof MindmapPlayer){
                        ((MindmapPlayer)currentView).mindMapPopView(v);
                    }else if (currentView instanceof CustomWebRelativeLayout){
                        ((CustomWebRelativeLayout)currentView).showinfoPopOverView(v);
                    }else if (currentView instanceof CustomImageRelativeLayout){
                        ((CustomImageRelativeLayout)currentView).showinfoPopOverView(v);
                    }
                }
            });
            ctxBookView.designPageLayout.addView(handlerArea9, handlerSize, handlerSize);
        }
        if (newLyout==null){
            if (currentView instanceof CustomImageRelativeLayout) {
                String objType = ((CustomImageRelativeLayout)currentView).getObjectType();
                if (objType.equals("Image")||objType.equals("YouTube")){
                    newLyout = new RelativeLayout(ctxBookView);
                    RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(currentView.getLayoutParams());
                    newLyout.setLayoutParams(relativeParams);

                    addInsideLayout(newLyout, 1);
                    addInsideLayout(newLyout, 2);
                    addInsideLayout(newLyout, 3);
                    addInsideLayout(newLyout, 4);
                    ctxBookView.designPageLayout.addView(newLyout);
                }
            }
        }
        reassignHandlerArea(view);
    }

    /**
     * @param view
     * @author HandlerMethods Reassigning the handler areas
     */
    public void reassignHandlerArea(RelativeLayout view) {
        int handlerSize = (int) ctxBookView.getResources().getDimension(R.dimen.object_handler_size);
        handlerArea1.setX(view.getX() - handlerSize);
        handlerArea1.setY(view.getY() - handlerSize);
        handlerArea1.setVisibility(View.VISIBLE);
        handlerArea2.setX((view.getX() + view.getLayoutParams().width / 2) - handlerSize / 2);
        handlerArea2.setY(view.getY() - handlerSize);
        handlerArea2.setVisibility(View.INVISIBLE);
        handlerArea3.setX((view.getX() + view.getLayoutParams().width));
        handlerArea3.setY(view.getY() - handlerSize);
        handlerArea3.setVisibility(View.VISIBLE);
        handlerArea4.setX((view.getX() + view.getLayoutParams().width));
        handlerArea4.setY((view.getY() + view.getLayoutParams().height / 2) - handlerSize / 2);
        handlerArea4.setVisibility(View.INVISIBLE);
        handlerArea5.setX((view.getX() + view.getLayoutParams().width));
        handlerArea5.setY((view.getY() + view.getLayoutParams().height));
        handlerArea5.setVisibility(View.VISIBLE);
        handlerArea6.setX((view.getX() + view.getLayoutParams().width / 2) - handlerSize / 2);
        handlerArea6.setY((view.getY() + view.getLayoutParams().height));
        handlerArea6.setVisibility(View.INVISIBLE);
        handlerArea7.setX(view.getX() - handlerSize);
        handlerArea7.setY((view.getY() + view.getLayoutParams().height));
        handlerArea7.setVisibility(View.VISIBLE);
        handlerArea8.setX(view.getX() - handlerSize);
        handlerArea8.setY((view.getY() + view.getLayoutParams().height / 2) - handlerSize / 2);
        handlerArea8.setVisibility(View.INVISIBLE);
        handlerArea9.setX((view.getX() + view.getLayoutParams().width));
        handlerArea9.setY(view.getY() + handlerSize/2);
        handlerArea9.setVisibility(View.VISIBLE);
        if (newLyout!=null) {
            newLyout.setVisibility(View.GONE);
        }
        if (currentView instanceof CustomImageRelativeLayout) {
            String objType = ((CustomImageRelativeLayout)currentView).getObjectType();
            if (objType.equals("Image")||objType.equals("YouTube")) {
                if (newLyout != null) {
                    newLyout.bringToFront();
                    RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(currentView.getLayoutParams());
                    newLyout.setLayoutParams(relativeParams);
                    newLyout.setX(view.getX());
                    newLyout.setY(view.getY());
                    newLyout.setVisibility(View.VISIBLE);
                    for (int i = 0; i < newLyout.getChildCount(); i++) {
                        View framelayout = newLyout.getChildAt(i);
                        if (framelayout instanceof FrameLayout) {
                            setDimensionForlayout((FrameLayout) framelayout, currentView, i + 1);
                            View imgView = ((FrameLayout) framelayout).getChildAt(0);
                            if (imgView instanceof ImageView){
                                int layoutPos = i+1;
                                if (objType.equals("Image")) {
                                    if (layoutPos == 1) {
                                        imgView.setBackgroundResource(R.drawable.image_frequentobjects);
                                    } else if (layoutPos == 2) {
                                        imgView.setBackgroundResource(R.drawable.camera_frequentobjects);
                                    }
                                }else{
                                    if (layoutPos==1){
                                        imgView.setBackgroundResource(R.drawable.videogallery_frequentobjects);
                                    }else if (layoutPos==2){
                                        imgView.setBackgroundResource(R.drawable.videocamera_frequentobjects);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Make the handler area visibility to GONE and set current selected view to null
     */
    public void removeHandlerArea() {
        if (handlerArea1 != null || handlerArea2 != null || handlerArea3 != null || handlerArea4 != null || handlerArea5 != null || handlerArea6 != null || handlerArea7 != null || handlerArea8 != null) {
            hideKeyboard();
            handlerArea1.setVisibility(View.GONE);
            handlerArea2.setVisibility(View.GONE);
            handlerArea3.setVisibility(View.GONE);
            handlerArea4.setVisibility(View.GONE);
            handlerArea5.setVisibility(View.GONE);
            handlerArea6.setVisibility(View.GONE);
            handlerArea7.setVisibility(View.GONE);
            handlerArea8.setVisibility(View.GONE);
            handlerArea9.setVisibility(View.GONE);
            if (newLyout!=null){
                newLyout.setVisibility(View.GONE);
            }
            currentView = null;
            //Globals.hideKeyboard(ctxBookView);
        }
    }

    /**
     * @param positionX
     * @param positionY
     * @param previousPosX
     * @param previousPosY
     * @param view
     * @param handlerView
     * @author HandlerMethods to Resize objects
     */
    public void objectMoveAndPinch(float positionX, float positionY, float previousPosX, float previousPosY, View view, View handlerView) {
        float scaleX = 0, scaleY = 0;
        int dipValue = Globals.getDeviceIndependentPixels(10, ctxBookView);
        android.widget.RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) view.getLayoutParams();

        scaleX = positionX - previousPosX;
        scaleY = positionY - previousPosY;

        switch (handlerView.getId()) {
            case R.id.imghandler1: {

                Rect rectHandlerArea1 = new Rect((int) handlerArea1.getX() - dipValue, (int) handlerArea1.getY() - dipValue, (int) handlerArea1.getX() + handlerArea1.getWidth() + dipValue, (int) handlerArea1.getY() + handlerArea1.getHeight() + dipValue);
                Rect rectHandlerArea2 = new Rect((int) handlerArea2.getX() - dipValue, (int) handlerArea2.getY() - dipValue, (int) handlerArea2.getX() + handlerArea2.getWidth() + dipValue, (int) handlerArea2.getY() + handlerArea2.getHeight() + dipValue);
                Rect rectHandlerArea8 = new Rect((int) handlerArea8.getX() - dipValue, (int) handlerArea8.getY() - dipValue, (int) handlerArea8.getX() + handlerArea8.getWidth() + dipValue, (int) handlerArea8.getY() + handlerArea8.getHeight() + dipValue);

                if (Rect.intersects(rectHandlerArea1, rectHandlerArea2) || Rect.intersects(rectHandlerArea1, rectHandlerArea8)) {
                    if (scaleX > 0 || scaleY > 0) {
                        break;
                    }
                }

                view.setX(view.getX() + scaleX);
                view.setY(view.getY() + scaleY);
                layoutParams.width = (int) (layoutParams.width - scaleX);
                layoutParams.height = (int) (layoutParams.height - scaleY);

                view.setLayoutParams(layoutParams);

                if (currentView instanceof CustomWebRelativeLayout) {
                    ((CustomWebRelativeLayout) currentView).resizeDivWidthAndHeight(view.getWidth(), view.getHeight());
                }

                break;
            }

            case R.id.imghandler2: {

                Rect rectHandlerArea1 = new Rect((int) handlerArea1.getX() - dipValue, (int) handlerArea1.getY() - dipValue, (int) handlerArea1.getX() + handlerArea1.getWidth() + dipValue, (int) handlerArea1.getY() + handlerArea1.getHeight() + dipValue);
                Rect rectHandlerArea8 = new Rect((int) handlerArea8.getX() - dipValue, (int) handlerArea8.getY() - dipValue, (int) handlerArea8.getX() + handlerArea8.getWidth() + dipValue, (int) handlerArea8.getY() + handlerArea8.getHeight() + dipValue);

                if (Rect.intersects(rectHandlerArea1, rectHandlerArea8)) {
                    if (scaleY > 0) {
                        break;
                    }
                }

                view.setX(view.getX());
                view.setY(view.getY() + scaleY);
                layoutParams.width = (int) (layoutParams.width);
                layoutParams.height = (int) (layoutParams.height - scaleY);

                view.setLayoutParams(layoutParams);

                if (currentView instanceof CustomWebRelativeLayout) {
                    ((CustomWebRelativeLayout) currentView).resizeDivWidthAndHeight(view.getWidth(), view.getHeight());
                }

                break;
            }

            case R.id.imghandler3: {

                Rect rectHandlerArea2 = new Rect((int) handlerArea2.getX() - dipValue, (int) handlerArea2.getY() - dipValue, (int) handlerArea2.getX() + handlerArea2.getWidth() + dipValue, (int) handlerArea2.getY() + handlerArea2.getHeight() + dipValue);
                Rect rectHandlerArea3 = new Rect((int) handlerArea3.getX() - dipValue, (int) handlerArea3.getY() - dipValue, (int) handlerArea3.getX() + handlerArea3.getWidth() + dipValue, (int) handlerArea3.getY() + handlerArea3.getHeight() + dipValue);
                Rect rectHandlerArea4 = new Rect((int) handlerArea4.getX() - dipValue, (int) handlerArea4.getY() - dipValue, (int) handlerArea4.getX() + handlerArea4.getWidth() + dipValue, (int) handlerArea4.getY() + handlerArea4.getHeight() + dipValue);

                if (Rect.intersects(rectHandlerArea2, rectHandlerArea3) || Rect.intersects(rectHandlerArea4, rectHandlerArea3)) {
                    if (scaleX < 0 || scaleY > 0) {
                        break;
                    }
                }

                view.setX(view.getX());
                view.setY(view.getY() + scaleY);
                layoutParams.width = (int) (layoutParams.width + scaleX);
                layoutParams.height = (int) (layoutParams.height - scaleY);

                view.setLayoutParams(layoutParams);

                if (currentView instanceof CustomWebRelativeLayout) {
                    ((CustomWebRelativeLayout) currentView).resizeDivWidthAndHeight(view.getWidth(), view.getHeight());
                }

                break;
            }

            case R.id.imghandler4: {

                Rect rectHandlerArea2 = new Rect((int) handlerArea2.getX() - dipValue, (int) handlerArea2.getY() - dipValue, (int) handlerArea2.getX() + handlerArea2.getWidth() + dipValue, (int) handlerArea2.getY() + handlerArea2.getHeight() + dipValue);
                Rect rectHandlerArea3 = new Rect((int) handlerArea3.getX() - dipValue, (int) handlerArea3.getY() - dipValue, (int) handlerArea3.getX() + handlerArea3.getWidth() + dipValue, (int) handlerArea3.getY() + handlerArea3.getHeight() + dipValue);

                if (Rect.intersects(rectHandlerArea2, rectHandlerArea3)) {
                    if (scaleX < 0) {
                        break;
                    }
                }

                view.setX(view.getX());
                view.setY(view.getY());
                layoutParams.width = (int) (layoutParams.width + scaleX);
                layoutParams.height = (int) (layoutParams.height);

                view.setLayoutParams(layoutParams);

                if (currentView instanceof CustomWebRelativeLayout) {
                    ((CustomWebRelativeLayout) currentView).resizeDivWidthAndHeight(view.getWidth(), view.getHeight());
                }

                break;
            }

            case R.id.imghandler5: {

                Rect rectHandlerArea4 = new Rect((int) handlerArea4.getX() - dipValue, (int) handlerArea4.getY() - dipValue, (int) handlerArea4.getX() + handlerArea4.getWidth() + dipValue, (int) handlerArea4.getY() + handlerArea4.getHeight() + dipValue);
                Rect rectHandlerArea5 = new Rect((int) handlerArea5.getX() - dipValue, (int) handlerArea5.getY() - dipValue, (int) handlerArea5.getX() + handlerArea5.getWidth() + dipValue, (int) handlerArea5.getY() + handlerArea5.getHeight() + dipValue);
                Rect rectHandlerArea6 = new Rect((int) handlerArea6.getX() - dipValue, (int) handlerArea6.getY() - dipValue, (int) handlerArea6.getX() + handlerArea6.getWidth() + dipValue, (int) handlerArea6.getY() + handlerArea6.getHeight() + dipValue);

                if (Rect.intersects(rectHandlerArea4, rectHandlerArea5) || Rect.intersects(rectHandlerArea5, rectHandlerArea6)) {
                    if (scaleX < 0 || scaleY < 0) {
                        break;
                    }
                }

                view.setX(view.getX());
                view.setY(view.getY());
                layoutParams.width = (int) (layoutParams.width + scaleX);
                layoutParams.height = (int) (layoutParams.height + scaleY);

                view.setLayoutParams(layoutParams);

                if (currentView instanceof CustomWebRelativeLayout) {
                    ((CustomWebRelativeLayout) currentView).resizeDivWidthAndHeight(view.getWidth(), view.getHeight());
                }

                break;
            }

            case R.id.imghandler6: {

                Rect rectHandlerArea8 = new Rect((int) handlerArea8.getX() - dipValue, (int) handlerArea8.getY() - dipValue, (int) handlerArea8.getX() + handlerArea8.getWidth() + dipValue, (int) handlerArea8.getY() + handlerArea8.getHeight() + dipValue);
                Rect rectHandlerArea7 = new Rect((int) handlerArea7.getX() - dipValue, (int) handlerArea7.getY() - dipValue, (int) handlerArea7.getX() + handlerArea7.getWidth() + dipValue, (int) handlerArea7.getY() + handlerArea7.getHeight() + dipValue);

                if (Rect.intersects(rectHandlerArea8, rectHandlerArea7)) {
                    if (scaleY < 0) {
                        break;
                    }
                }

                view.setX(view.getX());
                view.setY(view.getY());
                layoutParams.width = (int) (layoutParams.width);
                layoutParams.height = (int) (layoutParams.height + scaleY);

                view.setLayoutParams(layoutParams);

                if (currentView instanceof CustomWebRelativeLayout) {
                    ((CustomWebRelativeLayout) currentView).resizeDivWidthAndHeight(view.getWidth(), view.getHeight());
                }

                break;
            }

            case R.id.imghandler7: {

                Rect rectHandlerArea6 = new Rect((int) handlerArea6.getX() - dipValue, (int) handlerArea6.getY() - dipValue, (int) handlerArea6.getX() + handlerArea6.getWidth() + dipValue, (int) handlerArea6.getY() + handlerArea6.getHeight() + dipValue);
                Rect rectHandlerArea8 = new Rect((int) handlerArea8.getX() - dipValue, (int) handlerArea8.getY() - dipValue, (int) handlerArea8.getX() + handlerArea8.getWidth() + dipValue, (int) handlerArea8.getY() + handlerArea8.getHeight() + dipValue);
                Rect rectHandlerArea7 = new Rect((int) handlerArea7.getX() - dipValue, (int) handlerArea7.getY() - dipValue, (int) handlerArea7.getX() + handlerArea7.getWidth() + dipValue, (int) handlerArea7.getY() + handlerArea7.getHeight() + dipValue);

                if (Rect.intersects(rectHandlerArea7, rectHandlerArea8) || Rect.intersects(rectHandlerArea7, rectHandlerArea6)) {
                    if (scaleX > 0 || scaleY < 0) {
                        break;
                    }
                }

                view.setX(view.getX() + scaleX);
                view.setY(view.getY());
                layoutParams.width = (int) (layoutParams.width - scaleX);
                layoutParams.height = (int) (layoutParams.height + scaleY);

                view.setLayoutParams(layoutParams);

                if (currentView instanceof CustomWebRelativeLayout) {
                    ((CustomWebRelativeLayout) currentView).resizeDivWidthAndHeight(view.getWidth(), view.getHeight());
                }

                break;
            }

            case R.id.imghandler8: {

                Rect rectHandlerArea1 = new Rect((int) handlerArea1.getX() - dipValue, (int) handlerArea1.getY() - dipValue, (int) handlerArea1.getX() + handlerArea1.getWidth() + dipValue, (int) handlerArea1.getY() + handlerArea1.getHeight() + dipValue);
                Rect rectHandlerArea2 = new Rect((int) handlerArea2.getX() - dipValue, (int) handlerArea2.getY() - dipValue, (int) handlerArea2.getX() + handlerArea2.getWidth() + dipValue, (int) handlerArea2.getY() + handlerArea2.getHeight() + dipValue);

                if (Rect.intersects(rectHandlerArea1, rectHandlerArea2)) {
                    if (scaleX > 0) {
                        break;
                    }
                }

                view.setX(view.getX() + scaleX);
                view.setY(view.getY());
                layoutParams.width = (int) (layoutParams.width - scaleX);
                layoutParams.height = (int) (layoutParams.height);

                view.setLayoutParams(layoutParams);

                if (currentView instanceof CustomWebRelativeLayout) {
                    ((CustomWebRelativeLayout) currentView).resizeDivWidthAndHeight(view.getWidth(), view.getHeight());
                }

                break;
            }

            default:
                break;
        }
    }

    /**
     * getRecentColorsFromSharedPreferences
     *
     * @param key
     * @return
     */
    public ArrayList<String> getRecentColorsFromSharedPreference(String key) {
        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(ctxBookView);
        ArrayList<String> recentColorList = null;
        try {
            recentColorList = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (recentColorList.size() == 0) {
            recentColorList.add("#B0171F");
            recentColorList.add("#FF82AB");
            recentColorList.add("#0000FF");
            recentColorList.add("#000000");
            recentColorList.add("#006400");
        }
        return recentColorList;
    }

    /**
     * setandUpdateRecentColorsToSharedPreferences
     *
     * @param key
     * @param recentColorList
     * @param recentColor
     */
    public void setandUpdateRecentColorsToSharedPreferences(String key, ArrayList<String> recentColorList, String recentColor) {
        ArrayList<String> tempArrayList = new ArrayList<String>();
        for (int i = 0; i < recentColorList.size(); i++) {
            if (i == 0) {
                tempArrayList.add(recentColor);
            } else {
                tempArrayList.add(recentColorList.get(i - 1));
            }
        }
        recentColorList.clear();
        recentColorList = tempArrayList;

        SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(ctxBookView);
        Editor editor = sharedPreference.edit();
        try {
            editor.putString(key, ObjectSerializer.serialize(recentColorList));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        editor.commit();
    }

    /**
     * Extend Scroll view height to increase page size while moving objects down
     */
    public void extendScrollViewHeight(View touchView, PointF viewCenterPoint) {
        View lastFrameView = null;
        if (touchView.getY() + touchView.getHeight() > ctxBookView.designScrollPageView.getHeight()) {
            float newHeight = touchView.getHeight() - (ctxBookView.designPageLayout.getHeight() - touchView.getY());
            ////System.out.println("newHeight: "+newHeight);
            if (newHeight >= previousScrollHeight) {
                LayoutParams designPageLayoutParams = ctxBookView.designPageLayout.getLayoutParams();
                int designPageLayoutheight = ctxBookView.designPageLayout.getHeight();
                designPageLayoutParams.height = (int) (designPageLayoutheight + newHeight);
                ctxBookView.designPageLayout.setLayoutParams(designPageLayoutParams);

                LayoutParams imgLayoutParams = ctxBookView.bgImgView.getLayoutParams();
                imgLayoutParams.height = (int) (designPageLayoutheight + newHeight);
                ctxBookView.bgImgView.setLayoutParams(imgLayoutParams);

                ctxBookView.designScrollPageView.fullScroll(ScrollView.FOCUS_DOWN);
                previousScrollHeight = newHeight;
            } else {
                ////System.out.println("Else loop: "+ctxBookView.designPageLayout.getHeight());
                for (int i = 0; i < ctxBookView.designPageLayout.getChildCount(); i++) {
                    View childView = ctxBookView.designPageLayout.getChildAt(i);
                    if (touchView != childView && (childView instanceof CustomImageRelativeLayout || childView instanceof CustomWebRelativeLayout)) {
                        if (lastFrameView == null) {
                            lastFrameView = childView;
                        } else if (childView.getY() + childView.getHeight() > lastFrameView.getY() + lastFrameView.getHeight()) {
                            lastFrameView = childView;
                        }
                    }
                }
                if (lastFrameView != null) {
                    if (touchView.getY() + touchView.getHeight() > lastFrameView.getY() + lastFrameView.getHeight()) {
                        LayoutParams designPageLayoutParams = ctxBookView.designPageLayout.getLayoutParams();
                        int designPageLayoutheight = ctxBookView.designPageLayout.getHeight();
                        designPageLayoutParams.height = (int) (designPageLayoutheight + newHeight);
                        ctxBookView.designPageLayout.setLayoutParams(designPageLayoutParams);

                        LayoutParams imgLayoutParams = ctxBookView.bgImgView.getLayoutParams();
                        imgLayoutParams.height = (int) (designPageLayoutheight + newHeight);
                        ctxBookView.bgImgView.setLayoutParams(imgLayoutParams);

                        ////System.out.println("DesignPageLayoutHeight2: "+ctxBookView.designPageLayout.getHeight());
                        ctxBookView.designScrollPageView.fullScroll(ScrollView.FOCUS_DOWN);
                        previousScrollHeight = newHeight;
                    }
                } else {
                    LayoutParams designPageLayoutParams = ctxBookView.designPageLayout.getLayoutParams();
                    int designPageLayoutheight = ctxBookView.designPageLayout.getHeight();
                    designPageLayoutParams.height = (int) (designPageLayoutheight + newHeight);
                    ctxBookView.designPageLayout.setLayoutParams(designPageLayoutParams);

                    LayoutParams imgLayoutParams = ctxBookView.bgImgView.getLayoutParams();
                    imgLayoutParams.height = (int) (designPageLayoutheight + newHeight);
                    ctxBookView.bgImgView.setLayoutParams(imgLayoutParams);

                    ////System.out.println("DesignPageLayoutHeight3: "+ctxBookView.designPageLayout.getHeight());
                    ctxBookView.designScrollPageView.fullScroll(ScrollView.FOCUS_DOWN);
                    previousScrollHeight = newHeight;
                }
            }
        } else if (ctxBookView.designPageLayout.getHeight() > Globals.getDeviceHeight()) {
            if (lastFrameView == null) {
                for (int i = 0; i < ctxBookView.designPageLayout.getChildCount(); i++) {
                    View childView = ctxBookView.designPageLayout.getChildAt(i);
                    if (touchView != childView && (childView instanceof CustomImageRelativeLayout || childView instanceof CustomWebRelativeLayout)) {
                        if (lastFrameView == null) {
                            lastFrameView = childView;
                        } else if (childView.getY() + childView.getHeight() > lastFrameView.getY() + lastFrameView.getHeight()) {
                            lastFrameView = childView;
                        }
                    }
                }
            }
            if (lastFrameView == null || (lastFrameView != null && lastFrameView.getY() + lastFrameView.getHeight() < Globals.getDeviceHeight())) {
                LayoutParams designPageLayoutParams = ctxBookView.designPageLayout.getLayoutParams();
                designPageLayoutParams.height = Globals.getDeviceHeight();
                ctxBookView.designPageLayout.setLayoutParams(designPageLayoutParams);
            }
        }
    }

    /**
     * CheckForSnapBetweenObjects
     *
     * @param touchView
     * @param viewCenterPoint
     * @return
     */
    public Object[] checkForSnapBetweenObjects(View touchView, PointF viewCenterPoint) {
        View childView = null;
        boolean objectsPointSnapped = false;
        for (int i = 0; i < ctxBookView.designPageLayout.getChildCount(); i++) {
            childView = ctxBookView.designPageLayout.getChildAt(i);
            if (touchView != childView && (childView instanceof CustomImageRelativeLayout || childView instanceof CustomWebRelativeLayout)) {
                PointF childViewCenterPoint = new PointF(childView.getX() + (childView.getWidth() / 2), childView.getY() + (childView.getHeight() / 2));

                //Snap the points between objects
                if (viewCenterPoint.x >= childViewCenterPoint.x - snapPoints && viewCenterPoint.x <= childViewCenterPoint.x + snapPoints) {
                    viewCenterPoint.x = childViewCenterPoint.x;
                    objectsPointSnapped = true;
                    return new Object[]{viewCenterPoint, childView, objectsPointSnapped};
                } else if (viewCenterPoint.x - (touchView.getWidth() / 2) >= childView.getX() - snapPoints && viewCenterPoint.x - (touchView.getWidth() / 2) <= childView.getX() + snapPoints) {
                    viewCenterPoint.x = (childViewCenterPoint.x - ((childView.getWidth() - touchView.getWidth()) / 2));
                    objectsPointSnapped = true;
                    return new Object[]{viewCenterPoint, childView, objectsPointSnapped};
                } else if (viewCenterPoint.x + (touchView.getWidth() / 2) >= (childView.getX() + childView.getWidth()) - snapPoints && viewCenterPoint.x + (touchView.getWidth() / 2) <= (childView.getX() + childView.getWidth()) + snapPoints) {
                    viewCenterPoint.x = (childViewCenterPoint.x + ((childView.getWidth() - touchView.getWidth()) / 2));
                    objectsPointSnapped = true;
                    return new Object[]{viewCenterPoint, childView, objectsPointSnapped};
                }
                //Snap the points between objects
                if (viewCenterPoint.y >= childViewCenterPoint.y - snapPoints && viewCenterPoint.y <= childViewCenterPoint.y + snapPoints) {
                    viewCenterPoint.y = childViewCenterPoint.y;
                    objectsPointSnapped = true;
                    return new Object[]{viewCenterPoint, childView, objectsPointSnapped};
                } else if (viewCenterPoint.y - (touchView.getHeight() / 2) >= childView.getY() - snapPoints && viewCenterPoint.y - (touchView.getHeight() / 2) <= childView.getY() + snapPoints) {
                    viewCenterPoint.y = (childViewCenterPoint.y - ((childView.getHeight() - touchView.getHeight()) / 2));
                    objectsPointSnapped = true;
                    return new Object[]{viewCenterPoint, childView, objectsPointSnapped};
                } else if (viewCenterPoint.y + (touchView.getHeight() / 2) >= (childView.getY() + childView.getHeight()) - snapPoints && viewCenterPoint.y + (touchView.getHeight() / 2) <= (childView.getY() + childView.getHeight()) + snapPoints) {
                    viewCenterPoint.y = (childViewCenterPoint.y + ((childView.getHeight() - touchView.getHeight()) / 2));
                    objectsPointSnapped = true;
                    return new Object[]{viewCenterPoint, childView, objectsPointSnapped};
                }
            } else {
                childView = null;
            }
        }
        return new Object[]{viewCenterPoint, childView, objectsPointSnapped};
    }

    /**
     * SnapToObjects
     *
     * @param touchView
     * @param compareView
     */
    public void snapToObjects(View touchView, View compareView) {
        View childView = compareView;
        if (childView != null && touchView != childView && (childView instanceof CustomImageRelativeLayout || childView instanceof CustomWebRelativeLayout)) {
            PointF touchViewCenterPoint = new PointF(touchView.getX() + (touchView.getWidth() / 2), touchView.getY() + (touchView.getHeight() / 2));
            PointF childViewCenterPoint = new PointF(childView.getX() + (childView.getWidth() / 2), childView.getY() + (childView.getHeight() / 2));

            //HorizontalYGridLine
            if ((touchViewCenterPoint.x == childViewCenterPoint.x) || (touchView.getX() == childView.getX()) || touchViewCenterPoint.x + (touchView.getWidth() / 2) == childViewCenterPoint.x + (childView.getWidth() / 2)) {
                float height = touchViewCenterPoint.y - childViewCenterPoint.y;
                if (height < 0) {
                    height = childViewCenterPoint.y - touchViewCenterPoint.y;
                }
                height = height + (touchView.getHeight() / 2 + childView.getHeight() / 2);
                float yposition = 0;
                if (touchView.getY() < childView.getY()) {
                    yposition = touchView.getY();
                } else {
                    yposition = childView.getY();
                }
                //HorizontalCentreGridLine
                if (touchViewCenterPoint.x == childViewCenterPoint.x) {
                    //System.out.println("equal");
                    float xposition = childViewCenterPoint.x;
                    if (horizontalY2GridLineView == null) {
                        horizontalY2GridLineView = new ImageView(ctxBookView);
                        horizontalY2GridLineView.setBackgroundColor(Color.BLUE);
                        ctxBookView.designPageLayout.addView(horizontalY2GridLineView);
                    }
                    horizontalY2GridLineView.setVisibility(View.VISIBLE);
                    horizontalY2GridLineView.setLayoutParams(new RelativeLayout.LayoutParams(gridLineWidth, (int) height));
                    horizontalY2GridLineView.setX(xposition);
                    horizontalY2GridLineView.setY(yposition);
                } else {
                    if (horizontalY2GridLineView != null) {
                        horizontalY2GridLineView.setVisibility(View.GONE);
                    }
                }
                //HorizontalLeftGridLine
                if (touchView.getX() == childView.getX()) {
                    float xposition = childView.getX();
                    if (horizontalY1GridLineView == null) {
                        horizontalY1GridLineView = new ImageView(ctxBookView);
                        horizontalY1GridLineView.setBackgroundColor(Color.BLUE);
                        ctxBookView.designPageLayout.addView(horizontalY1GridLineView);
                    }
                    horizontalY1GridLineView.setVisibility(View.VISIBLE);
                    horizontalY1GridLineView.setLayoutParams(new RelativeLayout.LayoutParams(gridLineWidth, (int) height));
                    horizontalY1GridLineView.setX(xposition);
                    horizontalY1GridLineView.setY(yposition);

                } else {
                    if (horizontalY1GridLineView != null) {
                        horizontalY1GridLineView.setVisibility(View.GONE);
                    }
                }
                //HorizontalRightGridLine
                if (touchViewCenterPoint.x + (touchView.getWidth() / 2) == childViewCenterPoint.x + (childView.getWidth() / 2)) {
                    float xposition = childViewCenterPoint.x + (childView.getWidth() / 2);
                    if (horizontalY3GridLineView == null) {
                        horizontalY3GridLineView = new ImageView(ctxBookView);
                        horizontalY3GridLineView.setBackgroundColor(Color.BLUE);
                        ctxBookView.designPageLayout.addView(horizontalY3GridLineView);
                    }
                    horizontalY3GridLineView.setVisibility(View.VISIBLE);
                    horizontalY3GridLineView.setLayoutParams(new RelativeLayout.LayoutParams(gridLineWidth, (int) height));
                    horizontalY3GridLineView.setX(xposition);
                    horizontalY3GridLineView.setY(yposition);
                } else {
                    if (horizontalY3GridLineView != null) {
                        horizontalY3GridLineView.setVisibility(View.GONE);
                    }
                }
            } else {
                if (horizontalY2GridLineView != null) {
                    horizontalY2GridLineView.setVisibility(View.GONE);
                }
                if (horizontalY1GridLineView != null) {
                    horizontalY1GridLineView.setVisibility(View.GONE);
                }
                if (horizontalY3GridLineView != null) {
                    horizontalY3GridLineView.setVisibility(View.GONE);
                }
            }

            if ((touchViewCenterPoint.y == childViewCenterPoint.y) || (touchView.getY() == childView.getY()) || touchViewCenterPoint.y + (touchView.getHeight() / 2) == childViewCenterPoint.y + (childView.getHeight() / 2)) {

                float width = touchViewCenterPoint.x - childViewCenterPoint.x;
                if (width < 0) {
                    width = childViewCenterPoint.x - touchViewCenterPoint.x;
                }
                width = width + (touchView.getWidth() / 2 + childView.getWidth() / 2);
                float xposition = 0;
                if (touchView.getX() < childView.getX()) {
                    xposition = touchView.getX();
                } else {
                    xposition = childView.getX();
                }
                //VerticalCentreGridLine
                if (touchViewCenterPoint.y == childViewCenterPoint.y) {
                    //System.out.println("equal");
                    float yposition = childViewCenterPoint.y;
                    if (horizontalX2GridLineView == null) {
                        horizontalX2GridLineView = new ImageView(ctxBookView);
                        horizontalX2GridLineView.setBackgroundColor(Color.BLUE);
                        ctxBookView.designPageLayout.addView(horizontalX2GridLineView);
                    }
                    horizontalX2GridLineView.setVisibility(View.VISIBLE);
                    horizontalX2GridLineView.setLayoutParams(new RelativeLayout.LayoutParams((int) width, gridLineWidth));
                    horizontalX2GridLineView.setX(xposition);
                    horizontalX2GridLineView.setY(yposition);
                } else {
                    if (horizontalX2GridLineView != null) {
                        horizontalX2GridLineView.setVisibility(View.GONE);
                    }
                }
                //VerticalTopGridLine
                if (touchView.getY() == childView.getY()) {
                    float yposition = childView.getY();
                    if (horizontalX1GridLineView == null) {
                        horizontalX1GridLineView = new ImageView(ctxBookView);
                        horizontalX1GridLineView.setBackgroundColor(Color.BLUE);
                        ctxBookView.designPageLayout.addView(horizontalX1GridLineView);
                    }
                    horizontalX1GridLineView.setVisibility(View.VISIBLE);
                    horizontalX1GridLineView.setLayoutParams(new RelativeLayout.LayoutParams((int) width, gridLineWidth));
                    horizontalX1GridLineView.setX(xposition);
                    horizontalX1GridLineView.setY(yposition);

                } else {
                    if (horizontalX1GridLineView != null) {
                        horizontalX1GridLineView.setVisibility(View.GONE);
                    }
                }
                //VerticalBottomGridLine
                if (touchViewCenterPoint.y + (touchView.getHeight() / 2) == childViewCenterPoint.y + (childView.getHeight() / 2)) {
                    float yposition = childViewCenterPoint.y + (childView.getHeight() / 2);
                    if (horizontalX3GridLineView == null) {
                        horizontalX3GridLineView = new ImageView(ctxBookView);
                        horizontalX3GridLineView.setBackgroundColor(Color.BLUE);
                        ctxBookView.designPageLayout.addView(horizontalX3GridLineView);
                    }
                    horizontalX3GridLineView.setVisibility(View.VISIBLE);
                    horizontalX3GridLineView.setLayoutParams(new RelativeLayout.LayoutParams((int) width, gridLineWidth));
                    horizontalX3GridLineView.setX(xposition);
                    horizontalX3GridLineView.setY(yposition);
                } else {
                    if (horizontalX3GridLineView != null) {
                        horizontalX3GridLineView.setVisibility(View.GONE);
                    }
                }
            } else {
                if (horizontalX2GridLineView != null) {
                    horizontalX2GridLineView.setVisibility(View.GONE);
                }
                if (horizontalX1GridLineView != null) {
                    horizontalX1GridLineView.setVisibility(View.GONE);
                }
                if (horizontalX3GridLineView != null) {
                    horizontalX3GridLineView.setVisibility(View.GONE);
                }
            }
        } else {
            removeObjectGridLines();
        }
    }

    /**
     * SnapToGrid
     *
     * @param view
     * @return
     */
    public void snapToGrid(View view) {
        float designPageLayoutXpos = ctxBookView.designPageLayout.getWidth() / 2;
        float designPageLayoutYpos = ctxBookView.designPageLayout.getHeight() / 2;
        float viewCentreX = view.getX() + (view.getWidth() / 2);
        float viewCentreY = view.getY() + (view.getHeight() / 2);
        PointF viewCenterPoint = new PointF(viewCentreX, viewCentreY);

        //Horizontal Center Line
        if (viewCenterPoint.equals(designPageLayoutXpos, viewCenterPoint.y)) {
            displayHorizontalLineView(designPageLayoutXpos);
        }
        //Horizontal Left Center Line
        else if (viewCenterPoint.equals(designPageLayoutXpos / 2, viewCenterPoint.y)) {
            displayHorizontalLineView(designPageLayoutXpos / 2);
        }
        //Horizontal Right Center Line
        else if (viewCenterPoint.equals(designPageLayoutXpos + (designPageLayoutXpos / 2), viewCenterPoint.y)) {
            displayHorizontalLineView(designPageLayoutXpos + (designPageLayoutXpos / 2));
        } else {
            if (horizontalLineView != null) {
                horizontalLineView.setVisibility(View.GONE);
            }
        }

        //Vertical Center Line
        if (viewCenterPoint.equals(viewCenterPoint.x, designPageLayoutYpos)) {
            displayVerticalLineView(designPageLayoutYpos);
        }
        //Vertical Top Center Line
        else if (viewCenterPoint.equals(viewCenterPoint.x, designPageLayoutYpos / 2)) {
            displayVerticalLineView(designPageLayoutYpos / 2);
        }
        //Vertical Bottom Center Line
        else if (viewCenterPoint.equals(viewCenterPoint.x, designPageLayoutYpos + (designPageLayoutYpos / 2))) {
            displayVerticalLineView(designPageLayoutYpos + (designPageLayoutYpos / 2));
        } else {
            if (verticalLineView != null) {
                verticalLineView.setVisibility(View.GONE);
            }
        }
    }

    /**
     * display horizontal line view
     *
     * @param position
     */
    private void displayHorizontalLineView(float position) {
        if (horizontalLineView == null) {
            horizontalLineView = new ImageView(ctxBookView);
            horizontalLineView.setBackgroundColor(Color.BLUE);
            ctxBookView.designPageLayout.addView(horizontalLineView, new LayoutParams(gridLineWidth, ctxBookView.designPageLayout.getHeight()));
        }
        horizontalLineView.setX(position);
        horizontalLineView.setY(0);
        horizontalLineView.setVisibility(View.VISIBLE);
    }

    /**
     * display vertical line view
     *
     * @param position
     */
    private void displayVerticalLineView(float position) {
        if (verticalLineView == null) {
            verticalLineView = new ImageView(ctxBookView);
            verticalLineView.setBackgroundColor(Color.BLUE);
            ctxBookView.designPageLayout.addView(verticalLineView, new LayoutParams(ctxBookView.designPageLayout.getWidth(), gridLineWidth));
        }
        verticalLineView.setX(0);
        verticalLineView.setY(position);
        verticalLineView.setVisibility(View.VISIBLE);
    }

    /**
     * remove snap grid Lines
     */
    public void removeGridLines() {
        if (horizontalLineView != null) {
            horizontalLineView.setVisibility(View.GONE);
        }
        if (verticalLineView != null) {
            verticalLineView.setVisibility(View.GONE);
        }
    }

    /**
     * remove snap object grid lines
     */
    public void removeObjectGridLines() {
        if (horizontalY2GridLineView != null) {
            horizontalY2GridLineView.setVisibility(View.GONE);
        }
        if (horizontalY1GridLineView != null) {
            horizontalY1GridLineView.setVisibility(View.GONE);
        }
        if (horizontalY3GridLineView != null) {
            horizontalY3GridLineView.setVisibility(View.GONE);
        }
        if (horizontalX2GridLineView != null) {
            horizontalX2GridLineView.setVisibility(View.GONE);
        }
        if (horizontalX1GridLineView != null) {
            horizontalX1GridLineView.setVisibility(View.GONE);
        }
        if (horizontalX3GridLineView != null) {
            horizontalX3GridLineView.setVisibility(View.GONE);
        }
    }

    /**
     * check for snap points for grid
     *
     * @param viewCenterPoint
     * @return
     */
    public Object[] checkForSnapPointsForGrid(PointF viewCenterPoint) {
        boolean gridPointsSnapped = false;
        float designPageLayoutXpos = ctxBookView.designPageLayout.getWidth() / 2;
        float designPageLayoutYpos = ctxBookView.designPageLayout.getHeight() / 2;
        if (viewCenterPoint.y >= designPageLayoutYpos - snapPoints && viewCenterPoint.y <= designPageLayoutYpos + snapPoints) {
            viewCenterPoint.y = designPageLayoutYpos;
            gridPointsSnapped = true;
        } else if (viewCenterPoint.y >= (designPageLayoutYpos / 2) - snapPoints && viewCenterPoint.y <= (designPageLayoutYpos / 2) + snapPoints) {
            viewCenterPoint.y = designPageLayoutYpos / 2;
            gridPointsSnapped = true;
        } else if (viewCenterPoint.y >= (designPageLayoutYpos + (designPageLayoutYpos / 2)) - snapPoints && viewCenterPoint.y <= (designPageLayoutYpos + (designPageLayoutYpos / 2)) + snapPoints) {
            viewCenterPoint.y = designPageLayoutYpos + (designPageLayoutYpos / 2);
            gridPointsSnapped = true;
        }
        if (viewCenterPoint.x >= designPageLayoutXpos - snapPoints && viewCenterPoint.x <= designPageLayoutXpos + snapPoints) {
            viewCenterPoint.x = designPageLayoutXpos;
            gridPointsSnapped = true;
        } else if (viewCenterPoint.x >= (designPageLayoutXpos / 2) - snapPoints && viewCenterPoint.x <= (designPageLayoutXpos / 2) + snapPoints) {
            viewCenterPoint.x = designPageLayoutXpos / 2;
            gridPointsSnapped = true;
        } else if (viewCenterPoint.x >= (designPageLayoutXpos + (designPageLayoutXpos / 2)) - snapPoints && viewCenterPoint.x <= (designPageLayoutXpos + (designPageLayoutXpos / 2)) + snapPoints) {
            viewCenterPoint.x = designPageLayoutXpos + (designPageLayoutXpos / 2);
            gridPointsSnapped = true;
        }
        //return viewCenterPoint;
        return new Object[]{viewCenterPoint, gridPointsSnapped};
    }

    /**
     * this sets the undo and redo button to enable or disable
     *
     * @param eDbtnUndo
     * @param eDbtnRedo
     */
    public void setUndoRedoBtnEnableOrDisable(final boolean eDbtnUndo, final boolean eDbtnRedo) {
        ctxBookView.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                ctxBookView.btnUndo.setEnabled(eDbtnUndo);
                ctxBookView.btnRedo.setEnabled(eDbtnRedo);
            }
        });
    }

    /**
     * Sets the undo and redo Object and action name
     */
    public void createAndRegisterUndoRedoObjectData(UndoRedoObjectData uRObjData, String actionName, String objState) {
        UndoRedoObjectAction urObJAction = new UndoRedoObjectAction();
        urObJAction.setRuObjActionName(actionName);
        urObJAction.setRuObject(uRObjData);
        urObJAction.setRuObjState(objState);
        undoManager.setUndoRedoAction(urObJAction);
    }

    /**
     * this sets undo redo for background image content
     *
     * @param status
     * @param objState
     */
    public void undoRedoForBackgroundImageContent(String status, String objState) {
        UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
        String prevBgImgPath = null;
        int pageNo = Integer.parseInt(ctxBookView.currentPageNumber);
        if (pageNo == 1 || pageNo == ctxBookView.pageCount) {
            if (checkPageBGexists(pageBG_PATH)) {
                bgImageContentChangeCount++;
                prevBgImgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/tempFiles/pageBg_" + bgImageContentChangeCount + ".png";
                UserFunctions.copyFiles(pageBG_PATH, prevBgImgPath);
                uRPrevObjData.setuRObjDataSize(scaleToFitOrTiled);
            } else if (checkPageColorexists(ctxBookView.currentPageNumber, enrichedPageId)) {
                prevBgImgPath = pageBackgroundColor;
                uRPrevObjData.setuRObjPickedColorValue(true);
            } else {
                prevBgImgPath = defaultBgImagePath;
                uRPrevObjData.setuRObjDataSize(scaleToFitOrTiled);
            }
        } else {
            if (checkPageBGexists(pageBG_PATH)) {
                bgImageContentChangeCount++;
                prevBgImgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/tempFiles/pageBg_" + bgImageContentChangeCount + ".png";
                UserFunctions.copyFiles(pageBG_PATH, prevBgImgPath);
                uRPrevObjData.setuRObjDataSize(scaleToFitOrTiled);
            } else if (checkPageColorexists(ctxBookView.currentPageNumber, enrichedPageId)) {
                prevBgImgPath = pageBackgroundColor;
                uRPrevObjData.setuRObjPickedColorValue(true);
            } else if (checkPageBG_ALLexists(pageNo, pageBG_ALL_PATH)) {
                bgImageContentChangeCount++;
                prevBgImgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/tempFiles/pageBgAll_" + bgImageContentChangeCount + ".png";
                UserFunctions.copyFiles(pageBG_ALL_PATH, prevBgImgPath);
                uRPrevObjData.setuRObjDataSize(scaleToFitOrTiled);
            } else {
                prevBgImgPath = defaultBgImagePath;
                uRPrevObjData.setuRObjDataSize(scaleToFitOrTiled);
            }
        }
        uRPrevObjData.setuRPageNoObjDataStatus(status);
        uRPrevObjData.setuRObjDataContent(prevBgImgPath);
        createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_BG_IMAGE_CHANGED, Globals.OBJECT_PREVIOUS_STATE);
    }

    /**
     * this performs the undo and redo action for the object
     *
     * @param undoRedoObject
     */
    public void performUndoRedoActioForObjects(UndoRedoObjectAction undoRedoObject) {
        if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_DELETED) {
            //System.out.println("objectDleted");
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            View uRObject = uRObjectData.getuRObjDataCustomView();
            if (uRObject instanceof CustomImageRelativeLayout) {
                ctxBookView.designPageLayout.removeView(uRObject);
                objectListArray.remove(uRObject);
                deletedObjectsListArray.add(uRObject);
            } else if (uRObject instanceof CustomWebRelativeLayout) {
                CustomWebRelativeLayout urWebObj = (CustomWebRelativeLayout) uRObject;
                if (urWebObj.getObjType().equals(Globals.objType_pageNoText)) {
                    if (urWebObj.getObjType().equals(Globals.objType_pageNoText)) {
                        if (uRObjectData.getuRPageNoObjDataStatus().equals("apply")) {
                            deleteCurrentSelectedView(pageNumberObject);
                            pageNumberObject = null;
                        } else if (uRObjectData.getuRPageNoObjDataStatus().equals("applyAll")) {
                            deleteCurrentSelectedView(pageNumberObject);
                            deletePageNumberObjectFromAllPages();
                            pageNumberObject = null;
                        }
                    }
                } else {
                    ctxBookView.designPageLayout.removeView(urWebObj);
                    objectListArray.remove(urWebObj);
                    deletedObjectsListArray.add(urWebObj);
                }
            }else if (uRObject instanceof MindmapPlayer){
                MindmapPlayer urWebObj = (MindmapPlayer) uRObject;
                ctxBookView.designPageLayout.removeView(urWebObj);
                objectListArray.remove(urWebObj);
                deletedObjectsListArray.add(urWebObj);
            }
            removeHandlerArea();
        } else if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_CREATED) {
            //System.out.println("objectCreated");
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            View uRObject = uRObjectData.getuRObjDataCustomView();
            if (uRObject instanceof CustomImageRelativeLayout) {
                CustomImageRelativeLayout urImaObj = (CustomImageRelativeLayout) uRObject;
                addImageViewLayout(urImaObj);
                deletedObjectsListArray.remove(urImaObj);
                setCurrentView((CustomImageRelativeLayout) urImaObj);
                makeSelectedArea((CustomImageRelativeLayout) urImaObj);
            } else if (uRObject instanceof CustomWebRelativeLayout) {
                CustomWebRelativeLayout urWebObj = (CustomWebRelativeLayout) uRObject;
                CustomWebRelativeLayout newUrWebObj = null;
                if (urWebObj.getObjType().equals(Globals.objType_pageNoText)) {
                    if (uRObjectData.getuRPageNoObjDataStatus().equals("apply")) {
                        newUrWebObj = ctxBookView.addNewWebRL(uRObjectData.getuRObjDataLocation(), uRObjectData.getuRObjDataSize(), uRObjectData.getuRObjDataType(), uRObjectData.getuRObjDataContent());
                        uRObjectData.setuRObjDataUniqueId(newUrWebObj.getObjectUniqueId());
                        uRObjectData.setuRObjDataCustomView(newUrWebObj);
                    } else if (uRObjectData.getuRPageNoObjDataStatus().equals("applyAll")) {
                        newUrWebObj = ctxBookView.addNewWebRL(uRObjectData.getuRObjDataLocation(), uRObjectData.getuRObjDataSize(), uRObjectData.getuRObjDataType(), uRObjectData.getuRObjDataContent());
                        ctxBookView.addPageNoObjectToAllPage(uRObjectData.getuRObjDataLocation(), uRObjectData.getuRObjDataSize(), uRObjectData.getuRObjDataType(), uRObjectData.getuRObjDataContent());
                        uRObjectData.setuRObjDataUniqueId(newUrWebObj.getObjectUniqueId());
                        uRObjectData.setuRObjDataCustomView(newUrWebObj);
                    }
                    setCurrentView(newUrWebObj);
                    makeSelectedArea(newUrWebObj);
                } else {
                    addWebViewLayout(urWebObj);
                    deletedObjectsListArray.remove(urWebObj);
                    setCurrentView((CustomWebRelativeLayout) urWebObj);
                    makeSelectedArea((CustomWebRelativeLayout) urWebObj);
                }
            }else if (uRObject instanceof MindmapPlayer) {
                MindmapPlayer urMpObj = (MindmapPlayer) uRObject;
                addMindMapLayout(urMpObj);
                deletedObjectsListArray.remove(urMpObj);
                setCurrentView((MindmapPlayer) urMpObj);
                makeSelectedArea((MindmapPlayer) urMpObj);
            }
        } else if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_MOVED) {
            //System.out.println("objectMoved");
            View childView = compareViewsAndGetViewFromCurrentLayout(undoRedoObject);
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            if (uRObjectData.getuRObjDataType() != null && uRObjectData.getuRObjDataType().equals(Globals.objType_pageNoText)) {
                childView = pageNumberObject;
            }
            if (childView instanceof CustomImageRelativeLayout) {
                ((CustomImageRelativeLayout) childView).setXAndYPos(uRObjectData.getuRObjDataXPos(), uRObjectData.getuRObjDataYPos());
                setCurrentView((CustomImageRelativeLayout) childView);
                makeSelectedArea((CustomImageRelativeLayout) childView);
            } else if (childView instanceof CustomWebRelativeLayout) {
                CustomWebRelativeLayout urWebObj = (CustomWebRelativeLayout) childView;
                if (urWebObj.getObjType().equals(Globals.objType_pageNoText)) {
                    if (uRObjectData.getuRPageNoObjDataStatus().equals("apply")) {
                        String location = uRObjectData.getuRObjDataXPos() + "|" + uRObjectData.getuRObjDataYPos();
                        ctxBookView.updatePosPageNumberInPage(location);
                    } else if (uRObjectData.getuRPageNoObjDataStatus().equals("applyAll")) {
                        String location = uRObjectData.getuRObjDataXPos() + "|" + uRObjectData.getuRObjDataYPos();
                        ctxBookView.updatePosPageNumberInAllPages(location);
                    }
                } else {
                    urWebObj.setXAndYPos(uRObjectData.getuRObjDataXPos(), uRObjectData.getuRObjDataYPos());
                    setCurrentView(urWebObj);
                    makeSelectedArea(urWebObj);
                }
            }else if (childView instanceof MindmapPlayer) {
                ((MindmapPlayer) childView).setXAndYPos(uRObjectData.getuRObjDataXPos(), uRObjectData.getuRObjDataYPos());
                setCurrentView((MindmapPlayer) childView);
                makeSelectedArea((MindmapPlayer) childView);
            }
        } else if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_CONTENT_EDITED) {
            View childView = compareViewsAndGetViewFromCurrentLayout(undoRedoObject);
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            if (uRObjectData.getuRObjDataType().equals(Globals.objType_pageNoText)) {
                childView = pageNumberObject;
            }
            if (childView instanceof CustomWebRelativeLayout) {
                if (uRObjectData.getuRObjDataType().equals(Globals.objType_pageNoText)) {
                    int currentPageNo = Integer.parseInt(currentPageStr) - 1;
                    ((CustomWebRelativeLayout) childView).loadObjContentToWebView(uRObjectData.getuRObjDataContent() + currentPageNo + "</div>");
                    if (uRObjectData.getuRPageNoObjDataStatus().equals("apply")) {
                        ((CustomWebRelativeLayout) childView).pageNoContentUpdateForCurrentPage(uRObjectData.getuRObjDataContent());
                    } else if (uRObjectData.getuRPageNoObjDataStatus().equals("applyAll")) {
                        ((CustomWebRelativeLayout) childView).pageNoContentUpdateForAllPages(uRObjectData.getuRObjDataContent());
                    }
                } else {
                    ((CustomWebRelativeLayout) childView).loadObjContentToWebView(uRObjectData.getuRObjDataContent());
                    ((CustomWebRelativeLayout) childView).objContent = uRObjectData.getuRObjDataContent();
                }

                if (uRObjectData.getuRObjDataType() != null && uRObjectData.getuRObjDataType().equals("WebTable") && uRObjectData.getuRObjDataWidth() != 0 && uRObjectData.getuRObjDataHeight() != 0) {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(uRObjectData.getuRObjDataWidth(), uRObjectData.getuRObjDataHeight());
                    ((CustomWebRelativeLayout) childView).setWebTableTransformation(uRObjectData.getuRObjDataWebTableRow(), uRObjectData.getuRObjDataWebTableColumn(), layoutParams);
                }
                setCurrentView((CustomWebRelativeLayout) childView);
                makeSelectedArea((CustomWebRelativeLayout) childView);
            } else if (childView instanceof CustomImageRelativeLayout) {
                String uRObjPathContent = uRObjectData.getuRObjDataContent();
                String objType = uRObjectData.getuRObjDataType();
                if (objType.equals("Image")) {
                    ((CustomImageRelativeLayout) childView).SaveUriFile(uRObjPathContent);
                    ((CustomImageRelativeLayout) childView).setImageBackground(uRObjPathContent);
                } else if (objType.equals("DrawnImage")) {
                    if (new File(uRObjPathContent).exists()) {
                        ((CustomImageRelativeLayout) childView).SaveUriFile(uRObjPathContent);
                    } else {
                        new File(((CustomImageRelativeLayout) childView).getObjectPathContent()).delete();
                    }
                    ((CustomImageRelativeLayout) childView).setBackgroundImageForDrawing();
                } else if (objType.equals("YouTube")) {
                    String videoThumbPath = uRObjectData.getuRObjDataVideoThumbPath();
                    String toVideoThumbPath = ((CustomImageRelativeLayout) childView).getPathByUniqueId();
                    if (new File(uRObjPathContent).exists()) {
                        ((CustomImageRelativeLayout) childView).SaveUriFile(uRObjPathContent);
                        UserFunctions.copyFiles(videoThumbPath, toVideoThumbPath);
                    } else {
                        new File(toVideoThumbPath).delete();
                        new File(((CustomImageRelativeLayout) childView).getObjectPathContent()).delete();
                    }
                    ((CustomImageRelativeLayout) childView).setVideoThumbnail(toVideoThumbPath);
                } else if (objType.equals("IFrame") || objType.equals("EmbedCode")) {
                    ((CustomImageRelativeLayout) childView).setObjectPathContent(uRObjPathContent);
                }
                setCurrentView((CustomImageRelativeLayout) childView);
                makeSelectedArea((CustomImageRelativeLayout) childView);
            }else if (childView instanceof MindmapPlayer){
                ((MindmapPlayer) childView).setObjContent(uRObjectData.getuRObjDataContent());
                String url = Globals.TARGET_BASE_MINDMAP_PATH + uRObjectData.getuRObjDataContent();
                RelativeLayout layout= (RelativeLayout) ((MindmapPlayer) childView).getChildAt(0);
                TwoDScrollView scrollView= (TwoDScrollView) (layout).getChildAt(0);
                RelativeLayout drawingView= (RelativeLayout) (scrollView).getChildAt(0);
                LoadMindMapContent content=new LoadMindMapContent(scrollView,drawingView,ctxBookView);
                content.loadXmlData(url, false);
                setCurrentView((MindmapPlayer) childView);
                makeSelectedArea((MindmapPlayer) childView);
            }
        } else if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_CONTENT_URL_EDITED) {
            View childView = compareViewsAndGetViewFromCurrentLayout(undoRedoObject);
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            String uRObjPathContent = uRObjectData.getuRObjDataContent();
            String objType = uRObjectData.getuRObjDataType();
            String videoThumbPath = uRObjectData.getuRObjDataVideoThumbPath();
            String toVideoThumbPath = ((CustomImageRelativeLayout) childView).getPathByUniqueId();
            if (objType.equals("YouTube")) {
                if (new File(videoThumbPath).exists()) {
                    UserFunctions.copyFiles(videoThumbPath, toVideoThumbPath);
                    ((CustomImageRelativeLayout) childView).setObjectPathContent(uRObjPathContent);
                } else {
                    new File(toVideoThumbPath).delete();
                    ((CustomImageRelativeLayout) childView).setObjectPathContent("");
                }
                ((CustomImageRelativeLayout) childView).setVideoThumbnail(toVideoThumbPath);
            }
        } else if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_SCALED) {
            View childView = compareViewsAndGetViewFromCurrentLayout(undoRedoObject);
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            if (childView instanceof CustomImageRelativeLayout) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(uRObjectData.getuRObjDataWidth(), uRObjectData.getuRObjDataHeight());
                ((CustomImageRelativeLayout) childView).setTransformation(uRObjectData.getuRObjDataXPos(), uRObjectData.getuRObjDataYPos(), layoutParams);
                setCurrentView((CustomImageRelativeLayout) childView);
                makeSelectedArea((CustomImageRelativeLayout) childView);
            } else if (childView instanceof CustomWebRelativeLayout) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(uRObjectData.getuRObjDataWidth(), uRObjectData.getuRObjDataHeight());
                ((CustomWebRelativeLayout) childView).setTransformation(uRObjectData.getuRObjDataXPos(), uRObjectData.getuRObjDataYPos(), layoutParams);
                setCurrentView((CustomWebRelativeLayout) childView);
                makeSelectedArea((CustomWebRelativeLayout) childView);
            } else if (childView instanceof MindmapPlayer) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(uRObjectData.getuRObjDataWidth(), uRObjectData.getuRObjDataHeight());
                ((MindmapPlayer) childView).setTransformation(uRObjectData.getuRObjDataXPos(), uRObjectData.getuRObjDataYPos(), layoutParams);
                setCurrentView((MindmapPlayer) childView);
                makeSelectedArea((MindmapPlayer) childView);
            }
        } else if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_LOCKED) {
            View childView = compareViewsAndGetViewFromCurrentLayout(undoRedoObject);
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            if (childView instanceof CustomImageRelativeLayout) {
                ((CustomImageRelativeLayout) childView).setObjLocked(uRObjectData.isuRObjDataLocked());
                setCurrentView((CustomImageRelativeLayout) childView);
                makeSelectedArea((CustomImageRelativeLayout) childView);
            } else if (childView instanceof CustomWebRelativeLayout) {
                ((CustomWebRelativeLayout) childView).setObjLocked(uRObjectData.isuRObjDataLocked());
                setCurrentView((CustomWebRelativeLayout) childView);
                makeSelectedArea((CustomWebRelativeLayout) childView);
            }
        } else if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_SEQUENTIAL_ID_CHANGED) {
            View childView = compareViewsAndGetViewFromCurrentLayout(undoRedoObject);
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            if (uRObjectData.getuRObjDataType() != null && uRObjectData.getuRObjDataType().equals(Globals.objType_pageNoText)) {
                childView = pageNumberObject;
            }
            if (childView instanceof CustomImageRelativeLayout) {
                int urSeqId = uRObjectData.getuRObjDataSequentialId();
                int chSeqId = ((CustomImageRelativeLayout) childView).getObjectSequentialId();
                if (urSeqId > chSeqId) {
                    bringViewToFront(childView, urSeqId);
                } else {
                    sendViewToBack(childView, urSeqId);
                }
                setCurrentView((CustomImageRelativeLayout) childView);
                makeSelectedArea((CustomImageRelativeLayout) childView);
            } else if (childView instanceof CustomWebRelativeLayout) {
                int urSeqId = uRObjectData.getuRObjDataSequentialId();
                int chSeqId = ((CustomWebRelativeLayout) childView).getObjectSequentialId();
                if (urSeqId > chSeqId) {
                    bringViewToFront(childView, urSeqId);
                } else {
                    sendViewToBack(childView, urSeqId);
                }
                setCurrentView((CustomWebRelativeLayout) childView);
                makeSelectedArea((CustomWebRelativeLayout) childView);
            }
        } else if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_CLEAR_ALL_CREATED) {
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            ArrayList<View> uRObjDataViewArrayList = uRObjectData.getuRObjDataViewArrayList();
            for (int i = 0; i < uRObjDataViewArrayList.size(); i++) {
                View uRObject = uRObjDataViewArrayList.get(i);
                if (uRObject instanceof CustomImageRelativeLayout) {
                    CustomImageRelativeLayout urImaObj = (CustomImageRelativeLayout) uRObject;
                    addImageViewLayout(urImaObj);
                    deletedObjectsListArray.remove(urImaObj);
                } else if (uRObject instanceof CustomWebRelativeLayout) {
                    CustomWebRelativeLayout urWebObj = (CustomWebRelativeLayout) uRObject;
                    addWebViewLayout(urWebObj);
                    deletedObjectsListArray.remove(urWebObj);
                }else if (uRObject instanceof MindmapPlayer) {
                    MindmapPlayer player = (MindmapPlayer) uRObject;
                    addMindMapLayout(player);
                    deletedObjectsListArray.remove(player);
                }
            }
        } else if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_CLEAR_ALL_DELETED) {
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            ArrayList<View> uRObjDataViewArrayList = uRObjectData.getuRObjDataViewArrayList();
            for (int i = 0; i < uRObjDataViewArrayList.size(); i++) {
                View uRObject = uRObjDataViewArrayList.get(i);
                ctxBookView.designPageLayout.removeView(uRObject);
                objectListArray.remove(uRObject);
                deletedObjectsListArray.add(uRObject);
                removeHandlerArea();
            }
        } else if (undoRedoObject.getRuObjActionName() == Globals.OBJECT_BG_IMAGE_CHANGED) {
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            String srcImagContent = uRObjectData.getuRObjDataContent();
            if (uRObjectData.getuRPageNoObjDataStatus() == "apply") {
                if (uRObjectData.isuRObjPickedColorValue()) {
                    ctxBookView.DeleteCurrentPageBackground();
                    int color = Integer.parseInt(srcImagContent);
                    setPageBackgroundWithColor(color);
                    ctxBookView.pageBackgroundColorEntryinDB(color, currentPageStr);
                } else {
                    String size = uRObjectData.getuRObjDataSize();
                    ctxBookView.ApplyPageBackground(srcImagContent, size, Globals.OBJTYPE_PAGEBG);
                }
            } else if (uRObjectData.getuRPageNoObjDataStatus() == "applyAll") {
                if (uRObjectData.isuRObjPickedColorValue()) {
                    ctxBookView.DeleteApplyApplyAllPageBackgrounds();
                    int color = Integer.parseInt(srcImagContent);
                    setPageBackgroundWithColor(color);
                    ctxBookView.pageBackgroundColorEntryinDB(color, "0");
                } else {
                    String size = uRObjectData.getuRObjDataSize();
                    ctxBookView.ApplyAllPageBackground(srcImagContent, size);
                }
            } else if (uRObjectData.getuRPageNoObjDataStatus() == "Tiled") {
                String objType = uRObjectData.getuRObjDataType();
                //Apply scaleToFit mode
                applyTiledToPageBackground(srcImagContent, pNo);
                //Upade in db with scaleToFit mode
                db.updateTiledOrScaleToFitPageBackground(ctxBookView.currentBook.getBookID(), objType, pNo, "Tiled");
            } else if (uRObjectData.getuRPageNoObjDataStatus() == "ScaleToFit") {
                String objType = uRObjectData.getuRObjDataType();
                //Apply scaleToFit mode
                applyScaleTofitPageBackground(srcImagContent, pNo);
                //Upade in db with scaleToFit mode
                db.updateTiledOrScaleToFitPageBackground(ctxBookView.currentBook.getBookID(), objType, pNo, "ScaleToFit");
            } else if (uRObjectData.getuRPageNoObjDataStatus() == "Rotated") {
                int angle = uRObjectData.getuRObjectDataRotatedAngle();
                rotateBackground(srcImagContent, pNo, angle, true);
            } else if (uRObjectData.getuRPageNoObjDataStatus() == "remove") {
                ctxBookView.DeleteCurrentPageBackground();
                setBackgroundImage(currentPageStr, false);
            } else if (uRObjectData.getuRPageNoObjDataStatus() == "removeAll") {
                ctxBookView.DeleteOnlyAllPageBackgrounds();
                setBackgroundImage(currentPageStr, false);
            }
        }
    }

    /**
     * this compares the view in the layout and gets the exact view
     *
     * @param undoRedoObject
     * @return
     */
    private View compareViewsAndGetViewFromCurrentLayout(UndoRedoObjectAction undoRedoObject) {
        for (int i = 0; i < ctxBookView.designPageLayout.getChildCount(); i++) {
            View childView = ctxBookView.designPageLayout.getChildAt(i);
            UndoRedoObjectData uRObjectData = undoRedoObject.getRuObject();
            if (childView instanceof CustomImageRelativeLayout) {
                int chObjectUniqueId = ((CustomImageRelativeLayout) childView).getObjectUniqueId();
                int urObjectUniqueId = uRObjectData.getuRObjDataUniqueId();
                if (chObjectUniqueId == urObjectUniqueId) {
                    return childView;
                }
            } else if (childView instanceof CustomWebRelativeLayout) {
                int chObjectUniqueId = ((CustomWebRelativeLayout) childView).getObjectUniqueId();
                int urObjectUniqueId = uRObjectData.getuRObjDataUniqueId();
                if (chObjectUniqueId == urObjectUniqueId) {
                    return childView;
                }
            }else if (childView instanceof MindmapPlayer) {
                int chObjectUniqueId = ((MindmapPlayer) childView).getObjectUniqueId();
                int urObjectUniqueId = uRObjectData.getuRObjDataUniqueId();
                if (chObjectUniqueId == urObjectUniqueId) {
                    return childView;
                }
            }
        }
        return null;
    }

    /**
     * this performs the undo
     */
    public void undo() {
        undoManager.shiftPointerOnUndo();
    }

    /**
     * performs redo
     */
    public void redo() {
        undoManager.shiftPointerOnRedo();
    }

    /**
     * @param view
     * @param event
     * @author HandlerArea OnTouch Methods
     */

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int action = event.getAction();
        float positionX = event.getRawX();
        float positionY = event.getRawY();

        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                if (currentView instanceof CustomImageRelativeLayout) {
                    ((CustomImageRelativeLayout) currentView).setTransformationValues(currentView.getX(), currentView.getY(), (android.widget.RelativeLayout.LayoutParams) currentView.getLayoutParams());
                    ((CustomImageRelativeLayout) currentView).mImagePrevXpos = ((CustomImageRelativeLayout) currentView).mImageXpos;
                    ((CustomImageRelativeLayout) currentView).mImagePrevYpos = ((CustomImageRelativeLayout) currentView).mImageYpos;
                    ((CustomImageRelativeLayout) currentView).mImagePrevWidth = ((CustomImageRelativeLayout) currentView).mImageWidth;
                    ((CustomImageRelativeLayout) currentView).mImagePrevHeight = ((CustomImageRelativeLayout) currentView).mImageHeight;
                } else if (currentView instanceof CustomWebRelativeLayout) {
                    ((CustomWebRelativeLayout) currentView).setTransformationValues(currentView.getX(), currentView.getY(), (android.widget.RelativeLayout.LayoutParams) currentView.getLayoutParams());
                    ((CustomWebRelativeLayout) currentView).mWebTextPrevXpos = ((CustomWebRelativeLayout) currentView).mWebTextXpos;
                    ((CustomWebRelativeLayout) currentView).mWebTextPrevYpos = ((CustomWebRelativeLayout) currentView).mWebTextYpos;
                    ((CustomWebRelativeLayout) currentView).mWebTextPrevWidth = ((CustomWebRelativeLayout) currentView).mWebTextWidth;
                    ((CustomWebRelativeLayout) currentView).mWebTextPrevHeight = ((CustomWebRelativeLayout) currentView).mWebTextHeight;
                } else if (currentView instanceof MindmapPlayer) {
                    ((MindmapPlayer) currentView).setTransformationValues(currentView.getX(), currentView.getY(), (android.widget.RelativeLayout.LayoutParams) currentView.getLayoutParams());
                    ((MindmapPlayer) currentView).mImagePrevXpos = ((MindmapPlayer) currentView).mImageXpos;
                    ((MindmapPlayer) currentView).mImagePrevYpos = ((MindmapPlayer) currentView).mImageYpos;
                    ((MindmapPlayer) currentView).mImagePrevWidth = ((MindmapPlayer) currentView).mImageWidth;
                    ((MindmapPlayer) currentView).mImagePrevHeight = ((MindmapPlayer) currentView).mImageHeight;
                }
                ctxBookView.designScrollPageView.setEnableScrolling(false);
                previousPositionX = positionX;
                previousPositionY = positionY;
                enableHandlerArea(view,false);

                break;
            }

            case MotionEvent.ACTION_MOVE: {

                objectMoveAndPinch(positionX, positionY, previousPositionX, previousPositionY, currentView, view);
                reassignHandlerArea(currentView);
                if (currentView instanceof MindmapPlayer) {
                    RelativeLayout layout = (RelativeLayout) currentView.getChildAt(0);
                    TwoDScrollView scrollView = (TwoDScrollView) layout.getChildAt(0);
                    scrollView.scroll((int) (AppDict.DEFAULT_OFFSET - currentView.getWidth() + 120) / 2,
                            (int) (AppDict.DEFAULT_OFFSET - currentView.getHeight()) / 2);
                }

                previousPositionX = positionX;
                previousPositionY = positionY;

                break;
            }

            case MotionEvent.ACTION_UP: {
                if (currentView instanceof CustomWebRelativeLayout) {
                    if (((CustomWebRelativeLayout) currentView).getObjType().equals(Globals.OBJTYPE_MINDMAPWEBTEXT)) {
//					((CustomWebRelativeLayout)currentView).resizeMindmapDivWidthAndHeight(currentView.getWidth(), currentView.getHeight());
                    }
                }
                ctxBookView.designScrollPageView.setEnableScrolling(true);
                previousPositionX = 0;
                previousPositionY = 0;

                if (currentView instanceof CustomImageRelativeLayout) {
                    ((CustomImageRelativeLayout) currentView).setTransformationValues(currentView.getX(), currentView.getY(), (android.widget.RelativeLayout.LayoutParams) currentView.getLayoutParams());
                    if (((CustomImageRelativeLayout) currentView).mImagePrevXpos != ((CustomImageRelativeLayout) currentView).mImageXpos || ((CustomImageRelativeLayout) currentView).mImagePrevYpos != ((CustomImageRelativeLayout) currentView).mImageYpos || ((CustomImageRelativeLayout) currentView).mImagePrevWidth != ((CustomImageRelativeLayout) currentView).mImageWidth || ((CustomImageRelativeLayout) currentView).mImagePrevHeight != ((CustomImageRelativeLayout) currentView).mImageHeight) {
                        UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
                        uRPrevObjData.setuRObjDataUniqueId(((CustomImageRelativeLayout) currentView).getObjectUniqueId());
                        uRPrevObjData.setuRObjDataXPos(((CustomImageRelativeLayout) currentView).mImagePrevXpos);
                        uRPrevObjData.setuRObjDataYPos(((CustomImageRelativeLayout) currentView).mImagePrevYpos);
                        uRPrevObjData.setuRObjDataWidth(((CustomImageRelativeLayout) currentView).mImagePrevWidth);
                        uRPrevObjData.setuRObjDataHeight(((CustomImageRelativeLayout) currentView).mImagePrevHeight);
                        createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_SCALED, Globals.OBJECT_PREVIOUS_STATE);

                        UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                        uRCurrObjData.setuRObjDataUniqueId(((CustomImageRelativeLayout) currentView).getObjectUniqueId());
                        uRCurrObjData.setuRObjDataXPos(((CustomImageRelativeLayout) currentView).mImageXpos);
                        uRCurrObjData.setuRObjDataYPos(((CustomImageRelativeLayout) currentView).mImageYpos);
                        uRCurrObjData.setuRObjDataWidth(((CustomImageRelativeLayout) currentView).mImageWidth);
                        uRCurrObjData.setuRObjDataHeight(((CustomImageRelativeLayout) currentView).mImageHeight);
                        createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_SCALED, Globals.OBJECT_PRESENT_STATE);
                    }
                } else if (currentView instanceof CustomWebRelativeLayout) {
                    ((CustomWebRelativeLayout) currentView).setTransformationValues(currentView.getX(), currentView.getY(), (android.widget.RelativeLayout.LayoutParams) currentView.getLayoutParams());
                    if (((CustomWebRelativeLayout) currentView).mWebTextPrevXpos != ((CustomWebRelativeLayout) currentView).mWebTextXpos || ((CustomWebRelativeLayout) currentView).mWebTextPrevYpos != ((CustomWebRelativeLayout) currentView).mWebTextYpos || ((CustomWebRelativeLayout) currentView).mWebTextPrevWidth != ((CustomWebRelativeLayout) currentView).mWebTextWidth || ((CustomWebRelativeLayout) currentView).mWebTextPrevHeight != ((CustomWebRelativeLayout) currentView).mWebTextHeight) {
                        ((CustomWebRelativeLayout) currentView).getAndSetWebViewContent();
                        UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
                        uRPrevObjData.setuRObjDataUniqueId(((CustomWebRelativeLayout) currentView).getObjectUniqueId());
                        uRPrevObjData.setuRObjDataXPos(((CustomWebRelativeLayout) currentView).mWebTextPrevXpos);
                        uRPrevObjData.setuRObjDataYPos(((CustomWebRelativeLayout) currentView).mWebTextPrevYpos);
                        uRPrevObjData.setuRObjDataWidth(((CustomWebRelativeLayout) currentView).mWebTextPrevWidth);
                        uRPrevObjData.setuRObjDataHeight(((CustomWebRelativeLayout) currentView).mWebTextPrevHeight);
                        createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_SCALED, Globals.OBJECT_PREVIOUS_STATE);

                        UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                        uRCurrObjData.setuRObjDataUniqueId(((CustomWebRelativeLayout) currentView).getObjectUniqueId());
                        uRCurrObjData.setuRObjDataXPos(((CustomWebRelativeLayout) currentView).mWebTextXpos);
                        uRCurrObjData.setuRObjDataYPos(((CustomWebRelativeLayout) currentView).mWebTextYpos);
                        uRCurrObjData.setuRObjDataWidth(((CustomWebRelativeLayout) currentView).mWebTextWidth);
                        uRCurrObjData.setuRObjDataHeight(((CustomWebRelativeLayout) currentView).mWebTextHeight);
                        createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_SCALED, Globals.OBJECT_PRESENT_STATE);
                    }
                } else if (currentView instanceof MindmapPlayer) {
                    ((MindmapPlayer) currentView).setTransformationValues(currentView.getX(), currentView.getY(), (android.widget.RelativeLayout.LayoutParams) currentView.getLayoutParams());
                    if (((MindmapPlayer) currentView).mImagePrevXpos != ((MindmapPlayer) currentView).mImageXpos || ((MindmapPlayer) currentView).mImagePrevYpos != ((MindmapPlayer) currentView).mImageYpos || ((MindmapPlayer) currentView).mImagePrevWidth != ((MindmapPlayer) currentView).mImageWidth || ((MindmapPlayer) currentView).mImagePrevHeight != ((MindmapPlayer) currentView).mImageHeight) {
                        UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
                        uRPrevObjData.setuRObjDataUniqueId(((MindmapPlayer) currentView).getObjectUniqueId());
                        uRPrevObjData.setuRObjDataXPos(((MindmapPlayer) currentView).mImagePrevXpos);
                        uRPrevObjData.setuRObjDataYPos(((MindmapPlayer) currentView).mImagePrevYpos);
                        uRPrevObjData.setuRObjDataWidth(((MindmapPlayer) currentView).mImagePrevWidth);
                        uRPrevObjData.setuRObjDataHeight(((MindmapPlayer) currentView).mImagePrevHeight);
                        createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_SCALED, Globals.OBJECT_PREVIOUS_STATE);

                        UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                        uRCurrObjData.setuRObjDataUniqueId(((MindmapPlayer) currentView).getObjectUniqueId());
                        uRCurrObjData.setuRObjDataXPos(((MindmapPlayer) currentView).mImageXpos);
                        uRCurrObjData.setuRObjDataYPos(((MindmapPlayer) currentView).mImageYpos);
                        uRCurrObjData.setuRObjDataWidth(((MindmapPlayer) currentView).mImageWidth);
                        uRCurrObjData.setuRObjDataHeight(((MindmapPlayer) currentView).mImageHeight);
                        createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_SCALED, Globals.OBJECT_PRESENT_STATE);
                    }
                }
                enableHandlerArea(view,true);

                break;
            }

            case MotionEvent.ACTION_CANCEL: {

                break;
            }

            case MotionEvent.ACTION_POINTER_UP: {

                break;
            }

            default:
                break;
        }

        return true;
    }

    /**
     * @return the pageType
     */
    public String getPageType() {
        return pageType;
    }

    /**
     * @param pageType the pageType to set
     */
    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    private void enableHandlerArea(View view,boolean enableAll){
        if (!enableAll) {
            if (handlerArea1 != null && view.getId() != handlerArea1.getId()) {
                handlerArea1.setEnabled(false);
            }if (handlerArea2 != null && view.getId() != handlerArea2.getId()) {
                handlerArea2.setEnabled(false);
            }if (handlerArea3 != null && view.getId() != handlerArea3.getId()) {
                handlerArea3.setEnabled(false);
            }if (handlerArea4 != null && view.getId() != handlerArea4.getId()) {
                handlerArea4.setEnabled(false);
            }if (handlerArea5 != null && view.getId() != handlerArea5.getId()) {
                handlerArea5.setEnabled(false);
            }if (handlerArea6 != null && view.getId() != handlerArea6.getId()) {
                handlerArea6.setEnabled(false);
            }if (handlerArea7 != null && view.getId() != handlerArea7.getId()) {
                handlerArea7.setEnabled(false);
            }if (handlerArea8 != null && view.getId() != handlerArea8.getId()) {
                handlerArea8.setEnabled(false);
            }
        }else{
            handlerArea1.setEnabled(true);
            handlerArea2.setEnabled(true);
            handlerArea3.setEnabled(true);
            handlerArea4.setEnabled(true);
            handlerArea5.setEnabled(true);
            handlerArea6.setEnabled(true);
            handlerArea7.setEnabled(true);
            handlerArea8.setEnabled(true);
        }
    }

    private void addInsideLayout(RelativeLayout newLyout,int layoutPos){
       FrameLayout frameLayout = new FrameLayout(ctxBookView);
        int width = newLyout.getLayoutParams().width/2;
        int height = newLyout.getLayoutParams().height/2;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width-15,height-15);
        if (layoutPos==1){
            layoutParams.setMargins(10,10,5,5);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        }else if (layoutPos == 2){
            layoutParams.setMargins(5,10,10,5);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        }else if (layoutPos == 3){
            layoutParams.setMargins(10,5,5,10);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        }else if (layoutPos == 4){
            layoutParams.setMargins(5,5,10,10);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        }
        frameLayout.setPadding(layoutParams.width / 4, layoutParams.height / 4, layoutParams.width / 4, layoutParams.height / 4);
        frameLayout.setLayoutParams(layoutParams);
        frameLayout.setBackgroundColor(ctxBookView.getResources().getColor(R.color.popup_actionbar_color));
        frameLayout.setAlpha((float) 0.85);
        newLyout.addView(frameLayout);
        imageView = new ImageView(ctxBookView);
        FrameLayout.LayoutParams imgLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT);
        imgLayoutParams.gravity = Gravity.CENTER;
        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(ScaleType.CENTER_CROP);
        imageView.setLayoutParams(imgLayoutParams);
        if (((CustomImageRelativeLayout)currentView).getObjectType().equals("Image")){
            if (layoutPos==1){
                imageView.setId(R.id.imgLayout1);
                imageView.setBackgroundResource(R.drawable.image_frequentobjects);
            }else if (layoutPos==2){
                imageView.setId(R.id.imgLayout2);
                imageView.setBackgroundResource(R.drawable.camera_frequentobjects);
            }else if (layoutPos==3){
                imageView.setId(R.id.imgLayout3);
                imageView.setBackgroundResource(R.drawable.websearch_frequentobjects);
            }else if (layoutPos==4){
                imageView.setId(R.id.imgLayout4);
                imageView.setBackgroundResource(R.drawable.delete_cloud);
            }

        }else if (((CustomImageRelativeLayout)currentView).getObjectType().equals("YouTube")){
            if (layoutPos==1){
                imageView.setId(R.id.imgLayout1);
                imageView.setBackgroundResource(R.drawable.videogallery_frequentobjects);
            }else if (layoutPos==2){
                imageView.setId(R.id.imgLayout2);
                imageView.setBackgroundResource(R.drawable.videocamera_frequentobjects);
            }else if (layoutPos==3){
                imageView.setId(R.id.imgLayout3);
                imageView.setBackgroundResource(R.drawable.websearch_frequentobjects);
            }else if (layoutPos==4){
                imageView.setId(R.id.imgLayout4);
                imageView.setBackgroundResource(R.drawable.delete_cloud);
            }
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.imgLayout1: {
                        if (((CustomImageRelativeLayout)currentView).getObjectType().equals("Image")){
                            ((CustomImageRelativeLayout)currentView).takePhotoFromGallery();
                        }else if (((CustomImageRelativeLayout)currentView).getObjectType().equals("YouTube")){
                            ((CustomImageRelativeLayout)currentView).takeVideoFromGallery();
                        }
                        break;
                    }case R.id.imgLayout2: {
                        if (((CustomImageRelativeLayout)currentView).getObjectType().equals("Image")){
                            ((CustomImageRelativeLayout)currentView).callCameraToTakePhoto();
                        }else if (((CustomImageRelativeLayout)currentView).getObjectType().equals("YouTube")){
                            ((CustomImageRelativeLayout)currentView).callCameraToTakeVideo();
                        }
                        break;
                    }case R.id.imgLayout3: {
                        if (((CustomImageRelativeLayout)currentView).getObjectType().equals("Image")){
                            GoogleImageSearchDialogWindow googleImgSearchView = new GoogleImageSearchDialogWindow(ctxBookView,currentView);
                            googleImgSearchView.callDialogWindow();
                        }else if (((CustomImageRelativeLayout)currentView).getObjectType().equals("YouTube")){
                            YoutubePopUpWindow youTubeWindow = new YoutubePopUpWindow(ctxBookView);
                            youTubeWindow.callPopupWindow();
                        }
                        break;
                    }case R.id.imgLayout4: {
                       setCurrentView(currentView);
                       makeSelectedArea(currentView);
                       ctxBookView.deleteSelectedObject();
                        break;
                    }
                }
            }
        });
        frameLayout.addView(imageView);
    }

    private void setDimensionForlayout(FrameLayout layoutframe,RelativeLayout newLyout,int layoutPos){
        int width = newLyout.getLayoutParams().width/2;
        int height = newLyout.getLayoutParams().height/2;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width-15,height-15);
        if (layoutPos==1){
            layoutParams.setMargins(10,10,5,5);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        }else if (layoutPos == 2){
            layoutParams.setMargins(5,10,10,5);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        }else if (layoutPos == 3){
            layoutParams.setMargins(10,5,5,10);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        }else if (layoutPos == 4){
            layoutParams.setMargins(5,5,10,10);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        }
        layoutframe.setPadding(layoutParams.width / 4, layoutParams.height / 4, layoutParams.width / 4, layoutParams.height / 4);
        layoutframe.setLayoutParams(layoutParams);
        View imgView =  layoutframe.getChildAt(0);
        if (imgView instanceof ImageView){
            FrameLayout.LayoutParams imgLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.WRAP_CONTENT);
            imgLayoutParams.gravity = Gravity.CENTER;
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ScaleType.CENTER_CROP);
            imgView.setLayoutParams(imgLayoutParams);
        }
    }

    public void readJsonFile() {
        malzamahBookDetails=new ArrayList<String>();
        String filePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + ctxBookView.currentBook.getBookID() + "/FreeFiles/";
        File file = new File(filePath + "malzamah.json");
        if (file.exists()) {
            String jsonStr = null;
            try {
                FileInputStream stream = new FileInputStream(file);
                FileChannel fc = stream.getChannel();
                try {
                    stream = new FileInputStream(file);
                    MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                    jsonStr = Charset.defaultCharset().decode(bb).toString();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    stream.close();
                }

                JSONObject jsonObject = new JSONObject(jsonStr);
                JSONArray jsonArray= jsonObject.getJSONArray("bookDetails");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonSub = jsonArray.getJSONObject(i);
                    String description=jsonSub.getString("description");
                    String purchaseType=jsonSub.getString("purchaseType");
                    String author=jsonSub.getString("author");
                    String totalPages=jsonSub.getString("totalPages");
                    String title=jsonSub.getString("title");
                    String price=jsonSub.getString("price");
                    String storeID=jsonSub.getString("storeID");
                    String domainURL=jsonSub.getString("domainURL");
                    String language=jsonSub.getString("language");
                    String imageURL=jsonSub.getString("imageURL");
                    String isEditable=jsonSub.getString("isEditable");
                    String downloadURL=jsonSub.getString("downloadURL");
                    String bookData=description+"##"+purchaseType+"##"+author+"##"+totalPages+"##"+title+"##"+price+"##"+storeID+"##"+domainURL+"##"+language+"##"+imageURL+"##"+isEditable+"##"+downloadURL;
                    malzamahBookDetails.add(bookData);

                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void callBookmodalPopUpWindow(String storeBid) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxBookView);
        final int subscriptDays = Integer.parseInt(prefs.getString(Globals.SUBSCRIPTDAYSLEFT, "0"));
        //bookModalPopwindow = new Dialog(fa,R.style.PauseDialog);
        bookModalPopwindow = new Dialog(ctxBookView);
        bookModalPopwindow.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        bookModalPopwindow.setContentView(ctxBookView.getLayoutInflater().inflate(R.layout.bookmodal_neww, null));
        bookModalPopwindow.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.5));
        bookModalPopwindow.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ViewGroup viewGroup = (RelativeLayout) bookModalPopwindow.findViewById(R.id.bookmodal_layout);
        Typeface font = Typeface.createFromAsset(ctxBookView.getAssets(), "fonts/FiraSans-Regular.otf");
        UserFunctions.changeFont(viewGroup, font);

        String CurrentBook = getSourceStoreBookDetails(storeBid);
        final String[] BookDetails = CurrentBook.split("##");
        String priceContent = BookDetails[5];
        String bookTitleContent = BookDetails[4];
        String descriptionContent = BookDetails[0];
        String authorContent = BookDetails[2];
        if (authorContent.equals("null")) {
            authorContent = "";
        }
        String totalPagesContent = BookDetails[3];
        if (totalPagesContent.equals("null")) {
            totalPagesContent = "-";
        }
        String langContent = BookDetails[8];
        if (langContent.equals("null")) {
            langContent = "-";
        }
        final String imageURL = BookDetails[9];
        String size = null;
        if (size==null) {
            size = "-";
        }
        String BookID = BookDetails[0];

        //final  RelativeLayout ll_txtcontainer = (RelativeLayout) bookModalPopwindow.findViewById(R.id.ll_txtcontainer);
        //ll_txtcontainer.setVisibility(View.VISIBLE);
        //TextView title =  (TextView)bookModalPopwindow.findViewById(R.id.title);
        TextView titleLabel = (TextView) bookModalPopwindow.findViewById(R.id.book_Label);
        TextView languageLabel = (TextView) bookModalPopwindow.findViewById(R.id.langLabel);
        TextView descriptionLabel = (TextView) bookModalPopwindow.findViewById(R.id.desLabel);
        TextView authorLabel = (TextView) bookModalPopwindow.findViewById(R.id.authorLabel);
        TextView totalPagesLabel = (TextView) bookModalPopwindow.findViewById(R.id.totalPagesLabel);
        TextView txtSize = (TextView) bookModalPopwindow.findViewById(R.id.txtSize);
        TextView txtCategory = (TextView) bookModalPopwindow.findViewById(R.id.txtCategory);
        //TextView priceLabel =  (TextView) bookModalPopwindow.findViewById(R.id.priceLabel);
        ImageView bookImage = (ImageView) bookModalPopwindow.findViewById(R.id.imageView1);
        CircleButton back_btn = (CircleButton) bookModalPopwindow.findViewById(R.id.btn_back_bookmodel);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookModalPopwindow.dismiss();
            }
        });
        bookModalPopwindow.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
//                if (isBookModalDownloadClicked) {
//                    ctxBookView.gridShelf.invalidateViews();
//                    isBookModalDownloadClicked = false;
//                }
            }
        });

       // ETagValue = 6;
       // bookModalFlag = 0;

        Glide.with(ctxBookView).load(imageURL)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(200, 200)
                .into(bookImage);
        titleLabel.setText(bookTitleContent);
        languageLabel.setText(langContent);
        descriptionLabel.setText(descriptionContent);
        authorLabel.setText(authorContent);
        totalPagesLabel.setText(totalPagesContent);
        txtSize.setText(size);
        final Button download = (Button) bookModalPopwindow.findViewById(R.id.downloadBtn);
        final String bookID = BookDetails[0];
        final float price = (float) Float.parseFloat(BookDetails[5]);
        String purchaseType = "";
       // prefs = PreferenceManager.getDefaultSharedPreferences(fa_enrich);
       // int subscriptDays = Integer.parseInt(prefs.getString(Globals.SUBSCRIPTDAYSLEFT, "0"));
       if (price > 0.00) {
            if (Globals.isLimitedVersion()) {
                if (subscriptDays > 0) {
                    if (ctxBookView.subscription.isBookPurchased) {
                        purchaseType = "purchased";
                    } else {
                        purchaseType = "subscribed";
                    }
                } else {
                    purchaseType = "purchased";
                }
            } else {
                purchaseType = "purchased";
            }
        } else {
            purchaseType = "free";
        }
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean bookExist=false;
                for(int i=0;i<ctxBookView.gridShelf.bookListArray.size();i++){
                    bookExist=false;
                    Book book= (Book) ctxBookView.gridShelf.bookListArray.get(i);
                    if(book.is_bStoreBook()&&book.get_bStoreID().equals(BookDetails[6])){
                        download.setText(book.getIsDownloadCompleted());
                        download.setBackgroundColor(ctxBookView.getResources().getColor(R.color.hovercolor));
                        bookExist=true;
                        break;
                    }
                }
                if(!bookExist) {
                    if (!download.getText().toString().equals(ctxBookView.getResources().getString(R.string.downloaded)) && !download.getText().toString().equals(ctxBookView.getResources().getString(R.string.downloading))) {
                        download.setText(ctxBookView.getResources().getString(R.string.downloading));
                        download.setBackgroundColor(ctxBookView.getResources().getColor(R.color.hovercolor));
                        int templateId = 6;

                        bookDetails = null + "#" + BookDetails[6] + "#" + BookDetails[11] + "#" + BookDetails[4] + "#" + BookDetails[0] + "#" + 6 + "#" + BookDetails[3] + "#" + BookDetails[6] + "#" + 1 + "#" + BookDetails[8] + "#" + "downloaded" + "#" + BookDetails[11] + "#" + BookDetails[10] + "#" + BookDetails[5] + "#" + BookDetails[9] + "#" + BookDetails[1] + "#" + BookDetails[7];

                        downloadClicked();
                        if (subscriptDays>0) {
                            download.setText(ctxBookView.getResources().getString(R.string.downloading));
                            //isBookModalDownloadClicked = true;
                        }else {
                            if (!(price >0.00)){
                                download.setText(ctxBookView.getResources().getString(R.string.downloading));
                               // isBookModalDownloadClicked = true;
                            }
                        }

                    }
                }

                v.invalidate();
            }
        });

//        jsonSub.put("description", split[0]);
//        jsonSub.put("purchaseType",split[1]);
//        jsonSub.put("author",split[2]);
//        jsonSub.put("totalPages",split[3]);
//        jsonSub.put("title",split[4]);
//        jsonSub.put("price",split[5]);
//        jsonSub.put("storeID",split[6]);
//        jsonSub.put("domainURL",split[7]);
//        jsonSub.put("language",split[8]);
//        jsonSub.put("imageURL",split[9]);
//        jsonSub.put("isEditable",split[10]);
//        jsonSub.put("downloadURL",split[11]);
        if (price > 0.00) {
            if (Globals.isLimitedVersion()) {
                if (subscriptDays > 0) {
                    download.setText(ctxBookView.getResources().getString(R.string.get));
                } else {
                    download.setText(String.valueOf(price) + "$");
                }
            } else {
                download.setText(String.valueOf(price) + "$");
            }
        } else {
            boolean bookExist=false;
            for(int i=0;i<ctxBookView.gridShelf.bookListArray.size();i++){
                bookExist=false;
                Book book= (Book) ctxBookView.gridShelf.bookListArray.get(i);
                if(book.is_bStoreBook()&&book.get_bStoreID().equals(BookDetails[6])){
                    download.setText(book.getIsDownloadCompleted());
                    download.setBackgroundColor(ctxBookView.getResources().getColor(R.color.hovercolor));
                    bookExist=true;
                    break;
                }
            }
            if(!bookExist) {
                download.setText(ctxBookView.getResources().getString(R.string.download));
                download.setBackgroundColor(ctxBookView.getResources().getColor(R.color.hovercolor));
            }
        }

        bookModalPopwindow.show();

    }
    private String getSourceStoreBookDetails(String StoreBID){
        String bookDetails=null;
        if(malzamahBookDetails!=null&&malzamahBookDetails.size()>0){
            for(int i=0;i<malzamahBookDetails.size();i++){
                String data=malzamahBookDetails.get(i);
                String split[]=data.split("##");
                if(split[6].equals(StoreBID)){
                    bookDetails=data;
                    return bookDetails;

                }
            }
        }
        return bookDetails;
    }

    /**
     * Initiate the purchase
     *
     * @paraminAppProductId
     */
    private void startPurchase() {
        //ITEM_SKU = "android.test.purchased";
       // ctxBookView.ITEM_SKU = inAppProductId;
        ctxBookView.mHelper = new IabHelper(ctxBookView, Globals.manahijBase64EncodedPublicKey);
        ctxBookView.mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    UserFunctions.complain("Problem setting up in-app billing: " + result, ctxBookView);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (ctxBookView.mHelper == null) return;

                ctxBookView.mHelper.launchPurchaseFlow(ctxBookView, ctxBookView.ITEM_SKU, 10001, mPurchaseFinishedListener, "");
            }
        });

    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            // if we were disposed of in the meantime, quit.
            if (ctxBookView.mHelper == null) return;

            if (result.isFailure()) {
                int response = result.getResponse();
                if (response == 7) {
                    UserFunctions.DisplayAlertDialog(ctxBookView, R.string.item_already_owned, R.string.restoring_purchase);
                    startDownload();
                   // gridview.invalidateViews();
                } else {
                    //UserFunctions.complain("Error purchasing: " + result, fa_enrich);
                }
                return;
            }

            //Log.d(TAG, "Purchase successful.");

            if (purchase.getSku().equals(ctxBookView.ITEM_SKU)) {
                //fa_enrich.mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                startDownload();
               // gridview.invalidateViews();
            } else if (purchase.getSku().equals("")){
                ctxBookView.mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                //UserFunctions.alert("Thank you for subscribing", fa_enrich);
            }

            /*if (result.isFailure()) {
                String message = result.getMessage();
                int response = result.getResponse();
                //System.out.println(result+":"+response);
                if (response == 7) {
                    UserFunctions.DisplayAlertDialog(fa_enrich, R.string.item_already_owned, R.string.restoring_purchase);
                    startDownload();
                } else {
                    String str = Enrichment.this.getResources().getString(R.string.in_app_purchase_failed_);
                    UserFunctions.DisplayAlertDialog(fa_enrich, str + result, R.string.purchase_failed);
                }
                return;
            } else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();
            }*/
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            // if we were disposed of in the meantime, quit.
            if (ctxBookView.mHelper == null) return;

            if (result.isSuccess()) {
                ////System.out.println("Success");
                //UserFunctions.DisplayAlertDialog(fa_enrich, R.string.purchase_completed_successfully, R.string.purchased);
                //startDownload();
                if (purchase.getSku().equals("")) {
                    //UserFunctions.alert(purchase.getOriginalJson(), fa_enrich);
             //       ctxBookView.subscription.new SaveSubscriptionGroups(purchase, ctxBookView,null).execute();
                }
            } else {
                ////System.out.println("Failed");
                //UserFunctions.DisplayAlertDialog(fa_enrich, result.getMessage(), R.string.purchase_failed);
                UserFunctions.complain("Error while consuming: " + result, ctxBookView);
            }
        }
    };

    public void downloadClicked() {
//    	if(!rootedDevice){
        float price = 0;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxBookView);
        int subscriptDays = Integer.parseInt(prefs.getString(Globals.SUBSCRIPTDAYSLEFT, "0"));
        //String currentBook = books.get(EbookClickPosition);
        String[] BookDetails = bookDetails.split("#");
        //String bnameCheck = StoreDatabase.getPayPalPaidBooks(currentBook);
        price = (float) Float.parseFloat(BookDetails[13]);
        if (price > 0.00) {
            if (Globals.isLimitedVersion()) {
                if (subscriptDays > 0) {
//                    String inAppProductId = Globals.SUBSCRIPTION_INAPP_PURCHASE_PRODUCTID;
//                    ctxBookView.subscription.checkForStoreBooksPurchased(inAppProductId);
                    startDownload();
                } else {
                    //String inAppProductId = BookDetails[29];
                    startPurchase();
                }
            } else {
                String inAppProductId = BookDetails[29];
                startPurchase();
            }
        } else {
            startDownload();
        }
//		} else {
//            UserFunctions.DisplayAlertDialog(fa_enrich, R.string.books_cannot_download_rooted_device, R.string.rooted_device);
//		}
        // startDownload();
    }
     private void startDownload(){
         GDriveExport gdrive = new GDriveExport(ctxBookView, bookDetails);
         gdrive.downloadBook();
     }
}
