package com.semanoor.source_sboookauthor;

public interface WebTextJavascriptInterfaceListener {
	/**
	 * Informs the listener to set the web content.
	 * @param text
	 */
	public abstract void wtjiSetWebContent(String text);

	/**
	 * Informs the listener to set the web font size.
	 * @param fontSize
	 */
	public abstract void wtjisetWebFontSize(int fontSize);

	public abstract void tsjiJSError(String error);
	
	/**
	 * Informs the listener to set the web font style.
	 * @param fontFamily
	 */
	public abstract void wtjisetWebFontFamily(String fontFamily);

	/**
	 * Informs the listener to set the web font color.
	 * @param fontColor
	 */
	public abstract void wtjisetWebFontColor(String fontColor);

	/**
	 * Informs the listener to set the web Background color.
	 * @param backgroundColor
	 */
	public abstract void wtjisetWebBackgroundColor(String backgroundColor);

	/**
	 * Informs the listener to set selection active to true or false.
	 * @param selectionActive
	 */
	public abstract void wtjisetSelectionActive(String selectionActive);
	
	public abstract void wtjisetDocumentHeight(String documentHeight);
	
	/**
	 * Informs the listener to set the Table cell color.
	 * @param cellColor
	 */
	public abstract void wtjisetTableCellColor(String cellColor);
	
	/**
	 * Informs the listener to set the Table row color.
	 * @param rowColor
	 */
	public abstract void wtjisetTableRowColor(String rowColor);
	
	/**
	 * Informs the listener to set the Table column color.
	 * @param columnColor
	 */
	public abstract void wtjisetTableColumnColor(String columnColor);

	/**
	 * Informs the listener to set the Table background color.
	 * @param backgroundColor
	 */
	public abstract void wtjisetTableBackgroundColor(String backgroundColor);
	
	/**
	 * Informs the listener to set the Table font color.
	 * @param fontColor
	 */
	public abstract void wtjisetTableFontColor(String fontColor);
	
	/**
	 * Informs the listener to set the Table height:
	 * @param height
	 */
	public abstract void wtjisetTableHeight(int height);

	public abstract void wtjisetSelectedText(String selectedText);

	public abstract void wtjisetParentTableBg(String content);

	public abstract void wtjisetLanguage(String language);

	public abstract void wtjiSetAndReturnWebContent(String webContent);

	public abstract void wtjisetSelectedRowColumn(String row,String column);
}
