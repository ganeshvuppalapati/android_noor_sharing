package com.semanoor.source_sboookauthor;

import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;

public class WebTableListAdapter extends BaseAdapter {
	
	private BookViewActivity context;
	private Integer[] webTableInfoListArray;
	private CustomWebRelativeLayout customWRL;
	private Boolean AddRowRadioIsBefore;
	private Boolean AddColumnRadioIsBefore;

	public WebTableListAdapter(BookViewActivity _context, Integer[] _webTableInfoListArray, CustomWebRelativeLayout customWebRelativeLayout) {
		this.context = _context;
		this.webTableInfoListArray = _webTableInfoListArray;
		this.customWRL = customWebRelativeLayout;
	}

	@Override
	public int getCount() {
		return webTableInfoListArray.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (position == 0) { //Cell color
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webTableInfoListArray[position]);
			ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
			if (customWRL.tableCellColor != null && !customWRL.tableCellColor.equals("transparent")) {
				iv.setBackgroundColor(Color.parseColor(customWRL.tableCellColor));
			}
		} else if (position == 1) { //Row color
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webTableInfoListArray[position]);
			ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
			if (customWRL.tableRowColor != null && !customWRL.tableRowColor.equals("transparent")) {
				iv.setBackgroundColor(Color.parseColor(customWRL.tableRowColor));
			}
		} else if (position == 2) { //Column color
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webTableInfoListArray[position]);
			ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
			if (customWRL.tableColumnColor != null && !customWRL.tableColumnColor.equals("transparent")) {
				iv.setBackgroundColor(Color.parseColor(customWRL.tableColumnColor));
			}
		} else if (position == 3) { //Background color
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webTableInfoListArray[position]);
			ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
			if (customWRL.tableBackgroundColor != null && !customWRL.tableBackgroundColor.equals("transparent")) {
				iv.setBackgroundColor(Color.parseColor(customWRL.tableBackgroundColor));
			}
		}else if(position == 4){ //Font color
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_colour, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webTableInfoListArray[position]);
			ImageView iv = (ImageView) view.findViewById(R.id.imageView1);
			if (customWRL.fontColor != null && !customWRL.fontColor.equals("transparent")) {
				iv.setBackgroundColor(Color.parseColor(customWRL.fontColor));
			}
		}else if (position == 5) { //Font size
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_size, null);
			NumberPicker np = (NumberPicker) view.findViewById(R.id.numberPicker1);
			np.setMinValue(8);
			np.setMaxValue(72);
			np.setValue(customWRL.fontSize);
			np.setOnValueChangedListener(new OnValueChangeListener() {
				
				@Override
				public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
					customWRL.changeFontSize(newVal, "SBATable");
				}
			});
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webTableInfoListArray[position]);
		}else if (position == 6) { //Font
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtext_font, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setText(webTableInfoListArray[position]);
			TextView tv_fontFamily = (TextView) view.findViewById(R.id.textView2);
			tv_fontFamily.setText(customWRL.fontFamily);
		} else if(position == 7){ //Add rows
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtable_addrow, null);
			final EditText rowEdit = (EditText) view.findViewById(R.id.rows_editText);
			final EditText positionEdit = (EditText) view.findViewById(R.id.position_editText);
			final RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup1);
			AddRowRadioIsBefore = false;
			//int row=customWRL.rowPosition+1;
			positionEdit.setText(String.valueOf(customWRL.rowPosition));
			//int radioGroupSelect = radioGroup.getCheckedRadioButtonId();
			//final RadioButton radioBtn = (RadioButton) view.findViewById(radioGroupSelect);
			radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					if(checkedId == R.id.radio0){
						//System.out.println("Before");
						AddRowRadioIsBefore = true;
						
					}else if(checkedId == R.id.radio1){
						//System.out.println("After");
						AddRowRadioIsBefore = false;
			
					}
				}
			});	 
			Button okBtn = (Button) view.findViewById(R.id.okBtn);
			okBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//Call method
					int newRows = Integer.parseInt(rowEdit.getText().toString());
					int rowAt = Integer.parseInt(positionEdit.getText().toString());
					if(AddRowRadioIsBefore && rowAt >0){
						//Decrement row position
						rowAt = rowAt - 2;
					}
					customWRL.insertRowsInTable(rowAt, newRows);
				}
			});
			
		}else if(position == 8){ //Delete rows
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtable_deleterow, null);
			final EditText rowEdit = (EditText) view.findViewById(R.id.rows_editText);
			final EditText positionEdit = (EditText) view.findViewById(R.id.position_editText);
			Button okBtn = (Button) view.findViewById(R.id.okBtn);
			positionEdit.setText(String.valueOf(customWRL.rowPosition));
			okBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//Call method 
					 int deleteRows = Integer.parseInt(rowEdit.getText().toString());
					 int rowAt = Integer.parseInt(positionEdit.getText().toString());
					 rowAt=rowAt-1;
					 customWRL.deleteRowInTable(rowAt, deleteRows);
				}
			});
			
		}else if(position == 9){ //Add columns
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtable_addcolumn, null);
			final EditText columnEdit = (EditText) view.findViewById(R.id.rows_editText);
			final EditText positionEdit = (EditText) view.findViewById(R.id.position_editText);
			final RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup1);
			AddColumnRadioIsBefore = false;
			positionEdit.setText(String.valueOf(customWRL.columnPosition));
			radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					if(checkedId == R.id.radio0){
						//System.out.println("Before");
						AddColumnRadioIsBefore = true;
						
					}else if(checkedId == R.id.radio1){
						//System.out.println("After");
						AddColumnRadioIsBefore = false;
			
					}
				}
			});
			Button okBtn = (Button) view.findViewById(R.id.okBtn);
			okBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//Call method 
					 int newColumns = Integer.parseInt(columnEdit.getText().toString());
					 int columnAt = Integer.parseInt(positionEdit.getText().toString());
					 if(AddColumnRadioIsBefore && columnAt >0){
						 columnAt = columnAt - 2;
					 }
					customWRL.insertColumnsInTable(columnAt, newColumns);
				}
			});
			
		}else if(position == 10){ //Delete columns
			view = context.getLayoutInflater().inflate(R.layout.inflate_webtable_deletecolumn, null);
			final EditText columnEdit = (EditText) view.findViewById(R.id.rows_editText);
			final EditText positionEdit = (EditText) view.findViewById(R.id.position_editText);
			Button okBtn = (Button) view.findViewById(R.id.okBtn);
			positionEdit.setText(String.valueOf(customWRL.columnPosition));
			okBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//Call method 
					int deleteColumns = Integer.parseInt(columnEdit.getText().toString());
					int columnAt = Integer.parseInt(positionEdit.getText().toString());
					columnAt=columnAt-1;
					customWRL.deleteColumnInTable(columnAt, deleteColumns);
				}
			});
		}
		else if (position == 11) { 
			if (customWRL.isObjLocked()) {
				webTableInfoListArray[11] = R.string.unlock;
			} else {
				webTableInfoListArray[11] = R.string.lock;
			}
			view = context.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setTextColor(Color.BLACK);
			tv.setText(webTableInfoListArray[position]);
		}
		else {
			view = context.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
			TextView tv = (TextView) view.findViewById(R.id.textView1);
			tv.setTextColor(Color.BLACK);
			tv.setText(webTableInfoListArray[position]);
		}
		return view;
	}
}
