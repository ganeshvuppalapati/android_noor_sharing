package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.widget.RelativeLayout;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

public class ExportToEpub {
	
	private Book currentBook;
	private Context context;
	private DatabaseHandler db;
	private int pageCount;
	private String strToc, strContent, strManifest, strSpine;
	String currentBookPath;
	int currentYear;
	String toLocation;
	RelativeLayout rootView;
	
	public ExportToEpub(Context _context, Book _currentBook, DatabaseHandler _db, String ePubPath, RelativeLayout layout) {
		this.context = _context;
		this.currentBook = _currentBook;
		this.db = _db;
		this.pageCount = currentBook.getTotalPages();
		this.toLocation = ePubPath;
		rootView=layout;
	}

	public void copyDefaultEpubTemplatetoFilesDir() {
		Calendar calender = Calendar.getInstance();
		currentYear = calender.get(Calendar.YEAR);
		
		currentBookPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID();
		String ePubDestinationPath = currentBookPath+"/ePub/";
		UserFunctions.copyTemplatesDirFromAssetToFilesDir(context, ePubDestinationPath, "epubTemplate");
		
		//Copy FreeFiles to ePub directory
		String srcFreefilesPath = currentBookPath+"/FreeFiles/";
		String desFreefilesPath = currentBookPath+"/ePub/OEBPS/FreeFiles/";
		UserFunctions.createNewBooksDirAndCoptTemplates(srcFreefilesPath, desFreefilesPath);
		
		//Copy Image Directory to ePub directory
		String srcImageDirPath = currentBookPath+"/images/";
		String desImageDirPath = currentBookPath+"/ePub/OEBPS/images/";
		if (new File(srcImageDirPath).exists()) {
			UserFunctions.createNewBooksDirAndCoptTemplates(srcImageDirPath, desImageDirPath);
		}
		
		//Generate Html files to ePub directory
		String htmlFilePath = currentBookPath+"/ePub/OEBPS/";
		for (int i = 0; i < pageCount; i++) {

			//new GenerateHTML(context, db, currentBook, String.valueOf(i+1), null).generatePage(htmlFilePath);
			new EpubExportGenerateHTML(context, db, currentBook, String.valueOf(i+1), null).generatePage(htmlFilePath);

			generateTocFile(i+1);
			generateContentFile(i+1);
		}
		
		String sourcePath = currentBookPath+"/ePub/";
		//String toLocation = currentBookPath+"/SBA_"+currentBook.getBookID()+".epub";
		//My changes
		//String extEpubPath = Environment.getExternalStorageDirectory() + "/SboookAuthor/";
		//String toLocation = extEpubPath + currentBook.getBookID()+".epub";
		//////////////
		Zip zip = new Zip();
		zip.zipFilesForEpub(new File(sourcePath), toLocation);
	}
	
	/**
	 * generate content opf file
	 * @param pageNumber
	 */
	private void generateContentFile(int pageNumber) {
		if (pageNumber == 1) {
			strContent = "<?xml version=\"1.0\"?>\n" +
					"<package xmlns=\"http://www.idpf.org/2007/opf\" unique-identifier=\"dcidid\" version=\"2.0\">\n" +
					"<metadata xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:opf=\"http://www.idpf.org/2007/opf\">\n" +
				    "<dc:title>SbookAuthor1 Epub</dc:title>\n" +
				    "<dc:creator>Semanoor</dc:creator>\n" +
				    "<dc:publisher>iStory</dc:publisher>\n" +
				    "<dc:date>"+currentYear+"</dc:date>\n" +
				    "<dc:language>en</dc:language>\n" +
				    "<dc:identifier id=\"book-id\" opf:scheme=\"ISBN\">000-0-00000-000-0</dc:identifier>\n" +
				    "<dc:rights>iStory</dc:rights>\n" +
				    "<meta name=\"cover\" content=\"coverimage\" />\n" +
				    "</metadata>\n";
			
			strManifest = "<manifest>\n" +
					"<item id=\"ncx\" href=\"toc.ncx\" media-type=\"application/x-dtbncx+xml\" />\n" +
					"<item id=\"page"+pageNumber+"\" href=\""+pageNumber+".htm\" media-type=\"application/xhtml+xml\" />\n";
			
			strSpine = "<spine toc=\"ncx\">\n" +
					"<itemref idref=\"page"+pageNumber+"\" />\n";
		} else if (pageNumber == pageCount) {
			/*strManifest = strManifest.concat("<manifest>\n" +
					"<item id=\"ncx\" href=\"toc.ncx\" media-type=\"application/x-dtbncx+xml\" />\n" +
					"<item id=\"page"+pageNumber+"\" href=\""+pageNumber+".htm\" media-type=\"application/xhtml+xml\" />\n" +
					"<item id=\"coverimage\" href=\"FreeFiles/card.png\" media-type=\"image/jpeg\"></item>\n" +
					"</manifest>\n");*/
			strManifest = strManifest.concat(
					"<item id=\"page"+pageNumber+"\" href=\""+pageNumber+".htm\" media-type=\"application/xhtml+xml\" />\n" +
					"<item id=\"coverimage\" href=\"FreeFiles/card.png\" media-type=\"image/jpeg\"></item>\n" +
					"</manifest>\n");
			
			/*strSpine = strSpine.concat("<spine toc=\"ncx\">\n" +
					"<itemref idref=\"page"+pageNumber+"\" />\n" +
					"</spine>\n");*/
			strSpine = strSpine.concat("<itemref idref=\"page"+pageNumber+"\" />\n" +
					"</spine>\n");
			
			strContent = strContent.concat(strManifest+strSpine+"</package>");
			
			//Write string to file path
			String strContentPath = currentBookPath+"/ePub/OEBPS/content.opf";
			try {
				FileWriter fileWriter = new FileWriter(strContentPath);
				fileWriter.write(strContent);
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} else {
			/*strManifest = strManifest.concat("<manifest>\n" +
					"<item id=\"ncx\" href=\"toc.ncx\" media-type=\"application/x-dtbncx+xml\" />\n" +
					"<item id=\"page"+pageNumber+"\" href=\""+pageNumber+".htm\" media-type=\"application/xhtml+xml\" />\n");*/
			strManifest = strManifest.concat("<item id=\"page"+pageNumber+"\" href=\""+pageNumber+".htm\" media-type=\"application/xhtml+xml\" />\n");
			
			/*strSpine = strSpine.concat("<spine toc=\"ncx\">\n" +
					"<itemref idref=\"page"+pageNumber+"\" />\n");*/
			strSpine = strSpine.concat("<itemref idref=\"page"+pageNumber+"\" />\n");
		}
	}
	
	/**
	 * generate toc ncx file
	 * @param pageNumber
	 */
	private void generateTocFile(int pageNumber) {
		if (pageNumber == 1) {
			strToc = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
					"<!DOCTYPE ncx PUBLIC \"-//NISO//DTD ncx 2005-1//EN\" \"http://www.daisy.org/z3986/2005/ncx-2005-1.dtd\">\n" +
					"<ncx xmlns=\"http://www.daisy.org/z3986/2005/ncx/\" version=\"2005-1\" xml:lang=\"en\">\n" +
					"<head>\n" +
					"<meta name=\"dtb:uid\" content=\"uid\" />\n" +
					"<meta name=\"dtb:depth\" content=\"1\" />\n" +
					"<meta name=\"dtb:totalPageCount\" content=\"0\" />\n" +
					"<meta name=\"dtb:maxPageNumber\" content=\"0\" />\n" +
					"</head>\n" +
					"<docTitle>\n" +
					"<text>SbookAuthorEpub</text>\n" +
					"</docTitle>\n";
			strToc = strToc.concat("<navMap>\n"+
						"<navPoint id=\"navPoint-"+pageNumber+"\" playOrder=\""+pageNumber+"\">\n"+
						"<navLabel>\n"+
						"<text>Cover Page</text>\n"+
						"</navLabel>\n"+
						"<content src=\""+pageNumber+".htm\"/>\n"+
						"</navPoint>\n");
		} else if (pageNumber == pageCount) {
			/*strToc = strToc.concat("<navMap>\n"+
					"<navPoint id=\"navPoint-"+pageNumber+"\" playOrder=\""+pageNumber+"\">\n"+
					"<navLabel>\n"+
					"<text>End Page</text>\n"+
					"</navLabel>\n"+
					"<content src=\""+pageNumber+".htm\"/>\n"+
					"</navPoint>\n"+
					"</navMap>\n"+
					"</ncx>");*/
			strToc = strToc.concat("<navPoint id=\"navPoint-"+pageNumber+"\" playOrder=\""+pageNumber+"\">\n"+
					"<navLabel>\n"+
					"<text>End Page</text>\n"+
					"</navLabel>\n"+
					"<content src=\""+pageNumber+".htm\"/>\n"+
					"</navPoint>\n"+
					"</navMap>\n"+
					"</ncx>");
			
			//Write string to file path
			String tocPath = currentBookPath+"/ePub/OEBPS/toc.ncx";
			try {
				FileWriter fileWriter = new FileWriter(tocPath);
				fileWriter.write(strToc);
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			int pno = pageNumber - 1;
			/*strToc = strToc.concat("<navMap>\n"+
					"<navPoint id=\"navPoint-"+pageNumber+"\" playOrder=\""+pageNumber+"\">\n"+
					"<navLabel>\n"+
					"<text>Page "+pno+"</text>\n"+
					"</navLabel>\n"+
					"<content src=\""+pageNumber+".htm\"/>\n"+
					"</navPoint>\n");*/
			strToc = strToc.concat("<navPoint id=\"navPoint-"+pageNumber+"\" playOrder=\""+pageNumber+"\">\n"+
					"<navLabel>\n"+
					"<text>Page "+pno+"</text>\n"+
					"</navLabel>\n"+
					"<content src=\""+pageNumber+".htm\"/>\n"+
					"</navPoint>\n");
		}
	}
	
}
