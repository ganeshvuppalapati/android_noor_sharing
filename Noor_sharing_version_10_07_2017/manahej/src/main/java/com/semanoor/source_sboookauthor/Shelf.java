package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.semanoor.manahij.GroupBooks;
import com.semanoor.manahij.MainActivity;
import com.semanoor.manahij.R;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;


public class Shelf implements Serializable {
	
	private int _noOfBooks;
	public transient DatabaseHandler db;
	public transient MainActivity context;
	public ArrayList<Object> bookListArray = new ArrayList<Object>();
	public transient UserFunctions userFunction;
	public ArrayList<Templates> templatesPortraitListArray;
	public ArrayList<Templates> templatesLandscapeListArray;
	public ArrayList<Category> categoryListArray;
	public ArrayList<Book> downloadInProgressList = new ArrayList<>();
	public ArrayList<String> malzamahBookList;
	public ArrayList<GroupBooks> groupBook=new ArrayList<GroupBooks>();

	public Shelf(final MainActivity _context, DatabaseHandler _databaseHandler, UserFunctions _userfn){
		this.context = _context;
		this.db = _databaseHandler;
		this.userFunction = _userfn;

		if (Globals.isTablet()) {
			this.templatesPortraitListArray = db.getAllTemplatesListBasedOnOrientation("portrait");
			this.templatesLandscapeListArray = db.getAllTemplatesListBasedOnOrientation("landscape");
		}
		this.categoryListArray = db.getAllCategoryList();
		this.bookListArray = db.getAllBookContent(this,"");
		this.groupBook=db.getAllGroupList();
		this._noOfBooks = this.bookListArray.size();
	}

	public ArrayList<Category> reloadCategoryListArray(){
		this.categoryListArray.clear();
		this.categoryListArray = db.getAllCategoryList();
		return categoryListArray;
	}
	public ArrayList<Book> downloadInProgressArray(){
	//	this.bookListArray = db.getAllBookContent(this,"");
		this.downloadInProgressList.clear();
		for (int i = 0;i<this.bookListArray.size();i++){
			Book book = (Book) this.bookListArray.get(i);
			if (book.getIsDownloadCompleted().equals("downloading")){
				downloadInProgressList.add(downloadInProgressList.size(),book);
			}
		}
		Collections.reverse(downloadInProgressList);
		return downloadInProgressList;
	}


	public void setNoOfBooks(int bookCount){
		this._noOfBooks = bookCount;
	}
	
	public int getNoOfBooks(){
		return this._noOfBooks;
	}
	
	/**
	 * @author add new book in database and create a new directory
	 * @param orientation 
	 */
	public void addNewBook(int orientation, int templateId){
		Book lastBook;
		int lastBookIndex;
		if (bookListArray.isEmpty()) {
			lastBookIndex = 0;
		} else {
			lastBook = (Book) bookListArray.get(bookListArray.size()-1);
			lastBookIndex = lastBook.getBookIndex();
		}
		int lastBookId = db.getMaxUniqueRowID("books");
		int lastViewedPage = 1;
		String bStoreID = null;
		boolean isStoreBook = false;
		boolean showPageNo = false;
		Templates template = getTemplates(templateId, orientation,context);
		boolean isStoreBookCreatedFromTab = false;
		int categoryId = 2;
		String bookLanguage = "English";
		int Bid=lastBookId+1;
		Book newBook = new Book(Bid, "New Book "+Bid, "Book", "Lorem Ipsum", templateId, 3, 0, orientation, lastBookIndex+1, isStoreBook,lastViewedPage,bStoreID,null,showPageNo,null,null, template, isStoreBookCreatedFromTab, categoryId, bookLanguage, "downloaded", "", "yes", "no", "0.00", "", "created",0,"0","ltr");
		this.bookListArray.add(0,newBook);
		this.setNoOfBooks(this.bookListArray.size());
		db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,ShowPageNo,CID,StoreBookCreatedFromIpad,blanguage,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,clientID,bookDirection) values('"+"New Book "+Bid+"','"+"Book"+"','"+"User"+"','"+templateId+"','"+"3"+"','"+"0"+"','"+orientation+"','"+newBook.getBookIndex()+"','"+isStoreBook+"','"+lastViewedPage+"','"+showPageNo+"','"+categoryId+"','"+"no"+"', '"+bookLanguage+"','downloaded','','yes','no','0.00','','created','0','ltr')");
		UserFunctions.createNewBooksDirAndCoptTemplates(Globals.TARGET_BASE_TEMPATES_PATH+templateId, Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID()+"/FreeFiles");
		UserFunctions.makeFileWorldReadable(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID());
		UserFunctions.createNewDirectory(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+newBook.getBookID());
		UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID()+"/tempFiles");
	//	addBookTitleAndAuthorName(newBook);
	}

	public void createNewAlbum(String albumTitle, int pageCount, DatabaseHandler databaseHandler){
		Book lastBook;
		int lastBookIndex;
		if (bookListArray.isEmpty()) {
			lastBookIndex = 0;
		} else {
			lastBook = (Book) bookListArray.get(bookListArray.size()-1);
			lastBookIndex = lastBook.getBookIndex();
		}

		int lastBookId = databaseHandler.getMaxUniqueRowID("books")+1;
		int lastViewedPage = 1;
		String bStoreID = "A"+lastBookId;
		boolean isStoreBook = false;
		boolean showPageNo = false;
		//Templates template = getTemplates(templateId, orientation);
		boolean isStoreBookCreatedFromTab = false;
		int categoryId = Globals.myMediaCatId;
		String bookLanguage = "English";
		Book newBook = new Book(lastBookId, albumTitle, "MyMedia", "semaandroid", pageCount, 0, 0, 0, lastBookIndex+1, isStoreBook,lastViewedPage,bStoreID,null,showPageNo,null,null, null, isStoreBookCreatedFromTab, categoryId, bookLanguage, "downloaded", "", "no", "no", "0.00", "", "created",0,"0","ltr");
		this.bookListArray.add(0,newBook);
		this.setNoOfBooks(this.bookListArray.size());
		databaseHandler.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,storeID,ShowPageNo,CID,StoreBookCreatedFromIpad,blanguage,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,clientID,bookDirection) values('"+albumTitle+"','"+"MyMedia"+"','"+"semaandroid"+"','"+"0"+"','"+pageCount+"','"+"0"+"','"+"0"+"','"+newBook.getBookIndex()+"','"+isStoreBook+"','"+lastViewedPage+"','"+bStoreID+"','"+showPageNo+"','"+categoryId+"','"+"no"+"', '"+bookLanguage+"','downloaded','','no','no','0.00','','created','0','ltr')");
		databaseHandler.executeQuery("update tblCategory set isHidden='false' where CID='"+categoryId+"'and isHidden='true'");
		UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID()+"/FreeFiles");
		String srcLoc = Globals.TARGET_BASE_BOOKS_DIR_PATH+"album/org_image/";
		String srcThmbLoc = Globals.TARGET_BASE_BOOKS_DIR_PATH+"album/thumb_image/";
		String destLoc = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID();
		try {
			UserFunctions.copyDirectory(new File(srcLoc), new File(destLoc));
			UserFunctions.copyDirectory(new File(srcThmbLoc), new File(destLoc));
		} catch (IOException e) {
			e.printStackTrace();
		}
		//Copy Card Image
		String[] albumImgList = new File(srcLoc).list();
		Bitmap bitmap = BitmapFactory.decodeFile(srcLoc+albumImgList[0]);
		Bitmap scaleBitmap = Bitmap.createScaledBitmap(bitmap, 200, 300, false);
		String cardImgPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID()+"/FreeFiles/card.png";
		UserFunctions.saveBitmapImage(scaleBitmap, cardImgPath);
	}
	
	/**
	 * get template
	 * @param templateId
	 * @param orientation
	 * 
	 * @return
	 */
	public Templates getTemplates(int templateId, int orientation, Context context) {
		ArrayList<Templates> templateList = null;
		db = DatabaseHandler.getInstance(context);
		if (orientation == Globals.portrait) {
			templateList =  db.getAllTemplatesListBasedOnOrientation("portrait");
		}
		if(templateList!=null) {
			for (Templates templates : templateList) {
				if (templates.getTemplateId() == templateId) {
					return templates;
				}
			}
		}
		return null;
	}
	
	/**
	 * add book title and author for newly added book
	 * @param book
	 */
	private void addBookTitleAndAuthorName(Book book){
		int width, height;
		if (book.getBookOrientation() == Globals.portrait) {
			if (Globals.getDeviceWidth() > Globals.getDeviceHeight()) {
				width = Globals.getDeviceHeight();
				height = Globals.getDeviceWidth();
			} else {
				width = Globals.getDeviceWidth();
				height = Globals.getDeviceHeight();
			}
		} else {
			if (Globals.getDeviceWidth() > Globals.getDeviceHeight()) {
				width = Globals.getDeviceWidth();
				height = Globals.getDeviceHeight();
			} else {
				width = Globals.getDeviceHeight();
				height = Globals.getDeviceWidth();
			}
		}
		Templates template = book.getTemplate();
		String[] titleColor = template.getTemplateTitleColor().split("-");
		//System.out.println(titleColor);
		int titleTextWidth = (int) context.getResources().getDimension(R.dimen.bookview_new_title_text_width);
		int titleTextHeight = (int) context.getResources().getDimension(R.dimen.bookview_new_title_text_height);
		int pxWidth = Globals.getPixelValue(titleTextWidth, context);
		int pxHeight = Globals.getPixelValue(titleTextHeight, context);
		int titleTextSize = (int) context.getResources().getDimension(R.dimen.bookview_new_title_text_size);
		int titleTextXPos = (int) (width - (titleTextWidth + context.getResources().getDimension(R.dimen.bookview_new_title_author_text_padding_right)));
		int titleTextYPos = height - (height/2 + titleTextHeight);
		String title = book.getBookTitle();
		String titleText = "<div id=\"content\" contenteditable=\"true\" style=\"background-color: transparent; font-size: "+titleTextSize+"px; margin-top:30px; color: rgb("+titleColor[0]+", "+titleColor[1]+", "+titleColor[2]+"); " +
				"direction: ltr; text-align: center; width: "+pxWidth+"px; height: "+pxHeight+"px;box-sizing:border-box;font-family: "+template.getTemplateTitleFont()+";\">"+title+"</div>";
		int objSequentialId = 1;
		String objType = Globals.OBJTYPE_TITLETEXT;
		String location = titleTextXPos+"|"+titleTextYPos;
		String size = titleTextWidth+"|"+titleTextHeight;
		String objContent = titleText;
		addWebTextToDB(location, size, objContent, objSequentialId, 1, 0, objType, book);
		
		String[] authorColor = template.getTemplateAuthorColor().split("-");
		int authorTextWidth = (int) context.getResources().getDimension(R.dimen.bookview_new_author_text_width);
		int authorTextHeight = (int) context.getResources().getDimension(R.dimen.bookview_new_author_text_height);
		int pxWidth1 = Globals.getPixelValue(authorTextWidth, context);
		int pxHeight1 = Globals.getPixelValue(authorTextHeight, context);
		int authorTextSize = (int) context.getResources().getDimension(R.dimen.bookview_new_author_text_size);
		int authorTextXPos = (int) (width - (authorTextWidth + context.getResources().getDimension(R.dimen.bookview_new_title_author_text_padding_right)));
		int authorTextYPos = height - (authorTextHeight*6);
		String authorName = book.getBookAuthorName();
		String authorText = "<div id=\"content\" contenteditable=\"true\" style=\"background-color: transparent; font-size: "+authorTextSize+"px; margin-top:30px; color: rgb("+authorColor[0]+", "+authorColor[1]+", "+authorColor[2]+"); " +
				"direction: ltr; text-align: center; width: "+pxWidth1+"px; height: "+pxHeight1+"px;box-sizing:border-box;font-family: "+template.getTemplateAuthorFont()+";\">"+authorName+"</div>";
		int objSequentialId1 = 2;
		String objType1 = Globals.OBJTYPE_AUTHORTEXT;
		String location1 = authorTextXPos+"|"+authorTextYPos;
		String size1 = authorTextWidth+"|"+authorTextHeight;
		String objContent1 = authorText;
		addWebTextToDB(location1, size1, objContent1, objSequentialId1, 1, 0, objType1, book);
	}
	
	/**
	 * Add WebText To database
	 * @param location
	 * @param size
	 * @param objContent
	 * @param objSequentialId
	 * @param pageNumber
	 * @param enrichPageId
	 */
	private void addWebTextToDB(String location, String size, String objContent, int objSequentialId, int pageNumber, int enrichPageId, String objType, Book book){
		int objBID = book.getBookID();
		String objSclaPageToFit = "no";
		boolean objLocked = false;
		db.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('"+""+"', '"+objType+"', '"+objBID+"', '"+pageNumber+"', '"+location+"', '"+size+"', '"+objContent+"', '"+objSequentialId+"', '"+objSclaPageToFit+"', '"+objLocked+"', '"+enrichPageId+"')");
	}
	
	/**
	 * Add New Store Book
	 * @param bTitle
	 * @param bDescription
	 * @param bAuthor
	 * @param bTemplate
	 * @param bTotalPages
	 * @param bPageBgValue
	 * @param bOrientation
	 * @param isStoreBook
	 * @param lastViewedPage
	 * @param bStoreID
	 * @param categoryId
	 * @param lSearch
	 * @param lBookmark
	 * @param lCopy
	 * @param lFlip
	 * @param lNavigation
	 * @param lHighlight
	 * @param lNote
	 * @param lGoToPage
	 * @param lIndexPage
	 * @param lZoom
	 * @param lWebSearch
	 * @param lWikiSearch
	 * @param lUpdateCounts
	 * @param lGoogle
	 * @param lYoutube
	 * @param lNoor
	 * @param lNoorBook
	 * @param lYahoo
	 * @param lBing
	 * @param lAsk
	 * @param lJazira
	 * @param lBookLanguage
	 * @param lTranslationSearch
	 * @param lDictionarySearch
	 * @param lBookSearch
	 * @param url
	 * @return
	 */
	public Book addNewStoreBook(String bTitle, String bDescription, String bAuthor, int bTemplate, int bTotalPages, int bPageBgValue, int bOrientation, Boolean isStoreBook, int lastViewedPage, String bStoreID, int categoryId, String lSearch, String lBookmark, String lCopy, String lFlip, String lNavigation, String lHighlight, String lNote, String lGoToPage, String lIndexPage, String lZoom, String lWebSearch, String lWikiSearch, String lUpdateCounts, String lGoogle, String lYoutube, String lNoor, String lNoorBook, String lYahoo, String lBing, String lAsk, String lJazira, String lBookLanguage, String lTranslationSearch, String lDictionarySearch, String lBookSearch, String isDownloadCompleted, String downloadURL, String isEditable, String price, String imgURL, String purchaseType, String url,String clientID) {
		Book lastBook;
		int lastBookIndex;
		if (bookListArray.isEmpty()) {
			lastBookIndex = 1;
		} else {
			lastBook = (Book) bookListArray.get(bookListArray.size()-1);
			lastBookIndex = lastBook.getBookIndex();
		}
		if (db == null) {
			db = DatabaseHandler.getInstance(context);
		}
		int lastBookId = db.getMaxUniqueRowID("books");
		boolean showPageNo = false;
		Templates template = getTemplates(bTemplate, bOrientation,context);
		boolean isStoreBookCreatedFromTab = false;
		if(lastViewedPage==0){
			lastViewedPage=1;
		}
		String isMalzamah = "no";
		Book newBook = new Book(lastBookId+1, bTitle, bDescription, bAuthor, bTemplate, bTotalPages, bPageBgValue, bOrientation, lastBookIndex+1, isStoreBook, lastViewedPage, bStoreID, null, showPageNo, null, null, template, isStoreBookCreatedFromTab, categoryId, lBookLanguage, isDownloadCompleted, downloadURL, isEditable, isMalzamah, price, imgURL, purchaseType,0,clientID,"ltr");
		this.bookListArray.add(0,newBook);
		this.setNoOfBooks(this.bookListArray.size());
		db.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,storeID,ShowPageNo,CID,Search,Bookmark,Copy,Flip,Navigation,Highlight,Note,GotoPage,IndexPage,Zoom,WebSearch,WikiSearch,UpdateCounts,google,youtube,semaBook,semaContent,yahoo,bing,ask,jazeera,blanguage,TranslationSearch,dictionarySearch,booksearch,StoreBookCreatedFromIpad,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,domainUrl,clientID,bookDirection) " +
				"values('"+bTitle+"','"+bDescription+"','"+bAuthor+"','"+bTemplate+"','"+bTotalPages+"','"+bPageBgValue+"','"+bOrientation+"','"+newBook.getBookIndex()+"','"+isStoreBook+"','"+lastViewedPage+"','"+bStoreID+"','"+showPageNo+"','"+categoryId+"','"+lSearch+"','"+lBookmark+"','"+lCopy+"','"+lFlip+"','"+lNavigation+"','"+lHighlight+"','"+lNote+"','"+lGoToPage+"','"+lIndexPage+"','"+lZoom+"','"+lWebSearch+"','"+lWikiSearch+"','"+lUpdateCounts+"','"+lGoogle+"','"+lYoutube+"','"+lNoorBook+"','"+lNoor+"','"+lYahoo+"','"+lBing+"','"+lAsk+"','"+lJazira+"','"+lBookLanguage+"','"+lTranslationSearch+"','"+lDictionarySearch+"','"+lBookSearch+"','"+"no"+"','"+isDownloadCompleted+"','"+downloadURL+"','"+isEditable+"','"+isMalzamah+"','"+price+"','"+imgURL+"','"+purchaseType+"','"+url+"','"+clientID+"','ltr')");
        db.executeQuery("update tblCategory set isHidden='false' where CID='"+categoryId+"'and isHidden='true'");
		UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID()+"/FreeFiles");
		UserFunctions.makeFileWorldReadable(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID());
		UserFunctions.createNewDirectory(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+newBook.getBookID());
		if (newBook.getbCategoryId()==Globals.mindMapCatId){
			UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID()+"/mindMaps");
		}else{
			UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID()+"/tempFiles");
		}
		return newBook;
	}
	
	/**
	 * @author deleteBook
	 * @param position
	 */
	public void deleteBook(int position){
		Book book = (Book) this.bookListArray.get(position);
		int BID= book.getBookID();
	    this.bookListArray.remove(position);
		this.setNoOfBooks(this.bookListArray.size());
		File bookPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+BID);
		userFunction.DeleteDirectory(bookPath);
		File screenShotDirPath = new File(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+BID);
		userFunction.DeleteDirectory(screenShotDirPath);
		db.executeQuery("delete from books where BID = '"+BID+"'");
		db.executeQuery("delete from objects where BID = '"+BID+"'");
		db.executeQuery("delete from BookmarkedSearchTabs where BID = '"+BID+"'");
		db.executeQuery("delete from enrichments where BID = '"+BID+"'");

		if(!book.is_bStoreBook() &&book.getIsMalzamah().equals("yes")){
			db.executeQuery("delete from malzamah where BID = '"+BID+"'");
		}
		if(book.is_bStoreBook()){
			db.executeQuery("delete from tblHighlight where BName like \"%"+book.get_bStoreID()+"%\"");
			db.executeQuery("delete from tblNote where BName like \"%"+book.get_bStoreID()+"%\"");
			db.executeQuery("delete from tblBookmark where BName like \"%"+book.get_bStoreID()+"%\"");
			File storeBookPath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+book.get_bStoreID()+"Book");
			userFunction.DeleteDirectory(storeBookPath);
		}
		if (book.getbCategoryId()==Globals.mindMapCatId) {
			File mindMapImgPath = new File(Globals.TARGET_BASE_MINDMAP_PATH + book.getBookTitle() + ".png");
			File mindMapXmlPath = new File(Globals.TARGET_BASE_MINDMAP_PATH + book.getBookTitle() + ".xml");
			if (mindMapImgPath.exists()) {
				mindMapImgPath.delete();
				mindMapXmlPath.delete();
				// Delete Enrichment tabs and objects
				String deleteEnrQuery = "delete from enrichments where Path='" + book.getBookTitle() + "' and Type='" + Globals.OBJTYPE_MINDMAPTAB + "'";
				String deleteObjQuery = "delete from objects where content='" + book.getBookTitle()+".xml" + "' and objType='" + Globals.OBJTYPE_MINDMAPWEBTEXT + "'";
				db.executeQuery(deleteEnrQuery);
				db.executeQuery(deleteObjQuery);
			}
		}

	}

	public void createWebBook(int orientation,int templateId,int totalPages,ArrayList<String> urlList,DatabaseHandler databaseHandler){
		Book lastBook;
		int lastBookIndex;
		if (bookListArray.isEmpty()) {
			lastBookIndex = 0;
		} else {
			lastBook = (Book) bookListArray.get(bookListArray.size()-1);
			lastBookIndex = lastBook.getBookIndex();
		}
		int lastBookId = databaseHandler.getMaxUniqueRowID("books");
		int lastViewedPage = 1;
		String bStoreID = "w"+lastBookId;
		boolean isStoreBook = false;
		boolean showPageNo = false;
		Templates template = getTemplates(templateId, 0,context);
		boolean isStoreBookCreatedFromTab = false;
		int categoryId = 2;
		String bookLanguage = "English";
		Book newBook = new Book(lastBookId+1, "New WebBook", "Book", "Lorem Ipsum", templateId, totalPages, 0, 0, lastBookIndex+1, isStoreBook,lastViewedPage,bStoreID,null,showPageNo,null,null, template, isStoreBookCreatedFromTab, categoryId, bookLanguage, "downloaded", "", "yes", "no", "0.00", "", "created",0,"0","ltr");
		this.bookListArray.add(0,newBook);
		this.setNoOfBooks(this.bookListArray.size());
		databaseHandler.executeQuery("insert into books(Title,Description,Author,template,totalPages,PageBGvalue,oriantation,bIndex,StoreBook,LastViewedPage,ShowPageNo,CID,StoreBookCreatedFromIpad,blanguage,isDownloadedCompleted,downloadURL,Editable,isMalzamah,price,imageURL,purchaseType,bookDirection) values('"+"New WebBook"+"','"+"Book"+"','"+"User"+"','"+templateId+"','"+totalPages+"','"+"0"+"','"+0+"','"+newBook.getBookIndex()+"','"+isStoreBook+"','"+lastViewedPage+"','"+showPageNo+"','"+categoryId+"','"+"no"+"', '"+bookLanguage+"','downloaded','','yes','no','0.00','','created','ltr')");
		UserFunctions.createNewBooksDirAndCoptTemplates(Globals.TARGET_BASE_TEMPATES_PATH+templateId, Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID()+"/FreeFiles");
		UserFunctions.makeFileWorldReadable(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID());
		UserFunctions.createNewDirectory(Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+newBook.getBookID());
		UserFunctions.createNewDirectory(Globals.TARGET_BASE_BOOKS_DIR_PATH+newBook.getBookID()+"/tempFiles");
	//	addBookTitleAndAuthorName(newBook);
		String imageType = "IFrame";
		String objSclaPageToFit = "no";
		int width = (int) (Globals.getDeviceWidth()-60);
		int height = (int) (Globals.getDeviceHeight()-120);
		String size = width+"|"+height;
		int imageXpos = Globals.getDeviceWidth()/2 -width/2;
		int imageYpos = Globals.getDeviceHeight()/2 - height/2;
		String location = imageXpos+"|"+imageYpos;
		boolean objLocked = false;
		for (int i=0;i<totalPages-2;i++){
			String objContent = urlList.get(i);
			int pageNo = i+2;
			databaseHandler.executeQuery("insert into objects(objID, objType, BID, pageNO, location, size, content, SequentialID, ScalePageToFit, locked, EnrichedID) values('" + "" + "', '" + imageType + "', '" + newBook.getBookID() + "', '" + pageNo + "', '" + location + "', '" + size + "', '" + objContent.replace("'", "''") + "', '" + 1 + "', '" + objSclaPageToFit + "', '" + objLocked + "', '" + 0 + "')");
		}
	}
	
}
