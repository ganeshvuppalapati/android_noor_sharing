package com.semanoor.source_sboookauthor;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ptg.views.CircleButton;
import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.CloudActivity;
import com.semanoor.manahij.InAppIds;
import com.semanoor.manahij.InappIdData;
import com.semanoor.manahij.MainActivity;
import com.semanoor.manahij.R;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class UserFunctions {

	private static UserFunctions instance;
	public static ArrayList<String> m_fontPaths;
	public static ArrayList<String> m_fontNames;
	public static String fontFace;

	public static synchronized UserFunctions getInstance(){
		if(instance == null){
			instance = new UserFunctions();
		}
		return instance;
	}

	/** Display Alert Dialog
	 * @param ctx
	 * @param msg */

	public static void DisplayAlertDialog(Context ctx, int msg, int title)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	/** Display Alert Dialog
	 * @param ctx
	 * @param msg */

	public static void DisplayAlertDialog(Context ctx, String msg, int title)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	/** Display Alert Dialog
	 * @param ctx
	 * */

	public static void DisplayAlertDialogNotFromStringsXML(Context ctx, String msg, String title)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(title);
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public static ProgressDialog displayProcessDialog(Context context, String msg){
		ProgressDialog processDialog = new ProgressDialog(context);
		processDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		processDialog.setMessage(msg);
		processDialog.setCancelable(false);
		processDialog.setCanceledOnTouchOutside(false);
		processDialog.show();
		return processDialog;
	}

    /** Check Internet Availability
     * @param ctx*/

	public static boolean isInternetExist(Context ctx)
	{
		ConnectivityManager conMgr = (ConnectivityManager) ctx.getSystemService (Context.CONNECTIVITY_SERVICE);

		if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnectedOrConnecting())
		{ return true; }
		else
		{ return false; }
	}


	/** Display Toast Message
	 * @param ctx
	 * @param msg*/

	public void DisplayToast(Context ctx,String msg)
	{
		Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	/** Get Display Resolution
	 * @param display*/

	@SuppressLint("NewApi")
	public DisplayMetrics GetDeviceResolution(Display display)
	{
		DisplayMetrics displayMetrics = new DisplayMetrics();
		display.getRealMetrics(displayMetrics);
		return displayMetrics;
	}

	/** Check for Application Installed
	 * @param ctx*/

	public boolean AppInstalled(Context ctx){
		if(!PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean("installed", false)){
			(PreferenceManager.getDefaultSharedPreferences(ctx).edit().putBoolean("installed", true)).commit();
			return false;
		}else{
			return true;
		}
	}

	/** Copy Template folder from Asset to Files Directory
	 * @param ctx
	 * @param templatesPath
	 * */

	public static void copyTemplatesDirFromAssetToFilesDir(Context ctx, String templatesPath, String folderName){
		File templateDir = new File(templatesPath);
		if (!templateDir.exists()) {
			try {
				String[] path = ctx.getAssets().list(folderName);
				for(String file : path){
					if (file.equals("mimetype")) {
						copyAsset(ctx.getAssets(),folderName + "/" + file,templatesPath + "/"+file);
					} else {
						String fromPath = folderName+"/"+file;
						String toCopyPath = templatesPath+file+"/";
						copyAssetFolder(ctx.getAssets(),fromPath,toCopyPath);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/** Copy folder from Asset to Files Directory
	 * @param assetManager
	 * @param fromAssetPath
	 * @param toPath*/

	public static boolean copyAssetFolder(AssetManager assetManager,String fromAssetPath,String toPath){
		try{
			String[] files = assetManager.list(fromAssetPath);
			new File(toPath).mkdirs();
			boolean res = true;
			for(String file : files)
				if(file.contains("."))
					res &= copyAsset(assetManager,fromAssetPath + "/" + file,toPath + "/"+file);
				else
					res &= copyAssetFolder(assetManager, fromAssetPath + "/" + file,toPath + "/"+file);

			return res;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public static boolean copyAsset(AssetManager assetManager,String fromAssetPath, String toPath){
		InputStream in =null;
		OutputStream out =null;
		try{
			in = assetManager.open(fromAssetPath);
			new File(toPath).createNewFile();
			out = new FileOutputStream(toPath);
			copyFile(in,out);
			in.close();
			in=null;
			out.flush();
			out.close();
			out = null;
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}

	}

	/** Copy font folder from Asset to Files Directory
	 * @param assetManager
	 * @param fromAssetPath
	 * @param toPath*/

	public static boolean copyFontAssetFolder(AssetManager assetManager,String fromAssetPath,String toPath){
		try{
			String[] files = assetManager.list(fromAssetPath);
			new File(toPath).mkdirs();
			boolean res = true;
			for(String file : files)
				if(file.contains("."))
					res &= copyFontAsset(assetManager,fromAssetPath + "/" + file,toPath + "/"+file);
				else
					res &= copyFontAssetFolder(assetManager, fromAssetPath + "/" + file,toPath + "/"+file);

			return res;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public static boolean copyFontAsset(AssetManager assetManager,String fromAssetPath, String toPath){
		File flToPath = new File(toPath);
		if (flToPath.exists()) {
			return false;
		}
		InputStream in =null;
		OutputStream out =null;
		try{
			in = assetManager.open(fromAssetPath);
			new File(toPath).createNewFile();
			out = new FileOutputStream(toPath);
			copyFile(in,out);
			in.close();
			in=null;
			out.flush();
			out.close();
			out = null;
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	private static void copyFile(InputStream in,OutputStream out) throws IOException{
		byte[] buffer = new byte[1024];
		int read;
		while((read = in.read(buffer)) != -1){
			out.write(buffer,0,read);
		}
	}

	/** Put Hash Map
	 * @param key
	 * @param value
	 * */

	public static HashMap<String, String> putData(String key, String value){
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("key", key);
        item.put("value", value);
        return item;
    }

	/** Copy Templates to Book Directory
	 * @param srcPath
	 * @param destPath
	 * */
	public static void createNewBooksDirAndCoptTemplates(String srcPath, String destPath){
		File srcDir = new File(srcPath);
		File destDir = new File(destPath);
		if (!destDir.exists()) {
			destDir.mkdirs();
		}
		if(srcDir.exists()){
			String[] path = srcDir.list();
			for(String file : path){
				String fromPath = srcDir+"/"+file;
				String toCopyPath = destDir+"/"+file;
				copyFiles(fromPath,toCopyPath);
			}
		}
	}

	public static boolean copyFiles(String fromPath, String toPath){
		InputStream in =null;
		OutputStream out =null;
		try{
			in = new FileInputStream(fromPath);
			new File(toPath).createNewFile();
			out = new FileOutputStream(toPath);
			copyFile(in,out);
			in.close();
			in=null;
			out.flush();
			out.close();
			out = null;
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Copy Directory from one destination to another
	 */
	public static void copyDirectory(File sourceLocation , File targetLocation)
	throws IOException {

	    if (sourceLocation.isDirectory()) {
	        if (!targetLocation.exists() && !targetLocation.mkdirs()) {
	            throw new IOException("Cannot create dir " + targetLocation.getAbsolutePath());
	        }

	        String[] children = sourceLocation.list();
	        for (int i=0; i<children.length; i++) {
	            copyDirectory(new File(sourceLocation, children[i]),
	                    new File(targetLocation, children[i]));
	        }
	    } else {

	        // make sure the directory we plan to store the recording in exists
	        File directory = targetLocation.getParentFile();
	        if (directory != null && !directory.exists() && !directory.mkdirs()) {
	            throw new IOException("Cannot create dir " + directory.getAbsolutePath());
	        }

	        InputStream in = new FileInputStream(sourceLocation);
	        OutputStream out = new FileOutputStream(targetLocation);

	        // Copy the bits from instream to outstream
			//copyFile(in,out);
	        byte[] buf = new byte[1024];
	        int len;
	        while ((len = in.read(buf)) > 0) {
	            out.write(buf, 0, len);
	        }
	        in.close();
	        out.close();
	    }
	}

	public static void traverseAndMakeFileWorldReadable(File dir) {
		if (dir.exists()) {
			makeFileWorldReadable(dir.toString());
			File[] files = dir.listFiles();
			for (int i = 0; i < files.length; ++i) {
				File file = files[i];
				if (file.isDirectory()) {
					makeFileWorldReadable(file.toString());
					traverseAndMakeFileWorldReadable(file);
				} else {
					makeFileWorldReadable(file.toString());
				}
			}
		}
	}

	/**
	 *
	 * @param path
	 */
	public static void makeFileWorldReadable(String path){
		File f = new File(path);
		if (f.exists()) {
			f.setReadable(true, false);
			f.setExecutable(true, false);
		}
	}

	/** Replace the Existing Templates File
	 * @param srcPath
	 * @param destPath
	 * */
	public void replaceExistingTemplates(String srcPath, String destPath){
		File srcDir = new File(srcPath);
		File destDir = new File(destPath);

			String[] path = srcDir.list();
			for(String file : path){
				String fromPath = srcDir+"/"+file;
				String toCopyPath = destDir+"/"+file;
				copyFiles(fromPath,toCopyPath);

		}
	}

	/** ReadFileFromPath
	 * @param fileName
	 * */
	public static String readFileFromAssetPath(AssetManager assetManager, String fileName)
	{
		BufferedReader reader = null;
		StringBuilder builder = null;
		InputStream in = null;
		try
		{
			in = assetManager.open(fileName);
			reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			builder = new StringBuilder();
			String line;
			while((line = reader.readLine())!=null)
			{
				builder.append(line+"\n");//bug fixed
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(reader != null)
			{
				try
				{
					reader.close();
				}
				catch (Exception e2)
				{
					e2.printStackTrace();
				}
			}
		}
		return builder.toString();
	}

	/**
	 * Read File from path
	 * @param file
	 * @return
	 */
	public static String readFileFromPath(File file)
	{
		if(!file.exists())
		{
			return null;
		}

		BufferedReader reader = null;
		StringBuilder builder = null;
		try
		{
			reader = new BufferedReader(new FileReader(file));
			builder = new StringBuilder();
			String line;
			while((line = reader.readLine())!=null)
			{
				builder.append(line+"\n");//bug fixed
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(reader != null)
			{
				try
				{
				reader.close();
				}
				catch (Exception e2)
				{
					e2.printStackTrace();
				}
			}
		}
		return builder.toString();
	}


	/** load all System Fonts
	 *
	 */

	public static void loadSystemFonts()
    {

        // Get the fonts on the device
        HashMap< String, String > fonts = FontManager.enumerateFonts();

        m_fontPaths = new ArrayList< String >();
        m_fontNames = new ArrayList< String >();

        fontFace = "";

        // Get the current value to find the checked item
        //String selectedFontPath = ((MainActivity)context).getSharedPreferences().getString( getKey(), "");
        //int idx = 0, checked_item = 0;
        int i = 0;
        for ( String path : fonts.keySet() )
        {
            //if ( path.equals( selectedFontPath ) )
                //checked_item = idx;
        	//m_fontPaths[i] = path;
        	//m_fontNames[i] = fonts.get(path);
        	fontFace = fontFace.concat("@font-face {\n");
            m_fontPaths.add( path );
            m_fontNames.add( fonts.get(path) );
            fontFace = fontFace.concat("font-family: '"+fonts.get(path)+"';\nsrc: url('file://"+path+"');\n");
            fontFace = fontFace.concat("}");
            //idx++;
            i++;
        }

        //fontFace = fontFace.concat("}");
        // Create out adapter
        // If you're building for API 11 and up, you can pass builder.getContext
        // instead of current context
        //FontAdapter adapter = new FontAdapter();

        //builder.setSingleChoiceItems( adapter, checked_item, this );

        // The typical interaction for list-based dialogs is to have click-on-an-item dismiss the dialog
        //builder.setPositiveButton(null, null);
    }

	/** parse Color value
	 * @param input
	 * */
	/*public static int parseColor(String input){
		Pattern c = Pattern.compile("rgb *\\( *([0-9]+), *([0-9]+), *([0-9]+) *\\)");
		Matcher m = c.matcher(input);
		if (m.matches()) {
			return Color.rgb(Integer.valueOf(m.group(1)), Integer.valueOf(m.group(2)), Integer.valueOf(m.group(3)));
		}
		return 0;
	}*/

	/** Delete Directory
	 * @param path (File) */

	public static boolean DeleteDirectory(File path)
	{
		if(path.exists())
		{
			File[] files = path.listFiles();
			for(int i=0;i<files.length;i++)
			{
				if(files[i].isDirectory())
				{
					DeleteDirectory(files[i]);
				}
				else
				{
					files[i].delete();
				}
			}
		}
		return(path.delete());
	}

	/**
	 * this deletes all files from the directory
	 * @param path
	 */
	public static void deleteAllFilesFromDirectory(File path) {
		if(path.exists())
		{
			File[] files = path.listFiles();
			for(int i=0;i<files.length;i++)
			{
				if(files[i].isDirectory())
				{
					DeleteDirectory(files[i]);
				}
				else
				{
					files[i].delete();
				}
			}
		}
	}

	/**
	 * Create New Directory
	 * @param path
	 */
	public static void createNewDirectory(String path){
		File newFile = new File(path);
		if(!newFile.exists()){
			newFile.mkdirs();
		}
	}

	/**
	 * Delete the generated HTML files
	 */
	public static void deleteGeneratedHtmlPage(String htmlFilePath) {
		File filePath = new File(htmlFilePath);
		File[] files = filePath.listFiles();
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			if (file != null) {
				String path = file.getPath();
				if (path != null) {
					if (path.endsWith(".htm") && !path.contains("-")) {
						file.delete();
					}
				}
			}
		}
	}
	/**
	 * Save Bitmap image to the path
	 */
	public static void saveBitmapImage(Bitmap bitmap, String path) {
		File imagePath = new File(path);

		FileOutputStream fos;
		try{
			fos = new FileOutputStream(imagePath);
			bitmap.compress(CompressFormat.JPEG, 100, fos);
			fos.flush();
			fos.close();
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * get bitmap from asset path
	 * @param context
	 * @param strName
	 * @return
	 */
	public static Bitmap getBitmapFromAsset(Context context, String strName) {
		AssetManager assetManager = context.getAssets();

		InputStream istr;
		Bitmap bitmap = null;
		try {
			istr = assetManager.open(strName);
			bitmap = BitmapFactory.decodeStream(istr);
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		return bitmap;
	}

	 /**

	 * Get a file path from a Uri. This will get the the path for Storage Access
	 * Framework Documents, as well as the _data field for the MediaStore and
	 * other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri The Uri to query.
	 */
	public static String getPathFromGalleryForPickedItem(final Context context, final Uri uri) {

	    final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

	    // DocumentProvider
	    if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
	        // ExternalStorageProvider
	        if (isExternalStorageDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            if ("primary".equalsIgnoreCase(type)) {
	                return Environment.getExternalStorageDirectory() + "/" + split[1];
	            }

	            // TODO handle non-primary volumes
	        }
	        // DownloadsProvider
	        else if (isDownloadsDocument(uri)) {

	            final String id = DocumentsContract.getDocumentId(uri);
	            final Uri contentUri = ContentUris.withAppendedId(
	                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

	            return getDataColumn(context, contentUri, null, null);
	        }
	        // MediaProvider
	        else if (isMediaDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
	            final String type = split[0];

	            Uri contentUri = null;
	            if ("image".equals(type)) {
	                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
	            } else if ("video".equals(type)) {
	                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
	            } else if ("audio".equals(type)) {
	                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
	            }

	            final String selection = "_id=?";
	            final String[] selectionArgs = new String[] {
	                    split[1]
	            };

	            return getDataColumn(context, contentUri, selection, selectionArgs);
	        }
	    }
	    // MediaStore (and general)
	    else if ("content".equalsIgnoreCase(uri.getScheme())) {
	        return getDataColumn(context, uri, null, null);
	    }
	    // File
	    else if ("file".equalsIgnoreCase(uri.getScheme())) {
	        return uri.getPath();
	    }

	    return null;
	}

	/**
	 * Get the value of the data column for this Uri. This is useful for
	 * MediaStore Uris, and other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri The Uri to query.
	 * @param selection (Optional) Filter used in the query.
	 * @param selectionArgs (Optional) Selection arguments used in the query.
	 * @return The value of the _data column, which is typically a file path.
	 */
	public static String getDataColumn(Context context, Uri uri, String selection,
	        String[] selectionArgs) {

	    Cursor cursor = null;
	    final String column = "_data";
	    final String[] projection = {
	            column
	    };

	    try {
	        cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
	                null);
	        if (cursor != null && cursor.moveToFirst()) {
	            final int column_index = cursor.getColumnIndexOrThrow(column);
	            return cursor.getString(column_index);
	        }
	    } finally {
	        if (cursor != null)
	            cursor.close();
	    }
	    return null;
	}


	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
	    return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
	    return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
	    return "com.android.providers.media.documents".equals(uri.getAuthority());
	}

	/**
	 * Validate the Email ID
	 * @param str
	 * @return
	 */
	public static boolean CheckValidEmail(String str)
	{
		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(str);
		if(matcher.matches())
		{
			return true;
		}
		else
			return false;
	}

	/**
	 * getRecentColorsFromSharedPreferences
	 * @param key
	 * @return
	 *
	 */
	public static ArrayList<String> getRecentColorsFromSharedPreference(String key, Context context){
		SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
		ArrayList<String> recentColorList = null;
		try {
			recentColorList = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreference.getString(key, ObjectSerializer.serialize(new ArrayList<String>())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (recentColorList.size() == 0) {
			recentColorList.add("#B0171F");
			recentColorList.add("#FF82AB");
			recentColorList.add("#0000FF");
			recentColorList.add("#000000");
			recentColorList.add("#006400");
		}
		return recentColorList;
	}

	/**
	 * setandUpdateRecentColorsToSharedPreferences
	 * @param key
	 * @param recentColorList
	 * @param recentColor
	 */
	public static void setandUpdateRecentColorsToSharedPreferences(String key, ArrayList<String> recentColorList, String recentColor, Context context){
		ArrayList<String> tempArrayList = new ArrayList<String>();
		for (int i = 0; i < recentColorList.size(); i++) {
			if (i == 0) {
				tempArrayList.add(recentColor);
			} else {
				tempArrayList.add(recentColorList.get(i-1));
			}
		}
		recentColorList.clear();
		recentColorList = tempArrayList;

		SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor = sharedPreference.edit();
		try {
			editor.putString(key, ObjectSerializer.serialize(recentColorList));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		editor.commit();
	}

	public static void copyHTMLPageToMalzamah(Book currentBook){
		if(currentBook.get_bStoreID() != null && currentBook.get_bStoreID().contains("C")) {
			UserFunctions.createNewBooksDirAndCoptTemplates(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/FreeFiles/", Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/FreeFiles/");
			UserFunctions.createNewBooksDirAndCoptTemplates(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/images/", Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/images/");
			File srcDir = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID()+"/");
			File destDir = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/");
			if(srcDir.exists()){
				String[] path = srcDir.list();
				for(String file : path){
					String fromPath = srcDir+"/"+file;
					if(file.contains(".htm")) {
						String toCopyPath = destDir + "/" + file;
						UserFunctions.copyFiles(fromPath, toCopyPath);
						if (new File(toCopyPath).exists()){
							UserFunctions.encryptFile(new File(toCopyPath));
						}
					}
				}
			}
		}
	}

	public static void saveMindMapScreenShotForPageBg(String mindMapTitle, DatabaseHandler db, Book currentBook, GridShelf gridShelf) {
		ArrayList<String> contentList = db.selectMindMapBgFromObjects(mindMapTitle);
		for (int i=0; i<contentList.size(); i++){
			String content = contentList.get(i);
			String[] splitContent = content.split("##");
			int bid = Integer.parseInt(splitContent[0]);
			int pNo = Integer.parseInt(splitContent[1]);
			int enrId = Integer.parseInt(splitContent[2]);
			int pCount = 0;
			if (bid == currentBook.getBookID()){
				pCount = currentBook.getTotalPages();
			} else {
				ArrayList<Object> bookList = gridShelf.bookListArray;
				for (int j=0; j<bookList.size(); j++){
					Book book = (Book) bookList.get(i);
					if (bid == book.getBookID()){
						pCount = book.getTotalPages();
						break;
					}
				}
			}
			String bgImagePath;
			if(pNo != pCount) {
				if (enrId == 0) {
					bgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + bid + "/FreeFiles/pageBG_" + pNo + ".png";
				} else {
					bgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + bid + "/FreeFiles/pageBG_" + pNo + "-" + enrId + ".png";
				}
			} else {
				bgImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bid+"/FreeFiles/pageBG_End.png";
			}
			String mindMapFilePath = Globals.TARGET_BASE_MINDMAP_PATH+mindMapTitle+".png";
			UserFunctions.copyFiles(mindMapFilePath, bgImagePath);
		}
	}

	public static void writeFile(String filePath, String content){
		try {
			FileWriter fileWriter = new FileWriter(filePath);
			fileWriter.write(content);
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getCurrentDateTimeInString(){
		String rtnValue = "";
		Calendar Cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		rtnValue = df.format(Cal.getTime());
		//Toast.makeText(this, rtnValue, Toast.LENGTH_SHORT).show();
		return rtnValue;
	}

	public static String getValuFromJsonreader(String value){
		String msg = "null";
		JsonReader reader = new JsonReader(new StringReader(value));
		reader.setLenient(true);
		try {
			if (reader.peek() != JsonToken.NULL) {
				if (reader.peek() == JsonToken.STRING) {
					msg = reader.nextString();
					if (msg != null) {
						return msg;
					}
				}
			}
		} catch (Exception e) {

		} finally {
			try {
				reader.close();
			} catch (Exception e2) {
			}
		}
		return msg;
	}

	public static byte[] convertFileToBytearray(File f){
		byte[] byteArray = null;
		try {
			InputStream inputStream = new FileInputStream(f);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] b = new byte[1024 * 8];
			int bytesRead = 0;
			while ((bytesRead = inputStream.read(b)) != -1) {
				bos.write(b, 0, bytesRead);
			}
			byteArray = bos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return byteArray;
	}

	public static void changeFont(ViewGroup parent, Typeface font) {
		for (int i = 0; i < parent.getChildCount(); i++) {
			View child = parent.getChildAt(i);
			if (child instanceof ViewGroup) {
				changeFont(((ViewGroup) child), font);
			} else {
				if (child.getClass() == Button.class) {
					((Button) child).setTypeface(font);
				} else if (child.getClass() == TextView.class) {
					((TextView) child).setTypeface(font);
				}
			}
		}
     }
	public static void complain(String message, Context ctx) {
		//Log.e(TAG, "**** TrivialDrive Error: " + message);
		alert("Error: " + message, ctx);
	}

	public static void alert(String message, Context ctx) {
		AlertDialog.Builder bld = new AlertDialog.Builder(ctx);
		bld.setMessage(message);
		bld.setNeutralButton("OK", null);
		//Log.d(TAG, "Showing alert dialog: " + message);
		bld.create().show();
	}

	public static String milisecondstoDate(long purchaseTime)
	{
		DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");//HH:mm:ss
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(purchaseTime);
		String date=df.format(cal.getTime());

		return date;
	}
	public static String milisecondstoEndDate(long purchaseTime,int days)
	{
		DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");//HH:mm:ss
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(purchaseTime);
		cal.add(Calendar.DATE, days); // Subscription total number of months
		String date=df.format(cal.getTime());

		return date;
	}

	public static long dateDifference(String startDate, String endDate){
		SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		Date d1 = null;
		Date d2 = null;
		try{
			d1 = format.parse(startDate);
			d2 = format.parse(endDate);

			long diff = d2.getTime() - d1.getTime();

			long diffDays = diff / (24 * 60 * 60 * 1000);
			return diffDays;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static long dateDiff(String startDate,String endDate){
		long differenceDates = 0;
		try {
			Date date1;
			Date date2;
			SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");
			date1 = dates.parse(startDate);
			date2 = dates.parse(endDate);
			long difference = Math.abs(date1.getTime() - date2.getTime());
			differenceDates = difference / (24 * 60 * 60 * 1000);
			return differenceDates;
		} catch (Exception exception) {
		}
		return differenceDates;
	}

	public static String getMacAddress(Context ctx){
		WifiManager wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wInfo = wifiManager.getConnectionInfo();
		String macAddress = wInfo.getMacAddress();
	//	String uniqueId = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
		return macAddress;
		//return Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
	}

	public static boolean checkLoginAndAlert(Context ctx, boolean withAlert){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		String scopeId = prefs.getString(Globals.sUserIdKey, "");
		int scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
		if (scopeType > 0 && !scopeId.equals("")) {
			return true;
		} else {
			if (withAlert) {
				DisplayAlertDialog(ctx, R.string.login_subscript, R.string.login);
			}
			return false;
		}
	}
	public static boolean checkLoginAndSubscription(Context context, boolean checkForLogin,ArrayList<HashMap<String,String>> existList,String inAppId,boolean checkForGroups,String purchaseType){
		/*//return true;
		if (Globals.isLimitedVersion()) {
			//if (checkForLogin) {
				if (checkLoginAndAlert(context, true)) {
					return checkForSubscription(context,existList,inAppId,checkForGroups,purchaseType);
				} else {
					return false;
				}

		}else{
			if (checkForLogin){
				return checkLoginAndAlert(context, true);
			}else{
				return true;
			}
		}*/
		return true;
   }
	private static boolean checkForSubscription(Context ctx,ArrayList<HashMap<String,String>> existList,String inAppId,boolean checkForGroups,String purchaseType){
		/*if (purchaseType.equals("purchased")){
			return true;
		}else if (checkRedeemIdExist(inAppId)){
			return true;
		}else{
			if (Globals.isLimitedVersion()) {
				if (UserFunctions.checkClientIdExist(existList, inAppId)) {
					HashMap<String, String> hashMap = getHashMap(existList, inAppId);
					int subscriptDays = Integer.parseInt(hashMap.get("daysLeft"));
					if (subscriptDays > 0) {
						return true;
					} else {
						if (checkForGroups) {
							return true;
						} else {
							DisplayAlertDialog(ctx, R.string.subs_msg, R.string.subs_redeem);
							return false;
						}
					}
				} else {
					if (checkForGroups) {
						return true;
					} else {
						DisplayAlertDialog(ctx, R.string.subs_msg, R.string.subs_redeem);
						return false;
					}
				}
			} else {
				return true;
			}
		}*/
		return true;
	}

	public static boolean checkRedeemInAppIdExist(String inAppId){
		/*if (!getRedeemInAppPurchaseIdEndDate(inAppId).equals("")){
			String endDate = getRedeemInAppPurchaseIdEndDate(inAppId);
			Calendar c = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			String startDate = df.format(c.getTime());
			long days = dateDiff(startDate,endDate);
			if (days>0){
				return true;
			}else{
				return false;
			}
		}
		return false;*/
		return true;
	}

	public static boolean checkRedeemIdExist(String inAppId){
		/*if (!getRedeemInAppIdEndDate(inAppId).equals("")){
			String endDate = getRedeemInAppIdEndDate(inAppId);
			Calendar c = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			String startDate = df.format(c.getTime());
			long days = dateDiff(startDate,endDate);
			if (days>0){
				return true;
			}else{
				return false;
			}
		}
		return false;*/
		return true;
	}

	public static String getDaysleft(String inAppId){
		if (!getRedeemInAppIdEndDate(inAppId).equals("")) {
			String endDate = getRedeemInAppIdEndDate(inAppId);
			Calendar c = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			String startDate = df.format(c.getTime());
			return String.valueOf(dateDiff(startDate, endDate));
		}
		return "0";
	}

	public static String getRedeemInAppIdEndDate(String id){
		InAppIds inAppId = InAppIds.getInstance();
		for (int i=0;i<inAppId.getInAppIdsArrayList().size();i++){
			InappIdData data = inAppId.getInAppIdsArrayList().get(i);
			id = id.toLowerCase();
			if (data.getClientIds().toLowerCase().contains(id)){
				return data.getEndDate();
			}
		}
		return "";
	}

	public static String getRedeemInAppPurchaseIdEndDate(String id){
		InAppIds inAppId = InAppIds.getInstance();
		for (int i=0;i<inAppId.getInAppIdsArrayList().size();i++){
			InappIdData data = inAppId.getInAppIdsArrayList().get(i);
			id = id.toLowerCase();
			if (data.getInAppIds().toLowerCase().contains(id)){
				return data.getEndDate();
			}
		}
		return "";
	}

	public static boolean checkAnyClientSubscribed(Context context){
		/*SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(context);
		ArrayList<HashMap<String,String>> existList = null;
		try {
			existList = (ArrayList<HashMap<String,String>>) ObjectSerializer.deserialize(sharedPreference.getString(Globals.SUBSLIST, ObjectSerializer.serialize(new ArrayList<HashMap<String,String>>())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (existList!=null && existList.size()>0){
			return true;
		}else{
			return false;
		}*/
		return true;
	}

	private static HashMap<String,String> getHashMap(ArrayList<HashMap<String,String>> existList,String inAppId){
		HashMap<String,String> hashMap = null;
		for (int i=0;i<existList.size();i++){
			hashMap = existList.get(i);
			if (hashMap.get("AppstoreID").equalsIgnoreCase(inAppId)) {
				return hashMap;
			}
		}
		return hashMap;
	}

	public static boolean checkClientIdExist(ArrayList<HashMap<String,String>> hashMapArrayList,String inAppId){
		for (int i=0;i<hashMapArrayList.size();i++){
			HashMap<String,String> hashMap = hashMapArrayList.get(i);
			if (inAppId.equalsIgnoreCase(hashMap.get("AppstoreID"))){
				return true;
			}
		}
		return false;
	}

	private static void displayPurchaseAgreementDialog(final Context context){
		final Dialog dialog = new Dialog(context);
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(((Activity) context).getLayoutInflater().inflate(R.layout.purchase_agreement, null));
		dialog.getWindow().setLayout((int) context.getResources().getDimension(R.dimen.purchase_dialog_width), (int) context.getResources().getDimension(R.dimen.purchase_dialog_height));
		CircleButton back_btn = (CircleButton) dialog.findViewById(R.id.btnback);
		back_btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		final ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar1);
		progressBar.setVisibility(View.VISIBLE);
		final WebView webView = (WebView) dialog.findViewById(R.id.webViewPurchase);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String language = prefs.getString(Globals.languagePrefsKey, "en");
		if (language.equals("en")){
			webView.loadUrl("http://www.nooor.com/stores/subscriptionEN.html");
		}else {
			webView.loadUrl("http://www.nooor.com/stores/subscriptionAR.html");
		}

		webView.setWebViewClient(new WebViewClient(){

			@Override
			public void onPageFinished(WebView view, String url){
				progressBar.setVisibility(View.GONE);
			}
		});
		Button dont_Agree = (Button) dialog.findViewById(R.id.btnDonAgree);
		dont_Agree.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			    dialog.dismiss();
			}
		});

		Button btnAgree = (Button) dialog.findViewById(R.id.btnAgree);
		btnAgree.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				if (context instanceof BookViewReadActivity) {
//					((BookViewReadActivity) context).subscribePurchase(Globals.SUBSCRIPTION_INAPP_PURCHASE_PRODUCTID);
//				} else if(context instanceof MainActivity) {
//					((MainActivity) context).subscribePurchase(Globals.SUBSCRIPTION_INAPP_PURCHASE_PRODUCTID);
//				}
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	public static String convertStreamToString(InputStream input) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		StringBuilder sb = new StringBuilder();
		String line = null;

		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line).append('\n');
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				input.close();
			}catch (IOException e){
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	public static String convertJsonFileToString(File input) throws FileNotFoundException {

		BufferedReader reader = new BufferedReader(new FileReader(input));
		StringBuilder sb = new StringBuilder();
		String line = null;

		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line).append('\n');
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	public static byte[] generateKey(String password) throws Exception {
		byte[] keyStart = password.getBytes("UTF-8");
		KeyGenerator kgen = KeyGenerator.getInstance("AES");
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
		sr.setSeed(keyStart);
		kgen.init(128, sr);
		SecretKey skey = kgen.generateKey();
		return skey.getEncoded();
	}

	public static byte[] decodeFile(byte[] key, byte[] fileData) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/ZeroBytePadding");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec,new IvParameterSpec(new byte[cipher.getBlockSize()]));
		return cipher.doFinal(fileData);
	}
	public static String decryptFile(File file){
		byte[] fileData = new byte[(int) file.length()];
		FileInputStream fin = null;
		BufferedOutputStream bos = null;
		String sourceString = "";
		try {
			fin = new FileInputStream(file);
			fin.read(fileData);
			byte[] yourKey = generateKey(Globals.encryptFilePassWord);
			byte[] filesBytes = decodeFile(yourKey, fileData);
			sourceString = new String(filesBytes,"UTF-8");
			fin.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sourceString;
	}
	public static byte[] encodeFile(byte[] key, byte[] fileData) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/ZeroBytePadding");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec,new IvParameterSpec(new byte[cipher.getBlockSize()]));
		byte[] encrypted = cipher.doFinal(fileData);
		return encrypted;
	}

	public static void encryptFile(File file){
		byte[] fileData = new byte[(int) file.length()];
		FileInputStream fin = null;
		BufferedOutputStream bos = null;
		try {
			fin = new FileInputStream(file);
			fin.read(fileData);
			bos = new BufferedOutputStream(new FileOutputStream(file));
			byte[] yourKey = generateKey(Globals.encryptFilePassWord);
			byte[] filesBytes = encodeFile(yourKey, fileData);
			bos.write(filesBytes);
			bos.flush();
			bos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** Copy Themes folder from Asset to Files Directory
	 * @param ctx
	 * @param templatesPath
	 * */

	public static void copyThemesFromAssetToFilesDir(Context ctx, String templatesPath, String folderName){
		File templateDir = new File(templatesPath);
		if (!templateDir.exists()) {
			try {
				String[] path = ctx.getAssets().list(folderName);
				for(String file : path){
					if (file.equals("mimetype")) {
						copyAsset(ctx.getAssets(),folderName + "/" + file,templatesPath + "/"+file);
					} else {
						String fromPath = folderName+"/"+file;
						String toCopyPath = templatesPath+file+"/";
						copyAssetthemesFolder(ctx.getAssets(),fromPath,toCopyPath,ctx);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static boolean copyAssetthemesFolder(AssetManager assetManager, String fromAssetPath, String toPath, Context ctx){
		try{
			String[] files = assetManager.list(fromAssetPath);
			new File(toPath).mkdirs();
			boolean res = true;
			for(String file : files)
			   // String fileformat=file.replace(".jpg","png");
				if(file.contains(".")) {
					String fileName=file;
					fileName = fileName.replace(".jpg", ".png");
					res &= scaleImages(assetManager, fromAssetPath + "/" + file, toPath+ fileName, fileName, toPath, ctx);
				}else {
					res &= copyAssetFolder(assetManager, fromAssetPath + "/" + file, toPath + file);
				}
			return res;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	public static boolean scaleImages(AssetManager assetManager, String fromAssetPath, String toPath, String f, String path, Context contxt){
		InputStream in =null;
		OutputStream out =null;
		try{
			in = assetManager.open(fromAssetPath);
			//new File(toPath).createNewFile();
			Bitmap b=BitmapFactory.decodeStream(in);
			//if(f.contains("Shelf_bg.jpg")){

				String cardImage=path+"."+f;
			    Bitmap cardBitmap = Bitmap.createScaledBitmap(b, 200, 300, true);
				//Bitmap cardBitmap = Bitmap.createScaledBitmap(b, Globals.getDeviceIndependentPixels(200, contxt), Globals.getDeviceIndependentPixels(300, contxt), true);
				UserFunctions.saveBitmapImage(cardBitmap, cardImage);

			//}
			 Bitmap frontThumbBitmap = Bitmap.createScaledBitmap(b, Globals.getDeviceWidth(), Globals.getDeviceHeight(), true);
			 UserFunctions.saveBitmapImage(frontThumbBitmap, toPath);
			//out = new FileOutputStream(toPath);
			//copyFile(in,out);
			in.close();
			in=null;
			out.flush();
			out.close();
			out = null;
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
//	public void scalingThemeFiles(String srcPath, String destPath,Context contxt){
//		File srcDir = new File(srcPath);
//		File destDir = new File(destPath);
//		if (!destDir.exists()) {
//			destDir.mkdirs();
//		}
//		String[] path = srcDir.list();
//		for(String file : path){
//			String fromPath = srcDir+"/"+file;
//			file=file.replace(".jpg", ".png");
//			String toCopyPath = destDir+"/"+file;
//			Bitmap bitmap = BitmapFactory.decodeFile(fromPath);
//            if(file.contains("shelf_bg.png")){
//				String cardImage=destDir+"/card.png";
//				Bitmap cardBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceIndependentPixels(200, contxt), Globals.getDeviceIndependentPixels(300, contxt), true);
//				UserFunctions.saveBitmapImage(cardBitmap, cardImage);
//
//			}
//			Bitmap frontThumbBitmap = Bitmap.createScaledBitmap(bitmap, Globals.getDeviceWidth(), Globals.getDeviceHeight(), true);
//			UserFunctions.saveBitmapImage(frontThumbBitmap, toCopyPath);
//		}
//	}

	public static void applyingBackgroundImage(String path, final RelativeLayout rl, Activity activity){
		Bitmap b= BitmapFactory.decodeFile(path);
		final BitmapDrawable drawable=new BitmapDrawable(b);
		//ImageView imageView = (ImageView) findViewById(R.id.image);

		//imageView.setImageDrawable(Drawable.createFromPath(mypath.toString()));
		//rl.setBackgroundDrawable(drawable);
		//rl.setBackgroundResource(drawable);
		activity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
					rl.setBackgroundDrawable(drawable);
				} else {
					rl.setBackground(drawable);
				}
			}
		});
	}

	public static String convertUnicode2String(String uniStr) {

		uniStr = uniStr.replaceAll("0x", "\\u");

		String[] array = uniStr.split("u");
		String stringText = "";
		for (int i = 1; i < array.length; i++) {
			int hexValue = Integer.parseInt(array[i], 16);
			stringText += (char) hexValue;
		}
		Spanned sample = Html.fromHtml(stringText);
		return sample.toString();

	}

	public static String reverseString(String str) {

		int len = str.length();
		char[] c = new char[len];
		for (int j = len - 1, i = 0; j >= 0; j--, i++) {
			c[i] = str.charAt(j);
		}
		str = String.copyValueOf(c);
		return str;
	}

	public class processXML extends AsyncTask<Void, ArrayList<String>, Void> {
		final ArrayList<String> searchArray = new ArrayList<String>();
		String filePath;

		public processXML(String filePath) {
			this.filePath = filePath;
		}

		@Override
		protected Void doInBackground(Void... params) {
			searchArray.clear();
			try {
				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();

				DefaultHandler handler = new DefaultHandler() {
					public void startElement(String uri, String localName, String qName,
											 Attributes attributes) throws SAXException {
						if (qName != "Search") {
							String attrValue = attributes.getValue("Data");
							searchArray.add(attrValue);
						}
					}

				};

				//File sdPath = getFilesDir();
				//revert_book_hide :
				//String filePath = currentBookPath + filename;
				//filePath = sdPath+"/"+bookName+"Book/Search.xml";
				InputStream inputStream = new FileInputStream(filePath);
				Reader reader = new InputStreamReader(inputStream, "UTF-8");

				InputSource is = new InputSource(reader);
				is.setEncoding("UTF-8");

				saxParser.parse(is, handler);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
		}
	}

	public static ArrayList<String> processXML(String filePath) {
		final ArrayList<String> searchArray = new ArrayList<String>();
		searchArray.clear();
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			DefaultHandler handler = new DefaultHandler() {
				public void startElement(String uri, String localName, String qName,
										 Attributes attributes) throws SAXException {
					if (qName != "Search") {
						String attrValue = attributes.getValue("Data");
						searchArray.add(attrValue);
					}
				}

			};

			//File sdPath = getFilesDir();
			//revert_book_hide :
			//String filePath = currentBookPath + filename;
			//filePath = sdPath+"/"+bookName+"Book/Search.xml";
			InputStream inputStream = new FileInputStream(filePath);
			Reader reader = new InputStreamReader(inputStream, "UTF-8");

			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");

			saxParser.parse(is, handler);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return searchArray;
	}

	public static boolean checkIsTablet(Activity activity) {
		boolean isTablet = false;
		Display display = activity.getWindowManager().getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);

		float widthInches = metrics.widthPixels / metrics.xdpi;
		float heightInches = metrics.heightPixels / metrics.ydpi;
		double diagonalInches = Math.sqrt(Math.pow(widthInches, 2) + Math.pow(heightInches, 2));
		if (diagonalInches >= 7.0) {
			isTablet = true;
		}

		return isTablet;
	}

	public static boolean isTabletDevice(Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	public static HashMap<Float, Float> hm_pxvalues = new HashMap<Float, Float>();

	public static float toPx(float value) {
		float px = 0;
		if (hm_pxvalues.get(value) != null)
			px = hm_pxvalues.get(value);
		else
			px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, MainActivity.mContext.getResources().getDisplayMetrics());
		return px;
	}

	public static void showAlert(Context context, String message) {
		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setMessage(message);
		alertDialogBuilder.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {

					}
				});


		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();

	}
}
