package com.semanoor.source_sboookauthor;

import java.util.ArrayList;

import com.semanoor.manahij.ExportActivity;
import com.semanoor.manahij.R;
import com.semanoor.manahij.ExportActivity.SearchFeatues;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class UploadSearchListadapter extends BaseAdapter {
	
	private ExportActivity context;
	private ArrayList<Object> searchList;

	public UploadSearchListadapter(ExportActivity ctx,
			ArrayList<Object> searchList) {
		this.context = ctx;
		this.searchList = searchList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return searchList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		if (view == null) {
			view = context.getLayoutInflater().inflate(R.layout.inflate_checklist, null);
		}
		SearchFeatues searchFeature = (SearchFeatues) searchList.get(position);
		TextView tv = (TextView) view.findViewById(R.id.textView1);
		tv.setText(searchFeature.getSearchValues());
		CheckBox cb = (CheckBox) view.findViewById(R.id.checkBox1);
		cb.setChecked(searchFeature.isChecked());
		return view;
	}

}
