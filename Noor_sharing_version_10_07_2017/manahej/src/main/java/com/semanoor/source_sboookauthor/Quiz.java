package com.semanoor.source_sboookauthor;

import android.webkit.WebView;

import com.semanoor.manahij.BookViewActivity;

import java.io.File;
import java.util.Random;

public class Quiz {
	String HTMLString = null;
	private static BookViewActivity context;
	public DatabaseHandler db;
	String Language;


	public Quiz(BookViewActivity _context, DatabaseHandler db){
		super();
		this.context = _context;
		this.db=db;
	}
	public String getTypeOneTemplate(int uniqueId, boolean withContainer, String language){
		int typeNo = 1;

		db.executeQuery("insert into tblMultiQuiz(quizId) values('"+uniqueId+"')");

		int sectionId = db.getMaxUniqueRowID("tblMultiQuiz");

		String quizTitleElement = "<div class=\"\" id=\"quizTitle_"+uniqueId+"_"+sectionId+"\" style=\"width:100%; height:65px; overflow:scroll; padding:10px 10px;\n"+
				"box-sizing: border-box;border-bottom:1px solid lightgray; vertical-align:top; display:table;\">\n"+
				"<p id=\"pTitle_"+uniqueId+"\" contenteditable=\"true\" onclick=\"selectAllText()\" style=\"font-weight:bold; word-break: break-all;\">Title"+sectionId+" : Lorem Ipsum dolor amet, consectetur adipisicing elit</p></div>\n";

		String quizQuestionElement = "<div class=\"\" id=\"quiz_question_"+uniqueId+"_"+sectionId+"\" style=\"width:100%;height:90px;background-color:#FFFFFF;\n"+
				"overflow:scroll; padding: 10px 10px; box-sizing:border-box;border-bottom:1px solid lightgray; vertical-align:top; display:table;\">\n"+
				"<p id=\"pQuiz_"+uniqueId+"\" contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor incididunt ut labore et dolore magnaaliqua.</p></div>\n";

		/*String inputDivElement1 = getRadioInputDivElement(uniqueId, sectionId, "block", "A.", "Answer 1", "checked=\"true\"", "a");

		String inputDivElement2 = getRadioInputDivElement(uniqueId, sectionId, "block", "B.", "Answer 2", "", "b");

		String inputDivElement3 = getRadioInputDivElement(uniqueId, sectionId, "block", "C.", "Answer 3", "", "c");

		String inputDivElement4 = getRadioInputDivElement(uniqueId, sectionId, "block", "D.", "Answer 4", "", "d");

		String inputDivElement5 = getRadioInputDivElement(uniqueId, sectionId, "none", "E.", "Answer 5", "", "e");

		String inputDivElement6 = getRadioInputDivElement(uniqueId, sectionId, "none", "F.", "Answer 6", "", "f");*/

		String inputBtnPrev = getInputBtnPrevious(uniqueId, sectionId, withContainer);

		String inputBtnDele = getInputBtnDelete(uniqueId, sectionId, typeNo);

		String inputBtnChkAns = getInputBtnCheckAns(uniqueId, sectionId);

		String inputBtnAdd = getInputBtnAdd(uniqueId, sectionId);

		String inputBtnNext = getInputBtnNext(uniqueId ,sectionId, withContainer);

		String resultsContainer="";
		if (withContainer) {
			resultsContainer = getResultsContainer(uniqueId,language);
		}
		String typeOneTemplate;
		String direction="ltr";
        String optionsElement1= "A.", optionsElement2 = "B.", optionsElement3 = "C.", optionsElement4 ="D.", optionsElement5 ="E.", optionsElement6="F.";
		if(language.contains("ar")){
			direction="rtl";
			optionsElement1= "أ) "; optionsElement2 = "ب)"; optionsElement3 = "ت)"; optionsElement4 ="جـ)"; optionsElement5 ="حـ)"; optionsElement6="د)";

		}
		Language=language;
		String inputDivElement1 = getRadioInputDivElement(uniqueId, sectionId, "block", optionsElement1, "Answer 1", "checked=\"true\"", "a",withContainer);

		String inputDivElement2 = getRadioInputDivElement(uniqueId, sectionId, "block", optionsElement2, "Answer 2", "", "b",withContainer);

		String inputDivElement3 = getRadioInputDivElement(uniqueId, sectionId, "block", optionsElement3, "Answer 3", "", "c",withContainer);

		String inputDivElement4 = getRadioInputDivElement(uniqueId, sectionId, "block", optionsElement4, "Answer 4", "", "d",withContainer);

		String inputDivElement5 = getRadioInputDivElement(uniqueId, sectionId, "none", optionsElement5, "Answer 5", "", "e",withContainer);

		String inputDivElement6 = getRadioInputDivElement(uniqueId, sectionId, "none", optionsElement6, "Answer 6", "", "f",withContainer);

		if(withContainer){

			typeOneTemplate = "<div class=\"\" id=\"content\" style=\"width:400px; height:610px; background-color: rgb(227, 227, 227);box-sizing:border-box;overflow:hidden;display:-webkit-box;font-size:16px;font-family:Verdana;\">" +
					"<div class=\"quiz_1\" id=\"q"+sectionId+"_"+uniqueId+"\" style=\"width:100%; height:100%; position:relative; box-sizing: border-box; padding: 10px;direction:"+direction+"\">\n"+quizTitleElement+""+quizQuestionElement+""+
					"<div class=\"\" id=\"quiz_options_"+uniqueId+"_"+sectionId+"\" style=\"width:100%;height:315px;background-color:#FFFFFF;overflow:scroll; padding:5px 5px;box-sizing:border-box; text-align:center; vertical-align:top; display:table;\">\n"+
					"<div class=\"\" id=\"q"+sectionId+"_result_"+uniqueId+"\" style=\"text-align:center;font-weight:bold; visibility:hidden;\"><p> Excellent </p></div>"+
					""+inputDivElement1+""+inputDivElement2+""+inputDivElement3+""+inputDivElement4+""+inputDivElement5+""+inputDivElement6+"</div>\n"+
			 		"<div class=\"\" id=\"quiz_checkanswer_"+uniqueId+"\" style=\"width:95%;height:55px;padding:5px 5px;\n"+
					"box-sizing:border-box;overflow:hidden;text-align:center; position:absolute; bottom:0;\">\n"+
					""+inputBtnPrev+""+inputBtnDele+""+inputBtnChkAns+""+inputBtnAdd+""+inputBtnNext+"</div></div>"+resultsContainer+"</div>";
		}else{

			typeOneTemplate = "<div class=\"quiz_1\" id=\"q"+sectionId+"_"+uniqueId+"\" style=\"width:100%; height:100%; position:relative; box-sizing: border-box; padding: 10px;direction:"+direction+"\">\n"+quizTitleElement+""+quizQuestionElement+""+
					"<div class=\"\" id=\"quiz_options_"+uniqueId+"_"+sectionId+"\" style=\"width:100%;height:315px;background-color:#FFFFFF;overflow:scroll; padding:5px 5px;box-sizing:border-box; text-align:center; vertical-align:top; display:table;\">\n"+
					"<div class=\"\" id=\"q"+sectionId+"_result_"+uniqueId+"\" style=\"text-align:center;font-weight:bold; visibility:hidden;\"><p> Excellent </p></div>"+
					""+inputDivElement1+""+inputDivElement2+""+inputDivElement3+""+inputDivElement4+""+inputDivElement5+""+inputDivElement6+"</div>\n"+
					"<div class=\"\" id=\"quiz_checkanswer_"+uniqueId+"\" style=\"width:95%;height:55px;padding:5px 5px;\n"+
					"box-sizing:border-box;overflow:hidden;text-align:center; position:absolute; bottom:0;\">\n"+
					""+inputBtnPrev+""+inputBtnDele+""+inputBtnChkAns+""+inputBtnAdd+""+inputBtnNext+"</div></div>"+resultsContainer+"";
		}

		return typeOneTemplate;
	}

	public  String getRadioInputDivElement(int uniqueId, int sectionId, String display, String option, String answer, String checked, String answerVal, boolean withContainer)
	{
        /*String inputDivElement = "<div class=\"\" id=\"q"+sectionId+"_div_"+answerVal+"_"+uniqueId+"\" style=\"width:100%; height:40px;display:"+display+";\">\n"+
				"<div class=\"\" style=\"width:36%; text-align:right; float:left;\">\n"+
				"<input type=\"radio\" name=\"q"+sectionId+"_"+uniqueId+"\" value=\""+answerVal+"\" id=\"q"+sectionId+""+answerVal+"_"+uniqueId+"\" "+checked+" onclick=\"assignAnswer('q"+sectionId+""+answerVal+"_"+uniqueId+"', 'btnCheckAnswer"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_result_"+uniqueId+"')\">\n</div>\n"+
				"<div class=\"\" style=\"width:60%; text-align:left; float:right;\">\n"+
				"<label contenteditable=\"true\"><b> "+option+"&nbsp;</b></label><label contenteditable=\"true\" onclick=\"selectAllText()\">"+answer+"</label></div></div>\n";
		*/

		String div1Style = "id=\"q"+sectionId+"_div_1_"+answerVal+"_"+uniqueId+"\" style=\"width:36%; text-align:right; float:left; clear:both; margin-right:2%;\"";
		String div2Style = "id=\"q"+sectionId+"_div_2_"+answerVal+"_"+uniqueId+"\"style=\"width:60%; text-align:left; float:left;\"";
		if (withContainer){
			if (Language.contains("ar")) {
				div1Style = "id=\"q"+sectionId+"_div_1_"+answerVal+"_"+uniqueId+"\"style=\"width:36%; text-align:left; float:right; clear:both; margin-right:2%;\"";
				div2Style = "id=\"q"+sectionId+"_div_2_"+answerVal+"_"+uniqueId+"\" style=\"width:60%; text-align:right; float:left;\"";
			}
		}else{
			if (Language.contains("ar")) {
					div1Style = "id=\"q"+sectionId+"_div_1_"+answerVal+"_"+uniqueId+"\" style=\"width:36%;text-align:left; float:right; clear:both; margin-right:2%;\"";
					div2Style = "id=\"q"+sectionId+"_div_2_"+answerVal+"_"+uniqueId+"\" style=\"width:60%;text-align:right;float:left;\"";
			}

		}

        String inputDivElement = "<div class=\"\" id=\"q"+sectionId+"_div_"+answerVal+"_"+uniqueId+"\" style=\"width:100%; height:auto;display:"+display+";min-height:40px;float:right;margin-right:0px;margin-left:2%;\">\n"+
				"<div "+div1Style+">\n"+
				"<input type=\"radio\" name=\"q"+sectionId+"_"+uniqueId+"\" value=\""+answerVal+"\" id=\"q"+sectionId+""+answerVal+"_"+uniqueId+"\" "+checked+" onclick=\"assignAnswer('q"+sectionId+""+answerVal+"_"+uniqueId+"', 'btnCheckAnswer"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_result_"+uniqueId+"')\"style=\"left:0px; right: 1%;\">\n</div>\n"+
				"<div "+div2Style+">\n"+
				"<label contenteditable=\"true\"><b> "+option+"&nbsp;</b></label><label contenteditable=\"true\" onclick=\"selectAllText()\"  style=\"word-break: break-all;\">"+answer+"</label></div></div>\n";

		return inputDivElement;
	}

	public String getInputBtnPrevious(int uniqueId, int sectionId, boolean withContainer){
		String inputBtnPrev;
		if(withContainer){
			inputBtnPrev = "<input type=\"button\" class=\"btnPrev\" value=\"<\" style=\"visibility:hidden\" id=\"btnPrevious\" onclick=\"window.location='previousquestion:q" + sectionId + "_" + uniqueId + ":" + sectionId + "';quizPreviousView('q" + sectionId + "_" + uniqueId + "')\">\n";
        }else {
			inputBtnPrev = "<input type=\"button\" class=\"btnPrev\" value=\"<\" id=\"btnPrevious\" onclick=\"window.location='previousquestion:q" + sectionId + "_" + uniqueId + ":" + sectionId + "';quizPreviousView('q" + sectionId + "_" + uniqueId + "')\">\n";
		}
			return inputBtnPrev;
	}

	public String getInputBtnDelete(int uniqueId, int sectionId,int typeNo){
		String inputBtnDele = "<input type=\"button\" class=\"btnDel\" value=\"-\" id=\"btnDelete\" onclick=\"window.location='deletequestion:q"+sectionId+"_"+uniqueId+":"+sectionId+":"+typeNo+"'\">\n";
		return inputBtnDele;	
	}

	public String  getInputBtnCheckAns(int uniqueId, int sectionId){
		String inputBtnChkAns = "<input class=\"btnChkAnswer\" type=\"button\" value=\"CheckAnswer\" style=\"visibility:hidden\" id=\"btnCheckAnswer"+sectionId+"_"+uniqueId+"\" onclick=\"checkAnswer('q"+sectionId+"a_"+uniqueId+"', 'btnCheckAnswer"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_result_"+uniqueId+"')\">\n";
		return inputBtnChkAns;
	}

	public String getInputBtnAdd(int uniqueId, int sectionId){
		String inputBtnAdd = "<input type=\"button\" class=\"btnAdd\" value=\"+\" id=\"btnAdd\" onclick=\"window.location='addquestions:q"+sectionId+"_"+uniqueId+":"+sectionId+"'\">\n";
		return inputBtnAdd;
	}
	public String getInputBtnNext(int uniqueId, int sectionId, boolean withContainer){
		String inputBtnNext;
		if(withContainer){
		  inputBtnNext = "<input type=\"button\" class=\"btnNext\" value=\">\" style=\"visibility:hidden\" id=\"btnNext" + sectionId + "_" + uniqueId + "\" onclick=\"window.location='nextquestion:q" + sectionId + "_" + uniqueId + ":" + sectionId + "';checkAnswer('q" + sectionId + "a_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "');quizNextView('q" + sectionId + "_" + uniqueId + "');\">";
      }else{
		  inputBtnNext = "<input type=\"button\" class=\"btnNext\" value=\">\" id=\"btnNext" + sectionId + "_" + uniqueId + "\" onclick=\"window.location='nextquestion:q" + sectionId + "_" + uniqueId + ":" + sectionId + "';checkAnswer('q" + sectionId + "a_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "');quizNextView('q" + sectionId + "_" + uniqueId + "');\">";
		}
		return inputBtnNext;
	}

	public String getResultsContainer(int uniqueId,String language){
		String btn_back="Back";
		String btn_Restart="Restart";
		if(language.contains("ar")){
			btn_back = "رجوع";
			btn_Restart = "إعادة الاختبار";
		}
		String resultsContainer = "<div class=\"\" id=\"results_container_"+uniqueId+"\" style=\"width:100%; height:100%; box-sizing: border-box;padding: 10px; position:relative;text-align:center;\">\n"+
				"<div class=\"\" id=\"results_"+uniqueId+"\" style=\"background-color:#FFFFFF;display: block;padding: 50px 10px;position: relative;top: 50%;left: 50%;-webkit-transform: translate(-50%, -50%);-ms-transform: translate(-50%, -50%);transform: translate(-50%, -50%);\">\n"+
				"</div>\n"+
				"<div class=\"\" id=\"quiz_result_cont_"+uniqueId+"\" style=\"width:95%;vertical-align:bottom;bottom:0;padding:10px 10px;box-sizing:border-box;overflow:hidden;position:absolute;\">\n"+
				"<input type=\"button\" class=\"btnQuizBack\" value=\""+btn_back+"\" id=\"btnBack\" onclick=\"quizPreviousView('results_container_"+uniqueId+"')\">\n"+
				"<input type=\"button\" class=\"btnQuizRestart\" value=\""+btn_Restart+"\" id=\"btnRestart\" onclick=\"restartQuiz('results_container_"+uniqueId+"')\">\n"+
				"</div>\n";
		return resultsContainer;
	}

	public String getTypeTwoTemplate(int uniqueId,boolean withContainer,String language){
		int typeNo = 2;
		String bookDir=Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID();


		//	[MyWebView copyBundleImageToImagePath:@"DefaultBg" toPath:[NSString stringWithFormat:@"q%d_img_%d", sectionId, uniqueId] fileExtension:@".png"];
		int randomNoToRefresh = new Random().nextInt();

		db.executeQuery("insert into tblMultiQuiz(quizId) values('"+uniqueId+"')");
		int sectionId = db.getMaxUniqueRowID("tblMultiQuiz");

		String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/"+"images/";

		File fileImgDir = new File(imgDir);
		if (!fileImgDir.exists()) {
			fileImgDir.mkdir();
		}
		String imgPath = imgDir+"q"+sectionId+"_img_"+uniqueId+""+".png";
		UserFunctions.copyAsset(context.getAssets(), "defaultbg.png", imgPath);
		Language=language;
		String quizTitleElement = "<div class=\"\" id=\"quizTitle_"+uniqueId+"_"+sectionId+"\" style=\"width:100%; height:66px; overflow:scroll; padding:10px 10px;\n"+
				"box-sizing:border-box;border-bottom:1px solid lightgray; vertical-align:top; display:table;\">\n"+
				"<p id=\"pTitle_"+uniqueId+"\" contenteditable=\"true\" onclick=\"selectAllText()\" style=\"font-weight:bold; word-break: break-all;\">Title"+sectionId+" : Lorem Ipsum dolor amet, consectetur adipisicing elit</p></div>\n";

		String quizQuestionElement = "<div class=\"\" id=\"quiz_question_"+uniqueId+"_"+sectionId+"\" style=\"width:100%;height:88px;background-color:#FFFFFF;\n"+
				"overflow:scroll; padding: 10px 10px; box-sizing:border-box;border-bottom:1px solid lightgray; vertical-align:top; display:table;\">\n"+
				"<p id=\"pQuiz_"+uniqueId+"\" contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor incididunt ut labore et dolore magnaaliqua.</p></div>\n";

        String inputBtnPrev = getInputBtnPrevious(uniqueId ,sectionId, withContainer);

		String inputBtnDele = getInputBtnDelete(uniqueId ,sectionId,typeNo);

		String inputBtnChkAns = getInputBtnCheckAns(uniqueId ,sectionId);

		String inputBtnAdd = getInputBtnAdd(uniqueId ,sectionId);

		String inputBtnNext = getInputBtnNext(uniqueId ,sectionId, withContainer);

		String resultsContainer = "";
		if (withContainer) {
			resultsContainer = getResultsContainer(uniqueId,language);
		}
		
		String typeTwoTemplate;
		String direction="ltr";
		String textAlign="left";
		String optionsElement1= "A.", optionsElement2 = "B.", optionsElement3 = "C.", optionsElement4 ="D.", optionsElement5 ="E.", optionsElement6="F.";

		if(language.contains("ar")){
			direction="rtl";
			textAlign="right";
			optionsElement1= "أ) "; optionsElement2 = "ب)"; optionsElement3 = "ت)"; optionsElement4 ="جـ)"; optionsElement5 ="حـ)"; optionsElement6="د)";
        }
		String inputDivElement1 = "<div class=\"\" id=\"q"+sectionId+"_div_a_"+uniqueId+"\">"+
				"<input type=\"radio\" name=\"q"+sectionId+"_"+uniqueId+"\" value=\"a\" id=\"q"+sectionId+"a_"+uniqueId+"\" checked=\"true\" onclick=\"assignAnswer('q"+sectionId+"a_"+uniqueId+"', 'btnCheckAnswer"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_result_"+uniqueId+"')\">\n"+
				"<label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"><b> "+optionsElement1+"</b></label><label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"> Answer 1</label><br><br></div>\n";

		String inputDivElement2 = "<div class=\"\" id=\"q"+sectionId+"_div_b_"+uniqueId+"\">"+
				"<input type=\"radio\" name=\"q"+sectionId+"_"+uniqueId+"\" value=\"b\" id=\"q"+sectionId+"b_"+uniqueId+"\" onclick=\"assignAnswer('q"+sectionId+"b_"+uniqueId+"', 'btnCheckAnswer"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_result_"+uniqueId+"')\">\n"+
				"<label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"><b> "+optionsElement2+"</b></label><label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"> Answer 2</label><br><br></div>\n";


		String inputDivElement3 = "<div class=\"\" id=\"q"+sectionId+"_div_c_"+uniqueId+"\">"+
				"<input type=\"radio\" name=\"q"+sectionId+"_"+uniqueId+"\" value=\"c\" id=\"q"+sectionId+"c_"+uniqueId+"\" onclick=\"assignAnswer('q"+sectionId+"c_"+uniqueId+"', 'btnCheckAnswer"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_result_"+uniqueId+"')\">\n"+
				"<label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"><b> "+optionsElement3+"</b></label><label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"> Answer 3</label><br><br></div>\n";

		String inputDivElement4 = "<div class=\"\" id=\"q"+sectionId+"_div_d_"+uniqueId+"\">"+
				"<input type=\"radio\" name=\"q"+sectionId+"_"+uniqueId+"\" value=\"d\" id=\"q"+sectionId+"d_"+uniqueId+"\" onclick=\"assignAnswer('q"+sectionId+"d_"+uniqueId+"', 'btnCheckAnswer"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_result_"+uniqueId+"')\">\n"+
				"<label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"><b> "+optionsElement4+"</b></label><label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"> Answer 4</label><br><br></div>\n";


		String inputDivElement5 = "<div class=\"\" id=\"q"+sectionId+"_div_e_"+uniqueId+"\" style=\"display:none;\">"+
				"<input type=\"radio\" name=\"q"+sectionId+"_"+uniqueId+"\" value=\"e\" id=\"q"+sectionId+"e_"+uniqueId+"\" onclick=\"assignAnswer('q"+sectionId+"e_"+uniqueId+"', 'btnCheckAnswer"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_result_"+uniqueId+"')\">\n"+
				"<label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"><b> "+optionsElement5+"</b></label><label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"> Answer 5</label><br><br></div>\n";

		String inputDivElement6 = "<div class=\"\" id=\"q"+sectionId+"_div_f_"+uniqueId+"\" style=\"display:none;\">"+
				"<input type=\"radio\" name=\"q"+sectionId+"_"+uniqueId+"\" value=\"f\" id=\"q"+sectionId+"f_"+uniqueId+"\" onclick=\"assignAnswer('q"+sectionId+"f_"+uniqueId+"', 'btnCheckAnswer"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_"+uniqueId+"', 'q"+sectionId+"_result_"+uniqueId+"')\">\n"+
				"<label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"><b> "+optionsElement6+"</b></label><label contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"> Answer 6</label><br><br></div>\n";

		String imgElement = "<div class=\"\" style=\"width:50%;height:80%; float:right;\">\n"+
				"<img  id=\"q"+sectionId+"_img_"+uniqueId+"\" style='width:100%; height:50%' src=\"/"+bookDir+"/images/q"+sectionId+"_img_"+uniqueId+".png?"+randomNoToRefresh+"\" onclick=\"window.location='ios:q"+sectionId+"_img_"+uniqueId+"'\"></img>\n"+
				"</div>\n";

		if(withContainer){
			typeTwoTemplate = "<div class=\"\" id=\"content\" style=\"width:400px; height:610px; background-color: rgb(227, 227, 227);box-sizing:border-box;overflow:hidden;display:-webkit-box;font-size:16px;font-family:Verdana;\">" +
					"<div class=\"quiz_2\" id=\"q"+sectionId+"_"+uniqueId+"\" style=\"width:100%; height:100%;position:relative; box-sizing: border-box; padding: 10px;direction:"+direction+"\">\n"+quizTitleElement+""+quizQuestionElement+""+
					"<div class=\"\" style=\"width:100%;height:315px;background-color:#FFFFFF;overflow:scroll; padding:5px 5px;box-sizing:border-box; vertical-align:top; display:table;\">\n"+
					"<div class=\"\" id=\"q"+sectionId+"_result_"+uniqueId+"\" style=\"text-align:center;font-weight:bold; visibility:hidden;\"><p> Excellent </p></div>"+
					"<div class=\"\" id=\"quiz_options_"+uniqueId+"_"+sectionId+"\" style=\"width:50%; height:100%; float:left;text-align:"+textAlign+"\">\n"+
					""+inputDivElement1+""+inputDivElement2+""+inputDivElement3+""+inputDivElement4+""+inputDivElement5+""+inputDivElement6+"</div>\n"+imgElement+"</div>\n"+
					"<div class=\"\" id=\"quiz_checkanswer_"+uniqueId+"\" style=\"width:95%;height:55px;padding:5px 5px;\n"+
					"box-sizing:border-box;overflow:hidden;text-align:center;position:absolute;bottom:0;\">\n"+
					""+inputBtnPrev+""+inputBtnDele+""+inputBtnChkAns+""+inputBtnAdd+""+inputBtnNext+"</div></div>"+resultsContainer+"</div>\n";
		}else{

			typeTwoTemplate = "<div class=\"quiz_2\" id=\"q"+sectionId+"_"+uniqueId+"\" style=\"width:100%; height:100%;position:relative; box-sizing: border-box; padding: 10px;direction:"+direction+"\">\n"+quizTitleElement+""+quizQuestionElement+""+
					"<div class=\"\" style=\"width:100%;height:315px;background-color:#FFFFFF;overflow:scroll; padding:5px 5px;box-sizing:border-box; vertical-align:top; display:table;\">\n"+
					"<div class=\"\" id=\"q"+sectionId+"_result_"+uniqueId+"\" style=\"text-align:center;font-weight:bold; visibility:hidden;\"><p> Excellent </p></div>"+
					"<div class=\"\" id=\"quiz_options_"+uniqueId+"_"+sectionId+"\" style=\"width:50%; height:100%; float:left;textAlign:"+textAlign+"\">\n"+
					""+inputDivElement1+""+inputDivElement2+""+inputDivElement3+""+inputDivElement4+""+inputDivElement5+""+inputDivElement6+"</div>\n"+imgElement+"</div>\n"+
					"<div class=\"\" id=\"quiz_checkanswer_"+uniqueId+"\" style=\"width:95%;height:55px;padding:5px 5px;\n"+
					"box-sizing:border-box;overflow:hidden;text-align:center;position:absolute;bottom:0;\">\n"+
					""+inputBtnPrev+""+inputBtnDele+""+inputBtnChkAns+""+inputBtnAdd+""+inputBtnNext+"</div></div>"+resultsContainer+"";
		}
		
		return typeTwoTemplate;
	}
	public String getTypeThreeTemplate(int uniqueId,boolean withContainer,String language){
		int typeNo = 3;
		String bookDir=Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID();


		//	[MyWebView copyBundleImageToImagePath:@"DefaultBg" toPath:[NSString stringWithFormat:@"q%d_img_%d", sectionId, uniqueId] fileExtension:@".png"];
		int randomNoToRefresh = new Random().nextInt();

		db.executeQuery("insert into tblMultiQuiz(quizId) values('"+uniqueId+"')");
		int sectionId = db.getMaxUniqueRowID("tblMultiQuiz");

		String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/"+"images/";

		File fileImgDir = new File(imgDir);
		if (!fileImgDir.exists()) {
			fileImgDir.mkdir();
		}
		String imgPath = imgDir+"q"+sectionId+"_img_"+uniqueId+""+".png";
		UserFunctions.copyAsset(context.getAssets(), "defaultbg.png", imgPath);    

		String quizTitleElement ="<div class=\"\" id=\"quizTitle_"+uniqueId+"_"+sectionId+"\" style=\"width:100%; height:66px; overflow:scroll; padding:10px 10px;\n"+
				"box-sizing:border-box;border-bottom:1px solid lightgray; vertical-align:top; display:table;\">\n"+
				"<p id=\"pTitle_"+uniqueId+"\" contenteditable=\"true\" onclick=\"selectAllText()\" style=\"font-weight:bold; word-break: break-all;\">Title"+sectionId+" : Lorem Ipsum dolor amet, consectetur adipisicing elit</p></div>\n";

		String quizQuestionElement = "<div class=\"\" id=\"quiz_question_"+uniqueId+"_"+sectionId+"\" style=\"width:100%;height:88px;background-color:#FFFFFF;\n"+
				"overflow:scroll; padding: 10px 10px; box-sizing:border-box;border-bottom:1px solid lightgray; vertical-align:top; display:table;\">\n"+
				"<p id=\"pQuiz_"+uniqueId+"\" contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor incididunt ut labore et dolore magnaaliqua.</p></div>\n";


		String imgElement = "<div class=\"\" style=\"width:100%; height:120px; background-color:#FFFFFF; box-sizing:border-box; text-align:center;padding:10px 10px;\">\n"+
				"<img id=\"q"+sectionId+"_img_"+uniqueId+"\" style=\"width:35%;height:100%;\" src=\"/"+bookDir+"/images/q"+sectionId+"_img_"+uniqueId+".png?"+randomNoToRefresh+"\" onclick=\"window.location='ios:q"+sectionId+"_img_"+uniqueId+"'\"></img>\n"+
				"</div>\n";

		Language=language;
        String inputBtnPrev = getInputBtnPrevious(uniqueId ,sectionId,withContainer);

		String inputBtnDele = getInputBtnDelete(uniqueId, sectionId ,typeNo);

		String inputBtnChkAns = getInputBtnCheckAns(uniqueId ,sectionId);

		String inputBtnAdd = getInputBtnAdd(uniqueId ,sectionId);

		String inputBtnNext = getInputBtnNext(uniqueId ,sectionId,withContainer);

		String resultsContainer = "";
		if (withContainer) {
			resultsContainer = getResultsContainer(uniqueId,language);
		}
		
		String typeThreeTemplate;
		String direction="ltr";
		String optionsElement1= "A.", optionsElement2 = "B.", optionsElement3 = "C.", optionsElement4 ="D.", optionsElement5 ="E.", optionsElement6="F.";

		if(language.contains("ar")){
			direction="rtl";
			optionsElement1= "أ)"; optionsElement2 = "ب)"; optionsElement3 = "ت)"; optionsElement4 ="جـ)"; optionsElement5 ="حـ)"; optionsElement6="د)";

		}
		String inputDivElement1 = getRadioInputDivElement(uniqueId, sectionId, "block", optionsElement1, "Answer 1", "checked=\"true\"", "a", withContainer);
		String inputDivElement2 = getRadioInputDivElement(uniqueId, sectionId, "block", optionsElement2, "Answer 2", "", "b", withContainer);
		String inputDivElement3 = getRadioInputDivElement(uniqueId ,sectionId ,"block", optionsElement3 ,"Answer 3" ,"", "c", withContainer);
		String inputDivElement4 = getRadioInputDivElement(uniqueId ,sectionId ,"block", optionsElement4 ,"Answer 4" ,"", "d", withContainer);
		String inputDivElement5 = getRadioInputDivElement(uniqueId ,sectionId ,"none",  optionsElement5 ,"Answer 5" ,"", "e", withContainer);
		String inputDivElement6 = getRadioInputDivElement(uniqueId, sectionId, "none",  optionsElement6, "Answer 6" ,"", "f", withContainer);

		if(withContainer){
           typeThreeTemplate = "<div class=\"\" id=\"content\" style=\"width:400px; height:610px; background-color: rgb(227, 227, 227);box-sizing:border-box;overflow:hidden;display:-webkit-box;font-size:16px;font-family:Verdana;\">" +
					"<div class=\"quiz_3\" id=\"q"+sectionId+"_"+uniqueId+"\" style=\"width:100%; height:100%; position:relative; box-sizing: border-box; padding: 10px;direction:"+direction+"\">\n"+quizTitleElement+""+quizQuestionElement+""+
		                                   ""+imgElement+"<div class=\"\" id=\"quiz_options_"+uniqueId+"_"+sectionId+"\" style=\"width:100%;height:167px;background-color:#FFFFFF;overflow:scroll; box-sizing:border-box; text-align:center; vertical-align:top; display:table;\">\n"+
		                                   "<div class=\"\" id=\"q"+sectionId+"_result_"+uniqueId+"\" style=\"text-align:center;font-weight:bold; visibility:hidden;\"><p> Excellent </p></div>"+
		                                   ""+inputDivElement1+""+inputDivElement2+""+inputDivElement3+""+inputDivElement4+""+inputDivElement5+""+inputDivElement6+"</div>\n"+
		                                   "<div  class=\"\" id=\"quiz_checkanswer_"+uniqueId+"\" style=\"width:95%;height:55px;padding:5px 5px;box-sizing:border-box;overflow:hidden;text-align:center;position:absolute;bottom:0;\">\n"+
		                                   ""+inputBtnPrev+""+inputBtnDele+""+inputBtnChkAns+""+inputBtnAdd+""+inputBtnNext+"</div></div>"+resultsContainer+"</div>\n";

		}else{

			typeThreeTemplate = "<div  class=\"quiz_3\" id=\"q"+sectionId+"_"+uniqueId+"\" style=\"width:100%; height:100%; position:relative; box-sizing: border-box; padding: 10px;direction:"+direction+"\">\n"+quizTitleElement+""+quizQuestionElement+""+
		                                   ""+imgElement+"<div class=\"\" id=\"quiz_options_"+uniqueId+"_"+sectionId+"\" style=\"width:100%;height:167px;background-color:#FFFFFF;overflow:scroll; box-sizing:border-box; text-align:center; vertical-align:top; display:table;\">\n"+
		                                   "<div class=\"\" id=\"q"+sectionId+"_result_"+uniqueId+"\" style=\"text-align:center;font-weight:bold; visibility:hidden;\"><p> Excellent </p></div>"+
		                                   ""+inputDivElement1+""+inputDivElement2+""+inputDivElement3+""+inputDivElement4+""+inputDivElement5+""+inputDivElement6+"</div>\n"+
		                                   "<div class=\"\" id=\"quiz_checkanswer_"+uniqueId+"\" style=\"width:95%;height:55px;padding:5px 5px;box-sizing:border-box;overflow:hidden;text-align:center;position:absolute;bottom:0;\">\n"+
		                                   ""+inputBtnPrev+""+inputBtnDele+""+inputBtnChkAns+""+inputBtnAdd+""+inputBtnNext+"</div></div>"+resultsContainer+"";
		}
		
	    return typeThreeTemplate;
	}

	public String getTypeFourTemplate(int uniqueId,boolean withContainer,String language){
		int typeNo = 4;
		String bookDir=Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID();


		//	[MyWebView copyBundleImageToImagePath:@"DefaultBg" toPath:[NSString stringWithFormat:@"q%d_img_%d", sectionId, uniqueId] fileExtension:@".png"];
		int randomNoToRefresh = new Random().nextInt();

		db.executeQuery("insert into tblMultiQuiz(quizId) values('"+uniqueId+"')");
		int sectionId = db.getMaxUniqueRowID("tblMultiQuiz");

		String imgDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/"+"images/";

		File fileImgDir = new File(imgDir);
		if (!fileImgDir.exists()) {
			fileImgDir.mkdir();
		}
		String imgPath = imgDir+"q"+sectionId+"_img_a_"+uniqueId+""+".png";
		UserFunctions.copyAsset(context.getAssets(), "defaultbg.png", imgPath); 

		String imgPatha = imgDir+"q"+sectionId+"_img_b_"+uniqueId+""+".png";
		UserFunctions.copyAsset(context.getAssets(), "defaultbg.png", imgPatha);

		String imgPathb = imgDir+"q"+sectionId+"_img_c_"+uniqueId+""+".png";
		UserFunctions.copyAsset(context.getAssets(), "defaultbg.png", imgPathb); 

		String imgPathc = imgDir+"q"+sectionId+"_img_d_"+uniqueId+""+".png";
		UserFunctions.copyAsset(context.getAssets(), "defaultbg.png", imgPathc); 

		String imgPathd = imgDir+"q"+sectionId+"_img_e_"+uniqueId+""+".png";
		UserFunctions.copyAsset(context.getAssets(), "defaultbg.png", imgPathd); 

		String imgPathe = imgDir+"q"+sectionId+"_img_f_"+uniqueId+""+".png";
		UserFunctions.copyAsset(context.getAssets(), "defaultbg.png", imgPathe); 

		String quizTitleElement = "<div class=\"\" id=\"quizTitle_"+uniqueId+"_"+sectionId+"\" style=\"width:100%; height:66px; overflow:scroll; padding:10px 10px;\n"+
                                  "box-sizing:border-box;border-bottom:1px solid lightgray; vertical-align:top; display:table;\">\n"+
                                  "<p id=\"pTitle_"+uniqueId+"\" contenteditable=\"true\" onclick=\"selectAllText()\" style=\"font-weight:bold; word-break: break-all;\">Title"+sectionId+" : Lorem Ipsum dolor amet, consectetur adipisicing elit</p></div>\n";
		
		String quizQuestionElement = "<div class=\"\" id=\"quiz_question_"+uniqueId+"_"+sectionId+"\" style=\"width:100%;height:88px;background-color:#FFFFFF;\n"+
                                     "overflow:scroll; padding: 10px 10px; box-sizing:border-box;border-bottom:1px solid lightgray; vertical-align:top; display:table;\">\n"+
                                     "<p id=\"pQuiz_"+uniqueId+"\" contenteditable=\"true\" onclick=\"selectAllText()\" style=\"word-break: break-all;\"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor incididunt ut labore et dolore magnaaliqua.</p></div>\n";
		String inputDivElement1;
		if (language.contains("ar")) {
			inputDivElement1 = "<div class=\"\" id=\"q" + sectionId + "_div_a_" + uniqueId + "\" style=\"width:49%; height:130px; position:relative; float:right; margin-left:2%;\">\n" +
					"<img id=\"q" + sectionId + "_img_a_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_a_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_a_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; right:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" checked=\"true\" value=\"a\" id=\"q" + sectionId + "a_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "a_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		} else {
			inputDivElement1 = "<div class=\"\" id=\"q" + sectionId + "_div_a_" + uniqueId + "\" style=\"width:49%; height:130px; position:relative; float:left; margin-right:2%;\">\n" +
					"<img id=\"q" + sectionId + "_img_a_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_a_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_a_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; left:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" checked=\"true\" value=\"a\" id=\"q" + sectionId + "a_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "a_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		}
		String inputDivElement2;
		if (language.contains("ar")) {
			inputDivElement2 = "<div class=\"\" id=\"q" + sectionId + "_div_b_" + uniqueId + "\" style=\"width:49%; height:130px; position:relative; float:right;\">\n" +
					"<img id=\"q" + sectionId + "_img_b_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_b_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_b_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; right:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" value=\"b\" id=\"q" + sectionId + "b_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "b_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		} else {
			inputDivElement2 = "<div class=\"\" id=\"q" + sectionId + "_div_b_" + uniqueId + "\" style=\"width:49%; height:130px; position:relative; float:left;\">\n" +
					"<img id=\"q" + sectionId + "_img_b_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_b_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_b_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; left:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" value=\"b\" id=\"q" + sectionId + "b_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "b_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		}
		String inputDivElement3;
		if (language.contains("ar")) {
			inputDivElement3 = "<div class=\"\" id=\"q" + sectionId + "_div_c_" + uniqueId + "\" style=\"width:49%; height:130px; position:relative; float:right; margin-left:2%;\">\n" +
					"<img id=\"q" + sectionId + "_img_c_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_c_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_c_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; right:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" value=\"c\" id=\"q" + sectionId + "c_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "c_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		} else {
			inputDivElement3 = "<div class=\"\" id=\"q" + sectionId + "_div_c_" + uniqueId + "\" style=\"width:49%; height:130px; position:relative; float:left; margin-right:2%;\">\n" +
					"<img id=\"q" + sectionId + "_img_c_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_c_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_c_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; left:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" value=\"c\" id=\"q" + sectionId + "c_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "c_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		}
		String inputDivElement4;
		if (language.contains("ar")) {
			inputDivElement4 = "<div class=\"\" id=\"q" + sectionId + "_div_d_" + uniqueId + "\" style=\"width:49%; height:130px; position:relative; float:right;\">\n" +
					"<img id=\"q" + sectionId + "_img_d_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_d_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_d_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; right:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" value=\"d\" id=\"q" + sectionId + "d_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "d_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		} else {
			inputDivElement4 = "<div class=\"\" id=\"q" + sectionId + "_div_d_" + uniqueId + "\" style=\"width:49%; height:130px; position:relative; float:left;\">\n" +
					"<img id=\"q" + sectionId + "_img_d_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_d_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_d_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; left:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" value=\"d\" id=\"q" + sectionId + "d_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "d_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		}
		String inputDivElement5;
		if (language.contains("ar")) {
			inputDivElement5 = "<div class=\"\" id=\"q" + sectionId + "_div_e_" + uniqueId + "\" style=\"width:49%; height:138px; position:relative; float:right; display:none; margin-left:2%;\">\n" +
					"<img id=\"q" + sectionId + "_img_e_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_e_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_e_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; right:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" value=\"e\" id=\"q" + sectionId + "e_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "e_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		} else {
			inputDivElement5 = "<div class=\"\" id=\"q" + sectionId + "_div_e_" + uniqueId + "\" style=\"width:49%; height:138px; position:relative; float:left; margin-right:2%; display:none;\">\n" +
					"<img id=\"q" + sectionId + "_img_e_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_e_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_e_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; left:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" value=\"e\" id=\"q" + sectionId + "e_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "e_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		}
		String inputDivElement6;
		if (language.contains("ar")) {
			inputDivElement6 = "<div class=\"\" id=\"q" + sectionId + "_div_f_" + uniqueId + "\" style=\"width:49%; height:138px; position:relative; float:right; display:none;\">\n" +
					"<img id=\"q" + sectionId + "_img_f_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_f_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_f_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; right:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" value=\"f\" id=\"q" + sectionId + "f_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "f_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		} else {
			inputDivElement6 = "<div class=\"\" id=\"q" + sectionId + "_div_f_" + uniqueId + "\" style=\"width:49%; height:138px; position:relative; float:left; display:none;\">\n" +
					"<img id=\"q" + sectionId + "_img_f_" + uniqueId + "\" style=\"width:100%;height:95%; position:absolute;\" src=\"/" + bookDir + "/images/q" + sectionId + "_img_f_" + uniqueId + ".png?" + randomNoToRefresh + "\" onclick=\"window.location='ios:q" + sectionId + "_img_f_" + uniqueId + "'\"/>\n" +
					"<input style=\"position:absolute; bottom:5%; left:1%;\" type=\"radio\" name=\"q" + sectionId + "_" + uniqueId + "\" value=\"f\" id=\"q" + sectionId + "f_" + uniqueId + "\"" +
					"onclick=\"assignAnswer('q" + sectionId + "f_" + uniqueId + "', 'btnCheckAnswer" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_" + uniqueId + "', 'q" + sectionId + "_result_" + uniqueId + "')\"><br><br></div>\n";
		}
		Language=language;
		String inputBtnPrev = getInputBtnPrevious(uniqueId ,sectionId, withContainer);

		String inputBtnDele = getInputBtnDelete(uniqueId,sectionId ,typeNo);

		String inputBtnChkAns = getInputBtnCheckAns(uniqueId, sectionId);

		String inputBtnAdd = getInputBtnAdd(uniqueId, sectionId);

		String inputBtnNext = getInputBtnNext(uniqueId, sectionId, withContainer);

		String resultsContainer = "";
		if (withContainer) {
			resultsContainer = getResultsContainer(uniqueId,language);
		}
		String typeFourTemplate;
		String direction="ltr";
		String textAlign="left";

		if(language.contains("ar")){
			direction="rtl";
			textAlign="right";
		}
		if(withContainer){
			typeFourTemplate = "<div class=\"\" id=\"content\" style=\"width:400px; height:610px; background-color: rgb(227, 227, 227);box-sizing:border-box;overflow:hidden;display:-webkit-box;font-size:16px;font-family:Verdana;\">"+
					"<div class=\"quiz_4\" id=\"q"+sectionId+"_"+uniqueId+"\" style=\"width:100%; height:100%; position:relative; box-sizing: border-box; padding: 10px;direction:"+direction+"\">\n"+quizTitleElement+""+quizQuestionElement+""+
                    "<div class=\"\" id=\"quiz_options_"+uniqueId+"_"+sectionId+"\" style=\"overflow:hidden; padding:0px 10px;box-sizing:border-box;background-color:#FFFFFF;text-align:"+textAlign+"\">\n"+
                    "<div class=\"\" id=\"q"+sectionId+"_result_"+uniqueId+"\" style=\"text-align:center;font-weight:bold; visibility:hidden;\"><p> Excellent </p></div>"+
                    ""+inputDivElement1+""+inputDivElement2+""+inputDivElement3+""+inputDivElement4+""+inputDivElement5+""+inputDivElement6+"</div>\n"+
                    "<div class=\"\" id=\"quiz_checkanswer_"+uniqueId+"\" style=\"width:95%;height:55px;padding:5px 5px;box-sizing:border-box;overflow:hidden;text-align:center;position:absolute;bottom:0;\">\n"+
                    ""+inputBtnPrev+""+inputBtnDele+""+inputBtnChkAns+""+inputBtnAdd+""+inputBtnNext+"</div></div>"+resultsContainer+"</div>\n";

		}else{
             typeFourTemplate = "<div class=\"quiz_4\" id=\"q"+sectionId+"_"+uniqueId+"\" style=\"width:100%; height:100%; position:relative; box-sizing: border-box; padding: 10px;direction:"+direction+"\">\n"+quizTitleElement+""+quizQuestionElement+""+
                                  "<div class=\"\" id=\"quiz_options_"+uniqueId+"_"+sectionId+"\" style=\"overflow:hidden; padding:0px 10px;box-sizing:border-box;background-color:#FFFFFF;text-align:"+textAlign+"\">\n"+
                                  "<div class=\"\" id=\"q"+sectionId+"_result_"+uniqueId+"\" style=\"text-align:center;font-weight:bold; visibility:hidden;\"><p> Excellent </p></div>"+
                                  ""+inputDivElement1+""+inputDivElement2+""+inputDivElement3+""+inputDivElement4+""+inputDivElement5+""+inputDivElement6+"</div>\n"+
                                  "<div class=\"\" id=\"quiz_checkanswer_"+uniqueId+"\" style=\"width:95%;height:55px;padding:5px 5px;box-sizing:border-box;overflow:hidden;text-align:center;position:absolute;bottom:0;\">\n"+
                                  ""+inputBtnPrev+""+inputBtnDele+""+inputBtnChkAns+""+inputBtnAdd+""+inputBtnNext+"</div></div>"+resultsContainer+"\n";
		}

		return typeFourTemplate;
	}
	
	public void webQuizTypeOneSelected(String quesId,int quesUniqueId,int quesSectionNo,WebView webview,String language){
		String typeOneTemp = getTypeOneTemplate(quesUniqueId, false,language);
		typeOneTemp = typeOneTemp.replace("\n", "\\n").replace("'", "\\'");
		String javascriptString ="javascript:embedNewQuiz('"+typeOneTemp+"', '"+quesSectionNo+"', '"+quesUniqueId+"');";
		CustomWebRelativeLayout.loadJSScript(javascriptString, webview);

		QuizSequenceObject quizSeqObject = new QuizSequenceObject();
		int sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
		quizSeqObject.setSectionId(sectionId);
	    quizSeqObject.setUniqueid(quesUniqueId);
	    ((CustomWebRelativeLayout)context.page.currentView).quizSequenceId += 1;
	    ((CustomWebRelativeLayout)context.page.currentView).quizSequenceObjectArray.add(((CustomWebRelativeLayout)context.page.currentView).quizSequenceId, quizSeqObject);
	}
	
	public void webQuizTypeTwoSelected(String quesId,int quesUniqueId,int quesSectionNo,WebView webview,String language){
		String typeTwoTemp = getTypeTwoTemplate(quesUniqueId, false,language);
		typeTwoTemp = typeTwoTemp.replace("\n", "\\n").replace("'", "\\'");
		String javascriptString ="javascript:embedNewQuiz('"+typeTwoTemp+"', '"+quesSectionNo+"', '"+quesUniqueId+"');";
		CustomWebRelativeLayout.loadJSScript(javascriptString, webview);
		
		QuizSequenceObject quizSeqObject = new QuizSequenceObject();
		int sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
		quizSeqObject.setSectionId(sectionId);
	    quizSeqObject.setUniqueid(quesUniqueId);
	    ((CustomWebRelativeLayout)context.page.currentView).quizSequenceId += 1;
	    ((CustomWebRelativeLayout)context.page.currentView).quizSequenceObjectArray.add(((CustomWebRelativeLayout)context.page.currentView).quizSequenceId, quizSeqObject);

	}
	public void webQuizTypeThreeSelected(String quesId,int quesUniqueId,int quesSectionNo,WebView webview,String language){
		String typeThreeTemp = getTypeThreeTemplate(quesUniqueId, false,language);
		typeThreeTemp = typeThreeTemp.replace("\n", "\\n").replace("'", "\\'");
		String javascriptString ="javascript:embedNewQuiz('"+typeThreeTemp+"', '"+quesSectionNo+"', '"+quesUniqueId+"');";
		CustomWebRelativeLayout.loadJSScript(javascriptString, webview);
		
		QuizSequenceObject quizSeqObject = new QuizSequenceObject();
		int sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
		quizSeqObject.setSectionId(sectionId);
	    quizSeqObject.setUniqueid(quesUniqueId);
	    ((CustomWebRelativeLayout)context.page.currentView).quizSequenceId += 1;
	    ((CustomWebRelativeLayout)context.page.currentView).quizSequenceObjectArray.add(((CustomWebRelativeLayout)context.page.currentView).quizSequenceId, quizSeqObject);

	}
	public void webQuizTypeFourSelected(String quesId,int quesUniqueId,int quesSectionNo,WebView webview,String language){
		String typeFourTemp = getTypeFourTemplate(quesUniqueId, false,language);
		typeFourTemp = typeFourTemp.replace("\n", "\\n").replace("'", "\\'");
		String javascriptString ="javascript:embedNewQuiz('"+typeFourTemp+"', '"+quesSectionNo+"', '"+quesUniqueId+"');";
		CustomWebRelativeLayout.loadJSScript(javascriptString, webview);
		
		QuizSequenceObject quizSeqObject = new QuizSequenceObject();
		int sectionId = db.getMaxUniqueRowID("tblMultiQuiz");
		quizSeqObject.setSectionId(sectionId);
	    quizSeqObject.setUniqueid(quesUniqueId);
	    ((CustomWebRelativeLayout)context.page.currentView).quizSequenceId += 1;
	    ((CustomWebRelativeLayout)context.page.currentView).quizSequenceObjectArray.add(((CustomWebRelativeLayout) context.page.currentView).quizSequenceId, quizSeqObject);

	}

}