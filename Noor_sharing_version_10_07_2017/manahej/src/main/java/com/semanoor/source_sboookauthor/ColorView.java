package com.semanoor.source_sboookauthor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.view.View;

public class ColorView extends View{
	
	Paint Colorpaint;
	Shader shader;
	final float[] color = { 1.f, 1.f, 1.f };

	public ColorView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ColorView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@SuppressLint("DrawAllocation")
	@Override
	
	/**
	 *  Method for onDraw canvas
	 */
	
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (Colorpaint == null) {
			Colorpaint = new Paint();
			shader = new LinearGradient(0.f, 0.f, 0.f, this.getMeasuredHeight(), 0xffffffff, 0xff000000, TileMode.CLAMP);
		}
		int rgb = Color.HSVToColor(color);
		Shader newshader = new LinearGradient(0.f, 0.f, this.getMeasuredWidth(), 0.f, 0xffffffff, rgb, TileMode.CLAMP);
		ComposeShader composeShader = new ComposeShader(shader, newshader, PorterDuff.Mode.MULTIPLY);
		Colorpaint.setShader(composeShader);
		canvas.drawRect(0.f, 0.f, this.getMeasuredWidth(), this.getMeasuredHeight(), Colorpaint);
	}
	
	
	/**
	 *  Method for setHue
	 */
	void setHue(float hue) {
		color[0] = hue;
		invalidate();
	}
}
