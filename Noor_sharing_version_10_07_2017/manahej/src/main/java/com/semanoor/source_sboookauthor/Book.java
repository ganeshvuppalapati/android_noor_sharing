package com.semanoor.source_sboookauthor;

import java.io.File;
import java.io.Serializable;

public class Book implements Serializable {

	private int idPos;
	private int _BID;
	private String _bTitle;
	private String _bDescription;
	private String _bAuthor;
	private int _bTemplateId;
	private int _bTotalPages;
	private int _bPagebgvalue;
	private int _bOrientation;
	private int _bIndex;
	private boolean _bStoreBook;
	private String bookCardImagePath;
	private int _lastViewedPage;
	private String _bStoreID;
	private String _bOffsetWidthAndHeight;
	private boolean _bShowPageNo;
	private int pageNoXPos, pageNoYPos;
	private String _bPageNoContent;
	private Templates template;
	private boolean isStoreBookCreatedFromTab;
	private int bCategoryId;
	private String bookLangugae;
	private String isDownloadCompleted;
	private String downloadURL;
	private String isEditable;
	private String isMalzamah;
	private String price;
	private String imageURL;
	private String purchaseType;
	public boolean isCssType;
	private int _groupId;
	private String clientID;
	private String bookDirection;
	public Book(int BID, String bTitle, String bDescription, String bAuthor, int bTemplateId, int bTotalPages, int bPagebgvalue, int bOrientation, int bIndex, boolean bStoreBook,int LastViewedPage, String bStoreID, String offsetWidthAndHeight, boolean bShowPageNo, String bPageNoPos, String bPageNoContent, Templates template, boolean isStoreBookCreatedFromTab, int bCategoryId, String bookLanguage, String isDownloadCompleted, String downloadURL, String isEditable, String isMalzamah, String price, String imageURL, String purchaseType,int groupId,String clientid,String bookDirec){
		this._BID = BID;
		this._bTitle = bTitle;
		this.set_bDescription(bDescription);
		this._bAuthor = bAuthor;
		this.set_bTemplateId(bTemplateId);
		this._bTotalPages = bTotalPages;
		this.set_bPagebgvalue(bPagebgvalue);
		this._bOrientation = bOrientation;
		this._bIndex = bIndex;
		this.set_bStoreBook(bStoreBook);
		this.set_lastViewedPage(LastViewedPage);
		this.set_bStoreID(bStoreID);
		this.set_bOffsetWidthAndHeight(offsetWidthAndHeight);
		this.set_bShowPageNo(bShowPageNo);
		this.set_bPageNoContent(bPageNoContent);
		if (bPageNoPos != null) {
			this.setPageNoXPos(Integer.parseInt(bPageNoPos.split("\\|")[0]));
			this.setPageNoYPos(Integer.parseInt(bPageNoPos.split("\\|")[1]));
		}
		this.setTemplate(template);
		this.setStoreBookCreatedFromTab(isStoreBookCreatedFromTab);
		this.setbCategoryId(bCategoryId);
		this.setBookLangugae(bookLanguage);
		this.isDownloadCompleted = isDownloadCompleted;
		this.downloadURL = downloadURL;
		this.isEditable = isEditable;
		this.isMalzamah = isMalzamah;
		this.price = price;
		this.imageURL = imageURL;
		this.setPurchaseType(purchaseType);
		this.set_groupId(groupId);
		this.setClientID(clientid);
		this.bookDirection = bookDirec;
		String cssFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bStoreID+"Book/FreeFiles/css";
		if (new File(cssFilePath).exists()) {
			isCssType = true;
		}
	}
	public Book(int BID, String bTitle, String bDescription, String bAuthor, int bTemplateId, int bTotalPages, int bPagebgvalue, int bOrientation, int bIndex, boolean bStoreBook,int LastViewedPage, String bStoreID, String offsetWidthAndHeight, boolean bShowPageNo, String bPageNoPos, String bPageNoContent, boolean isStoreBookCreatedFromTab, int bCategoryId, String bookLanguage, String isDownloadCompleted, String downloadURL, String isEditable, String isMalzamah, String price, String imageURL, String purchaseType,int groupId,String clientid){
		this._BID = BID;
		this._bTitle = bTitle;
		this.set_bDescription(bDescription);
		this._bAuthor = bAuthor;
		this.set_bTemplateId(bTemplateId);
		this._bTotalPages = bTotalPages;
		this.set_bPagebgvalue(bPagebgvalue);
		this._bOrientation = bOrientation;
		this._bIndex = bIndex;
		this.set_bStoreBook(bStoreBook);
		this.set_lastViewedPage(LastViewedPage);
		this.set_bStoreID(bStoreID);
		this.set_bOffsetWidthAndHeight(offsetWidthAndHeight);
		this.set_bShowPageNo(bShowPageNo);
		this.set_bPageNoContent(bPageNoContent);
		if (bPageNoPos != null) {
			this.setPageNoXPos(Integer.parseInt(bPageNoPos.split("\\|")[0]));
			this.setPageNoYPos(Integer.parseInt(bPageNoPos.split("\\|")[1]));
		}

		this.setStoreBookCreatedFromTab(isStoreBookCreatedFromTab);
		this.setbCategoryId(bCategoryId);
		this.setBookLangugae(bookLanguage);
		this.isDownloadCompleted = isDownloadCompleted;
		this.downloadURL = downloadURL;
		this.isEditable = isEditable;
		this.isMalzamah = isMalzamah;
		this.price = price;
		this.imageURL = imageURL;
		this.setPurchaseType(purchaseType);
		this.set_groupId(groupId);
		this.setClientID(clientid);
		String cssFilePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bStoreID+"Book/FreeFiles/css";
		if (new File(cssFilePath).exists()) {
			isCssType = true;
		}
	}
	public int getBookID(){
		return this._BID;
	}

	public String getBookTitle(){
		return this._bTitle;
	}

	public void setBookTitle(String bTitle){
		this._bTitle = bTitle;
	}

	public String getBookAuthorName(){
		return this._bAuthor;
	}

	public void setBookAuthorName(String bAuthor){
		this._bAuthor = bAuthor;
	}

	public int getBookIndex(){
		return this._bIndex;
	}

	public int getTotalPages(){
		return this._bTotalPages;
	}

	public void setTotalPages(int totalPages){
		this._bTotalPages = totalPages;
	}

	public int getBookOrientation(){
		return this._bOrientation;
	}

	public void setBookOrientation(int bookOrientation){
		this._bOrientation = bookOrientation;
	}
	public int get_lastViewedPage() {
		return _lastViewedPage;
	}

	public void set_lastViewedPage(int _lastViewedPage) {
		this._lastViewedPage = _lastViewedPage;
	}

	/**
	 * @return the _bStoreBook
	 */
	public boolean is_bStoreBook() {
		return _bStoreBook;
	}

	/**
	 * @param _bStoreBook the _bStoreBook to set
	 */
	public void set_bStoreBook(boolean _bStoreBook) {
		this._bStoreBook = _bStoreBook;
	}

	/**
	 * @return the _bOffsetWidthAndHeight
	 */
	public String get_bOffsetWidthAndHeight() {
		return _bOffsetWidthAndHeight;
	}

	/**
	 * @param _bOffsetWidthAndHeight the _bOffsetWidthAndHeight to set
	 */
	public void set_bOffsetWidthAndHeight(String _bOffsetWidthAndHeight) {
		this._bOffsetWidthAndHeight = _bOffsetWidthAndHeight;
	}

	/**
	 * @return the _bStoreID
	 */
	public String get_bStoreID() {
		return _bStoreID;
	}

	/**
	 * @param _bStoreID the _bStoreID to set
	 */
	public void set_bStoreID(String _bStoreID) {
		this._bStoreID = _bStoreID;
	}

	/**
	 * @return the _bShowPageNo
	 */
	public boolean is_bShowPageNo() {
		return _bShowPageNo;
	}

	/**
	 * @param _bShowPageNo the _bShowPageNo to set
	 */
	public void set_bShowPageNo(boolean _bShowPageNo) {
		this._bShowPageNo = _bShowPageNo;
	}

	/**
	 * @return the pageNoXPos
	 */
	public int getPageNoXPos() {
		return pageNoXPos;
	}

	/**
	 * @param pageNoXPos the pageNoXPos to set
	 */
	public void setPageNoXPos(int pageNoXPos) {
		this.pageNoXPos = pageNoXPos;
	}

	/**
	 * @return the pageNoYPos
	 */
	public int getPageNoYPos() {
		return pageNoYPos;
	}

	/**
	 * @param pageNoYPos the pageNoYPos to set
	 */
	public void setPageNoYPos(int pageNoYPos) {
		this.pageNoYPos = pageNoYPos;
	}

	/**
	 * @return the _bPageNoContent
	 */
	public String get_bPageNoContent() {
		return _bPageNoContent;
	}

	/**
	 * @param _bPageNoContent the _bPageNoContent to set
	 */
	public void set_bPageNoContent(String _bPageNoContent) {
		this._bPageNoContent = _bPageNoContent;
	}

	/**
	 * @return the _bTemplates
	 */
	public int get_bTemplateId() {
		return _bTemplateId;
	}

	/**
	 * @param _bTemplateId the _bTemplates to set
	 */
	public void set_bTemplateId(int _bTemplateId) {
		this._bTemplateId = _bTemplateId;
	}

	/**
	 * @return the template
	 */
	public Templates getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(Templates template) {
		this.template = template;
	}

	/**
	 * @return the isStoreBookCreatedFromTab
	 */
	public boolean isStoreBookCreatedFromTab() {
		return isStoreBookCreatedFromTab;
	}

	/**
	 * @param isStoreBookCreatedFromTab the isStoreBookCreatedFromTab to set
	 */
	public void setStoreBookCreatedFromTab(boolean isStoreBookCreatedFromTab) {
		this.isStoreBookCreatedFromTab = isStoreBookCreatedFromTab;
	}

	/**
	 * @return the bCategoryId
	 */
	public int getbCategoryId() {
		return bCategoryId;
	}

	/**
	 * @param bCategoryId the bCategoryId to set
	 */
	public void setbCategoryId(int bCategoryId) {
		this.bCategoryId = bCategoryId;
	}

	/**
	 * @return the idPos
	 */
	public int getIdPos() {
		return idPos;
	}

	/**
	 * @param idPos the idPos to set
	 */
	public void setIdPos(int idPos) {
		this.idPos = idPos;
	}

	public String getBookLangugae() {
		return bookLangugae;
	}

	public void setBookLangugae(String bookLangugae) {
		this.bookLangugae = bookLangugae;
	}

	public String get_bDescription() {
		return _bDescription;
	}

	public void set_bDescription(String _bDescription) {
		this._bDescription = _bDescription;
	}

	public String getIsDownloadCompleted() {
		return isDownloadCompleted;
	}

	public void setIsDownloadCompleted(String isDownloadCompleted) {
		this.isDownloadCompleted = isDownloadCompleted;
	}

	public String getDownloadURL() {
		return downloadURL;
	}

	public void setDownloadURL(String downloadURL) {
		this.downloadURL = downloadURL;
	}

	public String getIsEditable() {
		return isEditable;
	}

	public void setIsEditable(String isEditable) {
		this.isEditable = isEditable;
	}

	public String getIsMalzamah() {
		return isMalzamah;
	}

	public void setIsMalzamah(String isMalzamah) {
		this.isMalzamah = isMalzamah;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public int get_bPagebgvalue() {
		return _bPagebgvalue;
	}

	public void set_bPagebgvalue(int _bPagebgvalue) {
		this._bPagebgvalue = _bPagebgvalue;
	}

	public int get_groupId() {
		return _groupId;
	}

	public void set_groupId(int _groupId) {
		this._groupId = _groupId;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getBookDirection() {
		return bookDirection;
	}

	public void setBookDirection(String bookDirection) {
		this.bookDirection = bookDirection;
	}
}
