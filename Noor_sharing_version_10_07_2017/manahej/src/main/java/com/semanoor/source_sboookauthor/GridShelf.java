package com.semanoor.source_sboookauthor;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dragsortadapter.DragSortAdapter;
import com.dragsortadapter.NoForegroundShadowBuilder;
import com.ptg.views.CircleButton;
import com.semanoor.manahij.ExportEnrActivity;
import com.semanoor.manahij.ExportEnrGroupActivity;
import com.semanoor.manahij.GenerateThumbnailsPdf;
import com.semanoor.manahij.GroupBooks;
import com.semanoor.manahij.MainActivity;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.PopoverView.PopoverViewDelegate;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.graphics.Bitmap.createScaledBitmap;

public class GridShelf extends Shelf implements ListAdapter,OnClickListener, PopoverViewDelegate, Serializable {

    private boolean isInEditMode;
    public Book currentBook;
    private  EditText et_cat;
    public Category categoryRack;
    public Category editCategory;
    ArrayList<GroupBooks> selectedGrpList = new ArrayList<>();
    private ArrayList<Integer> editModeList = new ArrayList<>();
    int fromPos;

    public GridShelf(MainActivity context, DatabaseHandler databaseHandler, UserFunctions userfn) {
        super(context, databaseHandler, userfn);
    }
    @Override
    public int getCount() {
        return categoryListArray.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public static class ViewHolder {
        public TextView txtTitle,tv_noBooks;
        public Button btnPropertieCategory;
        public Button btnPropertyAccord;
        public ImageView btnArrangeCategory;
        public RecyclerView categoryGridView;
        public ImageView bgRackImageView;
        public int position;
        public FrameLayout btnFramePropCategory;
        public FrameLayout btnFramePropAccord;
        public RelativeLayout drag_layout;
        public RelativeLayout shelf_layout;
        public FrameLayout editLayout;
        public Button edit_btn;

    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        final Category category = categoryListArray.get(position);

        if (view == null) {
            view = context.getLayoutInflater().inflate(R.layout.shelf_scroll_inflate, null);
            Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
            UserFunctions.changeFont(parent,font);
            holder = new ViewHolder();
            holder.editLayout = (FrameLayout) view.findViewById(R.id.editLayout);
            holder.edit_btn = (Button) view.findViewById(R.id.editButton);
            holder.btnFramePropAccord = (FrameLayout) view.findViewById(R.id.frameButton1);
            holder.btnFramePropCategory = (FrameLayout) view.findViewById(R.id.frameButton2);
            holder.shelf_layout = (RelativeLayout) view.findViewById(R.id.shelf_layout);
            holder.drag_layout = (RelativeLayout) view.findViewById(R.id.drag_handle);
            holder.txtTitle = (TextView) view.findViewById(R.id.text);
            holder.btnPropertieCategory = (Button) view.findViewById(R.id.button2);
            holder.btnPropertyAccord = (Button) view.findViewById(R.id.button1);
            holder.btnArrangeCategory = (ImageView) view.findViewById(R.id.drag);
            holder.categoryGridView = (RecyclerView) view.findViewById(R.id.gridView1);
            holder.tv_noBooks =(TextView) view.findViewById(R.id.tv_noboooks);

            if (holder.categoryGridView.getTag()==null) {
                holder.categoryGridView.setTag(position);
            }
            ArrayList<GroupBooks> bookList=filteringbooks(category);
            if(bookList.size()==0){
                if(context.Search){
                    holder.tv_noBooks.setVisibility(View.VISIBLE);
                }else{
                    holder.tv_noBooks.setVisibility(View.INVISIBLE);
                }
            }else{
                holder.tv_noBooks.setVisibility(View.INVISIBLE);
            }
            HorizontalAdapter horizontalAdapter=new HorizontalAdapter(bookList,holder.categoryGridView,category);
            holder.categoryGridView.setAdapter(horizontalAdapter);


            holder.bgRackImageView = (ImageView) view.findViewById(R.id.bg_rack_image);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        setTexttoCategoryName(category, holder.txtTitle);
        holder.btnPropertyAccord.setId(position);
        holder.btnPropertieCategory.setId(position);
        holder.btnFramePropCategory.setId(position);
        holder.btnFramePropAccord.setId(position);
        holder.position = position;

         /*   if (category.getCategoryId() == 0) {
                holder.btnAddCategory.setVisibility(View.VISIBLE);
                holder.btnPropertieCategory.setVisibility(View.INVISIBLE);
                holder.btnPropertyAccord.setVisibility(View.INVISIBLE);
                holder.btnFramePropCategory.setVisibility(View.INVISIBLE);
                holder.btnFramePropAccord.setVisibility(View.INVISIBLE);
                holder.scrollView.setVisibility(View.GONE);
                holder.categoryGridView.setVisibility(View.GONE);
                holder.bgRackImageView.setVisibility(View.VISIBLE);
                Bitmap bitmap = BitmapFactory.decodeFile(category.getCategoryImagePath());
                holder.bgRackImageView.setImageBitmap(bitmap);
                holder.btnAddCategory.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Category lastCat = categoryListArray.get(categoryListArray.size() - 2);
                        Category category = new Category();
                        int maxCategoryId = db.getMaxUniqueRowID("tblCategory") + 1;
                        String imagePath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + maxCategoryId + ".png";
                        String catImgName = maxCategoryId + ".png";
                        category.setCategoryId(maxCategoryId);
                        category.setCategoryName("New Category");
                        category.setCategoryImagePath(imagePath);
                        category.setNewCatID(lastCat.getNewCatID() + 1);
                        UserFunctions.copyAsset(context.getAssets(), "4.png", imagePath);
                        categoryListArray.add(categoryListArray.size() - 1, category);
                        context.gridView.invalidateViews();
                        String insertQuery = "insert into tblCategory(CName, CBgImagePath, newCID) values('" + category.getCategoryName() + "', '" + catImgName + "', '" + category.getNewCatID() + "')";
                        db.executeQuery(insertQuery);
                    }
                });
            } else {*/
        if (position==categoryListArray.size()-1){
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.shelf_layout.getLayoutParams();
            layoutParams.setMargins(0,0,0,0);
        }

        holder.btnPropertieCategory.setVisibility(View.VISIBLE);
        holder.btnPropertyAccord.setVisibility(View.VISIBLE);
        holder.btnFramePropAccord.setVisibility(View.VISIBLE);
        holder.bgRackImageView.setVisibility(View.VISIBLE);
        holder.bgRackImageView.setImageBitmap(null);

        if (context.editmode){
            holder.btnFramePropCategory.setVisibility(View.VISIBLE);
            context.gridView.setDragEnabled(true);
            holder.btnPropertyAccord.setEnabled(false);
            holder.btnFramePropAccord.setEnabled(false);
            holder.edit_btn.setEnabled(false);
            holder.editLayout.setEnabled(false);
            if (editModeList.size()>0){
                editModeList.clear();
            }
            if (category.isAccordExpanded()){
                category.setAccordExpanded(false);
            }
            holder.drag_layout.setAlpha(0.75f);
        }else{
            holder.btnFramePropCategory.setVisibility(View.GONE);
            context.gridView.setDragEnabled(false);
            holder.edit_btn.setEnabled(true);
            holder.editLayout.setEnabled(true);
            holder.btnPropertyAccord.setEnabled(true);
            holder.btnFramePropAccord.setEnabled(true);
            holder.txtTitle.setFocusable(true);
        }
        if (checkIsEditMode(position)){
            holder.edit_btn.setBackgroundResource(R.drawable.done);
        }else{
            holder.edit_btn.setBackgroundResource(R.drawable.ic_edit);
        }

        //new addBooksToScrollView(category, linearScrollLayout).execute();

        if (category.isAccordExpanded()) {
            holder.btnPropertyAccord.setBackgroundResource(R.drawable.ic_collapse);
            // holder.scrollView.setVisibility(View.GONE);
            holder.categoryGridView.setVisibility(View.VISIBLE);
            LayoutParams gridlayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.categoryGridView.getLayoutParams();
            LayoutParams imglayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.bgRackImageView.getLayoutParams();
            gridlayoutParams.height=(int) context.getResources().getDimension(R.dimen.shelf_height)+(int) context.getResources().getDimension(R.dimen.shelf_height);
            imglayoutParams.height = holder.categoryGridView.getHeight();
            holder.categoryGridView.setLayoutParams(gridlayoutParams);
            holder.categoryGridView.addOnLayoutChangeListener(new OnLayoutChangeListener() {

                @Override
                public void onLayoutChange(View v, int left, int top, int right,
                                           int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    LayoutParams imglayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.bgRackImageView.getLayoutParams();
                    //System.out.println("GridView Height: "+v.getHeight());
                    imglayoutParams.height = v.getHeight();
                    holder.bgRackImageView.setLayoutParams(imglayoutParams);
                }
            });
            GridLayoutManager gridLayout=new GridLayoutManager(context,context.getResources().getInteger(R.integer.grid_column),GridLayoutManager.VERTICAL,false);
            //llm.canScrollHorizontally();
            holder.categoryGridView.setLayoutManager(gridLayout);
            holder.categoryGridView.setHasFixedSize(true);
            holder.categoryGridView.invalidate();
        } else {
            holder.btnPropertyAccord.setBackgroundResource(R.drawable.ic_expand);
            holder.categoryGridView.setVisibility(View.VISIBLE);
            LayoutParams imglayoutParams = (android.widget.RelativeLayout.LayoutParams) holder.bgRackImageView.getLayoutParams();
            LayoutParams scrollLayoutParams = holder.categoryGridView.getLayoutParams();
            imglayoutParams.height = scrollLayoutParams.height;
            LinearLayoutManager horizontalLayoutManagaer= new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.categoryGridView.setLayoutManager(horizontalLayoutManagaer);
            holder.categoryGridView.setHasFixedSize(true);
            holder.categoryGridView.invalidate();

            new addBooksToScrollView(position, category, holder).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

        }

        holder.txtTitle.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Category category;
                for (int i=0 ; i<categoryListArray.size();i++){
                    category = categoryListArray.get(i);
                    if (category.getCategoryId()==holder.txtTitle.getId()){
                        editCategory=category;
                    }
                }
                return context.gestureDetector.onTouchEvent(event);
            }
        });

        holder.editLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean exist = false;
                for (int i = 0;i<editModeList.size();i++){
                    int rack = editModeList.get(i);
                    if (rack==position){
                        exist=true;
                        editModeList.remove(i);
                        holder.edit_btn.setBackgroundResource(R.drawable.ic_edit);
                        break;
                    }
                }
                if (!exist){
                    editModeList.add(position);
                    holder.edit_btn.setBackgroundResource(R.drawable.done);
                }
                context.gridView.invalidateViews();
            }
        });

        holder.edit_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean exist = false;
                for (int i = 0;i<editModeList.size();i++){
                    int rack = editModeList.get(i);
                    if (rack==position){
                        exist=true;
                        editModeList.remove(i);
                        holder.edit_btn.setBackgroundResource(R.drawable.ic_edit);
                        break;
                    }
                }
                if (!exist){
                    editModeList.add(position);
                    holder.edit_btn.setBackgroundResource(R.drawable.done);
                }
                context.gridView.invalidateViews();
            }
        });

        holder.btnPropertieCategory.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                propertyCategoryClicked(v, holder);
            }
        });

        holder.btnPropertyAccord.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                propertyAccordClicked(v, holder);
            }
        });

        holder.btnFramePropCategory.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                propertyCategoryClicked(v, holder);
            }
        });

        holder.btnFramePropAccord.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                propertyAccordClicked(v, holder);
            }
        });
//            }
        return view;
    }
    private ArrayList<GroupBooks> filteringbooks(Category category){
        ArrayList<GroupBooks> bookList=new ArrayList<GroupBooks>();
        if(checkGrpSelectedForCategory(category.getCategoryId())){
            bookList=BooksListInsideGroup(category.getCategoryId(),getSeletedGrp(category.getCategoryId()));
        }else {
            for (int i = 0; i < groupBook.size(); i++) {
                GroupBooks grp = groupBook.get(i);
                if (grp.getCategoryId() == category.getCategoryId()) {
                    GroupBooks grpDetails = new GroupBooks();
                    grpDetails.setCategoryId(category.getCategoryId());
                    grpDetails.setGroupId(grp.getGroupId());
                    // grpDetails.setBookId(grp.getBookID());
                    grpDetails.setPosition(i);
                    grpDetails.setGroupName(grp.getGroupName());
                    for (int j = 0; j < bookListArray.size(); j++) {
                        Book book = (Book) bookListArray.get(j);
                        if (book.getbCategoryId() == category.getCategoryId() && book.get_groupId() == grp.getGroupId()) {
                            GroupBooks child = new GroupBooks();
                            child.setCategoryId(category.getCategoryId());
                            child.setGroupId(grp.getGroupId());
                            child.setBookId(book.getBookID());
                            child.setIsDownloadCompleted(book.getIsDownloadCompleted());
                            child.setPosition(j);
                            child.setBook(book);
                            grpDetails.getChildobjlist().add(child);
                            if (grpDetails.getChildobjlist().size() == 1) {
                                grpDetails.setBook(book);
                            }

                        }
                    }
                    bookList.add(grpDetails);
                }
            }

            for (int i = 0; i < bookListArray.size(); i++) {
                Book book = (Book) bookListArray.get(i);
                book.setIdPos(i);
                if (book.getbCategoryId() == category.getCategoryId() && book.get_groupId() == 0) {
                    // categoryBooksList.add(book);
                    GroupBooks grp = new GroupBooks();
                    grp.setCategoryId(category.getCategoryId());
                    grp.setGroupId(book.get_groupId());
                    grp.setBookId(book.getBookID());
                    grp.setIsDownloadCompleted(book.getIsDownloadCompleted());
                    grp.setPosition(i);
                    grp.setBook(book);
                    bookList.add(grp);
                }
            }
        }

        return bookList;
    }
    public void addCategory(){
        Category category = new Category();
        int maxCategoryId = db.getMaxUniqueRowID("tblCategory") + 1;
        String imagePath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + maxCategoryId + ".png";
        String catImgName = maxCategoryId + ".png";
        if (maxCategoryId == 9){
            category.setCategoryId(maxCategoryId+1);
        }else {
            category.setCategoryId(maxCategoryId);
        }
        category.setCategoryName("New Category");
        category.setCategoryImagePath(imagePath);
        category.setNewCatID(maxCategoryId);
        UserFunctions.copyAsset(context.getAssets(), "4.png", imagePath);
        categoryListArray.add(category);
//        context.cursor.newRow().add(categoryListArray.size()+1).add(category.getCategoryName());
//        this.changeCursor(context.cursor);
//        this.notifyDataSetChanged();
        String insertQuery = "insert into tblCategory(CName, CBgImagePath, newCID) values('" + category.getCategoryName() + "', '" + catImgName + "', '" + category.getNewCatID() + "')";
        db.executeQuery(insertQuery);
        context.gridView.invalidateViews();
        context.gridView.smoothScrollToPosition(categoryListArray.size()-1);
    }

    private void propertyAccordClicked(View v, ViewHolder holder) {
        int pos = v.getId();
        Category category = categoryListArray.get(pos);
        int j = 0;
        for (int i = 0; i < bookListArray.size(); i++) {
            Book book = (Book) bookListArray.get(i);
            if (!book.getIsDownloadCompleted().equals("downloading")) {
                if (book.getbCategoryId() == category.getCategoryId()) {
                    j = j + 1;
                    if (j == 2) {
                        if (category.isAccordExpanded()) {
                            holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_expanded_new);
                            category.setAccordExpanded(false);
                            context.gridView.invalidateViews();
                        } else {
                            holder.btnPropertyAccord.setBackgroundResource(R.drawable.category_acordian_collapsed_new);
                            category.setAccordExpanded(true);
                            context.gridView.invalidateViews();
                        }
                        break;
                    }
                }
            }
        }
    }

    private void propertyCategoryClicked(View v, final ViewHolder holder) {
        int pos = v.getId();
        final Category category = categoryListArray.get(pos);
//        View view = context.getLayoutInflater().inflate(R.layout.categories_property_view, null);
//        view.setBackgroundResource(R.drawable.category_border);
//        //Show the info view in a PopOver
//        final RelativeLayout rootView = (RelativeLayout) context.findViewById(R.id.shelfRootViewLayout);
//        final PopupWindow infoPopupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        infoPopupWindow.setContentView(view);
//        infoPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        infoPopupWindow.setOutsideTouchable(true);
//        infoPopupWindow.setFocusable(true);
//        infoPopupWindow.showAsDropDown(holder.btnPropertieCategory, 0, 0);
//        Button img_btn = (Button) view.findViewById(R.id.btn_backgrnd_image);
//        img_btn.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                categoryRack = category;
//                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
//                galleryIntent.setType("image/*");
//                context.startActivityForResult(galleryIntent, context.RACK_CAPTURE_GALLERY);
//                infoPopupWindow.dismiss();
//            }
//        });
//        Button btn_paint = (Button) view.findViewById(R.id.btn_paint);
//        btn_paint.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int color = 0;
//                if (category.getCategoryImagePath() != null && category.getCategoryImagePath().contains("#")) {
//                    color = Color.parseColor(category.getCategoryImagePath());
//                }
//                final ArrayList<String> recentBackgroundColorList = UserFunctions.getRecentColorsFromSharedPreference(Globals.rackBackgroundColorKey, context);
//                new PopupColorPicker(context, color, null, recentBackgroundColorList, false, new ColorPickerListener() {
//                    String hexColor = null;
//
//                    @Override
//                    public void pickedColor(int color) {
//                        holder.bgRackImageView.setImageBitmap(null);
//                        hexColor = String.format("#%06X", (0xFFFFFF & color));
//                        holder.bgRackImageView.setBackgroundColor(color);
//                    }
//
//                    @Override
//                    public void dismissPopWindow() {
//                        if (hexColor != null) {
//                            File rackImgFilePath = new File(Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + category.getCategoryId() + ".png");
//                            if (rackImgFilePath.exists()) {
//                                rackImgFilePath.delete();
//                            }
//                            UserFunctions.setandUpdateRecentColorsToSharedPreferences(Globals.rackBackgroundColorKey, recentBackgroundColorList, hexColor, context);
//                            category.setCategoryImagePath(hexColor);
//                            String query = "update tblCategory set CBgImagePath='" + hexColor + "' where CID='" + category.getCategoryId() + "'";
//                            context.db.executeQuery(query);
//                        }
//                    }
//
//                    @Override
//                    public void pickedTransparentColor(String transparentColor) {
//                    }
//
//                    @Override
//                    public void pickColorFromBg() {
//                    }
//                });
//                infoPopupWindow.dismiss();
//            }
//        });
//        Button btn_delete = (Button) view.findViewById(R.id.btn_delete);
//        btn_delete.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
        if (Globals.isTablet()) {
            if ( category.getCategoryId() == 1 || category.getCategoryId() == 2 || category.getCategoryId() == 3 || category.getCategoryId() == 4 || category.getCategoryId() == 5 || category.getCategoryId() == 6 || category.getCategoryId() == 7|| category.getCategoryId() == 8 ) {
                UserFunctions.DisplayAlertDialog(context, R.string.categories_delete_msg, R.string.delete);
            } else {
                Builder builder = new AlertDialog.Builder(context);
                builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean isCategoryEmpty = true;
                        for (int i = 0; i < bookListArray.size(); i++) {
                            Book book = (Book) bookListArray.get(i);
                            if (category.getCategoryId() == book.getbCategoryId()) {
                                isCategoryEmpty = false;
                                break;
                            }
                        }
                        if (isCategoryEmpty) {
                            categoryListArray.remove(category);
                            context.gridView.invalidateViews();
                            String query = "delete from tblCategory where CID='" + category.getCategoryId() + "'";
                            context.db.executeQuery(query);
//                            String updateQuery = "update tblCategory set newCID=newCID-1 where newCID>'"+category.getNewCatID()+"'";
//                            context.db.executeQuery(updateQuery);
                            String destRackImgPath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + category.getCategoryId() + ".png";
                            File fileImgDir1 = new File(destRackImgPath);
                            if (fileImgDir1.exists()) {
                                fileImgDir1.delete();
                            }
                            reloadCategoryListArray();
//                            infoPopupWindow.dismiss();
                        } else {
                            UserFunctions.DisplayAlertDialog(context, R.string.categories_delete_msg1, R.string.delete);
                        }}
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setTitle(R.string.delete_category);
                builder.setMessage(R.string.delete_sure);
                builder.show();
            }
        }
        else if (!Globals.isTablet()) {
            if (category.getCategoryId() == 1 || category.getCategoryId() == 2 || category.getCategoryId() == 3 || category.getCategoryId() == 4 || category.getCategoryId() == 5 || category.getCategoryId() == 7) {
                UserFunctions.DisplayAlertDialog(context, R.string.categories_delete_msg, R.string.delete);
            } else {
                Builder builder = new AlertDialog.Builder(context);
                builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean isCategoryEmpty = true;
                        for (int i = 0; i < bookListArray.size(); i++) {
                            Book book = (Book) bookListArray.get(i);
                            if (category.getCategoryId() == book.getbCategoryId()) {
                                isCategoryEmpty = false;
                                break;
                            }
                        }
                        if (isCategoryEmpty) {
                            categoryListArray.remove(category);
                            context.gridView.invalidateViews();
                            String query = "delete from tblCategory where CID='" + category.getCategoryId() + "'";
                            context.db.executeQuery(query);
//                            String updateQuery = "update tblCategory set newCID=newCID-1 where newCID>'"+category.getNewCatID()+"'";
//                            context.db.executeQuery(updateQuery);
                            String destRackImgPath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + category.getCategoryId() + ".png";
                            File fileImgDir1 = new File(destRackImgPath);
                            if (fileImgDir1.exists()) {
                                fileImgDir1.delete();
                            }
                            reloadCategoryListArray();
//                            infoPopupWindow.dismiss();
                        } else {
                            UserFunctions.DisplayAlertDialog(context, R.string.categories_delete_msg1, R.string.delete);
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setTitle(R.string.delete_category);
                builder.setMessage(R.string.delete_sure);
                builder.show();
            }

        }
//                infoPopupWindow.dismiss();
//            }
//        });
//        Button btn_restore = (Button) view.findViewById(R.id.btn_restore);
//        btn_restore.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String destRackImgPath = Globals.TARGET_BASE_SHELF_RACK_IMG_DIR_PATH + category.getCategoryId() + ".png";
//                category.setCategoryImagePath(destRackImgPath);
//                File fileImgDir1 = new File(destRackImgPath);
//                if (fileImgDir1.exists()) {
//                    fileImgDir1.delete();
//                }
//                String catImageName = category.getCategoryId() + ".png";
//                String query = "update tblCategory set CBgImagePath='" + catImageName + "' where CID='" + category.getCategoryId() + "'";
//                if (category.getCategoryId() == 1 || category.getCategoryId() == 2 || category.getCategoryId() == 3) {
//                    UserFunctions.copyAsset(context.getAssets(), catImageName, destRackImgPath);
//                } else {
//                    UserFunctions.copyAsset(context.getAssets(), "4.png", destRackImgPath);
//                }
//                context.db.executeQuery(query);
//                context.gridView.invalidateViews();
//                infoPopupWindow.dismiss();
//            }
//        });
//        final Button btn_edit_catgry_name = (Button) view.findViewById(R.id.btn_edit_category);
//        btn_edit_catgry_name.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (category.getNewCatID() == 1 || category.getNewCatID() == 2 || category.getNewCatID() == 3 || category.getNewCatID() == 4 || category.getNewCatID() == 5 || category.getNewCatID() == 6 || category.getNewCatID() == 7|| category.getNewCatID() == 8) {
//                    UserFunctions.DisplayAlertDialog(context, "You cannot edit this category title", R.string.edit_category);
//                } else {
//                    View view1 = context.getLayoutInflater().inflate(R.layout.category_property_listview, null);
//                    AlertDialog.Builder popView = new AlertDialog.Builder(context);
//                    popView.setView(view1);
//                    popView.setCancelable(false);
//                    popView.setTitle("Edit title");
//                    et_cat = (EditText) view1.findViewById(R.id.editText1);
//                    //et_cat.setText(category.getCategoryName());
//                    setTexttoCategoryName(category,holder.txtTitle);
//                    et_cat.setTag(category);
//                    if (Globals.isLimitedVersion()) {
//                        if (category.getNewCatID() == 1 || category.getNewCatID() == 2 || category.getNewCatID() == 3 || category.getNewCatID() == 4 || category.getNewCatID() == 5) {
//                            et_cat.setKeyListener(null);
//                        }
//                    } else if (!Globals.isLimitedVersion()) {
//                        if (category.getNewCatID() == 1 || category.getNewCatID() == 2 || category.getNewCatID() == 3 || category.getNewCatID() == 4 || category.getNewCatID() == 5 || category.getNewCatID() == 6) {
//                            et_cat.setKeyListener(null);
//                        }
//                    }
//                    popView.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Category category = (Category) et_cat.getTag();
//                            String catText = et_cat.getText().toString();
//                            if (!category.getCategoryName().equals(catText)) {
//                                category.setCategoryName(catText);
//                                context.gridView.invalidateViews();
//                                String query = "update tblCategory set CName='" + catText + "' where CID='" + category.getCategoryId() + "'";
//                                context.db.executeQuery(query);
//                            }
//                            dialog.cancel();
//                        }
//                    });
//                    popView.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    });
//                    AlertDialog alert = popView.create();
//                    alert.show();
//                }
//                infoPopupWindow.dismiss();
//            }
//        });
    }

    //	private class listAdapter extends BaseAdapter{
//
//		private Context mContext;
//
//		public Integer[] mThumbIds = {
//				R.drawable.arrange,
//				R.drawable.shelf_tools_bg_icon,
//				R.drawable.shelf_tools_delete_icon,
//				R.drawable.shelf_tools_paint_icon,
//				R.drawable.shelf_tools_rotate_icon
//		};
//
//
//		public listAdapter(Context context){
//			mContext = context;
//		}
//
//		@Override
//		public int getCount() {
//			return mThumbIds.length;
//		}
//
//		@Override
//		public Object getItem(int position) {
//			return null;
//		}
//
//		@Override
//		public long getItemId(int position) {
//			return 0;
//		}
//
//		@Override
//		public View getView(int position, View convertView, ViewGroup parent) {
//			View v = convertView;
//			if (v == null) {
//				v = context.getLayoutInflater().inflate(R.layout.category_property_listview, null);
//			}
//			ImageView iv = (ImageView) v.findViewById(R.id.category_image);
//			iv.setImageResource(mThumbIds[position]);
//			return v;
//		}
//	}
    public class HorizontalAdapter extends DragSortAdapter<HorizontalAdapter.ItemViewHolder> {

        private List<String> horizontalList;
        private ArrayList<Book> categoryBooksList = new ArrayList<Book>();
        ArrayList<GroupBooks> bookList;
        // private Category category;
        View itemView;
        RecyclerView recycler;
        Category category;

        @Override
        public int getPositionForId(long id) {
            return bookList.indexOf((int) id);
        }

        @Override
        public boolean move(int fromPosition, int toPosition) {
            if (fromPos!=toPosition && toPosition!=-1) {
                if (checkGrpSelectedForCategory(category.getCategoryId()) && toPosition == 0 && bookList.size() > fromPos) {
                    GroupBooks book = bookList.get(fromPos);
                    if (book.getBook() != null) {
                        int BId = book.getBook().getBookID();
                        db.executeQuery("update books set groupId='" + 0 + "' where BID='" + BId + "'");
                        UpdatingBookListArray(0, book.getBook(), false);
                        if (bookList.size() == 2) {
                            for (int i = 0; i < groupBook.size(); i++) {
                                GroupBooks grp = groupBook.get(i);
                                // Book bk= (Book) bookListArray.get(i);
                                if (grp.getGroupId() == book.getGroupId()) {
                                    db.executeQuery("delete from groupBooks where ID='" + grp.getGroupId() + "'");
                                    groupBook.remove(i);
                                    for (int i1 = 0;i1<selectedGrpList.size();i1++){
                                        GroupBooks grpBooks = selectedGrpList.get(i1);
                                        if (grpBooks.getCategoryId()==grp.getCategoryId()){
                                            selectedGrpList.remove(i1);
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }

                        }
                        bookList.remove(fromPos);
                    }
                }else{
                    GroupBooks fromBook = bookList.get(fromPos);
                    GroupBooks toBook = bookList.get(toPosition);
                    if (toBook.getIsDownloadCompleted().equals("downloaded")) {
                        Book book = fromBook.getBook();
                        int Id = 0;
                        if (toBook.getGroupId() == 0) {
                            createGroup(fromBook, toBook);
                        } else {
                            Id = toBook.getGroupId();
                            db.executeQuery("update books set groupId='" + Id + "' where BID='" + book.getBookID() + "'");
                            UpdatingBookListArray(Id, book, true);
                        }
                    }else{
                        UserFunctions.DisplayAlertDialog(context,R.string.cannot_create_folder, R.string.warning);
                    }
                }
            }
            context.gridView.invalidateViews();
            return false;
        }


        public class ItemViewHolder extends DragSortAdapter.ViewHolder{
            public TextView txtView;
            ImageView bookImg,img_grp,img_grp1,img_grp2,img_booktype;
            TextView tv_Title;
            RelativeLayout rl,rl_grp,rl_parent;
            FrameLayout layout1,layout2,layout3,layout4,lay_elesson,elesson_layout;

            // CardView rl;
            TextCircularProgressBar progressBar;
            Button imgInfoBtn,imgExportBtn,imgDeleteBtn,imginfo_btn1,el_imageBut_del,imageButt_cat;
            public ItemViewHolder(DragSortAdapter adapter,View itemView) {
                super(adapter,itemView);

                rl = (RelativeLayout) itemView.findViewById(R.id.shelfbookLayout);
                rl_parent = (RelativeLayout) itemView.findViewById(R.id.relativeLayout1);
                ViewGroup.MarginLayoutParams margins = (MarginLayoutParams) rl.getLayoutParams();
                margins.setMargins(0, 0, 0, 0);
                rl_grp = (RelativeLayout) itemView.findViewById(R.id.layout);
                rl_grp.setVisibility(View.INVISIBLE);
                img_grp = (ImageView) itemView.findViewById(R.id.img_grp);
                img_grp1 = (ImageView) itemView.findViewById(R.id.img_grp1);
                img_grp2 = (ImageView) itemView.findViewById(R.id.img_grp2);
                bookImg = (ImageView) itemView.findViewById(R.id.imageView2);
                progressBar = (TextCircularProgressBar) itemView.findViewById(R.id.circular_progress);
                progressBar.setVisibility(View.INVISIBLE);
                tv_Title = (TextView) itemView.findViewById(R.id.textView1);
                imgInfoBtn = (Button) itemView.findViewById(R.id.imageButton1);
                imgExportBtn = (Button) itemView.findViewById(R.id.imageButton2);
                imgDeleteBtn = (Button) itemView.findViewById(R.id.imageButton3);
                imginfo_btn1 = (Button) itemView.findViewById(R.id.imageButton4);
                el_imageBut_del = (Button) itemView.findViewById(R.id.el_imageBut_del);
                imageButt_cat = (Button) itemView.findViewById(R.id.imageButt_cat);
                layout1 = (FrameLayout) itemView.findViewById(R.id.layout1);
                layout2 = (FrameLayout) itemView.findViewById(R.id.layout2);
                layout3 = (FrameLayout) itemView.findViewById(R.id.layout3);
                layout4 = (FrameLayout) itemView.findViewById(R.id.layout4);
                lay_elesson = (FrameLayout) itemView.findViewById(R.id.lay_elesson);
                elesson_layout = (FrameLayout) itemView.findViewById(R.id.elesson_layout);
                img_booktype=(ImageView) itemView.findViewById(R.id.img_booktype);
            }

            @Override public View.DragShadowBuilder getShadowBuilder(View itemView, Point touchPoint) {
                return new NoForegroundShadowBuilder(itemView, touchPoint);
            }
        }

        public void createGroup(final GroupBooks fromBook, final GroupBooks toBook){
            final Dialog folderDialog = new Dialog(context);
            folderDialog.setTitle(context.getResources().getString(R.string.create_folder));
            folderDialog.setContentView(context.getLayoutInflater().inflate(R.layout.add_folder, null));
            folderDialog.getWindow().setLayout(Globals.getDeviceIndependentPixels(300, context), RelativeLayout.LayoutParams.WRAP_CONTENT);
            final EditText et_name = (EditText) folderDialog.findViewById(R.id.et_name);
            Button btn1 = (Button) folderDialog.findViewById(R.id.add);
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String folderName = et_name.getText().toString();
                    if (!folderName.equals("")) {
                        Book book= fromBook.getBook();
                        Book bo = null;
                        String grpName;
                        // if(rObject.getGroupName()==null){
                        bo= toBook.getBook();
                        grpName=folderName;
                        String insertQuery = "insert into groupBooks(groupName, categoryID) values('"+grpName+"', '" + fromBook.getCategoryId() + "')";
                        db.executeQuery(insertQuery);
                        int Id=db.getMaxUniqueRowID("groupBooks");
                        db.executeQuery("update books set groupId='"+Id +"' where BID='"+ bo.getBookID() +"'");
                        db.executeQuery("update books set groupId='"+Id +"' where BID='"+ book.getBookID() +"'");
                        GroupBooks grp=new GroupBooks();
                        grp.setGroupId(Id);
                        grp.setGroupName(grpName);
                        grp.setBookId(bo.getBookID());
                        grp.setCategoryId(fromBook.getCategoryId());
                        ArrayList<GroupBooks> childList = new ArrayList<GroupBooks>();
                        childList.add(fromBook);
                        childList.add(toBook);
                        grp.setChildobjlist(childList);
                        groupBook.add(0,grp);
                        UpdatingBookListArray(Id,bo,false);
                        UpdatingBookListArray(Id,book,true);
                        folderDialog.dismiss();
                    }

                }
            });
            folderDialog.show();
        }




        public HorizontalAdapter(ArrayList<GroupBooks> groupListArray,RecyclerView categoryGridView, Category catgory) {
            super(categoryGridView);
            //this.category = category;
            Book book = null;
            category=catgory;
            recycler=categoryGridView;
            bookList=groupListArray;
        }

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shelf_book, parent, false);
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shelf_book, parent, false);
            ItemViewHolder itemViewHolder = new ItemViewHolder(this,view);
            return itemViewHolder;
            // return new MyViewHolder(itemView);
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        public void onBindViewHolder(final ItemViewHolder holder, final int position) {
            // if(position<categoryBooksList.size()-1) {
            //final Book book = categoryBooksList.get(position);
            Book book = null;
            //final Book book = categoryBooksList.get(position);
            final GroupBooks grp = bookList.get(position);
            if(grp.getGroupName()==null) {
                book=grp.getBook();
                ArrayList<Book> downloadprogressList = downloadInProgressArray();
                if (book.getIsDownloadCompleted().equals("downloading") && downloadprogressList.size() > 0 &&book!=null) {
                    holder.progressBar.setMax(100);
                    holder.progressBar.getCircularProgressBar().setCircleWidth(20);
                    holder.progressBar.setVisibility(View.VISIBLE);
                    final DownloadBackground background = DownloadBackground.getInstance();
                    background.setContext(context);
                    Book progressBook = downloadprogressList.get(downloadprogressList.size() - 1);
//                    if (background.downLoadBook!=null && book.is_bStoreBook()&&book.get_bStoreID().equals(background.downLoadBook.get_bStoreID())) {
//                        background.setTextCircularProgressBar(holder.progressBar);
//                    }else
                    if (background.downLoadBook!=null &&book.getBookID()==background.downLoadBook.getBookID()) {
                        background.setTextCircularProgressBar(holder.progressBar);
                    }
                }

                int BID = book.getBookID();
                String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/pageBG_1.png";
                if (!new File(bookCardImagePath).exists()) {
                    bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/card.png";
                }
                if(book.get_bStoreID()!=null && book.get_bStoreID().contains("P")){
                    holder.bookImg.setBackgroundColor(Color.WHITE);
                }
                Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
                holder.bookImg.setImageBitmap(bitmap);
                holder.bookImg.setVisibility(View.VISIBLE);
                holder.rl_grp.setVisibility(View.INVISIBLE);
                if (checkIsEditMode((Integer) recycler.getTag()) && !book.getIsDownloadCompleted().equals("downloading")) {
                    holder.imgInfoBtn.setVisibility(View.VISIBLE);
                    if (Globals.isTablet() && book.getbCategoryId() == 1) {
                        holder.imgExportBtn.setVisibility(View.VISIBLE);
                    }
                    holder.imgDeleteBtn.setVisibility(View.VISIBLE);
                    if (book.getbCategoryId() == 2) {
                        holder.imginfo_btn1.setVisibility(View.VISIBLE);
                    }
                    if (book.is_bStoreBook() && book.get_bStoreID().contains("E") ||book.is_bStoreBook() && book.get_bStoreID().contains("R") ||book.is_bStoreBook() && book.get_bStoreID().contains("Q") || book.is_bStoreBook() && book.get_bStoreID().contains("I")|| book.is_bStoreBook() && book.get_bStoreID().contains("A")) {
                        holder.layout1.setVisibility(View.INVISIBLE);
                        holder.layout2.setVisibility(View.INVISIBLE);
                        holder.layout3.setVisibility(View.INVISIBLE);
                        holder.layout4.setVisibility(View.INVISIBLE);
                        holder.lay_elesson.setVisibility(View.VISIBLE);
                        holder.elesson_layout.setVisibility(View.VISIBLE);
                    }else{
                        holder.layout1.setVisibility(View.VISIBLE);
                        holder.layout2.setVisibility(View.VISIBLE);
                        holder.lay_elesson.setVisibility(View.INVISIBLE);
                        if (!book.is_bStoreBook() &&book.get_bStoreID() != null && (book.get_bStoreID().charAt(0) == 'I' || book.get_bStoreID().charAt(0) == 'A')){
                            holder.layout3.setVisibility(View.INVISIBLE);
                            holder.layout4.setVisibility(View.INVISIBLE);
                            holder.elesson_layout.setVisibility(View.VISIBLE);
                        }else {
                            holder.layout3.setVisibility(View.VISIBLE);
                            holder.layout4.setVisibility(View.VISIBLE);
                            holder.elesson_layout.setVisibility(View.INVISIBLE);
                        }
                    }


                } else {
                    holder.imgInfoBtn.setVisibility(View.INVISIBLE);
                    holder.imgExportBtn.setVisibility(View.INVISIBLE);
                    holder.imgDeleteBtn.setVisibility(View.INVISIBLE);
                    holder.imginfo_btn1.setVisibility(View.INVISIBLE);
                    holder.layout1.setVisibility(View.INVISIBLE);
                    holder.layout2.setVisibility(View.INVISIBLE);
                    holder.layout3.setVisibility(View.INVISIBLE);
                    holder.layout4.setVisibility(View.INVISIBLE);
                    holder.lay_elesson.setVisibility(View.INVISIBLE);
                    holder.elesson_layout.setVisibility(View.INVISIBLE);
                }

                if (!book.is_bStoreBook()&&book.get_bStoreID()==null ||book.is_bStoreBook() && book.get_bStoreID().contains("M")){
                    holder.img_booktype.setVisibility(View.VISIBLE);
                    holder.img_booktype.setBackgroundResource(R.drawable.ebe);

                }else if(book.get_bStoreID()!=null&&book.get_bStoreID().contains("P")){
                    holder.img_booktype.setVisibility(View.VISIBLE);
                    holder.img_booktype.setBackgroundResource(R.drawable.pdf);
                }else{
                    holder.img_booktype.setVisibility(View.GONE);
                }

                holder.bookImg.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if (!grp.getBook().getIsDownloadCompleted().equals("downloading")) {
                            if (checkGrpSelectedForCategory(grp.getCategoryId())) {
                                if (position != 0) {
                                    fromPos = position;
                                    Globals.shelfPosition = (int) recycler.getTag();
                                    holder.startDrag();
                                }
                            } else {
                                fromPos = position;
                                Globals.shelfPosition = (int) recycler.getTag();
                                holder.startDrag();
                            }
                        }
                        return true;
                    }
                });

                holder.bookImg.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!grp.getBook().getIsDownloadCompleted().equals("downloading")) {
                            for (int i = 0; i < bookListArray.size(); i++) {
                                Book bk = (Book) bookListArray.get(i);
                                if (bk.getBookID() == grp.getBookId()) {

                                    try {
                                        String domainURL = "http://www.nooor.com/semaenrichments/IPadSSboookService.asmx";
                                        JSONObject bookDetails = new JSONObject();
                                        bookDetails.put("book_id", bk.get_bStoreID());
                                        bookDetails.put("download_url", bk.getDownloadURL());
                                        bookDetails.put("book_title", bk.getBookTitle());
                                        bookDetails.put("b_description", bk.get_bDescription());
                                        bookDetails.put("total_pages", bk.getTotalPages());
                                        bookDetails.put("bstore_id", bk.get_bStoreID());
                                        bookDetails.put("b_category_id",bk.getbCategoryId());
                                        bookDetails.put("book_langugae", bk.getBookLangugae());
                                       // bookDetails.put("IsDownloadCompleted", bk.getIsDownloadCompleted());
                                        bookDetails.put("is_editable", bk.getIsEditable() );
                                        bookDetails.put("price", bk.getPrice());
                                        bookDetails.put("image_url", bk.getImageURL() );//bookDetails.put("type", "free");
                                        bookDetails.put("domain_uri", domainURL );


                                        context.bookClicked(bk.get_bStoreID(), bookDetails.toString(), true,bk);
                                        break;
                                    }catch (Exception e){
                                        Log.e("Ex",e.toString());
                                    }


                                   // context.bookClicked(i);
                                    break;
                                }
                            }
                        }

                    }
                });
                grp.setViewHolder(holder);

                if (book.getBookTitle().equals("null") || book.getBookTitle() == null) {
                    holder.tv_Title.setVisibility(View.GONE);
                } else {
                    holder.tv_Title.setVisibility(View.VISIBLE);
                    holder.tv_Title.setText(book.getBookTitle());
                }
                holder.imgInfoBtn.setOnClickListener(GridShelf.this);
                holder.imgExportBtn.setOnClickListener(GridShelf.this);
                holder.imginfo_btn1.setOnClickListener(GridShelf.this);
                holder.imageButt_cat.setOnClickListener(GridShelf.this);
                holder.el_imageBut_del.setOnClickListener(GridShelf.this);
                // holder.imgExportBtn.setTag(position);

                holder.imgDeleteBtn.setOnClickListener(GridShelf.this);
                // holder.imgDeleteBtn.setTag(position);
                for(int i=0;i<bookListArray.size();i++){
                    Book bk=(Book)bookListArray.get(i);
                    if(bk.getBookID()==grp.getBook().getBookID()){
                        holder.imgInfoBtn.setTag(i);
                        holder.imgExportBtn.setTag(i);
                        holder.imgDeleteBtn.setTag(i);
                        holder.imginfo_btn1.setTag(i);
                        holder.imageButt_cat.setTag(i);
                        holder.el_imageBut_del.setTag(i);
                        break;
                    }
                }
                itemView.setId(book.getIdPos());
            } else {

                String bookCardImagePath = null;
                if(grp.getChildobjlist().size()>0) {
                    bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + grp.getChildobjlist().get(0).getBookId() + "/FreeFiles/pageBG_1.png";
                    if (!new File(bookCardImagePath).exists()) {
                        bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + grp.getChildobjlist().get(0).getBookId() + "/FreeFiles/card.png";
                    }
                }
                Bitmap bitmap;
                if(bookCardImagePath==null && grp.getGroupName().equals("Back")){
                    //bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.back_book);
                    // holder.img_grp.setImageBitmap(bitmap);
                    reloadCategoryListArray();
                    holder.rl_grp.setBackgroundResource(R.drawable.cloud_back);
                    holder.img_grp.setImageBitmap(null);
                    holder.img_grp1.setImageBitmap(null);
                    holder.img_grp2.setImageBitmap(null);
                    holder.tv_Title.setText(category.getCategoryName());
                    holder.tv_Title.setVisibility(View.VISIBLE);
                    holder.img_booktype.setVisibility(View.GONE);
                }else {
                    ArrayList<GroupBooks> imgList = grp.getChildobjlist();
                    for (int img = 0;img<imgList.size();img++){
                        GroupBooks grpBooks = imgList.get(img);
                        if (img==0){
                           String cardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + grpBooks.getBookId() + "/FreeFiles/pageBG_1.png";
                            if (!new File(cardImagePath).exists()) {
                                cardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + grpBooks.getBookId() + "/FreeFiles/card.png";
                            }
                            holder.img_grp.setImageBitmap(BitmapFactory.decodeFile(cardImagePath));
                            holder.img_grp1.setImageBitmap(null);
                            holder.img_grp2.setImageBitmap(null);
                        }else if (img==1){
                            String cardImagePath1 = Globals.TARGET_BASE_BOOKS_DIR_PATH + grpBooks.getBookId() + "/FreeFiles/pageBG_1.png";
                            if (!new File(cardImagePath1).exists()) {
                                cardImagePath1 = Globals.TARGET_BASE_BOOKS_DIR_PATH + grpBooks.getBookId() + "/FreeFiles/card.png";
                            }
                            holder.img_grp1.setImageBitmap(BitmapFactory.decodeFile(cardImagePath1));
                            holder.img_grp2.setImageBitmap(null);
                        }else if (img ==2){
                            String cardImagePath2 = Globals.TARGET_BASE_BOOKS_DIR_PATH + grpBooks.getBookId() + "/FreeFiles/pageBG_1.png";
                            if (!new File(cardImagePath2).exists()) {
                                cardImagePath2 = Globals.TARGET_BASE_BOOKS_DIR_PATH + grpBooks.getBookId() + "/FreeFiles/card.png";
                            }
                            holder.img_grp2.setImageBitmap(BitmapFactory.decodeFile(cardImagePath2));
                            break;
                        }
                    }
                    holder.tv_Title.setText(grp.getGroupName());
                    holder.tv_Title.setVisibility(View.VISIBLE);
                }


                holder.bookImg.setVisibility(View.INVISIBLE);
                holder.rl_grp.setVisibility(View.VISIBLE);
                holder.rl_grp.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //grpSelected=true;
                        //grpId=grp.getGroupId();
                        //context.gridView.invalidateViews();
                        if(grp.getGroupName().equals("Back")){
                           for (int i1 = 0;i1<selectedGrpList.size();i1++){
                               GroupBooks grpBooks = selectedGrpList.get(i1);
                               if (grpBooks.getCategoryId()==grp.getCategoryId()){
                                   selectedGrpList.remove(i1);
                                   break;
                               }
                           }
                            context.gridView.invalidateViews();
                        }else {
                            ArrayList<GroupBooks> childGrp = null;
                            if(grp.getChildobjlist().size()>0){
                                selectedGrpList.add(grp);
                                childGrp=BooksListInsideGroup(grp.getCategoryId(),grp);
                            }
                            bookList.clear();
                            bookList = childGrp;
                            notifyDataSetChanged();
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return bookList.size();
        }

    }

    private class CategoryGridAdapter extends BaseAdapter {

        private ArrayList<Book> categoryBooksList = new ArrayList<Book>();
        private Category category;

        private CategoryGridAdapter(Category category) {
            this.category = category;
            Book book = null;
//            if (category.getCategoryId() == 2 || category.getCategoryId() == Globals.myMediaCatId|| category.getCategoryId() == Globals.mindMapCatId) {
//                categoryBooksList.add(book);
//            }
            for (int i = 0; i < bookListArray.size(); i++) {
                book = (Book) bookListArray.get(i);
                book.setIdPos(i);
                if (book.getbCategoryId() == category.getCategoryId()) {
                    categoryBooksList.add(book);
                }
            }
        }

        @Override
        public int getCount() {
            return categoryBooksList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
                view = context.getLayoutInflater().inflate(R.layout.shelf_book, null);
            }
            final Book book = categoryBooksList.get(position);
            //CardView   rl = (CardView)view.findViewById(R.id.relativeLayout2);
            LinearLayout rl = (LinearLayout) view.findViewById(R.id.relativeLayout2);
            ViewGroup.MarginLayoutParams margins = (MarginLayoutParams) rl.getLayoutParams();
            margins.setMargins(0, 0, 0, 0);
            ImageView bookImg = (ImageView) view.findViewById(R.id.imageView2);
            TextCircularProgressBar progressBar = (TextCircularProgressBar) view.findViewById(R.id.circular_progress);
            progressBar.setVisibility(View.INVISIBLE);
            ArrayList<Book> downloadprogressList = downloadInProgressArray();
            if (book.getIsDownloadCompleted().equals("downloading") && downloadprogressList.size() > 0) {
                progressBar.setMax(100);
                progressBar.getCircularProgressBar().setCircleWidth(20);
                progressBar.setVisibility(View.VISIBLE);
                final DownloadBackground background = DownloadBackground.getInstance();
                background.setContext(context);
                Book progressBook = downloadprogressList.get(downloadprogressList.size()-1);
                if (book.get_bStoreID().equals(progressBook.get_bStoreID())) {
                    background.setTextCircularProgressBar(progressBar);
                }
            }
            TextView tv_Title = (TextView) view.findViewById(R.id.textView1);
            int BID = book.getBookID();
            String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/pageBG_1.png";
            if (!new File(bookCardImagePath).exists()) {
                bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/card.png";
            }
            Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
            bookImg.setImageBitmap(bitmap);
            bookImg.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (!book.getIsDownloadCompleted().equals("downloading")) {
                     //   editBooks();
                        context.gridView.invalidateViews();
                    }
                    return false;
                }
            });
            bookImg.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (!book.getIsDownloadCompleted().equals("downloading")) {
                       // context.bookClicked(position);
                        context.bookClicked("downloading","",false,book);
                    }
                }
            });

            if (book.getBookTitle().equals("null") || book.getBookTitle() == null) {
                tv_Title.setVisibility(View.GONE);
            } else {
                tv_Title.setVisibility(View.VISIBLE);
                tv_Title.setText(book.getBookTitle());
            }

            view.setId(book.getIdPos());

            Button imgInfoBtn = (Button) view.findViewById(R.id.imageButton1);
            imgInfoBtn.setOnClickListener(GridShelf.this);
            imgInfoBtn.setTag(position);
            Button imgExportBtn = (Button) view.findViewById(R.id.imageButton2);
            imgExportBtn.setOnClickListener(GridShelf.this);
            imgExportBtn.setTag(position);
            Button imgDeleteBtn = (Button) view.findViewById(R.id.imageButton3);
            imgDeleteBtn.setOnClickListener(GridShelf.this);
            imgDeleteBtn.setTag(position);

            if (!book.getIsDownloadCompleted().equals("downloading")) {
                imgInfoBtn.setVisibility(View.VISIBLE);
                if (Globals.isTablet() && book.getbCategoryId() == 1) {
                    imgExportBtn.setVisibility(View.VISIBLE);
                }
                imgDeleteBtn.setVisibility(View.VISIBLE);

            } else {
                imgInfoBtn.setVisibility(View.INVISIBLE);
                imgExportBtn.setVisibility(View.INVISIBLE);
                imgDeleteBtn.setVisibility(View.INVISIBLE);
            }
            return view;
        }
    }

    private class addBooksToScrollView extends AsyncTask<Void, Object[], Void> {

        private Category category;
        //private LinearLayout linearScrollLayout;
        private ViewHolder holder;
        private int position;

        private addBooksToScrollView(int position, Category category, ViewHolder holder) {
            this.category = category;
            //this.linearScrollLayout = holder.linearScrollLayout;
            this.holder = holder;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            if (holder.position == position) {
                //holder.categoryGridView.removeAllViews();
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            /*for (int i = 0; i < bookListArray.size(); i++) {
				Book book = (Book) bookListArray.get(i);
				if (book.getbCategoryId() == category.getCategoryId()) {
					View gridImg = context.getLayoutInflater().inflate(R.layout.shelf_book, null);
					ImageView bookImg = (ImageView) gridImg.findViewById(R.id.imageView2);
					int BID = book.getBookID();
					String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/FreeFiles/pageBG_1.png";
					if(!new File(bookCardImagePath).exists()){
						bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/FreeFiles/card.png";
					}
					Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
					bookImg.setImageBitmap(bitmap);

					linearScrollLayout.addView(gridImg);
					//Object[] obj = {bookImg, bitmap, gridImg};
					//publishProgress(obj);
				}
			}*/
            if (holder.position == position) {
//                if (category.getCategoryId() == 2 || category.getCategoryId() == Globals.myMediaCatId|| category.getCategoryId() == Globals.mindMapCatId) {
//                    addNewBookButton();
//                }
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < bookListArray.size(); i++) {
                            final Book book = (Book) bookListArray.get(i);
                            if (book.getbCategoryId() == category.getCategoryId()) {

                                final View gridImg = context.getLayoutInflater().inflate(R.layout.shelf_book, null);
                                ImageView bookImg = (ImageView) gridImg.findViewById(R.id.imageView2);
                                TextCircularProgressBar progressBar = (TextCircularProgressBar) gridImg.findViewById(R.id.circular_progress);
                                progressBar.setVisibility(View.INVISIBLE);
                                ArrayList<Book> downloadprogressList = downloadInProgressArray();
                                if (book.getIsDownloadCompleted().equals("downloading") && downloadprogressList.size()>0){
                                    progressBar.setMax(100);
                                    //    progressBar.clearAnimation();
                                    progressBar.getCircularProgressBar().setCircleWidth(20);
                                    progressBar.setVisibility(View.VISIBLE);
                                    final DownloadBackground background = DownloadBackground.getInstance();
                                    background.setContext(context);
                                    Book progressBook = downloadprogressList.get(0);
                                    if (book.is_bStoreBook() &&book.get_bStoreID().equals(progressBook.get_bStoreID())) {
                                        background.setTextCircularProgressBar(progressBar);
                                    }else if(!book.is_bStoreBook()&&book.getBookID()==progressBook.getBookID()){
                                        background.setTextCircularProgressBar(progressBar);
                                    }
                                    if (downloadprogressList.size()>0 && !background.isDownloadTaskRunning()){
                                        if (!(background.getList().size() >0)) {
                                            background.setList(downloadprogressList);
                                        }
                                        final Book book1 = background.getList().get(0);
                                        background.setDownLoadBook(book1);
                                        if(!book1.is_bStoreBook()){
                                            ArrayList<String> Stringlist= null;
                                            ArrayList<HashMap<String, String>> CloudEnrList = null;
                                            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
                                            try {
                                                CloudEnrList= (ArrayList<HashMap<String, String>> ) ObjectSerializer.deserialize(sharedPref.getString("selectedCloudList", ObjectSerializer.serialize(new ArrayList<HashMap<String, String>>())));
                                                Stringlist = (ArrayList<String>) ObjectSerializer.deserialize(sharedPref.getString("jsonString", ObjectSerializer.serialize(new ArrayList<String>())));
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            background.setString(Stringlist);
                                            background.setSelectedCloudEnrList(CloudEnrList);
                                        }
                                        background.setDownloadTaskRunning(true);
                                        if (!book.is_bStoreBook() && book.get_bStoreID()!=null &&book.get_bStoreID().contains("P")){
                                            new savingScrennShotForPDF(book).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                        }
                                        if (book.is_bStoreBook() &&book1.get_bStoreID().contains("E") && new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+book1.get_bStoreID()+"Book").exists()){
                                            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
                                            String str = pref.getString(Globals.IJSTversion,"");
                                            if (str.equals("")) {
                                                background.new downloadIJST().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Globals.IJSTupdateUrl);
                                            }
                                        }else {
                                            new android.os.Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    background.new downloadAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, book1.getDownloadURL());
                                                }
                                            }, 2000);
                                        }
                                    }
                                }
                                TextView tv_Title = (TextView) gridImg.findViewById(R.id.textView1);

                                bookImg.setId(i);
                                if (!context.editmode){
                                    bookImg.setOnClickListener(new OnClickListener() {

                                        @Override
                                        public void onClick(View v) {
                                            if (!book.getIsDownloadCompleted().equals("downloading")) {
                                               // context.bookClicked(v.getId());
                                                context.bookClicked("downloading","",false,book);
                                            }
                                        }
                                    });
                                    bookImg.setOnLongClickListener(new View.OnLongClickListener() {
                                        @Override
                                        public boolean onLongClick(View v) {
                                            if (!book.getIsDownloadCompleted().equals("downloading")) {
                                            //    editBooks();
                                                context.gridView.invalidateViews();
                                            }
                                            return false;
                                        }
                                    });

                                    bookImg.setOnTouchListener(new OnTouchListener() {

                                        @Override
                                        public boolean onTouch(View v, MotionEvent event) {
                                            switch (event.getAction()) {
                                                case MotionEvent.ACTION_DOWN:
                                                    ImageView view = (ImageView) v;
                                                    view.getDrawable().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                                                    view.invalidate();
                                                    break;

                                                case MotionEvent.ACTION_UP:
                                                case MotionEvent.ACTION_CANCEL:
                                                    ImageView view1 = (ImageView) v;
                                                    view1.getDrawable().clearColorFilter();
                                                    view1.invalidate();
                                                    break;

                                                default:
                                                    break;
                                            }
                                            return false;
                                        }
                                    });
                                }

                                Button imgInfoBtn = (Button) gridImg.findViewById(R.id.imageButton1);
                                imgInfoBtn.setOnClickListener(GridShelf.this);
                                imgInfoBtn.setTag(i);
                                Button imgExportBtn = (Button) gridImg.findViewById(R.id.imageButton2);
                                imgExportBtn.setOnClickListener(GridShelf.this);
                                imgExportBtn.setTag(i);
                                Button imgDeleteBtn = (Button) gridImg.findViewById(R.id.imageButton3);
                                imgDeleteBtn.setOnClickListener(GridShelf.this);
                                imgDeleteBtn.setTag(i);
                                Button imageButt_cat = (Button) gridImg.findViewById(R.id.imageButt_cat);
                                imageButt_cat.setOnClickListener(GridShelf.this);
                                imageButt_cat.setTag(i);

                                if (isInEditMode && !book.getIsDownloadCompleted().equals("downloading")) {
                                    imgInfoBtn.setVisibility(View.VISIBLE);
                                    // if (Globals.isTablet() && book.getbCategoryId() == 1) {
                                    imgExportBtn.setVisibility(View.VISIBLE);
                                    //}
                                    imgDeleteBtn.setVisibility(View.VISIBLE);
                                } else {
                                    imgInfoBtn.setVisibility(View.INVISIBLE);
                                    imgExportBtn.setVisibility(View.INVISIBLE);
                                    imgDeleteBtn.setVisibility(View.INVISIBLE);
                                }

                                int BID = book.getBookID();
                                String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/pageBG_1.png";
                                if (!new File(bookCardImagePath).exists()) {
                                    bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/card.png";
                                }

                                if (book.getBookTitle().equals("null") || book.getBookTitle() == null) {
                                    tv_Title.setVisibility(View.GONE);
                                } else {
                                    tv_Title.setVisibility(View.VISIBLE);
                                    tv_Title.setText(book.getBookTitle());
                                }

                                Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
                                bookImg.setImageBitmap(bitmap);
                                Object[] obj = {bookImg, bitmap, gridImg,book};

                                //publishProgress(obj);
                                holder.categoryGridView.getAdapter().notifyDataSetChanged();
                                //linearScrollLayout.addView(gridImg);
                            }
                        }
                    }
                });
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Object[]... obj) {
            super.onProgressUpdate(obj);
            if (holder.position == position) {

                Object[] obj1 = obj[0];
                ImageView bookImg = (ImageView) obj1[0];
                Bitmap bitmap = (Bitmap) obj1[1];
                View gridImg = (View) obj1[2];
                // holder.categoryGridView.getAdapter().notifyDataSetChanged();
                // holder.categoryGridView.notifyDataSetChanged();
                //bookImg.setImageBitmap(bitmap);
                //holder.categoryGridView.addView(gridImg);
                //context.gridView.invalidateViews();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (context.editmode){
                holder.txtTitle.setOnTouchListener(null);
            }
        }

        private void addNewBookButton() {
            View gridImg = context.getLayoutInflater().inflate(R.layout.shelf_book, null);
            ImageView bookImg = (ImageView) gridImg.findViewById(R.id.imageView2);
            bookImg.setBackgroundResource(R.drawable.add_shelf);
            if (category.getCategoryId() == 2) {
                bookImg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.showAddNewBookDialog();
                    }
                });
            } else if(category.getCategoryId() == Globals.myMediaCatId) {
                bookImg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.showAddNewMediaAlbumActivity();
                    }
                });
            }else if(category.getCategoryId()==Globals.mindMapCatId){
                bookImg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MindMapDialog dialog = new MindMapDialog(context,false);
                        dialog.showMindMapDialog("");
                    }
                });
            }

            TextView tv_Title = (TextView) gridImg.findViewById(R.id.textView1);
            tv_Title.setVisibility(View.GONE);
            Button imgInfoBtn = (Button) gridImg.findViewById(R.id.imageButton1);
            imgInfoBtn.setVisibility(View.GONE);
            Button imgExportBtn = (Button) gridImg.findViewById(R.id.imageButton2);
            imgExportBtn.setVisibility(View.GONE);
            Button imgDeleteBtn = (Button) gridImg.findViewById(R.id.imageButton3);
            imgDeleteBtn.setVisibility(View.GONE);
            Object[] obj = {bookImg, null, gridImg};
            publishProgress(obj);
        }

    }

	/*@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		Book book = (Book) bookListArray.get(position);

		if (view == null) {
			view = context.getLayoutInflater().inflate(R.layout.shelf_book, null);
		}

		ImageView bookImg = (ImageView) view.findViewById(R.id.imageView2);
		int BID = book.getBookID();
		String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/FreeFiles/pageBG_1.png";
		if(!new File(bookCardImagePath).exists()){
			bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/FreeFiles/card.png";
		}
		Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
		bookImg.setImageBitmap(bitmap);

		TextView txtBookTilte = (TextView) view.findViewById(R.id.textView1);
		String bookTitle = book.getBookTitle();
		txtBookTilte.setText(bookTitle);

		ImageButton imgInfoBtn = (ImageButton) view.findViewById(R.id.imageButton1);
		imgInfoBtn.setOnClickListener(this);
		imgInfoBtn.setTag(position);
		ImageButton imgExportBtn = (ImageButton) view.findViewById(R.id.imageButton2);
		imgExportBtn.setOnClickListener(this);
		imgExportBtn.setTag(position);
		ImageButton imgDeleteBtn = (ImageButton) view.findViewById(R.id.imageButton3);
		imgDeleteBtn.setOnClickListener(this);
		imgDeleteBtn.setTag(position);
		if (isInEditMode) {
			imgInfoBtn.setVisibility(View.VISIBLE);
			imgExportBtn.setVisibility(View.VISIBLE);
			imgDeleteBtn.setVisibility(View.VISIBLE);

		}else{
			imgInfoBtn.setVisibility(View.INVISIBLE);
			imgExportBtn.setVisibility(View.INVISIBLE);
			imgDeleteBtn.setVisibility(View.INVISIBLE);
		}

		return view;
	}*/

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.imageButton4: {
                int position = (Integer) view.getTag();
                currentBook = (Book) bookListArray.get(position);
                if (currentBook.is_bStoreBook()){
                    infoPopupForStoreBook(currentBook);
                }else if (currentBook.getbCategoryId()==2){
                    infoPopupForNormalBook(view);
                }
                break;
            }
            case R.id.el_imageBut_del:{
                //Get the current book from the clicked position
                int position = (Integer) view.getTag();
                currentBook = (Book) bookListArray.get(position);

                //Show the info view in a PopOver
                RelativeLayout rootView = (RelativeLayout) context.findViewById(R.id.shelfRootViewLayout);
                final PopoverView popoverView = new PopoverView(context, R.layout.categories_list_view);
                popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.shelf_info_view_width), (int) context.getResources().getDimension(R.dimen.shelf_info_view_height)));
                popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(view), PopoverView.PopoverArrowDirectionAny, true);

                ListView listView = (ListView) popoverView.findViewById(R.id.listView1);
                TextView txtNoCategories = (TextView) popoverView.findViewById(R.id.textView1);
                ArrayList<Category> catList = new ArrayList<Category>();
                for (int i = 0; i < categoryListArray.size() ; i++) {
                    Category category = categoryListArray.get(i);
                    if (currentBook.is_bStoreBook() && (currentBook.get_bStoreID().charAt(1) == 'M' || currentBook.get_bStoreID().charAt(0) == 'C')) {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'E') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 5 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'Q') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'R') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'P') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.get_bStoreID()!= null && (currentBook.get_bStoreID().charAt(0) == 'I' || currentBook.get_bStoreID().charAt(1) == 'I' )) {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 ) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.get_bStoreID()!= null && !currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(0) == 'A') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6  && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5) {
                            catList.add(category);
                        }
                    }
                    else {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5) {
                            catList.add(category);
                        }
                    }
                }
                if (catList.isEmpty()) {
                    txtNoCategories.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    txtNoCategories.setText(R.string.add_categories);
                } else {
                    txtNoCategories.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                    listView.setAdapter(new CategoryListAdapter(catList));
                    listView.setOnItemClickListener(new OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> adapterView, final View view,
                                                int position, long id) {
                            final Category cat = (Category) view.getTag();
                            String moveDialog = context.getResources().getString(R.string.movedialog) + " " + cat.getCategoryName();
                            Builder builder = new AlertDialog.Builder(context);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (checkGrpSelectedForCategory(currentBook.getbCategoryId())){
                                        for (Category curCategory : categoryListArray){
                                            if (currentBook.getbCategoryId()==curCategory.getCategoryId()){
                                                ArrayList<GroupBooks> categoryBookList = filteringbooks(curCategory);
                                                if (categoryBookList.size() == 2) {
                                                    for (int i = 0; i < groupBook.size(); i++) {
                                                        GroupBooks grp = groupBook.get(i);
                                                        // Book bk= (Book) bookListArray.get(i);
                                                        if (grp.getGroupId() == currentBook.get_groupId()) {
                                                            db.executeQuery("delete from groupBooks where ID='" + grp.getGroupId() + "'");
                                                            groupBook.remove(i);
                                                            for (int i1 = 0;i1<selectedGrpList.size();i1++){
                                                                GroupBooks grpBooks = selectedGrpList.get(i1);
                                                                if (grpBooks.getCategoryId()==grp.getCategoryId()){
                                                                    selectedGrpList.remove(i1);
                                                                    break;
                                                                }
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    if (currentBook.get_groupId()!=0){
                                        db.executeQuery("update books set groupId='" + 0 + "' where BID='" + currentBook.getBookID() + "'");
                                    }
                                    currentBook.set_groupId(0);
                                    currentBook.setbCategoryId(cat.getCategoryId());
                                    context.gridView.invalidateViews();
                                    String query = "update books set CID='" + cat.getCategoryId() + "' where BID ='" + currentBook.getBookID() + "'";
                                    context.db.executeQuery(query);
                                    popoverView.dissmissPopover(true);
                                    for (int i = 0; i < bookListArray.size(); i++) {
                                        Book  boook = (Book) bookListArray.get(i);
                                        if(currentBook.is_bStoreBook()) {
                                            if (currentBook.get_bStoreID().equals(boook.get_bStoreID()) &&currentBook.getBookID()==boook.getBookID()) {
                                                bookListArray.remove(i);
                                                bookListArray.add(0, currentBook);
                                                break;
                                            }
                                        }else {
                                            if(currentBook.getBookID()==boook.getBookID()){
                                                bookListArray.remove(i);
                                                bookListArray.add(0, currentBook);
                                                break;
                                            }
                                        }
                                    }
                                }
                            });

                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.setTitle(R.string.movebook);
                            builder.setMessage(moveDialog);
                            builder.show();

                        }
                    });
                }
                break;
            }
            case R.id.imageButton1: {
                //Get the current book from the clicked position
                int position = (Integer) view.getTag();
                currentBook = (Book) bookListArray.get(position);

                //Show the info view in a PopOver
                RelativeLayout rootView = (RelativeLayout) context.findViewById(R.id.shelfRootViewLayout);
                final PopoverView popoverView = new PopoverView(context, R.layout.categories_list_view);
                popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.shelf_info_view_width), (int) context.getResources().getDimension(R.dimen.shelf_info_view_height)));
                popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(view), PopoverView.PopoverArrowDirectionAny, true);

                ListView listView = (ListView) popoverView.findViewById(R.id.listView1);
                TextView txtNoCategories = (TextView) popoverView.findViewById(R.id.textView1);
                ArrayList<Category> catList = new ArrayList<Category>();
                for (int i = 0; i < categoryListArray.size() ; i++) {
                    Category category = categoryListArray.get(i);
                    if (currentBook.is_bStoreBook() && (currentBook.get_bStoreID().charAt(1) == 'M' || currentBook.get_bStoreID().charAt(0) == 'C')) {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'E') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 5 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'Q') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'R') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 7) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(1) == 'P') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.get_bStoreID()!= null && (currentBook.get_bStoreID().charAt(0) == 'I' || currentBook.get_bStoreID().charAt(1) == 'I' )) {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 ) {
                            catList.add(category);
                        }
                    }
                    else if (currentBook.get_bStoreID()!= null && !currentBook.is_bStoreBook() && currentBook.get_bStoreID().charAt(0) == 'A') {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6  && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 2 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5) {
                            catList.add(category);
                        }
                    }
                    else {
                        if (Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5 && category.getCategoryId() != 6 && category.getCategoryId() != 7 && category.getCategoryId() != 8) {
                            catList.add(category);
                        } else if (!Globals.isTablet() && currentBook.getbCategoryId() != category.getCategoryId() && category.getCategoryId() != 1 && category.getCategoryId() != 3 && category.getCategoryId() != 4 && category.getCategoryId() != 5) {
                            catList.add(category);
                        }
                    }
                }
                if (catList.isEmpty()) {
                    txtNoCategories.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    txtNoCategories.setText(R.string.add_categories);
                } else {
                    txtNoCategories.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                    listView.setAdapter(new CategoryListAdapter(catList));
                    listView.setOnItemClickListener(new OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> adapterView, final View view,
                                                int position, long id) {
                            final Category cat = (Category) view.getTag();
                            String moveDialog = context.getResources().getString(R.string.movedialog) + " " + cat.getCategoryName();
                            Builder builder = new AlertDialog.Builder(context);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (checkGrpSelectedForCategory(currentBook.getbCategoryId())){
                                        for (Category curCategory : categoryListArray){
                                            if (currentBook.getbCategoryId()==curCategory.getCategoryId()){
                                                ArrayList<GroupBooks> categoryBookList = filteringbooks(curCategory);
                                                if (categoryBookList.size() == 2) {
                                                    for (int i = 0; i < groupBook.size(); i++) {
                                                        GroupBooks grp = groupBook.get(i);
                                                        // Book bk= (Book) bookListArray.get(i);
                                                        if (grp.getGroupId() == currentBook.get_groupId()) {
                                                            db.executeQuery("delete from groupBooks where ID='" + grp.getGroupId() + "'");
                                                            groupBook.remove(i);
                                                            for (int i1 = 0;i1<selectedGrpList.size();i1++){
                                                                GroupBooks grpBooks = selectedGrpList.get(i1);
                                                                if (grpBooks.getCategoryId()==grp.getCategoryId()){
                                                                    selectedGrpList.remove(i1);
                                                                    break;
                                                                }
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    if (currentBook.get_groupId()!=0){
                                        db.executeQuery("update books set groupId='" + 0 + "' where BID='" + currentBook.getBookID() + "'");
                                    }
                                    currentBook.set_groupId(0);
                                    currentBook.setbCategoryId(cat.getCategoryId());
                                    context.gridView.invalidateViews();
                                    String query = "update books set CID='" + cat.getCategoryId() + "' where BID ='" + currentBook.getBookID() + "'";
                                    context.db.executeQuery(query);
                                    popoverView.dissmissPopover(true);
                                    for (int i = 0; i < bookListArray.size(); i++) {
                                        Book  boook = (Book) bookListArray.get(i);
                                        if(currentBook.is_bStoreBook()) {
                                            if (currentBook.get_bStoreID().equals(boook.get_bStoreID()) &&currentBook.getBookID()==boook.getBookID()) {
                                                bookListArray.remove(i);
                                                bookListArray.add(0, currentBook);
                                                break;
                                            }
                                        }else {
                                            if(currentBook.getBookID()==boook.getBookID()){
                                                bookListArray.remove(i);
                                                bookListArray.add(0, currentBook);
                                                break;
                                            }
                                        }
                                    }
                                }
                            });

                            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.setTitle(R.string.movebook);
                            builder.setMessage(moveDialog);
                            builder.show();

                        }
                    });
                }
                break;
            }
            case R.id.imageButton2: {
                if (UserFunctions.checkLoginAndAlert(context,true)) {
                    if (context.group.getShelfCloudSharingEnable()) {
                        int position = (Integer) view.getTag();
                        currentBook = (Book) bookListArray.get(position);
                        Intent ExportIntent = null;
                       if (currentBook.is_bStoreBook()) {
                            String query = "select count(*) from enrichments where BID='"+currentBook.getBookID()+"'";
                            int enrichmentCount=context.db.getCountForEnrichments(currentBook.getBookID(),query);
                            String noteCountquery = "select count(*) from  tblNote where BName='"+currentBook.get_bStoreID()+"'";
                            int notesCount=context.db.getCountForEnrichments(currentBook.getBookID(),noteCountquery);
                            if(notesCount>0 ||enrichmentCount>0) {
                                Intent intent = new Intent(context, ExportEnrActivity.class);
                                intent.putExtra("Book", currentBook);
                                intent.putExtra("Shelf", GridShelf.this);
                                context.startActivity(intent);
                            }else{
                                UserFunctions.DisplayAlertDialog(context, R.string.no_enrichments, R.string.share);
                            }
                       /*ExportIntent = new Intent(context, ExportEnrichmentActivity.class);
                        ExportIntent.putExtra("Book", currentBook);
                        //ExportIntent.putExtra("UserCredentials", context.SlideMenuActivity.userCredentials);
                        context.startActivity(ExportIntent); */
                    } else {
                        Intent intent = new Intent(context, ExportEnrGroupActivity.class);
                        intent.putExtra("Book", currentBook);
                        intent.putExtra("Shelf", GridShelf.this);
                        context.startActivity(intent);
                        //BookExportDialog exportDialog=new BookExportDialog(context,currentBook);
                       // exportDialog.exportDialog();
                       /* ExportIntent = new Intent(context, ExportActivity.class);
                        ExportIntent.putExtra("Book", currentBook);
                        // ExportIntent.putExtra("UserCredentials", context.SlideMenuActivity.userCredentials);
                        context.startActivity(ExportIntent);*/

                        }
                    }else{
                        UserFunctions.DisplayAlertDialog(context, R.string.subs_msg, R.string.subs_redeem);
                    }
                }

                break;
            }
            case R.id.imageButton3: {
                String query="select * from malzamah";
                malzamahBookList=db.getMalzamahBooks(query);
                int position = (Integer) view.getTag();
                Book delBook = (Book)bookListArray.get(position);
                boolean  exist = false;
                if(delBook.is_bStoreBook()) {
                    for (int i = 0; i < malzamahBookList.size(); i++) {
                        exist = false;
                        String malzamahBook = malzamahBookList.get(i);
                        String[] split = malzamahBook.split("##");
                        int bookId = Integer.parseInt(split[0]);
                        int PageNo = Integer.parseInt(split[1]);
                        String sourceStoreId = split[2];
                        String htmlPath = split[3];
                        String thumbPath = split[4];
                        if (sourceStoreId .equals(delBook.get_bStoreID())) {
                            exist = true;
                            break;
                        }
                    }
                }
                if(exist) {
                    checkingMalzamahBook(position,delBook, R.string.delete_storebook,exist);
                }else {
                    checkingMalzamahBook(position, delBook,R.string.deletebook,exist);

                }
                break;
            }
            case R.id.imageButt_cat: {
                String query="select * from malzamah";
                malzamahBookList=db.getMalzamahBooks(query);
                int position = (Integer) view.getTag();
                Book delBook = (Book)bookListArray.get(position);
                boolean  exist = false;
                if(delBook.is_bStoreBook()) {
                    for (int i = 0; i < malzamahBookList.size(); i++) {
                        exist = false;
                        String malzamahBook = malzamahBookList.get(i);
                        String[] split = malzamahBook.split("##");
                        int bookId = Integer.parseInt(split[0]);
                        int PageNo = Integer.parseInt(split[1]);
                        String sourceStoreId = split[2];
                        String htmlPath = split[3];
                        //int totalPages = Integer.parseInt(c.getString(5));
                        //boolean storeBook = Boolean.parseBoolean(c.getString(9));
                        String thumbPath = split[4];
                        if (sourceStoreId .equals(delBook.get_bStoreID())) {
                            exist = true;
                            break;
                        }
                    }
                }
                if(exist) {
                    checkingMalzamahBook(position,delBook, R.string.delete_storebook,exist);
                }else {
                    checkingMalzamahBook(position, delBook,R.string.deletebook,exist);

                }
                break;
            }

            default:
                break;
        }
    }

    private class CategoryListAdapter extends BaseAdapter {

        private ArrayList<Category> catList;

        private CategoryListAdapter(ArrayList<Category> catList) {
            this.catList = catList;
        }

        @Override
        public int getCount() {
            return catList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
                view = context.getLayoutInflater().inflate(R.layout.inflate_text_view, null);
            }
            Category category = catList.get(position);
            view.setTag(category);

            TextView txtView = (TextView) view.findViewById(R.id.textView1);
            txtView.setText(category.getCategoryName());
            txtView.setTextColor(Color.BLACK);
            // setTexttoCategoryName(category,txtView);
            return view;
        }

    }

    /**
     * @author Change if books in editmode to true else to false
     */
    public boolean  editBooks() {
        if (isInEditMode) {
            isInEditMode = false;
//            context.btnEditBook.setText(R.string.edit);
        } else {
            isInEditMode = true;

//            context.btnEditBook.setText(R.string.done);
        }
        return isInEditMode;
    }

    /**
     * @author update the info view values to database and to Objects
     */
   /* private void updateInfoViewComponentsTodbAndObjects() {
        String bookTitle = etBookTitle.getText().toString();
        String bookAuthor = etBookAuthor.getText().toString();
        currentBook.setBookTitle(bookTitle);
        currentBook.setBookAuthorName(bookAuthor);
        db.executeQuery("update books set Title='" + bookTitle + "',Author='" + bookAuthor + "', oriantation='" + currentBook.getBookOrientation() + "' where BID ='" + currentBook.getBookID() + "'");
    }*/

    @Override
    public void popoverViewWillShow(PopoverView view) {

    }

    @Override
    public void popoverViewDidShow(PopoverView view) {

    }

    @Override
    public void popoverViewWillDismiss(PopoverView view) {
        Category category = (Category) et_cat.getTag();
        String catText = et_cat.getText().toString();
        if (!category.getCategoryName().equals(catText)) {
            category.setCategoryName(catText);
            context.gridView.invalidateViews();
            String query = "update tblCategory set CName='" + catText + "' where CID='" + category.getCategoryId() + "'";
            context.db.executeQuery(query);
        }
    }

    @Override
    public void popoverViewDidDismiss(PopoverView view) {

    }


    public void setTexttoCategoryName(Category category, TextView txtView){
        if (category.getCategoryId()==1){
            txtView.setId(category.getCategoryId());
            txtView.setText(R.string.books);

        }else if (category.getCategoryId()==2){
            txtView.setId(category.getCategoryId());
            txtView.setText(R.string.created_books);

        }else if (category.getCategoryId()==3){
            txtView.setId(category.getCategoryId());
            txtView.setText(R.string.Quizzes);

        }else if (category.getCategoryId()==4){
            txtView.setId(category.getCategoryId());
            txtView.setText(R.string.elesson);

        }else if (category.getCategoryId()==5){
            txtView.setId(category.getCategoryId());
            txtView.setText(R.string.rzooom);

        }else if (category.getCategoryId()==Globals.mindMapCatId){
            txtView.setText(R.string.shelf_Mindmap);
            txtView.setId(category.getCategoryId());
        }else if(category.getCategoryId()==Globals.myMediaCatId){
            txtView.setText(R.string.my_media);
            txtView.setId(category.getCategoryId());
        }
        else if (category.getCategoryName().equals("New Category")){
            txtView.setId(category.getCategoryId());
            txtView.setText(R.string.new_category);
        }else if (category.getCategoryName().equals("Add category")){
            txtView.setText(R.string.add_category);
        }else {
            txtView.setId(category.getCategoryId());
            txtView.setText(category.getCategoryName());
        }
    }

    public void editTitle(Category category){
        if (category.getCategoryId() == 1 || category.getCategoryId() == 2 || category.getCategoryId() == 3 || category.getCategoryId() == 4 || category.getCategoryId() == 5 || category.getCategoryId() == 6 || category.getCategoryId() == 7|| category.getCategoryId() == 8) {
            UserFunctions.DisplayAlertDialog(context, "You cannot edit this category title", R.string.edit_category);
        } else {
            View view1 = context.getLayoutInflater().inflate(R.layout.category_property_listview, null);
            AlertDialog.Builder popView = new AlertDialog.Builder(context);
            popView.setView(view1);
            popView.setCancelable(false);
            popView.setTitle("Edit title");
            et_cat = (EditText) view1.findViewById(R.id.editText1);
            et_cat.setText(category.getCategoryName());
//                    setTexttoCategoryName(category,txtTitle);
            et_cat.setTag(category);
            if (Globals.isLimitedVersion()) {
                if (category.getCategoryId() == 1 || category.getCategoryId() == 2 || category.getCategoryId() == 3 || category.getCategoryId() == 4 || category.getCategoryId() == 5 || category.getCategoryId() ==7) {
                    et_cat.setKeyListener(null);
                }
            } else if (!Globals.isLimitedVersion()) {
                if (category.getCategoryId() == 1 || category.getCategoryId() == 2 || category.getCategoryId() == 3 || category.getCategoryId() == 4 || category.getCategoryId() == 5 || category.getCategoryId() == 6 || category.getCategoryId() ==7 || category.getCategoryId() ==8) {
                    et_cat.setKeyListener(null);
                }
            }
            popView.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Category category = (Category) et_cat.getTag();
                    String catText = et_cat.getText().toString();
                    if (!category.getCategoryName().equals(catText)) {
                        category.setCategoryName(catText);
                        context.gridView.invalidateViews();
                        String query = "update tblCategory set CName='" + catText + "' where CID='" + category.getCategoryId() + "'";
                        context.db.executeQuery(query);
                    }
                    dialog.cancel();
                }
            });
            popView.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = popView.create();
            alert.show();
        }
    }

    public void UpdatingBookListArray(int Id,Book book, boolean invalidate){
        for(int i=0;i<bookListArray.size();i++) {
            Book bk = (Book) bookListArray.get(i);
            if(bk.getBookID()==book.getBookID()){
                bookListArray.remove(i);
                bk.set_groupId(Id);
                bookListArray.add(bk);
                break;
            }
        }
        if(invalidate) {
            context.gridView.invalidateViews();
        }
    }

    public ArrayList<GroupBooks> BooksListInsideGroup(int categoryId, GroupBooks selectedGrp){
        ArrayList<GroupBooks> childGrp = new ArrayList<GroupBooks>();

        for (int i = 0; i < bookListArray.size(); i++) {
            Book book = (Book) bookListArray.get(i);
            book.setIdPos(i);
            if (i == 0) {
                GroupBooks arrowBack = new GroupBooks();
                //ChildDetails.setBook(grp.getBook());
                arrowBack.setIsDownloadCompleted("downloaded");
                arrowBack.setGroupId(selectedGrp.getGroupId());
                arrowBack.setCategoryId(selectedGrp.getCategoryId());
                arrowBack.setGroupName("Back");
                //ChildDetails.setBookId(childgrp.getBookId());
                arrowBack.setPosition(childGrp.size());
                childGrp.add(arrowBack);
            }

            if (book.getbCategoryId() == categoryId && book.get_groupId() == selectedGrp.getGroupId()) {
                // categoryBooksList.add(book);
                GroupBooks grp = new GroupBooks();
                grp.setCategoryId(categoryId);
                grp.setGroupId(book.get_groupId());
                grp.setBookId(book.getBookID());
                grp.setIsDownloadCompleted(book.getIsDownloadCompleted());
                grp.setPosition(i);
                grp.setBook(book);
                childGrp.add(grp);
            }
        }
        return childGrp;
    }
    private boolean checkGrpSelectedForCategory(int categoryID){
        if (selectedGrpList.size()>0){
            for (int i=0;i<selectedGrpList.size();i++){
                GroupBooks grpBooks = selectedGrpList.get(i);
                if (grpBooks.getCategoryId()==categoryID){
                    return true;
                }
            }
        }
        return false;
    }
    private GroupBooks getSeletedGrp(int categoryId){
        if (selectedGrpList.size()>0){
            for (int i=0;i<selectedGrpList.size();i++){
                GroupBooks grpBooks = selectedGrpList.get(i);
                if (grpBooks.getCategoryId()==categoryId){
                    return grpBooks;
                }
            }
        }
        return null;
    }
    private boolean checkIsEditMode(int shelfPos){
        for (int i1=0;i1<editModeList.size();i1++){
            int value = editModeList.get(i1);
            if (value==shelfPos){
                return true;
            }
        }
        return false;
    }

    private void infoPopupForNormalBook(View view){
        int position = (Integer) view.getTag();
        currentBook = (Book) bookListArray.get(position);
        final Dialog info_dialog = new Dialog(context);
        info_dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        info_dialog.setContentView(context.getLayoutInflater().inflate(R.layout.popover_info_view, null));
        if (Globals.isTablet()) {
            info_dialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.5));
        }else{
            info_dialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        }
        info_dialog.getWindow().getAttributes().windowAnimations=R.style.DialogAnimation;
        SegmentedRadioButton segmentedRadioGroup = (SegmentedRadioButton)info_dialog.findViewById(R.id.segment_text);
        RadioButton ltr_btn = (RadioButton) info_dialog.findViewById(R.id.btn_ltr);
        RadioButton rtl_btn = (RadioButton) info_dialog.findViewById(R.id.btn_rtl);
        if (currentBook.getBookDirection().equals("ltr")){
            ltr_btn.setChecked(true);
        }else{
            rtl_btn.setChecked(true);
        }
        segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.btn_ltr) {
                    currentBook.setBookDirection("ltr");
                    String updateDirectionQuery = "update books set bookDirection='ltr' where BID='" + currentBook.getBookID() + "'";
                    db.executeQuery(updateDirectionQuery);
                } else if (checkedId == R.id.btn_rtl) {
                    currentBook.setBookDirection("rtl");
                    String updateDirectionQuery = "update books set bookDirection='rtl' where BID='" + currentBook.getBookID() + "'";
                    db.executeQuery(updateDirectionQuery);
                }
            }
        });
        CircleButton back_btn = (CircleButton) info_dialog.findViewById(R.id.btn_back_bookmodel);
        back_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                info_dialog.dismiss();
            }
        });
        final EditText etBookTitle = (EditText) info_dialog.findViewById(R.id.bookname);
        etBookTitle.setText(currentBook.getBookTitle());
        final EditText etBookAuthor = (EditText) info_dialog.findViewById(R.id.authorname);
        etBookAuthor.setText(currentBook.getBookAuthorName());
        final ListView listView = (ListView) info_dialog.findViewById(R.id.listView1);
        //if (currentBook.getBookOrientation() == Globals.portrait) {
        db = DatabaseHandler.getInstance(context);
        templatesPortraitListArray =  db.getAllTemplatesListBasedOnOrientation("portrait");
        listView.setAdapter(new TemplatesListAdapter(context, templatesPortraitListArray));
//			} else {
//				listView.setAdapter(new TemplatesListAdapter(context, templatesLandscapeListArray));
//			}
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view,
                                    int position, long arg3) {
                info_dialog.dismiss();
                userFunction.replaceExistingTemplates(Globals.TARGET_BASE_TEMPATES_PATH + view.getId(), Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.getBookID() + "/FreeFiles");
                currentBook.set_bTemplateId(view.getId());
                Templates template = getTemplates(view.getId(), currentBook.getBookOrientation(),context);
                currentBook.setTemplate(template);
                db.executeQuery("update books set template='" + view.getId() + "' where BID='" + currentBook.getBookID() + "'");
                context.gridView.invalidateViews();
            }
        });
        info_dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!etBookTitle.getText().toString().equals("")&&!etBookAuthor.getText().toString().equals("")) {
                    if (!etBookTitle.getText().toString().equals(currentBook.getBookTitle())) {
                        currentBook.setBookTitle(etBookTitle.getText().toString());
                        String updateTitleQuery = "update books set Title='" + etBookTitle.getText().toString() + "' where BID='" + currentBook.getBookID() + "'";
                        db.executeQuery(updateTitleQuery);
                    }
                    if (!etBookAuthor.getText().toString().equals(currentBook.getBookAuthorName())) {
                        currentBook.setBookAuthorName(etBookAuthor.getText().toString());
                        String updateAuthorQuery = "update books set Author='" + etBookAuthor.getText().toString() + "' where BID='" + currentBook.getBookID() + "'";
                        db.executeQuery(updateAuthorQuery);
                    }
                    context.gridView.invalidateViews();
                }
            }
        });
        info_dialog.show();
    }
    private void infoPopupForStoreBook(Book book){
        final Dialog info_dialog = new Dialog(context);
        info_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.semi_transparent)));
        info_dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        info_dialog.setContentView(context.getLayoutInflater().inflate(R.layout.bookmodal_neww, null));
        if (Globals.isTablet()) {
            info_dialog.getWindow().setLayout((int) (Globals.getDeviceWidth() / 1.2), (int) (Globals.getDeviceHeight() / 1.7));
        }else{
            info_dialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        }
        info_dialog.getWindow().getAttributes().windowAnimations=R.style.DialogAnimation;
        TextView titleLabel = (TextView) info_dialog.findViewById(R.id.book_Label);
        titleLabel.setText(book.getBookTitle());
        TextView languageLabel = (TextView) info_dialog.findViewById(R.id.langLabel);
        String langContent = book.getBookLangugae();
        if (langContent.equals("null")) {
            langContent = "-";
        }
        languageLabel.setText(langContent);
        TextView descriptionLabel = (TextView) info_dialog.findViewById(R.id.desLabel);
        descriptionLabel.setText(book.get_bDescription());
        TextView authorLabel = (TextView) info_dialog.findViewById(R.id.authorLabel);
        authorLabel.setText(book.getBookAuthorName());
        TextView totalPagesLabel = (TextView) info_dialog.findViewById(R.id.totalPagesLabel);
        totalPagesLabel.setText(String.valueOf(book.getTotalPages()));
        TextView txtSize = (TextView) info_dialog.findViewById(R.id.txtSize);
        txtSize.setVisibility(View.GONE);
        TextView txtCategory = (TextView) info_dialog.findViewById(R.id.txtCategory);
        //TextView priceLabel =  (TextView) bookModalPopwindow.findViewById(R.id.priceLabel);
        ImageView bookImage = (ImageView) info_dialog.findViewById(R.id.imageView1);
        int BID = book.getBookID();
        String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/pageBG_1.png";
        if (!new File(bookCardImagePath).exists()) {
            bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH + BID + "/FreeFiles/card.png";
        }
        Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
        bookImage.setImageBitmap(bitmap);
        CircleButton back_btn = (CircleButton) info_dialog.findViewById(R.id.btn_back_bookmodel);
        back_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                info_dialog.dismiss();
            }
        });
        Button download = (Button) info_dialog.findViewById(R.id.downloadBtn);
        download.setVisibility(View.GONE);
        info_dialog.show();
    }

    private void checkingMalzamahBook(final int position, final Book delBook, int title,boolean status){

      /*  Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //  int position = (Integer) view.getTag();
                //  Book delBook = (Book) bookListArray.get(position);
                deleteBook(position);
                boolean emptyCategory = true;
                for (int i = 0; i < bookListArray.size(); i++) {
                    Book book = (Book) bookListArray.get(i);
                    if (delBook.getbCategoryId() == book.getbCategoryId()) {
                        emptyCategory = false;
                        break;
                    }
                }
                if (emptyCategory){
                    if ( Globals.isTablet() ? delBook.getbCategoryId()!=2 : delBook.getbCategoryId()!=1) {
                        db.executeQuery("update tblCategory set isHidden='true' where CID='" + delBook.getbCategoryId() + "'and isHidden='false'");
                        reloadCategoryListArray();
                    }
                }
                context.gridView.invalidateViews();

            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setTitle(title);
        builder.setMessage(R.string.suredelete);
        builder.show();  */


        Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
               // int position = (Integer) view.getTag();
                Book delBook = (Book)bookListArray.get(position);
                deleteBook(position);
                if(delBook.get_groupId()!=0) {
                    int count = db.getBookCount(delBook.get_groupId());
                    if(count==0){
                        db.executeQuery("delete from groupBooks where ID='" + delBook.get_groupId() + "'");
                        groupBook=db.getAllGroupList();
                        for (int i1 = 0;i1<selectedGrpList.size();i1++){
                            GroupBooks grpBooks = selectedGrpList.get(i1);
                            if (grpBooks.getCategoryId()==delBook.getbCategoryId()){
                                selectedGrpList.remove(i1);
                                break;
                            }
                        }
                    }
                }
                boolean emptyCategory = true;
                for (int i = 0; i < bookListArray.size(); i++) {
                    Book book = (Book) bookListArray.get(i);
                    if (delBook.getbCategoryId() == book.getbCategoryId()) {
                        emptyCategory = false;
                        break;
                    }
                }
                if (emptyCategory){
                    if ( Globals.isTablet() ? delBook.getbCategoryId()!=2 : delBook.getbCategoryId()!=1) {
                        db.executeQuery("update tblCategory set isHidden='true' where CID='" + delBook.getbCategoryId() + "'and isHidden='false'");
                        reloadCategoryListArray();
                    }
                }
                context.gridView.invalidateViews();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setTitle(title);
        if(!status) {
            builder.setMessage(R.string.suredelete);
        }
        builder.show();

    }
    private class savingScrennShotForPDF extends AsyncTask<Void,Integer,Void>{
        Book downLoadBook;
        DownloadBackground textCircularProgressBar;

        public savingScrennShotForPDF(Book book){
            downLoadBook = book;
            textCircularProgressBar = DownloadBackground.getInstance();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String sourceDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + downLoadBook.get_bStoreID() + "Book/Card.gif";
            String pdfName = null;
            File srcDir = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+downLoadBook.get_bStoreID()+"Book");
            String[] path = srcDir.list();
            for(String file : path) {
                String fromPath = srcDir + "/" + file;
                String[] str = file.split("\\.");
                if(file.contains(".pdf")){
                    pdfName=fromPath;
                    break;
                }
            }
            //   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                GenerateThumbnailsPdf thumbnailsPdf = new GenerateThumbnailsPdf(context, pdfName,null);

                for (int i = 0; i < thumbnailsPdf.countPages(); i++) {
                    publishProgress((int) ((i * 100) / thumbnailsPdf.countPages()));
                    String screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + downLoadBook.getBookID() + "/thumbnail_" + 1 + ".png";
                     if (!new File(screenShot).exists()) {
                         Bitmap b = thumbnailsPdf.createThumbnail(Globals.getDeviceWidth(), Globals.getDeviceHeight(), i);
                         if (context instanceof MainActivity) {
                             savingImages(b, downLoadBook, i, thumbnailsPdf.countPages(), textCircularProgressBar.getTextCircularProgressBar());
                         } else {
                             savingImages(b, downLoadBook, i, thumbnailsPdf.countPages(), null);
                         }
                      }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (textCircularProgressBar.getTextCircularProgressBar()!=null){
                textCircularProgressBar.getTextCircularProgressBar().setProgress(values[0]);
                textCircularProgressBar.getTextCircularProgressBar().setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            db.executeQuery("update books set isDownloadedCompleted='downloaded'where BID ='" + downLoadBook.getBookID() + "'");
            if (textCircularProgressBar.getTextCircularProgressBar()!=null){
                textCircularProgressBar.getTextCircularProgressBar().setVisibility(View.INVISIBLE);
            }
            textCircularProgressBar.getList().remove(0);
            if (textCircularProgressBar.getList().size() > 0) {
                final Book book = textCircularProgressBar.getList().get(0);
                if (!book.is_bStoreBook() && book.get_bStoreID()!=null &&book.get_bStoreID().contains("P")){
                    new savingScrennShotForPDF(book).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                textCircularProgressBar.setDownloadTaskRunning(true);
            }else{
                textCircularProgressBar.setDownloadTaskRunning(false);
            }
            ((MainActivity) context).loadGridView();
        }
    }
    public void savingImages(Bitmap b, Book book, final int page, final int pageCount, final TextCircularProgressBar processDialog) {
        String screenShot;
        if(processDialog!=null) {

        }
        if (page == 0) {
            screenShot = Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID() + "/FreeFiles/card.png";
            Bitmap  bitmap = createScaledBitmap(b,Globals.getDeviceIndependentPixels(200, context), Globals.getDeviceIndependentPixels(300, context), true);
            //pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

            if(!new File(screenShot).exists()) {
                takeScreenShot(bitmap, screenShot);
            }
            int pageNo = page + 1;
            screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookID() + "/thumbnail_" + pageNo + ".png";
            takeScreenShot(bitmap,screenShot);
            String front_thumb_path = Globals.TARGET_BASE_BOOKS_DIR_PATH+book.getBookID()+"/FreeFiles/frontThumb.png";

            takeScreenShot(bitmap,front_thumb_path);
            // Bitmap pageScreenShot= savingPageImages(f,page);
            takeScreenShot(b, Globals.TARGET_BASE_BOOKS_DIR_PATH +book.getBookID()+ "/FreeFiles/front_P.png");

        } else if (page < pageCount- 1) {
            int pageNo = page + 1;
            screenShot = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookID() + "/thumbnail_" + pageNo + ".png";

            Bitmap  bitmap = createScaledBitmap(b,Globals.getDeviceIndependentPixels(130, context), Globals.getDeviceIndependentPixels(170, context), true);
            //pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            if(!new File(screenShot).exists()) {
                takeScreenShot(bitmap, screenShot);
                //Bitmap pageScreenShot= savingPageImages(f,page);
                takeScreenShot(b, Globals.TARGET_BASE_BOOKS_DIR_PATH +book.getBookID()+ "/FreeFiles/"+pageNo+".png");
            }
        } else {
            screenShot = Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID() + "/FreeFiles/back_P.png";

            Bitmap  bitmap = createScaledBitmap(b,Globals.getDeviceIndependentPixels(130, context), Globals.getDeviceIndependentPixels(170, context), true);
            // pr.openPage(page).render(b, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            if(!new File(screenShot).exists()) {
                takeScreenShot(bitmap, screenShot);
            }
            String screenShot1 = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + book.getBookID() + "/thumbnail_end.png";
            takeScreenShot(b,screenShot1);
        }
    }
    private void takeScreenShot(Bitmap bit,String imagePath){
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imagePath);
            bit.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();

        } catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
