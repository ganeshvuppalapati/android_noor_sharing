package com.semanoor.source_sboookauthor;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Krishna on 06-10-2016.
 */
public class MyContacts  implements Parcelable {
    private Context context;
    private int id;
    private String userName;
    private String emailId;
    private String loggedInUserId;
    private int groupsID;
    private int Enabled;
    private boolean isSelected;
    public String isOnline;
    public String isOnline() {
        return isOnline;
    }

    public void setOnline(String online) {
        isOnline = online;
    }



    public MyContacts(Context _context) {
        this.context = _context;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }

    public void setLoggedInUserId(String loggedInUserId) {
        this.loggedInUserId = loggedInUserId;
    }

    public int getGroupsID() {
        return groupsID;
    }

    public void setGroupsID(int groupsID) {
        this.groupsID = groupsID;
    }

    public int getEnabled() {
        return Enabled;
    }

    public void setEnabled(int enabled) {
        Enabled = enabled;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean groupSelected) {
        this.isSelected = groupSelected;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(id);
        dest.writeString(userName);
        dest.writeString(emailId);
        dest.writeString(loggedInUserId);
        dest.writeInt(groupsID);
        dest.writeString(isOnline);
    }


    public MyContacts(Parcel in){
        this.id = in.readInt();
        this.userName = in.readString();
        this.emailId = in.readString();
        this.loggedInUserId = in.readString();
        this.groupsID = in.readInt();
        this.isOnline = in.readString();
        // this.isSelected=in.readBooleanArray();

    }

    public static final Creator<MyContacts> CREATOR = new Creator<MyContacts>() {
        @Override
        public MyContacts createFromParcel(Parcel in) {
            return new MyContacts(in);
        }

        @Override
        public MyContacts[] newArray(int size) {
            return new MyContacts[size];
        }
    };

}