package com.semanoor.source_sboookauthor;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;


import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ActionMode.Callback;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.ViewParent;
import android.webkit.WebView;

import com.semanoor.manahij.R;

public class HelpWebView extends WebView {
    ActionMode.Callback mActionModeCallback;
    Context context;
    public ActionMode actionMode;
    public Callback callBack;
    private GestureDetector gestureDetector;
    public CustomWebRelativeLayout layout;
    // WebView web;
    private AtomicBoolean mPreventAction = new AtomicBoolean(false);
    private AtomicLong mPreventActionTime = new AtomicLong(0);
    String selText;
    //ActionBarCallBack mactionmode;
    String selectedText;

    public HelpWebView(Context context) {
        super(context);
        gestureDetector = new GestureDetector(context, new GestureListener());
    }

    public HelpWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        gestureDetector = new GestureDetector(context, new GestureListener());
    }

    public HelpWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        gestureDetector = new GestureDetector(context, new GestureListener());
    }


    public HelpWebView(Context context, AttributeSet attrs, int defStyle, boolean privateBrowsing) {
        super(context, attrs, defStyle, privateBrowsing);
        gestureDetector = new GestureDetector(context, new GestureListener());
    }


    public int getContentHeightt() {
        return this.computeVerticalScrollRange();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int index = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
        int pointId = event.getPointerId(index);

        // just use one(first) finger, prevent double tap with two and more fingers
        if (pointId == 0) {
            gestureDetector.onTouchEvent(event);

            if (mPreventAction.get()) {
                if (System.currentTimeMillis() - mPreventActionTime.get() > ViewConfiguration.getDoubleTapTimeout()) {
                    mPreventAction.set(false);
                } else {
                    return true;
                }
            }
            return super.onTouchEvent(event);
        } else {
            return true;
        }
    }


    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            mPreventAction.set(true);
            mPreventActionTime.set(System.currentTimeMillis());
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            mPreventAction.set(true);
            mPreventActionTime.set(System.currentTimeMillis());
            return true;
        }

        /*@Override
        public boolean onSingleTapUp(MotionEvent e) {
            //System.out.println("hello");
            if(CustomWebRelativeLayout.context.rlTabsLayout.getVisibility()== View.VISIBLE) {
                if (CustomWebRelativeLayout.context.enrichmentTabListArray.size() > 0 ) {
                    CustomWebRelativeLayout.context.rlTabsLayout.setVisibility(View.GONE);
                } else {
                    CustomWebRelativeLayout.context.rlTabsLayout.setVisibility(View.GONE);
                }
            }else{
                if (CustomWebRelativeLayout.context.enrichmentTabListArray.size() > 0) {
                    CustomWebRelativeLayout.context.rlTabsLayout.setVisibility(View.VISIBLE);
                } else {
                    CustomWebRelativeLayout.context.rlTabsLayout.setVisibility(View.GONE);
                }

            }
            if(CustomWebRelativeLayout.context.bkmarkLayout.getVisibility()==View.VISIBLE) {
                if (CustomWebRelativeLayout.context.enrichmentbkmrkList.size() > 0) {
                    CustomWebRelativeLayout.context.bkmarkLayout.setVisibility(View.GONE);
                }else{
                    CustomWebRelativeLayout.context.bkmarkLayout.setVisibility(View.GONE);
                }
            }else{
                if (CustomWebRelativeLayout.context.enrichmentbkmrkList.size() > 0) {
                    CustomWebRelativeLayout.context.bkmarkLayout.setVisibility(View.VISIBLE);
                } else {
                    CustomWebRelativeLayout.context.bkmarkLayout.setVisibility(View.GONE);
                }

            }
            return true;
        }*/
    }

    @Override
    public ActionMode startActionMode(Callback callback) {
        ViewParent parent = getParent();
        if (parent instanceof CustomWebRelativeLayout){
            String scaleToFit = ((CustomWebRelativeLayout)parent).getObjScalePageToFit();
            if (scaleToFit.equals("yes")){
                if (parent==null){
                    return null;
                }
                mActionModeCallback = new CustomActionModeCallback();
                return super.startActionMode(this.mActionModeCallback);
            }
            if (scaleToFit.equals("no")){
                callBack=callback;
            }
        }
        return super.startActionMode(callback);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public ActionMode startActionMode(Callback callback, int type) {
        ViewParent parent = getParent();
        if (parent instanceof CustomWebRelativeLayout){
           String scaleToFit = ((CustomWebRelativeLayout)parent).getObjScalePageToFit();
            if (scaleToFit.equals("yes")){
                if (parent==null){
                    return null;
                }
                mActionModeCallback = new CustomMarshActionModeCallback();
                return parent.startActionModeForChild(this,mActionModeCallback,type);
            }
            if (scaleToFit.equals("no")){
                callBack=callback;
            }
        }
        return super.startActionMode(callback,type);

        //return super.startActionMode(mActionModecallback, ActionMode.TYPE_PRIMARY);
    }

    class CustomActionModeCallback implements ActionMode.Callback {
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            clearFocus();
            actionMode = null;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            actionMode = mode;
            createActionMode(mode, menu);
            return true;
        }


        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            return true;
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    class CustomMarshActionModeCallback extends ActionMode.Callback2{

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            clearFocus();
            actionMode = null;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            actionMode = mode;
            createActionMode(mode, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, final Menu menu) {
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            return true;
        }

        @Override
        public void onGetContentRect(ActionMode mode, View view, Rect outRect) {
            super.onGetContentRect(mode, view, outRect);
        }
    }

    private void createActionMode(ActionMode mode, Menu menu) {
        MenuItem menu1 = menu.add(0, 1, 0, R.string.copy);
        menu1.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS | menu1.SHOW_AS_ACTION_WITH_TEXT);
        menu1.setOnMenuItemClickListener(new OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                CustomWebRelativeLayout.loadurl(HelpWebView.this);
                return false;
            }
        });

        MenuItem menu2 = menu.add(0, 1, 0, R.string.search);
        menu2.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS | menu2.SHOW_AS_ACTION_WITH_TEXT);
        menu2.setOnMenuItemClickListener(new OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                CustomWebRelativeLayout.SearchClicked = true;
                CustomWebRelativeLayout.loadurl(HelpWebView.this);
                //loadJSScript("javascript:getSelectedText();", webView);
                //System.out.println("Haiiiiiiiiiiiiii");
                return false;
            }


        });

        MenuItem menu3 = menu.add(0, 1, 0, R.string.mindmap);
        menu3.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS | menu2.SHOW_AS_ACTION_WITH_TEXT);
        menu3.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                CustomWebRelativeLayout.mindMapMenuClicked = true;
                CustomWebRelativeLayout.loadurl(HelpWebView.this);
                return false;
            }
        });
        MenuItem menu4 = menu.add(0, 1, 0, R.string.hyperlink);
        menu4.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS | menu4.SHOW_AS_ACTION_WITH_TEXT);
        menu4.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                CustomWebRelativeLayout.hyperLinkClicked = true;
                CustomWebRelativeLayout.loadurl(HelpWebView.this);

                return false;
            }
        });
    }

}



