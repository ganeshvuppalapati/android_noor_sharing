package com.semanoor.source_sboookauthor;

import android.util.Log;
import android.util.Xml;

import com.semanoor.manahij.Elesson;
import com.semanoor.manahij.RAdataobject;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by karthik on 01-10-2015.
 */
public class Statistics {
    Elesson elesson_Act;

    public Statistics(Elesson elesson) {
        elesson_Act = elesson;
    }
    /**
     *create Statistics XML
    */
    public String statisticsXml() {
        String data = "";
        File outputFile = new File(elesson_Act.currentbookpath + "Data/statistics.xml");
        try {
            outputFile.createNewFile();
            FileOutputStream fileos = new FileOutputStream(outputFile);
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);
            xmlSerializer.setFeature(
                    "http://xmlpull.org/v1/doc/features.html#indent-output",
                    true);
            xmlSerializer.startTag("", "Elesson");
            xmlSerializer.attribute("", "Title", elesson_Act.elessonBookTitle);
            xmlSerializer.attribute("", "LastTime", "");
            xmlSerializer.attribute("", "TotalPages", String.valueOf(elesson_Act.previewlist.size()));
            for (int i = 0; i < elesson_Act.previewlist.size(); i++) {
                RAdataobject dataObj = elesson_Act.previewlist.get(i);
                xmlSerializer.startTag("", "Page");
                xmlSerializer.attribute("", "IsPageViewed", "No");
                xmlSerializer.attribute("", "Title", dataObj.getName());
                xmlSerializer.attribute("", "TimeSpent", "0");
                xmlSerializer.attribute("", "PageID", dataObj.getPageId());
                xmlSerializer.attribute("","SequenceId", String.valueOf(i));
                xmlSerializer.attribute("","isDrawn","No");
                xmlSerializer.attribute("","isFavourite","No");
                xmlSerializer.attribute("","Notes","0");
                xmlSerializer.endTag("", "Page");
            }
            xmlSerializer.endDocument();
            writer.toString();
            data = writer.toString();
            fileos.write(writer.toString().getBytes());
        } catch (IOException e) {
            Log.e("IOException", "Exception in create new File(");
        }
        return data;
    }
    private String checkIsFavourite(String pageId){
        String favourite="No";
       for (int i=0;i<elesson_Act.favouritesArray.size();i++){
           HashMap<String, String> map = elesson_Act.favouritesArray.get(i);
           if (map.get("pageID").equals(pageId)){
               favourite="yes";
           }
       }
        return favourite;
    }
    private int Notes(String lessonId,String pageId){
        ArrayList<Integer> notesCount=new ArrayList<Integer>();
       for (int i=0;i<elesson_Act.db.getAllElessonNote(lessonId).size();i++){
           HashMap<String, String> map = elesson_Act.db.getAllElessonNote(lessonId).get(i);
           if (map.get("pageID").equals(pageId)){
               notesCount.add(i);
           }
       }
        return notesCount.size();
    }
    private String isDrawn(String pageId){
        String drawn="No";
        if (new File(elesson_Act.currentbookpath +pageId+".png").exists()){
            drawn="yes";
        }
        return drawn;
    }
     /**
      *Update Statistical datas pageviewed and timespent
      */
    public void readStatisticsXml() {
        try {
            String filepath = elesson_Act.currentbookpath + "Data/statistics.xml";
            String xmlContent = UserFunctions.readFileFromPath(new File(filepath));
            xmlContent = xmlContent.replace("\n", "");
            File xmlPath = new File(filepath);
            DocumentBuilderFactory document = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = document.newDocumentBuilder();
            Document document1 = builder.parse(xmlPath);
            Node node = document1.getElementsByTagName("Elesson").item(0);
            NodeList element = node.getChildNodes();
            for (int i = 0; i < element.getLength(); i++) {
                Node rootNode = element.item(i);
                NamedNodeMap nodeMap = rootNode.getAttributes();
                if (nodeMap != null) {
                    Node pageViewed = nodeMap.getNamedItem("IsPageViewed");
                    Node pageId = nodeMap.getNamedItem("PageID");
                    Node timeSpent = nodeMap.getNamedItem("TimeSpent");
                    Node favourite=nodeMap.getNamedItem("isFavourite");
                    Node notes=nodeMap.getNamedItem("Notes");
                    Node isDrawn=nodeMap.getNamedItem("isDrawn");
                    if (pageViewed.getNodeValue().equals("No") && pageId.getNodeValue().equals(elesson_Act.currentdataobj.getPageId())) {
                        pageViewed.setNodeValue("yes");
                    }
                    if (pageId.getNodeValue().equals(elesson_Act.currentdataobj.getPageId()) && timeSpent.getNodeValue().equals("0")) {
                        timeSpent.setNodeValue(String.valueOf(elesson_Act.elapsedTime));
                    } else if (!timeSpent.getNodeValue().equals("0") && pageId.getNodeValue().equals(elesson_Act.currentdataobj.getPageId())) {
                        int incrementTime = Integer.parseInt(timeSpent.getNodeValue());
                        timeSpent.setNodeValue(String.valueOf(incrementTime + elesson_Act.elapsedTime));
                    }
                    String fav=checkIsFavourite(pageId.getNodeValue());
                    favourite.setNodeValue(fav);

                    notes.setNodeValue(String.valueOf(Notes(elesson_Act.currentbook.get_bStoreID(),pageId.getNodeValue())));
                    isDrawn.setNodeValue(isDrawn(pageId.getNodeValue()));
                }
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document1);

            StreamResult streamResult = new StreamResult(new File(filepath));
            transformer.transform(domSource, streamResult);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

}
