package com.semanoor.source_sboookauthor;

import android.content.Context;

import com.semanoor.manahij.SharedUsers;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_manahij.Resources;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

public class WebService {

	private static String URL;
	private static String webServicePassword = "9EnFF7o4LB8C98cL9SMBS/WKwhv7ap6+";
	//private static String URL = "http://semanoor.com/sboook/sboookservice.asmx";
	private XMLParser Xmlparser;
	public String SignUpResponse = "";
	public boolean LogInResponse = false;
	public ArrayList<HashMap<String, String>> questionHashMapList;
	public ArrayList<HashMap<String, String>> groupHashMapList;
	public ArrayList<HashMap<String, String>> countryHashMapList;
	public ArrayList<HashMap<String, String>> universityHashMapList;
	public ArrayList<HashMap<String, String>> categoryHashMapList;
	public ArrayList<HashMap<String, String>> bookStatusList;
	public ArrayList<HashMap<String, String>> noteList, linkList, mindmapList;
	public ArrayList<Enrichments> enrichmentList, downloadedEnrichmentList;
	public ArrayList<Resources> resourceList, downloadedResourceList;
	public ArrayList<Resources> sharedUsersList, downloadedsharedUsersList;
	public HashMap<String, String> enrichUserLoginHashMapDatails;
	public String KEY_COUNTRY_ID = "country_id", KEY_COUNTRY_EN_NAME = "country_en_name", KEY_COUNTRY_AR_NAME = "country_ar_name", KEY_COUNTRY_FLAG = "country_flag";
	public String KEY_UNIVERSITY_ID = "university_id", KEY_UNIVERSITY_EN_NAME = "university_en_name", KEY_UNIVERSITY_AR_NAME = "university_ar_name", KEY_UNIVERSITY_FLAG = "university_flag", KEY_UNIVERSITY_COUNTRY_ID = "univ_ctry_id";
	public String KEY_CATEGORY_ID = "category_id", KEY_CATEGORY_EN_NAME = "category_en_name", KEY_CATEGORY_AR_NAME = "category_ar_name";
	public String KEY_QUESTION_ID = "question_id", KEY_QUESTION_EN_NAME = "question_en_name", KEY_QUESTION_AR_NAME = "question_ar_name";
	public String KEY_GROUP_ID = "group_id", KEY_GROUP_EN_NAME = "group_en_name", KEY_GROUP_AR_NAME = "group_ar_name";
	public String KEY_BOOK_STATUS_TITLE = "book_title", KEY_BOOK_STATUS = "book_status";
	public String KEY_ENRICH_USER_ID = "enrich_user_id", KEY_ENRICH_USER_NAME = "enrich_user_name", KEY_ENRICH_USER_EMAIL = "enrich_user_email", KEY_ENRICH_USER_CANPUBLISH = "enrich_user_canpublish", KEY_ENRICH_USER_AUTHORIZE_DIRECT_PUBLISH_ID = "enrich_user_authorize_directPublishId";
	public String KEY_ENRICH_STATUS_ID = "enrich_status_id", KEY_ENRICH_ISPUBLISHED = "enrich_ispublished";
	public String KEY_NOTE_BOOK_ID = "note_book_id", KEY_NOTE_PAGE_NO = "note_page_no", KEY_NOTE_STEXT = "note_stext", KEY_NOTE_OCCURENCE = "note_occurence", KEY_NOTE_SDESC = "note_sdesc", KEY_NOTE_NPOS = "note_npos", KEY_NOTE_COLOR = "note_color", KEY_NOTE_PROCESS_STEXT = "note_process_stext", KEY_NOTE_ENRICHID = "note_enrich_id";
	public String KEY_LINK_PAGE_NO = "link_page_no", KEY_LINK_TITLE = "link_title", KEY_LINK_URL = "link_url";
	public String KEY_MI_PAGE_NO = "mi_page_no", KEY_MI_TITLE = "mi_title", KEY_MI_PATH_NAME = "mi_path", KEY_MI_CONTENT = "mi_content";
	public ArrayList<String> priceList;
	public String uploadBookResponse = "";
	public String enrichUploadBookResponse = "";
	public ArrayList<Integer> enrichedPageResponseServerIdList;
	public ArrayList<String> enrichedPageUpdateResponseResult;
	public String insertAddNewEbookIpadJunkResult = "";
	public String uploadJunkFileIpadResult = "";
	public ArrayList<HashMap<String, String>> enrichStatusHashMapList;
	public String newSignInResponse = "";
	public String newSignUpResponse = "";
	public boolean isLocalFileParsing = false;
	public int ResourceId;
	public boolean isForParsingEnrichments = false;
	ArrayList<SharedUsers> blockmaild;
	public String mediaCategoryResponseResult, importMediaResponseResult;

	public static String KEY_CENRICHMENTS = "CENRICHMENT", KEY_CEMESSAGE = "EMESSAGE", KEY_CECONTENT = "ECONTENT", KEY_CLOUDENRSELECTED = "ESELECTED";
	public ArrayList<HashMap<String, String>> cloudEnrList;
	public ArrayList<Integer> downloadedCloudEnrList;
    public String userGrpData;
	public ArrayList<String>readEnrichments;
	public ArrayList<String>readResources;
	public HashMap<String, String> enrichmentsList;

	public WebService(Context context, String webServiceURL) {
		Xmlparser = new XMLParser(context, this);
		this.URL = webServiceURL;
	}

	/**
	 * load password question from webservice to arraylist for both english, arabic and Id
	 */
	public void loadPwdQuestion() {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<GetPasswordQues xmlns=\"http://semanoor.com.sa/\"/>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/GetPasswordQues";

		String response = CallWebService(soapAction, soapEnvelope);

		questionHashMapList = new ArrayList<HashMap<String, String>>();

		Xmlparser.parseXml(response);
	}

	/**
	 * load groups from webservice to arraylist for both english, arabic and Id
	 */
	public void loadGroups() {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<GetGroups xmlns=\"http://semanoor.com.sa/\"/>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/GetGroups";

		String response = CallWebService(soapAction, soapEnvelope);

		groupHashMapList = new ArrayList<HashMap<String, String>>();

		Xmlparser.parseXml(response);
	}

	/**
	 * Call Web Service
	 *
	 * @param soapAction
	 * @param envelope
	 */

	String CallOldWebService(String soapAction, String envelope)
	{
		final DefaultHttpClient httpClient=new DefaultHttpClient();

		// request parameters
		HttpParams params = httpClient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, 10000);
		HttpConnectionParams.setSoTimeout(params, 15000);
		//HttpConnectionParams.setSoTimeout(params, 7000);

		// set parameter
		HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

		// POST the envelope
		HttpPost httppost = new HttpPost(URL);
		// add headers
		httppost.setHeader("SOAPAction", soapAction);
		httppost.setHeader("Content-Type", "text/xml; charset=utf-8");

		String responseString = "";
		try {
			// the entity holds the request
			HttpEntity entity = new StringEntity(envelope, HTTP.UTF_8);
			httppost.setEntity(entity);

			// Response handler
			ResponseHandler<String> rh = new ResponseHandler<String>() {
				// invoked when client receives response
				public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
					// get response entity
					HttpEntity entity = response.getEntity();

					// read the response as byte array
					StringBuffer out = new StringBuffer();
					byte[] b = EntityUtils.toByteArray(entity);

					// write the response byte array to a string buffer
					out.append(new String(b, 0, b.length));
					return out.toString();
				}
			};
			responseString = httpClient.execute(httppost, rh);
		} catch (SocketException e) {
			System.out.println("Exception:" + e);
		} catch (IndexOutOfBoundsException e) {
		} catch (RuntimeException e) {
			System.out.println("Exception:" + e);
		} catch (Exception e) {
			System.out.println("Exception:" + e);
		}

		// close the connection
		httpClient.getConnectionManager().shutdown();

		responseString = responseString.replace("&lt;?xml version='1.0' encoding='UTF-8'?&gt;", "").replace("&lt;", "<").replace("&gt;", ">").replace("<?xml version='1.0' standalone='yes' ?>", "").replace("<?xml version=\"1.0\" encoding=\"utf-8\" ?>", "").replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
		////System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+responseString);
		if (!responseString.contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>")) {
			responseString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + responseString;
		}
		return responseString;
	}

	private String CallWebService(String sOAPAction, String eNVELOPE) {
		HttpURLConnection urlConnection = null;
		String returnResponse = "";
		try {
			URL urlToRequest = new URL(URL);

			urlConnection = (HttpURLConnection) urlToRequest.openConnection();
			urlConnection.setConnectTimeout(10000);
			urlConnection.setReadTimeout(25000);
			urlConnection.setRequestMethod("POST");
			urlConnection.setUseCaches(false);
			urlConnection.setDoInput(true);
			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-type", "text/xml; charset=utf-8");
			urlConnection.setRequestProperty("SOAPAction", sOAPAction);

			OutputStream out = urlConnection.getOutputStream();
			out.write(eNVELOPE.getBytes());
			int statusCode = urlConnection.getResponseCode();
			if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

			} else if (statusCode != HttpURLConnection.HTTP_OK) {

			}

			InputStream in = urlConnection.getInputStream();
			returnResponse = UserFunctions.convertStreamToString(in);
			if (returnResponse == null && returnResponse.equals("")) {
				return returnResponse;
			}
			returnResponse = returnResponse.replace("&lt;?xml version='1.0' encoding='UTF-8'?&gt;", "").replace("&lt;", "<").replace("&gt;", ">").replace("<?xml version='1.0' standalone='yes' ?>", "").replace("<?xml version=\"1.0\" encoding=\"utf-8\" ?>", "").replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
			if (!returnResponse.contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>")) {
				returnResponse = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + returnResponse;
			}
		} catch (MalformedURLException e) {

		} catch (SocketTimeoutException e) {

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return returnResponse;
	}

	/**
	 * Makes the new user to register in the domain
	 *
	 * @param name
	 * @param forgotques_id
	 * @param password
	 * @param answer
	 * @param fullname
	 * @param emailid
	 * @param DOB
	 * @param gender_id
	 * @param aca_level
	 * @param groupId
	 * @param address
	 * @param city
	 * @param country
	 * @param state
	 * @param pincode
	 * @param phoneno
	 * @param currentculture
	 */
	public void RegistrationUser(String name, int forgotques_id,
								 String password, String answer, String fullname, String emailid,
								 String DOB, int gender_id, String aca_level, int groupId,
								 String address, String city, String country, String state,
								 String pincode, String phoneno, String currentculture) {
		// TODO Auto-generated method stub
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<RegistratonUser xmlns=\"http://semanoor.com.sa/\">\n" +
				"<UserName>" + name + "</UserName>\n" +
				"<Forgotquestion>" + forgotques_id + "</Forgotquestion>\n" +
				"<Password>" + password + "</Password>\n" +
				"<PasswordHint>" + answer + "</PasswordHint>\n" +
				"<Fullname>" + fullname + "</Fullname>\n" +
				"<EmailId>" + emailid + "</EmailId>\n" +
				"<DOB>" + DOB + "</DOB>\n" +
				"<Sex>" + gender_id + "</Sex>\n" +
				"<AcaLevel>" + aca_level + "</AcaLevel>\n" +
				"<Group>" + groupId + "</Group>\n" +
				"<Address>" + address + "</Address>\n" +
				"<city>" + city + "</city>\n" +
				"<country>" + country + "</country>\n" +
				"<state>" + state + "</state>\n" +
				"<pincode>" + pincode + "</pincode>\n" +
				"<phone>" + phoneno + "</phone>\n" +
				"<CurrentCulture>" + currentculture + "</CurrentCulture>\n" +
				"</RegistratonUser>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/RegistratonUser";

		String response = CallWebService(soapAction, soapEnvelope);

		//System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+response);
		Xmlparser.parseXml(response);
	}

	/**
	 * Checks the User name and password to login into the domain
	 *
	 * @param username
	 * @param password
	 */
	public void CheckUserNamePassword(String username, String password) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<CheckUserNamePassword xmlns=\"http://semanoor.com.sa/\">\n" +
				"<UserName>" + username + "</UserName>\n" +
				"<Password>" + password + "</Password>\n" +
				"</CheckUserNamePassword>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";
		String SOAPAction = "http://semanoor.com.sa/CheckUserNamePassword";
		String response = CallWebService(SOAPAction, soapEnvelope);
		//System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+response);
		Xmlparser.parseXml(response);
	}

	/**
	 * Login for Enrichments by passing username and password
	 */
	public void enrichPublishLogin(String userName, String password) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<Login xmlns=\"http://SemaSvr.org/\">\n" +
				"<psw>" + webServicePassword + "</psw>\n" +
				"<UsrNm>" + userName + "</UsrNm>\n" +
				"<UsrPsw>" + password + "</UsrPsw>\n" +
				"</Login>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";
		String SOAPAction = "http://SemaSvr.org/Login";
		String response = CallWebService(SOAPAction, soapEnvelope);
		//System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+response);
		Xmlparser.parseXml(response);
	}

	/**
	 * Publish or Update Enriched pages as bytes
	 */
	public void publishOrUpdateEnrichedPagesAsBytes(String methodName, String fileName, String byteArray) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<" + methodName + " xmlns=\"http://SemaSvr.org/\">\n" +
				"<psw>" + webServicePassword + "</psw>\n" +
				"<sFileName>" + fileName + "</sFileName>\n" +
				"<bytFileByteArr>" + byteArray + "</bytFileByteArr>\n" +
				"</" + methodName + ">\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";
		String SOAPAction = "http://SemaSvr.org/" + methodName + "";
		String response = CallWebService(SOAPAction, soapEnvelope);
		//System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+response);
		Xmlparser.parseXml(response);
	}

	/**
	 * Publish or update enriched pages as XML
	 */
	public void publishOrUpdateEnrichmentXml(String methodName, String fileName, String enrichXML) {
		String encodedXml = enrichXML.replace("<", "&lt;").replace(">", "&gt;").replace("'", "&apos;");
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<" + methodName + " xmlns=\"http://SemaSvr.org/\">\n" +
				"<psw>" + webServicePassword + "</psw>\n" +
				"<EnrichmentsFile>" + fileName + "</EnrichmentsFile>\n" +
				"<xmlEnrichments>" + encodedXml + "</xmlEnrichments>\n" +
				"</" + methodName + ">\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";
		String SOAPAction = "http://SemaSvr.org/" + methodName + "";
		String response = CallWebService(SOAPAction, soapEnvelope);
		//System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+response);
		if (methodName.equals("PublishEnriched_Step3_Group")) {
			enrichedPageResponseServerIdList = new ArrayList<Integer>();
		} else {
			enrichedPageUpdateResponseResult = new ArrayList<String>();
		}
		Xmlparser.parseXml(response);
	}

	/**
	 * Check Single Enriched Page Status
	 *
	 * @param enrichedExportedId
	 */
	public void checkEnrichedPage(int enrichedExportedId) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<CheckEnrichedPage xmlns=\"http://semanoor.com.sa/\">\n" +
				"<IDEnriched>" + enrichedExportedId + "</IDEnriched>\n" +
				"</CheckEnrichedPage>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>\n";
		String SOAPAction = "http://semanoor.com.sa/CheckEnrichedPage";
		String response = CallWebService(SOAPAction, soapEnvelope);
		//System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+response);
		Xmlparser.parseXml(response);
	}

	/**
	 * Check Multiple Enriched Page Status
	 *
	 * @param enrichExpotedXML
	 */
	public void checkMultipleEnrichedPages(String enrichExpotedXML) {
		String encodedXml = enrichExpotedXML.replace("<", "&lt;").replace(">", "&gt;").replace("'", "&apos;").replace("\"", "&quot;");
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<CheckEnrichedPages xmlns=\"http://semanoor.com.sa/\">\n" +
				"<xmlEnrichments>" + encodedXml + "</xmlEnrichments>" +
				"</CheckEnrichedPages>" +
				"</soap:Body>" +
				"</soap:Envelope>";
		String SOAPAction = "http://semanoor.com.sa/CheckEnrichedPages";
		String response = CallWebService(SOAPAction, soapEnvelope);
		//System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+response);
		enrichStatusHashMapList = new ArrayList<HashMap<String, String>>();
		Xmlparser.parseXml(response);
	}

	/**
	 * get all the country list
	 */
	public void loadCountryList() {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<getCountryList xmlns=\"http://semanoor.com.sa/\">\n" +
				"</getCountryList>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";
		String soapAction = "http://semanoor.com.sa/getCountryList";
		String response = CallWebService(soapAction, soapEnvelope);
		countryHashMapList = new ArrayList<HashMap<String, String>>();
		Xmlparser.parseXml(response);
	}

	/**
	 * get all the University List
	 */
	public void loadUniversityList() {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<getUniversityList xmlns=\"http://semanoor.com.sa/\">\n" +
				"</getUniversityList>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/getUniversityList";
		String response = CallWebService(soapAction, soapEnvelope);
		universityHashMapList = new ArrayList<HashMap<String, String>>();
		Xmlparser.parseXml(response);
	}

	/**
	 * get all the category list
	 */
	public void loadCategoryList() {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<getCategoryList xmlns=\"http://semanoor.com.sa/\">\n" +
				"</getCategoryList>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/getCategoryList";
		String response = CallWebService(soapAction, soapEnvelope);
		categoryHashMapList = new ArrayList<HashMap<String, String>>();
		Xmlparser.parseXml(response);
	}

	/**
	 * get all the price list
	 */
	public void loadPriceList() {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<GetPriceXML xmlns=\"http://semanoor.com.sa/\" />\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/GetPriceXML";
		String response = CallWebService(soapAction, soapEnvelope);
		priceList = new ArrayList<String>();
		Xmlparser.parseXml(response);
	}

	/**
	 * Upload Book to the server
	 *
	 * @param username
	 * @param password
	 * @param language
	 * @param year
	 * @param activedays
	 * @param bookname
	 * @param book_author_name
	 * @param description
	 * @param directionType_id
	 * @param bookCardImageBase64
	 * @param bookcardImageName
	 * @param bookZipFileBase64
	 * @param bookZipFileName
	 * @param priceValue
	 * @param categoryId
	 * @param keywordTag
	 * @param search
	 * @param countryUniversityId
	 * @param bookOrientation
	 */
	public void uploadBook(String username, String password, int language, String year, int activedays, String bookname, String book_author_name, String description, int directionType_id, String bookCardImageBase64, String bookcardImageName, String bookZipFileBase64, String bookZipFileName, String priceValue, int categoryId, String keywordTag, String search, String countryUniversityId, int bookOrientation) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<AddNewEbooKIpadJunk xmlns=\"http://semanoor.com.sa/\">\n" +
				"<UserName>" + username + "</UserName>\n" +
				"<Password>" + password + "</Password>\n" +
				"<Language>" + language + "</Language>\n" +
				"<Year>" + year + "</Year>\n" +
				"<ActiveDays>" + activedays + "</ActiveDays>\n" +
				"<AuthorName>" + book_author_name + "</AuthorName>\n" +
				"<BoookName>" + bookname + "</BoookName>\n" +
				"<Description>" + description + "</Description>\n" +
				"<DirectionType>" + directionType_id + "</DirectionType>\n" +
				"<bookCardImage>" + bookCardImageBase64 + "</bookCardImage>\n" +
				"<bookCardImageName>" + bookcardImageName + "</bookCardImageName>\n" +
				"<bookZipFile>" + bookZipFileBase64 + "</bookZipFile>\n" +
				"<bookZipFileName>" + bookZipFileName + "</bookZipFileName>\n" +
				"<Price>" + priceValue + "</Price>\n" +
				"<Category>" + categoryId + "</Category>\n" +
				"<Tag>" + keywordTag + "</Tag>\n" +
				"<Search>" + search + "</Search>\n" +
				"<CountryUniversityIDs>" + countryUniversityId + "</CountryUniversityIDs>\n" +
				"<Orientation>" + bookOrientation + "</Orientation>\n" +
				"</AddNewEbooKIpadJunk>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/AddNewEbooKIpadJunk";
		String response = CallWebService(soapAction, soapEnvelope);
		Xmlparser.parseXml(response);
	}

	/**
	 * Upload book to the server new method
	 *
	 * @param userScopeId
	 * @param bookName
	 * @param sAccountCat
	 * @param dayCount
	 * @param bookTitle
	 * @param bookDescription
	 * @param isBookFree
	 * @param academic
	 * @param startPageNo
	 * @param endPageNo
	 * @param countryUniversityId
	 * @param categoryId
	 * @param keywordTag
	 * @param indexPage
	 * @param totalNoOfPages
	 * @param bookLanguage
	 * @param searchOption
	 */
	public void newUploadBook(String userScopeId, String bookName, String sAccountCat, String dayCount, String bookTitle, String bookDescription, int isBookFree, String academic, int startPageNo, int endPageNo, String countryUniversityId, String categoryId, String keywordTag, int indexPage, int totalNoOfPages, int bookLanguage, String searchOption) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<AddNewBook xmlns=\"http://semanoor.com.sa/\">\n" +
				"<sGlobalId>" + userScopeId + "</sGlobalId>\n" +
				"<sBookName>" + bookName + "</sBookName>\n" +
				"<sAccountCat>" + sAccountCat + "</sAccountCat>\n" +
				"<sDayCount>" + dayCount + "</sDayCount>\n" +
				"<sTitle>" + bookTitle + "</sTitle>\n" +
				"<sDescription>" + bookDescription + "</sDescription>\n" +
				"<iBookFree>" + isBookFree + "</iBookFree>\n" +
				"<sAcademic>" + academic + "</sAcademic>\n" +
				"<iStartPageNo>" + startPageNo + "</iStartPageNo>\n" +
				"<iEndPageNo>" + endPageNo + "</iEndPageNo>\n" +
				"<sCountryUniversity>" + countryUniversityId + "</sCountryUniversity>\n" +
				"<sPublishCategory>" + categoryId + "</sPublishCategory>\n" +
				"<sTags>" + keywordTag + "</sTags>\n" +
				"<iIndexpage>" + indexPage + "</iIndexpage>\n" +
				"<iTotalNoofPages>" + totalNoOfPages + "</iTotalNoofPages>\n" +
				"<sBookLang>" + bookLanguage + "</sBookLang>\n" +
				"<sPublishOption>" + searchOption + "</sPublishOption>\n" +
				"</AddNewBook>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/AddNewBook";
		String response = CallWebService(soapAction, soapEnvelope);
		Xmlparser.parseXml(response);
	}

	/**
	 * Upload Book Data as Chunks
	 *
	 * @param userName
	 * @param bookName
	 * @param bookAuthorName
	 * @param bookZipBase64Chunks
	 * @param bookFlag
	 */
	public void uploadBookAsChunks(String userName, String bookName, String bookAuthorName, String bookZipBase64Chunks, String bookFlag) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<InsertAddNewEbookIpadJunk xmlns=\"http://semanoor.com.sa/\">\n" +
				"<sUserName>" + userName + "</sUserName>" +
				"<sBookName>" + bookName + "</sBookName>" +
				"<sBookAuthorName>" + bookAuthorName + "</sBookAuthorName>" +
				"<BookFile>" + bookZipBase64Chunks + "</BookFile>" +
				"<sBookFlag>" + bookFlag + "</sBookFlag>" +
				"</InsertAddNewEbookIpadJunk>" +
				"</soap:Body>" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/InsertAddNewEbookIpadJunk";
		String response = CallWebService(soapAction, soapEnvelope);
		Xmlparser.parseXml(response);
	}

	/**
	 * Upload book data as chunks new method
	 *
	 * @param userScopeId
	 * @param bookName
	 * @param bookZipBase64Chunks
	 * @param bookFlag
	 */
	public void newUploadBookAsChunks(String userScopeId, String bookName, String bookZipBase64Chunks, String bookFlag) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<UploadJunkFileIpad xmlns=\"http://semanoor.com.sa/\">\n" +
				"<sGlobalId>" + userScopeId + "</sGlobalId>\n" +
				"<sBookName>" + bookName + "</sBookName>\n" +
				"<BookFile>" + bookZipBase64Chunks + "</BookFile>\n" +
				"<sBookFlag>" + bookFlag + "</sBookFlag>\n" +
				"</UploadJunkFileIpad>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/UploadJunkFileIpad";
		String response = CallWebService(soapAction, soapEnvelope);
		Xmlparser.parseXml(response);
	}

	/**
	 * Load Book status based on the username and password
	 * @param username
	 * @param password
	 */
	public void loadBookStatus(String username, String password) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"+
                   "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"+
                   "<soap:Body>\n"+
                   "<getBooksDetailsByUser xmlns=\"http://semanoor.com.sa/\">\n"+
                   "<UserName>"+username+"</UserName>\n"+
                   "<Password>"+password+"</Password>\n"+
                   "</getBooksDetailsByUser>\n"+
                   "</soap:Body>\n"+
                   "</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/getBooksDetailsByUser";
		String response = CallWebService(soapAction, soapEnvelope);
		bookStatusList = new ArrayList<HashMap<String,String>>();
		Xmlparser.parseXml(response);
	}

	/**
	 * User SignUp
	 * @param username
	 * @param password
	 * @param emailId
	 * @param fullName
	 * @param uType
	 */
	public void signUp(String username, String password, String emailId, String fullName, int uType) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"+
                             "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"+
                             "<soap:Body>\n"+
                             "<GadgetRegistration xmlns=\"http://semanoor.com.sa/\">\n"+
                             "<sUsername>"+username+"</sUsername>\n"+
                             "<sPassword>"+password+"</sPassword>\n"+
                             "<sEmailID>"+emailId+"</sEmailID>\n"+
                             "<sFullName>"+fullName+"</sFullName>\n"+
                             "<iLoginType>"+uType+"</iLoginType>\n"+
                             "</GadgetRegistration>\n"+
                             "</soap:Body>\n"+
                             "</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/GadgetRegistration";
		String response = CallWebService(soapAction, soapEnvelope);
		Xmlparser.parseXml(response);
	}

	/**
	 * User SignIn
	 * @param userName
	 * @param password
	 * @param uType
	 */
	public void signIn(String userName, String password, int uType) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"+
                   "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"+
                   "<soap:Body>\n"+
                   "<Login xmlns=\"http://semanoor.com.sa/\">\n"+
                   "<sUsername>"+userName+"</sUsername>\n"+
                   "<sUserPassword>"+password+"</sUserPassword>\n"+
                   "<iLoginType>"+uType+"</iLoginType>\n"+
                   "</Login>\n"+
                   "</soap:Body>\n"+
                   "</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/Login";
		String response = CallWebService(soapAction, soapEnvelope);
		Xmlparser.parseXml(response);
	}


	/**
	 * UpdateDeviceTokenForPushNotification
	 * @param userId
	 * @param deviceToken
	 * @param deviceType
	 */
	public void updateDeviceTokenWithUserID(String userId, String deviceToken, String deviceType) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"+
                             "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"+
                             "<soap:Body>\n"+
                             "<UpdateDeviceToken xmlns=\"http://semanoor.com.sa/\">\n"+
                             "<sDeviceToken>"+deviceToken+"</sDeviceToken>\n"+
                             "<sGlobalId>"+userId+"</sGlobalId>\n"+
                             "<sDeviceType>"+deviceType+"</sDeviceType>"+
                             "</UpdateDeviceToken>\n"+
                             "</soap:Body>\n"+
                             "</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/UpdateDeviceToken";
		String response = CallWebService(soapAction, soapEnvelope);
		Xmlparser.parseXml(response);
	}

	/**
	 * get all book enrichments for the loggedin user
	 * @param userScopeId
	 */
	public void getEnrichmentPagesByUserId(String userScopeId, ArrayList<SharedUsers> sharedList) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"+
                             "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"+
                             "<soap:Body>\n"+
                             "<GetEnrichmentsPagesByUserID xmlns=\"http://semanoor.com.sa/\">\n"+
                             "<sGlobalId>"+userScopeId+"</sGlobalId>\n"+
                             "</GetEnrichmentsPagesByUserID>\n"+
                             "</soap:Body>\n"+
                             "</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/GetEnrichmentsPagesByUserID";
		String response = CallWebService(soapAction, soapEnvelope);
		enrichmentList = new ArrayList<Enrichments>();
		isLocalFileParsing = false;
		isForParsingEnrichments = true;
		blockmaild=sharedList;
	    Xmlparser.parseXml(response);
		isForParsingEnrichments = false;
	}

	/**
	 * get Enrichment list from local xml file for parsing
	 * @param xmlPath
	 */
	public void getDownloadedEnrichmentsAndResourcesFromXmlPath(String xmlPath){
		String xml = UserFunctions.readFileFromPath(new File(xmlPath));
		if (xml != null) {
			isLocalFileParsing = true;
			isForParsingEnrichments = true;
			Xmlparser.parseXml(xml);
		}
	}

	/**
	 * get all book resources for the loggedin user
	 * @param userScopeId
	 * @param sharedList
	 */
	public void getResourcesPagesByUserId(String userScopeId, ArrayList<SharedUsers> sharedList) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"+
                             "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"+
                             "<soap:Body>\n"+
                             "<GetResourcePagesByUserId xmlns=\"http://semanoor.com.sa/\">\n"+
                             "<sGlobalId>"+userScopeId+"</sGlobalId>\n"+
                             "</GetResourcePagesByUserId>\n"+
                             "</soap:Body>\n"+
                             "</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/GetResourcePagesByUserId";
		String response = CallWebService(soapAction, soapEnvelope);
		resourceList = new ArrayList<Resources>();
		isLocalFileParsing = false;
		blockmaild=sharedList;
		Xmlparser.parseXml(response);
	}

	/**
	 * get XML from url and parse it
	 *
	 * @param resourcePath
	 */
	public void getXmlFromUrlAndParse(String resourcePath) {
		URL url;
		StringBuilder xmlStr = null;
		try {
			url = new URL(resourcePath);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConnection = (HttpURLConnection) connection;
			InputStream in = httpConnection.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			xmlStr = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				xmlStr.append(line);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (xmlStr != null) {
			noteList = new ArrayList<HashMap<String, String>>();
			linkList = new ArrayList<HashMap<String, String>>();
			mindmapList = new ArrayList<HashMap<String, String>>();
			Xmlparser.parseXml(xmlStr.toString());
		}
	}

	public void getXMLandParseForResource(String resXml) {
		noteList = new ArrayList<HashMap<String, String>>();
		linkList = new ArrayList<HashMap<String, String>>();
		mindmapList = new ArrayList<HashMap<String, String>>();
		Xmlparser.parseXml(resXml);
	}

	public void getResourcePagesBasedOnBookId(String xmlUpdate, int BookId, int pageNumber, String userScopeId) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				+ "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
				+ "<soap:Body>\n"
				+ "<GetResourcePages  xmlns=\"http://semanoor.com.sa/\">\n"
				+ "<xmlCheckUpdate>" + xmlUpdate + "</xmlCheckUpdate>\n"
				+ "<IDBook>" + BookId + "</IDBook>\n"
				+ "<IDPage>" + pageNumber + "</IDPage>\n"
				+ "<sGlobalId>" + userScopeId + "</sGlobalId>"
				+ "</GetResourcePages>\n"
				+ "</soap:Body>\n"
				+ "</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/GetResourcePages";
		String response = CallWebService(soapAction, soapEnvelope);
		resourceList = new ArrayList<Resources>();
		isLocalFileParsing = false;
		Xmlparser.parseXml(response);
	}

	/**
	 * Publish or Update Enriched pages as bytes
	 */
	public void PublishNewEnrichedGroup(String globalId, String enrFileName, String xml, boolean isPrivate, String emails) {
		String Private = "false";
		if (isPrivate) {
			Private = "true";
		}
		//String encodedXml = xml.replace("<", "&lt;").replace(">", "&gt;").replace("'", "&apos;");
		String encodedXml = xml.replace("<", "&lt;").replace(">", "&gt;").replace("'", "&apos;").replace("\"", "&quot;");
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<PublishNewEnrichedGroup xmlns=\"http://semanoor.com.sa/\">\n" +
				"<sGlobalId>" + globalId + "</sGlobalId>\n" +
				"<sEnrichedFile>" + enrFileName + "</sEnrichedFile>\n" +
				"<sxmlEnrichments>" + encodedXml + "</sxmlEnrichments>\n" +
				"<bIsPrivate>" + Private + "</bIsPrivate>\n" +
				"<sRemails>" + emails + "</sRemails>\n" +
				"</PublishNewEnrichedGroup>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";
		String SOAPAction = "http://semanoor.com.sa/PublishNewEnrichedGroup";
		//URL=Globals.WEBSERVICEURL;
		String response = CallWebService(SOAPAction, soapEnvelope);
		enrichedPageResponseServerIdList = new ArrayList<Integer>();
		//System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+response);
		Xmlparser.parseXml(response);
	}

	public void ExportOnlineEnrichment(String globalId, String enrFileName, String xml, boolean isPrivate,int bookId,int enrichPageNo,String title) {
		String Private = "false";
		if (isPrivate) {
			Private = "true";
		}
		String encodedXml = xml.replace("<", "&lt;").replace(">", "&gt;").replace("'", "&apos;").replace("\"", "&quot;");
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"  <soap:Body>\n" +
				"    <ExportOnlineEnrichment xmlns=\"http://semanoor.com.sa/\">\n" +
				"      <sEnrichedFile>"+ enrFileName +"</sEnrichedFile>\n" +
				"      <sGlobalId>"+ globalId +"</sGlobalId>\n" +
				"      <iIDBook>"+bookId+"</iIDBook>\n" +
				"      <iIDPage>"+enrichPageNo+"</iIDPage>\n" +
				"      <sTitle>"+title+"</sTitle>\n" +
				"      <bIsPrivate>"+Private+"</bIsPrivate>\n" +
				"      <IsEdit>1</IsEdit>\n" +
				"      <EnrichID>0</EnrichID>\n" +
				"      <sXMLEnrichments>"+ encodedXml +"</sXMLEnrichments>\n" +
				"    </ExportOnlineEnrichment>\n" +
				"  </soap:Body>\n" +
				"</soap:Envelope>";
		String SOAPAction = "http://semanoor.com.sa/ExportOnlineEnrichment";
		//URL=Globals.WEBSERVICEURL;
		String response = CallWebService(SOAPAction, soapEnvelope);
		enrichedPageResponseServerIdList = new ArrayList<Integer>();
		//System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+response);
		Xmlparser.parseXml(response);
	}


	public void publishResouce(int resourceId, String globalid, String bookId, int pageNo, String title, String desc, int resouceType, int size, String byteArray, String emails, boolean isPrivate) {
		String Private = "false";
		if (isPrivate) {
			Private = "true";
		}

		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<SaveResourceDetails xmlns=\"http://semanoor.com.sa/\">\n" +
				"<iResourceID>" + resourceId + "</iResourceID>\n" +
				"<sGlobalId>" + globalid + "</sGlobalId>\n" +
				"<iIDBook>" + bookId + "</iIDBook>\n" +
				"<iIDPage>" + pageNo + "</iIDPage>\n" +
				"<sTitle>" + title + "</sTitle>\n" +
				"<sDescription>" + desc + "</sDescription>\n" +
				"<iResourceType>" + resouceType + "</iResourceType>\n" +
				"<iSize>" + size + "</iSize>\n" +
				"<bytFileByteArr>" + byteArray + "</bytFileByteArr>\n" +
				"<IsPrivate>" + Private + "</IsPrivate>\n" +
				"<Remails>" + emails + "</Remails>\n" +
				"</SaveResourceDetails>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String SOAPAction = "http://semanoor.com.sa/SaveResourceDetails";
		String response = CallWebService(SOAPAction, soapEnvelope);
		enrichedPageResponseServerIdList = new ArrayList<Integer>();
		//System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+response);
		Xmlparser.parseXml(response);
	}

	public void getMediaCategoryList() {
		String password = "SemaW2e0b0S9r7v2A7d8d0e5d";
		String categoryType = "Media";

		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				+ "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
				+ "<soap:Body>\n"
				+ "<GetMediaLibraryCategoryUpload xmlns=\"http://SemaMediaLibraryApi.org/\">\n"
				+ "<Password>" + password + "</Password>\n"
				+ "<UsertTypeID>1</UsertTypeID>\n"
				+ "<CategoryType>" + categoryType + "</CategoryType>\n"
				+ "</GetMediaLibraryCategoryUpload>\n"
				+ "</soap:Body>\n"
				+ "</soap:Envelope>";

		String soapAction = "http://SemaMediaLibraryApi.org/GetMediaLibraryCategoryUpload";
		String response = CallWebService(soapAction, soapEnvelope);
		System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + response);
		Xmlparser.parseXml(response);
	}

	public void mediaLibraryUpload(String fileName, String byteArray, String selectedNode, String userId, String title, String description, String mediaType, String published) {
		String password = "SemaW2e0b0S9r7v2A7d8d0e5d";

		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				+ "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
				+ "<soap:Body>\n"
				+ "<MediaLibraryUpload1 xmlns=\"http://SemaMediaService.org/\">\n"
				+ "<Passowrd>" + password + "</Passowrd>\n"
				+ "<sFileName>" + fileName + "</sFileName>\n"
				+ "<bytFileByteArr>" + byteArray + "</bytFileByteArr>\n"
				+ "<DetCatType>Media</DetCatType>\n"
				+ "<SelectedNodes>" + selectedNode + "~</SelectedNodes>\n"
				+ "<UserID>" + userId + "</UserID>\n"
				+ "<TitleAr>" + title + "</TitleAr>\n"
				+ "<TitleEn>" + title + "</TitleEn>\n"
				//+"<Media><img src='%@'></img></Media>\n"
				+ "<Publish>1</Publish>\n"
				+ "<Rating>1</Rating>\n"
				+ "<Embedding>1</Embedding>\n"
				+ "<Mature>1</Mature>\n"
				+ "<MediaTypeID>" + mediaType + "</MediaTypeID>\n"
				//"<MediaTypeID>EmbedImage</MediaTypeID>"
				+ "<Comments>1</Comments>\n"
				+ "<Feature>1</Feature>\n"
				+ "<Sponser> </Sponser>\n"
				+ "<Description>" + description + "</Description>\n"
				+ "<Thumb> </Thumb>\n"
				+ "<Tags> </Tags>\n"
				+ "<SizeOrCount>0</SizeOrCount>\n"
				+ "<MediaFullPath> </MediaFullPath>\n"
				+ "<BucketNameS3> </BucketNameS3>\n"
				+ "<ApplicationID>0</ApplicationID>\n"
				+ "<Longitude>" + "0" + "</Longitude>\n"
				+ "<Latitude>" + "0" + "</Latitude>\n"
				+ "</MediaLibraryUpload1>\n"
				+ "</soap:Body>\n"
				+ "</soap:Envelope>";

		String soapAction = "http://SemaMediaLibraryApi.org/GetMediaLibraryMediaByUserID";
		String response = CallWebService(soapAction, soapEnvelope);
		System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + response);
		//Xmlparser.parseXml(response);
	}

	public void GetMediaLibraryMediaByUserID(String userID, int pageNO, int pageSize, String type) {

		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<GetMediaLibraryMediaByUserID xmlns=\"http://SemaMediaLibraryApi.org/\">\n" +
				"<UserId>" + userID + "</UserId>\n" +         //String
				"<PageNumber>" + pageNO + "</PageNumber>\n" +    // int
				"<PageSize>" + pageSize + "</PageSize> \n" +          //   int
				"<SearchIn>" + type + "</SearchIn>\n" +      // String
				"</GetMediaLibraryMediaByUserID>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>\n";

		//String soapAction ="http://SemaMediaLibraryApi.org/GetMediaLibraryMediaByUserID";
		String soapAction = "http://SemaMediaLibraryApi.org/GetMediaLibraryMediaByUserID";
		String response = CallWebService(soapAction, soapEnvelope);
		System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + response);
		Xmlparser.parseXml(response);

	}

	public String newCloudEnrichmentsFromUser(String fromEmail, String toEmail, String message, String content) {
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<NewCloudEnrichments xmlns=\"http://semanoor.com.sa/\">\n" +
				"<FromUserMail>" + fromEmail + "</FromUserMail>\n" +
				"<ToUserMail>" + toEmail + "</ToUserMail>\n" +
				"<EMessage>" + message + "</EMessage>\n" +
				"<EContent>" + content + "</EContent>\n" +
				"</NewCloudEnrichments>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";
		String soapAction = "http://semanoor.com.sa/NewCloudEnrichments";
		String response = CallWebService(soapAction, soapEnvelope);
		return response;
	}

	public ArrayList<Integer> loadDownloadedCloudEnrList(Context context) {
		DatabaseHandler db = DatabaseHandler.getInstance(context);
		downloadedCloudEnrList = db.getDownloadedCloudEnrList();
		return downloadedCloudEnrList;
	}

	public void getCloudEnrichmentsFromUser(String email, Context context) {
		loadDownloadedCloudEnrList(context);
		String soapEnvelope = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<GetCloudEnrichmentList xmlns=\"http://semanoor.com.sa/\">\n" +
				"<UserMail>" + email + "</UserMail>\n" +
				"</GetCloudEnrichmentList>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";
		String soapAction = "http://semanoor.com.sa/GetCloudEnrichmentList";
		String response = CallWebService(soapAction, soapEnvelope);
		if (response != null && response != "") {
			cloudEnrList = new ArrayList<HashMap<String, String>>();
			Xmlparser.parseXml(response);
		}
	}

	public void saveUserGroupDetails(String scopeId, String jsonData) {
		String soapMessage1= "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<saveUserGroups xmlns=\"http://semanoor.com.sa/\">\n" +
				"<sGlobalId>"+scopeId+"</sGlobalId>\n" +
				"<sGroupJson>"+jsonData+"</sGroupJson>\n" +
				"</saveUserGroups>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";

		String soapAction = "http://semanoor.com.sa/saveUserGroups";
		String response = CallWebService(soapAction, soapMessage1);
	    //System.out.println(response1);
		System.out.println(response);
    }

	public void getUserGroupDetails(String scopeId) {
		String response;
		String soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<getUserGroups xmlns=\"http://semanoor.com.sa/\">\n" +
				"<sGlobalId>" + scopeId + "</sGlobalId>\n" +
				"</getUserGroups>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";
		String soapAction = "http://semanoor.com.sa/getUserGroups";
		response = CallWebService(soapAction, soapMessage);
		Xmlparser.parseXml(response);
	}
	public void deleteEnrichments(String jsonString,String ScopeId){
		enrichmentsList=new HashMap<String, String>();
		String response;
		long isCloud;
		if(Globals.ISGDRIVEMODE()){
			isCloud =1;
		}else{
			isCloud = 0;
		}

		String soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"<soap:Body>\n" +
				"<DeleteEnrichmentByUserID xmlns=\"http://semanoor.com.sa/\">\n" +
				"<sGlobalId>" + ScopeId + "</sGlobalId>\n" +
				"<isCloud>"+ isCloud +"</isCloud>\n" +
				"<DeleteIds>"+ jsonString +"</DeleteIds>\n" +
				"</DeleteEnrichmentByUserID>\n" +
				"</soap:Body>\n" +
				"</soap:Envelope>";
		String soapAction = "http://semanoor.com.sa/DeleteEnrichmentByUserID";
		response = CallWebService(soapAction, soapMessage);
		Xmlparser.parseXml(response);
		//System.out.println(response);
	}
}


