package com.semanoor.source_sboookauthor;

public class HTMLObjectHolder {

	private String objType;
	private int objBID;
	private String objPageNo;
	private int objXPos, objYPos, objWidth, objHeight;
	private String objContentPath;
	private int objUniqueRowId;
	private int objSequentialId;
	private String objScalePageToFit;
	private String objLocked;
	private int objEnrichID;
	private String objSize;
	
	public HTMLObjectHolder(String type, int BID, String pageNo, String location, String size, String contentPath, int rowId, int sequentialId, String scaleToFit, String objLocked, int objEnrichID) {
		this.setObjType(type);
		this.setObjBID(BID);
		this.setObjPageNo(pageNo);
		this.setObjXPos(Integer.parseInt(location.split("\\|")[0]));
		this.setObjYPos(Integer.parseInt(location.split("\\|")[1]));
		if (type.equals("PageBG")|| type.equals("mindmapBG")) {
			this.setObjSize(size);
		} else {
			this.setObjWidth(Integer.parseInt(size.split("\\|")[0]));
			this.setObjHeight(Integer.parseInt(size.split("\\|")[1]));
			if(type.equals("WebTable")){
				this.setObjSize(size);
			}
		}
		this.setObjContentPath(contentPath);
		this.setObjUniqueRowId(rowId);
		this.setObjSequentialId(sequentialId);
		this.setObjScalePageToFit(scaleToFit);
		this.setObjLocked(objLocked);
		this.setObjEnrichID(objEnrichID);
	}

	public String getObjType() {
		return objType;
	}

	public void setObjType(String objType) {
		this.objType = objType;
	}

	/**
	 * @return the objContentPath
	 */
	public String getObjContentPath() {
		return objContentPath;
	}

	/**
	 * @param objContentPath the objContentPath to set
	 */
	public void setObjContentPath(String objContentPath) {
		this.objContentPath = objContentPath;
	}

	/**
	 * @return the objSequentialId
	 */
	public int getObjSequentialId() {
		return objSequentialId;
	}

	/**
	 * @param objSequentialId the objSequentialId to set
	 */
	public void setObjSequentialId(int objSequentialId) {
		this.objSequentialId = objSequentialId;
	}

	/**
	 * @return the objXPos
	 */
	public int getObjXPos() {
		return objXPos;
	}

	/**
	 * @param objXPos the objXPos to set
	 */
	public void setObjXPos(int objXPos) {
		this.objXPos = objXPos;
	}

	/**
	 * @return the objYPos
	 */
	public int getObjYPos() {
		return objYPos;
	}

	/**
	 * @param objYPos the objYPos to set
	 */
	public void setObjYPos(int objYPos) {
		this.objYPos = objYPos;
	}

	/**
	 * @return the objWidth
	 */
	public int getObjWidth() {
		return objWidth;
	}

	/**
	 * @param objWidth the objWidth to set
	 */
	public void setObjWidth(int objWidth) {
		this.objWidth = objWidth;
	}

	/**
	 * @return the objHeight
	 */
	public int getObjHeight() {
		return objHeight;
	}

	/**
	 * @param objHeight the objHeight to set
	 */
	public void setObjHeight(int objHeight) {
		this.objHeight = objHeight;
	}

	/**
	 * @return the objUniqueRowId
	 */
	public int getObjUniqueRowId() {
		return objUniqueRowId;
	}

	/**
	 * @param objUniqueRowId the objUniqueRowId to set
	 */
	public void setObjUniqueRowId(int objUniqueRowId) {
		this.objUniqueRowId = objUniqueRowId;
	}

	/**
	 * @return the objBID
	 */
	public int getObjBID() {
		return objBID;
	}

	/**
	 * @param objBID the objBID to set
	 */
	public void setObjBID(int objBID) {
		this.objBID = objBID;
	}

	/**
	 * @return the objPageNo
	 */
	public String getObjPageNo() {
		return objPageNo;
	}

	/**
	 * @param objPageNo the objPageNo to set
	 */
	public void setObjPageNo(String objPageNo) {
		this.objPageNo = objPageNo;
	}

	/**
	 * @return the objScalePageToFit
	 */
	public String getObjScalePageToFit() {
		return objScalePageToFit;
	}

	/**
	 * @param objScalePageToFit the objScalePageToFit to set
	 */
	public void setObjScalePageToFit(String objScalePageToFit) {
		this.objScalePageToFit = objScalePageToFit;
	}

	/**
	 * @return the objLocked
	 */
	public String getObjLocked() {
		return objLocked;
	}

	/**
	 * @param objLocked the objLocked to set
	 */
	public void setObjLocked(String objLocked) {
		this.objLocked = objLocked;
	}

	/**
	 * @return the objEnrichID
	 */
	public int getObjEnrichID() {
		return objEnrichID;
	}

	/**
	 * @param objEnrichID the objEnrichID to set
	 */
	public void setObjEnrichID(int objEnrichID) {
		this.objEnrichID = objEnrichID;
	}

	/**
	 * @return the objSize
	 */
	public String getObjSize() {
		return objSize;
	}

	/**
	 * @param objSize the objSize to set
	 */
	public void setObjSize(String objSize) {
		this.objSize = objSize;
	}

}
