package com.semanoor.source_sboookauthor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout.LayoutParams;

import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;

public class YoutubePopUpWindow implements OnClickListener {
	
	private BookViewActivity ctx;
	private EditText youtubeEdit;
	private ListView youtubeResultList;
	private long totalItems;
	public long currentJSONData;
	public TextView totalResultsText;
	private int MAX_YOUTUBE_RESULTS = 5;
	private int CURRENT_YOUTUBE_RESULTS = 0;
	private Button prevBtn;
	private Button nextBtn;
	public ArrayList<String> SearchTitleArray = new ArrayList<String>();
	public ArrayList<String> SearchMessageArray = new ArrayList<String>();
	public ArrayList<Bitmap> SearchThumbArray = new ArrayList<Bitmap>();
	//public ArrayList<String> SearchUploaderArray = new ArrayList<String>();
	//public ArrayList<String> SearchViewCountArray = new ArrayList<String>();
	//public ArrayList<String> SearchDurationArray = new ArrayList<String>();
	String youTubeResult="result";
	String text;
	private JSONObject json;
	
	private ProgressBar youtubeProgress;
	public YoutubePopUpWindow(BookViewActivity context) {
		this.ctx = context;
	}
	
	/**
	 * @author callPopUpWindow for Youtube
	 * 
	 */
	public void callPopupWindow(){

		final PopupWindow popwindow= new PopupWindow(ctx);
		final LinearLayout popLayout= new LinearLayout(ctx);
		final View popView = ctx.getLayoutInflater().inflate(R.layout.youtube_results, null, false);
		//final View popView = ctx.getLayoutInflater().inflate(R.layout.image_results, null, false);
		popLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		popLayout.addView(popView);
		popwindow.setContentView(popLayout);
		youtubeEdit = (EditText) popView.findViewById(R.id.editText1);
		youtubeEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					if (!youtubeEdit.getText().equals("")) {
						searchClicked();
					}
				}
				return false;
			}
		});
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		text = prefs.getString(youTubeResult, "");
		youtubeEdit.setText("");
		Button searchBtn = (Button)popView.findViewById(R.id.button1);
		searchBtn.setOnClickListener(this);
		youtubeResultList = (ListView) popView.findViewById(R.id.listView1);
		prevBtn =(Button)popView.findViewById(R.id.button2);
		prevBtn.setOnClickListener(this);
		nextBtn =(Button)popView.findViewById(R.id.button3);
		nextBtn.setOnClickListener(this);
		prevBtn.setEnabled(false);
		nextBtn.setEnabled(false);
		totalResultsText = (TextView)popView.findViewById(R.id.textView1);
		youtubeProgress = (ProgressBar) popView.findViewById(R.id.progressBar1);
		youtubeProgress.setVisibility(View.GONE);
		if (!text.equals("")&& text!=null){
			youtubeEdit.setText(text);
			searchClicked();
		}
		youtubeResultList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
									long arg3) {

				View view = ctx.page.getCurrentView();
				if (view != null) {
					CustomImageRelativeLayout imageRLayout = (CustomImageRelativeLayout) view;
					imageRLayout.setUndoRedoForYoutubeVideo(SearchThumbArray.get(arg2), SearchMessageArray.get(arg2));
					imageRLayout.copyThumbnnailImageForVideo(SearchThumbArray.get(arg2));
					//imageRLayout.updateYoutubeLinkDB(SearchMessageArray.get(arg2));
					imageRLayout.setObjectPathContent(SearchMessageArray.get(arg2));
				}
				popwindow.dismiss();
			}
		});
		popwindow.setFocusable(true);
		popwindow.setTouchable(true);
		popwindow.showAtLocation(popLayout, Gravity.CENTER, 0, 0);
		popwindow.update((int) (Globals.getDeviceWidth() / 1.4), (int) (Globals.getDeviceHeight() / 1.5));
		popwindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
			@Override
			public void onDismiss() {
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString(youTubeResult, youtubeEdit.getText().toString());
				editor.commit();
			}
		});
	}

	public class youtubeSearch extends AsyncTask<Void, Void, Void>{

		String nextPageToken;
		String prevPageToken;

		public youtubeSearch(String nextPageToken, String prevPageToken) {
			this.nextPageToken = nextPageToken;
			this.prevPageToken = prevPageToken;
		}

		@Override
		protected Void doInBackground(Void... params) {

			SearchTitleArray.clear();
			SearchThumbArray.clear();
			//SearchUploaderArray.clear();
			//SearchViewCountArray.clear();
			//SearchDurationArray.clear();
			SearchMessageArray.clear();
			getJSONdata(nextPageToken, prevPageToken);
			return null;
		}

		@Override
		protected void onPreExecute() {
			youtubeProgress.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Void result) {
			
			super.onPostExecute(result);
			youtubeProgress.setVisibility(View.GONE);
			if(currentJSONData == 0){
				totalResultsText.setText("No results");
				prevBtn.setEnabled(false);
				nextBtn.setEnabled(false);
			}else{
				if(currentJSONData/MAX_YOUTUBE_RESULTS <=1){
					String resultText ="About "+totalItems+" results";
					prevBtn.setEnabled(false);
					nextBtn.setEnabled(true);
					totalResultsText.setText(resultText);
				}else{
					//System.out.println("current jsondata:"+currentJSONData);
					int currentPage = (int)currentJSONData/MAX_YOUTUBE_RESULTS;
					if(currentJSONData%MAX_YOUTUBE_RESULTS !=0){
						currentPage = currentPage + 1;
						totalResultsText.setText("Page "+currentPage+" of about "+totalItems+" results");
					}else{
						totalResultsText.setText("Page "+currentPage+" of about "+totalItems+" results");
					}
				}
				youtubeResultList.setAdapter(new youtubeAdapter(ctx, YoutubePopUpWindow.this));
			}
		}
	}

	private void getJSONdata(String nextPageToken, String prevPageToken){
		String searchText = youtubeEdit.getText().toString().replace(" ", "");
		String percentEscpe = Uri.encode(searchText);
		/*HttpClient httpClient = new DefaultHttpClient();
		httpClient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		String youtubeSearchText = "http://gdata.youtube.com/feeds/api/videos?q="+searchText+"&start-index="+(currentJSONData+1)+"&max-results="+MAX_YOUTUBE_RESULTS+"&v=2&alt=jsonc";
		HttpGet request = new HttpGet(youtubeSearchText);*/
		URL url;

		try{
			//url = new URL("http://gdata.youtube.com/feeds/api/videos?q="+searchText+"&start-index="+(currentJSONData+1)+"&max-results="+MAX_YOUTUBE_RESULTS+"&v=2&alt=jsonc");
			if (nextPageToken != "") {
				url = new URL("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=" + MAX_YOUTUBE_RESULTS + "&pageToken="+nextPageToken+"&q=" + percentEscpe + "&key="+Globals.googleYoutubeSearchKey+"");
			} else if (prevPageToken != "") {
				url = new URL("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=" + MAX_YOUTUBE_RESULTS + "&pageToken="+prevPageToken+"&q=" + percentEscpe + "&key="+Globals.googleYoutubeSearchKey+"");
			} else {
				url = new URL("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=" + MAX_YOUTUBE_RESULTS + "&q=" + percentEscpe + "&key="+Globals.googleYoutubeSearchKey+"");
			}

			URLConnection connection = url.openConnection();

			String line;
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while((line = reader.readLine()) != null) {
				builder.append(line);
			}

			json = new JSONObject(builder.toString());

			/*HttpResponse response = httpClient.execute(request);
			HttpEntity resEntity = response.getEntity();
			String stringResponse = EntityUtils.toString(resEntity);
			JSONObject json = new JSONObject(stringResponse);*/

			totalItems = json.getJSONObject("pageInfo").getInt("totalResults");
			JSONArray jsonArray = json.getJSONArray("items");
			currentJSONData = currentJSONData + jsonArray.length();
			CURRENT_YOUTUBE_RESULTS = jsonArray.length();
			//System.out.println("CurrentJSONData length:"+jsonArray.length());
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				try{
					String titleString = jsonObject.getJSONObject("snippet").getString("title");
					SearchTitleArray.add(titleString);
				}catch(Exception e){
					SearchTitleArray.add("");
					e.printStackTrace();
				}
				try{
					JSONObject jsonObject1 = jsonObject.getJSONObject("snippet").getJSONObject("thumbnails");
					String thumbString = jsonObject1.getJSONObject("default").getString("url");
					URL thumbUrl = new URL(thumbString);
					Bitmap thumbBitmap = BitmapFactory.decodeStream(thumbUrl.openConnection().getInputStream());
					SearchThumbArray.add(thumbBitmap);
				}catch(Exception e){
					Bitmap thumbBitmap = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.default_video_bg);
					SearchThumbArray.add(thumbBitmap);
					e.printStackTrace();
				}
				/*try{
					String uploaderStr = jsonObject.getString("uploader");
					SearchUploaderArray.add(uploaderStr);
				}catch(Exception e){
					SearchUploaderArray.add("--");
					e.printStackTrace();
				}
				try{
					String viewStr = jsonObject.getString("viewCount") +" views";
					SearchViewCountArray.add(viewStr);
				}catch(Exception e){
					SearchViewCountArray.add("--");
					e.printStackTrace();
				}
				try{
					int duration = Integer.parseInt(jsonObject.getString("duration"));
					String durationStr = getDuration(duration);
					SearchDurationArray.add(durationStr);
				}catch(Exception e){
					SearchDurationArray.add("--:--:--");
					e.printStackTrace();
				}*/
				try{
					String msgURL;
					String videoId = jsonObject.getJSONObject("id").getString("videoId");
					//msgURL = jsonObject.getJSONObject("player").getString("mobile"); //default
					msgURL = "http://www.youtube.com/embed/"+videoId;
					SearchMessageArray.add(msgURL);
				}catch (JSONException e) {
					SearchMessageArray.add("http://www.youtube.com/");
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		//httpClient.getConnectionManager().shutdown();
	}

	private String getDuration(int seconds){

		int hrs = seconds/3600;
		int minutes = (seconds % 3600)/60;
		seconds = seconds % 60;
		return twoDigitString(hrs)+ ":" + twoDigitString(minutes)+ ":" + twoDigitString(seconds);
	}

	private String twoDigitString(int value){
		if(value == 0){
			return "00";
		}if(value/10 == 0){
			return "0" + value;
		}
		return String.valueOf(value);

	}
	
	private void searchClicked(){
		if (UserFunctions.isInternetExist(ctx)) {
			currentJSONData = 0;
			new youtubeSearch("", "").execute();
		} else {
			UserFunctions.DisplayAlertDialogNotFromStringsXML(ctx, ctx.getResources().getString(R.string.check_internet_connectivity), ctx.getResources().getString(R.string.connection_error));
		}
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.button1){   //Search clicked
			
			InputMethodManager inputmanager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE); //For dismissing the keyboard.
	        inputmanager.hideSoftInputFromWindow(youtubeEdit.getWindowToken(), 0);
			searchClicked();
			
		}else if(v.getId() == R.id.button2){	 //Prev clicked

			if(currentJSONData >=MAX_YOUTUBE_RESULTS*2){
				prevBtn.setEnabled(true);
				currentJSONData = currentJSONData - (CURRENT_YOUTUBE_RESULTS + MAX_YOUTUBE_RESULTS);
				String prevPageToken = "";
				try {
					prevPageToken = json.getString("prevPageToken");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				new youtubeSearch("", prevPageToken).execute();
			}else{
				prevBtn.setEnabled(false);
			}

		}else if(v.getId() == R.id.button3){	  //Next clicked
			
			if(currentJSONData < totalItems){
				prevBtn.setEnabled(true); 
				nextBtn.setEnabled(true);
				String nextPageToken = "";
				try {
					nextPageToken = json.getString("nextPageToken");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				new youtubeSearch(nextPageToken, "").execute();
			}else{
				nextBtn.setEnabled(false);
			}
		}
	}
}
