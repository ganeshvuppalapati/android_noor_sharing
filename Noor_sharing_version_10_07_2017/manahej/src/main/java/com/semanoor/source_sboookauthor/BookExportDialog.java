package com.semanoor.source_sboookauthor;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.Permission;
import com.semanoor.inappbilling.util.IabHelper;
import com.semanoor.inappbilling.util.IabResult;
import com.semanoor.inappbilling.util.Inventory;
import com.semanoor.inappbilling.util.Purchase;
import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.DirectoryPickerActivity;
import com.semanoor.manahij.ExportEnrGroupActivity;
import com.semanoor.manahij.GDriveExport;
import com.semanoor.manahij.MainActivity;
import com.semanoor.manahij.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Krishna on 21-06-2016.
 */
public class BookExportDialog {
    MainActivity activity;
    Book currentBook;
    Button btnPdf,btnEpub,btn_cancel,btn_sharetofrnd;
    EditText et_title;
    ListView lv_grpdetails;
    TextView et_email;
    SharedPreferences prefs;
    public RelativeLayout rootView;
    public IabHelper mHelper;
    private String ePubPath, pdfPath;
    private static final String ITEM_SKU = "sa001";
    public String userName;
    public String scopeId;
    public int scopeType;
    public String fromEmailId,filepath,toEmailId,title,isEditable = "no";;
    private AccountManager accountManager;
    private Account mAccount;
    private static Drive drive;
    private boolean isFileUploaded;
    public ProgressDialog progresdialog;
    GDriveExport gDriveExport;
    //static final String ITEM_SKU = "android.test.purchased";
    //static final String ITEM_SKU = "android.test.cancelled";
    //static final String ITEM_SKU = "android.test.refunded";
    //static final String ITEM_SKU = "android.test.item_unavailable";
    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmfAWdB6FXFoyVXufHBg+JfScBwQg33M3E87MgGSTCCM4uUXN8JX/dxEZ2d5EtcOfNSIcH2t9xXAvBRM8MfxwWaRdnPAgPdUhixDOeSVfeKpE14L9kSwNV3bfg7FB7c/29L54rNRrhgf3ddqr0s+BlX7rOp0KIf1n/KtCo58przpA0elgJ5bBtXYcxYlbbK+2p0Y4+f73Bln1FIOEahFQXXUFVXMN0fDmdiD/9t1ksgOsdJWiM/0Yb8CKDhZEUQAyfKLj0gpYLFyd37fndObyyx4qdDXEP4SDIjCkEggrrSHLXVePZlb8nSpn8UDFx/MIYNUZTSgAeBIpWtBxLdNLOQIDAQAB";
    private int year, month, day;
    public ArrayList<UserGroups> groups,group_details;
    ArrayList<UserGroups> selectedGroups=new ArrayList<UserGroups>();
    String emailIds = null;
    ProgressDialog progressDialog;
    String pdfFilePath;
    boolean isOpenFile;
    Dialog addNewBookDialog;
    Context ctxt;
    public BookExportDialog(Context context, Book book) {
        ctxt=context;
        currentBook=book;
        // prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        File fileImgDir = new File(Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR);
        if (!fileImgDir.exists()) {
            fileImgDir.mkdir();
        }

        File epubPathDir = new File(Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR+"ePub");
        File pdfPathDir = new File(Globals.TARGET_BASE_EXTERNAL_STORAGE_DIR+"Pdf");
        if (!epubPathDir.exists() || !pdfPathDir.exists()) {
            epubPathDir.mkdirs();
            pdfPathDir.mkdirs();
        }

        //ePubPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/SBA_"+currentBook.getBookID()+".epub";
        //pdfPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID()+"/SBA_"+currentBook.getBookID()+".pdf";
        ePubPath = epubPathDir.toString()+"/SBA_"+currentBook.getBookID()+".epub";
        pdfPath = pdfPathDir.toString()+"/SBA_"+currentBook.getBookID()+".pdf";


        prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
        scopeId = prefs.getString(Globals.sUserIdKey, "");
        userName = prefs.getString(Globals.sUserNameKey, "");
        scopeType = prefs.getInt(Globals.sScopeTypeKey, 0);
        fromEmailId = prefs.getString(Globals.sUserEmailIdKey, "");

    }

    public void exportDialog(){
        addNewBookDialog = new Dialog(activity);
        addNewBookDialog.getWindow().setBackgroundDrawable(new ColorDrawable(activity.getResources().getColor(R.color.semi_transparent)));
        //addNewBookDialog.setTitle(R.string.create_new_book);
        addNewBookDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addNewBookDialog.setContentView(activity.getLayoutInflater().inflate(R.layout.bookexportdialog, null));
        addNewBookDialog.getWindow().setLayout((int)(Globals.getDeviceWidth()/1.3), (int)(Globals.getDeviceHeight()/1.2));
        btnPdf = (Button)addNewBookDialog. findViewById(R.id.btn_pdf);
        et_title= (EditText) addNewBookDialog. findViewById(R.id.et_title);
        et_email= (TextView) addNewBookDialog. findViewById(R.id.et_mailid);
        lv_grpdetails=(ListView) addNewBookDialog.findViewById(R.id.lv_grpdetails);
        rootView = (RelativeLayout)addNewBookDialog.findViewById(R.id.export_rootview);
        groups=activity.db.getMyGroups(scopeId,activity);
        lv_grpdetails.setAdapter(new groupListAdapter(groups));

        lv_grpdetails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserGroups group = groups.get(position);
                emailIds= null;
                if(group.isGroupSelected()){
                    group.setGroupSelected(false);
                    selectedGroups.remove(group);
                }else{
                    group.setGroupSelected(true);
                    selectedGroups.add(group);
                }
                  addingEmailIds(selectedGroups);
                 lv_grpdetails.invalidateViews();
            }
        });
        btnPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    boolean isPdfPurchased = prefs.getBoolean(Globals.inAppKey, false);
                  //  isPdfPurchased = true;
                    if (isPdfPurchased) {
                        showPopOverToSaveOrOpenFile(v);
                    } else {
                        startPurchase();
                    }
                } else {
                    UserFunctions.DisplayAlertDialog(activity, R.string.supports_above_kitkat_version, R.string.not_supported);
                }
            }
        });
        btnEpub = (Button)addNewBookDialog. findViewById(R.id.btn_epub);
        btnEpub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isEpubPurchased = prefs.getBoolean(Globals.inAppKey, false);
               // isEpubPurchased = true;
                if (isEpubPurchased) {
                    showPopOverToSaveOrOpenFile(v);
                } else {
                    startPurchase();
                }
            }
        });
        btn_cancel = (Button)addNewBookDialog. findViewById(R.id.btn_cancel);
        btn_sharetofrnd= (Button)addNewBookDialog. findViewById(R.id.btn_sharetofrnd);
        btn_sharetofrnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toEmailId=et_email.getText().toString();
                title=et_title.getText().toString();
                if(!toEmailId.equals("") &&!title.equals("")) {
                   // new ExportBook().execute();
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewBookDialog.cancel();
            }
        });
        Switch btnswitch=(Switch)addNewBookDialog.findViewById(R.id.tbn_switch);
        btnswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isEditable = "yes";
                } else {
                    isEditable = "no";
                }
            }
        });
        addNewBookDialog.show();
    }
    private void addingEmailIds(ArrayList<UserGroups> selectedGroups){
        for(UserGroups group :selectedGroups) {
            group_details = activity.db.getselectedgroupdetails(group.getGroupName(), group.getUserScopeId(), activity);
            for (int i = 0; i < group_details.size(); i++) {
                UserGroups usrgroups = group_details.get(i);
                if (i == 0 && emailIds == null) {
                    if (group_details.size() == 1) {
                        emailIds = usrgroups.getEmailId();
                    } else {
                        emailIds = usrgroups.getEmailId() + ",";
                    }
                } else {
                    emailIds = emailIds + "," + usrgroups.getEmailId();
                }
            }
        }

       if(emailIds!=null) {
            et_email.setText(emailIds);
       }else{
           et_email.setText("");
       }
    }
    private class groupListAdapter extends BaseAdapter {
        ArrayList<UserGroups> groups=new ArrayList<UserGroups>();

        public groupListAdapter(ArrayList<UserGroups> groups2) {
            // TODO Auto-generated constructor stub
            groups=groups2;

        }
        //@Override
        public int getCount() {

            return groups.size();
        }
        ///@Override
        public Object getItem(int position) {

            return null;
        }

        //@Override
        public long getItemId(int position) {

            return 0;
        }

        //@Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if(convertView == null){
                Typeface font = Typeface.createFromAsset(activity.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(parent,font);
                vi = activity.getLayoutInflater().inflate(R.layout.inflate_checklist, null);
            }
            TextView txtTitle = (TextView) vi.findViewById(R.id.textView1);
            txtTitle.setTextColor(activity.getResources().getColor(R.color.ToolBarTextColor));
            CheckBox checkBox = (CheckBox) vi.findViewById(R.id.checkBox1);
            UserGroups group=groups.get(position);
            txtTitle.setText(group.getGroupName());
            //tv.setTextColor(Color.rgb(0, 0, 0));
            if (group.isGroupSelected()) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            return vi;
        }
    }
    public class ExportBook extends AsyncTask<Void, Void, Void> {

        public ExportBook(String mail, String ttle, String isEditble) {
            toEmailId=mail;
            title=ttle;
            isEditable=isEditble;
        }

        @Override
        protected void onPreExecute() {
            String str = ctxt.getResources().getString(R.string.please_wait);
            progresdialog = ProgressDialog.show(ctxt, "", str, true);
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
            String formattedDate = df.format(c.getTime());
            filepath=Globals.TARGET_BASE_FILE_PATH+formattedDate+".zip";
            Zip zip = new Zip();
            String source = Globals.TARGET_BASE_BOOKS_DIR_PATH+currentBook.getBookID();
           //zip.zipFileAtPath(source, filepath);
            zip.zipFilesForEpub(new File(source), filepath);
//            String absPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/";
//            try {
//                UserFunctions.copyDirectory(new File(filepath),new File(absPath+formattedDate+".zip"));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            // initUpload(filepath);

        }
        @Override
        protected Void doInBackground(Void... params) {
            //progresdialog.setMessage(activity.getResources().getString(R.string.upload_gdrive_please_wait));
            File enrichZipFilePath = new File(filepath);
            String zipFileName = enrichZipFilePath.getName();
            if(ctxt instanceof ExportEnrGroupActivity){
                gDriveExport = new GDriveExport((ExportEnrGroupActivity)ctxt, enrichZipFilePath.getPath(), zipFileName, toEmailId,  title, progresdialog,currentBook,filepath,isEditable);

            }else{
                gDriveExport = new GDriveExport((MainActivity)ctxt, enrichZipFilePath.getPath(), zipFileName, toEmailId,  title, progresdialog,currentBook,filepath,isEditable);

            }

            if (Build.VERSION.SDK_INT >= 23) {
                requestAccountsPermissionAndAuthorize();
            } else {
                gDriveExport.Authorize();
            }
            return null;
        }
    }
   public void Authorize(){
       gDriveExport.Authorize();
   }
    public void requestAccountsPermissionAndAuthorize(){
        if (ContextCompat.checkSelfPermission(ctxt, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            if(ctxt instanceof ExportEnrGroupActivity) {
                ActivityCompat.requestPermissions((ExportEnrGroupActivity) ctxt, new String[]{Manifest.permission.GET_ACCOUNTS}, BookViewReadActivity.MY_PERMISSIONS_REQUEST_GET_ACCOUNTS);
            }else{
                ActivityCompat.requestPermissions((MainActivity) ctxt, new String[]{Manifest.permission.GET_ACCOUNTS}, BookViewReadActivity.MY_PERMISSIONS_REQUEST_GET_ACCOUNTS);

            }
        } else if (ContextCompat.checkSelfPermission(ctxt, Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED){
            gDriveExport.Authorize();
        }
    }


    private static Permission insertPermission(Drive service, String fileId, String value, String type, String role) {
        Permission newPermisssion = new Permission();
        newPermisssion.setValue(value);
        newPermisssion.setType(type);
        newPermisssion.setRole(role);
        try {
            return service.permissions().insert(fileId, newPermisssion).execute();
        } catch (IOException e) {
            System.out.println("An Error occured" +e);
        }
        return null;
    }
    public static class FileUploadProgressListener implements MediaHttpUploaderProgressListener {

        @Override
        public void progressChanged(MediaHttpUploader uploader) throws IOException {
            switch (uploader.getUploadState()) {
                case INITIATION_STARTED:
                    //View.header2("Upload Initiation has started.");
                    break;
                case INITIATION_COMPLETE:
                    //View.header2("Upload Initiation is Complete.");
                    break;
                case MEDIA_IN_PROGRESS:
                    //View.header2("Upload is In Progress: " + NumberFormat.getPercentInstance().format(uploader.getProgress()));
                    break;
                case MEDIA_COMPLETE:
                    //View.header2("Upload is Complete!");
                    break;
            }
        }
    }
    private void initUpload(String filepath){

    }
    public void showPopOverToSaveOrOpenFile(final View v) {
        final PopoverView popoverView = new PopoverView(activity, R.layout.list_view);
        popoverView.setContentSizeForViewInPopover(new Point((int)activity.getResources().getDimension(R.dimen.export_epub_pdf_popover_width), (int)activity.getResources().getDimension(R.dimen.export_epub_pdf_popover_height)));
        popoverView.showPopoverFromRectInViewGroup(rootView, PopoverView.getFrameForView(v), PopoverView.PopoverArrowDirectionUp, true);

        ListView listView = (ListView) popoverView.findViewById(R.id.listView1);
        listView.setAdapter(new ExportEpubPdfListAdapter(activity, Globals.listArray));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,
                                    int position, long arg3) {
                boolean isOpenFile = false;
                if (position == 0) { //Open File
                    isOpenFile = true;
                } else if (position == 1) { //SaveFile
                    isOpenFile = false;
                }
                if (v.getId() == R.id.btn_pdf) {
                    new createPdfFile(pdfPath, isOpenFile).execute();
                } else {
                    new createEpubFile(ePubPath, isOpenFile).execute();
                }
                popoverView.dissmissPopover(true);
            }
        });
    }
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {

        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                String message = result.getMessage();
                int response = result.getResponse();
                //System.out.println(result+":"+response);
                if (response == 7) {
                    updateInAppPurchaseState();
                    UserFunctions.DisplayAlertDialog(activity, R.string.item_already_owned, R.string.restoring_purchase);
                } else {
                    String str = activity.getResources().getString(R.string.in_app_purchase_failed_);
                    UserFunctions.DisplayAlertDialog(activity, str +result, R.string.purchase_failed);
                }
                return;
            } else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();
            }
        }
    };

    private void updateInAppPurchaseState(){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Globals.inAppKey, true);
        editor.commit();
    }
    private void startPurchase(){
        mHelper = new IabHelper(activity, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    ////System.out.println("In-app Billing setup failed: " + result);
                    String str = activity.getResources().getString(R.string.in_app_setup_failed);
                    UserFunctions.DisplayAlertDialog(activity, str + result, R.string.billing_setup_failed);
                } else {
                    ////System.out.println("In-app Billing setup isOK");
                    mHelper.launchPurchaseFlow(activity, ITEM_SKU, 10001, mPurchaseFinishedListener, "purchasetoken");
                }
            }
        });
    }

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {

        @Override
        public void onQueryInventoryFinished(IabResult result, Inventory inv) {
            if (result.isFailure()) {
                //System.out.println("Failed");
                UserFunctions.DisplayAlertDialogNotFromStringsXML(activity, result.getMessage(), "");
            } else {
                mHelper.consumeAsync(inv.getPurchase(ITEM_SKU), mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {

        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess()) {
                //System.out.println("Success");
                updateInAppPurchaseState();
                UserFunctions.DisplayAlertDialog(activity, R.string.purchase_completed_successfully, R.string.purchased);
            } else {
                //System.out.println("Failed");
                UserFunctions.DisplayAlertDialog(activity, result.getMessage(), R.string.purchase_failed);
            }
        }
    };

    /*
	 * this lets to create pdf file
	 */
    public class createPdfFile extends AsyncTask<Void, Void, Void> {



        public createPdfFile(String _pdfPath, boolean OpenFile) {
            pdfFilePath = _pdfPath;
            isOpenFile = OpenFile;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!new File(pdfFilePath).exists()) {
                String str = activity.getResources().getString(R.string.create_pdf);
                progressDialog = ProgressDialog.show(activity, "", str, true);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (!new File(pdfFilePath).exists()) {
               ExportToPdf exportPdf = new ExportToPdf(activity, currentBook, activity.db, pdfPath,rootView);
               exportPdf.callToGeneratePDF();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (new File(pdfFilePath).exists()) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (isOpenFile) {
                    openPdfFile(pdfFilePath);
                } else {
                    saveFile(pdfFilePath);
                }
            }
        }
     }
    public void onFinishCreatePdf() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        if (isOpenFile) {
            openPdfFile(pdfFilePath);
        } else {
            saveFile(pdfFilePath);
        }
    }
    /**
     * Create ePub File
     *
     *
     */
    private class createEpubFile extends AsyncTask<Void, Void, Void>{

        ProgressDialog progressDialog;
        String ePubPath;
        boolean isOpenFile;

        public createEpubFile(String _ePubPath, boolean isOpenFile) {
            this.ePubPath = _ePubPath;
            this.isOpenFile = isOpenFile;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!new File(ePubPath).exists()) {
                String str = activity.getResources().getString(R.string.create_epub);
                progressDialog = ProgressDialog.show(activity, "", str, true);
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (!new File(ePubPath).exists()) {
                ExportToEpub exportEpub = new ExportToEpub(activity, currentBook, activity.db, ePubPath,rootView);
                exportEpub.copyDefaultEpubTemplatetoFilesDir();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                String msg = "File saved successfully to "+ePubPath;
                Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
            if (isOpenFile) {
                openEpubFile(ePubPath);
            } else {
                saveFile(ePubPath);
            }
        }

    }
    public void saveFile(String ePubPath) {
        Intent intent = new Intent(activity, DirectoryPickerActivity.class);
        intent.putExtra("sourcePath", ePubPath);
        activity.startActivityForResult(intent, 435);
    }
    public void openEpubFile(String ePubPath){
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(ePubPath)), "application/epub");
            activity.startActivity(intent);
        } catch (Exception e) {
            e.getMessage();
            UserFunctions.DisplayAlertDialog(activity, R.string.no_application_found_to_open_file, R.string.no_application_found);
        }
    }

    public void openPdfFile(String pdfPath){
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(pdfPath)), "application/pdf");
            activity.startActivity(intent);
        } catch (Exception e) {
            e.getMessage();
            UserFunctions.DisplayAlertDialog(activity, R.string.no_application_found_to_open_file, R.string.no_application_found);
        }
    }

    /**
     * this lets to save files to user defined path
     * @author Suriya
     *
     */
    public class saveFileToUserDefinedPath extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;
        String fileSourcePath, fileDestpath;
        boolean savedSuccessfully;

        public saveFileToUserDefinedPath(String fileSourcePath, String fileDestpath) {
            this.fileSourcePath = fileSourcePath;
            this.fileDestpath = fileDestpath;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String str = activity.getResources().getString(R.string.saving_file_please_wait);
            progressDialog = ProgressDialog.show(activity, "", str, true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            savedSuccessfully = UserFunctions.copyFiles(fileSourcePath, fileDestpath);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (savedSuccessfully) {
                String msg = "File saved successfully to "+fileDestpath;
                Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
            } else {
                String msg = "Failed to save file to "+fileDestpath;
                Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
            }
        }

    }

}
