package com.semanoor.source_sboookauthor;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobeta.android.dslv.SimpleDragSortCursorAdapter;
import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.R;

import java.io.File;

public class PageAdapter extends SimpleDragSortCursorAdapter {
	
	private BookViewActivity context;
	private int pageCount;

	public PageAdapter(BookViewActivity _context, int rmid, Cursor c, String[] cols, int[] ids, int something) {
		super(_context, rmid, c, cols, ids, something);
		this.context = _context;
	}

	@Override
	public int getCount() {
		this.pageCount = context.pageCount;
		return this.pageCount;
	}

	@Override
	public View getView(int position, final View convertView, ViewGroup parent) {
		
		View view = super.getView(position, convertView, parent);
		int pageNumber = position + 1;
		
		/*if (view == null) {
			view = ((BookViewActivity) context).getLayoutInflater().inflate(R.layout.page_view, null);
		}*/
		
		//Highlight the Default row of current Selection
		if (position == Integer.parseInt(context.currentPageNumber)-1) {
			context.highlightCurrentSlectedView(view);
		}else {
			context.UnHighlightCurrentSlectedView(view);
		}
		
		//Assign the pageNumebr to textView and thumbnail image for the imageview based on book id
		TextView pageNoText = (TextView) view.findViewById(R.id.text);
		ImageView imgThumbnailView = (ImageView) view.findViewById(R.id.imageView1);
		if(context.currentBook.get_bStoreID()!=null&&context.currentBook.get_bStoreID().contains("P")){
			imgThumbnailView.setBackgroundColor(Color.WHITE);
		}
		if (pageNumber == 1) {
			pageNoText.setText(R.string.cover_page);
			imgThumbnailView.setBackgroundResource(0);
			String thumbnailImagePath=Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+context.currentBook.getBookID()+"/thumbnail_"+pageNumber+".png";
			if(!new File(thumbnailImagePath).exists()){
				thumbnailImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/"+"FreeFiles/frontThumb.png";
			}
			Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
			imgThumbnailView.setImageBitmap(bitmap);
		}else if (pageNumber == context.pageCount) {
			pageNoText.setText(R.string.end_page);
			String thumbnailImagePath=Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+context.currentBook.getBookID()+"/thumbnail_end.png";
			if(!new File(thumbnailImagePath).exists()){
				thumbnailImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/"+"FreeFiles/backThumb.png";
			}
			Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
			imgThumbnailView.setBackgroundResource(0);
			imgThumbnailView.setImageBitmap(bitmap);
		}else{
			int pno = pageNumber - 1;
			String page = context.getResources().getString(R.string.page);
			pageNoText.setText(page+": "+pno);
			String thumbnailImagePath=Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH+"Book_"+context.currentBook.getBookID()+"/thumbnail_"+pageNumber+".png";
			if(!new File(thumbnailImagePath).exists()){
				thumbnailImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+context.currentBook.getBookID()+"/"+"FreeFiles/pageThumb.png";
			}
			Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
			imgThumbnailView.setImageBitmap(bitmap);
			imgThumbnailView.setBackgroundResource(R.color.white);
		}
		
		ImageView iv_dragHandle = (ImageView) view.findViewById(R.id.drag_handle);
        ImageView iv_clickRemove = (ImageView) view.findViewById(R.id.click_remove);
        if (context.listViewOnLongClick && pageNumber != 1 && pageNumber != context.pageCount) {
        	iv_clickRemove.setVisibility(View.VISIBLE);
			iv_dragHandle.setVisibility(View.VISIBLE);
		} else {
			iv_clickRemove.setVisibility(View.GONE);
			iv_dragHandle.setVisibility(View.GONE);
		}
        
        TextView enrichTag = (TextView) view.findViewById(R.id.enrichText);
        if(context.currentBook.is_bStoreBook() ||context.currentBook.get_bStoreID()!=null&&context.currentBook.get_bStoreID().contains("P")){
        	int enrichTabCount = context.enrichTabCountListAllPages.get(position);
        	if(enrichTabCount>0){
        		enrichTag.setVisibility(View.VISIBLE);
        	}else{
        		enrichTag.setVisibility(View.GONE);
        	}
        }else{
        	enrichTag.setVisibility(View.GONE);
        }

		return view;
	}

}
