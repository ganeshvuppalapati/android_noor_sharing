package com.semanoor.source_manahij;

import java.util.ArrayList;

import com.semanoor.manahij.BookView;
import com.semanoor.manahij.R;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class checklistAdapter extends BaseAdapter {
    
//	private String[] listContent;
	private BookView context;
	//CheckBox checkbox;
	//private String enrSelect;
	public ArrayList<String> downloadList = new ArrayList<String>();
	
	public checklistAdapter(BookView context, ArrayList<String> enrichDownloadArray) {
		downloadList = enrichDownloadArray;
		this.context = context;
	}

	@Override
	public int getCount() {
		return downloadList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null) {
			view =  context.getLayoutInflater().inflate(R.layout.inflate_checkbox, null);
		}
		String enrType[] = downloadList.get(position).split("##");
		String enrCurrentType = enrType[10];
		TextView text = (TextView)view.findViewById(R.id.textView1);
		text.setText(enrType[0]);
		ImageView check_image = (ImageView)view.findViewById(R.id.check_image);
		
		if(enrCurrentType.equals("newEnr")){
			check_image.setBackgroundResource(R.drawable.downloadedicon);
		}else if(enrCurrentType.equals("updateEnr")){
			check_image.setBackgroundResource(R.drawable.updateicon);
		}else if(enrCurrentType.equals("enrDownloaded")){
			check_image.setBackgroundResource(R.drawable.downloadedicon);
		}else{
			check_image.setBackgroundResource(R.drawable.downloadedicon);
		}
		
		if(this.context.itemChecked.get(position)){
			check_image.setVisibility(View.VISIBLE);
			//check_image.setBackgroundResource(R.drawable.downloadedicon);
			}
		else{
			check_image.setVisibility(View.GONE);
		}
		
		/*CheckBox checkbox = (CheckBox)view.findViewById(R.id.checkbox);
		checkbox.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CheckBox cb = (CheckBox)v.findViewById(R.id.checkbox);
				if(cb.isChecked()){
					context.itemChecked.set(position,true);
				}else if(!cb.isChecked()){
					context.itemChecked.set(position,false);
				}
			}
		});
		checkbox.setChecked(this.context.itemChecked.get(position));*/
		return view;
	}
}
