package com.semanoor.source_manahij;

import java.util.ArrayList;

import com.semanoor.manahij.BookView;
import com.semanoor.manahij.R;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

public class ListContentAdapter implements ListAdapter {
	BookView popContext;
//	String[] list;
	public ArrayList<String> list = new ArrayList<String>();
	public ListContentAdapter(BookView bookview, ArrayList<String> enrichPagesArray) {
		popContext = bookview;
		list = enrichPagesArray;
	}
	//@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}
	//@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	//@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if(convertView == null){
			LayoutInflater layoutinflater = (LayoutInflater) popContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutinflater.inflate(R.layout.enrich_page_text, null);
		}
			TextView text = (TextView) view.findViewById(R.id.textView1);
			String page = popContext.getResources().getString(R.string.page);
			int  pageno=Integer.parseInt(list.get(position));
			int no=pageno-1;
			text.setText(page+" "+no);
		return view;
	}

	//@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	//@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	//@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	//@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	//@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		// TODO Auto-generated method stub
		
	}

	//@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		// TODO Auto-generated method stub
		
	}

	//@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	//@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return true;
	}
}
