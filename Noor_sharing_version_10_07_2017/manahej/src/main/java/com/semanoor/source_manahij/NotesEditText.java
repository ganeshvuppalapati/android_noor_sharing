package com.semanoor.source_manahij;

import android.R.color;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;

public class NotesEditText extends EditText implements OnTouchListener
{
	    private Rect mRect;
	    private Paint mPaint;

	    // we need this constructor for LayoutInflater
	    public NotesEditText(Context context, AttributeSet attrs) {
	        super(context, attrs);

	        mRect = new Rect();
	        mPaint = new Paint();
	        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
	        mPaint.setColor(Color.rgb(0, 0, 0)); //SET YOUR OWN COLOR HERE
	    }

	    @Override
	    protected void onDraw(Canvas canvas) {
	        //int count = getLineCount();

	        int height = getHeight();
	        int line_height = getLineHeight();

	        int count = height / line_height;

	        if (getLineCount() > count)
	            count = getLineCount();//for long text with scrolling

	        Rect r = mRect;
	        Paint paint = mPaint;
	        int baseline = getLineBounds(0, r);//first line

	        for (int i = 0; i < count; i++) {

	            canvas.drawLine(r.left, baseline + 1, r.right, baseline + 1, paint);
	            baseline += getLineHeight();//next line
	        }

	        super.onDraw(canvas);
	    }

		
	    
	   @Override
		public boolean onTouch(View arg0, MotionEvent event) {
			// TODO Auto-generated method stub
			event.setLocation(event.getX(), event.getY());
			return super.onTouchEvent(event);
		}
	   
   
	  
	
	/* private Rect mRect;
	 private Paint mPaint;

	 // we need this constructor for LayoutInflater
	 public NotesEditText(Context context, AttributeSet attrs) 
	 {
		 super(context, attrs);
		 mRect = new Rect();
		 mPaint = new Paint();
		 mPaint.setStyle(Paint.Style.STROKE);
		 mPaint.setColor(0x80000000);
	 }
	 
	 @Override
	 protected void onDraw(Canvas canvas) 
	 {
		 int count = getLineCount();
	     Rect r = mRect;
	     Paint paint = mPaint;

	     for (int i = 0; i < count; i++) 
	     {
	    	 int baseline = getLineBounds(i, r);
	    	 canvas.drawLine(r.left, baseline + 1, r.right, baseline + 1, paint);
	     }
	     super.onDraw(canvas);
	 }*/
	    
}
