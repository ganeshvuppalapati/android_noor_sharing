package com.semanoor.source_manahij;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.GDriveExport;
import com.semanoor.manahij.R;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_sboookauthor.GenerateHTML;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.UserGroups;
import com.semanoor.source_sboookauthor.Zip;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class ExportEnrResViewAdapter extends PagerAdapter implements android.widget.CompoundButton.OnCheckedChangeListener{

	private BookViewReadActivity bookView;
	private ArrayList<Enrichments> enrichmentList, enrichmentPageList, enrichmentTotalPageNoList, enrichmentSelectedList;
	private ArrayList<Enrichments> enrichmentLinksList, enrichmentPageLinkList, enrichmentLinkTotalPageNoList, enrichmentSelectedLinkList;
	private ArrayList<Note> noteList, notePageList, noteTotalPageNoList, noteSelectedList;
	private ArrayList<Enrichments> mindmapList, mindmapPageList, mindmapTotalPageNoList, mindmapSelectedList;
	private boolean enrichmentTabSelected = true, noteTabSelected = false, enrichmentLinkSelected = false, groupSelected=false,emailSelected=false, mindmapTabSelected = false;
	ListView enrSelectlist,pageListView,lv_groups;
	public ArrayList<UserGroups> groups;
	public ArrayList<UserGroups> selectedGroup = new ArrayList<UserGroups>();
	public ArrayList<UserGroups> Groupdetails = new ArrayList<UserGroups>();
	StringBuilder searchFormat;
	boolean isprivate;
	String mailid;
	String resourceTitle;
	EditText et_mailid;
	public ProgressDialog progresdialog;
	String filepath;
	File enrichZipFilePath;
	String enrichXmlString;
	Switch btnswitch;
	HashMap dicEnrichments;

	//String globalid="12";
	private InputStream chunkInputStream;
	private ByteArrayOutputStream chunkBaos;
	private byte[] chunkBytes;
	private int chunkBytesRead;
	private String chunkBookFlag;	
	private int chunkNoOfExecution;
	String desc;
	EditText et_resourcetitle;
	RelativeLayout rl_groups;

	public GDriveExport gDriveExport;

	public ExportEnrResViewAdapter(BookViewReadActivity bookView) {
		this.bookView = bookView;
		enrichmentList = bookView.db.getAllEnrichmentTabListForExport(bookView.currentBook.getBookID(), bookView);
		enrichmentLinksList = bookView.db.getAllEnrichmentLinksForExport(bookView.currentBook.getBookID(), bookView);
		noteList = bookView.db.getAllNotesForExport(bookView.currentBook.get_bStoreID(), bookView);
		mindmapList = bookView.db.getAllEnrichmentMindmapForExport(bookView.currentBook.getBookID(), bookView);
		enrichmentSelectedList = new ArrayList<Enrichments>();
		enrichmentSelectedLinkList = new ArrayList<Enrichments>();
		noteSelectedList = new ArrayList<Note>();
		mindmapSelectedList = new ArrayList<Enrichments>();
	}

	@Override
	public int getCount() {
		return 3;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		LayoutInflater inflater = (LayoutInflater) bookView.getSystemService(bookView.LAYOUT_INFLATER_SERVICE);
		Typeface font = Typeface.createFromAsset(bookView.getAssets(),"fonts/FiraSans-Regular.otf");
		UserFunctions.changeFont(container,font);
		View view = null;
		switch (position) {
		case 0:
			view = inflater.inflate(R.layout.export_enr, null);
			enrichmentTotalPageNoList = getTotalNumberOfEnrichedPageList();
			enrichmentLinkTotalPageNoList = getTotalNumberOfEnrichedLinkPageList();
			noteTotalPageNoList = getTotalNumberOfNotePageList();
			mindmapTotalPageNoList = getTotalNumberOfMindmapPageList();
			SegmentedRadioButton segmentGroup = (SegmentedRadioButton)view. findViewById(R.id.segment_text);
			RadioButton Radio_enr=(RadioButton)segmentGroup. findViewById(R.id.segEnr);
			RadioButton Radio_notes=(RadioButton)segmentGroup. findViewById(R.id.segNotes);
			RadioButton Radio_links=(RadioButton)segmentGroup. findViewById(R.id.segLinks);
			pageListView = (ListView)view.findViewById(R.id.listView1);
			segmentGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					if (checkedId == R.id.segEnr) {
						enrichmentTabSelected = true;
						noteTabSelected = false;
						enrichmentLinkSelected = false;
						mindmapTabSelected = false;
						pageListView.setAdapter(new PageListAdapter(bookView, enrichmentTotalPageNoList, null, null, null));
					} else if (checkedId == R.id.segNotes) {
						enrichmentTabSelected = false;
						noteTabSelected = true;
						enrichmentLinkSelected = false;
						mindmapTabSelected = false;
						pageListView.setAdapter(new PageListAdapter(bookView, null, null, noteTotalPageNoList, null));
					} else if (checkedId == R.id.segLinks) {
						enrichmentTabSelected = false;
						noteTabSelected = false;
						enrichmentLinkSelected = true;
						mindmapTabSelected = false;
						pageListView.setAdapter(new PageListAdapter(bookView, null, enrichmentLinkTotalPageNoList, null, null));
					} else if (checkedId == R.id.segMindmap) {
						enrichmentTabSelected = false;
						noteTabSelected = false;
						enrichmentLinkSelected = false;
						mindmapTabSelected = true;
						pageListView.setAdapter(new PageListAdapter(bookView, null, null, null, mindmapTotalPageNoList));
					}
				}
			});

			pageListView.setAdapter(new PageListAdapter(bookView, enrichmentTotalPageNoList, null, null, null));
			ProgressBar enrichPage1bar =(ProgressBar)view.findViewById(R.id.enrich_progressbar);

			pageListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapterView, View v,int position, long id) {
					if (enrichmentTabSelected) {
						Enrichments enrichment = enrichmentTotalPageNoList.get(position);
						enrichmentPageList = getTotalNoOfEnrichedPages(enrichment.getEnrichmentPageNo());
					} else if (enrichmentLinkSelected) {
						Enrichments enrichment = enrichmentLinkTotalPageNoList.get(position);
						enrichmentPageLinkList = getTotalNoOfEnrichedLinkPages(enrichment.getEnrichmentPageNo());
					} else if (mindmapTabSelected) {
						Enrichments enrichment = mindmapTotalPageNoList.get(position);
						mindmapPageList = getTotalNoOfMindmapPages(enrichment.getEnrichmentPageNo());
					} else if (noteTabSelected) {
						Note note = noteTotalPageNoList.get(position);
						notePageList = getTotalNoOfNotePages(note.getPageNo());
					}
					((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
					enrSelectlist.invalidateViews();
					bookView.exportEnrViewPager.setCurrentItem(1);
				}
			});
			break;

		case 1:
			view = inflater.inflate(R.layout.enr_listview, null);
			Button btnBack2 = (Button) view.findViewById(R.id.btnBack);
			Button btnpublish = (Button) view.findViewById(R.id.btnExport);
			enrSelectlist=(ListView) view.findViewById(R.id.listView1);
			enrSelectlist.setAdapter(new PageListSelectAdapter());
			enrSelectlist.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> adapterView, View v,
						int position, long id) {
					if (enrichmentTabSelected) {
						Enrichments enrichment = enrichmentPageList.get(position);
						if (enrichment.isEnrichmentSelected()) {
							enrichment.setEnrichmentSelected(false);
							enrichmentSelectedList.remove(enrichment);
						} else {
							enrichment.setEnrichmentSelected(true);
							enrichmentSelectedList.add(enrichment);
						}
					}  else if (enrichmentLinkSelected){
						Enrichments enrichment = enrichmentPageLinkList.get(position);
						if (enrichment.isEnrichmentSelected()) {
							enrichment.setEnrichmentSelected(false);
							enrichmentSelectedLinkList.remove(enrichment);
						} else {
							enrichment.setEnrichmentSelected(true);
							enrichmentSelectedLinkList.add(enrichment);
						}
					} else if (mindmapTabSelected) {
						Enrichments enrichment = mindmapPageList.get(position);
						if (enrichment.isEnrichmentSelected()) {
							enrichment.setEnrichmentSelected(false);
							mindmapSelectedList.remove(enrichment);
						} else {
							enrichment.setEnrichmentSelected(true);
							mindmapSelectedList.add(enrichment);
						}
					} else if (noteTabSelected) {
						Note note = notePageList.get(position);
						if (note.isNoteSelected()) {
							note.setNoteSelected(false);
							noteSelectedList.remove(note);
						} else {
							note.setNoteSelected(true);
							noteSelectedList.add(note);
						}
					}
					((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
				}
			});

			ProgressBar enrichPagebar =(ProgressBar)view.findViewById(R.id.progressBar1);

			btnBack2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					bookView.exportEnrViewPager.setCurrentItem(0);
				}
			});
			btnpublish.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if(enrichmentLinkSelected){
						if(enrichmentSelectedLinkList.size()>0){
							et_resourcetitle.setVisibility(View.VISIBLE);
							bookView.exportEnrViewPager.setCurrentItem(2);
						}else{
							UserFunctions.DisplayAlertDialog(bookView, R.string.no_enrichment_selected, R.string.select_enrichment_to_export);
						}
					}else if(noteTabSelected){
						if(noteSelectedList.size()>0){
							et_resourcetitle.setVisibility(View.VISIBLE);
							bookView.exportEnrViewPager.setCurrentItem(2);
						}
						else {
							UserFunctions.DisplayAlertDialog(bookView, "Note is not Selected", R.string.select_note_to_publish);
						}

					}else if (mindmapTabSelected) {
						if (mindmapSelectedList.size()>0){
							et_resourcetitle.setVisibility(View.VISIBLE);
							bookView.exportEnrViewPager.setCurrentItem(2);
						} else {
							UserFunctions.DisplayAlertDialog(bookView, R.string.no_mindmap_selected, R.string.select_mindmap_to_export);
						}
					} else {
						if(enrichmentSelectedList.size()>0){
							et_resourcetitle.setVisibility(View.VISIBLE);
							bookView.exportEnrViewPager.setCurrentItem(2);
						}else{
							UserFunctions.DisplayAlertDialog(bookView, R.string.no_enrichment_selected, R.string.select_enrichment_to_export);

						}
					}

					if (!enrichmentTabSelected) {
						if (Globals.ISGDRIVEMODE()) {
							btnswitch.setVisibility(View.GONE);
						} else {
							btnswitch.setVisibility(View.INVISIBLE);
						}
						btnswitch.setChecked(true);
						rl_groups.setVisibility(View.VISIBLE);
						et_mailid.setVisibility(View.INVISIBLE);
						lv_groups.setVisibility(View.VISIBLE);
						groupSelected = true;
						emailSelected = false;
						isprivate=true;
					} else {
						if (Globals.ISGDRIVEMODE()) {
							btnswitch.setVisibility(View.GONE);
						} else {
							btnswitch.setVisibility(View.VISIBLE);
						}
						btnswitch.setChecked(true);
						rl_groups.setVisibility(View.VISIBLE);
						et_mailid.setVisibility(View.INVISIBLE);
						lv_groups.setVisibility(View.VISIBLE);
						groupSelected = true;
						emailSelected = false;
						isprivate=true;
						//btnswitch.setOnCheckedChangeListener(ExportEnrResViewAdapter.this);
					}

					if(groups==null){
						 groups=bookView.db.getMyGroups(bookView.scopeId,bookView);
					}
					lv_groups.setAdapter(new groupListAdapter(bookView,groups)); 

				}
			});
			break;
		case 2:
			view = inflater.inflate(R.layout.export_enr_login, null);
            et_resourcetitle = (EditText) view.findViewById(R.id.et_resource);
		    rl_groups=(RelativeLayout) view.findViewById(R.id.group_view);
		    SegmentedRadioButton Group_selection = (SegmentedRadioButton)rl_groups. findViewById(R.id.segment_text);
			RadioButton Radio_group=(RadioButton)Group_selection. findViewById(R.id.seggroup);
			RadioButton Radio_email=(RadioButton)Group_selection. findViewById(R.id.segemail);
			lv_groups=(ListView) rl_groups.findViewById(R.id.lv_groups);
		    et_mailid = (EditText) rl_groups.findViewById(R.id.et_mailid);
			et_mailid.setVisibility(View.INVISIBLE);
		    rl_groups.setVisibility(View.VISIBLE);
			btnswitch = (Switch) view.findViewById(R.id.switch1);
			if (Globals.ISGDRIVEMODE()) {
				btnswitch.setChecked(true);
				btnswitch.setVisibility(View.GONE);
			} else {
				btnswitch.setChecked(true);
				btnswitch.setVisibility(View.VISIBLE);
			}
			//btnswitch.setChecked(true);

			if(btnswitch.isChecked()){
				rl_groups.setVisibility(View.VISIBLE);
				et_mailid.setVisibility(View.INVISIBLE);
				lv_groups.setVisibility(View.VISIBLE);
				groupSelected = true;
				emailSelected = false;
				isprivate=true;
			}else{
				rl_groups.setVisibility(View.GONE);
				groupSelected = false;
				emailSelected = false;
				isprivate=false;
			}

			if(Radio_group.isChecked()){
				groupSelected = true;
				emailSelected = false;
				et_mailid.setVisibility(View.INVISIBLE);
				lv_groups.setVisibility(View.VISIBLE);
			}else if(Radio_email.isChecked()){
				groupSelected = false;
				emailSelected = true;
				et_mailid.setVisibility(View.VISIBLE);
				lv_groups.setVisibility(View.INVISIBLE);
			}

			Button btnPublish = (Button) view.findViewById(R.id.btnPublish);
			
			Group_selection.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					if (checkedId == R.id.seggroup){
						groupSelected = true;
						emailSelected = false;
						et_mailid.setVisibility(View.INVISIBLE);
						lv_groups.setVisibility(View.VISIBLE);
					} else if (checkedId == R.id.segemail){
						groupSelected = false;
						emailSelected = true;
						et_mailid.setVisibility(View.VISIBLE);
						lv_groups.setVisibility(View.INVISIBLE);
					}
				}
			});
			
			Button btnBack = (Button) view.findViewById(R.id.btnBack);
			btnBack.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					bookView.exportEnrViewPager.setCurrentItem(1);
					et_mailid.getText().clear();
					et_resourcetitle.getText().clear();
				}
			});
			btnPublish.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					resourceTitle=et_resourcetitle.getText().toString();
                    if(enrichmentLinkSelected){
						publishmethod();
					}else if(noteTabSelected){
						publishmethod();
					}else if (mindmapTabSelected){
						publishmethod();
					}else{
						publishmethod();
					}
				}
			});
			lv_groups.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapterView, View v,
						int position, long id) {
					UserGroups group=groups.get(position);
					if (group.isGroupSelected()) {
						group.setGroupSelected(false);
						selectedGroup.remove(group);;
					} else {
						group.setGroupSelected(true);
						selectedGroup.add(group);
					}
					lv_groups.invalidateViews();
				}
		    });
			ImageView imgViewCover = (ImageView) view.findViewById(R.id.imgView_cover);
			int BID = bookView.currentBook.getBookID();
			String bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/FreeFiles/pageBG_1.png";
			if(!new File(bookCardImagePath).exists()){
				bookCardImagePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+BID+"/FreeFiles/card.png";
			}
			Bitmap bitmap = BitmapFactory.decodeFile(bookCardImagePath);
			imgViewCover.setImageBitmap(bitmap);


			break;

		default:
			break;
		}

		((ViewPager)container).addView(view, 0);
		return view;
		//return super.instantiateItem(container, position);
	}
	@Override
	public void destroyItem(ViewGroup container, int position, Object object){	
		((ViewPager)container).removeView((View) object);
	}

	public void requestAccountsPermissionAndAuthorize(){
		if (ContextCompat.checkSelfPermission(bookView, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(bookView, new String[]{Manifest.permission.GET_ACCOUNTS}, BookViewReadActivity.MY_PERMISSIONS_REQUEST_GET_ACCOUNTS);
		} else if (ContextCompat.checkSelfPermission(bookView, Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED){
			gDriveExport.Authorize();
		}
	}

	private class validateLoginAndPublishEnrichments extends AsyncTask<Void, Void, Void>{
		@Override
		protected void onPreExecute() {
			
		    if(enrichmentLinkSelected){
				progresdialog.setMessage(bookView.getResources().getString(R.string.upload_please_wait));
				genratingXmlforLinksSelected();
				desc="Hello This is My Link";
				filepath=Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/resources.xml";
			}else if(noteTabSelected){
				progresdialog.setMessage(bookView.getResources().getString(R.string.upload_please_wait));
				generatingXmlfornoteSelected();
				desc="Hello This is My Note";
				filepath=Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/resources.xml";
			}else if (mindmapTabSelected){
				progresdialog.setMessage(bookView.getResources().getString(R.string.upload_please_wait));
				genratingXmlforMindmapSelected();
				desc="Hello This is My Mindmap";
				Calendar c = Calendar.getInstance();
				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
				String formattedDate = df.format(c.getTime());
				filepath=Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/"+formattedDate+".zip";
				Zip zip = new Zip();
				String sourcePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/";
				zip.zipFilesForEnrichments(new File(sourcePath), filepath);
			}else{
				if (Globals.ISGDRIVEMODE()) {
					progresdialog.setMessage(bookView.getResources().getString(R.string.upload_gdrive_please_wait));
				} else {
					progresdialog.setMessage(bookView.getResources().getString(R.string.upload_please_wait));
				}
				Calendar c = Calendar.getInstance();
				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
				String formattedDate = df.format(c.getTime());
				filepath=Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/"+formattedDate+".zip";
				initUpload(filepath);
			}
			
			enrichZipFilePath = new File(filepath);
		}

		@Override
		protected Void doInBackground(Void... params) {
			if (Globals.ISGDRIVEMODE()) {
				if (enrichmentTabSelected || mindmapTabSelected) {
					String zipFileName = enrichZipFilePath.getName();
					String type;
					if (enrichmentTabSelected){
						type = "Enrichment";
					}else{
						type = "Resource";
					}
					gDriveExport = new GDriveExport(bookView, enrichZipFilePath.getPath(), zipFileName, mailid, enrichXmlString, dicEnrichments, resourceTitle, progresdialog,type);
					if (Build.VERSION.SDK_INT >= 23) {
						requestAccountsPermissionAndAuthorize();
					} else {
						gDriveExport.Authorize();
					}
				} else {
					gDriveExport = new GDriveExport(bookView, mailid, enrichXmlString, resourceTitle, progresdialog);
					bookView.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							gDriveExport.new createJsonValueForResource().execute();
						}
					});
				}
			} else {
				if (enrichmentLinkSelected || noteTabSelected || enrichmentTabSelected || mindmapTabSelected) {
					PublishEnrichmentsAsChunks();
				}
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}

		public void zippingMindmapPageForExport(int enrichId,String mindMapPath,int pageNumber){
			File file=new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp");
			if (file.exists()){
				//file.delete();
				UserFunctions.deleteAllFilesFromDirectory(file);
			}
			String htmlFileDirPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/"+bookView.currentBook.getBookID()+"-"+pageNumber+"-"+enrichId;
			UserFunctions.createNewDirectory(htmlFileDirPath);
			String htmlFilePath = htmlFileDirPath+"/mindmap.xml";
			UserFunctions.copyFiles(mindMapPath,htmlFilePath);
			Zip zip = new Zip();
			String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/"+bookView.currentBook.getBookID()+"-"+pageNumber+"-"+enrichId+".zip";
			zip.zipFileAtPath(htmlFileDirPath, destPath);
			UserFunctions.DeleteDirectory(new File(htmlFileDirPath));
		}
		
		/**
		 * Generate all the selected enrichment html pages and and make it as zip and create an xml to prepare for publish or update.
		 */
		private void initUpload(String destPath){

			//String storBookId = bookView.currentBook.get_bStoreID().replace("M", "");
			String[] bookName= bookView.currentBook.get_bStoreID().split("M");
			String storBookId =bookName[1];
				enrichXmlString = "<Enrichments>";
			if (Globals.ISGDRIVEMODE()) {
				dicEnrichments = new HashMap();
			}
			int i = 0;
			for (Enrichments enrichments : enrichmentSelectedList) {
				if (enrichments.isEnrichmentSelected()) {
					new GenerateHTML(bookView, bookView.db, bookView.currentBook, String.valueOf(enrichments.getEnrichmentPageNo()), null,false).generateEnrichedPageForExportAndZipIt(enrichments);
					String fileName = bookView.currentBook.getBookID()+"-"+enrichments.getEnrichmentPageNo()+"-"+enrichments.getEnrichmentId()+".zip";
					//String fileName = enrichments.getEnrichmentPageNo()+"-"+enrichments.getEnrichmentId()+".zip";

					int idPage = enrichments.getEnrichmentPageNo();
					String bookname=bookView.currentBook.get_bStoreID().replace("M", "");
					//bookname=bookname.replace("M", "");

					enrichXmlString = enrichXmlString.concat("<Enriched IDUser='"+bookView.scopeId+"' IDBook='"+storBookId+"' IDPage='"+idPage+"' Title='"+enrichments.getEnrichmentTitle()+"' Password='' isPublished='3' PublishToEnrichementsSite='true' IsEditable='0' file='"+fileName+"' />");
					if (Globals.ISGDRIVEMODE()) {
						HashMap<String, String> dicEnr = new HashMap<String, String>();
						String storeID = "001";
						String currentDateTime = UserFunctions.getCurrentDateTimeInString();
						String enrIDGlobal = storeID + "-" + bookView.currentBook.getBookID() + "-" + idPage + "-" + enrichments.getEnrichmentId() + "-" + bookView.scopeId;
						dicEnr.put("file1", fileName);
						dicEnr.put("title", enrichments.getEnrichmentTitle());
						dicEnr.put("pageNo", String.valueOf(idPage));
						dicEnr.put("dateTime", currentDateTime);
						dicEnr.put("enrichmentIDGlobal", enrIDGlobal);
						String enrKey = "enrichment" + i;
						dicEnrichments.put(enrKey, dicEnr);
					}
					i++;
				}
			}
			enrichXmlString = enrichXmlString.concat("</Enrichments>");
			
			Zip zip = new Zip();
			String sourcePath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/";
			//String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp/zipFile222.zip";
			zip.zipFilesForEnrichments(new File(sourcePath), destPath);

		}

		/**
		 * generating xml for Links
		 * @return
		 */
		private void generatingXmlfornoteSelected() {
			int idPage;
			enrichXmlString = "<Notes>";
			for (Note notes : noteSelectedList) {
				if (notes.isNoteSelected()) {
					enrichXmlString =enrichXmlString.concat("<note>");
					idPage = notes.getPageNo();
					enrichXmlString =enrichXmlString.concat("\n"+bookView.currentBook.get_bStoreID()+"|"+idPage+"|"+notes.getsText()+"|"+notes.getOccurence()+"|"+notes.getsDesc()+"|"+notes.getnPos()+"|"+notes.getColor()+"|"+notes.getProcessStext()+"|"+notes.getTabNo()+"\n");
					enrichXmlString =enrichXmlString.concat("</note>");
				}
			}
            enrichXmlString =enrichXmlString.concat( "</Notes>");
		}
		/**
		 * generating xml for Notes
		 * @return
		 */
		private void genratingXmlforLinksSelected(){
			int idPage;
			enrichXmlString = "<Links>";
			for (Enrichments enrichments : enrichmentSelectedLinkList) {
				if (enrichments.isEnrichmentSelected()) {
					enrichXmlString =enrichXmlString.concat("<link>");
					idPage = enrichments.getEnrichmentPageNo();
					enrichXmlString =enrichXmlString.concat("\n"+enrichments.getEnrichmentId()+"|"+enrichments.getEnrichmentBid()+"|"+idPage+"|"+enrichments.getEnrichmentTitle()+"|"+enrichments.getEnrichmentType()+"|"+enrichments.getEnrichmentPath()+"|"+enrichments.getEnrichmentExportValue()+"\n");
					enrichXmlString =enrichXmlString.concat("</link>");
				}
			}
            enrichXmlString =enrichXmlString.concat("</Links>");
		}

		/**
		 * generating xml for Mindmap
		 * @return
		 */
		private void genratingXmlforMindmapSelected(){
			int idPage;
			enrichXmlString = "<MindMaps>";
			for (Enrichments enrichments : mindmapSelectedList) {
				if (enrichments.isEnrichmentSelected()) {
					enrichXmlString =enrichXmlString.concat("<mindmap>");
					idPage = enrichments.getEnrichmentPageNo();
					String enrPath = enrichments.getEnrichmentPath()+".xml";
					String mindMapPath = Globals.TARGET_BASE_MINDMAP_PATH+enrPath;
					zippingMindmapPageForExport(enrichments.getEnrichmentId(),mindMapPath,enrichments.getEnrichmentPageNo());
					String mindMapContent = UserFunctions.readFileFromPath(new File(mindMapPath));
					mindMapContent = mindMapContent.replace("\n", "");
					mindMapContent = mindMapContent.replace("<", "&lt;");
					mindMapContent = mindMapContent.replace(">", "&gt;");
					enrichXmlString =enrichXmlString.concat("\n"+enrichments.getEnrichmentId()+"|"+enrichments.getEnrichmentBid()+"|"+idPage+"|"+enrichments.getEnrichmentTitle()+"|"+enrichments.getEnrichmentType()+"|"+enrPath+"|"+enrichments.getEnrichmentExportValue()+"|"+mindMapContent+"\n");
					enrichXmlString =enrichXmlString.concat("</mindmap>");
				}
			}
			enrichXmlString =enrichXmlString.concat("</MindMaps>");
		}


		/**
		 * Initialize all values to publish book as chunks
		 */
		private void PublishEnrichmentsAsChunks(){
			try {
				chunkInputStream = new FileInputStream(filepath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			chunkBaos = new ByteArrayOutputStream();
			chunkBytes = new byte[1024*1024];
			chunkBytesRead = 0;
			chunkNoOfExecution = 1;
			new publishEnrichmentsAsChunks().execute();
		}
	}

	/**
	 * Async task class to publish book as chunks
	 * @author 
	 *
	 */
	private class publishEnrichmentsAsChunks extends AsyncTask<Void, Void, Void>{

		String bookZipBase64Chunks = null;
		byte[] byteArray = null;
		String base64Binary;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if(enrichmentLinkSelected||noteTabSelected||mindmapTabSelected){
				base64Binary=Base64.encodeToString(enrichXmlString.getBytes(), Base64.DEFAULT);
			}else{
				convertFileToBase64BinaryChunks(enrichZipFilePath);
			}
		}

		
		@Override
		protected Void doInBackground(Void... params) {
			//System.out.println(chunkBytesRead+"");
			//String storeBookId=bookView.currentBook.get_bStoreID().replace("M", "");
			String[] bookId=bookView.currentBook.get_bStoreID().split("M");
			String storeBookId=bookId[1];
			if (enrichmentLinkSelected||noteTabSelected||mindmapTabSelected){
				bookView.webService.publishResouce(0, bookView.scopeId, storeBookId, 0, resourceTitle, desc, 1, 5, base64Binary, mailid, isprivate);
			}
			else if (enrichmentTabSelected){
				if(chunkBytesRead > 0){
					// String encode=URLEncodedUtils.isEncoded(entity)
					//bookView.webService.Xmlparser.parseXml(enrichXmlString);
					bookView.webService.newUploadBookAsChunks(bookView.scopeId, enrichZipFilePath.getName(), bookZipBase64Chunks, chunkBookFlag);
				}else{
					bookView.webService.PublishNewEnrichedGroup(bookView.scopeId, enrichZipFilePath.getName(), enrichXmlString, isprivate, mailid);
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (bookView.webService.ResourceId!=0 && enrichmentLinkSelected){
				for(Enrichments enrich:enrichmentSelectedLinkList){
					enrich.setEnrichmentExportValue(bookView.webService.ResourceId);
					enrich.setEnrichmentSelected(false);
					bookView.db.executeQuery("update enrichments set Exported='"+bookView.webService.ResourceId+"' where enrichmentID='"+enrich.getEnrichmentId()+"'");
				}
				UserFunctions.DisplayAlertDialog(bookView, R.string.publish_completed_successfully, R.string.published);
				File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp");
				UserFunctions.DeleteDirectory(enrichedPagesFilePath);
				progresdialog.dismiss();
			} else if (bookView.webService.ResourceId!=0 && mindmapTabSelected){
				for(Enrichments enrich:mindmapSelectedList){
					enrich.setEnrichmentExportValue(bookView.webService.ResourceId);
					enrich.setEnrichmentSelected(false);
					bookView.db.executeQuery("update enrichments set Exported='"+bookView.webService.ResourceId+"' where enrichmentID='"+enrich.getEnrichmentId()+"'");
				}
				UserFunctions.DisplayAlertDialog(bookView, R.string.publish_completed_successfully, R.string.published);
				File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp");
				UserFunctions.DeleteDirectory(enrichedPagesFilePath);
				progresdialog.dismiss();
			}
			else if(bookView.webService.ResourceId!=0 && noteTabSelected){
				for (Note notes : noteSelectedList) {
					notes.setExportedId(bookView.webService.ResourceId);
					notes.setNoteSelected(false);
					bookView.db.executeQuery("update tblNote set Exported='"+bookView.webService.ResourceId+"' where TID='"+notes.getId()+"'");

				}
				UserFunctions.DisplayAlertDialog(bookView, R.string.publish_completed_successfully, R.string.published);
				File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp");
				UserFunctions.DeleteDirectory(enrichedPagesFilePath);
				progresdialog.dismiss();

			}else if(enrichmentTabSelected){
				if (chunkBytesRead > 0){
					if (bookView.webService.uploadJunkFileIpadResult != "") {
						if (bookView.webService.uploadJunkFileIpadResult.equals("true")) {
							chunkNoOfExecution++;
							new publishEnrichmentsAsChunks().execute();
						} else {
							progresdialog.dismiss();
							String destPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp";

							File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp");
							UserFunctions.DeleteDirectory(enrichedPagesFilePath);

							UserFunctions.DisplayAlertDialog(bookView, R.string.went_wrong, R.string.oops);
						}
					}
				}else{   
					if(bookView.webService.enrichedPageResponseServerIdList.size() > 0){
						for (int i = 0; i < bookView.webService.enrichedPageResponseServerIdList.size(); i++) {
							Enrichments enrichments = enrichmentSelectedList.get(i);
							int enrichPageServerId = bookView.webService.enrichedPageResponseServerIdList.get(i);
							enrichments.setEnrichmentExportValue(enrichPageServerId);
							enrichments.setEnrichmentSelected(false);
							bookView.db.executeQuery("update enrichments set Exported='"+enrichPageServerId+"' where enrichmentID='"+enrichments.getEnrichmentId()+"'");
						} 
						//enrichmentSelectedList.clear();
						progresdialog.dismiss();
						File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp");
						UserFunctions.DeleteDirectory(enrichedPagesFilePath);
						UserFunctions.DisplayAlertDialog(bookView, R.string.publish_completed_successfully, R.string.published);

					}else{
						progresdialog.dismiss();
						File enrichedPagesFilePath = new File(Globals.TARGET_BASE_BOOKS_DIR_PATH+bookView.currentBook.getBookID()+"/temp");
						UserFunctions.DeleteDirectory(enrichedPagesFilePath);
						UserFunctions.DisplayAlertDialog(bookView, R.string.went_wrong, R.string.oops);

					}  
				}
			}
			else {
				progresdialog.dismiss();
				UserFunctions.DisplayAlertDialog(bookView, R.string.went_wrong, R.string.oops);
			}
		}


		/**
		 * convert file to base64 binary Chunks
		 * @param file
		 * @return
		 */
		private void convertFileToBase64BinaryChunks(File file) {
			try {
				if ((chunkBytesRead = chunkInputStream.read(chunkBytes)) != -1) {
					////System.out.println("bytesRead: "+chunkBytesRead+"i: "+i);
					chunkBaos.write(chunkBytes, 0, chunkBytesRead);
					byteArray = chunkBaos.toByteArray();
					bookZipBase64Chunks = Base64.encodeToString(byteArray, Base64.DEFAULT).trim();
					if (chunkNoOfExecution == 1) {
						chunkBookFlag = "Start";
					} else if (chunkBytesRead < 1024*1024) {
						chunkBookFlag = "End";
					} else {
						chunkBookFlag = "Middle";
					}
					chunkBaos.reset();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	
	private class groupListAdapter extends BaseAdapter{
		ArrayList<UserGroups> groups=new ArrayList<UserGroups>();
		
		public groupListAdapter(BookViewReadActivity bookView,
				ArrayList<UserGroups> groups2) {
			// TODO Auto-generated constructor stub
			groups=groups2;
			
		}
		//@Override
		public int getCount() {

			return groups.size();
		}
		///@Override
		public Object getItem(int position) {

			return null;
		}

		//@Override
		public long getItemId(int position) {

			return 0;
		}

		//@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if(convertView == null){
				Typeface font = Typeface.createFromAsset(bookView.getAssets(),"fonts/FiraSans-Regular.otf");
				UserFunctions.changeFont(parent,font);
				vi = bookView.getLayoutInflater().inflate(R.layout.inflate_checklist, null);
			}
			TextView txtTitle = (TextView) vi.findViewById(R.id.textView1);
			CheckBox checkBox = (CheckBox) vi.findViewById(R.id.checkBox1);
			UserGroups group=groups.get(position);
			txtTitle.setText(group.getGroupName());
			//tv.setTextColor(Color.rgb(0, 0, 0));
			if (group.isGroupSelected()) {
				checkBox.setChecked(true);
			} else {
				checkBox.setChecked(false);
			}
			return vi;
		}
	}
	/**
	 * Get the total number of enriched page no by passing the page number
	 * @param pageNo
	 * @return
	 */
	private ArrayList<Enrichments> getTotalNoOfEnrichedPages(int pageNo){
		ArrayList<Enrichments> enrichedPageList = new ArrayList<Enrichments>();
		for (Enrichments enrichments : enrichmentList) {
			if (pageNo == enrichments.getEnrichmentPageNo()) {
				enrichedPageList.add(enrichments);
			}
		}
		return enrichedPageList;
	}

	/**
	 * Get the total number of mindmap page no by passing the page number
	 * @param pageNo
	 * @return
	 */
	private ArrayList<Enrichments> getTotalNoOfMindmapPages(int pageNo){
		ArrayList<Enrichments> mindmapPageList = new ArrayList<Enrichments>();
		for (Enrichments enrichments : mindmapList) {
			if (pageNo == enrichments.getEnrichmentPageNo()) {
				mindmapPageList.add(enrichments);
			}
		}
		return mindmapPageList;
	}

	/**
	 * Get the total number of  link page no by passing the page number
	 * @param pageNo
	 * @return
	 */
	private ArrayList<Enrichments> getTotalNoOfEnrichedLinkPages(int pageNo){
		ArrayList<Enrichments> enrichedLinkPageList = new ArrayList<Enrichments>();
		for (Enrichments enrichments : enrichmentLinksList) {
			if (pageNo == enrichments.getEnrichmentPageNo()) {
				enrichedLinkPageList.add(enrichments);
			}
		}
		return enrichedLinkPageList;
	}

	/**
	 * Get the total number of  note page no by passing the page number
	 * @param pageNo
	 * @return
	 */
	private ArrayList<Note> getTotalNoOfNotePages(int pageNo){
		ArrayList<Note> notePageList = new ArrayList<Note>();
		for (Note note : noteList) {
			if (pageNo == note.getPageNo()) {
				notePageList.add(note);
			}
		}
		return notePageList;
	}

	/**
	 * Get All the total number of Enriched page list
	 * @return
	 */
	private ArrayList<Enrichments> getTotalNumberOfEnrichedPageList(){
		ArrayList<Enrichments> enrichPageNoList = new ArrayList<Enrichments>();
		for (Enrichments enrichment : enrichmentList) {
			boolean isEnrichPageNoExist = false;
			for (Enrichments filteredEnrichments : enrichPageNoList) {
				if (filteredEnrichments.getEnrichmentPageNo() == enrichment.getEnrichmentPageNo()) {
					isEnrichPageNoExist = true;
					break;
				} else {
					isEnrichPageNoExist = false;
				}
			}
			if (!isEnrichPageNoExist && enrichment.getEnrichmentExportValue()==0) {
				enrichPageNoList.add(enrichment);
			}
		}
		return enrichPageNoList;
	}

	/**
	 * Get All the total number of Mindmap page list
	 * @return
	 */
	private ArrayList<Enrichments> getTotalNumberOfMindmapPageList(){
		ArrayList<Enrichments> mindmapPageNoList = new ArrayList<Enrichments>();
		for (Enrichments enrichment : mindmapList) {
			boolean isEnrichPageNoExist = false;
			for (Enrichments filteredEnrichments : mindmapPageNoList) {
				if (filteredEnrichments.getEnrichmentPageNo() == enrichment.getEnrichmentPageNo()) {
					isEnrichPageNoExist = true;
					break;
				} else {
					isEnrichPageNoExist = false;
				}
			}
			if (!isEnrichPageNoExist && enrichment.getEnrichmentExportValue()==0) {
				mindmapPageNoList.add(enrichment);
			}
		}
		return mindmapPageNoList;
	}

	/**
	 * Get All the total number of Enriched Link page list
	 * @return
	 */
	private ArrayList<Enrichments> getTotalNumberOfEnrichedLinkPageList(){
		ArrayList<Enrichments> enrichPageNoList = new ArrayList<Enrichments>();
		for (Enrichments enrichment : enrichmentLinksList) {
			boolean isEnrichPageNoExist = false;
			for (Enrichments filteredEnrichments : enrichPageNoList) {
				if (filteredEnrichments.getEnrichmentPageNo() == enrichment.getEnrichmentPageNo()) {
					isEnrichPageNoExist = true;
					break;
				} else {
					isEnrichPageNoExist = false;
				}
			}
			if (!isEnrichPageNoExist && enrichment.getEnrichmentExportValue()==0) {
				enrichPageNoList.add(enrichment);
			}
		}
		return enrichPageNoList;
	}

	/**
	 * Get All the total number of Note page list
	 * @return
	 */
	private ArrayList<Note> getTotalNumberOfNotePageList(){
		ArrayList<Note> notePageNoList = new ArrayList<Note>();
		for (Note note : noteList) {
			boolean isNotePageNoExist = false;
			for (Note filteredNote : notePageNoList) {
				if (filteredNote.getPageNo() == note.getPageNo()) {
					isNotePageNoExist = true;
					break;
				} else {
					isNotePageNoExist = false;
				}
			}
			if (!isNotePageNoExist && note.getExportedId()==0) {
				notePageNoList.add(note);
			}
		}
		return notePageNoList;
	}

	private class PageListSelectAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			if (enrichmentTabSelected) {
				if (enrichmentPageList != null) {
					return enrichmentPageList.size();
				} else {
					return 0;
				}
			} else if (enrichmentLinkSelected) {
				if (enrichmentPageLinkList != null) {
					return enrichmentPageLinkList.size();
				} else {
					return 0;
				}
			} else if (mindmapTabSelected) {
				if (mindmapPageList != null) {
					return mindmapPageList.size();
				} else {
					return 0;
				}
			} else {
				if (notePageList != null) {
					return notePageList.size();
				} else {
					return 0;
				}
			}
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			Typeface font = Typeface.createFromAsset(bookView.getAssets(),"fonts/FiraSans-Regular.otf");
			UserFunctions.changeFont(parent,font);
			if (enrichmentTabSelected || enrichmentLinkSelected || mindmapTabSelected) {
				//if(view == null){
				LayoutInflater layoutinflater = (LayoutInflater) bookView.getSystemService(bookView.LAYOUT_INFLATER_SERVICE);
				view = layoutinflater.inflate(R.layout.inflate_checklist, null);
				//}
				TextView txtTitle = (TextView) view.findViewById(R.id.textView1);
				CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
				if (enrichmentTabSelected) {
					Enrichments enrichment = enrichmentPageList.get(position);
					txtTitle.setText(enrichment.getEnrichmentTitle());
					if (enrichment.isEnrichmentSelected()) {
						checkBox.setChecked(true);
					} else {
						checkBox.setChecked(false);
					}
				} else if (mindmapTabSelected) {
					Enrichments enrichment = mindmapPageList.get(position);
					txtTitle.setText(enrichment.getEnrichmentTitle());
					if (enrichment.isEnrichmentSelected()) {
						checkBox.setChecked(true);
					} else {
						checkBox.setChecked(false);
					}
				} else {
					Enrichments enrichment = enrichmentPageLinkList.get(position);
					txtTitle.setText(enrichment.getEnrichmentTitle());
					if (enrichment.isEnrichmentSelected()) {
						checkBox.setChecked(true);
					} else {
						checkBox.setChecked(false);
					}
				}

			} else {
				//if(view == null){
				LayoutInflater layoutinflater = (LayoutInflater) bookView.getSystemService(bookView.LAYOUT_INFLATER_SERVICE);
				view = layoutinflater.inflate(R.layout.inflate_slct_users_res, null);
				//}
				TextView txtTitle = (TextView) view.findViewById(R.id.textView1);
				TextView txtDesc = (TextView) view.findViewById(R.id.textView2);
				CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
				ImageView iv_unread=(ImageView) view.findViewById(R.id.iv_unread);
				iv_unread.setVisibility(View.INVISIBLE);
				Note note = notePageList.get(position);
				txtTitle.setText(note.getsText());
				txtDesc.setText(note.getsDesc());
				if (note.isNoteSelected()) {
					checkBox.setChecked(true);
				} else {
					checkBox.setChecked(false);
				}
			}
			return view;
		}

	}

	private class PageListAdapter extends BaseAdapter{

		private BookViewReadActivity bookView;
		private ArrayList<Enrichments> enrPageList;
		private ArrayList<Enrichments> enrLinkPageList;
		private ArrayList<Note> notePageList;
		private ArrayList<Enrichments> mindmapPageList;

		public PageListAdapter(BookViewReadActivity bookView,
				ArrayList<Enrichments> enrPageList, ArrayList<Enrichments> enrLinkPageList, ArrayList<Note> notePageList, ArrayList<Enrichments> mindmapPageList) {
			this.bookView = bookView;
			this.enrPageList = enrPageList;
			this.enrLinkPageList = enrLinkPageList;
			this.notePageList = notePageList;
			this.mindmapPageList = mindmapPageList;
		}

		@Override
		public int getCount() {
			if (enrichmentTabSelected) {
				return enrPageList.size();
			} else if (enrichmentLinkSelected) {
				return enrLinkPageList.size();
			} else if (mindmapTabSelected){
				return mindmapPageList.size();
			} else {
				return notePageList.size();
			}

		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if(convertView == null){
				LayoutInflater layoutinflater = (LayoutInflater) bookView.getSystemService(bookView.LAYOUT_INFLATER_SERVICE);
				view = layoutinflater.inflate(R.layout.inflate_export_enr_text, null);
				Typeface font = Typeface.createFromAsset(bookView.getAssets(),"fonts/FiraSans-Regular.otf");
				UserFunctions.changeFont(parent,font);
			}
			TextView text = (TextView) view.findViewById(R.id.textView1);
			ImageView img_view= (ImageView) view.findViewById(R.id.enr_img);
			String page = bookView.getResources().getString(R.string.page);
			if (enrichmentTabSelected) {
				Enrichments enrichments = enrPageList.get(position);
				int pageno=enrichments.getEnrichmentPageNo()-1;
				text.setText(page+" "+pageno);
				img_view.setVisibility(View.VISIBLE);
				String thumbnailImagePath = Globals.TARGET_BASE_SCREENSHOTS_DIR_PATH + "Book_" + bookView.currentBook.getBookID() + "/thumbnail_" + enrichments.getEnrichmentPageNo() + ".png";
				Bitmap bitmap = BitmapFactory.decodeFile(thumbnailImagePath);
				img_view.setImageBitmap(bitmap);
			} else if (enrichmentLinkSelected) {
				Enrichments enrichments = enrLinkPageList.get(position);
				int pageno=enrichments.getEnrichmentPageNo()-1;
				text.setText(page+" "+pageno);
				img_view.setVisibility(View.GONE);
			} else if (mindmapTabSelected) {
				Enrichments enrichments = mindmapPageList.get(position);
				int pageno=enrichments.getEnrichmentPageNo()-1;
				text.setText(page+" "+pageno);
				img_view.setVisibility(View.GONE);
			} else {
				Note note = notePageList.get(position);
				int pageno=note.getPageNo()-1;
				text.setText(page+" "+pageno);
				img_view.setVisibility(View.GONE);
			}
			return view;
		}

	}

	@Override
	public void onCheckedChanged(CompoundButton view, boolean isChecked) {
		if(view.getId()==R.id.switch1){
			if(isChecked){
				rl_groups.setVisibility(View.VISIBLE);
				et_mailid.setVisibility(View.INVISIBLE);
				lv_groups.setVisibility(View.VISIBLE);
				groupSelected = true;
				emailSelected = false;
				isprivate=true;
             }else{
				rl_groups.setVisibility(View.GONE);
				groupSelected = false;
				emailSelected = false;
				isprivate=false;
			}

		}
	}
	
	public void publishmethod(){
		if(isprivate){
			if(groupSelected){
				
				if(selectedGroup.size()>0){
					boolean first = false;
					for(UserGroups groupslist:selectedGroup){
						Groupdetails= bookView.db.getselectedgroupdetails(groupslist.getGroupName(),groupslist.getUserScopeId(),bookView);
						for(UserGroups getemail:Groupdetails){
							if(!first){
							   mailid=getemail.getEmailId()+",";
							   first=true;
							}else{
							   mailid=mailid.concat(getemail.getEmailId()+",");
							}
						}
						groupslist.setGroupSelected(false);
					}
					if(resourceTitle.trim().length()<1){
						UserFunctions.DisplayAlertDialog(bookView, R.string.resource_title, R.string.title);
					}else{
						String str = bookView.getResources().getString(R.string.please_wait);
						progresdialog = ProgressDialog.show(bookView, "", str, true);
						new validateLoginAndPublishEnrichments().execute();
					}	
				}else{
					UserFunctions.DisplayAlertDialog(bookView, R.string.group_selection, R.string.oops);
				}
			}else{
				 mailid=et_mailid.getText().toString();
				if (!UserFunctions.CheckValidEmail(et_mailid.getText().toString())) {
					UserFunctions.DisplayAlertDialog(bookView, R.string.input_valid_mail_id, R.string.invalid_email_id);
				}else if(resourceTitle.trim().length()<1){
					UserFunctions.DisplayAlertDialog(bookView, R.string.resource_title, R.string.title);

				}else{
					String str = bookView.getResources().getString(R.string.please_wait);
					progresdialog = ProgressDialog.show(bookView, "", str, true);
					new validateLoginAndPublishEnrichments().execute();
				}
			}
		}else{
			mailid="";
			if(resourceTitle.trim().length()<1){
				UserFunctions.DisplayAlertDialog(bookView, R.string.resource_title, R.string.title);
			}else{
				String str = bookView.getResources().getString(R.string.please_wait);
				progresdialog = ProgressDialog.show(bookView, "", str, true);
				new validateLoginAndPublishEnrichments().execute();
			}
		}

    }


	/*public void zipFolder(String srcFolder, String destZipFolder) {
		    FileOutputStream fileWriter;
	        ZipOutputStream zip;
	        try {
	        	out=new  ZipOutputStream();
				dest=new FileOutputStream(destZipFolder);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	}*/
}
