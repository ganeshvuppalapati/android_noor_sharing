package com.semanoor.source_manahij;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.semanoor.androidwebviewselection.TextSelectionSupport;
import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.R;
import com.semanoor.manahij.mqtt.MQTTSubscriptionService;
import com.semanoor.manahij.mqtt.datamodels.Constants;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.MindMapDialog;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class SemaWebView extends WebView {

	public TextSelectionSupport mTextSelectionSupport;
	private Context context;
	public String selectedText = "";
	public boolean addNoteFromPopMenu;
	/** The context menu. */
	//private QuickAction mContextMenu;
	/** Flag to stop from showing context menu twice. */
	protected boolean contextMenuVisible = false;
	boolean highlighted;
	public static boolean noted;
	ClipboardManager mClipboard;
	String selText = "";
	Rect displayRect = new Rect();
	Rect mSelectionBounds;
	Rect popupDisplayRect = new Rect();

	private ActionMode.Callback mActionModecallback;
	private ActionMode.Callback mSelectActionModeCallback;
	private ActionMode mActionMode;
	private Menu actionModeMenu;

	public SemaWebView(Context context) {
		super(context);
		this.context = context;
		this.setUp(context);
	}

	public SemaWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		this.setUp(context);
	}

	public SemaWebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		this.setUp(context);
	}

	private void setUp(final Context context) {
		mTextSelectionSupport = TextSelectionSupport.support((BookViewReadActivity) context, this);
		mTextSelectionSupport.setSelectionListener(new TextSelectionSupport.SelectionListener() {
			@Override
			public void startSelection() {
			}

			@Override
			public void selectionChanged(String text, Rect mSelectionBounds, Rect noDensitymSelectionBounds) {
				//Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
				selectedText = text;
				//BookViewReadActivity.mScale = TextSelectionSupport.mScale;
				SemaWebView.this.mSelectionBounds = noDensitymSelectionBounds;
				BookViewReadActivity.mNoteBounds = noDensitymSelectionBounds;
				displayRect.top = (int) (noDensitymSelectionBounds.top);
				displayRect.bottom = (int) (noDensitymSelectionBounds.bottom);
				displayRect.left = (int) (noDensitymSelectionBounds.left);
				displayRect.right = (int) (noDensitymSelectionBounds.right);

				//SemaWebView.this.mSelectionBounds = mSelectionBounds;
				//BookView.mNoteBounds = mSelectionBounds;
				popupDisplayRect.top = (int) (mSelectionBounds.top);
				popupDisplayRect.bottom = (int) (mSelectionBounds.bottom);
				popupDisplayRect.left = (int) (mSelectionBounds.left);
				popupDisplayRect.right = (int) (mSelectionBounds.right);
				
				/*float scale = getDensityIndependentValue(SemaWebView.this.getScale(), context);
				//BookView.mScale = scale;
				BookView.mScale = TextSelectionSupport.mScale;
				SemaWebView.this.mSelectionBounds = mSelectionBounds;
				BookView.mNoteBounds = mSelectionBounds;
				displayRect.top = (int) (mSelectionBounds.top);
				displayRect.bottom = (int) (mSelectionBounds.bottom);
				displayRect.left = (int) (mSelectionBounds.left);
				displayRect.right = (int) (mSelectionBounds.right);*/
				
				/*selectionBoundRect.top = (int) (selectionBoundRect.top/mTextSelectionSupport.mScale);
				selectionBoundRect.bottom = (int) (selectionBoundRect.bottom/mTextSelectionSupport.mScale);
				selectionBoundRect.left = (int) (selectionBoundRect.left/mTextSelectionSupport.mScale);
				selectionBoundRect.right = (int) (selectionBoundRect.right/mTextSelectionSupport.mScale);*/
			}

			@Override
			public void endSelection() {
				selectedText = "";
			}

			@Override
			public void getSplit(String selText1) {
				getSplitt(selText1);
			}
		});
	}

	private void getSplitt(String selText1) {
		//System.out.println("sele Text1:"+selText1);
		String[] ts = selText1.split("\\|");
		if (!ts[0].isEmpty()) {
			//if(SemaWebView.this.getScrollX() != 0){
			//float scale = getDensityIndependentValue(SemaWebView.this.getScale(), context);
			//int coordsY = Math.round(mSelectionBounds.top/scale);
			String[] splitText = selText1.split("\\|");
			int coordsY = Integer.parseInt(splitText[2]);
			/*if (!(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)) {
				coordsY = Math.round(mSelectionBounds.top);
			}*/
			//final float scale = mTextSelectionSupport.getDensityIndependentValue(TextSelectionSupport.mScale, context);
			//coordsY = (int)(mTextSelectionSupport.getDensityDependentValue(coordsY, context) * scale);
			//int coordsY = Math.round(mSelectionBounds.top/BookView.mScale);

			//int newCoordsY = (int) (Integer.parseInt(splitText[2])*BookView.mScale);
			selText1 = splitText[0] + "|" + splitText[1] + "|" + coordsY + "|" + splitText[3];
			//}
			selText = BookViewReadActivity.getSelectedPosition(selText1);
			//System.out.println("selectionText:"+selText);
			String[] searchResults = selText.split("\\|");

			if (!searchResults[0].isEmpty()) {
				selectedText = searchResults[0];
				//int highCount = db.getHighlightCount(BookView.bookName, BookView.pageNumber, selText);
				int highCount = ((BookViewReadActivity) context).getHiighCount(selText);
				if (highCount == 0) {
					highlighted = false;
				} else {
					highlighted = true;
				}
				int noteCount = ((BookViewReadActivity) context).getNoteCount(selText);
				if (noteCount == 0) {
					noted = false;
				} else {
					noted = true;
				}
				//showContextMenu(displayRect);
				((BookViewReadActivity) context).runOnUiThread(new Runnable() {

					@Override
					public void run() {
						if (!(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)) {
							showPopOverContextMenu(popupDisplayRect);
						}
					}
				});
			}
		}
	}


	public void endSelectionMode(){
		mTextSelectionSupport.endSelectionMode();
	}

	private void showPopOverContextMenu(Rect selectionBoundRect) {
		final PopoverView popoverView = new PopoverView(context, R.layout.context_menu);
		popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.context_menu_width), (int) context.getResources().getDimension(R.dimen.context_menu_height)));
		//New changes for textselection on-zoom :		
		int topRestrict = (int) context.getResources().getDimension(R.dimen.popover_top_restrict);
		if(TextSelectionSupport.ScaleChanged == true){
			selectionBoundRect.top = (int) context.getResources().getDimension(R.dimen.scale_popover_top);
			selectionBoundRect.left = (int) context.getResources().getDimension(R.dimen.scale_popover_left);
			selectionBoundRect.bottom = (int) context.getResources().getDimension(R.dimen.scale_popover_bottom);
			selectionBoundRect.right = (int) context.getResources().getDimension(R.dimen.scale_popover_right);
			popoverView.showPopoverFromRectInViewGroup(((BookViewReadActivity)context).rootviewRL, selectionBoundRect, PopoverView.PopoverArrowDirectionDown, true);
		}
		else if (selectionBoundRect.top > topRestrict) {
			popoverView.showPopoverFromRectInViewGroup(((BookViewReadActivity)context).rootviewRL, selectionBoundRect, PopoverView.PopoverArrowDirectionDown, true);
		} else {
			selectionBoundRect.bottom = selectionBoundRect.bottom + (mTextSelectionSupport.mStartSelectionHandle.getDrawable().getIntrinsicHeight()*2);
			popoverView.showPopoverFromRectInViewGroup(((BookViewReadActivity)context).rootviewRL, selectionBoundRect, PopoverView.PopoverArrowDirectionUp, true);
		}
		Button btnCopy = (Button) popoverView.findViewById(R.id.btnCopy);
		Button btnSearch = (Button) popoverView.findViewById(R.id.btnSearch);
		Button btnTextToSpeech = (Button) popoverView.findViewById(R.id.btnTxtToSpeech);
		Button btnShare = (Button) popoverView.findViewById(R.id.btnShare);
		Button btnHighlight = (Button) popoverView.findViewById(R.id.btnHighlight);
		Button btnNote = (Button) popoverView.findViewById(R.id.btnNote);
		Button btnMindmap = (Button) popoverView.findViewById(R.id.btnMindmap);
		if (!Globals.isTablet()) {
			btnMindmap.setVisibility(View.GONE);
		}
		//Set language :
		btnCopy.setText(getResources().getString(R.string.copy));
		btnSearch.setText(getResources().getString(R.string.search));
		btnTextToSpeech.setText(getResources().getString(R.string.text_to_speech));
		btnShare.setText(getResources().getString(R.string.share));

		if(highlighted == false){
			btnHighlight.setText(getResources().getString(R.string.highlight));
		}
		else{
			btnHighlight.setText(getResources().getString(R.string.remove_highlight));
		}
	
		if(noted == false){
			btnNote.setText(getResources().getString(R.string.note));
		}
		else{
			btnNote.setText(getResources().getString(R.string.remove_note));
		}

		
		btnCopy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*if(BookView.LCopy.equalsIgnoreCase("yes")){ //yes
					if(allowPublish(150)){
						copySelection();
					}
				}
				else{
					mHandler.post(mUpdatePurchase);
				}*/
				copySelection();
				popoverView.dissmissPopover(true);
				endSelectionMode();
			}
		});
		btnSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*if(BookView.LNote.equalsIgnoreCase("yes")){ //yes
					((BookView)context).advancedSearch(selectedText);
				}
				else{
					mHandler.post(mUpdatePurchase);
				}*/
					((BookViewReadActivity) context).advancedSearch(selectedText);
					popoverView.dissmissPopover(true);
					endSelectionMode();

			}
		});
		btnTextToSpeech.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*if(BookView.LNote.equalsIgnoreCase("yes")){ //yes
					if(selectedText.length() < 100){
						BookView.textToSpeech(selectedText);
					}
					else{
						AlertDialog.Builder builder = new AlertDialog.Builder(context);
						builder.setMessage("TTS does not support reading more than 100 characters");
						builder.setCancelable(false);
						builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
						AlertDialog alert = builder.create();
						alert.show();
					}
				}
				else{
					mHandler.post(mUpdatePurchase);
				}*/
					if (selectedText.length() < 100) {
						((BookViewReadActivity) context).textToSpeech(selectedText, SemaWebView.this);
					} else {
						AlertDialog.Builder builder = new AlertDialog.Builder(context);
						builder.setMessage("TTS does not support reading more than 100 characters");
						builder.setCancelable(false);
						builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
						AlertDialog alert = builder.create();
						alert.show();
					}
					popoverView.dissmissPopover(true);
					endSelectionMode();
				}
		});
		btnShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*if(BookView.LNote.equalsIgnoreCase("yes")){ //yes
					((BookView) context).share(selectedText);
				}
				else{
					mHandler.post(mUpdatePurchase);
				}*/
					((BookViewReadActivity) context).share(selectedText, SemaWebView.this);
					popoverView.dissmissPopover(true);
					endSelectionMode();
				}
		});
		btnHighlight.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*if(BookView.LHighlight.equalsIgnoreCase("yes")){ //yes
					highlightClicked();
				}
				else{
					mHandler.post(mUpdatePurchase);
				}*/
					highlightClicked();
					popoverView.dissmissPopover(true);
					endSelectionMode();
				}
		});
		btnNote.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*if(BookView.LNote.equalsIgnoreCase("yes")){ //yes
					noteClicked();
				}
				else{
					mHandler.post(mUpdatePurchase);
				}*/
					noteClicked();
					popoverView.dissmissPopover(true);
					endSelectionMode();
				}
		});
		btnMindmap.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
					MindMapDialog mindMapDialog = new MindMapDialog(((BookViewReadActivity) context), false);
					if (Globals.mindMapFileNameLastOpened == null) {
						mindMapDialog.showMindMapDialog(selectedText);
					} else {
						mindMapDialog.openMindmapActivity("", selectedText, Globals.mindMapFileNameLastOpened, Globals.mindMapLastBook);
					}
					popoverView.dissmissPopover(true);
					endSelectionMode();
				}
		});
	}

	/**
	 * Shows the context menu using the given region as an anchor point.
	 */
	/*private void showContextMenu(Rect displayRect){

		// Don't show this twice
		if(this.contextMenuVisible){
			return;
		}

		// Don't use empty rect
		//if(displayRect.isEmpty()){
	

		//Copy action item
		ActionItem buttonOne = new ActionItem();
		if(BookView.lngCategoryID == 1){
			buttonOne.setTitle("Copy");
		}
		else{
			buttonOne.setTitle("Ù†Ø³Ø®");
		}
		buttonOne.setActionId(1);

		//Highlight action item
		ActionItem buttonTwo = new ActionItem();
		if(BookView.lngCategoryID == 1){
			buttonTwo.setTitle("Search");
		}
		else{
			buttonTwo.setTitle("Ø¨Ø­Ø«");
		}
		buttonTwo.setActionId(2);

		ActionItem buttonThree = new ActionItem();
		if(BookView.lngCategoryID == 1){
			buttonThree.setTitle("TextToSpeech");
		}
		else{
			buttonThree.setTitle("Ù‚Ø±Ø§Ø¡Ø© Ø§Ù„Ù†Øµ");
		}
		buttonThree.setActionId(3);

		ActionItem buttonFour = new ActionItem();
		if(BookView.lngCategoryID == 1){
			buttonFour.setTitle("Share");
		}
		else{
			buttonFour.setTitle("Ù…Ø´Ø§Ø±ÙƒØ©");
		}
		buttonFour.setActionId(4);

		ActionItem buttonFive = new ActionItem();
		if(highlighted == false){
			if(BookView.lngCategoryID == 1){
				buttonFive.setTitle("Highlight");
			}
			else{
				buttonFive.setTitle("ØªØ¸Ù„ÙŠÙ„");
			}
		}
		else{
			if(BookView.lngCategoryID == 1){
				buttonFive.setTitle("RemoveHighlight");
			}
			else{
				buttonFive.setTitle("Ø¥Ù„ØºØ§Ø¡ Ø§Ù„ØªØ¸Ù„ÙŠÙ„");
			}
		}
		buttonFive.setActionId(5);

		ActionItem buttonSix = new ActionItem();
		if(noted == false){
			if(BookView.lngCategoryID == 1){
				buttonSix.setTitle("Note");
			}
			else{
				buttonSix.setTitle("Ù…Ù„Ø§Ø­Ø¸Ø©");
			}
		}
		else{
			if(BookView.lngCategoryID == 1){
				buttonSix.setTitle("RemoveNote");
			}
			else{
				buttonSix.setTitle("Ø¥Ù„ØºØ§Ø¡ Ø§Ù„Ù…Ù„Ø§Ø­Ø¸Ø©");
			}
		}
		buttonSix.setActionId(6);



		// The action menu
		mContextMenu  = new QuickAction(this.getContext());
		mContextMenu.setOnDismissListener(this);

		// Add buttons
		mContextMenu.addActionItem(buttonOne);
		mContextMenu.addActionItem(buttonTwo);
		mContextMenu.addActionItem(buttonThree);
		mContextMenu.addActionItem(buttonFour);
		mContextMenu.addActionItem(buttonFive);
		mContextMenu.addActionItem(buttonSix);
		//setup the action item click listener
		mContextMenu.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {

			//@Override
			public void onItemClick(QuickAction source, int pos,
					int actionId) {
				try{
					mContextMenu.dismiss();
				}
				catch(Exception e){

				}
				endSelectionMode();
				if (actionId == 1) { 
					if(BookView.LCopy.equalsIgnoreCase("no")){ //yes
						if(allowPublish(150)){
							copySelection();
						}
					}
					else{
						mHandler.post(mUpdatePurchase);
					}
				} 
				else if (actionId == 2) { 
					if(BookView.LNote.equalsIgnoreCase("no")){ //yes
						BookView.advancedSearch(selectedText);
					}
					else{
						mHandler.post(mUpdatePurchase);
					}
				} 
				else if (actionId == 3) { 
					if(BookView.LNote.equalsIgnoreCase("no")){ //yes
						if(selectedText.length() < 100){
							BookView.textToSpeech(selectedText);
						}
						else{
							if(BookView.lngCategoryID == 1){
								AlertDialog.Builder builder = new AlertDialog.Builder(context);
								builder.setMessage("TTS does not support reading more than 100 characters");
								builder.setCancelable(false);
								builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.cancel();
									}
								});
								AlertDialog alert = builder.create();
								alert.show();
							}
							else{
								AlertDialog.Builder builder = new AlertDialog.Builder(context);
								builder.setMessage("Ù‚Ø±Ø§Ø¡Ø© Ø§Ù„Ù†Øµ Ù„Ø§ ØªØ³Ù…Ø­ Ø¨Ù‚Ø±Ø§Ø¡Ø© Ø£ÙƒØ«Ø± Ù…Ù† Ù¡Ù Ù  Ø­Ø±Ù�");
								builder.setCancelable(false);
								builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.cancel();
									}
								});
								AlertDialog alert = builder.create();
								alert.show();
							}
						}
					}
					else{
						mHandler.post(mUpdatePurchase);
					}
				}
				else if(actionId == 4){
					if(BookView.LNote.equalsIgnoreCase("no")){ //yes
						BookView.share(selectedText);
					}
					else{
						mHandler.post(mUpdatePurchase);
					}
				}
				else if(actionId == 5){
					if(BookView.LHighlight.equalsIgnoreCase("no")){ //yes
						highlightClicked();
					}
					else{
						mHandler.post(mUpdatePurchase);
					}
				}
				else if(actionId == 6){
					if(BookView.LNote.equalsIgnoreCase("no")){ //yes
						noteClicked();
					}
					else{
						mHandler.post(mUpdatePurchase);
					}
				}
				contextMenuVisible = false;
			}
		});
		this.contextMenuVisible = true;
		mContextMenu.show(this, displayRect);
	}*/

	protected void copySelection() {
		mClipboard = (ClipboardManager)context.getSystemService(context.CLIPBOARD_SERVICE);
		ClipData data = ClipData.newPlainText("label", this.selectedText);
		mClipboard.setPrimaryClip(data);
		//mClipboard.setText(this.selectedText);
		Toast toast = Toast.makeText(context, "Text Copied to Clipboard", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.BOTTOM, 0, 0);
		toast.show();
	}

	final Handler mHandler = new Handler();

	final Runnable mUpdateResults = new Runnable(){
		public void run() {
			if(noted == false){
				addNoteFromPopMenu = true;
				String[] ts = selText.split("\\|");
				if(ts.length==5) {
					String vc = "A|" + ts[0] + "|" + ts[1] + "|" + ts[2] + "|" + ts[4] + "";
					Log.e("vc and selectTxt", vc+ " and"+ selText.toString());
					((BookViewReadActivity) context).addNote(selText, vc, SemaWebView.this);
				}
			}
			else{
				((BookViewReadActivity) context).removeNote(selText, SemaWebView.this);
			}
		}
	};

	final Runnable mUpdateHighlight = new Runnable(){
		public void run() {
			/*if(highlighted == false){
				((BookViewReadActivity) context).addHighlights(selText, SemaWebView.this);
			}
			else{
				((BookViewReadActivity) context).removeHighlights(selText, SemaWebView.this);
			}*/



			if (highlighted == false) {
				Log.e("selected","selected");
				try {
					if(ManahijApp.getInstance().getPrefManager().getISMainChannel()) {
						MQTTSubscriptionService ss = new MQTTSubscriptionService();
						// SharedPreferences pref = context.getSharedPreferences(Constants.SHARINGNAME, MODE_PRIVATE);
						String TopicName = ManahijApp.getInstance().getPrefManager().getMainChannel();  /*pref.getString(Constants.OTHERTOPICNAME, null);*/
						JSONObject object = new JSONObject();
						object.put("type", Constants.KEYBOOK);
						object.put("action", Constants.HIGHLIGHT);
						object.put("selected_text", selText);
						object.put("device_id", ManahijApp.getInstance().getPrefManager().getDeviceID());
						ss.publishMsg(TopicName, object.toString() /*"textselect," + selText+",select"*/, context);
					}
				} catch (Exception e) {
					Log.e("ViewPager", "" + e.toString());
				}
				((BookViewReadActivity) context).addHighlights(selText, SemaWebView.this);
			} else {
				((BookViewReadActivity) context).removeHighlights(selText, SemaWebView.this);
			}
		}
	};

	protected void noteClicked() {
		mHandler.post(mUpdateResults);
	}

	protected void highlightClicked() {
		mHandler.post(mUpdateHighlight);
	}

	/*final Runnable mUpdatePurchase = new Runnable(){
		public void run() {
			((BookView) context).purchaseGeneral();
		}
	};*/

	public void playMp3(byte[] mp3SoundByteArray) {
		try {
			// create temp file that will hold byte array
			final File tempMp3 = File.createTempFile("kurchina", "mp3", context.getCacheDir());
			//tempMp3.deleteOnExit();
			FileOutputStream fos = new FileOutputStream(tempMp3);
			fos.write(mp3SoundByteArray);
			fos.close();

			// Tried reusing instance of media player
			// but that resulted in system crashes...  
			MediaPlayer mediaPlayer = new MediaPlayer();
			mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

				//@Override
				public void onCompletion(MediaPlayer mp) {
					tempMp3.delete();
				}
			});

		 	// Tried passing path directly, but kept getting 
			// "Prepare failed.: status=0x1"
			// so using file descriptor instead
			FileInputStream fis = new FileInputStream(tempMp3);
			mediaPlayer.setDataSource(fis.getFD());

			mediaPlayer.prepare();
			mediaPlayer.start();
		} catch (IOException ex) {
			//String s = ex.toString();
			ex.printStackTrace();
		}
	}

	public boolean allowPublish(int i) {
		if(selectedText.length() > i){
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle(getResources().getString(R.string.error));
				builder.setMessage(getResources().getString(R.string.you_cannot_copy)+" "+i+" "+getResources().getString(R.string.characters));
				builder.setCancelable(false);
				builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			return false;
		}
		else{
			return true;
		}
	}
	
	/**
	 * this loads the javascript by comparing the version
	 * @param script
	 * @param view
	 */
	public void loadJSScript(final String script, final WebView view){
		view.post(new Runnable() {
			
			@Override
			public void run() {
				if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT){
					view.evaluateJavascript(script, null);
				} else {
					view.loadUrl(script);
				}
			}
		});
	}

	private float getDensityIndependentValue(float val, Context ctx){
		Display display = ((WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		return val / (metrics.densityDpi / 160f);
	}

	@Override
	public ActionMode startActionMode(ActionMode.Callback callback) {
		String name = callback.getClass().toString();
		if (name.contains("SelectActionModeCallback")) {
			mSelectActionModeCallback = callback;
		}
		mActionModecallback = new CustomActionModeCallback();
		//return super.startActionMode(mActionModecallback);
		return super.startActionModeForChild(this, mActionModecallback);
	}

	@Override
	public ActionMode startActionMode(ActionMode.Callback callback, int type) {
		String name = callback.getClass().toString();
		if (name.contains("SelectActionModeCallback")) {
			mSelectActionModeCallback = callback;
		}
		mActionModecallback = new CustomMarshActionModeCallback();
		//return super.startActionMode(mActionModecallback, type);
		return super.startActionMode(mActionModecallback, ActionMode.TYPE_PRIMARY);
	}

	private void createActionMode(ActionMode mode, Menu menu){
		actionModeMenu = menu;
		mActionMode = mode;
		MenuInflater inflater = mode.getMenuInflater();
		inflater.inflate(R.menu.webview_read_menu, menu);
		if (!Globals.isTablet()) {
			MenuItem menuItem = menu.findItem(R.id.action_mindmap);
			menuItem.setVisible(false);
		}
	}

	private void prepareActionMode(ActionMode mode, final Menu menu){
		String version = "KITKAT";
		//SemaWebView.this.loadJSScript("javascript:android.selection.selectionChanged(true);", SemaWebView.this);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			SemaWebView.this.evaluateJavascript("javascript:fn('" + version + "')", new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    value = UserFunctions.getValuFromJsonreader(value);
                    getSplitt(value);
                    if (highlighted) {
                        MenuItem menuItem = menu.findItem(R.id.action_highlight);
                        menuItem.setTitle(R.string.remove_highlight);
                    } else {
                        MenuItem menuItem = menu.findItem(R.id.action_highlight);
                        menuItem.setTitle(R.string.highlight_actonbar);
                    }
                    if (noted) {
                        MenuItem menuItem = menu.findItem(R.id.action_note);
                        menuItem.setTitle(R.string.remove_note);
                    } else {
                        MenuItem menuItem = menu.findItem(R.id.action_note);
                        menuItem.setTitle(R.string.note_actonbar);
                    }
                }
            });
		}
	}

	private void actionItemClicked(ActionMode mode, MenuItem item){
		String version = "KITKAT";
		switch (item.getItemId()) {
			case R.id.action_copy:
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
					SemaWebView.this.evaluateJavascript("javascript:fn('" + version + "')", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                            value = UserFunctions.getValuFromJsonreader(value);
                            getSplitt(value);
                            copySelection();
                        }
                    });
				}
				break;
			case R.id.action_highlight:
				//if (UserFunctions.checkLoginAndSubscription(context,false,((BookViewReadActivity)context).existList,((BookViewReadActivity)context).currentBook.getClientID(),((BookViewReadActivity)context).groups.getBookReaderHighlightEnable(),((BookViewReadActivity)context).currentBook.getPurchaseType())) {
					highlightClicked();
				//}
				break;
			case R.id.action_note:
			//	if (UserFunctions.checkLoginAndSubscription(context,false,((BookViewReadActivity)context).existList,((BookViewReadActivity)context).currentBook.getClientID(),((BookViewReadActivity)context).groups.getBookReaderNoteEnable(),((BookViewReadActivity)context).currentBook.getPurchaseType())) {
					noteClicked();
				//}
				break;
			case R.id.action_mindmap:
				//if (UserFunctions.checkLoginAndSubscription(context,false,((BookViewReadActivity)context).existList,((BookViewReadActivity)context).currentBook.getClientID(),((BookViewReadActivity)context).groups.getBookReaderMindmapEnable(),((BookViewReadActivity)context).currentBook.getPurchaseType())) {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
						SemaWebView.this.evaluateJavascript("javascript:fn('" + version + "')", new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String value) {
                                value = UserFunctions.getValuFromJsonreader(value);
                                getSplitt(value);
                                MindMapDialog mindMapDialog = new MindMapDialog(((BookViewReadActivity) context), false);
                                if (Globals.mindMapFileNameLastOpened == null) {
                                    mindMapDialog.showMindMapDialog(selectedText);
                                } else {
                                    mindMapDialog.openMindmapActivity("", selectedText, Globals.mindMapFileNameLastOpened, Globals.mindMapLastBook);
                                }
                            }
                        });
					}
			//	}
				break;
			case R.id.action_search:
				//if (UserFunctions.checkLoginAndSubscription(context,false,((BookViewReadActivity)context).existList,((BookViewReadActivity)context).currentBook.getClientID(),((BookViewReadActivity)context).groups.getBookReaderAdvanceSearchEnable(),((BookViewReadActivity)context).currentBook.getPurchaseType())) {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
						SemaWebView.this.evaluateJavascript("javascript:fn('" + version + "')", new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String value) {
                                value = UserFunctions.getValuFromJsonreader(value);
                                getSplitt(value);
                                ((BookViewReadActivity) context).advancedSearch(selectedText);
                            }
                        });
					}
			//	}
				break;
			case R.id.action_share:
				if (UserFunctions.checkLoginAndSubscription(context,false,((BookViewReadActivity)context).existList,((BookViewReadActivity)context).currentBook.getClientID(),false,((BookViewReadActivity)context).currentBook.getPurchaseType())) {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
						SemaWebView.this.evaluateJavascript("javascript:fn('" + version + "')", new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String value) {
                                value = UserFunctions.getValuFromJsonreader(value);
                                getSplitt(value);
                                ((BookViewReadActivity) context).share(selectedText, SemaWebView.this);
                            }
                        });
					}
				}
				break;
			case R.id.action_textToSpeech:
				//if (UserFunctions.checkLoginAndSubscription(context,false,((BookViewReadActivity)context).existList,((BookViewReadActivity)context).currentBook.getClientID(),false,((BookViewReadActivity)context).currentBook.getPurchaseType())) {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
						SemaWebView.this.evaluateJavascript("javascript:fn('" + version + "')", new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String value) {
                                value = UserFunctions.getValuFromJsonreader(value);
                                getSplitt(value);
                                if (selectedText.length() < 100) {
                                    ((BookViewReadActivity) context).textToSpeech(selectedText, SemaWebView.this);
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("TTS does not support reading more than 100 characters");
                                    builder.setCancelable(false);
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }
                            }
                        });
					}
				//}
				break;
			default:
				break;
		}
		mode.finish();
	}

	private void destroyActionMode(ActionMode mode){
		clearFocus();
		selectedText = "";
		if (mSelectActionModeCallback != null) {
			mSelectActionModeCallback.onDestroyActionMode(mode);
		}
		mActionMode = null;
	}

	class CustomActionModeCallback implements ActionMode.Callback{

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			createActionMode(mode, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			prepareActionMode(mode, menu);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			actionItemClicked(mode, item);
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			destroyActionMode(mode);
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	class CustomMarshActionModeCallback extends ActionMode.Callback2{

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			createActionMode(mode, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, final Menu menu) {
			prepareActionMode(mode, menu);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			actionItemClicked(mode, item);
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			destroyActionMode(mode);
		}

		@Override
		public void onGetContentRect(ActionMode mode, View view, Rect outRect) {
			if (actionModeMenu != null) {
				onPrepareActionMode(mode, actionModeMenu);
			}
			super.onGetContentRect(mode, view, outRect);
		}
	}

}
