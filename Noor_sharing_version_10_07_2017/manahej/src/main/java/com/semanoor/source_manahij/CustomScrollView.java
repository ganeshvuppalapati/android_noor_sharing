package com.semanoor.source_manahij;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

import com.semanoor.manahij.MainActivity;

/**
 * Created by karthik on 14-03-2016.
 */
public class CustomScrollView extends HorizontalScrollView {
    Context context;
    public CustomScrollView(Context context) {
        super(context);
        this.context = context;
    }
    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (context instanceof MainActivity){
            if (!((MainActivity) context).editmode){
                return super.onInterceptTouchEvent(event);
            }
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (context instanceof MainActivity){
            if (!((MainActivity) context).editmode){
                return super.onTouchEvent(ev);
            }
        }
        return false;
    }
}
