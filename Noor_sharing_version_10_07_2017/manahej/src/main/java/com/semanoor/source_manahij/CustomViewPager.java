package com.semanoor.source_manahij;

import android.content.Context;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {

	public SemaWebView webView;
	
	public CustomViewPager(Context context) {
		super(context);
	}
	
	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event){
		if(this.webView.selectedText == "" || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)){
			return super.onInterceptTouchEvent(event);
		}
		return false;
	}
	
	/*@Override
	public boolean onTouchEvent(MotionEvent event){
		
		if(this.webView.selectedText == ""){
			return true;
		}
		else{
			return false;
		}
	}*/

}
