package com.semanoor.source_manahij;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserCredentials;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Suriya on 15/05/15.
 */

public class OnlineEnrichments extends AsyncTask<Void, Void, Void> {

    private Context context;
    private String bookId;
    private UserCredentials userCredentials;
    private boolean isOnlineEnrichmentsExist;
    private ProgressDialog progressDialog;

    public OnlineEnrichments(Context context) {
        this.context = context;
       // this.bookId = ((BookViewReadActivity) context).currentBook.get_bStoreID().replace("M", "");
        String[] bookDetails= ((BookViewReadActivity) context).currentBook.get_bStoreID().split("M");
        this.bookId=bookDetails[1];
       // this.userCredentials = ((BookViewReadActivity) context).userCredentials;
    }

    private void displayProgressDialog() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(context.getResources().getString(R.string.syncing_online_enr_pls_wait));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        displayProgressDialog();
        ((BookViewReadActivity)context).db.executeQuery("delete from enrichments where BID='" + ((BookViewReadActivity) context).currentBook.getBookID() + "'and Type='" + Globals.onlineEnrichmentsType + "'");
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                + "<soap:Body>"
                + "<GetEmbedCodeEnrichmentByBookId xmlns=\"http://semanoor.com.sa/\">"
                + "<iBookId>"+bookId+"</iBookId>"
                + "<iUserID>"+((BookViewReadActivity)context).scopeId+"</iUserID>"
                + "<sUserName>"+((BookViewReadActivity)context).userName+"</sUserName>"
                + "<sPassword>"+""+"</sPassword>"
                + "</GetEmbedCodeEnrichmentByBookId>"
                + "</soap:Body>"
                + "</soap:Envelope>";
        String SOAPAction = "http://semanoor.com.sa/GetEmbedCodeEnrichmentByBookId";
        String ENVELOPE = String.format(soapMessage, null);
        String returnXml = CallWebService(Globals.getNewCurriculumWebServiceURL(), SOAPAction, ENVELOPE);   //Call to web services
        //System.out.println("returnxml:" + returnXml);
//        if (returnXml.contains("><?xml version='1.0' encoding='UTF-8' ?>")){
//            returnXml = returnXml.replace("><?xml version='1.0' encoding='UTF-8' ?>",">");
//        }
        parseXml(returnXml);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (!isOnlineEnrichmentsExist){
            UserFunctions.DisplayAlertDialog(context, R.string.no_online_enr_msg, R.string.no_online_enr);
        } else {
            ((BookViewReadActivity) context).loadViewPager();
        }
        progressDialog.dismiss();
    }

    private String CallOldWebService(String url, String soapAction, String envelope)
    {
        final DefaultHttpClient httpClient=new DefaultHttpClient();

        // request parameters
        HttpParams params = httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, 10000);
        HttpConnectionParams.setSoTimeout(params, 15000);

        // set parameter
        HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

        // POST the envelope
        HttpPost httppost = new HttpPost(url);
        // add headers
        httppost.setHeader("SOAPAction", soapAction);
        httppost.setHeader("Content-Type", "text/xml; charset=utf-8");

        String responseString="";
        try
        {
            // the entity holds the request
            HttpEntity entity = new StringEntity(envelope,HTTP.UTF_8);
            httppost.setEntity(entity);

            // Response handler
            ResponseHandler<String> rh=new ResponseHandler<String>()
            {
                // invoked when client receives response
                public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException
                {
                    // get response entity
                    HttpEntity entity = response.getEntity();

                    // read the response as byte array
                    StringBuffer out = new StringBuffer();
                    byte[] b = EntityUtils.toByteArray(entity);

                    // write the response byte array to a string buffer
                    out.append(new String(b, 0, b.length));
                    return out.toString();
                }
            };
            responseString=httpClient.execute(httppost, rh);
        }
        catch(SocketException e){}
        catch (IndexOutOfBoundsException e){}
        catch(RuntimeException e){
            //System.out.println("Exception:"+e);
        }
        catch (Exception e){
            //System.out.println("Exception:"+e);
        }

        // close the connection
        httpClient.getConnectionManager().shutdown();

        responseString = responseString.replace("&lt;", "<").replace("&gt;", ">").replace("<?xml version='1.0' standalone='yes' ?>", "").replace("<?xml version=\"1.0\" encoding=\"utf-8\" ?>", "").replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
        ////System.out.println("RESPONSE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+responseString);

        return responseString;
    }

    private String CallWebService(String url, String sOAPAction, String eNVELOPE){
        HttpURLConnection urlConnection = null;
        String returnResponse = "";
        try {
            URL urlToRequest = new URL(url);

            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(25000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            urlConnection.setRequestProperty("SOAPAction", sOAPAction);

            OutputStream out = urlConnection.getOutputStream();
            out.write(eNVELOPE.getBytes());
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

            } else if (statusCode != HttpURLConnection.HTTP_OK) {

            }

            InputStream in = urlConnection.getInputStream();
            returnResponse = UserFunctions.convertStreamToString(in);
            returnResponse = returnResponse.replace("&lt;", "<").replace("&gt;", ">").replace("<?xml version='1.0' standalone='yes' ?>", "").replace("<?xml version=\"1.0\" encoding=\"utf-8\" ?>", "").replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");

        } catch (MalformedURLException e) {

        } catch (SocketTimeoutException e) {

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return returnResponse;
    }

    /*
    *  Parse Xml method for parsing the return string of Webservice calls
    */
    private void parseXml(String returnXml) {
        try {
            SAXParserFactory mySAXParserFactory = SAXParserFactory.newInstance();
            SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
            XMLReader myXMLReader = mySAXParser.getXMLReader();
            MyXmlHandler myRSSHandler = new MyXmlHandler();
            myXMLReader.setContentHandler(myRSSHandler);
            InputSource inputsource = new InputSource();
            inputsource.setEncoding("UTF-8");
            inputsource.setCharacterStream(new StringReader(returnXml));
            myXMLReader.parse(inputsource);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class MyXmlHandler extends DefaultHandler {
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);
            if (localName.equals("EnrichmentsDetails")) {
                String ID = attributes.getValue("Id").trim();
                String title = attributes.getValue("Title").trim();
                String isExport =  attributes.getValue("IsExport").trim();
                int pageNo;
                if (isExport.equalsIgnoreCase("true")){
                    pageNo = Integer.parseInt(attributes.getValue("PageNo").trim());
                }else{
                    pageNo = Integer.parseInt(attributes.getValue("PageNo").trim())+1;
                }
                String url = attributes.getValue("URL").trim();
                isOnlineEnrichmentsExist = true;
                ((BookViewReadActivity) context).db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,categoryID)values('" + ((BookViewReadActivity) context).currentBook.getBookID() + "','" + pageNo + "','" + title + "','" + Globals.onlineEnrichmentsType + "', '0','" + url + "','0')");
            }
        }

    }

}
