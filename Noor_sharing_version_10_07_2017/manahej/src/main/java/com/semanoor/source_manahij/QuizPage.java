package com.semanoor.source_manahij;

import java.util.ArrayList;

public class QuizPage {

	private String quizETitle, quizATitle;
	private ArrayList<QuizEnrichment> quizEnrichmentList;
	public String getQuizETitle() {
		return quizETitle;
	}
	public void setQuizETitle(String quizETitle) {
		this.quizETitle = quizETitle;
	}
	public String getQuizATitle() {
		return quizATitle;
	}
	public void setQuizATitle(String quizATitle) {
		this.quizATitle = quizATitle;
	}
	public ArrayList<QuizEnrichment> getQuizEnrichmentList() {
		return quizEnrichmentList;
	}
	public void setQuizEnrichmentList(ArrayList<QuizEnrichment> quizEnrichmentList) {
		this.quizEnrichmentList = quizEnrichmentList;
	}
}
