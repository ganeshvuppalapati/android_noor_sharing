package com.semanoor.source_manahij;

import android.content.Context;

public class Note {
	private int id;
	private String bName;
	private int pageNo;
	private String sText;
	private int occurence;
	private String sDesc;
	private int nPos;
	private int color;
	private String processStext;
	private int tabNo;
	private int exportedId;
	private boolean noteSelected;
	
	public Note(Context context) {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the bName
	 */
	public String getbName() {
		return bName;
	}
	/**
	 * @param bName the bName to set
	 */
	public void setbName(String bName) {
		this.bName = bName;
	}
	/**
	 * @return the pageNo
	 */
	public int getPageNo() {
		return pageNo;
	}
	/**
	 * @param pageNo the pageNo to set
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	/**
	 * @return the sText
	 */
	public String getsText() {
		return sText;
	}
	/**
	 * @param sText the sText to set
	 */
	public void setsText(String sText) {
		this.sText = sText;
	}
	/**
	 * @return the occurence
	 */
	public int getOccurence() {
		return occurence;
	}
	/**
	 * @param occurence the occurence to set
	 */
	public void setOccurence(int occurence) {
		this.occurence = occurence;
	}
	/**
	 * @return the sDesc
	 */
	public String getsDesc() {
		return sDesc;
	}
	/**
	 * @param sDesc the sDesc to set
	 */
	public void setsDesc(String sDesc) {
		this.sDesc = sDesc;
	}
	/**
	 * @return the nPos
	 */
	public int getnPos() {
		return nPos;
	}
	/**
	 * @param nPos the nPos to set
	 */
	public void setnPos(int nPos) {
		this.nPos = nPos;
	}
	/**
	 * @return the color
	 */
	public int getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(int color) {
		this.color = color;
	}
	/**
	 * @return the processStext
	 */
	public String getProcessStext() {
		return processStext;
	}
	/**
	 * @param processStext the processStext to set
	 */
	public void setProcessStext(String processStext) {
		this.processStext = processStext;
	}
	/**
	 * @return the tabNo
	 */
	public int getTabNo() {
		return tabNo;
	}
	/**
	 * @param tabNo the tabNo to set
	 */
	public void setTabNo(int tabNo) {
		this.tabNo = tabNo;
	}
	/**
	 * @return the exportedId
	 */
	public int getExportedId() {
		return exportedId;
	}
	/**
	 * @param exportedId the exportedId to set
	 */
	public void setExportedId(int exportedId) {
		this.exportedId = exportedId;
	}
	/**
	 * @return the noteSelected
	 */
	public boolean isNoteSelected() {
		return noteSelected;
	}
	/**
	 * @param noteSelected the noteSelected to set
	 */
	public void setNoteSelected(boolean noteSelected) {
		this.noteSelected = noteSelected;
	}
}
