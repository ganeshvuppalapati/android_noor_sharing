package com.semanoor.source_manahij;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.GDriveExport;
import com.semanoor.manahij.R;
import com.semanoor.manahij.SharedUsers;
import com.semanoor.sboookauthor_store.Enrichments;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.SegmentedRadioButton;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.Zip;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by Suriya on 19/05/15.
 */
public class EnrResDownload extends AsyncTask<Void, Void, Void> {

    private ArrayList<SharedUsers> sharedList;
    private BookViewReadActivity bookViewContext;
    private ArrayList<Resources> resourceList = new ArrayList<Resources>();
    private ArrayList<Enrichments> enrichmentList = new ArrayList<Enrichments>();
    private ViewPager enrichViewpager;
    private boolean isEnrSelected, isResSelected;
    private ArrayList<Resources> resSelectList;
    private ArrayList<Enrichments> enrSelectList;
    private ArrayList<Resources> resSelectedList = new ArrayList<Resources>();
    private ArrayList<Enrichments> enrSelectedList = new ArrayList<Enrichments>();
    private ProgressDialog downloadProgDialog;
    private String bookDir;
    public PopoverView popoverView;
    private ListView enrichPageListView;
    private String emailId;
    private ArrayList<HashMap<String, String>> cloudEnrichmentList;
    private ArrayList<HashMap<String, String>> cloudResourceList;
    private ArrayList<HashMap<String, String>> selectedCloudEnrichmentList;
    private ArrayList<HashMap<String, String>> selectedCloudResourceList;

    public EnrResDownload(BookViewReadActivity bookViewContext){
        this.bookViewContext = bookViewContext;
        bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookViewContext.currentBook.get_bStoreID()+"Book/";
        initailizeDownloadProgress();
    }

    private void initailizeDownloadProgress(){
        downloadProgDialog = new ProgressDialog(bookViewContext);
        downloadProgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        downloadProgDialog.setMessage("");
        downloadProgDialog.getWindow().setTitleColor(Color.rgb(222, 223, 222));
        downloadProgDialog.setIndeterminate(false);
        downloadProgDialog.setMax(100);
        downloadProgDialog.setCancelable(false);
        downloadProgDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (Globals.ISGDRIVEMODE()) {
            cloudEnrichmentList = new ArrayList<HashMap<String, String>>();
            cloudResourceList = new ArrayList<HashMap<String, String>>();
            selectedCloudEnrichmentList = new ArrayList<HashMap<String, String>>();
            selectedCloudResourceList = new ArrayList<HashMap<String, String>>();
        }
        bookViewContext.btn_text.setEnabled(false);
        if (!Globals.ISGDRIVEMODE()) {
            sharedList = bookViewContext.db.getCountForSharedList(bookViewContext.scopeId, bookViewContext);
        } else {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(bookViewContext);
            emailId = prefs.getString(Globals.sUserEmailIdKey, "");
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if (Globals.ISGDRIVEMODE()) {
            bookViewContext.webService.getCloudEnrichmentsFromUser(emailId, bookViewContext);
        } else {
            bookViewContext.webService.downloadedResourceList = new ArrayList<Resources>();
            bookViewContext.webService.downloadedEnrichmentList = new ArrayList<Enrichments>();
            String xmlUpdateRecordsPath = Globals.TARGET_BASE_BOOKS_DIR_PATH + bookViewContext.currentBook.get_bStoreID() + "Book/xmlForUpdateRecords.xml";
            if (new File(xmlUpdateRecordsPath).exists()) {
                bookViewContext.webService.getDownloadedEnrichmentsAndResourcesFromXmlPath(xmlUpdateRecordsPath);
            }
            bookViewContext.webService.getEnrichmentPagesByUserId(bookViewContext.scopeId, sharedList);
            bookViewContext.webService.getResourcesPagesByUserId(bookViewContext.scopeId, sharedList);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (Globals.ISGDRIVEMODE()) {
            String storeBookId = bookViewContext.currentBook.get_bStoreID();
            if (bookViewContext.webService.cloudEnrList != null) {
                for (int i = 0; i < bookViewContext.webService.cloudEnrList.size(); i++) {
                    HashMap<String, String> map = bookViewContext.webService.cloudEnrList.get(i);
                    String jsonContent = map.get(bookViewContext.webService.KEY_CECONTENT);
                    String message = map.get(bookViewContext.webService.KEY_CEMESSAGE);
                    JSONObject jsonObj = null;
                    try {
                        jsonObj = new JSONObject(jsonContent);
                        String type = jsonObj.getString("type");
                        String enrStoreBookId = jsonObj.getString("IDBook");
                        if (enrStoreBookId.equals(storeBookId)) {
                            if (type.equals("Enrichment")) {
                                cloudEnrichmentList.add(map);
                            } else {
                                cloudResourceList.add(map);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            updateBtnEnrResCount();
        } else {
            String[] book = bookViewContext.currentBook.get_bStoreID().split("M");
            //int bookId = Integer.parseInt(bookViewContext.currentBook.get_bStoreID().replace("M", ""));
            int bookId = Integer.parseInt(book[1]);
            for (int i = 0; i < bookViewContext.webService.enrichmentList.size(); i++) {
                Enrichments enrichment = bookViewContext.webService.enrichmentList.get(i);
                if (enrichment.getEnrichmentBid() == bookId && !enrichment.isDownloaded()) {
                    enrichmentList.add(enrichment);
                }
            }
            for (int i = 0; i < bookViewContext.webService.resourceList.size(); i++) {
                Resources resource = bookViewContext.webService.resourceList.get(i);
                if (resource.getResourceBid() == bookId && !resource.isResourceDownloaded()) {
                    resourceList.add(resource);
                }
            }
            updateBtnEnrResCount();
        }
        bookViewContext.btn_text.setEnabled(true);
    }

    public void updateBtnEnrResCount(){
        int resEnrCount;
        if (Globals.ISGDRIVEMODE()) {
            resEnrCount = cloudEnrichmentList.size() + cloudResourceList.size();
        } else {
            resEnrCount = enrichmentList.size() + resourceList.size();
        }
        if (resEnrCount > 0) {
            bookViewContext.btn_text.setBackgroundResource(R.drawable.circular_red_button);
        } else {
            bookViewContext.btn_text.setBackgroundResource(R.drawable.circular_grey_button);
        }
        bookViewContext.btn_text.setText(String.valueOf(resEnrCount));
    }

    /**
     * Method to call the Enrichment pages of the current book.
     */
    public void enrichPopup(){
        popoverView = new PopoverView(bookViewContext, R.layout.enrich_viewpager);
        int width =  (int) bookViewContext.getResources().getDimension(R.dimen.enrichpopup_width);
        int height =  (int) bookViewContext.getResources().getDimension(R.dimen.enrichpopup_height);
        popoverView.setContentSizeForViewInPopover(new Point(width, height));
        popoverView.showPopoverFromRectInViewGroup(bookViewContext.rootviewRL, PopoverView.getFrameForView(bookViewContext.btnEnrResDownload), PopoverView.PopoverArrowDirectionUp, true);

        enrichViewpager = (ViewPager)popoverView.findViewById(R.id.viewPager);
        enrichViewpager.setAdapter(new EnrichViewAdapter());
        enrichViewpager.setOffscreenPageLimit(1);
        enrichViewpager.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    private class EnrichViewAdapter extends PagerAdapter{

        ListView selectEnrichListView;
        SegmentedRadioButton btnSegmentGroup;
        Resources res;
        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Typeface font = Typeface.createFromAsset(bookViewContext.getAssets(),"fonts/FiraSans-Regular.otf");
            UserFunctions.changeFont(container,font);
            LayoutInflater inflater = (LayoutInflater) bookViewContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = null;
            switch (position) {
                case 0:
                    view = inflater.inflate(R.layout.enrich_page_listview, null);

                    ListView listView = (ListView) view.findViewById(R.id.listView3);
                    listView.setAdapter(new ListAdapter());
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            if (position == 0){
                                isEnrSelected = true;
                                isResSelected = false;
                                if (Globals.ISGDRIVEMODE()) {
                                    btnSegmentGroup.setVisibility(View.GONE);
                                    enrichViewpager.setCurrentItem(2);
                                } else {
                                    enrichPageListView.setAdapter(new EnrichPageListViewAdapter(true, false));
                                    btnSegmentGroup.setVisibility(View.VISIBLE);
                                    enrichViewpager.setCurrentItem(1);
                                }
                            } else {
                                isEnrSelected = false;
                                isResSelected = true;
                                if (Globals.ISGDRIVEMODE()) {
                                    btnSegmentGroup.setVisibility(View.GONE);
                                    enrichViewpager.setCurrentItem(2);
                                } else {
                                    //enrichPageListView.setAdapter(new EnrichPageListViewAdapter(false, true));
                                    res = (Resources) view.getTag();
                                    //selectEnrichListView.setAdapter(new SelectListViewAdapter(false, true, null, res));
                                    btnSegmentGroup.setVisibility(View.GONE);
                                    enrichViewpager.setCurrentItem(2);
                                }
                            }

                        }
                    });
                    break;
                case 1:
                    view = inflater.inflate(R.layout.enrich_page_list, null);
                    enrichPageListView = (ListView) view.findViewById(R.id.listView1);
                    final ProgressBar enrichPage1bar =(ProgressBar)view.findViewById(R.id.enrich_progressbar);
                    enrichPage1bar.setVisibility(View.GONE);
                    RelativeLayout noenrichPage1_layout = (RelativeLayout)view.findViewById(R.id.noenrich_layout);

                    btnSegmentGroup = (SegmentedRadioButton) view.findViewById(R.id.btn_segment_group);

                    final TextView noEnrichtext = (TextView) view.findViewById(R.id.noenrich);

                    btnSegmentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                            if (checkedId == R.id.btn_segment_private) {
                                new GetPrivateOrPublicEnrichments(bookViewContext.scopeId, enrichPage1bar).execute();
                            } else if (checkedId == R.id.btn_segment_public) {
                                new GetPrivateOrPublicEnrichments("0", enrichPage1bar).execute();
                            }
                        }
                    });

                    enrichPageListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            if (isEnrSelected){
                                Enrichments enr = (Enrichments) view.getTag();
                                selectEnrichListView.setAdapter(new SelectListViewAdapter(true, false, enr, null));
                            } else {
                                Resources res = (Resources) view.getTag();
                                selectEnrichListView.setAdapter(new SelectListViewAdapter(false, true, null, res));
                            }
                            enrichViewpager.setCurrentItem(2);
                        }
                    });

                    Button btnBack = (Button) view.findViewById(R.id.button4);
                    btnBack.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            enrichViewpager.setCurrentItem(0);
                        }
                    });

                    break;
                case 2:
                    view = inflater.inflate(R.layout.enrichment_list, null);

                    ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.enrich_progressbar);
                    progressBar.setVisibility(View.GONE);

                    Button btnBack1 = (Button) view.findViewById(R.id.btnback);
                    btnBack1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (Globals.ISGDRIVEMODE()) {
                                enrichViewpager.setCurrentItem(0);
                            } else {
                                if(isResSelected){
                                    enrichViewpager.setCurrentItem(0);
                                }else {
                                    enrichViewpager.setCurrentItem(1);
                                }
                            }
                        }
                    });

                    Button btnDownload = (Button) view.findViewById(R.id.btndownload);
                    btnDownload.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (Globals.ISGDRIVEMODE()) {
                                if (isEnrSelected) {
                                    if (selectedCloudEnrichmentList.size() > 0) {
                                        ProgressDialog syncProgressDialog = ProgressDialog.show(bookViewContext, "", bookViewContext.getResources().getString(R.string.syncing_please_wait), true);
                                        for (int i = 0; i < selectedCloudEnrichmentList.size(); i++) {
                                            HashMap<String, String> map = selectedCloudEnrichmentList.get(i);
                                            String jsonContent = map.get(bookViewContext.webService.KEY_CECONTENT);
                                            String message = map.get(bookViewContext.webService.KEY_CEMESSAGE);
                                            try {
                                                JSONObject jsonObj = new JSONObject(jsonContent);
                                                ArrayList<Object> bookList = bookViewContext.gridShelf.bookListArray;
                                                ProgressDialog downloadProgDialog = new ProgressDialog(bookViewContext);
                                              //  GDriveExport gdrive=new GDriveExport(bookViewContext,message);
                                                new GDriveExport.EnrichmentDownloadFromGDrive(bookViewContext, downloadProgDialog, message, jsonObj, bookList, selectedCloudEnrichmentList, i, cloudEnrichmentList, null, syncProgressDialog).execute();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                } else {
                                   if (selectedCloudResourceList.size() > 0) {
                                        ProgressDialog syncProgressDialog = ProgressDialog.show(bookViewContext, "", bookViewContext.getResources().getString(R.string.syncing_please_wait), true);
                                        for (int i = 0; i < selectedCloudResourceList.size(); i++) {
                                            HashMap<String, String> map = selectedCloudResourceList.get(i);
                                            String jsonContent = map.get(bookViewContext.webService.KEY_CECONTENT);
                                            String message = map.get(bookViewContext.webService.KEY_CEMESSAGE);
                                            try {
                                                JSONObject jsonObj = new JSONObject(jsonContent);
                                                ArrayList<Object> bookList = bookViewContext.gridShelf.bookListArray;
                                                String storeBookId = jsonObj.getString("IDBook");
                                                String contentXML = jsonObj.getString("ContentXMl");
                                                Resources resource = new Resources(bookViewContext);
                                                resource.setResourceId(10);
                                                resource.new getResourceXmlFromUrl(bookViewContext.db, bookViewContext.webService, bookList, contentXML, storeBookId, selectedCloudResourceList, i, cloudResourceList, null, syncProgressDialog).execute();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (isEnrSelected) {
                                    for (int i = 0; i < enrSelectedList.size(); i++) {
                                        Enrichments enrichment = enrSelectList.get(i);
                                        downloadEnrichFiles(enrichment);
                                    }
                                } else {
                                    downloadResource(resSelectedList);
                                }
                            }
                            bookViewContext.loadViewPager();
                        }
                    });

                    selectEnrichListView = (ListView)view.findViewById(R.id.listView2);
                    if (Globals.ISGDRIVEMODE()) {
                        selectEnrichListView.setAdapter(new SelectListViewAdapter(isEnrSelected, isResSelected, null, null));
                    } else {
                        if(isResSelected){
                            selectEnrichListView.setAdapter(new SelectListViewAdapter(false, true, null, res));
                        }
                    }
                    selectEnrichListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            if (isEnrSelected){
                                if (Globals.ISGDRIVEMODE()) {
                                    HashMap<String, String> map = cloudEnrichmentList.get(position);
                                    String cloudEnrselected = map.get(bookViewContext.webService.KEY_CLOUDENRSELECTED);
                                    if (cloudEnrselected.equals("false")) {
                                        map.put(bookViewContext.webService.KEY_CLOUDENRSELECTED, "true");
                                        selectedCloudEnrichmentList.add(map);
                                    } else {
                                        map.put(bookViewContext.webService.KEY_CLOUDENRSELECTED, "false");
                                        selectedCloudEnrichmentList.remove(map);
                                    }
                                } else {
                                    Enrichments enrichment = enrSelectList.get(position);
                                    if (enrichment.isEnrichmentSelected()){
                                        enrichment.setEnrichmentSelected(false);
                                        enrSelectedList.remove(enrichment);
                                    } else {
                                        enrichment.setEnrichmentSelected(true);
                                        if (!enrichment.isDownloaded()) {
                                            enrSelectedList.add(enrichment);
                                        }
                                    }
                                }
                            } else {
                                if (Globals.ISGDRIVEMODE()) {
                                    HashMap<String, String> map = cloudResourceList.get(position);
                                    String cloudEnrselected = map.get(bookViewContext.webService.KEY_CLOUDENRSELECTED);
                                    if (cloudEnrselected.equals("false")) {
                                        map.put(bookViewContext.webService.KEY_CLOUDENRSELECTED, "true");
                                        selectedCloudResourceList.add(map);
                                    } else {
                                        map.put(bookViewContext.webService.KEY_CLOUDENRSELECTED, "false");
                                        selectedCloudResourceList.remove(map);
                                    }
                                } else {
                                    Resources resource = resourceList.get(position);
                                    if (resource.isResourceSelected()){
                                        resource.setResourceSelected(false);
                                        resSelectedList.remove(resource);
                                    } else {
                                        resource.setResourceSelected(true);
                                        resSelectedList.add(resource);
                                    }
                                }
                            }
                            ((BaseAdapter) adapterView.getAdapter()).notifyDataSetChanged();
                        }
                    });
                    break;
            }
            ((ViewPager)container).addView(view, 0);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object){
            ((ViewPager)container).removeView((View) object);
        }
    }

    private class GetPrivateOrPublicEnrichments extends AsyncTask<Void, Void, Void>{

        String userScopeId;
        ProgressBar progressBar;

        public GetPrivateOrPublicEnrichments(String userScopeId, ProgressBar progressBar){
            enrichmentList.clear();
            this.userScopeId = userScopeId;
            this.progressBar = progressBar;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            bookViewContext.webService.downloadedResourceList = new ArrayList<Resources>();
            bookViewContext.webService.downloadedEnrichmentList = new ArrayList<Enrichments>();
            String xmlUpdateRecordsPath = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookViewContext.currentBook.get_bStoreID()+"Book/xmlForUpdateRecords.xml";
            if (new File(xmlUpdateRecordsPath).exists()) {
                bookViewContext.webService.getDownloadedEnrichmentsAndResourcesFromXmlPath(xmlUpdateRecordsPath);
            }
            bookViewContext.webService.getEnrichmentPagesByUserId(userScopeId, sharedList);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //int bookId = Integer.parseInt(bookViewContext.currentBook.get_bStoreID().replace("M", ""));
            String[] data=bookViewContext.currentBook.get_bStoreID().split("M");
            int bookId = Integer.parseInt(data[1]);
            for (int i = 0; i < bookViewContext.webService.enrichmentList.size(); i++) {
                Enrichments enrichment = bookViewContext.webService.enrichmentList.get(i);
                if (enrichment.getEnrichmentBid() == bookId){
                    enrichmentList.add(enrichment);
                }
            }
            enrichPageListView.setAdapter(new EnrichPageListViewAdapter(true, false));
            progressBar.setVisibility(View.GONE);
        }
    }

    private void downloadResource(ArrayList<Resources> resList){
        ProgressDialog resourceProgressDialog = null;
        if (resList.size() > 0){
            resourceProgressDialog = ProgressDialog.show(bookViewContext, "", bookViewContext.getResources().getString(R.string.syncing_resource), true);
        }
        boolean dismissProgressDialog = false;
        for (int i = 0; i < resList.size(); i++) {
            Resources resource = resList.get(i);
            //resource.getResourceXmlFromUrl(bookViewContext.db, bookViewContext.webService, null, bookViewContext.currentBook.getBookID());
            if (i == resList.size()-1){
                dismissProgressDialog = true;
            }
            resource.new getResourceXmlFromUrl(bookViewContext.db, bookViewContext.webService, null, resourceProgressDialog, bookViewContext.currentBook.getBookID(), bookViewContext.currentBook, dismissProgressDialog).execute();
            resourceList.remove(resource);
            resList.remove(resource);
        }

        updateBtnEnrResCount();
        popoverView.dissmissPopover(true);
    }

    private class SelectListViewAdapter extends BaseAdapter{

        boolean isForEnrichment; boolean isForResource;
        Enrichments selEnr; Resources selRes;

        private SelectListViewAdapter(boolean isForEnrichment, boolean isForResource, Enrichments selEnr, Resources selRes){
            this.isForEnrichment = isForEnrichment;
            this.isForResource = isForResource;
            if (!Globals.ISGDRIVEMODE()){
                if (isForEnrichment){
                    enrSelectList = getTotalNoOfEnrichedPages(selEnr.getEnrichmentPageNo());
                } else {
                    //resSelectList = getTotalNoOfResourcePages(selRes.getResourcePageNo());
                }
            }
        }

        @Override
        public int getCount() {
            if (isForEnrichment){
                if (Globals.ISGDRIVEMODE()) {
                    return cloudEnrichmentList.size();
                } else {
                    return enrSelectList.size();
                }
            } else {
                if (Globals.ISGDRIVEMODE()) {
                    return cloudResourceList.size();
                } else {
                    return resourceList.size();
                }
            }
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            View view = convertView;
            if (convertView == null) {
                Typeface font = Typeface.createFromAsset(bookViewContext.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(viewGroup,font);
                if (Globals.ISGDRIVEMODE()) {
                    view = bookViewContext.getLayoutInflater().inflate(R.layout.inflate_slct_users_res, null);
                } else {
                    if (isForEnrichment) {
                        view = bookViewContext.getLayoutInflater().inflate(R.layout.inflate_checklist, null);
                    } else {
                        view = bookViewContext.getLayoutInflater().inflate(R.layout.inflate_slct_users_res, null);
                    }
                }
            }
            if (Globals.ISGDRIVEMODE()) {
                HashMap<String, String> map;
                if (isForEnrichment){
                    map = cloudEnrichmentList.get(position);
                } else {
                    map = cloudResourceList.get(position);
                }
                String jsonContent = map.get(bookViewContext.webService.KEY_CECONTENT);
                String message = map.get(bookViewContext.webService.KEY_CEMESSAGE);
                String sharedBy = "";
                String dateTime = "";
                try {
                    JSONObject jsonObj = new JSONObject(jsonContent);
                    sharedBy = jsonObj.getString("sharedBy");
                    dateTime = jsonObj.getString("dateTime");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                TextView txtSharedBy = (TextView) view.findViewById(R.id.textView2);
                String txtDateSharedBy = sharedBy+" "+dateTime;
                txtSharedBy.setText(txtDateSharedBy);
                TextView txtTitle = (TextView) view.findViewById(R.id.textView1);
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
                txtTitle.setText(message);
                String isSelected = map.get(bookViewContext.webService.KEY_CLOUDENRSELECTED);
                if (isSelected.equals("false")) {
                    checkBox.setChecked(false);
                } else {
                    checkBox.setChecked(true);
                }
            } else {
                if (isForEnrichment){
                    TextView textView = (TextView) view.findViewById(R.id.textView1);
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
                    Enrichments enr = enrSelectList.get(position);
                    if (enr.isDownloaded()){
                        checkBox.setVisibility(View.INVISIBLE);
                    }else{
                        checkBox.setVisibility(View.VISIBLE);
                    }
                    if (enr.isDownloaded()){
                        view.setBackgroundColor(Color.parseColor("#A54970"));
                    }else{
                        view.setBackgroundColor(Color.TRANSPARENT);
                    }
                    textView.setText(enr.getEnrichmentTitle());
                    if (enr.isEnrichmentSelected()){
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                } else {
                    TextView txtSharedBy = (TextView) view.findViewById(R.id.textView2);
                    TextView txtTitle = (TextView) view.findViewById(R.id.textView1);
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
                    Resources res = resourceList.get(position);
                    txtSharedBy.setText("Shared by: "+res.getResourceUserName());
                    txtTitle.setText(res.getResourceTitle());
                    if (res.isResourceSelected()){
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                }
            }

            return view;
        }
    }

    /**
     * Get the total number of enriched page no by passing the page number
     * @param pageNo
     * @return
     */
    private ArrayList<Enrichments> getTotalNoOfEnrichedPages(int pageNo){
        ArrayList<Enrichments> enrichedPageList = new ArrayList<Enrichments>();
        for (Enrichments enrichments : enrichmentList) {
            if (pageNo == enrichments.getEnrichmentPageNo()) {
                enrichedPageList.add(enrichments);
            }
        }
        return enrichedPageList;
    }

    /**
     * Get the total number of enriched page no by passing the page number
     * @param pageNo
     * @return
     */
    private ArrayList<Resources> getTotalNoOfResourcePages(int pageNo){
        ArrayList<Resources> resourcePageList = new ArrayList<Resources>();
        for (Resources resource : resourceList) {
            if (pageNo == resource.getResourcePageNo()) {
                resourcePageList.add(resource);
            }
        }
        return resourcePageList;
    }

    private class EnrichPageListViewAdapter extends BaseAdapter{

        private ArrayList<Resources> resPageList;
        private ArrayList<Enrichments> enrPageList;
        boolean isForEnrichment; boolean isForResource;

        private EnrichPageListViewAdapter(boolean isForEnrichment, boolean isForResource){
            this.isForEnrichment = isForEnrichment;
            this.isForResource = isForResource;
            if (isForEnrichment){
                enrPageList = getTotalNumberOfEnrichedPageList();
            } else {
                resPageList = getTotalNumberOfResourcePageList();
            }
        }

        @Override
        public int getCount() {
            if (isForEnrichment){
                return enrPageList.size();
            } else {
                return resPageList.size();
            }
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            View view = convertView;
            if (view == null) {
                Typeface font = Typeface.createFromAsset(bookViewContext.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(viewGroup,font);
                view = bookViewContext.getLayoutInflater().inflate(R.layout.enrich_page_text, null);
            }
            TextView textView = (TextView) view.findViewById(R.id.textView1);
            String page = bookViewContext.getResources().getString(R.string.page);
            if (isForEnrichment){
                Enrichments enr = enrPageList.get(position);
                int pageNo = enr.getEnrichmentPageNo() - 1;
                textView.setText(page+" "+pageNo);
                view.setTag(enr);
            } else {
                Resources res = resPageList.get(position);
                int pageNo = res.getResourcePageNo();
                textView.setText(page+" "+pageNo);
                view.setTag(res);
            }
            return view;
        }
    }

    /**
     * Get All the total number of Enriched page list
     * @return
     */
    private ArrayList<Enrichments> getTotalNumberOfEnrichedPageList(){
        ArrayList<Enrichments> enrichPageNoList = new ArrayList<Enrichments>();
        for (Enrichments enrichment : enrichmentList) {
            boolean isEnrichPageNoExist = false;
            for (Enrichments filteredEnrichments : enrichPageNoList) {
                if (filteredEnrichments.getEnrichmentPageNo() == enrichment.getEnrichmentPageNo()) {
                    isEnrichPageNoExist = true;
                } else {
                    isEnrichPageNoExist = false;
                }
            }
            if (!isEnrichPageNoExist) {
                enrichPageNoList.add(enrichment);
            }
        }
        return enrichPageNoList;
    }

    /**
     * Get All the total number of Enriched page list
     * @return
     */
    private ArrayList<Resources> getTotalNumberOfResourcePageList(){
        ArrayList<Resources> resPageNoList = new ArrayList<Resources>();
        for (Resources resource : resourceList) {
            boolean isResPageNoExist = false;
            for (Resources filteredRes : resPageNoList) {
                if (filteredRes.getResourcePageNo() == resource.getResourcePageNo()) {
                    isResPageNoExist = true;
                } else {
                    isResPageNoExist = false;
                }
            }
            if (!isResPageNoExist) {
                resPageNoList.add(resource);
            }
        }
        return resPageNoList;
    }



    private class ListAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            View view = convertView;
            if (view == null) {
                Typeface font = Typeface.createFromAsset(bookViewContext.getAssets(),"fonts/FiraSans-Regular.otf");
                UserFunctions.changeFont(viewGroup,font);
                view = bookViewContext.getLayoutInflater().inflate(R.layout.enr_res_page_text, null);
            }
            TextView textView = (TextView) view.findViewById(R.id.textView1);
            TextView txtCount = (TextView) view.findViewById(R.id.textView2);
            Button btnResSync = (Button) view.findViewById(R.id.button);
            btnResSync.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    downloadResource(resourceList);
                }
            });
            if (Globals.ISGDRIVEMODE()) {
                btnResSync.setVisibility(View.GONE);
                if (position == 0){
                    textView.setText(R.string.enrichment);
                    txtCount.setText(String.valueOf(cloudEnrichmentList.size()));
                } else {
                    txtCount.setText(String.valueOf(cloudResourceList.size()));
                    textView.setText(R.string.resources);
                }
            } else {
                btnResSync.setVisibility(View.VISIBLE);
                if (position == 0){
                    textView.setText(R.string.enrichment);
                    txtCount.setText(String.valueOf(enrichmentList.size()));
                    btnResSync.setVisibility(View.GONE);
                } else {
                    btnResSync.setVisibility(View.VISIBLE);
                    txtCount.setText(String.valueOf(resourceList.size()));
                    textView.setText(R.string.resources);
                }
            }
            return view;
        }
    }

    /**
     * Call to download enrichFiles :
     */
    public void downloadEnrichFiles(Enrichments enrichment){
        downloadProgDialog.setTitle(enrichment.getEnrichmentTitle());
        downloadProgDialog.setProgress(0);
        downloadProgDialog.setMax(100);
        downloadProgDialog.show();
        //Get the fileName where the files has to be downloaded. Create sub folders if not created.
        String pageDir =bookDir+"Enrichments/Page"+enrichment.getEnrichmentPageNo();
        File enrichFile = new File(pageDir);
        if(!enrichFile.exists()){
            enrichFile.mkdirs();
        }

        String XMLfilePath = bookDir+"xmlForUpdateRecords.xml";
        File xmlFile = new File(XMLfilePath);
        if(!xmlFile.exists()){
            //Copy xml file from assets to book folder :
            copyFiles("xmlForUpdateRecords.xml", XMLfilePath);
        }
        new progressDownload(enrichment).execute(enrichment.getEnrichmentPath()); //bookURL
    }

    /*
    * AsyncTask for downloading Enriches
    */
    private  class  progressDownload extends AsyncTask<String, Integer, String>{
        public String DprogressString="";
        public Boolean downloadSuccess;
        private Enrichments enrichment;

        public String enrichFileName;

        public progressDownload(Enrichments enrichment) {
            this.enrichment = enrichment;
            enrichFileName =bookDir+"Enrichments/Page"+enrichment.getEnrichmentPageNo()+ "/Enriched"+enrichment.getEnrichmentId();
        }

        protected void onPreExecute(){
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... downloadurl) {

            final int TIMEOUT_CONNECTION = 5000;//5sec
            final int TIMEOUT_SOCKET = 30000;//30sec
            long bytes = 1024 * 1024;
            try{
                URL url = new URL(downloadurl[0]);
                //Open a connection to that URL.
                URLConnection ucon = url.openConnection();
                ucon.connect();
                int   fileLength = ucon.getContentLength();
                //////System.out.println("filelength:"+fileLength);
                long startTime = System.currentTimeMillis();

                //this timeout affects how long it takes for the app to realize there's a connection problem
                ucon.setReadTimeout(TIMEOUT_CONNECTION);
                ucon.setConnectTimeout(TIMEOUT_SOCKET);
                //Define InputStreams to read from the URLConnection.
                // uses 3KB download buffer
                InputStream is = ucon.getInputStream();
                BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
                //Internal storage
                String filePath = enrichFileName+".zip";
                FileOutputStream outStream = new FileOutputStream(filePath);
                byte[] buff = new byte[5 * 1024];
                //Read bytes (and store them) until there is nothing more to read(-1)
                int len;
                String totalMB,fileLengthMB;
                long total = 0;
                DecimalFormat twoDformat = new DecimalFormat("#.##");
                while ((len = inStream.read(buff)) != -1)
                {
                    total += len;
                    ////////System.out.println("total:"+total);
                    totalMB=twoDformat.format((float)total/bytes);
                    fileLengthMB= twoDformat.format((float)fileLength/bytes);
                    DprogressString=totalMB+" MB"+" of "+fileLengthMB+" MB";
                    publishProgress((int)((total*100)/fileLength));
                    outStream.write(buff,0,len);
                }
                //clean up
                outStream.flush();
                outStream.close();
                inStream.close();
                downloadSuccess = true;
                ////System.out.println( "download completed in "+ ((System.currentTimeMillis() - startTime) / 1000)+ " sec");

            }
            catch (IOException e){
                ////System.out.println("error while storing:"+e);
                downloadSuccess = false;
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress){
            super.onProgressUpdate(progress);
            if(downloadProgDialog != null){
                ////System.out.println("OnprogressUpdate:"+progress[0]);
                downloadProgDialog.setMessage(DprogressString);
                downloadProgDialog.setProgress(progress[0]);
            }
        }

        protected void onPostExecute(String result){
            //////System.out.println("OnpostExecute");
            if(downloadSuccess){ //unzipping will be done when downloadSuccess is true.

                String zipfile = enrichFileName + ".zip";
                String unziplocation = enrichFileName + "/"; //"/" is a must here..
                Boolean unzipSuccess= Zip.unzipDownloadfileTemp(zipfile,unziplocation); //if unzipping success then need to store in database:
                if(unzipSuccess){

                    //DB entry :
                }
                //Need to delete the zip file which is download
                File zipfilelocation = new File(enrichFileName+".zip");  //Deleting zip file in the internal storage
                try{
                    zipfilelocation.delete();
                }catch(Exception e){
                    //////System.out.println("Failed to deletefile with Error "+e);
                }
                checkAndUpdateBooksXml(enrichment);
                checkAndUpdateXmlForUpdateRecords(enrichment);

                ////System.out.println("enrichSelectArray count:"+enrichSelectArray.size());
                //enrichSelectArray.remove(0);
             //   enrichmentList.remove(enrichment);
                enrSelectedList.remove(enrichment);
                enrichment.setDownloaded(true);
                if(enrSelectedList.size() == 0){
                    downloadProgDialog.dismiss();
                    //Call method to reload enrichments :
                    bookViewContext.loadViewPager();
                    updateBtnEnrResCount();
                    popoverView.dissmissPopover(true);
                }
            }
        }
    }

    public void checkAndUpdateXmlForUpdateRecords(Enrichments enrichment){

        //revert_book_hide :
        //String bookXmlPath =  bookView.getFilesDir().getPath()+"/M"+BookID+"Book"+"/xmlForUpdateRecords.xml"; //data/data/com.semanoor.manahij/files/
        //String bookXmlPath =  bookView.getFilesDir().getPath()+"/.M"+BookID+"Book"+"/xmlForUpdateRecords.xml";
        String bookXmlPath = bookDir+"xmlForUpdateRecords.xml";
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            Document document = documentBuilder.parse(new File(bookXmlPath));
            String tagName = "pages";
            String toModifyTagName = "page"+enrichment.getEnrichmentPageNo();
            Boolean isTagExists = false;
            Node pagesNode = document.getElementsByTagName(tagName).item(0);

            NodeList pagesnodesList = pagesNode.getChildNodes();
            for(int i = 0;i< pagesnodesList.getLength(); i++){

                Node element = pagesnodesList.item(i);
                if(element.getNodeName().equals(toModifyTagName)){
                    //tag exists
                    isTagExists = true;
                    break;
                }else{
                    //Tag doesn't exist. create a new tag.
                    isTagExists = false;
                }
            }

            if(!isTagExists){
                Element pageElement = document.createElement(toModifyTagName);
                pagesNode.appendChild(pageElement);
            }

            //Check and delete pre-existing enrich entries :
			           /* for (int i = 0; i < nodes.getLength(); i++) {

			            	Node element = nodes.item(i);
			                NamedNodeMap attribute = element.getAttributes();
				            Node nodeAttr = attribute.getNamedItem("ID");
				            ////System.out.println(attribute.getNamedItem("ID").toString());
				            if(attribute.getNamedItem("ID").toString().equals("337")){
				            	element.removeChild(element);

				            }
			                if(element.getNodeName().equals("ID")){

			                	if(element.getNodeValue().equals("377")){
			                		element.removeChild(element);
			                	}
			                }
			            }*/
            //create new and update elements :
            Node currentPageNode = document.getElementsByTagName(toModifyTagName).item(0);
            //String[] enrichValues = enrichDetails.split("##");
            String eleName = "enriched";
            String eleID = String.valueOf(enrichment.getEnrichmentId());
            String eleTitle = enrichment.getEnrichmentTitle();
            String eleDateUp = enrichment.getEnrichmentDateUp();
            String eleUserName = enrichment.getEnrichmentUserName();
            String eleSize = enrichment.getEnrichmentSize();
            String elePath = enrichment.getEnrichmentPath();
            String eleIDUser = String.valueOf(enrichment.getEnrichmentUserId());
            String eleDate = enrichment.getEnrichmentDate();
            String elePassword = "";
            String idPageNo = String.valueOf(enrichment.getEnrichmentPageNo());
            String bookTitle = enrichment.getEnrichmentBookTitle();
            String bookId = String.valueOf(enrichment.getEnrichmentBid());
            String emailId = enrichment.getEnrichmentEmailId();

            Element enrElement = document.createElement(eleName);
            enrElement.setAttribute("IDPage", idPageNo);
            enrElement.setAttribute("ID", eleID);
            enrElement.setAttribute("Title", eleTitle);
            enrElement.setAttribute("DateUp", eleDateUp);
            enrElement.setAttribute("UserName", eleUserName);
            enrElement.setAttribute("Size", eleSize);
            enrElement.setAttribute("Path", elePath);
            enrElement.setAttribute("IDUser", eleIDUser);
            enrElement.setAttribute("Date", eleDate);
            enrElement.setAttribute("Password", elePassword);
            enrElement.setAttribute("BookTitle", bookTitle);
            enrElement.setAttribute("BookID", bookId);
            enrElement.setAttribute("Email", emailId);

            currentPageNode.appendChild(enrElement);

            // write the DOM object to the file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);

            StreamResult streamResult = new StreamResult(new File(bookXmlPath));
            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        }
    }

    public void checkAndUpdateBooksXml(Enrichments enrichment){

        String enrPath = "Enrichments/Page"+enrichment.getEnrichmentPageNo()+"/Enriched"+enrichment.getEnrichmentId()+"/index.htm";
        bookViewContext.db.executeQuery("insert into enrichments(BID, pageNO, Title, Type, Exported,Path,categoryID)values('" + bookViewContext.currentBook.getBookID() + "','" + enrichment.getEnrichmentPageNo() + "','" + enrichment.getEnrichmentTitle() + "','"+Globals.downloadedEnrichmentType+"', '0','" + enrPath + "','0')");
        //revert_book_hide :
        //String bookXmlPath =  bookView.getFilesDir().getPath()+"/M"+BookID+"Book"+"/Book.xml"; //data/data/com.semanoor.manahij/files/
        //String bookXmlPath =  bookView.getFilesDir().getPath()+"/.M"+BookID+"Book"+"/Book.xml";
        /*String bookXmlPath = bookDir+"Book.xml";
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            Document document = documentBuilder.parse(new File(bookXmlPath));
            String tagName = "P"+(enrichment.getEnrichmentPageNo()-1);
            Node pageNode = document.getElementsByTagName(tagName).item(0);
            NodeList nodes = pageNode.getChildNodes();

            //create new and update elements :
            //String[] enrichValues = enrichDetails.split("##");
            String eleName = "Enr"+"1";
            String elePath = "Enrichments/Page"+enrichment.getEnrichmentPageNo()+"/Enriched"+enrichment.getEnrichmentId()+"/index.htm";
            String eleTitle = enrichment.getEnrichmentTitle();
            String eleID = String.valueOf(enrichment.getEnrichmentId());
            String eleType = Globals.downloadedEnrichmentType;
            Element enrElement = document.createElement(eleName);
            enrElement.setAttribute("Path", elePath);
            enrElement.setAttribute("Title", eleTitle);
            enrElement.setAttribute("ID", eleID);
            enrElement.setAttribute("Type", eleType);
            pageNode.appendChild(enrElement);

            // write the DOM object to the file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);

            StreamResult streamResult = new StreamResult(new File(bookXmlPath));
            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        }*/
    }

    private Boolean unzipDownloadfileNew(String zipfilepath, String unziplocation)
    {
        InputStream is;
        ZipInputStream zis;
        try
        {
            is = new FileInputStream(zipfilepath);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null)
            {
                String zipEntryName = ze.getName();
                zipEntryName = zipEntryName.replace("\\", "/");
                //////System.out.println("zipEntryName: "+zipEntryName);
                File  file = new File(unziplocation+zipEntryName);
                if(file.exists()){
                    //////System.out.println("file exists at the given path:" +file);
                }else{

                    if(ze.isDirectory()){
                        //////System.out.println("Ze is an directory. It will create directory and exit");
                        file.mkdirs();
                        continue;
                    }

                    file.getParentFile().mkdirs();   //Horror line..............
                    byte[] buffer = new byte[1024];
                    FileOutputStream fout = new FileOutputStream(file);
                    BufferedOutputStream baos  = new BufferedOutputStream(fout, 1024);
                    int count;
                    while((count = zis.read(buffer, 0, 1024))!= -1){
                        baos.write(buffer, 0, count);
                    }
                    baos.flush();
                    baos.close();
                }
            }
            zis.close();
            //////System.out.println("finished unzipping");
            return true;
        }catch(Exception e){
            //////System.out.println("unzipping error"+e);
            return false;
        }
    }


    /**
     * Method to copy file xmlForUpdateRecords.xml to the book
     * @param fromPath
     * @param toPath
     * @return
     */
    public boolean copyFiles(String fromPath, String toPath){
        InputStream in =null;
        OutputStream out =null;
        try{
            in = bookViewContext.getAssets().open(fromPath);
            new File(toPath).createNewFile();
            out = new FileOutputStream(toPath);
            byte[] buffer = new byte[1024];
            int read;
            while((read = in.read(buffer)) != -1){
                out.write(buffer,0,read);
            }
            in.close();
            in=null;
            out.flush();
            out.close();
            out = null;
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
