package com.semanoor.source_manahij;

import com.semanoor.manahij.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class AdvancedSearchWV extends Activity {
	
	WebView myWebView;
	String url;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.adv_search_webview);
		Intent i = getIntent();
		url = i.getExtras().getString("urlstring");
		myWebView = (WebView) findViewById(R.id.webViewAdvSearch);
		WebSettings websettings = myWebView.getSettings();
		websettings.setJavaScriptEnabled(true);
		websettings.setSupportZoom(true);
		websettings.setAllowFileAccess(true);
		websettings.setLoadsImagesAutomatically(true);
		myWebView.setWebViewClient(new WebViewClient());
		openURL();
		
		Button btnBack = (Button) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new OnClickListener() {
			
			//@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

	private void openURL() {
		// TODO Auto-generated method stub
		myWebView.loadUrl(url);
		myWebView.requestFocus();
	}

}
