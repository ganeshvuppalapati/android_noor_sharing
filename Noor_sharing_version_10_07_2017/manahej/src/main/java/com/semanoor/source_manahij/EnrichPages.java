package com.semanoor.source_manahij;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.semanoor.manahij.BookView;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class EnrichPages  {
	private String soapMessage="";
	private String ENVELOPE="";
	private String url="";
	private String SOAPAction="";
	private String BookID;
	private String GlobalId;
	private static BookView bookView;
    private int serviceType;
    public ArrayList<String> enrichPagesArray= new ArrayList<String>();
    public ArrayList<String> enrichDownloadArray = new ArrayList<String>();
    public ArrayList<String> enrichSelectArray = new ArrayList<String>();
    private String xmlUpdate = "";
    private String	currentEnrType;
    public int currentPageNumber;
    private String bookXMLForValidation ="";
    protected ProgressDialog downloadProgDialog;
    private String bookDir = "";
    protected String[] enrichDetailsArray;
    
    public void LoadEnrichmentFirst(BookView bookview, String enrichBookId, int EnrServiceType){
    	BookID = enrichBookId.substring(1);
    	bookView = bookview;
    	serviceType = EnrServiceType;
    	//revert_book_hide :
    	//bookDir = Environment.getDataDirectory().getPath().concat("/data/com.semanoor.manahij/files/M").concat(BookID).concat("Book");
    	//bookDir = Environment.getDataDirectory().getPath().concat("/data/com.semanoor.manahij/files/.M").concat(BookID).concat("Book");
    	bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+bookview.bookName+"Book/";
    	if(bookview.downloadType=="private"){
    		GlobalId = bookView.userCredentials.getUserScopeId();
    	}else{
    		GlobalId = "0";
    	}
    	if(EnrServiceType ==0){ 
    		//Web service to get all enrich pages of the book :
    		enrichPagesArray.clear();
    		new GetAllEnrBookPages().execute();
    	}else if(EnrServiceType == 1){
    		//Web service to get the selected page enrichments
    		enrichDownloadArray.clear();
    		loadXmlUpdate();
    		new GetAllEnrBookPages().execute();
    	}else if(EnrServiceType == 2){
    		
    	}
    	downloadProgDialog = new ProgressDialog(bookView);
		downloadProgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);   
		downloadProgDialog.setMessage("");
		downloadProgDialog.getWindow().setTitleColor(Color.rgb(222, 223, 222));
		downloadProgDialog.setIndeterminate(false);
		downloadProgDialog.setMax(100);
		downloadProgDialog.setCancelable(false);
		downloadProgDialog.setCanceledOnTouchOutside(false);
    }
    
    /**
     * Method to get the XmlUpdate content :
     */
    private void loadXmlUpdate(){
    	
    	String XMLfilePath = bookDir+"xmlForUpdateRecords.xml";
    	File xmlFile = new File(XMLfilePath);
    	if(xmlFile.exists()){
    		String xmlContent = readFromFile(XMLfilePath);
    		String startNode = "<page"+currentPageNumber+">";
    		String endNode = "</page"+currentPageNumber+">";
    		String xmlUpdateContent = getEnrichmentsFromXML(xmlContent, startNode, endNode);
   
    		if(xmlUpdateContent !=""){
    			xmlUpdate ="<Enrichments>"+xmlUpdateContent+"</Enrichments>";
    			//xmlUpdateContent = URLEncoder.encode(xmlUpdateContent, "UTF-8");
    			xmlUpdate = 	xmlUpdate.replace("<", "&lt;"); 
    			xmlUpdate = xmlUpdate.replace(">", "&gt;");
    		}else{
    			xmlUpdate = "";
    		}
    	}else{
    		xmlUpdate = "";
    	}
    }
    
    
    public class GetAllEnrBookPages extends AsyncTask<Void, Void, Void>{
	protected Void doInBackground(Void... params) {
		
		if(serviceType == 0){
			soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				 + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
				 +"<soap:Body>\n"
                 +"<GetAllEnrBookPages xmlns=\"http://semanoor.com.sa/\">\n"
                 +"<IDBook>"+BookID+"</IDBook>\n"
                 +"<sGlobalId>"+GlobalId+"</sGlobalId>"
                 +"</GetAllEnrBookPages>\n"
                 +"</soap:Body>\n"
                 +"</soap:Envelope>";
		 SOAPAction = "http://semanoor.com.sa/GetAllEnrBookPages";
		 
		}else if(serviceType ==1){
			soapMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
					 + "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
					 +"<soap:Body>\n"
	                 +"<GetEnrichmentsPages xmlns=\"http://semanoor.com.sa/\">\n"
	                 +"<xmlCheckUpdate>"+ xmlUpdate+"</xmlCheckUpdate>\n"
	                 +"<IDBook>"+BookID+"</IDBook>\n"
                     +"<IDPage>"+currentPageNumber+"</IDPage>\n"
                     +"<sGlobalId>"+GlobalId+"</sGlobalId>"
	                 +"</GetEnrichmentsPages>\n"
	                 +"</soap:Body>\n"
	                 +"</soap:Envelope>";
			 SOAPAction = "http://semanoor.com.sa/GetEnrichmentsPages";
		}else if(serviceType ==2){
			
		}
		
		//System.out.println(soapMessage);
		ENVELOPE = String.format(soapMessage, null);
	//	url = "http://sem20.nooor.com/IPadAdmin/SboookService.asmx";
		url = Globals.getNewCurriculumWebServiceURL();
		String returnXml = CallWebService(url, SOAPAction, ENVELOPE);
		//System.out.println("returnxml:"+returnXml); 
		
		if(serviceType ==0){
			////System.out.println(enrichPagesArray);
		}else if(serviceType == 1){
			returnXml = returnXml.replaceAll("<new></new>", "");
			returnXml = returnXml.replaceAll("<update></update>", "");
			//get the book.xml content :
			//revert_book_hide :
			//String bookXmlPath =  bookView.getFilesDir().getAbsolutePath()+"/M"+BookID+"Book/"+"Book.xml";
			//String bookXmlPath =  bookView.getFilesDir().getAbsolutePath()+"/.M"+BookID+"Book/"+"Book.xml";
			//String bookXmlPath = bookDir+"Book.xml";
			//bookXMLForValidation = readFromFile(bookXmlPath);
			////System.out.println(enrichDownloadArray);
		}
		parseXml(returnXml);
		bookXMLForValidation = "";
		
		return null;		

	}
	
	@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(serviceType ==0){
				bookView.enrichPage1Progressbar();
				bookView.listviewPage1Invalidate();
				//bookView.enrichPopup();
			}else if(serviceType ==1){
				//bookView.setViewPagerCurrentItem();
				bookView.enrichprogressbar();
				bookView.listviewInvalidate();
			}else if(serviceType ==2){
				
			}
		}
	/*
     *  Web service call to get details of Countries list, Universities list, Categories list and Books list
     */
    private String CallOldWebService(String url, String sOAPAction, String eNVELOPE) {
		final DefaultHttpClient httpClient = new DefaultHttpClient();
	    // request parameters
	    HttpParams params = httpClient.getParams();
	    HttpConnectionParams.setConnectionTimeout(params, 10000);
	    HttpConnectionParams.setSoTimeout(params, 15000);
	    // set parameter
	    HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);
	    // POST the envelope
	    HttpPost httppost = new HttpPost(url);
	    // add headers
	    httppost.setHeader("SOAPAction", sOAPAction);
	    httppost.setHeader("Content-Type", "text/xml; charset=utf-8");
	    String responseString = "Nothingggg";
	    try {
	        // the entity holds the request
	        HttpEntity entity = new StringEntity(eNVELOPE,HTTP.UTF_8);  //Changes
	        httppost.setEntity(entity);
	        // Response handler
	        ResponseHandler<String> rh = new ResponseHandler<String>() {
	            // invoked when client receives response
				public String handleResponse(HttpResponse response)
						throws ClientProtocolException, IOException {
					// get response entity
	                HttpEntity entity = response.getEntity();
	                 //read the response as byte array
	                StringBuffer out = new StringBuffer();
	                byte[] b = EntityUtils.toByteArray(entity);
	                // write the response byte array to a string buffer
	                out.append(new String(b, 0, b.length)); 
	                String responseXml = out.toString(); //&gt;&lt; 
	                String finalStr = responseXml.replace("&lt;", "<");
	                finalStr = finalStr.replace("&gt;", ">");
	                String splitStr[]={""};
	                String finalSplitStr[]={""};
	                try {
	                	if(serviceType ==0){
		                	splitStr = finalStr.split("<GetAllEnrBookPagesResult>");
		                	finalSplitStr= splitStr[1].split("</GetAllEnrBookPagesResult>");
		                }else if(serviceType ==1){
		                	splitStr = finalStr.split("<GetEnrichmentsPagesResult>");
		                	finalSplitStr= splitStr[1].split("</GetEnrichmentsPagesResult>");
		                }
					} catch (Exception e) {
						////System.out.println("Catch error :"+e.getMessage());
						return "";
					}
	                
	                
	                return finalSplitStr[0];
				}
	        };
	         responseString = httpClient.execute(httppost, rh);
	    } catch (Exception e) {
	        e.printStackTrace();
	        Log.d("me","Exc : "+ e.toString());
	    }
	    // close the connection
	    httpClient.getConnectionManager().shutdown();
	    return responseString;
	}

		private String CallWebService(String url, String sOAPAction, String eNVELOPE){
			HttpURLConnection urlConnection = null;
			String returnResponse = "";
			String finalSplitStr[]={""};
			try {
				URL urlToRequest = new URL(url);

				urlConnection = (HttpURLConnection) urlToRequest.openConnection();
				urlConnection.setConnectTimeout(10000);
				urlConnection.setReadTimeout(25000);
				urlConnection.setRequestMethod("POST");
				urlConnection.setUseCaches(false);
				urlConnection.setDoInput(true);
				urlConnection.setDoOutput(true);
				urlConnection.setRequestProperty("Content-type", "text/xml; charset=utf-8");
				urlConnection.setRequestProperty("SOAPAction", sOAPAction);

				OutputStream out = urlConnection.getOutputStream();
				out.write(eNVELOPE.getBytes());
				int statusCode = urlConnection.getResponseCode();
				if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {

				} else if (statusCode != HttpURLConnection.HTTP_OK) {

				}

				InputStream in = urlConnection.getInputStream();
				returnResponse = UserFunctions.convertStreamToString(in);
				String finalStr = returnResponse.replace("&lt;", "<");
				finalStr = finalStr.replace("&gt;", ">");
				String splitStr[]={""};
				try {
					if(serviceType ==0){
						splitStr = finalStr.split("<GetAllEnrBookPagesResult>");
						if (splitStr.length >= 2) {
							finalSplitStr = splitStr[1].split("</GetAllEnrBookPagesResult>");
						}
					}else if(serviceType ==1){
						splitStr = finalStr.split("<GetEnrichmentsPagesResult>");
						if (splitStr.length >= 2) {
							finalSplitStr = splitStr[1].split("</GetEnrichmentsPagesResult>");
						}
					}
				} catch (Exception e) {
					return "";
				}

			} catch (MalformedURLException e) {

			} catch (SocketTimeoutException e) {

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (urlConnection != null) {
					urlConnection.disconnect();
				}
			}
			return finalSplitStr[0];
		}
    
    /*
     *  Parse Xml method for parsing the return string of Webservice calls
     */
    private void parseXml(String returnXml) {
		try{
        	SAXParserFactory mySAXParserFactory = SAXParserFactory.newInstance();
        	SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
        	XMLReader myXMLReader = mySAXParser.getXMLReader();
        	MyXmlHandler myRSSHandler = new MyXmlHandler();
        	myXMLReader.setContentHandler(myRSSHandler);
        	InputSource inputsource = new InputSource();
        	inputsource.setEncoding("UTF-8");
        	inputsource.setCharacterStream(new StringReader(returnXml));
            myXMLReader.parse(inputsource);
        	
        }catch(MalformedURLException e){
        	e.printStackTrace();
        }catch(ParserConfigurationException e){
        	e.printStackTrace();
        }catch(SAXException e){
        	e.printStackTrace();
        }catch(IOException e){
        	e.printStackTrace();
        }
 }
    
    private class MyXmlHandler extends DefaultHandler{
    	
    	public void startDocument() throws SAXException {
    		//super.startDocument();
    	}
    	public void endDocument() throws SAXException {
    		//super.endDocument();
    	}
    	public void startElement(String uri, String localName, String qName,
    			Attributes attributes) throws SAXException {
    		//super.startElement(uri, localName, qName, attributes);
    		////System.out.println("In parse Start Element Method with localname: "+localName);
    		////System.out.println("In parse Start Element Method with qName: "+qName);
    		if(serviceType == 0){
    				if(localName.equals("pg"))
    				{
		    			String PageNo = attributes.getValue("Page");
		    			enrichPagesArray.add(PageNo);
    				}
    		}
    		if(serviceType == 1){
    			
    			if(localName.equals("new")){
    				currentEnrType ="newEnr";
    			}else if(localName.equals("update")){
    				currentEnrType = "updateEnr";
    			}
    			
    			if(localName.equals("enriched")){
    				String title = attributes.getValue("Title");
	    			String path = attributes.getValue("Path");
	    			String size = attributes.getValue("Size");
	    			String ID = attributes.getValue("ID");
	    			String enrPageNo = attributes.getValue("IDPage");
	    			String IDEnr = attributes.getValue("IDEnr");
	    			String IDUser = attributes.getValue("IDUser");
	    			String UserName = attributes.getValue("UserName");
	    			String Password = attributes.getValue("Password");
	    			String DateUp = attributes.getValue("DateUp");
	    			String Date = attributes.getValue("Date");
	    			String bookTitle = attributes.getValue("BookTitle");
	    			if (bookTitle == null) {
						bookTitle = bookView.currentBook.getBookTitle();
					}
	    			String bookId = attributes.getValue("BookID");
	    			if (bookId == null) {
						//bookId = bookView.currentBook.get_bStoreID().replace("M", "");
						String[] data=bookView.currentBook.get_bStoreID().split("M");
						bookId = data[1];
					}
	    			String emailId = attributes.getValue("Email");
	    			
	    	        String strEnrValues="";
	    			
    				if(currentEnrType.equals("newEnr") && bookXMLForValidation.contains("IDEnr="+IDEnr)){ //Need to check this
    					
    				}else{
    					strEnrValues = title+"##"+path+"##"+size+"##"+ID+"##"+IDEnr+"##"+IDUser+"##"+UserName+"##"+Password+"##"+DateUp+"##"+Date+"##"+currentEnrType+"##"+enrPageNo+"##"+bookTitle+"##"+bookId+"##"+emailId;
        				enrichDownloadArray.add(strEnrValues);
    					}
    				}
    			}
    		}
	   }
    }
    
    /**
     * Method to split the updateXml to get the current page content :
     * @param xmlContent
     * @param startNode
     * @param endNode
     * @return
     */
    private String getEnrichmentsFromXML(String xmlContent, String startNode, String endNode){
    	
    	String result = "";
    	if(xmlContent.contains(startNode)){
    		int tillStartElement = xmlContent.indexOf(startNode, 0) + startNode.length();
    		int tillEndElement = xmlContent.indexOf(endNode, 0);
    		result = (String) xmlContent.subSequence(tillStartElement, tillEndElement);
    		////System.out.println("Result :"+result);
    	}
    	return result;
    }
    
    /**
     * Method to read the file from file's directory and returning as a string
     * @param path
     * @return
     */
    private String readFromFile(String path){
    	String ret = "";
    	try{
    		InputStream inputStream = new FileInputStream(path);
    		if(inputStream != null){
    			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
    			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
    			String receiveString ="";
    			StringBuilder stringBuilder = new StringBuilder();
    			while((receiveString = bufferedReader.readLine()) != null){
    				stringBuilder.append(receiveString);
    			}
    			inputStream.close();
    			ret = stringBuilder.toString();
    		}
    		
    	}catch (FileNotFoundException e) {
			Log.e("xml file", "File not found:"+e.toString());
		}catch(IOException e){
			Log.e("xml file", "cannot read file:"+e.toString());
		}
    	return ret;
    }
    /**
     * Method to copy file xmlForUpdateRecords.xml to the book 
     * @param fromPath
     * @param toPath
     * @return
     */
    public static boolean copyFiles(String fromPath, String toPath){
		InputStream in =null;           
		OutputStream out =null;
		try{
			in = bookView.getAssets().open(fromPath);
			new File(toPath).createNewFile();
			out = new FileOutputStream(toPath);
			byte[] buffer = new byte[1024];
			int read;
			while((read = in.read(buffer)) != -1){
				out.write(buffer,0,read);
			}
			in.close();
			in=null;
			out.flush();
			out.close();
			out = null;
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
    
    /**
     * Call to download enrichFiles :
     */
    public void downloadEnrichFiles(String enrichDetails){
    	
    	enrichDetailsArray = enrichDetails.split("##");

    	downloadProgDialog.setTitle(enrichDetailsArray[0]);
    	downloadProgDialog.setProgress(0);
    	downloadProgDialog.setMax(100);
    	downloadProgDialog.show();
		//Get the fileName where the files has to be downloaded. Create sub folders if not created.
		String pageDir =bookDir+"Enrichments/Page"+currentPageNumber;
		File enrichFile = new File(pageDir);
		if(!enrichFile.exists()){
			enrichFile.mkdirs();
		}
		
		String XMLfilePath = bookDir+"xmlForUpdateRecords.xml";
    	File xmlFile = new File(XMLfilePath);
    	if(!xmlFile.exists()){
    		//Copy xml file from assets to book folder :
    		copyFiles("xmlForUpdateRecords.xml", XMLfilePath);
    	}
		new progressDownload().execute(enrichDetailsArray[1]); //bookURL
    }
    
    /*
	 * AsyncTask for downloading Enriches  
	 */
	private  class  progressDownload extends AsyncTask<String, Integer, String>{
	    	public String DprogressString="";
	    	public Boolean downloadSuccess;
	    	
	    	public String enrichFileName =bookDir+"Enrichments/Page"+currentPageNumber+ "/Enriched"+enrichDetailsArray[3];
	    	
	    	protected void onPreExecute(){
	    		super.onPreExecute();
	    		
	    	}

			@Override
			protected String doInBackground(String... downloadurl) {

				final int TIMEOUT_CONNECTION = 5000;//5sec
				 final int TIMEOUT_SOCKET = 30000;//30sec
				long bytes = 1024 * 1024;
				 try{
				 URL url = new URL(downloadurl[0]);
				//Open a connection to that URL.
				 URLConnection ucon = url.openConnection();
				ucon.connect();
				int   fileLength = ucon.getContentLength();
				//////System.out.println("filelength:"+fileLength);
				 long startTime = System.currentTimeMillis();
				 
				 //this timeout affects how long it takes for the app to realize there's a connection problem
				 ucon.setReadTimeout(TIMEOUT_CONNECTION);
				 ucon.setConnectTimeout(TIMEOUT_SOCKET);
				 //Define InputStreams to read from the URLConnection.
				 // uses 3KB download buffer
				 InputStream is = ucon.getInputStream();
				 BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
				  //Internal storage
				 String filePath = enrichFileName+".zip";
				 FileOutputStream outStream = new FileOutputStream(filePath);
				 byte[] buff = new byte[5 * 1024];
				 //Read bytes (and store them) until there is nothing more to read(-1)
				 int len;
				String totalMB,fileLengthMB;
				long total = 0;
				DecimalFormat twoDformat = new DecimalFormat("#.##");
				 while ((len = inStream.read(buff)) != -1)
				 {
					total += len;
					////////System.out.println("total:"+total);
					totalMB=twoDformat.format((float)total/bytes);
					fileLengthMB= twoDformat.format((float)fileLength/bytes);
					DprogressString=totalMB+" MB"+" of "+fileLengthMB+" MB";
					publishProgress((int)((total*100)/fileLength));
					outStream.write(buff,0,len);
				 }
				 //clean up
				 outStream.flush();
				 outStream.close();
				 inStream.close();
				 downloadSuccess = true;
				 ////System.out.println( "download completed in "+ ((System.currentTimeMillis() - startTime) / 1000)+ " sec");
				 
				 }
				 catch (IOException e){
					 ////System.out.println("error while storing:"+e);
					 downloadSuccess = false;
				 }
					return null;
			}
			
			protected void onProgressUpdate(Integer... progress){
				super.onProgressUpdate(progress);
				if(downloadProgDialog != null){
					////System.out.println("OnprogressUpdate:"+progress[0]);
					downloadProgDialog.setMessage(DprogressString);
					downloadProgDialog.setProgress(progress[0]);
				}
			}
			
			protected void onPostExecute(String result){
				//////System.out.println("OnpostExecute");
				if(downloadSuccess){ //unzipping will be done when downloadSuccess is true.
	
		    		String zipfile = enrichFileName + ".zip";
					String unziplocation = enrichFileName + "/"; //"/" is a must here..
				    Boolean unzipSuccess= unzipDownloadfileNew(zipfile,unziplocation); //if unzipping success then need to store in database:
				    if(unzipSuccess){
				    	
				     //DB entry :
				    }
				  //Need to delete the zip file which is download
				    File zipfilelocation = new File(enrichFileName+".zip");  //Deleting zip file in the internal storage
				    try{
			    		zipfilelocation.delete();
			    		}catch(Exception e){
					    		//////System.out.println("Failed to deletefile with Error "+e);
					   }
				    checkAndUpdateBooksXml(enrichSelectArray.get(0));
				    checkAndUpdateXmlForUpdateRecords(enrichSelectArray.get(0));
				    
				    ////System.out.println("enrichSelectArray count:"+enrichSelectArray.size());
				    enrichSelectArray.remove(0);
				    if(enrichSelectArray.size()== 0){
				    	downloadProgDialog.dismiss();
				    	//Call method to reload enrichments :
				    	bookView.reloadEnrichments(currentPageNumber-1);

				    }
				}
			}
	}
	
	private Boolean unzipDownloadfileNew(String zipfilepath, String unziplocation)
    { 
    	InputStream is;
    	ZipInputStream zis;
    	try
    	{
    		is = new FileInputStream(zipfilepath);
    		zis = new ZipInputStream(new BufferedInputStream(is));
    		ZipEntry ze;
    		while ((ze = zis.getNextEntry()) != null) 
    		{	
    			String zipEntryName = ze.getName();
    			zipEntryName = zipEntryName.replace("\\", "/");
    			//////System.out.println("zipEntryName: "+zipEntryName);
    			File  file = new File(unziplocation+zipEntryName);
    			if(file.exists()){
    				//////System.out.println("file exists at the given path:" +file);
    			}else{
    				
    				if(ze.isDirectory()){
    					//////System.out.println("Ze is an directory. It will create directory and exit");
    					file.mkdirs();
    					continue;
    				}
    				
    					file.getParentFile().mkdirs();   //Horror line..............
    					byte[] buffer = new byte[1024];
    					FileOutputStream fout = new FileOutputStream(file);
    					BufferedOutputStream baos  = new BufferedOutputStream(fout, 1024);
    					int count;
    					while((count = zis.read(buffer, 0, 1024))!= -1){
    						baos.write(buffer, 0, count);
    					}
    					baos.flush();
    					baos.close();
    			}
    		}
    			zis.close();
    			//////System.out.println("finished unzipping");
    			return true;
    	}catch(Exception e){
    		//////System.out.println("unzipping error"+e);
    		return false;
    	}
    }
	
	public void checkAndUpdateBooksXml(String enrichDetails){
		//revert_book_hide :
		//String bookXmlPath =  bookView.getFilesDir().getPath()+"/M"+BookID+"Book"+"/Book.xml"; //data/data/com.semanoor.manahij/files/
		//String bookXmlPath =  bookView.getFilesDir().getPath()+"/.M"+BookID+"Book"+"/Book.xml"; 
		String bookXmlPath = bookDir+"Book.xml";
		 try {
	            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	            
	            Document document = documentBuilder.parse(new File(bookXmlPath));
	            String tagName = "P"+(currentPageNumber-1);
	            Node pageNode = document.getElementsByTagName(tagName).item(0);
	            NodeList nodes = pageNode.getChildNodes();
 	     
	            //Check and delete pre-existing enrich entries :
	           /* for (int i = 0; i < nodes.getLength(); i++) {       
	                
	            	Node element = nodes.item(i);
	                NamedNodeMap attribute = element.getAttributes();
		            Node nodeAttr = attribute.getNamedItem("ID");
		            ////System.out.println(attribute.getNamedItem("ID").toString());
		            if(attribute.getNamedItem("ID").toString().equals("337")){
		            	element.removeChild(element);
		            	
		            }
	                if(element.getNodeName().equals("ID")){

	                	if(element.getNodeValue().equals("377")){
	                		element.removeChild(element);
	                	}
	                }
	            }*/	 
	            //create new and update elements :
	            String[] enrichValues = enrichDetails.split("##");
	            String eleName = "Enr"+"1";
	            String elePath = "Enrichments/Page"+currentPageNumber+"/Enriched"+enrichValues[3]+"/index.htm";
	            String eleTitle = enrichValues[0];
	            String eleID = enrichValues[3];
	            String eleType = "Downloaded";
	            Element enrElement = document.createElement(eleName);
	            enrElement.setAttribute("Path", elePath);
	            enrElement.setAttribute("Title", eleTitle);
	            enrElement.setAttribute("ID", eleID);
	            enrElement.setAttribute("Type", eleType);
	            pageNode.appendChild(enrElement);
	            
	            // write the DOM object to the file
	            TransformerFactory transformerFactory = TransformerFactory.newInstance();
	            Transformer transformer = transformerFactory.newTransformer();
	            DOMSource domSource = new DOMSource(document);
	 
	            StreamResult streamResult = new StreamResult(new File(bookXmlPath));
	            transformer.transform(domSource, streamResult);
	               
 	        } catch (ParserConfigurationException pce) {
	            pce.printStackTrace();	        
	            } catch (TransformerException tfe) {
	            tfe.printStackTrace();	        
	            } catch (IOException ioe) {
	            ioe.printStackTrace();
	        } catch (SAXException sae) {
	            sae.printStackTrace();
	        }
	}
	
	public void checkAndUpdateXmlForUpdateRecords(String enrichDetails){
		
		//revert_book_hide :
				//String bookXmlPath =  bookView.getFilesDir().getPath()+"/M"+BookID+"Book"+"/xmlForUpdateRecords.xml"; //data/data/com.semanoor.manahij/files/
				//String bookXmlPath =  bookView.getFilesDir().getPath()+"/.M"+BookID+"Book"+"/xmlForUpdateRecords.xml";
				String bookXmlPath = bookDir+"xmlForUpdateRecords.xml";
				 try {
			            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			            
			            Document document = documentBuilder.parse(new File(bookXmlPath));
			            String tagName = "pages";
			            String toModifyTagName = "page"+currentPageNumber;
			            Boolean isTagExists = false;
			            Node pagesNode = document.getElementsByTagName(tagName).item(0);
			           
			            NodeList pagesnodesList = pagesNode.getChildNodes();
			            for(int i = 0;i< pagesnodesList.getLength(); i++){
			            	
			            	Node element = pagesnodesList.item(i);
			            	if(element.getNodeName().equals(toModifyTagName)){
			            		//tag exists
			            		isTagExists = true;
			            		break;
			            	}else{
			            		//Tag doesn't exist. create a new tag.
			            		isTagExists = false;
			            	}
			            }
			            
			            if(!isTagExists){
			            	Element pageElement = document.createElement(toModifyTagName);
			            	pagesNode.appendChild(pageElement);
			            }
		 	     
			            //Check and delete pre-existing enrich entries :
			           /* for (int i = 0; i < nodes.getLength(); i++) {       
			                
			            	Node element = nodes.item(i);
			                NamedNodeMap attribute = element.getAttributes();
				            Node nodeAttr = attribute.getNamedItem("ID");
				            ////System.out.println(attribute.getNamedItem("ID").toString());
				            if(attribute.getNamedItem("ID").toString().equals("337")){
				            	element.removeChild(element);
				            	
				            }
			                if(element.getNodeName().equals("ID")){

			                	if(element.getNodeValue().equals("377")){
			                		element.removeChild(element);
			                	}
			                }
			            }*/	 
			            //create new and update elements :
			            Node currentPageNode = document.getElementsByTagName(toModifyTagName).item(0);
			            String[] enrichValues = enrichDetails.split("##");
			            String eleName = "enriched";
			            String eleID = enrichValues[3];
			            String eleTitle = enrichValues[0];
			            String eleDateUp = enrichValues[8];
			            String eleUserName = enrichValues[6];
			            String eleSize = enrichValues[2];
			            String elePath = enrichValues[1];
			            String eleIDEnr = enrichValues[4];
			            String eleIDUser = enrichValues[5];
			            String eleDate = enrichValues[9];
			            String elePassword = "";
			            String idPageNo = enrichValues[11];
			            String bookTitle = enrichValues[12];
			            String bookId = enrichValues[13];
			            String emailId = enrichValues[14];
			           
			            Element enrElement = document.createElement(eleName);
			            enrElement.setAttribute("IDPage", idPageNo);
			            enrElement.setAttribute("ID", eleID);
			            enrElement.setAttribute("Title", eleTitle);
			            enrElement.setAttribute("DateUp", eleDateUp);
			            enrElement.setAttribute("UserName", eleUserName);
			            enrElement.setAttribute("Size", eleSize);
			            enrElement.setAttribute("Path", elePath);
			            enrElement.setAttribute("IDEnr", eleIDEnr);
			            enrElement.setAttribute("IDUser", eleIDUser);
			            enrElement.setAttribute("Date", eleDate);
			            enrElement.setAttribute("Password", elePassword);
			            enrElement.setAttribute("BookTitle", bookTitle);
						enrElement.setAttribute("BookID", bookId);
						enrElement.setAttribute("Email", emailId);
			            
			            currentPageNode.appendChild(enrElement);
			            
			            // write the DOM object to the file
			            TransformerFactory transformerFactory = TransformerFactory.newInstance();
			            Transformer transformer = transformerFactory.newTransformer();
			            DOMSource domSource = new DOMSource(document);
			 
			            StreamResult streamResult = new StreamResult(new File(bookXmlPath));
			            transformer.transform(domSource, streamResult);
			               
		 	        } catch (ParserConfigurationException pce) {
			            pce.printStackTrace();	        
			            } catch (TransformerException tfe) {
			            tfe.printStackTrace();	        
			            } catch (IOException ioe) {
			            ioe.printStackTrace();
			        } catch (SAXException sae) {
			            sae.printStackTrace();
			        }
	}
}

