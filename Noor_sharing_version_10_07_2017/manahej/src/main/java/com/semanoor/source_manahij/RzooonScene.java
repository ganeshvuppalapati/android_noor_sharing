package com.semanoor.source_manahij;

public class RzooonScene{
	
	private String rUrl, rPageNo,rFolderName,rBgAudio,rFilename,rScenePath;

	public String getrUrl() {
		return rUrl;
	}

	public void setrUrl(String rUrl) {
		this.rUrl = rUrl;
	}

	public String getrPageNo() {
		return rPageNo;
	}

	public void setrPageNo(String rPageNo) {
		this.rPageNo = rPageNo;
	}
	public String getrBgAudio() {
		return rBgAudio;
	}

	public void setrBgAudio(String rBgAudio) {
		this.rBgAudio = rBgAudio;
	}
	public String getrFolderName() {
		return rFolderName;
	}

	public void setrFolderName(String rFolderName) {
		this.rFolderName = rFolderName;
	}
	public String getrFilename() {
		return rFilename;
	}

	public void setrFilename(String rFilename) {
		this.rFilename = rFilename;
	}

	public String getrScenePath() {
		return rScenePath;
	}

	public void setrScenePath(String rScenePath) {
		this.rScenePath = rScenePath;
	}
}
