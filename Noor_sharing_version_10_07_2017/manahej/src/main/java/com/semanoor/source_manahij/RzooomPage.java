package com.semanoor.source_manahij;

import java.util.ArrayList;

public class RzooomPage {
	
	private String rsooomName,rzooomAuthorname,rsooomType;
	private ArrayList<RzooonScene> rzooomSceneList;
	public String rwidth,rheight,rzooombgColor;
	
	public String getRzooomName() {
		return rsooomName;
	}
	public void setRzooomName(String rsooomName) {
		this.rsooomName = rsooomName;
	}
	public String getRzooomAuthorName() {
		return rzooomAuthorname;
	}
	public void setRzooomAuthorName(String rzooomAuthorname) {
		this.rzooomAuthorname = rzooomAuthorname;
	}
	public String getrsooomType() {
		return rsooomType;
	}
	public void setrsooomType(String rsooomType) {
		this.rsooomType = rsooomType;
	}
	public String getrwidth(){
		return this.rwidth;
	}
	public void setrwidth(String rwidth){
		this.rwidth = rwidth;
	}
	public String getrheight(){
		return this.rheight;
	}
	public void setrheight(String rheight){
		this.rheight = rheight;
	}
	public String getrzooombgColor(){
		return this.rzooombgColor;
	}
	public void setrzooombgColor(String rzooombgColor){
		this.rzooombgColor = rzooombgColor;
	}
	public ArrayList<RzooonScene> getRzooomSceneList() {
		return rzooomSceneList;
	}
	public void setRzoomSceneList(ArrayList<RzooonScene> rzooomSceneList) {
		this.rzooomSceneList = rzooomSceneList;
	}
}
