package com.semanoor.source_manahij;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.CloudData;
import com.semanoor.manahij.CloudEnrichDetails;
import com.semanoor.manahij.SlideMenuWithActivityGroup;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.DatabaseHandler;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;
import com.semanoor.source_sboookauthor.WebService;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Resources {
	
	private Context context;
	private int resourceId;
	private int resourceBid;
	private int resourcePageNo;
	private String resourceTitle;
	private boolean resourceSelected;
	private String resourcePath;
	private int resourceUserId;
	private String resourceUserName;
	private String resourceDateUp;
	private String resourceDate;
	private String resourceSize;
	private String resourceBookTitle;
	private String resourceEmailId;
	private String resourceStoreId;
	private boolean isReadStatus;
	private boolean isResourceDownloaded;

	public Resources(Context context) {
		this.context = context;
	}

	/**
	 * @return the resourceId
	 */
	public int getResourceId() {
		return resourceId;
	}

	/**
	 * @param resourceId the resourceId to set
	 */
	public void setResourceId(int resourceId) {
		this.resourceId = resourceId;
	}

	/**
	 * @return the resourceBid
	 */
	public int getResourceBid() {
		return resourceBid;
	}

	/**
	 * @param resourceBid the resourceBid to set
	 */
	public void setResourceBid(int resourceBid) {
		this.resourceBid = resourceBid;
	}

	/**
	 * @return the resourcePageNo
	 */
	public int getResourcePageNo() {
		return resourcePageNo;
	}

	/**
	 * @param resourcePageNo the resourcePageNo to set
	 */
	public void setResourcePageNo(int resourcePageNo) {
		this.resourcePageNo = resourcePageNo;
	}

	/**
	 * @return the resourceTitle
	 */
	public String getResourceTitle() {
		return resourceTitle;
	}

	/**
	 * @param resourceTitle the resourceTitle to set
	 */
	public void setResourceTitle(String resourceTitle) {
		this.resourceTitle = resourceTitle;
	}

	/**
	 * @return the resourceSelected
	 */
	public boolean isResourceSelected() {
		return resourceSelected;
	}

	/**
	 * @param resourceSelected the resourceSelected to set
	 */
	public void setResourceSelected(boolean resourceSelected) {
		this.resourceSelected = resourceSelected;
	}

	/**
	 * @return the resourcePath
	 */
	public String getResourcePath() {
		return resourcePath;
	}

	/**
	 * @param resourcePath the resourcePath to set
	 */
	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	/**
	 * @return the resourceUserId
	 */
	public int getResourceUserId() {
		return resourceUserId;
	}

	/**
	 * @param resourceUserId the resourceUserId to set
	 */
	public void setResourceUserId(int resourceUserId) {
		this.resourceUserId = resourceUserId;
	}

	/**
	 * @return the resourceUserName
	 */
	public String getResourceUserName() {
		return resourceUserName;
	}

	/**
	 * @param resourceUserName the resourceUserName to set
	 */
	public void setResourceUserName(String resourceUserName) {
		this.resourceUserName = resourceUserName;
	}

	/**
	 * @return the resourceDateUp
	 */
	public String getResourceDateUp() {
		return resourceDateUp;
	}

	/**
	 * @param resourceDateUp the resourceDateUp to set
	 */
	public void setResourceDateUp(String resourceDateUp) {
		this.resourceDateUp = resourceDateUp;
	}

	/**
	 * @return the resourceSize
	 */
	public String getResourceSize() {
		return resourceSize;
	}

	/**
	 * @param resourceSize the resourceSize to set
	 */
	public void setResourceSize(String resourceSize) {
		this.resourceSize = resourceSize;
	}

	/**
	 * @return the resourceBookTitle
	 */
	public String getResourceBookTitle() {
		return resourceBookTitle;
	}

	/**
	 * @param resourceBookTitle the resourceBookTitle to set
	 */
	public void setResourceBookTitle(String resourceBookTitle) {
		this.resourceBookTitle = resourceBookTitle;
	}

	/**
	 * @return the resourceEmailId
	 */
	public String getResourceEmailId() {
		return resourceEmailId;
	}

	/**
	 * @param resourceEmailId the resourceEmailId to set
	 */
	public void setResourceEmailId(String resourceEmailId) {
		this.resourceEmailId = resourceEmailId;
	}
	
	/**
	 * @return the resourceDate
	 */
	public String getResourceDate() {
		return resourceDate;
	}

	/**
	 * @param resourceDate the resourceDate to set
	 */
	public void setResourceDate(String resourceDate) {
		this.resourceDate = resourceDate;
	}

	
	public void parseAndUpdate(Resources resource) {
		String bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+"M"+resourceBid+"Book/";
    	
    	String XMLfilePath = bookDir+"xmlForUpdateRecords.xml";
    	File xmlFile = new File(XMLfilePath);
    	if(!xmlFile.exists()){
    		//Copy xml file from assets to book folder :
    		copyFiles("xmlForUpdateRecords.xml", XMLfilePath);
    	}
    	checkAndUpdateXmlForUpdateRecords(XMLfilePath);
	}
	
    /**
     * Method to copy file xmlForUpdateRecords.xml to the book 
     * @param fromPath
     * @param toPath
     * @return
     */
    public boolean copyFiles(String fromPath, String toPath){
		InputStream in =null;           
		OutputStream out =null;
		try{
			in = context.getAssets().open(fromPath);
			new File(toPath).createNewFile();
			out = new FileOutputStream(toPath);
			byte[] buffer = new byte[1024];
			int read;
			while((read = in.read(buffer)) != -1){
				out.write(buffer,0,read);
			}
			in.close();
			in=null;
			out.flush();
			out.close();
			out = null;
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public String getResourceStoreId() {
		return resourceStoreId;
	}

	public void setResourceStoreId(String resourceStoreId) {
		this.resourceStoreId = resourceStoreId;
	}

	public boolean isReadStatus() {
		return isReadStatus;
	}

	public void setReadStatus(boolean readStatus) {
		isReadStatus = readStatus;
	}

	public boolean isResourceDownloaded() {
		return isResourceDownloaded;
	}

	public void setResourceDownloaded(boolean resourceDownloaded) {
		isResourceDownloaded = resourceDownloaded;
	}

	/*public void getResourceXmlFromUrl(DatabaseHandler db, WebService webService, ArrayList<Object> bookListArray, int bookId){
		String bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH+"M"+resourceBid+"Book/";

		String XMLfilePath = bookDir+"xmlForUpdateRecords.xml";
		File xmlFile = new File(XMLfilePath);
		if(!xmlFile.exists()){
			//Copy xml file from assets to book folder :
			copyFiles("xmlForUpdateRecords.xml", XMLfilePath);
		}
		webService.getXmlFromUrlAndParse(resourcePath);
		if (webService.noteList != null) {
			for (int i = 0; i < webService.noteList.size(); i++) {
				HashMap<String, String> noteMap = webService.noteList.get(i);
				String bookName = noteMap.get(webService.KEY_NOTE_BOOK_ID);
				String pageNo = noteMap.get(webService.KEY_NOTE_PAGE_NO);
				String sText = noteMap.get(webService.KEY_NOTE_STEXT);
				String occurence = noteMap.get(webService.KEY_NOTE_OCCURENCE);
				String sDesc = noteMap.get(webService.KEY_NOTE_SDESC);
				String nPos = noteMap.get(webService.KEY_NOTE_NPOS);
				String color = noteMap.get(webService.KEY_NOTE_COLOR);
				String processStext = noteMap.get(webService.KEY_NOTE_PROCESS_STEXT);
				String enrichId = noteMap.get(webService.KEY_NOTE_ENRICHID);
				String query = "insert into tblNote(BName, PageNo, SText, Occurence, SDesc, NPos, Color, ProcessSText, TabNo, Exported) values ('"+bookName+"', '"+pageNo+"', '"+sText+"', '"+occurence+"', '"+sDesc+"', '"+nPos+"', '"+color+"', '"+processStext+"', '"+enrichId+"', '"+resourceId+"')";
				db.executeQuery(query);
			}
		}
		if (webService.linkList != null) {
			for (int i = 0; i < webService.linkList.size(); i++) {
				HashMap<String, String> linkMap = webService.linkList.get(i);
				String linkPageNo = linkMap.get(webService.KEY_LINK_PAGE_NO);
				String linkTitle = linkMap.get(webService.KEY_LINK_TITLE);
				String linkURL = linkMap.get(webService.KEY_LINK_URL);
				int SBAId = 0;
				if (bookListArray != null) {
					for (int j = 0; j < bookListArray.size(); j++) {
						Book book = (Book) bookListArray.get(j);
						if (book.is_bStoreBook()) {
							int bookStoreId = Integer.parseInt(book.get_bStoreID().replace("M", ""));
							if (bookStoreId == resourceBid) {
								SBAId = book.getBookID();
								break;
							}
						}
					}
				} else {
					SBAId = bookId;
				}
				String query = "insert into enrichments(BID, pageNO, Title, Type, Exported, Path) values ('"+SBAId+"', '"+linkPageNo+"', '"+linkTitle+"', '"+"Search"+"', '"+resourceId+"', '"+linkURL+"')";
				db.executeQuery(query);
			}
		}
		checkAndUpdateXmlForUpdateRecords(XMLfilePath);
	}*/
    
    public class getResourceXmlFromUrl extends AsyncTask<Void, Void, Void>{

    	String XMLfilePath;
    	DatabaseHandler db;
    	WebService webService;
    	ArrayList<Object> bookListArray;
    	ProgressDialog resourceProgressDialog;
		int bookId;
		boolean dismissProgressDialog;
		String resourceXml;
		String storeBookId;
		ArrayList<HashMap<String, String>> selectedCloudEnrList;
		int selectedCloudEnrPosition;
		ArrayList<HashMap<String, String>> cloudEnrList;
		SlideMenuWithActivityGroup.ViewAdapter loginAdapter;
		ProgressDialog syncProgressDialog;
		Book currentBook;
    	
    	public getResourceXmlFromUrl(DatabaseHandler db, WebService webService, ArrayList<Object> bookListArray, ProgressDialog resourceProgressDialog, int bookId, Book currentBook, boolean dismissProgressDialog) {
    		this.db = db;
    		this.webService = webService;
    		this.bookListArray = bookListArray;
    		this.resourceProgressDialog = resourceProgressDialog;
			this.bookId = bookId;
			this.dismissProgressDialog = dismissProgressDialog;
			this.currentBook = currentBook;
		}

		public getResourceXmlFromUrl(DatabaseHandler db, WebService webService, ArrayList<Object> bookListArray, String resourceXML, String storeBookId, ArrayList<HashMap<String, String>> selectedCloudEnrList, int selectedCloudEnrPosition, ArrayList<HashMap<String, String>> cloudEnrList, SlideMenuWithActivityGroup.ViewAdapter loginAdapter, ProgressDialog syncProgressDialog){
			this.db = db;
			this.webService = webService;
			this.bookListArray = bookListArray;
			this.resourceXml = resourceXML;
			this.storeBookId = storeBookId;
			this.selectedCloudEnrList = selectedCloudEnrList;
			this.selectedCloudEnrPosition = selectedCloudEnrPosition;
			this.cloudEnrList = cloudEnrList;
			this.loginAdapter = loginAdapter;
			this.syncProgressDialog = syncProgressDialog;
		}

		@Override
    	protected void onPreExecute() {
    		super.onPreExecute();
			if (!Globals.ISGDRIVEMODE()) {
				String bookDir;
				if (currentBook != null) {
					bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + currentBook.get_bStoreID() + "Book/";
				} else {
					bookDir = Globals.TARGET_BASE_BOOKS_DIR_PATH + resourceStoreId + "M" + resourceBid + "Book/";
				}

				XMLfilePath = bookDir + "xmlForUpdateRecords.xml";
				File xmlFile = new File(XMLfilePath);
				if (!xmlFile.exists()) {
					//Copy xml file from assets to book folder :
					copyFiles("xmlForUpdateRecords.xml", XMLfilePath);
				}
			}
    	}

		@Override
		protected Void doInBackground(Void... params) {
			if (Globals.ISGDRIVEMODE()) {
				webService.getXMLandParseForResource(resourceXml);
			} else {
				webService.getXmlFromUrlAndParse(resourcePath);
			}
			if (webService.noteList != null) {
				for (int i = 0; i < webService.noteList.size(); i++) {
					HashMap<String, String> noteMap = webService.noteList.get(i);
					String bookName = noteMap.get(webService.KEY_NOTE_BOOK_ID);
					if (bookName.contains("\n")) {
						bookName=bookName.replaceAll("\n", "");
					}
					String pageNo = noteMap.get(webService.KEY_NOTE_PAGE_NO);
					String sText = noteMap.get(webService.KEY_NOTE_STEXT);
					String occurence = noteMap.get(webService.KEY_NOTE_OCCURENCE);
					String sDesc = noteMap.get(webService.KEY_NOTE_SDESC);
					String nPos = noteMap.get(webService.KEY_NOTE_NPOS);
					String color = noteMap.get(webService.KEY_NOTE_COLOR);
					String processStext = noteMap.get(webService.KEY_NOTE_PROCESS_STEXT);
					String enrichId = noteMap.get(webService.KEY_NOTE_ENRICHID);
					if (enrichId.contains("\n")) {
						enrichId=enrichId.replaceAll("\n", "");
					}
					String query = "insert into tblNote(BName, PageNo, SText, Occurence, SDesc, NPos, Color, ProcessSText, TabNo, Exported,NoteType) values ('"+bookName+"', '"+pageNo+"', '"+sText+"', '"+occurence+"', '"+sDesc+"', '"+nPos+"', '"+color+"', '"+processStext+"', '"+enrichId+"', '"+resourceId+"','1')";
					db.executeQuery(query);
				}
			}
			if (webService.linkList != null) {
				for (int i = 0; i < webService.linkList.size(); i++) {
					HashMap<String, String> linkMap = webService.linkList.get(i);
					String linkPageNo = linkMap.get(webService.KEY_LINK_PAGE_NO);
					String linkTitle = linkMap.get(webService.KEY_LINK_TITLE);
					String linkURL = linkMap.get(webService.KEY_LINK_URL);
					int SBAId;
					if (Globals.ISGDRIVEMODE()) {
						SBAId = getBookIDFromStoreId(storeBookId);
					} else {
						SBAId = getSBAId();
					}
					String query = "insert into enrichments(BID, pageNO, Title, Type, Exported, Path,categoryID) values ('"+SBAId+"', '"+linkPageNo+"', '"+linkTitle+"', '"+"Search"+"', '"+resourceId+"', '"+linkURL+"','0')";
					db.executeQuery(query);
				}
			}
			if (webService.mindmapList != null){
				for (int i = 0; i < webService.mindmapList.size(); i++){
					HashMap<String, String> mindMap = webService.mindmapList.get(i);
					String mindMapPageNo = mindMap.get(webService.KEY_MI_PAGE_NO);
					String mindMapTitle = mindMap.get(webService.KEY_MI_TITLE);
					String mindMapPathName = mindMap.get(webService.KEY_MI_PATH_NAME);
					String mindMapContent = mindMap.get(webService.KEY_MI_CONTENT);
					String miXmlfilePath = Globals.TARGET_BASE_MINDMAP_PATH+mindMapPathName+".xml";
					if (new File(miXmlfilePath).exists()) {
						String timeStamp = UserFunctions.getCurrentDateTimeInString();
						mindMapPathName = mindMapPathName.concat("_"+timeStamp);
						miXmlfilePath = Globals.TARGET_BASE_MINDMAP_PATH+mindMapPathName+".xml";
					}
					UserFunctions.createNewDirectory(Globals.TARGET_BASE_MINDMAP_PATH);
					UserFunctions.writeFile(miXmlfilePath, mindMapContent);
					int SBAId;
					if (Globals.ISGDRIVEMODE()) {
						SBAId = getBookIDFromStoreId(storeBookId);
					} else {
						SBAId = getSBAId();
					}
					String query = "insert into enrichments(BID, pageNO, Title, Type, Exported, Path,categoryID) values ('"+SBAId+"', '"+mindMapPageNo+"', '"+mindMapTitle+"', '"+Globals.OBJTYPE_MINDMAPTAB+"', '"+resourceId+"', '"+mindMapPathName+"','0')";
					db.executeQuery(query);
				}
			}
			return null;
		}

		private int getBookIDFromStoreId(String bookStoreId){
			for (int i = 0; i<bookListArray.size(); i++) {
				Book book = (Book) bookListArray.get(i);
				if (book.is_bStoreBook() && book.get_bStoreID().equals(bookStoreId)) {
					return book.getBookID();
				}
			}
			return 0;
		}

		private int getSBAId(){
			int SBAId = 0;
			String[] bookIdSplit = new String[0];
			if (bookListArray != null) {
				for (int j = 0; j < bookListArray.size(); j++) {
					Book book = (Book) bookListArray.get(j);
					if (book.is_bStoreBook()) {
						int bookStoreId = 0;
						if (book.get_bStoreID().contains("M")) {
							bookIdSplit = book.get_bStoreID().split("M");
							bookStoreId = Integer.parseInt(bookIdSplit[1]);
						}
						if (bookStoreId == resourceBid && book.get_bStoreID().contains("M")) {
							SBAId = book.getBookID();
							break;
						}
					}
				}
			} else {
				SBAId = bookId;
			}
			return SBAId;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (Globals.ISGDRIVEMODE()) {
				HashMap<String, String> selectedMap = selectedCloudEnrList.get(selectedCloudEnrPosition);
				String enrID = selectedMap.get(WebService.KEY_CENRICHMENTS);
				DatabaseHandler db = DatabaseHandler.getInstance(context);
				db.executeQuery("insert into CloudObjects(objID)values('"+enrID+"')");
				if(cloudEnrList!=null) {
					cloudEnrList.remove(selectedMap);
				}
				if (selectedCloudEnrPosition == selectedCloudEnrList.size()-1 &&cloudEnrList==null) {
					selectedCloudEnrList.clear();
					if (loginAdapter != null) {
						loginAdapter.listViewSelectEnrRes.invalidateViews();
						loginAdapter.txtCloudShare.setText(String.valueOf(cloudEnrList.size()));
					}
					if (context instanceof BookViewReadActivity) {
						((BookViewReadActivity) context).enrResDownload.popoverView.dissmissPopover(true);
						((BookViewReadActivity) context).enrResDownload.updateBtnEnrResCount();
					}else if(context instanceof CloudEnrichDetails){
						for(int i=0;i< ((CloudEnrichDetails) context).enrich_Resource.size();i++){
							CloudData data=((CloudEnrichDetails) context).enrich_Resource.get(i);
							if(data.getMap().equals(selectedMap)){
								((CloudEnrichDetails) context).enrich_Resource.remove(i);
							}
						}
					}

					syncProgressDialog.dismiss();
				}
			} else {
				checkAndUpdateXmlForUpdateRecords(XMLfilePath);
				if (dismissProgressDialog) {
					resourceProgressDialog.dismiss();
				}
			}
		}
    }
	
	/**
	 * Check and update xml in XmlUpdateRecordsinXml
	 * @param XmlPath
	 */
	public void checkAndUpdateXmlForUpdateRecords(String XmlPath){

		//revert_book_hide :
		//String bookXmlPath =  bookView.getFilesDir().getPath()+"/M"+BookID+"Book"+"/xmlForUpdateRecords.xml"; //data/data/com.semanoor.manahij/files/
		//String bookXmlPath =  bookView.getFilesDir().getPath()+"/.M"+BookID+"Book"+"/xmlForUpdateRecords.xml";
		String bookXmlPath = XmlPath;
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

			Document document = documentBuilder.parse(new File(bookXmlPath));
			String tagName = "pages";
			String toModifyTagName = "Resources";
			Boolean isTagExists = false;
			Node pagesNode = document.getElementsByTagName(tagName).item(0);

			NodeList pagesnodesList = pagesNode.getChildNodes();
			for(int i = 0;i< pagesnodesList.getLength(); i++){

				Node element = pagesnodesList.item(i);
				if(element.getNodeName().equals(toModifyTagName)){
					//tag exists
					isTagExists = true;
					break;
				}else{
					//Tag doesn't exist. create a new tag.
					isTagExists = false;
				}
			}

			if(!isTagExists){
				Element pageElement = document.createElement(toModifyTagName);
				pagesNode.appendChild(pageElement);
			}

			//create new and update elements :
			Node currentPageNode = document.getElementsByTagName(toModifyTagName).item(0);
			String eleName = "Resource";

			Element enrElement = document.createElement(eleName);
			enrElement.setAttribute("Title", resourceTitle);
			enrElement.setAttribute("DateUp", resourceDateUp);
			enrElement.setAttribute("ResourceID", String.valueOf(resourceId));
			enrElement.setAttribute("IDPage", String.valueOf(resourcePageNo));
			enrElement.setAttribute("Path", resourcePath);
			enrElement.setAttribute("Size", resourceSize);
			enrElement.setAttribute("BookID", String.valueOf(resourceBid));
			enrElement.setAttribute("IDUser", String.valueOf(resourceUserId));
			enrElement.setAttribute("Date", resourceDate);
			enrElement.setAttribute("UserName", resourceUserName);
			if (resourceBookTitle != null) {
				enrElement.setAttribute("BookTitle", resourceBookTitle);
			}
			enrElement.setAttribute("Email", resourceEmailId);
			
			currentPageNode.appendChild(enrElement);

			// write the DOM object to the file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource domSource = new DOMSource(document);

			StreamResult streamResult = new StreamResult(new File(bookXmlPath));
			transformer.transform(domSource, streamResult);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();	        
		} catch (TransformerException tfe) {
			tfe.printStackTrace();	        
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}

}
