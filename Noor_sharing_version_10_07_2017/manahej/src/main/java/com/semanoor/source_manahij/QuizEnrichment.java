package com.semanoor.source_manahij;

import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.semanoor.manahij.QuizBookViewActivity;
import com.semanoor.manahij.R;

public class QuizEnrichment {

	private String quizEnrichmentPath, quizEnrichmentTitle;

	public String getQuizEnrichmentPath() {
		return quizEnrichmentPath;
	}

	public void setQuizEnrichmentPath(String quizEnrichmentPath) {
		this.quizEnrichmentPath = quizEnrichmentPath;
	}

	public String getQuizEnrichmentTitle() {
		return quizEnrichmentTitle;
	}

	public void setQuizEnrichmentTitle(String quizEnrichmentTitle) {
		this.quizEnrichmentTitle = quizEnrichmentTitle;
	}
	
	public Button createEnrichmentTabs(final QuizBookViewActivity ctxQuiz, int i) {
		final Button btnEnrichmentTab = new Button(ctxQuiz);
		btnEnrichmentTab.setId(R.id.btnEnrichmentTab);
		//btnEnrichmentTab.setTag(this.enrichmentId);
	//	quizEnrichmentTitle = ".."+quizEnrichmentTitle.substring(0,10);
		btnEnrichmentTab.setText(quizEnrichmentTitle);
		btnEnrichmentTab.setTextColor(Color.WHITE);
		btnEnrichmentTab.setEllipsize(android.text.TextUtils.TruncateAt.END);
		btnEnrichmentTab.setSingleLine(true);
		btnEnrichmentTab.setHorizontallyScrolling(true);
		btnEnrichmentTab.setBackgroundResource(R.drawable.btn_enrich_tab);
		if(i==0){
			btnEnrichmentTab.setSelected(true);
		}
		btnEnrichmentTab.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ctxQuiz.makeEnrichedBtnSlected(v);
				ctxQuiz.loadEnricmentPage(quizEnrichmentPath);
			}
		});
		ctxQuiz.enrTabsLayout.addView(btnEnrichmentTab, new RelativeLayout.LayoutParams((int)ctxQuiz.getResources().getDimension(R.dimen.enrichment_btn_width), (int)ctxQuiz.getResources().getDimension(R.dimen.enrichment_btn_height)));
		return btnEnrichmentTab;
	}
}
