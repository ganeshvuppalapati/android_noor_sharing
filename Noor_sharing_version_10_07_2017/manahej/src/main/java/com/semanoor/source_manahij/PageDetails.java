package com.semanoor.source_manahij;

/**
 * Created by Krishna on 09-05-2015.
 */
public class PageDetails {
    private String Path;
    private String Title;

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
