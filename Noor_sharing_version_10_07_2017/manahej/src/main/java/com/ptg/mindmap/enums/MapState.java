package com.ptg.mindmap.enums;

public enum MapState {
	MPStateNormal ,
		    MPStateBeginDragDrop,
		    MPStateBeingDragging,
		    MPStateEndDragDrop
		   
}
