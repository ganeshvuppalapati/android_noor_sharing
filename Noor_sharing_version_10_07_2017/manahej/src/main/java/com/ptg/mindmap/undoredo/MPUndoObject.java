package com.ptg.mindmap.undoredo;

import android.graphics.Bitmap;

import com.ptg.mimdmap.node.MPNode;
import com.ptg.mindmap.enums.MapLayoutLineType;
import com.ptg.mindmap.enums.MapLayoutType;
import com.ptg.mindmap.enums.UndoOperationKey;

public class MPUndoObject {

	public MPNode objold;
	public MPNode objnew;
	public MPNode objExtra;
	public String shape_oldColor;
	public String shape_newColor;
	public String ebag_oldColor;
	public String ebag_newColor;
	public MapLayoutLineType oldLineStyle;
	public MapLayoutType oldMapStyle;
	public UndoOperationKey keyType;
	public int oldTxtAlignMent;
	public int oldObjIndex=0;
	public Bitmap nadeAsImage;
	public String info,info2;
	public String multimediapath="";
	public int mediatype=-1;
    public int grad=-1;
}
