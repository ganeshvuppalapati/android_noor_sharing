package com.ptg.mindmap.layouts;

import android.content.Context;
import android.graphics.Point;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;

import com.ptg.mimdmap.line.MPLinePoint;
import com.ptg.mimdmap.line.MyPoint;
import com.ptg.mimdmap.node.MPNode;
import com.ptg.mimdmap.node.MPNodeView;
import com.semanoor.manahij.ManahijApp;

import java.util.ArrayList;

public class FreeMapLayout extends MPLayout {
	Context fContext;

	public FreeMapLayout(Context context) {
		super(context);
		this.fContext = context;
	}

	public void setLayout(MPNode node) {
		MPEbag curEbag = ManahijApp.m_curEbag;
		MPNode curNode;
		if (node != null) {
			curNode = node;
		} else {
			curNode = curEbag;
		}
		MPNodeView shape = curNode.m_shape;

		// shape.setNodeText(curNode.m_strNode_text);

		ArrayList<MPNode> arr = new ArrayList<MPNode>();

		if (curNode.m_parentNode != null) {
			arr.add(curNode);
			setPositions(curNode.m_parentNode, arr,
					curNode.m_parentNode.m_isCollapsed);

		} else {

			setPositions(curNode, curNode.m_arrChildren, curNode.m_isCollapsed);
		}
		getCurrentDraggingNodeLine();

	}

	
	public void setPositions(MPNode parentNode,
			ArrayList<MPNode> arrChildNodes, boolean collapse) {
		// console.log('Parent page '+parentPage.m_textFormat.m_strNode_text);
		ArrayList<MyPoint> arrPoints = new ArrayList<MyPoint>();
		int draggedLayerPosition = -1;
		MPEbag curEbag = ManahijApp.m_curEbag;
		if (arrChildNodes != null)
			for (int i = 0; i < arrChildNodes.size(); i++) {

				MPNode childNode = arrChildNodes.get(i);
				MPNodeView childShape = childNode.m_shape;
				if (childNode != curEbag.m_currNodeBeingDragged) {

					if (parentNode.m_isCollapsed == true || collapse) {

						childShape.hidden = true;
						childShape.rect = new RectF();
						childShape.setVisibility(View.GONE);
						childNode.m_lp = null;
						setPositions(childNode, childNode.m_arrChildren, true);

					} else {
						childShape.hidden = false;
						childShape.setVisibility(View.VISIBLE);
						MyPoint cmd = new MyPoint();
						cmd.tagid = childNode;
						cmd.m_fArg1 = (float) (childNode.m_fX + childNode.m_fWidth / 2.0);
						cmd.m_fArg2 = (float) (childNode.m_fY + childNode.m_fHeight / 2.0);

						arrPoints.add(cmd);
						setPositions(childNode, childNode.m_arrChildren, false);

					}

				} else {
					draggedLayerPosition = i;
					setPositions(childNode, childNode.m_arrChildren, false);
					MyPoint cmd = new MyPoint();
					cmd.tagid = childNode;
					cmd.m_fArg1 = (float) (childNode.m_fX + childNode.m_fWidth / 2.0);
					cmd.m_fArg2 = (float) (childNode.m_fY + childNode.m_fHeight / 2.0);
					arrPoints.add(cmd);

				}

			}

		float xFrom = parentNode.m_fX + parentNode.m_fWidth / 2;
		float yFrom = parentNode.m_fY + parentNode.m_fHeight / 2;
		/*if(	parentNode.m_isCollapsed&&parentNode.m_parentNode!=null){
			parentNode.m_shape.rect = new RectF();
			parentNode.m_shape.setVisibility(View.GONE);
		}*/
		for (int j = 0; j < arrPoints.size(); j++) {

			if (draggedLayerPosition != j) {
				MyPoint cmd = arrPoints.get(j);
				// int sign = xFrom > ((CGCmd2 *)cmd).m_fArg1 ? 1 : -1;
				MPLinePoint lpoint = new MPLinePoint();

				// lpoint.nodeId = cmd.tagid;
				float cp1x = xFrom;
				float cp1y = (cmd.m_fArg2 + yFrom) / 2;
				float cp2x = cmd.m_fArg1;
				float cp2y = (cmd.m_fArg2 + yFrom) / 2;
				float x = cmd.m_fArg1;
				float y = cmd.m_fArg2;
				lpoint.moveTo(xFrom, yFrom);
				lpoint.bCurveto(cp1x, cp1y, cp2x, cp2y, x, y);
				// [parentNode.arrGLPoints addObject:lpoint];
				cmd.tagid.m_lp = lpoint;
			}
		}

	}

	public void getCurrentDraggingNodeLine() {
		{
			if (ManahijApp.m_curEbag.m_currNodeBeingDragged != null) {
				MPEbag curEbag = ManahijApp.m_curEbag;
				MPNodeView draggingShap = curEbag.m_currNodeBeingDragged.m_shape;
				MPNode draggingNode = curEbag.m_currNodeBeingDragged;
				if (draggingShap == null)
					return;
				MPNodeView nearestShape;// = self.m_node.m_parentNode;
				nearestShape = hitTest(draggingShap);
if(nearestShape!=null||draggingNode.m_parentNode!=null){
				if (nearestShape == null) {

					nearestShape = draggingNode.m_parentNode.m_shape;
				}

				MPLinePoint lpoint = new MPLinePoint();
				// lpoint.nodeId = draggingNode;
				float midX = curEbag.m_currNodeBeingDragged.m_fX
						+ curEbag.m_currNodeBeingDragged.m_fWidth / 2.0f;
				float midY = curEbag.m_currNodeBeingDragged.m_fY
						+ curEbag.m_currNodeBeingDragged.m_fHeight / 2.0f;
				float nmidX = nearestShape.m_node.m_fX
						+ nearestShape.m_node.m_fWidth / 2.0f;
				float nmidY = nearestShape.m_node.m_fY
						+ nearestShape.m_node.m_fHeight / 2.0f;
				lpoint.moveTo(midX, midY);

				float cp1x = midX;
				float cp1y = (midY + nmidY) / 2;
				float cp2x = nmidX;
				float cp2y = (midY + nmidY) / 2;

				lpoint.bCurveto(cp1x, cp1y, cp2x, cp2y, nmidX, nmidY);
				lpoint.isHighlight = true;
				// [draggingNode.m_parentNode.arrGLPoints addObject:lpoint];
				draggingNode.m_lp = lpoint;

			}
			}
		}
	}

	public MPNodeView hitTestForEllipse(MPNodeView shape) {
		if (shape == null)
			return null;

		RectF rect1 = shape.rect;
		rect1 = new RectF(shape.m_node.m_fX, shape.m_node.m_fY,
				shape.m_node.m_fX + shape.m_node.m_fWidth, shape.m_node.m_fY
						+ shape.m_node.m_fHeight);
		ViewGroup mapView = getMapView(ManahijApp.m_curEbag.m_shape);

		if (mapView instanceof ViewGroup)
			for (MPNodeView shape1 : getChilds(mapView)) {
				if ((shape1 instanceof MPNodeView) && !(shape.equals(shape1))) {
					RectF rect2 = new RectF(shape1.m_node.m_fX,
							shape1.m_node.m_fY, shape1.m_node.m_fX
									+ shape1.m_node.m_fWidth,
							shape1.m_node.m_fY + shape1.m_node.m_fHeight);
					if (RectF.intersects(rect1, rect2)) {
						return shape1;
					}

				}
			}

		return null;

	}

	public Point getelipsePoint(Point pos, MPNodeView parent, float ax,
			float by, Point range, int type, Point offset, MPNode childNode) {// In
																				// degrees

		float px, py;// ,px1,py1;
		boolean hasPoint = true;
		Point point = new Point();

		for (float i = range.x; i < range.y + 0.1;) {

			px = (float) (pos.x - (ax * Math.sin(i)) * Math.sin(0 * Math.PI) + (by * Math
					.cos(i)) * Math.cos(0 * Math.PI));
			py = (float) (pos.y + (by * Math.cos(i)) * Math.sin(0 * Math.PI) + (ax * Math
					.sin(i)) * Math.cos(0 * Math.PI));

			RectF rect = new RectF(px, py, offset.x, offset.y);

			MPNodeView newShape = childNode.m_shape;
			childNode.m_fX = px;
			childNode.m_fY = py;
			newShape.rect = rect;
			MPNodeView shape = hitTestForEllipse(newShape);

			// hasPoint = shape ? true:false;
			if (shape != null)
				hasPoint = true;
			else
				hasPoint = false;
			if (!hasPoint) {
				point = new Point((int) px, (int) py);
				break;
			}
			// [shape highlight:FALSE];
			switch (type) {

			case 0:
				i = (float) (i + Math.PI / 4);
				break;
			case 1:
				i = (float) (i + Math.PI / 8);
				break;
			case 2:
				i = (float) (i + Math.PI / 12);
				break;

			}

		}
		return point;
	}

	public Point caluculateRange(float angle, Point range, float diff) {

		if (angle <= 45 || angle >= 315) { // S1

			if (angle <= 45)
				diff = (float) (Math.PI / 4 * angle / 45);
			else
				diff = (float) (-Math.PI / 4 * angle / 315);

			range.x = (int) (-Math.PI / 4 + diff);
			range.y = (int) (Math.PI / 4 + diff);

		} else if (angle > 45 && angle <= 135) { // S2

			if (angle <= 90)
				diff = (float) (-Math.PI / 4 * (90 - angle) / 45);
			else
				diff = (float) (Math.PI * 3 / 4 * (angle - 90) / 135);

			range.x = (int) (Math.PI / 4 + diff);
			range.y = (int) (Math.PI * 3 / 4 + diff);

		} else if (angle > 135 && angle <= 225) { // S3

			if (angle <= 180)
				diff = (float) (-Math.PI * 3 / 4 * (180 - angle) / 135);
			else
				diff = (float) (Math.PI * 5 / 4 * (angle - 180) / 225);

			range.x = (int) (Math.PI * 3 / 4 + diff);
			range.y = (int) (Math.PI * 5 / 4 + diff);

		} else if (angle > 225 && angle < 315) { // S4

			if (angle <= 270)
				diff = (float) (-Math.PI * 5 / 4 * (270 - angle) / 225);
			else
				diff = (float) (Math.PI * 7 / 4 * (angle - 270) / 315);

			range.x = (int) (Math.PI * 5 / 4 + diff);
			range.y = (int) (Math.PI * 7 / 4 + diff);

		}

		return range;
	}

	public Point caluculateRange1(Point range, int type) {

		if (type == 1) { // S1
			range.x = (int) (-Math.PI / 2 + 0.1);
			range.y = 0;

		} else if (type == 2) { // S2

			range.x = (int) 0.1;
			range.y = (int) (Math.PI / 2);

		} else if (type == 3) { // S3

			range.x = (int) (Math.PI / 2 + 0.1);
			range.y = (int) Math.PI;

		} else if (type == 4) { // S4

			range.x = (int) (Math.PI + 0.1);
			range.y = (int) (Math.PI + (Math.PI / 2) - 0.1);

		}

		return range;
	}

	double M_PI = 3.14159265358979323846264338327950288;

	public Point calculateNewChildPosition1(MPNode parentNode, MPNode childNode) {

		Point pos = new Point((int) parentNode.m_fX, (int) parentNode.m_fY);

		Point pos1 = null;
		boolean hasPoint = true;
		float cy = (float) (parentNode.m_fWidth * 1.9);
		float cx = (float) (parentNode.m_fHeight * 1.9);
		float ax = (float) 0.0, ay = (float) 0.0;
		Point range = new Point();

		range.x = 0;
		range.y = (int) (3 * M_PI);

		int type = 0, aType = 0;
		int count = 0;

		Point offset = new Point();

		offset.x = childNode.m_fWidth;
		offset.y = childNode.m_fHeight;

		if (parentNode.m_parentNode != null) {

			Point pt2 = new Point((int) parentNode.m_parentNode.m_fX,
					(int) parentNode.m_parentNode.m_fY);

			float angle = (float) ((Math.atan2(pt2.y - pos.y, pt2.x - pos.x)) * 180 / Math.PI);

			float diff = 0;

			angle = 180 + angle;

			if (angle == 0 || angle == 360)
				angle = 0;

			range.x = 0;
			range = caluculateRange(angle, range, diff);

		}

		while (hasPoint) {

			type = (type == 0) ? 1 : 0;

			if (count > 0) {

				if (type == 0) {
					cx = (float) ((Math.sqrt(3) / 2 * ay) + 45);
					cy = (float) ((Math.sqrt(3) / 2 * ax) * 1.9);
				} else {
					cx = (float) ((Math.sqrt(3) / 2 * ax) + 45);
					cy = (float) ((Math.sqrt(3) / 2 * ay) * 1.9);
				}

				type = (type == 0) ? 1 : 0;
				if (parentNode.m_parentNode != null) {
					aType = 1;

					if (count > 3)
						aType = 2;
				} else
					aType = 0;

			}

			ax = cx;
			ay = cy;

			pos1 = getelipsePoint(pos, parentNode.m_shape, cx, cy, range,
					aType, offset, childNode);
			hasPoint = pos1.equals(new Point()) ? true : false;

			count++;
		}

		if (!hasPoint) {
			return pos1;
		} else {

			return new Point();
		}

		// return new Point();
	}

	public void generatenewPositions(MPNode childs) {
		if (childs.getM_arrChildren() != null&& childs.getM_arrChildren().size() > 0) 
		for (MPNode node : childs.m_arrChildren) {
			generatenewPositions(node);
		}
			
		if (childs.m_fX == 0
					|| childs.m_fY == 0
					|| ManahijApp.m_curEbag.m_isKeepFreeMapSamePositions == false) {
			if(childs.m_parentNode!=null){	
			Point pt = calculateNewChildPosition1(childs.m_parentNode, childs);
				childs.m_fX = pt.x;
				childs.m_fY = pt.y;
			}
			
			
			}
		}
	
}
