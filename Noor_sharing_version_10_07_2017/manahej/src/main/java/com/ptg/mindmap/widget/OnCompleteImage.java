package com.ptg.mindmap.widget;

import android.graphics.Bitmap;

public interface OnCompleteImage {
 public void imageLoaded(Bitmap bitmap);
}
