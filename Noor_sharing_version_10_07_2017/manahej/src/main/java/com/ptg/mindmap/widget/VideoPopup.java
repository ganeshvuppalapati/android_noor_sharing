package com.ptg.mindmap.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.semanoor.manahij.R;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class VideoPopup extends Dialog implements SurfaceHolder.Callback,OnSeekBarChangeListener
{
	MediaPlayer mediaPlayer;
	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;
	//boolean pausing = false;;
	String mediafilepath = "";
	RelativeLayout errorLayout;
	RelativeLayout videolayout;
	int mediatype = 0;
	Context Mcont;
	TextView seektext;
	private double startTime = 0;
	private double finalTime = 0;
	private Handler myHandler = new Handler();
	ImageButton fullscreenbtn;
	SeekBar playerseek;
	ToggleButton playpause_btn;
	LinearLayout contollerview;
	boolean isfullscreen=false;
	RelativeLayout.LayoutParams layoutParms;
	int errorWidth=400,errorheight=400;
	public VideoPopup(Context context, int mediatype, final String path) 
	{
		super(context/*,android.R.style.Theme_Black_NoTitleBar*/);
		this.mediatype = mediatype;
		this.mediafilepath = path;
		this.Mcont=context;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custom_dialog);
		playerseek=(SeekBar)findViewById(R.id.video_seek);
		fullscreenbtn=(ImageButton)findViewById(R.id.fullscreenbtn);
		seektext=(TextView)findViewById(R.id.seektext);
		playpause_btn=(ToggleButton)findViewById(R.id.playpause_btn);
		errorLayout=(RelativeLayout)findViewById(R.id.errorLayout);
		videolayout=(RelativeLayout)findViewById(R.id.videolayout);
		//playerseek.setClickable(false);
		// setTitle(this.videoName);
		errorLayout.setVisibility(View.GONE);
		 layoutParms = (RelativeLayout.LayoutParams) errorLayout.getLayoutParams();
		layoutParms.width=errorWidth;
		layoutParms.height=errorheight;
		errorLayout.setLayoutParams(layoutParms);
		contollerview=(LinearLayout)findViewById(R.id.contollerview);
		contollerview.setVisibility(View.VISIBLE);
		getWindow().setFormat(PixelFormat.UNKNOWN);
		if(mediatype==3){
			surfaceView = (SurfaceView)findViewById(R.id.videoElem);

			surfaceHolder = surfaceView.getHolder();
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
			surfaceView.setZOrderOnTop(true);
			surfaceView.setVisibility(View.VISIBLE);
			surfaceView.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					if(contollerview.getVisibility()==View.VISIBLE){
						contollerview.setVisibility(View.GONE);

					}
					else{
						contollerview.setVisibility(View.VISIBLE);
						if(mediaPlayer.isPlaying()){
							mediaPlayer.pause();
							//play.setVisibility(View.VISIBLE);
						}

					}
					return false;
				}
			});
		}
		if(mediatype==2){
			fullscreenbtn.setVisibility(View.GONE);
			errorWidth=400;
			errorheight=100;
		}
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		playerseek.setOnSeekBarChangeListener(this);

		String path = Environment.getExternalStorageDirectory().toString();
		try {
			errorLayout.setVisibility(View.GONE);
			mediaPlayer.setDataSource(Mcont,Uri.parse(mediafilepath));
			mediaPlayer.prepare();
		} /*catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */catch (IOException e) {
			// TODO Auto-generated catch block
			invalidateErrorlayout();
			errorLayout.setVisibility(View.VISIBLE);
			e.printStackTrace();
		}

		finalTime = mediaPlayer.getDuration();
		startTime = mediaPlayer.getCurrentPosition();
		playerseek.setMax((int) finalTime);
		playerseek.setProgress((int)startTime);
		if(mediatype==3){
			setVideoSize(400,400);
			//Handle the click-event on start button
			//Handle the click-event on pause button
			fullscreenbtn.setOnClickListener(new View.OnClickListener(){


				@Override
				public void onClick(View v) {
					if(!isfullscreen){
						
						setVideoSize(MindMapActivity.width-60, MindMapActivity.height-300);
						isfullscreen=true;
					}else{
						setVideoSize(400,400);
						isfullscreen=false;
					}
				}
			}); 
		}
		myHandler.postDelayed(UpdateSongTime,1000);

		

		

		mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				playpause_btn.setChecked(true);



			}
		});

		//Handle the click-event on stop button
		/*	pause.setOnClickListener(new Button.OnClickListener(){
			@Override
			public void onClick(View v)
			{
				mediaPlayer.pause(); 
				play.setVisibility(View.VISIBLE);
				pause.setVisibility(View.GONE);
			}}); 
		 */
		playpause_btn.setOnCheckedChangeListener(new ToggleButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(!isChecked){
					if(mediaPlayer.isPlaying()){
						mediaPlayer.reset();
						mediaPlayer = new MediaPlayer();
						try {
							mediaPlayer.setDataSource(Mcont,
									Uri.parse(mediafilepath));
							mediaPlayer.prepare();
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalStateException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if(mediatype==3)
						mediaPlayer.setDisplay(surfaceHolder);
					mediaPlayer.start();  

				}
				else
					mediaPlayer.pause(); 
			}
		});

		/*	play.setOnClickListener(new Button.OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//pausing = false;

				if(mediaPlayer.isPlaying()){
					mediaPlayer.reset();
				}

				mediaPlayer.setDisplay(surfaceHolder);
				mediaPlayer.start();  
				play.setVisibility(View.GONE);
				pause.setVisibility(View.VISIBLE);
				finalTime = mediaPlayer.getDuration();
				startTime = mediaPlayer.getCurrentPosition();
				playerseek.setProgress((int)startTime);
				myHandler.postDelayed(UpdateSongTime,1000);
			}});
		 */
		



	}
	
	private void setVideoSize(float width,float height) {
		
		
		// // Get the dimensions of the video
		int videoWidth = mediaPlayer.getVideoWidth();
		int videoHeight = mediaPlayer.getVideoHeight();
		float videoProportion = (float) videoWidth / (float) videoHeight;

		float screenProportion = (float) width / (float) height;

		// Get the SurfaceView layout parameters
		android.view.ViewGroup.LayoutParams lp = surfaceView.getLayoutParams();
		if (videoProportion > screenProportion) {
			lp.width = (int) width;
			lp.height = (int) ((float) width / videoProportion);
			
		} else if (videoProportion < screenProportion){
			lp.width = (int) (videoProportion * (float) height);
			//lp.height = (int) height; @ganesh
			lp.height = (int) (videoProportion * (float) height);
		}else{
			lp.width = (int) (width);
			lp.height = (int) height;
		}
		errorWidth=lp.width;
		errorheight=lp.height;
		invalidateErrorlayout();
		// Commit the layout parameters
		surfaceView.setLayoutParams(lp);
	}
	
	public void stopPlayer(){
		if(mediaPlayer.isPlaying()){
			mediaPlayer.stop();
			mediaPlayer.reset();
		}
	}
	
	public void invalidateErrorlayout(){
		layoutParms.width=errorWidth;
		layoutParms.height=errorheight;
		errorLayout.setLayoutParams(layoutParms);
		errorLayout.invalidate();
	}
	/*
	@Override
	public void setOnDismissListener(OnDismissListener listener) {
		// TODO Auto-generated method stub

		if(mediaPlayer.isPlaying()){
			mediaPlayer.stop();
			mediaPlayer.reset();
		}
		super.setOnDismissListener(listener);
	}*/
	private Runnable UpdateSongTime = new Runnable() {
		public void run() {
			startTime = mediaPlayer.getCurrentPosition();
			playerseek.setProgress((int)startTime);
			//float minutes=startTime/60
			//	seektext.setText(gettime(startTime));
			myHandler.postDelayed(this, 1000);
			seektext.setText(gettime(startTime));
		}
	};

	public  String gettime(double time)  {

		String sfr=	String.format("%d:%d", 
				TimeUnit.MILLISECONDS.toMinutes((long) time),
				TimeUnit.MILLISECONDS.toSeconds((long) time) - 
				TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) time))
				);

		return sfr ;
	}
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) { }


	@Override
	public void surfaceCreated(SurfaceHolder holder) { }


	@Override
	public void surfaceDestroyed(SurfaceHolder holder) { 
		if(mediaPlayer.isPlaying()){
			mediaPlayer.stop();

		}
	}


	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		mediaPlayer.pause();
	}


	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		//	myHandler.removeCallbacks(UpdateSongTime);
		startTime =seekBar.getProgress();
		if(!playpause_btn.isChecked())
			mediaPlayer.start();
		// forward or backward to certain seconds
		mediaPlayer.seekTo((int) startTime);
		// update timer progress again
		//updateProgressBar();
	}
	public int progressToTimer(int progress, double finalTime) {
		int currentDuration = 0;
		finalTime = (int) (finalTime / 1000);
		currentDuration = (int) ((((double)progress) / 100) * finalTime);

		// return current duration in milliseconds
		return currentDuration ;
	}
	public void updateProgressBar() {
		myHandler.postDelayed(UpdateSongTime, 1000);        
	}	
}
