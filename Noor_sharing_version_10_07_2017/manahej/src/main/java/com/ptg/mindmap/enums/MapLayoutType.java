package com.ptg.mindmap.enums;

public enum MapLayoutType {
	 MPLayout_CircularMap ,
			    MPLayout_LeftMap,
			    MPLayout_RightMap,
			    MPLayout_FreeMap,
			    MPLayout_Count
}
