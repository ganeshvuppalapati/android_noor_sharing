package com.ptg.mindmap.enums;

public enum MapLayoutLineType {
	 MPLayout_Line_Straight(1) ,
			    MPLayout_Line_Curved(2),
			    MPLayout_Line_CurvedThick(3),
			    MPLayout_Line_Count(4);
int value;
	 MapLayoutLineType(int value){
		this.value= value;
	 }
public int getType(){
	return this.value;
}
}
