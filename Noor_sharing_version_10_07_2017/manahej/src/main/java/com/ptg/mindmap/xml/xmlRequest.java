package com.ptg.mindmap.xml;

import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

@SuppressWarnings("deprecation")
public class xmlRequest  extends AsyncTask<Void, Void, String>{
	String xml_string="",xml_url;
	 public  xmlRequest(String url) {
		 this.xml_url=url;
	 }
	 

	@Override
	protected String doInBackground(Void... params) {
		try {
			 
			DefaultHttpClient httpClient = new DefaultHttpClient();
			 HttpGet httpPost = new HttpGet(this.xml_url);
			 HttpResponse httpResponse = httpClient.execute(httpPost);
			 HttpEntity httpEntity = httpResponse.getEntity();
			 xml_string = EntityUtils.toString(httpEntity);
			 
			 } catch (UnsupportedEncodingException e) {
			 e.printStackTrace();
			 } catch (ClientProtocolException e) {
			 e.printStackTrace();
			 } catch (IOException e) {
			 e.printStackTrace();
			 }
			 
			 
	return xml_string;
	}
	 @Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
	}
	}
