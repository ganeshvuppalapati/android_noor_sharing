package com.ptg.mindmap.xml;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.TypedValue;

import com.ptg.mimdmap.node.MPNode;
import com.ptg.mindmap.enums.MapLayoutLineType;
import com.ptg.mindmap.enums.MapLayoutShapeType;
import com.ptg.mindmap.enums.MapLayoutType;
import com.ptg.mindmap.layouts.MPEbag;
import com.ptg.mindmap.widget.AppDict;
import com.ptg.mindmap.widget.MindMapActivity;
import com.semanoor.manahij.ManahijApp;

import org.apache.commons.lang.StringEscapeUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Stack;
import java.util.regex.Pattern;

public class MindMapXmlHandler extends DefaultHandler {

    private ArrayList<MPNode> results;
    private Stack<MPNode> stack;
    private StringBuffer buffer = new StringBuffer();
    Xmltags tags=new Xmltags();
    MPEbag ebag = new MPEbag();
    boolean isEbag=false;
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    	try{
    	if (tags.SemaFileSystem.equals(localName)) {
    		isEbag =true;
    		ebag = new MPEbag();
        }
        if (tags.readAllSemaNodesinAMapResult.equals(localName)) {
        	isEbag =false;
            results = new ArrayList<MPNode>();
            stack = new Stack<MPNode>();
        } else if (tags.SemaNode.equals(localName)) {
            MPNode currentMPNode = new MPNode();
            //currentMPNode.setId(attributes.getValue("id"));
            stack.push(currentMPNode);
        } else /*if ("title".equals(localName)) */{
            buffer.setLength(0);
        }
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
    	try{
    	if (tags.SemaFileSystem.equals(localName)) {
    		ebag .setM_arrChildren(results);
    	}
    	// checkinh end of file sema
    	else if (tags.SemaNode.equals(localName)) {
            MPNode currentMPNode = stack.pop();
            if (stack.isEmpty()) {
                results.add(currentMPNode);
            } else {
                MPNode parent = stack.peek();
                parent.addSection(currentMPNode);
            }
        }
        else if (tags.m_strMapLayoutFillColor.equals(localName)) {
			String nodeText = StringEscapeUtils.unescapeJava(buffer.toString().trim());
			ebag.m_strMapLayoutFillColor = nodeText;
			;
		} else if (tags.m_iMapWidth.equals(localName)) {
			String nodeText = StringEscapeUtils.unescapeJava(buffer.toString().trim());
			ebag.setM_fWidth((int) Float.parseFloat(nodeText));
		} else if (tags.m_iMapHeight.equals(localName)) {
			String nodeText = StringEscapeUtils.unescapeJava(buffer.toString().trim());
			ebag.setM_fHeight((int) Float.parseFloat(nodeText));
		} else if (tags.m_strName.equals(localName)) {
			String nodeText = StringEscapeUtils.unescapeJava(buffer.toString().trim());
			ebag.setM_strNode_text(nodeText);
		}
		else if (tags.m_strMapDescription.equals(localName)) {
			String nodeText = StringEscapeUtils.unescapeJava(buffer.toString().trim());
			ebag.setM_strNode_text(nodeText);
		}
		
		else if (tags.m_iNumberOfRightNodes.equals(localName)) {
			ebag.setM_iNumberOfRightNodes((Integer.parseInt(buffer.toString().trim())));
		}
		else if (tags.m_iMapStyle.equals(localName)) {
			String nodeText = StringEscapeUtils.unescapeJava(buffer.toString().trim());
			MapLayoutLineType type = null;
			switch (Integer.parseInt(nodeText)) {
			case 1:
				type = MapLayoutLineType.MPLayout_Line_Straight;
				break;
			case 2:
				type = MapLayoutLineType.MPLayout_Line_Curved;
				break;
			case 3:
				type = MapLayoutLineType.MPLayout_Line_CurvedThick;
				break;
			case 4:
				type = MapLayoutLineType.MPLayout_Line_Count;
				break;
			}
			ebag.setM_iMapLayoutLine(type);
		}
		else if (tags.m_iMapLayout.equals(localName)) {
			String nodeText = StringEscapeUtils.unescapeJava(buffer.toString().trim());
			MapLayoutType type = null;
			int x = Integer.parseInt(nodeText);

			switch (x) {

			case 0:
				type = MapLayoutType.MPLayout_CircularMap;
				break;
			case 1:
				type = MapLayoutType.MPLayout_LeftMap;
				break;
			case 2:
				type = MapLayoutType.MPLayout_RightMap;
				break;
			case 3:
				type = MapLayoutType.MPLayout_FreeMap;
				break;
			case 4:
				type = MapLayoutType.MPLayout_Count;
				break;
			}
			ebag.setM_iMapLayout(type);
		}
        else if (tags.children.equals(localName)) {
            MPNode currentMPNode = stack.peek();
            for(MPNode nodes:currentMPNode.getXmlSection()){
            	nodes.m_parentNode=currentMPNode;
            }
            currentMPNode.setM_arrChildren(currentMPNode.getXmlSection());
           // currentMPNode.setTitle(buffer.toString().trim());
        } 
        else if (tags.m_iId.equals(localName)) {
			if (isEbag) {
				ebag.setM_iId(Integer.parseInt(buffer.toString().trim()));
			} else {
				MPNode currentMPNode = stack.peek();
				currentMPNode.setM_iId(Integer.parseInt(buffer.toString().trim()));
			}

		}
        
        else if (tags.m_iLevelId.equals(localName)) {
        	MPNode currentMPNode = stack.peek();
   		 currentMPNode.setM_iLevelId(Integer.parseInt(buffer.toString().trim()));
			
		}
       else if (tags.m_iParentId.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
       	currentMPNode.setM_iParentId((Integer.parseInt(buffer.toString().trim())));
			
		}
       else if (tags.m_fX.equals(localName)) {
    	   if(isEbag){
    		  ebag.setM_fX(Float.parseFloat(buffer.toString().trim()));
    	   }else{
    	   MPNode currentMPNode = stack.peek();
       	currentMPNode.setM_fX(Float.parseFloat(buffer.toString().trim()));
    	   }
			
		}
       else if (tags.m_fY.equals(localName)) {
    	   if(isEbag){
     		  ebag.setM_fY(Float.parseFloat(buffer.toString().trim()));
     	   }else{
    	   MPNode currentMPNode = stack.peek();
       	currentMPNode.setM_fY(Float.parseFloat(buffer.toString().trim()));
     	   }
			
		}
       else if (tags.m_fWidth.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
      // 	currentMPNode.setM_fWidth((int)Float.parseFloat(buffer.toString().trim()));
			
		}
       else if (tags.m_fHeight.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
       //	currentMPNode.setM_fHeight(((int)Float.parseFloat(buffer.toString().trim())));
			
		}
       else if (tags.m_strNode_text.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
    	  String nodeText= StringEscapeUtils.unescapeJava(buffer.toString().trim());
       	currentMPNode.setM_strNode_text(nodeText);
			
		}
       else if (tags.m_strStartDate.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
    	  String nodeText= buffer.toString().trim();
       	currentMPNode.setM_start_date(nodeText);
			
		}
       else if (tags.m_strNode_TextFontFamily.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
			String nodeText = StringEscapeUtils.unescapeJava(buffer.toString().trim());
			if(isEbag)
				ebag.setM_strFontName(nodeText);
			else
				currentMPNode.setM_strFontName(nodeText);
		}
       else if (tags.m_strDueDate.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
    	  String nodeText= buffer.toString().trim();
       	currentMPNode.setM_end_date(nodeText);
			
		}
       else if (tags.m_strResourceType.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
    	  String nodeText= buffer.toString().trim();
       	currentMPNode.setM_resource(nodeText);
			
		}
       else if (tags.m_strDurationType.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
    	  String nodeText= buffer.toString().trim();
       	currentMPNode.setM_duration_type(nodeText);
			
		}
       else if (tags.m_iResourceDuration.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
    	  int nodeText= Integer.parseInt(buffer.toString().trim());
       	currentMPNode.setM_duration(nodeText);
			
		}
       else if (tags.m_iNode_TextSize.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
       	currentMPNode.setM_iNode_TextSize(AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP, (Integer.parseInt(buffer.toString().trim()))));
			
		}
       else if (tags.m_iNode_Order.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
       	currentMPNode.setM_iNode_Order(((Integer.parseInt(buffer.toString().trim()))));
			
		}
       else if (tags.m_iGradientType.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
       	currentMPNode.m_iGradientType=Integer.parseInt(buffer.toString().trim());
			
		}
       else if (tags.m_isRightnode.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
       	if(Integer.parseInt(buffer.toString().trim())==1)
       		currentMPNode.m_isRightnode=true;
			else
				currentMPNode.m_isRightnode=false;
			
		}
       else if (tags.m_iIsAboveCenter.equals(localName)) {
    	   MPNode currentMPNode = stack.peek();
       	if(Integer.parseInt(buffer.toString().trim())==1)
       		currentMPNode.m_isAboveCenter=true;
			else
				currentMPNode.m_isAboveCenter=false;
			
		}
       else if(tags.m_strNodeShapeFill.equals(localName)){
    	   MPNode currentMPNode = stack.peek();
    	   currentMPNode.setHexaColor(buffer.toString().trim());
    	   currentMPNode.setBackgroundColor(Color.parseColor(buffer.toString().trim()));
       }
       else if(tags.m_strNode_TextColor.equals(localName)){
    	   MPNode currentMPNode = stack.peek();
    	   if(buffer.toString().trim().contains("#"))
    		   currentMPNode.setTextColor(buffer.toString().trim());
    			else
    				currentMPNode.setTextColor(buffer.toString().trim().replace("0x","#"));
    	 //  currentMPNode.setTextColor(buffer.toString().trim());
       }
       else if(tags.m_strNode_Notes.equals(localName)){
    	   MPNode currentMPNode = stack.peek();
    	  String  m_strNode_Notes=buffer.toString().trim().replace("(null)", "").replace("\n", "").trim();
    	  String strNodeNotes= StringEscapeUtils.unescapeJava(m_strNode_Notes);
    	   currentMPNode.setM_strNode_Notes(strNodeNotes);
       }
       else if(tags.m_iNodeShape.equals(localName)){
    	   MPNode currentMPNode = stack.peek();
    	   MapLayoutShapeType type=null;
			switch(Integer.parseInt(buffer.toString().trim())){
			case 1:
				 type=MapLayoutShapeType.MPLAYOUT_SHAPE_RECTANGLE;
				break;
			case 2:
				 type= MapLayoutShapeType.MPLAYOUT_SHAPE_ROUNDEDRECTANGLE;
				break;
			case 3:
				 type=MapLayoutShapeType.MPLAYOUT_SHAPE_OVAL;
				break;
			case 4:
				 type=MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON;
				break;
			case 5:
				 type=MapLayoutShapeType.MPLAYOUT_SHAPE_COMMENT;
				break;
			case 6:
				 type=MapLayoutShapeType.MPLAYOUT_SHAPE_FREEFORM;
				break;
			case 7:
				 type=MapLayoutShapeType.MPLAYOUT_SHAPE_OCTAGON;
				break;
			case 8:
				 type=MapLayoutShapeType.MPLAYOUT_SHAPE_INVERSE_TRIANGLE;
				break;
			case 9:
				 type=MapLayoutShapeType.MPLAYOUT_SHAPE_RHOMBUS;
				break;
			}
    	   currentMPNode.setM_iMapLayoutShape(type);
       }
       /*else if(tags.m_strSmileImage.equals(localName)){
    	   MPNode currentMPNode = stack.peek();
    	   String base64= buffer.toString().trim().replace("data:image/png;base64,", "");
		   if(!base64.equals("(null)")&&!base64.equals(""))
	   currentMPNode.setM_smile_image(AppDict.decodeBase64((base64)));
    	
       }*/
       else if(tags.m_strAttachmentImage.equals(localName)){
    	   MPNode currentMPNode = stack.peek();
    	   String base64=   buffer.toString().trim().replace("data:image/png;base64,", "");
    		   if(!base64.equals("(null)")&&!base64.equals("")){
    	   currentMPNode.setM_Attachement_Image(AppDict.decodeBase64((base64)));
    	   currentMPNode.mediaType=0;
    	   currentMPNode.imageAttachementype = 2;
    		   }
    	   
      
       }
       else if(tags.m_strEmbedImage.equals(localName)){
    	   MPNode currentMPNode = stack.peek();
    	   String base64=   buffer.toString().trim().replace("data:image/png;base64,", "");
    		   if(!base64.equals("(null)")&&!base64.equals("")){
    			   currentMPNode.base64image=base64;
    	   currentMPNode.embededImage= AppDict.decodeBase64((base64));
    	   currentMPNode.mediaType=0;
    	   currentMPNode.imageAttachementype = 1;
    		   }
    	   
      
       }
       else if(tags.m_strBgImage.equals(localName)){
    	   MPNode currentMPNode = stack.peek();
    	   String base64=   buffer.toString().trim().replace("data:image/png;base64,", "");
    		   if(!base64.equals("(null)")&&!base64.equals("")){
    			   currentMPNode.base64image=base64;
    	   currentMPNode.setM_background_image(AppDict.decodeBase64((base64)));
    	   currentMPNode.mediaType=0;
    	   currentMPNode.imageAttachementype = 3;
    		   }
    	   
      
       }
       else if (tags.m_iIsBold.equals(localName)) {
       	MPNode currentMPNode = stack.peek();
  		 currentMPNode.setM_iIsBold(buffer.toString().trim());
			
		}
       else if (tags.m_iIsItalic.equals(localName)) {
          	MPNode currentMPNode = stack.peek();
     		 currentMPNode.setM_iIsItalic(buffer.toString().trim());
   			
   		}
       else if (tags.m_iIsUnderline.equals(localName)) {
          	MPNode currentMPNode = stack.peek();
     		 currentMPNode.setM_iIsUnderline(buffer.toString().trim());
   			
   		}
       else if (tags.m_strMultimediaType.equals(localName)) {
         	MPNode currentMPNode = stack.peek();
    		 currentMPNode.setM_strMultimediaType(buffer.toString().trim());
  			
  		}
       else if (tags.m_strMultimediaUrl.equals(localName)) {
        	MPNode currentMPNode = stack.peek();
   		 currentMPNode.setM_strMultimediaUrl(buffer.toString().trim().replace(" ", "").replace("\n", "").replace("\r", ""));
   		 if(currentMPNode.getM_strMultimediaType().equals("Image")){
   			 Bitmap dummyImage=null;
    			if(!currentMPNode.getM_strMultimediaUrl().equals("")){
    			 currentMPNode.imageUrl=currentMPNode.getM_strMultimediaUrl();
    			 String x=currentMPNode.imageUrl;
    			 String[] arrayList =x.split(Pattern.quote("?"));
    			 if(arrayList.length>0&&arrayList.length>2){
    				 String[] arrarS = arrayList[1].replace("sz=", "").split(Pattern.quote("+"));
    				 String width=arrarS[0].replace("w","");
    				 String height=arrarS[1].replace("h","");
    				 currentMPNode.bitmapHeight=Integer.parseInt(height);
    				 currentMPNode.bitmapWidth=Integer.parseInt(width);
    				 dummyImage= Bitmap.createScaledBitmap(MindMapActivity.BitmapDummyImage,Integer.parseInt(width) , Integer.parseInt(height), true);
    			 }
    				if(currentMPNode.getM_strMultimediaUrl().contains("Embed")){
    					currentMPNode.imageAttachementype = 1;
    					currentMPNode.embededImage=dummyImage;//getBitmapFromURL(currentMPNode.getM_strMultimediaUrl());
           				currentMPNode.mediaType=0;
    					
    				}else if(currentMPNode.getM_strMultimediaUrl().contains("ImgNode")){
    					currentMPNode.imageAttachementype = 3;
    					currentMPNode.m_background_image=dummyImage;//getBitmapFromURL(currentMPNode.getM_strMultimediaUrl());
           				currentMPNode.mediaType=0;
    				}
    				else{
    					currentMPNode.m_attachement_image= MindMapActivity.BitmapDummyImage;
    							//getBitmapFromURL(currentMPNode.getM_strMultimediaUrl());
    					currentMPNode.imageAttachementype = 2;
           				currentMPNode.mediaType=0;
    				}
    			}
    			}
   		 else if(currentMPNode.getM_strMultimediaType().equals("Video")){
    			if(!currentMPNode.getM_strMultimediaUrl().equals("")){
       				currentMPNode.multimediaurl=currentMPNode.getM_strMultimediaUrl();
       				currentMPNode.mediaType=3;
       			}
       			 
       		 }	
   		 else if(currentMPNode.getM_strMultimediaType().equalsIgnoreCase("Audio")){
 			if(!currentMPNode.getM_strMultimediaUrl().equals("")){
    				currentMPNode.multimediaurl=currentMPNode.getM_strMultimediaUrl();
    				currentMPNode.mediaType=2;
    			}
    			 
    		 }	
 			
 		}
       else if (tags.m_strHyperlinkUrl.equals(localName)) {
       	MPNode currentMPNode = stack.peek();
  		 currentMPNode.setM_strHyperlinkUrl(buffer.toString().trim().replace("\n", "").trim());
			
		}
       else if (tags.m_strHyperlinkDescription.equals(localName)) {
          	MPNode currentMPNode = stack.peek();
     		 currentMPNode.setM_strHyperlinkDescription(buffer.toString().trim());
   			
   		}
       else if (tags.m_iSmileys.equals(localName)) {
         	MPNode currentMPNode = stack.peek();
    		 currentMPNode.smileIconPosition=Integer.parseInt(buffer.toString().trim());
  			if(currentMPNode.smileIconPosition!=0){
  				Bitmap smileImg = BitmapFactory.decodeResource(ManahijApp.mAppContext.getResources(),
						AppDict.icons[currentMPNode.smileIconPosition]);
				smileImg = Bitmap.createScaledBitmap(smileImg, 40, 40, true);
				currentMPNode.setSmileIconPosition(currentMPNode.smileIconPosition);
				currentMPNode.setM_smile_image(smileImg);
  			}
  		}
       else if (tags.m_iPriority.equals(localName)) {
        	MPNode currentMPNode = stack.peek();
   		 currentMPNode.priorityCharacterPosition=Integer.parseInt(buffer.toString().trim());
 			if(currentMPNode.priorityCharacterPosition!=0){
 				Bitmap smileImg = BitmapFactory.decodeResource(ManahijApp.mAppContext.getResources(),
						AppDict.priority[currentMPNode.priorityCharacterPosition-1]);
				smileImg = Bitmap.createScaledBitmap(smileImg,40, 40, true);
				currentMPNode.setPriorityCharacter(smileImg);
 			}
 		}
       else if (tags.m_iCompletion.equals(localName)) {
       	MPNode currentMPNode = stack.peek();
  		 currentMPNode.completionCharacterPosition=Integer.parseInt(buffer.toString().trim());
			if(currentMPNode.completionCharacterPosition!=0){
				Bitmap smileImg = BitmapFactory.decodeResource(ManahijApp.mAppContext.getResources(),
						AppDict.completion[currentMPNode.completionCharacterPosition-1]);
				smileImg = Bitmap.createScaledBitmap(smileImg, 40, 40, true);
				currentMPNode.setCompletionCharacter(smileImg);
			}
		}
		else if (tags.m_iSymbol.equals(localName)) {
	       	MPNode currentMPNode = stack.peek();
	  		 currentMPNode.dCharacterPosition=Integer.parseInt(buffer.toString().trim());
				if(currentMPNode.dCharacterPosition!=0){
					Bitmap smileImg = BitmapFactory.decodeResource(ManahijApp.mAppContext.getResources(),
							AppDict.tdicons[currentMPNode.dCharacterPosition-1]);
					smileImg = Bitmap.createScaledBitmap(smileImg, 40, 40, true);
					currentMPNode.setdCharacter(smileImg);
				}
			}
        
		else if (tags.m_iCheckedStatus.equals(localName)) {
	       	MPNode currentMPNode = stack.peek();
	  		 currentMPNode.genCharacterPosition=Integer.parseInt(buffer.toString().trim());
				if(currentMPNode.genCharacterPosition!=0){
					Bitmap smileImg = BitmapFactory.decodeResource(ManahijApp.mAppContext.getResources(),
							AppDict.genicons[currentMPNode.genCharacterPosition-1]);
					smileImg = Bitmap.createScaledBitmap(smileImg, 40, 40, true);
					currentMPNode.setGenCharacter(smileImg);
				}
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
        
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        buffer.append(ch, start, length);
    }

    public MPEbag getResults() {
        return ebag;
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            //myBitmap=  Bitmap.createScaledBitmap(myBitmap,400,400,true);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
}
