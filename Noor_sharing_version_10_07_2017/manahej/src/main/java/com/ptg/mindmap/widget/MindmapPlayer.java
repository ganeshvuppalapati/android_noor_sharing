package com.ptg.mindmap.widget;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptg.gesturedetectors.MoveGestureDetector;
import com.ptg.mimdmap.node.MPNode;
import com.ptg.mimdmap.node.MPNodeView;
import com.ptg.mindmap.enums.MapState;
import com.ptg.mindmap.layouts.MPLayout;
import com.ptg.views.TwoDScrollView;
import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.PopoverView;
import com.semanoor.source_sboookauthor.UndoRedoObjectData;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by karthik on 10-12-2015.
 */
public class MindmapPlayer extends RelativeLayout implements View.OnTouchListener, View.OnClickListener,PopoverView.PopoverViewDelegate {
    private BookViewActivity context;
    private static final int RESULT_LOAD_IMAGE = 0;
    public static RelativeLayout canvasView;
    public ViewGroup drawingView;
    ArrayList<String> fontsList;
    public static Bitmap BitmapDummyImage = null;
    // RelativeLayout removeSmile;
    public int width = 0, height = 0;
    public static MPLayout mplayout;
    // public static HScroll hScroll;
    // public static VScroll vScroll;
    public TwoDScrollView twoDScroll;
    public int mImageXpos, mImageYpos, mImageWidth, mImageHeight;
    public int mImagePrevXpos, mImagePrevYpos, mImagePrevWidth, mImagePrevHeight;
    // TreeView treeView;
    public static int centerX, centerY;
    boolean focusAddtext;
    public static int densityDpi;
    private MoveGestureDetector mMoveDetector;
    public static MPNodeView clickedMpnodeView;
    public static MPNode nodeProp;
    InputMethodManager inputMethodManager;
    InputMethodSubtype inputMethodSubtype;
    int prevX;
    int prevY;
    String url;
    private String objLocation, objSize;
    private String objContent, prevObjContent;
    private int objUniqueId;
    private int objSequentialId, prevObjSequentialId;
    private String objScalePageToFit;
    private boolean objLocked;
    private boolean prevObjLocked;
    public boolean addInsidepage;
    public String objType;
    private int objBid, objEnrichId;
    private String objPageNo;
    private Button btnInfo;
    private ArrayList<String> mindMapFileNames;
    LoadMindMapContent mindMapContent;

    public MindmapPlayer(BookViewActivity _context, String _type, int _BID, String _objPageNo,  final String _location, final String _size, String _objContent, int _objUniqueId, int _objSequentialId, String _objScalePageToFit, boolean _objLocked, int _objEnrichId, AttributeSet _attrs, boolean addAsTab) {
        super(_context);
        this.context = _context;
        this.objType = _type;
        this.setObjBid(_BID);
        this.setObjPageNo(_objPageNo);
        this.setObjLocation(_location);
        this.setObjSize(_size);
        mImageXpos = Integer.parseInt(getObjLocation().split("\\|")[0]);
        mImageYpos = Integer.parseInt(getObjLocation().split("\\|")[1]);
        mImageWidth = Integer.parseInt(getObjSize().split("\\|")[0]);
        mImageHeight = Integer.parseInt(getObjSize().split("\\|")[1]);
        this.setObjContent(_objContent);
        this.objUniqueId = _objUniqueId;
        this.objSequentialId = _objSequentialId;
//        this.setObjScalePageToFit(_objScalePageToFit);
        this.setObjLocked(_objLocked);
        this.setObjEnrichId(_objEnrichId);
        this.setObjScalePageToFit(_objScalePageToFit);
        final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mImageWidth, mImageHeight);
        inputMethodManager = (InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodSubtype = inputMethodManager.getCurrentInputMethodSubtype();
        _context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                canvasView = new RelativeLayout(context);
                canvasView.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                twoDScroll = new TwoDScrollView(context,MindmapPlayer.this);
                twoDScroll.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                twoDScroll.setScrollBarStyle(SCROLLBARS_OUTSIDE_OVERLAY);
                twoDScroll.setEnabled(false);
                canvasView.addView(twoDScroll);
                drawingView = new RelativeLayout(context);
                drawingView.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                twoDScroll.addView(drawingView);
                fontsList = readAllFonts();
                mplayout = new MPLayout(context);
                //setSelectedView(viewEdit);
                Display display=((BookViewActivity)context).getWindowManager().getDefaultDisplay();
                width = display.getWidth();
                height = display.getHeight();
                drawingView.getLayoutParams().width = AppDict.DEFAULT_OFFSET;
                drawingView.getLayoutParams().height = AppDict.DEFAULT_OFFSET;
                fontsList = readAllFonts();
                url = Globals.TARGET_BASE_MINDMAP_PATH + objContent;
                mindMapContent=new LoadMindMapContent(twoDScroll,drawingView,context);
                mindMapContent.loadXmlData(url,false);
                Rect rectf = new Rect();
                drawingView.getLocalVisibleRect(rectf);
                MindmapPlayer.this.addView(canvasView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
                MindmapPlayer.this.setX(mImageXpos);
                MindmapPlayer.this.setY(mImageYpos);
                MindmapPlayer.this.setLayoutParams(layoutParams);
                addInfoandDelBtn();
                twoDScroll.calculateBounds();
                twoDScroll.scroll((int) (AppDict.DEFAULT_OFFSET - Integer.parseInt(_size.split("\\|")[0])+120 ) / 2,
                        (int) (AppDict.DEFAULT_OFFSET - Integer.parseInt(_size.split("\\|")[1])) / 2);
            }
        });
    }


    public void setTransformationValues(float xPos, float yPos, RelativeLayout.LayoutParams layoutParams) {
        mImageXpos = (int) xPos;
        mImageYpos = (int) yPos;
        mImageWidth = layoutParams.width;
        mImageHeight = layoutParams.height;
    }

    /**
     * This sets the transformation for the views
     */
    public void setTransformation(float xPos, float yPos, RelativeLayout.LayoutParams layoutParams) {
        this.setX(xPos);
        this.setY(yPos);
        this.setLayoutParams(layoutParams);
        mImageXpos = (int) xPos;
        mImageYpos = (int) yPos;
        mImageWidth = layoutParams.width;
        mImageHeight = layoutParams.height;
        twoDScroll.scrollTo((int) (AppDict.DEFAULT_OFFSET - mImageWidth + 120) / 2,
                        (int) (AppDict.DEFAULT_OFFSET - mImageHeight) / 2);
    }

    //boolean nodIsClicked;


    int _xDelta = 0, _yDelta = 0;


    public static void hideMenuItem() {
        if (popUpLayout != null && popUpLayout.getVisibility() == View.VISIBLE) {
            // animate(true);
            // animateRadialMenu(false);
            popUpLayout.setVisibility(View.GONE);
        }
    }

    public static View popUpLayout;

    // pop up view


    private ArrayList<String> readAllFonts() {

        ArrayList<String> fontNames = new ArrayList<String>();

        File temp = new File("/system/fonts/");

        String fontSuffix = ".ttf";

        for (File font : temp.listFiles()) {

            String fontName = font.getName();

            if (fontName.endsWith(fontSuffix)) {

                fontNames.add(fontName.subSequence(0,
                        fontName.lastIndexOf(fontSuffix)).toString());

            }

        }

        return fontNames;

    }

    ImageView img;
    Bitmap bitmap;
    ProgressDialog pDialog;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int action = event.getAction();
        int positionX = (int) event.getRawX();
        int positionY = (int) event.getRawY();
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                context.designScrollPageView.setEnableScrolling(false);
                prevX = positionX;
                prevY = positionY;

                mImagePrevXpos = mImageXpos;
                mImagePrevYpos = mImageYpos;

                context.page.setCurrentView(this);
                context.page.makeSelectedArea(this);
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                int deltaX = positionX - prevX;
                int deltaY = positionY - prevY;

                float viewCenterX = (this.getX() + deltaX) + (this.getWidth() / 2);
                float viewCenterY = (this.getY() + deltaY) + (this.getHeight() / 2);
                PointF viewCenterPoint = new PointF(viewCenterX, viewCenterY);


                //Snap To grid
                Object[] gridArr = context.page.checkForSnapPointsForGrid(viewCenterPoint);
                viewCenterPoint = (PointF) gridArr[0];
                boolean gridPointsSnapped = (Boolean) gridArr[1];
                Object[] objArr = context.page.checkForSnapBetweenObjects(this, viewCenterPoint);
                viewCenterPoint = (PointF) objArr[0];
                boolean objectPointsSnapped = (Boolean) objArr[2];

                float viewXpos = viewCenterPoint.x - (this.getWidth() / 2);
                float viewYpos = viewCenterPoint.y - (this.getHeight() / 2);

                //Restrict the objects within the boundry
                if (viewXpos <= 0) {
                    viewXpos = 0;
                }
                if (viewYpos <= 0) {
                    viewYpos = 0;
                }
                if (viewXpos + getWidth() >= context.designPageLayout.getWidth()) {
                    viewXpos = context.designPageLayout.getWidth() - getWidth();
                }
                if (viewYpos + getHeight() >= context.designPageLayout.getHeight()) {
                    //viewYpos = context.designPageLayout.getHeight() - getHeight();
                }

                setXAndYPos(viewXpos, viewYpos);

                prevX = positionX;
                prevY = positionY;

                if (objectPointsSnapped) {
                    context.page.snapToObjects(this, (View) objArr[1]);
                } else {
                    context.page.removeObjectGridLines();
                }

                if (gridPointsSnapped) {
                    context.page.snapToGrid(this);
                } else {
                    context.page.removeGridLines();
                }

                context.page.extendScrollViewHeight(this, viewCenterPoint);

                context.page.reassignHandlerArea(this);

                break;
            }

            case MotionEvent.ACTION_UP: {
                context.designScrollPageView.setEnableScrolling(true);
                context.page.removeObjectGridLines();
                context.page.removeGridLines();

                if (mImagePrevXpos != mImageXpos || mImagePrevYpos != mImageYpos) {
                    UndoRedoObjectData uRPrevObjData = new UndoRedoObjectData();
                    uRPrevObjData.setuRObjDataUniqueId(objUniqueId);
                    uRPrevObjData.setuRObjDataXPos(mImagePrevXpos);
                    uRPrevObjData.setuRObjDataYPos(mImagePrevYpos);
                    context.page.createAndRegisterUndoRedoObjectData(uRPrevObjData, Globals.OBJECT_MOVED, Globals.OBJECT_PREVIOUS_STATE);

                    UndoRedoObjectData uRCurrObjData = new UndoRedoObjectData();
                    uRCurrObjData.setuRObjDataUniqueId(objUniqueId);
                    uRCurrObjData.setuRObjDataXPos(mImageXpos);
                    uRCurrObjData.setuRObjDataYPos(mImageYpos);
                    context.page.createAndRegisterUndoRedoObjectData(uRCurrObjData, Globals.OBJECT_MOVED, Globals.OBJECT_PRESENT_STATE);
                }

                break;
            }

            case MotionEvent.ACTION_CANCEL: {

                break;
            }

            case MotionEvent.ACTION_POINTER_UP: {

                break;
            }
        }
        //return true;
        return true;
    }

    public void setXAndYPos(float viewXpos, float viewYpos) {
        this.setX(viewXpos);
        this.setY(viewYpos);
        mImageXpos = (int) viewXpos;
        mImageYpos = (int) viewYpos;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnInfo:
                context.page.setCurrentView(this);
                context.page.makeSelectedArea(this);
                if (context.currentBook.is_bStoreBook() && !context.currentBook.isStoreBookCreatedFromTab()) {
                    context.designScrollPageView.setEnableScrolling(false);
                }
                context.hideToolBarLayout();
                mindMapPopView(v);
                break;

            case R.id.btn_delete_object:
                context.page.setCurrentView(this);
                context.page.makeSelectedArea(this);
                context.deleteSelectedObject();
                break;

            default:
                break;
        }
    }

    public int getObjectUniqueId() {
        return this.objUniqueId;
    }

    /**
     * @return
     * @author getObjectSequentialId
     */
    public int getObjectSequentialId() {
        this.objSequentialId = context.db.getSequentialIdFromDb(this.objUniqueId);
        return this.objSequentialId;
    }

    /**
     * this sets the object Sequentialid
     *
     * @param objSequentialId
     */
    public void setObjectSequentialId(int objSequentialId) {
        this.objSequentialId = objSequentialId;
    }

    /**
     * @return the objType
     */
    public String getObjType() {
        return objType;
    }

    public boolean isObjLocked() {
        return objLocked;
    }

    /**
     * @param objLocked the objLocked to set
     */
    public void setObjLocked(boolean objLocked) {
        this.objLocked = objLocked;
    }

    /**
     * @return the objScalePageToFit
     */
    public String getObjScalePageToFit() {
        return objScalePageToFit;
    }

    /**
     * @param objScalePageToFit the objScalePageToFit to set
     */
    public void setObjScalePageToFit(String objScalePageToFit) {
        this.objScalePageToFit = objScalePageToFit;
    }

    /**
     * @return the objLocation
     */
    public String getObjLocation() {
        return objLocation;
    }

    /**
     * @param objLocation the objLocation to set
     */
    public void setObjLocation(String objLocation) {
        this.objLocation = objLocation;
    }

    /**
     * @return the objSize
     */
    public String getObjSize() {
        return objSize;
    }

    /**
     * @param objSize the objSize to set
     */
    public void setObjSize(String objSize) {
        this.objSize = objSize;
    }

    /**
     * @return the objBid
     */
    public int getObjBid() {
        return objBid;
    }

    /**
     * @param objBid the objBid to set
     */
    public void setObjBid(int objBid) {
        this.objBid = objBid;
    }

    /**
     * @return the objPageNo
     */
    public String getObjPageNo() {
        return objPageNo;
    }

    /**
     * @param objPageNo the objPageNo to set
     */
    public void setObjPageNo(String objPageNo) {
        this.objPageNo = objPageNo;
    }

    /**
     * @return the objEnrichId
     */
    public int getObjEnrichId() {
        return objEnrichId;
    }

    /**
     * @param objEnrichId the objEnrichId to set
     */
    public void setObjEnrichId(int objEnrichId) {
        this.objEnrichId = objEnrichId;
    }

    public String getObjContent() {
        return objContent;
    }

    public void setObjContent(String objContent) {
        this.objContent = objContent;
    }

    @Override
    public void popoverViewWillShow(PopoverView view) {

    }

    @Override
    public void popoverViewDidShow(PopoverView view) {

    }

    @Override
    public void popoverViewWillDismiss(PopoverView view) {

    }

    @Override
    public void popoverViewDidDismiss(PopoverView view) {

    }

//    public boolean isAddInsidePage() {
//        return addInsidePage;
//    }
//
//    public void setAddInsidePage(boolean addInsidePage) {
//        this.addInsidePage = addInsidePage;
//    }

    Dialog d;

    int tempX = 0, tempY = 0;
    public MyGestureListener myGestureListener;
    public boolean singleTap = false;
    int X = 0, Y = 0;
    private float mScaleFactor = 1.0f;
    Bitmap bitmapScale;


    private class MoveListener extends
            MoveGestureDetector.SimpleOnMoveGestureListener {

        @Override
        public boolean onMoveBegin(MoveGestureDetector detector) {
            // mPUndoManager.addUndooperationOnNode(nodeProp,
            // UndoOperationKey.UndoOperationKey_changeNodePosition);
            Log.d("", "onMoveBegin ");
            ManahijApp.m_curEbag.m_impstate = MapState.MPStateBeginDragDrop;
            // }
            return super.onMoveBegin(detector);
        }

        @Override
        public void onMoveEnd(MoveGestureDetector detector) {
            if (nodeProp.m_parentNode != null) {
                ManahijApp.m_curEbag.m_impstate = MapState.MPStateEndDragDrop;
                ManahijApp.m_curEbag.m_currNodeBeingDragged = null;
                mplayout.changeNodePositionOf(nodeProp);
                nodeProp.m_isCurrShapeBeingDragged = false;
                mplayout.setLayout(nodeProp);

                ManahijApp.m_curEbag.m_impstate = MapState.MPStateNormal;
            }
            super.onMoveEnd(detector);

        }

        @Override
        public boolean onMove(MoveGestureDetector detector) {
            if (clickedMpnodeView != null) {
                ManahijApp.m_curEbag.m_impstate = MapState.MPStateBeingDragging;
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) clickedMpnodeView
                        .getLayoutParams();
                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                layoutParams.rightMargin = -250;
                layoutParams.bottomMargin = -250;
                if (nodeProp.m_iLevelId != 0) {
                    // isMoved = true;
                    nodeProp.m_isCurrShapeBeingDragged = true;
                    nodeProp.m_fX = (X - _xDelta);
                    nodeProp.m_fY = (Y - _yDelta);
                    // nodeProp.isHilight = true;
                    ManahijApp.m_curEbag.m_currNodeBeingDragged = nodeProp;
                    mplayout.setLayout(nodeProp);
                }
            }

            // mFocusX = detector.getFocusX();
            // mFocusY = detector.getFocusY();
            Log.d("", "onMove ");

            return true;
        }
    }


    DatePicker cv;
    LinearLayout mainLayout;
    TextView duration;
    ImageView plus, minus;
    LinearLayout priorityGroup, completionGroup;
    TextView category;

    public class MyGestureListener extends GestureDetector.SimpleOnGestureListener implements
            OnTouchListener {
        Context context;
        MPNodeView view;
        GestureDetector gDetector;

        public MyGestureListener() {
            super();
        }

        public MyGestureListener(Context context) {
            this(context, null);
        }

        public MyGestureListener(Context context, GestureDetector gDetector) {

            if (gDetector == null)
                gDetector = new GestureDetector(context, this);

            this.context = context;
            this.gDetector = gDetector;
            gDetector.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {

                @Override
                public boolean onSingleTapConfirmed(MotionEvent event) {
                    singleTap = true;
//					showOptionPopup(event);

                    return true;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    Log.d("", "" + "on onDoubleTapEvent Tap");

                    return false;
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    return false;
                }
            });
        }

        public String logposition(Point point, MPNode node) {
            for (Map.Entry<String, android.graphics.RectF> entry : node.propertiesPositions
                    .entrySet()) {
                String key = entry.getKey();
                RectF value = entry.getValue();
                if (value.contains(point.x, point.y)) {
                    return key;
                }

            }
            return "";
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            boolean isMoved = false;
            gDetector.onTouchEvent(event);
/*				if (nodeProp != null && nodeProp.m_iLevelId != 0)
            if (ManahijApp.m_curEbag.m_currNodeBeingDragged == null|| ManahijApp.m_curEbag.m_currNodeBeingDragged == nodeProp)
					mMoveDetector.onTouchEvent(event);*/
            // mScaleDetector.onTouchEvent(event);
            hideMenuItem();
            /* final int */
            X = (int) (event.getRawX() / TwoDScrollView.scaleFactor);
            /* final int */
            Y = (int) (event.getRawY() / TwoDScrollView.scaleFactor);
            view = (MPNodeView) v;
            Point point = new Point();
            point.x = (int) event.getX();
            point.y = (int) event.getY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    if (nodeProp != null)
                        nodeProp.selected = false;
                    if (view.getRect().contains((int) point.x, (int) point.y)) {
                        if (nodeProp != null)
                            nodeProp.selected = false;
                        clickedMpnodeView = view;
                        if (ManahijApp.m_curEbag.m_impstate == MapState.MPStateNormal) {
                            nodeProp = view.getm_mpNode();
                        }
                    }
                    RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) v
                            .getLayoutParams();
                    _xDelta = (int) (X - nodeProp.getM_fX());
                    _yDelta = (int) (Y - nodeProp.getM_fY());
                    // break;
                    if (drawingView.getParent() != null) {
                        ((ViewParent) drawingView.getParent())
                                .requestDisallowInterceptTouchEvent(true);
                    }
                    // twoDScroll.setScrollingEnabled(false);

                    // hScroll.setScrollingEnabled(false);
                    return true;
                case MotionEvent.ACTION_UP:

                    if (view.getRect().contains((int) point.x, (int) point.y))
                        if (nodeProp.m_lp != null)

                            if (drawingView.getParent() != null) {
                                ((ViewParent) drawingView.getParent())
                                        .requestDisallowInterceptTouchEvent(true);
                            }
                    // clickedMpnodeView.invalidate();
                    // }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
            }
            drawingView.invalidate();
            // return gDetector.onTouchEvent(event);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {

            return super.onFling(e1, e2, velocityX, velocityY);
        }


        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {

            return super.onSingleTapConfirmed(e);
        }

        public GestureDetector getDetector() {
            return gDetector;
        }
    }


    // LoadImageFromServer loadImageFromServer= new LoadImageFromServer();



//	public void resizeBounds(View v){
//		float scaleX = canvasView.getScaleX();
//		BigDecimal roundfinalPrice = new BigDecimal(scaleX).setScale(1,BigDecimal.ROUND_HALF_UP);
//		if(roundfinalPrice.floatValue()<=1.0&&(roundfinalPrice.floatValue())>=0.5) // minimum and maximum value for  resize bounds
//		switch (v.getId()) {
//		case R.id.btn_increase:
//
//			BigDecimal roundfinalPrice2 = new BigDecimal(scaleX+0.1).setScale(1,BigDecimal.ROUND_HALF_UP);
//			if(roundfinalPrice2.floatValue()<=1.0){
//			canvasView.setScaleX(roundfinalPrice2.floatValue());
//			canvasView.setScaleY(roundfinalPrice2.floatValue());
//
//			}
//			break;
//		case R.id.btn_decrese:
//			BigDecimal roundfinalPrice1 = new BigDecimal(scaleX-0.1).setScale(1,BigDecimal.ROUND_HALF_UP);
//			if(roundfinalPrice1.floatValue()>0.5){
//			canvasView.setScaleX(roundfinalPrice1.floatValue());
//			canvasView.setScaleY(roundfinalPrice1.floatValue());
//			}
//			break;
//		default:
//			break;
//		}
//		//canvasView.updateViewLayout(drawingView, drawingView.getLayoutParams());
//		canvasView.invalidate();
//		canvasView.getScaleX();
//	}


    //    @Override
//    public boolean onTouch(View view, MotionEvent event) {
//        final int action = event.getAction();
//        int positionX = (int) event.getRawX();
//        int positionY = (int) event.getRawY();
//
//        switch (action & MotionEvent.ACTION_MASK) {
//            case MotionEvent.ACTION_DOWN: {
//                prevX = positionX;
//                prevY = positionY;
//               _context.page.makeSelectedArea(canvasView);
//                break;
//            }
//
//            case MotionEvent.ACTION_MOVE: {
//                int deltaX = positionX - prevX;
//                int deltaY = positionY - prevY;
//
//                float viewCenterX = (canvasView.getX() + deltaX) + (canvasView.getWidth() / 2);
//                float viewCenterY = (canvasView.getY() + deltaY) + (canvasView.getHeight() / 2);
//                PointF viewCenterPoint = new PointF(viewCenterX, viewCenterY);
//
//
//                float viewXpos = viewCenterPoint.x - (canvasView.getWidth() / 2);
//                float viewYpos = viewCenterPoint.y - (canvasView.getHeight() / 2);
//
//                //Restrict the objects within the boundry
//                if (viewXpos <= 0) {
//                    viewXpos = 0;
//                }
//                if (viewYpos <= 0) {
//                    viewYpos = 0;
//                }
//                if (viewXpos + getWidth() >= canvasView.getWidth()) {
//                    //viewXpos = MindMapPlayerActivity.canvasView.getWidth() - getWidth();
//                }
//                if (viewYpos + getHeight() >= canvasView.getHeight()) {
//                    //viewYpos = MindMapPlayerActivity.canvasView.getHeight() - getHeight();
//                }
//                canvasView.setX(viewXpos);
//                canvasView.setY(viewYpos);
//                prevX = positionX;
//                prevY = positionY;
//               _context.page.reassignHandlerArea(canvasView);
////				extendScrollViewHeight(this,viewCenterPoint);
//            }
//            break;
//            case MotionEvent.ACTION_UP:
//
//        }
//
//
//        return false;
//    }
    public void addInfoandDelBtn() {
        RelativeLayout toolBarlayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams tbLayoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        tbLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        tbLayoutParams.setMargins(5, 5, 5, 0);
        toolBarlayout.setLayoutParams(tbLayoutParams);
        MindmapPlayer.this.addView(toolBarlayout);

        int dpDimen = (int) context.getResources().getDimension(R.dimen.objects_btn_size);
        btnInfo = new Button(context);
        btnInfo.setBackgroundResource(R.drawable.button_info_black);
        btnInfo.setOnClickListener(this);
        btnInfo.setVisibility(GONE);
        btnInfo.setId(R.id.btnInfo);

        toolBarlayout.addView(btnInfo, dpDimen, dpDimen);

        Button btnObjDelete = new Button(context);
        btnObjDelete.setBackgroundResource(R.drawable.delete_normal_new);
        btnObjDelete.setId(R.id.btn_delete_object);
        btnObjDelete.setOnClickListener(this);
        RelativeLayout.LayoutParams btnDelLayoutParams = new RelativeLayout.LayoutParams(dpDimen, dpDimen);
        btnDelLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        btnObjDelete.setLayoutParams(btnDelLayoutParams);
        toolBarlayout.addView(btnObjDelete);
    }
    public void mindMapPopView(View view){
        prevObjSequentialId = getObjectSequentialId();
        final PopoverView popoverView = new PopoverView(context, R.layout.list_info_view);
        popoverView.setContentSizeForViewInPopover(new Point((int) context.getResources().getDimension(R.dimen.info_popover_width), (int) context.getResources().getDimension(R.dimen.web_info_height)));
        popoverView.setDelegate(this);
        popoverView.showPopoverFromRectInViewGroup(context.mainDesignView, PopoverView.getFrameForView(view), PopoverView.PopoverArrowDirectionAny, true);
        ListView listView = (ListView) popoverView.findViewById(R.id.info_list_view);
        listView.setVisibility(View.GONE);
        TextView txtTitle = (TextView) popoverView.findViewById(R.id.textView1);
        txtTitle.setVisibility(View.GONE);

        GridView gridView = (GridView) popoverView.findViewById(R.id.gridView);
        gridView.setVisibility(View.VISIBLE);
        gridView.setAdapter(new MindmapGridadapter());
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                changeMindMapContent(position);
                popoverView.dissmissPopover(false);
            }
        });
    }
    public class MindmapGridadapter extends BaseAdapter {

        public MindmapGridadapter() {
            File mindMapDir = new File(Globals.TARGET_BASE_MINDMAP_PATH);
            final String[] mindMapFiles = mindMapDir.list();
            if (mindMapFiles != null) {
                getMindMapFiles(mindMapFiles);
            }
        }

        private void getMindMapFiles(String[] mindMapFiles) {
            mindMapFileNames = new ArrayList<String>();
            for (int i = 0; i < mindMapFiles.length; i++) {
                if (mindMapFiles[i].contains(".xml")) {
                    mindMapFileNames.add(mindMapFiles[i].replace(".xml", ""));
                }
            }
        }

        @Override
        public int getCount() {
            return mindMapFileNames.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ((Activity) context).getLayoutInflater().inflate(R.layout.mindmap_grid_inflate, null);
            }
            String mindMapFileName = mindMapFileNames.get(position);
            String imgFilePath = Globals.TARGET_BASE_MINDMAP_PATH + mindMapFileName + ".png";
            ImageView imgView = (ImageView) view.findViewById(R.id.imageView);
            Bitmap bitmap = BitmapFactory.decodeFile(imgFilePath);
            imgView.setImageBitmap(bitmap);

            Button btnEdit = (Button) view.findViewById(R.id.btnEdit);
            btnEdit.setVisibility(View.GONE);
            Button btnDelete = (Button) view.findViewById(R.id.btnDelete);
            btnDelete.setVisibility(View.GONE);

            TextView txtTitle = (TextView) view.findViewById(R.id.textView5);
            txtTitle.setText(mindMapFileName);

            return view;
        }
    }
    public void changeMindMapContent(int position){
        String mindMapFileName = mindMapFileNames.get(position);
        prevObjContent=objContent;
        objContent = mindMapFileName+".xml";
        MindmapPlayer.this.setObjContent(objContent);
        url = Globals.TARGET_BASE_MINDMAP_PATH + objContent;
        mindMapContent.loadXmlData(url, false);
        MindmapPlayer.this.invalidate();
        if (prevObjContent != null && !prevObjContent.equals(objContent)) {
            UndoRedoObjectData prevURObjData = new UndoRedoObjectData();
            prevURObjData.setuRObjDataUniqueId(getObjectUniqueId());
            prevURObjData.setuRObjDataType(objType);
            prevURObjData.setuRObjDataContent(prevObjContent);
            context.page.createAndRegisterUndoRedoObjectData(prevURObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PREVIOUS_STATE);

            UndoRedoObjectData currURObjData = new UndoRedoObjectData();
            currURObjData.setuRObjDataUniqueId(getObjectUniqueId());
            currURObjData.setuRObjDataType(objType);
            currURObjData.setuRObjDataContent(objContent);
            context.page.createAndRegisterUndoRedoObjectData(currURObjData, Globals.OBJECT_CONTENT_EDITED, Globals.OBJECT_PRESENT_STATE);
        }
    }
}

