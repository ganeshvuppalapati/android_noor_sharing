package com.ptg.mindmap.undoredo;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;

import com.ptg.mimdmap.node.MPNode;
import com.ptg.mimdmap.node.MPNodeView;
import com.ptg.mindmap.enums.MapLayoutLineType;
import com.ptg.mindmap.enums.MapLayoutShapeType;
import com.ptg.mindmap.enums.MapLayoutType;
import com.ptg.mindmap.enums.UndoOperationKey;
import com.ptg.mindmap.layouts.MPEbag;
import com.ptg.mindmap.layouts.MPLayout;
import com.ptg.mindmap.widget.MindMapActivity;
import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.R;

import java.util.Stack;

public class MPUndoManager {
	MPUndoObject mpundoObj;

	public static MPUndoManager sharedInstance;
	public  Stack undoStack;
	public Stack redoStack;
	public int stackLimit=25;
	public MPLayout mpLayout;
	public  MPUndoManager(MPLayout mpLayout){
		this.mpLayout=mpLayout;
		undoStack = new Stack();
		redoStack = new Stack();
	}

	public void addUndooperationOnNode(MPNode node,UndoOperationKey keytype){
		MPUndoObject mpundoObj = new MPUndoObject();
		mpundoObj.keyType = keytype;
		//undoStack.push(mpundoObj);
		pushUndoStack(mpundoObj);
		mpundoObj.objold = node;
		mpundoObj.objnew = node.copy();

		switch (keytype) {
		case UndoOperationKey_changeLineStyle:
			mpundoObj.oldLineStyle =((MPEbag)node).m_iMapLayoutLine;
			break;
		case UndoOperationKey_changeMapStyle:
			mpundoObj.oldMapStyle = ((MPEbag)node).m_iMapLayout;
			break;
		case UndoOperationKey_changeShapeStyle:
			break;

		default:
			break;
		}
		this.clearRedoStack();
	}
	public void AddNodeForClear(MPNode node,UndoOperationKey keytype,int index){
		MPUndoObject mpundoObj = new MPUndoObject();

		mpundoObj.keyType = keytype;
		mpundoObj.oldObjIndex=index;
		mpundoObj.objold = node;
		mpundoObj.objnew = node.copy();
		pushUndoStack(mpundoObj);
		this.clearRedoStack();
	}
	public void addUndooperationOnNodeColor(MPNode node,String color ){
		MPUndoObject mpundoObj = new MPUndoObject();
		mpundoObj.shape_oldColor = color;
		mpundoObj.shape_newColor = node.hexaColor;
		mpundoObj.objold  = node;
		mpundoObj.grad=node.m_iGradientType;
		mpundoObj.keyType = UndoOperationKey.UndoOperationKey_ShapeColor;
		pushUndoStack(mpundoObj);
		this.clearRedoStack();
	}
	public void addUndooperationOnTextAlign(MPNode node,int align ){
		MPUndoObject mpundoObj = new MPUndoObject();
		mpundoObj.oldTxtAlignMent = node.textAlign;
		mpundoObj.objnew  = node;
		mpundoObj.keyType = UndoOperationKey.UndoOperationKey_textAlign;
		pushUndoStack(mpundoObj);
		this.clearRedoStack();
	}

	public void addUndooperationOnNodeTextColor(MPNode node,String color ,UndoOperationKey keytype){
		MPUndoObject mpundoObj = new MPUndoObject();
		mpundoObj.shape_oldColor = color;
		mpundoObj.shape_newColor = node.textColor;
		mpundoObj.objold  = node;
		mpundoObj.keyType = keytype;
		pushUndoStack(mpundoObj);
		this.clearRedoStack();
	}
	public void addUndooperationOnOldNode(MPNode node1,MPNode node2,UndoOperationKey keytype){
		MPUndoObject mpundoObj =new MPUndoObject();
		mpundoObj.keyType = keytype;
		mpundoObj.objold = node1;
		mpundoObj.objnew = node2;
		pushUndoStack(mpundoObj);
		this.clearRedoStack();
	}
	public void addUndooperationOnTextProperties(MPNode node1,MPNode node2,MPNode extraNode,UndoOperationKey keytype){
		MPUndoObject mpundoObj =new MPUndoObject();
		mpundoObj.keyType = keytype;
		mpundoObj.objold = node1;
		mpundoObj.objnew = node2;
		mpundoObj.objExtra=extraNode;
		pushUndoStack(mpundoObj);
		this.clearRedoStack();
	}
	public void addUndooperationOnEbagColor(String oldColor){
		MPUndoObject mpundoObj = new MPUndoObject();
		mpundoObj.ebag_oldColor = oldColor;
		mpundoObj.objold  = null;
		mpundoObj.keyType = UndoOperationKey.UndoOperationKey_BackgroundColor;
		//  mpundoObj.keyType = UndoOperationKey.UndoOperationKey_ShapeColor;
		pushUndoStack(mpundoObj);
		this.clearRedoStack();
	}
	public void addUndoOperations(MPUndoObject mpundoObj){
		pushUndoStack(mpundoObj);
	}
	public void doUndoRedo(View v){

		switch (v.getId()) {
		case R.id.btn_undo:
			this.doOpeartionUndo();
			break;
		case R.id.btn_redo:
			this.doOpeartionRedo();
			break;
		default:
			break;
		} 
	}

	public void doOpeartionUndo() {
		if(undoStack.size()==0)
			return;
		mpundoObj = (MPUndoObject) this.undoStack.pop();
		if(mpundoObj==null)
			return;
		MPNode newnode,oldnode;
		switch (mpundoObj.keyType) {
		case UndoOperationKey_imageAsNode:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			mpundoObj.nadeAsImage= oldnode.m_background_image ;
			//	MPNodeView shape = newnode.m_shape;
		
			
			mpundoObj.info= oldnode.m_strMultimediaUrl;
			mpundoObj.info2= oldnode.imageUrl;
			mpundoObj.grad= oldnode.imageAttachementype;
			mpundoObj.shape_oldColor= oldnode.base64image;
			mpundoObj.mediatype = newnode.mediaType;
			
			oldnode.m_strMultimediaUrl=newnode.m_strMultimediaUrl;
			oldnode.multimediaurl=newnode.multimediaurl;
			oldnode.imageUrl=newnode.imageUrl;
			oldnode.base64image=newnode.base64image;
			oldnode.mediaType = newnode.mediaType;
			oldnode.imageAttachementype=newnode.imageAttachementype;
			
			oldnode.m_attachement_image =  newnode.m_attachement_image;
			oldnode.embededImage =  newnode.embededImage;
			oldnode.m_background_image =  newnode.m_background_image;
			
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			mpLayout.setLayout(ManahijApp.m_curEbag);

			//this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);

			break;
		case UndoOperationKey_imageAttachment:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			mpundoObj.info= oldnode.m_strMultimediaUrl;
			mpundoObj.info2= oldnode.imageUrl;
			mpundoObj.grad= oldnode.imageAttachementype;
			mpundoObj.shape_oldColor= oldnode.base64image;
			mpundoObj.mediatype = newnode.mediaType;
			mpundoObj.multimediapath = newnode.multimediaurl;
			
				oldnode.m_strMultimediaUrl=newnode.m_strMultimediaUrl;
			oldnode.multimediaurl=newnode.multimediaurl;
			oldnode.imageUrl=newnode.imageUrl;
			oldnode.base64image=newnode.base64image;
			oldnode.imageAttachementype=newnode.imageAttachementype;
			
			
			mpundoObj.nadeAsImage= oldnode.m_attachement_image ;
			oldnode.m_attachement_image =  newnode.m_attachement_image;
			oldnode.embededImage =  newnode.embededImage;
			oldnode.m_background_image =  newnode.m_background_image;
			oldnode.mediaType = newnode.mediaType;
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			mpLayout.setLayout(ManahijApp.m_curEbag);
			pushRedoStack(mpundoObj);

			break;
		case UndoOperationKey_imageEmbed:

				newnode = (MPNode)mpundoObj.objnew;
				oldnode = (MPNode)mpundoObj.objold;
				
				mpundoObj.nadeAsImage= oldnode.embededImage ;
				mpundoObj.info= oldnode.m_strMultimediaUrl;
				mpundoObj.info2= oldnode.imageUrl;
				mpundoObj.grad= oldnode.imageAttachementype;
				mpundoObj.shape_oldColor= oldnode.base64image;
				mpundoObj.mediatype= oldnode.mediaType;
				mpundoObj.multimediapath= oldnode.multimediaurl;
				
				oldnode.m_attachement_image =  newnode.m_attachement_image;
				oldnode.embededImage =  newnode.embededImage;
				oldnode.m_background_image =  newnode.m_background_image;
				
				oldnode.multimediaurl=newnode.multimediaurl;
				oldnode.m_strMultimediaUrl=newnode.m_strMultimediaUrl;
				oldnode.imageUrl=newnode.imageUrl;
				oldnode.base64image=newnode.base64image;
				oldnode.mediaType = newnode.mediaType;
				oldnode.imageAttachementype=newnode.imageAttachementype;
				
				oldnode.m_shape.calculateTextWidth();
				mpLayout.setLayout(oldnode);
				mpLayout.setLayout(ManahijApp.m_curEbag);
				pushRedoStack(mpundoObj);

				break;
		case UndoOperationKey_videoattachment:

					newnode = (MPNode)mpundoObj.objnew;
					oldnode = (MPNode)mpundoObj.objold;
					
					mpundoObj.info= oldnode.m_strMultimediaUrl;
					mpundoObj.info2= oldnode.imageUrl;
					mpundoObj.grad= oldnode.imageAttachementype;
					mpundoObj.shape_oldColor= oldnode.base64image;
					mpundoObj.mediatype= oldnode.mediaType;
					mpundoObj.multimediapath= oldnode.multimediaurl;
					
					
					oldnode.m_attachement_image = newnode.m_attachement_image;
					oldnode.embededImage =  newnode.embededImage;
					oldnode.m_background_image =  newnode.m_background_image;
					
					
					oldnode.multimediaurl=newnode.multimediaurl;
					oldnode.m_strMultimediaUrl=newnode.m_strMultimediaUrl;
					oldnode.imageUrl=newnode.imageUrl;
					oldnode.base64image=newnode.base64image;
					oldnode.mediaType = newnode.mediaType;
					oldnode.imageAttachementype=newnode.imageAttachementype;
					
					oldnode.m_shape.calculateTextWidth();
					
					
					mpLayout.setLayout(oldnode);
					mpLayout.setLayout(ManahijApp.m_curEbag);
					pushRedoStack(mpundoObj);

					break;
		case UndoOperationKey_audioattachment:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			
			mpundoObj.info= oldnode.m_strMultimediaUrl;
			mpundoObj.info2= oldnode.imageUrl;
			mpundoObj.grad= oldnode.imageAttachementype;
			mpundoObj.shape_oldColor= oldnode.base64image;
			mpundoObj.mediatype= oldnode.mediaType;
			mpundoObj.multimediapath= oldnode.multimediaurl;
			
			
			
			oldnode.m_attachement_image = newnode.m_attachement_image;
			oldnode.embededImage =  newnode.embededImage;
			oldnode.m_background_image =  newnode.m_background_image;
			oldnode.multimediaurl=newnode.multimediaurl;
			oldnode.mediaType = newnode.mediaType;
			
			oldnode.m_attachement_image = newnode.m_attachement_image;
			oldnode.embededImage =  newnode.embededImage;
			oldnode.m_background_image =  newnode.m_background_image;
			
			
			oldnode.m_strMultimediaUrl=newnode.m_strMultimediaUrl;
			oldnode.imageUrl=newnode.imageUrl;
			oldnode.base64image=newnode.base64image;
			oldnode.imageAttachementype=newnode.imageAttachementype;
			
			
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			mpLayout.setLayout(ManahijApp.m_curEbag);
			pushRedoStack(mpundoObj);

			break;
		case UndoOperationKey_addNode:
		{
			MPNode node = (MPNode)mpundoObj.objold;
			if(node.m_parentNode==null)
				return;
			mpLayout.addUndoNode(node);
			mpundoObj.keyType = UndoOperationKey.UndoOperationKey_deleteNode;
			// this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);
			mpLayout.setLayout(ManahijApp.m_curEbag);
		}

		break;
		case UndoOperationKey_deleteNode:
		{

			MPNode node = (MPNode)mpundoObj.objold;
			mpLayout.deleteNodeOf(node.m_shape);
			mpundoObj.keyType = UndoOperationKey.UndoOperationKey_addNode;
			// this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);
		}
		break;
		case UndoOperationKey_ShapeColor:
		{            
			MPNode node = (MPNode)mpundoObj.objold;
			node.m_selectedColor = Color.parseColor(mpundoObj.shape_oldColor);
			node.setBackgroundColor(
					node.m_selectedColor);
			//
			int gradientPos = node.m_iGradientType;
			node.m_iGradientType = mpundoObj.grad;

			mpundoObj.grad = gradientPos;
			//
			mpLayout.tempUpdateChildrenColor(node.m_shape,
					node.m_selectedColor, mpundoObj.shape_oldColor);
			node.setHexaColor(mpundoObj.shape_oldColor);
			node.m_shape.calculateTextWidth();
			mpLayout.updateChildrenColor(node ,node.m_arrChildren);
			//this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);
			mpLayout.setLayout(node);
		}
		break;

		case UndoOperationKey_BackgroundColor:
		{

			MPEbag ebag = ManahijApp.m_curEbag;
			String newColor = ebag.m_strMapLayoutFillColor;
			ebag.m_strMapLayoutFillColor = mpundoObj.ebag_oldColor;

			mpundoObj.ebag_oldColor = newColor;
			MindMapActivity.drawingView.setBackgroundColor(Color.parseColor( ebag.m_strMapLayoutFillColor));
			pushRedoStack(mpundoObj);

		}
		break;

		case UndoOperationKey_changeMapStyle:
		{
			MPEbag ebag = ManahijApp.m_curEbag;
			MapLayoutType newlType = ebag.m_iMapLayout;
			ebag.m_iMapLayout = mpundoObj.oldMapStyle;
			mpundoObj.oldMapStyle = newlType;
			// this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);
			if(ebag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap){
			}else{
				mpLayout.setLayout(ebag);
			}
			this.deleteUndoRedoStackOtherOperationsAfterSwitchingOneLayoutToOtherLayout();
		}
		break;
		case UndoOperationKey_changeLineStyle:
		{
			MPEbag ebag = ManahijApp.m_curEbag;
			MapLayoutLineType newlType = ebag.m_iMapLayoutLine;
			ebag.m_iMapLayoutLine = mpundoObj.oldLineStyle;
			mpundoObj.oldLineStyle = newlType;
			//this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);
			mpLayout.setLayout(ebag);

		}
		break;
		case UndoOperationKey_changeShapeStyle:
		{
			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			MapLayoutShapeType nsType = oldnode.m_iMapLayoutShape;
			oldnode.m_iMapLayoutShape = newnode.m_iMapLayoutShape;
			newnode.m_iMapLayoutShape = nsType;
			pushRedoStack(mpundoObj);
			mpLayout.setLayout(oldnode);
			mpLayout.setLayout(ManahijApp.m_curEbag);
			oldnode.m_shape.invalidate();
			MindMapActivity act=new MindMapActivity();
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
		}
		break;
		case UndoOperationKey_copyPast:
		{
			this.handleCopyPastOperations(mpundoObj ,true);
			pushRedoStack(mpundoObj);
		}
		break;
		case UndoOperationKey_cutPast:
		{
			this.handleCopyPastOperations(mpundoObj ,true);
			pushRedoStack(mpundoObj);
		}
		break;
		case UndoOperationKey_copyPastPro:
		{
			this.handleCopyPastOperations(mpundoObj ,true);
			pushRedoStack(mpundoObj);
		}
		break;
		case UndoOperationKey_hyperAttachment:
			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			mpundoObj.info= oldnode.m_strHyperlinkUrl ;
			oldnode.m_strHyperlinkUrl=newnode.m_strHyperlinkUrl;
			
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.redoStack.push(mpundoObj);

			break;
		case UndoOperationKey_smiley:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			mpundoObj.nadeAsImage= oldnode.m_smile_image ;
			oldnode.m_smile_image=newnode.m_smile_image;
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.redoStack.push(mpundoObj);

			break;
		case UndoOperationKey_priority:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			mpundoObj.nadeAsImage= oldnode.priorityCharacter ;
			oldnode.priorityCharacter=newnode.priorityCharacter;
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.redoStack.push(mpundoObj);

			break;
		case UndoOperationKey_completion:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			mpundoObj.nadeAsImage= oldnode.completionCharacter ;
			oldnode.completionCharacter=newnode.completionCharacter;
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.redoStack.push(mpundoObj);

			break;
		case UndoOperationKey_StartDate:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;

			mpundoObj.info= oldnode.getM_start_date() ;
			oldnode.setM_start_date(newnode.getM_start_date());

			mpundoObj.info2= oldnode.getM_end_date() ;
			oldnode.setM_end_date(newnode.getM_end_date());
			
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.redoStack.push(mpundoObj);

			break;
		case UndoOperationKey_endDate:


			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;

			mpundoObj.info= oldnode.getM_end_date();
			oldnode.setM_end_date(newnode.getM_end_date());

			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.redoStack.push(mpundoObj);

			break;


		case UndoOperationKey_generalimg:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			mpundoObj.nadeAsImage= oldnode.genCharacter ;
			oldnode.genCharacter=newnode.genCharacter;
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.redoStack.push(mpundoObj);

			break;
		case UndoOperationKey_clipart:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			mpundoObj.nadeAsImage= oldnode.dCharacter ;
			oldnode.dCharacter=newnode.dCharacter;
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.redoStack.push(mpundoObj);

			break;
		case UndoOperationKey_textAlign:
		{
			newnode  = (MPNode)mpundoObj.objnew;
			int nstl = newnode.textAlign;
			newnode.textAlign = mpundoObj.oldTxtAlignMent;
			newnode.m_shape.invalidate();
			mpundoObj.oldTxtAlignMent      = nstl;
			pushRedoStack(mpundoObj);
		}
		break;
		case UndoOperationKey_font:
		{
			newnode          = (MPNode)mpundoObj.objnew;
			oldnode          = (MPNode)mpundoObj.objold;
			float fSize              = newnode.m_iNode_TextSize;
			String strFont        = newnode.m_strFontName;
			newnode.m_strFontName    = oldnode.m_strFontName;
			newnode.m_iNode_TextSize = oldnode.m_iNode_TextSize;
			oldnode.m_strFontName    = strFont;
			oldnode.m_iNode_TextSize = (int) fSize;
			Typeface typeFace=	MindMapActivity.mplayout.createTypeface(newnode, strFont);
			newnode.m_str_Typeface=typeFace;
			newnode.m_shape.getm_mpNode().setM_str_Typeface(typeFace);
			newnode.m_shape.calculateTextWidth();
			newnode.m_shape.invalidate();
			if (newnode.m_parentNode!=null) {
				mpLayout.setLayout(newnode.m_parentNode);
			}else{
				mpLayout.setLayout(newnode);
			}
			pushRedoStack(mpundoObj);
		}
		break;
		case UndoOperationKey_textColor:
		{
			MPNode node = (MPNode)mpundoObj.objold;
			node.textColor = mpundoObj.shape_oldColor;
			node.m_shape.calculateTextWidth();
			node.m_shape.invalidate();

			pushRedoStack(mpundoObj);
		}
		break;


		case UndoOperationKey_textEditing:

			newnode          = (MPNode)mpundoObj.objnew;
			oldnode          = (MPNode)mpundoObj.objold;
			String strText        = newnode.m_strNode_text;
			newnode.m_strNode_text   = oldnode.m_strNode_text;
			oldnode.m_strNode_text   = strText;
			if (newnode.m_parentNode!=null) {
				mpLayout.setLayout(newnode.m_parentNode);
			}else{
				mpLayout.setLayout(newnode);
			}
			// this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);

			break;
		case UndoOperationKey_resources:

			newnode          = (MPNode)mpundoObj.objnew;
			oldnode          = (MPNode)mpundoObj.objold;
			strText        = newnode.m_resource;
			newnode.m_resource   = oldnode.m_resource;
			oldnode.m_resource   = strText;
			if (newnode.m_parentNode!=null) {
				mpLayout.setLayout(newnode.m_parentNode);
			}else{
				mpLayout.setLayout(newnode);
			}
			// this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);
			break;
		case UndoOperationKey_duration:

			newnode          = (MPNode)mpundoObj.objnew;
			oldnode          = (MPNode)mpundoObj.objold;
			int duration        = newnode.m_duration;
			newnode.m_duration   = oldnode.m_duration;
			oldnode.m_duration   = duration;

			String durationtype        = newnode.m_duration_type;
			newnode.m_duration_type   = oldnode.m_duration_type;
			oldnode.m_duration_type   = durationtype;
			if (newnode.m_parentNode!=null) {
				mpLayout.setLayout(newnode.m_parentNode);
			}else{
				mpLayout.setLayout(newnode);
			}
			// this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);
			break;
		case UndoOperationKey_notesAttachment:
		{
			newnode           = (MPNode)mpundoObj.objnew;
			oldnode           = (MPNode)mpundoObj.objold;
			strText         = newnode.m_strNode_Notes;
			newnode.m_strNode_Notes   = oldnode.m_strNode_Notes;
			oldnode.m_strNode_Notes   = strText;

			if (newnode.m_parentNode!=null) {
				mpLayout.setLayout(newnode.m_parentNode);
			}else{
				mpLayout.setLayout(newnode);
			}
			// this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);
		}
		break;
		case UndoOperationKey_changeNodePosition:
		{
			newnode   = (MPNode)mpundoObj.objnew;
			oldnode   = (MPNode)mpundoObj.objold;
			MPNode oldParent = oldnode.m_parentNode;
			MPNode newParent = newnode.m_parentNode;
			int newNodePosition = newParent.m_arrChildren.indexOf(newnode);
			boolean newnodeIsaboveCenter = newnode.m_isAboveCenter;
			boolean newnodeIsRightNode = newnode.m_isRightnode;

			if(oldParent!=null){
			if(oldParent.equals(newParent)){
				if(newParent.m_arrChildren.size()<mpundoObj.oldObjIndex){
					this.undoStack.remove(mpundoObj);
					return;
				}
				newParent.m_arrChildren.remove(newnode);

			}else{
				if(oldParent.m_arrChildren.size()<mpundoObj.oldObjIndex){
					this.undoStack.remove(mpundoObj);
					return;
				}
				newParent.m_arrChildren.remove(newnode);
				newnode.m_parentNode = oldParent;
			}
			}
			newnode.m_isAboveCenter = oldnode.m_isAboveCenter;
			newnode.m_isRightnode = oldnode.m_isRightnode;
			int index = mpundoObj.oldObjIndex;
			if (index == -1)
				index = 0;
			newnode.m_parentNode.m_arrChildren.add(index,newnode );
			oldnode.m_isRightnode = newnodeIsRightNode;
			oldnode.m_isAboveCenter = newnodeIsaboveCenter;
			mpundoObj.oldObjIndex = newNodePosition;
			oldnode.m_parentNode = newParent;
			MPEbag curEbag = ManahijApp.m_curEbag;

			if(curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap){
				float xoff = newnode.m_fX ;
				float yoff =  newnode.m_fY;
				newnode.m_fX = oldnode.m_fX;
				newnode.m_fY = oldnode.m_fY;
				oldnode.m_fX = xoff;
				oldnode.m_fY = yoff;

			}
			if(newnode.equals(curEbag) && curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap){
				mpLayout.setLayout(ManahijApp.m_curEbag);
			}else{
				mpLayout.setLayout(ManahijApp.m_curEbag);
			}
			// this.redoStack.push(mpundoObj);
			pushRedoStack(mpundoObj);
		}
		break;
		case UndoOperationKey_expand:
		{
			newnode   = (MPNode)mpundoObj.objnew;
			mpLayout.collapseNodeOf(newnode.m_shape);
			pushRedoStack(mpundoObj);
		}
		break;
		case UndoOperationKey_orphannode:
		{
			newnode   = (MPNode)mpundoObj.objnew;
			oldnode   = (MPNode)mpundoObj.objold;
			MPNode newParent = newnode.m_parentNode;
			oldnode.m_iLevelId = newnode.m_iLevelId;
			if(newParent!=null){
			int newNodePosition   = newParent.m_arrChildren.indexOf(newnode);
			mpundoObj.oldObjIndex = newNodePosition;
			oldnode.m_parentNode = newParent;
		
			
			newnode.m_iLevelId = -1;
			newParent.m_arrChildren.remove(newnode);
			newnode.m_parentNode = null;
		}
			((MPNodeView)newnode.m_shape).rect = oldnode.getBounds();
			mpLayout.deleteNodeOf(newnode.m_shape);
			pushRedoStack(mpundoObj);
		}
		break;

		default:
			break;
		}
	}

	public void doOpeartionRedo()
	{
		if(redoStack.size()==0)
			return;
		mpundoObj = (MPUndoObject) this.redoStack.pop();
		if(mpundoObj==null)
			return;


		switch (mpundoObj.keyType) {
		case UndoOperationKey_addNode:
		{
			MPNode node = (MPNode)mpundoObj.objold;
			if(node.m_parentNode==null)
				return;
			mpLayout.addUndoNode(node);
			mpundoObj.keyType = UndoOperationKey.UndoOperationKey_deleteNode;
			this.undoStack.push(mpundoObj);
			mpLayout.setLayout(ManahijApp.m_curEbag);
		}
		break;
		case UndoOperationKey_deleteNode:
		{

			MPNode node = (MPNode)mpundoObj.objold;
			mpLayout.deleteNodeOf(node.m_shape);
			mpundoObj.keyType = UndoOperationKey.UndoOperationKey_addNode;
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_ShapeColor:
		{

			MPNode node = (MPNode)mpundoObj.objold;
			node.m_selectedColor = Color.parseColor(mpundoObj.shape_newColor);
			node.setBackgroundColor(
					node.m_selectedColor);
			//
			int gradientPos = node.m_iGradientType;
			node.m_iGradientType = mpundoObj.grad;
			mpundoObj.grad = gradientPos;
			//
			mpLayout.tempUpdateChildrenColor(node.m_shape,
					node.m_selectedColor, mpundoObj.shape_newColor);
			node.setHexaColor(mpundoObj.shape_oldColor);
			node.m_shape.invalidate();
			mpLayout.updateChildrenColor(node ,node.m_arrChildren);
			this.undoStack.push(mpundoObj);
			mpLayout.setLayout(node);
		}
		break;
		case UndoOperationKey_BackgroundColor:
		{


			MPEbag ebag = ManahijApp.m_curEbag;
			String newColor = ebag.m_strMapLayoutFillColor;
			ebag.m_strMapLayoutFillColor = mpundoObj.ebag_oldColor;
			mpundoObj.ebag_oldColor = newColor;
			MindMapActivity.drawingView.setBackgroundColor(Color.parseColor( ebag.m_strMapLayoutFillColor));
			this.undoStack.push(mpundoObj);

		}
		break;
		case UndoOperationKey_changeMapStyle:
		{

			MPEbag ebag = ManahijApp.m_curEbag;
			MapLayoutType newlType = ebag.m_iMapLayout;
			ebag.m_iMapLayout = mpundoObj.oldMapStyle;
			mpundoObj.oldMapStyle = newlType;
			this.undoStack.push(mpundoObj);
			if(ebag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap){

			}else{
				mpLayout.setLayout(ebag);
			}
			this.deleteUndoRedoStackOtherOperationsAfterSwitchingOneLayoutToOtherLayout();

		}
		break;
		case UndoOperationKey_changeLineStyle:
		{

			MPEbag ebag = ManahijApp.m_curEbag;
			MapLayoutLineType newlType = ebag.m_iMapLayoutLine;
			ebag.m_iMapLayoutLine = mpundoObj.oldLineStyle;
			mpundoObj.oldLineStyle = newlType;
			this.undoStack.push(mpundoObj);
			mpLayout.setLayout(ebag);

		}
		break;

		case UndoOperationKey_changeShapeStyle:

			MPNode newnode = (MPNode)mpundoObj.objnew;
			MPNode oldnode = (MPNode)mpundoObj.objold;
			MapLayoutShapeType nsType = oldnode.m_iMapLayoutShape;
			oldnode.m_iMapLayoutShape = newnode.m_iMapLayoutShape;
			newnode.m_iMapLayoutShape = nsType;
			this.undoStack.push(mpundoObj);
			mpLayout.setLayout(oldnode);
			mpLayout.setLayout(ManahijApp.m_curEbag);
			ManahijApp.m_curEbag.linesLayerView.invalidate();
			MindMapActivity act=new MindMapActivity();
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);

			break;

		case UndoOperationKey_copyPast:
		{
			this.handleCopyPastOperations(mpundoObj ,false);
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_cutPast:
		{
			this.handleCopyPastOperations(mpundoObj ,false);
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_copyPastPro:
		{
			this.handleCopyPastOperations(mpundoObj ,false);
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_hyperAttachment:
			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			oldnode.m_strHyperlinkUrl = mpundoObj.info;
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.undoStack.push(mpundoObj);

			break;
		case UndoOperationKey_smiley:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			oldnode.m_smile_image = mpundoObj.nadeAsImage;
			//mpLayout.setLayout(oldnode);
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.undoStack.push(mpundoObj);

			break;
			
		case UndoOperationKey_priority:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			oldnode.priorityCharacter = mpundoObj.nadeAsImage;
			//mpLayout.setLayout(oldnode);
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.undoStack.push(mpundoObj);

			break;
		case UndoOperationKey_completion:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			oldnode.completionCharacter = mpundoObj.nadeAsImage;
			//mpLayout.setLayout(oldnode);
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.undoStack.push(mpundoObj);

			break;
		case UndoOperationKey_StartDate:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			oldnode.m_start_date = mpundoObj.info;
			oldnode.m_end_date = mpundoObj.info2;
			//mpLayout.setLayout(oldnode);
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.undoStack.push(mpundoObj);

			break;
		case UndoOperationKey_endDate:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;

			oldnode.m_end_date = mpundoObj.info;
			//mpLayout.setLayout(oldnode);
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.undoStack.push(mpundoObj);

			break;


		case UndoOperationKey_generalimg:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			oldnode.genCharacter = mpundoObj.nadeAsImage;
			//mpLayout.setLayout(oldnode);
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.undoStack.push(mpundoObj);

			break;
		case UndoOperationKey_clipart:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			oldnode.dCharacter = mpundoObj.nadeAsImage;
			//	mpLayout.setLayout(oldnode);
			oldnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(oldnode);
			this.undoStack.push(mpundoObj);

			break;

		case UndoOperationKey_imageAsNode:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			oldnode.m_background_image = mpundoObj.nadeAsImage;
			
			oldnode.imageAttachementype = mpundoObj.grad;
			oldnode.m_strMultimediaUrl = mpundoObj.info;
			oldnode.imageUrl = mpundoObj.info2;
			oldnode.base64image = mpundoObj.shape_oldColor;
			oldnode.mediaType =  mpundoObj.mediatype;
			oldnode.multimediaurl =mpundoObj.multimediapath;
			
			oldnode.embededImage = null;
			oldnode.m_attachement_image = null;
			
			/*oldnode.m_shape.invalidate();*/
			
			newnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(newnode);
			mpLayout.setLayout(ManahijApp.m_curEbag);
			this.undoStack.push(mpundoObj);

			break;
		case UndoOperationKey_imageAttachment:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			oldnode.m_strMultimediaUrl = mpundoObj.info;
			oldnode.imageUrl = mpundoObj.info2;
			oldnode.imageAttachementype = mpundoObj.grad;
			oldnode.base64image = mpundoObj.shape_oldColor;
			oldnode.mediaType =  mpundoObj.mediatype;
			oldnode.multimediaurl =mpundoObj.multimediapath;
			oldnode.m_attachement_image = mpundoObj.nadeAsImage;
			oldnode.embededImage = null;
			oldnode.m_background_image = null;
			newnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(newnode);
			mpLayout.setLayout(ManahijApp.m_curEbag);
			this.undoStack.push(mpundoObj);

			break;
		case UndoOperationKey_imageEmbed:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			
			oldnode.embededImage = mpundoObj.nadeAsImage;
			
			oldnode.m_strMultimediaUrl = mpundoObj.info;
			oldnode.imageUrl = mpundoObj.info2;
			oldnode.imageAttachementype = mpundoObj.grad;
			oldnode.base64image = mpundoObj.shape_oldColor;
			
			oldnode.mediaType =  mpundoObj.mediatype;
			oldnode.multimediaurl =mpundoObj.multimediapath;
			oldnode.m_attachement_image = null;
			oldnode.m_background_image = null;
			newnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(newnode);
			mpLayout.setLayout(ManahijApp.m_curEbag);

			this.undoStack.push(mpundoObj);

			break;
		case UndoOperationKey_videoattachment:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			
			oldnode.multimediaurl = mpundoObj.multimediapath;
			oldnode.mediaType = mpundoObj.mediatype;
			

			oldnode.m_strMultimediaUrl = mpundoObj.info;
			oldnode.imageUrl = mpundoObj.info2;
			oldnode.imageAttachementype = mpundoObj.grad;
			oldnode.base64image = mpundoObj.shape_oldColor;
			
			oldnode.m_attachement_image = null;
			oldnode.m_background_image = null;
			oldnode.embededImage = null;
			newnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(newnode);
			mpLayout.setLayout(ManahijApp.m_curEbag);
			this.undoStack.push(mpundoObj);

			break;
		case UndoOperationKey_audioattachment:

			newnode = (MPNode)mpundoObj.objnew;
			oldnode = (MPNode)mpundoObj.objold;
			oldnode.multimediaurl = mpundoObj.multimediapath;
			oldnode.mediaType = mpundoObj.mediatype;

			oldnode.m_strMultimediaUrl = mpundoObj.info;
			oldnode.imageUrl = mpundoObj.info2;
			oldnode.imageAttachementype = mpundoObj.grad;
			oldnode.base64image = mpundoObj.shape_oldColor;
			
			oldnode.m_attachement_image = null;
			oldnode.m_background_image = null;
			oldnode.embededImage = null;
			newnode.m_shape.calculateTextWidth();
			mpLayout.setLayout(newnode);
			mpLayout.setLayout(ManahijApp.m_curEbag);
			this.undoStack.push(mpundoObj);

			break;

		case UndoOperationKey_textAlign:
		{
			newnode = (MPNode)mpundoObj.objnew;
			int nstl = newnode.textAlign;
			newnode.textAlign = mpundoObj.oldTxtAlignMent;
			newnode.m_shape.invalidate();
			mpundoObj.oldTxtAlignMent = nstl;
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_font:
		{
			newnode          = (MPNode)mpundoObj.objnew;
			oldnode          = (MPNode)mpundoObj.objold;
			int fSize              = newnode.m_iNode_TextSize;
			String strFont        = newnode.m_strFontName;
			newnode.m_strFontName    = oldnode.m_strFontName;
			newnode.m_iNode_TextSize = oldnode.m_iNode_TextSize;
			oldnode.m_strFontName    = strFont;
			oldnode.m_iNode_TextSize = fSize;
			Typeface typeFace=	MindMapActivity.mplayout.createTypeface(newnode, strFont);
			newnode.m_str_Typeface=typeFace;
			newnode.m_shape.getm_mpNode().setM_str_Typeface(typeFace);
			newnode.m_shape.calculateTextWidth();
			newnode.m_shape.invalidate();
			if (newnode.m_parentNode!=null) {
				mpLayout.setLayout(newnode.m_parentNode);
			}else{
				mpLayout.setLayout(newnode);
			}
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_textColor:
		{
			// MPNode newnode          = (MPNode )mpundoObj.objnew;
			oldnode          = (MPNode)mpundoObj.objold;
			oldnode.textColor = mpundoObj.shape_newColor;
			oldnode.m_shape.calculateTextWidth();
			oldnode.m_shape.invalidate();
			this.undoStack.push(mpundoObj);
		}
		break;
		
		case UndoOperationKey_textEditing:
		{
			newnode          = (MPNode)mpundoObj.objnew;
			oldnode          = (MPNode)mpundoObj.objold;
			String strText        = newnode.m_strNode_text;
			newnode.m_strNode_text   = oldnode.m_strNode_text;
			oldnode.m_strNode_text   = strText;

			if (newnode.m_parentNode!=null) {
				mpLayout.setLayout(newnode.m_parentNode);
			}else{
				mpLayout.setLayout(newnode);
			}
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_resources:
		{
			newnode          = (MPNode)mpundoObj.objnew;
			oldnode          = (MPNode)mpundoObj.objold;
			String strText        = newnode.m_resource;
			newnode.m_resource   = oldnode.m_resource;
			oldnode.m_resource   = strText;

			if (newnode.m_parentNode!=null) {
				mpLayout.setLayout(newnode.m_parentNode);
			}else{
				mpLayout.setLayout(newnode);
			}
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_duration:


			newnode          = (MPNode)mpundoObj.objnew;
			oldnode          = (MPNode)mpundoObj.objold;
			int duration        = newnode.m_duration;
			newnode.m_duration   = oldnode.m_duration;
			oldnode.m_duration   = duration;

			String durationtype=newnode.m_duration_type;
			newnode.m_duration_type   = oldnode.m_duration_type;
			oldnode.m_duration_type   = durationtype;

			if (newnode.m_parentNode!=null) {
				mpLayout.setLayout(newnode.m_parentNode);
			}else{
				mpLayout.setLayout(newnode);
			}
			this.undoStack.push(mpundoObj);
			break;
		case UndoOperationKey_notesAttachment:
		{
			newnode          = (MPNode)mpundoObj.objnew;
			oldnode          = (MPNode)mpundoObj.objold;
			String strText        = newnode.m_strNode_Notes;
			newnode.m_strNode_Notes  = oldnode.m_strNode_Notes;
			oldnode.m_strNode_Notes  = strText;
			if (newnode.m_parentNode!=null) {
				mpLayout.setLayout(newnode.m_parentNode);
			}else{
				mpLayout.setLayout(newnode);
			}
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_changeNodePosition:
		{
			newnode   = (MPNode)mpundoObj.objnew;
			oldnode   = (MPNode)mpundoObj.objold;
			MPNode oldParent = oldnode.m_parentNode;
			MPNode newParent = newnode.m_parentNode;
			int newNodePosition   = newParent.m_arrChildren.indexOf(newnode);
			boolean newnodeIsaboveCenter = newnode.m_isAboveCenter;
			boolean newnodeIsRightNode = newnode.m_isRightnode;          

			if(oldParent.equals(newParent)){
				if(newParent.m_arrChildren.size()<mpundoObj.oldObjIndex){
					this.undoStack.remove(mpundoObj);
					return;
				}
				newParent.m_arrChildren.remove(newnode);

			}else{
				if(oldParent.m_arrChildren.size()<mpundoObj.oldObjIndex){
					this.undoStack.remove(mpundoObj);
					return;
				}
				newParent.m_arrChildren.remove(newnode);
				newnode.m_parentNode = oldParent;
			}
			newnode.m_isAboveCenter = oldnode.m_isAboveCenter;
			newnode.m_isRightnode = oldnode.m_isRightnode;

			newnode.m_parentNode.m_arrChildren.add(mpundoObj.oldObjIndex,newnode );
			oldnode.m_isRightnode = newnodeIsRightNode;
			oldnode.m_isAboveCenter = newnodeIsaboveCenter;
			mpundoObj.oldObjIndex = newNodePosition;
			oldnode.m_parentNode = newParent;
			MPEbag curEbag = ManahijApp.m_curEbag;
			if(curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap){
				float xoff = newnode.m_fX ;
				float yoff =  newnode.m_fY;
				newnode.m_fX = oldnode.m_fX;
				newnode.m_fY = oldnode.m_fY;
				oldnode.m_fX = xoff;
				oldnode.m_fY = yoff;

			}
			if(newnode.equals(curEbag) && curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap){
				mpLayout.setLayout(newnode);
			}else{
				//  mpLayout.animatedMap(newnode);
				mpLayout.setLayout(newnode);
			}
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_expand:
		{
			newnode   = (MPNode)mpundoObj.objnew;
			mpLayout.collapseNodeOf(newnode.m_shape);
			this.undoStack.push(mpundoObj);
		}
		break;
		case UndoOperationKey_orphannode:
		{
			newnode   = (MPNode)mpundoObj.objnew;
			oldnode   = (MPNode)mpundoObj.objold;
			newnode.m_parentNode = oldnode.m_parentNode;
			if(newnode.m_parentNode!=null&&newnode.m_parentNode.m_arrChildren.size()<mpundoObj.oldObjIndex){
				this.undoStack.remove(mpundoObj);
				return;

			}
			if(newnode.m_parentNode!=null)
			newnode.m_parentNode.m_arrChildren.add(mpundoObj.oldObjIndex,newnode );
			newnode.m_iLevelId = oldnode.m_iLevelId;
			 mpLayout.addshapes(MindMapActivity.drawingView,newnode);//(newnode);
			this.undoStack.push(mpundoObj);
		}
		break;

		/*  */

		default:
			break;
		}


	}

	public void handleCopyPastOperations(MPUndoObject mpuObj ,boolean value)
	{

		MPEbag curEbag = ManahijApp.m_curEbag;
		ViewGroup mapView = mpLayout.getMapView(curEbag.m_shape);

		switch (mpuObj.keyType) {
		case UndoOperationKey_copyPast:
		{
			MPNode oldNode = (MPNode)mpundoObj.objold;
			MPNode newNode = (MPNode)mpundoObj.objnew;
			if (value) {
				newNode.deleteNode();
				newNode.m_parentNode = null;
			}else{
				newNode.m_parentNode = oldNode.m_parentNode;
				oldNode.m_parentNode.m_arrChildren.add(newNode);
				mpLayout.addshapes(mapView, newNode);
				MPNodeView newShape = newNode.m_shape;
			}

			mpLayout.setLayout(oldNode);

		}
		break;
		case UndoOperationKey_cutPast:
		{
			MPNode oldNode = (MPNode)mpundoObj.objold;//copy of editable node
			MPNode newNode = (MPNode)mpundoObj.objnew;
			MPNode oldParent = oldNode.m_parentNode;
			MPNode newParent = newNode.m_parentNode;
			newNode.m_parentNode = oldNode.m_parentNode;
			oldNode.m_parentNode = newParent;
			oldParent.m_arrChildren.add(newNode);
			newParent.m_arrChildren.remove(newNode);
			//  newNode.deleteOnlyShapes();

			if (curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap) {
				//mpLayout.resetNodePositionForFreemMap(newNode);                
			}
			//mapView.addView(newNode.m_shape);

			mpLayout.setLayout(newNode);

		}
		break;
		case UndoOperationKey_copyPastPro:
		{
			MPNode oldNode = (MPNode)mpundoObj.objold;//copy of selectedNode i.e undoable node
			MPNode newNode = (MPNode)mpundoObj.objnew;
			MPNode extraNode = (MPNode)mpundoObj.objExtra;
			if (value){              
				newNode.m_selectedColor = oldNode.m_selectedColor;
				newNode.hexaColor = oldNode.hexaColor;
				newNode.backgroundColor=oldNode.backgroundColor;
				newNode.m_strNode_Notes = oldNode.m_strNode_Notes;
				newNode.m_strFontName = oldNode.m_strFontName;
				newNode.m_iNode_TextSize = oldNode.m_iNode_TextSize;
				newNode.m_iMapLayoutShape = oldNode.m_iMapLayoutShape;
				//newNode.m_smile_image=oldNode.m_smile_image;
				//newNode.m_attachement_image=oldNode.m_attachement_image;
				//newNode.m_background_image=oldNode.m_background_image;
				newNode.textColor = oldNode.textColor;
				newNode.m_iGradientType = oldNode.m_iGradientType;
				newNode.m_str_Typeface= oldNode.m_str_Typeface;
				MPNodeView shape = newNode.m_shape;
				newNode.m_isCollapsed = oldNode.m_isCollapsed;
				newNode.m_shape.invalidate();

			}else{

				newNode.hexaColor = extraNode.hexaColor;
				newNode.backgroundColor=extraNode.backgroundColor;
				newNode.m_selectedColor = extraNode.m_selectedColor;
				newNode.m_strNode_Notes = extraNode.m_strNode_Notes;
				newNode.m_strFontName   = extraNode.m_strFontName;
				newNode.m_iNode_TextSize = extraNode.m_iNode_TextSize;
				newNode.m_iMapLayoutShape = extraNode.m_iMapLayoutShape;
				newNode.textColor = extraNode.textColor;
				newNode.m_iGradientType = extraNode.m_iGradientType;
				newNode.m_str_Typeface= extraNode.m_str_Typeface;;
				//newNode.m_smile_image=extraNode.m_smile_image;
				//newNode.m_attachement_image=extraNode.m_attachement_image;
				//newNode.m_background_image=extraNode.m_background_image;
				MPNodeView shape = newNode.m_shape;
				newNode.m_isCollapsed = extraNode.m_isCollapsed;

			}
			newNode.m_shape.calculateTextWidth();
			newNode.m_shape.invalidate();
			/*if(newNode.m_shape.m_node.m_background_image!=null)
	    			newNode.m_shape.calculateWidth();*/
			mpLayout.setLayout(newNode);
			mpLayout.setLayout(ManahijApp.m_curEbag);
		}
		break;
		case UndoOperationKey_textAlign:
			//  self.selectedNode.m_txtlbl.textAlignment = NSTextAlignmentLeft;
			break;

		default:
			break;
		}
	}
	public void deleteUndoRedoStackOtherOperationsAfterSwitchingOneLayoutToOtherLayout()
	{
		Stack clonedUndoStack=(Stack) this.undoStack.clone();
		for(Object obj :clonedUndoStack){
			if(((MPUndoObject)obj).keyType != UndoOperationKey.UndoOperationKey_changeMapStyle){
				this.undoStack.remove(obj);
			}
		}
		Stack clonedRedoStack=(Stack) this.redoStack.clone();
		for(Object obj : clonedRedoStack){
			if(((MPUndoObject)obj).keyType != UndoOperationKey.UndoOperationKey_changeMapStyle){
				this.redoStack.remove(obj);
			}
		}


	}
	public void cleanUndoManager(){
		undoStack.removeAllElements();;
		redoStack.removeAllElements();
	}

	public void clearRedoStack()
	{
		redoStack.removeAllElements();;
	}

	public void pushUndoStack(MPUndoObject mpundoObj){
		if(undoStack.size()==stackLimit)
			undoStack.remove(0);
		undoStack.push(mpundoObj);
	}
	public void pushRedoStack(MPUndoObject mpundoObj){
		if(redoStack.size()==stackLimit)
			redoStack.remove(0);
		redoStack.push(mpundoObj);
	}
}
