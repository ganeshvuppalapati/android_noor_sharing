package com.ptg.mindmap.enums;

public enum UndoOperationKey {
	UndoOperationKey_addNode ,//*
    UndoOperationKey_deleteNode,//*
    UndoOperationKey_changeNodePosition,
    UndoOperationKey_changeMapStyle,//*
    UndoOperationKey_changeLineStyle,//*
    UndoOperationKey_changeShapeStyle,//*
    UndoOperationKey_addNodeBgImage,//*
    UndoOperationKey_addNodeImageAttachment,//*
    UndoOperationKey_addNodeSmileImage,
    UndoOperationKey_ShapeColor,//*
    UndoOperationKey_BackgroundColor,//*
    UndoOperationKey_copyPast,//*
    UndoOperationKey_cutPast,//*
    UndoOperationKey_copyPastPro,
    UndoOperationKey_textAlign,//*
    UndoOperationKey_textColor,//*
    UndoOperationKey_textEditing,//*
    UndoOperationKey_font,//*
    UndoOperationKey_fontColor,//*
    UndoOperationKey_imageAsNode,//*
    UndoOperationKey_imageAttachment,//*
    UndoOperationKey_imageEmbed,
    UndoOperationKey_videoattachment,
    UndoOperationKey_audioattachment,
    UndoOperationKey_smiley,//*
    UndoOperationKey_generalimg,
    UndoOperationKey_clipart,
    UndoOperationKey_notesAttachment,
    UndoOperationKey_expand,//*
    UndoOperationKey_orphannode,
    UndoOperationKey_priority,
    UndoOperationKey_completion,
    UndoOperationKey_endDate,
    UndoOperationKey_StartDate,
    UndoOperationKey_duration,
    UndoOperationKey_resources,
    UndoOperationKey_hyperAttachment

}
