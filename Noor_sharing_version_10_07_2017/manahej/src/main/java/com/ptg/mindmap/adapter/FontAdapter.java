package com.ptg.mindmap.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.ptg.mimdmap.spinner.AbstractWheelTextAdapter;
import com.semanoor.manahij.R;

import java.util.ArrayList;

public class FontAdapter extends AbstractWheelTextAdapter {
	// Countries names
	Context context;
	int pad = 1;

	int index = -1;
	ArrayList<String> fontsList;
	public boolean issetFont = true;
boolean value;
	public FontAdapter(Context context, ArrayList<String> fonts,boolean value) {
		super(context, R.layout.mi_font_item, NO_RESOURCE);
		this.context = context;
		fontsList = fonts;
		pad = 2;
		this.value=value;
		setItemTextResource(R.id.font_name);
	}
	

	@Override
	public View getItem(int position, View cachedView, ViewGroup parent) {
		TextView tv;
		
		View view = super.getItem(position, cachedView, parent);
		tv = (TextView) view.findViewById(R.id.font_name);
		String font = fontsList.get(position);
		// tv = new TextView(context);
		LayoutParams paramsExample = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		 tv.setLayoutParams(paramsExample);

		// tv.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.listselector));
		tv.setGravity(Gravity.CENTER_VERTICAL);
		tv.setTextAlignment(Gravity.CENTER_VERTICAL);
		tv.setSingleLine();
		tv.setText(font);
		// tv.setHeight((int)Util.toPx(30));

		if (index == position)
			tv.setTextColor(Color.WHITE);
		else
			tv.setTextColor(Color.BLACK);

		//tv.setPadding(pad, pad, pad, pad);

		tv.setGravity(Gravity.LEFT);
		
			tv.setTextSize(16);
		 if(value)
		 {
			if (issetFont)
			{
				if(font.equals("Arial"))
				{
					tv.setTypeface(Typeface.createFromAsset(context.getAssets(),"fontfamily/" + font+".ttf"));
	
				}		
				else if(font.equals("Courier"))
				{
					tv.setTypeface(Typeface.createFromAsset(context.getAssets(),"fontfamily/" + font+".ttf"));
	
				}
				else if(font.equals("Helvetica"))
				{
					tv.setTypeface(Typeface.createFromAsset(context.getAssets(),"fontfamily/" + font+".ttf"));
	
				}
				else if(font.equals("Open Sans"))
				{
					tv.setTypeface(Typeface.createFromAsset(context.getAssets(),"fontfamily/" + font+".ttf"));
	
				}
				else if(font.equals("Tahoma"))
				{
					tv.setTypeface(Typeface.createFromAsset(context.getAssets(),"fontfamily/" + font+".ttf"));
	
				}
				else if(font.equals("Times New Roman"))
				{
					tv.setTypeface(Typeface.createFromAsset(context.getAssets(),"fontfamily/" + font+".ttf"));
	
				}
				else if(font.equals("Verdana"))
				{
					tv.setTypeface(Typeface.createFromAsset(context.getAssets(),"fontfamily/" + font+".ttf"));
	
				}				
				else
				{
					tv.setTypeface(Typeface.createFromFile("/system/fonts/" + font+ ".ttf"));
				}
			}
			else 
			{
			 //tv = (TextView) view;
			}
		}
		return view;
	}

	@Override
	public int getItemsCount() {
		return fontsList.size();
	}

	@Override
	protected CharSequence getItemText(int index) {
		return fontsList.get(index);
	}
}
