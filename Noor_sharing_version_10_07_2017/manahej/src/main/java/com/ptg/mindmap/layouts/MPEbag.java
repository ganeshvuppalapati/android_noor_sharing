package com.ptg.mindmap.layouts;

import android.graphics.RectF;

import com.ptg.mimdmap.node.LinesLayer;
import com.ptg.mimdmap.node.MPNode;
import com.ptg.mimdmap.node.ShadowLayer;
import com.ptg.mindmap.enums.MapLayoutLineType;
import com.ptg.mindmap.enums.MapLayoutType;
import com.ptg.mindmap.enums.MapState;

import java.util.ArrayList;

public class MPEbag extends MPNode {
	public ArrayList<MPNode> m_arrRightTop = new ArrayList<MPNode>();
	public ArrayList<MPNode> m_arrRightBottom = new ArrayList<MPNode>();
	public ArrayList<MPNode> m_arrLeftBottom = new ArrayList<MPNode>();
	public ArrayList<MPNode> m_arrLeftTop = new ArrayList<MPNode>();
	public ShadowLayer layer;
	float m_frtHeight, m_frbHeight, m_flbHeight, m_fltHeight;
	public float m_fminX, m_fmaxX, m_fminY, m_fmaxY;
	MPNode m_currentNode;
	public MPNode m_currNodeBeingDragged;
	MPNode m_nearestNodeToTheCurrentNodeBeingDragged;
	boolean m_isRightNodeBeingMoved;
	int m_iaddPagePosition;
	public int m_iNumberOfRightNodes;
	public MapLayoutLineType m_iMapLayoutLine;
	public MapState m_impstate;
	int m_itotalNodes;
	public MPNode copyMpNode;
	public MPNode cutMpNode;
	public MPNode copyPropertiesMpNode;
	public String m_strMapLayoutFillColor;
	// Map Properties
	public MapLayoutType m_iMapLayout;
	public boolean m_isKeepFreeMapSamePositions;
	public LinesLayer linesLayerView;
	public boolean isEnableScroll=true;
	public boolean isEnableScroll() {
		return isEnableScroll;
	}
	public void setEnableScroll(boolean isEnableScroll) {
		this.isEnableScroll = isEnableScroll;
	}
	float m_fscale;

	public MPEbag() {
		init();
	}
	public MPEbag copyMpeBag(){
		MPEbag tempebag=new MPEbag();
		tempebag.m_arrRightTop=this.m_arrRightTop;
		tempebag.m_arrRightBottom=this.m_arrRightBottom;
		tempebag.m_arrLeftBottom=this.m_arrLeftBottom;
		tempebag.m_arrLeftTop=this.m_arrLeftTop;
		
		
		tempebag.layer=this.layer;
		tempebag.m_fminX=this.m_fminX;
		tempebag.m_fmaxX=this.m_fmaxX;
		tempebag.m_fmaxY=this.m_fmaxY;
		tempebag.m_fminY=this.m_fminY;
		tempebag.m_fX=this.m_fX;
		tempebag.m_fY=this.m_fY;
		tempebag.m_currentNode=this.m_currentNode;
		tempebag.m_currNodeBeingDragged=this.m_currNodeBeingDragged;
		tempebag.m_isRightNodeBeingMoved=this.m_isRightNodeBeingMoved;
		tempebag.m_iaddPagePosition=this.m_iaddPagePosition;
		tempebag.m_iNumberOfRightNodes=this.m_iNumberOfRightNodes;
		tempebag.m_iMapLayoutLine=this.m_iMapLayoutLine;
		
		tempebag.m_impstate=this.m_impstate;
		tempebag.linesLayerView=this.linesLayerView;
		tempebag.m_itotalNodes=this.m_itotalNodes;
		tempebag.copyMpNode=this.copyMpNode;
		tempebag.m_iMapLayoutLine=this.m_iMapLayoutLine;
		
		tempebag.m_fWidth = this.m_fWidth;
		tempebag.m_fHeight = this.m_fHeight;
		tempebag.m_childrenHeight = this.m_childrenHeight;
		tempebag.m_iLevelId = this.m_iLevelId == 0 ? 1 : this.m_iLevelId;
		tempebag.m_iNode_TextSize = this.m_iNode_TextSize;
		tempebag.m_strLocation = this.m_strLocation;
		tempebag.m_strNode_Notes = this.m_strNode_Notes;
		tempebag.m_strNode_text = this.m_strNode_text;
		tempebag.m_isAboveCenter = this.m_isAboveCenter;
		tempebag.m_isCollapsed = this.m_isCollapsed;
		tempebag.m_frtHeight= this.m_frtHeight; 
		tempebag.m_frbHeight= this.m_frbHeight; 
		tempebag.m_flbHeight= this.m_flbHeight; 
		tempebag.m_fltHeight= this.m_fltHeight; 
		
		tempebag.m_isRightnode = this.m_isRightnode;
		tempebag.m_parentNode = this.m_parentNode;
		tempebag.hexaColor = this.hexaColor;
		tempebag.m_selectedColor = this.m_selectedColor;
		tempebag.m_arrChildren = childrenCopy();
		
		if (m_str_Typeface != null)
			tempebag.m_str_Typeface = this.m_str_Typeface;
		tempebag.m_smile_image = this.m_smile_image;
		tempebag.m_attachement_image = this.m_attachement_image;
		tempebag.m_background_image = this.m_background_image;
		tempebag.m_strFontName = this.m_strFontName;
		tempebag.embededImage = this.embededImage;
		tempebag.m_iMapLayoutShape = this.m_iMapLayoutShape;
		tempebag.m_strBgImage = this.m_strBgImage;
		return tempebag;
	}

	public void init() {
		super.init();
		this.m_parentNode = null;
		this.m_iMapLayout = MapLayoutType.MPLayout_CircularMap;
		this.m_iMapLayoutLine = MapLayoutLineType.MPLayout_Line_Curved;
		this.m_impstate = MapState.MPStateNormal;
		this.m_fscale = 1.0f;
	}

	public void reset() {
		m_arrLeftBottom = new ArrayList<MPNode>();
		m_arrLeftTop = new ArrayList<MPNode>();
		m_arrRightBottom = new ArrayList<MPNode>();
		m_arrRightTop = new ArrayList<MPNode>();
		m_currentNode = null;
		m_currNodeBeingDragged = null;
		m_nearestNodeToTheCurrentNodeBeingDragged = null;
		this.m_arrChildren = new ArrayList<MPNode>();
		m_itotalNodes = 0;
	}

	public RectF getMapBounds() {
		RectF rect = new RectF(this.m_fminX, this.m_fminY,
				this.m_fmaxX/*-this.m_fminX*/, this.m_fmaxY/*-this.m_fminY*/);
		return rect;
	}

	public RectF setMapBounds() {
		this.m_fminX = this.getM_fX();
		this.m_fminY = this.getM_fY();
		this.m_fmaxX = this.getM_fX() + this.getM_fWidth();
		this.m_fmaxY = this.getM_fY() + this.getM_fHeight();
		this.calculateMapBounds(this.getM_arrChildren());
		RectF rect = new RectF(this.m_fminX, this.m_fminY,
				this.m_fmaxX/*-this.m_fminX*/, this.m_fmaxY/*-this.m_fminY*/);
		return rect;
	}

	public void calculateMapBounds(ArrayList<MPNode> arrChildren) {
		if (arrChildren != null)
			for (MPNode node : arrChildren) {
				this.calculateMapBounds(node.getM_arrChildren());
				this.m_fminX = Math.min(this.m_fminX, node.getM_fX());
				this.m_fminY = Math.min(this.m_fminY, node.getM_fY());
				this.m_fmaxX = Math.max(this.m_fmaxX,
						node.getM_fX() + node.getM_fWidth());
				this.m_fmaxY = Math.max(this.m_fmaxY,
						node.getM_fY() + node.getM_fHeight());
			}
	}

	public MapLayoutType getM_iMapLayout() {
		return m_iMapLayout;
	}

	public void setM_iMapLayout(MapLayoutType m_iMapLayout) {
		this.m_iMapLayout = m_iMapLayout;
	}

	public MapLayoutLineType getM_iMapLayoutLine() {
		return m_iMapLayoutLine;
	}

	public void setM_iMapLayoutLine(MapLayoutLineType m_iMapLayoutLine) {
		this.m_iMapLayoutLine = m_iMapLayoutLine;
	}

	public int getM_iNumberOfRightNodes() {
		return m_iNumberOfRightNodes;
	}

	public void setM_iNumberOfRightNodes(int m_iNumberOfRightNodes) {
		this.m_iNumberOfRightNodes = m_iNumberOfRightNodes;
	}
	public  void arrageChildrenOrder()
	{

	    if (m_iMapLayout != MapLayoutType.MPLayout_FreeMap){
	        int nodeorder = 0;
	        m_iNumberOfRightNodes = m_arrRightBottom.size() + m_arrRightTop.size();
	        for (int k=0;k<m_arrRightTop.size(); k++){
	            nodeorder ++;
	            MPNode node = m_arrRightTop.get(k);
	            node.m_iNode_Order = nodeorder;
	        }
	        for (int k=0;k<m_arrRightBottom.size(); k++){
	            nodeorder ++;
	            MPNode node = m_arrRightBottom.get(k);
	            node.m_iNode_Order = nodeorder;
	        }
	        for (int k=m_arrLeftBottom.size()-1;k>=0; k--){
	            nodeorder ++;
	            MPNode node = m_arrLeftBottom.get(k);
	            node.m_iNode_Order = nodeorder;
	        }
	        for (int k=m_arrLeftTop.size()-1;k>=0; k--){
	            nodeorder ++;
	            MPNode node = m_arrLeftTop.get(k);
	            node.m_iNode_Order = nodeorder;
	        }
	    }
	    
	    
	}
}
