package com.ptg.mindmap.widget;

import android.content.Context;
import android.graphics.Color;
import android.view.Display;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ptg.mimdmap.node.MPNode;
import com.ptg.mimdmap.node.MPNodeView;
import com.ptg.mindmap.enums.MapLayoutLineType;
import com.ptg.mindmap.enums.MapLayoutShapeType;
import com.ptg.mindmap.layouts.MPEbag;
import com.ptg.mindmap.layouts.MPLayout;
import com.ptg.mindmap.xml.LoadImageFromServer;
import com.ptg.mindmap.xml.MindMapXmlHandler;
import com.ptg.mindmap.xml.xmlRequest;
import com.ptg.views.TwoDScrollView;
import com.semanoor.manahij.BookViewActivity;
import com.semanoor.manahij.BookViewReadActivity;
import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.pdfActivity;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ExecutionException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by karthik on 21-12-2015.
 */
public class LoadMindMapContent {
    Context context;
    TwoDScrollView twoDScrollView;
    ViewGroup drawingView;
    public static MPLayout mplayout;
    public static MPNodeView clickedMpnodeView;
    public static MPNode nodeProp;
    public static int centerX, centerY;
    Display display;

    public LoadMindMapContent(TwoDScrollView scrollView,ViewGroup drawView,Context context){
        this.twoDScrollView=scrollView;
        this.drawingView=drawView;
        this.context=context;
    }

    public void loadXmlData(String url, boolean ishttp) {
        try {
            // TODO Auto-generated method stub
            MPEbag ebag = new MPEbag();
            ArrayList<MPNode> childs;
            ebag = parsingSemaNodeXml(url, ishttp);
            mplayout = new MPLayout(context);
            childs = ebag.m_arrChildren;
            if (childs != null) {
                if (childs.get(0).m_arrChildren.size() > 0) {
                    sortlist(childs.get(0));
                    for (int i = 0; i < childs.get(0).m_arrChildren.size(); i++) {
                        if (i < ebag.getM_iNumberOfRightNodes()) {
                            childs.get(0).m_arrChildren.get(i).m_isRightnode = true;
                            if (childs.get(0).m_arrChildren.get(i).m_isAboveCenter)
                                ebag.m_arrRightTop.add(childs.get(0).m_arrChildren
                                        .get(i));
                            else
                                ebag.m_arrRightBottom.add(childs.get(0).m_arrChildren
                                        .get(i));
                        } else {
                            if (childs.get(0).m_arrChildren.get(i).m_isAboveCenter)
                                ebag.m_arrLeftTop.add(childs.get(0).m_arrChildren
                                        .get(i));
                            else
                                ebag.m_arrLeftBottom.add(childs.get(0).m_arrChildren
                                        .get(i));
                        }
                    }
                }
                MPNode rootnode = ebag.m_arrChildren.get(0);
                rootnode.m_arrChildren.clear();
                for (int i = ebag.m_arrRightTop.size() - 1; i >= 0; i--) {
                    rootnode.m_arrChildren.add(ebag.m_arrRightTop.get(i));
                }
                for (int i = 0; i < ebag.m_arrRightBottom.size(); i++) {
                    rootnode.m_arrChildren.add(ebag.m_arrRightBottom.get(i));
                }
                for (int i = ebag.m_arrLeftBottom.size() - 1; i >= 0; i--) {
                    rootnode.m_arrChildren.add(ebag.m_arrLeftBottom.get(i));
                }
                for (int i = 0; i < ebag.m_arrLeftTop.size(); i++) {
                    rootnode.m_arrChildren.add(ebag.m_arrLeftTop.get(i));
                }
                generateMap(ebag, childs, drawingView);
            }
        } catch (Exception e) {
            Toast.makeText(context, "invalid xml", Toast.LENGTH_LONG).show();
        }
    }

    public MPEbag parsingSemaNodeXml(String url, boolean ishttp) {


        MPEbag mpEbag = null;
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            MindMapXmlHandler saxp = new MindMapXmlHandler();
            if (ishttp) {
                String response = new xmlRequest(url).execute().get();
                if (response != null) {
                    InputSource source = new InputSource(new StringReader(response));
                    parser.parse(source, saxp);
                    mpEbag = saxp.getResults();
                } else {
                    Toast.makeText(context, "File doesn't exist", Toast.LENGTH_LONG).show();
                }
            } else {
                File file = new File(url);
                if (file.exists()) {
                    FileInputStream fis = new FileInputStream(file);
                    parser.parse(fis, saxp);
                    mpEbag = saxp.getResults();
                } else {
                    Toast.makeText(context, "File doesn't exist", Toast.LENGTH_LONG).show();
                }
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return mpEbag;
    }
    public void sortlist(MPNode node) {
        Comparator<MPNode> idComparator = new Comparator<MPNode>() {
            @Override
            public int compare(MPNode o1, MPNode o2) {
                return Integer.valueOf(o1.m_iNode_Order).compareTo(
                        o2.m_iNode_Order);
            }
        };
        Collections.sort(node.m_arrChildren, idComparator);
    }

    public void reset() {
//        drawingView.removeView(popUpLayout);
        if(context instanceof BookViewActivity){
         display =((BookViewActivity)context).getWindowManager().getDefaultDisplay();
        }else if (context instanceof BookViewReadActivity){
            display =((BookViewReadActivity)context).getWindowManager().getDefaultDisplay();
        }else if (context instanceof MindMapPlayerActivity){
            display =((MindMapPlayerActivity)context).getWindowManager().getDefaultDisplay();
        }else if (context instanceof pdfActivity){
            display =((pdfActivity)context).getWindowManager().getDefaultDisplay();
        }
        final int width = display.getWidth();
        final int height = display.getHeight();
        drawingView.removeView(ManahijApp.m_curEbag.linesLayerView);
        ManahijApp.m_curEbag = null;
        MPLayout layout = new MPLayout(context);
        drawingView.setScaleX(1f);
        drawingView.setScaleY(1f);
        TwoDScrollView.scaleFactor = 1f;
        drawingView.removeAllViewsInLayout();
        ArrayList<MPNodeView> childs = layout.getChilds(drawingView);
        for (MPNodeView child : childs) {
            drawingView.removeView(child);
        }
        twoDScrollView.post(new Runnable() {
            public void run() {
                twoDScrollView.scroll((int) (AppDict.DEFAULT_OFFSET - width) / 2,
                        (int) (AppDict.DEFAULT_OFFSET - height) / 2);
                }
            });
        }

    float fx, fy;
    MPNode mainparent;

    public void generateMap(MPEbag eBag, ArrayList<MPNode> nodes,
                            ViewGroup drawingView) {
        reset();
        int i = 1;
        mainparent = nodes.get(0);
        drawingView.removeAllViewsInLayout();
        eBag.setM_iId(i++);
        eBag.setM_parentNode(null);
        if (mainparent.backgroundColor == 0) {
            eBag.setBackgroundColor(Color.parseColor("#e3e3e3"));
            eBag.setHexaColor("#e3e3e3");
        } else {
            eBag.setBackgroundColor(mainparent.getBackgroundColor());
            eBag.setHexaColor(mainparent.getHexaColor());
        }

        // eBag.setTextColor("#000000");
        if (mainparent.m_background_image == null) {
            eBag.setM_fHeight(AppDict.mDrawBitmapHeight);
            eBag.setM_fWidth(AppDict.mDrawBitmapWidth);
        } else {
            eBag.setM_fHeight(AppDict.mDrawBackgroundBitmapHeight);
            eBag.setM_fWidth(AppDict.mDrawBackgroundBitmapWidth);
        }
        eBag.setM_iNode_TextSize(14);
        eBag.setM_strNode_Notes(mainparent.getM_strNode_Notes());
        eBag.m_iLevelId = 0;
        eBag.m_iMapLayoutShape = mainparent.m_iMapLayoutShape;

        if (mainparent.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON)
            eBag.m_fHeight = eBag.m_fWidth;
        eBag.setM_strFontName(mainparent.m_strFontName);
        eBag.setM_str_Typeface(mplayout.createTypeface(eBag, mainparent.m_strFontName));
        eBag.setTextColor(mainparent.getTextColor());
        eBag.setM_background_image(mainparent.m_background_image);
        eBag.setM_smile_image(mainparent.m_smile_image);
        eBag.setM_Attachement_Image(mainparent.m_attachement_image);
        eBag.setM_strMultimediaType(mainparent.getM_strMultimediaType());
        eBag.setM_strMultimediaUrl(mainparent.getM_strMultimediaUrl());
        eBag.embededImage = mainparent.embededImage;
        eBag.m_iGradientType = mainparent.m_iGradientType;
        eBag.m_strHyperlinkUrl = mainparent.m_strHyperlinkUrl;
        eBag.dCharacterPosition = mainparent.dCharacterPosition;
        eBag.dCharacter = mainparent.dCharacter;
        eBag.priorityCharacterPosition = mainparent.priorityCharacterPosition;
        eBag.priorityCharacter = mainparent.priorityCharacter;
        eBag.completionCharacterPosition = mainparent.completionCharacterPosition;
        eBag.completionCharacter = mainparent.completionCharacter;
        eBag.genCharacterPosition = mainparent.genCharacterPosition;
        eBag.genCharacter = mainparent.genCharacter;
        eBag.embededImage = mainparent.embededImage;
        eBag.m_background_image = mainparent.m_background_image;
        eBag.m_attachement_image = mainparent.m_attachement_image;
        eBag.setM_strNode_text(mainparent.getM_strNode_text());
        eBag.m_start_date = mainparent.m_start_date;
        eBag.m_end_date = mainparent.m_end_date;
        eBag.m_duration = mainparent.m_duration;
        eBag.m_duration_type = mainparent.m_duration_type;
        eBag.mediaType = mainparent.mediaType;
        eBag.m_resource = mainparent.m_resource;
        eBag.imageUrl = mainparent.imageUrl;
        eBag.m_strMultimediaUrl = mainparent.m_strMultimediaUrl;
        eBag.imageAttachementype = mainparent.imageAttachementype;
        eBag.mediaType = mainparent.mediaType;
        fx = AppDict.DEFAULT_OFFSET * TwoDScrollView.scaleFactor / 2
                - mainparent.m_fX;
        fy = AppDict.DEFAULT_OFFSET * TwoDScrollView.scaleFactor / 2
                - mainparent.m_fY;
        eBag.setM_fX((int) ((AppDict.DEFAULT_OFFSET
                * TwoDScrollView.scaleFactor / 2) /*- 100*/));

        eBag.setM_fY((int) ((AppDict.DEFAULT_OFFSET
                * TwoDScrollView.scaleFactor / 2)/* - 100 */));
        mainparent
                .setM_fX((AppDict.DEFAULT_OFFSET * TwoDScrollView.scaleFactor / 2));
        mainparent
                .setM_fY((AppDict.DEFAULT_OFFSET * TwoDScrollView.scaleFactor / 2));
        MPNodeView nodeView = new MPNodeView(context, eBag);

        nodeView.setId(eBag.getM_iId());
        if (eBag.m_background_image != null)
            // nodeView.bringToFront();
            eBag.setM_shape(nodeView);
        clickedMpnodeView = nodeView;
        //
        // mplayout.addLinelayer(drawingView);
        //drawingView.setBackgroundColor(Color.parseColor(eBag.m_strMapLayoutFillColor));
        centerX = 800 * 7 / 2;
        centerY = 1232 * 7 / 2;
        ManahijApp.m_curEbag = eBag;
        ManahijApp.m_curEbag.m_iMapLayoutLine = MapLayoutLineType.MPLayout_Line_Curved;
        drawingView.addView(nodeView, AppDict.mDrawBitmapWidth,

                AppDict.mDrawBitmapHeight);

        nodeView.calculateTextWidth();
//        nodeView.setOnTouchListener(myGestureListener);
        clickedMpnodeView = nodeView;
        nodeProp = clickedMpnodeView.m_node;
        MPLayout layout = new MPLayout(context);
        drawingView.removeAllViewsInLayout();
        drawingView.addView(nodeView, AppDict.mDrawBitmapWidth,
                AppDict.mDrawBitmapHeight);
        eBag.setM_arrChildren(mainparent.getM_arrChildren());
        mainparent = eBag;

        if (mainparent.getM_arrChildren() != null
                && mainparent.getM_arrChildren().size() > 0) {
            for (MPNode nodesp : mainparent.getM_arrChildren()) {
                nodesp.m_parentNode = mainparent;
            }
            createNodeView(mainparent.getM_arrChildren(), drawingView);
        }

        mplayout.setLayout(ManahijApp.m_curEbag);
        layout.setLayout(ManahijApp.m_curEbag);

    }
    public void createNodeView(ArrayList<MPNode> parentNode,
                               ViewGroup drawingView) {

        for (int i = 0; i < parentNode.size(); i++) {
            MPNode newnode = parentNode.get(i);

            newnode.m_parentNode.m_shape.bringToFront();

            // newnode.textColor = newnode.m_parentNode.textColor;
            newnode.m_iLevelId = newnode.m_parentNode.m_iLevelId + 1;
            if (newnode.m_background_image == null) {
                newnode.setM_fHeight(AppDict.mDrawBitmapHeight);
                newnode.setM_fWidth(AppDict.mDrawBitmapWidth);
            } else {
                newnode.setM_fHeight(AppDict.mDrawBackgroundBitmapHeight);
                newnode.setM_fWidth(AppDict.mDrawBackgroundBitmapWidth);
            }

            if (newnode.backgroundColor == 0) {
                newnode.backgroundColor = newnode.m_parentNode.backgroundColor;
                newnode.hexaColor = newnode.m_parentNode.hexaColor;
            }


			/*
             * }else{ newnode.m_fX=fx-newnode.m_fX;
			 * newnode.m_fY=newnode.m_fY+fy; }
			 */


            newnode.m_iNode_TextSize = newnode.m_parentNode.m_iNode_TextSize;
            //newnode.m_str_Typeface = newnode.m_parentNode.m_str_Typeface;
            newnode.m_strNode_Notes = newnode.m_strNode_Notes;
            //newnode.m_strFontName = newnode.m_parentNode.m_strFontName;
            newnode.m_str_Typeface = mplayout.createTypeface(newnode, newnode.m_strFontName);
            if (newnode.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON)
                newnode.m_fHeight = newnode.m_fWidth;
            // newnode.m_iMapLayoutShape =
            // MapLayoutShapeType.MPLAYOUT_SHAPE_ROUNDEDRECTANGLE;
            MPNodeView newShape = new MPNodeView(context, newnode);
            if (newnode.m_background_image != null)
                newShape.setBackgroundColor(Color.TRANSPARENT);
//            newShape.setOnTouchListener(myGestureListener);

			/*
             * drawingView.getLayoutParams().width =
			 * drawingView.getLayoutParams().width + 200;
			 * drawingView.getLayoutParams().height = drawingView
			 * .getLayoutParams().height + 200;
			 */
            if (newnode.m_parentNode.m_iLevelId != 0) {

                newnode.m_isRightnode = newnode.m_parentNode.m_isRightnode;
                newnode.m_isAboveCenter = newnode.m_parentNode.m_isAboveCenter;
            }

            newnode.m_fX += fx;
            newnode.m_fY += fy;
            newnode.m_fY += (newnode.m_fY - mainparent.m_fY) * 1.3f;
            newnode.m_fX += (newnode.m_fX - mainparent.m_fX) * 1.3f;
            newShape.m_node = newnode;
            newnode.m_shape = newShape;
            if (!newShape.m_node.imageUrl.equals("")) {
                LoadImageFromServer loadImageFromServer = new LoadImageFromServer(
                        context, newShape,
                        newShape.m_node.imageAttachementype);
                loadImageFromServer.execute();
                // loadImageFromServer.loadIamgee(newShape,
                // newShape.m_node.imageAttachementype);
            }
            newShape.calculateTextWidth();
            newShape.backgroundColor = newnode.m_parentNode.backgroundColor;
            newnode.m_selectedColor = newnode.m_parentNode.m_selectedColor;

            drawingView.addView(newShape, AppDict.mDrawBitmapWidth,
                    AppDict.mDrawBitmapHeight);


            if (newnode.getM_arrChildren() != null
                    && newnode.getM_arrChildren().size() > 0) {
                createNodeView(newnode.getM_arrChildren(), drawingView);
            }
        }
    }
}
