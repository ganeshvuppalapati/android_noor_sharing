package com.ptg.mindmap.layouts;

import android.content.Context;

import com.ptg.mimdmap.node.MPNode;
import com.ptg.mimdmap.node.MPNodeView;
import com.semanoor.manahij.ManahijApp;

import java.util.Collections;

public class MPRightLayout extends MPLayout {
	Context rContext;

	public MPRightLayout(Context context) {
		super(context);
		this.rContext = context;
	}

	public void setLayout(MPNode node) {
		MPEbag curEbag = ManahijApp.m_curEbag;
		MPNodeView shape = curEbag.m_shape;

		getChildrenHeight(curEbag, curEbag.m_arrChildren);
		updateNodePositions(curEbag);
		//shape.setNodeText(curEbag.m_strNode_text);
		setPositionsRightMap(curEbag, curEbag.m_arrRightTop, (curEbag.m_fY
				- curEbag.m_frtHeight - 40), false);
		setPositionsRightMap(curEbag, curEbag.m_arrRightBottom,
				(curEbag.m_fY + 40 + curEbag.m_fHeight), false);
		setPositionsLeftMap(curEbag, curEbag.m_arrLeftTop, (curEbag.m_fY
				- curEbag.m_fltHeight - 40), false);
		setPositionsLeftMap(curEbag, curEbag.m_arrLeftBottom,
				(curEbag.m_fY + 40 + curEbag.m_fHeight), false);
		getCurrentDraggingNodeLine();

	}
	
	public  void updateNodePositions(MPEbag ebag)
	{
	    super.updateNodePositions(ebag);
	    if(ebag.m_arrChildren!=null){
	    int ct =  ebag.m_arrChildren.size()-1;
	    
	    while( ct >= 0){
	        
	        MPNode curNode = ebag.m_arrChildren.get(ct);
	        
	        //if (curNode.m_isRightnode){
	        if(curNode.m_isAboveCenter) {
	            ebag.m_frtHeight += curNode.m_childrenHeight;
	            ebag.m_arrRightTop.add(curNode);
	           // curNode.m_strLocation = [NSString stringWithFormat:@"%d",ct];
	        }else{
	            ebag.m_arrRightBottom.add(curNode);
	            ebag.m_frbHeight += curNode.m_childrenHeight;
	           // curNode.m_strLocation = [NSString stringWithFormat:@"%d",ct];
	        }
	        
	      
	        //curNode.m_shape.invalidate();
	        ct--;
	    }
	    
	    Collections.reverse(ebag.m_arrRightBottom);
		Collections.reverse(ebag.m_arrLeftBottom);
	    
	    
	}
	}
}
