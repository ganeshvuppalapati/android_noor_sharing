package com.ptg.mindmap.xml;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.android.volley.toolbox.ImageLoader;
import com.ptg.mimdmap.node.MPNodeView;
import com.ptg.mindmap.widget.MindMapActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class LoadImageFromServer  extends AsyncTask<Void, Void, Bitmap>{
	MPNodeView mpNodeView;
	URL url;
	int imageType=-1;
	 ImageLoader imageLoader;
	 Bitmap myBitmap;
	 Context mContext;
	/*public LoadImageFromServer(MPNodeView mpNodeView,int imageAttachementype){
		this.mpNodeView=mpNodeView;
		this.imageType=imageAttachementype;
		
	}
	public LoadImageFromServer(){
		
	}
	public void  loadIamgee(final MPNodeView mpNodeView,final int imageAttachementype){
		  imageLoader = MindMapApplication.getInstance().getImageLoader();
		  String uriPath=mpNodeView.m_node.imageUrl.trim().replace(" ","");
		// If you are using normal ImageView
		imageLoader.get(uriPath, new ImageListener() {
		 
		    @Override
		    public void onErrorResponse(VolleyError error) {
		        Log.e("", "Image Load Error: " + error.getMessage());
		    }
		 
		    @Override
		    public void onResponse(ImageContainer response, boolean arg1) {
		        if (response.getBitmap() != null) {
		            // load image into imageview
		          //  imageView.setImageBitmap(response.getBitmap());
		            if(imageAttachementype==1){
		            	mpNodeView.m_node.embededImage=myBitmap;
		    			}
		    			else if(imageAttachementype==3){
		    				mpNodeView.m_node.m_background_image=myBitmap;
		    			}
		    			else{
		    				mpNodeView.m_node.m_attachement_image=myBitmap;
		    			}
		    		mpNodeView.calculateTextWidth();
		    		MindMapActivity.mplayout.setLayout(mpNodeView.m_node);
		        }
		    }
		});
	}*/
	
	public LoadImageFromServer(Context context,MPNodeView mpNodeView,int imageAttachementype){
		this.mpNodeView=mpNodeView;
		this.imageType=imageAttachementype;
		this.mContext=context;
		
	}
	@Override
	protected Bitmap doInBackground(Void... params) {
		try {
			String uriPath=mpNodeView.m_node.imageUrl.trim().replace(" ","");
			url = new URL(uriPath);
		
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
         myBitmap = BitmapFactory.decodeStream(input);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			 myBitmap=null;
			e.printStackTrace();
		}
		catch (IOException e) {
            e.printStackTrace();
            myBitmap=null;
            return null;
        }
		return myBitmap;
	}
	
	@Override
	protected void onPostExecute(Bitmap result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(this.imageType==1){
			if(myBitmap!=null)
			myBitmap=Bitmap.createScaledBitmap(myBitmap,mpNodeView.m_node.bitmapWidth , mpNodeView.m_node.bitmapHeight, true);
			mpNodeView.m_node.embededImage=myBitmap;
			}
			else if(this.imageType==3){
				if(myBitmap!=null)
				myBitmap=Bitmap.createScaledBitmap(myBitmap,mpNodeView.m_node.bitmapWidth , mpNodeView.m_node.bitmapHeight, true);
				mpNodeView.m_node.m_background_image=myBitmap;
			}
			else{
				mpNodeView.m_node.m_attachement_image=myBitmap;
			}
		mpNodeView.calculateTextWidth();
		mpNodeView.invalidate();
		MindMapActivity.mplayout.setLayout(mpNodeView.m_node);
	}

}
