package com.ptg.mindmap.layouts;

import android.content.Context;
import android.graphics.RectF;

import com.ptg.mimdmap.node.MPNode;
import com.ptg.mimdmap.node.MPNodeView;
import com.semanoor.manahij.ManahijApp;

import java.util.Collections;

public class MPCircularLayout extends MPLayout{
Context cir_Context;
	public MPCircularLayout(Context context) {
		super(context);
		this.cir_Context=context;
	}
public void setLayout(MPNode node){
		MPEbag curEbag = ManahijApp.m_curEbag;
		MPNodeView shape = curEbag.m_shape;
		getChildrenHeight(curEbag, curEbag.m_arrChildren);
		updateNodePositions(curEbag);
		// setNodeText(curEbag.m_strNode_text);
		setPositionsRightMap(curEbag, curEbag.m_arrRightTop, (curEbag.m_fY
				- curEbag.m_frtHeight - 40), false);
		setPositionsRightMap(curEbag, curEbag.m_arrRightBottom,
				(curEbag.m_fY + 40 + curEbag.m_fHeight), false);
		setPositionsLeftMap(curEbag, curEbag.m_arrLeftTop, (curEbag.m_fY
				- curEbag.m_fltHeight - 40), false);
		setPositionsLeftMap(curEbag, curEbag.m_arrLeftBottom,
				(curEbag.m_fY + 40 + curEbag.m_fHeight), false);
		getCurrentDraggingNodeLine();
//tempUpdateChildrenColor(node.m_parentNode.m_shape, node.m_parentNode.backgroundColor, node.m_parentNode.hexaColor);
		// [self getChildrenHeight:curEbag ChildNodes:curEbag.m_arrChildren];
	}
public void updateNodePositions(MPEbag ebag) {
	
	super.updateNodePositions(ebag);
	int ct=0;
	if(ebag.m_arrChildren!=null&&ebag.m_arrChildren.size()>0){
	if(ebag.m_arrChildren!=null&&ebag.m_arrChildren.size()>0)
	 ct = ebag.m_arrChildren.size() - 1;

	while (ct >= 0) {

		MPNode curNode = ebag.m_arrChildren.get(ct);

		if (curNode.m_isRightnode) {
			if (curNode.m_isAboveCenter == true) {
				ebag.m_frtHeight += curNode.m_childrenHeight;
				ebag.m_arrRightTop.add(curNode);
				// curNode.m_strLocation = [NSString
				// stringWithFormat:@"%d",ct];
			} else {
				ebag.m_arrRightBottom.add(curNode);
				ebag.m_frbHeight += curNode.m_childrenHeight;
				// curNode.m_strLocation = [NSString
				// stringWithFormat:@"%d",ct];
			}

		} else {

			if (curNode.m_isAboveCenter == true) {
				ebag.m_fltHeight += curNode.m_childrenHeight;
				ebag.m_arrLeftTop.add(curNode);
				// curNode.m_strLocation = [NSString
				// stringWithFormat:@"%d",ct];
			} else {

				ebag.m_flbHeight += curNode.m_childrenHeight;
				ebag.m_arrLeftBottom.add(curNode);
				// ebag.m_strLocation = [NSString
				// stringWithFormat:@"%d",ct];

			}

		}

		ct--;
	}

	Collections.reverse(ebag.m_arrRightBottom);
	Collections.reverse(ebag.m_arrLeftBottom);
	// getMapView(ebag.m_shape).invalidate();
	}
}
public void setPositions(MPNode node)
{
    MPNodeView shape = node.m_shape;
    shape.rect = new RectF(node.m_fX,node.m_fY, node.m_fWidth,node.m_fHeight);
  //  shape.setNodeText(node.m_strNode_text);
    for (MPNode node1 : node.m_arrChildren) {
        setPositions(node1);
    }
}
}
