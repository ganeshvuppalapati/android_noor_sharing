package com.ptg.mindmap.widget;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;

import com.semanoor.manahij.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class AppDict {
	
	public static int[] colors=new int[]{Color.parseColor("#C4DF9B"),Color.parseColor("#FDC689"),Color.parseColor("#F5989D"),Color.parseColor("#A864A8"),Color.parseColor("#1CBBB4"),Color.parseColor("#FFF568"),Color.parseColor("#9BCEDF"),Color.parseColor("#89FDB4"),Color.parseColor("#ABF598"),Color.parseColor("#A89E64"),Color.parseColor("#9C1CBB"),Color.parseColor("#68FFDE"),Color.parseColor("#B6FD89"),Color.parseColor("#98B6F5")};
	//public static int[] icons=new int[]{R.drawable.mi_emoticon_01,R.drawable.mi_emoticon_02,R.drawable.mi_emoticon_03,R.drawable.mi_emoticon_04,R.drawable.mi_emoticon_05,R.drawable.mi_emoticon_06,R.drawable.mi_emoticon_07,R.drawable.mi_emoticon_08,R.drawable.mi_emoticon_09,R.drawable.mi_emoticon_10,R.drawable.mi_emoticon_11,R.drawable.mi_emoticon_12};
public static int[] icons=new int[]{R.drawable.mi_emoticon_01, R.drawable.mi_emoticon_02, R.drawable.mi_emoticon_03, R.drawable.mi_emoticon_04, R.drawable.mi_emoticon_05, R.drawable.mi_emoticon_06, R.drawable.mi_emoticon_07, R.drawable.mi_emoticon_08, R.drawable.mi_emoticon_09, R.drawable.mi_emoticon_10, R.drawable.mi_emoticon_11, R.drawable.mi_emoticon_12 , R.drawable.mi_smile13 , R.drawable.mi_smile14 , R.drawable.mi_smile15 , R.drawable.mi_smile16 , R.drawable.mi_smile17 , R.drawable.mi_smile18};
	
	public static int[] genicons=new int[]{R.drawable.mi_gena, R.drawable.mi_genb, R.drawable.mi_genc, R.drawable.mi_gend, R.drawable.mi_gene, R.drawable.mi_genf, R.drawable.mi_geng, R.drawable.mi_genh, R.drawable.mi_geni, R.drawable.mi_genj, R.drawable.mi_genk, R.drawable.mi_genl , R.drawable.mi_genm , R.drawable.mi_genn , R.drawable.mi_geno , R.drawable.mi_genp };
	
	public static int[] tdicons=new int[]{R.drawable.mi_cliparts1, R.drawable.mi_cliparts2, R.drawable.mi_cliparts3, R.drawable.mi_cliparts4, R.drawable.mi_cliparts5, R.drawable.mi_cliparts6, R.drawable.mi_cliparts7, R.drawable.mi_cliparts8, R.drawable.mi_cliparts9, R.drawable.mi_cliparts10, R.drawable.mi_cliparts11, R.drawable.mi_cliparts12 , R.drawable.mi_cliparts13 , R.drawable.mi_cliparts14 , R.drawable.mi_cliparts15 , R.drawable.mi_cliparts16 , R.drawable.mi_cliparts17 , R. drawable.mi_cliparts18};
		
	
	/*@"#C4DF9B",@"#FDC689",@"#F5989D",@"#A864A8",@"#1CBBB4",@"#FFF568",@"#9BCEDF",@"#89FDB4",@"#ABF598",@"#A89E64",@"#9C1CBB",@"#68FFDE",@"#B6FD89",@"#98B6F5"*/
	public static String[] colorCodes=new String[]{"#C4DF9B","#FDC689","#F5989D","#A864A8","#1CBBB4","#FFF568","#9BCEDF","#89FDB4","#ABF598","#A89E64","#9C1CBB","#68FFDE","#B6FD89","#98B6F5"};
	public  static int mDrawBitmapWidth = 200; // !< Default width of view
	public static int mDrawBitmapHeight = 80;
	public static int mDrawBackgroundBitmapWidth = 280; // !< Default width of view
	public static int mDrawBackgroundBitmapHeight = 100;
	public static int DEFAULT_OFFSET=500000;
	public static ArrayList<String> fontsizes=new ArrayList<String>(Arrays.asList("8", "9","10","11","12","13","14","16","18","20","22","24","26","28","36","48","50","52","54","56","58","60","62","64","66","68","70","72"));
	public static int[] priority=
			new int[]{R.drawable.mi_prior1, R.drawable.mi_prior2, R.drawable.mi_prior3, R.drawable.mi_prior4, R.drawable.mi_prior5, R.drawable.mi_prior6, R.drawable.mi_prior7, R.drawable.mi_prior8, R.drawable.mi_prior9};
	
	public static int[] completion=new int[]{R.drawable.mi_completion1, R.drawable.mi_completion2, R.drawable.mi_completion3 , R.drawable.mi_completion4 , R.drawable.mi_completion5 , R.drawable.mi_completion6 , R.drawable.mi_completion7 , R.drawable.mi_completion8 , R.drawable.mi_completion9 , R.drawable.mi_completion10};
	public static	GradientDrawable.Orientation[] gradientPositions = {  GradientDrawable.Orientation.TOP_BOTTOM,
			GradientDrawable.Orientation.BOTTOM_TOP , 
			GradientDrawable.Orientation.RIGHT_LEFT , 
			GradientDrawable.Orientation.LEFT_RIGHT,
			GradientDrawable.Orientation.TL_BR,
			GradientDrawable.Orientation.BL_TR,
			GradientDrawable.Orientation.TR_BL,
			GradientDrawable.Orientation.BR_TL, GradientDrawable.Orientation.TOP_BOTTOM,
			GradientDrawable.Orientation.BOTTOM_TOP , 
			GradientDrawable.Orientation.RIGHT_LEFT , 
			GradientDrawable.Orientation.LEFT_RIGHT,
			GradientDrawable.Orientation.TL_BR,
			GradientDrawable.Orientation.BL_TR,
			GradientDrawable.Orientation.TR_BL,
			GradientDrawable.Orientation.BR_TL,
			 };

	public static ArrayList<String> items = new ArrayList<String>(Arrays.asList("Minutes(s)","Days(s)","Month(s)","Week(s)","Hour(s)"));
	public static int getPixels(int unit, float size) {
	    DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
	    return (int)TypedValue.applyDimension(unit, size, metrics);
	}
	
	public static  Bitmap getOvalImage(Drawable bitmapDrawable) {
        Bitmap bitmap = ((BitmapDrawable)bitmapDrawable).getBitmap();
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = 100;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        Drawable image = new BitmapDrawable(output);
        return output;

    }
	public  static Bitmap getHexagonShape(Bitmap scaleBitmapImage) {
	    // TODO Auto-generated method stub
	    int targetWidth = 200;
	    int targetHeight =50;
	    Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, 
	            targetHeight, Config.ARGB_8888);

	    Canvas canvas = new Canvas(targetBitmap);

	    Path path = new Path();
	    float stdW = 200;
	    float stdH = 200;
	    float w3 =stdW / 2;
	    float h2 = stdH / 2;


	    float radius=stdH/2-10;
	    float triangleHeight = (float) (Math.sqrt(3) * radius / 2);
	      float centerX = stdW/2;
	      float centerY = stdH/2;
	      path.moveTo(centerX, centerY + radius);
	      path.lineTo(centerX - triangleHeight, centerY + radius/2);
	      path.lineTo(centerX - triangleHeight, centerY - radius/2);
	      path.lineTo(centerX, centerY - radius);
	      path.lineTo(centerX + triangleHeight, centerY - radius/2);
	      path.lineTo(centerX + triangleHeight, centerY + radius/2);
	      path.moveTo(centerX, centerY + radius);


	    canvas.clipPath(path);
	    Bitmap sourceBitmap = scaleBitmapImage;
	    canvas.drawBitmap(sourceBitmap, 
	            new Rect(0, 0, sourceBitmap.getWidth(),
	                    sourceBitmap.getHeight()), 
	                    new Rect(0, 0, targetWidth,
	                            targetHeight), null);
	    return targetBitmap;
	}
	public static String encodeTobase64(Bitmap image)
	{
	    Bitmap immagex=image;
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
	    immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
	    byte[] b = baos.toByteArray();
	    String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

	    Log.e("LOOK", imageEncoded);
	    return imageEncoded;
	}
	public static Bitmap decodeBase64(String input) 
	{
	    byte[] decodedByte = Base64.decode(input, 0);
	    return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	}
	
	
}
