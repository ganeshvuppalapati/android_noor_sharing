package com.ptg.mindmap.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptg.gesturedetectors.MoveGestureDetector;
import com.ptg.mimdmap.node.MPNode;
import com.ptg.mimdmap.node.MPNodeView;
import com.ptg.mindmap.enums.MapState;
import com.ptg.mindmap.layouts.MPLayout;
import com.ptg.views.CircleButton;
import com.ptg.views.TwoDScrollView;
import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.Globals;

import java.io.File;
import java.util.ArrayList;


public class MindMapPlayerActivity extends Activity implements OnClickListener {
    private static final int RESULT_LOAD_IMAGE = 0;
    public static  RelativeLayout canvasView;
    public static ViewGroup drawingView;
    ArrayList<String> fontsList;
    public static Bitmap BitmapDummyImage = null;
    // RelativeLayout removeSmile;
//    public static RelativeLayout LinearLayout01;
    public static int width = 0, height = 0;
    public static MPLayout mplayout;
    // public static HScroll hScroll;
    // public static VScroll vScroll;
    public static TwoDScrollView twoDScroll;
    // TreeView treeView;
    public static int centerX, centerY;
    boolean focusAddtext;
    public static DisplayMetrics displayMetrics;
    public static int densityDpi;
    private MoveGestureDetector mMoveDetector;
    private TextView txt_Count;
    private Book book;
    private ArrayList<String> miArrayList;
    private String filepath;
    //	public static CustomDesignScrollView designScrollPageView;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mind_map_player);

        RelativeLayout lay = (RelativeLayout)findViewById(R.id.lay);
        lay.bringToFront();

        displayMetrics = getResources().getDisplayMetrics();
        densityDpi = displayMetrics.densityDpi;
        BitmapDummyImage = BitmapFactory.decodeResource(getResources(),R.drawable.mi_dummy_image);
        book = (Book) getIntent().getSerializableExtra("book");
        filepath= Globals.TARGET_BASE_BOOKS_DIR_PATH+book.getBookID()+"/mindMaps/";
        File file=new File(filepath);
        String[] fileList=file.list();
        miArrayList=new ArrayList<String>();
        for (int i=0;i<fileList.length;i++){
            if (fileList[i].contains(".xml")){
                miArrayList.add(fileList[i]);
            }
        }
        if (miArrayList.size()>1){
            RelativeLayout mi_layout= (RelativeLayout) findViewById(R.id.nav_layout);
            mi_layout.setVisibility(View.VISIBLE);
            txt_Count= (TextView) findViewById(R.id.mi_count);
            txt_Count.setText(1+"/"+miArrayList.size());
        }
        initializeLayout();
        mMoveDetector = new MoveGestureDetector(getApplicationContext(),
                (MoveGestureDetector.OnMoveGestureListener) new MoveListener());
    }

    InputMethodManager inputMethodManager;
    InputMethodSubtype inputMethodSubtype;

    public void initializeLayout() {
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodSubtype = inputMethodManager.getCurrentInputMethodSubtype();
        CircleButton prevBtn= (CircleButton) findViewById(R.id.prev_btn);
        prevBtn.setOnClickListener(this);
        CircleButton nxtBtn= (CircleButton) findViewById(R.id.nxt_btn);
        nxtBtn.setOnClickListener(this);
        TextView txt_title= (TextView) findViewById(R.id.titleText);
        txt_title.setText(book.getBookTitle());
//		designScrollPageView = (CustomDesignScrollView) findViewById(R.id.designScrollView);
       // Button btnBack = (Button) findViewById(R.id.btnBack);
        CircleButton btnBack = (CircleButton) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        twoDScroll = (TwoDScrollView) findViewById(R.id.hScroll);
//        LinearLayout01 = (RelativeLayout) findViewById(R.id.LinearLayout01);
        canvasView = (RelativeLayout) findViewById(R.id.canvasView);
        drawingView = (ViewGroup) findViewById(R.id.drawingView);
        fontsList = readAllFonts();
        mplayout = new MPLayout(MindMapPlayerActivity.this);
        myGestureListener = new MyGestureListener(this);
        //setSelectedView(viewEdit);
        Display mDisplay = getWindowManager().getDefaultDisplay();
        width = mDisplay.getWidth();
        height = mDisplay.getHeight();
        drawingView.getLayoutParams().width = AppDict.DEFAULT_OFFSET;
        drawingView.getLayoutParams().height = AppDict.DEFAULT_OFFSET;
        fontsList = readAllFonts();
//		Bundle extras = getIntent().getExtras();
//		//rootNode();
//		String path=extras.getString("PATH");
//		boolean isHttp=extras.getBoolean("IS_HTTP");;
        //loadXmlData(Environment.getExternalStorageDirectory()+ "/MindMap/SemaNodes.xml",false);
        LoadMindMapContent mindMapContent = new LoadMindMapContent(twoDScroll, drawingView, MindMapPlayerActivity.this);
        mindMapContent.loadXmlData(filepath+miArrayList.get(0), false);
//        loadXmlData(filepath+miArrayList.get(0),false);
        Rect rectf = new Rect();
        drawingView.getLocalVisibleRect(rectf);
		twoDScroll.post(new Runnable() {
			public void run() {
				twoDScroll.scrollTo((int) (AppDict.DEFAULT_OFFSET - canvasView.getWidth()+120) / 2,
						(int) (AppDict.DEFAULT_OFFSET - canvasView.getHeight()) / 2);
			}
		});
    }
    int i = 0;
    public static MPNodeView clickedMpnodeView;
    public static MPNode nodeProp;
    //boolean nodIsClicked;
    boolean cutNode = false;
    public void reset() {
        drawingView.removeView(popUpLayout);
        popUpLayout = null;
        drawingView.removeView(ManahijApp.m_curEbag.linesLayerView);
        ManahijApp.m_curEbag = null;
        MPLayout layout = new MPLayout(MindMapPlayerActivity.this);
        drawingView.setScaleX(1f);
        drawingView.setScaleY(1f);
        TwoDScrollView.scaleFactor = 1f;
        drawingView.removeAllViewsInLayout();
        ArrayList<MPNodeView> childs = layout.getChilds(drawingView);
        for (MPNodeView child : childs) {
            drawingView.removeView(child);
        }
        twoDScroll.post(new Runnable() {
            public void run() {

                twoDScroll.scrollTo((int) (AppDict.DEFAULT_OFFSET - width) / 2,
                        (int) (AppDict.DEFAULT_OFFSET - height) / 2);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prev_btn:
                if (!txt_Count.getText().equals("1/"+miArrayList.size())) {
                    String txtCount= String.valueOf(txt_Count.getText());
                    String[] count=txtCount.split("/");
                    int mapPosition= Integer.parseInt(count[0]);
                    LoadMindMapContent mindMapContent = new LoadMindMapContent(twoDScroll, drawingView, MindMapPlayerActivity.this);
                    mindMapContent.loadXmlData(Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID() + "/mindMaps/" + miArrayList.get(mapPosition-2), false);
                    txt_Count.setText(mapPosition-1+"/"+miArrayList.size());
                }
                break;

            case R.id.nxt_btn:
                if (!txt_Count.getText().equals(miArrayList.size()+"/"+miArrayList.size())) {
                    String txtCount= String.valueOf(txt_Count.getText());
                    String[] count=txtCount.split("/");
                    int mapPosition= Integer.parseInt(count[0]);
                    LoadMindMapContent mindMapContent = new LoadMindMapContent(twoDScroll, drawingView, MindMapPlayerActivity.this);
                    mindMapContent.loadXmlData(Globals.TARGET_BASE_BOOKS_DIR_PATH + book.getBookID() + "/mindMaps/" + miArrayList.get(mapPosition), false);
                    txt_Count.setText(mapPosition+1+"/"+miArrayList.size());
                }
                break;
            case R.id.btnBack:
                 finish();
                break;
        }

    }


    int _xDelta = 0, _yDelta = 0;


    TextView imagePanel, audioPanel, videoPanel,
            textPanel;
    public int REQUEST_TYPE = 0;


    public static void hideMenuItem() {
        if (popUpLayout != null && popUpLayout.getVisibility() == View.VISIBLE) {
            // animate(true);
            // animateRadialMenu(false);
            popUpLayout.setVisibility(View.GONE);
        }
    }

    public static View popUpLayout;

    // pop up view


    private ArrayList<String> readAllFonts() {

        ArrayList<String> fontNames = new ArrayList<String>();

        File temp = new File("/system/fonts/");

        String fontSuffix = ".ttf";

        for (File font : temp.listFiles()) {

            String fontName = font.getName();

            if (fontName.endsWith(fontSuffix)) {

                fontNames.add(fontName.subSequence(0,
                        fontName.lastIndexOf(fontSuffix)).toString());

            }

        }

        return fontNames;

    }


    public MyGestureListener myGestureListener;
    public boolean singleTap = false;
    int X = 0, Y = 0;


    private class MoveListener extends
            MoveGestureDetector.SimpleOnMoveGestureListener {

        @Override
        public boolean onMoveBegin(MoveGestureDetector detector) {
            // mPUndoManager.addUndooperationOnNode(nodeProp,
            // UndoOperationKey.UndoOperationKey_changeNodePosition);
            Log.d("", "onMoveBegin ");
            ManahijApp.m_curEbag.m_impstate = MapState.MPStateBeginDragDrop;
            // }
            return super.onMoveBegin(detector);
        }

        @Override
        public void onMoveEnd(MoveGestureDetector detector) {
            if (nodeProp.m_parentNode != null) {
                ManahijApp.m_curEbag.m_impstate = MapState.MPStateEndDragDrop;
                ManahijApp.m_curEbag.m_currNodeBeingDragged = null;
                mplayout.changeNodePositionOf(nodeProp);
                nodeProp.m_isCurrShapeBeingDragged = false;
                mplayout.setLayout(nodeProp);

                ManahijApp.m_curEbag.m_impstate = MapState.MPStateNormal;
            }
            super.onMoveEnd(detector);

        }

        @Override
        public boolean onMove(MoveGestureDetector detector) {
            if (clickedMpnodeView != null) {
                ManahijApp.m_curEbag.m_impstate = MapState.MPStateBeingDragging;
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) clickedMpnodeView
                        .getLayoutParams();
                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                layoutParams.rightMargin = -250;
                layoutParams.bottomMargin = -250;
                if (nodeProp.m_iLevelId != 0) {
                    // isMoved = true;
                    nodeProp.m_isCurrShapeBeingDragged = true;
                    nodeProp.m_fX = (X - _xDelta);
                    nodeProp.m_fY = (Y - _yDelta);
                    // nodeProp.isHilight = true;
                    ManahijApp.m_curEbag.m_currNodeBeingDragged = nodeProp;
                    mplayout.setLayout(nodeProp);
                }
            }

            // mFocusX = detector.getFocusX();
            // mFocusY = detector.getFocusY();
            Log.d("", "onMove ");

            return true;
        }
    }

    public class MyGestureListener extends SimpleOnGestureListener implements
            OnTouchListener {
        Context context;
        MPNodeView view;
        GestureDetector gDetector;

        public MyGestureListener() {
            super();
        }

        public MyGestureListener(Context context) {
            this(context, null);
        }

        public MyGestureListener(Context context, GestureDetector gDetector) {

            if (gDetector == null)
                gDetector = new GestureDetector(context, this);

            this.context = context;
            this.gDetector = gDetector;
            gDetector.setOnDoubleTapListener(new OnDoubleTapListener() {

                @Override
                public boolean onSingleTapConfirmed(MotionEvent event) {
                    singleTap = true;
//					showOptionPopup(event);

                    return true;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    Log.d("", "" + "on onDoubleTapEvent Tap");

                    return false;
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    return false;
                }
            });
        }


        @Override
        public boolean onTouch(View v, MotionEvent event) {
            boolean isMoved = false;
            gDetector.onTouchEvent(event);
/*				if (nodeProp != null && nodeProp.m_iLevelId != 0)
			if (ManahijApp.m_curEbag.m_currNodeBeingDragged == null|| ManahijApp.m_curEbag.m_currNodeBeingDragged == nodeProp)
					mMoveDetector.onTouchEvent(event);*/
            // mScaleDetector.onTouchEvent(event);
            hideMenuItem();
			/* final int */X = (int) (event.getRawX() / TwoDScrollView.scaleFactor);
			/* final int */Y = (int) (event.getRawY() / TwoDScrollView.scaleFactor);
            view = (MPNodeView) v;
            Point point = new Point();
            point.x = (int) event.getX();
            point.y = (int) event.getY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    if (nodeProp != null)
                        nodeProp.selected = false;
                    if (view.getRect().contains((int) point.x, (int) point.y)) {
                        if (nodeProp != null)
                            nodeProp.selected = false;
                        clickedMpnodeView = view;
                        if (ManahijApp.m_curEbag.m_impstate == MapState.MPStateNormal) {
                            nodeProp = view.getm_mpNode();
                        }
                    }
                    RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) v
                            .getLayoutParams();
                    _xDelta = (int) (X - nodeProp.getM_fX());
                    _yDelta = (int) (Y - nodeProp.getM_fY());
                    // break;
                    if (drawingView.getParent() != null) {
                        ((ViewParent) drawingView.getParent())
                                .requestDisallowInterceptTouchEvent(true);
                    }
                    // twoDScroll.setScrollingEnabled(false);

                    // hScroll.setScrollingEnabled(false);
                    return true;
                case MotionEvent.ACTION_UP:

                    if (view.getRect().contains((int) point.x, (int) point.y))
                        if (nodeProp.m_lp != null)

                            if (drawingView.getParent() != null) {
                                ((ViewParent) drawingView.getParent())
                                        .requestDisallowInterceptTouchEvent(true);
                            }
                    // clickedMpnodeView.invalidate();
                    // }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
            }
            drawingView.invalidate();
            // return gDetector.onTouchEvent(event);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {

            return super.onFling(e1, e2, velocityX, velocityY);
        }


        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {

            return super.onSingleTapConfirmed(e);
        }

        public GestureDetector getDetector() {
            return gDetector;
        }
    }

    // LoadImageFromServer loadImageFromServer= new LoadImageFromServer();
    }

