package com.ptg.mindmap.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.BidiFormatter;
import android.text.Editable;
import android.text.Html;
import android.text.TextDirectionHeuristics;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.ObjectAnimator;
import com.ptg.gesturedetectors.MoveGestureDetector;
import com.ptg.mimdmap.node.MPNode;
import com.ptg.mimdmap.node.MPNodeView;
import com.ptg.mimdmap.spinner.AbstractWheel;
import com.ptg.mimdmap.spinner.OnWheelScrollListener;
import com.ptg.mimdmap.spinner.WheelVerticalView;
import com.ptg.mindmap.adapter.FontAdapter;
import com.ptg.mindmap.enums.MapLayoutLineType;
import com.ptg.mindmap.enums.MapLayoutShapeType;
import com.ptg.mindmap.enums.MapLayoutType;
import com.ptg.mindmap.enums.MapState;
import com.ptg.mindmap.enums.UndoOperationKey;
import com.ptg.mindmap.layouts.FreeMapLayout;
import com.ptg.mindmap.layouts.MPEbag;
import com.ptg.mindmap.layouts.MPLayout;
import com.ptg.mindmap.undoredo.MPUndoManager;
import com.ptg.mindmap.undoredo.MPUndoObject;
import com.ptg.mindmap.xml.DetectHtml;
import com.ptg.mindmap.xml.LoadImageFromServer;
import com.ptg.mindmap.xml.MindMapXmlHandler;
import com.ptg.mindmap.xml.xmlRequest;
import com.ptg.views.CircleButton;
import com.ptg.views.MindMapButton;
import com.ptg.views.MindMapImageButton;
import com.ptg.views.TwoDScrollView;
import com.semanoor.manahij.ManahijApp;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.Book;
import com.semanoor.source_sboookauthor.Globals;
import com.semanoor.source_sboookauthor.UserFunctions;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class MindMapActivity extends Activity implements OnClickListener {
	private static final int RESULT_LOAD_IMAGE = 0;
	ImageView slideHandleButton;
	LinearLayout slidingDrawer;
	public static  RelativeLayout canvasView;
	public static ViewGroup drawingView;
	// MyZoomView drawingViewMain;
	// AutoBgButton bPrevoius, bNext;
	ImageView tapView, viewPicimage;
	HorizontalScrollView mHList, /* horizontalScroll1, */smileScroll,generalScroll,threedScroll;
	TextView viewEdit, viewInsert, viewNotes, viewLayout;
	ViewGroup layoutNotes, layoutInsert, layoutEdit, layoutTypes;
	MindMapImageButton txtCut, txtCopy, txtPaste, txtPastePro, txtCopyPro,
	txtCenterAlign, txtRightAlign, txtLeftAlign, shapePentagon,
	shapeComment, shapeFreeForm, shapeRectangle, shapeRoundedRect,
	shapeOval, shapeRoundedRect1;
	MindMapButton txtFontFace, txtFontSize;
	/*AutoBgButton smile1, smile2, smile3, smile4, smile5, smile6, smile7,
			smile8, smile9, smile10, smile11, smile12;*/
	MindMapImageButton btnLeftMap, btnRghtMap, btnCircularMap, btnCircularMap1,
	btnFreeMap;
	public static EditText  editAddNotes;
	TextView addText;
	MindMapImageButton viewClose, viewFileOpen;
	// View linearGrid;
	LinearLayout txtStraightLine, txtCurvedLine1, txtCurvedThickLine,insertParent;
	MindMapImageButton btnStraightLine, btnCurvedLine1, btnCurvedThickLine;
	// LinearLayout txtLeftMap, txtRghtMap, txtCircularMap, txtFreeMap;
	// AutoBgButton btnLeftMap, btnRghtMap, btnCircularMap, btnFreeMap;
	ArrayList<String> fontsList;
	public int[] colors = new int[] { Color.parseColor("#C4DF9B"),
			Color.parseColor("#FDC689"), Color.parseColor("#F5989D"),
			Color.parseColor("#A864A8"), Color.parseColor("#1CBBB4"),
			Color.parseColor("#FFF568"), Color.parseColor("#9BCEDF"),
			Color.parseColor("#89FDB4"), Color.parseColor("#ABF598"),
			Color.parseColor("#A89E64"), Color.parseColor("#9C1CBB"),
			Color.parseColor("#68FFDE"), Color.parseColor("#B6FD89"),
			Color.parseColor("#98B6F5") };
	public int[] icons = new int[] { R.drawable.mi_emoticon_01,
			R.drawable.mi_emoticon_02, R.drawable.mi_emoticon_03,
			R.drawable.mi_emoticon_04, R.drawable.mi_emoticon_05,
			R.drawable.mi_emoticon_06, R.drawable.mi_emoticon_07,
			R.drawable.mi_emoticon_08, R.drawable.mi_emoticon_09,
			R.drawable.mi_emoticon_10, R.drawable.mi_emoticon_11,
			R.drawable.mi_emoticon_12 };
	CircleButton color1, color2, color3, color4, color5, color6, color7,
	color8, color9, color10, color11, color12, color13, color14,
	color15, color16;
	TextView noColor, labelNotes;
	CircleButton textColor1, textColor2, textColor3, textColor4, textColor5,
	textColor6, textColor7, textColor8, textColor9, textColor10,
	textColor11, textColor12, textColor13, textColor14, textColor15,
	textColor16;
	public static Bitmap BitmapDummyImage = null;
	// RelativeLayout removeSmile;
	RelativeLayout LinearLayout01;
	public static int width = 0, height = 0;
	public static MPLayout mplayout;
	public MPUndoManager mPUndoManager;
	// public static HScroll hScroll;
	// public static VScroll vScroll;
	public static TwoDScrollView twoDScroll;
	public  RelativeLayout popup_container;
	// TreeView treeView;
	public static int centerX, centerY;
	private String rootNodeText;
	private String orphanNodeText;
	private String mindMapTitle;
	private boolean isOpenMindMapDialog = false;
	boolean focusAddtext;
	public static DisplayMetrics displayMetrics;
	public static int densityDpi;
	private MoveGestureDetector mMoveDetector;
	private Book book;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mi_main);
		rootNodeText = getIntent().getStringExtra("MIRootNodeText");
		orphanNodeText = getIntent().getStringExtra("MIOrphanNodeText");
		mindMapTitle = getIntent().getStringExtra("MindMapTitle");
//		bookListArray = (ArrayList<Object>) getIntent().getSerializableExtra("bookArray");
		book = (Book) getIntent().getSerializableExtra("book");
		displayMetrics = getResources().getDisplayMetrics();
		densityDpi = displayMetrics.densityDpi;
		BitmapDummyImage = BitmapFactory.decodeResource(getResources(),
				R.drawable.mi_dummy_image);
		boolean fileExists = false;

		if (!mindMapTitle.equals("")) {
			String mindMapDir = Globals.TARGET_BASE_MINDMAP_PATH;
			UserFunctions.createNewDirectory(mindMapDir);
			String filePath = mindMapDir + mindMapTitle + ".xml";
			if (new File(filePath).exists()) {
				fileExists = true;
			}
		}

		initializeLayout(fileExists);

		mMoveDetector = new MoveGestureDetector(getApplicationContext(),
				new MoveListener());
	}

	LinearLayout navigate;
	InputMethodManager inputMethodManager;
	InputMethodSubtype inputMethodSubtype;
	Handler handler;
	public void initializeLayout(boolean fileExists) {
		handler = new Handler();
		layoutNotes = (ViewGroup) findViewById(R.id.layoutNotes);
		layoutInsert = (ViewGroup) findViewById(R.id.layoutInsert);
		layoutEdit = (ViewGroup) findViewById(R.id.layoutEdit);
		layoutTypes = (ViewGroup) findViewById(R.id.layoutTypes);
		mHList = (HorizontalScrollView) findViewById(R.id.horizontalScroll);
		// horizontalScroll1 = (HorizontalScrollView)
		// findViewById(R.id.horizontalScroll1);
		smileScroll = (HorizontalScrollView) findViewById(R.id.horizontalscroll_smile);
		generalScroll = (HorizontalScrollView) findViewById(R.id.horizontalscroll_general);
		threedScroll = (HorizontalScrollView) findViewById(R.id.horizontalscroll_threed);
		slideHandleButton = (ImageView) findViewById(R.id.slideHandleButton);
		// txtCut=(ImageView)findViewById(R.id.txtCut);
		inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		labelNotes = (TextView) findViewById(R.id.labelNotes);
		inputMethodSubtype = inputMethodManager.getCurrentInputMethodSubtype();
		// hScroll = (HScroll) findViewById(R.id.hScroll);
		// vScroll = (VScroll) findViewById(R.id.vScroll);
		twoDScroll = (TwoDScrollView) findViewById(R.id.hScroll);
		// bPrevoius = (AutoBgButton) findViewById(R.id.bPrevoius);
		// bNext = (AutoBgButton) findViewById(R.id.bNext);
		LinearLayout01 = (RelativeLayout) findViewById(R.id.LinearLayout01);

		viewEdit = (TextView) findViewById(R.id.viewEdit);
		viewInsert = (TextView) findViewById(R.id.viewInsert);
		viewNotes = (TextView) findViewById(R.id.viewNotes);
		viewLayout = (TextView) findViewById(R.id.viewLayout);
		slidingDrawer = (LinearLayout) findViewById(R.id.SlidingDrawer);
		canvasView = (RelativeLayout) findViewById(R.id.canvasView);
		drawingView = (ViewGroup) findViewById(R.id.drawingView);
		// drawingViewMain = (MyZoomView) findViewById(R.id.drawingViewMain);
		tapView = (ImageView) findViewById(R.id.tapView);
		// linearGrid = (View) findViewById(R.id.linearGrid);
		viewPicimage = (ImageView) findViewById(R.id.viewAttachment);
		insertParent=(LinearLayout)findViewById(R.id.insertParent);
		editAddNotes = (EditText) findViewById(R.id.editAddNotes);
		txtCut = (MindMapImageButton) findViewById(R.id.txtCut);
		txtCopy = (MindMapImageButton) findViewById(R.id.txtCopy);
		txtPaste = (MindMapImageButton) findViewById(R.id.txtPaste);
		txtPastePro = (MindMapImageButton) findViewById(R.id.txtPastePro);
		txtCopyPro = (MindMapImageButton) findViewById(R.id.txtCopyPro);
		txtCenterAlign = (MindMapImageButton) findViewById(R.id.txtCenterAlign);
		txtRightAlign = (MindMapImageButton) findViewById(R.id.txtRightAlign);
		txtLeftAlign = (MindMapImageButton) findViewById(R.id.txtLeftAlign);
		txtFontSize = (MindMapButton) findViewById(R.id.txtFontSize);
		txtFontFace = (MindMapButton) findViewById(R.id.txtFontFace);
		popup_container=(RelativeLayout)findViewById(R.id.popup_container);
		viewClose = (MindMapImageButton) findViewById(R.id.viewClose);
		viewFileOpen = (MindMapImageButton) findViewById(R.id.viewFileOpen);
		// treeView = (TreeView) findViewById(R.id.treeView);
		navigate = (LinearLayout) findViewById(R.id.nav);
		addText = (TextView) findViewById(R.id.txtAddText);
		color1 = (CircleButton) findViewById(R.id.color1);
		color2 = (CircleButton) findViewById(R.id.color2);
		color3 = (CircleButton) findViewById(R.id.color3);
		color4 = (CircleButton) findViewById(R.id.color4);
		color5 = (CircleButton) findViewById(R.id.color5);
		color6 = (CircleButton) findViewById(R.id.color6);
		color7 = (CircleButton) findViewById(R.id.color7);
		color8 = (CircleButton) findViewById(R.id.color8);
		color9 = (CircleButton) findViewById(R.id.color9);
		color10 = (CircleButton) findViewById(R.id.color10);
		color11 = (CircleButton) findViewById(R.id.color11);
		color12 = (CircleButton) findViewById(R.id.color12);
		color13 = (CircleButton) findViewById(R.id.color13);
		color14 = (CircleButton) findViewById(R.id.color14);
		color15 = (CircleButton) findViewById(R.id.color15);
		color16 = (CircleButton) findViewById(R.id.color16);
		noColor = (TextView) findViewById(R.id.noColor);
		// removeSmile=(RelativeLayout)findViewById(R.id.removeSmile);
		fontsList = readAllFonts();
		mplayout = new MPLayout(MindMapActivity.this);
		txtStraightLine = (LinearLayout) findViewById(R.id.txtStraightLine);
		txtCurvedLine1 = (LinearLayout) findViewById(R.id.txtCurvedLine1);
		txtCurvedThickLine = (LinearLayout) findViewById(R.id.txtCurvedThickLine);
		btnStraightLine = (MindMapImageButton) findViewById(R.id.btnStraightLine);
		btnCurvedLine1 = (MindMapImageButton) findViewById(R.id.btnCurvedLine1);
		btnCurvedThickLine = (MindMapImageButton) findViewById(R.id.btnCurvedThickLine);
		shapeRoundedRect = (MindMapImageButton) findViewById(R.id.shapeRoundedRect);
		btnCircularMap = (MindMapImageButton) findViewById(R.id.btnCircularMap);
		btnCircularMap.setOnClickListener(this);
		shapeRoundedRect.setOnClickListener(this);

		informationPopup = (ImageView) findViewById(R.id.viewInfo);
		myGestureListener = new MyGestureListener(this);
		cancelSmiley = (TextView)findViewById(R.id.cancelSmiley);
		cancelGen = (TextView)findViewById(R.id.cancelGeneral);
		cancelThreed = (TextView)findViewById(R.id.cancelThreed);

		cancelSmiley.setOnClickListener(this);
		cancelGen.setOnClickListener(this);
		cancelThreed.setOnClickListener(this);
		txtStraightLine.setOnClickListener(this);
		txtCurvedLine1.setOnClickListener(this);
		txtCurvedThickLine.setOnClickListener(this);
		btnStraightLine.setOnClickListener(this);
		btnCurvedLine1.setOnClickListener(this);
		btnCurvedThickLine.setOnClickListener(this);
		informationPopup.setOnClickListener(this);

		color1.setOnClickListener(this);
		color2.setOnClickListener(this);
		color3.setOnClickListener(this);
		color4.setOnClickListener(this);
		color5.setOnClickListener(this);
		color6.setOnClickListener(this);
		color7.setOnClickListener(this);
		color8.setOnClickListener(this);
		color9.setOnClickListener(this);
		color10.setOnClickListener(this);
		color11.setOnClickListener(this);
		color12.setOnClickListener(this);
		color13.setOnClickListener(this);
		color14.setOnClickListener(this);
		color15.setOnClickListener(this);
		color16.setOnClickListener(this);
		noColor.setOnClickListener(this);
		slideHandleButton.setOnClickListener(this);
		// bPrevoius.setOnClickListener(this);
		// bNext.setOnClickListener(this);
		viewEdit.setOnClickListener(this);
		viewInsert.setOnClickListener(this);
		viewNotes.setOnClickListener(this);
		viewLayout.setOnClickListener(this);
		slidingDrawer.setOnClickListener(this);
		tapView.setOnClickListener(this);
		viewPicimage.setOnClickListener(this);

		txtCut.setOnClickListener(this);
		txtCopy.setOnClickListener(this);
		txtPaste.setOnClickListener(this);
		txtPastePro.setOnClickListener(this);
		txtCopyPro.setOnClickListener(this);
		txtCenterAlign.setOnClickListener(this);
		txtRightAlign.setOnClickListener(this);
		txtLeftAlign.setOnClickListener(this);
		txtFontSize.setOnClickListener(this);
		txtFontFace.setOnClickListener(this);
		// centerNode.setOnTouchListener(this);
		viewClose.setOnClickListener(this);
		viewFileOpen.setOnClickListener(this);
		// removeSmile.setOnClickListener(this);
		init();

		mPUndoManager = new MPUndoManager(mplayout);
		Display mDisplay = getWindowManager().getDefaultDisplay();
		width = mDisplay.getWidth();
		height = mDisplay.getHeight();
		LayoutParams parms = new LayoutParams(width
				- (2 * labelNotes.getLayoutParams().width + 20)-50,
				editAddNotes.getLayoutParams().height);
		parms.leftMargin = 12;
		editAddNotes.setLayoutParams(parms);

		//layoutInsert.getLayoutParams().width=width-20;
		// layoutTypes.getLayoutParams().width = width;
		layoutTypes.setMinimumWidth(width);
		addText.clearFocus();
		editAddNotes.clearFocus();
		drawingView.getLayoutParams().width = AppDict.DEFAULT_OFFSET;
		drawingView.getLayoutParams().height = AppDict.DEFAULT_OFFSET;
		fontsList = readAllFonts();

		addText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// addText.setFocusable(true);
				//addText.requestFocus();
				if (clickedMpnodeView != null && nodeProp != null) {
					showTopicEditText();
				} else
					Toast.makeText(MindMapActivity.this, "Select Topic",
							Toast.LENGTH_SHORT).show();

			}


		});
		editAddNotes.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					hideMenuItem();
					editAddNotes.addTextChangedListener(addNotesTextwatcher);
					addNotesUndo = new MPUndoObject();
					addNotesUndo.objold = nodeProp.copy();
					addNotesUndo.objnew = nodeProp;
					addNotesUndo.keyType = UndoOperationKey.UndoOperationKey_notesAttachment;
				} else {
					editAddNotes.removeTextChangedListener(addNotesTextwatcher);
					if (!addNotesUndo.objold.getM_strNode_Notes().equals(nodeProp.getM_strNode_Notes()) && addNotesUndo != null) {
						mPUndoManager.addUndoOperations(addNotesUndo);
						addNotesUndo = null;
					}
				}

			}
		});


		OnTouchListener innerScrollViewTouchListener = new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int action = event.getAction();
				switch (action) {
				case MotionEvent.ACTION_DOWN:
					// Disallow ScrollView to intercept touch events.
					v.getParent().requestDisallowInterceptTouchEvent(true);
					break;

				case MotionEvent.ACTION_UP:
					// Allow ScrollView to intercept touch events.
					v.getParent().requestDisallowInterceptTouchEvent(false);
					break;
				}
				return false;
			}
		};
		smileScroll.setOnTouchListener(innerScrollViewTouchListener);
		threedScroll.setOnTouchListener(innerScrollViewTouchListener);
		generalScroll.setOnTouchListener(innerScrollViewTouchListener);

		if (fileExists){
			new LoadXmlData(Globals.TARGET_BASE_FILE_PATH + "/mindMaps/" + mindMapTitle + ".xml", false).execute();
		} else if (!rootNodeText.isEmpty()){
			rootNode();
		}
		Rect rectf = new Rect();
		drawingView.getLocalVisibleRect(rectf);
		twoDScroll.post(new Runnable() {
			public void run() {
				twoDScroll.scrollTo((int) (AppDict.DEFAULT_OFFSET - width) / 2,
						(int) (AppDict.DEFAULT_OFFSET - height) / 2);
			}
		});

	}
	EditText enterTopicName;
	boolean isEnglish=false;
	public  void showTopicEditText() {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.mi_topicedittext, null);
		popup_container.setVisibility(View.VISIBLE);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		params.setMargins(width/2 - 100, 0, 0, slidingDrawer.getHeight() + tapView.getHeight() );
		params.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL);
		popup_container.addView(view,params);

		//popup = new PopupWindow(view, LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT, false);
		enterTopicName = (EditText) view.findViewById(R.id.editTextTopicName);
		enterTopicName.setOnClickListener(this);
		if (clickedMpnodeView != null && nodeProp != null)
			enterTopicName.setText(nodeProp.getM_strNode_text());
		enterTopicName.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				enterTopicName.requestFocus();
			}
		});



		final TextWatcher addtextWatcher = new TextWatcher() {
			public void afterTextChanged(Editable s) {

				if (clickedMpnodeView != null && nodeProp != null) {
					/*
					 * if(!nodeProp.getM_strNode_text().equals(addText.getText().
					 * toString())) mPUndoManager.addUndooperationOnNode(nodeProp,
					 * UndoOperationKey.UndoOperationKey_textEditing);
					 */

					inputMethodSubtype = inputMethodManager
							.getCurrentInputMethodSubtype();
					if (inputMethodSubtype.getLocale().equals("ar")) {
						enterTopicName.requestFocus(View.FOCUS_LEFT);
						clickedMpnodeView.getm_mpNode().setM_strNode_text(
								bidiFormatter.unicodeWrap(enterTopicName.getText()
										.toString(), TextDirectionHeuristics.RTL));

					} else {
						addText.requestFocus(View.FOCUS_RIGHT);
						clickedMpnodeView.getm_mpNode().setM_strNode_text(
								bidiFormatter.unicodeWrap(enterTopicName.getText()
										.toString(), TextDirectionHeuristics.LTR));
						//clickedMpnodeView.setTextDirection(View.TEXT_DIRECTION_LTR);
					}
					if (HasArabicCharacters(enterTopicName.getText().toString()))
						clickedMpnodeView.getm_mpNode().setM_strNode_text(
								bidiFormatter.unicodeWrap(enterTopicName.getText()
										.toString(), TextDirectionHeuristics.RTL));
					else
						clickedMpnodeView.getm_mpNode().setM_strNode_text(
								bidiFormatter.unicodeWrap(enterTopicName.getText()
										.toString(), TextDirectionHeuristics.LTR));
					// clickedMpnodeView.getm_mpNode().setM_strNode_text(addText.getText().toString());
					clickedMpnodeView.refreshDrawableState();
					clickedMpnodeView.invalidate();
					mplayout.setLayout(nodeProp);

				} else if (enterTopicName.getText().toString().length() > 0)
					Toast.makeText(MindMapActivity.this, "Select Topic",
							Toast.LENGTH_SHORT).show();
				// currentTextView.setText("" + addText.getText().toString());
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				Log.d("on before changed",
						"on before changed $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// addText.setText(addText.getText().toString());

			}
		};
		enterTopicName.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					hideMenuItem();
					enterTopicName.addTextChangedListener(addtextWatcher);
					addtextUndo = new MPUndoObject();
					addtextUndo.objold = nodeProp.copy();
					addtextUndo.objnew = nodeProp;
					addtextUndo.keyType = UndoOperationKey.UndoOperationKey_textEditing;
				} else {
					enterTopicName.removeTextChangedListener(addtextWatcher);
					if (!addtextUndo.objold.getM_strNode_text().equals(
							nodeProp.getM_strNode_text())
							&& addtextUndo != null) {
						HasArabicCharacters(nodeProp.m_strNode_text);
						mPUndoManager.addUndoOperations(addtextUndo);
						addtextUndo = null;
					}
				}

			}
		});
	}
	BidiFormatter bidiFormatter = BidiFormatter.getInstance();
	Locale l = Locale.getDefault();
	TextWatcher addtextWatcher = new TextWatcher() {
		public void afterTextChanged(Editable s) {
			mHList.scrollTo(0, 300);
			if (clickedMpnodeView != null && nodeProp != null) {

				inputMethodSubtype = inputMethodManager
						.getCurrentInputMethodSubtype();
				if (inputMethodSubtype.getLocale().equals("ar")) {
					clickedMpnodeView.setTextDirection(View.TEXT_DIRECTION_RTL);
					//addText.setTextDirection(View.TEXT_DIRECTION_RTL);
					nodeProp.m_shape.setTextDirection(View.TEXT_DIRECTION_RTL);
					// nodeProp.m_shape.setGravity(Gravity.RIGHT);
					nodeProp.m_shape.requestFocus(View.FOCUS_LEFT);
					//addText.setGravity(Gravity.RIGHT);
					addText.requestFocus(View.FOCUS_LEFT);
					clickedMpnodeView.getm_mpNode().setM_strNode_text(
							bidiFormatter.unicodeWrap(addText.getText()
									.toString(), TextDirectionHeuristics.RTL));

				} else {
					//addText.setTextDirection(View.TEXT_DIRECTION_LTR);
					//addText.setGravity(Gravity.LEFT);
					addText.requestFocus(View.FOCUS_RIGHT);
					clickedMpnodeView.getm_mpNode().setM_strNode_text(
							bidiFormatter.unicodeWrap(addText.getText()
									.toString(), TextDirectionHeuristics.LTR));
					clickedMpnodeView.setTextDirection(View.TEXT_DIRECTION_LTR);
				}
				if (HasArabicCharacters(addText.getText().toString())){
					addText.clearFocus();

					addText.requestFocus(View.FOCUS_LEFT);
					clickedMpnodeView.getm_mpNode().setM_strNode_text(
							bidiFormatter.unicodeWrap(addText.getText()
									.toString(), TextDirectionHeuristics.RTL));
				}
				else
					clickedMpnodeView.getm_mpNode().setM_strNode_text(
							bidiFormatter.unicodeWrap(addText.getText()
									.toString(), TextDirectionHeuristics.LTR));
				clickedMpnodeView.refreshDrawableState();
				clickedMpnodeView.invalidate();
				mplayout.setLayout(nodeProp);

			} else if (addText.getText().toString().length() > 0)
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			// currentTextView.setText("" + addText.getText().toString());
		}

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			Log.d("on before changed",
					"on before changed $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// addText.setText(addText.getText().toString());

		}
	};
	TextWatcher addNotesTextwatcher = new TextWatcher() {
		public void afterTextChanged(Editable s) {
			if (clickedMpnodeView != null && nodeProp != null) {
				inputMethodSubtype = inputMethodManager
						.getCurrentInputMethodSubtype();
				if (inputMethodSubtype.getLocale().equals("ar")) {
					clickedMpnodeView.setTextDirection(View.TEXT_DIRECTION_RTL);
					editAddNotes.setTextDirection(View.TEXT_DIRECTION_RTL);
					nodeProp.m_shape.setTextDirection(View.TEXT_DIRECTION_RTL);
					nodeProp.m_shape.requestFocus(View.FOCUS_LEFT);
					editAddNotes.setGravity(Gravity.RIGHT);
					editAddNotes.requestFocus(View.FOCUS_LEFT);

				} else {
					editAddNotes.setTextDirection(View.TEXT_DIRECTION_LTR);
					editAddNotes.setGravity(Gravity.LEFT);
					clickedMpnodeView.setTextDirection(View.TEXT_DIRECTION_LTR);
				}

				String text = editAddNotes.getText().toString();
				if (HasArabicCharacters(text)){
					text	=bidiFormatter.unicodeWrap(text, TextDirectionHeuristics.RTL);
				}
				else{
					text=bidiFormatter.unicodeWrap(text, TextDirectionHeuristics.LTR);
				}
				clickedMpnodeView.getm_mpNode().setM_strNode_Notes(text);
				if(nodeProp.m_strNode_Notes.length()==0)
					nodeProp.propertiesPositions.remove("1");
				// clickedMpnodeView.getm_mpNode().setM_strNode_Notes(editAddNotes.getText().toString());
				clickedMpnodeView.invalidate();
				MPLayout layout = new MPLayout(MindMapActivity.this);
				layout.setLayout(nodeProp);
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			// currentTextView.setText("" + addText.getText().toString());
		}

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

		}
	};

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		// Checks whether a hardware keyboard is available
		if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
			Toast.makeText(this, "keyboard visible", Toast.LENGTH_SHORT).show();
		} else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
			Toast.makeText(this, "keyboard hidden", Toast.LENGTH_SHORT).show();
		}
	}

	MPUndoObject addtextUndo, addNotesUndo,addinfoundo;
	int slidingDrawerHeight = 0;

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		// RelativeLayout.LayoutParams navDimens = (RelativeLayout.LayoutParams)
		// navigate.getLayoutParams();

		slidingDrawerHeight = slidingDrawer.getHeight();
	}

	public boolean isInBounds(float f, float g, RectF rectF) {
		if (rectF.contains(f, g)) {
			return true;
		}

		return false;
	}

	int i = 0;

	public void init() {
		layoutEdit.setVisibility(View.VISIBLE);
		layoutNotes.setVisibility(View.GONE);
		layoutInsert.setVisibility(View.GONE);
		layoutTypes.setVisibility(View.GONE);
	}

	public static MPNodeView clickedMpnodeView;

	public void rootNode() {
		MPEbag eBag = new MPEbag();
		eBag.setM_iId(i++);
		eBag.setM_parentNode(null);
		eBag.setBackgroundColor(Color.parseColor("#e3e3e3"));
		eBag.setHexaColor("#e3e3e3");
		// eBag.setM_selectedColor(Color.parseColor("#e3e3e3"));
		eBag.setTextColor("#000000");
		eBag.setM_fHeight(AppDict.mDrawBitmapHeight);
		eBag.setM_fWidth(AppDict.mDrawBitmapWidth);
		eBag.setM_iNode_TextSize(14);
		eBag.setM_strNode_Notes("");
		eBag.m_iLevelId = 0;
		eBag.textAlign=0;
		eBag.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_ROUNDEDRECTANGLE;
		// eBag.setM_isCollapsed(true);
		//Typeface ttf=Typeface.createFromFile("/system/fonts/"+ fontsList.get(0) + ".ttf");
		eBag.setM_str_Typeface(mplayout.createTypeface(eBag,"Open Sans")/*Typeface.createFromFile("/system/fonts/"+ fontsList.get(0) + ".ttf")*/);
		eBag.setM_strFontName("Open Sans");

		eBag.setM_strNode_text(rootNodeText);
		eBag.setM_fX((int) AppDict.DEFAULT_OFFSET / 2/* ((7 * width / 2) ) */);
		eBag.setM_fY((int) AppDict.DEFAULT_OFFSET / 2/* ((7 * height / 2)) */);
		eBag.istaskinfo = true;
		ManahijApp.m_curEbag.m_iMapLayoutLine = MapLayoutLineType.MPLayout_Line_Curved;
		MPNodeView nodeView = new MPNodeView(MindMapActivity.this, eBag);
		// nodeView.embeddImg=embeddImg;
		nodeView.setId(eBag.getM_iId());
		nodeView.bringToFront();
		eBag.setM_shape(nodeView);
		clickedMpnodeView = nodeView;
		centerX = (width * 7) / 2;
		centerY = (height * 7) / 2;
		ManahijApp.m_curEbag = eBag;
		ManahijApp.m_curEbag.m_strMapLayoutFillColor = "#FFFFFF";
		mplayout.addLinelayer(drawingView);
		drawingView.addView(nodeView, AppDict.mDrawBitmapWidth,
				AppDict.mDrawBitmapHeight);
		nodeView.setOnTouchListener(myGestureListener);
		resetWidth(nodeView);
		clickedMpnodeView = nodeView;
		nodeProp = clickedMpnodeView.m_node;
		// nodeView.bringToFront();

	}

	public static MPNode nodeProp;
	//boolean nodIsClicked;
	boolean cutNode = false;

	public void reset() {
		drawingView.removeView(popUpLayout);
		popUpLayout = null;
		drawingView.removeView(ManahijApp.m_curEbag.linesLayerView);
		ManahijApp.m_curEbag = null;
		MPLayout layout = new MPLayout(MindMapActivity.this);
		drawingView.setScaleX(1f);
		drawingView.setScaleY(1f);
		TwoDScrollView.scaleFactor = 1f;
		drawingView.removeAllViewsInLayout();
		ArrayList<MPNodeView> childs = layout.getChilds(drawingView);
		for (MPNodeView child : childs) {
			drawingView.removeView(child);
		}
		twoDScroll.post(new Runnable() {
			public void run() {

				twoDScroll.scrollTo((int) (AppDict.DEFAULT_OFFSET - width) / 2,
						(int) (AppDict.DEFAULT_OFFSET - height) / 2);
			}
		});
	}

	public void priorityClick(View v) {
		hideMenuItem();
		completionGroup.setVisibility(View.INVISIBLE);
		String smile = (String) v.getTag();
		// gn
		if(v.getId()== R.id.prior1){
			if(priorityGroup.getVisibility()==View.INVISIBLE){
				priorityGroup.setVisibility(View.VISIBLE);
			}
			else{
				if (clickedMpnodeView != null && nodeProp != null) {
					mPUndoManager.addUndooperationOnNode(nodeProp,
							UndoOperationKey.UndoOperationKey_priority);
					int smilePosition = Integer.parseInt(smile);
					Bitmap cImg = BitmapFactory.decodeResource(
							MindMapActivity.this.getResources(),
							AppDict.priority[Integer.parseInt(smile) - 1]);
					cImg = Bitmap.createScaledBitmap(cImg, 40, 40, true);
					nodeProp.setPriorityCharacterPosition(smilePosition);
					nodeProp.setPriorityCharacter(cImg);
					resetWidth(clickedMpnodeView);
					priorityGroup.setVisibility(View.INVISIBLE);
				}
			}
		}else{

			if (clickedMpnodeView != null && nodeProp != null) {
				int smilePosition = Integer.parseInt(smile);
				mPUndoManager.addUndooperationOnNode(nodeProp,
						UndoOperationKey.UndoOperationKey_priority);
				Bitmap cImg = BitmapFactory.decodeResource(
						MindMapActivity.this.getResources(),
						AppDict.priority[Integer.parseInt(smile) - 1]);
				cImg = Bitmap.createScaledBitmap(cImg, 40, 40, true);
				nodeProp.setPriorityCharacterPosition(smilePosition);
				nodeProp.setPriorityCharacter(cImg);
				resetWidth(clickedMpnodeView);

				priorityGroup.setVisibility(View.INVISIBLE);
			}
		}
	}

	public void completionClick(View v) {
		hideMenuItem();
		// gn
		priorityGroup.setVisibility(View.INVISIBLE);
		String smile = (String) v.getTag();
		if(v.getId()== R.id.completion1){
			if(completionGroup.getVisibility()==View.INVISIBLE){
				completionGroup.setVisibility(View.VISIBLE);
			}else{
				if (clickedMpnodeView != null && nodeProp != null) {
					mPUndoManager.addUndooperationOnNode(nodeProp,
							UndoOperationKey.UndoOperationKey_completion);
					int smilePosition = Integer.parseInt(smile);
					Bitmap cImg = BitmapFactory.decodeResource(
							MindMapActivity.this.getResources(),
							AppDict.completion[Integer.parseInt(smile) - 1]);
					cImg = Bitmap.createScaledBitmap(cImg, 40, 40, true);
					nodeProp.setCompletionCharacterPosition(smilePosition);
					nodeProp.setCompletionCharacter(cImg);
					resetWidth(clickedMpnodeView);
					completionGroup.setVisibility(View.INVISIBLE);
				}
			}
		}else{
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnNode(nodeProp,
						UndoOperationKey.UndoOperationKey_completion);
				int smilePosition = Integer.parseInt(smile);
				Bitmap cImg = BitmapFactory.decodeResource(
						MindMapActivity.this.getResources(),
						AppDict.completion[Integer.parseInt(smile) - 1]);
				cImg = Bitmap.createScaledBitmap(cImg, 40, 40, true);
				nodeProp.setCompletionCharacterPosition(smilePosition);
				nodeProp.setCompletionCharacter(cImg);
				resetWidth(clickedMpnodeView);
				completionGroup.setVisibility(View.INVISIBLE);
			}
		}
	}

	public void generalClick(View v) {
		hideMenuItem();
		removePopContainer();
		// gn
		String smile = (String) v.getTag();
		if (clickedMpnodeView != null && nodeProp != null) {
			mPUndoManager.addUndooperationOnNode(nodeProp,
					UndoOperationKey.UndoOperationKey_generalimg);
			int smilePosition = Integer.parseInt(smile);
			Bitmap smileImg = BitmapFactory.decodeResource(getResources(),
					AppDict.genicons[smilePosition - 1]);
			smileImg = Bitmap.createScaledBitmap(smileImg, 40, 40, true);
			nodeProp.setGenCharacterPosition(smilePosition);
			nodeProp.setGenCharacter(smileImg);
			resetWidth(clickedMpnodeView);
		}
	}

	public void clipArtClick(View v) {
		hideMenuItem();
		removePopContainer();
		String smile = (String) v.getTag();
		if (clickedMpnodeView != null && nodeProp != null) {
			mPUndoManager.addUndooperationOnNode(nodeProp,
					UndoOperationKey.UndoOperationKey_clipart);
			int smilePosition = Integer.parseInt(smile);
			Bitmap smileImg = BitmapFactory.decodeResource(getResources(),
					AppDict.tdicons[smilePosition - 1]);
			smileImg = Bitmap.createScaledBitmap(smileImg, 40, 40, true);
			nodeProp.setdCharacterPosition(smilePosition);
			nodeProp.setdCharacter(smileImg);
			resetWidth(clickedMpnodeView);
		}
	}

	public void smileClick(View v) {
		hideMenuItem();
		removePopContainer();
		String smile = (String) v.getTag();
		if (clickedMpnodeView != null && nodeProp != null) {
			int smilePosition = Integer.parseInt(smile);
			Bitmap smileImg = BitmapFactory.decodeResource(getResources(),
					AppDict.icons[smilePosition - 1]);
			smileImg = Bitmap.createScaledBitmap(smileImg, 40, 40, true);
			mPUndoManager.addUndooperationOnNode(nodeProp,
					UndoOperationKey.UndoOperationKey_smiley);
			nodeProp.setSmileIconPosition(smilePosition);
			nodeProp.setM_smile_image(smileImg);
			clickedMpnodeView.invalidate();
			resetWidth(clickedMpnodeView);
		} else
			Toast.makeText(MindMapActivity.this, "Select Topic",
					Toast.LENGTH_SHORT).show();
	}

	public class ParseSemaNode extends AsyncTask<Void, Void, String> {
		ProgressDialog progressDialog;
		String dataToReturn="";
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new ProgressDialog(MindMapActivity.this);
			progressDialog.setMessage("saving data...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}
		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
			progressDialog.setMessage("saving Map...");
		}
		@Override
		protected String doInBackground(Void... params) {
			dataToReturn=mplayout.getMapXML();
			return dataToReturn;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(progressDialog!=null&&progressDialog.isShowing())
				progressDialog.dismiss();
			mplayout.takeScreenshoot(mindMapTitle);
		}

	}
	@Override
	public void onBackPressed() {
			if (isOpenMindMapDialog) {
				setResult(RESULT_OK, getIntent().putExtra("MIOpenDialog", isOpenMindMapDialog));
				setResult(RESULT_OK, getIntent().putExtra("MIOrphanNodeText", orphanNodeText));
			}
			setResult(RESULT_OK, getIntent().putExtra("MITitle", mindMapTitle));
			super.onBackPressed();
			try {
				ManahijApp.m_curEbag.arrageChildrenOrder();
				//mplayout.addOrphanNode("Learning and sharing knowledge is a never-ending process. The written form of communication ");
				String xmlContent = mplayout.getMapXML();
				saveXMLFile(mindMapTitle, xmlContent);
				mplayout.takeScreenshoot(mindMapTitle);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String mindMapDir = Globals.TARGET_BASE_MINDMAP_PATH;
			String filePath = mindMapDir+mindMapTitle+".png";
			String filePath1 = Globals.TARGET_BASE_BOOKS_DIR_PATH+book.getBookID()+"/FreeFiles/card.png";
			UserFunctions.copyFiles(filePath,filePath1);
			String xmlPath = mindMapDir+mindMapTitle+".xml";
			String xmlPath1 = Globals.TARGET_BASE_BOOKS_DIR_PATH+book.getBookID()+"/mindMaps/"+mindMapTitle+".xml";
			UserFunctions.copyFiles(xmlPath,xmlPath1);
		    finish();
	}

	private void saveXMLFile(String fileName, String xmlContent){
		String mindMapDir = Globals.TARGET_BASE_MINDMAP_PATH;
		String filePath = mindMapDir+fileName+".xml";
		try {
			FileWriter fileWriter = new FileWriter(filePath);
			fileWriter.write(xmlContent);
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {

		addText.clearFocus();
		editAddNotes.clearFocus();
		removePopContainer();
		hideMenuItem();
		if (popup != null) {
			if (popup.isShowing())
				popup.dismiss();
		}
		final MPLayout layout = new MPLayout(MindMapActivity.this);
		switch (v.getId()) {

		case R.id.btnStraightLine:
		case R.id.txtStraightLine:
			mPUndoManager.addUndooperationOnNode(ManahijApp.m_curEbag,
					UndoOperationKey.UndoOperationKey_changeLineStyle);

			ManahijApp.m_curEbag.m_iMapLayoutLine = MapLayoutLineType.MPLayout_Line_Straight;
			layout.setLayout(ManahijApp.m_curEbag);
			break;
		case R.id.btnCurvedLine1:
		case R.id.txtCurvedLine1:
			mPUndoManager.addUndooperationOnNode(ManahijApp.m_curEbag,
					UndoOperationKey.UndoOperationKey_changeLineStyle);
			ManahijApp.m_curEbag.m_iMapLayoutLine = MapLayoutLineType.MPLayout_Line_Curved;
			layout.setLayout(ManahijApp.m_curEbag);
			break;
		case R.id.btnCurvedThickLine:
		case R.id.txtCurvedThickLine:
			mPUndoManager.addUndooperationOnNode(ManahijApp.m_curEbag,
					UndoOperationKey.UndoOperationKey_changeLineStyle);
			ManahijApp.m_curEbag.m_iMapLayoutLine = MapLayoutLineType.MPLayout_Line_CurvedThick;
			layout.setLayout(ManahijApp.m_curEbag);
			break;

		case R.id.viewClose:
			try {
				ManahijApp.m_curEbag.arrageChildrenOrder();
				//mplayout.addOrphanNode("Learning and sharing knowledge is a never-ending process. The written form of communication ");
				new ParseSemaNode().execute();
				isOpenMindMapDialog=false;
				onBackPressed();

			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		 case R.id.viewFileOpen:
			 isOpenMindMapDialog=true;
			 onBackPressed();
			   break;
		case R.id.slideHandleButton:
			slideVisibleAnimation(slidingDrawer);
			slideVisibleAnimation1(navigate);
			tapView.setVisibility(View.VISIBLE);
			// linearGrid.setVisibility(View.VISIBLE);
			break;
		case R.id.tapView:
			if (slidingDrawer.getVisibility() == View.INVISIBLE)
				slidingDrawer.setVisibility(View.GONE);
			slideGoneAnimation(slidingDrawer);
			slideGoneAnimation1(navigate);
			break;
		case R.id.viewEdit:
			init();
			// mHList.scrollTo(0, 0);
			setSelectedView(viewEdit);
			break;

		case R.id.viewInsert:
			setSelectedView(viewInsert);
			// mHList.scrollTo(0, 0);
			layoutEdit.setVisibility(View.GONE);
			layoutNotes.setVisibility(View.GONE);
			layoutInsert.setVisibility(View.VISIBLE);
			layoutTypes.setVisibility(View.GONE);
			break;
		case R.id.viewNotes:
			// mHList.scrollTo(0, 0);
			setSelectedView(viewNotes);
			editAddNotes.requestFocus();
			layoutEdit.setVisibility(View.GONE);
			layoutNotes.setVisibility(View.VISIBLE);
			layoutInsert.setVisibility(View.GONE);
			layoutTypes.setVisibility(View.GONE);
			break;
		case R.id.viewLayout:
			// mHList.scrollTo(0, 0);
			setSelectedView(viewLayout);
			layoutEdit.setVisibility(View.GONE);
			layoutNotes.setVisibility(View.GONE);
			layoutInsert.setVisibility(View.GONE);
			layoutTypes.setVisibility(View.VISIBLE);
			break;
		case R.id.viewAttachment:
			if (clickedMpnodeView != null && nodeProp != null) {
				showBackgroundImagePopUp();
				//showBackgroundImagePopUp();
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();

			break;
		case R.id.viewInfo:
			if (clickedMpnodeView != null && nodeProp != null) {
				showInformationPopUp();

			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;
		case R.id.txtCut:
			if (clickedMpnodeView == null && nodeProp == null)
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_LONG).show();
			else {
				ManahijApp.m_curEbag.copyMpNode = nodeProp/* .copy() */;
				ManahijApp.m_curEbag.cutMpNode = nodeProp;
				cutNode = true;
			}
			break;
		case R.id.txtCopy:
			if (clickedMpnodeView == null && nodeProp == null)
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_LONG).show();
			else {
				ManahijApp.m_curEbag.copyMpNode = nodeProp/* .copy() */;
				cutNode = false;
			}
			break;
		case R.id.txtPaste:
			if (ManahijApp.m_curEbag.copyMpNode == null
			|| clickedMpnodeView == null && nodeProp == null) {
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_LONG).show();
			} else {

				if (cutNode) {
					// cut logic
					variable = false;
					variable1 = false;
					if (!checkChild(nodeProp)
							&& !checkParentChild(ManahijApp.m_curEbag.cutMpNode)) {
						if (clickedMpnodeView != null && nodeProp != null) {
							// if(ManahijApp.m_curEbag.copyMpNode!=nodeProp||ManahijApp.m_curEbag.cutMpNode!=nodeProp){
							MPNode cutNodeInstance = ManahijApp.m_curEbag.cutMpNode;
							MPNode cutUndoNode = cutNodeInstance.copy();
							mPUndoManager.addUndooperationOnOldNode(
									cutUndoNode, cutNodeInstance,
									UndoOperationKey.UndoOperationKey_cutPast);

							cutNodeInstance.m_parentNode.m_arrChildren
							.remove(cutNodeInstance);
							cutNodeInstance.m_parentNode = nodeProp;
							if (nodeProp.getM_arrChildren() != null
									&& nodeProp.getM_arrChildren().size() > 0)
								nodeProp.getM_arrChildren()
								.add(cutNodeInstance);
							else {
								ArrayList<MPNode> childs = new ArrayList<MPNode>();
								childs.add(cutNodeInstance);
								nodeProp.setM_arrChildren(childs);
							}
							cutNode = false;
							// createShapes(copyNode,nodeProp);
							if (ManahijApp.m_curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap) {
								FreeMapLayout freeMap = new FreeMapLayout(
										MindMapActivity.this);
								freeMap.generatenewPositions(cutNodeInstance);

							}

							// ManahijApp.m_curEbag.cutMpNode.deleteNode();
							cutNode = false;
							layout.setLayout(ManahijApp.m_curEbag);
							ManahijApp.m_curEbag.copyMpNode = null;
						}
					}
				} else {
					// copy logic
					if (clickedMpnodeView != null && nodeProp != null) {

						// if(ManahijApp.m_curEbag.copyMpNode!=nodeProp||ManahijApp.m_curEbag.cutMpNode!=nodeProp){
						MPNode copyNode = ManahijApp.m_curEbag.copyMpNode
								.copy();
						copyNode.m_parentNode = nodeProp;
						MPNode copyUndoNode = copyNode.copy();
						mPUndoManager.addUndooperationOnOldNode(copyUndoNode,
								copyNode,
								UndoOperationKey.UndoOperationKey_copyPast);
						if (nodeProp.getM_arrChildren() != null
								&& nodeProp.getM_arrChildren().size() > 0)
							nodeProp.getM_arrChildren().add(copyNode);
						else {
							ArrayList<MPNode> childs = new ArrayList<MPNode>();
							childs.add(copyNode);
							nodeProp.setM_arrChildren(childs);
						}
						cutNode = false;
						createShapes(copyNode, nodeProp);

						if (ManahijApp.m_curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap) {
							FreeMapLayout freeMap = new FreeMapLayout(
									MindMapActivity.this);
							// newNodes(ManahijApp.m_curEbag.copyMpNode);
							ManahijApp.m_curEbag.m_isKeepFreeMapSamePositions = false;
							freeMap.generatenewPositions(copyNode);

						}
						layout.setLayout(copyNode);

						// ManahijApp.m_curEbag.copyMpNode = null;
					}

				}
			}

			break;
		case R.id.txtPastePro:
			if (clickedMpnodeView != null && nodeProp != null) {

				if (ManahijApp.m_curEbag.copyPropertiesMpNode != null) {
					MPNode copyNodeProperties = nodeProp.copy();
					nodeProp.pasteProperties(ManahijApp.m_curEbag.copyPropertiesMpNode);
					mPUndoManager.addUndooperationOnTextProperties(
							copyNodeProperties, nodeProp,
							ManahijApp.m_curEbag.copyPropertiesMpNode,
							UndoOperationKey.UndoOperationKey_copyPastPro);
					clickedMpnodeView.calculateTextWidth();
					clickedMpnodeView.invalidate();
					layout.setLayout(nodeProp);
				}
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;
		case R.id.txtCopyPro:
			if (clickedMpnodeView != null && nodeProp != null) {
				ManahijApp.m_curEbag.copyPropertiesMpNode = nodeProp;
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();

			break;
		case R.id.txtCenterAlign:
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnTextAlign(nodeProp, 0);
				nodeProp.textAlign = 0;
				clickedMpnodeView.invalidate();
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;
		case R.id.txtRightAlign:
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnTextAlign(nodeProp, 2);
				nodeProp.textAlign = 2;
				clickedMpnodeView.invalidate();
			} else

				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();

			break;
		case R.id.txtLeftAlign:
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnTextAlign(nodeProp, 1);
				nodeProp.textAlign = 1;
				clickedMpnodeView.invalidate();
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();

			break;
		case R.id.txtFontSize:
		case R.id.txtFontFace:
			if (clickedMpnodeView != null && nodeProp != null) {
				showCustomSpinner();
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;
		case R.id.btnCircularMap:
			showMapsPopUp();
			break;

		case R.id.shapeRoundedRect:
			if (clickedMpnodeView != null && nodeProp != null) {
			showShapesPopUp();
			 } else
				  Toast.makeText(MindMapActivity.this,
				  "Select Topic",Toast.LENGTH_SHORT).show();

			break;

		case R.id.color1:
		case R.id.color2:
		case R.id.color3:
		case R.id.color4:
		case R.id.color5:
		case R.id.color6:
		case R.id.color7:
		case R.id.color8:
		case R.id.color9:
		case R.id.color10:
		case R.id.color11:
		case R.id.color12:
		case R.id.color13:
		case R.id.color14:
		case R.id.color15:
		case R.id.color16:
			// case R.id.noColor:
			String color = (String) v.getTag();
			if (clickedMpnodeView != null && nodeProp != null) {
				String backgroundColor = nodeProp.hexaColor;
				clickedMpnodeView.getm_mpNode().setBackgroundColor(
						Color.parseColor(color));
				layout.tempUpdateChildrenColor(clickedMpnodeView,
						Color.parseColor(color), color);
				clickedMpnodeView.getm_mpNode().setHexaColor(color);
				ManahijApp.m_curEbag.m_shape.bringToFront();
				mPUndoManager.addUndooperationOnNodeColor(nodeProp,
						backgroundColor);
				layout.setLayout(nodeProp);
				clickedMpnodeView.invalidate();
				applyGradientMethod(color);

			} else {
				mPUndoManager
				.addUndooperationOnEbagColor(ManahijApp.m_curEbag.m_strMapLayoutFillColor);
				ManahijApp.m_curEbag.m_strMapLayoutFillColor = color;
				drawingView.setBackgroundColor(Color.parseColor(color));
			}
			break;
		case R.id.noColor:
			color = (String) v.getTag();
			if (clickedMpnodeView != null && nodeProp != null) {
				String backgroundColor = nodeProp.hexaColor;
				clickedMpnodeView.getm_mpNode().setBackgroundColor(
						Color.parseColor(color));
				nodeProp.m_iGradientType=0;
				layout.tempUpdateChildrenColor(clickedMpnodeView,
						Color.parseColor(color), color);
				clickedMpnodeView.getm_mpNode().setHexaColor(color);
				ManahijApp.m_curEbag.m_shape.bringToFront();
				mPUndoManager.addUndooperationOnNodeColor(nodeProp,
						backgroundColor);
				layout.setLayout(nodeProp);
				clickedMpnodeView.invalidate();
				//applyGradientMethod(color);

			} else {
				mPUndoManager
				.addUndooperationOnEbagColor(ManahijApp.m_curEbag.m_strMapLayoutFillColor);
				ManahijApp.m_curEbag.m_strMapLayoutFillColor = color;
				drawingView.setBackgroundColor(Color.parseColor(color));
			}
			break;
		case R.id.textColor1:
		case R.id.textColor2:
		case R.id.textColor3:
		case R.id.textColor4:
		case R.id.textColor5:
		case R.id.textColor6:
		case R.id.textColor7:
		case R.id.textColor8:
		case R.id.textColor9:
		case R.id.textColor10:
		case R.id.textColor11:
		case R.id.textColor12:
		case R.id.textColor13:
		case R.id.textColor14:
		case R.id.textColor15:
		case R.id.textColor16:
			String textColor = (String) v.getTag();
			if (clickedMpnodeView != null && nodeProp != null) {
				String oldTextColor = nodeProp.getTextColor();
				clickedMpnodeView.getm_mpNode().setTextColor(textColor);
				mPUndoManager.addUndooperationOnNodeTextColor(nodeProp,oldTextColor, UndoOperationKey.UndoOperationKey_textColor);
				clickedMpnodeView.invalidate();
			}
			break;

		case R.id.cancelSmiley:
			if(clickedMpnodeView!=null && nodeProp.m_smile_image!=null){
				mPUndoManager.addUndooperationOnNode(nodeProp,
						UndoOperationKey.UndoOperationKey_smiley);
				nodeProp.smileIconPosition=-1;
				nodeProp.m_smile_image = null;
				clickedMpnodeView.invalidate();
			}
			break;

		case R.id.cancelGeneral:
			if(clickedMpnodeView!=null && nodeProp.genCharacter!=null){
				mPUndoManager.addUndooperationOnNode(nodeProp,
						UndoOperationKey.UndoOperationKey_generalimg);
				nodeProp.genCharacterPosition=-1;
				nodeProp.genCharacter = null;
				clickedMpnodeView.invalidate();
			}
			break;

		case R.id.cancelThreed:
			if(clickedMpnodeView!=null && nodeProp.dCharacter!=null){
				mPUndoManager.addUndooperationOnNode(nodeProp,
						UndoOperationKey.UndoOperationKey_clipart);
				nodeProp.dCharacterPosition=-1;
				nodeProp.dCharacter = null;
				if(clickedMpnodeView!=null)
					clickedMpnodeView.invalidate();
			}
			break;
		default:
			mplayout.setLayout(ManahijApp.m_curEbag);
			break;
		}

	}

	public class LoadXmlData extends AsyncTask<Void,Void, Void>{
		String url;
		boolean ishttp;
		ProgressDialog dialog;
		MPEbag ebag = new MPEbag();
		ArrayList<MPNode> childs;
		LoadXmlData(String url,boolean ishttp){
			this.url=url;
			this.ishttp=ishttp;
		}
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog=new ProgressDialog(MindMapActivity.this);
			dialog.setMessage("loading map");
			dialog.setCancelable(false);
			dialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			try{
				ebag = parsingSemaNodeXml (url,ishttp);
				childs = ebag.m_arrChildren;
				if(childs!=null){
					if (childs.get(0).m_arrChildren.size() > 0) {
						sortlist(childs.get(0));
						for (int i = 0; i < childs.get(0).m_arrChildren.size(); i++) {
							if (i < ebag.getM_iNumberOfRightNodes()) {
								childs.get(0).m_arrChildren.get(i).m_isRightnode = true;
								if (childs.get(0).m_arrChildren.get(i).m_isAboveCenter)
									ebag.m_arrRightTop.add(childs.get(0).m_arrChildren
											.get(i));
								else
									ebag.m_arrRightBottom.add(childs.get(0).m_arrChildren
											.get(i));
							} else {
								if (childs.get(0).m_arrChildren.get(i).m_isAboveCenter)
									ebag.m_arrLeftTop.add(childs.get(0).m_arrChildren
											.get(i));
								else
									ebag.m_arrLeftBottom.add(childs.get(0).m_arrChildren
											.get(i));
							}
						}
					}
					MPNode rootnode = ebag.m_arrChildren.get(0);
					rootnode.m_arrChildren.clear();
					for (int i = ebag.m_arrRightTop.size() - 1; i >= 0; i--) {
						rootnode.m_arrChildren.add(ebag.m_arrRightTop.get(i));
					}
					for (int i = 0; i < ebag.m_arrRightBottom.size(); i++) {
						rootnode.m_arrChildren.add(ebag.m_arrRightBottom.get(i));
					}
					for (int i = ebag.m_arrLeftBottom.size() - 1; i >= 0; i--) {
						rootnode.m_arrChildren.add(ebag.m_arrLeftBottom.get(i));
					}
					for (int i = 0; i < ebag.m_arrLeftTop.size(); i++) {
						rootnode.m_arrChildren.add(ebag.m_arrLeftTop.get(i));
					}
				}
			}catch(Exception e){

			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			if(ebag!=null&&childs!=null&&childs.size()>0)
				generateMap(ebag, childs, drawingView);
				else
					Toast.makeText(MindMapActivity.this,"invalid xml",Toast.LENGTH_SHORT).show();
			if(dialog!=null&&dialog.isShowing())
				dialog.dismiss();
		}

	}




	public void applyGradientMethod(String color) {
		// TODO Auto-generated method stub
		GradientDrawable gd = null;
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View gradientView = inflater.inflate(R.layout.mi_layout_gradient, null);
		popup = new PopupWindow(gradientView, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, true);
		popup.setBackgroundDrawable(new BitmapDrawable());
		popup.setOutsideTouchable(true);
		popup.setFocusable(true);
		if (popup.isShowing())
			popup.dismiss();
		int OFFSET_Y = 50;
		popup.showAtLocation(slidingDrawer, Gravity.BOTTOM
				| Gravity.CENTER_HORIZONTAL, (int) color1.getLeft(),
				viewNotes.getHeight() + slidingDrawer.getHeight() + OFFSET_Y);

		LinearLayout parentLayout, linearLayoutWhite, linearLayoutBlack;

		parentLayout = (LinearLayout) gradientView
				.findViewById(R.id.parentGradientLayout);
		linearLayoutWhite = new LinearLayout(this);
		linearLayoutBlack = new LinearLayout(this);

		int[] colorsWhite = { Color.parseColor(color),
				Color.parseColor("#FFFFFF") };
		int[] colorsBlack = { Color.parseColor(color),
				Color.parseColor("#000000") };

		LayoutParams layoutParamsWhite = new LayoutParams(
				LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		layoutParamsWhite.setMargins(2, 2, 2, 2);
		linearLayoutWhite.setLayoutParams(layoutParamsWhite);

		LayoutParams layoutParamsBlack = new LayoutParams(
				LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		layoutParamsBlack.setMargins(2, 10, 2, 2);
		linearLayoutBlack.setLayoutParams(layoutParamsBlack);

		LayoutParams layoutParams = new LayoutParams(
				80, 80);
		layoutParams.setMargins(10, 0, 0, 0);

		OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				int position = (Integer) v.getTag();
				if (nodeProp != null && clickedMpnodeView != null) {
					nodeProp.m_iGradientType = position;
					clickedMpnodeView.invalidate();

				}
			}
		};

		for (int i = 0; i < 16; i++) {
			Button g1 = new Button(this);

			g1.setTag(i + 1);
			g1.setOnClickListener(listener);
			g1.setTag(R.id.parentGradientLayout, color);
			if (i <= 7) {
				gd = new GradientDrawable(AppDict.gradientPositions[i],
						colorsWhite);
				g1.setBackground(gd);
				linearLayoutWhite.addView(g1, layoutParams);
			} else {
				gd = new GradientDrawable(AppDict.gradientPositions[i],
						colorsBlack);
				g1.setBackground(gd);
				linearLayoutBlack.addView(g1, layoutParams);
			}
		}

		parentLayout.addView(linearLayoutWhite);
		parentLayout.addView(linearLayoutBlack);

	}

	public void shapesClick(View v) {
		if (clickedMpnodeView != null && nodeProp != null) {
		switch (v.getId()) {
		case R.id.shapeRoundedRect:
			shapeRoundedRect
			.setBackgroundResource(R.drawable.mi_rounded_rectangle);
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_changeShapeStyle);
				nodeProp.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_ROUNDEDRECTANGLE;

				clickedMpnodeView.invalidate();

				resetWidth(clickedMpnodeView);
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;
		case R.id.shapePentagon:

			shapeRoundedRect.setBackgroundResource(R.drawable.mi_pentagon);
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_changeShapeStyle);
				nodeProp.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON;
				nodeProp.m_fHeight = nodeProp.m_fWidth;
				clickedMpnodeView.invalidate();
				resetWidth(clickedMpnodeView);
				// layout.setLayout(nodeProp);

			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;

		case R.id.shapeComment:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_comment);
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_changeShapeStyle);
				nodeProp.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_COMMENT;
				clickedMpnodeView.invalidate();
				resetWidth(clickedMpnodeView);
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;

		case R.id.shapeFreeForm:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_freeform);
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_changeShapeStyle);
				nodeProp.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_FREEFORM;
				clickedMpnodeView.invalidate();
				resetWidth(clickedMpnodeView);
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;

		case R.id.shapeRectangle:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_rectangle);
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_changeShapeStyle);
				nodeProp.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_RECTANGLE;
				clickedMpnodeView.invalidate();
				resetWidth(clickedMpnodeView);
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;

		case R.id.shapeOval:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_oval);
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_changeShapeStyle);
				nodeProp.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_OVAL;
				clickedMpnodeView.invalidate();
				resetWidth(clickedMpnodeView);
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;
		case R.id.shape_hexagon:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_octagon);
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_changeShapeStyle);
				nodeProp.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_OCTAGON;
				clickedMpnodeView.invalidate();
				resetWidth(clickedMpnodeView);
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;
		case R.id.shape_inverse_triangle:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_reversetriangle);
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_changeShapeStyle);
				nodeProp.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_INVERSE_TRIANGLE;
				clickedMpnodeView.invalidate();
				resetWidth(clickedMpnodeView);
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;
		case R.id.shape_rhombus:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_rhombus);
			if (clickedMpnodeView != null && nodeProp != null) {
				mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_changeShapeStyle);
				nodeProp.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_RHOMBUS;
				clickedMpnodeView.invalidate();
				resetWidth(clickedMpnodeView);
			} else
				Toast.makeText(MindMapActivity.this, "Select Topic",
						Toast.LENGTH_SHORT).show();
			break;
		}
		}
	}

	private void showShapesPopUp() {
		// TODO Auto-generated method stub
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View viewShapes = inflater.inflate(R.layout.mi_layout_shapes, null);

		popup = new PopupWindow(viewShapes, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, true);
		popup.setBackgroundDrawable(new BitmapDrawable());
		popup.setOutsideTouchable(true);
		popup.setFocusable(true);
		if (popup.isShowing())
			popup.dismiss();
		int OFFSET_Y = 50;
		popup.showAtLocation(slidingDrawer, Gravity.BOTTOM | Gravity.LEFT,
				(int) shapeRoundedRect.getLeft(), viewNotes.getHeight()
				+ slidingDrawer.getHeight() + OFFSET_Y);

	}

	private void showMapsPopUp() {
		// TODO Auto-generated method stub

		final MPLayout layout = new MPLayout(MindMapActivity.this);

		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View viewMaps = inflater.inflate(R.layout.mi_layout_maps, null);

		popup = new PopupWindow(viewMaps, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, true);

		btnCircularMap1 = (MindMapImageButton) viewMaps
				.findViewById(R.id.btnCircularMap1);
		btnLeftMap = (MindMapImageButton) viewMaps.findViewById(R.id.btnLeftMap);
		btnRghtMap = (MindMapImageButton) viewMaps.findViewById(R.id.btnRghtMap);
		btnFreeMap = (MindMapImageButton) viewMaps.findViewById(R.id.btnFreeMap);

		OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {

				case R.id.btnCircularMap1:

					btnCircularMap.setBackgroundResource(R.drawable.mi_circularmap);
					mPUndoManager.addUndooperationOnNode(ManahijApp.m_curEbag, UndoOperationKey.UndoOperationKey_changeMapStyle);
					ManahijApp.m_curEbag.m_iMapLayout = MapLayoutType.MPLayout_CircularMap;
					layout.setLayout(ManahijApp.m_curEbag);
					break;
				case R.id.btnLeftMap:

					btnCircularMap.setBackgroundResource(R.drawable.mi_leftmap);
					mPUndoManager.addUndooperationOnNode(ManahijApp.m_curEbag, UndoOperationKey.UndoOperationKey_changeMapStyle);
					ManahijApp.m_curEbag.m_iMapLayout = MapLayoutType.MPLayout_LeftMap;
					layout.setLayout(ManahijApp.m_curEbag);
					break;

				case R.id.btnRghtMap:

					btnCircularMap
					.setBackgroundResource(R.drawable.mi_rightmap);
					mPUndoManager.addUndooperationOnNode(ManahijApp.m_curEbag, UndoOperationKey.UndoOperationKey_changeMapStyle);
					ManahijApp.m_curEbag.m_iMapLayout = MapLayoutType.MPLayout_RightMap;
					layout.setLayout(ManahijApp.m_curEbag);
					break;

				case R.id.btnFreeMap:

					btnCircularMap.setBackgroundResource(R.drawable.mi_freemap);

					AlertDialog.Builder builder1 = new AlertDialog.Builder(
							MindMapActivity.this);
					builder1.setTitle("Info");
					builder1.setMessage("Do you want to keep same positions");
					builder1.setCancelable(true);
					builder1.setPositiveButton("yes",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int id) {
							dialog.cancel();
							mPUndoManager.addUndooperationOnNode(ManahijApp.m_curEbag, UndoOperationKey.UndoOperationKey_changeMapStyle);
							ManahijApp.m_curEbag.m_isKeepFreeMapSamePositions = true;
							ManahijApp.m_curEbag.m_iMapLayout = MapLayoutType.MPLayout_FreeMap;
							layout.setLayout(ManahijApp.m_curEbag);
						}
					});

					builder1.setNegativeButton("no",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int id) {
							dialog.cancel();
							FreeMapLayout freeMap = new FreeMapLayout(
									MindMapActivity.this);
							mPUndoManager.addUndooperationOnNode(ManahijApp.m_curEbag, UndoOperationKey.UndoOperationKey_changeMapStyle);
							ManahijApp.m_curEbag.m_isKeepFreeMapSamePositions = false;
							ManahijApp.m_curEbag.m_iMapLayout = MapLayoutType.MPLayout_FreeMap;
							ManahijApp.m_curEbag.m_isKeepFreeMapSamePositions = false;
							if (!ManahijApp.m_curEbag.m_isKeepFreeMapSamePositions) {
								if (ManahijApp.m_curEbag.m_arrChildren != null)
									freeMap.generatenewPositions(ManahijApp.m_curEbag);
							}
							layout.setLayout(ManahijApp.m_curEbag);

						}
					});

					AlertDialog alert11 = builder1.create();
					alert11.show();

					break;
				}

			}
		};
		btnLeftMap.setOnClickListener(listener);
		btnRghtMap.setOnClickListener(listener);
		btnFreeMap.setOnClickListener(listener);
		btnCircularMap1.setOnClickListener(listener);
		popup.setBackgroundDrawable(new BitmapDrawable());
		popup.setOutsideTouchable(true);
		popup.setFocusable(true);
		if (popup.isShowing())
			popup.dismiss();

		int OFFSET_X = 100;


		int OFFSET_Y = 50;
		popup.showAtLocation(slidingDrawer, Gravity.BOTTOM | Gravity.RIGHT,
				(int) btnCircularMap.getY() + OFFSET_X, viewNotes.getHeight()
				+ slidingDrawer.getHeight() + OFFSET_Y);

	}

	int _xDelta = 0, _yDelta = 0;

	public void setDisSelectedPanel() {
		GradientDrawable bgShape = (GradientDrawable) imagePanel
				.getBackground();
		bgShape.setColor(Color.parseColor("#6C466F"));
		GradientDrawable bgShape1 = (GradientDrawable) audioPanel
				.getBackground();
		bgShape1.setColor(Color.parseColor("#6C466F"));
		GradientDrawable bgShape2 = (GradientDrawable) videoPanel
				.getBackground();
		bgShape2.setColor(Color.parseColor("#6C466F"));
		GradientDrawable bgShape3 = (GradientDrawable) textPanel
				.getBackground();
		bgShape3.setColor(Color.parseColor("#6C466F"));
	}

	public void setSelectedViewPanel(TextView btn) {
		setDisSelectedPanel();
		GradientDrawable bgShape = (GradientDrawable) btn.getBackground();
		bgShape.setColor(Color.parseColor("#AC517C"));
	}

	LayoutInflater inflater;
	EditText editUrl,editWidth , editHeight;;
	MindMapImageButton loadFromUrl, loadFronLibrary;
	TextView txtAttachement, txtBackground, imagePanel, audioPanel, videoPanel,
	textPanel, loadImageLabel,clearAttachment;
	RelativeLayout calculateEmbededWidthHeight;
	public int REQUEST_TYPE = 0;
	Button applyDimens;

	public void showBackgroundImagePopUp(){
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		popup_container.removeAllViews();
		View view = inflater.inflate(R.layout.mi_layout_popup_pic_image, null);
		view.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		editUrl = (EditText) view.findViewById(R.id.editUrl);
		editUrl.requestFocus();
		loadFromUrl = (MindMapImageButton) view.findViewById(R.id.loadFromUrl);
		loadFronLibrary = (MindMapImageButton) view
				.findViewById(R.id.loadFronLibrary);
		loadImageLabel = (TextView) view.findViewById(R.id.loadImage);
		txtAttachement = (TextView) view.findViewById(R.id.txtAttachement);
		imagePanel = (TextView) view.findViewById(R.id.image);
		audioPanel = (TextView) view.findViewById(R.id.audio);
		videoPanel = (TextView) view.findViewById(R.id.video);
		textPanel = (TextView) view.findViewById(R.id.text);
		clearAttachment = (TextView) view.findViewById(R.id.clearAttachment);
		calculateEmbededWidthHeight = (RelativeLayout) view
				.findViewById(R.id.imageresizerLayout);
		editWidth = (EditText) view.findViewById(R.id.etWidth);
		editHeight = (EditText) view.findViewById(R.id.etHeight);
		applyDimens = (Button) view.findViewById(R.id.apply);
		imagePanel.setSelected(true);
		editUrl.setText(nodeProp.imageUrl);
		if (nodeProp.m_attachement_image != null
				|| nodeProp.m_background_image != null
				|| nodeProp.embededImage != null
				|| nodeProp.m_strMultimediaUrl.length()> 0
				){
			clearAttachment.setVisibility(View.VISIBLE);
		}else
			clearAttachment.setVisibility(View.GONE);






		if ( (nodeProp.m_background_image != null
				|| nodeProp.embededImage != null
				|| nodeProp.m_strMultimediaUrl.length()> 0)&&REQUEST_TYPE==0
				){
			calculateEmbededWidthHeight.setVisibility(View.VISIBLE);
			Bitmap tempMap=null;
			if(nodeProp.m_background_image!=null)
				tempMap=nodeProp.m_background_image;
			if(nodeProp.embededImage!=null)
				tempMap=nodeProp.embededImage;
			if(tempMap!=null){
				editWidth.setText(tempMap.getWidth()+"");
				editHeight.setText(tempMap.getHeight()+"");
			}
		}else
			calculateEmbededWidthHeight.setVisibility(View.GONE);

		setDisSelectedPanel();
		setSelectedViewPanel(imagePanel);
		REQUEST_TYPE = 0;
		OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.image:
					if (nodeProp.m_attachement_image != null
					|| nodeProp.m_background_image != null
					|| nodeProp.embededImage != null
					|| nodeProp.m_strMultimediaUrl.length()> 0
							)
						clearAttachment.setVisibility(View.VISIBLE);
					else
						clearAttachment.setVisibility(View.GONE);

					calculateEmbededWidthHeight.setVisibility(View.VISIBLE);

					REQUEST_TYPE = 0;
					setSelectedViewPanel(imagePanel);
					loadImageLabel.setVisibility(View.VISIBLE);
					loadFronLibrary.setVisibility(View.VISIBLE);
						editUrl.setText(nodeProp.imageUrl);
					break;
				case R.id.text:
					if(nodeProp.m_strHyperlinkUrl.length()>0){
						clearAttachment.setVisibility(View.VISIBLE);
					}else
						clearAttachment.setVisibility(View.GONE);
					calculateEmbededWidthHeight.setVisibility(View.GONE);
					REQUEST_TYPE = 1;
					setSelectedViewPanel(textPanel);
					loadImageLabel.setVisibility(View.INVISIBLE);
					loadFronLibrary.setVisibility(View.INVISIBLE);
						editUrl.setText(nodeProp.m_strHyperlinkUrl);
					break;
				case R.id.audio:
					if(nodeProp.multimediaurl.length()>0){
						clearAttachment.setVisibility(View.VISIBLE);
					}else
						clearAttachment.setVisibility(View.GONE);
					calculateEmbededWidthHeight.setVisibility(View.GONE);
					REQUEST_TYPE = 2;
					setSelectedViewPanel(audioPanel);
					loadImageLabel.setVisibility(View.VISIBLE);
					loadFronLibrary.setVisibility(View.VISIBLE);
						editUrl.setText(nodeProp.multimediaurl);
					break;
				case R.id.video:
					if(nodeProp.multimediaurl.length()>0){
						clearAttachment.setVisibility(View.VISIBLE);
					}else
						clearAttachment.setVisibility(View.GONE);
					REQUEST_TYPE = 3;
					calculateEmbededWidthHeight.setVisibility(View.GONE);
					setSelectedViewPanel(videoPanel);
					loadImageLabel.setVisibility(View.VISIBLE);
					loadFronLibrary.setVisibility(View.VISIBLE);
						editUrl.setText(nodeProp.multimediaurl);
					break;
				case R.id.loadFromUrl:
					if(isConnectingToInternet()){
						if (Patterns.WEB_URL.matcher(editUrl.getText().toString())
								.matches()) {
							if (REQUEST_TYPE == 1) {
								if (nodeProp != null && clickedMpnodeView != null)
									mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_hyperAttachment);
								nodeProp.setM_strHyperlinkUrl(editUrl.getText()
										.toString());
								clickedMpnodeView.invalidate();
							} else if(REQUEST_TYPE == 0){
								new LoadImage(0,0).execute(editUrl.getText().toString());

							}
							else if(REQUEST_TYPE ==3){
								String texturl=editUrl.getText().toString();
								if(texturl.contains(".mp4")|| texturl.contains(".3gp")  ){
									mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_videoattachment);
									nodeProp.multimediaurl=texturl;
									nodeProp.mediaType=3;
									resetWidth(clickedMpnodeView);
								}else
									Toast.makeText(getApplicationContext(), "Media format not supported.", Toast.LENGTH_SHORT).show();
							}else if(REQUEST_TYPE ==2){
								String texturl=editUrl.getText().toString();
								if(texturl.contains(".mp3")  ){
									mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_audioattachment);
									nodeProp.multimediaurl=texturl;
									nodeProp.mediaType=2;
									resetWidth(clickedMpnodeView);

								}
							}
						}
					} else
						Toast.makeText(MindMapActivity.this,
								"Invalid location", Toast.LENGTH_SHORT).show();
					removePopContainer();
					break;
				case R.id.loadFronLibrary:
					removePopContainer();
					loadImageFromLocal();
					break;
				case R.id.clearAttachment:
					removePopContainer();
					if (nodeProp != null && clickedMpnodeView != null) {
						switch (REQUEST_TYPE) {
						case 0:

							if(nodeProp.embededImage!=null){
								mPUndoManager.addUndooperationOnNode(
										nodeProp, UndoOperationKey.UndoOperationKey_imageEmbed);

								nodeProp.setEmbededImage(null);
							}else if(nodeProp.m_attachement_image!=null){
								mPUndoManager.addUndooperationOnNode(
										nodeProp, UndoOperationKey.UndoOperationKey_imageAttachment);
								nodeProp.setM_Attachement_Image(null);
							}else if(nodeProp.m_background_image!=null ){
								mPUndoManager.addUndooperationOnNode(
										nodeProp, UndoOperationKey.UndoOperationKey_imageAsNode);
								nodeProp.setM_background_image(null);
							}else if(nodeProp.m_strMultimediaUrl.length()>0){
								/*	mPUndoManager.addUndooperationOnNode(
										nodeProp,UndoOperationKey.UndoOperationKey_imageAttachment);*/
								nodeProp.m_strMultimediaUrl = "";
							}
							nodeProp.imageUrl="";
							nodeProp.base64image="";
							break;
						case 1:
							if(nodeProp.m_strHyperlinkUrl.length()>0){
								mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_hyperAttachment);
								nodeProp.m_strHyperlinkUrl="";

							}
							break;
						case 2:
							if(nodeProp.multimediaurl.length()>0){
								mPUndoManager.addUndooperationOnNode(
										nodeProp, UndoOperationKey.UndoOperationKey_audioattachment);
								nodeProp.multimediaurl="";

							}
							break;
						case 3:
							if(nodeProp.multimediaurl.length()>0){
								mPUndoManager.addUndooperationOnNode(
										nodeProp, UndoOperationKey.UndoOperationKey_videoattachment);
								nodeProp.multimediaurl="";

							}
							break;
						default:
							break;
						}
					}
					resetWidth(clickedMpnodeView);
					break;

				case R.id.apply:
					// Bitmap smileImg =
					// BitmapFactory.decodeFile(nodeProp.picturePath);

					if (nodeProp != null && clickedMpnodeView != null) {
						if(nodeProp.imageUrl.length()>0 || nodeProp.base64image.length()>0 || nodeProp.m_strMultimediaUrl.length()>0 ){


							String width = editWidth.getText().toString();
							String height = editHeight.getText().toString();
							if(width.length()>0 &&height.length()>0){
								int w = Integer.parseInt(width);
								int h = Integer.parseInt(height);
								if(h<5 || w<5){
									Toast.makeText(MindMapActivity.this,"Insufficient dimensions of image.",Toast.LENGTH_SHORT).show();
									return;
								}
								if(w>1500 || h>1500){
									Toast.makeText(getApplicationContext(), "Maximum width or height entered.", Toast.LENGTH_SHORT).show();
									return ;
								}
								InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
								inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
								Bitmap embeddImg = null;
								try {
									if(nodeProp.m_strMultimediaUrl.length()<=0 && nodeProp.imageUrl.length()<=0 && nodeProp.base64image.length()>0){
										embeddImg= AppDict.decodeBase64(nodeProp.base64image);
										embeddImg = Bitmap.createScaledBitmap(embeddImg,w, h,  false);

								}
								else if(nodeProp.m_strMultimediaUrl.length()>0 && 	isConnectingToInternet()){
									new LoadImage(w,h).execute(nodeProp.m_strMultimediaUrl);
								}else if(nodeProp.imageUrl.length()>=0)
									embeddImg = resizeImage(nodeProp.imageUrl, w, h);
							} catch (FileNotFoundException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							if(embeddImg==null){
								return;
							}
							switch (nodeProp.imageAttachementype) {
							case 1:
								mPUndoManager
								.addUndooperationOnNode(
										nodeProp,
										UndoOperationKey.UndoOperationKey_imageEmbed);
								nodeProp.setM_Attachement_Image(null);
								nodeProp.embededImage = embeddImg;
								nodeProp.m_background_image = null;
								//	clickedMpnodeView.calculateTextWidth();
								resetWidth(clickedMpnodeView);
								break;

							case 2:
								if (clickedMpnodeView != null && nodeProp != null) {
									mPUndoManager
									.addUndooperationOnNode(
											nodeProp,
											UndoOperationKey.UndoOperationKey_imageAttachment);
									nodeProp.setM_Attachement_Image(embeddImg);
									nodeProp.embededImage = null;
									nodeProp.m_background_image = null;
									resetWidth(clickedMpnodeView);
								}
								break;

							case 3:
								if (clickedMpnodeView != null && nodeProp != null) {
									mPUndoManager
									.addUndooperationOnNode(
											nodeProp,
											UndoOperationKey.UndoOperationKey_imageAsNode);
									nodeProp.setM_background_image(embeddImg);
									nodeProp.embededImage = null;
									//clickedMpnodeView.calculateTextWidth();
									resetWidth(clickedMpnodeView);
								}
								break;
							}


						}else
							Toast.makeText(MindMapActivity.this,"Insufficient dimensions of image.",Toast.LENGTH_SHORT).show();

					}
					} else {
						Toast.makeText(getApplicationContext(),
								"Please attach image to resize.",
								Toast.LENGTH_SHORT).show();
						return;
					}
					break;


				}

			}
		};

		imagePanel.setOnClickListener(listener);
		audioPanel.setOnClickListener(listener);
		videoPanel.setOnClickListener(listener);
		textPanel.setOnClickListener(listener);
		loadFromUrl.setOnClickListener(listener);
		clearAttachment.setOnClickListener(listener);
		loadFronLibrary.setOnClickListener(listener);
		applyDimens.setOnClickListener(listener);
		int OFFSET_Y = 50;
		popup_container.setVisibility(View.VISIBLE);
		RelativeLayout.LayoutParams parma=new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		parma.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM|RelativeLayout.ALIGN_PARENT_LEFT);
		parma.setMargins(0, 0, 0, 0);
		view.setLayoutParams(parma);
		popup_container.addView(view);
		RelativeLayout.LayoutParams relativelParms = (RelativeLayout.LayoutParams) popup_container.getLayoutParams();
		relativelParms.setMargins(viewPicimage.getLeft(), 0, 0, slidingDrawer.getHeight()+tapView.getHeight());
		relativelParms.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		relativelParms.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		popup_container.setLayoutParams(relativelParms);
	}
	public boolean isConnectingToInternet(){
		ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}

		}
		return false;
	}
	public  void removePopContainer(){
		popup_container.setVisibility(View.GONE);
		popup_container.removeAllViews();
		if(datechanged==true ){
			if( nodeProp.m_duration!=0 &&  nodeProp.m_duration_type!="" && addinfoundo != null){
				mPUndoManager.addUndoOperations(addinfoundo);
				addinfoundo = null;

			}
			datechanged=false;
		}
		isEnglish=false;
		if(resourcesedit!=null){
			resourcesedit.clearFocus();
		}
	}


	public  Bitmap resizeImage( String path,int reqWidth,int reqHeight) throws FileNotFoundException {

		Bitmap resizedimage = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		int imageHeight = options.outHeight;  //1800,2880
		int imageWidth = options.outWidth;
		int inSampleSize = 1;
		if (imageHeight > 1400 || imageWidth > 1400) {

			final int halfHeight = imageHeight / 2;
			final int halfWidth = imageWidth / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > 1200
					|| (halfWidth / inSampleSize) > 1200) {
				inSampleSize *= 2;
			}
		}
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		options.inSampleSize=inSampleSize;
		resizedimage= BitmapFactory.decodeFile(path, options);
		if(resizedimage!=null ){

			resizedimage = Bitmap.createScaledBitmap(resizedimage,reqWidth, reqHeight,  false);
		}
		return resizedimage;
	}

	public void setSelectedView(TextView btn) {
		setDisSelected();
		// ColorDrawable bgShape = (ColorDrawable) btn.getBackground();
		btn.setBackgroundColor(Color.parseColor("#6C466F"));
		// bgShape.setColor(Color.parseColor("#6C466F"));
	}

	public void setDisSelected() {
		ColorDrawable bgShape = (ColorDrawable) viewEdit.getBackground();

		bgShape.setColor(Color.parseColor("#AC517C"));

		ColorDrawable bgShape1 = (ColorDrawable) viewInsert.getBackground();

		bgShape1.setColor(Color.parseColor("#AC517C"));

		ColorDrawable bgShape2 = (ColorDrawable) viewNotes.getBackground();

		bgShape2.setColor(Color.parseColor("#AC517C"));

		ColorDrawable bgShape3 = (ColorDrawable) viewLayout.getBackground();

		bgShape3.setColor(Color.parseColor("#AC517C"));
	}

	// To animate view slide out from bottom to top
	public void slideVisibleAnimation(View view) {
		TranslateAnimation animate = new TranslateAnimation(0, 0, 0,
				view.getHeight());
		animate.setDuration(500);
		animate.setFillAfter(false);
		view.startAnimation(animate);
		view.setVisibility(View.GONE);
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(addText.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);

	}

	public void slideVisibleAnimation1(final View view) {
		TranslateAnimation animate = new TranslateAnimation(0, 0, 0,
				slidingDrawer.getHeight());

		animate.setDuration(500);
		animate.setFillAfter(false);
		view.startAnimation(animate);
		animate.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				RelativeLayout.LayoutParams dimens = (RelativeLayout.LayoutParams) navigate
						.getLayoutParams();
				// dimens.bottomMargin = - slidingDrawer.getHeight();
				int subDimens = dimens.bottomMargin;
				dimens.bottomMargin = subDimens - slidingDrawer.getHeight();
				navigate.setLayoutParams(dimens);
			}
		});
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(addText.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);

	}

	public void slideGoneAnimation1(View view) {
		TranslateAnimation animate = new TranslateAnimation(0, 0,
				slidingDrawer.getHeight(), 0);
		RelativeLayout.LayoutParams dimens = (RelativeLayout.LayoutParams) view
				.getLayoutParams();
		int height = 0;
		if (slidingDrawer.getHeight() == 0)
			height = 213;
		else
			height = slidingDrawer.getHeight();
		dimens.bottomMargin = height;
		animate.setDuration(300);
		animate.setFillAfter(false);
		view.startAnimation(animate);
		view.setVisibility(View.VISIBLE);
		animate.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				tapView.setVisibility(View.GONE);
				// linearGrid.setVisibility(View.GONE);
				calculateDimensions();
				LayoutParams parms = new LayoutParams(width
						- (2 * labelNotes.getLayoutParams().width + 20),
						editAddNotes.getLayoutParams().height);
				parms.leftMargin = 12;
				editAddNotes.setLayoutParams(parms);

			}
		});
	}

	public void slideGoneAnimation(View view) {
		TranslateAnimation animate = new TranslateAnimation(0, 0,
				view.getHeight(), 0);
		animate.setDuration(300);
		animate.setFillAfter(false);
		view.startAnimation(animate);
		view.setVisibility(View.VISIBLE);
		animate.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				tapView.setVisibility(View.GONE);
				// linearGrid.setVisibility(View.GONE);
				calculateDimensions();
				LayoutParams parms = new LayoutParams(width
						- (2 * labelNotes.getLayoutParams().width + 20),
						editAddNotes.getLayoutParams().height);
				parms.leftMargin = 12;
				editAddNotes.setLayoutParams(parms);

			}
		});
	}

	public void calculateDimensions() {
		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
		width = displayMetrics.widthPixels;
		height = displayMetrics.heightPixels;
		// measuredWidth = bPrevoius.getMeasuredWidth();
		measuredHeight = editAddNotes.getMeasuredHeight();
	}

	int measuredWidth = 0, measuredHeight = 0;

	Point p;
	PopupWindow popup;

	public static void hideMenuItem() {
		if (popUpLayout != null && popUpLayout.getVisibility() == View.VISIBLE) {
			// animate(true);
			// animateRadialMenu(false);
			popUpLayout.setVisibility(View.GONE);
		}
	}

	public static View popUpLayout;

	// pop up view
	MindMapImageButton delButton, addButton, collapseButton;

	public void openPopUpMenu(Activity context, View v) {
		int[] location = new int[2];
		MPNodeView view = (MPNodeView) v;
		v.getLocationOnScreen(location);
		if(clickedMpnodeView!=null){
		switch (clickedMpnodeView.m_node.m_iMapLayoutShape)
		{
		case MPLAYOUT_SHAPE_ROUNDEDRECTANGLE:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_rounded_rectangle);
			break;

		case MPLAYOUT_SHAPE_RECTANGLE:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_rectangle);
			break;

		case MPLAYOUT_SHAPE_COMMENT:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_comment);
			break;

		case MPLAYOUT_SHAPE_OVAL:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_oval);
			break;

		case MPLAYOUT_SHAPE_PENTAGON:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_pentagon);
			break;

		case MPLAYOUT_SHAPE_FREEFORM:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_freeform);
			break;
			/*CHANGES BY ANITA*/
		case MPLAYOUT_SHAPE_OCTAGON:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_octagon);
			break;

		case MPLAYOUT_SHAPE_INVERSE_TRIANGLE:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_reversetriangle);
			break;

		case MPLAYOUT_SHAPE_RHOMBUS:
			shapeRoundedRect.setBackgroundResource(R.drawable.mi_rhombus);
			break;
			/***********/
		default:
			break;
		}
		}
		// Initialize the Point with x, and y positions
		p = new Point();
		p.x = location[0];
		p.y = location[1];
		int popupWidth = 100;
		int popupHeight = 500;
		if (popUpLayout != null && popUpLayout.getVisibility() == View.GONE) {
			drawingView.removeView(popUpLayout);
			popUpLayout = null;
		}
		if (popUpLayout == null) {
			popUpLayout = showRadialMenu(view);

			popup = new PopupWindow(context);
			popup.setContentView(popUpLayout);
			popup.setBackgroundDrawable(new BitmapDrawable());
			popup.setWidth(popupWidth);
			popup.setHeight(popupHeight);
			popup.setFocusable(true);
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
					130, 180);
			lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			if (view.m_node.m_isRightnode || view.m_node.m_iLevelId == 0) {
				if (view.m_node.m_fWidth <= 220) {
					// lp.leftMargin = (int) /* p.x */view.m_node.m_fX -
					// v.getWidth()/ 5;
					lp.leftMargin = (int) /* p.x */view.m_node.m_fX
							+ v.getWidth()
							- popUpLayout.getLayoutParams().width / 4/*- v.getWidth()/ 5*/;
				} else {
					// lp.leftMargin = (int) /* p.x */view.m_node.m_fX +
					// v.getWidth()/ 7;
					lp.leftMargin = (int) /* p.x */view.m_node.m_fX
							+ v.getWidth()
							- popUpLayout.getLayoutParams().width / 4/*- v.getWidth()/ 5*/;
				}
			} else {
				if (view.m_node.m_fWidth <= 220) {
					// lp.leftMargin = (int) /* p.x */view.m_node.m_fX -
					// v.getWidth()/ 5;
					lp.leftMargin = (int) /* p.x */view.m_node.m_fX
							- popUpLayout.getLayoutParams().width / 2/*- v.getWidth()/ 5*/;
				} else {
					// lp.leftMargin = (int) /* p.x */view.m_node.m_fX +
					// v.getWidth()/ 7;
					lp.leftMargin = (int) /* p.x */view.m_node.m_fX
							- popUpLayout.getLayoutParams().width / 2/*- v.getWidth()/ 5*/;
				}
			}
			// lp.topMargin = (int) /* p.y */view.m_node.m_fY + v.getHeight() +
			// 10;
			lp.topMargin = (int) /* p.y */view.m_node.m_fY
					- popUpLayout.getLayoutParams().height / 2
					+ view.m_node.m_fHeight / 2 - 10/* + v.getHeight() + 10 */;
			//drawingView.addView(popUpLayout, lp);
			drawingView.addView(popUpLayout, lp);
		} else {
			popUpLayout = showRadialMenu(view);
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
					130, 180);
			if (view.m_node.m_fWidth <= 220) {
				lp.leftMargin = (int) /* p.x */view.m_node.m_fX + v.getWidth()
						+ 10;
			} else {
				lp.leftMargin = (int) view.m_node.m_fX + v.getWidth()
						/ 7;
			}
			lp.topMargin = (int) view.m_node.m_fY
					- popUpLayout.getHeight() / 2 + view.m_node.m_fHeight / 2;
			lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			popUpLayout.setLayoutParams(lp);
			popUpLayout.setVisibility(View.VISIBLE);
			popUpLayout.bringToFront();
			popUpLayout.invalidate();
		}
	}

	public void openPopUpMenu1(Activity context, View v) {
		int[] location = new int[2];
		MPNodeView view = (MPNodeView) v;
		v.getLocationOnScreen(location);
		// Initialize the Point with x, and y positions
		p = new Point();
		p.x = location[0];
		p.y = location[1];
		int popupWidth = 100;
		int popupHeight = 500;

		if (popUpLayout == null) {
			LayoutInflater layoutInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			popUpLayout = layoutInflater.inflate(
					R.layout.mi_layout_popup_select, null);
			addButton = (MindMapImageButton) popUpLayout.findViewById(R.id.buttonAdd);
			delButton = (MindMapImageButton) popUpLayout
					.findViewById(R.id.buttonClose);
			collapseButton = (MindMapImageButton) popUpLayout
					.findViewById(R.id.buttonExpand);
			addButton.setOnClickListener(popupClickListener);
			delButton.setOnClickListener(popupClickListener);
			collapseButton.setOnClickListener(popupClickListener);
			// Creating the PopupWindow
			popup = new PopupWindow(context);
			popup.setContentView(popUpLayout);
			popup.setBackgroundDrawable(new BitmapDrawable());
			popup.setWidth(popupWidth);
			popup.setHeight(popupHeight);
			popup.setFocusable(true);
			if (view.m_node.m_iLevelId == 0)
				delButton.setEnabled(false);
			else
				delButton.setEnabled(true);
			if (view.m_node.m_iLevelId == -1) {
				addButton.setEnabled(false);
				collapseButton.setEnabled(false);
			} else {
				addButton.setEnabled(true);
				collapseButton.setEnabled(true);
			}
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
					180, 270);
			lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			if (view.m_node.m_fWidth <= 220) {
				lp.leftMargin = (int) view.m_node.m_fX - v.getWidth()
						/ 5;
			} else {
				lp.leftMargin = (int) view.m_node.m_fX + v.getWidth()
						/ 7;
			}
			lp.topMargin = (int) view.m_node.m_fY + v.getHeight() + 10;

			drawingView.addView(popUpLayout, lp);
		} else {
			if (view.m_node.m_iLevelId == 0)
				delButton.setEnabled(false);
			else
				delButton.setEnabled(true);
			if (view.m_node.m_iLevelId == -1) {
				addButton.setEnabled(false);
				collapseButton.setEnabled(false);
			} else {
				addButton.setEnabled(true);
				collapseButton.setEnabled(true);
			}
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
					180, 270);
			if (view.m_node.m_fWidth <= 220) {
				lp.leftMargin = (int) view.m_node.m_fX + v.getWidth()
						+ 10;
			} else {
				lp.leftMargin = (int) view.m_node.m_fX + v.getWidth()
						/ 7;
			}
			lp.topMargin = (int) view.m_node.m_fY
					- popUpLayout.getHeight() / 2 + view.m_node.m_fHeight / 2;
			lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			popUpLayout.setLayoutParams(lp);
			popUpLayout.setVisibility(View.VISIBLE);
			popUpLayout.bringToFront();
			popUpLayout.invalidate();
		}

	}

	public OnClickListener popupClickListener = new OnClickListener() {

		MPLayout mpLayout = new MPLayout(MindMapActivity.this);

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.buttonAdd:
				if (nodeProp.m_parentNode != null)
					popUpLayout.setVisibility(View.GONE);
				// popup.dismiss();
				// v.getParent().requestDisallowInterceptTouchEvent(true);
				MPNode mpNodeView = nodeProp;
				mpLayout.addNewNodeOf(clickedMpnodeView);
				break;
			case R.id.buttonClose:
				popUpLayout.setVisibility(View.GONE);
				// popup.dismiss();
				if (clickedMpnodeView.m_node.m_iLevelId != 0) {
					mPUndoManager.addUndooperationOnNode(
							clickedMpnodeView.m_node,
							UndoOperationKey.UndoOperationKey_addNode);
					mpLayout.deleteNodeOf(clickedMpnodeView);
				}
				break;
			case R.id.buttonExpand:
				popUpLayout.setVisibility(View.GONE);
				// popup.dismiss();
				mPUndoManager.addUndooperationOnNode(nodeProp,
						UndoOperationKey.UndoOperationKey_expand);
				mpLayout.collapseNodeOf(clickedMpnodeView);
				// mpLayout.setLayout(nodeProp);
			default:
				break;
			}
		}
	};

	private ArrayList<String> readAllFonts() {

		Typeface tfArial,tfCourier,tfHelvetica,tfOpensans,tfTahoma,tfTimes,tfVerdana;

		ArrayList<String> fontNames = new ArrayList<String>();

		File temp = new File("/system/fonts/");

		String fontSuffix = ".ttf";

		tfArial = Typeface.createFromAsset(getAssets(),"fontfamily/Arial.ttf");
		tfCourier = Typeface.createFromAsset(getAssets(),"fontfamily/Courier.ttf");
		tfHelvetica = Typeface.createFromAsset(getAssets(),"fontfamily/Helvetica.ttf");
		tfOpensans = Typeface.createFromAsset(getAssets(),"fontfamily/Open Sans.ttf");
		tfTahoma = Typeface.createFromAsset(getAssets(),"fontfamily/Tahoma.ttf");
		tfTimes = Typeface.createFromAsset(getAssets(),"fontfamily/Times New Roman.ttf");
		tfVerdana = Typeface.createFromAsset(getAssets(),"fontfamily/Verdana.ttf");

		fontNames.add("Arial");
		fontNames.add("Courier");
		fontNames.add("Helvetica");
		fontNames.add("Open Sans");
		fontNames.add("Tahoma");
		fontNames.add("Times New Roman");
		fontNames.add("Verdana");

		for (File font : temp.listFiles()) {

			String fontName = font.getName();

			if (fontName.endsWith(fontSuffix)) {

				fontNames.add(fontName.subSequence(0,
						fontName.lastIndexOf(fontSuffix)).toString());

			}

		}

		return fontNames;

	}

	ImageView img;
	Bitmap bitmap;
	ProgressDialog pDialog;

	private class LoadImage extends AsyncTask<String, String, Bitmap> {
		String mediaurl="";
		int widthimg,heightimg;
		public LoadImage(int width,int height){
			this.widthimg=width;
			this.heightimg=height;
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MindMapActivity.this);
			pDialog.setMessage("Loading Image ....");
			pDialog.show();
		}

		protected Bitmap doInBackground(String... args) {
			mediaurl=args[0];
			try {
				bitmap=decodeimageFromURL(mediaurl, widthimg,heightimg);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return bitmap;
		}

		protected void onPostExecute(final Bitmap image) {
			if (image != null) {

				pDialog.dismiss();
				if(widthimg==0){
					final Dialog dialog = new Dialog(MindMapActivity.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.mi_image_attachement_options);
					// dialog.setTitle("Do you want to use selected attachement as");
					dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					// set the custom dialog components - text, image and button
					MindMapButton txtEmbed = (MindMapButton) dialog.findViewById(R.id.txtEmbed);
					MindMapButton txtAttachement = (MindMapButton) dialog.findViewById(R.id.txtAttachement);
					MindMapButton txtImgAsNode = (MindMapButton) dialog.findViewById(R.id.txtImgAsNode);

					// if button is clicked, close the custom dialog
					OnClickListener listener = new OnClickListener() {
						@Override
						public void onClick(View v)
						{
							nodeProp.imageUrl=mediaurl;
							nodeProp.propertiesPositions.remove("2");
							switch (v.getId()) {
							case R.id.txtEmbed:
								mPUndoManager.addUndooperationOnNode(nodeProp,
										UndoOperationKey.UndoOperationKey_imageEmbed);
								// embeddImg= Bitmap.createScaledBitmap(embeddImg,
								// 500, 500, true);
								nodeProp.setM_Attachement_Image(null);
								nodeProp.embededImage = image;
								nodeProp.m_background_image = null;
								nodeProp.imageAttachementype = 1;
								nodeProp.mediaType = 0;
								//clickedMpnodeView.invalidate();
								//clickedMpnodeView.calculateWidth();
								resetWidth(clickedMpnodeView);
								break;
							case R.id.txtAttachement:
									mPUndoManager.addUndooperationOnNode(nodeProp,
											UndoOperationKey.UndoOperationKey_imageAttachment);
									nodeProp.setM_Attachement_Image(image);
									clickedMpnodeView.imageAttachementBitmap = null;
									nodeProp.mediaType = 0;
									nodeProp.embededImage = null;
									nodeProp.m_background_image = null;
									nodeProp.imageAttachementype = 2;
									//	clickedMpnodeView.invalidate();
									resetWidth(clickedMpnodeView);
								break;
							case R.id.txtImgAsNode:
									mPUndoManager
									.addUndooperationOnNode(
											nodeProp,
											UndoOperationKey.UndoOperationKey_imageAsNode);
									nodeProp.setM_background_image(image);
									nodeProp.mediaType = 0;
									// clickedMpnodeView.invalidate();
									nodeProp.imageAttachementype = 3;
									nodeProp.embededImage = null;
									//clickedMpnodeView.calculateWidth();
									resetWidth(clickedMpnodeView);
									// mplayout.setLayout(ManahijApp.m_curEbag);
								break;
							default:
								break;
							}
							dialog.dismiss();
							nodeProp.m_strMultimediaUrl=mediaurl;
							nodeProp.multimediaurl ="";
							nodeProp.imageUrl="";
							nodeProp.base64image="";
						}
					};
					txtEmbed.setOnClickListener(listener);
					txtAttachement.setOnClickListener(listener);
					txtImgAsNode.setOnClickListener(listener);
					dialog.show();
				}else{
					switch (nodeProp.imageAttachementype) {
					case 1:
						mPUndoManager.addUndooperationOnNode(nodeProp,
								UndoOperationKey.UndoOperationKey_imageEmbed);
						nodeProp.embededImage = image;

						resetWidth(clickedMpnodeView);
						break;
					case 2:
						mPUndoManager.addUndooperationOnNode(nodeProp,
								UndoOperationKey.UndoOperationKey_imageAttachment);
						nodeProp.setM_Attachement_Image(image);
						resetWidth(clickedMpnodeView);
						break;
					case 3:
						mPUndoManager
						.addUndooperationOnNode(
								nodeProp,
								UndoOperationKey.UndoOperationKey_imageAsNode);
						nodeProp.setM_background_image(image);
						resetWidth(clickedMpnodeView);
						break;
					default:
						break;
					}
				}
			} else {
				pDialog.dismiss();
				Toast.makeText(MindMapActivity.this,
						"Image Does Not exist or Network Error",
						Toast.LENGTH_SHORT).show();
			}
		}
	}
	private static Bitmap decodeimageFromURL(String path, int maxWidth, int maxHeight) throws MalformedURLException, IOException {
		Bitmap bitmap = null;

		try {
			bitmap = BitmapFactory.decodeStream((InputStream) new URL(path).getContent());
		} catch (OutOfMemoryError e) {
			// TODO: handle exception
			return null;
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		if(maxWidth==0 || maxHeight==0){
			maxWidth=700;
			maxHeight=700;
		}else{
			bitmap = Bitmap.createScaledBitmap(bitmap, maxWidth, maxHeight, true);
		}
		if (maxHeight > 0 && maxWidth > 0) {
			int width = bitmap.getWidth();
			int height = bitmap.getHeight();
			if(width<maxWidth && height<maxHeight){
				return bitmap;
			}
			float ratioBitmap = (float) width / (float) height;
			float ratioMax = (float) maxWidth / (float) maxHeight;

			int finalWidth = maxWidth;
			int finalHeight = maxHeight;
			if (ratioMax > 1) {
				finalWidth = (int) ((float)maxHeight * ratioBitmap);
			} else {
				finalHeight = (int) ((float)maxWidth / ratioBitmap);
			}
			bitmap = Bitmap.createScaledBitmap(bitmap, finalWidth, finalHeight, true);
			return bitmap;
		} else {
			return bitmap;
		}
	}
	public void loadImageFromLocal() {
		Intent i = null;
		if (REQUEST_TYPE == 0) {
			i = new Intent(
					Intent.ACTION_PICK,
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			i.setType("image/*");
			try{
				startActivityForResult(i, 111);
			}catch(ActivityNotFoundException e){
				Toast.makeText(MindMapActivity.this,"Activity Not Found",Toast.LENGTH_SHORT).show();

			}
		} else if (REQUEST_TYPE == 2) {
			i = new Intent(
					Intent.ACTION_GET_CONTENT,
					MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
			i.setType("audio/*");
			// i.setAction(
			try{
				startActivityForResult(i, 222);
			}catch(ActivityNotFoundException e){
				Toast.makeText(MindMapActivity.this,"Activity Not Found",Toast.LENGTH_SHORT).show();

			}
		} else if (REQUEST_TYPE == 3) {
			i = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

			i.setType("video/*");
			//i.setAction(Intent.ACTION_GET_CONTENT);
			try{
				startActivityForResult(i, 333);
			}catch(ActivityNotFoundException e){
				Toast.makeText(MindMapActivity.this,"Activity Not Found",Toast.LENGTH_SHORT).show();

			}
		}

	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (popup != null && popup.isShowing())
			popup.dismiss();
		if (resultCode == RESULT_OK && null != data) {
			if (resultCode == RESULT_OK && null != data) {
				switch (requestCode) {
				case 111:
					// handle image

					final Uri selectedImage = data.getData();
					if (selectedImage.toString() == null
							|| selectedImage.toString() == "") {
						return;
					}
					final MPUndoObject mpundoObj = new MPUndoObject();
					mpundoObj.objold = nodeProp;
					mpundoObj.objnew = nodeProp.copy();
					nodeProp.imageUrl=getRealPathFromURI(selectedImage);
					final String picturePath = getRealPathFromURI(selectedImage);
					if(picturePath==null || picturePath==""){
						return;
					}
					final Dialog dialog = new Dialog(MindMapActivity.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.mi_image_attachement_options);
					// dialog.setTitle("Are you want to use selected attachement as");
					dialog.getWindow().setBackgroundDrawable(
							new ColorDrawable(
									Color.TRANSPARENT));
					// set the custom dialog components - text, image and button
					MindMapButton txtEmbed = (MindMapButton) dialog
							.findViewById(R.id.txtEmbed);
					MindMapButton txtAttachement = (MindMapButton) dialog
							.findViewById(R.id.txtAttachement);
					MindMapButton txtImgAsNode = (MindMapButton) dialog
							.findViewById(R.id.txtImgAsNode);

					// if button is clicked, close the custom dialog
					OnClickListener listener = new OnClickListener() {

						@Override
						public void onClick(View v) {


							int orientation=getCameraPhotoOrientation(selectedImage, nodeProp.imageUrl);
							Bitmap embeddImg=null;
							try
							{
								embeddImg = decodeimage(nodeProp.imageUrl, 600, 600, orientation);
							}
							catch (FileNotFoundException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							if(embeddImg==null ){
								dialog.dismiss();
								return;
							}


							nodeProp.propertiesPositions.remove("2");
							switch (v.getId()) {
							case R.id.txtEmbed:
								mpundoObj.keyType= UndoOperationKey.UndoOperationKey_imageEmbed;
								mPUndoManager.addUndoOperations(mpundoObj);
								//	Bitmap embeddImg = BitmapFactory.decodeFile(picturePath);
								// embeddImg=
								// Bitmap.createScaledBitmap(embeddImg,
								// 500, 500, true);


								nodeProp.multimediaurl = "";
								nodeProp.base64image = "";
								nodeProp.setM_Attachement_Image(null);
								nodeProp.embededImage = embeddImg;
								nodeProp.m_background_image = null;
								nodeProp.imageAttachementype = 1;
								nodeProp.m_strMultimediaUrl ="";
								nodeProp.mediaType = 0;
								resetWidth(clickedMpnodeView);
								break;
							case R.id.txtAttachement:

								if (clickedMpnodeView != null
								&& nodeProp != null) {
									mpundoObj.keyType= UndoOperationKey.UndoOperationKey_imageAttachment;
									mPUndoManager.addUndoOperations(mpundoObj);
									nodeProp.multimediaurl = "";
									nodeProp.setM_Attachement_Image(embeddImg);
									clickedMpnodeView.imageAttachementBitmap = null;
									nodeProp.mediaType = 0;
									nodeProp.embededImage = null;
									nodeProp.m_background_image = null;
									nodeProp.imageAttachementype = 2;
									nodeProp.m_strMultimediaUrl ="";
									nodeProp.base64image = "";
									resetWidth(clickedMpnodeView);
								}
								break;
							case R.id.txtImgAsNode:

								if (clickedMpnodeView != null
								&& nodeProp != null) {
									mpundoObj.keyType= UndoOperationKey.UndoOperationKey_imageAsNode;
									mPUndoManager.addUndoOperations(mpundoObj);
									nodeProp.multimediaurl = "";
									nodeProp.setM_background_image(embeddImg);
									nodeProp.mediaType = 0;
									// clickedMpnodeView.invalidate();
									nodeProp.imageAttachementype = 3;
									nodeProp.embededImage = null;
									nodeProp.m_strMultimediaUrl ="";
									nodeProp.base64image = "";
									nodeProp.setM_Attachement_Image(null);
									resetWidth(clickedMpnodeView);
									// mplayout.setLayout(ManahijApp.m_curEbag);
								}
								break;
							default:
								break;
							}
							dialog.dismiss();
						}
					};
					txtEmbed.setOnClickListener(listener);
					txtAttachement.setOnClickListener(listener);
					txtImgAsNode.setOnClickListener(listener);
					dialog.show();

					break;
				case 222:
					Uri selectedaudio = null; // content://media/external/images/media/8196
					String audiopath = null;
					try {
						selectedaudio = data.getData();
					} catch (NullPointerException e) {
						// TODO: handle exception
						selectedaudio = null;
					} catch (Exception e) {
						// TODO: handle exception

					}
					boolean value=false;
					if(selectedaudio.buildUpon().toString().contains("content://com.google.android.apps.photos.contentprovider/")){
						if(selectedaudio.getPath().split("/")[1].equalsIgnoreCase("0")){
							value=true;
							Toast.makeText(MindMapActivity.this, "Select from device",Toast.LENGTH_SHORT).show();
						}
					}
					if (selectedaudio == null || selectedaudio.equals("")||value) {
						return;
					}

					// audiopath =getRealPathFromURI(selectedaudio);
					audiopath = selectedaudio.toString();

					// sound file
					if (clickedMpnodeView != null && nodeProp != null) {
						mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_audioattachment);
						nodeProp.m_attachement_image=null;
						nodeProp.embededImage=null;
						nodeProp.m_background_image=null;
						nodeProp.mediaType = 2;
						nodeProp.m_strMultimediaUrl="";
						nodeProp.base64image = "";
						nodeProp.imageUrl="";
						if(getRealPathFromUriAudio(data.getData())!=null)
							nodeProp.multimediaurl = getRealPathFromUriAudio(data.getData());//audiopath;
						else
							nodeProp.multimediaurl=selectedaudio.toString();
						resetWidth(clickedMpnodeView);
					}
					break;
				case 333:
					Uri selectedvideo = null; // content://media/external/images/media/8196
					String videopath = null;
					try {
						selectedvideo = data.getData();

					} catch (NullPointerException e) {
						// TODO: handle exception
						selectedvideo = null;
					} catch (Exception e) {
						// TODO: handle exception

					}
					value=false;
					if(selectedvideo==null || selectedvideo.equals("")){
						return;
					}
					if(selectedvideo.buildUpon().toString().contains("content://com.google.android.apps.photos.contentprovider/")){
						if(selectedvideo.getPath().split("/")[1].equalsIgnoreCase("0")){
							value=true;
							Toast.makeText(MindMapActivity.this, "Select from device",Toast.LENGTH_SHORT).show();
						}
					}
					if (selectedvideo == null || selectedvideo.equals("")||value) {
						return;
					}
					if (clickedMpnodeView != null && nodeProp != null ) {
						mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_videoattachment);
						nodeProp.multimediaurl=selectedvideo.toString();//picturePath;
						nodeProp.m_attachement_image=null;
						nodeProp.m_background_image=null;
						nodeProp.embededImage=null;
						nodeProp.m_strMultimediaUrl="";
						nodeProp.imageUrl="";
						nodeProp.base64image = "";
						if(getRealPathFromUriVideo(data.getData())!=null)
							nodeProp.multimediaurl = getRealPathFromUriVideo(data.getData());//selectedvideo.toString();// picturePath;
						else
							nodeProp.multimediaurl =selectedvideo.toString();;
							nodeProp.mediaType = 3;
							resetWidth(clickedMpnodeView);
					}
					break;
				}

			}
		}

	}


	public  int getCameraPhotoOrientation( Uri imageUri, String imagePath){
		int rotate = 0;
		try {
			getContentResolver().notifyChange(imageUri, null);
			File imageFile = new File(imagePath);
			ExifInterface exif = new ExifInterface(
					imageFile.getAbsolutePath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		return rotate;
	}
	public  Bitmap decodeimage( String path,int reqWidth,int reqHeight,int orientation)
			throws FileNotFoundException {
		Bitmap resizedimage = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);
		int imageHeight = options.outHeight;  //1800,2880
		int imageWidth = options.outWidth;
		int inSampleSize = 1;
		if (imageHeight > reqHeight || imageWidth > reqWidth) {

			final int halfHeight = imageHeight / 2;
			final int halfWidth = imageWidth / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					|| (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		options.inSampleSize=inSampleSize;
		resizedimage= BitmapFactory.decodeFile(path, options);
		if(resizedimage!=null && orientation!=0){
			Matrix matrix = new Matrix();
			matrix.postRotate(orientation);
			resizedimage = Bitmap.createBitmap(resizedimage, 0, 0, resizedimage.getWidth(), resizedimage.getHeight(), matrix, false);
		}
		return resizedimage;
	}
	public String getRealPathFromUriAudio(Uri contentUri)
	{
		String[] proj = { MediaStore.Audio.Media.DATA };
		Cursor cursor = managedQuery(contentUri, proj, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	public String getRealPathFromUriVideo(Uri contentUri)
	{
		try
		{
			String[] proj = {MediaStore.Video.Media.DATA};
			Cursor cursor = managedQuery(contentUri, proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		catch (Exception e)
		{
			return contentUri.getPath();
		}
	}
	public String getRealPathFromURI(Uri contentUri) {
		Cursor cursor = null;
		int column_index = 0;
		String path;
		try {
			if (contentUri.getPath().contains("/external/image")
					|| contentUri.getPath().contains("/internal/image")) {
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				cursor = getContentResolver().query(contentUri, filePathColumn,
						null, null, null);
				cursor.moveToFirst();
				column_index = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			} else if (contentUri.getPath().contains("video")) {
				String[] filePathColumn = { MediaStore.Video.VideoColumns.DATA };
				cursor = getContentResolver().query(contentUri, filePathColumn,
						null, null, null);
				cursor.moveToFirst();
				column_index = cursor
						.getColumnIndexOrThrow(MediaStore.Video.VideoColumns.DATA);
			} else if (contentUri.getPath().contains("/external/audio")
					|| contentUri.getPath().contains("/internal/audio")) {
				String[] filePathColumn = { MediaStore.Audio.AudioColumns.DATA };
				cursor = getContentResolver().query(contentUri, filePathColumn,
						null, null, null);
				cursor.moveToFirst();
				column_index = cursor
						.getColumnIndexOrThrow(MediaStore.Audio.AudioColumns.DATA);
			} else
				return contentUri.getPath();

			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	private boolean timeChanged = false;
	private boolean timeScrolled = false;

	public void showCustomSpinner() {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.mi_layout_custom_spinner, null);

		popup = new PopupWindow(view, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, true);
		final AbstractWheel fontSize = (AbstractWheel) view
				.findViewById(R.id.hour);
		// final NumericWheelAdapter numericWheelAdapter = new
		// NumericWheelAdapter(this, 5, 23);
		FontAdapter adapterFontSizes = new FontAdapter(MindMapActivity.this,
				AppDict.fontsizes, false);

		fontSize.setViewAdapter(adapterFontSizes);
		// fontSize.setCyclic(true);

		final AbstractWheel fontFace = (AbstractWheel) view
				.findViewById(R.id.mins);
		FontAdapter adapte = new FontAdapter(MindMapActivity.this, fontsList,
				true);
		fontFace.setViewAdapter(adapte);
		// fontFace.setCyclic(true);

		// fontSize.setCurrentItem(10);
		// fontFace.setCurrentItem(1);
		textColor1 = (CircleButton) view.findViewById(R.id.textColor1);
		textColor2 = (CircleButton) view.findViewById(R.id.textColor2);
		textColor3 = (CircleButton) view.findViewById(R.id.textColor3);
		textColor4 = (CircleButton) view.findViewById(R.id.textColor4);
		textColor5 = (CircleButton) view.findViewById(R.id.textColor5);
		textColor6 = (CircleButton) view.findViewById(R.id.textColor6);
		textColor7 = (CircleButton) view.findViewById(R.id.textColor7);
		textColor8 = (CircleButton) view.findViewById(R.id.textColor8);
		textColor9 = (CircleButton) view.findViewById(R.id.textColor9);
		textColor10 = (CircleButton) view.findViewById(R.id.textColor10);
		textColor11 = (CircleButton) view.findViewById(R.id.textColor11);
		textColor12 = (CircleButton) view.findViewById(R.id.textColor12);
		textColor13 = (CircleButton) view.findViewById(R.id.textColor13);
		textColor14 = (CircleButton) view.findViewById(R.id.textColor14);
		textColor15 = (CircleButton) view.findViewById(R.id.textColor15);
		textColor16 = (CircleButton) view.findViewById(R.id.textColor16);
		textColor1.setOnClickListener(this);
		textColor2.setOnClickListener(this);
		textColor3.setOnClickListener(this);
		textColor4.setOnClickListener(this);
		textColor5.setOnClickListener(this);
		textColor6.setOnClickListener(this);
		textColor7.setOnClickListener(this);
		textColor8.setOnClickListener(this);
		textColor9.setOnClickListener(this);
		textColor10.setOnClickListener(this);
		textColor11.setOnClickListener(this);
		textColor12.setOnClickListener(this);
		textColor13.setOnClickListener(this);
		textColor14.setOnClickListener(this);
		textColor15.setOnClickListener(this);
		textColor16.setOnClickListener(this);

		OnWheelScrollListener scrollListenerSize = new OnWheelScrollListener() {
			public void onScrollingStarted(AbstractWheel wheel) {
				timeScrolled = true;
			}

			public void onScrollingFinished(AbstractWheel wheel) {
				timeScrolled = false;
				timeChanged = true;
				int selectedPosition = fontSize.getCurrentItem();
				View viewSelected = wheel.getItemView(selectedPosition);
				viewSelected
				.setBackgroundResource(R.drawable.mi_item_background);
				String selectedSize = AppDict.fontsizes.get(selectedPosition);
				if (clickedMpnodeView != null && nodeProp != null) {
					mPUndoManager.addUndooperationOnNode(nodeProp,
							UndoOperationKey.UndoOperationKey_font);
					clickedMpnodeView.getm_mpNode().setM_iNode_TextSize(
							Integer.parseInt(selectedSize));
					txtFontSize.setText("" + selectedSize);
					clickedMpnodeView.calculateTextWidth();
					clickedMpnodeView.invalidate();
					mplayout.setLayout(nodeProp);

					// mplayout.setLayout(ManahijApp.m_curEbag);
					// currentTextView.setTextSize(fontSize.getCurrentItem());
					// popup.dismiss();

				}
				timeChanged = false;
			}
		};
		OnWheelScrollListener scrollListenerFont = new OnWheelScrollListener()
		{
			public void onScrollingStarted(AbstractWheel wheel)
			{
				timeScrolled = true;
			}

			public void onScrollingFinished(AbstractWheel wheel)
			{
				timeScrolled = false;
				timeChanged = true;
				Typeface typeFace;
				int selectedPosition = fontFace.getCurrentItem();
				String selectedFont = fontsList.get(selectedPosition);
				Log.d("", "selectedFont : " + selectedFont);

				if (clickedMpnodeView != null && nodeProp != null)
				{

					typeFace=mplayout.createTypeface(nodeProp, selectedFont);

					nodeProp.setM_strFontName(selectedFont);
					clickedMpnodeView.getm_mpNode().setM_str_Typeface(typeFace);
					txtFontFace.setTypeface(typeFace);
					txtFontFace.setText(selectedFont);
					clickedMpnodeView.invalidate();
					mplayout.setLayout(nodeProp);
					mPUndoManager.addUndooperationOnNode(nodeProp, UndoOperationKey.UndoOperationKey_font);
					// mplayout.setLayout(ManahijApp.m_curEbag);
				}
				// popup.dismiss();
				timeChanged = false;
			}
		};
		fontSize.addScrollingListener(scrollListenerSize);
		fontFace.addScrollingListener(scrollListenerFont);
		popup.setBackgroundDrawable(new BitmapDrawable());
		int[] location = new int[2];

		popup.setWidth(700);
		// popup.setHeight(300);
		popup.setOutsideTouchable(true);
		popup.setFocusable(true);
		if (popup.isShowing())
			popup.dismiss();
		viewNotes.getLocationOnScreen(location);
		p = new Point();
		p.x = location[0];
		p.y = location[1];
		int OFFSET_X = 3;
		int OFFSET_Y = 50;
		popup.showAtLocation(slidingDrawer, Gravity.BOTTOM,
				(int) txtFontSize.getY(),
				viewNotes.getHeight() + slidingDrawer.getHeight() + OFFSET_Y); // window.showAtLocation(viewInsert,
		// 0);
	}

	private float mx, my;

	public void openPopNotes(Activity context, View v) {
		int[] location = new int[2];
		MPNodeView view = (MPNodeView) v;
		v.getLocationOnScreen(location);
		// Initialize the Point with x, and y positions
		p = new Point();
		p.x = location[0];
		p.y = location[1];
		int popupWidth = 350;
		int popupHeight = 350;
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = layoutInflater.inflate(R.layout.mi_layout_popup_notes,null);

		TextView notesText = (TextView) layout.findViewById(R.id.textNotes);
		inputMethodSubtype = inputMethodManager.getCurrentInputMethodSubtype();
		// FrameLayout.LayoutParams layoutParams = new
		// FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT,
		// LayoutParams.WRAP_CONTENT);
		if (inputMethodSubtype.getLocale().equals("ar")) {
			notesText.setGravity(Gravity.RIGHT);
		} else {
			notesText.setGravity(Gravity.CENTER);
		}
	    if ( DetectHtml.isHtml(nodeProp.getM_strNode_Notes())) {
	    	notesText.setText(Html.fromHtml(nodeProp.getM_strNode_Notes()));
	    }else
		notesText.setText(nodeProp.getM_strNode_Notes());

		// Creating the PopupWindow
		popup = new PopupWindow(context);
		popup.setContentView(layout);
		popup.setBackgroundDrawable(new BitmapDrawable());
		popup.setWidth(popupWidth);
		popup.setHeight(popupWidth);
		popup.setFocusable(true);
		RectF rectDef = nodeProp.propertiesPositions.get("1");
		popup.showAtLocation(
				view,
				Gravity.NO_GRAVITY,
				(int) (p.x - 195 + (rectDef.right) * TwoDScrollView.scaleFactor),
				(int) (p.y + (rectDef.bottom - 45) * TwoDScrollView.scaleFactor- popupHeight));

	}

	public void openPopImagePreview(Activity context, View v) {
		int[] location = new int[2];
		MPNodeView view = (MPNodeView) v;
		v.getLocationOnScreen(location);
		// Initialize the Point with x, and y positions
		p = new Point();
		p.x = location[0];
		p.y = location[1];
		int popupWidth = 350;
		int popupHeight = 350;
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = layoutInflater.inflate(
				R.layout.mi_layout_popup_image_preview, null);

		final ImageView imgPreview = (ImageView) layout
				.findViewById(R.id.img_preview);
		ImageView imgMax = (ImageView) layout.findViewById(R.id.img_max);
		Bitmap smileImg = BitmapFactory.decodeResource(getResources(),
				R.drawable.mi_icon);
		smileImg = Bitmap.createScaledBitmap(smileImg, 50, 50, true);
		imgPreview.setImageBitmap(nodeProp.getM_Attachement_Image());
		imgMax.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showmaximizeWindow(nodeProp.getM_Attachement_Image());
			}
		});
		// Creating the PopupWindow
		popup = new PopupWindow(context);
		popup.setContentView(layout);
		popup.setBackgroundDrawable(new BitmapDrawable());
		popup.setWidth(popupWidth);
		popup.setHeight(popupWidth);
		popup.setFocusable(true);
		int x = 0;
		if (view.m_node.m_smile_image != null)
			x = x + 40;
		x = (int) (x * TwoDScrollView.scaleFactor);
		x = (int) (x * TwoDScrollView.scaleFactor);
		RectF rectDef = nodeProp.propertiesPositions.get("2");
		popup.showAtLocation(
				view,
				Gravity.NO_GRAVITY,
				(int) (p.x - 195 + (rectDef.right) * TwoDScrollView.scaleFactor),
				(int) (p.y + (rectDef.bottom - 45) * TwoDScrollView.scaleFactor- popupHeight));


	}

	public void openHyperLinkPreview(Activity context, View v) {
		int[] location = new int[2];
		MPNodeView view = (MPNodeView) v;
		v.getLocationOnScreen(location);
		// Initialize the Point with x, and y positions
		p = new Point();
		p.x = location[0];
		p.y = location[1];
		int popupWidth = width - 40;
		int popupHeight = height - 40;
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = layoutInflater.inflate(R.layout.mi_layout_webview, null);

		final WebView webview = (WebView) layout.findViewById(R.id.mi_web_view);
		ImageView imgMax = (ImageView) layout.findViewById(R.id.img_max);
		Bitmap smileImg = BitmapFactory.decodeResource(getResources(),
				R.drawable.mi_icon);
		smileImg = Bitmap.createScaledBitmap(smileImg, 50, 50, true);
		WebSettings settings = webview.getSettings();
		settings.setJavaScriptEnabled(true);
		webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		webview.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

			public void onPageFinished(WebView view, String url) {
			}

			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
			}
		});
		webview.loadUrl(nodeProp.m_strHyperlinkUrl);
		imgMax.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//showmaximizeWindow(nodeProp.getM_Attachement_Image());
			}
		});
		// Creating the PopupWindow
		popup = new PopupWindow(context);
		popup.setContentView(layout);
		popup.setBackgroundDrawable(new BitmapDrawable());
		popup.setWidth(popupWidth);
		popup.setHeight(popupHeight);
		popup.setFocusable(true);

		RectF rectDef = nodeProp.propertiesPositions.get("3");
		popup.showAtLocation(
				view,
				Gravity.NO_GRAVITY,
				(int) (p.x - 195 + (rectDef.right) * TwoDScrollView.scaleFactor),
				(int) (p.y + (rectDef.bottom - 45) * TwoDScrollView.scaleFactor- popupHeight));

	}

	Dialog d;

	public void showmaximizeWindow(Bitmap bitmap) {
		final Dialog dialog = new Dialog(MindMapActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.TRANSPARENT));
		dialog.setContentView(R.layout.layout_image_preview);
		// set the custom dialog components - text, image and button
		ImageView img_preview = (ImageView) dialog
				.findViewById(R.id.img_preview);
		ImageView image = (ImageView) dialog.findViewById(R.id.img_max);
		img_preview.setImageBitmap(bitmap);
		// if button is clicked, close the custom dialog
		image.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public void openPopVideoPreview(Activity context, View v) {
		int[] location = new int[2];
		MPNodeView view = (MPNodeView) v;
		if(!nodeProp.m_strMultimediaUrl.contains(".flv"))
		showVideo(nodeProp.mediaType, v);
		else
			Toast.makeText(MindMapActivity.this, "invalid format",Toast.LENGTH_SHORT).show();
			
	}

	VideoPopup videoPopup = null;

	public void showVideo(int mediatype, View v) {
		int[] location = new int[2];
		MPNodeView view = (MPNodeView) v;
		v.getLocationOnScreen(location);
		// Initialize the Point with x, and y positions
		p = new Point();
		p.x = location[0];
		p.y = location[1];

		if (mediatype == 2)
			videoPopup = new VideoPopup(MindMapActivity.this, mediatype,
					nodeProp.multimediaurl);
		else
			videoPopup = new VideoPopup(MindMapActivity.this, mediatype,
					nodeProp.multimediaurl);
		WindowManager.LayoutParams attrib = videoPopup.getWindow()
				.getAttributes();
		if (mediatype == 3) {
			attrib.x = p.x - 300;
			attrib.y = p.y - 910;
		} else {
			attrib.x = p.x - 300;
			attrib.y = p.y - 700;
		}
		videoPopup.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {
				// TODO Auto-generated method stub
				videoPopup.stopPlayer();
			}
		});

		attrib.verticalMargin = .2f;
		attrib.gravity = Gravity.LEFT | Gravity.TOP;
		// attrib.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		videoPopup.getWindow().setAttributes(attrib);
		videoPopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
		videoPopup.show();
	}

	int tempX = 0, tempY = 0;
	public MyGestureListener myGestureListener;
	public boolean singleTap = false;
	int X = 0, Y = 0;
	MPUndoObject mpuobj;
	private float mScaleFactor = 1.0f;
	Bitmap bitmapScale;

	boolean isMoved=false;

	private class MoveListener extends
	MoveGestureDetector.SimpleOnMoveGestureListener {

		@Override
		public boolean onMoveBegin(MoveGestureDetector detector) {
			// mPUndoManager.addUndooperationOnNode(nodeProp,
			// UndoOperationKey.UndoOperationKey_changeNodePosition);
			isMoved=false;
			ManahijApp.m_curEbag.m_impstate = MapState.MPStateBeginDragDrop;
			mpuobj = new MPUndoObject();
			mpuobj.objold = nodeProp.copy();
			if (nodeProp.m_parentNode != null
					&& nodeProp.m_parentNode.m_arrChildren != null)
				mpuobj.oldObjIndex = nodeProp.m_parentNode.m_arrChildren
				.indexOf(nodeProp);
			mpuobj.objnew = nodeProp;
			/*
			 * if(self.m_node.m_iLevelId == -1){ mpuobj.keyType =
			 * UndoOperationKey_orphannode; }else{
			 */
			mpuobj.keyType = UndoOperationKey.UndoOperationKey_changeNodePosition;
			// }
			return super.onMoveBegin(detector);
		}

		@Override
		public void onMoveEnd(MoveGestureDetector detector) {
			if (nodeProp.m_parentNode != null) {
				ManahijApp.m_curEbag.m_impstate = MapState.MPStateEndDragDrop;
				ManahijApp.m_curEbag.m_currNodeBeingDragged = null;
				if(	isMoved)
					mplayout.changeNodePositionOf(nodeProp);
				isMoved=false;
				nodeProp.m_isCurrShapeBeingDragged = false;
				mplayout.setLayout(nodeProp);

				ManahijApp.m_curEbag.m_impstate = MapState.MPStateNormal;
			}
			if (mpuobj != null) {
				MPNode oldNode = mpuobj.objold;
				MPNode newNode = mpuobj.objnew;
				if (newNode.m_parentNode!=null&&newNode.m_parentNode.m_arrChildren!=null&&mpuobj.oldObjIndex != newNode.m_parentNode.m_arrChildren
						.indexOf(newNode)
						|| oldNode.m_parentNode != newNode.m_parentNode) {
					mPUndoManager.addUndoOperations(mpuobj);
				} else if (ManahijApp.m_curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap) {
					mPUndoManager.addUndoOperations(mpuobj);
				}
			}
			super.onMoveEnd(detector);

		}

		@Override
		public boolean onMove(MoveGestureDetector detector) {
			if (clickedMpnodeView != null) {
				PointF distance = detector.getFocusDelta();
				float mFocusX = distance.x;
				float mFocusY = distance.y;
				if(mFocusX>0&&mFocusY>0)
					isMoved=true;
				ManahijApp.m_curEbag.m_impstate = MapState.MPStateBeingDragging;
				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) clickedMpnodeView
						.getLayoutParams();
				layoutParams.leftMargin = X - _xDelta;
				layoutParams.topMargin = Y - _yDelta;
				layoutParams.rightMargin = -250;
				layoutParams.bottomMargin = -250;
				if (nodeProp.m_iLevelId != 0) {
					// isMoved = true;
					nodeProp.m_isCurrShapeBeingDragged = true;
					nodeProp.m_fX = (X - _xDelta);
					nodeProp.m_fY = (Y - _yDelta);
					// nodeProp.isHilight = true;
					ManahijApp.m_curEbag.m_currNodeBeingDragged = nodeProp;
					mplayout.setLayout(nodeProp);
				}
			}

			// mFocusX = detector.getFocusX();
			// mFocusY = detector.getFocusY();
			Log.d("", "onMove ");

			return true;
		}
	}
	RelativeLayout taskGroup;
	LinearLayout pcGroup;
	OnTouchListener touchChildListener=new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			categoryPopup.setVisibility(View.GONE);
			if(resourcesedit!=null){
				resourcesedit.clearFocus();
			}
			return true;
		}
	};
	EditText resourcesedit ;
	private void showInformationPopUp(){
		popup_container.removeAllViews();
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewInfo = inflater.inflate(R.layout.mi_layout_info_popup, null);
		/*viewInfo.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return true;
			}
		});*/
		priorityGroup = (LinearLayout) viewInfo
				.findViewById(R.id.priorityGroupID);

		completionGroup = (LinearLayout) viewInfo
				.findViewById(R.id.completionGroupID);

		taskGroup=(RelativeLayout)viewInfo.findViewById(R.id.taskGroup);
		pcGroup=(LinearLayout)viewInfo.findViewById(R.id.pcGroup);
		taskGroup.setOnTouchListener(touchChildListener);
		pcGroup.setOnTouchListener(touchChildListener);
		plus = (ImageView) viewInfo.findViewById(R.id.plus);

		minus = (ImageView) viewInfo.findViewById(R.id.minus);

		duration = (TextView) viewInfo.findViewById(R.id.duration);
		clearStartDate = (TextView) viewInfo.findViewById(R.id.clearStart);

		clearEndDate = (TextView) viewInfo.findViewById(R.id.clearEnd);

		categoryPopup = (WheelVerticalView) viewInfo.findViewById(R.id.hour);

		category = (TextView) viewInfo.findViewById(R.id.cat);
		if(duration!=null)
			duration.setText(String.valueOf(nodeProp.m_duration));	

		if (nodeProp != null&&nodeProp.getM_duration_type().length()==0 ){
			category.setText(AppDict.items.get(0));

		}
		else
			category.setText(nodeProp.getM_duration_type());
		resourcesedit= (EditText) viewInfo
				.findViewById(R.id.resourcesedit);
		resourcesedit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				resourcesedit.setFocusableInTouchMode(true);
			}
		});
		final FontAdapter adapterCat = new FontAdapter(MindMapActivity.this,
				AppDict.items, false);
		clearStartDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*Toast.makeText(getApplicationContext(),
						"clear start and end date", 2000).show();*/
				if(clickedMpnodeView.m_node.m_start_date!=""){
					mPUndoManager.addUndooperationOnNode(nodeProp,
							UndoOperationKey.UndoOperationKey_StartDate);

					clickedMpnodeView.m_node.m_start_date = "";
					clickedMpnodeView.m_node.m_end_date = "";
					clickedMpnodeView.invalidate();
				}
			}
		});

		clearEndDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*	Toast.makeText(getApplicationContext(), "clear only end date",
						2000).show();*/
				if(clickedMpnodeView.m_node.m_end_date!=""){
					mPUndoManager.addUndooperationOnNode(nodeProp,
							UndoOperationKey.UndoOperationKey_endDate);
					clickedMpnodeView.m_node.m_end_date = "";
					clickedMpnodeView.invalidate();
				}
			}
		});
		categoryPopup.setViewAdapter(adapterCat);
		resourcesedit.setText(nodeProp.getM_resource());
		OnWheelScrollListener scrollListenerDurationType = new OnWheelScrollListener() {
			public void onScrollingStarted(AbstractWheel wheel) {
				timeScrolled = true;
			}

			public void onScrollingFinished(AbstractWheel wheel) {
				timeScrolled = false;
				timeChanged = true;
				int selectedPosition = categoryPopup.getCurrentItem();
				String selectedFont = AppDict.items.get(selectedPosition);

				if (clickedMpnodeView != null && nodeProp != null) {
					/*if(nodeProp.m_duration!=0 && selectedFont!=""){
						mPUndoManager.addUndooperationOnNode(nodeProp,	UndoOperationKey.UndoOperationKey_duration);
					}*/
					if(addinfoundo== null){
						addinfoundo = new MPUndoObject();
						addinfoundo.keyType = UndoOperationKey.UndoOperationKey_duration;
						addinfoundo.objold = nodeProp.copy();
						addinfoundo.objnew = nodeProp;
					}


					datechanged=true;
					nodeProp.setM_duration_type(selectedFont);
					category.setText(nodeProp.getM_duration_type());
				}
				// popup.dismiss();
				timeChanged = false;
			}
		};

		categoryPopup.addScrollingListener(scrollListenerDurationType);
		resourcesedit.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (nodeProp != null && clickedMpnodeView != null)
					nodeProp.setM_resource(resourcesedit.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		category.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (categoryPopup.getVisibility() == View.VISIBLE)
					categoryPopup.setVisibility(View.GONE);
				else
					categoryPopup.setVisibility(View.VISIBLE);
			}
		});
		resourcesedit.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					hideMenuItem();
					addtextUndo = new MPUndoObject();
					addtextUndo.objold = nodeProp.copy();
					addtextUndo.objnew = nodeProp;
					addtextUndo.keyType = UndoOperationKey.UndoOperationKey_resources;
				} else {
					if (!addtextUndo.objold.getM_resource().equals(
							nodeProp.getM_resource())
							&& addtextUndo != null) {
						mPUndoManager.addUndoOperations(addtextUndo);
						addtextUndo = null;
					}
				}

			}
		});
		popup_container.setVisibility(View.VISIBLE);
		RelativeLayout.LayoutParams parma=new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		parma.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM|RelativeLayout.ALIGN_PARENT_RIGHT);
		parma.setMargins(0, 0, 0, 0);
		viewInfo.setLayoutParams(parma);
		popup_container.addView(viewInfo);
		RelativeLayout.LayoutParams relativelParms = (RelativeLayout.LayoutParams) popup_container.getLayoutParams();
		relativelParms.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		relativelParms.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
		relativelParms.setMargins(0, 0, 0, slidingDrawer.getHeight()+tapView.getHeight());
		popup_container.setLayoutParams(relativelParms);

	}

	static Boolean datechanged=false;
	DatePicker cv;

	LinearLayout mainLayout;
	TextView duration, clearStartDate, clearEndDate;;
	ImageView plus, minus;
	LinearLayout priorityGroup, completionGroup;
	TextView category;
	WheelVerticalView categoryPopup;
	ImageView informationPopup;
	TextView cancelSmiley , cancelGen , cancelThreed;
	public void selectStartDate(final View v) 
	{
		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View viewInfo=null;
		if(android.os.Build.VERSION.SDK_INT==android.os.Build.VERSION_CODES.LOLLIPOP)
			viewInfo = inflater.inflate(R.layout.mi_calendarview_lolipop, null);
		else
			viewInfo = inflater.inflate(R.layout.mi_calendarview, null);
		final PopupWindow popup = new PopupWindow(viewInfo,LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

		cv = (DatePicker) viewInfo.findViewById(R.id.calendar1);

		final Calendar calendar = Calendar.getInstance();

		/*CHANGES BY ANITA*/		
		if(v.getId() == R.id.calEnd && nodeProp.m_start_date.length() > 0)
		{
			String[] datesArray = (nodeProp.m_start_date).split("/");		
			calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(datesArray[0]));
			calendar.set(Calendar.MONTH, Integer.parseInt(datesArray[1])-1);
			calendar.set(Calendar.YEAR, Integer.parseInt(datesArray[2]));
			cv.setMinDate(calendar.getTimeInMillis());		
		}

		if(v.getId() == R.id.calStart && nodeProp.m_end_date.length() > 0)
		{
			String[] datesArray = (nodeProp.m_end_date).split("/");		
			calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(datesArray[0]));
			calendar.set(Calendar.MONTH, Integer.parseInt(datesArray[1])-1);
			calendar.set(Calendar.YEAR, Integer.parseInt(datesArray[2]));
			cv.setMaxDate(calendar.getTimeInMillis());

		}

		OnDateChangedListener dateChangeListener = new OnDateChangedListener() 
		{

			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth)
			{
				if(!view.isShown()) return;
				int mYear = year;
				int mMonth = monthOfYear + 1;
				int mDay = dayOfMonth;

				if(nodeProp!=null && clickedMpnodeView!=null)
				{
					switch (v.getId()) 
					{				
					case R.id.calStart:
						mPUndoManager.addUndooperationOnNode(nodeProp,
								UndoOperationKey.UndoOperationKey_StartDate);
						nodeProp.setM_start_date(mDay+"/"+mMonth+"/"+mYear);
						clickedMpnodeView.calculateTextWidth();
						clickedMpnodeView.invalidate();
						resetWidth(clickedMpnodeView);
						popup.dismiss();   
						break;

					case R.id.calEnd:

						if(nodeProp.getM_start_date() == "")
						{								
							final Dialog dialog = new Dialog(MindMapActivity.this);
							dialog.setContentView(R.layout.cal_dialog);
							dialog.setTitle("Alert");
							TextView text = (TextView) dialog.findViewById(R.id.text);
							dialog.show();					                 
							Button ok = (Button) dialog.findViewById(R.id.ok);
							ok.setOnClickListener(new OnClickListener() 
							{
								@Override
								public void onClick(View v) 
								{
									// Close dialog
									dialog.dismiss();
								}
							});
						}  

						else 
						{
							mPUndoManager.addUndooperationOnNode(nodeProp,
									UndoOperationKey.UndoOperationKey_endDate);
							clickedMpnodeView.calculateTextWidth();
							nodeProp.setM_end_date(mDay+"/"+mMonth+"/"+mYear);
							clickedMpnodeView.invalidate();
							resetWidth(clickedMpnodeView);
						}
						popup.dismiss();   
						break;
					default:
						break;
					}
				}

			}
		};
		cv.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),	dateChangeListener);
		popup.showAtLocation(slidingDrawer, Gravity.CENTER, 0, 0);
	}

	public void onStepperClick(View v) {
		int count = Integer.parseInt(duration.getText().toString());
		switch (v.getId()) {
		case R.id.plus:

			if (count >= 0) {
				minus.setClickable(true);
			}
			duration.setText(count + 1 + "");

			break;
		case R.id.minus:
			if (count == 0) {
				minus.setClickable(false);

			} else
				duration.setText((count - 1) + "");

			break;
		default:

			break;

		}
		if (nodeProp != null && clickedMpnodeView != null ){

			/*if(!duration.getText().toString().equals("0") && !nodeProp.m_duration_type.equals(""))
				mPUndoManager.addUndooperationOnNode(nodeProp,
						UndoOperationKey.UndoOperationKey_duration);*/
			if(addinfoundo== null){
				addinfoundo = new MPUndoObject();
				addinfoundo.keyType = UndoOperationKey.UndoOperationKey_duration;
				addinfoundo.objold = nodeProp.copy();
				addinfoundo.objnew = nodeProp;
			}
			datechanged=true;
			nodeProp.setM_duration(Integer.parseInt(duration.getText().toString()));
			clickedMpnodeView.invalidate();
		}
	}
boolean doubleTap=false;
	public class MyGestureListener extends SimpleOnGestureListener implements
	OnTouchListener {
		Context context;
		MPNodeView view;
		GestureDetector gDetector;

		public MyGestureListener() {
			super();
		}

		public MyGestureListener(Context context) {
			this(context, null);
		}

		public MyGestureListener(Context context, GestureDetector gDetector) {

			if (gDetector == null)
				gDetector = new GestureDetector(context, this);

			this.context = context;
			this.gDetector = gDetector;
			gDetector.setOnDoubleTapListener(new OnDoubleTapListener() {

				@Override
				public boolean onSingleTapConfirmed(MotionEvent event) {
					singleTap = true;
					if (nodeProp.m_iLevelId != -1)
						showOptionPopup(event);
					return true;

				}

				@Override
				public boolean onDoubleTapEvent(MotionEvent e) {
					Log.d("", "" + "on onDoubleTapEvent Tap");

					return false;
				}

				@Override
				public boolean onDoubleTap(MotionEvent e) {
					doubleTap = true;
					slideGoneAnimation(slidingDrawer);
					if (clickedMpnodeView != null && nodeProp != null) {
						showTopicEditText();
					}
					return false;
				}
			});
		}

		public String logposition(Point point, MPNode node) {
			for (Entry<String, RectF> entry : node.propertiesPositions
					.entrySet()) {
				String key = entry.getKey();
				RectF value = entry.getValue();
				if (value.contains(point.x, point.y)) {
					return key;
				}
			}
			return "";
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			boolean isMoved = false;
			if(!doubleTap)
			removePopContainer();
			gDetector.onTouchEvent(event);
			if (nodeProp != null && nodeProp.m_iLevelId != 0)
				if (ManahijApp.m_curEbag.m_currNodeBeingDragged == null|| ManahijApp.m_curEbag.m_currNodeBeingDragged == nodeProp)
					mMoveDetector.onTouchEvent(event);
			// mScaleDetector.onTouchEvent(event);
			hideMenuItem();
			X = (int) (event.getRawX() / TwoDScrollView.scaleFactor);
			Y = (int) (event.getRawY() / TwoDScrollView.scaleFactor);
			view = (MPNodeView) v;
			Point point = new Point();
			point.x = (int) event.getX();
			point.y = (int) event.getY();
			switch (event.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				if (nodeProp != null)
					nodeProp.selected = false;
				if (view.getRect().contains((int) point.x, (int) point.y)) {
					if (nodeProp != null)
						nodeProp.selected = false;
					clickedMpnodeView = view;
					if (ManahijApp.m_curEbag.m_impstate == MapState.MPStateNormal) {
						nodeProp = view.getm_mpNode();
						nodeProp.selected = true;
						// twoDScroll.setEnabled(false);
						addText.setText(nodeProp.getM_strNode_text());
						addText.clearFocus();
						txtFontFace.setText(nodeProp.getM_strFontName());
						txtFontSize
						.setText("" + nodeProp.getM_iNode_TextSize());
						editAddNotes.setText(nodeProp.getM_strNode_Notes());
						editAddNotes.clearFocus();
					}
				}
				RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) v
						.getLayoutParams();
				_xDelta = (int) (X - nodeProp.getM_fX());
				_yDelta = (int) (Y - nodeProp.getM_fY());
				// break;
				if (drawingView.getParent() != null) {
					((ViewParent) drawingView.getParent())
					.requestDisallowInterceptTouchEvent(true);
				}
				return true;
			case MotionEvent.ACTION_UP:
				doubleTap=false;
				if (view.getRect().contains((int) point.x, (int) point.y))
					if (nodeProp.m_lp != null)
						nodeProp.m_lp.isHighlight = false;

				if (drawingView.getParent() != null) {
					((ViewParent) drawingView.getParent())
					.requestDisallowInterceptTouchEvent(true);
				}
				// clickedMpnodeView.invalidate();
				// }
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				break;
			case MotionEvent.ACTION_POINTER_UP:
				break;
			case MotionEvent.ACTION_MOVE:
				break;
			}
			drawingView.invalidate();
			// return gDetector.onTouchEvent(event);
			return true;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {

			return super.onFling(e1, e2, velocityX, velocityY);
		}

		public void showOptionPopup(MotionEvent event) {
			final int X = (int) (event.getRawX() / TwoDScrollView.scaleFactor);
			final int Y = (int) (event.getRawY() / TwoDScrollView.scaleFactor);
			Point point = new Point();
			point.x = (int) event.getX();
			point.y = (int) event.getY();
			if (view != null) {
				String log = logposition(point, nodeProp);
				Log.d("", "logposition: " + log);
				if (log.equals("1")) {
					openPopNotes(MindMapActivity.this, view);
				} else if (log.equals("2")) {
					if (nodeProp.mediaType == 0)
						openPopImagePreview(MindMapActivity.this, view);
					else if (nodeProp.mediaType == 2)
						openPopVideoPreview(MindMapActivity.this, view);
					else if (nodeProp.mediaType == 3)
						openPopVideoPreview(MindMapActivity.this, view);
				} else if (log.equals("3")) {
					openHyperLinkPreview(MindMapActivity.this, view);
				} else {
					openPopUpMenu(MindMapActivity.this, view);
				}

				if (drawingView.getParent() != null) {
					((ViewParent) drawingView.getParent())
					.requestDisallowInterceptTouchEvent(true);
				}
			}
		}

		@Override
		public void onLongPress(MotionEvent event) {
			super.onLongPress(event);
			switch (event.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_UP:
				openPopUpMenu(MindMapActivity.this, view);
			}
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {

			return super.onSingleTapConfirmed(e);
		}

		public GestureDetector getDetector() {
			return gDetector;
		}
	}

	float fx, fy;
	MPNode mainparent;
	public void generateMap(MPEbag eBag, ArrayList<MPNode> nodes,
			ViewGroup drawingView) {
		reset();
		i = 1;
		mainparent = nodes.get(0);
		drawingView.removeAllViewsInLayout();
		eBag.setM_iId(i++);
		eBag.setM_parentNode(null);
		if (mainparent.backgroundColor == 0) {
			eBag.setBackgroundColor(Color.parseColor("#e3e3e3"));
			eBag.setHexaColor("#e3e3e3");
		} else {
			eBag.setBackgroundColor(mainparent.getBackgroundColor());
			eBag.setHexaColor(mainparent.getHexaColor());
		}

		// eBag.setTextColor("#000000");
		if (mainparent.m_background_image == null) {
			eBag.setM_fHeight(AppDict.mDrawBitmapHeight);
			eBag.setM_fWidth(AppDict.mDrawBitmapWidth);
		} else {
			eBag.setM_fHeight(AppDict.mDrawBackgroundBitmapHeight);
			eBag.setM_fWidth(AppDict.mDrawBackgroundBitmapWidth);
		}
		eBag.setM_iNode_TextSize(/*
		 * AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,
		 */14/* ) */);
		eBag.setM_strNode_Notes(mainparent.getM_strNode_Notes());
		eBag.m_iLevelId = 0;
		eBag.m_iMapLayoutShape = mainparent.m_iMapLayoutShape;

		if (mainparent.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON)
			eBag.m_fHeight = eBag.m_fWidth;
		// eBag.setM_isCollapsed(true);
		//eBag.setM_str_Typeface(Typeface.createFromFile("/system/fonts/"+ fontsList.get(5) + ".ttf"));
		eBag.setM_strFontName(mainparent.m_strFontName);
		eBag.setM_str_Typeface(mplayout.createTypeface(eBag,mainparent.m_strFontName));
		eBag.setTextColor(mainparent.getTextColor());
		eBag.setM_background_image(mainparent.m_background_image);
		eBag.setM_smile_image(mainparent.m_smile_image);
		eBag.setM_Attachement_Image(mainparent.m_attachement_image);
		eBag.setM_strMultimediaType(mainparent.getM_strMultimediaType());
		eBag.setM_strMultimediaUrl(mainparent.getM_strMultimediaUrl());
		eBag.embededImage = mainparent.embededImage;
		eBag.m_iGradientType = mainparent.m_iGradientType;
		eBag.multimediaurl = mainparent.multimediaurl;
		eBag.m_strHyperlinkUrl = mainparent.m_strHyperlinkUrl;
		eBag.dCharacterPosition = mainparent.dCharacterPosition;
		eBag.dCharacter = mainparent.dCharacter;
		eBag.priorityCharacterPosition = mainparent.priorityCharacterPosition;
		eBag.priorityCharacter = mainparent.priorityCharacter;
		eBag.completionCharacterPosition = mainparent.completionCharacterPosition;
		eBag.completionCharacter = mainparent.completionCharacter;
		eBag.genCharacterPosition = mainparent.genCharacterPosition;
		eBag.genCharacter = mainparent.genCharacter;
		eBag.embededImage = mainparent.embededImage;
		eBag.m_background_image = mainparent.m_background_image;
		eBag.m_attachement_image = mainparent.m_attachement_image;
		eBag.setM_strNode_text(mainparent.getM_strNode_text());
		eBag.m_start_date = mainparent.m_start_date;
		eBag.m_end_date = mainparent.m_end_date;
		eBag.m_duration = mainparent.m_duration;
		eBag.m_duration_type = mainparent.m_duration_type;
		eBag.mediaType = mainparent.mediaType;
		eBag.m_resource = mainparent.m_resource;
		eBag.imageUrl = mainparent.imageUrl;
		eBag.base64image = mainparent.base64image;
		eBag.m_strMultimediaUrl = mainparent.m_strMultimediaUrl;
		eBag.imageAttachementype = mainparent.imageAttachementype;
		eBag.mediaType = mainparent.mediaType;
		eBag.multimediaurl = mainparent.multimediaurl;
		fx = AppDict.DEFAULT_OFFSET * TwoDScrollView.scaleFactor / 2
				- mainparent.m_fX;
		fy = AppDict.DEFAULT_OFFSET * TwoDScrollView.scaleFactor / 2
				- mainparent.m_fY;
		eBag.setM_fX((int) ((AppDict.DEFAULT_OFFSET
				* TwoDScrollView.scaleFactor / 2) /*- 100*/));

		eBag.setM_fY((int) ((AppDict.DEFAULT_OFFSET
				* TwoDScrollView.scaleFactor / 2)/* - 100 */));
		mainparent
		.setM_fX((AppDict.DEFAULT_OFFSET * TwoDScrollView.scaleFactor / 2));
		mainparent
		.setM_fY((AppDict.DEFAULT_OFFSET * TwoDScrollView.scaleFactor / 2));
		MPNodeView nodeView = new MPNodeView(MindMapActivity.this, eBag);

		nodeView.setId(eBag.getM_iId());
		//if (eBag.m_background_image != null)
			// nodeView.bringToFront();
			eBag.setM_shape(nodeView);
		clickedMpnodeView = nodeView;
		//
		// mplayout.addLinelayer(drawingView);
		drawingView.setBackgroundColor(Color
				.parseColor(eBag.m_strMapLayoutFillColor));
		centerX = width * 7 / 2;
		centerY = height * 7 / 2;
		ManahijApp.m_curEbag = eBag;
		//ManahijApp.m_curEbag.m_iMapLayoutLine = MapLayoutLineType.MPLayout_Line_Curved;
		drawingView.addView(nodeView, AppDict.mDrawBitmapWidth,
				AppDict.mDrawBitmapHeight);
		nodeView.setOnTouchListener(myGestureListener);
		clickedMpnodeView = nodeView;
		nodeProp = clickedMpnodeView.m_node;
		MPLayout layout = new MPLayout(MindMapActivity.this);
		drawingView.removeAllViewsInLayout();
		drawingView.addView(nodeView, AppDict.mDrawBitmapWidth,
				AppDict.mDrawBitmapHeight);
		nodeView.calculateTextWidth();
		eBag.setM_arrChildren(mainparent.getM_arrChildren());
		mainparent = eBag;
		if (mainparent.getM_arrChildren() != null
				&& mainparent.getM_arrChildren().size() > 0) {
			for (MPNode nodesp : mainparent.getM_arrChildren()) {
				nodesp.m_parentNode = mainparent;
			}
			createNodeView(mainparent.getM_arrChildren(), drawingView);
		}

		mplayout.setLayout(ManahijApp.m_curEbag);
		if (!orphanNodeText.isEmpty()) {
			mplayout.addOrphanNode(orphanNodeText);
		}

	}
	public void createNodeView(ArrayList<MPNode> parentNode,
			ViewGroup drawingView) {

		for (int i = 0; i < parentNode.size(); i++) {
			MPNode newnode = parentNode.get(i);

			newnode.m_parentNode.m_shape.bringToFront();

			// newnode.textColor = newnode.m_parentNode.textColor;
			newnode.m_iLevelId = newnode.m_parentNode.m_iLevelId + 1;
			if (newnode.m_background_image == null) {
				newnode.setM_fHeight(AppDict.mDrawBitmapHeight);
				newnode.setM_fWidth(AppDict.mDrawBitmapWidth);
			} else {
				newnode.setM_fHeight(AppDict.mDrawBackgroundBitmapHeight);
				newnode.setM_fWidth(AppDict.mDrawBackgroundBitmapWidth);
			}

			if (newnode.backgroundColor == 0) {
				newnode.backgroundColor = newnode.m_parentNode.backgroundColor;
				newnode.hexaColor = newnode.m_parentNode.hexaColor;
			}


			/*
			 * }else{ newnode.m_fX=fx-newnode.m_fX;
			 * newnode.m_fY=newnode.m_fY+fy; }
			 */


			newnode.m_iNode_TextSize = newnode.m_parentNode.m_iNode_TextSize;
			//newnode.m_str_Typeface = newnode.m_parentNode.m_str_Typeface;
			newnode.m_strNode_Notes = newnode.m_strNode_Notes;
			//newnode.m_strFontName = newnode.m_parentNode.m_strFontName;
			newnode.m_str_Typeface=mplayout.createTypeface(newnode,newnode.m_strFontName);
			
			if (newnode.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON)
				newnode.m_fHeight = newnode.m_fWidth;
			// newnode.m_iMapLayoutShape =
			// MapLayoutShapeType.MPLAYOUT_SHAPE_ROUNDEDRECTANGLE;
			MPNodeView newShape = new MPNodeView(MindMapActivity.this, newnode);
			if (newnode.m_background_image != null)
				newShape.setBackgroundColor(Color.TRANSPARENT);
			newShape.setOnTouchListener(myGestureListener);

			/*
			 * drawingView.getLayoutParams().width =
			 * drawingView.getLayoutParams().width + 200;
			 * drawingView.getLayoutParams().height = drawingView
			 * .getLayoutParams().height + 200;
			 */
			if(newnode.m_parentNode.m_iLevelId!=0){

				newnode.m_isRightnode=newnode.m_parentNode.m_isRightnode;
				newnode.m_isAboveCenter=newnode.m_parentNode.m_isAboveCenter;
			}
			
			newnode.m_fX += fx;
			newnode.m_fY += fy;
			newnode.m_fY+=(newnode.m_fY-mainparent.m_fY) *1.3f;
			newnode.m_fX +=(newnode.m_fX-mainparent.m_fX) *1.3f;
			if (!newShape.m_node.imageUrl.equals("")) {
				LoadImageFromServer loadImageFromServer = new LoadImageFromServer(
						MindMapActivity.this, newShape,
						newShape.m_node.imageAttachementype);
				loadImageFromServer.execute();
				// loadImageFromServer.loadIamgee(newShape,
				// newShape.m_node.imageAttachementype);
			}
			newShape.m_node = newnode;

			newnode.m_shape = newShape;
			drawingView.addView(newShape, AppDict.mDrawBitmapWidth,
					AppDict.mDrawBitmapHeight);

			newnode.m_parentNode.m_shape.invalidate();
			newShape.calculateTextWidth();
			newShape.backgroundColor = newnode.m_parentNode.backgroundColor;
			newnode.m_selectedColor = newnode.m_parentNode.m_selectedColor;

			if (newnode.getM_arrChildren() != null
					&& newnode.getM_arrChildren().size() > 0) {
				createNodeView(newnode.getM_arrChildren(), drawingView);
			}
		}
	}

	// LoadImageFromServer loadImageFromServer= new LoadImageFromServer();

	public void sortlist(MPNode node) {
		Comparator<MPNode> idComparator = new Comparator<MPNode>() {
			@Override
			public int compare(MPNode o1, MPNode o2) {
				return Integer.valueOf(o1.m_iNode_Order).compareTo(
						o2.m_iNode_Order);
			}
		};
		Collections.sort(node.m_arrChildren, idComparator);
	}

	public MPEbag parsingSemaNodeXml(String url,boolean ishttp) {


		MPEbag mpEbag = null;
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			MindMapXmlHandler saxp = new MindMapXmlHandler();
			if(ishttp){
				String response=new xmlRequest(url).execute().get();
				if(response!=null){
					InputSource source = new InputSource(new StringReader(response));
					parser.parse(source, saxp);
					mpEbag = saxp.getResults();
				}
			}else{
				File file = new File(url);
				if(file.exists()){
					FileInputStream fis = new FileInputStream(file);
					parser.parse(fis, saxp);
					mpEbag = saxp.getResults();
				}
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mpEbag;

	}

	public void createShapes(MPNode node, MPNode parentNode) {
		if (node.m_arrChildren != null)
			for (MPNode node1 : node.m_arrChildren) {
				createShapes(node1, node);
			}
		node.m_parentNode = parentNode;
		MPNodeView newShape = new MPNodeView(MindMapActivity.this, node);
		newShape.setBackgroundColor(Color.TRANSPARENT);
		newShape.setOnTouchListener(myGestureListener);

		drawingView.addView(newShape, AppDict.mDrawBitmapWidth,
				AppDict.mDrawBitmapHeight);

		newShape.m_node = node;
		node.m_shape = newShape;

	}

	public boolean checkChilds() {
		int left = 0, right = 0, top = 0, bottom = 0;
		boolean yes = false, no = false;
		left = (int) ManahijApp.m_curEbag.m_shape.getX();
		right = ManahijApp.m_curEbag.m_shape.getRight();
		top = (int) ManahijApp.m_curEbag.m_shape.getY();
		ManahijApp.m_curEbag.setMapBounds();

		bottom = ManahijApp.m_curEbag.m_shape.getBottom();
		MPLayout layout = new MPLayout(MindMapActivity.this);
		ArrayList<MPNodeView> childs = layout.getChilds(drawingView);
		Rect scrollBounds = new Rect();
		// vScroll.getHitRect(scrollBounds);
		for (MPNodeView view : childs) {
			if (view.getLocalVisibleRect(scrollBounds)) {

				// Any portion of the view, even a single pixel, is within the
				// visible window
				yes = true;
			} else {
				yes = false;
				// NONE of the view is within the visible window
			}
			if (view.getX() < left) {
				left = view.getLeft();
				Log.d("view.getLeft()", "view.getLeft()" + view.getLeft());
			}
			if (view.getRight() > right)
				right = view.getRight();
			if (view.getY() < top)
				top = view.getTop();
			if (view.getBottom() > bottom)
				bottom = view.getBottom();
		}
		Rect drawingBounds = new Rect();
		drawingView.getLocalVisibleRect(drawingBounds);
		RectF bounds1 = new RectF(drawingBounds);
		RectF bounds = ManahijApp.m_curEbag.getMapBounds();
		if (bounds1.contains(bounds.left - 100, bounds.bottom - 100)) {
			Log.d("View ", "inside the visible bounds");
		} else
			Log.d("View ", "outside the visible bounds");
		return yes || no;
	}

	Rect drawingBounds;
	RectF visibleRectangle;
	RectF ebagBounds;

	public void calculateBounds() {
		ManahijApp.m_curEbag.setMapBounds();
		drawingBounds = new Rect();

		drawingView.getLocalVisibleRect(drawingBounds);
		visibleRectangle = new RectF(drawingBounds);
		ebagBounds = ManahijApp.m_curEbag.getMapBounds();
	}

	boolean variable = false;

	public boolean checkChild(MPNode node) {
		boolean variabl1 = false;
		if (node.getM_arrChildren() != null)
			for (MPNode node1 : node.getM_arrChildren()) {
				variable = checkChild(node1);
			}
		if (node.equals(ManahijApp.m_curEbag.cutMpNode))
			variabl1 = true;
		else
			variabl1 = false;
		return variabl1 || variable;

	}

	boolean variable1 = false;

	public boolean checkParentChild(MPNode node) {
		boolean variabl1 = false;
		if (node.getM_arrChildren() != null)
			for (MPNode node1 : node.getM_arrChildren()) {
				variable1 = checkParentChild(node1);
			}
		if (node.equals(nodeProp))
			variabl1 = true;
		else
			variabl1 = false;
		return variabl1 || variable1;

	}

	public void resetWidth(MPNodeView nodeView) {
		nodeView.calculateTextWidth();


		mplayout.setLayout(nodeView.m_node);
		mplayout.setLayout(ManahijApp.m_curEbag);
	}

	static MindMapButton btnAdd, btnExpand, btnClose;

	public RelativeLayout showRadialMenu(MPNodeView nodeView) {
		RelativeLayout layout = new RelativeLayout(MindMapActivity.this);
		// layout.setBackgroundColor(Color.parseColor("#80808080"));
		layout.setLayoutParams(new LayoutParams(150, 160));

		btnAdd = new MindMapButton(MindMapActivity.this);
		btnAdd.setBackgroundResource(R.drawable.mi_add);
		btnAdd.setId(R.id.buttonAdd);
		btnAdd.setWidth(40);
		btnAdd.setHeight(40);

		btnClose = new MindMapButton(MindMapActivity.this);
		btnClose.setId(R.id.buttonClose);
		btnClose.setBackgroundResource(R.drawable.mi_close);
		btnClose.setWidth(40);
		btnClose.setHeight(40);

		btnExpand = new MindMapButton(MindMapActivity.this);
		btnExpand.setId(R.id.buttonExpand);

		btnExpand.setBackgroundResource(R.drawable.mi_fit);
		btnExpand.setWidth(40);
		btnExpand.setHeight(40);
		if(nodeView.m_node.m_iLevelId == 0){
			btnClose.setEnabled(false);
		}else{
			btnClose.setEnabled(true);
		}
		if (nodeView.m_node.m_isRightnode || nodeView.m_node.m_iLevelId == 0) {
			RelativeLayout.LayoutParams btnAddParams = new RelativeLayout.LayoutParams(
					60, 60);
			// btnAddParams.addRule(RelativeLayout.ALIGN_LEFT);
			btnAddParams.addRule(RelativeLayout.CENTER_IN_PARENT);
			btnAdd.setLayoutParams(btnAddParams);

			RelativeLayout.LayoutParams btnCloseParams = new RelativeLayout.LayoutParams(
					60, 60);
			// btnCloseParams.addRule(RelativeLayout.ALIGN_LEFT);
			btnCloseParams.addRule(RelativeLayout.CENTER_IN_PARENT);
			// btnCloseParams.addRule(RelativeLayout.BELOW, btnAdd.getId());
			// btnCloseParams.addRule(RelativeLayout.RIGHT_OF, btnAdd.getId());
			btnClose.setLayoutParams(btnCloseParams);

			RelativeLayout.LayoutParams btnExLayoutParams = new RelativeLayout.LayoutParams(
					60, 60);
			// btnExLayoutParams.addRule(RelativeLayout.ALIGN_LEFT);
			btnExLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
			// btnExLayoutParams.addRule(RelativeLayout.BELOW,
			// btnClose.getId());
			btnExpand.setLayoutParams(btnExLayoutParams);

			layout.addView(btnAdd, btnAddParams);
			layout.addView(btnClose, btnCloseParams);
			layout.addView(btnExpand, btnExLayoutParams);

			animateRadialMenu(true);

		} else {
			RelativeLayout.LayoutParams btnAddParams = new RelativeLayout.LayoutParams(
					60, 60);
			btnAddParams.addRule(RelativeLayout.CENTER_IN_PARENT);
			btnAdd.setLayoutParams(btnAddParams);

			RelativeLayout.LayoutParams btnCloseParams = new RelativeLayout.LayoutParams(
					60, 60);
			btnCloseParams.addRule(RelativeLayout.CENTER_IN_PARENT);
			btnClose.setLayoutParams(btnCloseParams);

			RelativeLayout.LayoutParams btnExLayoutParams = new RelativeLayout.LayoutParams(
					60, 60);
			btnExLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
			btnExpand.setLayoutParams(btnExLayoutParams);

			layout.addView(btnClose, btnCloseParams);
			layout.addView(btnAdd, btnAddParams);
			layout.addView(btnExpand, btnExLayoutParams);

			animateRadialMenu(false);

		}

		btnExpand.setOnClickListener(popupClickListener);
		btnAdd.setOnClickListener(popupClickListener);
		btnClose.setOnClickListener(popupClickListener);


		return layout;
	}

	public static void animateRadialMenu(boolean right) {
		if (right) {
			ObjectAnimator transAnimation_addY = ObjectAnimator.ofFloat(btnAdd,
					"translationY", 10, -60);
			transAnimation_addY.setDuration(300);// set duration
			transAnimation_addY.start();

			ObjectAnimator transAnimation_closeX = ObjectAnimator.ofFloat(
					btnClose, "translationX", 10, 30);
			transAnimation_closeX.setDuration(300);// set duration
			transAnimation_closeX.start();

			ObjectAnimator transAnimation_expandY = ObjectAnimator.ofFloat(
					btnExpand, "translationY", 10, 60);
			transAnimation_expandY.setDuration(300);// set duration
			transAnimation_expandY.start();
		} else {
			ObjectAnimator transAnimation_addY11 = ObjectAnimator.ofFloat(
					btnAdd, "translationY", 10, -60);
			transAnimation_addY11.setDuration(300);// set duration
			transAnimation_addY11.start();

			ObjectAnimator transAnimation_closeX1 = ObjectAnimator.ofFloat(
					btnClose, "translationX", 10, -30);
			transAnimation_closeX1.setDuration(300);// set duration
			transAnimation_closeX1.start();

			ObjectAnimator transAnimation_expandY1 = ObjectAnimator.ofFloat(
					btnExpand, "translationY", 10, 60);
			transAnimation_expandY1.setDuration(300);// set duration
			transAnimation_expandY1.start();
		}
	}

	public static void animate(boolean value) {
		if (value) {
			ObjectAnimator transAnimation_addY = ObjectAnimator.ofFloat(btnAdd,
					"translationY", 10, 60);
			transAnimation_addY.setDuration(300);// set duration
			transAnimation_addY.addListener(new AnimatorListener() {

				@Override
				public void onAnimationCancel(Animator arg0) {
				}

				@Override
				public void onAnimationEnd(Animator arg0) {
					btnAdd.setX(btnAdd.getX() + 60);
					// btnAdd.setVisibility(View.GONE);
				}

				@Override
				public void onAnimationRepeat(Animator arg0) {

				}

				@Override
				public void onAnimationStart(Animator arg0) {

				}

			});
			transAnimation_addY.start();

			ObjectAnimator transAnimation_closeX = ObjectAnimator.ofFloat(
					btnClose, "translationX", 10, -10);
			transAnimation_closeX.setDuration(300);// set duration
			transAnimation_closeX.addListener(new AnimatorListener() {

				@Override
				public void onAnimationCancel(Animator arg0) {
				}

				@Override
				public void onAnimationEnd(Animator arg0) {
					btnClose.setX(btnClose.getX() - 10);
					// btnClose.setVisibility(View.GONE);
				}

				@Override
				public void onAnimationRepeat(Animator arg0) {

				}

				@Override
				public void onAnimationStart(Animator arg0) {

				}

			});
			transAnimation_closeX.start();

			ObjectAnimator transAnimation_expandY = ObjectAnimator.ofFloat(
					btnExpand, "translationY", 10, -60);
			transAnimation_expandY.setDuration(300);// set duration
			transAnimation_expandY.addListener(new AnimatorListener() {

				@Override
				public void onAnimationCancel(Animator arg0) {
				}

				@Override
				public void onAnimationEnd(Animator arg0) {
					btnExpand.setX(btnExpand.getX() - 60);
					// btnExpand.setVisibility(View.GONE);
				}

				@Override
				public void onAnimationRepeat(Animator arg0) {

				}

				@Override
				public void onAnimationStart(Animator arg0) {

				}

			});
			transAnimation_expandY.start();
		} else {
			ObjectAnimator transAnimation = ObjectAnimator.ofFloat(btnAdd,
					"translationY", 10, +60);
			transAnimation.setDuration(500);// set duration
			transAnimation.start();
		}
	}

	public void undoRedo(View v) {
		editAddNotes.clearFocus();
		removePopContainer();
		addText.clearFocus();
		hideMenuItem();
		mPUndoManager.doUndoRedo(v);


	}

	public boolean HasArabicCharacters(String text) {

		String pattern = "[\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufc3f]|[\ufe70-\ufefc]";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(text);
		return m.find();
	}
}