package com.ptg.mindmap.widget;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.ptg.mindmap.enums.MapState;
import com.ptg.mindmap.layouts.MPEbag;
import com.ptg.mindmap.xml.LruBitmapCache;

public class MindMapApplication extends Application{
	MindMapApplication mindMapApplication;
	public static MPEbag m_curEbag;
	public static  Context mAppContext;
	ProgressDialog pDialog;
	public static final String TAG = MindMapApplication.class
            .getSimpleName();
 
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
 
    private static MindMapApplication mInstance;
	@Override
	public void onCreate() {
		super.onCreate();
		this.m_curEbag = new MPEbag();
		this.m_curEbag.m_impstate = MapState.MPStateNormal;
		mInstance = this;
		mAppContext=this;
	}
	public static synchronized MindMapApplication getInstance() {
        return mInstance;
    }
 
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
 
        return mRequestQueue;
    }
 
    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }
 
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }
 
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
 
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
