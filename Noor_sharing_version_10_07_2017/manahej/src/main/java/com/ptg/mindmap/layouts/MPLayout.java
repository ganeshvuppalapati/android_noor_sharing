package com.ptg.mindmap.layouts;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ptg.mimdmap.line.MPLinePoint;
import com.ptg.mimdmap.line.MyPoint;
import com.ptg.mimdmap.node.LinesLayer;
import com.ptg.mimdmap.node.MPNode;
import com.ptg.mimdmap.node.MPNodeView;
import com.ptg.mimdmap.node.ShadowLayer;
import com.ptg.mindmap.enums.MapLayoutLineType;
import com.ptg.mindmap.enums.MapLayoutShapeType;
import com.ptg.mindmap.enums.MapLayoutType;
import com.ptg.mindmap.enums.MapState;
import com.ptg.mindmap.enums.UndoOperationKey;
import com.ptg.mindmap.widget.AppDict;
import com.ptg.mindmap.widget.MindMapActivity;
import com.semanoor.manahij.ManahijApp;
import com.ptg.mindmap.xml.Xmltags;
import com.semanoor.manahij.R;
import com.semanoor.source_sboookauthor.Globals;

import org.apache.commons.lang.StringEscapeUtils;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

public class MPLayout {
	Context mp_context;

	public MPLayout(Context context) {
		this.mp_context = context;
	}

	public void setLayout(MPNode node) {

		MPEbag eBag = ManahijApp.m_curEbag;
		ViewGroup parent = getMapView(ManahijApp.m_curEbag.m_shape);
		//if (eBag.m_arrChildren != null) {
			if (eBag.m_impstate == MapState.MPStateNormal) {
				// [[NSNotificationCenter defaultCenter]
				// postNotificationName:HidePopups object:nil];
			}
			switch (eBag.m_iMapLayout) {
			case MPLayout_CircularMap:
				MPCircularLayout circularLayout = new MPCircularLayout(
						mp_context);
				circularLayout.setLayout(node);
				// MPCircularLayout.setLayout(node);
				// [MPCircularLayout setLayout:node];
				break;
			case MPLayout_FreeMap:
				FreeMapLayout freeMapLayout = new FreeMapLayout(mp_context);
				freeMapLayout.setLayout(node);
				break;
			case MPLayout_LeftMap:
				MPLeftLayout leftLayout = new MPLeftLayout(mp_context);
				leftLayout.setLayout(node);
				break;
			case MPLayout_RightMap:
				MPRightLayout rightLayout = new MPRightLayout(mp_context);
				rightLayout.setLayout(node);
				break;
			default: 

				break;
			}
			updateLayout(eBag);

			addLinelayer(parent);
			//parent.refreshDrawableState();
			//parent.requestLayout();
		//	parent.invalidate();
		//}
			
	}

	/*public void loadSemaFile(String url, boolean isHttp) {
		((MindMapActivity) this.mp_context).loadXmlData(url, isHttp);
	}*/
	public void getChildrenHeight(MPNode parentnode,
			ArrayList<MPNode> childNodes) {
		if (childNodes == null)
			childNodes = new ArrayList<MPNode>();
		int childrenHeight = 0;
		for (int i = 0; i < childNodes.size(); i++) {
			MPNode node = childNodes.get(i);
			getChildrenHeight(node, node.m_arrChildren);
			childrenHeight = childrenHeight + node.m_childrenHeight;
		}
		if (childrenHeight > parentnode.getM_fHeight())
			parentnode.m_childrenHeight = childrenHeight;
		else
			parentnode.m_childrenHeight = parentnode.m_fHeight;
		if (childrenHeight == 0 || parentnode.m_isCollapsed == true)
			parentnode.m_childrenHeight = parentnode.m_fHeight + 20;
		

	}
  public Typeface createTypeface(MPNode node,String fontName){
	  Typeface ttf=null;
	  try{
	  ttf=Typeface.createFromFile("/system/fonts/"+ fontName + ".ttf");
	  }catch(Exception e){
		  try{
		  ttf= Typeface.createFromAsset(mp_context.getAssets(),"fontfamily/" + fontName+".ttf");
		  }catch(Exception e1){
			  ttf= Typeface.createFromAsset(mp_context.getAssets(),"fontfamily/Open Sans.ttf");
			  node.m_strFontName="Open Sans";
		  }
	  }
  return ttf;
  }
	public int getRealChildrenHeight(ArrayList<MPNode> childNodes) {
		int childrenht = 0;
		for (MPNode obj : childNodes) {
			childrenht = childrenht + obj.m_childrenHeight;
		}
		return childrenht;
	}
	public void updateNodePositions(MPEbag ebag) {
		if (ebag.m_arrRightTop == null) {
			ebag.m_arrRightTop = new ArrayList<MPNode>();
		}
		if (ebag.m_arrRightBottom == null) {
			ebag.m_arrRightBottom = new ArrayList<MPNode>();
		}
		if (ebag.m_arrLeftBottom == null) {
			ebag.m_arrLeftBottom = new ArrayList<MPNode>();
		}
		if (ebag.m_arrRightTop == null) {
			ebag.m_arrRightTop = new ArrayList<MPNode>();
		}
		if (ebag.m_arrLeftTop == null) {
			ebag.m_arrLeftTop = new ArrayList<MPNode>();
		}
		ebag.m_arrRightTop.clear();
		ebag.m_frtHeight = 0;
		ebag.m_arrRightBottom.clear();
		ebag.m_frbHeight = 0;
		ebag.m_arrLeftBottom.clear();
		ebag.m_flbHeight = 0;
		ebag.m_arrLeftTop.clear();
		ebag.m_fltHeight = 0;
		// [super updateNodePositions:ebag];

	}

	int i = 1;
	public void setRightnodes(){
		
	}
	
	
	public void getCurrentDraggingNodeLine() {
		if (ManahijApp.m_curEbag.m_currNodeBeingDragged != null) {
			MPEbag curEbag = ManahijApp.m_curEbag;
			MPNodeView draggingShap = curEbag.m_currNodeBeingDragged.m_shape;
			MPNode draggingNode = curEbag.m_currNodeBeingDragged;
			// draggingNode.m_parentNode.m_shape.bringToFront();
			// draggingNode.m_shape.lineView.bringToFront();
			// draggingNode.m_shape.bringToFront();
			if (draggingShap == null)
				return;
			MPNodeView nearestShape = null;// = self.m_node.m_parentNode;
			nearestShape = hitTest(draggingShap);
			if(nearestShape!=null||draggingNode.m_parentNode!=null){
			if (nearestShape == null) {
				
				nearestShape = draggingNode.m_parentNode.m_shape;
			}
			nearestShape.bringToFront();
			if (draggingNode.m_fX + draggingNode.m_fWidth / 2 > curEbag.m_fX
					+ curEbag.m_fWidth / 2) {

				if (nearestShape.m_node.m_iLevelId == 0) {
					// draggingNode.m_iisRightnode = TRUE;
					MPLinePoint lpoint = new MPLinePoint();
					// lpoint.nodeId = draggingNode;
					lpoint.moveTo(nearestShape.m_node.m_fX + 10
							+ nearestShape.m_node.m_fWidth / 2,
							nearestShape.m_node.m_fY
									+ nearestShape.m_node.m_fHeight / 2);
					lpoint.qCurveto(draggingNode.m_fX - 60, draggingNode.m_fY
							+ draggingNode.m_fHeight / 2, draggingNode.m_fX,
							draggingNode.m_fY + draggingNode.m_fHeight / 2);
					lpoint.isHighlight = true;
					// [draggingNode.m_parentNode.arrGLPoints addObject:lpoint];
					draggingNode.m_lp = lpoint;
				} else {
					MPLinePoint lpoint = new MPLinePoint();
					// lpoint.nodeId = draggingNode;
					lpoint.moveTo(nearestShape.m_node.m_fX - 10
							+ nearestShape.m_node.m_fWidth,
							nearestShape.m_node.m_fY
									+ nearestShape.m_node.m_fHeight / 2);
					lpoint.qCurveto(draggingNode.m_fX - 25, draggingNode.m_fY
							+ draggingNode.m_fHeight / 2, draggingNode.m_fX,
							draggingNode.m_fY + draggingNode.m_fHeight / 2);
					lpoint.isHighlight = true;
					// [draggingNode.m_parentNode.arrGLPoints addObject:lpoint];
					draggingNode.m_lp = lpoint;
				}

			} else {

				if (nearestShape.m_node.m_iLevelId == 0) {
					// draggingNode.m_iisRightnode = FALSE;
					MPLinePoint lpoint = new MPLinePoint();
					// lpoint.nodeId = draggingNode;
					lpoint.moveTo(draggingNode.m_fX + draggingNode.m_fWidth,
							draggingNode.m_fY + draggingNode.m_fHeight / 2);
					lpoint.qCurveto(draggingNode.m_fX + draggingNode.m_fWidth
							+ 10 + 60, draggingNode.m_fY
							+ draggingNode.m_fHeight / 2,
							nearestShape.m_node.m_fX
									+ nearestShape.m_node.m_fWidth / 2,
							nearestShape.m_node.m_fY
									+ nearestShape.m_node.m_fHeight / 2);
					lpoint.isHighlight = true;
					// [draggingNode.m_parentNode.arrGLPoints addObject:lpoint];
					draggingNode.m_lp = lpoint;

				} else {

					MPLinePoint lpoint = new MPLinePoint();
					// lpoint.nodeId = draggingNode;
					lpoint.moveTo(draggingNode.m_fX + draggingNode.m_fWidth,
							draggingNode.m_fY + draggingNode.m_fHeight / 2);
					lpoint.qCurveto(draggingNode.m_fX + draggingNode.m_fWidth
							+ 10 + 25, draggingNode.m_fY
							+ draggingNode.m_fHeight / 2,
							nearestShape.m_node.m_fX, nearestShape.m_node.m_fY
									+ nearestShape.m_node.m_fHeight / 2);
					lpoint.isHighlight = true;
					// [draggingNode.m_parentNode.arrGLPoints addObject:lpoint];
					draggingNode.m_lp = lpoint;

				}
			}
			}
			// tempUpdateChildrenColor(draggingNode.m_shape,draggingNode.m_parentNode.m_shape.m_node.backgroundColor);
		}
	}

	int scrollX, scrollY;

	public void updateLayout(MPNode node) {
		if (node.m_arrChildren != null)
			for (MPNode childNode : node.m_arrChildren) {
				// shape.backgroundColor = node.m_selectedColor;
				updateLayout(childNode);
			}
		MPNodeView shape = node.m_shape;
		shape.rect = node.getBounds();

		ViewGroup mapView = getMapView(shape);
		scrollX = (int) shape.m_node.m_fX;
		scrollY = (int) shape.m_node.m_fY;
		//shape.setNodeText(node.m_strNode_text);

	}

	//NEW
	public void setPositionsRightMap(MPNode parentNode,
			ArrayList<MPNode> childnodes, float startY, boolean collapse) {
		if (childnodes != null && childnodes.size() > 0) {
			float xPos = parentNode.m_fX + parentNode.m_fWidth + 40;
			float yPos;

			if (startY == -9999999){
				//yPos = parentNode.m_fY - parentNode.m_childrenHeight / 2+ parentNode.m_fHeight / 2;
				 int childrenHt = getRealChildrenHeight(parentNode.m_arrChildren);
			            yPos = parentNode.m_fY + parentNode.m_fHeight/2 - childrenHt/2 ;
			}
			else{
				yPos = startY;
			}

			ArrayList<MyPoint> arrPoints = new ArrayList<MyPoint>();
			// [parentNode.arrGPoints addObject:arrPoints];
			int draggedLayerPosition = -1;

			for (int i = 0; i < childnodes.size(); i++) {

				MPNode childNode = childnodes.get(i);
				MPNodeView childShape = childNode.m_shape;
				yPos = yPos + childNode.m_childrenHeight / 2;

				// if(parentNode.m_iLevelId != 0)
				// childNode.m_strLocation = [NSString
				// stringWithFormat:@"%@/%d",parentNode.m_strLocation,i];

				if (!childNode.m_isCurrShapeBeingDragged) {

					if (parentNode.m_isCollapsed == true || collapse == true) {
						// MPShapeView *parentShape = parentNode.m_shape;
						childShape.rect = new RectF();
						childShape.setVisibility(View.GONE);
						childShape.hidden = true;
						childNode.m_lp = null;
						setPositionsRightMap(childNode,
								childNode.m_arrChildren, -9999999, true);

					} else {
						childShape.hidden = false;
						childShape.setVisibility(View.VISIBLE);
						/*if (i == 0 && childnodes.size() == 1
								&& parentNode.m_fHeight > childNode.m_fHeight) {
							// console.log("Special case");
							childNode.m_fX = xPos;
							childNode.m_fY = yPos + parentNode.m_fHeight / 2
									- childNode.m_fHeight;

						} else*/ {
							childNode.m_fX = xPos;
							childNode.m_fY = (float) (yPos - childNode.m_fHeight / 2.0);
						}
						// childShape.frame = CGRectMake(childNode.m_fX,
						// childNode.m_fY,childNode.m_fWidth,childNode.m_fHeight);
						// [childShape setNodeText:childNode.m_strNode_text];
						// [arrPoints addObject:[NSNumber
						// numberWithFloat:xPos]];
						float lineX = xPos;
						float lineY = (float) 0.0;

						/*if (i == 0 && childnodes.size() == 1
								&& parentNode.m_fHeight > childNode.m_fHeight) {
							// [arrPoints addObject:[NSNumber
							// numberWithFloat:(yPos
							// -2.5 + parentNode.m_selfHeight/2 -
							// node.m_selfHeight/2 )]];
							lineY = (float) (yPos - 2.5 + parentNode.m_fHeight
									/ 2 - childNode.m_fHeight / 2);
						} else*/ {
							// [arrPoints addObject:[NSNumber
							// numberWithFloat:(yPos
							// -2.5 )]];
							lineY = (float) (yPos - 2.5);
						}

						MyPoint cmd = new MyPoint();
						cmd.tagid = childNode;
						cmd.m_fArg1 = lineX;
						cmd.m_fArg2 = lineY;
						arrPoints.add(cmd);

						setPositionsRightMap(childNode,
								childNode.m_arrChildren, -9999999, false);

					}
					// [parentNode.m_rightnodes addObject:childNode];

				} else {
					draggedLayerPosition = i;
					MPEbag rootNode = ManahijApp.m_curEbag;
					if (childNode.m_fX + childNode.m_fWidth / 2 > rootNode.m_fX
							+ rootNode.m_fWidth / 2) {
						// curEbag.m_rightPages.push(pages[i]);
						setPositionsRightMap(childNode,
								childNode.m_arrChildren, -9999999, false);
						MyPoint cmd = new MyPoint();
						cmd.m_fArg1 = childNode.m_fX + 10;
						cmd.m_fArg2 = childNode.m_fY + childNode.m_fHeight / 2;
						cmd.tagid = childNode;
						arrPoints.add(cmd);

					} else {
						// curEbag.m_leftPages.push(pages[i]);
						setPositionsLeftMap(childNode, childNode.m_arrChildren,
								-9999999, false);
						MyPoint cmd = new MyPoint();
						cmd.tagid = childNode;
						cmd.m_fArg1 = childNode.m_fX + childNode.m_fWidth + 10;
						cmd.m_fArg2 = childNode.m_fY + childNode.m_fHeight / 2;
						arrPoints.add(cmd);
					}

				}
				yPos = yPos + childNode.m_childrenHeight / 2;
			}

			float xFrom = parentNode.m_fX + parentNode.m_fWidth + 10;
			float yFrom = (float) (parentNode.m_fY + parentNode.m_fHeight / 2 - 2.5);

			for (int j = 0; j < arrPoints.size(); j++) {
				if (draggedLayerPosition != j) {
					MyPoint cmd = arrPoints.get(j);
					int sign = xFrom > cmd.m_fArg1 ? 1 : -1;
					MPLinePoint lpoint = new MPLinePoint();
					lpoint.m_iType = 0;
					// lpoint.nodeId = cmd.tagid;
					if (parentNode.m_iLevelId == 0) {
						lpoint.moveTo(xFrom - parentNode.m_fWidth / 2, yFrom);
						lpoint.qCurveto(
								(cmd.m_fArg1 + sign * 25 - parentNode.m_fWidth / 2),
								cmd.m_fArg2, cmd.m_fArg1, cmd.m_fArg2);
					} else {
						lpoint.moveTo(xFrom - 10, yFrom);
						lpoint.qCurveto((cmd.m_fArg1 + sign * 25), cmd.m_fArg2,
								cmd.m_fArg1, cmd.m_fArg2);

					}
					// [parentNode.arrGLPoints addObject:lpoint];
					cmd.tagid.m_lp = lpoint;
				}
			}
		}
	}
//new
	public void setPositionsLeftMap(MPNode parentNode,
			ArrayList<MPNode> childnodes, float startY, boolean collapse) {
		if (childnodes != null && childnodes.size() > 0) {
			float xPos = parentNode.m_fX - 40;
			float yPos;
			if (startY == -9999999){
				//yPos = parentNode.m_fY - parentNode.m_childrenHeight / 2+ parentNode.m_fHeight / 2;
				int childrenHt = getRealChildrenHeight(parentNode.m_arrChildren);
	            yPos = parentNode.m_fY + parentNode.m_fHeight/2 - childrenHt/2 ;
			}else
				yPos = startY;

			ArrayList<MyPoint> arrPoints = new ArrayList<MyPoint>();
			// [parentNode.arrGPoints addObject:arrPoints];
			int draggedLayerPosition = -1;

			for (int i = 0; i < childnodes.size(); i++) {

				MPNode childNode = childnodes.get(i);
				MPNodeView childshape = childNode.m_shape;
				yPos = yPos + childNode.m_childrenHeight / 2;

				// if(parentNode.m_iLevelId != 0)
				// childNode.m_strLocation = [NSString
				// stringWithFormat:@"%@/%d",parentNode.m_strLocation,i];

				if (!childNode.m_isCurrShapeBeingDragged) {
					if (parentNode.m_isCollapsed == true || collapse == true) {
						childshape.rect = new RectF();
						childshape.setVisibility(View.GONE);
						childshape.hidden = true;
						childNode.m_lp = null;
						setPositionsLeftMap(childNode, childNode.m_arrChildren,
								-9999999, true);

					} else {

						childshape.hidden = false;
						childshape.setVisibility(View.VISIBLE);
						/*if (i == 0 && childnodes.size() == 1
								&& parentNode.m_fHeight > childNode.m_fHeight) {
							// console.log("Special case");
							childNode.m_fX = xPos - childNode.m_fWidth;
							childNode.m_fY = yPos + parentNode.m_fHeight / 2
									- childNode.m_fHeight;
						}else  */{
							childNode.m_fX = xPos - childNode.m_fWidth;
							childNode.m_fY = (float) (yPos - childNode.m_fHeight / 2.0);
						}
						// childshape.frame = CGRectMake(childNode.m_fX,
						// childNode.m_fY,childNode.m_fWidth,childNode.m_fHeight);
						// [childshape setNodeText:childNode.m_strNode_text];
						float lineX = (xPos + 10);
						float lineY = (float) 0.0;
						/*if (i == 0 && childnodes.size() == 1
								&& parentNode.m_fHeight > childNode.m_fHeight) {
							lineY = (float) (yPos - 2.5 + parentNode.m_fHeight
									/ 2 - childNode.m_fHeight / 2);
						} else*/ {
							lineY = (float) (yPos - 2.5);

						}
						MyPoint cmd = new MyPoint();
						cmd.m_fArg1 = lineX - 10;
						cmd.m_fArg2 = lineY;
						cmd.tagid = childNode;
						arrPoints.add(cmd);
						// [parentNode.m_leftnodes addObject:childNode];

						setPositionsLeftMap(childNode, childNode.m_arrChildren,
								-9999999, false);
					}
				} else {
					draggedLayerPosition = i;

					MPEbag rootNode = ManahijApp.m_curEbag;
					if (childNode.m_fX + childNode.m_fWidth / 2 > rootNode.m_fX
							+ rootNode.m_fWidth / 2) {

						// [parentNode.m_rightnodes addObject:childNode];
						setPositionsRightMap(childNode,
								childNode.m_arrChildren, -9999999, false); // Why
																			// Rightmap
																			// here?
						MyPoint cmd = new MyPoint();
						cmd.m_fArg1 = childNode.m_fX;
						cmd.m_fArg2 = childNode.m_fY + childNode.m_fHeight / 2;
						cmd.tagid = childNode;
						arrPoints.add(cmd);

					} else {
						// [parentNode.m_leftnodes addObject:childNode];
						setPositionsLeftMap(childNode, childNode.m_arrChildren,
								-9999999, false);
						MyPoint cmd = new MyPoint();
						cmd.m_fArg1 = childNode.m_fX + childNode.m_fWidth;
						cmd.m_fArg2 = childNode.m_fY + childNode.m_fHeight / 2;
						cmd.tagid = childNode;
						arrPoints.add(cmd);
					}

				}
				yPos = yPos + childNode.m_childrenHeight / 2;
			}
			float xFrom = parentNode.m_fX;
			float yFrom = (float) (parentNode.m_fY + parentNode.m_fHeight / 2 - 2.5);
			for (int j = 0; j < arrPoints.size(); j++) {
				if (draggedLayerPosition != j) {
					MyPoint cmd = arrPoints.get(j);
					int sign = ((xFrom > cmd.m_fArg1) ? 1 : -1);
					MPLinePoint lpoint = new MPLinePoint();
					lpoint.m_iType = 1;
					if (parentNode.m_iLevelId == 0) {
						// context.moveTo(xFrom + parentPage.m_selfWidth/2,
						// yFrom);
						lpoint.moveTo(xFrom + parentNode.m_fWidth / 2, yFrom);
						lpoint.qCurveto(
								(cmd.m_fArg1 + sign * 25 + parentNode.m_fWidth / 2),
								cmd.m_fArg2, cmd.m_fArg1, cmd.m_fArg2);
						// lpoint.clr = parentPage.m_strNodeShapeFill;
						// context.quadraticCurveTo(arrPoints[j] + sign* 25 +
						// parentPage.m_selfWidth/2, arrPoints[j+1],
						// arrPoints[j],
						// arrPoints[j+1]);
					} else {
						// context.moveTo(xFrom , yFrom);
						lpoint.moveTo(xFrom, yFrom);
						lpoint.qCurveto((cmd.m_fArg1 + sign * 25), cmd.m_fArg2,
								cmd.m_fArg1, cmd.m_fArg2);
						// context.quadraticCurveTo(arrPoints[j] + sign* 25 ,
						// arrPoints[j+1], arrPoints[j], arrPoints[j+1]);
					}
					// [parentNode.arrGLPoints addObject:lpoint];
					cmd.tagid.m_lp = lpoint;
				}
			}
		}
	}

	public void deleteNodeOf(MPNodeView viewNode) {
		
		
		ViewParent mapView = viewNode.getParent();
		if (mapView == null)
			return;
		else
		// [mapView hideMenuItem];
		{
			viewNode.m_node.deleteNode();
			setLayout(viewNode.m_node.m_parentNode);
		}

	}


	public void collapseNodeOf(MPNodeView shape) {
		if (shape.m_node.m_arrChildren != null)
			shape.m_node.m_isCollapsed = shape.m_node.m_isCollapsed?false:true;
			/*shape.m_node.m_isCollapsed = shape.m_node.m_isCollapsed ? false: true;
		//collapseChilds(shape.m_node.m_arrChildren, shape.m_node.m_isCollapsed);
		shape.m_node.m_shape.setVisibility(View.VISIBLE);*/
		setLayout(shape.m_node);

	}

	public void collapseChilds(ArrayList<MPNode> m_arrChildren,
			boolean m_isCollapsed) {
		if (m_arrChildren != null)
			for (MPNode node1 : m_arrChildren) {
				collapseChilds(node1.m_arrChildren, m_isCollapsed);
				node1.m_isCollapsed = m_isCollapsed;
				if (node1.m_isCollapsed)
					node1.m_shape.setVisibility(View.GONE);
				else
					node1.m_shape.setVisibility(View.VISIBLE);

			}

	}

	public ViewGroup getMapView(MPNodeView shape) {

		ViewGroup mapView = (ViewGroup) shape.getParent();
		if (mapView instanceof ViewGroup)
			return mapView;

		return null;
	}

	public void addNewNodeOf(MPNodeView shape) {
		ViewGroup mapView = getMapView(shape);
		if (mapView == null)
			return;

		MPEbag curEbag = ManahijApp.m_curEbag;
		MPNode selectedNode = shape.m_node;
		if (selectedNode.m_arrChildren == null)
			selectedNode.m_arrChildren = new ArrayList<MPNode>();
		int nodeOrder = selectedNode.m_arrChildren.size() + 1;
		MPNode newnode = null;
		String text = "Topic";
		int addAt = 0;
		boolean isAbove = false;
		boolean m_isRightnode = false;
		if (selectedNode.m_iLevelId == 0) { // root node
			text = text + " " + nodeOrder;
			switch (curEbag.m_iaddPagePosition) {
			case 0:
				// addAt = 0;
				curEbag.m_iNumberOfRightNodes++;
				m_isRightnode = true;
				isAbove = true;
				break;
			case 1:
				// addAt = curEbag.m_iNumberOfRightNodes;
				curEbag.m_iNumberOfRightNodes++;
				m_isRightnode = true;
				isAbove = false;
				break;
			case 2:
				// addAt = curEbag.m_iNumberOfRightNodes;
				isAbove = false;
				break;
			case 3:
				addAt = curEbag.m_arrChildren.size();
				isAbove = true;
				break;
			case 4:
				// addAt = 0;
				curEbag.m_iaddPagePosition = 0;
				curEbag.m_iNumberOfRightNodes++;
				isAbove = true;
				m_isRightnode = true;
				break;
			}

			curEbag.m_iaddPagePosition++;

		} else {
			m_isRightnode=selectedNode.m_isRightnode;
			// [mapView hideMenuItem];
		}
		newnode = new MPNode();

		if (selectedNode.m_arrChildren == null) {
			selectedNode.m_arrChildren = new ArrayList<MPNode>();
		}
		selectedNode.m_arrChildren.add(newnode);
		newnode.m_parentNode = selectedNode;
		newnode.m_strNode_text = text;
		newnode.textColor = selectedNode.textColor;
		newnode.m_isAboveCenter = isAbove;
		newnode.m_isRightnode = m_isRightnode;
		newnode.m_fWidth = AppDict.mDrawBitmapWidth;
		newnode.m_fHeight = AppDict.mDrawBitmapHeight;
		newnode.m_iLevelId = selectedNode.m_iLevelId + 1;
		newnode.backgroundColor = selectedNode.backgroundColor;
		newnode.hexaColor = selectedNode.hexaColor;
		newnode.m_iNode_TextSize = /*
									 * AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP
									 * ,
									 */14/* ) */;
		newnode.m_str_Typeface = selectedNode.m_str_Typeface;
		newnode.m_isCollapsed = selectedNode.m_isCollapsed;
		newnode.m_strNode_Notes = "";
		newnode.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_ROUNDEDRECTANGLE;
		newnode.m_strFontName = selectedNode.m_strFontName;
		// // CGPoint pt = [MPFreemapLayout
		// calculateNewChildPosition:parentShape];
		MPNodeView newShape = new MPNodeView(mp_context, newnode);
		if (curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap) {
			FreeMapLayout freeMapLayout = new FreeMapLayout(mp_context);
			Point pt = freeMapLayout.calculateNewChildPosition1(selectedNode,
					newnode);
			newnode.m_fX = pt.x;
			newnode.m_fY = pt.y;
			if(!newnode.m_parentNode.m_isCollapsed)
			newShape.rect = newnode.getBounds();
		}
		// newShape.bringToFront();
		newShape.setBackgroundColor(Color.TRANSPARENT);
		newShape.delegate = mapView;
		newShape.setOnTouchListener(((MindMapActivity) mp_context).myGestureListener);
		mapView.addView(newShape, AppDict.mDrawBitmapWidth, AppDict.mDrawBitmapHeight);
		//newShape

		/*
		 * if(newnode.m_isCollapsed ) newShape.setVisibility(View.GONE);
		 */
		newShape.m_node = newnode;
		newnode.m_shape = newShape;
		newShape.backgroundColor = selectedNode.m_selectedColor;
		//newShape.invalidate();
		newnode.m_selectedColor = selectedNode.m_selectedColor;
		((MindMapActivity)mp_context).mPUndoManager.addUndooperationOnNode(newnode , UndoOperationKey.UndoOperationKey_deleteNode);
		setLayout(newnode);

	}

	public ArrayList<MPNodeView> getChilds(ViewGroup view) {
		ArrayList<MPNodeView> views = new ArrayList<MPNodeView>();
		for (int i = 0; i < view.getChildCount(); ++i) {
			if (view.getChildAt(i) instanceof MPNodeView)
				views.add((MPNodeView) view.getChildAt(i));
		}
		return views;

	}

	public MPNodeView hitTest(MPNodeView shape) {
		if (shape == null)
			return null;

		RectF rect1 = shape.rect;
		rect1 = new RectF(shape.m_node.m_fX, shape.m_node.m_fY,
				shape.m_node.m_fX + shape.m_node.m_fWidth, shape.m_node.m_fY
						+ shape.m_node.m_fHeight);
		ViewGroup mapView = getMapView(shape);

		// [((MPMapView *)shape.superview) resetShapesHighlight];
		// [mapView highLightShadowView:nil];
		if (mapView instanceof ViewGroup)
			for (MPNodeView shape1 : getChilds(mapView)) {
				// if(!shape1.equals(shape))
				if (shape1 instanceof MPNodeView && !shape.equals(shape1)
						&& shape1.getVisibility() == View.VISIBLE) {
					// RectF rect2 = shape1.rect;
					RectF rect2 = new RectF(shape1.m_node.m_fX,
							shape1.m_node.m_fY, shape1.m_node.m_fX
									+ shape1.m_node.m_fWidth,
							shape1.m_node.m_fY + shape1.m_node.m_fHeight);
					boolean value=false;
					if(shape.m_node.m_parentNode==null){
						value=false;
					}
					else
				 value=	shape1.equals(shape.m_node.m_parentNode.m_shape);
					if (RectF.intersects(rect1, rect2)&& !(value)) {
						shape1.m_node.m_isChild = false;
						if (!isNearestNodeIsChildrenOf(shape.m_node,
								shape1.m_node)) {
							// shape1.highlight(true);
							// shape1.invalidate();
							shape1.bringToFront();
							highLightShadowView(shape1, true);

							ManahijApp.m_curEbag.m_nearestNodeToTheCurrentNodeBeingDragged = shape1.m_node;
							draggingTempUpdateChildrenColor(shape,
									shape1.m_node.backgroundColor,
			      						shape1.m_node.hexaColor);
							if(shape.m_node.m_iLevelId==-1/*m_parentNode==null*/&&shape.m_node.m_isCurrShapeBeingDragged/*ManahijApp.m_curEbag.m_impstate==MapState.MPStateEndDragDrop*/){
								shape.m_node.m_parentNode=shape1.m_node;
								shape.m_node.m_iLevelId=1;
								if(shape1.m_node.getM_arrChildren()!=null){
								ArrayList<MPNode> listdata = shape1.m_node.getM_arrChildren();
								listdata.add(shape.m_node);
								shape1.m_node.setM_arrChildren(listdata);
								}else{
									ArrayList<MPNode> listdata=new ArrayList<MPNode>();
									listdata.add(shape.m_node);
									shape1.m_node.setM_arrChildren(listdata);
								}
								
							}
							return shape1;
						}
					} else {
						// shape1.highlight(false);
						// shape1.invalidate();
						highLightShadowView(shape1, false);
						// [mapView highLightShadowView:nil];

						ManahijApp.m_curEbag.m_nearestNodeToTheCurrentNodeBeingDragged = null;
					}

				}
			}

		return null;

	}

	public boolean isNearestNodeIsChildrenOf1(MPNode curNode, MPNode nearestnode) {

		for (MPNode node : curNode.m_arrChildren) {

			if (node.equals(nearestnode)) {
				nearestnode.m_isChild = true;
				break;
			}
			isNearestNodeIsChildrenOf(node, nearestnode);

		}

		return nearestnode.m_isChild;
	}

	public void tempUpdateChildrenColor(MPNodeView parentShape, int color,
			String hexaColor) {
		if (parentShape.m_node.m_arrChildren != null)
			for (MPNode node : parentShape.m_node.m_arrChildren) {
				tempUpdateChildrenColor(node.m_shape, color, hexaColor);
			}

		parentShape.m_node.setBackgroundColor(color);
		parentShape.m_node.setHexaColor(hexaColor);

	}
	
	public void draggingTempUpdateChildrenColor(MPNodeView parentShape, int color,
			String hexaColor) {
		if (parentShape.m_node.m_arrChildren != null)
			for (MPNode node : parentShape.m_node.m_arrChildren) {
				draggingTempUpdateChildrenColor(node.m_shape, color, hexaColor);
			}

		/*parentShape.m_node.setBackgroundColor(color);
		parentShape.m_node.setHexaColor(hexaColor);*/
		parentShape.setShapeBackgroundColor(color);

	}

	public void updateBoundsOf(MPNode node, Point pt) {
		if (node.m_arrChildren != null)
			for (MPNode childNode : node.m_arrChildren) {
				updateBoundsOf(childNode, pt);
			}
		node.m_fX = node.m_fX + pt.x;
		node.m_fY = node.m_fY + pt.y;

	}

	public void highLightShadowView(MPNodeView view, boolean value) {
		ViewGroup parent = getMapView(ManahijApp.m_curEbag.m_shape);
		if (value) {
			if (ManahijApp.m_curEbag.layer == null) {
				ManahijApp.m_curEbag.layer = new ShadowLayer(
						mp_context, view);
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// add the rule that places your button below your Node
				params.addRule(RelativeLayout.BELOW,
						ManahijApp.m_curEbag.m_shape.getId()/*
																	 * m_shape.getId
																	 * ()
																	 */);
				ManahijApp.m_curEbag.layer.setLayoutParams(params);
				parent.addView(ManahijApp.m_curEbag.layer,
						view.m_node.m_fWidth + 10, view.m_node.m_fHeight + 10);
			} else {
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				// add the rule that places your button below your Node
				params.addRule(RelativeLayout.BELOW,
						ManahijApp.m_curEbag.m_shape.getId());
				ManahijApp.m_curEbag.layer.setLayoutParams(params);
				ManahijApp.m_curEbag.layer.setNodeView(view);
				ManahijApp.m_curEbag.layer.invalidate();
				// parent.bringToFront();
			}
		} else {
			if (ManahijApp.m_curEbag.layer != null) {
				parent.removeView(ManahijApp.m_curEbag.layer);
				ManahijApp.m_curEbag.layer = null;
			}
		}
	}

	public RectF setLayoutBounds() {
		MPEbag curEbag = ManahijApp.m_curEbag;
		return curEbag.setMapBounds();
	}

	public void copyChildrenOfNode(MPNode selectedNode, MPNode newNode) {
	if(selectedNode.m_arrChildren!=null)
		for (MPNode childNode : selectedNode.m_arrChildren) {
			MPNode newChildNode = childNode;
			if (newNode.m_arrChildren == null) {
				newNode.m_arrChildren = new ArrayList<MPNode>();
			}
			newChildNode.m_parentNode = newNode;
			newNode.m_arrChildren.add(newChildNode);
			copyChildrenOfNode(childNode, newChildNode);
		}
	}

	public void calculateCenter(ViewGroup group) {
		float centerx = (group.getX() + group.getWidth()) / 2;
		float centery = (group.getY() + group.getHeight()) / 2;

		int x = ((group.getWidth() / 2) - MindMapActivity.centerX);
		int y = ((group.getHeight() / 2) - MindMapActivity.centerY);
		MindMapActivity.centerX = (group.getWidth() / 2);
		MindMapActivity.centerY = (group.getHeight() / 2);
		// MainActivity.twoDScroll.smoothScrollBy(x,y);
		// MainActivity.hScroll.smoothScrollBy(x, 0);
		// MainActivity.vScroll.smoothScrollBy(0,y);
		translatePosition(x, y, ManahijApp.m_curEbag);

	}

	public void translatePosition(int x, int y, MPNode node) {
		if (node.m_arrChildren != null)
			for (MPNode child : node.m_arrChildren) {
				translatePosition(x, y, child);
			}
		if (node != null) {
			node.m_fX = node.m_fX + x;
			node.m_fY = node.m_fY + y;
		}
	}

	public boolean changeNodePositionOf(MPNode node) {
		
			if(node.m_isCurrShapeBeingDragged){
			ViewGroup mapView = getMapView(node.m_shape);
			if (mapView == null)
				return false;

			// [mapView hideMenuItem];

			MPNodeView shape = node.m_shape;

			// [((MPMapView *)shape.superview) resetShapesHighlight];
			highLightShadowView(null, false);
			MPEbag curEbag = ManahijApp.m_curEbag;

			MPNode nearestNode = curEbag.m_nearestNodeToTheCurrentNodeBeingDragged;

			if (nearestNode != null && !nearestNode.equals(node.m_parentNode)) {

				if (nearestNode.m_arrChildren == null) {
					nearestNode.m_arrChildren = new ArrayList<MPNode>();
				}
				if (!nearestNode.m_arrChildren.contains(node)) {
					nearestNode.m_arrChildren.add(node);
					node.m_parentNode.m_arrChildren.remove(node);
					node.m_parentNode = nearestNode;
					this.updateChildrenColor(nearestNode,
							nearestNode.m_arrChildren);

				}

			}

			if (curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap
					&& nearestNode != null) {
				FreeMapLayout freeMapLayout = new FreeMapLayout(mp_context);
				Point pt = freeMapLayout.calculateNewChildPosition1(
						nearestNode, node);
				node.m_fX = pt.x;
				node.m_fY = pt.y;

			} else {

				if (nearestNode == null) {
					nearestNode = node.m_parentNode;
				}
				if (node.m_fY < nearestNode.m_fY) {
					node.m_isAboveCenter = true;
				} else {
					node.m_isAboveCenter = false;
				}
				if (node.m_fX + node.m_fWidth / 2 > curEbag.m_fX
						+ curEbag.m_fWidth / 2) {// Right Position
					if (nearestNode.m_iLevelId == 0) {
						node.m_isRightnode = true;

					}
				} else {// LeftPosition

					if (nearestNode.m_iLevelId == 0) {
						node.m_isRightnode = false;

					}
				}

				 if (curEbag.m_iMapLayout!= MapLayoutType.MPLayout_FreeMap&& nearestNode.equals(node.m_parentNode)) {
			          chageNodeOrder(node);
			        }	
			}
			}
			return false;
		

	}
	public void chageNodeOrder(MPNode curNode)
	{
	     ArrayList<MPNode> arrNodes = curNode.m_parentNode.m_arrChildren;
	    MPNodeView curShape = curNode.m_shape;
	    RectF curShapeRect = curShape.rect;
	    curShapeRect = new RectF(curShape.m_node.m_fX, curShape.m_node.m_fY,
	    		curShape.m_node.m_fX + curShape.m_node.m_fWidth, curShape.m_node.m_fY
						+ curShape.m_node.m_fHeight);
	    int oldIndexOfCurNode = arrNodes.indexOf(curNode);
	    int curIndex = 0;
	    for (int k=0;k<arrNodes.size();k++) {
	        MPNode node = arrNodes.get(k);
	        MPNodeView shape = node.m_shape;
	        RectF shapeRect = shape.rect;
	        shapeRect = new RectF(shape.m_node.m_fX, shape.m_node.m_fY,
					shape.m_node.m_fX + shape.m_node.m_fWidth, shape.m_node.m_fY
							+ shape.m_node.m_fHeight);
	        if (node.m_isAboveCenter && curNode.m_isAboveCenter) {
	            if (curShapeRect.top<shapeRect.top) {
	                curIndex = arrNodes.indexOf(node);
	               // NSLog(@"CurIndex = %d",curIndex);
	               // break;
	            }
	        }else if(!node.m_isAboveCenter && !curNode.m_isAboveCenter) {
	            
	            if (curShapeRect.top>shapeRect.top) {
	                  curIndex = arrNodes.indexOf(node);
	                  //NSLog(@"CurIndex = %d",curIndex);
	                  //break;
	            }
	            
	        }
	    }
	    arrNodes.remove(oldIndexOfCurNode);
	    arrNodes.add(curIndex, curNode); 
	}

	public void updateChildrenColor(MPNode parent, ArrayList<MPNode> arrChildren) {
		if (arrChildren != null && arrChildren.size() > 0)
			for (MPNode node : arrChildren) {
				node.backgroundColor = parent.backgroundColor;
				MPNodeView shape = node.m_shape;
				shape.setShapeBackgroundColor(node.backgroundColor);
				updateChildrenColor(node, node.m_arrChildren);
			}
	}

	public boolean isNearestNodeIsChildrenOf(MPNode curNode, MPNode nearestnode) {
		if (curNode.m_arrChildren != null && curNode.m_arrChildren.size() > 0)
			for (MPNode node : curNode.m_arrChildren) {

				if (node.equals(nearestnode)) {
					nearestnode.m_isChild = true;
					break;
				}
				isNearestNodeIsChildrenOf(node, nearestnode);

			}

		return nearestnode.m_isChild;
	}

	public void bringFronAll(MPNode view) {
		if (view.getM_arrChildren() != null
				&& view.getM_arrChildren().size() > 0)
			for (MPNode child : view.getM_arrChildren()) {
				bringFronAll(child);
			}
		view.m_shape.bringToFront();
	}

	public void addLinelayer(ViewGroup parent) {
		ArrayList<MPNodeView> childs = getChilds(parent);
		if (ManahijApp.m_curEbag.linesLayerView == null) {
			ManahijApp.m_curEbag.m_shape.bringToFront();
			// bringFronAll(ManahijApp.m_curEbag);
			ManahijApp.m_curEbag.linesLayerView = new LinesLayer(
					mp_context, childs);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					parent.getLayoutParams().width,
					parent.getLayoutParams().height);
			// add the rule that places your button below your Node
			params.addRule(RelativeLayout.BELOW,
					ManahijApp.m_curEbag.m_shape.getId());
			//params.addRule(RelativeLayout.CENTER_IN_PARENT);
			ManahijApp.m_curEbag.linesLayerView.setLayoutParams(params);
			// ManahijApp.m_curEbag.linesLayerView.setLayoutParams(new
			// RelativeLayout.LayoutParams(parent.getLayoutParams().width,parent.getLayoutParams().height));

			parent.addView(ManahijApp.m_curEbag.linesLayerView,
					AppDict.DEFAULT_OFFSET, AppDict.DEFAULT_OFFSET);
			parent.bringChildToFront(ManahijApp.m_curEbag.m_shape);
		} else {
			ManahijApp.m_curEbag.linesLayerView.childs = childs;
			// ManahijApp.m_curEbag.linesLayerView.getLayoutParams().width=parent.getLayoutParams().width;
			// ManahijApp.m_curEbag.linesLayerView.getLayoutParams().height=parent.getLayoutParams().height;
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					parent.getLayoutParams().width,
					parent.getLayoutParams().height);
			ManahijApp.m_curEbag.linesLayerView.setLayoutParams(params);
			ManahijApp.m_curEbag.linesLayerView.invalidate();
			bringFronAll(ManahijApp.m_curEbag);
			if (MindMapActivity.popUpLayout != null&& MindMapActivity.popUpLayout.getVisibility() == View.VISIBLE)
				MindMapActivity.popUpLayout.bringToFront();
		}
	}

	Rect drawingBounds;
	RectF visibleRectangle;
	RectF ebagBounds;

	public void calculateBounds() {
		ManahijApp.m_curEbag.setMapBounds();
		drawingBounds = new Rect();

		getMapView(ManahijApp.m_curEbag.m_shape).getLocalVisibleRect(
				drawingBounds);
		visibleRectangle = new RectF(drawingBounds);
		ebagBounds = ManahijApp.m_curEbag.getMapBounds();
	}

	public Bitmap takeScreenshoot(String title) {
		LayoutInflater mInflater = (LayoutInflater) mp_context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View screenShot = mInflater.inflate(R.layout.mi_layout_screenshot, null);
		RelativeLayout screenShotView = (RelativeLayout) screenShot
				.findViewById(R.id.screenShot);
		ManahijApp.m_curEbag.setMapBounds();
		calculateBounds();
		RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
				(int) (ebagBounds.right - ebagBounds.left) + 100,
				(int) (ebagBounds.bottom - ebagBounds.top) + 100);
		screenShotView.setLayoutParams(rlp);
		MPEbag tempebaEbag = ManahijApp.m_curEbag.copyMpeBag();
		tempebaEbag.m_fX = (tempebaEbag.m_fX - ebagBounds.left) + 50;
		tempebaEbag.m_fY = (tempebaEbag.m_fY - ebagBounds.top) + 50;
		ArrayList<MPNodeView> childs = getChilds(getMapView(ManahijApp.m_curEbag.m_shape));
		ArrayList<MPNodeView> subChilds = new ArrayList<MPNodeView>();
		
		for (MPNodeView nodeView : childs) {
			MPNode node = nodeView.m_node.copy();
			if (node.m_parentNode != null)
				node.m_parentNode = nodeView.m_node.m_parentNode.copy();
			float x = node.m_fX - ebagBounds.left;
			float y = node.m_fY - ebagBounds.top;
			if (node.m_parentNode == null)
				node.m_iLevelId = 0;
			node.m_fX = x + 50;
			node.m_fY = y + 50;
			node.m_isCollapsed=false;
			MPNodeView view = new MPNodeView(mp_context, node);
			view = getMpLine(view, tempebaEbag);
			subChilds.add(view);
		}
		screenShotView.removeView(ManahijApp.m_curEbag.linesLayerView);
		LinesLayer linesLayerView = new LinesLayer(mp_context, subChilds);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				screenShotView.getLayoutParams().width,
				screenShotView.getLayoutParams().height);
		linesLayerView.setLayoutParams(params);
		screenShotView.addView(linesLayerView,
				screenShotView.getLayoutParams().width,
				screenShotView.getLayoutParams().height);
		for (MPNodeView nodeview : subChilds) {
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			lp.leftMargin = (int) nodeview.m_node.m_fX;
			lp.topMargin = (int) nodeview.m_node.m_fY;
			screenShotView.addView(nodeview, lp);
		}
		screenShotView.setDrawingCacheEnabled(true);
		screenShotView.measure(MeasureSpec.makeMeasureSpec(
				screenShotView.getLayoutParams().width, MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(
						screenShotView.getLayoutParams().height,
						MeasureSpec.EXACTLY));
		screenShotView.layout(0, 0, screenShotView.getMeasuredWidth(),
				screenShotView.getMeasuredHeight());
		Bitmap b =null;
		try{
		 b = Bitmap.createBitmap(screenShotView.getWidth(),screenShotView.getHeight(), Bitmap.Config.RGB_565);
	
		Canvas c = new Canvas(b);
		c.drawColor(Color
				.parseColor(ManahijApp.m_curEbag.m_strMapLayoutFillColor));
		screenShotView.draw(c);
		screenShotView.setDrawingCacheEnabled(false);
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			File f = new File(Globals.TARGET_BASE_MINDMAP_PATH+title+".png");
		try {
			f.createNewFile();
			FileOutputStream fo = new FileOutputStream(f);
			fo.write(bytes.toByteArray());
			fo.close();
			Toast.makeText(mp_context, "Screenshot saved sucessfully ", Toast.LENGTH_LONG).show();
		} catch (Exception e) {
		}
		}
		 catch (OutOfMemoryError e) {

				} catch (Exception e) {

				}
		return b;

	}

	public MPNodeView getMpLine(MPNodeView view, MPEbag tempebaEbag) {
		MPNodeView draggingShap = view;
		MPNode draggingNode = view.m_node;
		if (draggingNode.m_parentNode != null) {
			draggingNode.m_parentNode.m_fX = (draggingNode.m_parentNode.m_fX - ebagBounds.left) + 50;
			draggingNode.m_parentNode.m_fY = (draggingNode.m_parentNode.m_fY - ebagBounds.top) + 50;
			if (draggingShap == null)
				return null;
			MPNodeView nearestShape = null;// = self.m_node.m_parentNode;
			nearestShape = hitTest(draggingShap);
			if (nearestShape == null) {
				if (draggingNode.m_parentNode.m_parentNode == null)
					draggingNode.m_parentNode.m_iLevelId = 0;
				nearestShape = new MPNodeView(mp_context,
						draggingNode.m_parentNode);// draggingNode.m_parentNode.m_shape;
			}
			if (draggingNode.m_fX + draggingNode.m_fWidth / 2 > tempebaEbag.m_fX
					+ tempebaEbag.m_fWidth / 2) {

				if (nearestShape.m_node.m_iLevelId == 0) {
					MPLinePoint lpoint = new MPLinePoint();
					lpoint.moveTo(nearestShape.m_node.m_fX + 10
							+ nearestShape.m_node.m_fWidth / 2,
							nearestShape.m_node.m_fY
									+ nearestShape.m_node.m_fHeight / 2);
					lpoint.qCurveto(draggingNode.m_fX - 60, draggingNode.m_fY
							+ draggingNode.m_fHeight / 2, draggingNode.m_fX,
							draggingNode.m_fY + draggingNode.m_fHeight / 2);
					draggingNode.m_lp = lpoint;
				} else {
					MPLinePoint lpoint = new MPLinePoint();
					lpoint.moveTo(nearestShape.m_node.m_fX - 10
							+ nearestShape.m_node.m_fWidth,
							nearestShape.m_node.m_fY
									+ nearestShape.m_node.m_fHeight / 2);
					lpoint.qCurveto(draggingNode.m_fX - 25, draggingNode.m_fY
							+ draggingNode.m_fHeight / 2, draggingNode.m_fX,
							draggingNode.m_fY + draggingNode.m_fHeight / 2);
					draggingNode.m_lp = lpoint;
				}

			} else {

				if (nearestShape.m_node.m_iLevelId == 0) {
					MPLinePoint lpoint = new MPLinePoint();
					lpoint.moveTo(draggingNode.m_fX + draggingNode.m_fWidth,
							draggingNode.m_fY + draggingNode.m_fHeight / 2);
					lpoint.qCurveto(draggingNode.m_fX + draggingNode.m_fWidth
							+ 10 + 60, draggingNode.m_fY
							+ draggingNode.m_fHeight / 2,
							nearestShape.m_node.m_fX
									+ nearestShape.m_node.m_fWidth / 2,
							nearestShape.m_node.m_fY
									+ nearestShape.m_node.m_fHeight / 2);
					draggingNode.m_lp = lpoint;

				} else {

					MPLinePoint lpoint = new MPLinePoint();
					lpoint.moveTo(draggingNode.m_fX + draggingNode.m_fWidth,
							draggingNode.m_fY + draggingNode.m_fHeight / 2);
					lpoint.qCurveto(draggingNode.m_fX + draggingNode.m_fWidth
							+ 10 + 25, draggingNode.m_fY
							+ draggingNode.m_fHeight / 2,
							nearestShape.m_node.m_fX, nearestShape.m_node.m_fY
									+ nearestShape.m_node.m_fHeight / 2);
					draggingNode.m_lp = lpoint;

				}
			}
		}
		draggingShap.setm_mpNode(draggingNode);
		return draggingShap;

	}
	public String getMapXML()
	{
		String dataTOReturn = "";
	   String str1="";
	
	   str1 =generateMapXml();
	
	   str1 = str1.replaceAll("\n", " ");
	  dataTOReturn=str1;
	  Log.d("","dataTOReturn"+dataTOReturn);
	return dataTOReturn;
	}
	
	public String generateMapXml() {
		String dataTOReturn="";
		Xmltags xmlTags=new Xmltags();
		File folder = new File(Environment.getExternalStorageDirectory()
				+ "/MindMap/");
		//	boolean success = true;
		if (!folder.exists()) {
		 folder.mkdir();
		}
		File outputFile = new File(Environment.getExternalStorageDirectory()
				+ "/MindMap/MindMap.xml");
		// File f = new File(getFilesDir(),"semanodeAndroid.xml");
		try {
			outputFile.createNewFile();
			FileOutputStream fileos = new FileOutputStream(outputFile);
			XmlSerializer xmlSerializer = Xml.newSerializer();
			StringWriter writer = new StringWriter();
			xmlSerializer.setOutput(writer);
			MPEbag ebag = ManahijApp.m_curEbag;
			ebag.setMapBounds();
			xmlSerializer.setFeature(
					"http://xmlpull.org/v1/doc/features.html#indent-output",
					true);
			xmlSerializer.startTag("", xmlTags.SemaFileSystem);
			xmlSerializer.startTag("", xmlTags.m_iId);
			xmlSerializer.text("" + ebag.m_iId);
			xmlSerializer.endTag("", xmlTags.m_iId);

			xmlSerializer.startTag("", xmlTags.m_iMaporFolder);
			xmlSerializer.text("" + 0);
			xmlSerializer.endTag("", xmlTags.m_iMaporFolder);
			xmlSerializer.startTag("", xmlTags.m_iParentFolderId);
			xmlSerializer.text("" + -1);
			xmlSerializer.endTag("", xmlTags.m_iParentFolderId);
			xmlSerializer.startTag("", xmlTags.m_strName);
			xmlSerializer.text("" + ebag.m_strNode_text);
			xmlSerializer.endTag("", xmlTags.m_strName);
			xmlSerializer.startTag("", xmlTags.m_strMapLayoutFillColor);
			xmlSerializer.text("" + ebag.m_strMapLayoutFillColor);
			xmlSerializer.endTag("", xmlTags.m_strMapLayoutFillColor);
			xmlSerializer.startTag("", xmlTags.m_iMapWidth);
			xmlSerializer.text("" + (ebag.m_fmaxX - ebag.m_fminX));
			xmlSerializer.endTag("", xmlTags.m_iMapWidth);
			xmlSerializer.startTag("", xmlTags.m_iMapHeight);
			xmlSerializer.text("" + (ebag.m_fmaxY - ebag.m_fminY));
			xmlSerializer.endTag("", xmlTags.m_iMapHeight);
			xmlSerializer.startTag("", xmlTags.m_iMapStyle);
			if (ebag.m_iMapLayoutLine == MapLayoutLineType.MPLayout_Line_Count)
				xmlSerializer.text("" + 1);
			else if (ebag.m_iMapLayoutLine == MapLayoutLineType.MPLayout_Line_Curved)
				xmlSerializer.text("" + 2);
			else if (ebag.m_iMapLayoutLine == MapLayoutLineType.MPLayout_Line_CurvedThick)
				xmlSerializer.text("" + 3);
			else if (ebag.m_iMapLayoutLine == MapLayoutLineType.MPLayout_Line_Straight)
				xmlSerializer.text("" + 4);
			xmlSerializer.endTag("", xmlTags.m_iMapStyle);
			xmlSerializer.startTag("", xmlTags.m_iMapLayout);
			if (ebag.m_iMapLayout == MapLayoutType.MPLayout_CircularMap)
				xmlSerializer.text("" + 0);
			else if (ebag.m_iMapLayout == MapLayoutType.MPLayout_LeftMap)
				xmlSerializer.text("" + 1);
			else if (ebag.m_iMapLayout == MapLayoutType.MPLayout_RightMap)
				xmlSerializer.text("" + 2);
			else if (ebag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap)
				xmlSerializer.text("" + 3);
			else if (ebag.m_iMapLayout == MapLayoutType.MPLayout_Count)
				xmlSerializer.text("" + 4);
			xmlSerializer.endTag("", xmlTags.m_iMapLayout);
			xmlSerializer.startTag("", xmlTags.m_strModifiedDate);
			xmlSerializer.endTag("", xmlTags.m_strModifiedDate);

			xmlSerializer.startTag("", xmlTags.m_strCreatedDate);
			xmlSerializer.endTag("", xmlTags.m_strCreatedDate);
			xmlSerializer.startTag("", xmlTags.m_strAuthor);
			xmlSerializer.endTag("", xmlTags.m_strAuthor);
			xmlSerializer.startTag("", xmlTags.m_strMapProperties);
			xmlSerializer.endTag("", xmlTags.m_strMapProperties);
			xmlSerializer.startTag("", xmlTags.m_strMapDescription);
			xmlSerializer.endTag("", xmlTags.m_strMapDescription);
			xmlSerializer.startTag("", xmlTags.m_strComment);
			xmlSerializer.endTag("", xmlTags.m_strComment);
			xmlSerializer.startTag("", xmlTags.m_iNumberOfRightNodes);
			xmlSerializer.text("" + ebag.m_iNumberOfRightNodes);
			xmlSerializer.endTag("", xmlTags.m_iNumberOfRightNodes);
			xmlSerializer.startTag("", xmlTags.m_isPublished);
			xmlSerializer.text("" + 0);
			xmlSerializer.endTag("", xmlTags.m_isPublished);
			xmlSerializer.startTag("", xmlTags.readAllSemaNodesinAMapResult);

			generatingNodeFromXml(ManahijApp.m_curEbag, xmlSerializer);
			xmlSerializer.endTag("", xmlTags.readAllSemaNodesinAMapResult);
			xmlSerializer.endTag("", xmlTags.SemaFileSystem);
			xmlSerializer.endDocument();
			writer.toString();
			dataTOReturn=writer.toString();
			fileos.write(writer.toString().getBytes());
		} catch (IOException e) {
			Log.e("IOException", "Exception in create new File(");
		}
		return dataTOReturn;
	}
	public String generateNodesXML() {
		String dataTOReturn = "";
		Xmltags xmlTags=new Xmltags();
		File outputFile = new File(Environment.getExternalStorageDirectory()
				+ "/MindMap/MindMap.xml");
		// File f = new File(getFilesDir(),"semanodeAndroid.xml");
		try {
			outputFile.createNewFile();
			FileOutputStream fileos = new FileOutputStream(outputFile);
			XmlSerializer xmlSerializer = Xml.newSerializer();
			StringWriter writer = new StringWriter();
			xmlSerializer.setOutput(writer);
			xmlSerializer.setFeature(
					"http://xmlpull.org/v1/doc/features.html#indent-output",
					true);
			xmlSerializer.startTag("", xmlTags.readAllSemaNodesinAMapResult);

			generatingNodeFromXml(ManahijApp.m_curEbag, xmlSerializer);
			xmlSerializer.endTag("", xmlTags.readAllSemaNodesinAMapResult);
			xmlSerializer.endDocument();
			writer.toString();
			 dataTOReturn = writer.toString();
			fileos.write(writer.toString().getBytes());
			Log.d("","writer.toString()"+writer.toString());
		} catch (IOException e) {
			Log.e("IOException", "Exception in create new File(");
		}
		return dataTOReturn;
	}
	Xmltags xmlTags=new Xmltags();
public void generatingNodeFromXml(MPNode ebag, XmlSerializer xmlSerializer) {
		
		try {
			xmlSerializer.startTag("", xmlTags.SemaNode);
			xmlSerializer.startTag("", xmlTags.m_iId);
			xmlSerializer.text("" + ebag.getM_iId());
			xmlSerializer.endTag("", xmlTags.m_iId);
			xmlSerializer.startTag("", xmlTags.m_iMapId);
			xmlSerializer.text("" + ebag.m_iMapId);
			xmlSerializer.endTag("", xmlTags.m_iMapId);
			/*
			 * xmlSerializer.startTag("", "m_iLevelId"); xmlSerializer.text(
			 * ""+ebag.m_iLevelId); xmlSerializer.endTag("","m_iLevelId");
			 */
			xmlSerializer.startTag("", xmlTags.m_iParentId);
			xmlSerializer.text("" + ebag.m_iParentId);
			xmlSerializer.endTag("", xmlTags.m_iParentId);
			xmlSerializer.startTag("", xmlTags.m_fX);
			xmlSerializer.text("" + ebag.m_fX);
			xmlSerializer.endTag("", xmlTags.m_fX);
			xmlSerializer.startTag("", xmlTags.m_fY);
			xmlSerializer.text("" + ebag.m_fY);
			xmlSerializer.endTag("", xmlTags.m_fY);
			xmlSerializer.startTag("", xmlTags.m_fWidth);
			xmlSerializer.text("" + ebag.m_fWidth);
			xmlSerializer.endTag("", xmlTags.m_fWidth);
			xmlSerializer.startTag("", xmlTags.m_fHeight);
			xmlSerializer.text("" + ebag.m_fHeight);
			xmlSerializer.endTag("", xmlTags.m_fHeight);
			xmlSerializer.startTag("", xmlTags.m_strNode_text);
			String nodeText= StringEscapeUtils.unescapeJava(ebag.m_strNode_text);
			xmlSerializer.text("" + nodeText);
			xmlSerializer.endTag("", xmlTags.m_strNode_text);

			xmlSerializer.startTag("", xmlTags.m_iNode_TextSize);
			xmlSerializer.text("" + ebag.getM_iNode_TextSize());
			xmlSerializer.endTag("", xmlTags.m_iNode_TextSize);
			xmlSerializer.startTag("", xmlTags.m_strNode_TextFontFamily);
			xmlSerializer.text("" + ebag.getM_strFontName());
			xmlSerializer.endTag("", xmlTags.m_strNode_TextFontFamily);

			xmlSerializer.startTag("", xmlTags.m_strNode_TextAlign);
			xmlSerializer.text("" + 1);
			xmlSerializer.endTag("", xmlTags.m_strNode_TextAlign);
			
			xmlSerializer.startTag("", xmlTags.m_strNode_TextFillColor);
			xmlSerializer.text(""+ebag.getTextColor());
			xmlSerializer.endTag("", xmlTags.m_strNode_TextFillColor);
			
			
			xmlSerializer.startTag("", xmlTags.m_strNode_TextFillColor);
			xmlSerializer.text("" + ebag.getTextColor());
			xmlSerializer.endTag("", xmlTags.m_strNode_TextFillColor);

			xmlSerializer.startTag("", xmlTags.m_boolPinned);
			xmlSerializer.text("" + 0);
			xmlSerializer.endTag("", xmlTags.m_boolPinned);
			xmlSerializer.startTag("", xmlTags.m_iPriority);
			xmlSerializer.text("" + ebag.getPriorityCharacterPosition());
			xmlSerializer.endTag("", xmlTags.m_iPriority);
			xmlSerializer.startTag("", xmlTags.m_iCompletion);
			xmlSerializer.text("" +  ebag.getCompletionCharacterPosition());
			xmlSerializer.endTag("", xmlTags.m_iCompletion);
			xmlSerializer.startTag("", xmlTags.m_strStartDate);
			xmlSerializer.text("" +  ebag.getM_start_date());
			xmlSerializer.endTag("", xmlTags.m_strStartDate);
			xmlSerializer.startTag("", xmlTags.m_strDueDate);
			xmlSerializer.text("" +  ebag.getM_end_date());
			xmlSerializer.endTag("", xmlTags.m_strDueDate);

			xmlSerializer.startTag("", xmlTags.m_strResourceType);
			xmlSerializer.text("" +  ebag.getM_resource());
			xmlSerializer.endTag("", xmlTags.m_strResourceType);
			xmlSerializer.startTag("", xmlTags.m_strShapeLineColor);
			xmlSerializer.text("" +"#FFFFFF");
			xmlSerializer.endTag("", xmlTags.m_strShapeLineColor);
			
			xmlSerializer.startTag("", xmlTags.m_iResourceDuration);
			xmlSerializer.text("" +ebag.m_duration);
			xmlSerializer.endTag("", xmlTags.m_iResourceDuration);
			xmlSerializer.startTag("", xmlTags.m_strDurationType);
			xmlSerializer.text("" +ebag.getM_duration_type());
			xmlSerializer.endTag("", xmlTags.m_strDurationType);
			xmlSerializer.startTag("", xmlTags.m_iIsBold);
			xmlSerializer.text("" + ebag.m_iIsBold);
			xmlSerializer.endTag("", xmlTags.m_iIsBold);
			xmlSerializer.startTag("", xmlTags.m_iIsItalic);
			xmlSerializer.text("" +  ebag.m_iIsItalic);
			xmlSerializer.endTag("", xmlTags.m_iIsItalic);
			xmlSerializer.startTag("", xmlTags.m_iIsUnderline);
			xmlSerializer.text("" +  ebag.m_iIsUnderline);
			xmlSerializer.endTag("", xmlTags.m_iIsUnderline);
			xmlSerializer.startTag("", xmlTags.m_strMultimediaType);
			String m_type="";
			switch (ebag.mediaType) {
			case 0:
				m_type="Image";
				break;
			case 1:
				m_type="swf";
				break;
			case 2:
				m_type="Audio";
				break;
			case 3:
				m_type="Video";
				break;
			default:
				break;
			}
			xmlSerializer.text("" +m_type);
			xmlSerializer.endTag("", xmlTags.m_strMultimediaType);
			xmlSerializer.startTag("", xmlTags.m_strMultimediaUrl);
			xmlSerializer.text("" +ebag.m_strMultimediaUrl);
			xmlSerializer.endTag("", xmlTags.m_strMultimediaUrl);
			xmlSerializer.startTag("", xmlTags.m_strHyperlinkUrl);
			xmlSerializer.text("" +ebag.m_strHyperlinkUrl);
			xmlSerializer.endTag("", xmlTags.m_strHyperlinkUrl);
			xmlSerializer.startTag("", xmlTags.m_strHyperlinkDescription);
			xmlSerializer.text("" +ebag.m_strHyperlinkDescription);
			xmlSerializer.endTag("", xmlTags.m_strHyperlinkDescription);
			xmlSerializer.startTag("", xmlTags.m_strOpenLinkIn);
			xmlSerializer.endTag("", xmlTags.m_strOpenLinkIn);
			xmlSerializer.startTag("", xmlTags.m_iSymbol);
			xmlSerializer.text("" + ebag.getdCharacterPosition());
			xmlSerializer.endTag("", xmlTags.m_iSymbol);
			xmlSerializer.startTag("", xmlTags.m_iIsAttachment);
			xmlSerializer.text("" + 0);
			xmlSerializer.endTag("", xmlTags.m_iIsAttachment);
			xmlSerializer.startTag("", xmlTags.m_strAttachmentImage);
			if (ebag.m_attachement_image == null)
				xmlSerializer.text("data:image/png;base64,(null)");
			else {
				xmlSerializer.text("data:image/png;base64,"
						+ AppDict.encodeTobase64(ebag.m_attachement_image));
			}
			xmlSerializer.endTag("", xmlTags.m_strAttachmentImage);
			
			xmlSerializer.startTag("", xmlTags.m_strEmbedImage);
			if (ebag.embededImage == null)
				xmlSerializer.text("data:image/png;base64,(null)");
			else {
				xmlSerializer.text("data:image/png;base64,"
						+ AppDict.encodeTobase64(ebag.embededImage));
			}
			xmlSerializer.endTag("", xmlTags.m_strEmbedImage);
			
			xmlSerializer.startTag("", xmlTags.m_strNodeShapeFill);
			xmlSerializer.text("" + ebag.getHexaColor());
			xmlSerializer.endTag("", xmlTags.m_strNodeShapeFill);
			xmlSerializer.startTag("", xmlTags.m_iSmileys);
			xmlSerializer.text("" + ebag.getSmileIconPosition());
			xmlSerializer.endTag("", xmlTags.m_iSmileys);
			/*xmlSerializer.startTag("", xmlTags.m_strSmileImage);
			if (ebag.m_smile_image == null)
				xmlSerializer.text("data:image/png;base64,(null)");
			else
				xmlSerializer.text("data:image/png;base64,"
						+ AppDict.encodeTobase64(ebag.m_smile_image));
			xmlSerializer.endTag("", xmlTags.m_strSmileImage);*/

			xmlSerializer.startTag("", xmlTags.m_strBgImage);
			if (ebag.m_background_image == null)
				xmlSerializer.text("data:image/png;base64,(null)");
			else
				xmlSerializer.text("data:image/png;base64,"
						+ AppDict.encodeTobase64(ebag.m_background_image));
			xmlSerializer.endTag("", xmlTags.m_strBgImage);
			

			xmlSerializer.startTag("", xmlTags.m_strMultimediaType);
			if(ebag.mediaType==2)
			xmlSerializer.text("" + "Audio");
			else if(ebag.mediaType==3)
			xmlSerializer.text("" +"Video");
			xmlSerializer.endTag("", xmlTags.m_strMultimediaType);
			
			xmlSerializer.startTag("", xmlTags.m_strMultimediaUrl);
			if(ebag.mediaType==2||ebag.mediaType==3)
			xmlSerializer.text("" + ebag.multimediaurl);
			xmlSerializer.endTag("", xmlTags.m_strMultimediaUrl);
			
			
			
			xmlSerializer.startTag("", xmlTags.m_iFlags);
			xmlSerializer.text("" + 0);
			xmlSerializer.endTag("", xmlTags.m_iFlags);
			xmlSerializer.startTag("", xmlTags.m_iCheckedStatus);
			xmlSerializer.text("" + ebag.genCharacterPosition);
			xmlSerializer.endTag("", xmlTags.m_iCheckedStatus);
			xmlSerializer.startTag("", xmlTags.m_lImage);
			xmlSerializer.text("" + 0);
			xmlSerializer.endTag("", xmlTags.m_lImage);
			xmlSerializer.startTag("", xmlTags.m_iGradientType);
			xmlSerializer.text("" + ebag.m_iGradientType);
			xmlSerializer.endTag("", xmlTags.m_iGradientType);
			xmlSerializer.startTag("", xmlTags.m_iNodeShape);
			if (ebag.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON)
				xmlSerializer.text("" + 4);
			else if (ebag.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_COMMENT)
				xmlSerializer.text("" + 5);
			else if (ebag.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_FREEFORM)
				xmlSerializer.text("" + 6);
			else if (ebag.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_RECTANGLE)
				xmlSerializer.text("" + 1);
			else if (ebag.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_ROUNDEDRECTANGLE)
				xmlSerializer.text("" + 2);
			else if (ebag.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_OVAL)
				xmlSerializer.text("" + 3);
			else if (ebag.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_OCTAGON)
				xmlSerializer.text("" + 7);
			else if (ebag.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_INVERSE_TRIANGLE)
				xmlSerializer.text("" + 8);
			else if (ebag.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_RHOMBUS)
				xmlSerializer.text("" + 9);
			xmlSerializer.endTag("", xmlTags.m_iNodeShape);

			xmlSerializer.startTag("", xmlTags.m_iNode_Order);
			xmlSerializer.text("" + ebag.m_iNode_Order);
			xmlSerializer.endTag("", xmlTags.m_iNode_Order);
			xmlSerializer.startTag("", xmlTags.m_strNode_Notes);
			String m_strNode_Notes= StringEscapeUtils.unescapeJava(ebag.m_strNode_Notes);
			xmlSerializer.text("" + m_strNode_Notes);
			xmlSerializer.endTag("", xmlTags.m_strNode_Notes);
			xmlSerializer.startTag("", xmlTags.m_iIsAboveCenter);
			if (ebag.m_isAboveCenter)
				xmlSerializer.text("" + 1);
			else
				xmlSerializer.text("" + 0);
			xmlSerializer.endTag("", xmlTags.m_iIsAboveCenter);
			xmlSerializer.startTag("", xmlTags.m_isRightnode);
			if (ebag.m_isRightnode)
				xmlSerializer.text("" + 1);
			else
				xmlSerializer.text("" + 0);
			xmlSerializer.endTag("", xmlTags.m_isRightnode);
			if (ebag.m_arrChildren != null) {
				xmlSerializer.startTag("", xmlTags.children);
				for (MPNode node : ebag.m_arrChildren) {
					generatingNodeFromXml(node, xmlSerializer);
				}
				xmlSerializer.endTag("", xmlTags.children);

			} else {
				xmlSerializer.startTag("", xmlTags.children);
				xmlSerializer.endTag("", xmlTags.children);
			}
			xmlSerializer.endTag("", xmlTags.SemaNode);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void addOrphanNode(String strNodeText){
	{
	   // Point offset = self.contentOffset;
		
	    calculateBounds();
	    MPNode orphanNode = new MPNode();
	    orphanNode.m_iLevelId = 1;
	    orphanNode.m_strNode_text = strNodeText;
	    orphanNode.m_fWidth = AppDict.mDrawBitmapWidth;
	    orphanNode.m_fHeight = AppDict.mDrawBitmapHeight;
	    orphanNode.m_iLevelId=-1;
	    orphanNode.m_fX=(float) (visibleRectangle.left+ MindMapActivity.width/2.0);
	    orphanNode.m_fY=visibleRectangle.top+50;
	    RectF rect = orphanNode.getBounds();
	    orphanNode.setM_strNode_Notes("");
	    MPNodeView Orphanshape = new MPNodeView(mp_context, orphanNode);
	    Orphanshape.m_node = orphanNode;
	    Orphanshape.backgroundColor = orphanNode.backgroundColor;
	    Orphanshape.setOnTouchListener(((MindMapActivity) mp_context).myGestureListener);
	   // NSLog(@"orphanNode is calling");    
	    ((MindMapActivity)mp_context).mPUndoManager.addUndooperationOnNode(orphanNode , UndoOperationKey.UndoOperationKey_orphannode);
		getMapView(ManahijApp.m_curEbag.m_shape).addView(Orphanshape, AppDict.mDrawBitmapWidth, AppDict.mDrawBitmapHeight);
	    orphanNode.m_shape=Orphanshape;
	    if (orphanNode.m_strNode_text.length()>0) {
	        if (orphanNode.m_strNode_text.length()>15){
	            orphanNode.m_strNode_Notes = orphanNode.m_strNode_text;
	            String strText = orphanNode.m_strNode_text.substring(0, 15);
	            orphanNode.m_strNode_text = strText;
	            //Orphanshape setNotesImageProperty:TRUE];
	        }
	    }

	    
	    //Orphanshape.setNodeText(orphanNode.m_strNode_text);
	}
	}
	public void invalidate(ViewGroup parent ){
		ArrayList<MPNodeView> childs = getChilds(parent);
		for(MPNodeView node:childs){
			node.invalidate();
		}
	}
	
	public void createShapes(MPNode node, MPNode parentNode) {
		if (node.m_arrChildren != null)
			for (MPNode node1 : node.m_arrChildren) {
				createShapes(node1, node);
			}
		node.m_parentNode = parentNode;
		MPNodeView newShape = new MPNodeView(mp_context, node);
		newShape.setBackgroundColor(Color.TRANSPARENT);
		newShape.setOnTouchListener(((MindMapActivity) mp_context).myGestureListener);

		getMapView(parentNode.m_shape).addView(newShape, AppDict.mDrawBitmapWidth,
				AppDict.mDrawBitmapHeight);

		newShape.m_node = node;
		node.m_shape = newShape;
		//node.backgroundColor = node.m_parentNode.m_selectedColor;
		//node.m_selectedColor = node.m_parentNode.m_selectedColor;

		

	}
	public void addUndoNode(MPNode node) {
		 ViewGroup mapView = getMapView(node.m_parentNode.m_shape);
	    if (mapView == null)
	        return;
	    //MPEbag *curEbag = [MPLibrary sharedInstance].m_curEbag;
	    if (node.m_parentNode.m_arrChildren == null) {
	        node.m_parentNode.m_arrChildren = new ArrayList<MPNode>();
	    }
	    node.m_parentNode.m_arrChildren.add(node);
	    mapView.addView(node.m_shape);
	    if(node.m_arrChildren!=null)
	    	for(MPNode nodeChild:node.m_arrChildren)
	    		addshapes(mapView,nodeChild);
	  this.setLayout(node);
	    mapView.invalidate();;
	}
	public void addshapes(ViewGroup mapView,MPNode node){
		if(node.m_arrChildren!=null)
			for(MPNode nodeChild:node.m_arrChildren)
				addshapes(mapView,nodeChild);
		/*if(node.m_shape.getParent()==mapView)
			mapView.removeView(node.m_shape);*/
		mapView.addView(node.m_shape);
		  this.setLayout(node);
	}

}
