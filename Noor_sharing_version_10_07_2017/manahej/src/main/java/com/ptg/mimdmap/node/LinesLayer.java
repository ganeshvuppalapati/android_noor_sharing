package com.ptg.mimdmap.node;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.view.View;

import com.ptg.mindmap.enums.MapLayoutLineType;
import com.ptg.mindmap.enums.MapLayoutType;
import com.semanoor.manahij.ManahijApp;

import java.util.ArrayList;

public class LinesLayer extends View {

	Paint paint = new Paint();
	MPNode mNode;
	Path path;
	public ArrayList<MPNodeView> childs;
	public LinesLayer(Context context, ArrayList<MPNodeView> childs) {
		super(context);
		path = new Path();
		//setBackgroundColor(Color.parseColor("#80123456"));
		paint.setFlags(Paint.ANTI_ALIAS_FLAG);
		this.childs=childs;
	}
	RectF visibleRectangle;
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		for(MPNodeView view:childs ){
			this.mNode = view.m_node;
			path = new Path();
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(5.0f);
		if(mNode.m_lp!=null)
		if (mNode.m_lp.isHighlight)
			paint.setColor(Color.RED);
		else
			paint.setColor(mNode.getBackgroundColor());
	
		if (mNode.m_lp != null) {
			path.reset();
					drawLine();
						paint.setPathEffect(null);
			if(ManahijApp.m_curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap){
			paint.setPathEffect(new DashPathEffect(new float[] { 5, 10,},0));
			paint.setStrokeWidth(6);
			}
			if(ManahijApp.m_curEbag.m_iMapLayoutLine== MapLayoutLineType.MPLayout_Line_CurvedThick)
				paint.setStrokeWidth(6);
			canvas.drawPath(path, paint);
		}
		}
		// invalidate();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int heightMeasured = 0;

		if (heightMeasureSpec == 0) {
			heightMeasureSpec = MeasureSpec.makeMeasureSpec(heightMeasured,
					MeasureSpec.EXACTLY);
		}

		setMeasuredDimension(
				getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec),
				getDefaultSize(this.getSuggestedMinimumHeight(),
						heightMeasureSpec));
	}
	
	/* @Override
	    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	        setMeasuredDimension(measureWidth(widthMeasureSpec),
	                measureHeight(heightMeasureSpec));
	    }*/

	    /**
	     * Determines the width of this view
	     * @param measureSpec A measureSpec packed into an int
	     * @return The width of the view, honoring constraints from measureSpec
	     */
	    private int measureWidth(int measureSpec) {
	        int result = 0;
	        //This is because of background image in relativeLayout, which is 1000*1000px
	      //  measureSpec =  (int) MindMapApplication.m_curEbag.m_fmaxX+MindMapApplication.m_curEbag.m_fWidth;
	        if(mNode.m_isRightnode)
	        	  measureSpec =  (int) (mNode.m_fX-mNode.m_parentNode.m_fX);
	        else
	        measureSpec =  (int) (mNode.m_parentNode.m_fX-mNode.m_fX);
	        int specMode = MeasureSpec.getMode(measureSpec);
	        int specSize = MeasureSpec.getSize(measureSpec);

	        if (specMode == MeasureSpec.UNSPECIFIED) {
	            // We were told how big to be
	            result = specSize;
	        }

	        return result;
	    }

	    /**
	     * Determines the height of this view
	     * @param measureSpec A measureSpec packed into an int
	     * @return The height of the view, honoring constraints from measureSpec
	     */
	   private int measureHeight(int measureSpec) {
	        int result = 0;
	        //This is because of background image in relativeLayout, which is 1000*1000px 
	       // measureSpec = (int) MindMapApplication.m_curEbag.m_fmaxY+MindMapApplication.m_curEbag.m_fWidth;
	        if(mNode.m_isAboveCenter)
	        	measureSpec=(int)  (mNode.m_parentNode.m_fY-mNode.m_fY);
	        else
	        	measureSpec =  (int) (mNode.m_fY-mNode.m_parentNode.m_fY);
	        int specMode = MeasureSpec.getMode(measureSpec);
	        int specSize = MeasureSpec.getSize(measureSpec);


	        if (specMode == MeasureSpec.UNSPECIFIED) {
	            // Here we say how Heigh to be
	            result = specSize;
	        } 
	        return result;
	    }


	public void drawLine() {

		path.moveTo(mNode.m_lp.moveToPoint.m_fArg1,
				mNode.m_lp.moveToPoint.m_fArg2);
		// for line style fo current node will always curved
		if (ManahijApp.m_curEbag.m_iMapLayout == MapLayoutType.MPLayout_FreeMap
				|| ManahijApp.m_curEbag.m_iMapLayoutLine == MapLayoutLineType.MPLayout_Line_Curved
				|| (mNode.m_parentNode
						.equals(ManahijApp.m_curEbag.m_currNodeBeingDragged))) {

			if (mNode.m_lp.qCurveToPoint != null) {
				path.quadTo(mNode.m_lp.qCurveToPoint.m_fArg1,
						mNode.m_lp.qCurveToPoint.m_fArg2,
						mNode.m_lp.qCurveToPoint.m_fArg3,
						mNode.m_lp.qCurveToPoint.m_fArg4);
			} else if (mNode.m_lp.bCurveToPoint != null) {
				path.cubicTo(mNode.m_lp.bCurveToPoint.m_fArg1,
						mNode.m_lp.bCurveToPoint.m_fArg2,
						mNode.m_lp.bCurveToPoint.m_fArg3,
						mNode.m_lp.bCurveToPoint.m_fArg4,
						mNode.m_lp.bCurveToPoint.m_fArg5,
						mNode.m_lp.bCurveToPoint.m_fArg6);
				
			}

		} else {

			if (ManahijApp.m_curEbag.m_iMapLayoutLine == MapLayoutLineType.MPLayout_Line_CurvedThick) {
				path.quadTo(mNode.m_lp.qCurveToPoint.m_fArg1,
						mNode.m_lp.qCurveToPoint.m_fArg2,
						mNode.m_lp.qCurveToPoint.m_fArg3,
						mNode.m_lp.qCurveToPoint.m_fArg4);
				path.moveTo(mNode.m_lp.moveToPoint.m_fArg1 + 1,
						mNode.m_lp.moveToPoint.m_fArg2);
				path.quadTo(mNode.m_lp.qCurveToPoint.m_fArg1,
						mNode.m_lp.qCurveToPoint.m_fArg2,
						mNode.m_lp.qCurveToPoint.m_fArg3,
						mNode.m_lp.qCurveToPoint.m_fArg4);
				path.moveTo(mNode.m_lp.moveToPoint.m_fArg1 + 2,
						mNode.m_lp.moveToPoint.m_fArg2);
				path.quadTo(mNode.m_lp.qCurveToPoint.m_fArg1,
						mNode.m_lp.qCurveToPoint.m_fArg2,
						mNode.m_lp.qCurveToPoint.m_fArg3,
						mNode.m_lp.qCurveToPoint.m_fArg4);
				path.moveTo(mNode.m_lp.moveToPoint.m_fArg1 + 3,
						mNode.m_lp.moveToPoint.m_fArg2);
				path.quadTo(mNode.m_lp.qCurveToPoint.m_fArg1,
						mNode.m_lp.qCurveToPoint.m_fArg2,
						mNode.m_lp.qCurveToPoint.m_fArg3,
						mNode.m_lp.qCurveToPoint.m_fArg4);
				path.moveTo(mNode.m_lp.moveToPoint.m_fArg1 + 4,
						mNode.m_lp.moveToPoint.m_fArg2);
				path.quadTo(mNode.m_lp.qCurveToPoint.m_fArg1,
						mNode.m_lp.qCurveToPoint.m_fArg2,
						mNode.m_lp.qCurveToPoint.m_fArg3,
						mNode.m_lp.qCurveToPoint.m_fArg4);
			} else {
				float off = 0.0f;
				if(mNode.m_parentNode!=null)
				if (mNode.m_parentNode.m_iLevelId == 0) {
					if (mNode.m_lp.m_iType == 0) {// right Nodes
						off = -5.0f;
					} else {
						off = 15.0f;
					}
				} else {
					if (mNode.m_lp.m_iType == 0) {// right Nodes
						off = -14.0f;
					} else {
						off = 15.0f;
					}
				}

				path.lineTo(mNode.m_lp.qCurveToPoint.m_fArg1 + off,
						mNode.m_lp.qCurveToPoint.m_fArg2);
				path.lineTo(mNode.m_lp.qCurveToPoint.m_fArg3,
						mNode.m_lp.qCurveToPoint.m_fArg4);

			}

		}
	}

}
