package com.ptg.mimdmap.line;


public class MPLinePoint {
	
	public MoveToPoint moveToPoint;
	public QCurveToPoint qCurveToPoint;
	public BCurveToPoint bCurveToPoint;
	public boolean isHighlight;
	 public int m_iType;

	public void moveTo(float x, float y){
		moveToPoint=new MoveToPoint(x, y);
	}
	public void qCurveto (float cpx,float cpy,float qx, float qy){
		qCurveToPoint=new QCurveToPoint(cpx, cpy, qx, qy);
	}
	public void bCurveto (float cpx, float cpy,float cp1x,float cp1y,float qx,float qy){
		bCurveToPoint=new BCurveToPoint(cpx, cpy, cp1x, cp1y, qx, qy);
	}

	

}
