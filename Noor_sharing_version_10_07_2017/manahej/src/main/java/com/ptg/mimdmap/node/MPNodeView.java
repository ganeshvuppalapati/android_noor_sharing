package com.ptg.mimdmap.node;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import com.ptg.mimdmap.line.Line;
import com.ptg.mindmap.enums.MapLayoutShapeType;
import com.ptg.mindmap.widget.AppDict;
import com.semanoor.manahij.R;

public class MPNodeView extends View{
	protected Paint backgroundPaint;
	protected Paint hilightPaint;
	protected Paint borderPaint;
	protected Paint collapsePaint;
	public MPNode m_node;
	public RectF rect;
	RectF selectedRect;
	public Line lineView;
	public Bitmap imageAttachementBitmap=null;
	public boolean hilightHit=false;
	public Line getLineView() {
		return lineView;
	}

	public void setLineView(Line lineView) {
		this.lineView = lineView;
	}

	public MPNode getm_mpNode() {
		return m_node;
	}

	public void setm_mpNode(MPNode mPNodeProperties) {
		this.m_node = mPNodeProperties;
	}

	protected static final int borderSize = 5;
	public Paint textPaint;
	public Paint taskInfoPaint;

	public RectF getRect() {
		return this.rect;
	}

	public void setRect(RectF rect) {
		this.rect = rect;
	}
	
	//private static final int TEXT_LEFT = 100;
	private static int LINE1_VERT_OFFSET = 45;
	private static final int CORNER_RADIUS = 20;
	private Context context;
	public int backgroundColor;
	public boolean hidden=false;
	public ViewGroup delegate;
	public Bitmap notesImg;
	public Bitmap hyperLink,audioHyperLink,videoHyperLink;
	public Bitmap imageLinkImg;
	public Bitmap smileImg;
	//private static final int PIC_HORIZONTAL_MARGIN = 5;
	private static int PIC_VERTICAL_MARGIN = 10;
	//	private static final int ATTACHEMENT_PIC_VERTICAL_MARGIN = 70;
	//private static final int SMILE_PIC_VERTICAL_MARGIN = 40;

	private final int PADDING_RIGHT=20;
	private   int PIC_SIZE = 40;
	private int info_posY=20;
	//	Rect rectFRect;
	public MPNodeView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public MPNodeView(Context context, AttributeSet aSet) {
		super(context, aSet);
		this.context = context;

		init();


	}

	public MPNodeView(Context context) {
		super(context);
		setFocusable(true);
	}

	public MPNodeView(Context context, MPNode node) {
		super(context);
		this.context = context;
		this.m_node = node;
		//this.m_node.m_img3=this.m_node.embededImage;
		this.m_node.m_shape=this;

		init();
	}

	public void init() {
		backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		borderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		collapsePaint= new Paint(Paint.ANTI_ALIAS_FLAG);
		textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		taskInfoPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		taskInfoPaint.setTextSize(AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,
				10));
		this.rect = new RectF(0, 0, m_node.getM_fWidth(), m_node.getM_fHeight());
		selectedRect = new RectF(-10,  - 10, m_node.getM_fWidth() + 10,  m_node.getM_fHeight()
				+ 10);

		hidden=false;
		textPaint.setStyle(Paint.Style.STROKE);
		taskInfoPaint.setStyle(Paint.Style.STROKE);
		borderPaint.setStyle(Paint.Style.STROKE);
		collapsePaint.setStyle(Paint.Style.STROKE);
		borderPaint.setColor(Color.BLUE);
		borderPaint.setStrokeWidth(4f);
		collapsePaint.setColor(Color.BLUE);
		collapsePaint.setStrokeWidth(4f);
		collapsePaint.setPathEffect(new DashPathEffect(new float[] { 10, 20,},0));
		notesImg = BitmapFactory.decodeResource(context.getResources(), R.drawable.notes);
		notesImg = scaleAttchament(notesImg);
		imageLinkImg = BitmapFactory.decodeResource(context.getResources(), R.drawable.imagelink);
		imageLinkImg = scaleAttchament(imageLinkImg);
		hyperLink = BitmapFactory.decodeResource(context.getResources(), R.drawable.mi_hyperlink);
		hyperLink = scaleAttchament(hyperLink);
		audioHyperLink = BitmapFactory.decodeResource(context.getResources(), R.drawable.mi_audio_hyperlink);
		audioHyperLink = scaleAttchament(audioHyperLink);
		videoHyperLink = BitmapFactory.decodeResource(context.getResources(), R.drawable.mi_video_hyperlink);
		videoHyperLink = scaleAttchament(videoHyperLink);
		hilightPaint=backgroundPaint;
		// hilightPaint.setColor(Color.MAGENTA);

	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		calculateTextWidth();
		Path path = new Path();
		//canvas.drawColor(Color.parseColor("#6E2E0E"));
		int xw=m_node.m_fWidth;
		int yh=m_node.m_fHeight;
		Rect rectFRect =null;
		switch (m_node.m_iMapLayoutShape){
		case MPLAYOUT_SHAPE_ROUNDEDRECTANGLE:
			if(m_node.m_background_image==null){
				canvas.drawRoundRect(rect, CORNER_RADIUS, CORNER_RADIUS,backgroundPaint);

			}else
			{
				rectFRect = new Rect(0, 0, m_node.m_background_image.getWidth(), m_node.m_background_image.getHeight());
				canvas.drawBitmap(getRoundedCornerBitmap(m_node.m_background_image, CORNER_RADIUS), rectFRect, rect, backgroundPaint);

			}
			if (m_node.isM_isCollapsed()) 
				canvas.drawRoundRect(rect, CORNER_RADIUS, CORNER_RADIUS,collapsePaint);

			else{
				if (m_node.selected) 
					canvas.drawRoundRect(rect, CORNER_RADIUS, CORNER_RADIUS,borderPaint);

			}
			break;
		case MPLAYOUT_SHAPE_PENTAGON:
			path.reset();
			if(m_node.m_background_image!=null){
				//calculateWidth();
			}
			/* m_node.m_fHeight= m_node.m_fWidth;
			 rect= rectfBounds();*/
			// m_node.m_fHeight=m_node.m_fWidth;;
			//	rect= rectfBounds();

			float minX = rect.left;
			float minY = rect.top;
			float midX = rect.centerX();
			float midY = rect.centerY();
			float maxX = rect.right;
			float maxY = rect.bottom;
			float   radius =( (m_node.m_fX+m_node.m_fWidth)-m_node.m_fX) / 2 - 10;
			path.moveTo(midX,minY);
			path.lineTo(maxX,midY);
			path.lineTo(maxX-maxX/4,maxY);
			path.lineTo(minX+maxX/4,maxY);
			path.lineTo(minX,midY);
			path.lineTo(midX,minY);
			if(m_node.m_background_image==null)
				canvas.drawPath(path, backgroundPaint);
			else {

				canvas.clipPath(path);
				Bitmap sourceBitmap = m_node.m_background_image;
				canvas.drawBitmap(sourceBitmap,new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight()), new Rect(0, 0, m_node.m_fWidth,m_node.m_fHeight), null);

			}


			if (m_node.isM_isCollapsed())
				canvas.drawPath(path,collapsePaint);
			else{
				if (m_node.selected) 
					canvas.drawPath(path,borderPaint);
			}

			break;

		case MPLAYOUT_SHAPE_RECTANGLE:
			if(m_node.m_background_image!=null){
				//m_node.m_fHeight=100;
				//	m_node.m_fWidth=280;
				rect= rectfBounds();
			}
			if(m_node.m_background_image==null){

				canvas.drawRect(rect, backgroundPaint);
			}	else	{
				//calculateWidth();
				//rect= rectfBounds();
				rectFRect = new Rect(0, 0, m_node.m_background_image.getWidth(), m_node.m_background_image.getHeight());
				canvas.drawBitmap(getRoundedCornerBitmap(m_node.m_background_image, 0), rectFRect, rect, backgroundPaint);

			}
			if (m_node.isM_isCollapsed())
				canvas.drawRect(rect,collapsePaint);
			else{
				if (m_node.selected) 
					canvas.drawRect(rect,borderPaint);
			}
			break;
		case MPLAYOUT_SHAPE_OVAL:
			path.reset();
			if(m_node.m_background_image==null){
				canvas.drawOval(rect,backgroundPaint);

			}else{
				rectFRect = new Rect(0, 0, m_node.m_background_image.getWidth(), m_node.m_background_image.getHeight());
				canvas.drawBitmap(getOvalCroppedBitmap(m_node.m_background_image,m_node.m_background_image.getHeight(), m_node.m_background_image.getWidth()), rectFRect, rect, backgroundPaint);

			}
			if (m_node.isM_isCollapsed()) 
				canvas.drawOval(rect,collapsePaint);

			else{
				if (m_node.selected) 
					canvas.drawOval(rect,borderPaint);

			}
			break;

		case MPLAYOUT_SHAPE_COMMENT:

			path.reset();

			float x = rect.left;
			float y = rect.top;
			float	w = rect.right-rect.left;
			float h = (float) ((rect.bottom-rect.top)/1.2);
			float kappa = 0.6522848f;
			float ox = (w / 2) * kappa;//, // control point offset horizontal
			float oy = (h / 2) * kappa;//, // control point offset vertical
			float xe = rect.right;           // x-end
			float ye = (float) (rect.bottom/1.2);           // y-end
			float xm = rect.centerX();       // x-middle
			float ym = rect.centerY();
			float k = h/6;       // y-middle

			path.moveTo(x,ym);
			path.cubicTo(x, ym - oy, xm - ox, y, xm,y);
			path.cubicTo(xm + ox, y, xe, ym - oy, xe, ym);
			path.cubicTo(xe, ym + oy, xm + ox, ye, xm+k, ye);
			path.lineTo(x + w/2+k, y + h);
			path.lineTo(x + w/2-k/2, y + h+k);
			path.lineTo(x + w/2, y + h);
			path.cubicTo(xm - ox, ye, x, ym + oy, x, ym);

			if(m_node.m_background_image==null){


				canvas.drawPath(path, backgroundPaint);
			}
			else{

				canvas.clipPath(path);
				Bitmap sourceBitmap = m_node.m_background_image;
				canvas.drawBitmap(sourceBitmap, 
						new Rect(0, 0, sourceBitmap.getWidth(),
								sourceBitmap.getHeight()), 
								new Rect(0, 0, m_node.m_fWidth,
										m_node.m_fHeight), null);
			}

			if (m_node.isM_isCollapsed())
				canvas.drawPath(path,collapsePaint);
			else{
				if (m_node.selected) 
					canvas.drawPath(path,borderPaint);
			}
			break;
		case MPLAYOUT_SHAPE_FREEFORM:
			/*if(m_node.m_background_image!=null){
				calculateWidth();
				rect= rectfBounds();
			}*/
			path.reset();
			x = rect.left;
			y = rect.top;
			w = rect.right;
			h = rect.bottom;
			double cp =  0.5522848;
			ox = (float) ((w / 2) * cp); // control point offset horizontal
			oy = (float) ((h / 2) * cp); // control point offset vertical
			xe = x + w;           // x-end
			ye = y + h;           // y-end
			xm = x + w / 2;       // x-middle
			ym = y + h / 2;       // y-middle
			path.moveTo(x, y);
			path.cubicTo(x, y , xm, ym-oy, xe, y);
			path.cubicTo(xe, y, xm+ox, ym, xe, ye);
			path.cubicTo(xe, ye, xm, ym+oy, x, ye);
			path.cubicTo(x, ye, xm-ox, ym, x, y);

			if(m_node.m_background_image==null){

				canvas.drawPath(path, backgroundPaint);
			}else{

				canvas.clipPath(path);
				Bitmap sourceBitmap = m_node.m_background_image;
				canvas.drawBitmap(sourceBitmap, 
						new Rect(0, 0, sourceBitmap.getWidth(),
								sourceBitmap.getHeight()), 
								new Rect(0, 0, m_node.m_fWidth,
										m_node.m_fHeight), null);
			}

			if (m_node.isM_isCollapsed())
				canvas.drawPath(path,collapsePaint);
			else{
				if (m_node.selected) 
					canvas.drawPath(path,borderPaint);
			}
			break;
		case MPLAYOUT_SHAPE_OCTAGON:



			float width = rect.width();
			float height   = rect.height();
			x = rect.left;
			y = rect.top;
			double facShort = 0.2;
			double facLong = 1 - facShort;
			path.reset();
			path.moveTo(x, (float) (y + height*facShort));
			path.lineTo(x, (float) (y + height*facShort));
			path.lineTo(x, (float) (y + height*facLong));
			path.lineTo((float) (x + height*facShort), y + height);
			path.lineTo((float) (x + width-height*facShort), y + height);
			path.lineTo(x + width, (float) (y + height*facLong));
			path.lineTo(x + width, (float) (y + height*facShort));
			path.lineTo((float) (x + width-height*facShort), y);
			path.lineTo((float) (x + height*facShort), y);
			path.close();
			/*path.reset();
			path.moveTo((float) (x + width*facShort), y);
			path.lineTo((float) (x + height*facShort), y);
			path.lineTo(x, (float) (y + height*facShort));
			path.lineTo(x, (float) (y + height*facLong));
			path.lineTo((float) (x + height*facShort), y + height);
			path.lineTo((float) (x + width-height*facShort), y + height);
			path.lineTo(x + width, (float) (y + height*facLong));
			path.lineTo(x + width, (float) (y + height*facShort));
			path.lineTo((float) (x + width-height*facShort), y);
			path.lineTo((float) (x + width*facShort), y);*/
			// path.close();
			if(m_node.m_background_image==null){

				canvas.drawPath(path, backgroundPaint);
			}else{

				canvas.clipPath(path);
				Bitmap sourceBitmap = m_node.m_background_image;
				canvas.drawBitmap(sourceBitmap, 
						new Rect(0, 0, sourceBitmap.getWidth(),
								sourceBitmap.getHeight()), 
								new Rect(0, 0, m_node.m_fWidth,
										m_node.m_fHeight), null);
			}


			if (m_node.isM_isCollapsed())
				canvas.drawPath(path,collapsePaint);
			else{
				if (m_node.selected) 
					canvas.drawPath(path,borderPaint);
			}
			// float radius;

			break;
		case MPLAYOUT_SHAPE_RHOMBUS:



			path.reset();;
			path.moveTo(rect.width() / 2, 0);
			path.lineTo(0, rect.height() / 2);
			path.lineTo(rect.width() / 2, rect.height());
			path.lineTo(rect.width(), rect.height() / 2);
			path.lineTo(rect.width() / 2, 0);
			if(m_node.m_background_image==null){

				canvas.drawPath(path, backgroundPaint);
			}else{

				canvas.clipPath(path);
				Bitmap sourceBitmap = m_node.m_background_image;
				canvas.drawBitmap(sourceBitmap, 
						new Rect(0, 0, sourceBitmap.getWidth(),
								sourceBitmap.getHeight()), 
								new Rect(0, 0, m_node.m_fWidth,
										m_node.m_fHeight), null);
			}



			if (m_node.isM_isCollapsed())
				canvas.drawPath(path,collapsePaint);
			else{
				if (m_node.selected) 
					canvas.drawPath(path,borderPaint);
			}
			break;
		case MPLAYOUT_SHAPE_INVERSE_TRIANGLE:


			path.reset();
			path.moveTo(rect.left , rect.top);
			path.lineTo(rect.right, rect.top);
			path.lineTo(rect.right/2,rect.bottom);
			path.lineTo(rect.left, rect.top);
			if(m_node.m_background_image==null){

				canvas.drawPath(path, backgroundPaint);
			}else{

				canvas.clipPath(path);
				Bitmap sourceBitmap = m_node.m_background_image;
				canvas.drawBitmap(sourceBitmap, 
						new Rect(0, 0, sourceBitmap.getWidth(),
								sourceBitmap.getHeight()), 
								new Rect(0, 0, m_node.m_fWidth,
										m_node.m_fHeight), null);
			}

			if (m_node.isM_isCollapsed())
				canvas.drawPath(path,collapsePaint);
			else{
				if (m_node.selected) 
					canvas.drawPath(path,borderPaint);
			}
			break;
		default:
			break;

		}
		if(m_node.embededImage!=null){

			if(m_node.m_iMapLayoutShape== MapLayoutShapeType.MPLAYOUT_SHAPE_FREEFORM||
					m_node.m_iMapLayoutShape== MapLayoutShapeType.MPLAYOUT_SHAPE_OCTAGON){
				canvas.drawBitmap(imageAttachementBitmap,/*(int)(20+m_node.m_fWidth*0.10)*/(int)(m_node.m_fWidth-imageAttachementBitmap.getWidth())/2, 20+(int)(m_node.m_fHeight*0.10), backgroundPaint);
			}else if(m_node.m_iMapLayoutShape== MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON
					||m_node.m_iMapLayoutShape== MapLayoutShapeType.MPLAYOUT_SHAPE_RHOMBUS){
				canvas.drawBitmap(imageAttachementBitmap,(m_node.m_fWidth-imageAttachementBitmap.getWidth())/2,(float)( (imageAttachementBitmap.getWidth()/2)*1.15), backgroundPaint);
			}
			else if(m_node.m_iMapLayoutShape== MapLayoutShapeType.MPLAYOUT_SHAPE_COMMENT
					||m_node.m_iMapLayoutShape== MapLayoutShapeType.MPLAYOUT_SHAPE_OVAL||
					m_node.m_iMapLayoutShape== MapLayoutShapeType.MPLAYOUT_SHAPE_INVERSE_TRIANGLE){
				canvas.drawBitmap(imageAttachementBitmap,(int)(m_node.m_fWidth-imageAttachementBitmap.getWidth())/2, 
						(int)(m_node.m_fHeight-imageAttachementBitmap.getHeight())/2-AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,
								m_node.getM_iNode_TextSize())/1.5f, backgroundPaint);
			}
			else{
				canvas.drawBitmap(imageAttachementBitmap,(int)(m_node.m_fWidth-imageAttachementBitmap.getWidth())/2, 20, backgroundPaint);
			}

		}


		LINE1_VERT_OFFSET=calculateTextCenter();
		/*if(m_node.m_iMapLayoutShape==MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON){
			LINE1_VERT_OFFSET=LINE1_VERT_OFFSET+30;
		}*/


		PIC_VERTICAL_MARGIN=(int) (LINE1_VERT_OFFSET-AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,
				m_node.getM_iNode_TextSize()));
		if(m_node.embededImage!=null){
			info_posY=(int) (LINE1_VERT_OFFSET-AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,
					m_node.getM_iNode_TextSize())/1.8f);
		}else{
			info_posY=LINE1_VERT_OFFSET-30;
		}
		//	if(m_node.m_strNode_text.length()>0)
		Rect bounds1 = new Rect();
		textPaint.getTextBounds(m_node.m_strNode_text, 0, m_node.m_strNode_text.length(), bounds1);
		if (m_node.textAlign == 0)
			canvas.drawText(m_node.getM_strNode_text(), checkSmile(8),
					LINE1_VERT_OFFSET, textPaint);
		else if (m_node.textAlign == 1)
			canvas.drawText(m_node.getM_strNode_text(), checkSmile(8) - 10,
					LINE1_VERT_OFFSET, textPaint);
		else if (m_node.textAlign == 2)
			canvas.drawText(m_node.getM_strNode_text(), checkSmile(8) + 10,
					LINE1_VERT_OFFSET, textPaint);

		Rect bounds = new Rect();

		taskInfoPaint.getTextBounds("Start date:10/15/2015", 0, 1, bounds);
		if(!m_node.getM_start_date().equals(""))
			canvas.drawText("Start date: "+m_node.getM_start_date(),checkSmile(8)+bounds1.width()+10 ,info_posY, taskInfoPaint);
		//	float y =LINE1_VERT_OFFSET -textPaint.ascent() + textPaint.descent();
		if(!m_node.getM_end_date().equals(""))
			canvas.drawText("End date:"+m_node.getM_end_date(),checkSmile(8)+bounds1.width()+10,bounds.height()+info_posY+5, taskInfoPaint);
		//y += -textPaint.ascent() + textPaint.descent();
		if(!m_node.getM_resource().equals(""))
			canvas.drawText("Resourses:"+m_node.getM_resource(),checkSmile(8)+bounds1.width()+10,2*bounds.height()+info_posY+10 , taskInfoPaint);
		//y += -textPaint.ascent() + textPaint.descent();
		if(!getDurationType().equals(""))
			canvas.drawText("Duration:"+getDurationType(),checkSmile(8)+bounds1.width()+10,3*bounds.height()+info_posY+15, taskInfoPaint);

		if(m_node.dCharacter!=null)
			canvas.drawBitmap(m_node.dCharacter,checkSmile(-1), info_posY, null);
		if(m_node.m_smile_image!=null)
			canvas.drawBitmap(m_node.m_smile_image, checkSmile(0), info_posY, null);
		if(m_node.genCharacter!=null)
			canvas.drawBitmap(m_node.genCharacter, checkSmile(1), info_posY, null);
		if(m_node.priorityCharacter!=null)
			canvas.drawBitmap(m_node.priorityCharacter, checkSmile(2), info_posY, null);
		if(m_node.completionCharacter!=null)
			canvas.drawBitmap(m_node.completionCharacter, checkSmile(3), info_posY, null);
		if (m_node.m_attachement_image != null
				|| m_node.multimediaurl.length() > 0) {
			if (m_node.m_attachement_image != null)
				canvas.drawBitmap(imageLinkImg, checkSmile(6),info_posY, null);
			if (m_node.multimediaurl.length() > 0)
				canvas.drawBitmap(videoHyperLink, checkSmile(6),info_posY, null);
			inserpos("2", new RectF(checkSmile(6), info_posY,checkSmile(6) + PIC_SIZE, info_posY + PIC_SIZE));

		}
		else
		    m_node.propertiesPositions.remove("2");
		
		
		
		if(m_node.m_strHyperlinkUrl!=null&&m_node.m_strHyperlinkUrl.length()>0){
			canvas.drawBitmap(hyperLink, checkSmile(4), info_posY, null);
			inserpos("3", new RectF(checkSmile(4), info_posY, checkSmile(4)+PIC_SIZE, info_posY+PIC_SIZE));

		}
		else
		    m_node.propertiesPositions.remove("3");
		if(m_node.m_strNode_Notes.length()>0&&!m_node.m_strNode_Notes.equals("(null)")/*m_node.videourl!=null&&!m_node.videourl.equals("")&&m_node.videourl.length()>0*/){
			canvas.drawBitmap(notesImg, checkSmile(5), info_posY, null);
			inserpos("1", new RectF(checkSmile(5), info_posY, checkSmile(5)+PIC_SIZE, info_posY+PIC_SIZE));

		}
		else
		    m_node.propertiesPositions.remove("1");
		this.layout((int) m_node.m_fX, (int) m_node.m_fY, (int) m_node.m_fX+ m_node.getM_fWidth(),(int) m_node.m_fY + m_node.getM_fHeight());
		//	invalidate();

		//handleMovementAnimations(2000);
	}
	@Override
	protected void onSizeChanged (int w, int h, int oldw, int oldh)
	{
		this.layout((int) m_node.m_fX, (int) m_node.m_fY, (int) m_node.m_fX+ m_node.getM_fWidth(),(int) m_node.m_fY + m_node.getM_fHeight());
	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

	}
	
	public Bitmap scaleAttchament(Bitmap bitmap){
		bitmap = Bitmap.createScaledBitmap(bitmap, PIC_SIZE,
				PIC_SIZE, true);
		return bitmap;
	}
	public int getTextWidth(String text, Paint paint) {
		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length()-1, bounds);
		int width = bounds.left + bounds.width();
		return width;
	}
	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
				.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = pixels;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}
	public static Bitmap getOvalCroppedBitmap(Bitmap bitmap, int height,int width) {
		Bitmap finalBitmap;

		finalBitmap = Bitmap.createScaledBitmap(bitmap, width, height,
				false);

		Bitmap output = Bitmap.createBitmap(finalBitmap.getWidth(),
				finalBitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, finalBitmap.getWidth(),
				finalBitmap.getHeight());

		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		paint.setDither(true);
		//canvas.drawARGB(0, 0, 0, 0);
		//paint.setColor(Color.parseColor("#BAB399"));
		RectF oval = new RectF(0, 0, width, height);
		canvas.drawOval(oval, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(finalBitmap, rect, oval, paint);

		return output;
	}

	public String getDurationType(){
		String	durationType="";
		if(m_node.getM_duration_type()==""){
			m_node.setM_duration_type("Minutes(s)");
		}
		if(m_node.m_duration>0){
			durationType=m_node.m_duration+" "+m_node.m_duration_type;
		}
		return 	durationType;
	}

	int contentlength=0;

	public void calculateTextWidth() {
		backgroundPaint.setColor(m_node.getBackgroundColor());
		textPaint.setTextSize(AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,
				m_node.getM_iNode_TextSize()));
		textPaint.setTypeface(m_node.getM_str_Typeface());
		textPaint.setColor(Color.parseColor(m_node.getTextColor()));
		if(m_node.m_iGradientType>0)
			backgroundPaint.setShader(generateGradient(m_node.m_iGradientType));
		else
			backgroundPaint.setShader(null);
			if(m_node.m_iIsItalic.equalsIgnoreCase("1"))
			textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.ITALIC));
			if(m_node.m_iIsBold.equalsIgnoreCase("1"))
				textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
			if(m_node.m_iIsUnderline.equalsIgnoreCase("1"))
				textPaint.setUnderlineText(true);
		///textPaint.setTextAlign(Align.CENTER);
		/*
		if (m_node.textAlign == 0)
			startPosition = 0;
		else if (m_node.textAlign == 1)
			startPosition = 20;
		else if (m_node.textAlign == 2)
			startPosition = -20;*/

		m_node.m_fWidth = (int) (checkGen()+ textPaint.measureText(m_node.getM_strNode_text()));
		



		int textSize,info_height=0, maxheight = 0;
		if(m_node.getM_strNode_text().length()>0){
			textSize = AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,
					m_node.getM_iNode_TextSize());
		}else
			textSize=16;

		m_node.m_fHeight=textSize+70;


		if (!m_node.m_start_date.equals("")||!m_node.m_end_date.equals("")||!m_node.m_resource.equals("")||(m_node.m_duration>0 && m_node.m_duration_type!="") ) {
			m_node.m_fWidth +=calculateWidthWithTaskInfo();
			info_height=(AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,10)*4)+25;
			if(info_height> m_node.m_fHeight)
				m_node.m_fHeight=info_height;
		}
		if (m_node.m_fWidth > 200)
			m_node.m_fWidth += PADDING_RIGHT;

		contentlength=m_node.m_fWidth;
		if (m_node.m_fWidth < 200) 
			m_node.m_fWidth = 200;
		if(m_node.embededImage!=null)
			imageAttachementBitmap=m_node.embededImage;
		else if(m_node.m_background_image!=null)
			imageAttachementBitmap=m_node.m_background_image;
		else
			imageAttachementBitmap=null;

		if( imageAttachementBitmap!=null){
			if (m_node.m_fWidth <=imageAttachementBitmap.getWidth())
				m_node.m_fWidth = imageAttachementBitmap.getWidth() + 40;
			maxheight	=textSize>=(info_height-20)?textSize:info_height-20;
			m_node.m_fHeight = imageAttachementBitmap.getHeight() +maxheight+40;

		}





		if (m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON)
			if(m_node.m_fHeight>m_node.m_fWidth)
				m_node.m_fWidth=m_node.m_fHeight;
			else
				m_node.m_fHeight=m_node.m_fWidth;
			if (m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_FREEFORM)
				m_node.m_fWidth=m_node.m_fWidth+40;
		if(m_node.embededImage!=null){
			if (m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_FREEFORM
					|| m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_OCTAGON
					|| m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_INVERSE_TRIANGLE) {
				m_node.m_fHeight = (int) (m_node.m_fHeight + m_node.m_fHeight * 0.25);
				m_node.m_fWidth = (int) (m_node.m_fWidth + m_node.m_fWidth * 0.25);
			} else if (m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON) {
				m_node.m_fWidth = (int) (m_node.m_fHeight + m_node.m_fWidth * 0.45);
				m_node.m_fHeight = (int) (m_node.m_fHeight + m_node.m_fWidth * 0.45);
			} else if (m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_RHOMBUS) {
				m_node.m_fWidth = (int) (m_node.m_fHeight + m_node.m_fWidth * 0.6);
				m_node.m_fHeight = (int) (m_node.m_fHeight + m_node.m_fWidth * 0.6);
			} else if (m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_COMMENT)
			{
				m_node.m_fWidth = (int) (m_node.m_fHeight + m_node.m_fWidth * 0.45);
				m_node.m_fHeight = (int) (m_node.m_fHeight + m_node.m_fWidth * 0.45);

			}
		}
		if (m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_OVAL || m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_COMMENT){
			if(m_node.embededImage!=null){
				if(imageAttachementBitmap.getWidth()>imageAttachementBitmap.getHeight()){
					m_node.m_fWidth =(int) ((m_node.m_fWidth+m_node.m_fHeight)*0.73) ;
					m_node.m_fHeight =(int) (m_node.m_fHeight+imageAttachementBitmap.getHeight()/1.7f) ;
				}
				else{
					m_node.m_fHeight = (int) ((m_node.m_fWidth+m_node.m_fHeight)*0.73) ;
					m_node.m_fWidth =(int) (m_node.m_fWidth+imageAttachementBitmap.getWidth()/1.7f) ;
				}

			}else{
				m_node.m_fWidth = (int) (m_node.m_fWidth+50);
				m_node.m_fHeight = (int) (m_node.m_fHeight+30);
			}

		}


		this.rect = rectfBounds();
	}
	//calculate length of task info
	public int calculateWidthWithTaskInfo(){
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setTextSize(AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,
				10));
		Rect taskInfoBounds=new Rect();
		Rect textBounds = new Rect();
		int widthTemp=0,temp=0,textWidthTask=0;
		textPaint.getTextBounds(m_node.m_strNode_text, 0, m_node.m_strNode_text.length(), textBounds);
		String data="Start date:"+m_node.m_start_date;
		if(m_node.m_start_date.equals("") && m_node.m_duration_type!="" && m_node.m_duration!=0){
			data="Duration:"+m_node.m_duration+" "+m_node.m_duration_type;
		}
		taskInfoPaint.getTextBounds(data, 0, data.length(), taskInfoBounds);
		widthTemp=taskInfoBounds.width();
		data="Resourses:"+m_node.m_resource;
		paint.getTextBounds(data, 0, data.length(), taskInfoBounds);
		temp=taskInfoBounds.width();
		if(widthTemp>temp)
			textWidthTask=widthTemp;
		else
			textWidthTask=temp;
		 if(m_node.textAlign==3)
			textWidthTask= textWidthTask+5;

		return textWidthTask;
	}




	public void setShapeBackgroundColor(int backgroundColor)
	{

		this.backgroundColor = backgroundColor;
		if(m_node.m_arrChildren!=null)
			for(MPNode node:m_node.m_arrChildren){
				node.m_shape.setShapeBackgroundColor(backgroundColor);
			}

	}
	/*public void setNodeText(String strTxt) {

	}*/

	public RectF rectfBounds(){
		return new RectF(0, 0, m_node.getM_fWidth(), m_node.getM_fHeight());
	}
	/*public void highlight(boolean value)
	{
		this.m_node.isHilight = value;    

	}*/
	public int calculateTextCenter(){

		int textSize = AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,m_node.getM_iNode_TextSize());


		if(m_node.embededImage!=null){
			if(m_node.m_iMapLayoutShape == MapLayoutShapeType.MPLAYOUT_SHAPE_PENTAGON)
				return  (int) (0.57f*imageAttachementBitmap.getWidth()+imageAttachementBitmap.getHeight()+textSize);
			else
				return  (int) ((int) (rect.centerY()+imageAttachementBitmap.getHeight()/2)+textSize/2f);
		}
		else
			return (int) (rect.centerY()+textSize/2);

	}

	

	public void inserpos(String index,RectF point){
		if(!m_node.propertiesPositions.containsKey(index)) {
			m_node.propertiesPositions.put(index,point);
		}
		else {
			m_node.propertiesPositions.get(index).set(point);
		}
	}

	public int checkSmile(int p0s) {

		//Log.d("","textWithTaskInfo"+ textWithTaskInfo);
		int x = 15;
		//int calWidth = calculateContentWidth();
		if(m_node.m_fWidth>=contentlength)
			x=((m_node.m_fWidth-contentlength)/2)+x;
		if(p0s==-1)
			return x;
		if (m_node.dCharacter != null) {
			x = x + 45;
			if (p0s == 0)
				return x;
		}
		if (m_node.m_smile_image != null && p0s != 0) {
			x = x + 45;
			if (p0s == 1)
				return x;

		}
		if (m_node.genCharacter != null && p0s > 1) {
			x = x + 45;
			if (p0s == 2)
				return x;
		}
		if (m_node.priorityCharacter != null && p0s > 2) {
			x = x + 45;
			if (p0s == 3)
				return x;
		}
		if (m_node.completionCharacter != null && p0s > 3) {
			x = x + 45;
			if (p0s == 4)
				return x;
		}
		if (m_node.m_strHyperlinkUrl != null && m_node.m_strHyperlinkUrl.length() > 0&& p0s > 4) {
			x = x + 45;
			if (p0s == 5)
				return x;
		}
		if (m_node.m_strNode_Notes.length() > 0 && p0s > 5) {

			x = x + 45;
			if (p0s == 6)
				return x;
		}


		if ((m_node.m_attachement_image != null|| m_node.multimediaurl.length()>0) &&p0s > 6) {
			x = x + 45;
			if (p0s == 7)
				return x;
		}

		return x;
	}

		public int checkGen(){
		 int p0s=8;
		 int x = 20;
		 if (m_node.dCharacter != null) {
			 x = x + 45;
			 if (p0s == 0)
				 return x;
		 }
		 if (m_node.m_smile_image != null && p0s != 0) {
			 x = x + 45;
			 if (p0s == 1)
				 return x;

		 }
		 if (m_node.genCharacter != null && p0s > 1) {
			 x = x + 45;
			 if (p0s == 2)
				 return x;
		 }
		 if (m_node.priorityCharacter != null && p0s > 2) {
			 x = x + 45;
			 if (p0s == 3)
				 return x;
		 }
		 if (m_node.completionCharacter != null && p0s > 3) {
			 x = x + 45;
			 if (p0s == 4)
				 return x;
		 }
		 if (m_node.m_strHyperlinkUrl != null && m_node.m_strHyperlinkUrl.length() > 0&& p0s > 4) {
			 x = x + 45;
			 if (p0s == 5)
				 return x;
		 }
		 if (m_node.m_strNode_Notes.length() > 0 && p0s > 5) {

			 x = x + 45;
			 if (p0s == 6)
				 return x;
		 }


		 if ((m_node.m_attachement_image != null|| m_node.multimediaurl.length()>0) &&p0s > 6) {
			 x = x + 45;
			 if (p0s == 7)
				 return x;
		 }

		 return x;
	 }
	 public LinearGradient generateGradient(int pos)
		{
			int color =(int) (pos>8 ? Color.BLACK: Color.WHITE ); 
			LinearGradient gradient=null;
				if(pos>8)
					pos=pos-8;
				switch (pos) 
				{
					
					case 1://bottom
						gradient = new LinearGradient(m_node.m_fWidth/2, m_node.m_fHeight, m_node.m_fWidth/2,m_node.m_fHeight/2, color,m_node.getBackgroundColor(), TileMode.CLAMP);
						break;
						
					case 2://top
						gradient = new LinearGradient(m_node.m_fWidth/2, m_node.m_fHeight/2,  m_node.m_fWidth/2,m_node.m_fHeight, color,m_node.getBackgroundColor(), TileMode.CLAMP);
						break;
						
					case 3://left
						gradient = new LinearGradient(0, m_node.m_fHeight/2, m_node.m_fWidth,m_node.m_fHeight,color,m_node.getBackgroundColor(), TileMode.CLAMP);
						break;
						
					case 4://right
						gradient = new LinearGradient(m_node.m_fWidth, m_node.m_fHeight/2,  m_node.m_fWidth/2,m_node.m_fHeight/2, color,m_node.getBackgroundColor(), TileMode.CLAMP);
						break;
						
					case 5://bottom right
						gradient = new LinearGradient(m_node.m_fWidth, m_node.m_fHeight, m_node.m_fWidth/2,0, color,m_node.getBackgroundColor(), TileMode.CLAMP);
						break;
						
					case 6://top right
						gradient = new LinearGradient(m_node.m_fWidth, m_node.m_fHeight, 0,m_node.m_fHeight/2, color,m_node.getBackgroundColor(), TileMode.CLAMP);
						break;
						
					case 7://bottom left
						gradient = new LinearGradient(0, m_node.m_fHeight, m_node.m_fWidth/2,0, color,m_node.getBackgroundColor(), TileMode.CLAMP);
						break;
						
					case 8://top left
						gradient = new LinearGradient(0, 0, m_node.m_fWidth/2,m_node.m_fHeight,color,m_node.getBackgroundColor(), TileMode.CLAMP);
						break;

					
					
					default:
						break;
				}

			return  gradient;
		}
}
