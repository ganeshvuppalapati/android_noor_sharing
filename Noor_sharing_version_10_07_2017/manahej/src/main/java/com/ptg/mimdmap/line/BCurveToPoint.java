package com.ptg.mimdmap.line;

public class BCurveToPoint {
     public BCurveToPoint(float m_fArg1, float m_fArg2, float m_fArg3,
			float m_fArg4, float m_fArg5, float m_fArg6) {
		super();
		this.m_fArg1 = m_fArg1;
		this.m_fArg2 = m_fArg2;
		this.m_fArg3 = m_fArg3;
		this.m_fArg4 = m_fArg4;
		this.m_fArg5 = m_fArg5;
		this.m_fArg6 = m_fArg6;
	}
	public float m_fArg1 ;
    public float m_fArg2 ;
    public float m_fArg3 ;
    public float m_fArg4 ;
    public float m_fArg5 ;
    public float m_fArg6 ;
    
    
}
