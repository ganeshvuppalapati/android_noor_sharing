package com.ptg.mimdmap.line;

public class MoveToPoint {
    public MoveToPoint(float m_fArg1, float m_fArg2) {
		super();
		this.m_fArg1 = m_fArg1;
		this.m_fArg2 = m_fArg2;
	}
    public float m_fArg1 ;
	public float m_fArg2;
}
