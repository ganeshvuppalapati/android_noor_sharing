package com.ptg.mimdmap.node;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.view.View;

public class ShadowLayer extends View {

	 public MPNodeView nodeView;
	 Path path;
	 public MPNodeView getNodeView() {
		return nodeView;
	}
	public void setNodeView(MPNodeView nodeView) {
		this.nodeView = nodeView;
	}
	public RectF getRect() {
		return rect;
	}
	public void setRect(RectF rect) {
		this.rect = rect;
	}
	public Paint getPaint() {
		return paint;
	}
	public void setPaint(Paint paint) {
		this.paint = paint;
	}
	RectF rect;
	 Paint paint;
	public ShadowLayer(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	public ShadowLayer(Context context,MPNodeView nodeView) {
		super(context);
		this.nodeView=nodeView;
		paint=new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setColor(Color.parseColor("#9a9cce"));
		paint.setStyle(Paint.Style.FILL);
		 //paint.setShadowLayer(12, 0, 0, Color.BLUE);

	       // setLayerType(LAYER_TYPE_SOFTWARE, paint);
		path=new Path();
	}
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		rect=new RectF(0, 0, nodeView.m_node.getM_fWidth()+10, nodeView.m_node.getM_fHeight()+10);
		switch (nodeView.m_node.m_iMapLayoutShape){
		case MPLAYOUT_SHAPE_ROUNDEDRECTANGLE:
				canvas.drawRoundRect(rect, 10, 10,paint);
					
			 break;
		 case MPLAYOUT_SHAPE_PENTAGON:
			 path.reset();
			 
			 float minX = rect.left;
	         float minY = rect.top;
	         float midX = rect.centerX();
	         float midY = rect.centerY();
	         float maxX = rect.right;
	         float maxY = rect.bottom;
	        
	           path.moveTo(midX,minY);
           path.lineTo(maxX,midY);
           path.lineTo(maxX-maxX/4,maxY);
           path.lineTo(minX+maxX/4,maxY);
           path.lineTo(minX,midY);
           path.lineTo(midX,minY);
           canvas.drawPath(path, paint);
           
			 break;
		 case MPLAYOUT_SHAPE_RECTANGLE:
			canvas.drawRect(rect, paint);
			
			 break;
		 case MPLAYOUT_SHAPE_OVAL:
				canvas.drawOval(rect, paint);
				
			
			 break;
		 case MPLAYOUT_SHAPE_COMMENT:
			 path.reset();
	            float x = rect.left;
	            float y = rect.top;
	            float w = rect.right-rect.left;
	            float h = rect.bottom-rect.top;//*1.1;
	            float kappa = 0.6522848f;
	            float ox = (w / 2) * kappa;//, // control point offset horizontal
	            float oy = (h / 2) * kappa;//, // control point offset vertical
	            float xe = rect.right;           // x-end
	            float ye = rect.bottom;           // y-end
	            float xm = rect.centerX();       // x-middle
	            float ym = rect.centerY();
	            float k = h/6;       // y-middle
	            
	           path.moveTo(x,ym);
	           path.cubicTo(x, ym - oy, xm - ox, y, xm,y);
	           path.cubicTo(xm + ox, y, xe, ym - oy, xe, ym);
	           path.cubicTo(xe, ym + oy, xm + ox, ye, xm+k, ye);
	           path.moveTo(x + w/2+k, y + h);
	           path.moveTo(x + w/2-k/2, y + h+k);
	           path.moveTo(x + w/2, y + h);
	            path.cubicTo(xm - ox, ye, x, ym + oy, x, ym);
	           canvas.drawPath(path, paint);
	          
			 break;
		 case MPLAYOUT_SHAPE_FREEFORM:
			 
			  minX = rect.left;
	          minY = rect.top;
	          midX = rect.centerX();
	          midY = rect.centerY();
	          maxX = rect.right;
	          maxY = rect.bottom;
	            
	          path.reset();
	          path.moveTo(minX,minY);
	          path.cubicTo(minX,minY,midX, minY+12, maxX,minY);//1
	          path.cubicTo(maxX,minY,maxX-12, midY, maxX,maxY);//2
	          path.cubicTo(maxX,maxY,midX, midY,minX,maxY);//3
	          path.cubicTo(minX,maxY,minX+12, midY, minX,minY);
	          canvas.drawPath(path, paint);
	         
			 break;
		}
		this.layout((int) nodeView.m_node.m_fX-4, (int) nodeView.m_node.m_fY-4, (int) nodeView.m_node.m_fX
				+ (nodeView.m_node.getM_fWidth()+10),(int) nodeView.m_node.m_fY + (nodeView.m_node.getM_fHeight()+10));
	invalidate();
	}

}
