package com.ptg.mimdmap.line;

import com.ptg.mimdmap.node.MPNode;


public class MyPoint {
	 public MPNode tagid ;
	 public  float m_fArg1 ;
	 public float m_fArg2 ;
	public MPNode getTagid() {
		return tagid;
	}
	public void setTagid(MPNode tagid) {
		this.tagid = tagid;
	}
	public float getM_fArg1() {
		return m_fArg1;
	}
	public void setM_fArg1(float m_fArg1) {
		this.m_fArg1 = m_fArg1;
	}
	public float getM_fArg2() {
		return m_fArg2;
	}
	public void setM_fArg2(float m_fArg2) {
		this.m_fArg2 = m_fArg2;
	}
    
    
}
