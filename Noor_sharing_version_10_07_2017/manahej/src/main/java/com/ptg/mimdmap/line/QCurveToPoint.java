package com.ptg.mimdmap.line;

public class QCurveToPoint {
	
	public QCurveToPoint(float m_fArg1, float m_fArg2, float m_fArg3,
			float m_fArg4) {
		super();
		this.m_fArg1 = m_fArg1;
		this.m_fArg2 = m_fArg2;
		this.m_fArg3 = m_fArg3;
		this.m_fArg4 = m_fArg4;
	}
	public float m_fArg1;
	public float m_fArg2;
	public float m_fArg3;
	public float m_fArg4;

}
