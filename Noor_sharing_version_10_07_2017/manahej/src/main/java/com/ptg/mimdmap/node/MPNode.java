package com.ptg.mimdmap.node;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.ptg.mimdmap.line.Line;
import com.ptg.mimdmap.line.MPLinePoint;
import com.ptg.mindmap.enums.MapLayoutShapeType;
import com.ptg.mindmap.widget.AppDict;

import java.util.ArrayList;
import java.util.HashMap;

public class MPNode {
	public enum MPNodeState {
		MPNodeStateNormal, MPNodeStateCut, MPNodeStateCopy, MPNodeStatePast, MPNodeStateCopyPro, MPNodeStatePastrPro
	}
	public boolean istaskinfo=false;
	public  int smileIconPosition = -1;
	public int m_iId, m_iMapId, m_iParentId, m_iNode_Order;
	public MPNode m_parentNode;
	public ArrayList<MPNode> m_arrChildren = new ArrayList<MPNode>();
	public boolean m_isAboveCenter;
	public int m_iNode_TextSize = 12;
	public String m_strFontName;
	public String m_strNode_Notes = "";
	public String m_strNode_text = "topic";
	public boolean m_isCollapsed;
	public int m_childrenHeight;
	public int m_fWidth = AppDict.mDrawBitmapWidth;
	public int m_fHeight = AppDict.mDrawBitmapHeight;
	public boolean isHilight;
	public  int bitmapWidth=200;
	public  int bitmapHeight=200;
	public String hexaColor;
	public int textCenter;
	 public int m_iGradientType=0; 
	public Typeface m_str_Typeface;
	public boolean m_isRightnode=false;
	public int m_iLevelId;
	public MPLinePoint m_lp;
	public MPNodeView m_shape;
	public int arPageOrder, pageLayoutPos, selRootPage;
	public String m_strLocation;
	public ArrayList<MPNode> m_leftnodes;
	public ArrayList<MPNode> m_rightnodes;
	public boolean m_isCurrShapeBeingDragged;
	public boolean m_isChild;
	public Bitmap m_smile_image;
	public Bitmap m_attachement_image;// attachment
	public Bitmap m_background_image;// notes
	//public String m_strAttachmentImage;
	public String base64image="";
	public String m_strBgImage;
	public int backgroundColor;
	public int borderColor;
	public String textColor;
	public Line drawingLine;
	public String iconDrawable;
	public int shape;
	public boolean showIcon;
	public float m_fX;
	public float m_fY;
	public boolean selected = false;
	public MapLayoutShapeType m_iMapLayoutShape;
	public int textAlign=0;
	RectF rect;
	public String imageUrl="";
	public String m_strMultimediaType;
	public String m_strMultimediaUrl = "";
	public String m_strHyperlinkUrl="";
	public String m_strHyperlinkDescription;
	public String m_iIsBold="";
	public String m_iIsItalic="";
	public String m_iIsUnderline="";
	public Bitmap embededImage;
	public int mediaType=-1;
	public String multimediaurl="";
	public int dCharacterPosition;
	public Bitmap dCharacter;
	public int genCharacterPosition;
	public Bitmap genCharacter;
	public int priorityCharacterPosition;
	public Bitmap priorityCharacter;
	public int completionCharacterPosition;
	public Bitmap completionCharacter;
	public int imageAttachementype=-1;
	public String m_start_date="";
	public String m_end_date="";
	public String m_resource="";
	public int m_duration=0;
	public String m_duration_type="";
	
	public String getM_duration_type() {
		return m_duration_type;
	}

	public void setM_duration_type(String m_duration_type) {
		this.m_duration_type = m_duration_type;
	}

	public String getM_start_date() {
		return m_start_date;
	}

	public void setM_start_date(String m_start_date) {
		this.m_start_date = m_start_date;
	}

	public String getM_end_date() {
		return m_end_date;
	}

	public void setM_end_date(String m_end_date) {
		this.m_end_date = m_end_date;
	}

	public String getM_resource() {
		return m_resource;
	}

	public void setM_resource(String m_resource) {
		this.m_resource = m_resource;
	}

	public int getM_duration() {
		return m_duration;
	}

	public void setM_duration(int m_duration) {
		this.m_duration = m_duration;
	}

	public Bitmap getM_smile_image() {
		return m_smile_image;
	}

	public void setM_smile_image(Bitmap m_smile_image) {
		this.m_smile_image = m_smile_image;
	}

	public Bitmap getM_attachement_image() {
		return m_attachement_image;
	}

	public void setM_attachement_image(Bitmap m_attachement_image) {
		this.m_attachement_image = m_attachement_image;
	}

	

	
	public Bitmap getEmbededImage() {
		return embededImage;
	}

	public void setEmbededImage(Bitmap embededImage) {
		this.embededImage = embededImage;
	}

	public int getdCharacterPosition() {
		return dCharacterPosition;
	}

	public void setdCharacterPosition(int dCharacterPosition) {
		this.dCharacterPosition = dCharacterPosition;
	}

	public Bitmap getdCharacter() {
		return dCharacter;
	}

	public void setdCharacter(Bitmap dCharacter) {
		this.dCharacter = dCharacter;
	}

	public int getGenCharacterPosition() {
		return genCharacterPosition;
	}

	public void setGenCharacterPosition(int genCharacterPosition) {
		this.genCharacterPosition = genCharacterPosition;
	}

	public Bitmap getGenCharacter() {
		return genCharacter;
	}

	public void setGenCharacter(Bitmap genCharacter) {
		this.genCharacter = genCharacter;
	}

	public int getPriorityCharacterPosition() {
		return priorityCharacterPosition;
	}

	public void setPriorityCharacterPosition(int priorityCharacterPosition) {
		this.priorityCharacterPosition = priorityCharacterPosition;
	}

	public Bitmap getPriorityCharacter() {
		return priorityCharacter;
	}

	public void setPriorityCharacter(Bitmap priorityCharacter) {
		this.priorityCharacter = priorityCharacter;
	}

	public int getCompletionCharacterPosition() {
		return completionCharacterPosition;
	}

	public void setCompletionCharacterPosition(int completionCharacterPosition) {
		this.completionCharacterPosition = completionCharacterPosition;
	}

	public Bitmap getCompletionCharacter() {
		return completionCharacter;
	}

	public void setCompletionCharacter(Bitmap completionCharacter) {
		this.completionCharacter = completionCharacter;
	}


	public HashMap<String, RectF> propertiesPositions = new HashMap<String, RectF>();

	public HashMap<String, RectF> getPropertiesPositions() {
		return propertiesPositions;
	}

	public void setPropertiesPositions(
			HashMap<String, RectF> propertiesPositions) {
		this.propertiesPositions = propertiesPositions;
	}

	public String getVideourl() {
		return multimediaurl;
	}

	public void setVideourl(String videourl) {
		this.multimediaurl = videourl;
	}

	public String getM_iIsBold() {
		return m_iIsBold;
	}

	public void setM_iIsBold(String m_iIsBold) {
		this.m_iIsBold = m_iIsBold;
	}

	public String getM_iIsItalic() {
		return m_iIsItalic;
	}

	public void setM_iIsItalic(String m_iIsItalic) {
		this.m_iIsItalic = m_iIsItalic;
	}

	public String getM_iIsUnderline() {
		return m_iIsUnderline;
	}

	public void setM_iIsUnderline(String m_iIsUnderline) {
		this.m_iIsUnderline = m_iIsUnderline;
	}

	public String getM_strHyperlinkDescription() {
		return m_strHyperlinkDescription;
	}

	public void setM_strHyperlinkDescription(String m_strHyperlinkDescription) {
		this.m_strHyperlinkDescription = m_strHyperlinkDescription;
	}

	public String getM_strHyperlinkUrl() {
		return m_strHyperlinkUrl;
	}

	public void setM_strHyperlinkUrl(String m_strHyperlinkUrl) {
		this.m_strHyperlinkUrl = m_strHyperlinkUrl;
	}

	public String getM_strMultimediaUrl() {
		return m_strMultimediaUrl;
	}

	public void setM_strMultimediaUrl(String m_strMultimediaUrl) {
		this.m_strMultimediaUrl = m_strMultimediaUrl;
	}

	public String getM_strMultimediaType() {
		return m_strMultimediaType;
	}

	public void setM_strMultimediaType(String m_strMultimediaType) {
		this.m_strMultimediaType = m_strMultimediaType;
	}

	public boolean isHilight() {
		return isHilight;
	}

	public void setHilight(boolean isHilight) {
		this.isHilight = isHilight;
	}

	public int m_selectedColor = Color.parseColor("#e3e3e3");

	public RectF getRect() {
		return rect;
	}

	public void setRect(RectF rect) {
		this.rect = rect;
	}

	public MPNode() {
		init();
	}

	public void init() {
		m_strNode_Notes = "";
		this.m_arrChildren = null;
		// arrGLPoints = [[NSMutableArray alloc] init];
		this.m_leftnodes = new ArrayList<MPNode>();
		this.m_rightnodes = new ArrayList<MPNode>();
		this.m_iNode_TextSize = 12;
		// this.m_selectedColor = Color.parseColor("#e3e3e3");
		this.m_isCollapsed = false;
		this.m_isChild = false;
		this.textColor = "#000000";
		this.m_iMapLayoutShape = MapLayoutShapeType.MPLAYOUT_SHAPE_ROUNDEDRECTANGLE;
		// m_strFontName = @"Arial";
		//this.m_strAttachmentImage = null;
		this.m_strNode_Notes = null;
		this.m_strBgImage = null;
		setBackgroundColor(Color.parseColor("#e3e3e3"));

	}

	public String getTextColor() {
		return textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}

	public float getM_fX() {
		return m_fX;
	}

	public void setM_fX(float x_Position) {
		this.m_fX = x_Position;
	}

	public float getM_fY() {
		return m_fY;
	}

	public void setM_fY(float y_Position) {
		this.m_fY = y_Position;
	}

	public int getM_iId() {
		return m_iId;
	}

	public void setM_iId(int m_iId) {
		this.m_iId = m_iId;
	}

	public int getM_iMapId() {
		return m_iMapId;
	}

	public void setM_iMapId(int m_iMapId) {
		this.m_iMapId = m_iMapId;
	}

	public int getM_iParentId() {
		return m_iParentId;
	}

	public void setM_iParentId(int m_iParentId) {
		this.m_iParentId = m_iParentId;
	}

	public int getM_iNode_Order() {
		return m_iNode_Order;
	}

	public void setM_iNode_Order(int m_iNode_Order) {
		this.m_iNode_Order = m_iNode_Order;
	}

	public MPNode getM_parentNode() {
		return m_parentNode;
	}

	public void setM_parentNode(MPNode clickedMPNode) {
		this.m_parentNode = clickedMPNode;
	}

	public ArrayList<MPNode> getM_arrChildren() {
		return m_arrChildren;
	}

	public void setM_arrChildren(ArrayList<MPNode> m_arrChildren) {
		this.m_arrChildren = m_arrChildren;
	}

	public boolean isM_isAboveCenter() {
		return m_isAboveCenter;
	}

	public void setM_isAboveCenter(boolean m_isAboveCenter) {
		this.m_isAboveCenter = m_isAboveCenter;
	}

	public int getM_iNode_TextSize() {
		return m_iNode_TextSize;
	}

	public void setM_iNode_TextSize(int m_iNode_TextSize) {
		this.m_iNode_TextSize = m_iNode_TextSize;
	}

	public String getM_strFontName() {
		return m_strFontName;
	}

	public void setM_strFontName(String m_strFontName) {
		this.m_strFontName = m_strFontName;
	}

	public String getM_strNode_Notes() {
		return m_strNode_Notes;
	}

	public void setM_strNode_Notes(String m_strNode_Notes) {
		this.m_strNode_Notes = m_strNode_Notes;
	}

	public String getM_strNode_text() {
		return m_strNode_text;
	}

	public void setM_strNode_text(String m_strNode_text) {
		this.m_strNode_text = m_strNode_text;
	}

	public boolean isM_isCollapsed() {
		return m_isCollapsed;
	}

	public void setM_isCollapsed(boolean m_isCollapsed) {
		this.m_isCollapsed = m_isCollapsed;
	}

	public int getM_childrenHeight() {
		return m_childrenHeight;
	}

	public void setM_childrenHeight(int m_childrenHeight) {
		this.m_childrenHeight = m_childrenHeight;
	}

	public int getM_fWidth() {
		return m_fWidth;
	}

	public void setM_fWidth(int m_fWidth) {
		this.m_fWidth = m_fWidth;
	}

	public int getM_fHeight() {
		return m_fHeight;
	}

	public void setM_fHeight(int m_fHeight) {
		this.m_fHeight = m_fHeight;
	}

	public boolean isM_isRightnode() {
		return m_isRightnode;
	}

	public void setM_isRightnode(boolean m_isRightnode) {
		this.m_isRightnode = m_isRightnode;
	}

	public int getM_iLevelId() {
		return m_iLevelId;
	}

	public void setM_iLevelId(int m_iLevelId) {
		this.m_iLevelId = m_iLevelId;
	}

	public int getArPageOrder() {
		return arPageOrder;
	}

	public void setArPageOrder(int arPageOrder) {
		this.arPageOrder = arPageOrder;
	}

	public int getPageLayoutPos() {
		return pageLayoutPos;
	}

	public void setPageLayoutPos(int pageLayoutPos) {
		this.pageLayoutPos = pageLayoutPos;
	}

	public int getSelRootPage() {
		return selRootPage;
	}

	public void setSelRootPage(int selRootPage) {
		this.selRootPage = selRootPage;
	}

	public String getM_strLocation() {
		return m_strLocation;
	}

	public void setM_strLocation(String m_strLocation) {
		this.m_strLocation = m_strLocation;
	}

	public ArrayList<MPNode> getM_leftnodes() {
		return m_leftnodes;
	}

	public void setM_leftnodes(ArrayList<MPNode> m_leftnodes) {
		this.m_leftnodes = m_leftnodes;
	}

	public ArrayList<MPNode> getM_rightnodes() {
		return m_rightnodes;
	}

	public void setM_rightnodes(ArrayList<MPNode> m_rightnodes) {
		this.m_rightnodes = m_rightnodes;
	}

	public boolean isM_isCurrShapeBeingDragged() {
		return m_isCurrShapeBeingDragged;
	}

	public void setM_isCurrShapeBeingDragged(boolean m_isCurrShapeBeingDragged) {
		this.m_isCurrShapeBeingDragged = m_isCurrShapeBeingDragged;
	}

	public boolean isM_isChild() {
		return m_isChild;
	}

	public void setM_isChild(boolean m_isChild) {
		this.m_isChild = m_isChild;
	}

	/*public Bitmap getM_img1() {
		return m_smile_image;
	}

	public void setM_img1(Bitmap m_img1) {
		this.m_smile_image = m_img1;
	}*/

	public Bitmap getM_Attachement_Image() {
		return m_attachement_image;
	}

	public void setM_Attachement_Image(Bitmap m_img2) {
		this.m_attachement_image = m_img2;
	}

	public Bitmap getM_background_image() {
		return m_background_image;
	}

	public void setM_background_image(Bitmap m_img3) {
		this.m_background_image = m_img3;
	}

	/*public String getM_strAttachmentImage() {
		return m_strAttachmentImage;
	}

	public void setM_strAttachmentImage(String m_strAttachmentImage) {
		this.m_strAttachmentImage = m_strAttachmentImage;
	}
*/
	public String getM_strBgImage() {
		return m_strBgImage;
	}

	public void setM_strBgImage(String m_strBgImage) {
		this.m_strBgImage = m_strBgImage;
	}

	public int getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public int getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(int borderColor) {
		this.borderColor = borderColor;
	}

	public String getIconDrawable() {
		return iconDrawable;
	}

	public void setIconDrawable(String iconDrawable) {
		this.iconDrawable = iconDrawable;
	}

	public int getShape() {
		return shape;
	}

	public void setShape(int shape) {
		this.shape = shape;
	}

	public boolean isShowIcon() {
		return showIcon;
	}

	public void setShowIcon(boolean showIcon) {
		this.showIcon = showIcon;
	}

	public MPNodeView getM_shape() {
		return m_shape;
	}

	public void setM_shape(MPNodeView m_shape) {
		this.m_shape = m_shape;
	}

	public MapLayoutShapeType getM_iMapLayoutShape() {
		return m_iMapLayoutShape;
	}

	public void setM_iMapLayoutShape(MapLayoutShapeType m_iMapLayoutShape) {
		this.m_iMapLayoutShape = m_iMapLayoutShape;
	}

	public MPLinePoint getM_lp() {
		return m_lp;
	}

	public void setM_lp(MPLinePoint m_lp) {
		this.m_lp = m_lp;
	}

	public  int getSmileIconPosition() {
		return smileIconPosition;
	}

	public void setSmileIconPosition(int smileIcon) {
		this.smileIconPosition = smileIcon;
	}

	public RectF getBounds() {
		if (this.isM_isCollapsed())
			return new RectF(0, 0, 0, 0);
		else {
			return new RectF(0, 0, /* this.m_fX+ */this.getM_fWidth(),
			/* this.m_fY+ */this.getM_fHeight());
		}

		// return new RectF(this.m_fX, this.m_fY,this.m_fWidth, this.m_fHeight);
	}

	public RectF getBasicPositionBounds() {
		/*
		 * return new RectF(0,0, this.m_fX+this.getM_fWidth(),
		 * this.m_fY+this.getM_fHeight());
		 */

		return new RectF(this.m_fX, this.m_fY, this.m_fX + this.getM_fWidth(),
				this.m_fY + this.getM_fHeight());
	}

	public Typeface getM_str_Typeface() {
		return m_str_Typeface;
	}

	public void setM_str_Typeface(Typeface m_str_Typeface) {
		this.m_str_Typeface = m_str_Typeface;
	}

	public int getM_selectedColor() {
		return m_selectedColor;
	}

	public void setM_selectedColor(int m_selectedColor) {
		this.m_selectedColor = m_selectedColor;
	}

	public void deleteNode() {
		if (m_iLevelId != 0/* m_parentNode != null */) {
			deleteAllChildren(this.m_arrChildren);
			ViewParent parent = this.m_shape.getParent();
			this.m_lp = null;
			((ViewGroup) parent).removeView(this.m_shape.lineView);
			((ViewGroup) parent).removeView(this.m_shape);
			if (this.m_parentNode != null)
				this.m_parentNode.m_arrChildren.remove(this);
			// this.m_shape=null;

		}

	}

	public void deleteAllChildren(ArrayList<MPNode> arrChildNodes) {
		if (arrChildNodes != null && arrChildNodes.size() > 0)
			for (MPNode childNode : arrChildNodes) {

				deleteAllChildren(childNode.m_arrChildren);
				// [((MPShapeView *)childNode.m_shape) removeFromSuperview];
				ViewParent parent = childNode.m_shape.getParent();
				// ((ViewGroup) parent).removeView(childNode.m_shape.lineView);
				if (childNode.m_shape != null)
					((ViewGroup) parent).removeView(childNode.m_shape);

			}

	}

	ArrayList<MPNode> xmlSection = new ArrayList<MPNode>();

	public ArrayList<MPNode> getXmlSection() {
		return xmlSection;
	}

	public void setXmlSection(ArrayList<MPNode> xmlSection) {
		this.xmlSection = xmlSection;
	}

	public void addSection(MPNode currentMPNode) {
		xmlSection.add(currentMPNode);

	}

	public String getHexaColor() {
		return hexaColor;
	}

	public void setHexaColor(String hexaColor) {
		this.hexaColor = hexaColor;
	}

	public MPNode copy() {
		MPNode newNode = new MPNode();
		newNode.m_fWidth = this.m_fWidth;
		newNode.m_fHeight = this.m_fHeight;
		newNode.m_fX = this.m_fX;
		newNode.m_fY = this.m_fY;
		newNode.textColor = this.textColor;
		newNode.m_childrenHeight = this.m_childrenHeight;
		newNode.m_iLevelId = this.m_iLevelId == 0 ? 1 : this.m_iLevelId;
		newNode.m_iNode_TextSize = this.m_iNode_TextSize;
		newNode.m_strLocation = this.m_strLocation;
		newNode.m_strNode_Notes = this.m_strNode_Notes;
		newNode.m_strNode_text = this.m_strNode_text;
		newNode.m_isAboveCenter = this.m_isAboveCenter;
		// newNode.m_isChild = self.m_isChild;
		newNode.m_isCollapsed = this.m_isCollapsed;
		// newNode.m_isCurrShapeBeingDragged =
		// newNode.m_isCurrShapeBeingDragged;
		newNode.m_isRightnode = this.m_isRightnode;
		newNode.m_parentNode = this.m_parentNode;
		newNode.hexaColor = this.hexaColor;
		newNode.m_selectedColor = this.m_selectedColor;
		newNode.m_arrChildren = childrenCopy();
		newNode.m_shape = this.m_shape;
		// newNode.m_leftnodes = self.m_leftnodes;
		// newNode.m_lp = self.m_lp;
		// newNode.m_rightnodes = self.m_rightnodes;
		// newNode.m_shape = [self.m_shape copy];
		if (m_str_Typeface != null)
			newNode.m_str_Typeface = this.m_str_Typeface;
		newNode.m_smile_image = this.m_smile_image;
		newNode.embededImage = this.embededImage;
		newNode.m_attachement_image = this.m_attachement_image;
		newNode.m_background_image = this.m_background_image;
		newNode.m_lp = this.m_lp;
		newNode.backgroundColor = this.backgroundColor;
		newNode.hexaColor = this.hexaColor;
		newNode.m_strFontName = this.m_strFontName;
		newNode.m_iMapLayoutShape = this.m_iMapLayoutShape;
		newNode.m_strBgImage = this.m_strBgImage;
		newNode.genCharacter = this.genCharacter;
		newNode.dCharacter = this.dCharacter;
		newNode.mediaType = this.mediaType;
		newNode.imageAttachementype=this.imageAttachementype;
		newNode.multimediaurl=this.multimediaurl;
		newNode.m_start_date=this.m_start_date;
		newNode.m_end_date=this.m_end_date;
		newNode.priorityCharacter=this.priorityCharacter;
		newNode.completionCharacter=this.completionCharacter;
		newNode.m_resource=this.m_resource;
		newNode.m_duration=this.m_duration;
		newNode.m_duration_type=this.m_duration_type;
		newNode.base64image=this.base64image;
		newNode.imageUrl=this.imageUrl;
		newNode.m_strMultimediaUrl=this.m_strMultimediaUrl;
		return newNode;

	}
	public ArrayList<MPNode> childrenCopy() {
		ArrayList<MPNode> childrens = new ArrayList<MPNode>();
		if (m_arrChildren != null && m_arrChildren.size() > 0) {
			for (MPNode cNode : m_arrChildren) {
				childrens.add(cNode.copy());
			}
		}
		return childrens;
	}

	public void pasteProperties(MPNode copyPropertiesMpNode) {
		//this.m_fWidth = copyPropertiesMpNode.m_fWidth;
		//this.m_fHeight = copyPropertiesMpNode.m_fHeight;
		this.m_iNode_TextSize = copyPropertiesMpNode.m_iNode_TextSize;
		this.m_strLocation = copyPropertiesMpNode.m_strLocation;
		this.m_strNode_Notes = copyPropertiesMpNode.m_strNode_Notes;
		// this.m_strNode_text = copyPropertiesMpNode.m_strNode_text;
		this.backgroundColor = copyPropertiesMpNode.backgroundColor;
		this.hexaColor = copyPropertiesMpNode.hexaColor;
		this.textColor = copyPropertiesMpNode.textColor;
		this.textAlign = copyPropertiesMpNode.textAlign;
		this.m_selectedColor = copyPropertiesMpNode.m_selectedColor;
		if (m_str_Typeface != null)
			this.m_str_Typeface = copyPropertiesMpNode.m_str_Typeface;
		//this.m_smile_image = copyPropertiesMpNode.m_smile_image;
		//this.genCharacter = copyPropertiesMpNode.genCharacter;
		//this.dCharacter=copyPropertiesMpNode.dCharacter;
		//this.m_attachement_image = copyPropertiesMpNode.m_attachement_image;
		//this.m_background_image = copyPropertiesMpNode.m_background_image;
		this.m_strFontName = copyPropertiesMpNode.m_strFontName;
		//this.m_strAttachmentImage = copyPropertiesMpNode.m_strAttachmentImage;
		this.m_iMapLayoutShape = copyPropertiesMpNode.m_iMapLayoutShape;
		this.m_iGradientType=copyPropertiesMpNode.m_iGradientType;
		this.m_str_Typeface=copyPropertiesMpNode.m_str_Typeface;
		//this.m_strBgImage = copyPropertiesMpNode.m_strBgImage;
	}

	public int getTextCenter() {
		int textSize = AppDict.getPixels(TypedValue.COMPLEX_UNIT_SP,
				getM_iNode_TextSize());
		textCenter = (int) (m_shape.rect.centerY() + textSize / 2);
		return textCenter;
	}

	public void setTextCenter(int textCenter) {
		this.textCenter = textCenter;
	}

	public void deleteOnlyShapes() {
		if (this.m_shape != null && this.m_shape.getParent() != null) {
			ViewGroup viewGroup = (ViewGroup) this.m_shape.getParent();

			viewGroup.removeView(this.m_shape);
			// this.m_shape=null;
		}

	}
}
