//Manahij script-----------------------------
var LastSearchWord="";
var findoccurencetext="";
var commonvariable="";
/*NewHighlight*/
function Highlight(SearchString,Occurence,ColorStyle)
{	
	//alert("SearchString"+SearchString);
    //alert("selecton:"+window.getSelection());
	//SearchString="Pupil's Book";
	SearchString=SearchString.replace(/dqzd/g,'\"');
	//alert(SearchString);
	SearchString=changeRegExpKeywords(SearchString);
	//alert(ColorStyle);
    var searchFor = new RegExp(SearchString.replace(/\s+/g,'\\s+'), 'g');
    //alert(searchFor);
    var occurenceFor=Occurence;
    //alert(Occurence);
    //alert(occurenceFor);
	//GetdoOccurence('test',ColorStyle,searchFor);
    //var newposition=GetSameWordOccurence(SearchString,occurenceFor);
	//alert(occurenceFor + "," + newposition);
	//alert("New Position:" + newposition);
    //alert(newposition);
    doHighlight('test', ColorStyle, searchFor, occurenceFor);
}

function GetSameWordOccurence(searchFor,Occurence)
{
    var newposition=0;
	//if(LastSearchWord!=searchFor)
	//{
		//alert('hai');
		LastSearchWord=searchFor;
		findoccurencetext="";
		var i=0;
		//alert(searchFor);
		var strFound=self.find(searchFor,1,1);
		while (strFound) {
			strFound=self.find(searchFor,1,1);
			//while (self.find(searchFor,0,1)) continue
		}
	    if(window.getSelection()==searchFor)
		{
			findoccurencetext=String(window.getSelection());
			i=1;
		}
		while(self.find(searchFor,1,0))
		{   
			if(i==0)
			{
				if(String(window.getSelection())!="")
				{
					findoccurencetext=String(window.getSelection());
					i=i+1;
				}                
			}
			else
			{
				findoccurencetext= findoccurencetext + "|" + String(window.getSelection());
				i=i+1;
			}
			continue;  
		}
		/*if(!self.find("-4-",0,0))
		{
			alert("Hai");
		}*/
	//}
	/*if(findoccurencetext=="")
	{
		alert("hai");
		var i=0;
		//alert(searchFor);
		while(self.find(searchFor,0,1))
		{   
			if(i==0)
			{
				if(String(window.getSelection())!="")
				{
					findoccurencetext=String(window.getSelection());
					i=i+1;
				}                
			}
			else
			{
				findoccurencetext= findoccurencetext + "|" + String(window.getSelection());
				i=i+1;
			}
			//continue;  
		}
	}*/
	//alert(findoccurencetext);
	/*strFound=self.find(searchFor);
	if (!strFound) {
		//alert("hai");
		strFound=self.find(searchFor,0,1)
		while (self.find(searchFor,0,1)) continue
	}
	//alert(strFound);
    //alert(i);*/

    var splittext=findoccurencetext.split('|');
    //alert(findoccurencetext);
    for(i=0;i<splittext.length && i<Occurence;i++)
    {
        //alert(splittext[i] + " " + splittext.length);
        //alert(searchFor + " == " + splittext[i] + " -> " + ((searchFor==splittext[i])?"true":"false"));
        if(searchFor==splittext[i])
        {
            newposition=newposition+1;
        }
    }
	//this.form.reset();
    //alert(newposition); 
    return newposition;
          
}

/*function addHyperLink(hrefValue){

    selectBetweentwoRange();
    if (selection.rangeCount){
        var range = selection.getRangeAt(0).cloneRange();

        var link = document.createElement("a");
        link.href = hrefValue;
        link.setAttribute("contentEditable",false);
        range.surroundContents(link);
        selection.removeAllRanges();
        selection.addRange(range);

    }
} */

function addHyperLink(hrefValue){

    selectBetweentwoRange();
    if (selection.rangeCount){
        var range = selection.getRangeAt(0).cloneRange();
        var splitHref = hrefValue.split("?");
        var link = document.createElement("a");
        var idValue = "HLink"+splitHref[1];
        link.setAttribute("id",idValue);
        link.href = hrefValue;
        link.setAttribute("contentEditable",false);
        range.surroundContents(link);
        selection.removeAllRanges();
        selection.addRange(range);

    }
}

function removeHyperLink(hrefValue){

    var splitHref = hrefValue.split("?");
    var idValue = "HLink"+splitHref[1];
    var aTag = document.getElementById(idValue);
    //Delete element
    var spanElement = document.createElement("span");
    var textNode = document.createTextNode(aTag.innerText);
    spanElement.appendChild(textNode);
    aTag.parentNode.replaceChild(spanElement, aTag);
}

function GetdoOccurence(node,className,searchFor,which){
    var doc = document;
    
    // normalize node argumeweContextnt
    if (typeof node === 'string') {
        //node = doc.getElementById(node);
        node=doc.body;
    }
    
    // normalize search arguments, here is what is accepted:
    // - single string
    // - single regex (optionally, a 'which' argument, default to 0)
    if (typeof searchFor === 'string') {
        searchFor = new RegExp(searchFor,'ig');
    }
    which = which || 0;
    
    // initialize root loop
    var indices = [],
    text = [], // will be morphed into a string later
    iNode = 0,
    nNodes = node.childNodes.length,
     nodeText,
    textLength = 0,
    stack = [],
    child, nChildren,
    state;
    // collect text and index-node pairs
    for (;;){
        while (iNode<nNodes){
            child=node.childNodes[iNode++];
            // text: collect and save index-node pair
            if (child.nodeType === 3){
                indices.push({i:textLength, n:child});
                nodeText = child.nodeValue;
                text.push(nodeText);
                textLength += nodeText.length;
            }
            // element: collect text of child elements,
            // except from script or style tags
            else if (child.nodeType === 1){
                // skip style/script tags
                if (child.tagName.search(/^(script|style)$/i)>=0){
                    continue;
                }
                // add extra space for tags which fall naturally on word boundaries
                if (child.tagName.search(/^(a|b|basefont|bdo|big|em|font|i|s|small|span|strike|strong|su[bp]|tt|u)$/i)<0){
                    text.push(' ');
                    textLength++;
                }
                // save parent's loop state
                nChildren = child.childNodes.length;
                if (nChildren){
                    stack.push({n:node, l:nNodes, i:iNode});
                    // initialize child's loop
                    node = child;
                    nNodes = nChildren;
                    iNode = 0;
                }
            }
        }
        // restore parent's loop state
        if (!stack.length){
            break;
        }
        state = stack.pop();
        node = state.n;
        nNodes = state.l;
        iNode = state.i;
    }
    
    // quit if found nothing
    if (!indices.length){
        return;
    }
    
    // morph array of text into contiguous text
    text = text.join('');
    
    
    // sentinel
    indices.push({i:text.length});
    
    // find and hilight all matches
    var iMatch, matchingText,
    iTextStart, iTextEnd,
    i, iLeft, iRight,
    iEntry, entry,
    parentNode, nextNode, newNode,
    iNodeTextStart, iNodeTextEnd,
    textStart, textMiddle, textEnd;
    
    var i=0;
    // loop until no more matches
    for (;;){
        
        // find matching text, stop if none
        matchingText = searchFor.exec(text);		
        if (!matchingText || matchingText.length<=which || !matchingText[which].length){
            break;
        }
        i=i+1;
    }
    //alert(i);
}

function changeRegExpKeywords(searchFor)
{
	var replacespecial=searchFor.replace(/\\/g,'\\\\');	
	
	replacespecial=replacespecial.replace(/\[/g,'\\['); 
											 
	replacespecial=replacespecial.replace(/\^/g,'\\^');       
											 
	replacespecial=replacespecial.replace(/\$/g,'\\$');
											 
	replacespecial=replacespecial.replace(/\./g,'\\.');
											 
	replacespecial=replacespecial.replace(/\|/g,'\\|');
											 
	replacespecial=replacespecial.replace(/\?/g,'\\?');
											 
	replacespecial=replacespecial.replace(/\*/g,'\\*');
											 
	replacespecial=replacespecial.replace(/\+/g,'\\+'); 
											 
	replacespecial=replacespecial.replace(/\(/g,'\\(');
																				   
	replacespecial=replacespecial.replace(/\)/g,'\\)');
											 
	return replacespecial;    
											 
}
											 
function Reset()
{
	var elems = document.querySelectorAll('.highlight');
    var n = elems.length;
    while (n--){
		var e = elems[n];
		e.parentNode.replaceChild(e.childNodes[0], e);
    }
}

function doHighlight(node,className,searchFor,Occurence,which){
	var doc = document;
	
	// normalize node argument
	if (typeof node === 'string') {
		//node = doc.getElementById(node);
		node=doc.body;
	}
	
	// normalize search arguments, here is what is accepted:
	// - single string
	// - single regex (optionally, a 'which' argument, default to 0)
	if (typeof searchFor === 'string') {
		searchFor = new RegExp(searchFor,'ig');
	}
	which = which || 0;
	
	// initialize root loop
	var indices = [],
	text = [], // will be morphed into a string later
	iNode = 0,
	nNodes = node.childNodes.length,
	nodeText,
	textLength = 0,
	stack = [],
	child, nChildren,
	state;
	// collect text and index-node pairs
	for (;;){
		while (iNode<nNodes){
			child=node.childNodes[iNode++];
			// text: collect and save index-node pair
			if (child.nodeType === 3){
				indices.push({i:textLength, n:child});
				nodeText = child.nodeValue;
				text.push(nodeText);
				textLength += nodeText.length;
			}
			// element: collect text of child elements,
			// except from script or style tags
			else if (child.nodeType === 1){
				// skip style/script tags
				if (child.tagName.search(/^(script|style)$/i)>=0){
					continue;
				}
				// add extra space for tags which fall naturally on word boundaries
				if (child.tagName.search(/^(a|b|basefont|bdo|big|em|font|i|s|small|span|strike|strong|su[bp]|tt|u)$/i)<0){
					text.push(' ');
					textLength++;
				}
				// save parent's loop state
				nChildren = child.childNodes.length;
				if (nChildren){
					stack.push({n:node, l:nNodes, i:iNode});
					// initialize child's loop
					node = child;
					nNodes = nChildren;
					iNode = 0;
				}
			}
		}
		// restore parent's loop state
		if (!stack.length){
			break;
		}
		state = stack.pop();
		node = state.n;
		nNodes = state.l;
		iNode = state.i;
	}
	
	// quit if found nothing
	if (!indices.length){
		return;
	}
	
	// morph array of text into contiguous text
	text = text.join('');
	
	
	// sentinel
	indices.push({i:text.length});
	
	// find and hilight all matches
	var iMatch, matchingText,
	iTextStart, iTextEnd,
	i, iLeft, iRight,
	iEntry, entry,
	parentNode, nextNode, newNode,
	iNodeTextStart, iNodeTextEnd,
	textStart, textMiddle, textEnd;
	
    var io=0;
	// loop until no more matches
	for (;;){
											 
		// find matching text, stop if none
		matchingText = searchFor.exec(text);		
		if (!matchingText || matchingText.length<=which || !matchingText[which].length){
			break;
		}
		
        if((io+1==Occurence)||(Occurence==0))
        {
											 //alert(i+1);
			// calculate a span from the absolute indices
			// for start and end of match
			iTextStart = matchingText.index;
			for (iMatch=1; iMatch < which; iMatch++){
				iTextStart += matchingText[iMatch].length;
			}
			iTextEnd = iTextStart + matchingText[which].length;
			
			// find entry in indices array (using binary search)
			iLeft = 0;
			iRight = indices.length;
			while (iLeft < iRight) {
				i=iLeft + iRight >> 1;
				if (iTextStart < indices[i].i){iRight = i;}
				else if (iTextStart >= indices[i+1].i){iLeft = i + 1;}
				else {iLeft = iRight = i;}
			}
			iEntry = iLeft;
			
			// for every entry which intersect with the span of the
			// match, extract the intersecting text, and put it into
			// a span tag with specified class
			while (iEntry < indices.length){
											 
				entry = indices[iEntry];
				node = entry.n;
				nodeText = node.nodeValue;
                if(!nodeText.replace(/\s/g,"") == " ")
                {
				parentNode = node.parentNode;
				nextNode = node.nextSibling;
				iNodeTextStart = iTextStart - entry.i;
				iNodeTextEnd = Math.min(iTextEnd,indices[iEntry+1].i) - entry.i;
				
				// slice of text before hilighted slice
				textStart = null;
				if (iNodeTextStart > 0){
					textStart = nodeText.substring(0,iNodeTextStart);
				}
				
				// hilighted slice
				textMiddle = nodeText.substring(iNodeTextStart,iNodeTextEnd);
				
				// slice of text after hilighted slice
				textEnd = null;
				if (iNodeTextEnd < nodeText.length){
					textEnd = nodeText.substr(iNodeTextEnd);
				}
											 //if(nodeText.length!=4)
											 //{
											 
												//alert(nodeText.length + " , "  + nodeText.replace(/    /g,"g"));
											 
				// update DOM according to found slices of text
				if (textStart){
					node.nodeValue = textStart;
				}
				else {
					parentNode.removeChild(node);
				}
				newNode = doc.createElement('span');
				newNode.appendChild(doc.createTextNode(textMiddle));
				newNode.className = className;
				parentNode.insertBefore(newNode,nextNode);
				if (textEnd){
											 //alert("Create Span");
					newNode = doc.createTextNode(textEnd);
					parentNode.insertBefore(newNode,nextNode);
					indices[iEntry] = {n:newNode,i:iTextEnd}; // important: make a copy, do not overwrite
				}
											 //}
                }
				// if the match doesn't intersect with the following
				// index-node pair, this means this match is completed
				iEntry++;
				if (iTextEnd <= indices[iEntry].i){
					break;
				}
			}
			io=io+1;
											 //alert("i incremented" + i);								 
        }
        else
        {
        	io=io+1;
        }
	}
}


//Old Release
function fh() {

  var txt = window.getSelection();
	
	var s=txt.focusNode.textContent;
	
  //var selectedText1= s.substring(txt.anchorOffset,txt.focusOffset);

	var selectedText= s.substring(txt.focusOffset,txt.anchorOffset);
	
	var i=0;
	
	while(self.find(txt))
	{
		i=i+1;
	}
	
	//var range = window.getSelection().getRangeAt(0);
	//alert(range.startOffset + ' ' + range.endOffset);
	
	//ar range = window.getSelection().getRangeAt(0);
	//var dummy = document.createElement("span");
	//range.insertNode(dummy);
	//var box = document.getBoxObjectFor(dummy);
	//var x = box.x, y = box.y;
	//dummy.parentNode.removeChild(dummy);
	//alert(x + ' , ' + y);
	
		
	//alert(txt.anchorOffsetLeft + '|' + posY);
	
	//alert(window.pageXOffset + ' , ' + window.pageYOffset);
	
	//getCursorPos();

	
  //return ( i + '|' + self.find(txt) + ' h|k ' + selectedText + '|' + txt.anchorOffset + '|' + txt.anchorNode.textContent + '|' + txt.focusOffset + '|' + txt.focusNode.textContent);
	//document.selection.clear();
	
	ResetSelection(txt,i);
	//alert(txt.HTMLText);
	return (txt + '|' +  i);

}

                                             /*function getSelectionCoordinatess(start) {
                                             var x = 0, y = 0, range;
                                             if (window.getSelection) {
                                             var sel = window.getSelection();
                                             //alert(sel);
                                             if (sel.rangeCount) {
                                             range = sel.getRangeAt(sel.rangeCount - 1);
                                             range.collapse(start);
                                             var dummy = document.createElement("span");
                                             range.insertNode(dummy);
                                             var rect = dummy.getBoundingClientRect();
                                             x = rect.left;
                                             y = rect.top;
                                             dummy.parentNode.removeChild(dummy);
                                             }
                                             } else if (document.selection && document.selection.type != "Control") {
                                             range = document.selection.createRange();
                                             range.collapse(start);
                                             x = range.boundingLeft;
                                             y = range.boundingTop;
                                             }
                                             return {x: x, y: y};
                                             }*/

                                             function getSelectionCoordinatess(start) {
                                             		var x = 0, y = 0;
                                             		var sel = window.getSelection();
                                                        if (!sel) {
                                                            return;
                                                        }
                                                        var range = sel.getRangeAt(0);

                                                        // Add spans to the selection to get page offsets
                                                        var selectionStart = $("<span id=\"selectionStart\">&#xfeff;</span>");
                                                        var selectionEnd = $("<span id=\"selectionEnd\"></span>");

                                                        var startRange = document.createRange();
                                                        startRange.setStart(range.startContainer, range.startOffset);
                                                        startRange.insertNode(selectionStart[0]);

                                                        var endRange = document.createRange();
                                                        endRange.setStart(range.endContainer, range.endOffset);
                                                        endRange.insertNode(selectionEnd[0]);

                                                        var handleBounds = "{'left': " + (selectionStart.offset().left) + ", ";
                                                        handleBounds += "'top': " + (selectionStart.offset().top + selectionStart.height()) + ", ";
                                                        handleBounds += "'right': " + (selectionEnd.offset().left) + ", ";
                                                        handleBounds += "'bottom': " + (selectionEnd.offset().top + selectionEnd.height()) + "}";

														x = selectionStart.offset().left;
														y = selectionStart.offset().top;

                                                        // Pull the spans
                                                        selectionStart.remove();
                                                        selectionEnd.remove();

                                                        // Reset range
                                                        sel.removeAllRanges();
                                                        sel.addRange(range);

														return {x: x, y: y};
                                             }
                                                                               
function GetNewPosition(SText,Occurence)
{
	//alert(SText);
    SText=SText.replace(/dqzs/g,'\\');
	SText=SText.replace(/dqzx/g,'\'');
	SText=SText.replace(/dqzd/g,'\"');
    //SText = SText.replace(/^\s+|\s+$/g,"");
    SText = SText.replace(/<br \/>/g, "\n");
    SText = SText.replace(/^\s+|\s+$/g,"");
    //alert(SText);
    var i=0;
    var j=0;
    //alert("hai");
    //alert(SText + " , " + Occurence);                                         
    while (self.find(SText,1,0))
    {    
        j=j+1;
        //alert("Selected Text: "+window.getSelection());
        if(window.getSelection()!="")
        {
             i=i+1;
        }                                                                                 
        if(i==Occurence)
        {                                    
			break;
		}
		else
		{   
			continue;
		}
	}
    //alert(window.getSelection());
    //alert(j);
    //alert("No of Occurence: " + Occurence + "\n No of times loop executed: " + j);                                        
    var coords = getSelectionCoordinatess(true);
    //var coords = getSelectionCoordinates(true);
    //alert(coords.y);
	return coords.y;
}
											 
function getPos(el) {
											 
// yay readability
											 
 for (var lx=0, ly=0; el != null; lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
                                             
                                             
											 
return {x: lx,y: ly};
											 
}

function fn(version) {
	var txtHtml = window.getSelection().toString();

	var i=0;
	var flag=0;
                                              
	while(window.find(txtHtml,1,1))
	{
		i=i+1;
		flag=1;
	}
	if(flag==1)
	{
		i=i+1;
	}
	else
	{
		i=1;
	}
	var j=1;
	while(j<i)
	{
		j=j+1;									
		window.find(txtHtml,1,0);	
	}
	var coords = getSelectionCoordinatess(true);
	if(version == "KITKAT"){
		return txtHtml + '|' +  i + '|' + coords.y + '|' + txtHtml;
	} else {
		window.TextSelection.getSplit(txtHtml + '|' +  i + '|' + coords.y + '|' + txtHtml);
	}
}

function GetOccurenceAfterSelectedText(html,SText) {
    var sel, range, node;
	var retval=0;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
			//var SText=GetHTMLText();
            range = window.getSelection().getRangeAt(0);
            range.collapse(false);
            node = range.createContextualFragment(html);
            range.insertNode(node);
			
			
			
			//alert(SText);
			var secosour=document.getElementsByTagName('html')[0].innerHTML;
			//range.removeNode(true);
			//node.parentNode.removeNode(node.childNodes);
			//alert(secosour);
			
			
			var searchResults=secosour.split(html);
			//alert(searchResults[1]);
			//alert(secosour);
			searchResults[1]=searchResults[1].replace(SText,"");
			var searchResults1=searchResults[1].split(SText);
			//alert(searchResults1.length);
			//alert(searchResults[1] + "\r\n\r\nGovind" + searchResults1[0] + "\r\n\r\n" + searchResults1[1] + "\r\n\r\n" + searchResults1[2]);
			//alert(searchResults1.length-1 + "\r\n   " + secosour  + "   " + SText);
			//document.getElementsByTagName('html')[0].innerHTML=searchResults[0] + SText + searchResults[1];
			//node.parentNode.removeChild(node);
			//alert(document.getElementsByTagName('html')[0].innerHTML);
			//alert('hai');
			var p2 = document.getElementsByTagName('tt')[0];
			p2.parentNode.removeChild(p2);
			//alert('hai');
			//alert(document.getElementsByTagName('html')[0].innerHTML);
			
			retval=(searchResults1.length-1);
			
        }
    } else if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        range.collapse(false);
        range.pasteHTML(html);
    }
	return retval;
}


function insertHtmlAfterSelection(html) {
    var sel, range, node;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = window.getSelection().getRangeAt(0);
            range.collapse(false);
            node = range.createContextualFragment(html);
            range.insertNode(node);
			
			
			var SText=GetHTMLText();
			var secosour=document.getElementsByTagName('html')[0].innerHTML;
			var searchResults=secosour.split("<tt></tt>");
			var searchResults1=searchResults[1].split(SText);
			
			//alert(searchResults1.length-1 + "\r\n   " + secosour  + "   " + SText);
			document.getElementsByTagName('html')[0].innerHTML=searchResults[0] + searchResults[1];
			//alert(document.getElementsByTagName('html')[0].innerHTML);
			
			
        }
    } else if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        range.collapse(false);
        range.pasteHTML(html);
    }
}

function SetHighlight(SourceString,SText,Occurence,Color)
 {
	 //var st=document.all(0).innerHTML;
	 //var splittxt=st.split("Ù�Ù�ÙŠ");
	 //alert('Hai');
	 
	 	 //var TText="king";
	 //SourceString=SourceString.replace(/king/g,SText);
	 //SourceString=SourceString.replace(/King/g,SText);
	 
 var searchResults=SourceString.split(SText);
 //var OccurenceCount=searchResults.length-1;
	 
	//alert(searchResults.length + SText + SourceString);
	 
 var finalresult="";
	 //alert(OccurenceCount);
	 
 //alert(searchResults.length +  '    ' + SText);
 var i=0;
 for(i=0;i<(searchResults.length-(Occurence+1));i++)
 {
	//alert('hai1');

	 
	 if((i+1)==(searchResults.length-(Occurence+1)))
	 {
		 
		 var isGreaterPosition=SText.indexOf('>');
		 //alert(isGreaterPosition);
		 if(isGreaterPosition>=0)
		 {
			 var splittxt=SText.split('>');
			 finalresult=finalresult + searchResults[i] + "<span style=\"background-Color:" + Color + "\">" +  splittxt[0] + "><span style=\"background-Color:" + Color + "\">" + splittxt[1] + "</span>";
		 }
		 else
		 {
			 finalresult=finalresult + searchResults[i] + "<span style=\"background-Color:" + Color + "\">" +  SText + "</span>";
		 }
 
	 }
	 else {		
		 //alert('hai');
		 finalresult=finalresult + searchResults[i] + SText;			
	 }
 }
	 
 
for(j=i;j<searchResults.length;j++)
 {
	 if((j+1)==searchResults.length)
	 {
		 finalresult=finalresult + searchResults[j];
	 }
	 else {			
		 finalresult=finalresult + searchResults[j] + SText;
	 }
 }
 
 return finalresult;
 
 }

function SetNotes(SourceString,SText,Occurence,Color,YPosition,TID)
{
	//alert('1S');
	//alert(SourceString + '       ' + SText);
	var searchResults=SourceString.split(SText);
	//alert(searchResults.length);
	
	
	var finalresult="";
	
	
	var i=0;
	for(i=0;i<(searchResults.length-(Occurence+1));i++)
	{
		
		if((i+1)==(searchResults.length-(Occurence+1)))
		{
			var isGreaterPosition=SText.indexOf('>');
			if(isGreaterPosition>=0)
			{
				var splittxt=SText.split('>');
				finalresult=finalresult + searchResults[i] + "<span style=\"background-Color:" + Color + "\">" +  splittxt[0] + "><span style=\"background-Color:" + Color + "\">" + splittxt[1] + "</span>";
			}
			else
			{
				finalresult=finalresult + searchResults[i] + "<span style=\"background-Color:" + Color + "\">" +  SText + "</span>";
			}
			
		}
		else {		
			finalresult=finalresult + searchResults[i] + SText;			
		}
	}
	
	var j=0;
	for(j=i;j<searchResults.length;j++)
	{
		if((j+1)==searchResults.length)
		{
			finalresult=finalresult + searchResults[j];
		}
		else {			
			finalresult=finalresult + searchResults[j] + SText;
		}
	}
	
	finalresult = finalresult + "<div style=z-index:101;position:absolute;width:46;height:46;left:920;top:" + YPosition + ";background-image:url(../../stick_samll.png);background-repeat:none; onclick=window.location=\"/E/" + TID + "\";>&nbsp;</div>";
	//alert(finalresult);
	
	return finalresult;
	
}

function GetHTMLText()
{
	var retval='';
	try
	{
		if (window.getSelection && (window.getSelection()).rangeCount == 1)
		{
			//alert('hai');
			/*var selection_text = (window.getSelection()).toString();
			 //alert(selection_text);
			 var range = (window.getSelection()).getRangeAt(0);
			 
			 
			 
			 
			 //range.setStart(startNode,startOffset);
			 //range.setEnd(endNode,endOffset);
			 alert(range.commonAncestorContainer.innerHTML);
			 var parent_html = range.commonAncestorContainer.innerHTML;
			 //alert('1');
			 //var b = document.createElement("b");
			 //b.innerHTML = parent_html;
			 //var parent_text = b.innerText || b.textContent;
			 var first_text = range.startContainer.nodeValue.substr(range.startOffset);
			 var last_text = (range.endContainer.nodeValue).substring(0,range.endOffset);
			 //alert(parent_html + '\n' + first_text + '\n' + last_text);
			 var start = parent_html.indexOf(first_text);
			 var end = parent_html.indexOf(last_text,start+1)+last_text.length;
			 //alert('1');
			 //alert(parent_html.substring(start,end));	
			 retval=parent_html.substring(start,end);*/
			
			
			var selection_text = (window.getSelection()).toString();
			var range = (window.getSelection()).getRangeAt(0);
			try
			{
				var parent_html = range.commonAncestorContainer.innerHTML;
				if(parent_html!=null)
				{
					//alert('hai');
					//var b = document.createElement("b");
					//b.innerHTML = parent_html;
					//var parent_text = b.innerText || b.textContent;
					var first_text = range.startContainer.nodeValue.substr(range.startOffset);
					var last_text = (range.endContainer.nodeValue).substring(0,range.endOffset);
					var start = parent_html.indexOf(first_text);
					var end = parent_html.indexOf(last_text,start+1)+last_text.length;
					retval=parent_html.substring(start,end);
					//retval=js_RemoveChar(retval);			
				}
				else
				{
					//alert('hai');
					retval=selection_txt;
				}
			}
			catch(err)
			{
				//alert('hai' + selection_txt);
				retval=selection_txt;
			}
		}
	}
	catch(err1)
	{
		retval=(window.getSelection()).toString();
	}
	return retval;
}

function ResetSelection(SText,Occurence)
{
	/*var i=0;
	var j=0;
	var flag=0;
	if(self.find(SText,0,1))
	{
		flag=1;
		i=i+1;
		while (self.find(SText))
		{
			i=i+1;
			continue;
		}
	}	
	alert(i);
	i=i-Occurence;
	if(self.find(SText,0,1))
	{
		//j=j+1;
		if(j==i){
			break;
		}
		else
		{
			j=j+1;
			while (self.find(SText))
			{
				j=j+1;
				if(j==i)
					break;
				else
					continue;
			}
		}
	}
	/*while (self.find(SText,0,1))
	{
		if(j==i)
			break;
		else
			continue;
	}*/
	
	if(Occurence>0)
	{
		//Occurence=Occurence-1;
		var i=0;
		while(self.find(SText,0,1))
		{
			i=i+1;
			//alert(i + ',' + Occurence);
			if(i==Occurence)
				break;
			else
				continue;
		}
			
	}
}

function RemoveSelection()
{
	document.selection.clear();
}

function fnMD()
{
	//alert("Hai");
	//var answer = document.window.showModalDialog("www.google.co.in","","dialogWidth:300px; dialogHeight:200px; center:yes");
	
	/*var dummy = document.createElement("div");
	dummy.id="d";
	dummy.innerHTML="Welcome";
	window.insertNode(dummy);*/
	
	var clicked=true;
	window.location="/click/"+clicked;
}

function getSelectionCoordinates(start) {
	var x = 0, y = 0, i = 0, range;
	//var txt=String(window.getSelection());
	var txt=(GetHTMLText()).toString();
	//var txt=GetHTMLText();
	//alert('1 ' + txt + '\n' + GetHTMLText());
	if (window.getSelection) {
		
		var sel = window.getSelection();
		if (sel.rangeCount) {
			range = sel.getRangeAt(sel.rangeCount - 1);
			range.collapse(start);
			
			/*while(self.find(sel))
			{
				i=i+1;
			}
			
			//alert(i);
			
			if(i>0)
			{
				
				//Occurence=Occurence-1;
				var j=0;
				while(self.find(sel,0,1))
				{
					j=j+1;
					//alert(j + ',' + i);
					if(j==i)
						break;
					else
						continue;
				}
				
			}*/
			
			var dummy = document.createElement("span");
			range.insertNode(dummy);
			var rect = dummy.getBoundingClientRect();
			x = rect.left;
			y = rect.top;
			
			
			
			
			dummy.parentNode.removeChild(dummy);
		}
	} else if (document.selection && document.selection.type != "Control") {
		range = document.selection.createRange();
		range.collapse(start);
		x = range.boundingLeft;
		y = range.boundingTop;
		while(self.find(document.selection))
		{
			i=i+1;
		}
		
		if(i>0)
		{
			//Occurence=Occurence-1;
			var j=0;
			while(self.find(document.selection,0,1))
			{
				j=j+1;
				//alert(j + ',' + i);
				if(j==(i-1))
					break;
				else
					continue;
			}
			
		}
	}
	return {x: x, y: y, i: i, txt: txt};
}


function findPosX(obj)
{
	var curleft = 0;
	if(obj.offsetParent)
		while(1) 
		{
			curleft += obj.offsetLeft;
			if(!obj.offsetParent)
				break;
			obj = obj.offsetParent;
		}
	else if(obj.x)
		curleft += obj.x;
	return curleft;
}

function findPosY(obj)
{
	var curtop = 0;
	if(obj.offsetParent)
		while(1)
		{
			curtop += obj.offsetTop;
			if(!obj.offsetParent)
				break;
			obj = obj.offsetParent;
		}
	else if(obj.y)
		curtop += obj.y;
	return curtop;
}



function getCursorPos() {
	var cursorPos;
	if (window.getSelection) {
		var selObj = window.getSelection();
		var selRange = selObj.getRangeAt(0);
		cursorPos =  findNode(selObj.anchorNode.parentNode.childNodes, selObj.anchorNode) + selObj.anchorOffset;
		/* FIXME the following works wrong in Opera when the document is longer than 32767 chars */
		//alert(cursorPos);
	}
	else if (document.selection) {
		var range = document.selection.createRange();
		var bookmark = range.getBookmark();
		/* FIXME the following works wrong when the document is longer than 65535 chars */
		cursorPos = bookmark.charCodeAt(2) - 11; /* Undocumented function [3] */
		//alert(cursorPos);
	}
}

function findNode(list, node) {
	for (var i = 0; i < list.length; i++) {
		if (list[i] == node) {
			return i;
		}
	}
	return -1;
}


//Epub Reader search Functions:

// We're using a global variable to store the number of occurrences
var MyApp_SearchResultCount = 0;

var console = "\n";
var results = "";

var neighSize = 20;

// helper function, recursively searches in elements and their child nodes
function MyApp_HighlightAllOccurencesOfStringForElement(element,keyword) {
    if (element) {
        if (element.nodeType == 3) {// Text node
            while (true) {
                var value = element.nodeValue;  // Search for keyword in text node
                var idx = value.toLowerCase().indexOf(keyword);
                
                if (idx < 0) break;             // not found, abort
                
                var span = document.createElement("highlight");
                span.className = "MyAppHighlight";
                var text = document.createTextNode(value.substr(idx,keyword.length));
                span.appendChild(text);
                
                var rightText = document.createTextNode(value.substr(idx+keyword.length));
                element.deleteData(idx, value.length - idx);
                
                var next = element.nextSibling;
                element.parentNode.insertBefore(rightText, next);
                element.parentNode.insertBefore(span, rightText);
                
                var leftNeighText = element.nodeValue.substr(element.length - neighSize, neighSize);
                var rightNeighText = rightText.nodeValue.substr(0, neighSize);
                
                element = rightText;
                MyApp_SearchResultCount++;	// update the counter
                
                console += "Span className: " + span.className + "\n";
                console += "Span position: (" + getPos(span).x + ", " + getPos(span).y + ")\n";
                
                results += getPos(span).x + "," + getPos(span).y + "," + escape(leftNeighText + text.nodeValue + rightNeighText) + ";";
                
                results;
            }
        } else if (element.nodeType == 1) { // Element node
            if (element.style.display != "none" && element.nodeName.toLowerCase() != 'select') {
                for (var i=element.childNodes.length-1; i>=0; i--) {
                    MyApp_HighlightAllOccurencesOfStringForElement(element.childNodes[i],keyword);
                }
            }
        }
    }
}

function getPos(el) {
    // yay readability
    for (var lx=0, ly=0; el != null; lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
    return {x: lx,y: ly};
}

// the main entry point to start the search
function MyApp_HighlightAllOccurencesOfString(keyword) {
    MyApp_RemoveAllHighlights();
    MyApp_HighlightAllOccurencesOfStringForElement(document.body, keyword.toLowerCase());
}

// helper function, recursively removes the highlights in elements and their childs
function MyApp_RemoveAllHighlightsForElement(element) {
    if (element) {
        if (element.nodeType == 1) {
            if (element.getAttribute("class") == "MyAppHighlight") {
                var text = element.removeChild(element.firstChild);
                element.parentNode.insertBefore(text,element);
                element.parentNode.removeChild(element);
                return true;
            } else {
                var normalize = false;
                for (var i=element.childNodes.length-1; i>=0; i--) {
                    if (MyApp_RemoveAllHighlightsForElement(element.childNodes[i])) {
                        normalize = true;
                    }
                }
                if (normalize) {
                    element.normalize();
                }
            }
        }
    }
    return false;
}

// the main entry point to remove the highlights
function MyApp_RemoveAllHighlights() {
    MyApp_SearchResultCount = 0;
    MyApp_RemoveAllHighlightsForElement(document.body);
}


//SboookAuthor script-----------------------------

var span;
var sel;
var startOffset, endOffset;
var selectionStartRange, selectionEndRange;

function saveSelectionStart(){
    try{
        
		// Save the starting point of the selection
	   	var sel = window.getSelection();
		var range = sel.getRangeAt(0);
		
		var saveRange = document.createRange();
		
		saveRange.setStart(range.startContainer, range.startOffset);
		
		selectionStartRange = saveRange;
	}catch(err){
		//window.TextSelection.jsError(err);
	}
}

function saveSelectionEnd(){
    
	try{
        
		// Save the end point of the selection
	   	var sel = window.getSelection();
		var range = sel.getRangeAt(0);
		
		var saveRange = document.createRange();
		saveRange.setStart(range.endContainer, range.endOffset);
		
		selectionEndRange = saveRange;
	}catch(err){
		//window.TextSelection.jsError(err);
	}
	
}
/*var selection;
function selectBetweentwoRange(){
    var startCaret = selectionStartRange;
    var endCaret = selectionEndRange;
    
    var range = document.createRange();
    range.setStart(startCaret.startContainer, startCaret.startOffset);
    range.setEnd(endCaret.startContainer, endCaret.startOffset);
    
    selection = window.getSelection();
    selection.addRange(range);
}

function surroundSelection(textColor){
    //alert(selection);
    selectBetweentwoRange();
    span.style.color = textColor;
    
    
    //alert(textColor);
    if (selection.rangeCount){
        var range = selection.getRangeAt(0).cloneRange();
        range.surroundContents(span);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}


function surroundSelectionFontFamily(fontFamily, fontSize){
    //"var span = document.createElement(\"span\");"
    selectBetweentwoRange();
    span.style.fontFamily = fontFamily;
    span.style.fontSize = fontSize;
    
    //"span.style.color = textColor;"
    
    
    //"alert(textColor);"
    if (selection.rangeCount){
        var range = selection.getRangeAt(0).cloneRange();
        range.surroundContents(span);
        selection.removeAllRanges();
        selection.addRange(range);
    } }*/


//Update the FontFamily in Store Books

function updateFontFamilyInStoreBooks(fontFace){
    var fontTagContent = document.body.getElementsByTagName("font").length;
    
    for(var i = 0; i < fontTagContent; i++){
        document.getElementsByTagName("font")[i].setAttribute("face",fontFace);
    }
}

//Update the FontSize in Store Books

function updateFontSizeInStoreBooks(fontSiz){
    //alert(fontSiz);
    var fontTagContent = document.body.getElementsByTagName("font").length;
    for(var i = 0; i < fontTagContent; i++){
        document.getElementsByTagName("font")[i].style.fontSize=fontSiz+"px";
    }
}

//Update the FontColor in Store Books

function updateFontColorInStoreBooks(fontColor){
    //alert(fontColor);
    var fontTagContent = document.body.getElementsByTagName("font").length;
    for(var i = 0; i < fontTagContent; i++){
        document.getElementsByTagName("font")[i].style.color=fontColor;
    }
}

// Enrichments
function disableContentEditable(){
    //alert("hiii");
    var totalDiv = document.body.getElementsByTagName("div").length;
    for(var i=0;i<totalDiv;i++){
        document.body.getElementsByTagName("div")[i].contentEditable = false;
    }
}

function getAndReturnWebContent(version){
	var text = document.body.innerHTML;
	if(version == "KITKAT"){
		return text.toString();
	} else {
		window.WebText.setAndReturnWebContent(text);
	}
}

//Get Web Content
function getWebContent(){
	//alert("hiii");
	var text = document.body.innerHTML;
	//alert(text);
	window.WebText.setWebContent(text);
};

//Get FontSize
function getFontSize(idValue){
	var font_size = window.getComputedStyle(document.getElementById(idValue)).fontSize;
	window.WebText.setFontSize(font_size);
};

//Get FontFamily
function getFontFamily(idValue){
	var font_family = window.getComputedStyle(document.getElementById(idValue)).fontFamily;
	window.WebText.setFontFamily(font_family);
};

//Get FontColor
function getFontColor(idValue){
	var font_color = window.getComputedStyle(document.getElementById(idValue)).color;
	var hexColorVal = rgb2hex(font_color);
	window.WebText.setFontColor(hexColorVal);
};

//Get BackgroundColor
function getBackgroundColor(idValue){
	var background_color = window.getComputedStyle(document.getElementById(idValue)).backgroundColor;
	var hexColorVal = rgb2hex(background_color);
	window.WebText.setBackgroundColor(hexColorVal);
};

var selection;
var selectionStartRange, selectionEndRange;
var isSpanCreated = false;
var surroundSpanContents = false;
function selectBetweentwoRange(){
    var startCaret = selectionStartRange;
    var endCaret = selectionEndRange;
    
    var range = document.createRange();
    range.setStart(startCaret.startContainer, startCaret.startOffset);
    range.setEnd(endCaret.startContainer, endCaret.startOffset);
    
    selection = window.getSelection();
    selection.addRange(range);
};

//Surround the selected text area
function surroundSelection(textColor){
    //alert(selection);
    selectBetweentwoRange();
    span.style.color = textColor;
    //alert(textColor);
    if (selection.rangeCount){
        var range = selection.getRangeAt(0).cloneRange();
        if(surroundSpanContents == false)
             range.surroundContents(span);
        selection.removeAllRanges();
        selection.addRange(range);
        surroundSpanContents=true;
    }
};

function createSpan(){
    if (window.getSelection && !isSpanCreated){
        span = document.createElement("span");
        sel = window.getSelection();
        selection = window.getSelection();
        //alert(sel.toString());
        //var range = sel.getRangeAt(0);
        //startOffset = range.startOffset;
        //endOffset = range.endOffset;
        saveSelectionStart();
        saveSelectionEnd();
        isSpanCreated = true;
    }
    //alert(sel.toString());
    //return sel.toString();
}

/*function createSpan(){

    if (window.getSelection){
        span = document.createElement("span");
        sel = window.getSelection();
        var range = sel.getRangeAt(0);
        //startOffset = range.startOffset;
        //endOffset = range.endOffset;
        saveSelectionStart();
        saveSelectionEnd();
    }

    return sel.toString();
}*/

function resetSelectionRangeValue(){
    //alert('reset');
    selectionStartRange = null;
    selectionEndRange = null;
    //selection.removeAllRanges();
    isSpanCreated = false;
    surroundSpanContents=false;
}

function surroundSelectionFontFamily(fontFamily){
    selectBetweentwoRange();
    span.style.fontFamily = fontFamily;

    if (selection.rangeCount){
        var range = selection.getRangeAt(0).cloneRange();
        if(surroundSpanContents == false)
            range.surroundContents(span);
        selection.removeAllRanges();
        selection.addRange(range);
        surroundSpanContents=true;
    }
}

function surroundSelectionFontSize(fontSize){
    selectBetweentwoRange();
    span.style.fontSize = fontSize;

    if (selection.rangeCount){
        var range = selection.getRangeAt(0).cloneRange();
        if(surroundSpanContents == false)
            range.surroundContents(span);
        selection.removeAllRanges();
        selection.addRange(range);
        surroundSpanContents=true;
    }
}

function saveSelectionStart(){
    try{
		// Save the starting point of the selection
	   	var sel = window.getSelection();
		var range = sel.getRangeAt(0);
		
		var saveRange = document.createRange();
		
		saveRange.setStart(range.startContainer, range.startOffset);
		
		selectionStartRange = saveRange;
	}catch(err){
		//window.TextSelection.jsError(err);
	}
}

function saveSelectionEnd(){
	try{
		// Save the end point of the selection
	   	var sel = window.getSelection();
		var range = sel.getRangeAt(0);
		
		var saveRange = document.createRange();
		saveRange.setStart(range.endContainer, range.endOffset);
		
		selectionEndRange = saveRange;
	}catch(err){
		//window.TextSelection.jsError(err);
	}
}

function isSelectionActive(){
	var selectionActive = rangy.getSelection().isCollapsed;
	window.WebText.setSelectionActive(selectionActive);
}

function changeLanguageQuiz(uniqId, sectId, dir, tAlign, backValue, restartValue){

    var quizID = "q"+sectId+"_"+uniqId;
    var quizTitleID = "quizTitle_"+uniqId+"_"+sectId;
    var quiz_questID = "quiz_question_"+uniqId+"_"+sectId;
    document.getElementById("q"+sectId+"_"+uniqId).style.direction = dir;
    document.getElementById("quizTitle_"+uniqId+"_"+sectId).style.direction = dir;
    document.getElementById("quiz_question_"+uniqId+"_"+sectId).style.direction = dir;

    //Text align for the quiz options ID:
    document.getElementById("quiz_options_"+uniqId+"_"+sectId).style.textAlign = tAlign;

    //Text value for back and restart:
    document.getElementById("btnBack").value =backValue;
    document.getElementById("btnRestart").value =restartValue;
    //Quiz options for Quiz type 1 and 3:
    var tAlign1 = "right";
    var float1 = "left";
    var tAlign2 = "left";
    var float2 = "right";
    var label1Html ="<b> A.&nbsp;</b>"; var label2Html ="<b> B.&nbsp;</b>"; var label3Html ="<b> C.&nbsp;</b>"; var label4Html ="<b>D.&nbsp;</b>"; var label5Html ="<b> E.&nbsp;</b>"; var label6Html ="<b> F.&nbsp;</b>";
    if(dir == "rtl"){
        tAlign1 = "left";
        float1 = "right";
        tAlign2 = "right";
        float2 = "left";
        label1Html ="<b> أ)&nbsp;</b>"; label2Html ="<b> ب)&nbsp;</b>"; label3Html ="<b> ت)&nbsp;</b>"; label4Html ="<b> جـ)&nbsp;</b>"; label5Html ="<b> حـ)&nbsp;</b>"; label6Html ="<b> د)&nbsp;</b>";
    }

        if(document.getElementsByClassName("quiz_4")){
            //alert("HI");
            if(dir == "rtl"){
                var type4_div_a = document.getElementById("q"+sectId+"_div_a_"+uniqId);
                type4_div_a.style.float = "right";
                type4_div_a.style.marginRight = "0";
                type4_div_a.style.marginLeft = "2%";

                var type4_input_a = document.getElementById("q"+sectId+"a_"+uniqId);
                type4_input_a.style.left = "0";
                type4_input_a.style.right = "1%";

                var type4_div_b = document.getElementById("q"+sectId+"_div_b_"+uniqId);
                type4_div_b.style.float = "right";

                var type4_input_b = document.getElementById("q"+sectId+"b_"+uniqId);
                type4_input_b.style.left = "0";
                type4_input_b.style.right = "1%";

                var type4_div_c = document.getElementById("q"+sectId+"_div_c_"+uniqId);
                type4_div_c.style.float = "right";
                type4_div_c.style.marginRight = "0";
                type4_div_c.style.marginLeft = "2%";

                var type4_input_c = document.getElementById("q"+sectId+"c_"+uniqId);
                type4_input_c.style.left = "0";
                type4_input_c.style.right = "1%";

                var type4_div_d = document.getElementById("q"+sectId+"_div_d_"+uniqId);
                type4_div_d.style.float = "right";

                var type4_input_d = document.getElementById("q"+sectId+"d_"+uniqId);
                type4_input_d.style.left = "0";
                type4_input_d.style.right = "1%";

                var type4_div_e = document.getElementById("q"+sectId+"_div_e_"+uniqId);
                type4_div_e.style.float = "right";
                type4_div_e.style.marginRight = "0";
                type4_div_e.style.marginLeft = "2%";

                var type4_input_e = document.getElementById("q"+sectId+"e_"+uniqId);
                type4_input_e.style.left = "0";
                type4_input_e.style.right = "1%";

                var type4_div_f = document.getElementById("q"+sectId+"_div_f_"+uniqId);
                type4_div_f.style.float = "right";

                var type4_input_f = document.getElementById("q"+sectId+"f_"+uniqId);
                type4_input_f.style.left = "0";
                type4_input_f.style.right = "1%";
            } else {
                var type4_div_a = document.getElementById("q"+sectId+"_div_a_"+uniqId);
                type4_div_a.style.float = "left";
                type4_div_a.style.marginRight = "0%";
                type4_div_a.style.marginLeft = "2%";
               // alert("HI");
                var type4_input_a = document.getElementById("q"+sectId+"a_"+uniqId);
                type4_input_a.style.left = "1%";
                type4_input_a.style.right = "0";

                var type4_div_b = document.getElementById("q"+sectId+"_div_b_"+uniqId);
                type4_div_b.style.float = "left";

                var type4_input_b = document.getElementById("q"+sectId+"b_"+uniqId);
                type4_input_b.style.left = "1%";
                type4_input_b.style.right = "0";

                var type4_div_c = document.getElementById("q"+sectId+"_div_c_"+uniqId);
                type4_div_c.style.float = "left";
                type4_div_c.style.marginRight = "0%";
                type4_div_c.style.marginLeft = "2%";

                var type4_input_c = document.getElementById("q"+sectId+"c_"+uniqId);
                type4_input_c.style.left = "1%";
                type4_input_c.style.right = "0";

                var type4_div_d = document.getElementById("q"+sectId+"_div_d_"+uniqId);
                type4_div_d.style.float = "left";

                var type4_input_d = document.getElementById("q"+sectId+"d_"+uniqId);
                type4_input_d.style.left = "1%";
                type4_input_d.style.right = "0";

                var type4_div_e = document.getElementById("q"+sectId+"_div_e_"+uniqId);
                type4_div_e.style.float = "left";
                type4_div_e.style.marginRight = "0%";
                type4_div_e.style.marginLeft = "2%";

                var type4_input_e = document.getElementById("q"+sectId+"e_"+uniqId);
                type4_input_e.style.left = "1%";
                type4_input_e.style.right = "0";

                var type4_div_f = document.getElementById("q"+sectId+"_div_f_"+uniqId);
                type4_div_f.style.float = "left";

                var type4_input_f = document.getElementById("q"+sectId+"f_"+uniqId);
                type4_input_f.style.left = "1%";
                type4_input_f.style.right = "0";
            }
        }

    if(document.getElementById("q"+sectId+"_div_1_a_"+uniqId)){

        var div_1_a = document.getElementById("q"+sectId+"_div_1_a_"+uniqId);
        div_1_a.style.textAlign = tAlign1;
        div_1_a.style.float = float1;
        var div_1_b = document.getElementById("q"+sectId+"_div_2_a_"+uniqId);
        div_1_b.style.textAlign = tAlign2;
        div_1_b.style.float = float2;
        div_1_b.getElementsByTagName("label")[0].innerHTML = label1Html;

        var div_2_a = document.getElementById("q"+sectId+"_div_1_b_"+uniqId);
        div_2_a.style.textAlign = tAlign1;
        div_2_a.style.float = float1;
        var div_2_b = document.getElementById("q"+sectId+"_div_2_b_"+uniqId);
        div_2_b.style.textAlign = tAlign2;
        div_2_b.style.float = float2;
        div_2_b.getElementsByTagName("label")[0].innerHTML = label2Html;

        var div_3_a = document.getElementById("q"+sectId+"_div_1_c_"+uniqId);
        div_3_a.style.textAlign = tAlign1;
        div_3_a.style.float = float1;
        var div_3_b = document.getElementById("q"+sectId+"_div_2_c_"+uniqId);
        div_3_b.style.textAlign = tAlign2;
        div_3_b.style.float = float2;
        div_3_b.getElementsByTagName("label")[0].innerHTML = label3Html;

        var div_4_a = document.getElementById("q"+sectId+"_div_1_d_"+uniqId);
        div_4_a.style.textAlign = tAlign1;
        div_4_a.style.float = float1;
        var div_4_b = document.getElementById("q"+sectId+"_div_2_d_"+uniqId);
        div_4_b.style.textAlign = tAlign2;
        div_4_b.style.float = float2;
        div_4_b.getElementsByTagName("label")[0].innerHTML = label4Html;

        var div_5_a = document.getElementById("q"+sectId+"_div_1_e_"+uniqId);
        div_5_a.style.textAlign = tAlign1;
        div_5_a.style.float = float1;
        var div_5_b = document.getElementById("q"+sectId+"_div_2_e_"+uniqId);
        div_5_b.style.textAlign = tAlign2;
        div_5_b.style.float = float2;
        div_5_b.getElementsByTagName("label")[0].innerHTML = label5Html;

        var div_6_a = document.getElementById("q"+sectId+"_div_1_f_"+uniqId);
        div_6_a.style.textAlign = tAlign1;
        div_6_a.style.float = float1;
        var div_6_b = document.getElementById("q"+sectId+"_div_2_f_"+uniqId);
        div_6_b.style.textAlign = tAlign2;
        div_6_b.style.float = float2;
        div_6_b.getElementsByTagName("label")[0].innerHTML = label6Html;

    }else{
        //Quiz options for Quiz type 2:
        var type2_div_a = document.getElementById("q"+sectId+"_div_a_"+uniqId);
        type2_div_a.getElementsByTagName("label")[0].innerHTML = label1Html;

        var type2_div_b = document.getElementById("q"+sectId+"_div_b_"+uniqId);
         type2_div_b.getElementsByTagName("label")[0].innerHTML = label2Html;

         var type2_div_c = document.getElementById("q"+sectId+"_div_c_"+uniqId);
         type2_div_c.getElementsByTagName("label")[0].innerHTML = label3Html;

         var type2_div_d = document.getElementById("q"+sectId+"_div_d_"+uniqId);
         type2_div_d.getElementsByTagName("label")[0].innerHTML = label4Html;

         var type2_div_e = document.getElementById("q"+sectId+"_div_e_"+uniqId);
         type2_div_e.getElementsByTagName("label")[0].innerHTML = label5Html;

         var type2_div_f = document.getElementById("q"+sectId+"_div_f_"+uniqId);
         type2_div_f.getElementsByTagName("label")[0].innerHTML = label6Html;

}
}

function getLanguage(uniqId, sectId,version){
       var lang = "en";
      // var dir =  document.getElementById("q"+sectId+"_"+uniqId).style.direction;
       var dir =document.getElementById("q"+sectId+"_"+uniqId).style.direction;
       if(dir =="rtl"){
          lang = "ar";
       }
       // alert(version);
        showorHideNextPrev();
       // window.WebText.setLanguage(lang);
     if(version == "KITKAT"){
    	return lang.toString();
     } else {
		window.WebText.setLanguage(lang);
     }
        //alert("hello"+lang);
      // window.WebText.setLanguage(lang);
      // return lang;
 }

function getContentDocumentHeight(){
	var height = document.body.offsetHeight;
	window.WebText.setDocumentHeight(height);
}

function getSelectedText(){
   var sel = window.getSelection().toString();
    //alert(sel);
   window.WebText.setSelectedText(sel);
}

//RgbtoHex
function rgb2hex(rgb){
	rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	function hex(x){
		return("0" + parseInt(x).toString(16)).slice(-2);
	}
	return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

//quiz
function selectAllText(){
    document.execCommand('selectAll', false, null);
}

function assignAnswer(inputId, buttonId, radioBtnGroupName, resultDivId){
    var radioBtnGroups = document.getElementsByName(radioBtnGroupName);
    for(var i=0; i<radioBtnGroups.length; i++) {
    				var radioBtn = radioBtnGroups[i];
    				if(radioBtn.checked) {
                        radioBtn.setAttribute('checked', true);
                    } else {
                        radioBtn.removeAttribute('checked');
                    }
    }
    var button = document.getElementById(buttonId);
    button.setAttribute('onclick','checkAnswer("'+inputId+'", "'+buttonId+'", "'+radioBtnGroupName+'", "'+resultDivId+'")');
    var splitDiv = radioBtnGroupName.split("_");
    var uniqueId = splitDiv[1];
    var secIdSplit = splitDiv[0].split("q");
    var secId = secIdSplit[1];
    var btnNextId = 'btnNext'+secId+'_'+uniqueId;
    var btnNext = document.getElementById(btnNextId);
    btnNext.setAttribute('onclick', 'window.location=\'nextquestion:'+radioBtnGroupName+':'+secId+'\';checkAnswer("'+inputId+'", "'+buttonId+'", "'+radioBtnGroupName+'", "'+resultDivId+'");quizNextView("'+radioBtnGroupName+'", \'btnNext"'+radioBtnGroupName+'"\')');
	getWebContent();
}

function showOrHideOptions(noOfOptions, uniqueId, radioBtnGroupName, sectionId){
    var radioBtnGroupLength = document.getElementsByName(radioBtnGroupName).length;
        for (var i=3; i<=radioBtnGroupLength; i++){
            switch (i) {
                case 3:
                    var childElemetId = 'q'+sectionId+'_div_c_'+uniqueId+'';
                    if(i<=noOfOptions){
                        document.getElementById(childElemetId).style.display = 'block';
                    } else {
                        document.getElementById(childElemetId).style.display = 'none';
                    }
                    break;
                case 4:
                    var childElemetId = 'q'+sectionId+'_div_d_'+uniqueId+'';
                    if(i<=noOfOptions){
                        document.getElementById(childElemetId).style.display = 'block';
                    } else {
                        document.getElementById(childElemetId).style.display = 'none';
                    }
                    break;
                case 5:
                    var childElemetId = 'q'+sectionId+'_div_e_'+uniqueId+'';
                    //alert(childElemetId);
                    if(i<=noOfOptions){
                        document.getElementById(childElemetId).style.display = 'block';
                    } else {
                        document.getElementById(childElemetId).style.display = 'none';
                    }
                    break;
                case 6:
                    var childElemetId = 'q'+sectionId+'_div_f_'+uniqueId+'';
                    if(i<=noOfOptions){
                        document.getElementById(childElemetId).style.display = 'block';
                    } else {
                        document.getElementById(childElemetId).style.display = 'none';
                    }
                    break;
            }
        }
}

function embedNewQuiz(embedHtmlText, sectionNo, quesUniqueId){
    //alert(embedHtmlText);
    //var parentElement = document.getElementById('content');
    // alert(sectionNo);
    //var divFragment = create(embedHtmlText);
    //alert(divFragment);
    //parentElement.insertAfter(divFragment, parentElement.childNodes[sectionNo]);
    //parentElement.innerHTML += embedHtmlText;
    var div = $(embedHtmlText);
    var div_id_after = 'q'+sectionNo+'_'+quesUniqueId;
    //alert(div_id_after);
    $('#'+div_id_after).after(div);
    quizNextView(div_id_after);
    showorHideNextPrev();
}

function showorHideNextPrev(){

    var prevBtn = document.getElementsByClassName("btnPrev");
    var nextBtn = document.getElementsByClassName("btnNext");
    var count = prevBtn.length;
    var displayButtons = "hidden";
    if(count > 1){
        displayButtons = "visible";
    }
   // alert("showorHideNextPrev");
    prevBtn[0].style.visibility = displayButtons;
    nextBtn[0].style.visibility = displayButtons;
}
   /*function getSelectedText(){
        var selectedText = window.getSelection();
        return selectedText;
    }*/

function checkAnswer(answerId, buttonId, radioBtnGroupName, resultDivId){
    return;
}

function quizNextView(parentDiv){
    var element = document.getElementById(parentDiv);
    if((element.parentNode.childElementCount - 2) * element.offsetWidth == element.parentNode.scrollLeft) {
        return;
    }
    element.parentNode.scrollLeft += element.offsetWidth;
   // showorHideNextPrev();
}

function quizPreviousView(parentDiv){

    var element = document.getElementById(parentDiv);
    element.parentNode.scrollLeft -= element.offsetWidth;
}

function quizDeleteView(parentDiv){

    var element = document.getElementById(parentDiv);
    var childElementLength = element.parentNode.children.length;
    
    if(childElementLength > 2) {
        quizPreviousView(parentDiv);
        element.parentNode.removeChild(element);
    }
     showorHideNextPrev();
}

function getParentTableBackground(version){

    var htmlBody = document.getElementsByTagName("body")[0];
    var htmlTable = htmlBody.getElementsByTagName("table");
    var Content = "";
    for(var i=0;i<htmlTable.length;i++){

        var currentTable = htmlTable[i];
        var tableAtr = currentTable.getAttribute("background");
        if(tableAtr != null){

            Content = Content + i + "##" + tableAtr + "$$";
            currentTable.setAttribute("background", "none");
        }
        for (var j=0; j<currentTable.rows.length; j++){

            var currentRow = currentTable.rows[j];
            var rowAtr = currentRow.getAttribute("background");
            if(rowAtr != null){

                Content = Content + i + "|" + j + "##" + rowAtr + "$$";
                currentRow.setAttribute("background", "none");
            }

            for (var k=0; k<currentRow.cells.length; k++){

                var currentCell = currentRow.cells[k];

                var cellAtr = currentCell.getAttribute("background");
                if(cellAtr != null){

                    Content = Content + i + "|" + j + "|" + k + "##" + cellAtr + "$$";
                    currentCell.setAttribute("background", "none");
                }
            }
        }
    }
    if(version == "KITKAT"){
    	return Content.toString();
    } else {
		window.WebText.setParentTableBg(Content);
    }

}

function setParentTableBackground(location, path){
    var htmlBody = document.getElementsByTagName("body")[0];
    var htmlTable = htmlBody.getElementsByTagName("table");
    var splitLocation = location.split("|");
    //alert("split location length: "+splitLocation.length);

    if(splitLocation.length == 1){
        var position = parseInt(splitLocation[0]);
        var currentTable = htmlTable[position];
        currentTable.setAttribute("background", path);

    }else if(splitLocation.length == 2){
        var currentTable = htmlTable[splitLocation[0]].rows[splitLocation[1]];
        currentTable.setAttribute("background", path);
    }else if(splitLocation.length == 3){

        var currentTable = htmlTable[splitLocation[0]].rows[splitLocation[1]].cells[splitLocation[2]];
        currentTable.setAttribute("background", path);
    }
}