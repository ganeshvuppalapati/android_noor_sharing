function getWebContent(currentPageNo){
    var WebContent = "";    
    var imageCount= document.body.getElementsByTagName("img").length;
    for(var i=0;i<imageCount;i++){
        var image = document.body.getElementsByTagName("img")[i].getBoundingClientRect();
        var imgWidth = image.width;
        document.body.getElementsByTagName("img")[i].setAttribute("width",imgWidth);
        var imgHeight = image.height;
        document.body.getElementsByTagName("img")[i].setAttribute("height",imgHeight);
    }
    WebContent = document.body.innerHTML;
    //alert(WebContent);
    window.StoreObjects.setWebContent(WebContent, currentPageNo);
}

function tagCoordinates(width, height, currentPageNo){
    var Content = "";
    //var scaleX = 0.784;var scaleY = 0.783;
    var imgTagCount = document.body.getElementsByTagName("img").length;
    //var scaleX = width/document.body.offsetWidth;
    //var scaleY = height/document.body.offsetHeight;
    var scaleX = width/document.body.scrollWidth;
    var scaleY = height/document.body.scrollHeight;
   
    for(var i=0;i<imgTagCount;i++){
        
        var imgSrc = document.getElementsByTagName("img")[i].getAttribute("src");
        var imgElement = document.getElementsByTagName("img")[i].getBoundingClientRect();
        var imgwidth = imgElement.width*scaleX;
        var imgheight =imgElement.height*scaleY;
        var posX = imgElement.left*scaleX;
        var posY = imgElement.top*scaleY;
        //var posX =imgleft;
        //var posY =imgtop;
                
        Content =Content+ posX+"#"+posY+"#"+imgwidth+"#"+imgheight+"#"+imgSrc+"$";
    }
    //alert(Content);
    window.StoreObjects.setTagCoordinates(Content, currentPageNo);
}

function getTableBackground(currentPageNo){
    var Content = "";
    var htmlBody = document.getElementsByTagName("body")[0];
    var htmlTable = htmlBody.getElementsByTagName("table");
    for(var i=0;i<htmlTable.length;i++){
        
        var currentTable = htmlTable[i];
        var tableAtr = currentTable.getAttribute("background");
        if(tableAtr != null){
            
            Content = Content + tableAtr + "$";
        }
        for (var j=0; j<currentTable.rows.length;j++){
            
            var currentRow = currentTable.rows[j];
            var rowAtr = currentRow.getAttribute("background");
            if(rowAtr != null){
                
                Content = Content + rowAtr + "$";
            }
            
            for (var k=0; k<currentRow.cells.length; k++){
                
                var currentCell = currentRow.cells[k];
                
                var cellAtr = currentCell.getAttribute("background");
                if(cellAtr != null){
                    
                    Content = Content + cellAtr + "$";
                }
            }
        }
    }
    //alert(Content);
    window.StoreObjects.setTableBackgroundContent(Content, currentPageNo);
}

function getHTMLDir(){
	var htmlDir = document.getElementsByTagName('html')[0].getAttribute('dir');
	window.StoreObjects.setHtmlDir(htmlDir);
}

function getDocumentOffsetWidthAndHeight(){
	var offsetWidth = document.body.scrollWidth;
	var offsetHeight= document.body.scrollHeight;
	var offsetWidthAndHeight = offsetWidth+"|"+offsetHeight;
	window.StoreObjects.setDocumentOffsetWidthAndHeight(offsetWidthAndHeight);
}

function getElementsTagNameLength(version){
	var length = document.body.getElementsByTagName("*").length;
	if(version == "KITKAT"){
		return length.toString();
	} else {
		window.StoreObjects.setElementsTagNameLength(length);
	}
}

function getElementsClassName(value, version){
	var className = document.body.getElementsByTagName("*")[value].getAttribute("class");
	if(version == "KITKAT"){
		return className.toString();
	} else {
		window.StoreObjects.setElementsClassName(className);
	}
}

function getDivContentHTML(value, version){
	var divContent = document.body.getElementsByTagName("*")[value].innerHTML;
	if(version == "KITKAT"){
		return divContent.toString();
	} else {
		window.StoreObjects.setDivContentHTML(divContent);
	}
}

function getElementXandYPos(value, version){
	var xPos = document.body.getElementsByTagName("*")[value].style.left;
	var yPos = document.body.getElementsByTagName("*")[value].style.top;
	var location = xPos+"|"+yPos;
	if(version == "KITKAT"){
		return location.toString();
	} else {
		window.StoreObjects.setElementXandYPos(location);
	}
}

function getWebTableElementSize(value, version){
	var width = document.body.getElementsByTagName("*")[value].style.width;
	var height = document.body.getElementsByTagName("*")[value].style.height;
	var rowCount = document.body.getElementsByTagName("*")[value].getElementsByTagName("*")[1].rows.length;
	var columnCount = document.body.getElementsByTagName("*")[value].getElementsByTagName("*")[1].rows[0].cells.length;
	var size = width+"|"+height+"|"+rowCount+"|"+columnCount;
	if(version == "KITKAT"){
		return size.toString();
	} else {
		window.StoreObjects.setWebTableElementSize(size);
	}
}

function getWebTextElementSize(value, version) {
	var width = document.body.getElementsByTagName("*")[value].getAttribute("width");
	var height = document.body.getElementsByTagName("*")[value].getAttribute("height");
	var size = width+"|"+height;
	if(version == "KITKAT"){
		return size.toString();
	} else {
		window.StoreObjects.setWebTextElementSize(size);
	}
}

function getImageElementSize(value, version){
	var imgLeft = document.body.getElementsByTagName("*")[value].style.left;
	var imgTop = document.body.getElementsByTagName("*")[value].style.top;
	var width = document.body.getElementsByTagName("*")[value].width;
	var height = document.body.getElementsByTagName("*")[value].height;
	var imgRotationAngle = document.body.getElementsByTagName("*")[value].style.webkitTransform;
	var imgSrc = document.body.getElementsByTagName("*")[value].getAttribute("src");
	var imgElemAttr = imgLeft+"|"+imgTop+"|"+width+"|"+height+"|"+imgRotationAngle+"|"+imgSrc;
	if(version == "KITKAT"){
		return imgElemAttr.toString();
	} else {
		window.StoreObjects.setImageElementSize(imgElemAttr);
	}
}

function getEmbedBoxSize(value, version) {
	var imgLeft = document.body.getElementsByTagName("*")[value].style.left;
	var imgTop = document.body.getElementsByTagName("*")[value].style.top;
	var width = document.body.getElementsByTagName("*")[value].style.width;
	var height = document.body.getElementsByTagName("*")[value].style.height; 
	var embedBoxAttr = imgLeft+"|"+imgTop+"|"+width+"|"+height;
	if(version == "KITKAT"){
		return embedBoxAttr.toString();
	} else {
		window.StoreObjects.setEmbedBoxSize(embedBoxAttr);
	}
}

function getIframeSize(value, version){
	var imgLeft = document.body.getElementsByTagName("*")[value].style.left;
	var imgTop = document.body.getElementsByTagName("*")[value].style.top;
	var width = document.body.getElementsByTagName("*")[value].style.width;
	var height = document.body.getElementsByTagName("*")[value].style.height; 
	var iframeSrc = document.body.getElementsByTagName("*")[value].getElementsByTagName("iframe")[0].getAttribute("src");
	var iframeElemAttr = imgLeft+"#|#"+imgTop+"#|#"+width+"#|#"+height+"#|#"+iframeSrc;
	if(version == "KITKAT"){
		return iframeElemAttr.toString();
	} else {
		window.StoreObjects.setIframeSize(iframeElemAttr);
	}
}

function getYoutubeSize(value, version){
	var imgLeft = document.body.getElementsByTagName("*")[value].style.left;
	var imgTop = document.body.getElementsByTagName("*")[value].style.top;
	var width = document.body.getElementsByTagName("*")[value].style.width;
	var height = document.body.getElementsByTagName("*")[value].style.height; 
	var youtubeSrc = document.body.getElementsByTagName("*")[value].getAttribute("src");
	var youtubeElemAttr = imgLeft+"#|#"+imgTop+"#|#"+width+"#|#"+height+"#|#"+youtubeSrc;
	if(version == "KITKAT"){
		return youtubeElemAttr.toString();
	} else {
		window.StoreObjects.setYoutubeSize(youtubeElemAttr);
	}
}

function getQuizElementLength(value, version){
	var quizElementLength = document.body.getElementsByTagName("*")[value].getElementsByTagName("*")[0].childElementCount;
	if(version == "KITKAT"){
		return quizElementLength.toString();
	} else {
		window.StoreObjects.setQuizElementLength(quizElementLength);
	}
}

function getQuizElementId(iValue, jValue, version) {
	var quizElementId = document.body.getElementsByTagName("*")[iValue].getElementsByTagName("*")[0].children[jValue].id;
	if(version == "KITKAT"){
		return quizElementId.toString();
	} else {
		window.StoreObjects.setQuizElementId(quizElementId);
	}
}

function getImgElemLength(iValue, version){
	var imgElemLength = document.body.getElementsByTagName("*")[iValue].getElementsByTagName("img").length;
	if(version == "KITKAT"){
		return imgElemLength.toString();
	} else {
		window.StoreObjects.setImgElemLength(imgElemLength);
	}
}

function getImgSrcElement(iValue, jValue, version){
	var imgSrc = document.body.getElementsByTagName("*")[iValue].getElementsByTagName("img")[jValue].getAttribute("src");
	if(version == "KITKAT"){
		return imgSrc.toString();
	} else {
		window.StoreObjects.setImgSrcElement(imgSrc);	
	}
}

function getAudioElemContent(iValue, version){
	var imgLeft = document.body.getElementsByTagName("*")[iValue].style.left;
	var imgTop = document.body.getElementsByTagName("*")[iValue].style.top;
	var width = document.body.getElementsByTagName("*")[iValue].width;
	var height = document.body.getElementsByTagName("*")[iValue].height; 
	var audioOnClickAttr = document.body.getElementsByTagName("*")[iValue].getAttribute("onClick");
	var audioElemAttr = imgLeft+"#|#"+imgTop+"#|#"+width+"#|#"+height+"#|#"+audioOnClickAttr;
	if(version == "KITKAT"){
		return audioElemAttr.toString();
	} else {
		window.StoreObjects.setAudioElemContent(audioElemAttr);
	}
}

function getYoutubeHtmlId(iValue, version){
	var youtubeHtmlId = document.body.getElementsByTagName("*")[iValue].getAttribute("id");
	if(version == "KITKAT"){
		return youtubeHtmlId.toString();
	} else {
		window.StoreObjects.setYoutubeHtmlId(youtubeHtmlId);
	}
}
